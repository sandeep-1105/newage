/**
 * PartyMasterOperationCtrl--which is performed update and save operations
 * 
 */
app.controller('PartyMasterOperationCtrl', ['$rootScope', '$scope', '$timeout', '$filter', '$log', 'discardService', 'PartyAccountListByNotInId', 'ValidateUtil', '$http', '$location', 'PartyMasterAdd', 'DivisionList', 'cloneService',
    'PartyMasterEdit', 'AutoMailGroupList', 'AutoMailList', 'ExternalAutoMailList', 'ExternalAutoMailGroupList',
    'CountryList', 'StateList', 'CityList', 'TosSearchKeyword',
    'PartyTypeGetAll', 'SalesmanList', 'CurrencyMasterSearchKeyword', 'LedgerList', 'AccountList',
    'CountryWiseLocation', 'CommodityList', 'CategoryList', 'EmployeeList', 'ServiceList', 'CompanyList',
    'DivisionListByCompany', 'PartyMasterService', 'PartyTypeKeyword', 'PortSearchKeyword', 'ngDialog', 'PartiesList', 'appMetaFactory',
    'ngProgressFactory', 'RecentHistorySaveService', 'PeriodList', 'NavigationService', 'PartyMasterView',
    'PartyMasterGetByCode', '$state', '$stateParams', '$anchorScroll', 'GoToUnfilledFormNavigationService',
    'discardService1', '$modal', 'appConstant', 'CommonValidationService', 'addressJoiner', 'gradeMasterFactory', 'roleConstant',
    function($rootScope, $scope, $timeout, $filter, $log, discardService, PartyAccountListByNotInId, ValidateUtil, $http, $location, PartyMasterAdd, DivisionList, cloneService,
        PartyMasterEdit, AutoMailGroupList, AutoMailList, ExternalAutoMailList, ExternalAutoMailGroupList,
        CountryList, StateList, CityList, TosSearchKeyword,
        PartyTypeGetAll, SalesmanList, CurrencyMasterSearchKeyword, LedgerList, AccountList,
        CountryWiseLocation, CommodityList, CategoryList, EmployeeList, ServiceList, CompanyList,
        DivisionListByCompany, PartyMasterService, PartyTypeKeyword, PortSearchKeyword, ngDialog, PartiesList, appMetaFactory,
        ngProgressFactory, RecentHistorySaveService, PeriodList, NavigationService, PartyMasterView,
        PartyMasterGetByCode, $state, $stateParams, $anchorScroll, GoToUnfilledFormNavigationService,
        discardService1, $modal, appConstant, CommonValidationService, addressJoiner, gradeMasterFactory, roleConstant) {

        $scope.$roleConstant = roleConstant;
        $scope.setOldDataVal = function() {
            $scope.oldData = JSON.stringify($scope.partyMaster);
        }

        $scope.disableSubmitBtn = false;
        $scope.partyFetchConfDlgOpened = false;
        $scope.localDynamicFormFieldMap = new Map();

        $scope.chkAndCallCountryField = function() {

            if ($scope.partyMaster != undefined && $scope.partyMaster != null && $scope.partyMaster.countryMaster != undefined && $scope.partyMaster.countryMaster != null &&
                $scope.partyMaster.countryMaster.id != undefined && $scope.partyMaster.countryMaster.id != null) {
                $scope.getDynamicFormField($scope.partyMaster.countryMaster.id);
            } else {
                $scope.localDynamicFormFieldMap = new Map();
            }
        }
        $scope.enableSubmitBtnFn = function() {
            $timeout(function() {
                console.log("Enable btn ,", new Date());
                $scope.disableSubmitBtn = false;
            }, 500);
        }


        $scope.partyInline = {};

        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('partyMain-panel'));
        $scope.contained_progressbar.setAbsolute();

        $scope.init = function() {

            $scope.moveToScreen = null // which is used for  navigating the link between screens
            console.log("Init Method is called.....");
            $scope.errorMap = new Map();

            $scope.errorShow = {};
            $scope.business = false;

            if ($scope.partyMaster == undefined || $scope.partyMaster == null) {
                $scope.partyMaster = {};
            }
            // Load initially all the party type list...... 

            PartyTypeGetAll.get(function(data) {
                if (data.responseCode == "ERR0") {
                    if (data.responseCode == "ERR0") {

                        if ($stateParams.fromHistory != "Yes" || $stateParams.fromHistory == null) {
                            $scope.tempPartyTypeList = data.responseObject.searchResult;
                        } else {
                            $scope.tempPartyTypeList = $scope.partyMaster.savedTempPartyTypeList;
                        }
                        $scope.isReloaded = localStorage.isPartyReloaded;

                        if (($scope.partyMaster == null || $scope.partyMaster.id == null) &&
                            ($stateParams.fromHistory != "Yes" || $stateParams.fromHistory == null)) {

                            console.log("Init method add page.........!")

                            // Initializing the list inside party master

                            $scope.partyMaster.partyBusinessDetailList = [{
                                bussinessStatus: 'Active'
                            }];
                            $scope.partyMaster.partyCreditLimitList = [{}];
                            $scope.partyMaster.partyAccountList = [{
                                "status": "Active",
                                "isTaxExempted": false
                            }];
                            $scope.partyMaster.partyCompanyAssociateList = [{}];
                            $scope.partyMaster.partyServiceList = [{}];

                            $scope.isReloaded = "NO"

                            $scope.partyMaster.status = 'Active';
                            $scope.partyMaster.defaulter = false;

                            $scope.partyMaster.partyDetail = {};
                            $scope.partyMaster.partyDetail.paymentSchedule = {};
                            $scope.partyMaster.companyMaster = {};
                            $scope.partyCreditLimit = {};
                            $scope.partyMaster.partyDetail.corporate = 'CORPORATE';
                            $scope.partyMaster.partyDetail.matchInvoiceQuoteOutput = false;
                            $scope.partyMaster.partyDetail.isBlanketNomination = false;
                            $scope.partyMaster.partyDetail.directStatusUpdate = false;
                            $scope.partyMaster.partyDetail.billingFscScOnGrossWeight = false;
                            $scope.partyMaster.partyDetail.emailMessageFormat = true;

                            $scope.partyAccountMaster = {};
                            $scope.partyAccountMaster.accountMaster = {};
                            $scope.partyAccountMaster.termCode = "CASH";
                            $scope.partyAccountMaster.status = "Active";
                            $scope.partyAccountMaster.locationMaster = {};
                            $scope.partyAccountMaster.currencyMaster = {};
                            $scope.partyAccountMaster.ledgerCode = {};
                            $scope.partyAccountMaster.ledgerName = {};
                            $scope.partyEmailMapping = {};
                            $scope.partyEmailMapping.imco = false;
                            $scope.listEmail = true;
                            $scope.showChild = true;
                            $scope.agentshow = false;

                            $scope.partyMaster.countryMaster = {};
                            $scope.partyBusinessDetail = {};
                            $scope.partyBusinessDetail.serviceMaster = {};
                            $scope.partyBusinessDetail.commodityMaster = {};
                            $scope.partyBusinessDetail.origin = {};
                            $scope.partyBusinessDetail.destination = {};
                            $scope.partyBusinessDetail.tosMaster = {};
                            $scope.partyBusinessDetail.currentPotential = false;
                            $scope.businessList = true;
                            $scope.showBusinessServiceList = false;
                            $scope.showBusinessTosList = false;
                            $scope.showBusinessoriginList = false;
                            $scope.showBusinessDestinationList = false;
                            $scope.showBusinessCommodityList = false;
                            $scope.showAccountList = false;
                            $scope.showaccountLocationList = false;
                            $scope.showCurrencyList = false;
                            $scope.showLedgerNameList = false;
                            $scope.showLedgerCodeList = false;

                            // flag used for to check party inline  forms are valid at final
                            $scope.partyBusinessValid = true;
                            $scope.partyServiceValid = true;
                            $scope.partyDivisionValid = true;
                            $scope.partyAccountValid = true;
                            $scope.partyCreditValid = true;

                            // Selecting the party type check boxes in the edit page
                            for (var j = 0; j < $scope.tempPartyTypeList.length; j++) {
                                if ($scope.tempPartyTypeList[j].partyTypeCode == 'CS' || $scope.tempPartyTypeList[j].partyTypeName == 'Customer') {
                                    $scope.tempPartyTypeList[j].selected = true;
                                    break;
                                }
                            }

                            $scope.partyIsInd();


                            if ($scope.partyMaster.partyAddressList == null || $scope.partyMaster.partyAddressList.length == 0) {
                                $scope.addAddress();
                                $scope.partyMaster.partyAddressList[0].addressType = 'Primary';
                            }



                        } else {
                            console.log("Init method edit page....................");

                            $scope.isReloaded = "NO";
                            localStorage.isPartyReloaded = "NO";
                            $rootScope.setNavigate3("Edit Party");
                            // flag used for to  check party inlin forms are valid at  final
                            $scope.partyBusinessValid = true;
                            $scope.partyServiceValid = true;
                            $scope.partyDivisionValid = true;
                            $scope.partyAccountValid = true;
                            $scope.partyCreditValid = true;

                            // Selecting the party type check boxes in the edit page
                            if ($scope.partyMaster != undefined && $scope.partyMaster.id != null && $scope.partyMaster.id != undefined) {

                                if ($scope.partyMaster.partyDetail == undefined || $scope.partyMaster.partyDetail == null) {
                                    $scope.partyMaster.partyDetail = {};
                                    $scope.partyMaster.partyDetail.paymentSchedule = {};
                                    $scope.partyMaster.partyDetail.corporate = 'CORPORATE';
                                    $scope.partyMaster.partyDetail.matchInvoiceQuoteOutput = false;
                                    $scope.partyMaster.partyDetail.isBlanketNomination = false;
                                    $scope.partyMaster.partyDetail.directStatusUpdate = false;
                                    $scope.partyMaster.partyDetail.billingFscScOnGrossWeight = false;
                                    $scope.partyMaster.partyDetail.emailMessageFormat = true;
                                }


                                if ($scope.partyMaster.partyTypeList != null) {
                                    for (var i = 0; i < $scope.partyMaster.partyTypeList.length; i++) {
                                        for (var j = 0; j < $scope.tempPartyTypeList.length; j++) {
                                            if ($scope.tempPartyTypeList[j].id == $scope.partyMaster.partyTypeList[i].partyTypeMaster.id) {
                                                $scope.tempPartyTypeList[j].selected = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                            }



                            $scope.businessList = true;



                            if ($scope.partyMaster.partyEmailMappingList != null && $scope.partyMaster.partyEmailMappingList.length != 0) {
                                for (var i = 0; i < $scope.partyMaster.partyEmailMappingList.length; i++) {
                                    if ($scope.partyMaster.partyEmailMappingList[i].partyEmailMessageMappingList != null &&
                                        $scope.partyMaster.partyEmailMappingList[i].partyEmailMessageMappingList.length != 0) {


                                        for (var j = 0; j < $scope.partyMaster.partyEmailMappingList[i].partyEmailMessageMappingList.length; j++) {
                                            $scope.partyMaster.partyEmailMappingList[i].partyEmailMessageMappingList[j].autoMailStatus = $scope.partyMaster.partyEmailMappingList[i].partyEmailMessageMappingList[j].autoMailStatus == 'Enable' ? true : false;
                                        }

                                    }
                                    $scope.partyEmailMappingIndex = i;
                                    $scope.mailArrangeEvent($scope.partyMaster.partyEmailMappingList[i]);
                                }
                                $scope.partyEmailMappingIndex = -1;
                            }


                            if ($scope.partyMaster.partyAccountList != undefined && $scope.partyMaster.partyAccountList != null && $scope.partyMaster.partyAccountList.length != 0) {
                                for (var i = 0; i < $scope.partyMaster.partyAccountList.length; i++) {
                                    $scope.partyMaster.partyAccountList[i].isTaxExempted = $scope.partyMaster.partyAccountList[i].isTaxExempted == 'Yes' ? true : false;
                                }
                            } else {
                                $scope.partyMaster.partyAccountList = [{
                                    "status": "Active",
                                    "isTaxExempted": false
                                }];
                            }


                            // In Edit Time Service Table first row should be open..
                            if ($scope.partyMaster.partyServiceList == undefined || $scope.partyMaster.partyServiceList == null || $scope.partyMaster.partyServiceList.length == 0) {
                                $scope.partyMaster.partyServiceList = [{}];
                            }

                            // In Edit Time Division Table first row should be open..
                            if ($scope.partyMaster.partyCompanyAssociateList == undefined || $scope.partyMaster.partyCompanyAssociateList == null || $scope.partyMaster.partyCompanyAssociateList.length == 0) {
                                $scope.partyMaster.partyCompanyAssociateList = [{}];
                            }

                            // In Edit Time Credit Table first row should be open..
                            if ($scope.partyMaster.partyCreditLimitList == undefined || $scope.partyMaster.partyCreditLimitList == null || $scope.partyMaster.partyCreditLimitList.length == 0) {
                                $scope.partyMaster.partyCreditLimitList = [{}];
                            }




                            if ($scope.partyMaster.partyBusinessDetailList != undefined && $scope.partyMaster.partyBusinessDetailList != null && $scope.partyMaster.partyBusinessDetailList.length > 0) {
                                for (var i = 0; i < $scope.partyMaster.partyBusinessDetailList.length; i++) {
                                    if ($scope.partyMaster.partyBusinessDetailList[i].currentPotential == 'Current') {
                                        $scope.partyMaster.partyBusinessDetailList[i].currentPotential = true;
                                    } else if ($scope.partyMaster.partyBusinessDetailList[i].currentPotential == 'Potential') {
                                        $scope.partyMaster.partyBusinessDetailList[i].currentPotential = false;
                                    }
                                }
                            } else {
                                $scope.partyMaster.partyBusinessDetailList = [{
                                    bussinessStatus: 'Active'
                                }];
                            }


                            $scope.partyIsInd();
                            if ($scope.partyMaster.partyDetail != null) {
                                $scope.partyMaster.partyDetail.matchInvoiceQuoteOutput = $scope.partyMaster.partyDetail.matchInvoiceQuoteOutput == 'TRUE' ? true : false;
                                $scope.partyMaster.partyDetail.isBlanketNomination = $scope.partyMaster.partyDetail.isBlanketNomination == 'TRUE' ? true : false;
                                $scope.partyMaster.partyDetail.directStatusUpdate = $scope.partyMaster.partyDetail.directStatusUpdate == 'TRUE' ? true : false;

                            }


                            /*
                             * Initially the first contact should be open
                             */
                            $scope.addressIndex = 0;

                            /*
                             * Initially the first contact should be open
                             */
                            $scope.selectedContact = 0;
                        }


                        if ($scope.partyMaster.contactList == null || $scope.partyMaster.contactList == undefined || $scope.partyMaster.contactList == "") {
                            $scope.addPartyContact();
                        }

                        if ($scope.partyMaster.partyEmailMappingList == null || $scope.partyMaster.partyEmailMappingList == undefined ||
                            $scope.partyMaster.partyEmailMappingList == "") {
                            $scope.addEmailRow();
                        }

                        $rootScope.navigateToNextField('partyName')
                        $scope.setOldDataVal();
                        $scope.openDefaultTab();
                    } else {
                        console.error('Error while fetching Party Type List');
                    }
                } else {
                    console.log("Party view Failed ", data.responseDescription)
                }
            }, function(error) {
                console.log("Error while fetching Party Type List : ", error)
            });
        };

        $(".toggle-pin").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });


        $scope.focusGoTo = function(currentField, nextField, index) {
            if (currentField == 'bankDLRCode' + index) {
                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster != null &&
                    $scope.partyMaster.countryMaster.countryCode != 'US') {
                    $rootScope.navigateToNextField(nextField);
                }
            } else if (currentField == 'knownShipperValidationDate' + index) {
                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster != null &&
                    $scope.partyMaster.countryMaster.countryCode == 'US') {
                    $scope.accessHasFocus('editifsBtn', nextField + index)
                }
            } else if (currentField == 'importExpirationDate' + index) {
                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster != null &&
                    $scope.partyMaster.countryMaster.countryCode == 'US') {
                    $scope.accessHasFocus('editaesBtn', nextField + index)
                }
            }
        }



        $scope.openDefaultTab = function() {

            if ($scope.partyMaster.id == undefined) {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ADDRESS_CREATE)) {
                    $scope.partyTab = 'address';
                    $scope.editmoreAddressTab = 'editmoreInfo';
                    $scope.addressIndex = 0;
                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                        if ($scope.partyMaster.partyAddressList[i].addressType == 'Primary') {
                            $scope.addressIndex = i;
                            $scope.editmoreAddressTab = 'editmoreInfo';
                            break;
                        }
                    }
                }
            } else {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ADDRESS_MODIFY)) {
                    $scope.partyTab = 'address';
                    $scope.editmoreAddressTab = 'editmoreInfo';
                    $scope.addressIndex = 0;
                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                        if ($scope.partyMaster.partyAddressList[i].addressType == 'Primary') {
                            $scope.addressIndex = i;
                            $scope.editmoreAddressTab = 'editmoreInfo';
                            break;
                        }
                    }
                }
            }
        }

        $scope.cancel = function() {

            if ($scope.oldData != JSON.stringify($scope.partyMaster)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn " ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 && $scope.partyMaster.id == null) {
                                $scope.save();
                            } else if (value == 1 && $scope.partyMaster.id != null) {
                                $scope.update();
                            } else if (value == 2) {
                                var param = {
                                    partyId: $scope.partyMaster.id,
                                    submitAction: "Cancelled"
                                };
                                if ($stateParams.nav_src_bkref_key != undefined &&
                                    $stateParams.nav_src_bkref_key != null &&
                                    $stateParams.nav_src_bkref_key != "" &&
                                    $rootScope.unfinishedFormHistoryList != undefined &&
                                    $rootScope.unfinishedFormHistoryList != null &&
                                    $rootScope.unfinishedFormHistoryList.length > 0) {
                                    for (var rchIndex = 0; rchIndex < $rootScope.unfinishedFormHistoryList.length; rchIndex++) {
                                        if ($rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != undefined &&
                                            $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != null &&
                                            $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key == $stateParams.nav_src_bkref_key) {
                                            console.log($rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key, " ======= ", $stateParams.nav_src_bkref_key);
                                            var controlParams = {
                                                forObj: $stateParams.forObj,
                                                submitAction: "Cancelled"
                                            };

                                            if ($scope.partyMaster.id == undefined ||
                                                $scope.partyMaster.id == null) {
                                                $rootScope.naviPartyMaster = {};
                                                GoToUnfilledFormNavigationService
                                                    .goToState($rootScope.unfinishedFormHistoryList[rchIndex], controlParams);
                                                break;
                                            } else {
                                                PartyMasterView.get({
                                                        id: $scope.partyMaster.id
                                                    },
                                                    function(data) {
                                                        if (data.responseCode == "ERR0") {
                                                            $rootScope.naviPartyMaster = data.responseObject;
                                                            GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[rchIndex], controlParams);
                                                        }
                                                    },
                                                    function(error) {
                                                        console.log("Party view Failed : ", error)
                                                    });
                                                break;
                                            }

                                        }
                                    }

                                } else {
                                    $state.go("layout.party", param);
                                }

                            } else {
                                console.log("Cancelled was cancel")
                            }
                        });

            } else {
                var loc = NavigationService.getLocation();
                if (loc != undefined && loc != null) {
                    NavigationService.setResultObj($scope.partyMaster);
                    $state.go(loc);
                } else {

                    var param = {
                        partyId: $scope.partyMaster.id,
                        submitAction: "Cancelled"
                    };
                    if ($stateParams.nav_src_bkref_key != undefined &&
                        $stateParams.nav_src_bkref_key != null &&
                        $stateParams.nav_src_bkref_key != "" &&
                        $rootScope.unfinishedFormHistoryList != undefined &&
                        $rootScope.unfinishedFormHistoryList != null &&
                        $rootScope.unfinishedFormHistoryList.length > 0) {
                        for (var rchIndex = 0; rchIndex < $rootScope.unfinishedFormHistoryList.length; rchIndex++) {
                            if ($rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != undefined &&
                                $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != null &&
                                $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key == $stateParams.nav_src_bkref_key) {
                                console
                                    .log(
                                        $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key,
                                        " ======= ",
                                        $stateParams.nav_src_bkref_key);
                                var controlParams = {
                                    forObj: $stateParams.forObj,
                                    submitAction: "Cancelled"
                                };

                                if ($stateParams.category != undefined &&
                                    $stateParams.category != null &&
                                    $stateParams.category == "Shipment") {
                                    controlParams.serIndx = $stateParams.serIndx;
                                    controlParams.docIndx = $stateParams.docIndx;
                                    controlParams.selectedTab = $stateParams.selectedTab;

                                }
                                if ($scope.partyMaster.id == undefined ||
                                    $scope.partyMaster.id == null) {
                                    $rootScope.naviPartyMaster = {};
                                    GoToUnfilledFormNavigationService
                                        .goToState(
                                            $rootScope.unfinishedFormHistoryList[rchIndex],
                                            controlParams);
                                    break;
                                } else {
                                    PartyMasterView
                                        .get({
                                                id: $scope.partyMaster.id
                                            },
                                            function(data) {
                                                if (data.responseCode == "ERR0") {

                                                    $rootScope.naviPartyMaster = data.responseObject;
                                                    GoToUnfilledFormNavigationService
                                                        .goToState(
                                                            $rootScope.unfinishedFormHistoryList[rchIndex],
                                                            controlParams);

                                                }

                                            },
                                            function(error) {
                                                console
                                                    .log("Party view Failed : " +
                                                        error)
                                            });
                                    break;
                                }
                            }
                        }

                    } else {
                        $state.go("layout.party", param);
                    }
                }
            }
        };

        /* $scope.showFrequencyList = false;

         $scope.frequencylistConfig = {
             search: true,
             showCode: true,
             ajax: true,
             columns: [{
                 "title": "frequencyName",
                 seperator: false
             }, {
                 "title": "frequencyCode",
                 seperator: true
             }]
         }

         $scope.showFrequency = function() {
             $scope.selectedItem = -1;
             $scope.panelTitle = "Payment Schedule";
             $scope.ajaxFrequencyEvent(null);
         };*/


        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }

        $scope.ajaxPartyEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            if ($stateParams.partyId != undefined && $stateParams.partyId != null && $stateParams.partyId.trim().length > 0) {
                $scope.searchDto.id = $stateParams.partyId
            }
            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.shipmentPartyList = data.responseObject.searchResult;
                        return $scope.shipmentPartyList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }


        $scope.selectedParty = function(obj, nxtId) {
            console.log("selectedShipmentParty is running", obj);
            if (!$scope.partyFetchConfDlgOpened) {
                if (obj != undefined && obj != null && obj.id != undefined && obj.id != null) {
                    $scope.partyMaster.partyName = obj.partyName;

                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR204"];
                    newScope.objId = obj.id;
                    $scope.partyFetchConfDlgOpened = true;
                    ngDialog
                        .openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                '<button type="button" class="btn btn-default btn-property cancel-btn " ng-click="confirm(-1)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn" ng-click="confirm(' + newScope.objId + ')">Yes</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                $scope.partyFetchConfDlgOpened = false;
                                console.log("Id Value : " + value);
                                if (value == -1) {
                                    $rootScope.navigateToNextField(nxtId);
                                } else {
                                    $state.go("layout.editParty", {
                                        partyId: value,
                                        submitAction: "Cancelled"
                                    });
                                }
                            });


                }
            }
        };

        $scope.ajaxFrequencyEvent = function(object) {
            console.log("showing list of frequency...", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PeriodList.get().$promise.then(function(data,
                status) {
                if (data.responseCode == "ERR0") {
                    $scope.frequencyList = data.responseObject;
                    return $scope.frequencyList;
                    // $scope.showFrequencyList=true;
                }
            }, function(errResponse) {
                console.error('Error while fetching Frequency');
            });

        }

        $scope.selectedFrequency = function(f1, f2, f3) {
            if ($scope.validatePartyMaster(18)) {
                if ($scope.localDynamicFormFieldMap.get('Party Master > CAF Percentage') == true || $scope.localDynamicFormFieldMap.get('Party Master > CAF Percentage') == 'true') {
                    nextFieldId = "caf";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > CC Percentage') == true || $scope.localDynamicFormFieldMap.get('Party Master > CC Percentage') == 'true') {
                    nextFieldId = "cc";
                } else {
                    nextFieldId = "fscor";
                }
                $rootScope.navigateToNextField(nextFieldId);
            }
        };

        /* $scope.cancelFrequency = function() {
             console.log("Canceling Frequency List..");
             $scope.showFrequencyList = false;
         }*/

        $scope.showResponse = function(partyMaster, isAdded) {
            var newScope = $scope.$new();
            var partyCode = partyMaster.partyCode;
            newScope.errorMessage = isAdded == true ? $rootScope.nls["ERR210"] :
                $rootScope.nls["ERR211"];
            if (newScope.errorMessage != undefined) {
                newScope.errorMessage = newScope.errorMessage
                    .replace("%s", partyCode);
            }

            ngDialog
                .openConfirm({
                    template: '<p>{{errorMessage}}</p> ' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                        '</button>' + '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        var stateCount = 0;
                        if ($stateParams.count != undefined &&
                            $stateParams.count != null) {
                            stateCount = $stateParams.count + 1;
                        }
                        var param = {
                            partyId: partyMaster.id,
                            submitAction: "Saved",
                            count: stateCount
                        };
                        if ($stateParams.nav_src_bkref_key != undefined &&
                            $stateParams.nav_src_bkref_key != null &&
                            $stateParams.nav_src_bkref_key != "" &&
                            $rootScope.unfinishedFormHistoryList != undefined &&
                            $rootScope.unfinishedFormHistoryList != null &&
                            $rootScope.unfinishedFormHistoryList.length > 0) {

                            PartyMasterView.get({
                                    id: partyMaster.id
                                },
                                function(data) {
                                    if (data.responseCode == "ERR0") {
                                        for (var rchIndex = 0; rchIndex < $rootScope.unfinishedFormHistoryList.length; rchIndex++) {

                                            if ($rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != undefined &&
                                                $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != null &&
                                                $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key == $stateParams.nav_src_bkref_key) {

                                                var controlParams = {
                                                    forObj: $stateParams.forObj,
                                                    submitAction: "Saved"
                                                };

                                                if ($stateParams.category != undefined &&
                                                    $stateParams.category != null &&
                                                    $stateParams.category == "Shipment") {
                                                    controlParams.serIndx = $stateParams.serIndx;
                                                    controlParams.docIndx = $stateParams.docIndx;
                                                    controlParams.selectedTab = $stateParams.selectedTab;

                                                }
                                                $rootScope.naviPartyMaster = data.responseObject;
                                                GoToUnfilledFormNavigationService
                                                    .goToState(
                                                        $rootScope.unfinishedFormHistoryList[rchIndex],
                                                        controlParams);
                                                break;
                                            }
                                        }

                                    } else {
                                        console
                                            .log("Party view Failed " +
                                                data.responseDescription)
                                    }
                                },
                                function(error) {
                                    console.log("Party view Failed : " +
                                        error)
                                });



                        } else {
                            $state.go("layout.editParty",
                                param);
                        }

                    },
                    function(value) {
                        console.log("deleted cancelled");

                    });
        }

        $scope.save = function() {
            console.log("Save Button is pressed.......");

            try {
                if (!$scope.disableSubmitBtn) {
                    $scope.disableSubmitBtn = true;
                    if ($scope.validatePartyMaster(0)) {

                        $rootScope.clientMessage = null;
                        $scope.errorMap = new Map();

                        // Validating Address Before Save
                        for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                            if ($scope.validatePartyAddressMaster(0, $scope.partyMaster.partyAddressList[i], i) == false) {
                                $scope.partyTab = 'address';
                                $scope.editAddress(i);
                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }

                        if ($scope.partyMaster.contactList != undefined && $scope.partyMaster.contactList != null && $scope.partyMaster.contactList.length > 0) {

                            for (var i = 0; i < $scope.partyMaster.contactList.length; i++) {

                                if (isContactEmptyRow($scope.partyMaster.contactList[i])) {

                                    $scope.partyMaster.contactList.splice(i, 1);
                                    delete $scope.partyMaster.contactList[i];

                                } else {
                                    if ($scope.validatePartyContact(0, $scope.partyMaster.contactList[i], i) == false) {
                                        $scope.partyTab = 'contactTab';
                                        $scope.chooseContact(i);
                                        $scope.enableSubmitBtnFn();
                                        return false;
                                    }
                                }
                            }

                        }

                        if ($scope.partyMaster.partyEmailMappingList != undefined &&
                            $scope.partyMaster.partyEmailMappingList != null &&
                            $scope.partyMaster.partyEmailMappingList.length > 0) {
                            for (var i = 0; i < $scope.partyMaster.partyEmailMappingList.length; i++) {

                                if (isEmailMappingEmptyRow($scope.partyMaster.partyEmailMappingList[i])) {

                                    $scope.partyMaster.partyEmailMappingList.splice(i, 1);
                                    delete $scope.partyMaster.partyEmailMappingList[i];

                                } else {
                                    if ($scope
                                        .validatePartyEmailMapping(
                                            0,
                                            $scope.partyMaster.partyEmailMappingList[i],
                                            i) == false) {
                                        $scope.partyTab = 'mailTab';
                                        $scope.editPartyEmailMApping(i);
                                        $scope.enableSubmitBtnFn();
                                        return false;
                                    }
                                }
                            }
                        }

                        if ($scope.partyMaster.partyServiceList != undefined &&
                            $scope.partyMaster.partyServiceList != null &&
                            $scope.partyMaster.partyServiceList.length > 0) {
                            if (!$scope.partyInline.partyServiceValid()) {
                                console.log("party Service invalid")
                                $scope.partyTab = 'mapping';
                                // $scope.errorMap.put("servicemappinginvalid",$rootScope.nls["ERR6082"]);
                                $rootScope.clientMessage = $rootScope.nls["ERR6082"];
                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }

                        if ($scope.partyMaster.partyCompanyAssociateList != undefined &&
                            $scope.partyMaster.partyCompanyAssociateList != null &&
                            $scope.partyMaster.partyCompanyAssociateList.length > 0) {
                            if (!$scope.partyInline.partyDivisionValid()) {
                                console.log("division invalid failed")
                                $scope.partyTab = 'mapping';
                                $rootScope.clientMessage = $rootScope.nls["ERR6078"];
                                $scope.enableSubmitBtnFn();
                                // $scope.errorMap.put("divisionInvalid",$rootScope.nls["ERR6078"]);
                                return false;
                            }
                        }

                        if ($scope.partyMaster.partyAccountList != undefined &&
                            $scope.partyMaster.partyAccountList != null &&
                            $scope.partyMaster.partyAccountList.length > 0) {
                            if (!$scope.partyInline.partyAccountValid()) {
                                console.log("account invalid")
                                $scope.partyTab = 'accountsTab';
                                $rootScope.clientMessage = $rootScope.nls["ERR6079"];
                                $scope.enableSubmitBtnFn();
                                // $scope.errorMap.put("accountsTabInvalid",$rootScope.nls["ERR6079"]);
                                return false;
                            }

                            if ($scope.partyMaster.partyAccountList.length > 1) {

                                if ($scope
                                    .checkDUplicate($scope.partyMaster.partyAccountList)) {
                                    console.log("partyaccountsDuplicated");
                                    $scope.partyTab = 'accountsTab';
                                    $scope.enableSubmitBtnFn();
                                    return false;

                                }
                            }
                        }

                        if ($scope.partyMaster.partyCreditLimitList != undefined &&
                            $scope.partyMaster.partyCreditLimitList != null &&
                            $scope.partyMaster.partyCreditLimitList.length > 0) {
                            if (!$scope.partyInline.partyCreditValid()) {
                                console.log("credit invalid")
                                $scope.partyTab = 'creditTab';
                                // $scope.errorMap.put("creditTabInvalid",$rootScope.nls["ERR6080"]);
                                $rootScope.clientMessage = $rootScope.nls["ERR6080"];
                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }

                        if ($scope.partyMaster.partyBusinessDetailList != undefined &&
                            $scope.partyMaster.partyBusinessDetailList != null &&
                            $scope.partyMaster.partyBusinessDetailList.length > 0) {

                            if (!$scope.partyInline.partyBusinessValid()) {
                                console.log("business invalid")
                                $scope.partyTab = 'businessTab';
                                // $scope.errorMap.put("businessInvalid",$rootScope.nls["ERR6076"]);
                                $rootScope.clientMessage = $rootScope.nls["ERR6076"];
                                $scope.enableSubmitBtnFn();
                                return false;
                            }

                            if ($scope.partyMaster.partyBusinessDetailList.length > 1)
                                if ($scope
                                    .checkBusinessDUplicate($scope.partyMaster.partyBusinessDetailList)) {

                                    $scope.enableSubmitBtnFn();
                                    return false;

                                }

                        }


                        if ($scope.partyMaster.partyCreditLimitList != undefined &&
                            $scope.partyMaster.partyCreditLimitList != null &&
                            $scope.partyMaster.partyCreditLimitList.length > 0) {
                            if ($scope
                                .checkCreditDUplicate($scope.partyMaster.partyCreditLimitList)) {

                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }

                        if ($scope.partyMaster.partyCompanyAssociateList.length == 0)
                            $scope.partyMaster.partyCompanyAssociateList = [];

                        if ($scope.partyMaster.partyServiceList.length == 0)
                            $scope.partyMaster.partyServiceList = [];

                        if ($scope.partyMaster.partyAccountList.length == 0)
                            $scope.partyMaster.partyAccountList = [];

                        if ($scope.partyMaster.partyBusinessDetailList.length == 0)
                            $scope.partyMaster.partyBusinessDetailList = [];

                        if ($scope.partyMaster.partyCreditLimitList == undefined || $scope.partyMaster.partyCreditLimitList == null || $scope.partyMaster.partyCreditLimitList.length == 0)
                            $scope.partyMaster.partyCreditLimitList = [];

                        $scope.contained_progressbar.start();

                        $scope.tmpObj = cloneService
                            .clone($scope.partyMaster);
                        $scope.tmpObj.partyTypeList = [];
                        for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {
                            if ($scope.tempPartyTypeList[i].selected) {
                                $scope.partyAssociateToPartyTypeMaster = {};
                                $scope.partyAssociateToPartyTypeMaster.partyTypeMaster = $scope.tempPartyTypeList[i];
                                $scope.tmpObj.partyTypeList
                                    .push($scope.partyAssociateToPartyTypeMaster);
                            }
                        }

                        for (var i = 0; i < $scope.tmpObj.partyAddressList.length; i++) {



                            if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate != undefined &&
                                $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate != null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate = $rootScope
                                    .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate);
                            } else {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate = null;
                            }
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth != undefined &&
                                $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth != null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth = $rootScope
                                    .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth);
                            } else {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth = null;
                            }
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate != undefined &&
                                $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate != null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate = $rootScope
                                    .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate);
                            } else {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate = null;
                            }
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate != undefined &&
                                $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate != null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate = $rootScope
                                    .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate);
                            } else {
                                $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate = null;
                            }

                        }

                        if ($scope.tmpObj.partyAccountList != null &&
                            $scope.tmpObj.partyAccountList.length != 0) {
                            for (var i = 0; i < $scope.tmpObj.partyAccountList.length; i++) {
                                $scope.tmpObj.partyAccountList[i].isTaxExempted = $scope.tmpObj.partyAccountList[i].isTaxExempted ? 'Yes' :
                                    'No';
                            }

                        }
                        $scope.tmpObj.partyDetail.matchInvoiceQuoteOutput = $scope.tmpObj.partyDetail.matchInvoiceQuoteOutput ? 'TRUE' :
                            'FALSE';
                        $scope.tmpObj.partyDetail.isBlanketNomination = $scope.tmpObj.partyDetail.isBlanketNomination ? 'TRUE' :
                            'FALSE';
                        $scope.tmpObj.partyDetail.directStatusUpdate = $scope.tmpObj.partyDetail.directStatusUpdate ? 'TRUE' :
                            'FALSE';


                        $scope.tmpObj.openDate = $rootScope
                            .sendApiStartDateTime(new Date());

                        if ($scope.tmpObj.partyBusinessDetailList != null) {
                            for (var i = 0; i < $scope.tmpObj.partyBusinessDetailList.length; i++) {
                                if ($scope.tmpObj.partyBusinessDetailList[i].currentPotential == true) {
                                    $scope.tmpObj.partyBusinessDetailList[i].currentPotential = "Current";
                                } else if ($scope.tmpObj.partyBusinessDetailList[i].currentPotential == false) {
                                    $scope.tmpObj.partyBusinessDetailList[i].currentPotential = "Potential";
                                }
                                if ($scope.tmpObj.partyBusinessDetailList[i].closureDate != undefined &&
                                    $scope.tmpObj.partyBusinessDetailList[i].closureDate != "" &&
                                    $scope.tmpObj.partyBusinessDetailList[i].closureDate != null) {
                                    $scope.tmpObj.partyBusinessDetailList[i].closureDate = $rootScope
                                        .sendApiStartDateTime($scope.tmpObj.partyBusinessDetailList[i].closureDate);
                                }

                            }
                        }

                        if ($scope.tmpObj.partyEmailMappingList != null &&
                            $scope.tmpObj.partyEmailMappingList.length != 0) {
                            for (var i = 0; i < $scope.tmpObj.partyEmailMappingList.length; i++) {

                                $scope.tmpObj.partyEmailMappingList[i].originCountry = $scope.checkObjectHavingId($scope.tmpObj.partyEmailMappingList[i].originCountry);
                                $scope.tmpObj.partyEmailMappingList[i].destinationCountry = $scope.checkObjectHavingId($scope.tmpObj.partyEmailMappingList[i].destinationCountry);


                                $scope.tmpObj.partyEmailMappingList[i].companyMaster = $rootScope.userProfile.selectedCompany;
                                $scope.tmpObj.partyEmailMappingList[i].countryMaster = $scope.tmpObj.partyEmailMappingList[i].locationMaster.countryMaster;

                                $scope.tmpObj.partyEmailMappingList[i].imco = $scope.tmpObj.partyEmailMappingList[i].imco ? 'Yes' :
                                    'No';

                                if ($scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList != null &&
                                    $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList.length != 0) {

                                    for (var j = 0; j < $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList.length; j++) {
                                        $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList[j].autoMailStatus = $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList[j].autoMailStatus == true ? 'Enable' :
                                            'Disable';
                                    }

                                } else {
                                    $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList = null;
                                }

                            }

                        } else {
                            $scope.tmpObj.partyEmailMappingList = null;
                        }

                        if ($scope.tmpObj.partyEmailMappingList != null && $scope.tmpObj.partyEmailMappingList.length > 1 && $scope.partyEmailDuplicate($scope.tmpObj.partyEmailMappingList)) {
                            $scope.enableSubmitBtnFn();
                            $scope.partyTab = 'mailTab';
                            return false;
                        }
                        if ($scope.tmpObj.contactList != null &&
                            $scope.tmpObj.contactList.length != 0) {
                            for (var i = 0; i < $scope.tmpObj.contactList.length; i++) {
                                if ($scope.tmpObj.contactList[i].relationList != null &&
                                    $scope.tmpObj.contactList[i].relationList != undefined && $scope.tmpObj.contactList[i].relationList != "") {
                                    for (var j = 0; j < $scope.tmpObj.contactList[i].relationList.length; j++) {
                                        $scope.tmpObj.contactList[i].relationList[j].personalEventDate = $rootScope
                                            .sendApiStartDateTime($scope.tmpObj.contactList[i].relationList[j].personalEventDate);
                                    }
                                }
                            }
                        } else {
                            $scope.tmpObj.contactList = null;
                        }

                        PartyMasterAdd.save($scope.tmpObj).$promise
                            .then(
                                function(data) {
                                    if (data.responseCode == 'ERR0') {
                                        $scope
                                            .showResponse(
                                                data.responseObject,
                                                true);
                                        $scope.enableSubmitBtnFn();

                                    } else {
                                        console
                                            .log("Party Master added from Server Side Filed :  " +
                                                data.responseDescription)
                                        $scope.enableSubmitBtnFn();
                                    }

                                    $scope.contained_progressbar
                                        .complete();
                                    angular.element(".panel-body")
                                        .animate({
                                            scrollTop: 0
                                        }, "slow");
                                    $scope.enableSubmitBtnFn();
                                },
                                function(error) {
                                    console
                                        .log("Party Master Addition Failed : " +
                                            error)
                                    $scope.enableSubmitBtnFn();
                                });
                    } else {
                        console.log("Validation Failed");
                        $scope.enableSubmitBtnFn();

                    }
                }
            } catch (err) {
                //console.error("Exception :: ", err);
                $scope.enableSubmitBtnFn();

            }
        }

        $scope.update = function() {

            console.log("UPdate Button is pressed.......");

            if (!$scope.disableSubmitBtn) {
                $scope.disableSubmitBtn = true;
                if ($scope.validatePartyMaster(0)) {
                    $scope.errorMap = new Map();

                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                        if ($scope.validatePartyAddressMaster(0, $scope.partyMaster.partyAddressList[i], i) == false) {
                            $scope.partyTab = 'address';
                            $scope.editAddress(i);
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                    }

                    if ($scope.partyMaster.contactList != undefined && $scope.partyMaster.contactList != null && $scope.partyMaster.contactList.length > 0) {

                        for (var i = 0; i < $scope.partyMaster.contactList.length; i++) {

                            if (isContactEmptyRow($scope.partyMaster.contactList[i])) {
                                $scope.partyMaster.contactList.splice(i, 1);
                                delete $scope.partyMaster.contactList[i];
                            } else {
                                if ($scope.validatePartyContact(0, $scope.partyMaster.contactList[i], i) == false) {
                                    $scope.partyTab = 'contactTab';
                                    $scope.chooseContact(i);
                                    $scope.enableSubmitBtnFn();
                                    return false;
                                }
                            }
                        }

                    }


                    if ($scope.partyMaster.partyEmailMappingList != undefined &&
                        $scope.partyMaster.partyEmailMappingList != null &&
                        $scope.partyMaster.partyEmailMappingList.length > 0) {
                        for (var i = 0; i < $scope.partyMaster.partyEmailMappingList.length; i++) {

                            if (isEmailMappingEmptyRow($scope.partyMaster.partyEmailMappingList[i])) {
                                $scope.partyMaster.partyEmailMappingList.splice(i, 1);
                                delete $scope.partyMaster.partyEmailMappingList[i];
                            } else {
                                if ($scope.validatePartyEmailMapping(0, $scope.partyMaster.partyEmailMappingList[i], i) == false) {
                                    $scope.partyTab = 'mailTab';
                                    $scope.editPartyEmailMApping(i);
                                    $scope.enableSubmitBtnFn();
                                    return false;
                                }
                            }
                        }
                    }



                    if ($scope.partyMaster.partyServiceList != undefined &&
                        $scope.partyMaster.partyServiceList != null &&
                        $scope.partyMaster.partyServiceList.length > 0) {
                        if (!$scope.partyInline.partyServiceValid()) {
                            console.log("party Service invalid")
                            $scope.partyTab = 'mapping';
                            // $scope.errorMap.put("servicemappinginvalid",$rootScope.nls["ERR6082"]);
                            $rootScope.clientMessage = $rootScope.nls["ERR6082"];
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                    }

                    if ($scope.partyMaster.partyCompanyAssociateList != undefined &&
                        $scope.partyMaster.partyCompanyAssociateList != null &&
                        $scope.partyMaster.partyCompanyAssociateList.length > 0) {
                        if (!$scope.partyInline.partyDivisionValid()) {
                            console.log("division invalid failed")
                            $scope.partyTab = 'mapping';
                            $rootScope.clientMessage = $rootScope.nls["ERR6078"];
                            $scope.enableSubmitBtnFn();
                            // $scope.errorMap.put("divisionInvalid",$rootScope.nls["ERR6078"]);
                            return false;
                        }
                    }

                    if ($scope.partyMaster.partyAccountList != undefined &&
                        $scope.partyMaster.partyAccountList != null &&
                        $scope.partyMaster.partyAccountList.length > 0) {
                        if (!$scope.partyInline.partyAccountValid()) {
                            console.log("account invalid")
                            $scope.partyTab = 'accountsTab';
                            $rootScope.clientMessage = $rootScope.nls["ERR6079"];
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.partyMaster.partyAccountList.length > 1) {

                            if ($scope
                                .checkDUplicate($scope.partyMaster.partyAccountList)) {
                                console.log("partyaccountsDuplicated");
                                $scope.partyTab = 'accountsTab';
                                $scope.enableSubmitBtnFn();
                                return false;

                            }
                        }
                    }

                    if ($scope.partyMaster.partyCreditLimitList != undefined &&
                        $scope.partyMaster.partyCreditLimitList != null &&
                        $scope.partyMaster.partyCreditLimitList.length > 0) {
                        if (!$scope.partyInline.partyCreditValid()) {
                            console.log("credit invalid")
                            $scope.partyTab = 'creditTab';
                            $rootScope.clientMessage = $rootScope.nls["ERR6080"];
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                    }

                    if ($scope.partyMaster.partyBusinessDetailList != undefined &&
                        $scope.partyMaster.partyBusinessDetailList != null &&
                        $scope.partyMaster.partyBusinessDetailList.length > 0) {

                        if (!$scope.partyInline.partyBusinessValid()) {
                            console.log("business invalid")
                            $scope.partyTab = 'businessTab';
                            $rootScope.clientMessage = $rootScope.nls["ERR6076"];
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.partyMaster.partyBusinessDetailList.length > 1)
                            if ($scope
                                .checkBusinessDUplicate($scope.partyMaster.partyBusinessDetailList)) {

                                $scope.enableSubmitBtnFn();
                                return false;

                            }

                    }

                    if ($scope.partyMaster.partyCreditLimitList != undefined &&
                        $scope.partyMaster.partyCreditLimitList != null &&
                        $scope.partyMaster.partyCreditLimitList.length > 0) {
                        if ($scope
                            .checkCreditDUplicate($scope.partyMaster.partyCreditLimitList)) {

                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                    }
                    if ($scope.partyMaster.partyEmailMappingList != null && $scope.partyMaster.partyEmailMappingList.length > 1 && $scope.partyEmailDuplicate($scope.partyMaster.partyEmailMappingList)) {
                        $scope.enableSubmitBtnFn();
                        $scope.partyTab = 'mailTab';
                        return false;
                    }


                    $rootScope.clientMessage = null;

                    if ($scope.partyMaster.partyCreditLimitList == undefined || $scope.partyMaster.partyCreditLimitList == null || $scope.partyMaster.partyCreditLimitList.length == 0)
                        $scope.partyMaster.partyCreditLimitList = [];

                    if ($scope.partyMaster.partyCompanyAssociateList.length == 0)
                        $scope.partyMaster.partyCompanyAssociateList = [];

                    if ($scope.partyMaster.partyServiceList.length == 0)
                        $scope.partyMaster.partyServiceList = [];

                    if ($scope.partyMaster.partyAccountList.length == 0)
                        $scope.partyMaster.partyAccountList = [];

                    if ($scope.partyMaster.partyBusinessDetailList.length == 0)
                        $scope.partyMaster.partyBusinessDetailList = [];

                    $scope.contained_progressbar.start();
                    $scope.tmpObj = cloneService
                        .clone($scope.partyMaster);

                    $scope.tmpObj.partyTypeList = [];

                    for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {
                        if ($scope.tempPartyTypeList[i].selected) {
                            $scope.partyAssociateToPartyTypeMaster = {};
                            $scope.partyAssociateToPartyTypeMaster.partyTypeMaster = $scope.tempPartyTypeList[i];
                            $scope.tmpObj.partyTypeList
                                .push($scope.partyAssociateToPartyTypeMaster);
                        }
                    }

                    if ($scope.tmpObj.partyAccountList != null &&
                        $scope.tmpObj.partyAccountList.length != 0) {
                        for (var i = 0; i < $scope.tmpObj.partyAccountList.length; i++) {
                            $scope.tmpObj.partyAccountList[i].isTaxExempted = $scope.tmpObj.partyAccountList[i].isTaxExempted ? 'Yes' :
                                'No';

                        }

                    }

                    $scope.tmpObj.partyDetail.matchInvoiceQuoteOutput = $scope.tmpObj.partyDetail.matchInvoiceQuoteOutput ? 'TRUE' :
                        'FALSE';
                    $scope.tmpObj.partyDetail.isBlanketNomination = $scope.tmpObj.partyDetail.isBlanketNomination ? 'TRUE' :
                        'FALSE';
                    $scope.tmpObj.partyDetail.directStatusUpdate = $scope.tmpObj.partyDetail.directStatusUpdate ? 'TRUE' :
                        'FALSE';



                    for (var i = 0; i < $scope.tmpObj.partyAddressList.length; i++) {


                        if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate != undefined &&
                            $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate != null) {
                            $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate = $rootScope
                                .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate);
                        } else {
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField == null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField = {};
                            }
                            $scope.tmpObj.partyAddressList[i].partyCountryField.exportExpirationDate = null;
                        }
                        if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth != undefined &&
                            $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth != null) {
                            $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth = $rootScope
                                .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth);
                        } else {
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField == null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField = {};
                            }
                            $scope.tmpObj.partyAddressList[i].partyCountryField.dateOfBirth = null;
                        }
                        if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate != undefined &&
                            $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate != null) {
                            $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate = $rootScope
                                .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate);
                        } else {
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField == null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField = {};
                            }
                            $scope.tmpObj.partyAddressList[i].partyCountryField.importExpirationDate = null;
                        }
                        if ($scope.tmpObj.partyAddressList[i].partyCountryField != null && $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate != undefined &&
                            $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate != null) {
                            $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate = $rootScope
                                .sendApiStartDateTime($scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate);
                        } else {
                            if ($scope.tmpObj.partyAddressList[i].partyCountryField == null) {
                                $scope.tmpObj.partyAddressList[i].partyCountryField = {};
                            }
                            $scope.tmpObj.partyAddressList[i].partyCountryField.knownShipperValidationDate = null;
                        }

                    }

                    if ($scope.tmpObj.partyBusinessDetailList != null) {
                        for (var i = 0; i < $scope.tmpObj.partyBusinessDetailList.length; i++) {
                            if ($scope.tmpObj.partyBusinessDetailList[i].currentPotential == false) {
                                $scope.tmpObj.partyBusinessDetailList[i].currentPotential = "Current";
                            } else if ($scope.tmpObj.partyBusinessDetailList[i].currentPotential == true) {
                                $scope.tmpObj.partyBusinessDetailList[i].currentPotential = "Potential";
                            }
                            if ($scope.tmpObj.partyBusinessDetailList[i].closureDate != undefined &&
                                $scope.tmpObj.partyBusinessDetailList[i].closureDate != "" &&
                                $scope.tmpObj.partyBusinessDetailList[i].closureDate != null) {
                                $scope.tmpObj.partyBusinessDetailList[i].closureDate = $rootScope
                                    .sendApiStartDateTime($scope.tmpObj.partyBusinessDetailList[i].closureDate);
                            }
                        }
                    }
                    if ($scope.tmpObj.partyEmailMappingList != null &&
                        $scope.tmpObj.partyEmailMappingList.length != 0) {
                        for (var i = 0; i < $scope.tmpObj.partyEmailMappingList.length; i++) {
                            $scope.tmpObj.partyEmailMappingList[i].originCountry = $scope.checkObjectHavingId($scope.tmpObj.partyEmailMappingList[i].originCountry);
                            $scope.tmpObj.partyEmailMappingList[i].destinationCountry = $scope.checkObjectHavingId($scope.tmpObj.partyEmailMappingList[i].destinationCountry);

                            $scope.tmpObj.partyEmailMappingList[i].imco = $scope.tmpObj.partyEmailMappingList[i].imco ? 'Yes' :
                                'No';

                            if ($scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList != null &&
                                $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList.length != 0) {

                                for (var j = 0; j < $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList.length; j++) {

                                    $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList[j].autoMailStatus = $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList[j].autoMailStatus == true ? 'Enable' :
                                        'Disable';
                                }

                            } else {
                                $scope.tmpObj.partyEmailMappingList[i].partyEmailMessageMappingList = null;
                            }

                        }
                    } else {
                        $scope.tmpObj.partyEmailMappingList = null;
                    }

                    if ($scope.tmpObj.contactList != null &&
                        $scope.tmpObj.contactList.length != 0) {
                        for (var i = 0; i < $scope.tmpObj.contactList.length; i++) {
                            if ($scope.tmpObj.contactList[i].relationList != null &&
                                $scope.tmpObj.contactList[i].relationList != undefined && $scope.tmpObj.contactList[i].relationList != "") {
                                for (var j = 0; j < $scope.tmpObj.contactList[i].relationList.length; j++) {
                                    $scope.tmpObj.contactList[i].relationList[j].personalEventDate = $rootScope
                                        .sendApiStartDateTime($scope.tmpObj.contactList[i].relationList[j].personalEventDate);
                                }
                            }
                        }
                    } else {
                        $scope.tmpObj.contactList = null;
                    }

                    PartyMasterEdit.save($scope.tmpObj).$promise
                        .then(
                            function(data) {
                                if (data.responseCode == 'ERR0') {

                                    $scope
                                        .showResponse(
                                            data.responseObject,
                                            false);

                                } else {
                                    console
                                        .log("Party Master added from Server Side Filed :  " +
                                            data.responseDescription)
                                }
                                $scope.contained_progressbar
                                    .complete();
                                angular.element(".panel-body")
                                    .animate({
                                        scrollTop: 0
                                    }, "slow");
                                $scope.enableSubmitBtnFn();

                            },
                            function(error) {
                                console
                                    .log("Party Master Addition Failed : " +
                                        error)
                                $scope.enableSubmitBtnFn();
                            });

                } else {
                    console.log("Validation Failed");
                    $scope.enableSubmitBtnFn();
                }
            }
        }


        $scope.partyEmailDuplicate = function(emailList) {

            if (emailList != undefined && emailList != null) {

                for (var i = 0; i < emailList.length; i++) {
                    for (var j = 0; j < emailList.length; j++) {
                        if (i == j)
                            continue
                        if (emailList[i].originCountry != null && emailList[i].destinationCountry != null &&
                            emailList[j].originCountry != null && emailList[j].destinationCountry != null && emailList[i].eMail == emailList[j].eMail && emailList[i].locationMaster.id == emailList[j].locationMaster.id &&
                            emailList[i].originCountry.id == emailList[j].originCountry.id && emailList[i].destinationCountry.id == emailList[j].destinationCountry.id &&
                            emailList[i].imco == emailList[j].imco) {
                            $rootScope.clientMessage = $rootScope.nls["ERR6087"];
                            return true;
                        }
                    }
                }
                return false;
            }

        }


        $scope.partyIsInd = function() {

            $scope.salutationShow = false;
            $scope.agentshow = false;
            console.log("PartyIsInd.....")

            for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {

                if ($scope.tempPartyTypeList[i].selected &&
                    $scope.tempPartyTypeList[i].partyTypeCode == "IN") {
                    console.log("Salutation is Displayed.....")
                    $scope.salutationShow = true;
                }

                if ($scope.tempPartyTypeList[i].selected &&
                    $scope.tempPartyTypeList[i].partyTypeCode == "AG") {
                    console.log("Salutation is Displayed.....")
                    $scope.agentshow = true;
                    if ($scope.partyMaster.id == null &&
                        $scope.partyMaster.partyDetail != undefined) {
                        $scope.partyMaster.partyDetail.iataCode = null;
                    } else {
                        console
                            .log("Retriving the saluation value back...................")
                    }
                }

            }

            if ($scope.salutationShow) {
                $scope.partyMaster.salutation = $scope.partyMaster.salutation != null ? $scope.partyMaster.salutation :
                    null;
                $scope.partyMaster.firstName = $scope.partyMaster.firstName != "" ? $scope.partyMaster.firstName :
                    "";
                $scope.partyMaster.lastName = $scope.partyMaster.lastName != "" ? $scope.partyMaster.lastName :
                    "";
            } else {
                $scope.partyMaster.salutation = null;
                $scope.partyMaster.firstName = "";
                $scope.partyMaster.lastName = "";
            }
        };

        /* Country List Select Picker Starts */

        $scope.showCountryList = false;

        $scope.countryList = [];

        $scope.searchText = "";

        $scope.listConfigCountry = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "countryName",
                seperator: false
            }, {
                "title": "countryCode",
                seperator: true
            }]
        }

        $scope.showCountry = function(country) {
            console
                .log("Showing List of Countries....................");
            $scope.panelTitle = "Country";
            $scope.selectedItem = -1;
            $scope.ajaxCountryEvent(null);
        };

        $scope.ajaxCountryEvent = function(object) {

            console.log("Country Ajax Event.... ", object);
            $scope.errorMap = new Map();
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CountryList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.countryList = data.responseObject.searchResult;
                            return $scope.countryList;
                            // $scope.showCountryList=true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Countries');
                    });

        }

        $scope.selectedCountry = function(nextIdValue, tabName) {
            if ($scope.validatePartyMaster(2)) {
                $scope.partyTab = tabName;
                $rootScope.navigateToNextField(nextIdValue);
            }

            if ($scope.partyMaster.countryMaster != undefined && $scope.partyMaster.countryMaster != null &&
                $scope.partyMaster.countryMaster.id != undefined && $scope.partyMaster.countryMaster.id != null) {
                for (i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                    if ($scope.partyMaster.partyAddressList[i].stateMaster != undefined && $scope.partyMaster.partyAddressList[i].stateMaster != null) {
                        console.log($scope.partyMaster.partyAddressList[i].stateMaster.countryMaster.id);
                        if ($scope.partyMaster.partyAddressList[i].stateMaster.countryMaster.id != $scope.partyMaster.countryMaster.id) {
                            $scope.partyMaster.partyAddressList[i].stateMaster = undefined;
                        }
                    }
                    if ($scope.partyMaster.partyAddressList[i].cityMaster != undefined && $scope.partyMaster.partyAddressList[i].cityMaster != null) {
                        console.log($scope.partyMaster.partyAddressList[i].cityMaster.countryMaster.id);
                        if ($scope.partyMaster.partyAddressList[i].cityMaster.countryMaster.id != $scope.partyMaster.countryMaster.id) {
                            $scope.partyMaster.partyAddressList[i].cityMaster = undefined;
                        }
                    }
                }
            }


            if ($scope.partyMaster.countryMaster != undefined && $scope.partyMaster.countryMaster != null &&
                $scope.partyMaster.countryMaster.id != undefined && $scope.partyMaster.countryMaster.id != null) {
                $scope.getDynamicFormField($scope.partyMaster.countryMaster.id);
            } else {
                $scope.localDynamicFormFieldMap = new Map();
            }
        };

        $scope.cancelCountry = function() {
            $scope.searchText = "";
            $scope.showCountryList = false;
        };

        $scope.$watch('partyMaster.countryMaster',
            function(newVal, oldVal) {
                if (oldVal != undefined &&
                    oldVal.id != undefined &&
                    newVal != undefined &&
                    newVal.id != undefined &&
                    newVal.id != null) {
                    if (oldVal.id != newVal.id) {
                        console.log("changed the country");
                        $scope.clearPartyAdressCityState();
                    }
                }
                if (newVal != undefined &&
                    newVal.id != undefined &&
                    oldVal == "") {
                    $scope.clearPartyAdressCityState();
                }

            });

        /* Country List Select Picker Ends */

        /* Category List Select Picker Starts */

        $scope.showCategoryList = false;

        $scope.categoryList = [];

        $scope.listConfigCategory = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "categoryName",
                seperator: false
            }, {
                "title": "categoryCode",
                seperator: true
            }]
        }

        $scope.showCategory = function(category) {
            console.log("Showing Category List......");
            $scope.panelTitle = "Category";
            $scope.selectedItem = -1;
            $scope.ajaxCategoryEvent(null);
        };

        $scope.ajaxGradeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return gradeMasterFactory.getall.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.gradeList = data.responseObject.searchResult;
                            return $scope.gradeList;
                            // $scope.showCategoryList=true;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Grade List');
                    });

        };


        $scope.ajaxCategoryEvent = function(object) {
            console.log("ajaxCategoryEvent called");
            /*
             * $scope.triggerChange = function() {
             * 
             * //Dummy Function needed for trigger toggle button };
             */

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CategoryList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.categoryList = data.responseObject.searchResult;
                            return $scope.categoryList;
                            // $scope.showCategoryList=true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Category List');
                    });

        };

        $scope.selectedCategory = function() {

            var nextIdValue = "";
            if ($scope.validatePartyMaster(7)) {
                if ($scope.salutationShow == true) {
                    nextIdValue = "salutation";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > VAT') == true || $scope.localDynamicFormFieldMap.get('Party Master > VAT') == 'true') {
                    nextIdValue = "vatCompliance";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > PAN') == true || $scope.localDynamicFormFieldMap.get('Party Master > PAN') == 'true') {
                    nextIdValue = "panNo";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > SVT Regn') == true || $scope.localDynamicFormFieldMap.get('Party Master > SVT Regn') == 'true') {
                    nextIdValue = "svtNo";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > TIN') == true || $scope.localDynamicFormFieldMap.get('Party Master > TIN') == 'true') {
                    nextIdValue = "tinNo";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > SVAT') == true || $scope.localDynamicFormFieldMap.get('Party Master > SVAT') == 'true') {
                    nextIdValue = "svatNo";
                } else if ($scope.localDynamicFormFieldMap.get('Party Master > Party Name (Regional)') == true || $scope.localDynamicFormFieldMap.get('Party Master > Party Name (Regional)') == 'true') {
                    nextIdValue = "partyRegional";
                } else if ($scope.agentshow == true) {
                    nextIdValue = "iataCode";
                } else {
                    nextIdValue = "racNo";
                }



                $rootScope.navigateToNextField(nextIdValue);
            }
        };

        $scope.cancelCategory = function() {
            console.log("Cancel Category List Button Pressed....");
            $scope.searchText = "";
            $scope.showCategoryList = false;
        };

        /* Category List Select Picker Ends */

        $scope.currentTab = "general";
        $scope.activeTab = function(tab) {
            $scope.currentTab = tab;
        };


        $scope.addAddress = function() {

            $rootScope.clientMessage = null;

            if ($scope.partyMaster.partyAddressList == null || $scope.partyMaster.partyAddressList.length == 0) {
                $scope.partyMaster.partyAddressList = [];
            }

            if ($scope.partyMaster.partyAddressList.length >= 4) {
                $rootScope.clientMessage = rootScope.nls['ERR6088'];
                return;
            }

            if ($scope.partyMaster.partyAddressList.length > 0) {
                if ($scope.validatePartyAddressMaster(0, $scope.partyMaster.partyAddressList[$scope.addressIndex], $scope.addressIndex) == false) {
                    return;
                }
            }

            var partyAddressMaster = {};

            partyAddressMaster.partyCountryField = {};

            partyAddressMaster.corporate = false;
            partyAddressMaster.partyCountryField.bondType = false;
            partyAddressMaster.partyCountryField.exportPowerOfAttorny = false;
            partyAddressMaster.partyCountryField.importPowerOfAttorny = false;
            partyAddressMaster.partyCountryField.aesToBeFiledBy = false
            partyAddressMaster.partyCountryField.aesFilingType = false

            partyAddressMaster.partyCountryField.knownShipperValidationNo = null;
            partyAddressMaster.partyCountryField.knownShipperValidationDate = null

            $scope.partyMaster.partyAddressList.push(partyAddressMaster);
            $scope.addressIndex = $scope.partyMaster.partyAddressList.length - 1;
            $scope.editmoreAddressTab = 'editmoreInfo';
            $rootScope.navigateToNextField("addressType" + $scope.addressIndex);

        };



        $scope.editAddress = function(index) {
            if ($scope.partyMaster.partyAddressList == undefined ||
                $scope.partyMaster.partyAddressList == null ||
                $scope.partyMaster.partyAddressList.length == 0) {
                $scope.addressIndex = -1;
            } else {
                $scope.addressIndex = index;
                // $scope.editmoreAddressTab = 'editmoreInfo';
            }
        };



        $scope.triggerChange = function() {

            // Dummy Function needed for trigger toggle button
        };

        $scope.addressDelete = function(index) {
            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected address ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyAddressList
                            .splice(index, 1);
                        if ($scope.partyMaster.partyAddressList == undefined ||
                            $scope.partyMaster.partyAddressList == null ||
                            $scope.partyMaster.partyAddressList.length == 0) {
                            $scope.partyMaster.partyAddressList = [];
                            $scope.selectedAddressIndex = -1;
                        } else {
                            $scope.selectedAddressIndex = $scope.partyMaster.partyAddressList.length - 1;
                        }
                    },
                    function(value) {
                        console
                            .log("Address Deletion cancelled");
                    });

        }



        /* City List Select Picker Start */

        $scope.ajaxCityEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            if ($scope.validatePartyMaster(2)) {
                return CityList
                    .fetch({
                        "countryId": $scope.partyMaster.countryMaster.id
                    }, $scope.searchDto).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                $scope.cityList = data.responseObject.searchResult;
                                return $scope.cityList;
                            }
                        },
                        function(errResponse) {
                            console
                                .error('Error while fetching City List Based on the Country');
                        });

            }
        }

        $scope.selectedCity = function(partyAddressMaster,
            nextFieldId, index) {
            if ($scope.validatePartyAddressMaster(34,
                    partyAddressMaster, index)) {
                $rootScope.navigateToNextField(nextFieldId + index);
            }

            if (partyAddressMaster.cityMaster.status == 'Active') {
                partyAddressMaster.stateMaster = partyAddressMaster.cityMaster.stateMaster;
                $scope.validatePartyAddressMaster(29,
                    partyAddressMaster, index);
            }

        };

        $scope.ajaxStateEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            if ($scope.validatePartyMaster(2)) {
                return StateList
                    .fetch({
                        "countryId": $scope.partyMaster.countryMaster.id
                    }, $scope.searchDto).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                $scope.stateList = data.responseObject.searchResult;
                                return $scope.stateList;
                            }
                        },
                        function(errResponse) {
                            console
                                .error('Error while fetching State List Based on the Country');
                        });

            }
        }

        $scope.selectedState = function(partyAddressMaster,
            nextFieldId, index) {
            if ($scope.validatePartyMaster(29, partyAddressMaster,
                    index)) {
                $rootScope.navigateToNextField(nextFieldId + index);
            }
        };

        $scope.corporateFlagChange = function(partyMaster, index, value) {
                for (var i = 0; i < partyMaster.partyAddressList.length; i++) {
                    if (i != index && value) {
                        partyMaster.partyAddressList[i].corporate = false;
                    }
                }
            }
            /* State List Select Picker Starts */

        /* Country of Issuance List Start */

        $scope.ajaxCountryIssuanceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CountryList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.countryIssueList = data.responseObject.searchResult;
                            return $scope.countryIssueList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Country of Issuance');
                    });

        }

        $scope.selectedCountryIssue = function(partyAddressMaster,
            nextFieldId, index) {
            if ($scope.validatePartyAddressMaster(35,
                    partyAddressMaster, index)) {
                $rootScope.navigateToNextField(nextFieldId + index);
            }
        };

        $scope.showCompanyList = false;
        $scope.companyList = [];
        $scope.listConfigCompany = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "companyName",
                seperator: false
            }, {
                "title": "companyCode",
                seperator: true
            }]
        }

        $scope.showCompany = function(company) {
            $scope.panelTitle = "Company";
            $scope.selectedItem = -1;
            $scope.ajaxCompanyEvent(null);

        };

        $scope.ajaxCompanyEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CompanyList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.companyList = data.responseObject.searchResult;
                            $scope.showCompanyList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Company');
                    });
        }
        $scope.selectedCompany = function(obj) {
            $scope.partyCompanyAssociation.companyMaster = obj;
            $scope.partyCompanyAssociation.divisionMaster = {};
            $scope.cancelCompany();
            $scope.validatePartyCompanyMapping(1);
        };
        $scope.cancelCompany = function() {
            console.log("Cancel Company List Button Pressed....");
            $scope.searchText = "";
            $scope.showCompanyList = false;
        };

        $scope.showDivisionList = false;
        $scope.divisionList = [];
        $scope.listConfigDivision = {
            search: true,
            showCode: true,
            columns: [{
                "title": "divisionName",
                seperator: false
            }, {
                "title": "divisionCode",
                seperator: true
            }]
        }

        $scope.showDivision = function(division) {
            console.log("Show Division is called..");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Division";
            $scope.ajaxDivisionEvent(null);

        };
        $scope.ajaxDivisionEvent = function(object) {
            console.log("ajaxStateEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            DivisionListByCompany
                .fetch({
                    "companyId": $scope.partyCompanyAssociation.companyMaster.id
                }, $scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.divisionList = data.responseObject.searchResult;
                            $scope.showDivisionList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching State List Based on the Country');
                    });

        }

        $scope.showCreditDivision = function(division) {
            console.log("Show Division is called..");
            $scope.panelTitle = "division";
            $scope.selectedItem = -1;
            $scope.ajaxCreditDivisionEvent(null);

        };

        $scope.ajaxCreditDivisionEvent = function(object) {

            console.log("ajaxStateEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            DivisionList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.divisionList = data.responseObject.searchResult;
                            $scope.showCreditDivisionList = true;

                        }
                    },
                    function(error) {
                        console
                            .error('Error while fetching division');
                    });

        }
        $scope.selectedDivision = function(obj) {
            $scope.partyCompanyAssociation.divisionMaster = obj;
            $scope.cancelDivision();
            $scope.validatePartyCompanyMapping(2);
        };
        $scope.cancelDivision = function() {
            console.log("Cancel division List Button Pressed....");
            $scope.searchText = "";
            $scope.showDivisionList = false;
        };

        $scope.selectedCreditDivision = function(obj) {
            $scope.partyCreditLimit.divisionMaster = obj;
            $scope.cancelCreditDivision();
            $scope.validatePartyCreditLimitMapping(3);
        };
        $scope.cancelCreditDivision = function() {
            console.log("Cancel division List Button Pressed....");
            $scope.searchText = "";
            $scope.showCreditDivisionList = false;
        };

        $scope.partyCompanyAssociation = {};
        $scope.mappingAddDivision = function() {
            console.log("Add Division is called.......");
            $scope.partyCompanyAssociation = {};
            $scope.partyCompanyAssociation.companyMaster = {};
            $scope.partyCompanyAssociation.divisionMaster = {};
            $scope.errorMap = new Map();
            $scope.flagCompany = 'Add';
            discardService.set($scope.partyCompanyAssociation);
        };

        $scope.mappingAddCredit = function() {
            console.log("Add Credit is called.......");
            $scope.partyCreditLimit = {};
            $scope.partyCreditLimit.serviceMaster = {};
            $scope.partyCreditLimit.locationMaster = {};
            $scope.partyCreditLimit.divisionMaster = {};
            $scope.errorMap = new Map();
            $scope.flagCredit = 'Add';

            discardService.set($scope.partyCreditLimit);

        };

        $scope.partyServiceMaster = {};

        $scope.mappingAddShow = function() {

            $scope.partyServiceMaster = {};
            $scope.partyServiceMaster.serviceMaster = {};
            $scope.partyServiceMaster.tosMaster = {};
            $scope.partyServiceMaster.locationMaster = {};
            $scope.partyServiceMaster.salesman = {};
            $scope.partyServiceMaster.customerService = {};
            $scope.errorMap = new Map();
            $scope.showPanelTemp = 'mappingAdd';

            discardService.set($scope.partyServiceMaster);
        };


        $scope.mappingEditShow = function(object, index) {
            $scope.editIndex = index;
            $scope.partyServiceMaster = cloneService.clone(object);
            $scope.errorMap = new Map();
            $scope.showPanelTemp = 'mappingEdit';
            discardService.set($scope.partyServiceMaster);
        };

        $scope.mappingEditCompany = function(object, index) {
            $scope.editIndex = index;
            $scope.partyCompanyAssociation = cloneService
                .clone(object);
            $scope.listCompany = false;
            $scope.flagCompany = 'Update';
            $scope.errorMap = new Map();
            discardService.set($scope.partyCompanyAssociation)
        };

        $scope.mappingEditCredit = function(object, index) {
            console.log("Edit Credit " + object)
            $scope.editIndex = index;
            $scope.partyCreditLimit = cloneService.clone(object);
            $scope.listCredits = false;
            $scope.flagCredit = 'Update';
            $scope.errorMap = new Map();
            discardService.set($scope.partyCreditLimit)
        };

        $scope.discardCreditMapping = function() {
            if (!discardService.compare($scope.partyCreditLimit)) {

                ngDialog
                    .openConfirm({
                        template: '<p>Do you want to update your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.addCreditLimitMapping();
                        } else {
                            $scope.listCredits = true;
                        }
                    });

            } else {
                $scope.listCredits = true;
            }
        }

        /*
         * Service List Select Picker Starts In Service & Division
         * Mapping Tab
         */

        $scope.showServiceList = false;

        $scope.serviceList = [];

        $scope.listConfigService = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "serviceName",
                seperator: false
            }, {
                "title": "serviceCode",
                seperator: true
            }]
        }

        $scope.showService = function(service) {
            console.log("Showing List of Services...............");
            $scope.panelTitle = "Service";
            $scope.selectedItem = -1;
            $scope.ajaxServiceEvent(null);
        };

        $scope.ajaxServiceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            ServiceList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.serviceList = data.responseObject.searchResult;
                            $scope.showServiceList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Services');
                    });
        }

        $scope.selectedService = function(serviceObj) {
            console.log("Selected service", serviceObj);
            $scope.partyServiceMaster.serviceMaster = serviceObj;
            $scope.validatePartyServiceMaster(1);
            if ($scope.partyServiceMaster.serviceMaster != null &&
                ($scope.partyServiceMaster.serviceMaster.status == 'Block' || $scope.partyServiceMaster.serviceMaster == 'Hide')) {
                $scope.partyServiceMaster.serviceMaster = null;
            }
            $scope.cancelService();
        };

        $scope.cancelService = function() {
            console.log("Cancel service List Button Pressed....");
            $scope.searchText = "";
            $scope.showServiceList = false;
        };

        /*
         * Service List Select Picker Ends In Service & Division
         * Mapping Tab
         */

        $scope.showCreditService = function(service) {
            console.log("show service called ");
            $scope.panelTitle = "Service";
            $scope.selectedItem = -1;
            $scope.ajaxCreditServiceEvent(null);
        };

        $scope.ajaxCreditServiceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            ServiceList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.serviceList = data.responseObject.searchResult;
                            $scope.showCreditServiceList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Services');
                    });
        }

        $scope.selectedCreditService = function(serviceObj) {
            console.log("Selected Credit Service", serviceObj);
            $scope.partyCreditLimit.serviceMaster = serviceObj;
            $scope.cancelCreditService();
            $scope.validatePartyCreditLimitMapping(2);
        };

        $scope.cancelCreditService = function() {
            console
                .log("Cancel credit service List Button Pressed....");
            $scope.searchText = "";
            $scope.showCreditServiceList = false;
        };

        $scope.listConfigSalesman = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "employeeName",
                seperator: false
            }, {
                "title": "employeeCode",
                seperator: true
            }]
        }

        $scope.showSalesmans = function(sales) {
            $scope.panelTitle = "Salesman";
            $scope.selectedItem = -1;
            $scope.ajaxSalesmansEvent(null);

        };

        $scope.ajaxSalesmansEvent = function(object) {
            console.log("ajaxSalesmansEvent is called.", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return SalesmanList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.salesmanList = data.responseObject.searchResult;
                            return $scope.salesmanList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Salesmans');
                    });
        }
        $scope.selectedSalesman = function(salesmanObj) {
            console.log("Selected Salesman", salesmanObj);
            $scope.partyServiceMaster.salesman = salesmanObj;
            $scope.cancelSalesman();
            $scope.validatePartyServiceMaster(4);
        };

        $scope.cancelSalesman = function() {
            console.log("Cancel salesman List Button Pressed....");
            $scope.searchText = "";
            $scope.showSalesmanList = false;
        };

        $scope.listConfigCustomerService = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "employeeName",
                seperator: false
            }, {
                "title": "employeeCode",
                seperator: true
            }]
        }
        $scope.showCustomerServices = function(customer) {
            $scope.panelTitle = "Customer Service";
            $scope.selectedItem = -1;
            $scope.ajaxCustomerServiceEvent(null);

        };

        $scope.ajaxCustomerServiceEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            EmployeeList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.customerServiceList = data.responseObject.searchResult;
                            $scope.showCustomerServiceList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Customer Services');
                    });
        }
        $scope.selectedCustomerService = function(
            customerServiceObj) {
            console.log("Selected customerService",
                customerServiceObj);
            $scope.partyServiceMaster.customerService = customerServiceObj;
            $scope.cancelCustomerService();
            $scope.validatePartyServiceMaster(5);
        };
        $scope.cancelCustomerService = function() {
            console
                .log("Cancel customer service List Button Pressed....");
            $scope.searchText = "";
            $scope.showCustomerServiceList = false;
        };

        /* Showing List of TOS in the select Picker */
        $scope.listConfigTos = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "tosName",
                seperator: false
            }, {
                "title": "tosCode",
                seperator: true
            }]
        }

        $scope.showTos = function(tos) {
            console.log("Showing List of TOS.... ");
            $scope.panelTitle = "TOS";
            $scope.selectedItem = -1;
            $scope.ajaxTosEvent(null);
        };

        $scope.ajaxTosEvent = function(object) {

            console.log("ajaxTosEvent " + object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            TosSearchKeyword.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.tosList = data.responseObject.searchResult;
                            $scope.showTosList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching TOS List..........');
                    });
        }

        $scope.selectedTos = function(tosObj) {
            console.log("Selected TOS", tosObj);
            $scope.partyServiceMaster.tosMaster = tosObj;
            $scope.cancelTos();
            $scope.validatePartyServiceMaster(2);
        };

        $scope.cancelTos = function() {
            console.log("Cancel tos List Button Pressed....");
            $scope.searchText = "";
            $scope.showTosList = false;
        };

        /* Showing List of Locations for the Select Picker....... */
        $scope.listConfigLocation = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "locationName",
                seperator: false
            }, {
                "title": "locationCode",
                seperator: true
            }]
        }
        $scope.showLocation = function(location) {
            console.log("show location called ");
            $scope.panelTitle = "Location";
            $scope.selectedItem = -1;
            $scope.ajaxLocationEvent(null);
        };

        $scope.ajaxLocationEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CountryWiseLocation
                .fetch({
                    "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                }, $scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.locationList = data.responseObject.searchResult;
                            $scope.showLocationList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Locations');
                    });
        }
        $scope.showCreditLocation = function(location) {
            console.log("show location called ");
            $scope.panelTitle = "Location";
            $scope.selectedItem = -1;
            $scope.ajaxCreditLocationEvent(null);

        };
        $scope.ajaxCreditLocationEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CountryWiseLocation
                .fetch({
                    "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                }, $scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.locationList = data.responseObject.searchResult;
                            $scope.showCreditLocationList = true;

                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Locations');
                    });
        }
        $scope.selectedLocation = function(locationObj) {
            console.log("Selected Location ", locationObj);
            $scope.partyServiceMaster.locationMaster = locationObj;
            $scope.cancelLocation();
            $scope.validatePartyServiceMaster(3);
        };

        $scope.selectedCreditLocation = function(locationObj) {
            console.log("Selected Location for party credit limit",
                locationObj);
            $scope.partyCreditLimit.locationMaster = locationObj;
            $scope.cancelCreditLocation();
            $scope.validatePartyCreditLimitMapping(1);
        };
        $scope.cancelCreditLocation = function() {
            console.log("Cancel Location List Button Pressed....");
            $scope.searchText = "";
            $scope.showCreditLocationList = false;
        };
        $scope.cancelLocation = function() {
            console.log("Cancel Location List Button Pressed....");
            $scope.searchText = "";
            $scope.showLocationList = false;
        };

        $scope.discardServiceMapping = function() {
            if (!discardService.compare($scope.partyServiceMaster)) {

                ngDialog
                    .openConfirm({
                        template: '<p>Do you want to update your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.addMapping();
                        } else {
                            $scope.showFullView();
                        }
                    });

            } else {
                $scope.showFullView();
            }
        }

        $scope.addMapping = function() {
            if ($scope.validatePartyServiceMaster(0)) {
                if ($scope.showPanelTemp == 'mappingAdd') {

                    if ($scope.partyMaster.partyServiceList == null ||
                        $scope.partyMaster.partyServiceList.length == 0) {
                        $scope.partyMaster.partyServiceList = [];
                    }

                    for (var i = 0; i < $scope.partyMaster.partyServiceList.length; i++) {
                        if ($scope.partyMaster.partyServiceList[i].serviceMaster.id == $scope.partyServiceMaster.serviceMaster.id &&
                            $scope.partyMaster.partyServiceList[i].locationMaster.id == $scope.partyServiceMaster.locationMaster.id) {
                            $scope.errorMap = new Map();
                            $scope.errorMap.put("service",
                                $rootScope.nls["ERR6035"]);
                            return false;
                        }

                    }

                    $scope.partyMaster.partyServiceList
                        .push($scope.partyServiceMaster);
                    $scope.showPanelTemp = 'listMapping';

                } else if ($scope.showPanelTemp == 'mappingEdit') {

                    for (var i = 0; i < $scope.partyMaster.partyServiceList.length; i++) {
                        if ($scope.editIndex != i &&
                            $scope.partyMaster.partyServiceList[i].serviceMaster.id == $scope.partyServiceMaster.serviceMaster.id &&
                            $scope.partyMaster.partyServiceList[i].locationMaster.id == $scope.partyServiceMaster.locationMaster.id) {
                            $scope.errorMap = new Map();
                            $scope.errorMap.put("service",
                                $rootScope.nls["ERR6035"]);
                            return false;
                        }

                    }

                    $scope.partyMaster.partyServiceList.splice(
                        $scope.editIndex, 1,
                        $scope.partyServiceMaster);

                    $scope.editIndex = null;
                    // $scope.showPanelTemp='fullView';
                    $scope.showPanelTemp = 'listMapping';
                }

            } else {
                console
                    .log("Party Service Master Validation Failed....");
            }
        };
        $scope.discardCompanyMapping = function() {
            if (!discardService
                .compare($scope.partyCompanyAssociation)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR201"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.addDivisionMapping();
                        } else {
                            $scope.listCompany = true;
                        }
                    });

            } else {
                $scope.listCompany = true;
            }
        }

        $scope.addDivisionMapping = function() {
            console
                .log("Add Division Button Pressed.....................")
            if ($scope.validatePartyCompanyMapping(0)) {
                if ($scope.flagCompany == 'Add') {
                    if ($scope.partyMaster.partyCompanyAssociateList == [] ||
                        $scope.partyMaster.partyCompanyAssociateList.length == 0) {
                        $scope.partyMaster.partyCompanyAssociateList = [];
                    } else {
                        for (var i = 0; i < $scope.partyMaster.partyCompanyAssociateList.length; i++) {

                            if ($scope.partyCompanyAssociation.divisionMaster == null ||
                                $scope.partyCompanyAssociation.divisionMaster.id == null) {
                                if ($scope.partyCompanyAssociation.companyMaster.id == $scope.partyMaster.partyCompanyAssociateList[i].companyMaster.id) {
                                    $scope.errorMap = new Map();
                                    $scope.errorMap
                                        .put(
                                            "company",
                                            $rootScope.nls["ERR6036"]);
                                    return false;
                                }
                            } else {
                                if ($scope.partyCompanyAssociation.companyMaster.id == $scope.partyMaster.partyCompanyAssociateList[i].companyMaster.id &&
                                    $scope.partyCompanyAssociation.divisionMaster.id == $scope.partyMaster.partyCompanyAssociateList[i].divisionMaster.id) {
                                    $scope.errorMap = new Map();
                                    $scope.errorMap
                                        .put(
                                            "company",
                                            $rootScope.nls["ERR6036"]);
                                    return false;
                                }
                            }

                        }
                    }
                    $scope.partyMaster.partyCompanyAssociateList
                        .push($scope.partyCompanyAssociation);

                    // $scope.showPanelTemp='fullView';
                    $scope.listCompany = true;

                } else if ($scope.flagCompany == 'Update') {

                    for (var i = 0; i < $scope.partyMaster.partyCompanyAssociateList.length; i++) {

                        if ($scope.editIndex == i)
                            continue;

                        if ($scope.partyCompanyAssociation.divisionMaster == null ||
                            $scope.partyCompanyAssociation.divisionMaster.id == null) {
                            if ($scope.partyCompanyAssociation.companyMaster.id == $scope.partyMaster.partyCompanyAssociateList[i].companyMaster.id) {
                                $scope.errorMap = new Map();
                                $scope.errorMap.put("company",
                                    $rootScope.nls["ERR6036"]);
                                return false;
                            }
                        } else {
                            if ($scope.partyCompanyAssociation.companyMaster.id == $scope.partyMaster.partyCompanyAssociateList[i].companyMaster.id &&
                                $scope.partyCompanyAssociation.divisionMaster.id == $scope.partyMaster.partyCompanyAssociateList[i].divisionMaster.id) {
                                $scope.errorMap = new Map();
                                $scope.errorMap.put("company",
                                    $rootScope.nls["ERR6036"]);
                                return false;
                            }
                        }

                    }

                    $scope.partyMaster.partyCompanyAssociateList
                        .splice($scope.editIndex, 1,
                            $scope.partyCompanyAssociation);

                    $scope.editIndex = null;
                    $scope.listCompany = true;
                    // $scope.showPanelTemp='fullView';
                }
            } else {
                console
                    .log("Division Mapping Validation Failed...........................");
            }

        }

        $scope.addCreditLimitMapping = function() {
            console.log("add credit mapping called..");
            if ($scope.validatePartyCreditLimitMapping(0)) {
                if ($scope.flagCredit == 'Add') {
                    /*
                     * if($scope.partyMaster.partyCreditLimitList.indexOf($scope.partyCreditLimit) ==
                     * -1) {
                     * $scope.partyMaster.partyCreditLimitList.push($scope.partyCreditLimit);
                     * $scope.listCredits=true; console.log("Credit
                     * Limit added...."); } else {
                     * console.log("Duplicate Credit Limit...."); }
                     */
                    $scope.isDuplicate = false;
                    $scope.isDuplicate = $scope
                        .uniqueCreditMapping(
                            $scope.partyCreditLimit,
                            $scope.partyMaster.partyCreditLimitList);

                    if ($scope.isDuplicate == true) {
                        $scope.errorMap.put("errorMsg",
                            $rootScope.nls["ERR6037"]);
                        return false;
                    } else {
                        $scope.listCredits = true;
                        console.log("Credit Limit added....");
                    }
                } else if ($scope.flagCredit == 'Update') {
                    $scope.partyMaster.partyCreditLimitList.splice(
                        $scope.editIndex, 1,
                        $scope.partyCreditLimit);
                    $scope.editIndex = null;
                    $scope.listCredits = true;
                }

            } else {
                console
                    .log("Party Credit Limit Validation Failed....");
            }
        };

        $scope.mappingCreditLimitDelete = function(index) {
            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected credit  ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyCreditLimitList
                            .splice(index, 1);
                    },
                    function(value) {
                        console
                            .log("Credit Deletion cancelled");
                    });

        }

        $scope.mappingDelete = function(index) {

            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected service mapping  ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyServiceList
                            .splice(index, 1);
                    },
                    function(value) {
                        console
                            .log("Service Mapping Deletion cancelled");
                    });

        }

        $scope.mappingDeleteCompany = function(index) {
            console.log("Delete Company is called " + index)

            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected divsion mapping  ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyCompanyAssociateList
                            .splice(index, 1);
                    },
                    function(value) {
                        console
                            .log("Divsion Mapping Deletion cancelled");
                    });

        }

        $scope.mappingDeleteCredit = function(index) {
            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected credit limit  ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyCreditLimitList
                            .splice(index, 1);
                    },
                    function(value) {
                        console
                            .log("Credit Limit Deletion cancelled");
                    });

        }

        // Party Account Tab code
        $scope.showAccounts = function(account) {
            console.log("show Accounts called ");
            $scope.panelTitle = "Accounts";
            $scope.selectedItemAcc = -1;
            $scope.ajaxAccountEvent(null);
        };

        /* $scope.ajaxAccountEvent = function(object) {

             $scope.searchDto = {};
             $scope.searchDto.keyword = object == null ? "" : object;
             $scope.searchDto.selectedPageNumber = 0;
             $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
             AccountList.fetch($scope.searchDto).$promise
                 .then(
                     function(data) {
                         if (data.responseCode == "ERR0") {
                             $scope.accountList = data.responseObject.searchResult;
                             $scope.showAccountList = true;
                         }
                     },
                     function(errResponse) {
                         console
                             .error('Error while fetching  Accounts');
                     });

         }*/
        $scope.selectedAccount = function(accountObj) {
            console.log("Selected Accounts ", accountObj);
            $scope.partyAccountMaster.accountMaster = accountObj;
            $scope.validatePartyAccount(1);
            $scope.cancelAccounts();
        };

        $scope.cancelAccounts = function() {
            console.log("Cancel Account List Button Pressed....");
            $scope.searchText = "";
            $scope.showAccountList = false;
        };

        $scope.listConfigAccount = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "accountCode",
                seperator: false
            }, {
                "title": "accountName",
                seperator: true
            }]
        }

        // Location

        $scope.showAccountsLocation = function(location) {
            console.log("show AccountsLocation called ");
            $scope.panelTitle = "Location";
            $scope.selectedIteml = -1;
            $scope.ajaxAccLocationEvent(null);
        };
        $scope.ajaxAccLocationEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CountryWiseLocation
                .fetch({
                    "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                }, $scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.accountlocationList = data.responseObject.searchResult;
                            $scope.showaccountLocationList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Location');
                    });
        }
        $scope.selectedAccLocation = function(locationobj) {
            console.log("Selected Location ", locationobj);
            $scope.partyAccountMaster.locationMaster = locationobj;
            $scope.validatePartyAccount(3);
            $scope.cancelAccountsLocation();
        };

        $scope.cancelAccountsLocation = function() {
            console.log("Cancel Account List Button Pressed....");
            $scope.searchText = "";
            $scope.showaccountLocationList = false;
        };

        $scope.listConfigAccLocation = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "locationName",
                seperator: false
            }, {
                "title": "locationCode",
                seperator: true
            }]
        }

        // currency

        $scope.showAccountsCurrency = function(currency) {
            console.log("show currency called ");
            $scope.panelTitle = "Currency";
            $scope.selectedItemcurrency = -1;
            $scope.ajaxAccCurrencyEvent(null);
        };

        $scope.ajaxAccCurrencyEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CurrencyMasterSearchKeyword.query($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.currencyList = data.responseObject.searchResult;
                            $scope.showCurrencyList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Currency');
                    });

        }
        $scope.selectedAccCurrency = function(currencyobj) {
            console.log("Selected Location ", currencyobj);
            $scope.partyAccountMaster.currencyMaster = currencyobj;
            $scope.validatePartyAccount(4);
            $scope.cancelAccountsCurrency();
        };

        $scope.cancelAccountsCurrency = function() {
            console.log("Cancel Account List Button Pressed....");
            $scope.searchText = "";
            $scope.showCurrencyList = false;
        };

        $scope.listConfigAccCurrency = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "currencyName",
                seperator: false
            }, {
                "title": "currencyCode",
                seperator: true
            }]
        }

        // Ledger
        $scope.showBillLedgerAccountName = function(ledger) {
            console.log("show Ledgers ");
            $scope.panelTitle = "Ledgers";
            $scope.selectedItemLedgerName = -1;
            $scope.ajaxLedgerAccNameEvent(null);
        };
        $scope.ajaxLedgerAccNameEvent = function(object) {

            console
                .log("ajaxLedgerAccNameEvent method : " +
                    object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartyAccountListByNotInId.fetch({
                    "partyId": $scope.partyMaster.id == undefined ||
                        $scope.partyMaster.id == null ? -1 : $scope.partyMaster.id
                }, $scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.ledgerNameList = data.responseObject.searchResult;

                            return $scope.ledgerNameList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Party account');
                    });

        }
        $scope.selectedLedgerAccName = function(ledgernameObj) {
            console.log("Selected Ledger ", ledgernameObj);
            $scope.partyAccountMaster.billingAccountName = ledgernameObj;
            $scope.validatePartyAccount(5);
            $scope.cancelLedgerName();
        };

        $scope.cancelLedgerName = function() {
            console.log("Cancel Ledger List Button Pressed....");
            $scope.searchText = "";
            $scope.showLedgerNameList = false;
        };

        $scope.listConfigLedgerName = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "ledgerCode",
                seperator: false
            }, {
                "title": "ledgerName",
                seperator: true
            }]
        }

        // Ledger Code

        $scope.showBillLedgertCode = function(ledger) {

            console.log("show Ledgers Codes");
            $scope.panelTitle = "Ledgers";
            $scope.selectedItemLedgerCode = -1;
            $scope.ajaxLedgerCodeEvent(null);

        };
        $scope.ajaxLedgerCodeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            LedgerList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.ledgerCodeList = data.responseObject.searchResult;
                            $scope.showLedgerCodeList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Ledgers');
                    });
        }
        $scope.selectedLedgerCode = function(ledgernameObj) {
            $scope.partyAccountMaster.billingSubLedgerCode = ledgernameObj;
            $scope.validatePartyAccount(6);
            $scope.cancelLedgerCode();
        };
        $scope.cancelLedgerCode = function() {
            console.log("Cancel Ledger List Button Pressed....");
            $scope.searchText = "";
            $scope.showLedgerCodeList = false;
        };

        $scope.listConfigLedgerCode = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "ledgerCode",
                seperator: false
            }, {
                "title": "ledgerName",
                seperator: true
            }]
        }

        $scope.savePartyAccount = function() {

            if ($scope.validatePartyAccount(0)) {

                $scope.partyAccountMaster.accountCode = $scope.partyAccountMaster.accountMaster.accountCode;
                $scope.partyAccountMaster.currencyCode = $scope.partyAccountMaster.currencyMaster.currencyCode;

                var flag = $scope.partyAccountunique(
                    $scope.partyAccountMaster,
                    $scope.partyMaster.partyAccountList)

                if (!flag) {
                    if ($scope.editAccountIndex == null) {
                        if ($scope.partyMaster.partyAccountList == null)
                            $scope.partyMaster.partyAccountList = [];
                        $scope.partyMaster.partyAccountList
                            .push($scope.partyAccountMaster);
                    } else {

                        $scope.partyMaster.partyAccountList.splice(
                            $scope.editAccountIndex, 1,
                            $scope.partyAccountMaster);

                    }
                    $scope.listAccount = true;

                } else {

                    $scope.errorMap = new Map();
                    $scope.errorMap.put("accountsDuplicates",
                        $rootScope.nls["ERR6038"]);

                }

            } else {
                console
                    .log("Party Account Master Validation Failed....");
            }
        }

        $scope.partyAccountunique = function(obj, origList) {

            console.log("unique called....");
            var found, y;

            for (var i = 0; i < origList.length; i++) {

                if ($scope.editAccountIndex == i) {

                    continue;
                }

                if (obj.locationMaster.locationName != origList[i].locationMaster.locationName) {
                    continue;
                }

                if (obj.accountMaster.accountCode != origList[i].accountMaster.accountCode) {
                    continue;
                }

                return true;
            }
            return false;

        }

        $scope.addAccountRow = function() {
            $scope.errorMap = new Map();
            $scope.editAccountIndex = null;
            $scope.listAccount = false;
            $scope.partyAccountMaster = {};
            $scope.partyAccountMaster.accountMaster = {};
            $scope.partyAccountMaster.status = "Active";
            $scope.partyAccountMaster.locationMaster = {};
            $scope.partyAccountMaster.currencyMaster = {};
            $scope.partyAccountMaster.billingSubLedgerCode = {};
            $scope.partyAccountMaster.billingAccountName = {};
            discardService.set($scope.partyAccountMaster);

        }

        $scope.editPartyAccount = function(index) {
            $scope.errorMap = new Map();
            $scope.listAccount = false;
            $scope.editAccountIndex = index;
            $scope.partyAccountMaster = cloneService
                .clone($scope.partyMaster.partyAccountList[index]);
            discardService.set($scope.partyAccountMaster);

        }
        $scope.deletePartyAccount = function(index) {
            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected account  ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyAccountList
                            .splice(index, 1);
                    },
                    function(value) {
                        console
                            .log("Account Deletion cancelled");
                    });

        }

        $scope.validateDate = function(index, id) {
            $scope.errorMap = new Map();
            if ($scope.partyMaster.partyAddressList[index].partyCountryField.dateOfBirth != null) {
                var selectedDate = $rootScope.convertToDate($scope.partyMaster.partyAddressList[index].partyCountryField.dateOfBirth);
                var currentDate = new Date();
                if (selectedDate >= currentDate) {
                    $scope.errorMap.put("dob" + index, $rootScope.nls["ERR3736"]);
                } else {
                    $rootScope.navigateToNextField(id);
                }
            }
        }

        $scope.closePartyAccount = function() {
            if (!discardService.compare($scope.partyAccountMaster)) {

                ngDialog
                    .openConfirm({
                        template: '<p>Do you want to update your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.savePartyAccount();
                        } else {
                            $scope.listAccount = true;
                        }
                    });

            } else {
                $scope.listAccount = true;
            }
        }

        // Validation for Party Account

        $scope.addNewBusinessDetail = function() {
            $scope.errorMap = new Map();
            $scope.businessList = false;
            $scope.partyBusinessDetail = {};
            $scope.business = false;
            $scope.editBusinessIndex = null;
            $scope.partyBusinessDetail.serviceMaster = {};
            $scope.partyBusinessDetail.origin = {};
            $scope.partyBusinessDetail.destination = {};
            $scope.partyBusinessDetail.commodityMaster = {};
            $scope.partyBusinessDetail.tosMaster = {};
            $scope.partyBusinessDetail.bussinessStatus = "Gained";

            discardService.set($scope.partyBusinessDetail);
        }

        $scope.closeBussinessDetail = function() {
            if (!discardService.compare($scope.partyBusinessDetail)) {

                ngDialog
                    .openConfirm({
                        template: '<p>Do you want to update your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.savePartyBusinessDetail();
                        } else {
                            $scope.businessList = true;
                        }
                    });

            } else {
                $scope.businessList = true;
            }
        }

        // List Service Master of Business

        // List Service Master of Business

        $scope.showBusinessServices = function(serviceCode) {

            $scope.selectedBusinessServiceIndex = -1;
            $scope.panelTitle = "Service";
            $scope.ajaxBusinessService(null);

        };

        $scope.ajaxBusinessService = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            ServiceList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.partyBusinessServiceList = data.responseObject.searchResult;
                            $scope.showBusinessServiceList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Services');
                    });
        }

        $scope.listConfigBusinessService = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "serviceName",
                seperator: false
            }, {
                "title": "serviceCode",
                seperator: true
            }]
        }

        $scope.selectedBusinessService = function(serviceobj) {
            console.log("serviceobj", serviceobj);
            $scope.partyBusinessDetail.serviceMaster = serviceobj;
            $scope.business = true;
            $scope.partyBusinessDetail.unitCode = ($scope.partyBusinessDetail.serviceMaster.transportMode == "Air" ? 'Kg' :
                'Lb');
            $scope.validateBusinessDetail(1);
            $scope.cancelBusinessService();

        };

        $scope.cancelBusinessService = function() {

            $scope.showBusinessServiceList = false;

        }

        // Tos Businesss

        $scope.showBusinessTos = function(tos) {
            console.log("show tos called ");
            $scope.panelTitle = "TOS";
            $scope.selectedBusinessTosIndex = -1;
            $scope.ajaxBusinessTosEvent(null);

        };
        $scope.ajaxBusinessTosEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            TosSearchKeyword.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.partyBusinessTosList = data.responseObject.searchResult;
                            $scope.showBusinessTosList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Customer Services');
                    });
        }
        $scope.cancelBusinessTos = function() {
            console.log("Cancel tos List Button Pressed....");
            $scope.searchText = "";
            $scope.showBusinessTosList = false;

        };
        $scope.selectedBusinessTos = function(tosObj) {
            $scope.partyBusinessDetail.tosMaster = tosObj;
            $scope.business = true;
            $scope.validateBusinessDetail(11);
            $scope.cancelBusinessTos();
        };

        $scope.listConfigBusinessTos = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "tosName",
                seperator: false
            }, {
                "title": "tosCode",
                seperator: true
            }]
        }

        // Bussiness Origin and Destination

        $scope.showBusinessOrigin = function(port) {
            console.log("show port origin called ");
            $scope.panelTitle = "Origin";
            $scope.selectedItemOriginIndex = -1;
            $scope.ajaxPortOriginEvent(null);
        };

        $scope.ajaxPortOriginEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            PortSearchKeyword.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.originList = [];
                            $scope.resultArr = data.responseObject.searchResult;
                            angular
                                .forEach(
                                    $scope.resultArr,
                                    function(map) {
                                        if ($scope.partyBusinessDetail.serviceMaster == null ||
                                            $scope.partyBusinessDetail.serviceMaster.id == null ||
                                            $scope.partyBusinessDetail.serviceMaster.id.trim == "") {
                                            $scope.originList
                                                .push(map);
                                        } else {
                                            if ($scope.partyBusinessDetail.serviceMaster.transportMode == map.transportMode) {
                                                $scope.originList
                                                    .push(map);
                                            }
                                        }

                                    });
                            $scope.showBusinessoriginList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Port');
                    });

        }

        $scope.selectedOriginPort = function(portObj) {
            $scope.partyBusinessDetail.origin = portObj;
            $scope.business = true;
            $scope.validateBusinessDetail(3);
            $scope.validateBusinessDetail(5);
            $scope.cancelOriginPort();

        };

        $scope.cancelOriginPort = function() {
            $scope.showBusinessoriginList = false;
        }

        // Destination

        $scope.showBusinessDestination = function(port) {
            console.log("show port called ");
            $scope.panelTitle = "Destination";
            $scope.selectedItemDestinationIndex = -1;
            $scope.ajaxPortDestinationEvent(null);
        };

        $scope.ajaxPortDestinationEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            PortSearchKeyword.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.destinationList = [];
                            $scope.resultArr = data.responseObject.searchResult;
                            angular
                                .forEach(
                                    $scope.resultArr,
                                    function(map) {
                                        $scope.destinationList
                                            .push(map);
                                    });

                            $scope.showBusinessDestinationList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Port');
                    });

        }

        $scope.selectedDestinationPort = function(portObj) {
            console.log("portObj", portObj);
            $scope.partyBusinessDetail.destination = portObj;
            $scope.business = true;
            $scope.validateBusinessDetail(5);
            $scope.cancelDestination();

        };

        $scope.cancelDestination = function() {
            $scope.showBusinessDestinationList = false;
        }

        $scope.listConfigBussinessPort = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "portName",
                seperator: false
            }, {
                "title": "portCode",
                seperator: true
            }]
        }

        // commodity
        $scope.showBusinessCommodity = function(commodity) {
            console.log("show Commodity called ");
            $scope.panelTitle = "Commodity";
            $scope.selectedItemCommodityIndex = -1;
            $scope.ajaxBusinessCommodityEvent(null);
        };

        $scope.ajaxBusinessCommodityEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CommodityList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.businesscommodityList = data.responseObject.searchResult;
                            $scope.showBusinessCommodityList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching commodity Services');
                    });
        }
        $scope.selectedBusinessCommodity = function(commodityObj) {
            console.log("commodityObj", commodityObj);
            $scope.partyBusinessDetail.commodity = commodityObj;
            $scope.business = true;
            $scope.validateBusinessDetail(10);
            $scope.cancelBusinessCommodity();

        };

        $scope.cancelBusinessCommodity = function() {
            console
                .log("Cancel Commodity  List Button Pressed....");
            $scope.searchText = "";
            $scope.showBusinessCommodityList = false;
        };

        $scope.listConfigBusinessCommodity = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "hsName",
                seperator: false
            }, {
                "title": "hsCode",
                seperator: true
            }]
        }

        $scope.savePartyBusinessDetail = function() {

            if ($scope.validateBusinessDetail(0)) {

                if ($scope.partyBusinessDetail.closureDate != undefined)
                    $scope.partyBusinessDetail.closureDate = $rootScope
                    .sendApiStartDateTime($scope.partyBusinessDetail.closureDate);

                if ($scope.partyBusinessDetail.currentPotential == undefined ||
                    $scope.partyBusinessDetail.currentPotential == false) {

                    $scope.partyBusinessDetail.currentPotential = "CURRENT";

                } else {

                    $scope.partyBusinessDetail.currentPotential = "POTENTIAL";

                }

                var flag = $scope.PartyBusinessunique(
                    $scope.partyBusinessDetail,
                    $scope.partyMaster.partyBusinessDetailList);

                if (!flag) {

                    if ($scope.editBusinessIndex == null) {

                        if ($scope.partyMaster.partyBusinessDetailList == null)
                            $scope.partyMaster.partyBusinessDetailList = [];

                        $scope.partyMaster.partyBusinessDetailList
                            .push($scope.partyBusinessDetail);

                    } else {

                        $scope.partyMaster.partyBusinessDetailList
                            .splice($scope.editBusinessIndex,
                                1,
                                $scope.partyBusinessDetail);

                    }
                    $scope.businessList = true;

                } else {
                    $scope.errorMap = new Map();
                    $scope.errorMap.put("bussinessDuplicates",
                        $rootScope.nls["ERR6038"]);
                }
            } else

            {
                console.log("Party Business Validation Failed....");
            }
        }

        $scope.PartyBusinessunique = function(obj, origList) {

            console.log("unique called....");
            var found, y;

            for (var i = 0; i < origList.length; i++) {

                if ($scope.editBusinessIndex == i) {

                    continue;
                }

                if (obj.serviceMaster.serviceName != origList[i].serviceMaster.serviceName) {
                    continue;
                }
                if (obj.tosMaster != undefined &&
                    obj.tosMaster != null &&
                    obj.tosMaster != "") {
                    if (obj.tosMaster.tosName != origList[i].tosMaster.tosName)
                        continue;
                }
                if (obj.origin != undefined && obj.origin != null &&
                    obj.origin != "") {
                    if (obj.origin.portCode != origList[i].origin.portCode)
                        continue;
                }
                if (obj.frequency != undefined &&
                    obj.frequency != null &&
                    obj.frequency != "") {
                    if (obj.frequency != origList[i].frequency)
                        continue;
                }
                if (obj.commodityMaster != undefined &&
                    obj.commodityMaster != null &&
                    obj.commodityMaster != "") {
                    if (obj.commodityMaster.hsName != origList[i].commodityMaster.hsName)
                        continue;
                }

                return true;
            }
            return false;

        }

        $scope.serialNo = function(index) {
            var index = index + 1;
            return index;
        }

        $scope.editBusinessDetail = function(index) {
            $scope.businessList = false;
            $scope.business = false;
            $scope.errorMap = new Map();
            $scope.editBusinessIndex = index;
            $scope.partyBusinessDetail = cloneService
                .clone($scope.partyMaster.partyBusinessDetailList[index]);
            if ($scope.partyBusinessDetail.currentPotential == "CURRENT") {
                $scope.partyBusinessDetail.currentPotential = false;
            } else if ($scope.partyBusinessDetail.currentPotential == "POTENTIAL") {
                $scope.partyBusinessDetail.currentPotential = true;

            }

            discardService.set($scope.partyBusinessDetail);
        }

        $scope.deleteBusinessDetail = function(index) {
            ngDialog
                .openConfirm({
                    template: '<p>Are you sure you want to delete selected business detail  ?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.partyMaster.partyBusinessDetailList
                            .splice(index, 1);
                    },
                    function(value) {
                        console
                            .log("Business Deletion cancelled");
                    });

        }

        $scope.changeCurrentPotential = function() {

            if ($scope.partyBusinessDetail.currentPotential == false ||
                $scope.partyBusinessDetail.currentPotential == undefined) {

                $scope.partyBusinessDetail.bussinessStatus = "Gained";

            }

        }

        // Validate Busssiness Details

        $scope.validateBusinessDetail = function(validateCode) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                if ($scope.partyBusinessDetail.serviceMaster.serviceName == null ||
                    $scope.partyBusinessDetail.serviceMaster.serviceName == undefined ||
                    $scope.partyBusinessDetail.serviceMaster.serviceName == "") {

                    $scope.errorMap.put("bussinessService",
                        $rootScope.nls["ERR6008"]);
                    return false;

                }
                if ($scope.partyBusinessDetail.serviceMaster.status == "Block") {
                    $scope.partyBusinessDetail.serviceMaster = null;
                    $scope.errorMap.put("bussinessService",
                        $rootScope.nls["ERR6061"]);
                    return false

                }

            }
            if (validateCode == 0 || validateCode == 3) {

                if ($scope.partyBusinessDetail.origin != null) {
                    if ($scope.partyBusinessDetail.origin.status == "Block") {
                        $scope.partyBusinessDetail.origin = null;
                        $scope.errorMap.put("origin",
                            $rootScope.nls["ERR6062"]);
                        return false

                    }
                }

            }

            if (validateCode == 0 || validateCode == 5) {
                if ($scope.partyBusinessDetail.destination != null &&
                    $scope.partyBusinessDetail.origin != null) {
                    if ($scope.partyBusinessDetail.destination.portCode != undefined) {
                        if ($scope.partyBusinessDetail.destination.portCode == $scope.partyBusinessDetail.origin.portCode) {
                            $scope.errorMap.put("tofromdifferent",
                                $rootScope.nls["ERR6063"]);
                            return false;
                        }
                    }
                }

                if ($scope.partyBusinessDetail.destination != null) {

                    if ($scope.partyBusinessDetail.destination.status == "Block") {
                        $scope.partyBusinessDetail.destination = null;
                        $scope.errorMap.put("tofromdifferent",
                            $rootScope.nls["ERR6064"]);
                        return false

                    }
                }
                if ($scope.partyBusinessDetail.origin != null) {

                    if ($scope.partyBusinessDetail.origin.status == "Block") {
                        $scope.partyBusinessDetail.origin = null;
                        $scope.errorMap.put("businessorigin",
                            $rootScope.nls["ERR6062"]);
                        return false

                    }
                }

            }

            if (validateCode == 0 || validateCode == 6) {

                if ($scope.partyBusinessDetail.estimatedRevenue != null &&
                    $scope.partyBusinessDetail.estimatedRevenuen != "" &&
                    $scope.partyBusinessDetail.estimatedRevenue != undefined) {
                    var regexp = new RegExp(
                        "^[0-9]{0,20}(.[0-9]{0,20})$");
                    if (!regexp
                        .test($scope.partyBusinessDetail.estimatedRevenue)) {
                        $scope.errorMap.put("estimatedRevenue",
                            $rootScope.nls["ERR6101"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 7) {
                if ($scope.partyBusinessDetail.businessNotes != null) {

                    var regexp = new RegExp(
                        "[ A-Za-z0-9\n_@.#&+-]{0,500}");
                    if (!regexp
                        .test($scope.partyBusinessDetail.businessNotes)) {
                        $scope.errorMap.put("businessNotes",
                            $rootScope.nls["ERR6065"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 8) {

                if ($scope.partyBusinessDetail.noOfUnits != null) {

                    var regexp = new RegExp("^[0-9]{0,30}$");
                    if (!regexp
                        .test($scope.partyBusinessDetail.noOfUnits)) {
                        $scope.errorMap.put("noofunits",
                            $rootScope.nls["ERR6102"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 9) {
                if ($scope.partyBusinessDetail.noOfShipments != null) {

                    var regexp = new RegExp("^[0-9]{0,30}$");
                    if (!regexp
                        .test($scope.partyBusinessDetail.noOfShipments)) {
                        $scope.errorMap.put("noOfShipments",
                            $rootScope.nls["ERR6103"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 10) {

                if ($scope.partyBusinessDetail.commodity != null) {
                    if ($scope.partyBusinessDetail.commodity.status == "Block") {

                        $scope.errorMap.put("businesscommodity",
                            $rootScope.nls["ERR6066"]);
                        $scope.partyBusinessDetail.commodity = null;
                        return false

                    }
                }

            }

            if (validateCode == 0 || validateCode == 11) {

                if ($scope.partyBusinessDetail.tosMaster != null) {
                    if ($scope.partyBusinessDetail.tosMaster.status == "Block") {
                        $scope.errorMap.put("businesstos",
                            $rootScope.nls["ERR6067"]);
                        $scope.partyBusinessDetail.tosMaster = null;
                        return false

                    }
                }

            }

            return true;
        };
        $scope.validatePartyAccount = function(validateCode) {

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.partyAccountMaster.accountMaster.id == undefined ||
                    $scope.partyAccountMaster.accountMaster.id == null ||
                    $scope.partyAccountMaster.accountMaster.id == "") {
                    console.log("Account is Mandatory..........")
                    $scope.errorMap.put("accounts",
                        $rootScope.nls["ERR6016"]);
                    return false;
                } else {
                    if ($scope.partyAccountMaster.accountMaster.status == 'Block') {
                        console.log("Selected Account is Blocked.");
                        $scope.partyAccountMaster.accountMaster = null;
                        $scope.errorMap.put("accounts",
                            $rootScope.nls["ERR32"]);
                        return false;
                    }

                    if ($scope.partyAccountMaster.accountMaster.status == 'Hide') {
                        console.log("Selected Account is Hidden.");
                        $scope.partyAccountMaster.accountMaster = null;
                        $scope.errorMap.put("accounts",
                            $rootScope.nls["ERR33"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.partyAccountMaster.status == undefined ||
                    $scope.partyAccountMaster.status == null ||
                    $scope.partyAccountMaster.status == "") {
                    console.log("status is Mandatory..........")
                    $scope.errorMap.put("status",
                        $rootScope.nls["ERR6039"]);
                    return false;
                }

            }

            if (validateCode == 0 || validateCode == 3) {
                if ($scope.partyAccountMaster.locationMaster.id == undefined ||
                    $scope.partyAccountMaster.locationMaster.id == null ||
                    $scope.partyAccountMaster.locationMaster.id == "") {
                    console.log("Location is Mandatory..........")
                    $scope.errorMap.put("acclocation",
                        $rootScope.nls["ERR44"]);
                    return false;
                } else {

                    if ($scope.partyAccountMaster.locationMaster.status == 'Block') {
                        console
                            .log("Selected Location is Blocked.");
                        $scope.partyAccountMaster.locationMaster = null;
                        $scope.errorMap.put("acclocation",
                            $rootScope.nls["ERR6028"]);
                        return false;
                    }

                    if ($scope.partyAccountMaster.locationMaster.status == 'Hide') {
                        console.log("Selected Location is Hidden.");
                        $scope.partyAccountMaster.locationMaster = null;
                        $scope.errorMap.put("acclocation",
                            $rootScope.nls["ERR6029"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 4) {
                if ($scope.partyAccountMaster.currencyMaster.id == undefined ||
                    $scope.partyAccountMaster.currencyMaster.id == null ||
                    $scope.partyAccountMaster.currencyMaster.id == "") {
                    console.log("Currency is Mandatory..........")
                    $scope.errorMap.put("accCurrency",
                        $rootScope.nls["ERR44"]);
                    return false;
                } else {

                    if ($scope.partyAccountMaster.currencyMaster.status == 'Block') {
                        console
                            .log("Selected Currency is Blocked.");
                        $scope.partyAccountMaster.currencyMaster = null;
                        $scope.errorMap.put("accCurrency",
                            $rootScope.nls["ERR6028"]);
                        return false;
                    }

                    if ($scope.partyAccountMaster.currencyMaster.status == 'Hide') {
                        console.log("Selected Currency is Hidden.");
                        $scope.partyAccountMaster.currencyMaster = null;
                        $scope.errorMap.put("accCurrency",
                            $rootScope.nls["ERR6029"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 5) {

                if ($scope.partyAccountMaster.billingAccountName != undefined &&
                    $scope.partyAccountMaster.billingAccountName != null &&
                    $scope.partyAccountMaster.billingAccountName != "")

                    if ($scope.partyAccountMaster.billingSubLedgerCode == undefined ||
                        $scope.partyAccountMaster.billingSubLedgerCode == null ||
                        $scope.partyAccountMaster.billingSubLedgerCode == "") {
                        $scope.errorMap.put("billingSubledgerCode",
                            $rootScope.nls["ERR6018"]);
                        return false;
                    }

            }
            if (validateCode == 0 || validateCode == 6) {

                if ($scope.partyAccountMaster.billingSubLedgerCode != undefined &&
                    $scope.partyAccountMaster.billingSubLedgerCode != null &&
                    $scope.partyAccountMaster.billingSubLedgerCode != "")
                    if ($scope.partyAccountMaster.billingAccountName == undefined ||
                        $scope.partyAccountMaster.billingAccountName == null ||
                        $scope.partyAccountMaster.billingAccountName == "") {
                        $scope.errorMap.put(
                            "billingSubledgerAccName",
                            $rootScope.nls["ERR6019"]);
                        return false;
                    }

            }

            return true;
        }

        /*
         * Form Validation Begins (General Tab)
         */

        $scope.validatePartyMaster = function(validateCode) {

            $scope.errorMap = new Map();
            $rootScope.clientMessage = null;

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.partyMaster.partyName == undefined ||
                    $scope.partyMaster.partyName == null ||
                    $scope.partyMaster.partyName == "") {
                    $scope.errorMap.put("partyName", $rootScope.nls["ERR3506"]);
                    return false;
                } else {
                    if ($rootScope.appMasterData['party.name.special.char.allowed'].toUpperCase() == "FALSE") {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_NAME, $scope.partyMaster.partyName)) {
                            $scope.errorMap.put("partyName", $rootScope.nls["ERR3507"]);
                            return false;
                        }
                    }
                }
            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.partyMaster.countryMaster == null ||
                    $scope.partyMaster.countryMaster.id == undefined ||
                    $scope.partyMaster.countryMaster.id == null ||
                    $scope.partyMaster.countryMaster.id == "") {
                    console.log("Country is Mandatory..........")
                    $scope.errorMap.put("country",
                        $rootScope.nls["ERR36"]);
                    return false;
                } else {
                    if ($scope.partyMaster.countryMaster.status == 'Block') {
                        console.log("Selected Country is Blocked.");
                        $scope.partyMaster.countryMaster = null;
                        $scope.errorMap.put("country",
                            $rootScope.nls["ERR3522"]);
                        $scope.clearPartyAdressCityState();

                        return false;
                    }

                    if ($scope.partyMaster.countryMaster.status == 'Hide') {
                        console.log("Selected Country is Hidden.");
                        $scope.partyMaster.countryMaster = null;
                        $scope.errorMap.put("country",
                            $rootScope.nls["ERR3523"]);
                        $scope.clearPartyAdressCityState();
                        return false;
                    }
                    console
                        .log("Country Selected.........." +
                            $scope.partyMaster.countryMaster.countryCode)
                }
            }

            if (validateCode == 0 || validateCode == 3) {
                var showError = true;
                var blocked = false;

                for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {
                    if ($scope.tempPartyTypeList[i].selected) {
                        showError = false;
                        if ($scope.tempPartyTypeList[i].status == 'Block') {
                            $scope.errorMap.put("partyType",
                                $rootScope.nls["ERR3520"]);
                            return false;
                        }
                        if ($scope.tempPartyTypeList[i].status == 'Hide') {
                            $scope.errorMap.put("partyType",
                                $rootScope.nls["ERR3521"]);
                            return false;
                        }
                    }
                }

                if (showError) {
                    $scope.errorMap.put("partyType",
                        $rootScope.nls["ERR3519"]);
                    $scope.partyTab = 'general';
                    return false;
                }

            }

            if (validateCode == 0 || validateCode == 4) {

                console.log("Validating Salutation...." +
                    validateCode)
                var checkForIndividual = true;

                for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {
                    if ($scope.tempPartyTypeList[i].selected &&
                        $scope.tempPartyTypeList[i].partyTypeCode == "IN") {
                        checkForIndividual = false;
                    }
                }

                if (!checkForIndividual) {
                    if ($scope.partyMaster.salutation == undefined ||
                        $scope.partyMaster.salutation == null ||
                        $scope.partyMaster.salutation == "") {
                        console.log("salutation is Mandatory");
                        $scope.errorMap.put("salutation",
                            $rootScope.nls["ERR3513"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                } else {
                    console.log(checkForIndividual)
                }
            }

            if (validateCode == 0 || validateCode == 5) {

                var checkForIndividual = true;

                for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {
                    if ($scope.tempPartyTypeList[i].selected &&
                        $scope.tempPartyTypeList[i].partyTypeCode == "IN") {
                        checkForIndividual = false;
                    }
                }

                if (!checkForIndividual) {
                    if ($scope.partyMaster.firstName == undefined ||
                        $scope.partyMaster.firstName == null ||
                        $scope.partyMaster.firstName == "") {
                        console.log("First Name is Mandatory");
                        $scope.errorMap.put("firstName",
                            $rootScope.nls["ERR3509"]);
                        $scope.partyTab = 'general';
                        return false;
                    } else {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_FIRST_NAME, $scope.partyMaster.firstName)) {
                            $scope.errorMap.put("firstName", $rootScope.nls["ERR3511"]);
                            $scope.partyTab = 'general';
                            return false;
                        }
                    }
                }
            }

            if (validateCode == 0 || validateCode == 6) {

                var checkForIndividual = true;

                for (var i = 0; i < $scope.tempPartyTypeList.length; i++) {
                    if ($scope.tempPartyTypeList[i].selected &&
                        $scope.tempPartyTypeList[i].partyTypeCode == "IN") {
                        checkForIndividual = false;
                    }
                }

                if (!checkForIndividual) {
                    if ($scope.partyMaster.lastName == undefined ||
                        $scope.partyMaster.lastName == null ||
                        $scope.partyMaster.lastName == "") {
                        console.log("Last Name is Mandatory");
                        $scope.errorMap.put("lastName", $rootScope.nls["ERR3510"]);
                        $scope.partyTab = 'general';
                        return false;
                    } else {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_FIRST_NAME, $scope.partyMaster.lastName)) {
                            $scope.errorMap.put("lastName", $rootScope.nls["ERR3512"]);
                            $scope.partyTab = 'general';
                            return false;
                        }

                    }
                }
            }

            if (validateCode == 0 || validateCode == 7) {

                if ($scope.partyMaster.categoryMaster != undefined &&
                    $scope.partyMaster.categoryMaster != null &&
                    $scope.partyMaster.categoryMaster != "" &&
                    $scope.partyMaster.categoryMaster.id != undefined &&
                    $scope.partyMaster.categoryMaster.id != null &&
                    $scope.partyMaster.categoryMaster.id != "") {

                    if ($scope.partyMaster.categoryMaster.status == 'Block') {
                        console
                            .log("Selected Category is Blocked.");
                        $scope.partyMaster.categoryMaster = null;
                        $scope.errorMap.put("category",
                            $rootScope.nls["ERR3516"]);
                        $scope.partyTab = 'general';
                        return false;
                    }

                    if ($scope.partyMaster.categoryMaster.status == 'Hide') {
                        console.log("Selected Category is Hidden.");
                        $scope.partyMaster.categoryMaster = null;
                        $scope.errorMap.put("category",
                            $rootScope.nls["ERR3517"]);
                        $scope.partyTab = 'general';
                        return false;
                    }

                } else {
                    $scope.partyMaster.categoryMaster = null;
                }

                /*
                 * As discussed with mani we are remove manadory
                 * check
                 * 
                 * if ($scope.partyMaster.categoryMaster ==
                 * undefined || $scope.partyMaster.categoryMaster ==
                 * null || $scope.partyMaster.categoryMaster== "") {
                 * console.log("Category is Mandatory..........")
                 * $scope.errorMap.put("category",$rootScope.nls["ERR3515"]);
                 * $scope.partyTab = 'general'; return false; } else {
                 * 
                 * if($scope.partyMaster.categoryMaster.status ==
                 * 'Block') { console.log("Selected Category is
                 * Blocked."); $scope.partyMaster.categoryMaster =
                 * null;
                 * $scope.errorMap.put("category",$rootScope.nls["ERR3516"]);
                 * $scope.partyTab = 'general'; return false; }
                 * 
                 * if($scope.partyMaster.categoryMaster.status ==
                 * 'Hide') { console.log("Selected Category is
                 * Hidden."); $scope.partyMaster.categoryMaster =
                 * null;
                 * $scope.errorMap.put("category",$rootScope.nls["ERR3517"]);
                 * $scope.partyTab = 'general'; return false; }
                 *  }
                 */
            }

            if (validateCode == 0 || validateCode == 8) {
                if ($scope.partyMaster.partyDetail != undefined &&
                    $scope.partyMaster.partyDetail != null &&
                    $scope.partyMaster.partyDetail.valueAddedTaxNo != null &&
                    $scope.partyMaster.partyDetail.valueAddedTaxNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_VAT_NO, $scope.partyMaster.partyDetail.valueAddedTaxNo)) {
                        console.log("Invalid VAT Number....");
                        $scope.errorMap.put("vatNumber", $rootScope.nls["ERR3749"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 9) {
                if ($scope.partyMaster.partyDetail != undefined &&
                    $scope.partyMaster.partyDetail != null &&
                    $scope.partyMaster.partyDetail.panNo != null &&
                    $scope.partyMaster.partyDetail.panNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_PAN_NO, $scope.partyMaster.partyDetail.panNo)) {
                        console.log("Invalid PAN Number....");
                        $scope.errorMap.put("panNumber", $rootScope.nls["ERR3750"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 10) {
                if ($scope.partyMaster.partyDetail != undefined &&
                    $scope.partyMaster.partyDetail != null &&
                    $scope.partyMaster.partyDetail.svtNo != null &&
                    $scope.partyMaster.partyDetail.svtNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_SVT_NO, $scope.partyMaster.partyDetail.svtNo)) {
                        console.log("Invalid SVT Number....");
                        $scope.errorMap.put("svtNumber", $rootScope.nls["ERR3752"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 11) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.tinNo != null &&
                    $scope.partyMaster.partyDetail.tinNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_TIN_NO, $scope.partyMaster.partyDetail.tinNo)) {
                        console.log("Invalid TIN Number....");
                        $scope.errorMap.put("tinNumber", $rootScope.nls["ERR3755"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 12) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.svatNo != null &&
                    $scope.partyMaster.partyDetail.svatNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_SVAT_NO, $scope.partyMaster.partyDetail.svatNo)) {
                        console.log("Invalid SVAT Number....");
                        $scope.errorMap.put("svatNumber", $rootScope.nls["ERR3753"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 19) {

                if ($scope.localDynamicFormFieldMap.get('Party Master > Party Name (Regional)') == true) {
                    if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.nlsCustomerName == undefined ||
                        $scope.partyMaster.partyDetail.nlsCustomerName == null ||
                        $scope.partyMaster.partyDetail.nlsCustomerName == "") {
                        console.log("Invalid Party Name (Regional)");
                        $scope.errorMap.put("regional", $rootScope.nls["ERR3757"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                } else {
                    if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.nlsCustomerName != null &&
                        $scope.partyMaster.partyDetail.nlsCustomerName != "") {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_REGIONAL_NAME, $scope.partyMaster.partyDetail.nlsCustomerName)) {
                            console.log("Invalid Party Name (Regional)");
                            $scope.errorMap.put("regional", $rootScope.nls["ERR3525"]);
                            $scope.partyTab = 'general';
                            return false;
                        }
                    }
                }

            }

            if (validateCode == 0 || validateCode == 13) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.iataCode != null &&
                    $scope.partyMaster.partyDetail.iataCode != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_IATA_CODE, $scope.partyMaster.partyDetail.iataCode)) {
                        console.log("Invalid IATA Code....");
                        $scope.errorMap.put("iataCode", $rootScope.nls["ERR3756"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 14) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.racNo != null &&
                    $scope.partyMaster.partyDetail.racNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_RAC_NO, $scope.partyMaster.partyDetail.racNo)) {
                        console.log("Invalid RAC Number...");
                        $scope.errorMap.put("racNumber", $rootScope.nls["ERR3754"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 18) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.paymentSchedule != null &&
                    $scope.partyMaster.partyDetail.paymentSchedule.id != null) {

                    if ($scope.partyMaster.partyDetail.paymentSchedule.status == "Block") {
                        $scope.partyMaster.partyDetail.paymentSchedule = null;
                        console.log("Payment Schedule is block")
                        $scope.errorMap.put("paymentSchedule",
                            $rootScope.nls["ERR3765"]);
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    if ($scope.partyMaster.partyDetail.paymentSchedule.status == "Hide") {
                        $scope.partyMaster.partyDetail.paymentSchedule = null;
                        $scope.errorMap.put("paymentSchedule",
                            $rootScope.nls["ERR3766"]);
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 15) {

                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.cafPercentage != null &&
                    $scope.partyMaster.partyDetail.cafPercentage != "" &&
                    $scope.partyMaster.partyDetail.cafPercentage != undefined) {
                    if ($scope.partyMaster.partyDetail.cafPercentage == 0) {
                        $scope.errorMap.put("cafPercentage",
                            $rootScope.nls["ERR6040"]);
                        console
                            .log("Caf Percentage Cannot be Zero");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    var regexp = new RegExp(
                        "^[+]?([0-9]{0,2}(?:[\.][0-9]{0,2})?|\.[0-9]+)$");
                    if (!regexp
                        .test($scope.partyMaster.partyDetail.cafPercentage)) {
                        $scope.errorMap.put("cafPercentage",
                            $rootScope.nls["ERR6041"]);
                        console.log("Caf Percentage is Invalid");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    if ($scope.partyMaster.partyDetail.cafPercentage < 0 ||
                        $scope.partyMaster.partyDetail.cafPercentage >= 100) {
                        $scope.errorMap.put("cafPercentage",
                            $rootScope.nls["ERR6041"]);
                        console.log("Caf Percentage is Invalid");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                }
            }
            if (validateCode == 0 || validateCode == 16) {

                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.ccPercentage != null &&
                    $scope.partyMaster.partyDetail.ccPercentage != "" &&
                    $scope.partyMaster.partyDetail.ccPercentage != undefined) {

                    if ($scope.partyMaster.partyDetail.ccPercentage == 0) {
                        $scope.errorMap.put("ccPercentage",
                            $rootScope.nls["ERR6042"]);
                        console.log("CC Percentage Cannot be Zero");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_DETAIL_CC_PERCENTAGE, $scope.partyMaster.partyDetail.ccPercentage)) {
                        $scope.errorMap.put("ccPercentage", $rootScope.nls["ERR6043"]);
                        console.log("CC Percentage is Invalid");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    if ($scope.partyMaster.partyDetail.ccPercentage < 0 ||
                        $scope.partyMaster.partyDetail.ccPercentage >= 100) {
                        $scope.errorMap.put("ccPercentage",
                            $rootScope.nls["ERR6043"]);
                        console.log("CC Percentage is Invalid");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 17) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.tdsPercentage != null &&
                    $scope.partyMaster.partyDetail.tdsPercentage != "" &&
                    $scope.partyMaster.partyDetail.tdsPercentage != undefined) {
                    if ($scope.partyMaster.partyDetail.tdsPercentage == 0) {
                        $scope.errorMap.put("tdsPercentage",
                            $rootScope.nls["ERR6044"]);
                        console
                            .log("TDS Percentage Cannot be Zero");
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_DETAIL_TDS_PERCENTAGE, $scope.partyMaster.partyDetail.tdsPercentage)) {
                        $scope.errorMap.put("tdsPercentage", $rootScope.nls["ERR6045"]);
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }

                    if ($scope.partyMaster.partyDetail.tdsPercentage < 0 ||
                        $scope.partyMaster.partyDetail.tdsPercentage >= 100) {
                        console.log("TDS Percentage is Invalid");
                        $scope.errorMap.put("tdsPercentage",
                            $rootScope.nls["ERR6045"]);
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 30) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.cstNo != null &&
                    $scope.partyMaster.partyDetail.cstNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CST_NO, $scope.partyMaster.partyDetail.cstNo)) {
                        $scope.errorMap.put("cstNumber", $rootScope.nls["ERR3751"]);
                        $scope.partyTab = 'accountsTab';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 31) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.accountNumber != null &&
                    $scope.partyMaster.partyDetail.accountNumber != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_ACC_NO, $scope.partyMaster.partyDetail.accountNumber)) {
                        $scope.errorMap.put("accountNumber", $rootScope.nls["ERR6092"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                } else {
                    if ($scope.partyMaster.partyDetail != null) {
                        $scope.partyMaster.partyDetail.accountNumber = null;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 32) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.pin != null &&
                    $scope.partyMaster.partyDetail.pin != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_PIN, $scope.partyMaster.partyDetail.pin)) {
                        $scope.errorMap.put("pin", $rootScope.nls["ERR6093"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                } else {
                    $scope.partyMaster.pin = null;
                }
            }


            if (validateCode == 0 || validateCode == 33) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.tsaNo != null &&
                    $scope.partyMaster.partyDetail.tsaNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_TSANO, $scope.partyMaster.partyDetail.tsaNo)) {
                        $scope.errorMap.put("tsaNo", $rootScope.nls["ERR6094"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                } else {
                    $scope.partyMaster.pin = null;
                }
            }


            if (validateCode == 0 || validateCode == 34) {
                if ($scope.partyMaster.partyDetail != null && $scope.partyMaster.partyDetail.spotNo != null &&
                    $scope.partyMaster.partyDetail.spotNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_SPOTNO, $scope.partyMaster.partyDetail.spotNo)) {
                        $scope.errorMap.put("spotNo", $rootScope.nls["ERR6095"]);
                        $scope.partyTab = 'general';
                        return false;
                    }
                } else {
                    $scope.partyMaster.pin = null;
                }
            }

            return true;
        }

        $scope.validatePartyAddressMaster = function(validateCode,
            partyAddress, index) {

            console.log("validatePartyAddressMaster " +
                validateCode);

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                console.log("Address Type : " +
                    partyAddress.addressType)
                if (partyAddress.addressType == undefined) {
                    $scope.errorMap.put("addressType" + index,
                        $rootScope.nls["ERR6046"]);
                    $scope.partyTab = 'address';
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 16) {
                if ($scope.partyAddressMaster != null &&
                    partyAddress.partyCountryField != null) {
                    if (partyAddress.partyCountryField.warehouseProviderName != null &&
                        partyAddress.partyCountryField.warehouseProviderName != "") {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_WAREHOUSE_PROVIDER_NAME, partyAddress.partyCountryField.warehouseProviderName)) {
                            console.log("Invalid Warehouse Name...");
                            $scope.errorMap.put("wareHouse" + index, $rootScope.nls["ERR6047"]);
                            $scope.partyTab = 'address';
                            return false;
                        }
                    }
                }
            }
            if (validateCode == 0 || validateCode == 17) {

            }

            if (validateCode == 0 || validateCode == 18) {

                console.log("Address Line 1 : " +
                    partyAddress.addressLine1)
                if (partyAddress.addressLine1 == undefined ||
                    partyAddress.addressLine1 == null ||
                    partyAddress.addressLine1 == "") {
                    $scope.errorMap.put("addressLine1" + index,
                        $rootScope.nls["ERR3701"]);
                    return false;
                }

            }

            if (validateCode == 118 || validateCode == 0) {
                console.log("Address Line 2 : " + partyAddress.addressLine2)
                if (partyAddress.addressLine2 == undefined ||
                    partyAddress.addressLine2 == null ||
                    partyAddress.addressLine2 == "") {
                    /*
                                    $scope.errorMap.put("addressLine2" + index,
                                        $rootScope.nls["ERR3702"]);
                                    return false;
                                */
                }
            }

            if (validateCode == 0 || validateCode == 19) {

                if (partyAddress.zipCode != null &&
                    partyAddress.zipCode != "") {
                    var regexp = new RegExp("^[0-9a-zA-Z-'.,&@/:;?!()$#*_ ]{0,10}$");
                    if (!regexp.test(partyAddress.zipCode)) {
                        console.log("Invalid ZipCode...");
                        $scope.errorMap.put("zipCode" + index,
                            $rootScope.nls["ERR3709"]);
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 20) {
                if (partyAddress.contactPerson == undefined ||
                    partyAddress.contactPerson == null ||
                    partyAddress.contactPerson == "") {
                    /*
                     * As discussed with mani we are removed
                     * $scope.errorMap.put("contactName"+index,$rootScope.nls["ERR6048"]);
                     * return false;
                     */
                }

                if (partyAddress.contactPerson != undefined &&
                    partyAddress.contactPerson != null &&
                    partyAddress.contactPerson != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CONTACT_PERSON, partyAddress.contactPerson)) {
                        console.log("Invalid Contact Person Name...");
                        $scope.errorMap.put("contactName" + index, $rootScope.nls["ERR3715"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 21) {

                /*
                 * if(partyAddress.mobileNo == undefined ||
                 * partyAddress.mobileNo == null ||
                 * partyAddress.mobileNo == "") {
                 * $scope.errorMap.put("mobileNumber","Mobile Number
                 * is mandatory"); return false; }
                 */

                if (partyAddress.mobileNo != undefined &&
                    partyAddress.mobileNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER, partyAddress.mobileNo)) {
                        console.log("Mobile Number Invalid");
                        $scope.errorMap.put("mobileNumber" + index, $rootScope.nls["ERR3712"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 22) {
                if (partyAddress.phone != undefined &&
                    partyAddress.phone != "") {

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, partyAddress.phone)) {
                        console.log("Phone Number Invalid");
                        $scope.errorMap.put("phoneNumber" + index, $rootScope.nls["ERR3711"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 23) {
                if (partyAddress.email != undefined &&
                    partyAddress.email != "") {

                    if (!CommonValidationService.checkMultipleMail(partyAddress.email)) {
                        $scope.errorMap.put("email" + index, $rootScope.nls["ERR3714"]);
                        return false;
                    }
                }

            }
            if (validateCode == 0 || validateCode == 24) {
                if (partyAddress.fax != undefined &&
                    partyAddress.fax != "") {

                }
            }

            if (validateCode == 0 || validateCode == 25) {
                if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.branchSlNo != undefined &&
                    partyAddress.partyCountryField.branchSlNo != null &&
                    partyAddress.partyCountryField.branchSlNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_BRANCH_SLNO, partyAddress.partyCountryField.branchSlNo)) {
                        console.log("branchSlNo Invalid");
                        $scope.errorMap.put("branchSlNo" + index, $rootScope.nls["ERR3724"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 26) {
                if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.bankDLRCode != undefined &&
                    partyAddress.partyCountryField.bankDLRCode != null &&
                    partyAddress.partyCountryField.bankDLRCode != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_BANK_DLR_CODE, partyAddress.partyCountryField.bankDLRCode)) {
                        console.log("bankDLRCode Invalid");
                        $scope.errorMap.put("bankDLRCode" + index, $rootScope.nls["ERR3720"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 27) {

                if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.knownShipperValidationNo != undefined &&
                    partyAddress.partyCountryField.knownShipperValidationNo != null &&
                    partyAddress.partyCountryField.knownShipperValidationNo != "" &&
                    ($scope.localDynamicFormFieldMap.get("Party Master > Shipper Validation") == true || $scope.localDynamicFormFieldMap.get("Party Master > Shipper Validation") == 'true')) {

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_SHIPPER_VALIDATION_NO, partyAddress.partyCountryField.knownShipperValidationNo)) {
                        console.log("shipperValidationNumber Invalid");
                        $scope.errorMap.put("shipperValidationNumber" + index, $rootScope.nls["ERR6049"]);
                        return false;
                    } else {
                        if (partyAddress.partyCountryField.knownShipperValidationDate == undefined || partyAddress.partyCountryField.knownShipperValidationDate == null ||
                            partyAddress.partyCountryField.knownShipperValidationDate == "") {
                            $log.error('shipperValidationDate is mandatory');
                            $scope.errorMap.put("shipperValidationDate" + index, $rootScope.nls["ERR6096"]);
                            return false;
                        }
                    }
                }
            }


            if (validateCode == 0 || validateCode == 28) {

                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster.countryCode != null &&
                    ($scope.localDynamicFormFieldMap.get("Party Master > ISF") == true || $scope.localDynamicFormFieldMap.get("Party Master > ISF") == 'true')) {

                    if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.idType != undefined && partyAddress.partyCountryField.idType != null) {
                        if (partyAddress.partyCountryField.idNumber == undefined ||
                            partyAddress.partyCountryField.idNumber == null ||
                            partyAddress.partyCountryField.idNumber == "") {
                            $scope.errorMap.put("idNumber" + index,
                                $rootScope.nls["ERR3723"]);
                            $scope.editmoreAddressTab = 'editifsBtn';

                            return false;
                        } else {
                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_ID_NUMBER, partyAddress.partyCountryField.idNumber)) {
                                $scope.errorMap.put("idNumber" + index, $rootScope.nls["ERR3722"]);
                                $scope.editmoreAddressTab = 'editifsBtn';
                                return false;
                            }
                        }
                    } else {
                        if (partyAddress.partyCountryField != null) {
                            partyAddress.partyCountryField.idType = null;
                            partyAddress.partyCountryField.idNumber = null;
                        }
                    }

                }

            }

            if (validateCode == 0 || validateCode == 29) {
                if (partyAddress.stateMaster == null ||
                    partyAddress.stateMaster.id == null) {
                    /*
                     * As discussed with mani we are removed
                     * mandatory check
                     * $scope.errorMap.put("state"+index,$rootScope.nls["ERR3706"]);
                     * return false;
                     */
                } else {

                    if (partyAddress.stateMaster.status == 'Block') {
                        console.log("Selected State is Blocked.");
                        partyAddress.stateMaster = null;
                        $scope.errorMap.put("state" + index,
                            $rootScope.nls["ERR3708"]);
                        return false;
                    }

                    if (partyAddress.stateMaster.status == 'Hide') {
                        console.log("Selected State is Hidden.");
                        partyAddress.stateMaster = null;
                        $scope.errorMap.put("state" + index,
                            $rootScope.nls["ERR3707"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0) {
                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster.countryCode != null &&
                    ($scope.localDynamicFormFieldMap.get("Party Master > AES") == true || $scope.localDynamicFormFieldMap.get("Party Master > AES") == 'true')) {
                    if (partyAddress.partyCountryField.aesConsigneeType == undefined ||
                        partyAddress.partyCountryField.aesConsigneeType == null ||
                        partyAddress.partyCountryField.aesConsigneeType == "") {
                        /*$scope.errorMap.put("aesConsigneeType" +index, $rootScope.nls["ERR3734"]);
                        $scope.editmoreAddressTab = 'editaesBtn';
                        return false;*/
                    }
                }
            }

            if (validateCode == 0 || validateCode == 31) {
                if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.bondNo != undefined &&
                    partyAddress.partyCountryField.bondNo != null &&
                    partyAddress.partyCountryField.bondNo != "") {

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_ID_NUMBER, partyAddress.partyCountryField.bondNo)) {
                        console.log("Bond Number Invalid");
                        $scope.errorMap.put("bondNumber" + index, $rootScope.nls["ERR3727"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 32) {
                if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.bondHolder != undefined &&
                    partyAddress.partyCountryField.bondHolder != null &&
                    partyAddress.partyCountryField.bondHolder != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_BOND_HOLDER, partyAddress.partyCountryField.bondHolder)) {
                        console.log("Bond Holder Invalid");
                        $scope.errorMap.put("bondHolder" + index, $rootScope.nls["ERR3726"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 33) {
                if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.bondSuretyCode != undefined &&
                    partyAddress.partyCountryField.bondSuretyCode != null &&
                    partyAddress.partyCountryField.bondSuretyCode != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_BOND_SECURITY_CODE, partyAddress.partyCountryField.bondSuretyCode)) {
                        console.log("Surety Code Invalid");
                        $scope.errorMap.put("suretyCode" + index, $rootScope.nls["ERR3728"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 34) {

                /*
                 * if(partyAddress.countryMaster == null ||
                 * partyAddress.countryMaster.id == null ||
                 * partyAddress.countryMaster == undefined) {
                 * $scope.errorMap.put("city","Select Country"); }
                 */

                if (partyAddress.cityMaster == null ||
                    partyAddress.cityMaster.id == undefined ||
                    partyAddress.cityMaster.id == null ||
                    partyAddress.cityMaster.id == "") {
                    partyAddress.cityMaster = null;
                } else {
                    if (partyAddress.cityMaster.status == 'Block') {
                        console.log("Selected City is Blocked.");
                        partyAddress.cityMaster = null;
                        $scope.errorMap.put("city" + index,
                            $rootScope.nls["ERR3719"]);
                        return false;
                    }

                    if (partyAddress.cityMaster.status == 'Hide') {
                        console.log("Selected City is Hidden.");
                        partyAddress.cityMaster = null;
                        $scope.errorMap.put("city" + index,
                            $rootScope.nls["ERR3718"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 35) {

                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster.countryCode != null &&
                    ($scope.localDynamicFormFieldMap.get("Party Master > ISF") == true || $scope.localDynamicFormFieldMap.get("Party Master > ISF") == 'true')) {

                    if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.idType != undefined && partyAddress.partyCountryField.idType != null) {

                        if (partyAddress.partyCountryField.countryOfissuance == null ||
                            partyAddress.partyCountryField.countryOfissuance.id == undefined ||
                            partyAddress.partyCountryField.countryOfissuance.id == null ||
                            partyAddress.partyCountryField.countryOfissuance.id == "") {
                            $scope.errorMap.put("countryOfissuance" +
                                index, $rootScope.nls["ERR3729"]);
                            $scope.editmoreAddressTab = 'editifsBtn';
                            return false;
                        } else {

                            if (partyAddress.partyCountryField.countryOfissuance.status == 'Block') {
                                console
                                    .log("Selected Country Of Issuance is Blocked.");
                                partyAddress.partyCountryField.countryOfissuance = null;
                                $scope.errorMap.put("countryOfissuance" +
                                    index,
                                    $rootScope.nls["ERR3730"]);
                                $scope.editmoreAddressTab = 'editifsBtn';
                                return false;
                            }

                            if (partyAddress.partyCountryField.countryOfissuance.status == 'Hide') {
                                console
                                    .log("Selected Country Of Issuance is Hidden.");
                                partyAddress.partyCountryField.countryOfissuance = null;
                                $scope.errorMap.put("countryOfissuance" +
                                    index,
                                    $rootScope.nls["ERR3731"]);
                                $scope.editmoreAddressTab = 'editifsBtn';
                                return false;
                            }

                        }

                    } else {
                        if (partyAddress.partyCountryField != null) {
                            partyAddress.partyCountryField.idType = null;
                            partyAddress.partyCountryField.countryOfissuance = null;
                        }
                    }


                }
            }



            if (validateCode == 0 || validateCode == 36) {
                if ($scope.partyMaster.partyAddressList != null &&
                    $scope.partyMaster.partyAddressList.length != 0) {
                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {

                        for (var j = 0; j < $scope.partyMaster.partyAddressList.length; j++) {

                            if (i != j &&
                                $scope.partyMaster.partyAddressList[j].addressType != 'Other' &&
                                $scope.partyMaster.partyAddressList[i].addressType == $scope.partyMaster.partyAddressList[j].addressType) {
                                console
                                    .log("Duplicate address type")
                                $rootScope.clientMessage = $rootScope.nls["ERR6050"];
                                // $scope.errorMap.put("addressType"+index,$rootScope.nls["ERR6050"]);
                                return false;
                            }

                        }

                    }
                }
            }

            if (validateCode == 0 || validateCode == 37) {
                if ($scope.partyMaster.countryMaster != undefined &&
                    $scope.partyMaster.countryMaster.countryCode != null &&
                    ($scope.localDynamicFormFieldMap.get("Party Master > ISF") == true || $scope.localDynamicFormFieldMap.get("Party Master > ISF") == 'true') &&
                    partyAddress.partyCountryField != null && partyAddress.partyCountryField.idType != undefined && partyAddress.partyCountryField.idType != null) {
                    if (partyAddress.partyCountryField.idType == 'SSN') {
                        if (partyAddress.partyCountryField.dateOfBirth == undefined ||
                            partyAddress.partyCountryField.dateOfBirth == null ||
                            partyAddress.partyCountryField.dateOfBirth == "") {
                            $scope.errorMap.put("dob" + index, $rootScope.nls["ERR3735"]);
                            $scope.editmoreAddressTab = 'editifsBtn';
                            return false;
                        } else {
                            var dob;
                            if (typeof partyAddress.partyCountryField.dateOfBirth == "string") {
                                dob = $rootScope.convertToDate(partyAddress.partyCountryField.dateOfBirth);
                            } else {
                                dob = partyAddress.partyCountryField.dateOfBirth;
                            }
                            if (moment().diff(dob) >= 0) {
                                console.log("valid date of birth");
                            } else {
                                $scope.errorMap.put("dob" + index, $rootScope.nls["ERR3736"]);
                                $scope.editmoreAddressTab = 'editifsBtn';
                                return false;
                            }
                        }
                    } else {
                        partyAddress.partyCountryField.dateOfBirth = null;
                    }
                } else {
                    if (partyAddress.partyCountryField != null) {
                        partyAddress.partyCountryField.idType = null;
                    }
                }
            }
            $rootScope.clientMessage = null;
            return true;
        }

        $scope.idTypeChange = function(partyAddress, index, ifNull, ifNotNull) {
            if (partyAddress.partyCountryField.idType == undefined || partyAddress.partyCountryField.idType == null || partyAddress.partyCountryField.idType == '') {
                partyAddress.partyCountryField.idType = null;
                partyAddress.partyCountryField.dateOfBirth = null;
                partyAddress.partyCountryField.countryOfissuance = null;
                partyAddress.partyCountryField.idNumber = null
                if ($scope.localDynamicFormFieldMap.get('Party Master > ISF') == true || $scope.localDynamicFormFieldMap.get('Party Master > ISF') == 'true') {
                    $rootScope.navigateToNextField(ifNotNull + index);
                } else {
                    $rootScope.navigateToNextField('partyName');
                }
            } else {
                $rootScope.navigateToNextField(ifNotNull + index);

            }

        }

        $scope.faxNoTabNavigation = function(id, index) {
            if ($scope.localDynamicFormFieldMap.get('Party Master > ISF') == true || $scope.localDynamicFormFieldMap.get('Party Master > ISF') == 'true') {
                $rootScope.navigateToNextField(id + index);
            } else {
                $rootScope.navigateToNextField('partyName');
            }
        }

        $scope.gradeSelection = function(nextId) {
            if (nextId != undefined) {
                $rootScope.navigateToNextField(nextId);
            }
        }

        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Block') {
                return 'blockedtype';
            } else if (data == 'Hide') {
                return 'hiddentype';
            }
        }

        $scope.validatePartyServiceMaster = function(validateCode) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.partyServiceMaster.serviceMaster == null ||
                    $scope.partyServiceMaster.serviceMaster.id == undefined ||
                    $scope.partyServiceMaster.serviceMaster.id == null ||
                    $scope.partyServiceMaster.serviceMaster.id == "") {
                    console.log("Service is Mandatory.")
                    $scope.partyServiceMaster.serviceMaster = null;
                    $scope.errorMap.put("service",
                        $rootScope.nls["ERR6008"]);
                    return false;
                } else {

                    if ($scope.partyServiceMaster.serviceMaster.status == 'Block') {
                        console.log("Selected Service is Blocked.");
                        $scope.partyServiceMaster.serviceMaster = null;
                        $scope.errorMap.put("service",
                            $rootScope.nls["ERR24"]);
                        return false;
                    }

                    if ($scope.partyServiceMaster.serviceMaster.status == 'Hide') {
                        console.log("Selected Service is Hidden.");
                        $scope.partyServiceMaster.serviceMaster = null;
                        $scope.errorMap.put("service",
                            $rootScope.nls["ERR25"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.partyServiceMaster.tosMaster != null &&
                    $scope.partyServiceMaster.tosMaster.id != undefined &&
                    $scope.partyServiceMaster.tosMaster.id != null &&
                    $scope.partyServiceMaster.tosMaster.id != "") {

                    if ($scope.partyServiceMaster.tosMaster.status == 'Block') {
                        console.log("Selected Tos is Blocked.");
                        $scope.partyServiceMaster.tosMaster = null;
                        $scope.errorMap.put("tos",
                            $rootScope.nls["ERR26"]);
                        return false;
                    }

                    if ($scope.partyServiceMaster.tosMaster.status == 'Hide') {
                        console.log("Selected Tos is Hidden.");
                        $scope.partyServiceMaster.tosMaster = null;
                        $scope.errorMap.put("tos",
                            $rootScope.nls["ERR27"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 3) {
                if ($scope.partyServiceMaster.locationMaster == null ||
                    $scope.partyServiceMaster.locationMaster.id == undefined ||
                    $scope.partyServiceMaster.locationMaster.id == null ||
                    $scope.partyServiceMaster.locationMaster.id == "") {
                    console.log("Location is Mandatory..........")
                    $scope.errorMap.put("location",
                        $rootScope.nls["ERR44"]);
                    return false;
                } else {

                    if ($scope.partyServiceMaster.locationMaster.status == 'Block') {
                        console
                            .log("Selected Location is Blocked.");
                        $scope.partyServiceMaster.locationMaster = null;
                        $scope.errorMap.put("location",
                            $rootScope.nls["ERR6028"]);
                        return false;
                    }

                    if ($scope.partyServiceMaster.locationMaster.status == 'Hide') {
                        console.log("Selected Location is Hidden.");
                        $scope.partyServiceMaster.locationMaster = null;
                        $scope.errorMap.put("location",
                            $rootScope.nls["ERR6029"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 4) {
                if ($scope.partyServiceMaster.salesman == null ||
                    $scope.partyServiceMaster.salesman.id == undefined ||
                    $scope.partyServiceMaster.salesman.id == null ||
                    $scope.partyServiceMaster.salesman.id == "") {
                    console.log("Salesman is Mandatory..........")
                    $scope.errorMap.put("salesman",
                        $rootScope.nls["ERR6051"]);
                    return false;
                } else {

                    if ($scope.partyServiceMaster.salesman.status == 'Block') {
                        console
                            .log("Selected Salesman is Blocked.");
                        $scope.partyServiceMaster.salesman = null;
                        $scope.errorMap.put("salesman",
                            $rootScope.nls["ERR28"]);
                        return false;
                    }

                    if ($scope.partyServiceMaster.salesman.status == 'Hide') {
                        $scope.partyServiceMaster.salesman = null;
                        console.log("Selected Salesman is Hidden.");
                        $scope.errorMap.put("salesman",
                            $rootScope.nls["ERR29"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 5) {
                if ($scope.partyServiceMaster.customerService == null ||
                    $scope.partyServiceMaster.customerService.id == undefined ||
                    $scope.partyServiceMaster.customerService.id == null ||
                    $scope.partyServiceMaster.customerService.id == "") {

                } else {

                    if ($scope.partyServiceMaster.customerService.status == 'Block') {
                        console
                            .log("Selected Customer Service Person Name is Blocked.");
                        $scope.partyServiceMaster.customerService = null;
                        $scope.errorMap.put("customerService",
                            $rootScope.nls["ERR30"]);
                        return false;
                    }

                    if ($scope.partyServiceMaster.customerService.status == 'Hide') {
                        $scope.partyServiceMaster.customerService = null;
                        console
                            .log("Selected Customer Service Person Name is Hidden.");
                        $scope.errorMap.put("customerService",
                            $rootScope.nls["ERR31"]);
                        return false;
                    }

                }
            }
            return true;
        }

        $scope.validatePartyCompanyMapping = function(validateCode) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.partyCompanyAssociation.companyMaster == null ||
                    $scope.partyCompanyAssociation.companyMaster.id == undefined ||
                    $scope.partyCompanyAssociation.companyMaster.id == null ||
                    $scope.partyCompanyAssociation.companyMaster.id == "") {
                    console.log("Company is Mandatory..........")
                    $scope.errorMap.put("company",
                        $rootScope.nls["ERR6012"]);
                    return false;
                } else {
                    $scope.errorMap.put("company", null);
                    console
                        .log("Company Selected.........." +
                            $scope.partyCompanyAssociation.companyMaster.companyCode)
                }
            }
            if (validateCode == 0 || validateCode == 2) {
                if ($scope.partyCompanyAssociation.divisionMaster != null &&
                    $scope.partyCompanyAssociation.divisionMaster != undefined ||
                    $scope.partyCompanyAssociation.divisionMaster.id != null ||
                    $scope.partyCompanyAssociation.divisionMaster.id != "") {

                    if ($scope.partyCompanyAssociation.divisionMaster.status == 'Block') {
                        console
                            .log("Selected Division is Blocked." +
                                $scope.partyCompanyAssociation.divisionMaster.divisionCode);
                        $scope.partyCompanyAssociation.divisionMaster = null;
                        $scope.errorMap.put("division",
                            $rootScope.nls["ERR22"]);
                        return false;
                    }

                    if ($scope.partyCompanyAssociation.divisionMaster.status == 'Hide') {
                        $scope.partyCompanyAssociation.divisionMaster = null;
                        console
                            .log("Selected Division is Hidden." +
                                $scope.partyCompanyAssociation.divisionMaster.divisionCode);
                        $scope.errorMap.put("division",
                            $rootScope.nls["ERR23"]);
                        return false;
                    }

                }

            }
            return true;
        }

        $scope.validatePartyCreditLimitMapping = function(
            validateCode) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.partyCreditLimit.locationMaster == null ||
                    $scope.partyCreditLimit.locationMaster.id == undefined ||
                    $scope.partyCreditLimit.locationMaster.id == null ||
                    $scope.partyCreditLimit.locationMaster.id == "") {
                    console.log("Location is Mandatory..........")
                    $scope.errorMap.put("creditlocation",
                        $rootScope.nls["ERR44"]);
                    return false;
                } else {

                    if ($scope.partyCreditLimit.locationMaster.status == 'Block') {
                        console
                            .log("Selected Location is Blocked.");
                        $scope.partyCreditLimit.locationMaster = null;
                        $scope.errorMap.put("creditlocation",
                            $rootScope.nls["ERR6028"]);
                        return false;
                    }

                    if ($scope.partyCreditLimit.locationMaster.status == 'Hide') {
                        console.log("Selected Location is Hidden.");
                        $scope.partyCreditLimit.locationMaster = null;
                        $scope.errorMap.put("creditlocation",
                            $rootScope.nls["ERR6029"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.partyCreditLimit.serviceMaster != null &&
                    $scope.partyCreditLimit.serviceMaster.id != undefined &&
                    $scope.partyCreditLimit.serviceMaster.id != null &&
                    $scope.partyCreditLimit.serviceMaster.id != "") {

                    if ($scope.partyCreditLimit.serviceMaster.status == 'Block') {
                        console.log("Selected Service is Blocked.");
                        $scope.partyCreditLimit.serviceMaster = null;
                        $scope.errorMap.put("creditservice",
                            $rootScope.nls["ERR24"]);
                        return false;
                    }

                    if ($scope.partyCreditLimit.serviceMaster.status == 'Hide') {
                        console.log("Selected Service is Hidden.");
                        $scope.partyCreditLimit.serviceMaster = null;
                        $scope.errorMap.put("creditservice",
                            $rootScope.nls["ERR25"]);
                        return false;
                    }

                } else {
                    if ($scope.partyCreditLimit.divisionMaster != null &&
                        $scope.partyCreditLimit.divisionMaster.id == undefined ||
                        $scope.partyCreditLimit.divisionMaster.id == null ||
                        $scope.partyCreditLimit.divisionMaster.id == "") {
                        $scope.errorMap.put("errorMsg",
                            $rootScope.nls["ERR6052"]);
                        return false;
                    }
                }

            }
            if (validateCode == 0 || validateCode == 3) {
                if ($scope.partyCreditLimit.divisionMaster != null &&
                    $scope.partyCreditLimit.divisionMaster.id != undefined &&
                    $scope.partyCreditLimit.divisionMaster.id != null &&
                    $scope.partyCreditLimit.divisionMaster.id != "") {

                    if ($scope.partyCreditLimit.divisionMaster.status == 'Block') {
                        console
                            .log("Selected Division is Blocked." +
                                $scope.partyCreditLimit.divisionMaster.divisionCode);
                        $scope.partyCreditLimit.divisionMaster = null;
                        $scope.errorMap.put("creditdivision",
                            $rootScope.nls["ERR22"]);
                        return false;
                    }

                    if ($scope.partyCreditLimit.divisionMaster.status == 'Hide') {
                        console
                            .log("Selected Division is Hidden." +
                                $scope.partyCreditLimit.divisionMaster.divisionCode);
                        $scope.partyCreditLimit.divisionMaster = null;
                        $scope.errorMap.put("creditdivision",
                            $rootScope.nls["ERR23"]);
                        return false;
                    }

                } else {
                    if ($scope.partyCreditLimit.serviceMaster.id == undefined ||
                        $scope.partyCreditLimit.serviceMaster.id == null ||
                        $scope.partyCreditLimit.serviceMaster.id == "") {
                        $scope.errorMap.put("errorMsg",
                            $rootScope.nls["ERR6052"]);
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 4) {
                if ($scope.partyCreditLimit.creditDays != undefined &&
                    $scope.partyCreditLimit.creditDays != null &&
                    $scope.partyCreditLimit.creditDays != "") {
                    console.log($scope.partyCreditLimit.creditDays);
                    if ($scope.partyCreditLimit.creditDays == 0) {
                        $scope.errorMap.put("creditDays",
                            $rootScope.nls["ERR6026"]);
                        return false;
                    }
                    var regexp = new RegExp("^[0-9]{0,3}$");
                    if (!regexp
                        .test($scope.partyCreditLimit.creditDays)) {
                        $scope.errorMap.put("creditDays",
                            $rootScope.nls["ERR6022"]);
                        return false;
                    }

                } else {
                    if ($scope.partyCreditLimit.creditAmount == undefined ||
                        $scope.partyCreditLimit.creditAmount == null ||
                        $scope.partyCreditLimit.creditAmount == "") {
                        $scope.errorMap.put("errorMsg",
                            $rootScope.nls["ERR6027"]);
                        return false;
                    }
                }
            }
            if (validateCode == 0 || validateCode == 5) {
                if ($scope.partyCreditLimit.publishCreditDays != undefined &&
                    $scope.partyCreditLimit.publishCreditDays != null &&
                    $scope.partyCreditLimit.publishCreditDays != "") {

                    if ($scope.partyCreditLimit.publishCreditDays == 0) {
                        $scope.errorMap.put("publishedCreditDays",
                            $rootScope.nls["ERR6020"]);
                        return false;
                    }
                    var regexp = new RegExp("^[0-9]{0,3}$");
                    if (!regexp
                        .test($scope.partyCreditLimit.publishCreditDays)) {
                        $scope.errorMap.put("publishedCreditDays",
                            $rootScope.nls["ERR6023"]);
                        return false;
                    }

                }
            }
            if (validateCode == 0 || validateCode == 6) {
                if ($scope.partyCreditLimit.creditAmount != undefined &&
                    $scope.partyCreditLimit.creditAmount != null &&
                    $scope.partyCreditLimit.creditAmount != "") {

                    if ($scope.partyCreditLimit.creditAmount == 0) {
                        $scope.errorMap.put("creditAmount",
                            $rootScope.nls["ERR6021"]);
                        return false;
                    }
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.partyCreditLimit.creditAmount)) {
                        $scope.errorMap.put("creditAmount",
                            $rootScope.nls["ERR6024"]);
                        return false;
                    }

                } else {
                    if ($scope.partyCreditLimit.creditDays == undefined ||
                        $scope.partyCreditLimit.creditDays == null ||
                        $scope.partyCreditLimit.creditDays == "") {
                        $scope.errorMap.put("errorMsg",
                            $rootScope.nls["ERR6027"]);
                        return false;
                    }
                }
            }

            return true;

        }

        // party email mapping start

        $scope.unique = function(obj, origList) {
            console.log("unique called....");
            var found, y;
            found = undefined;
            for (y = 0; y < origList.length; y++) {
                if (obj == origList[y]) {
                    console.log(" Duplicate Object found....");
                    $scope.errorMap.put("partyEmail",
                        $rootScope.nls["ERR6059"]);
                    return false;
                }
            }
            if (!found) {
                console.log("Object Added....");
                origList.push(obj);
            }

            return origList;
        }
        $scope.cityStateLine = function(partyMaster, partyAddress) {
            return addressJoiner.cityStateLine(partyMaster, partyAddress)
        }
        $scope.dynamicTextbox = function() {

            if ($scope.validatePartyEmailMapping(1) &&
                $scope.partyEmailMapping.email != null &&
                $scope.partyEmailMapping.email != "") {
                // $scope.emailList.push($scope.partyEmailMapping.tempEmail);
                $scope.duplicateFlag = $scope.unique(
                    $scope.partyEmailMapping.email,
                    $scope.emailList);
                if ($scope.duplicateFlag == false) {
                    return false;
                }
                $scope.partyEmailMapping.tempEmail = null;
            } else {
                console.log("Email validation failed");
            }

        }
        $scope.deleteTextbox = function(index) {

            $scope.emailList.splice(index, 1);
        }

        $scope.addTempEmailBox = function() {

            $scope.tempEmailList = [];
            $scope.tempEmailList.push("");
        }

        $scope.partyEmailMappingIndex = -1;
        $scope.addEmailRow = function() {

            if ($scope.hasAccess('PARTY_EMAIL_TAB_ADD')) {
                $scope.emailFlag = "Add";
                $scope.errorMap = new Map();
                $scope.tempEmailList = [];
                $scope.tempEmailList.push("");
                $scope.emailList = [];
                $scope.itemGroup = [];
                $scope.showChild = true;
                $scope.listEmail = false;
                $scope.emailFirstFocus = true;

                var partyEmailMapping = {};
                partyEmailMapping.status = "ACTIVE";
                partyEmailMapping.locationMaster = {};
                partyEmailMapping.originCountry = {};
                partyEmailMapping.destinationCountry = {};
                partyEmailMapping.partyEmailMessageMappingList = [];
                partyEmailMapping.mailEvents = true;
                partyEmailMapping.mails = new Map();
                partyEmailMapping.groupStatus = new Map();

                if ($scope.partyMaster.partyEmailMappingList == undefined ||
                    $scope.partyMaster.partyEmailMappingList == null ||
                    $scope.partyMaster.partyEmailMappingList.length == 0) {
                    $scope.partyMaster.partyEmailMappingList = [];
                }
                var flag = true;
                var errorIndex;
                if ($scope.partyMaster.partyEmailMappingList.length != 0) {
                    for (var i = 0; i < $scope.partyMaster.partyEmailMappingList.length; i++) {
                        if ($scope.validatePartyEmailMapping(0, $scope.partyMaster.partyEmailMappingList[i], i))
                            flag = true;
                        else {
                            flag = false;
                            errorIndex = i;
                            break;

                        }
                    }

                    if (flag) {
                        $scope.partyMaster.partyEmailMappingList
                            .push(partyEmailMapping);
                    }
                } else {
                    $scope.partyMaster.partyEmailMappingList
                        .push(partyEmailMapping);
                }
                if (flag == false) {
                    $scope.partyEmailMappingIndex = errorIndex;
                } else {
                    $scope.partyEmailMappingIndex = $scope.partyMaster.partyEmailMappingList.length - 1;
                }
                $rootScope.navigateToNextField('partyEmailMapping' +
                    $scope.partyEmailMappingIndex);


            }

        }

        $scope.ajaxPartyEmailLocationEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CountryWiseLocation
                .fetch({
                    "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                }, $scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.locationList = data.responseObject.searchResult;
                            return $scope.locationList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Locations');
                    });

        }

        $scope.selectedPartyEmailLocation = function(nextFieldId,
            partyEmailMapping, index) {
            if ($scope.validatePartyEmailMapping(2,
                    partyEmailMapping, index)) {
                $rootScope.navigateToNextField(nextFieldId + index);
            }
        };

        $scope.ajaxPartyEmailOrgCountryEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.countryList = [];
            return CountryList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.countryList = data.responseObject.searchResult;
                            if ($scope.partyEmailMapping != undefined &&
                                $scope.partyEmailMapping.destinationCountry != undefined &&
                                $scope.partyEmailMapping.destinationCountry != null &&
                                $scope.partyEmailMapping.destinationCountry.id != null) {
                                for (var i = 0; i < $scope.countryList.length; i++) {
                                    if ($scope.countryList[i].id === $scope.partyEmailMapping.destinationCountry.id)
                                        $scope.countryList
                                        .splice(i,
                                            1);
                                }

                            }
                            return $scope.countryList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Countries');
                    });

        }
        $scope.selectedPartyEmailOriginCountry = function(
            nextFieldId, partyEmailMapping, index) {
            if ($scope.validatePartyEmailMapping(3,
                    partyEmailMapping, index)) {
                $rootScope.navigateToNextField(nextFieldId + index);
            }
        };

        $scope.ajaxPartEmailDestCountryEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.countryToList = [];
            return CountryList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.countryToList = data.responseObject.searchResult;
                            if ($scope.partyEmailMapping != undefined &&
                                $scope.partyEmailMapping.originCountry != undefined &&
                                $scope.partyEmailMapping.originCountry != null &&
                                $scope.partyEmailMapping.originCountry.id != null) {
                                for (var i = 0; i < $scope.countryToList.length; i++) {
                                    if ($scope.countryToList[i].id === $scope.partyEmailMapping.originCountry.id)
                                        $scope.countryToList
                                        .splice(i,
                                            1);
                                }
                            }
                            return $scope.countryToList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Countries');
                    });

        }
        $scope.selectedPartyEmailDestinationCountry = function(
            nextFieldId, partyEmailMapping, index) {
            if ($scope.validatePartyEmailMapping(4,
                    partyEmailMapping, index)) {
                $rootScope.navigateToNextField(nextFieldId + index);
            }
        };

        $scope.editPartyEmailMApping = function(index) {


            $scope.emailFlag = "Update";
            $scope.listEmail = false;
            $scope.partyEmailMappingIndex = index;

            $scope
                .mailArrangeEvent($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex]);

            $rootScope.navigateToNextField('partyEmailMapping' +
                $scope.partyEmailMappingIndex);
        }

        $scope.changeImco = function() {
            if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].imco == undefined) {
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].imco = "No";
            } else if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].imco == true) {
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].imco = "Yes";
            }

        }

        $scope.validatePartyEmailMapping = function(validateCode,
            partyEmailMapping, index) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                if (partyEmailMapping.email == undefined ||
                    partyEmailMapping.email == "" ||
                    partyEmailMapping.email == null) {
                    $scope.errorMap.put("partyEmail" + index,
                        $rootScope.nls["ERR60025"]);
                    return false;
                }

                if (partyEmailMapping.email != undefined &&
                    partyEmailMapping.email != null &&
                    partyEmailMapping.email != "") {
                    if (ValidateUtil.isValidMail(partyEmailMapping.email)) {
                        $scope.errorMap.put("partyEmail" +
                            index,
                            $rootScope.nls["ERR60024"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 2) {
                if (partyEmailMapping.locationMaster == undefined ||
                    partyEmailMapping.locationMaster == null ||
                    partyEmailMapping.locationMaster == "" ||
                    partyEmailMapping.locationMaster.id == null ||
                    partyEmailMapping.locationMaster.id == undefined ||
                    partyEmailMapping.locationMaster.id == "") {
                    console.log("Location is Mandatory..........")
                    $scope.errorMap.put("partyEmailLocation" +
                        index, $rootScope.nls["ERR44"]);
                    return false;
                } else {

                    if (partyEmailMapping.locationMaster.status == 'Block') {
                        console
                            .log("Selected Location is Blocked.");
                        $scope.errorMap.put("partyEmailLocation" +
                            index, $rootScope.nls["ERR6028"]);
                        return false;
                    }

                    if (partyEmailMapping.locationMaster.status == 'Hide') {
                        console.log("Selected Location is Hidden.");
                        $scope.errorMap.put("partyEmailLocation" +
                            index, $rootScope.nls["ERR6029"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 3) {




                if (partyEmailMapping.originCountry == undefined ||
                    partyEmailMapping.originCountry == null ||
                    partyEmailMapping.originCountry == "" ||
                    partyEmailMapping.originCountry.id == null ||
                    partyEmailMapping.originCountry.id == undefined ||
                    partyEmailMapping.originCountry.id == "") {
                    if (partyEmailMapping.destinationCountry != undefined &&
                        partyEmailMapping.destinationCountry != null &&
                        partyEmailMapping.destinationCountry != "" &&
                        partyEmailMapping.destinationCountry.id != null &&
                        partyEmailMapping.destinationCountry.id != undefined &&
                        partyEmailMapping.destinationCountry.id != "") {
                        $scope.errorMap.put(
                            "originCountry" + index,
                            $rootScope.nls["ERR6053"]);
                        return false;
                    }
                } else {

                    if (partyEmailMapping.originCountry.status == 'Block') {
                        console
                            .log("Selected Origin Country is Blocked");
                        $scope.errorMap.put(
                            "originCountry" + index,
                            $rootScope.nls["ERR6054"]);
                        return false;
                    }

                    if (partyEmailMapping.originCountry.status == 'Hide') {
                        console
                            .log("Selected Origin Country is Hidden");
                        $scope.errorMap.put(
                            "originCountry" + index,
                            $rootScope.nls["ERR6055"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 4) {

                if (partyEmailMapping.destinationCountry == undefined ||
                    partyEmailMapping.destinationCountry == null ||
                    partyEmailMapping.destinationCountry == "" ||
                    partyEmailMapping.destinationCountry.id == null ||
                    partyEmailMapping.destinationCountry.id == undefined ||
                    partyEmailMapping.destinationCountry.id == "") {
                    if (partyEmailMapping.originCountry != undefined &&
                        partyEmailMapping.originCountry != null &&
                        partyEmailMapping.originCountry != "" &&
                        partyEmailMapping.originCountry.id != null &&
                        partyEmailMapping.originCountry.id != undefined &&
                        partyEmailMapping.originCountry.id != "") {
                        $scope.errorMap.put("destinationCountry" +
                            index, $rootScope.nls["ERR6056"]);
                        return false;
                    }
                } else {
                    if (partyEmailMapping.destinationCountry.status == 'Block') {
                        console
                            .log("Selected Destination Country is Blocked");
                        $scope.errorMap.put("destinationCountry" +
                            index, $rootScope.nls["ERR6057"]);
                        return false;
                    }

                    if (partyEmailMapping.destinationCountry.status == 'Hide') {
                        console
                            .log("Selected Destination Country is Hidden.");
                        $scope.errorMap.put("destinationCountry" +
                            index, $rootScope.nls["ERR6058"]);
                        return false;
                    }

                }

            }
            return true;
        }
        $scope.discardPartyEmail = function() {
            if (!discardService.compare($scope.partyEmailMapping)) {

                ngDialog
                    .openConfirm({
                        template: '<p>Do you want to update your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.addEmailRow();
                        } else {
                            $scope.closePartyEmail();
                        }
                    });

            } else {
                $scope.closePartyEmail();
            }
        }
        $scope.closePartyEmail = function() {
            $scope.tempEmailList = [];
            $scope.listEmail = true;
            $scope.partyEmailMapping = {};

        }
        $scope.deletePartyEmailMapping = function(index) {

            if (!isEmailMappingEmptyRow($scope.partyMaster.partyEmailMappingList[index])) {
                ngDialog
                    .openConfirm({
                        template: '<p>Are you sure you want to delete selected email  ?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button>' + '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            $scope.partyMaster.partyEmailMappingList
                                .splice(index, 1);
                            if ($scope.partyMaster.partyEmailMappingList == undefined ||
                                $scope.partyMaster.partyEmailMappingList == null ||
                                $scope.partyMaster.partyEmailMappingList.length == 0) {
                                $scope.partyEmailMappingIndex = -1;
                            } else {
                                $scope.partyEmailMappingIndex = $scope.partyMaster.partyEmailMappingList.length - 1;
                            }

                        },
                        function(value) {
                            console
                                .log("Account Deletion cancelled");
                        });
            } else {
                $scope.partyMaster.partyEmailMappingList
                    .splice(index, 1);
                if ($scope.partyMaster.partyEmailMappingList == undefined ||
                    $scope.partyMaster.partyEmailMappingList == null ||
                    $scope.partyMaster.partyEmailMappingList.length == 0) {
                    $scope.partyEmailMappingIndex = -1;
                } else {
                    $scope.partyEmailMappingIndex = $scope.partyMaster.partyEmailMappingList.length - 1;
                }
            }



        }

        $scope.showAutoMailList = false;
        $scope.autoMailList = [];
        $scope.searchText = "";

        $scope.listConfigMail = {
            search: true,
            multiple: true,
            ajax: true,
            modelName: "messageName",
            modelName1: "messageSendType",
            showCode: false,
        }

        $scope.showAutoMails = function() {
            $scope.errorMap = new Map();
            console.log("Import Auto eMail");
            $scope.panelTitle = "Import Auto eMail";
            $scope.selectedItem = -1;
            $scope.ajaxAutoMailEvent(null);
        };

        $scope.ajaxAutoMailEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            ExternalAutoMailList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.autoMailList = data.responseObject.searchResult;
                            $scope.showAutoMailList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Auto Mails');
                    });
        }
        $scope.selectedAutoMail = function(autoMailObj) {
            $scope.duplicateEmails = '';
            console.log("Selected Auto Mail", autoMailObj);
            $scope.errorMap = new Map();
            for (var i = 0; i < autoMailObj.length; i++) {
                $scope.uniqueForAutoMail(autoMailObj[i]);

            }

            if ($scope.duplicateEmails != '') {
                $scope.errorMap = new Map();
                $scope.errorMap.put("duplicate.automail",
                    $scope.duplicateEmails +
                    $rootScope.nls["ERR6077"]);
                $scope.duplicateEmails = '';
            }

            // $scope.validatePartyEmailMapping(6);
            $scope.cancelAutoMail();
        };

        $scope.disableMailFromGroup = function(key) {
            $scope.arrList = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                .get(key);
            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].groupStatus
                .put(
                    key,
                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].groupStatus
                    .get(key) ? false : true);
            for (var i = 0; i < $scope.arrList.length; i++) {
                for (var j = 0; j < $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length; j++) {
                    if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster != null &&
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster.messageGroupName == key &&
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.id == $scope.arrList[i].autoMailMaster.id) {
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailStatus = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].groupStatus
                            .get(key);
                    }
                }
            }

        }

        $scope.deleteMailFromGroup = function(key) {

            $scope.arrList = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                .get(key);

            for (var i = 0; i < $scope.arrList.length; i++) {
                for (var j = 0; j < $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length; j++) {
                    if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster != null &&
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster.messageGroupName == key &&
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.id == $scope.arrList[i].autoMailMaster.id) {
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList
                            .splice(j, 1);
                        break;
                    }
                }
            }

            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                .remove(key);
            $scope
                .mailArrangeEvent($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex]);
        }

        $scope.deleteMailFromSelected = function(key, id) {

            $scope.arrList = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                .get(key);

            var flag = false;
            for (var i = 0; i < $scope.arrList.length; i++) {
                for (var j = 0; j < $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length; j++) {
                    if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster == key &&
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.id == id) {
                        $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList
                            .splice(j, 1);
                        flag = true
                        break;
                    }
                }
                if (flag) {
                    $scope.arrList.splice(i, 1);
                    break;
                }
            }

            if ($scope.arrList.length == 0) {
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                    .remove(key);
            } else {
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                    .put(key, $scope.arrList);
            }

            $scope
                .mailArrangeEvent($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex]);
        }

        $scope.mailArrangeEventToggle = function(key) {

            // $scope.obj=$scope.partyEmailMapping.mails.get(key);
            if ($scope.showChild == null) {
                $scope.showChild = key;
            } else {
                $scope.showChild = null;
            }

        }

        /*
         * $scope.mailArrangeEventToggle = function (key){
         * $scope.showChild=key; }
         */

        $scope.mailArrangeEvent = function(partyEmailMapping) {
            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mailEvents = true;
            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails = new Map();
            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].groupStatus = new Map();

            if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList != null &&
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length != 0) {

                for (var i = 0; i < $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length; i++) {
                    if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i].autoMailGroupMaster == null) {
                        if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                            .get(null) == null) {
                            $scope.tmpList = [];
                            $scope.tmpList
                                .push($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i]);
                            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                                .put(null, $scope.tmpList);
                        } else {
                            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                                .get(null)
                                .push(
                                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i]);
                        }
                    } else {
                        if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                            .get($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName) == null) {
                            $scope.tmpList = [];
                            $scope.tmpList
                                .push($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i]);
                            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                                .put(
                                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName,
                                    $scope.tmpList);
                            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].groupStatus
                                .put(
                                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName,
                                    false);
                        } else {
                            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].mails
                                .get(
                                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName)
                                .push(
                                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[i]);
                        }
                    }
                }

            }

        }

        $scope.uniqueForAutoMail = function(autoMailList) {

            if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList == null ||
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length == 0) {
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList = [];
            }
            var flag = true;
            for (var j = 0; j < $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length; j++) {
                if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster == null) {
                    if (autoMailList.id == $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.id) {
                        if ($scope.duplicateEmails != '')
                            $scope.duplicateEmails = $scope.duplicateEmails +
                            ',' +
                            $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.messageName;
                        else {
                            $scope.duplicateEmails = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.messageName;
                        }
                        flag = false;
                        break;
                    }
                }

            }
            if (flag) {
                $scope.tmpObj = {};
                $scope.tmpObj.autoMailMaster = autoMailList;
                $scope.tmpObj.autoMailGroupMaster = null;
                $scope.tmpObj.autoMailStatus = autoMailList.autoMailStatus == 'Enable' ? true :
                    false;
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList
                    .push($scope.tmpObj);
            }

            $scope
                .mailArrangeEvent($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex]);
        }

        $scope.uniqueForMailGroup = function(autoMailList) {

            if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList == null ||
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length == 0) {
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList = [];
            }
            var flag = true;
            for (var j = 0; j < $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList.length; j++) {
                if ($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailGroupMaster != null &&
                    $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster != null) {
                    if (autoMailList.id == $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.id) {
                        if ($scope.duplicateEmails != '') {
                            var present = $scope.duplicateEmails
                                .indexOf($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.autoMailGroupMaster.messageGroupName) >= 0;
                            if (!present)
                                $scope.duplicateEmails = $scope.duplicateEmails +
                                ',' +
                                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.autoMailGroupMaster.messageGroupName;
                        } else {

                            $scope.duplicateEmails = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList[j].autoMailMaster.autoMailGroupMaster.messageGroupName;
                        }
                        flag = false;
                        break;
                    }
                }

            }
            if (flag) {
                $scope.tmpObj = {};
                $scope.tmpObj.autoMailMaster = autoMailList;
                $scope.tmpObj.autoMailGroupMaster = autoMailList.autoMailGroupMaster;
                $scope.tmpObj.autoMailStatus = autoMailList.autoMailStatus == 'Enable' ? true :
                    false;
                $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList
                    .push($scope.tmpObj);
            }
            $scope.itemGroup = $scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex].partyEmailMessageMappingList;

            $scope
                .mailArrangeEvent($scope.partyMaster.partyEmailMappingList[$scope.partyEmailMappingIndex]);
        }

        $scope.cancelAutoMail = function() {
            console.log("Cancel Auto Mail List Button Pressed....");
            $scope.searchText = "";
            $scope.showAutoMailList = false;
        };

        $scope.showMailGroupList = false;
        $scope.mailGroupList = [];
        $scope.searchText = "";

        $scope.listConfigMailGroup = {
            search: true,
            multiple: true,
            ajax: true,
            modelName: "messageGroupName",
            showCode: false,
        }

        $scope.showMailGroups = function() {
            $scope.errorMap = new Map();
            console.log("Import eMail Group");
            $scope.panelTitle = "Import eMail Group";
            $scope.selectedItem = -1;

            $scope.ajaxMailGroupEvent(null);
        };

        $scope.ajaxMailGroupEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            ExternalAutoMailGroupList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.mailGroupList = data.responseObject.searchResult;
                            $scope.showMailGroupList = true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Mail Groups');
                    });
        }
        $scope.selectedMailGroup = function(mailGroupObj) {

            console.log("Selected Mail Group", mailGroupObj);
            $scope.duplicateEmails = '';
            $scope.emptyMailGroups = '';

            for (var i = 0; i < mailGroupObj.length; i++) {
                if (mailGroupObj[i].autoMailMasterList != null) {
                    if (mailGroupObj[i].autoMailMasterList.length == 0) {
                        if ($scope.emptyMailGroups != '') {
                            var temp = $scope.emptyMailGroups
                                .indexOf(mailGroupObj[i].messageGroupName) >= 0;
                            if (!temp)
                                $scope.emptyMailGroups = $scope.emptyMailGroups +
                                ',' +
                                mailGroupObj[i].messageGroupName;
                        } else {

                            $scope.emptyMailGroups = mailGroupObj[i].messageGroupName;
                        }
                        $scope.errorMap = new Map();
                        $scope.errorMap
                            .put(
                                "emptyMailGroups",
                                $scope.emptyMailGroups +
                                $rootScope.nls["ERR60026"]);

                    }

                    for (var j = 0; j < mailGroupObj[i].autoMailMasterList.length; j++) {
                        $scope.autoMailGroupMaster = {};
                        $scope.autoMailMaster = {};
                        $scope.autoMailGroupMaster.id = mailGroupObj[i].id;
                        $scope.autoMailGroupMaster.messageGroupName = mailGroupObj[i].messageGroupName;
                        $scope.autoMailGroupMaster.messageGroupCode = mailGroupObj[i].messageGroupCode;
                        $scope.autoMailMaster = mailGroupObj[i].autoMailMasterList[j];
                        $scope.autoMailMaster.autoMailGroupMaster = $scope.autoMailGroupMaster;
                        $scope
                            .uniqueForMailGroup($scope.autoMailMaster);

                    }

                    if ($scope.duplicateEmails != '') {

                        $scope.errorMap
                            .put(
                                "duplicate.automail",
                                $scope.duplicateEmails +
                                $rootScope.nls["ERR6077"]);
                        $scope.duplicateEmails = '';
                    }

                } else {
                    console.log("Empty Mail Group List");
                }
            }

            // $scope.validatePartyEmailMapping(6);
            $scope.cancelMailGroup();
        };
        $scope.cancelMailGroup = function() {
            console
                .log("Cancel Mail Group List Button Pressed....");
            $scope.searchText = "";
            $scope.showMailGroupList = false;
        };

        $scope.clearPartyAdressCityState = function() {
            if ($scope.partyMaster != undefined &&
                $scope.partyMaster != null &&
                $scope.partyMaster.id != undefined &&
                $scope.partyMaster.id != null) {

                if ($scope.partyMaster.partyDetail != undefined &&
                    $scope.partyMaster.partyDetail != null)
                    $scope.partyMaster.partyDetail.svtNo = null;

                if ($scope.partyMaster.partyAddressList != undefined &&
                    $scope.partyMaster.partyAddressList.length > 0) {

                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {

                        if ($scope.partyMaster.partyAddressList[i] != undefined &&
                            $scope.partyMaster.partyAddressList[i] != null) {

                            $scope.partyMaster.partyAddressList[i].stateMaster = null;
                            $scope.partyMaster.partyAddressList[i].cityMaster = null;
                        }

                    }

                }
                if ($scope.partyMaster.partyAddressMaster != undefined &&
                    $scope.partyMaster.partyAddressList.length == 0) {

                    $scope.partyAddressMaster.cityMaster = null;
                    $scope.partyAddressMaster.stateMaster = null;

                }
                if ($scope.editIndex != null) {

                    $scope.partyAddressMaster.cityMaster = null;
                    $scope.partyAddressMaster.stateMaster = null;

                }

            } else {

                if ($scope.partyMaster.partyAddressList != undefined &&
                    $scope.partyMaster.partyAddressList.length > 0) {

                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {

                        if ($scope.partyMaster.partyAddressList[i] != undefined &&
                            $scope.partyMaster.partyAddressList[i] != null) {

                            $scope.partyMaster.partyAddressList[i].stateMaster = null;
                            $scope.partyMaster.partyAddressList[i].cityMaster = null;
                        }

                    }

                }
                if ($scope.partyAddressMaster != undefined &&
                    $scope.partyMaster.partyAddressList.length == 0) {

                    $scope.partyAddressMaster.cityMaster = null;
                    $scope.partyAddressMaster.stateMaster = null;

                }
                if ($scope.editIndex != null) {
                    $scope.partyAddressMaster.cityMaster = null;
                    $scope.partyAddressMaster.stateMaster = null;

                }

            }

        }

        $scope.clearAddressFieldValues = function(addressType) {

            if ($scope.partyAddressMaster == undefined ||
                $scope.partyAddressMaster == null) {
                return;
            }

            if (addressType == 'Primary') {
                if ($scope.partyAddressMaster.partyCountryField != undefined &&
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName != undefined &&
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName != null)
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName = null;

            }
            if (addressType == 'Billing') {

                if ($scope.partyAddressMaster.partyCountryField != undefined &&
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName != undefined &&
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName != null)
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName = null;

            }
            if (addressType == 'Warehouse') {

            }
            if (addressType == 'Other') {

                if ($scope.partyAddressMaster.partyCountryField != undefined &&
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName != undefined &&
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName != null)
                    $scope.partyAddressMaster.partyCountryField.warehouseProviderName = null;

            }

        }

        $scope.checkDUplicate = function(partyAccountList) {
            $scope.errorMap = new Map();
            if (partyAccountList.length > 1) {
                for (var i = 0; i < partyAccountList.length; i++) {
                    for (var j = 0; j < partyAccountList.length; j++) {
                        if (i == j)
                            continue
                        if (partyAccountList[i].locationMaster.id == partyAccountList[j].locationMaster.id &&
                            partyAccountList[i].accountMaster.id == partyAccountList[j].accountMaster.id &&
                            partyAccountList[i].currencyMaster.id == partyAccountList[j].currencyMaster.id) {
                            $rootScope.clientMessage = $rootScope.nls["ERR6060"];
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        $scope.countryRender = function(item) {
            return {
                label: item.countryName,
                item: item
            }
        }
        $scope.categoryRender = function(item) {
            return {
                label: item.categoryName,
                item: item
            }
        }
        $scope.cityRender = function(item) {
            return {
                label: item.cityName,
                item: item
            }
        }
        $scope.stateRender = function(item) {
            return {
                label: item.stateName,
                item: item
            }
        }
        $scope.countryIssuanceRender = function(item) {
            return {
                label: item.countryName,
                item: item
            }
        }
        $scope.paymentRender = function(item) {
            return {
                label: item.frequencyName,
                item: item
            }
        }
        $scope.locationRender = function(item) {
            return {
                label: item.locationName,
                item: item
            }
        }
        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }
        $scope.checkBusinessDUplicate = function(businessList) {
            for (var i = 0; i < businessList.length; i++) {
                for (var j = 0; j < businessList.length; j++) {
                    if (i == j)
                        continue
                    if (businessList[i].serviceMaster.id == businessList[j].serviceMaster.id) {
                        $rootScope.clientMessage = $rootScope.nls["ERR6083"];
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.checkCreditDUplicate = function(creditList) {
            for (var i = 0; i < creditList.length; i++) {
                for (var j = 0; j < creditList.length; j++) {
                    if (i == j)
                        continue
                    if (creditList[i].locationMaster.id == creditList[j].locationMaster.id &&
                        creditList[i].serviceMaster.id == creditList[j].serviceMaster.id) {
                        $rootScope.clientMessage = $rootScope.nls["ERR6084"];
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.getParty = function(partyId) {
            console.log("Get Party For Edit :: " + partyId);

            $rootScope.serviceCodeForUnHistory = "";
            $rootScope.orginAndDestinationUnHistory = "";

            $scope.salutationShow = false;
            $scope.availableAddressType = [];
            PartyMasterView.get({
                    id: partyId
                },
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.partyMaster = data.responseObject;
                        $scope.init();
                        $scope.chkAndCallCountryField();


                        for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {


                            if ($scope.partyMaster.partyAddressList[i].partyCountryField.exportExpirationDate != undefined &&
                                $scope.partyMaster.partyAddressList[i].partyCountryField.exportExpirationDate != null) {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.exportExpirationDate = $rootScope
                                    .dateToString($scope.partyMaster.partyAddressList[i].partyCountryField.exportExpirationDate);
                            } else {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.exportExpirationDate = null;
                            }

                            if ($scope.partyMaster.partyAddressList[i].partyCountryField.dateOfBirth != undefined &&
                                $scope.partyMaster.partyAddressList[i].partyCountryField.dateOfBirth != null) {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.dateOfBirth = $rootScope
                                    .dateToString($scope.partyMaster.partyAddressList[i].partyCountryField.dateOfBirth);
                            } else {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.dateOfBirth = null;
                            }
                            if ($scope.partyMaster.partyAddressList[i].partyCountryField.importExpirationDate != undefined &&
                                $scope.partyMaster.partyAddressList[i].partyCountryField.importExpirationDate != null) {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.importExpirationDate = $rootScope
                                    .dateToString($scope.partyMaster.partyAddressList[i].partyCountryField.importExpirationDate);
                            } else {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.importExpirationDate = null;
                            }
                            if ($scope.partyMaster.partyAddressList[i].partyCountryField.knownShipperValidationDate != undefined &&
                                $scope.partyMaster.partyAddressList[i].partyCountryField.knownShipperValidationDate != null) {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.knownShipperValidationDate = $rootScope
                                    .dateToString($scope.partyMaster.partyAddressList[i].partyCountryField.knownShipperValidationDate);
                            } else {
                                $scope.partyMaster.partyAddressList[i].partyCountryField.knownShipperValidationDate = null;
                            }

                        }

                        if ($scope.partyMaster.partyBusinessDetailList != null) {
                            for (var i = 0; i < $scope.partyMaster.partyBusinessDetailList.length; i++) {
                                if ($scope.partyMaster.partyBusinessDetailList[i].closureDate != undefined &&
                                    $scope.partyMaster.partyBusinessDetailList[i].closureDate != "" &&
                                    $scope.partyMaster.partyBusinessDetailList[i].closureDate != null) {
                                    $scope.partyMaster.partyBusinessDetailList[i].closureDate = $rootScope
                                        .dateToString($scope.partyMaster.partyBusinessDetailList[i].closureDate);
                                }
                            }
                        }


                        if ($scope.partyMaster.contactList != null &&
                            $scope.partyMaster.contactList.length != 0) {
                            for (var i = 0; i < $scope.partyMaster.contactList.length; i++) {
                                if ($scope.partyMaster.contactList[i].relationList != null &&
                                    $scope.partyMaster.contactList[i].relationList != undefined && $scope.partyMaster.contactList[i].relationList != "") {
                                    for (var j = 0; j < $scope.partyMaster.contactList[i].relationList.length; j++) {
                                        $scope.partyMaster.contactList[i].relationList[j].personalEventDate = $rootScope
                                            .dateToString($scope.partyMaster.contactList[i].relationList[j].personalEventDate);
                                    }
                                }
                            }
                        } else {
                            $scope.partyMaster.contactList = null;
                        }


                        if ($scope.partyMaster != undefined &&
                            $scope.partyMaster != null &&
                            $scope.partyMaster.partyName != undefined &&
                            $scope.partyMaster.partyName != null) {
                            $rootScope.subTitle = $scope.partyMaster.partyName;
                        } else {
                            $rootScope.subTitle = "Unknown Shipper"
                        }
                        var rHistoryObj = {
                            'title': 'Party Edit # ' +
                                $stateParams.quotationId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON
                                .stringify($stateParams),
                            'stateCategory': 'Party',
                            'serviceType': 'AIR'
                        }
                        RecentHistorySaveService
                            .form(rHistoryObj);
                    } else {
                        console.log("Party view Failed " + data.responseDescription);
                    }
                },
                function(error) {
                    console.log("Party view Failed : " + error);

                });
        }



        $scope.$on('addMasterPartyEvent', function(events, args) {

            $scope.partyMaster.savedTempPartyTypeList = []; //used for storing party type list in history/reload
            $scope.partyMaster.savedTempPartyTypeList = $scope.tempPartyTypeList;
            $rootScope.unfinishedData = $scope.partyMaster;
            $rootScope.category = $rootScope.appMasterData['Party_Label'];
            $rootScope.unfinishedFormTitle = $rootScope.category + " (New)";
            if ($scope.partyMaster != undefined &&
                $scope.partyMaster != null &&
                $scope.partyMaster.partyName != undefined &&
                $scope.partyMaster.partyName != null) {
                $rootScope.subTitle = $scope.partyMaster.partyName;
            } else {
                $rootScope.subTitle = "Unknown " + $rootScope.category;
            }
        })

        $scope.$on('editMasterPartyEvent', function(events, args) {
            console.log("Scope partyMaster data :: ", $scope.partyMaster);
            $scope.partyMaster.savedTempPartyTypeList = []; //used for storing party type list in history/reload
            $scope.partyMaster.savedTempPartyTypeList = $scope.tempPartyTypeList;
            $rootScope.unfinishedData = $scope.partyMaster;
            $rootScope.category = $rootScope.appMasterData['Party_Label'];
            $rootScope.unfinishedFormTitle = $rootScope.category + " Edit # " + $scope.partyMaster.id;
            if ($scope.partyMaster != undefined &&
                $scope.partyMaster != null &&
                $scope.partyMaster.partyName != undefined &&
                $scope.partyMaster.partyName != null) {
                $rootScope.subTitle = $scope.partyMaster.partyName;
            } else {
                $rootScope.subTitle = "Unknown " + $rootScope.category;
            }
        })
        $scope.$on('addMasterPartyEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";

            $scope.partyMaster.savedTempPartyTypeList = [];
            $scope.partyMaster.savedTempPartyTypeList = $scope.tempPartyTypeList;

            localStorage.reloadFormData = JSON.stringify($scope.partyMaster);
            localStorage.isPartyReloaded = "YES";
            console.log("$scope.partyMaster Before Reload Scope Obj :: ", $scope.partyMaster);
            console.log("$scope.partyMaster Before Reload Session Object:: ", localStorage.reloadFormData);
            e.preventDefault();
        });
        $scope.$on('editMasterPartyEventReload', function(e, confirmation) {
            $scope.partyMaster.savedTempPartyTypeList = [];
            $scope.partyMaster.savedTempPartyTypeList = $scope.tempPartyTypeList;
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.partyMaster);
            localStorage.isPartyReloaded = "YES";
            e.preventDefault();
        });


        $scope.getDynamicFormField = function(countryID) {

            appMetaFactory.country.dynamicFields({
                countryId: countryID
            }, function(data) {
                //console.log("REsponse -- ",JSON.stringify(data));
                $scope.localDynamicFields = data.responseObject;
                $scope.localDynamicFormFieldMap = new Map();
                if ($scope.localDynamicFields != undefined && $scope.localDynamicFields != null && $scope.localDynamicFields.length > 0) {
                    for (var it = 0; it < $scope.localDynamicFields.length; it++) {
                        $scope.localDynamicFormFieldMap.put($scope.localDynamicFields[it].fieldName, $scope.localDynamicFields[it].showField);
                    }
                }
            }, function(error) {
                console.log("Error");
            });
        }

        $scope.isReloaded = localStorage.isPartyReloaded;
        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isPartyReloaded = "NO";
                    $scope.partyMaster = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.partyMaster = $rootScope.selectedUnfilledFormData;
                }
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isPartyReloaded = "NO";
                    $scope.partyMaster = JSON.parse(localStorage.reloadFormData);
                    $scope.oldData = "";
                } else {
                    // $scope.partyMaster = {};
                }
            }
            $scope.init();
            $scope.chkAndCallCountryField();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.party"
                },
                {
                    label: $rootScope.appMasterData['Party_Label'],
                    state: "layout.party"
                },
                {
                    label: "Add " + $rootScope.appMasterData['Party_Label'],
                    state: null
                }
            ];

        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isPartyReloaded = "NO";
                    try {
                        $scope.partyMaster = JSON.parse(localStorage.reloadFormData);
                    } catch (e) {
                        console.log(e);
                    }
                } else {
                    $scope.partyMaster = $rootScope.selectedUnfilledFormData;
                }
                $scope.chkAndCallCountryField();
                $scope.init();

            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isPartyReloaded = "NO";
                    try {
                        $scope.partyMaster = JSON.parse(localStorage.reloadFormData);
                    } catch (e) {
                        console.log(e);
                    }
                    $scope.init();
                    $scope.chkAndCallCountryField();
                } else {
                    $scope.getParty($stateParams.partyId);
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.party"
                },
                {
                    label: $rootScope.appMasterData['Party_Label'],
                    state: "layout.party"
                },
                {
                    label: "Edit " + $rootScope.appMasterData['Party_Label'],
                    state: null
                }
            ];
        }

        $scope.$on('addMasterPartyEventMenuNavi', function(events,
            moveToScreen) {
            // console.log("Scope enquiryLog data :: ",
            // $scope.enquiryLog);
            // console.log("Scope State data :: ", $state);
            // console.log("Scope State
            // $rootScope.naviGotoStateName:: ",
            // $rootScope.naviGotoStateName);
            $scope.move(moveToScreen);

        })
        $scope.$on('editMasterPartyEventMenuNavi', function(events,
            moveToScreen) {
            // console.log("Scope enquiryLog data :: ",
            // $scope.enquiryLog);
            // console.log("Scope State data :: ", $state);
            // console.log("Scope State
            // $rootScope.naviGotoStateName:: ",
            // $rootScope.naviGotoStateName);
            $scope.move(moveToScreen);

        })

        $scope.move = function(moveToScreen) {
            if (!discardService.compare($scope.partyMaster)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.moveToScreen = moveToScreen;
                            if ($scope.partyMaster.id != null) {
                                $scope.update();
                            } else {
                                $scope.save();
                            }
                        } else if (value == 2) {
                            $state.go(moveToScreen);
                        } else if (value == 3) {
                            // $state.go(args);
                            console.log("cancelled");
                        } else {
                            console.log("nothing selected")
                        }
                    }, function(error) {
                        console.log('error occurred');
                    });
            } else {
                console.log("No Dirty" + moveToScreen);
                $state.go(moveToScreen);
            }
        }

        $scope.accessHasFocus = function(value, nextIdFocus) {
            if (nextIdFocus != undefined && nextIdFocus != null)
                $rootScope.navigateToNextField(nextIdFocus);
            if ($scope.partyMaster.id != null) {

                if (value == 'address' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ADDRESS_MODIFY)) {
                    $scope.partyTab = value;
                    // $scope.selectedAddressIndex = 0;
                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                        if ($scope.partyMaster.partyAddressList[i].addressType == 'Primary') {
                            $scope.selectedAddressIndex = i;
                            break;
                        }
                    }
                    $rootScope.navigateToNextField(nextIdFocus + $scope.selectedAddressIndex);
                }

                if (value == 'general' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_MORE_INFO_MODIFY)) {
                    $scope.partyTab = value;
                }


                if (value == 'contactTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_CONTACT_MODIFY)) {
                    $scope.partyTab = value;
                    $scope.contactInfoTab = 'official';
                    $scope.selectedContact = 0;
                    $rootScope.navigateToNextField(nextIdFocus + $scope.selectedContact);
                }


                if (value == 'mapping' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_SERVICE_DIVISION_MODIFY)) {
                    $scope.partyTab = value;
                }

                if (value == 'accountsTab') {
                    $scope.partyTab = value;
                }

                if (value == 'creditTab') {
                    $scope.partyTab = value;
                }

                if (value == 'mailTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_EMAIL_MODIFY)) {
                    $scope.partyTab = value;
                    if ($scope.partyMaster.partyEmailMappingList != undefined && $scope.partyMaster.partyEmailMappingList != null && $scope.partyMaster.partyEmailMappingList.length > 0) {
                        $scope.editPartyEmailMApping(0);
                    }

                }

                if (value == 'businessTab') {
                    $scope.partyTab = 'businessTab';
                }

                if (value == 'editmoreInfo') {
                    $scope.partyTab = 'address'
                    $scope.editmoreAddressTab = value;
                }
                if (value == 'editifsBtn') {
                    $scope.partyTab = 'address'
                    $scope.editmoreAddressTab = value;
                    $rootScope.navigateToNextField(nextIdFocus);
                }
                if (value == 'editaesBtn') {
                    $scope.partyTab = 'address'
                    $scope.editmoreAddressTab = value;
                }

            } else {

                if (value == 'address' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ADDRESS_CREATE)) {
                    $scope.partyTab = value;
                    // $scope.selectedAddressIndex = 0;
                    for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                        if ($scope.partyMaster.partyAddressList[i].addressType == 'Primary') {
                            $scope.selectedAddressIndex = i;
                            break;
                        }
                    }
                    $rootScope.navigateToNextField(nextIdFocus + $scope.selectedAddressIndex);
                }

                if (value == 'general' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_MORE_INFO_CREATE)) {
                    $scope.partyTab = value;
                }


                if (value == 'contactTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_CONTACT_CREATE)) {
                    $scope.partyTab = value;
                    $scope.contactInfoTab = 'official';
                    $scope.selectedContact = 0;
                    $rootScope.navigateToNextField(nextIdFocus + $scope.selectedContact);
                }


                if (value == 'mapping' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_SERVICE_DIVISION_CREATE)) {
                    $scope.partyTab = value;
                }

                if (value == 'accountsTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ACCOUNTS_CREATE)) {
                    $scope.partyTab = value;
                }

                if (value == 'creditTab') {
                    $scope.partyTab = value;
                }

                if (value == 'mailTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_EMAIL_CREATE)) {
                    $scope.partyTab = value;
                    if ($scope.partyMaster.partyEmailMappingList != undefined && $scope.partyMaster.partyEmailMappingList != null && $scope.partyMaster.partyEmailMappingList.length > 0) {
                        $scope.editPartyEmailMApping(0);
                    }

                }

                if (value == 'businessTab') {
                    $scope.partyTab = 'businessTab';
                }

                if (value == 'editmoreInfo') {
                    $scope.partyTab = 'address'
                    $scope.editmoreAddressTab = value;
                }
                if (value == 'editifsBtn') {
                    $scope.partyTab = 'address'
                    $scope.editmoreAddressTab = value;
                    $rootScope.navigateToNextField(nextIdFocus);
                }
                if (value == 'editaesBtn') {
                    $scope.partyTab = 'address'
                    $scope.editmoreAddressTab = value;
                }


            }



        }

        $scope.contactArr = [{
            icon: 'A',
            name: 'Alex Jeff',
            rated: true
        }, {
            icon: 'B',
            name: 'Bisla',
            rated: false
        }, {
            icon: 'C',
            name: 'Chandru',
            rated: true
        }]

        $scope.navigateForward = function(tabName, fieldName) {
            console.log(tabName, fieldName);
            $scope.partyTab = tabName;
            $scope.navigateToNextField(fieldName);
        }


        $scope.employeeRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }
        }

        $scope.addPartyContact = function() {
            if ($scope.partyMaster.contactList == undefined || $scope.partyMaster.contactList == null || $scope.partyMaster.contactList.length == 0) {
                $scope.partyMaster.contactList = [];
            }

            if ($scope.partyMaster.contactList.length > 0) {
                for (var i = 0; i < $scope.partyMaster.contactList.length; i++) {
                    if ($scope.validatePartyContact(0, $scope.partyMaster.contactList[i], i) == false) {
                        $scope.selectedContact = i;
                        return false;
                    }
                }
            }

            var obj = {};
            obj.salutationType = 'Mr';
            obj.isCall = false;
            obj.isSendMail = false;
            obj.isPrimary = false;
            obj.engagedUsPrevious = false;
            obj.isMarried = false;
            obj.isFavourite = false;
            // obj.salesman = {};

            $scope.partyMaster.contactList.push(obj);
            $scope.selectedContact = $scope.partyMaster.contactList.length - 1;
            $rootScope.navigateToNextField('firstNameContact' + $scope.selectedContact);
        }

        $scope.addRelation = function(contact, nextFocus) {

            if (contact.relationList == null ||
                contact.relationList.length == 0) {
                contact.relationList = [];
            }

            var relation = {};
            relation.salutationType = 'Mr';
            contact.relationList.push(relation);
            $rootScope.navigateToNextField(nextFocus + (contact.relationList.length - 1));
        }

        $scope.deletePartyContact = function(index) {
            $scope.partyMaster.contactList.splice(index, 1);
            if ($scope.partyMaster.contactList == null ||
                $scope.partyMaster.contactList.length == 0) {
                $scope.selectedContact = -1;
            } else {
                $scope.selectedContact = $scope.partyMaster.contactList.length - 1;
            }
        }

        $scope.deleteRelation = function(parentIndex, index) {
            console.log("parentIndex, index", parentIndex, index);
            $scope.partyMaster.contactList[parentIndex].relationList
                .splice(index, 1);
        }

        $scope.chooseContact = function(index) {
            console.log("chooseContact ", index);
            $scope.selectedContact = index;
        }

        $scope.partyContactPrimaryChange = function(partyMaster, index, changeValue) {

            if (partyMaster.contactList == undefined || partyMaster.contactList == null || partyMaster.contactList.lenght == 0) {
                partyMaster.contactList = [];
                return;
            }

            for (var i = 0; i < partyMaster.contactList.length; i++) {
                if (i != index && changeValue) {
                    partyMaster.contactList[i].isPrimary = false;
                }
            }
        }

        $scope.setContactTab = function(tabName, fieldId) {
            console.log("setContactTab ", tabName);

            if (tabName == 'official') {
                $scope.contactInfoTab = 'official';
                $rootScope.navigateToNextField(fieldId);
            } else if (tabName == 'personal') {
                $scope.contactInfoTab = 'personal';
                $rootScope.navigateToNextField(fieldId);
            } else {
                $scope.contactInfoTab = 'remarks';
                $rootScope.navigateToNextField(fieldId);
            }

        }


        $scope.getContactData = function() {
            var obj = {};
            obj.salutationType = 'Mr';
            obj.isCall = false;
            obj.isSendMail = false;
            obj.isPrimary = false;
            obj.engagedUsPrevious = false;
            obj.isMarried = false;
            obj.isFavourite = false;
            return obj;
        }


        $scope.getEmailData = function() {

            var partyEmailMapping = {};
            partyEmailMapping.status = "ACTIVE";
            partyEmailMapping.locationMaster = {};
            partyEmailMapping.originCountry = {};
            partyEmailMapping.destinationCountry = {};
            partyEmailMapping.partyEmailMessageMappingList = [];
            partyEmailMapping.mailEvents = true;
            partyEmailMapping.mails = new Map();
            partyEmailMapping.groupStatus = new Map();
            return partyEmailMapping;
        }

        function isContactEmptyRow(obj) {

            var isempty = true;

            console.log("empty obj*******", obj);

            $scope.emptyObjRef = $scope.getContactData();
            console.log("$scope.emptyObjRef*****",
                $scope.emptyObjRef);
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {

                //check the new key is empty or not
                if (obj[k[i]]) {
                    // check the default object contains the key
                    if (!$scope.emptyObjRef.hasOwnProperty(k[i])) {
                        isempty = false;
                        break;
                    }
                    //check the type is object or not
                    if (angular.isObject(obj[k[i]])) {
                        if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]].id != null && obj[k[i]].id != $scope.emptyObjRef[k[i]].id) {
                            isempty = false;
                            break;
                        }
                    } else {
                        if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                            isempty = false;
                            break;

                        }
                    }
                }
            }
            console.log("empty check *****", isempty)
            return isempty;
        }


        function isEmailMappingEmptyRow(obj) {


            var isempty = true;

            console.log("empty obj*******", obj);

            $scope.emptyObjRef = $scope.getEmailData();
            console.log("$scope.emptyObjRef*****",
                $scope.emptyObjRef);
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {

                //check the new key is empty or not
                if (obj[k[i]]) {
                    // check the default object contains the key
                    if (!$scope.emptyObjRef.hasOwnProperty(k[i])) {
                        isempty = false;
                        break;
                    }
                    //check the type is object or not
                    if (angular.isObject(obj[k[i]])) {
                        if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]]) {
                            isempty = false;
                            break;
                        }

                        if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]].id != null && obj[k[i]].id != $scope.emptyObjRef[k[i]].id) {
                            isempty = false;
                            break;
                        }
                    } else {
                        if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                            isempty = false;
                            break;

                        }
                    }
                }
            }
            console.log("empty check *****", isempty)
            return isempty;
        }




        $scope.validatePartyContact = function(validateCode,
            partyContact, index) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if (partyContact.officalEmail != undefined &&
                    partyContact.officalEmail != null && partyContact.officalEmail != "") {
                    if (!CommonValidationService.checkMultipleMail(partyContact.officalEmail)) {
                        console.log("partyContact.officalEmail Invalid");
                        $scope.errorMap.put("officalEmail" + index, $rootScope.nls["ERR6085"]);
                        return false;;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 2) {
                if (partyContact.personalEmail != undefined &&
                    partyContact.personalEmail != null && partyContact.personalEmail != "") {

                    if (!CommonValidationService.checkMultipleMail(partyContact.personalEmail)) {
                        console.log("partyContact.personalEmail Invalid");
                        $scope.errorMap.put("personalEmail" + index, $rootScope.nls["ERR6086"]);
                        return false;
                    }
                }
            }



            if (validateCode == 0 || validateCode == 3) {
                if (partyContact.firstName == undefined ||
                    partyContact.firstName == null ||
                    partyContact.firstName == "") {
                    $scope.errorMap.put(
                        "contact.firstName" + index,
                        $rootScope.nls["ERR5101"])
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 4) {
                if (partyContact.lastName == undefined ||
                    partyContact.lastName == null ||
                    partyContact.lastName == "") {
                    $scope.errorMap.put("contact.lastName" + index, $rootScope.nls["ERR5102"])
                    return false;
                }
            }


            if (validateCode == 0 || validateCode == 5) {
                if (partyContact.officalPhone != undefined && partyContact.officalPhone != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, partyContact.officalPhone)) {
                        console.log("partyContact.officalPhone Invalid");
                        $scope.errorMap.put("contact.officalPhone" + index, $rootScope.nls["ERR5108"]);
                        return false;
                    }
                }
            }



            if (validateCode == 0 || validateCode == 6) {
                if (partyContact.officalMobile != undefined && partyContact.officalMobile != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER, partyContact.officalMobile)) {
                        console.log("partyContact.officalMobile Invalid");
                        $scope.errorMap.put("contact.officalMobile" + index, $rootScope.nls["ERR5109"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 7) {
                if (partyContact.assistantPhone != undefined && partyContact.assistantPhone != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, partyContact.assistantPhone)) {
                        console.log("partyContact.assistantPhone Invalid");
                        $scope.errorMap.put("contact.assistantPhone" + index, $rootScope.nls["ERR5113"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 8) {
                if (partyContact.personalPhone != undefined && partyContact.personalPhone != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, partyContact.personalPhone)) {
                        console.log("partyContact.personalPhone Invalid");
                        $scope.errorMap.put("contact.personalPhone" + index, $rootScope.nls["ERR5119"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 9) {
                if (partyContact.personalMobile != undefined && partyContact.personalMobile != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER, partyContact.personalMobile)) {
                        console.log("partyContact.personalMobile Invalid");
                        $scope.errorMap.put("contact.personalMobile" + index, $rootScope.nls["ERR5120"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 10) {
                if (partyContact.noOfChildren != undefined && partyContact.noOfChildren != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CONTACT_PERSONAl_CHILDREN, partyContact.noOfChildren)) {
                        console.log("partyContact.noOfChildren Invalid");
                        $scope.errorMap.put("contact.noOfChildren" + index, $rootScope.nls["ERR5125"]);
                        return false;
                    }
                }
            }

            return true;

        }

        $scope.checkObjectHavingId = function(obj) {
            if (obj == null || obj == undefined || obj == "") {
                obj = null;
            } else {
                if (angular.isObject(obj)) {
                    if (obj.id == null || obj.id == undefined || obj.id == "") {
                        obj = null;
                    }
                }
            }
            return obj;
        }

        //
        $scope.directiveDatePickerOpts = angular
            .copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";

        $scope.validateEmail = function(partyEmailMapping, index) {
            $scope.errorMap = new Map();
            if (partyEmailMapping.email != undefined && partyEmailMapping.email != null && partyEmailMapping.email != "") {
                if (!CommonValidationService.checkMultipleMail(partyEmailMapping.email)) {
                    $scope.errorMap.put("partyEmail" + index, $rootScope.nls["ERR60024"]);
                    return false;
                }
            }
            return true;
        }



    }
]);