/**
 * PartyMaster Factory which initiates all Rest calls using resource service
 */
(function() {

    app.factory("PartyMasterSearch",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);


    app.factory("SearchPartyNotInList",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/search/exclude", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);



    app.factory("PartiesListByType",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/search/keyword/partytype/:partyType", {}, {
            fetch: {
                method: 'POST',
                params: {
                    partyType: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("PartyMasterAdd",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("PartyMasterEdit",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/update", {}, {
            update: {
                method: 'POST'
            }
        });
    }]);

    app.factory("PartyMasterView",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("PartyMasterGetByCode",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/code/:code", {}, {
            get: {
                method: 'GET',
                params: {
                    code: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("PartyMasterRemove",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);



    app.factory("AccountList",['$resource', function($resource) {
        return $resource("/api/v1/accountmaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("PartiesList",['$resource', function($resource) {

        return $resource("/api/v1/partymaster/get/search/keyword", {}, {
            query: {
                method: 'POST',
            }
        });
    }]);

    app.factory("AgentPartyList",['$resource', function($resource) {

        return $resource("/api/v1/partymaster/get/search/agent", {}, {
            query: {
                method: 'POST',
            }
        });
    }]);




    app.factory("PartyAccountListById",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/search/keyword/partyaccount/:partyId", {}, {
            fetch: {
                method: 'POST',
                params: {
                    partyId: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("PartyAccountListByNotInId",['$resource', function($resource) {
        return $resource("/api/v1/partymaster/get/search/keyword/partyaccount/notin/:partyId", {}, {
            fetch: {
                method: 'POST',
                params: {
                    partyId: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("LedgerList",['$resource', function($resource) {
        return $resource("/api/v1/generalledgermaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

    app.factory("AgentByServicePort",['$resource', function($resource) {
        return $resource("/api/v1/agentportmaster/get/search/:serviceId/:portId", {}, {
            fetch: {
                method: 'GET',
                params: {
                    serviceId: '',
                    portId: ''
                },
            }
        });
    }]);

    app.factory("AgentByPort",['$resource', function($resource) {
        return $resource("/api/v1/agentportmaster/get/agent/:portCode", {}, {
            fetch: {
                method: 'GET',
                params: {
                    portCode: ''
                },
            }
        });
    }]);


})();