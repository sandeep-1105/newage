app.service('PartyDataConversionService', function($rootScope) {

	this.convertEntityToData = function(partyMaster) {
		if(partyMaster.id != null) {
			partyMaster = this.clearData(partyMaster);
			partyMaster.partyCode = "";
			partyMaster.partyDetail = this.clearData(partyMaster.partyDetail);
			partyMaster.partyDetail.valueAddedTaxNo = null;
			partyMaster.partyDetail.panNo = null;
			partyMaster.partyDetail.racNo = null;
			partyMaster.partyDetail.cstNo = null;
			partyMaster.partyDetail.svtNo = null;
			partyMaster.partyDetail.svatNo = null;
			partyMaster.partyDetail.tinNo = null;
			partyMaster.partyDetail.iataCode = null;

//			partyMaster.partyTypeList = this.clearArrayData(partyMaster.partyTypeList);
			if(partyMaster.partyAddressList != undefined && partyMaster.partyAddressList != null && partyMaster.partyAddressList.length > 0){
				partyMaster.partyAddressList = this.clearArrayData(partyMaster.partyAddressList);
				for(var cIndex = 0; cIndex < partyMaster.partyAddressList.length; cIndex++){
					partyMaster.partyAddressList[cIndex].partyCountryField = this.clearData(partyMaster.partyAddressList[cIndex].partyCountryField);
				}
			} else {
				partyMaster.partyAddressList = [{ partyCountryField:{} }];
			}
			
			if(partyMaster.partyServiceList != undefined && partyMaster.partyServiceList != null && partyMaster.partyServiceList.length > 0){
				partyMaster.partyServiceList = this.clearArrayData(partyMaster.partyServiceList);
			} else {
				partyMaster.partyServiceList = [{}];
			}
			
			if(partyMaster.partyAccountList != undefined && partyMaster.partyAccountList != null && partyMaster.partyAccountList.length > 0){
				partyMaster.partyAccountList = this.clearArrayData(partyMaster.partyAccountList);
			} else {
				partyMaster.partyAccountList = [{status : 'Active', isTaxExempted: false}];
			}
			
			if(partyMaster.partyCreditLimitList != undefined && partyMaster.partyCreditLimitList != null && partyMaster.partyCreditLimitList.length > 0){
				partyMaster.partyCreditLimitList = this.clearArrayData(partyMaster.partyCreditLimitList);

			} else {
				partyMaster.partyCreditLimitList = [{}];
			}
			
			if(partyMaster.partyEmailMappingList != undefined && partyMaster.partyEmailMappingList != null && partyMaster.partyEmailMappingList.length > 0){
				partyMaster.partyEmailMappingList = this.clearArrayData(partyMaster.partyEmailMappingList);

			} else {
				partyMaster.partyEmailMappingList = [{}];
			}
			
			if(partyMaster.partyBusinessDetailList != undefined && partyMaster.partyBusinessDetailList != null && partyMaster.partyBusinessDetailList.length > 0){
				partyMaster.partyBusinessDetailList = this.clearArrayData(partyMaster.partyBusinessDetailList);

			} else {
				partyMaster.partyBusinessDetailList = [{ }];
			}
			
			if(partyMaster.partyCompanyAssociateList != undefined && partyMaster.partyCompanyAssociateList != null && partyMaster.partyCompanyAssociateList.length > 0){
				partyMaster.partyCompanyAssociateList = this.clearArrayData(partyMaster.partyCompanyAssociateList);

			} else {
				partyMaster.partyCompanyAssociateList = [{ }];
			}
			
			if(partyMaster.contactList != undefined && partyMaster.contactList != null && partyMaster.contactList.length > 0){
				partyMaster.contactList = this.clearArrayData(partyMaster.contactList);
				for(var cIndex = 0; cIndex < partyMaster.contactList.length; cIndex++){
					partyMaster.contactList[cIndex].relationList = this.clearArrayData(partyMaster.contactList[cIndex].relationList);
				}

			} else {
				partyMaster.contactList = [{ salutationType : 'Mr', isCall : false, isSendMail : false, isPrimary : false, engagedUsPrevious : false , isMarried : false, isFavourite : false }];
			}
			
		}
		return partyMaster;
	}
	
	this.clearArrayData = function(array){
		if(array != undefined && array != null && array.length > 0) {
			for(var i = 0; i < array.length; i++) {
				array[i] =  this.clearData(array[i]);
			}
		}
		return array;
	}
	
	this.clearData = function(idObject){
		if(idObject != undefined && idObject != null) {
			idObject.id = null;
			idObject.versionLock = null;
			idObject.systemTrack = {};
		}
		return idObject;
	}
});

