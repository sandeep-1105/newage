/**
 * PartyMasterCtrl which list all the details from database
 */
app.controller('PartyMasterCtrl',['$rootScope', '$scope', '$http', '$location', '$window', 'NavigationService', '$modal',
    'ngTableParams', 'PartyMasterSearch', 'PartyMasterView', 'ngDialog', 'appMetaFactory',
    'PartyMasterRemove', 'PartyMasterService', '$state', '$stateParams', 
    'RecentHistorySaveService', 'addressJoiner', 'roleConstant', 
	function($rootScope, $scope, $http, $location, $window, NavigationService, $modal,
    ngTableParams, PartyMasterSearch, PartyMasterView, ngDialog, appMetaFactory,
    PartyMasterRemove, PartyMasterService, $state, $stateParams, 
    RecentHistorySaveService, addressJoiner, roleConstant) {

    $scope.partyMaster = {};
    $scope.availableAddressType = [];
    $scope.selectedMaster = {};
    $scope.searchDto = {};
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.selectedAddressIndex = 0;


    $scope.localDynamicFields = [];
    $scope.localDynamicFormFieldMap = new Map();

    $scope.partyAddressSelect = function(index) {
        $scope.selectedAddressIndex = index;
    }
    PartyMasterService.set({});

    $scope.cityStateLine = function(partyMaster, partyAddress) {
        return addressJoiner.cityStateLine(partyMaster, partyAddress)
    }

    $scope.init = function() {
        console.log("PartyMaster Init method called.");
        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2($rootScope.appMasterData['Party_Label']);
        PartyMasterService.set({});
        $scope.listPartyTable = true;
        $scope.searchDto = {};
        $scope.partyMaster = {};
        $scope.searchDto.orderByType = $scope.sortSelection.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = $scope.sortSelection.sortKey;
        $scope.search();
        $scope.salutationShow = false;
    };

    $(".toggle-pin").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    $scope.partyHeadArr = [{
            "name": "#",
            "width": "width1per",
            "model": "no",
            "search": false,
        },
        {
            "name": "Name",
            "width": "width15per",
            "model": "partyName",
            "search": true,
            "wrap_cell": true,
            "type": "color-text",
            "sort": true,
            "key": "searchPartyName"
        },
        {
            "name": "Code",
            "width": "width8per",
            "model": "partyCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchPartyCode"
        },
        {
            "name": "Country",
            "width": "width13per",
            "model": "countryMaster.countryName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchCountryName"
        },
        {
            "name": "Status",
            "width": "width15per",
            "model": "status",
            "search": true,
            "type": "drop",
            "data": $rootScope.enum['LovStatus'],
            "key": "searchStatus",
            "sort": true
        }
    ];


    //$scope.partyArr = [{
    //	no:1,
    //	"partyName":"chandru",
    //	"partyCode":"23423",
    //	"countryMaster": {
    //		"countryName":"India"
    //	},
    //	"status":"ACTIVE"
    //}]

    $scope.sortSelection = {
        sortKey: "partyName",
        sortOrder: "asc"
    }

    $scope.changepage = function(param) {
        console.log("change Page..." + param.page);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change..." + item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_VIEW)) {
            console.log("$scope.searchDto --- ", $scope.searchDto);

            $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var param = {
                partyId: data.id,
                selectedPageNumber: $scope.selectedRecordIndex,
                totalRecord: $scope.totalRecord
            };
            console.log("View State Parameter : ", param);
            $state.go("layout.viewParty", $scope.searchDtoToStateParams(param));
        }
    }

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;


            if ($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
                stateParameters.searchStatus = $scope.searchDto.searchStatus = $stateParams.searchStatus;
            }
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            //Search By Party Code
            if ($stateParams.searchPartyCode != undefined && $stateParams.searchPartyCode != null) {
                stateParameters.searchPartyCode = $scope.searchDto.searchPartyCode = $stateParams.searchPartyCode;
            }
            if ($stateParams.searchCountryName != undefined && $stateParams.searchCountryName != null) {
                stateParameters.searchCountryName = $scope.searchDto.searchCountryName = $stateParams.searchCountryName;
            }
            if ($stateParams.searchPartyName != undefined && $stateParams.searchPartyName != null) {
                stateParameters.searchPartyName = $scope.searchDto.searchPartyName = $stateParams.searchPartyName;
            }
        } else {
            return;
        }
        PartyMasterSearch.query($scope.searchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            if (data.responseObject != null && data.responseObject.searchResult != undefined && data.responseObject.searchResult != null &&
                data.responseObject.searchResult.length > 0 && data.responseObject.searchResult[0].id != null) {
                stateParameters.partyId = data.responseObject.searchResult[0].id
                $state.go("layout.viewParty", stateParameters);

            }

        });
    }
    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                //Enquiry Status - Active, Quote, All
                if ($scope.searchDto.searchStatus != undefined && $scope.searchDto.searchStatus != null) {
                    param.searchStatus = $scope.searchDto.searchStatus;
                }
                //Search By Party Code
                if ($scope.searchDto.searchPartyCode != undefined && $scope.searchDto.searchPartyCode != null) {
                    param.searchPartyCode = $scope.searchDto.searchPartyCode;
                }
                //Search By Party Name
                if ($scope.searchDto.searchPartyName != undefined && $scope.searchDto.searchPartyName != null) {
                    param.searchPartyName = $scope.searchDto.searchPartyName;
                }
                //Search By Country Name
                if ($scope.searchDto.searchCountryName != undefined && $scope.searchDto.searchCountryName != null) {
                    param.searchCountryName = $scope.searchDto.searchCountryName;
                }
            }

        }
        return param;
    }

    $scope.clickOnPartyTab = function(tab) {

        if (tab == 'address' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ADDRESS_VIEW)) {
            $scope.partyTab = tab;
        }

        if (tab == 'general' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_MORE_INFO_VIEW)) {
            $scope.partyTab = tab;
        }
        if (tab == 'contactTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_CONTACT_VIEW)) {
            $scope.partyTab = tab;
        }

        if (tab == 'mapping' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_SERVICE_DIVISION_VIEW)) {
            $scope.partyTab = tab;
        }

        if (tab == 'accountsTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_ACCOUNTS_VIEW)) {
            $scope.partyTab = tab;
        }

        if (tab == 'creditsTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_CREDIT_VIEW)) {
            $scope.partyTab = tab;
        }

        if (tab == 'emailTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_EMAIL_VIEW)) {
            $scope.partyTab = tab;
        }

        if (tab == 'businessTab' && $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_BUSINESS_DETAILS_VIEW)) {
            $scope.partyTab = tab;
        }


    }

    $scope.cancel = function() {
        console.log("Cancel Detail View...");
        $scope.listPartyTable = true;
        $scope.partyMaster = {};
        $scope.availableAddressType = [];
        $state.go("layout.party");
    };

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_CREATE)) {
            console.log("Add Button is pressed...");
            /*
            PartyMasterService.set({});
            NavigationService.set(null);
            NavigationService.setLocation(null);*/
            //$location.path("/master/party_operation");
            $state.go("layout.addParty");
        }
    };


    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_MODIFY)) {
            console.log("Edit Button is pressed...");
            //				PartyMasterService.set($scope.partyMaster);
            //				NavigationService.set(null);
            //				NavigationService.setLocation(null);
            //				$location.path("/master/party_operation");

            var param = {
                partyId: $scope.partyMaster.id
            };
            console.log("View State Parameter : ", param);
            $state.go("layout.editParty", param);
        }
    };


    $scope.changeSearch = function(param) {
        console.log("Change Search is called." + param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();
        console.log("change search", $scope.searchDto);
    }

    $scope.sortChange = function(param) {
        console.log("sortChange is called. " + param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.deleteMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR246"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button></div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                PartyMasterRemove.remove({
                    id: $scope.partyMaster.id
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("PartyMaster deleted Successfully")

                        $state.go("layout.party", {
                            submitAction: "Deleted"
                        });


                    } else {
                        console.log("PartyMaster deleted Failed " + data.responseDescription)
                    }
                }, function(error) {
                    console.log("PartyMaster deleted Failed : " + error)
                });
            }, function(value) {
                console.log("deleted cancelled");

            });
        }
    }


    $scope.serialNo = function(index) {
        index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
            (index + 1);
        return index;
    }


    $scope.search = function() {
        $scope.PartyMaster = {};
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.partyArr = [];

        PartyMasterSearch.query($scope.searchDto).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            console.log("tempArr", tempArr);

            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                if (tempObj.gradeMaster != null) {
                    tempObj.changeColor = true;
                    tempObj.color = tempObj.gradeMaster.colorCode;
                }
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.partyArr = resultArr;

        });

    }




    /*Party Address Tab*/
    $scope.moreAddressTab = "moreInfo";
    $scope.selectedAddressType = function(type) {

        $scope.selectedAddress = {};
        if ($scope.partyMaster.partyAddressList != null) {

            for (var i = 0; i < $scope.partyMaster.partyAddressList.length; i++) {
                if ($scope.partyMaster.partyAddressList[i].addressType == type) {
                    $scope.selectedAddress = $scope.partyMaster.partyAddressList[i];
                    $scope.moreAddressTab = "moreInfo";
                    break;
                }
            }
        }
    }



    $scope.currentTab = "general";
    $scope.activeTab = function(tab) {
        $scope.currentTab = tab;
    }
    $scope.addressTab = "moreinfo";

    $scope.buttonTab = function(tab) {
        $scope.addressTab = tab;
    }

    $scope.data2 = {
        tos: "Freight On Board1",
        Frequency: "weekly1",
        Commodity: "Battery and Raw Goods1",
        Reason: "Rates1",
        Notes: "Rates are very reasonable for this shipping. Good Profit1"
    }

    $scope.mailArrangeEventToggle = function(key) {


        if ($scope.showChild == null) {
            $scope.showChild = key;
        } else {
            $scope.showChild = null;
        }

    }
    $scope.mailArrange = function(index) {

        $scope.partyEmailMapping = $scope.partyMaster.partyEmailMappingList[index];
        $scope.partyEmailMapping.mails = new Map();
        $scope.partyEmailMapping.groupStatus = new Map();

        if ($scope.partyEmailMapping.partyEmailMessageMappingList != null && $scope.partyEmailMapping.partyEmailMessageMappingList.length != 0) {

            for (var i = 0; i < $scope.partyEmailMapping.partyEmailMessageMappingList.length; i++) {
                if ($scope.partyEmailMapping.partyEmailMessageMappingList[i].autoMailGroupMaster == null) {
                    if ($scope.partyEmailMapping.mails.get(null) == null) {
                        $scope.tmpList = [];
                        $scope.tmpList.push($scope.partyEmailMapping.partyEmailMessageMappingList[i]);
                        $scope.partyEmailMapping.mails.put(null, $scope.tmpList);
                    } else {
                        $scope.partyEmailMapping.mails.get(null).push($scope.partyEmailMapping.partyEmailMessageMappingList[i]);
                    }
                } else {
                    if ($scope.partyEmailMapping.mails.get($scope.partyEmailMapping.partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName) == null) {
                        $scope.tmpList = [];
                        $scope.tmpList.push($scope.partyEmailMapping.partyEmailMessageMappingList[i]);
                        $scope.partyEmailMapping.mails.put($scope.partyEmailMapping.partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName, $scope.tmpList);
                        $scope.partyEmailMapping.groupStatus.put($scope.partyEmailMapping.partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName, false);
                    } else {
                        $scope.partyEmailMapping.mails.get($scope.partyEmailMapping.partyEmailMessageMappingList[i].autoMailGroupMaster.messageGroupName).push($scope.partyEmailMapping.partyEmailMessageMappingList[i]);
                    }
                }
            }


        }



    }




    $scope.getDynamicFormField = function(countryID) {

        appMetaFactory.country.dynamicFields({
            countryId: countryID
        }, function(data) {
            //console.log("REsponse -- ",JSON.stringify(data));
            $scope.localDynamicFields = data.responseObject;
            $scope.localDynamicFormFieldMap = new Map();
            if ($scope.localDynamicFields != undefined && $scope.localDynamicFields != null && $scope.localDynamicFields.length > 0) {
                for (var it = 0; it < $scope.localDynamicFields.length; it++) {
                    $scope.localDynamicFormFieldMap.put($scope.localDynamicFields[it].fieldName, $scope.localDynamicFields[it].showField);
                }
            }
        }, function(error) {
            console.log("Error");
        });
    }


    $scope.view = function(partyId, partyIndex) {
        console.log("View Party ID = " + partyId + " Party Index : " + partyIndex);


        $scope.salutationShow = false;
        $scope.availableAddressType = [];
        PartyMasterView.get({
                id: partyId
            },
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.partyMaster = data.responseObject;

                    if ($scope.partyMaster.countryMaster != undefined && $scope.partyMaster.countryMaster != null &&
                        $scope.partyMaster.countryMaster.id != undefined && $scope.partyMaster.countryMaster.id != null) {
                        $scope.getDynamicFormField($scope.partyMaster.countryMaster.id);
                    }

                    for (var i = 0; i < $scope.partyMaster.partyTypeList.length; i++) {
                        if ($scope.partyMaster.partyTypeList[i].partyTypeMaster.partyTypeCode == "IN") {
                            $scope.salutationShow = true;
                        }
                    }

                    $scope.availableAddressType = [];

                    var flagPrimary = false;

                    for (var j = 0; j < $scope.partyMaster.partyAddressList.length; j++) {
                        $scope.availableAddressType.push($scope.partyMaster.partyAddressList[j].addressType);
                        if ($scope.partyMaster.partyAddressList[j].addressType == 'Primary') {
                            flagPrimary = true;
                        }
                    }

                    if (flagPrimary) {
                        $scope.selectedAddressType('Primary');
                        $scope.addressTypeTab = "Primary";
                    } else {
                        if ($scope.availableAddressType.length != 0) {
                            $scope.selectedAddressType($scope.availableAddressType[0]);
                            $scope.addressTypeTab = $scope.availableAddressType[0];
                        } else {
                            $scope.selectedAddressType(null);
                        }
                    }

                    $scope.moreAddressTab = null;
                    $scope.listPartyTable = false;
                    console.log("state", $scope.addressTypeTab);


                    if ($scope.partyMaster != undefined && $scope.partyMaster != null &&
                        $scope.partyMaster.partyName != undefined && $scope.partyMaster.partyName != null) {
                        $rootScope.subTitle = $scope.partyMaster.partyName;
                    } else {
                        $rootScope.subTitle = "Unknown Shipper"
                    }

                    $rootScope.category = $rootScope.appMasterData['Party_Label'];

                    $rootScope.unfinishedFormTitle = $rootScope.category + " View # " + $scope.partyMaster.partyCode;

                    $rootScope.serviceCodeForUnHistory = "";
                    $rootScope.orginAndDestinationUnHistory = "";
                    var rHistoryObj = {
                        'title': 'Party View # ' + $scope.partyMaster.partyCode,
                        'subTitle': $rootScope.subTitle,
                        'stateName': $state.current.name,
                        'stateParam': JSON.stringify($stateParams),
                        'stateCategory': 'Party',
                        'serviceType': 'AIR'
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    console.log("Party view Failed " + data.responseDescription)
                }
            },
            function(error) {
                console.log("Party view Failed : " + error)
            });
    }

    if ($stateParams.action === "VIEW") {
        console.log("Party View Page........!");
        $rootScope.unfinishedData = undefined;
        $scope.view($stateParams.partyId, $stateParams.partyIndex);
        $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
        $scope.totalRecord = parseInt($stateParams.totalRecord);
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.party"
            },
            {
                label: $rootScope.appMasterData['Party_Label'],
                state: "layout.party"
            },
            {
                label: "View " + $rootScope.appMasterData['Party_Label'],
                state: null
            }
        ];
    } else if ($stateParams.action === "SEARCH") {
        console.log("Party Search Page........!");
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.party"
            },
            {
                label: $rootScope.appMasterData['Party_Label'],
                state: "layout.party"
            }
        ];
        $scope.init();
    } else {
        console.log("Other than View & Search Page........!");
    }

    /***** Business Table View More Info Popup  *****/

    $scope.moreInfo = function(moredit) {
        var newScope = $scope.$new();
        newScope.moredit = moredit;
        var myTotalOtherModal = $modal({
            scope: newScope,
            templateUrl: '/app/components/master/party/views/business_table_view_popup.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);
    };

    $scope.goToContact = function(index) {
        $scope.partyTab = 'contactTab';
        $scope.selectedContact = index;
    }
    $scope.selectedEmail = 0;
    $scope.clickEmail = function(index) {
        $scope.selectedEmail = index;
    }
}]);