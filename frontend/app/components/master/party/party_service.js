/**
 * PartyMasterService-which is for storage a values while navaigating/refresh
 */
app.factory('PartyMasterService', function() {
    var savedData = {}
    var location;

    function set(data) {
        savedData = data;
        localStorage.savedData = JSON.stringify(savedData);

    }

    function get() {
        savedData = localStorage.savedData == null ? null : JSON.parse(localStorage.savedData);
        return savedData;
    }

    return {
        set: set,
        get: get,
        setLocation: setLocation,
        getLocation: getLocation
    }


    function setLocation(loc) {
        this.location = loc;
    }

    function getLocation() {
        return this.location;
    }
});

app.service('partyService',['$rootScope', 'PartiesList', function($rootScope, PartiesList) {

    this.getPartyList = function(object) {
        var searchDto = {};
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    partyList = data.responseObject.searchResult;
                    return partyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }


}]);