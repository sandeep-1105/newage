(function() {

	app.factory("CategorySearch",['$resource', function($resource) {
		return $resource("/api/v1/categorymaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CategoryAdd",['$resource', function($resource) {
		return $resource("/api/v1/categorymaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CategoryEdit",['$resource', function($resource) {
		return $resource("/api/v1/categorymaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CategoryView",['$resource', function($resource) {
		return $resource("/api/v1/categorymaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	

	app.factory("CategoryRemove",['$resource', function($resource) {
		return $resource("/api/v1/categorymaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("CategoryList",['$resource', function($resource) {
		return $resource("/api/v1/categorymaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

})();
