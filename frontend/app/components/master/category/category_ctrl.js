app.controller('categoryCtrl',['$rootScope', '$state', '$scope', '$window', '$modal', '$stateParams', 'ngTableParams', 
    	'CategoryAdd', 'ngDialog', 'CategoryEdit', 'CategorySearch', 'CategoryRemove', 'CategoryView', 'roleConstant',
    function($rootScope, $state, $scope, $window, $modal, $stateParams, ngTableParams, 
    	CategoryAdd, ngDialog, CategoryEdit, CategorySearch, CategoryRemove, CategoryView, roleConstant) {



        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
        $scope.categoryHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-6",
                "model": "categoryName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchCategoryName"
            },
            {
                "name": "Code",
                "width": "col-xs-2half",
                "model": "categoryCode",
                "search": true,
                "type": "text",
                "wrap_cell": true,
                "sort": true,
                "key": "searchCategoryCode"
            },
            {
                "name": "Status",
                "width": "col-xs-3",
                "model": "status",
                "search": true,
                "type": "drop",
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus",
                "sort": true
            }
        ]

        $scope.sortSelection = {
            sortKey: "categoryName",
            sortOrder: "asc"
        }



        $scope.categoryMaster = {};
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.init = function() {
            console.log("category Init method called.................");
            $scope.searchDto = {};
            $scope.search();
        };


        $scope.limitChange = function(item) {
            console.log("Limit Change is called...", item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.rowSelect = function(data) {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_VERTICAL_VIEW)) {
                $scope.categoryMaster = data;
                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.categoryMaster.id != null) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    console.log("window resizing..." + $window.innerWidth);
                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/
            }
        }

        $scope.changeSearch = function(param) {
            console.log("Change Search", param)
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }

        $scope.sortChange = function(param) {
            console.log("Sort Change Called.", param);
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        }

        $scope.changepage = function(param) {
            console.log("Change Page Called.", param);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }



        $scope.search = function() {

            console.log("Search method is called............................!");

            $scope.categoryMaster = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.categoryArr = [];
            CategorySearch.query($scope.searchDto).$promise.then(function(
                data, status) {

                $scope.totalRecord = data.responseObject.totalRecord;
                console.log($scope.totalRecord);
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.categoryArr = resultArr;
            });

        }

        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
                (index + 1);
            return index;
        }




        $scope.deleteCategory = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_VERTICAL_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR236"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            CategoryRemove.remove({
                                    id: $scope.categoryMaster.id
                                },
                                function(data) {
                                    if (data.responseCode == 'ERR0') {
                                        console.log("Category deleted Successfully")
                                        $scope.init();
                                        $scope.deskTopView = true;
                                    } else {
                                        console.log("Category deleted Failed " + data.responseDescription)
                                    }
                                },
                                function(error) {
                                    console.log("Category deleted Failed : " + error)
                                });
                        },
                        function(value) {
                            console.log("deleted cancelled");
                        });
            }
        }


        $scope.editCategory = function(data) {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_VERTICAL_MODIFY)) {
                console.log("Edit Button is Pressed.");
                $state.go("layout.editCategory", {
                    categoryId: $scope.categoryMaster.id
                });
            }
        }

        $scope.categoryOperation = function() {
            console.log("Add Button is Pressed.")
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_VERTICAL_CREATE)) {
                $state.go("layout.addCategory");
            }
        }




        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) + (index + 1);
            return index;
        }


        $scope.cancel = function() {
            $scope.showHistory = false;
            $scope.categoryMaster = {};
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;
            $state.go("layout.category");

            /********************* ux fix ends here *****************************/

        }

        // Routing

        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.category"
                }, {
                    label: "Vertical",
                    state: "layout.category"
                }];
                break;
            case "VIEW":
                break;
        }
    }]);