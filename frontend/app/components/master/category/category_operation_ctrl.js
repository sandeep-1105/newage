app.controller('categoryOperationCtrl',['$rootScope', '$scope', '$state', '$stateParams', 'CategoryView',
    'CategoryAdd', 'ngDialog', 'CategoryEdit', 'CategoryRemove', 'ngProgressFactory',
    'appConstant', 'CommonValidationService', 
	function($rootScope, $scope, $state, $stateParams, CategoryView,
    CategoryAdd, ngDialog, CategoryEdit, CategoryRemove, ngProgressFactory,
    appConstant, CommonValidationService) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('category-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {
        if ($scope.categoryMaster == undefined || $scope.categoryMaster == null) {
            $scope.categoryMaster = {};
            $scope.categoryMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };


    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.categoryMaster);
        console.log("CategoryMaster Data Loaded");
    }

    $scope.cancel = function() {
        if ($scope.oldData != JSON.stringify($scope.categoryMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        if (value == 1 &&
                            $scope.categoryMaster.id == null) {
                            $scope.save();
                        } else if (value == 1 &&
                            $scope.categoryMaster.id != null) {
                            $scope.update();
                        } else if (value == 2) {
                            var params = {};
                            params.submitAction = 'Cancelled';
                            $state.go("layout.category", params);
                        } else {
                            console.log("cancelled");
                        }

                    });

        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.category", params);
        }
    }

    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateCategoryMaster(0)) {
            $scope.contained_progressbar.start();
            CategoryEdit.update($scope.categoryMaster).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Category updated Successfully")
                        var params = {}
                        params.submitAction = 'Saved';
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        $state.go("layout.category", params);
                    } else {
                        console.log("Category updated Failed ", data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Category updated Failed : ", error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validateCategoryMaster(0)) {
            $scope.contained_progressbar.start();
            CategoryAdd.save($scope.categoryMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    var params = {}
                    params.submitAction = 'Saved';
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.category", params);

                } else {
                    console.
                    console.log("Category added Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {

                console.log("Category added Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.validateCategoryMaster = function(validateCode) {
        console.log("Validate Called");
        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.categoryMaster.categoryName == undefined ||
                $scope.categoryMaster.categoryName == null ||
                $scope.categoryMaster.categoryName == "") {
                $scope.errorMap.put("categoryName", $rootScope.nls["ERR1307"]);
                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CATEGORY_NAME, $scope.categoryMaster.categoryName)) {
                    $scope.errorMap.put("categoryName", $rootScope.nls["ERR1305"]);
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.categoryMaster.categoryCode == undefined ||
                $scope.categoryMaster.categoryCode == null ||
                $scope.categoryMaster.categoryCode == "") {
                $scope.errorMap.put("categoryCode", $rootScope.nls["ERR1306"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CATEGORY_CODE, $scope.categoryMaster.categoryCode)) {
                    $scope.errorMap.put("categoryCode", $rootScope.nls["ERR1303"]);
                    return false;
                }
            }
        }

        return true;

    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }

    // History Start

    $scope.getCategoryById = function(categoryId) {
        console.log("getCategoryById ", categoryId);

        CategoryView.get({
            id: categoryId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting category.", data)
                $scope.categoryMaster = data.responseObject;
                $scope.setOldDataVal();
            }
        }, function(error) {
            console.log("Error while getting category.", error)
        });

    }


    //On leave the Unfilled category Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addCategoryEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.categoryMaster;
        $rootScope.category = "Vertical Master";
        $rootScope.unfinishedFormTitle = "Vertical (New)";
        if ($scope.categoryMaster != undefined && $scope.categoryMaster != null && $scope.categoryMaster.categoryName != undefined &&
            $scope.categoryMaster.categoryName != null && $scope.categoryMaster.categoryName != "") {
            $rootScope.subTitle = $scope.categoryMaster.categoryName;
        } else {
            $rootScope.subTitle = "Unknown Vertical"
        }
    });

    $scope.$on('editCategoryEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.categoryMaster;
        $rootScope.category = "Vertical Master";
        $rootScope.unfinishedFormTitle = "Vertical Edit (" + $scope.categoryMaster.categoryCode + ")";
        if ($scope.categoryMaster != undefined && $scope.categoryMaster != null && $scope.categoryMaster.categoryName != undefined &&
            $scope.categoryMaster.categoryName != null && $scope.categoryMaster.categoryName != "") {
            $rootScope.subTitle = $scope.categoryMaster.categoryName;
        } else {
            $rootScope.subTitle = "Unknown Vertical"
        }
    });


    $scope.$on('addCategoryEventReload', function(e, confirmation) {
        console.log("addCategoryEventReload is called ", $scope.categoryMaster);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.categoryMaster);
        localStorage.isCategoryReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editCategoryEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.categoryMaster);
        localStorage.isCategoryReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isCategoryReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCategoryReloaded = "NO";
                $scope.categoryMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.categoryMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCategoryReloaded = "NO";
                $scope.categoryMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
                $scope.oldData = "";
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.category"
            },
            {
                label: "Vertical",
                state: "layout.category"
            },
            {
                label: "Add Vertical",
                state: null
            }
        ];

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCategoryReloaded = "NO";
                $scope.categoryMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.categoryMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }

            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCategoryReloaded = "NO";
                $scope.categoryMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getCategoryById($stateParams.categoryId);
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.category"
            },
            {
                label: "Vertical",
                state: "layout.category"
            },
            {
                label: "Edit Vertical",
                state: null
            }
        ];
    }


    // History End
}]);