
app.controller("buyer_consolidationEditCtrl",['$rootScope','$scope','$location','ngDialog','$modal','$state',
    function($rootScope,$scope,$location,ngDialog,$modal,$state){

    $scope.cancel = function() {
        $state.go("layout.buyerConsolidationMaster");
    };
    $scope.buyMaster = {}; 
    $scope.buyMaster = { 
        "status" : "Active"
     };  
    $scope.setStatus = function(data){ 
        if(data=='Active'){ 
            return 'activetype'; 
        } else if(data=='Block'){ 
            return 'blockedtype'; 
        }else if(data=='Hide'){ 
            return 'hiddentype'; 
        } 
    }

}]);
