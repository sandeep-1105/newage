app.factory('buyerConsolidationFactory', function($resource) {

	return {
		common : $resource('/api/v1/bcmaster/:id', {}, {
			create : {method : 'POST'},
			update : {method : 'PUT'},
			findById : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		search : $resource('/api/v1/bcmaster/search', {}, {
			query : {method : 'POST'},
			keyword : {method : 'PUT'}
		})
	};
})