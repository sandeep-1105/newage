app.controller('buyer_consolidationCtrl',['$rootScope', '$scope', '$location', '$window', 'ngDialog', '$state', '$modal',
    function($rootScope, $scope, $location, $window, ngDialog, $state, $modal) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/


    $scope.buyerConsolidationHeadArr = [{
            "name": "#",
            "width": "col-xs-1",
            "model": "no",
            "search": false
        },
        {
            "name": " Name",
            "width": "inl_l",
            "model": "Name",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "BcName"

        },
        {
            "name": "Code",
            "width": "inl_m",
            "model": "Code",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "BcCode"

        },
        {
            "name": "Status",
            "width": "inl_m",
            "model": "status",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "sort": true,
            "key": "Status"

        }
    ];
    $scope.buyerConsolidationArr = [{
            "no": 1,
            "Name": "Auto Apperels",
            "Code": "Apperals",
            "status": "Active"
        },
        {
            "no": 2,
            "Name": "CAT Universal",
            "Code": "CAT",
            "status": "Active"
        },
        {
            "no": 3,
            "Name": "Visteon Automotive Systems India Pvt Ltd",
            "Code": "Visteon",
            "status": "Active"
        },
        {
            "no": 4,
            "Name": "Xerox Corporation",
            "Code": "Xerox",
            "status": "Active"
        }
    ];
    $scope.sortSelection = {
        sortKey: "userName",
        sortOrder: "asc"
    }
    $scope.page = 0;
    $scope.limit = 10;
    $scope.backToBcm = function() {
        $scope.showDetail = false;
        $scope.deskTopView = true;
    }
    $scope.showDetail = false;
    $scope.edit = function() {
        $state.go("layout.buyerConsolidationMasterEdit");
    };
    $scope.add = function() {
        $state.go("layout.buyerConsolidationMasterEdit");
    };

    $scope.rowSelect = function(data) {
        $scope.showHistory = false;
        $scope.showDetail = true;

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/

        var windowInner = $window.innerWidth;
        if (windowInner <= 1199) {
            $scope.deskTopView = false;

        } else {
            $scope.deskTopView = true;
        }
        angular.element($window).bind('resize', function() {
            if ($window.innerWidth >= 1200) {
                $scope.deskTopView = true;
            } else if ($window.innerWidth <= 1199) {
                if ($scope.buyerMonsolidation.id !== false) {
                    $scope.deskTopView = false;
                } else {
                    $scope.deskTopView = true;
                }
            }

            console.log("window resizing..." + $window.innerWidth);
            $scope.$digest();
        });

        /********************* ux fix ends here *****************************/




    }
    $scope.cancel = function() {
        $scope.showHistory = false;
        $scope.serviceMaster = {};

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/


    };

    $scope.carrierMaster = {}; 
    $scope.carrierMaster = { 
        "status": "Active" 
    };  

}]);