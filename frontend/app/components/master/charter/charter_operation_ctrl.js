app.controller('charterMasterEditController',['$rootScope', '$scope', 'CharterAdd', 'appConstant', 'CommonValidationService', 
		'CharterEdit', 'ngProgressFactory', 'ngDialog', '$stateParams', '$state', 'CharterView',
	function($rootScope, $scope, CharterAdd, appConstant, CommonValidationService, 
		CharterEdit, ngProgressFactory, ngDialog, $stateParams, $state, CharterView) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('charter-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {

        if ($scope.charterMaster == undefined || $scope.charterMaster == null) {
            $scope.charterMaster = {};
            $scope.charterMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.charterMaster);
    }

    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.charterMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.charterMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.charterMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.charter", params);
                } else {
                    console.log("cancelled");
                }
            });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.charter", params);
        }
    }




    $scope.update = function() {
        console.log("Update Method is called.");

        if ($scope.validateCharterMaster(0)) {

            $scope.contained_progressbar.start();

            CharterEdit.update($scope.charterMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Charter updated Successfully")
                    var params = {}
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    params.submitAction = 'Saved';
                    $state.go("layout.charter", params);
                } else {
                    console.log("Charter updated Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Charter updation Failed : ", error);
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }

    $scope.save = function() {
        console.log("Save Method is called.");

        if ($scope.validateCharterMaster(0)) {
            $scope.contained_progressbar.start();

            CharterAdd.save($scope.charterMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Charter Saved Successfully")
                    var params = {}
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    params.submitAction = 'Saved';
                    $state.go("layout.charter", params);
                } else {
                    console.log("Charter Saving Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Charter Saving Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }


    $scope.validateCharterMaster = function(validateCode) {
        console.log("Validate Called ", validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.charterMaster.charterName == undefined || $scope.charterMaster.charterName == null || $scope.charterMaster.charterName == "") {
                $scope.errorMap.put("charterName", $rootScope.nls["ERR16162"]);
                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charter_Name, $scope.charterMaster.charterName)) {
                    $scope.errorMap.put("charterName", $rootScope.nls["ERR16163"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.charterMaster.charterCode == undefined || $scope.charterMaster.charterCode == null || $scope.charterMaster.charterCode == "") {
                $scope.errorMap.put("charterCode", $rootScope.nls["ERR16161"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charter_Name, $scope.charterMaster.charterCode)) {
                    $scope.errorMap.put("charterCode", $rootScope.nls["ERR16164"]);
                    return false;
                }
            }
        }

        return true;
    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }


    // History Start

    $scope.getCharterById = function(charterId) {
        console.log("getCharterById ", charterId);

        CharterView.get({
            id: charterId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting charter.", data)
                $scope.charterMaster = data.responseObject;
            }
            $scope.setOldDataVal();
        }, function(error) {
            console.log("Error while getting charter.", error)
        });

    }


    //On leave the Unfilled charter Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addCharterEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.charterMaster;
        $rootScope.category = "Charter Master";
        $rootScope.unfinishedFormTitle = "Charter (New)";
        if ($scope.charterMaster != undefined && $scope.charterMaster != null && $scope.charterMaster.charterName != undefined &&
            $scope.charterMaster.charterName != null && $scope.charterMaster.charterName != "") {
            $rootScope.subTitle = $scope.charterMaster.charterName;
        } else {
            $rootScope.subTitle = "Unknown Charter"
        }
    })

    $scope.$on('editCharterEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.charterMaster;
        $rootScope.category = "Charter Master";
        $rootScope.unfinishedFormTitle = "Charter Edit (" + $scope.charterMaster.charterCode + ")";
        if ($scope.charterMaster != undefined && $scope.charterMaster != null && $scope.charterMaster.charterName != undefined &&
            $scope.charterMaster.charterName != null && $scope.charterMaster.charterName != "") {
            $rootScope.subTitle = $scope.charterMaster.charterName;
        } else {
            $rootScope.subTitle = "Unknown Charter"
        }
    })


    $scope.$on('addCharterEventReload', function(e, confirmation) {
        console.log("addCharterEventReload is called ", $scope.charterMaster);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.charterMaster);
        localStorage.isCharterReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editCharterEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.charterMaster);
        localStorage.isCharterReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isCharterReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCharterReloaded = "NO";
                $scope.charterMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.charterMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCharterReloaded = "NO";
                $scope.charterMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
                $scope.oldData = "";
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.charter"
            },
            {
                label: "Charter",
                state: "layout.charter"
            },
            {
                label: "Add Charter",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCharterReloaded = "NO";
                $scope.charterMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.charterMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCharterReloaded = "NO";
                $scope.charterMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getCharterById($stateParams.charterId);
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.charter"
            },
            {
                label: "Charter",
                state: "layout.charter"
            },
            {
                label: "Edit Charter",
                state: null
            }
        ];
    }


    // History End




}]);