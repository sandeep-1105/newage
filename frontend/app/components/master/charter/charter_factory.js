(function() {

	app.factory("CharterSearch",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("CharterList",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CharterAdd",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CharterEdit",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CharterView",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("CharterRemove",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("CharterGetByCode",['$resource', function($resource) {
		return $resource("/api/v1/chartermaster/get/code/:code", {}, {
			get : {
				method : 'GET',
				params : {
					code : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	
})();
