app.controller('charterMasterController', ['$rootScope', '$scope', '$state', '$stateParams', '$window',
	'ngDialog', 'CharterSearch', 'CharterRemove',
	function($rootScope, $scope, $state, $stateParams, $window, ngDialog, CharterSearch, CharterRemove) {


    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.charterHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false
        },
        {
            "name": "Name",
            "width": "col-xs-7",
            "model": "charterName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchCharterName"
        },
        {
            "name": "Code",
            "width": "col-xs-2half",
            "model": "charterCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchCharterCode"
        },
        {
            "name": "Status",
            "width": "w120px",
            "model": "status",
            "search": true,
            "sort": true,
            "type": "drop",
            "data": $rootScope.enum['LovStatus'],
            "key": "searchStatus"
        }
    ];

    $scope.sortSelection = {
        sortKey: "charterName",
        sortOrder: "asc"
    }

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.init = function() {
        console.log("charter Init method called.................");
        $scope.searchDto = {};
        $scope.charterMaster = {};
        $scope.search();
    };


    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }




    $scope.rowSelect = function(data) {

        $scope.charterMaster = data;

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/

        var windowInner = $window.innerWidth;
        if (windowInner <= 1199) {
            $scope.deskTopView = false;

        } else {
            $scope.deskTopView = true;
        }
        angular.element($window).bind('resize', function() {
            if ($window.innerWidth >= 1200) {
                $scope.deskTopView = true;
            } else if ($window.innerWidth <= 1199) {
                if ($scope.charterMaster.id != null) {
                    $scope.deskTopView = false;
                } else {
                    $scope.deskTopView = true;
                }
            }

            console.log("window resizing..." + $window.innerWidth);
            $scope.$digest();
        });

        /********************* ux fix ends here *****************************/


        console.log("selected Charter Master ", $scope.charterMaster);
    }

    $scope.changeSearch = function(param) {

        console.log("Change Search", param);

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        $scope.page = 0;
        console.log("change search", $scope.searchDto);

        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);

        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;

        $scope.search();
    }

    $scope.changePage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }



    $scope.search = function() {
        console.log("Search method is called............................!");

        $scope.charterMaster = {};
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.charterArr = [];

        CharterSearch.query($scope.searchDto).$promise.then(function(data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;

            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;

            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });

            $scope.charterArr = resultArr;
        });

    }


    $scope.addNewCharter = function() {
        console.log("Add Button is Pressed.")
        $state.go("layout.addCharter");
    }



    $scope.deleteCharter = function() {
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR161615"];
        ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button></div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            })
            .then(function(value) {
                CharterRemove.remove({
                    id: $scope.charterMaster.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Charter deleted Successfully")
                        $scope.cancel();
                        $scope.init();
                    } else {
                        console.log("Charter deleted Failed ", data.responseDescription);
                    }
                }, function(error) {
                    console.log("Charter deleted Failed : ", error);
                });
            }, function(value) {
                console.log("deleted cancelled ", value);
            });
    }

    $scope.editcharter = function(data) {
        console.log("Edit Button is Pressed.");
        $state.go("layout.editCharter", {
            charterId: $scope.charterMaster.id
        });
    }

    $scope.cancel = function() {

        $scope.charterMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    }



    switch ($stateParams.action) {
        case "SEARCH":
            console.log("Charter Master Search Page....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.charter"
                },
                {
                    label: "Charter",
                    state: "layout.charter"
                }
            ];
            break;
    }



}]);