app.service('countryMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
        return [{
                "name": "#",
                "width": "w50px",
                "model": "no",
                "prefWidth" : "50",
                "search": false
            },
            {
                "name": "Name",
                "width": "w200px",
                "model": "countryName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "prefWidth" : "200",
                "key": "searchCountryName"
            },
            {
                "name": "Code",
                "width": "w75px",
                "model": "countryCode",
                "search": true,
                "type": "text",
                "sort": true,
                "prefWidth" : "75",
                "key": "searchCountryCode"
            },
            /*
            {
                "name": "Region",
                "width": "col-md-1half",
                "model": "regionName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchServiceCode"
            },
            {
                "name": "Region Code",
                "width": "col-md-1half",
                "model": "regionCode",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchServiceCode"
            },
            */
            {
                "name": "Currency Code",
                "width": "w100px ",
                "model": "currencyMaster.currencyCode",
                "search": true,
                "type": "text",
                "sort": true,
                "prefWidth" : "100",
                "key": "searchCurrencyCode"
            },
            {
                "name": "Nationality",
                "width": "w100px",
                "model": "nationality",
                "search": true,
                "type": "text",
                "sort": true,
                "prefWidth" : "100",
                "key": "searchNationality"
            },
            {
                "name": "Inland Haulage Weight/CBM",
                "width": "w100px text-right",
                "model": "weightOrCbm",
                "search": true,
                "type": "number",
                "sort": true,
                "prefWidth" : "100",
                "key": "searchWeightOrCbm"
            },
            {
                "name": "Status",
                "width": "w150px",
                "model": "status",
                "search": true,
                "type": "drop",
                "prefWidth" : "150",
                "data": $rootScope.enum[
                    'LovStatus'
                ],
                "key": "searchStatus",
                "sort": true
            }
        ];
    };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.countryMaster"
            },
            {
                label: "Country",
                state: "layout.countryMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.countryMaster"
            },
            {
                label: "Country",
                state: "layout.countryMaster"
            },
            {
                label: "Add Country",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.countryMaster"
            },
            {
                label: "Country",
                state: "layout.countryMaster"
            },
            {
                label: "Edit Country",
                state: null
            }
        ];
    };
    this.getViewBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.countryMaster"
            },
            {
                label: "Country",
                state: "layout.countryMaster"
            },
            {
                label: "View Country",
                state: null
            }
        ];
    };

});