(function(){
app.controller("CountryMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'countryMasterDataService', 'countryMasterFactory', 'RecentHistorySaveService', 'countryMasterValidationService', 'Notification', '$timeout','CurrencyMasterSearchKeyword', 'roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, countryMasterDataService, countryMasterFactory, RecentHistorySaveService, countryMasterValidationService, Notification, $timeout, CurrencyMasterSearchKeyword, roleConstant) {

        var vm = this;
        vm.tableHeadArr = countryMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.searchData = {};
        vm.countryMaster = {};
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        
        
    	$scope.uploadText = true;
		$scope.isBusy = false;

        vm.sortSelection = {
            sortKey: "countryName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * Country Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Country Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Country Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Country Master from List page.
         * @param {string} data.id - The id of the Selected Country Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data, index) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COUNTRY_VIEW)){
            vm.selectedRecordIndex = (vm.searchData.recordPerPage * vm.searchData.selectedPageNumber) + index;
            var param = {
                id: data.id,
                selectedPageNumber: vm.selectedRecordIndex,
                totalRecord: vm.totalRecord
            }

            $state.go("layout.countryMasterView", vm.searchDtoToStateParams(param));
        	}
        }

        /**
         * Country Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to Country Master List Page.
         */
        vm.backToList = function() {
            $state.go("layout.countryMaster", {
                submitAction: "Cancelled"
            });
        }

        /**
         * Get Next or Previous Country Master Based on id and index.
         * @param {int} nxtIdx - The nxtIdx(-1, or 1) for view page .
         */
        
        vm.singlePageNavigation = function(val) {
	    	if($stateParams != undefined && $stateParams != null) {
	            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
	                return;
	            var stateParameters = {};
	            vm.searchData.recordPerPage = 1;
	            vm.searchData.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
				if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
					stateParameters.sortByColumn = vm.searchData.sortByColumn = $stateParams.sortByColumn;
	        	}
				if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
					stateParameters.orderByType = vm.searchData.orderByType = $stateParams.orderByType;
	        	}
				if($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
					stateParameters.recordPerPage = vm.searchData.recordPerPage = $stateParams.recordPerPage;
	        	}
				if($stateParams.searchCountryName != undefined && $stateParams.searchCountryName != null) {
					stateParameters.searchCountryName = vm.searchData.searchCountryName = $stateParams.searchCountryName;
	        	}
				if($stateParams.searchCountryCode != undefined && $stateParams.searchCountryCode != null) {
					stateParameters.searchCountryCode = vm.searchData.searchCountryCode = $stateParams.searchCountryCode;
	        	}
				if($stateParams.searchNationality != undefined && $stateParams.searchNationality != null) {
					stateParameters.searchNationality = vm.searchData.searchNationality = $stateParams.searchNationality;
	        	}
				if($stateParams.searchWeightOrCbm != undefined && $stateParams.searchWeightOrCbm != null) {
					stateParameters.searchWeightOrCbm = vm.searchData.searchWeightOrCbm = $stateParams.searchWeightOrCbm;
	        	}
				if($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
					stateParameters.searchStatus = vm.searchData.searchStatus = $stateParams.searchStatus;
	        	}
	    	} else {
	    		return;
	    	}
	    	countryMasterFactory.search.query(vm.searchData).$promise.then(function(data, status) {
	            vm.totalRecord = data.responseObject.totalRecord;
				stateParameters.selectedPageNumber = vm.searchData.selectedPageNumber;
	            stateParameters.id =  data.responseObject.searchResult[0].id;
	            stateParameters.totalRecord = vm.totalRecord;
	            $state.go("layout.countryMasterView", stateParameters);
			});
	    }
        
        vm.searchDtoToStateParams = function(param) {
	    	if(param == undefined || param == null) {
	    		param = {};
	    	}
	    	if(vm.searchData != undefined && vm.searchData != null) {
	    		if(vm.searchData != undefined && vm.searchData != null) {
	        		param.recordPerPage = 1;
	    			if(vm.searchData.sortByColumn != undefined && vm.searchData.sortByColumn != null) {
	            		param.sortByColumn = vm.searchData.sortByColumn;
	            	}
	    			if(vm.searchData.orderByType != undefined && vm.searchData.orderByType != null) {
	            		param.orderByType = vm.searchData.orderByType;
	            	}
	    			if(vm.searchData.searchCountryName != undefined && vm.searchData.searchCountryName != null) {
	            		param.searchCountryName = vm.searchData.searchCountryName;
	            	}
	    			if(vm.searchData.searchCountryCode != undefined && vm.searchData.searchCountryCode != null) {
	            		param.searchCountryCode = vm.searchData.searchCountryCode;
	            	}
	    			if(vm.searchData.searchNationality != undefined && vm.searchData.searchNationality != null) {
	            		param.searchNationality = vm.searchData.searchNationality;
	            	}
	    			if(vm.searchData.searchWeightOrCbm != undefined && vm.searchData.searchWeightOrCbm != null) {
	            		param.searchWeightOrCbm = vm.searchData.searchWeightOrCbm;
	            	}
	    			if(vm.searchData.searchStatus != undefined && vm.searchData.searchStatus != null) {
	            		param.searchStatus = vm.searchData.searchStatus;
	            	}
	        	}
	    	}
	    	return param;
	    }

        /**
         * Get Country Master's Based on Search Data.
         */
        vm.search = function() {
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;

            vm.dataArr = [];
            countryMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    $log.error('Error while fetching Countries');
                });
        }
        
        vm.ajaxCurrencyEvent = function(object) {

            console.log("currency ", object);

            vm.searchData = {};
            vm.searchData.keyword = object == null ? "" : object;
            vm.searchData.selectedPageNumber = 0;
            vm.searchData.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencyMasterSearchKeyword.query(vm.searchData).$promise.then(function(data, status) {
              if (data.responseCode == "ERR0") {
                $scope.totalRecord = data.responseObject.totalRecord;
                $scope.currencyList = data.responseObject.searchResult;
                return $scope.currencyList;
              // $scope.showRateCurrencyList=true;
              }
            },
              function(errResponse) {
                console.error('Error while fetching Currency List');
              }
            );
          }
        
        
        
        
        $scope.uploadImage = function(file){
        	
    		if(file != null || file != undefined) {
    			
    			$scope.uploadText=false;
				$scope.isBusy = true;
    			
    			var fileName = file.name;
    			
    			var extension = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
    			
    			console.log("file extension ", extension);
    			
    			var valid = false;
    			if(extension == 'jpeg' || extension == 'jpg' || extension == 'png' || extension == 'gif' || extension == 'svg') {
    				valid = true;
    			} else {
    				valid = false;
    			}
    			
    			
    			if(valid) {
    				var reader = new FileReader();
        			reader.onload = function(event){
        				var contents = event.target.result;
        				var uploadedFile = btoa(contents);
        				console.log("File contents : " , uploadedFile);
        				vm.countryMaster.image = uploadedFile;
        				$timeout(function(){
        					$scope.isBusy = false;
        				},1000);
        			};
        			reader.readAsBinaryString(file);
    			} else {
    				console.log("Not a valid Image");
    			}
    			
    			
    		}
    	}
    	
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        /**
         * Get Country Master By id.
         * @param {int} id - The id of a Country.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Country.
         */
        vm.view = function(id, isView) {
            countryMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.countryMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.countryMaster != undefined && vm.countryMaster != null && vm.countryMaster.id != undefined && vm.countryMaster.id != null) {
                        $rootScope.subTitle = vm.countryMaster.countryName;
                    	if(vm.countryMaster.encodedImage != undefined && vm.countryMaster.encodedImage != null) {
                    		vm.countryMaster.image = vm.countryMaster.encodedImage;
                    		$scope.uploadText=false;
                			$scope.isBusy = false;
        				}
                    } else {
                        $rootScope.subTitle = "Unknown Country"
                    }
                    $rootScope.unfinishedFormTitle = "Country View # " + vm.countryMaster.countryCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "Country View #" + vm.countryMaster.countryCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Country Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Country # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Country Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Country by Id ", data.responseDescription);
                }

            }, function(error) {
                $log.debug("Error ", error);
            });

        };

        /**
         * Go To Add Country Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COUNTRY_CREATE)){
            $state.go("layout.countryMasterAdd");
        	}
        }

        vm.validate = function(valElement, code, nextIdValue) {
        	$scope.errorMap = new Map();
            var validationResponse = countryMasterValidationService.validate(vm.countryMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				$scope.errorMap.put(valElement, null);
            	validationResponse.error = false;
            	if(nextIdValue!=undefined){
            		if(vm.countryMaster.id!=undefined && vm.countryMaster.id!=null){
            			$rootScope.navigateToNextField('status');
            		}else{
            		$rootScope.navigateToNextField(nextIdValue);	
            		}
            	}
            }
        }
        
        
        
        /**
         * Cancel Add or Edit Country Master.
         */
       
        vm.cancel = function(objId) {
            if(vm.countryForm!=undefined){
		    	vm.goTocancel(vm.countryForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.countryMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.countryMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COUNTRY_MODIFY)){

            $state.go("layout.countryMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COUNTRY_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage =$rootScope.nls["ERR06306"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	countryMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	Notification.success($rootScope.nls["ERR402"]);
	                            $state.go("layout.countryMaster", {
	                                submitAction: "Cancelled"
	                            });
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        	}
        }

        /**
         * Create New Country Master.
         */
        vm.create = function() {
            var validationResponse = countryMasterValidationService.validate(vm.countryMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create country..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	if(vm.countryMaster.status == undefined || vm.countryMaster.status == null || vm.countryMaster.status ==""){
            		vm.countryMaster.status ='Active';
                  }
            	countryMasterFactory.create.query(vm.countryMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Country Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go("layout.countryMaster", {
                            submitAction: "Saved"
                        });
		            } else {
		                $log.debug("Create country Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create country Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
        //Update Existing Country Master
        vm.update = function() {
        	 var validationResponse = countryMasterValidationService.validate(vm.countryMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the country..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
             	countryMasterFactory.update.query(vm.countryMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("Country Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	$state.go("layout.countryMaster", {
                            submitAction: "Saved"
                        });
 		            } else {
 		                $log.debug("Updating country Failed :" + data.responseDescription)
 		            }

 		        }, function(error) {
 		        	 $log.debug("Updating country Failed : " + error)
 		        });
             } 
        };
        
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isCountryMasterReloaded;
        $scope.$on('addCountryMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.countryMaster;
			$rootScope.category = "Country Master";
			$rootScope.unfinishedFormTitle = "Country (New)";
        	if(vm.countryMaster != undefined && vm.countryMaster != null && vm.countryMaster.countryName != undefined && vm.countryMaster.countryName != null) {
        		$rootScope.subTitle = vm.countryMaster.countryName;
        	} else {
        		$rootScope.subTitle = "Unknown Country "
        	}
        })
      
	    $scope.$on('editCountryMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.countryMaster;
			$rootScope.category = "Country Master";
			$rootScope.unfinishedFormTitle = "Country Edit ("+vm.countryMaster.countryCode + ")";
			if(vm.countryMaster != undefined && vm.countryMaster != null && vm.countryMaster.countryName != undefined && vm.countryMaster.countryName != null) {
        		$rootScope.subTitle = vm.countryMaster.countryName;
        	} else {
        		$rootScope.subTitle = "Unknown Country "
        	}
	    })
	      
	    $scope.$on('addCountryMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.countryMaster) ;
	    	    localStorage.isCountryMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editCountryMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.countryMaster) ;
	    	    localStorage.isCountryMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      

        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "VIEW":
                vm.view($stateParams.id, true);
                $rootScope.breadcrumbArr = countryMasterDataService.getViewBreadCrumb();
                vm.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
                vm.totalRecord = parseInt($stateParams.totalRecord);
                break;
            case "ADD":
                if ($stateParams.fromHistory === 'Yes') {
                  if (vm.isReloaded == "YES") {
                   vm.isReloaded = "NO";
                   localStorage.isCountryMasterReloaded = "NO";
                    vm.countryMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                    vm.countryMaster = $rootScope.selectedUnfilledFormData;
                  }
                } else {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isCountryMasterReloaded = "NO";
                    vm.countryMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                      $log.debug("Add NotHistory NotReload...")
                  }
                }
                $rootScope.breadcrumbArr = countryMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
                if (vm.fromHistory === 'Yes') {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isCountryMasterReloaded = "NO";
                    vm.countryMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                    vm.countryMaster = $rootScope.selectedUnfilledFormData;
                  }
                } else {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isCountryMasterReloaded = "NO";
                    vm.countryMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                      vm.view($stateParams.id, false);
                  }
                }
                $rootScope.breadcrumbArr = countryMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = countryMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();