app.factory('countryMasterFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/countrymaster/get/id/:id', {}, {
			query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/countrymaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/countrymaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/countrymaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/countrymaster/get/search', {}, {
			query : {method : 'POST'}
		}),
		searchCountryNotInList:$resource('/api/v1/countrymaster/get/search/exclude', {}, {
			query : {method : 'POST'}
		})
	};
}])

app.factory("CountryList",['$resource', function($resource) {
	return $resource("/api/v1/countrymaster/get/search/keyword", {}, {
		fetch : {
			method : 'POST'
		}
	});
}]);


