app.service('countryMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(masterObject, code) {
		//Validate Country Name
		if(code == 0 || code == 1) {
			if (masterObject.countryName == null ||masterObject.countryName == undefined  || masterObject.countryName == "") {
				return this.validationResponse(true, "countryName", $rootScope.nls["ERR06300"], masterObject);
			}
		}
		
		
		//validate currencyCode
		if(code == 0 || code == 4) {
			if(masterObject.currencyMaster == undefined || masterObject.currencyMaster == null || masterObject.currencyMaster =="" ){
				return this.validationResponse(true, "currencyCode", $rootScope.nls["ERR1614"], masterObject);
			}
			else if(masterObject.currencyMaster.currencyCode == undefined || masterObject.currencyMaster.currencyCode == null || masterObject.currencyMaster.currencyCode =="" ){
				return this.validationResponse(true, "currencyCode", $rootScope.nls["ERR1614"], masterObject);
			}else if(ValidateUtil.isStatusBlocked(masterObject.currencyMaster.status)){
	  				return this.validationResponse(true, "currencyCode", $rootScope.nls["ERR1615"], masterObject);
			   }
		}		
		
		
		
		//Validate Country Code
		if(code == 0 || code == 2) {
			if (masterObject.countryCode == null ||masterObject.countryCode == undefined  || masterObject.countryCode == "") {
				return this.validationResponse(true, "countryCode", $rootScope.nls["ERR06301"], masterObject);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Service_Code,masterObject.countryCode)){
      				return this.validationResponse(true, "countryCode", $rootScope.nls["ERR06301"], masterObject);
    		   }
			}
		}
		
		//validate currencyCode
		if(code == 0 || code == 4) {
			if(masterObject.currencyMaster == undefined || masterObject.currencyMaster == null || masterObject.currencyMaster =="" ){
				return this.validationResponse(true, "currencyCode", "currencyCode is mandatory", masterObject);
			}
			else if(masterObject.currencyMaster.currencyCode == undefined || masterObject.currencyMaster.currencyCode == null || masterObject.currencyMaster.currencyCode =="" ){
				return this.validationResponse(true, "currencyCode", "currencyCode is mandatory", masterObject);
			}else if(ValidateUtil.isStatusBlocked(masterObject.currencyMaster.status)){
	  				return this.validationResponse(true, "currencyCode", "currencyCode is blocked", masterObject);
			   }
		}		
		
		
		//Validate Country Weight/CBM
        if(code == 0 || code == 3) {
        	var wt = masterObject.weightOrCbm;
        	if(wt != undefined && wt != null && wt !=""){
        		/*if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_HEIGHT,masterObject.weightOrCbm)){
      				return this.validationResponse(true, "weightOrCbm", $rootScope.nls["ERR06305"], masterObject);
    		   }*/
        		
        		if(isNaN(wt)) {
      				return this.validationResponse(true, "weightOrCbm", $rootScope.nls["ERR06305"], masterObject);
        		}
        		else if(!isNaN(parseFloat(wt)) <=0 || !isNaN(parseFloat(wt)) > 99999999999.99){
      				return this.validationResponse(true, "weightOrCbm", $rootScope.nls["ERR06305"], masterObject);
        		}
        		else{
        			return this.validationResponse(false);
        		}
        	}
        	else{
        		masterObject.weightOrCbm = null;
        	}
		}
		
		return this.validationSuccesResponse(masterObject);
	}
 
	this.validationSuccesResponse = function(masterObject) {
	    return {error : false, obj : masterObject};
	}
	
	this.validationResponse = function(err, elem, message, masterObject) {
		return {error : err, errElement : elem, errMessage : message, obj : masterObject};
	}
	
}]);