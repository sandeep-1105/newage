/**
 *   Enquiry Service 
 */

app.factory('enquiryService', ['NavigationService', '$rootScope', '$location', 'valueAddedServicesFactory', 'roleConstant',
    function(NavigationService, $rootScope, $location, valueAddedServicesFactory, roleConstant) {
        var savedData = {}

        function set(data) {
            savedData = data;
            localStorage.savedData = JSON.stringify(savedData);
        }

        function get() {

            savedData = localStorage.savedData == null ? null : JSON.parse(localStorage.savedData);
            return savedData;
        }

        function setFalse(obj) {
            for (var i = 0; i < obj.length; i++) {
                obj[i].selected = false;
            }
            return obj;
        }

        return {
            set: set,
            get: get,
            setFalse: setFalse,
            goToAddEnquiry: goToAddEnquiry,
            createEnquiry: createEnquiry,
            getNewEnquiryDetail: getNewEnquiryDetail,
            getOriginalEnquiry: getOriginalEnquiry,
            isDimensionFound: isDimensionFound,
            isValueAddedFound: isValueAddedFound,
            isValueAddedFoundInViewPage: isValueAddedFoundInViewPage,
            isRateRequestFound: isRateRequestFound,
            isPickupDeliveryFound: isPickupDeliveryFound
        }




        function goToAddEnquiry() {


            var enquiry = {};
            enquiry.partyMaster = {};
            enquiry.salesCoOrdinator = {};
            enquiry.salesman = {};
            enquiry.loggedOn = $rootScope.dateToString(new Date());
            enquiry.loggedBy = $rootScope.userProfile.employee;
            enquiry.receivedOn = null;
            enquiry.enquiryDetailList = [];
            enquiry.enquiryDetailList.push({});

            enquiry.enquiryDetailList[0].serviceMaster = {};
            enquiry.enquiryDetailList[0].tosMaster = {};
            enquiry.enquiryDetailList[0].fromPortMaster = {};
            enquiry.enquiryDetailList[0].toPortMaster = {};
            enquiry.enquiryDetailList[0].commodityMaster = {};
            enquiry.enquiryDetailList[0].enquiryDimensionList = [];
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_DIMENSION_CREATE, true) || $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_DIMENSION_MODIFY, true)) {
                enquiry.enquiryDetailList[0].enquiryDimensionList.push({});
            }
            enquiry.enquiryDetailList[0].enquiryContainerList = [];
            enquiry.enquiryDetailList[0].enquiryAttachmentList = [];
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_ATTACHMENT_CREATE, true) || $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_ATTACHMENT_MODIFY, true)) {
                enquiry.enquiryDetailList[0].enquiryAttachmentList.push({});
            }
            enquiry.enquiryDetailList[0].clearance = false;
            enquiry.enquiryDetailList[0].overDimension = false;

            if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                enquiry.enquiryDetailList[0].dimensionUnit = true;
                enquiry.enquiryDetailList[0].dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            } else {
                enquiry.enquiryDetailList[0].dimensionUnit = false;
                enquiry.enquiryDetailList[0].dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            }
            enquiry.enquiryDetailList[0].hazardous = false;
            return enquiry;
        }

        function getNewEnquiryDetail() {

            var enquiryDetail = {};
            enquiryDetail.serviceMaster = {};
            enquiryDetail.tosMaster = {};
            enquiryDetail.fromPortMaster = {};
            enquiryDetail.toPortMaster = {};
            enquiryDetail.commodityMaster = {};
            enquiryDetail.enquiryDimensionList = [];
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_DIMENSION_CREATE, true) || $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_DIMENSION_MODIFY, true)) {
                enquiryDetail.enquiryDimensionList.push({});
            }
            enquiryDetail.enquiryContainerList = [];
            enquiryDetail.enquiryAttachmentList = [];
            enquiryDetail.clearance = false;
            enquiryDetail.overDimension = false;
            enquiryDetail.enquiryValueAddedTempServiceList = [];
            enquiryDetail.rateRequestList = [];
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_RATE_REQUEST_CREATE, true) || $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_RATE_REQUEST_MODIFY, true)) {
                enquiryDetail.rateRequestList.push({});
            }
            enquiryDetail.hazardous = false;
            enquiryDetail.importExport = 'Export';
            enquiryDetail.service = 'Air';
            if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                enquiryDetail.dimensionUnit = true;
                enquiryDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            } else {
                enquiryDetail.dimensionUnit = false;
                enquiryDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            }

            valueAddedServicesFactory.getAllValue.query().$promise.then(function(data, status) {

                if (data.responseCode == 'ERR0') {
                    enquiryDetail.enquiryValueAddedTempServiceList = setFalse(data.responseObject.searchResult);
                }


            }, function(response) {
                console.log("error ValueAddedServiceList");
            });

            console.log("Enquiry Detail ", enquiryDetail);
            return enquiryDetail;

        }


        function createEnquiry() {
            var enquiry = {};
            enquiry.partyMaster = {};
            enquiry.salesCoOrdinator = {};
            enquiry.salesman = {};
            enquiry.loggedOn = $rootScope.dateToString(new Date());
            enquiry.loggedBy = $rootScope.userProfile.employee;
            enquiry.receivedOn = null;
            enquiry.enquiryDetailList = [];
            return enquiry;
        }


        function getOriginalEnquiry(enquiryLog) {
            var duplicateEnquiry = JSON.parse(JSON.stringify(enquiryLog));
            duplicateEnquiry.enquiryCustomer = undefined;
            if (duplicateEnquiry.partyMaster != null) {
                duplicateEnquiry.partyMaster.partyName = undefined;
            }
            if (duplicateEnquiry.enquiryDetailList != null && duplicateEnquiry.enquiryDetailList.length > 0) {
                for (var i = 0; i < duplicateEnquiry.enquiryDetailList.length; i++) {
                    duplicateEnquiry.enquiryDetailList[i].enquiryValueAddedTempServiceList = [];

                    if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                        duplicateEnquiry.enquiryDetailList[i].dimensionUnit = true;
                        duplicateEnquiry.enquiryDetailList[i].dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                    } else {
                        duplicateEnquiry.enquiryDetailList[i].dimensionUnit = false;
                        duplicateEnquiry.enquiryDetailList[i].dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                    }
                }
            }

            return duplicateEnquiry;
        }


        function isDimensionFound(enquiryDetail) {

            var flag = false;

            if (enquiryDetail.enquiryDimensionList != undefined &&
                enquiryDetail.enquiryDimensionList != null &&
                enquiryDetail.enquiryDimensionList.length != 0) {

                if (enquiryDetail.enquiryDimensionList.length == 1) {
                    var keyNames = Object.keys(enquiryDetail.enquiryDimensionList[0]);
                    if (keyNames.length == 0) {
                        flag = false
                    } else {
                        flag = true;
                    }
                } else {
                    flag = true;
                }
            }

            return flag;
        }



        function isValueAddedFound(enquiryDetail) {
            if (enquiryDetail.enquiryValueAddedTempServiceList != undefined &&
                enquiryDetail.enquiryValueAddedTempServiceList != null &&
                enquiryDetail.enquiryValueAddedTempServiceList.length != 0) {

                for (var i = 0; i < enquiryDetail.enquiryValueAddedTempServiceList.length; i++) {
                    if (enquiryDetail.enquiryValueAddedTempServiceList[i].selected) {
                        return true;
                    }
                }
            }
            return false;
        }

        function isValueAddedFoundInViewPage(enquiryDetail) {
            if (enquiryDetail.enquiryValueAddedServiceList != undefined &&
                enquiryDetail.enquiryValueAddedServiceList != null) {
                if (enquiryDetail.enquiryValueAddedServiceList.length > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        }


        function isRateRequestFound(enquiryDetail) {

            var flag = false;

            if (enquiryDetail.rateRequestList != undefined &&
                enquiryDetail.rateRequestList != null &&
                enquiryDetail.rateRequestList.length != 0) {

                if (enquiryDetail.rateRequestList.length == 1) {
                    var keyNames = Object.keys(enquiryDetail.rateRequestList[0]);
                    if (keyNames.length == 0) {
                        flag = false
                    } else {
                        flag = true;
                    }
                } else {
                    flag = true;
                }
            }

            return flag;
        }


        function isPickupDeliveryFound(enquiryDetail) {

            var flag = false;

            if (enquiryDetail.pickupAddress != undefined &&
                enquiryDetail.pickupAddress != null) {

                var keyNames = Object.keys(enquiryDetail.pickupAddress);

                if (keyNames.length == 0) {
                    flag = false
                } else {
                    flag = true;
                }
            }

            if (enquiryDetail.deliveryAddress != undefined &&
                enquiryDetail.deliveryAddress != null) {

                var keyNames = Object.keys(enquiryDetail.deliveryAddress);

                if (keyNames.length == 0) {
                    flag = false
                } else {
                    flag = true;
                }
            }

            return flag;
        }

    }
]);