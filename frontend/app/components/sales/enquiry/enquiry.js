/**
 * Created by hmspl on 12/4/16.
 */
app.controller('EnquiryController',['$rootScope', '$scope', '$location', 'ngDialog',
    'EnquiryLogSearch', 'enquiryService', 'EnquiryLogRemove', 'RecentHistorySaveService',
    'GoToUnfilledFormNavigationService', 'EnquiryView', 'quotationService', 'NavigationService', 'addressJoiner',
    'cloneService', '$stateParams', '$state', 'downloadFactory', 'EnquiryAttachmentList', 'EnquiryViewByEnquiryNo',
    'EnquiryStatusChange', 'Notification', 'valueAddedServicesFactory', 'UnitMasterGetByCode', 'roleConstant',
    'EnquiryCreateCustomer', '$modal','isQuotationCreatedFromEnquiry', '$log', function($rootScope, $scope, $location, ngDialog,
    EnquiryLogSearch, enquiryService, EnquiryLogRemove, RecentHistorySaveService,
    GoToUnfilledFormNavigationService, EnquiryView, quotationService, NavigationService, addressJoiner,
    cloneService, $stateParams, $state, downloadFactory, EnquiryAttachmentList, EnquiryViewByEnquiryNo,
    EnquiryStatusChange, Notification, valueAddedServicesFactory, UnitMasterGetByCode, roleConstant,
    EnquiryCreateCustomer, $modal, isQuotationCreatedFromEnquiry, $log) {

    $scope.$roleConstant = roleConstant;

    $scope.isDimensionFound = function(enquiryDetail) {
        return enquiryService.isDimensionFound(enquiryDetail);
    }

    $scope.isPickupDeliveryFound = function(enquiryDetail) {
        return enquiryService.isPickupDeliveryFound(enquiryDetail);
    }

    $scope.isValueAddedFound = function(enquiryDetail) {
        return enquiryService.isValueAddedFoundInViewPage(enquiryDetail);
    }

    $scope.isRateRequestFound = function(enquiryDetail) {
        return enquiryService.isRateRequestFound(enquiryDetail);
    }

    var customerModal = $modal({
        scope: $scope,
        templateUrl: 'app/components/sales/enquiry/view/customer-details-view.html',
        show: false,
        keyboard: true
    });
    $scope.showCustomerModalView = function() {
        console.log("Customer Modal");
        customerModal.$promise.then(customerModal.show);
    };


    $scope.createEnquiryCustomer = function() {
        console.log("createEnquiryCustomer button is pressed ID " + $scope.enquiryLog.id + " Enquiry Customer " + $scope.enquiryLog.enquiryCustomer.id);

        $scope.spinner = true;
        EnquiryCreateCustomer.get({
            id: $scope.enquiryLog.id
        }, function(data) {

            if (data.responseCode == 'ERR0') {
                var refCount = 0;
                if ($stateParams.refCount != undefined && $stateParams.refCount != null) {
                    refCount = $stateParams.refCount + 1;
                }
                customerModal.$promise.then(customerModal.hide);
                $scope.spinner = false;
                Notification.success($rootScope.nls["ERR401"]);
                $state.go('layout.viewSalesEnquiry', {
                    refCount: refCount
                });

            } else {
                console.log("EnquiryCreateCustomer : ", data.responseDescription);
                Notification.error($rootScope.nls[data.responseCode]);
            }
        }, function(error) {
            console.log("EnquiryCreateCustomer : ", error)
        });

    }

    $scope.enquiryHeadArr = [{
            "name": "#",
            "width": "w50px",
            "model": "no",
            "search": false,
            "prefWidth": "50"
        },
        {
            "name": $rootScope.appMasterData['Party_Label'],
            "width": "w200px",
            "model": "customerName",
            "search": true,
            "wrap_cell": true,
            "type": "color-text",
            "sort": true,
            "key": "searchCustomerName",
            "prefWidth": "200"

        },

        {
            "name": "Enquiry",
            "width": "w90px",
            "model": "enquiryNo",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchEnquiryNo",
            "prefWidth": "90"

        },
        {
            "name": "Quote by Date",
            "width": "w150px",
            "model": "quoteBy",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "searchQuoteBy",
            "prefWidth": "150",
            "color": true
        },
        {
            "name": "Enquiry Received On",
            "width": "w150px",
            "model": "receivedOn",
            "search": true,
            "type": "date-time-range",
            "sort": true,
            "key": "searchReceivedOn",
            "prefWidth": "150"
        }, {
            "name": "Quotation No",
            "width": "w90px",
            "model": "quotationNo",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchQuotationNo",
            "prefWidth": "90"
        }, {
            "name": "Sales Coordinator",
            "width": "w130px",
            "model": "salesCoOrdinatorName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchSalesCoOrdinatorName",
            "prefWidth": "130"
        }, {
            "name": "Salesman",
            "width": "w130px",
            "model": "salesmanName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchSalesmanName",
            "prefWidth": "130"
        }, {
            "name": "Logged On",
            "width": "w150px",
            "model": "loggedOnDate",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "searchLoggedOnDate",
            "prefWidth": "150"
        }, {
            "name": "Logged By",
            "width": "w125px",
            "model": "loggedByName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchLoggedByName",
            "prefWidth": "125"
        }
    ]

    $scope.enquiryHeadActiveArr = [{
            "name": "#",
            "width": "w50px",
            "model": "no",
            "search": false,
            "prefWidth": "50"
        },
        {
            "name": $rootScope.appMasterData['Party_Label'],
            "width": "w200px",
            "model": "customerName",
            "search": true,
            "wrap_cell": true,
            "type": "color-text",
            "sort": true,
            "key": "searchCustomerName",
            "prefWidth": "200"

        },

        {
            "name": "Enquiry",
            "width": "w100px",
            "model": "enquiryNo",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchEnquiryNo",
            "prefWidth": "100"

        },
        {
            "name": "Quote by Date",
            "width": "w150px",
            "model": "quoteBy",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "searchQuoteBy",
            "prefWidth": "150",
            "color": true

        },
        {
            "name": "Enquiry Received On",
            "width": "w150px",
            "model": "receivedOn",
            "search": true,
            "type": "date-time-range",
            "sort": true,
            "key": "searchReceivedOn",
            "prefWidth": "150"
        }, {
            "name": "Sales Coordinator",
            "width": "w150px",
            "model": "salesCoOrdinatorName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchSalesCoOrdinatorName",
            "prefWidth": "150"
        }, {
            "name": "Salesman",
            "width": "w150px",
            "model": "salesmanName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchSalesmanName",
            "prefWidth": "150"
        }, {
            "name": "Logged On",
            "width": "w150px",
            "model": "loggedOnDate",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "searchLoggedOnDate",
            "prefWidth": "150"
        }, {
            "name": "Logged By",
            "width": "w125px",
            "model": "loggedByName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchLoggedByName",
            "prefWidth": "125"
        }
    ]

    $scope.setSearch = function(value) {
        $scope.searchFlag = value;
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.enquirysearch($scope.page, $scope.limit);
    }
    $scope.showActionButton = true;
    $scope.selectedRecordIndex = 0;

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
            if ($stateParams.searchEnquiryNo != undefined && $stateParams.searchEnquiryNo != null) {
                stateParameters.searchEnquiryNo = $scope.searchDto.searchEnquiryNo = $stateParams.searchEnquiryNo;
            }
            $scope.searchDto.searchQuoteBy = {};
            if ($stateParams.searchQuoteByStartDate != undefined && $stateParams.searchQuoteByStartDate != null) {
                stateParameters.searchQuoteByStartDate = $scope.searchDto.searchQuoteBy.startDate = $stateParams.searchQuoteByStartDate;
            }
            if ($stateParams.searchQuoteByEndDate != undefined && $stateParams.searchQuoteByEndDate != null) {
                stateParameters.searchQuoteByEndDate = $scope.searchDto.searchQuoteBy.endDate = $stateParams.searchQuoteByEndDate;
            }

            $scope.searchDto.searchReceivedOn = {};
            if ($stateParams.searchReceivedOnStartDate != undefined && $stateParams.searchReceivedOnStartDate != null) {
                stateParameters.searchReceivedOnStartDate = $scope.searchDto.searchReceivedOn.startDate = $stateParams.searchReceivedOnStartDate;
            }
            if ($stateParams.searchReceivedOnEndDate != undefined && $stateParams.searchReceivedOnEndDate != null) {
                stateParameters.searchReceivedOnEndDate = $scope.searchDto.searchReceivedOn.endDate = $stateParams.searchReceivedOnEndDate;
            }

            if ($stateParams.searchQuotationNo != undefined && $stateParams.searchQuotationNo != null) {
                stateParameters.searchQuotationNo = $scope.searchDto.searchQuotationNo = $stateParams.searchQuotationNo;
            }
            if ($stateParams.searchSalesCoOrdinatorName != undefined && $stateParams.searchSalesCoOrdinatorName != null) {
                stateParameters.searchSalesCoOrdinatorName = $scope.searchDto.searchSalesCoOrdinatorName = $stateParams.searchSalesCoOrdinatorName;
            }
            if ($stateParams.searchSalesmanName != undefined && $stateParams.searchSalesmanName != null) {
                stateParameters.searchSalesmanName = $scope.searchDto.searchSalesmanName = $stateParams.searchSalesmanName;
            }

            $scope.searchDto.searchLoggedOn = {};
            if ($stateParams.searchLoggedOnStartDate != undefined && $stateParams.searchLoggedOnStartDate != null) {
                stateParameters.searchLoggedOnStartDate = $scope.searchDto.searchLoggedOn.startDate = $stateParams.searchLoggedOnStartDate;
            }
            if ($stateParams.searchLoggedOnEndDate != undefined && $stateParams.searchLoggedOnEndDate != null) {
                stateParameters.searchLoggedOnEndDate = $scope.searchDto.searchLoggedOn.endDate = $stateParams.searchLoggedOnEndDate;
            }

            if ($stateParams.searchLoggedByName != undefined && $stateParams.searchLoggedByName != null) {
                stateParameters.searchLoggedByName = $scope.searchDto.searchLoggedByName = $stateParams.searchLoggedByName;
            }
            if ($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
                stateParameters.searchStatus = $scope.searchDto.searchStatus = $stateParams.searchStatus;
            }
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchFlag != undefined && $stateParams.searchFlag != null) {
                stateParameters.searchFlag = $scope.searchFlag = $stateParams.searchFlag;
            } else {
                stateParameters.searchFlag = $scope.searchFlag = "active";
            }
        } else {
            return;
        }
        EnquiryLogSearch.query({
            "searchFlag": $scope.searchFlag
        }, $scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.enqIdx = stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.enquiryId = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.viewSalesEnquiry", stateParameters);
        });
    }

    $scope.sortSelection = {
        sortKey: "enquiryNo",
        sortOrder: "desc"
    }


    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_VIEW)) {
            console.log("$scope.searchDto --- ", $scope.searchDto);
            $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var localRefStateParam = {
                enquiryId: data.id,
                enqIdx: $scope.selectedRecordIndex,
                searchFlag: $scope.searchFlag,
                totalRecord: $scope.totalRecord
            };

            console.log("Enquiry  $scope.searchFlag -------------- ", $scope.searchFlag);
            $state.go("layout.viewSalesEnquiry", $scope.searchDtoToStateParams(localRefStateParam));
        }
    }
    $scope.paramToParams = function(param) {

    }
    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.enqIdx;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                //Enquiry Status - Active, Quote, All
                if ($scope.searchDto.searchStatus != undefined && $scope.searchDto.searchStatus != null) {
                    param.searchStatus = $scope.searchDto.searchStatus;
                }
                //Enquiry No
                if ($scope.searchDto.searchEnquiryNo != undefined && $scope.searchDto.searchEnquiryNo != null) {
                    param.searchEnquiryNo = $scope.searchDto.searchEnquiryNo;
                }
                //Quote Created Date Between
                if ($scope.searchDto.searchQuoteBy != undefined && $scope.searchDto.searchQuoteBy != null) {
                    param.searchQuoteByStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchQuoteBy.startDate);
                    param.searchQuoteByEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchQuoteBy.endDate);
                }
                //Enquiry Received on Date Between
                if ($scope.searchDto.searchReceivedOn != undefined && $scope.searchDto.searchReceivedOn != null) {
                    param.searchReceivedOnStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchReceivedOn.startDate);
                    param.searchReceivedOnEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchReceivedOn.endDate);
                }
                //Enq to created Quotation | Quotation No
                if ($scope.searchDto.searchQuotationNo != undefined && $scope.searchDto.searchQuotationNo != null) {
                    param.searchQuotationNo = $scope.searchDto.searchQuotationNo;
                }
                //Sales Coordinator Name
                if ($scope.searchDto.searchSalesCoOrdinatorName != undefined && $scope.searchDto.searchSalesCoOrdinatorName != null) {
                    param.searchSalesCoOrdinatorName = $scope.searchDto.searchSalesCoOrdinatorName;
                }
                //Sales man name
                if ($scope.searchDto.searchSalesmanName != undefined && $scope.searchDto.searchSalesmanName != null) {
                    param.searchSalesmanName = $scope.searchDto.searchSalesmanName;
                }
                //Enquiry Created user logged on Date
                if ($scope.searchDto.searchLoggedOnDate != undefined && $scope.searchDto.searchLoggedOnDate != null) {
                    param.searchLoggedOnStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchLoggedOnDate.startDate);
                    param.searchLoggedOnEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchLoggedOnDate.endDate);
                }
                if ($scope.searchDto.searchLoggedByName != undefined && $scope.searchDto.searchLoggedByName != null) {
                    param.searchLoggedByName = $scope.searchDto.searchLoggedByName;
                }
            }

        }
        return param;
    }


    $scope.viewById = function(id) {
        EnquiryView.get({
            id: id
        }, function(data) {
            $rootScope.setNavigate1("Sales");
            $rootScope.setNavigate2("Enquiry");
            $scope.populateEnquiryData(data);
        }, function(error) {
            console.log("Enquiry view Failed : " + error)
        });
    }

    $scope.viewByEnquiryNo = function(enquiryNo) {
        EnquiryViewByEnquiryNo.get({
            enquiryNo: enquiryNo
        }, function(data) {
            $rootScope.setNavigate1("Sales");
            $rootScope.setNavigate2("Enquiry");
            $scope.populateEnquiryData(data);
        }, function(error) {
            console.log("Enquiry view Failed : " + error)
        });
    }

    $scope.populateEnquiryData = function(data) {
        if (data.responseCode == "ERR0") {
            console.log("Enquiry view Successfully")
            $scope.enquiryLog = data.responseObject;
            $scope.checkStatus($scope.enquiryLog);

            $rootScope.setNavigate1("Sales");
            $rootScope.setNavigate2("Enquiry");

            if ($scope.enquiryLog != undefined && $scope.enquiryLog != null && $scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null &&
                $scope.enquiryLog.partyMaster.partyName != undefined && $scope.enquiryLog.partyMaster.partyName != null) {
                $rootScope.subTitle = $scope.enquiryLog.partyMaster.partyName;
            }
            for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                $scope.enquiryLog.enquiryDetailList[i].dimensionUnitValue = $scope.enquiryLog.enquiryDetailList[i].dimensionUnit ? $rootScope.appMasterData['inch.to.pounds'] : $rootScope.appMasterData['centimeter.to.kilos'];
                $scope.enquiryLog.enquiryDetailList[i].attachConfig = {
                    "isEdit": false,
                    "isDelete": true,
                    "columnDefs": [{
                            "name": "Reference No",
                            "model": "refNo",
                            "type": "text",
                            "width": "w300px"
                        },
                        {
                            "name": "File Name",
                            "model": "fileName",
                            "type": "file",
                            "width": "w200px"
                        }
                    ]

                }
            }


            $rootScope.category = "Enquiry";
            $rootScope.serviceCodeForUnHistory = "";
            $rootScope.orginAndDestinationUnHistory = "";
            if ($scope.enquiryLog != undefined && $scope.enquiryLog != null &&
                $scope.enquiryLog.enquiryDetailList != undefined && $scope.enquiryLog.enquiryDetailList != null &&
                $scope.enquiryLog.enquiryDetailList.length > 0 && $scope.enquiryLog.enquiryDetailList[0] != null) {

                if ($scope.enquiryLog.enquiryDetailList[0].serviceMaster != undefined && $scope.enquiryLog.enquiryDetailList[0].serviceMaster != null) {
                    $rootScope.serviceCodeForUnHistory = $scope.enquiryLog.enquiryDetailList[0].serviceMaster.serviceCode;
                }

                if ($scope.enquiryLog.enquiryDetailList[0].fromPortMaster != undefined && $scope.enquiryLog.enquiryDetailList[0].fromPortMaster != null) {
                    $rootScope.orginAndDestinationUnHistory = $scope.enquiryLog.enquiryDetailList[0].fromPortMaster.portCode;
                }
                if ($scope.enquiryLog.enquiryDetailList[0].toPortMaster != undefined && $scope.enquiryLog.enquiryDetailList[0].toPortMaster != null) {
                    $rootScope.orginAndDestinationUnHistory = $rootScope.orginAndDestinationUnHistory + " > " + $scope.enquiryLog.enquiryDetailList[0].toPortMaster.portCode;
                }
            }
            $rootScope.unfinishedFormTitle = "Enquiry View # " + $scope.enquiryLog.enquiryNo;
            var rHistoryObj = {
                'title': 'Enquiry View #' + $scope.enquiryLog.enquiryNo,
                'subTitle': $rootScope.subTitle,
                'stateName': $state.current.name,
                'stateParam': JSON.stringify($stateParams),
                'stateCategory': 'Enquiry',
                'serviceType': 'AIR',
                'serviceCode': $rootScope.serviceCodeForUnHistory,
                'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                'showService': true

            }

            RecentHistorySaveService.form(rHistoryObj);


            /* for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                $scope.populateAttachment($scope.enquiryLog.enquiryDetailList[i]);
             }*/




        } else {
            console.log("enquiry view Failed " + data.responseDescription)
        }

        if (($stateParams.fromQuoId != null && $stateParams.fromQuoId != undefined && $stateParams.fromQuoId != "") ||
            ($stateParams.showActionButton != null && $stateParams.showActionButton != undefined && $stateParams.showActionButton != "0")) {
            $scope.showActionButton = false;
        }
    }



    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.enquirysearch($scope.page, $scope.limit);
    }

    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.enquirysearch($scope.page, $scope.limit);
    }

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.enquirysearch($scope.page, $scope.limit);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.enquirysearch($scope.page, $scope.limit);
    }

    $scope.init = function() {
        if (NavigationService.getSubData1() == "ENQUIRY_TO_QUOTATION") {
            $scope.detailTab = NavigationService.getSubData3();
            $scope.totalRecord = NavigationService.getSubData4();
            $scope.searchDto = NavigationService.getSubData5();
            $scope.searchFlag = NavigationService.getSubData6();
            $scope.rowSelect(NavigationService.get(), NavigationService.getSubData2())
            NavigationService.clear();
            console.log("ENQUIRY_TO_QUOTATION");
        } else {
            $scope.detailTab = 'active';
            $scope.EnquiryLog = {};
            enquiryService.set({});
            $scope.searchDto = {};
            $scope.setSearch('active');
        }

        getIndivisualCount();
    }

    $scope.back = function() {
        if ($stateParams.nav_src_bkref_key != undefined && $stateParams.nav_src_bkref_key != null &&
            $stateParams.bkState != undefined && $stateParams.bkState != null) {

            // If coming from all other pages
            if ($stateParams.bkParam != undefined && $stateParams.bkParam != null) {
                if ($stateParams.bkParam == 'activiti') {
                    $state.go($stateParams.bkState, {
                        activeTab: $stateParams.bkTab
                    });
                }
            }

            // If coming from Enquiry Edit page
            if ($rootScope.unfinishedFormHistoryList != undefined &&
                $rootScope.unfinishedFormHistoryList != null &&
                $rootScope.unfinishedFormHistoryList.length > 0) {
                for (var rchIndex = 0; rchIndex < $rootScope.unfinishedFormHistoryList.length; rchIndex++) {
                    if ($rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != undefined &&
                        $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key != null &&
                        $rootScope.unfinishedFormHistoryList[rchIndex].nav_src_bkref_key == $stateParams.nav_src_bkref_key) {
                        var controlParams = {
                            submitAction: "Cancelled"
                        };
                        GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[rchIndex], controlParams);
                        break;
                    }
                }
            }

            // If coming from quotation 
        } else if ($stateParams.fromQuoId != undefined && $stateParams.fromQuoId != null) {
            var param = {
                quotationId: $stateParams.fromQuoId
            };
            $state.go('layout.salesQuotationView', param);
        } else {
            if ($scope.detailTab == 'active') {
                $scope.setSearch('active');
            } else if ($scope.detailTab == 'all') {
                $scope.setSearch('all');
            } else if ($scope.detailTab == 'quoteCreated') {
                $scope.setSearch('quote');
            }
            $scope.enquiryLog = {};
            $state.go('layout.salesEnquiry');
        }

    };

    $scope.historyService = function() {

    }
    $scope.deleteEnquiry = function() {
        if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_DELETE)) {
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected Enquiry  ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                EnquiryLogRemove.remove({
                    id: $scope.enquiryLog.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Enquiry deleted Successfully");
                        $scope.init();
                        $scope.back();

                    } else {
                        console.log("Enquiry deleted Failed " + data.responseDescription)
                    }
                }, function(error) {
                    console.log("Enquiry deleted Failed : " + error)
                });
            }, function(value) {
                console.log("Enquiry cancelled");

            });
        }
    }


    var getIndivisualCount = function(selectedPageNumber, recordPerPage) {
        var tmpSearchDto = angular.copy($scope.searchDto);
        tmpSearchDto.selectedPageNumber = -1;
        tmpSearchDto.recordPerPage = -1;
        
        tmpSearchDto.status = "All";
        if (tmpSearchDto.searchQuoteBy != null) {
            tmpSearchDto.searchQuoteBy.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchQuoteBy.startDate);
            tmpSearchDto.searchQuoteBy.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchQuoteBy.endDate);
        }

        if (tmpSearchDto.searchReceivedOn != null) {
            tmpSearchDto.searchReceivedOn.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchReceivedOn.startDate);
            tmpSearchDto.searchReceivedOn.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchReceivedOn.endDate);
        }

        if (tmpSearchDto.searchLoggedOnDate != null) {
            tmpSearchDto.searchLoggedOnDate.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchLoggedOnDate.startDate);
            tmpSearchDto.searchLoggedOnDate.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchLoggedOnDate.endDate);
        }

        EnquiryLogSearch.query({
            "searchFlag": "All"
        }, tmpSearchDto).$promise.then(function(data, status) {
            $scope.activeTotalCount = 0;
            $scope.quoteTotalCount = 0;
            $scope.allTotalCount = 0;

            angular.forEach(data.responseObject.searchResult,function(item, index) {
                switch (item.status) {
                    case "Active":
                        $scope.activeTotalCount += 1; 
                        $scope.allTotalCount += 1; 
                        break;
                    case "Gained":
                        $scope.quoteTotalCount += 1;
                        $scope.allTotalCount += 1; 
                        break;
                }
                
            });
            
        });
    }

    $scope.enquirysearch = function(selectedPageNumber, recordPerPage) {

        $scope.EnquiryLog = {};
        $scope.searchDto.selectedPageNumber = selectedPageNumber;
        $scope.searchDto.recordPerPage = recordPerPage;
        $scope.tmpSearchDto = angular.copy($scope.searchDto);

        if ($scope.tmpSearchDto.searchQuoteBy != null) {
            console.log("$scope.searchDto ,", $scope.searchDto.searchQuoteBy.startDate)
            console.log("$scope.tmpSearchDto ,", $scope.tmpSearchDto.searchQuoteBy.startDate)
            $scope.tmpSearchDto.searchQuoteBy.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchQuoteBy.startDate);
            $scope.tmpSearchDto.searchQuoteBy.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchQuoteBy.endDate);
        }

        if ($scope.tmpSearchDto.searchReceivedOn != null) {
            $scope.tmpSearchDto.searchReceivedOn.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchReceivedOn.startDate);
            $scope.tmpSearchDto.searchReceivedOn.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchReceivedOn.endDate);
        }

        if ($scope.tmpSearchDto.searchLoggedOnDate != null) {
            $scope.tmpSearchDto.searchLoggedOnDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchLoggedOnDate.startDate);
            $scope.tmpSearchDto.searchLoggedOnDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchLoggedOnDate.endDate);
        }

        $scope.enquiryArrList = [];
        EnquiryLogSearch.query({
            "searchFlag": $scope.searchFlag
        }, $scope.tmpSearchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};

            angular.forEach(tempArr, function(item, index) {

                tempObj = item;

                tempObj.no = (index + 1) + ($scope.page * $scope.limit);

                var quoteByDate = tempObj.quoteBy;

                var today = $rootScope.sendApiStartDateTime($rootScope.dateToString(new Date()));




                if (tempObj.customerColorCode != null) {
                    tempObj.color = tempObj.customerColorCode;
                    tempObj.changeColor = true;
                }

                if (quoteByDate <= today && tempObj.quotationNo == null) {
                    tempObj.changeDateColor = true;
                }

                resultArr.push(tempObj);

                tempObj = {};
            });
            $scope.enquiryArrList = resultArr;
        });
    }

    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }

    $scope.enquiryToQuotation = function() {

        if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_CREATE)) {
            isQuotationCreatedFromEnquiry.get({
                enquiryNo: $scope.enquiryLog.enquiryNo
            }, function(data) {
                if (data.responseCode == "ERR0") {

                    if ($scope.enquiryLog.partyMaster == undefined || $scope.enquiryLog.partyMaster == null) {
                        Notification.error($rootScope.nls["ERR6098"]);
                        return false;
                    }
                    var idList = [];

                    for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                        for (j = 0; j < $scope.enquiryLog.enquiryDetailList[i].enquiryAttachmentList.length; j++) {
                            idList.push($scope.enquiryLog.enquiryDetailList[i].enquiryAttachmentList[j].id);
                        }
                    }

                    console.log("List of Attachment ID : ", idList);

                    if (idList.length > 0) {

                        EnquiryAttachmentList.get(idList).$promise.then(function(data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.goToQuotation(data.responseObject);
                                } else {
                                    console.log("Attachment List Getting error ", data.responseDescription)
                                }
                            },
                            function(error) {
                                console.log("Attachment List Getting error ", error)
                            });
                    } else {
                        $scope.goToQuotation(null);
                    }

                } else {
                    Notification.warning($rootScope.nls["ERR23101"]);
                }
            }, function(error) {
                console.log("enquiryToQuotation view Failed : " + error)
            });

        }
    }

    //For Cancelling the enquiry   
    $scope.enquiryCancelled = function(enq) {

        if (enq.status == 'Cancel') {
            Notification.warning("Already Enquiry was cancelled");
            return;
        }
        if (enq.statusChange == true) {
            ngDialog.openConfirm({
                template: '<p>Do you want to cancel the enquiry?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveEnquiryStatus("Cancelled", enq);
                } else {
                    enq.statusChange = false;
                }
            })
        }

    }


    $scope.saveEnquiryStatus = function(status, enq) {

        $scope.enquiryDto = {};
        $scope.enquiryDto.enqId = enq.id;
        $scope.enquiryDto.enquiryStatus = 'Cancel';
        EnquiryStatusChange.status($scope.enquiryDto).$promise
            .then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.enquiryLog = data.responseObject;
                    $scope.enquiryLog.statusChange = true;
                    Notification.success($rootScope.nls["ERR7025"]);
                } else {
                    Notification.error($rootScope.nls["ERR7026"]);
                    enq.statusChange = false;
                }
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                Notification.error($rootScope.nls["ERR7026"]);
                enq.statusChange = false;
            });
    }

    //for enabling the cancelled check box 
    $scope.checkStatus = function(dataObj) {
        if (dataObj != undefined) {
            dataObj.statusChange = dataObj.status == "Cancel" ? true : false;
        }
    }

    $scope.chargeDetailList = [];
    //for showing rate charges in list
    $scope.showRateChargesInList = function(rateReqObj, index) {

        $scope.chargeShowList = rateReqObj.chargeList;
        if ($scope.chargeDetailList[index]) {
            $scope.chargeDetailList[index] = false;
        } else {
            $scope.chargeDetailList[index] = true;
        }
    }




    $scope.goToQuotation = function(enquiryAttachmentList) {


        console.log("Getting List of Attachments..");
        valueAddedServicesFactory.getAllValue.query().$promise.then(function(data, status) {
            $scope.quotation = {};
            $scope.quotation.id = -1;
            $scope.quotation.locationMaster = $rootScope.userProfile.selectedUserLocation;
            $scope.quotation.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
            $scope.quotation.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
            $scope.quotation.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.quotation.enquiryNo = $scope.enquiryLog.enquiryNo;
            $scope.quotation.customer = $scope.enquiryLog.partyMaster;
            $scope.quotation.salesCoordinator = $scope.enquiryLog.salesCoOrdinator;
            $scope.quotation.salesman = $scope.enquiryLog.salesman;
            $scope.quotation.whoRouted = $scope.enquiryLog.salesman = 'Self';
            $scope.quotation.loggedOn = $rootScope.dateToString(new Date());
            $scope.quotation.loggedBy = $rootScope.userProfile.employee;
            $scope.quotation.footerNote = null;
            $scope.quotation.headerNote = null;
            $scope.quotation.quoteType = 'GENERAL';
            $scope.quotation.validFrom = $rootScope.dateToString(new Date());
            $scope.quotation.validTo = $rootScope.dateToString(moment().add('days', $rootScope.appMasterData['quotation.valid.days']));


            $scope.quotation.quotationDetailList = [];
            var exportSegment = false;
            for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                $scope.quotationDetail = {};
                $scope.quotationDetail.processInstanceId = $scope.enquiryLog.enquiryDetailList[i].processInstanceId;
                $scope.quotationDetail.taskId = $scope.enquiryLog.enquiryDetailList[i].taskId;
                $scope.quotationDetail.serviceMaster = $scope.enquiryLog.enquiryDetailList[i].serviceMaster;
                $scope.quotationDetail.origin = $scope.enquiryLog.enquiryDetailList[i].fromPortMaster;
                $scope.quotationDetail.destination = $scope.enquiryLog.enquiryDetailList[i].toPortMaster;
                $scope.quotationDetail.tosMaster = $scope.enquiryLog.enquiryDetailList[i].tosMaster;
                $scope.quotationDetail.commodityMaster = $scope.enquiryLog.enquiryDetailList[i].commodityMaster;
                if ($scope.enquiryLog.enquiryDetailList[i].commodityMaster != null && $scope.enquiryLog.enquiryDetailList[i].commodityMaster != undefined && $scope.enquiryLog.enquiryDetailList[i].commodityMaster.id != undefined) {
                    $scope.quotationDetail.commodityCode = $scope.enquiryLog.enquiryDetailList[i].commodityMaster.hsCode;
                }
                $scope.quotationDetail.dimensionUnit = $scope.enquiryLog.enquiryDetailList[i].dimensionUnit;
                $scope.quotationDetail.dimensionUnitValue = $scope.enquiryLog.enquiryDetailList[i].dimensionUnitValue;
                $scope.quotationDetail.hazardous = $scope.enquiryLog.enquiryDetailList[i].hazardous;
                /* $scope.quotationDetail.pickUpAddress = addressJoiner.addressMasterToString ($scope.enquiryLog.enquiryDetailList[i].pickupAddress).fullAddress;
                 $scope.quotationDetail.deliveryAddress = addressJoiner.addressMasterToString ($scope.enquiryLog.enquiryDetailList[i].deliveryAddress).fullAddress;
                 */

                /**
                 * Pickup / Delivery Details starts
                 * */


                $scope.quotationDetail.pickupAddress = {};
                if ($scope.enquiryLog.enquiryDetailList[i].pickupAddress != null && $scope.enquiryLog.enquiryDetailList[i].pickupAddress.id != null) {
                    $scope.quotationDetail.pickupAddress.addressLine1 = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.addressLine1;
                    $scope.quotationDetail.pickupAddress.addressLine2 = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.addressLine2;
                    $scope.quotationDetail.pickupAddress.addressLine3 = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.addressLine3;
                    $scope.quotationDetail.pickupAddress.city = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.city;
                    $scope.quotationDetail.pickupAddress.poBox = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.poBox;
                    $scope.quotationDetail.pickupAddress.state = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.state;
                    $scope.quotationDetail.pickupAddress.country = $scope.enquiryLog.enquiryDetailList[i].pickupAddress.country;
                }



                $scope.quotationDetail.deliveryAddress = {};
                if ($scope.enquiryLog.enquiryDetailList[i].deliveryAddress != null && $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.id != null) {
                    $scope.quotationDetail.deliveryAddress.addressLine1 = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.addressLine1;
                    $scope.quotationDetail.deliveryAddress.addressLine2 = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.addressLine2;
                    $scope.quotationDetail.deliveryAddress.addressLine3 = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.addressLine3;
                    $scope.quotationDetail.deliveryAddress.city = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.city;
                    $scope.quotationDetail.deliveryAddress.poBox = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.poBox;
                    $scope.quotationDetail.deliveryAddress.state = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.state;
                    $scope.quotationDetail.deliveryAddress.country = $scope.enquiryLog.enquiryDetailList[i].deliveryAddress.country;
                }


                /**
                 * Pickup / Delivery Details ends
                 * */

                $scope.quotationDetail.grossWeight = $scope.enquiryLog.enquiryDetailList[i].grossWeight;
                $scope.quotationDetail.volumeWeight = $scope.enquiryLog.enquiryDetailList[i].volWeight;

                if ($scope.enquiryLog.enquiryDetailList[i].volWeight < $scope.enquiryLog.enquiryDetailList[i].grossWeight) {
                    $scope.quotationDetail.chargebleWeight = $scope.enquiryLog.enquiryDetailList[i].grossWeight;
                } else {
                    $scope.quotationDetail.chargebleWeight = $scope.enquiryLog.enquiryDetailList[i].volWeight;
                }

                if ($scope.quotationDetail.serviceMaster.importExport == 'Export') {
                    exportSegment = true;
                }
                var totalNoOfPieces = 0;
                var dimensionVolWeight = 0;
                var dimensionGrossWeight = 0;
                var dimensionGrossWeightKg = 0;
                for (var j = 0; j < $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList.length; j++) {

                    if (j == 0) {
                        $scope.quotationDetail.quotationDimensionList = [];
                    }
                    $scope.dimension = {};
                    $scope.dimension.noOfPiece = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].noOfPiece;
                    $scope.dimension.length = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].length;
                    $scope.dimension.width = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].width;
                    $scope.dimension.height = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].height;
                    $scope.dimension.volWeight = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].volWeight;
                    $scope.dimension.grossWeight = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].grossWeight;
                    $scope.dimension.grossWeightKg = $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].grossWeightKg;
                    totalNoOfPieces = parseInt(totalNoOfPieces) + parseInt($scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].noOfPiece);
                    dimensionVolWeight = parseFloat(dimensionVolWeight) + parseInt($scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].volWeight);
                    dimensionGrossWeight = parseFloat(dimensionGrossWeight) + parseInt($scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].grossWeight);
                    dimensionGrossWeightKg = parseFloat(dimensionGrossWeightKg) + parseInt($scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList[j].grossWeightKg);
                    $scope.quotationDetail.quotationDimensionList.push($scope.dimension);

                }
                if ($scope.enquiryLog.enquiryDetailList[i].rateRequestList != undefined && $scope.enquiryLog.enquiryDetailList[i].rateRequestList != null && $scope.enquiryLog.enquiryDetailList[i].rateRequestList.length > 0) {
                    $scope.isRateAccepted = false;
                    if ($scope.enquiryLog.enquiryDetailList[i].rateRequestList.length == 1) {
                        $scope.acceptedVendor = {};
                        $scope.acceptedVendor = $scope.enquiryLog.enquiryDetailList[i].rateRequestList[0];
                        $scope.isRateAccepted = true;
                    } else {
                        for (var k = 0; k < $scope.enquiryLog.enquiryDetailList[i].rateRequestList.length; k++) {
                            if ($scope.enquiryLog.enquiryDetailList[i].rateRequestList[k].rateAccepted != undefined && $scope.enquiryLog.enquiryDetailList[i].rateRequestList[k].rateAccepted != null && $scope.enquiryLog.enquiryDetailList[i].rateRequestList[k].rateAccepted == 'Yes') {
                                $scope.acceptedVendor = {};
                                $scope.acceptedVendor = $scope.enquiryLog.enquiryDetailList[i].rateRequestList[k];
                                $scope.isRateAccepted = true;
                                break;
                            }
                        }
                    }
                    if (!$scope.isRateAccepted) {
                        Notification.error($rootScope.nls["ERR8836"]);
                        return;
                    }
                }

                if ($scope.quotationDetail.quotationCarrierList == undefined || $scope.quotationDetail.quotationCarrierList == null) {
                    $scope.quotationDetail.quotationCarrierList = [];
                    var quotationCarrier = {};
                    quotationCarrier.quotationChargeList = [];
                    $scope.quotationDetail.quotationCarrierList.push(quotationCarrier);
                }

                //move charge to quotation
                if ($scope.acceptedVendor != undefined && $scope.isRateAccepted) {
                    for (var l = 0; l < $scope.acceptedVendor.chargeList.length; l++) {
                        var quotationCharge = {};
                        quotationCharge.chargeMaster = $scope.acceptedVendor.chargeList[l].chargeMaster;
                        quotationCharge.chargeName = $scope.acceptedVendor.chargeList[l].chargeName;
                        quotationCharge.unitMaster = $scope.acceptedVendor.chargeList[l].unitMaster;
                        quotationCharge.currencyMaster = $scope.acceptedVendor.chargeList[l].currencyMaster;
                        quotationCharge.numberOfUnit = $scope.acceptedVendor.chargeList[l].unitSlap;
                        quotationCharge.buyRateCostPerUnit = $scope.acceptedVendor.chargeList[l].buyRate;
                        quotationCharge.buyRateMinCost = $scope.acceptedVendor.chargeList[l].buyRateMinCost;
                        quotationCharge.agentRateRequestChargeId = $scope.acceptedVendor.chargeList[l].id;
                        quotationCharge.roe = $scope.getRoe(quotationCharge);
                        $scope.quotationDetail.quotationCarrierList[0].quotationChargeList.push(quotationCharge);
                    }

                }
                $scope.quotationDetail.totalNoOfPieces = totalNoOfPieces;
                $scope.quotationDetail.dimensionVolWeight = dimensionVolWeight;
                $scope.quotationDetail.dimensionGrossWeight = dimensionGrossWeight;
                $scope.quotationDetail.dimensionGrossWeightInKg = dimensionGrossWeightKg;
                $scope.quotationDetail.quotationValueAddedTmpServiceList = [];
                $scope.quotationDetail.quotationValueAddedTmpServiceList = data.responseObject.searchResult;


                if ($scope.quotationDetail.quotationValueAddedTmpServiceList.length != 0 && $scope.quotationDetail.quotationValueAddedTmpServiceList != undefined) {

                    for (var k = 0; k < $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedServiceList.length; k++) {

                        for (var l = 0; l < $scope.quotationDetail.quotationValueAddedTmpServiceList.length; l++) {

                            if ($scope.quotationDetail.quotationValueAddedTmpServiceList[l].id == $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedServiceList[k].valueAddedServices.id) {
                                $scope.quotationDetail.quotationValueAddedTmpServiceList[l].selected = true;

                                if ($scope.quotationDetail.quotationValueAddedTmpServiceList[l].valueAddedChargeList != undefined && $scope.quotationDetail.quotationValueAddedTmpServiceList[l].valueAddedChargeList != null && $scope.quotationDetail.quotationValueAddedTmpServiceList[l].valueAddedChargeList.length > 0) {
                                    for (var m = 0; m < $scope.quotationDetail.quotationValueAddedTmpServiceList[l].valueAddedChargeList.length; m++) {
                                        var quotationCharge = {};
                                        quotationCharge.chargeMaster = $scope.quotationDetail.quotationValueAddedTmpServiceList[l].valueAddedChargeList[m].chargeMaster;
                                        $scope.makeQuotationCharge(quotationCharge);
                                        quotationCharge.valueAddedId = $scope.quotationDetail.quotationValueAddedTmpServiceList[l].id;
                                        $scope.quotationDetail.quotationCarrierList[0].quotationChargeList.push(quotationCharge);
                                    }
                                }
                            }
                        }
                    }
                }
                $scope.quotation.quotationDetailList.push($scope.quotationDetail);
            }

            if (exportSegment) {
                if ($scope.enquiryLog.partyMaster.partyTypeList != null) {
                    for (var i = 0; i < $scope.enquiryLog.partyMaster.partyTypeList.length; i++) {
                        if ($scope.enquiryLog.partyMaster.partyTypeList[i].partyTypeMaster.partyTypeCode == 'SP') {
                            $scope.quotation.shipper = $scope.enquiryLog.partyMaster;
                        }
                    }
                }
            }


            $scope.quotation.quotationAttachementList = [];
            if (enquiryAttachmentList != undefined && enquiryAttachmentList != null && enquiryAttachmentList != null && enquiryAttachmentList.length != 0) {
                for (j = 0; j < enquiryAttachmentList.length; j++) {
                    enquiryAttachmentList[j].id = null;
                    $scope.quotation.quotationAttachementList.push(enquiryAttachmentList[j]);
                }
            }

            quotationService.set($scope.quotation);
            quotationService.setLoc("ENQ");
            if ($rootScope.userProfile.featureMap['SALES_QUOTATION_SERVICE_TAB_VIEW']) {
                $scope.detailTab = 'Services';
            }
            console.log("Creating Quotation For : ", $scope.quotation);
            $rootScope.createQuotation = $scope.quotation;
            var param = {
                enquiryId: $scope.enquiryLog.id
            };
            $state.go('layout.salesQuotationCreate', param);

        }, function(response) {
            console.log("Error found in CreateQuotation  From Enqury", response);
        });


    }

    $scope.makeQuotationCharge = function(charge) {

        if (charge.chargeMaster != undefined && charge.chargeMaster != null) {

            charge.chargeName = charge.chargeMaster.chargeName;

            if (charge.chargeMaster.calculationType != undefined && charge.chargeMaster.calculationType != null) {

                var unitCode;

                if (charge.chargeMaster.calculationType == 'Percentage') {

                    unitCode = 'PER';

                } else if (charge.chargeMaster.calculationType == 'Shipment') {

                    unitCode = 'SHPT';

                } else if (charge.chargeMaster.calculationType == 'Unit') {

                    unitCode = 'KG';

                } else if (charge.chargeMaster.calculationType == 'Document') {

                    unitCode = 'DOC';

                } else {


                }

                UnitMasterGetByCode.get({

                    unitCode: unitCode

                }).$promise.then(function(resObj) {

                    if (resObj.responseCode == "ERR0") {

                        charge.unitMaster = resObj.responseObject;

                    }

                }, function(error) {

                    console.log("unit Get Failed : ", error)

                });

            }

        }

        charge.currencyMaster = $scope.quotation.localCurrency;

        var sellRate = null;

        if ($rootScope.baseCurrenyRate[charge.currencyMaster.id] != null && $scope.quotation.localCurrency.id != charge.currencyMaster.id) {

            sellRate = $rootScope.baseCurrenyRate[charge.currencyMaster.id].csell;

        } else if (charge.currencyMaster.id == $scope.quotation.localCurrency.id) {

            sellRate = 1;

        }

        charge.roe = sellRate;

        charge.actualchargeable = 'CHARGEABLE';


    }


    $scope.getRoe = function(dataObj) {
        var sellRate = null;
        if (dataObj.currencyMaster != undefined && dataObj.currencyMaster != null) {
            if ($rootScope.baseCurrenyRate[dataObj.currencyMaster.id] != null && $scope.quotation.localCurrency.id != dataObj.currencyMaster.id) {
                sellRate = $rootScope.baseCurrenyRate[dataObj.currencyMaster.id].csell;
            } else if (dataObj.currencyMaster.id == $scope.quotation.localCurrency.id) {
                sellRate = 1;
            }
            return sellRate;
        }

    }

    $scope.openDefault = function(index) {
        if ($rootScope.userProfile.featureMap['ENQUIRY_SERVICE_DIMENSIONS_TAB_VIEW']) {
            $scope.moreAddressTab = index + 'dimensionBtn';
            return true;
        }
    }

    $scope.rowLink = function(data) {
        console.log("data", data);
    }


    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/enquirylog/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }

    $scope.serviceEnquiryRolesView = function(tab) {
        if (tab.includes("dimensionRoles")) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_DIMENSION_VIEW)) {
                $scope.dimensionRoles = true;
            } else {
                $scope.dimensionRoles = false;
            }
        }
        if (tab.includes("pickUpDelivery")) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_PICKUP_DELIVERY_VIEW)) {
                $scope.pickUpDelivery = true;
            } else {
                $scope.pickUpDelivery = false;
            }
        }
        if (tab.includes("valueAddedService")) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_VALUE_ADDED_SERVICE_VIEW)) {
                $scope.valueAddedService = true;
            } else {
                $scope.valueAddedService = false;
            }
        }
        if (tab.includes("rateRequestRole")) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_RATE_REQUEST_VIEW)) {
                $scope.rateRequestRole = true;
            } else {
                $scope.rateRequestRole = false;
            }
        }
        if (tab.includes("attachmentRole")) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_ATTACHMENT_VIEW)) {
                $scope.attachmentRole = true;
            } else {
                $scope.attachmentRole = false;
            }
        }
        if (tab.includes("remarksRole")) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_REMARKS_VIEW)) {
                $scope.remarksRole = true;
            } else {
                $scope.remarksRole = false;
            }
        }
    }

    /***
     * Attachment Code Ends Here 
     ***/

    $scope.selectService = function(index) {
        if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_SERVICE_VIEW)) {
            $scope.serviceAccessRights = true;
            $scope.selectedTabIndex = index;

        } else {
            $scope.serviceAccessRights = false;
        }
    }
    $scope.addEnquiry = function() {
        if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_CREATE)) {
            $state.go("layout.addSalesEnquiry");
        }
    }

    $scope.editEnquiry = function() {
        if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_MODIFY)) {
            var localRefStateParam = {
                enquiryId: $scope.enquiryLog.id
            };
            //              console.log("State Parameters :: ", localRefStateParam);
            var ishistoryFounded = false;
            if ($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0) {
                for (var iIndex = 0; iIndex < $rootScope.unfinishedFormHistoryList.length; iIndex++) {
                    var tit = "Enquiry Edit # " + $scope.enquiryLog.id;
                    if ($rootScope.unfinishedFormHistoryList[iIndex].title == tit) {
                        var unSavedData = $rootScope.unfinishedFormHistoryList[iIndex];
                        GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[iIndex], undefined); // $rootScope.unfinishedFormHistoryList.push(stateDataObject);
                        ishistoryFounded = true;
                    }
                    if (ishistoryFounded) {
                        break;
                    }
                }
            }
            if (!ishistoryFounded) {
                $state.go("layout.editSalesEnquiry", localRefStateParam);
            }
        }
    }
    switch ($stateParams.action) {
        case "VIEW":
            $rootScope.unfinishedFormTitle = "Enquiry View # " + $stateParams.enquiryId;
            $rootScope.unfinishedData = undefined;
            $scope.selectService(0);
            $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
            $scope.totalRecord = parseInt($stateParams.totalRecord);
            if ($stateParams.enquiryId != undefined && $stateParams.enquiryId != null) {
                $scope.viewById($stateParams.enquiryId);
            } else if ($stateParams.enquiryNo != undefined && $stateParams.enquiryNo != null) {
                $scope.viewByEnquiryNo($stateParams.enquiryNo);
            }

            // $scope.view($stateParams.enquiryId);
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesEnquiry"
            }, {
                label: "Enquiry",
                state: "layout.salesEnquiry"
            }, {
                label: "View Enquiry",
                state: null
            }];
            break;
        case "SEARCH":
            console.log("I am In Enquiry List Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesEnquiry"
            }, {
                label: "Enquiry",
                state: null
            }];
            break;
        default:

    }

}]);