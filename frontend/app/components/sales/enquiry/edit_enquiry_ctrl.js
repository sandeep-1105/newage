app.controller("EditEnquiryController", ['$rootScope', '$scope', '$modal', '$location', 'ngDialog', 'EmployeeList',
    'EmployeeListOnLocation', 'SalesmanListOnLocation', 'SalesmanList', 'ServiceList', 'Notification',
    'TosSearchKeyword', 'PortSearchKeyword', 'CommodityService', 'EnquiryAdd',
    'PartiesList', 'enquiryService', 'EnquiryUpdate', 'cloneService', 'EnquiryStatusChange',
    'PortByTransportMode', 'quotationService', 'PartyMasterService', 'StateList', 'CityList', 'EnquiryView',
    '$stateParams', '$state', '$window', 'EnquiryLogSearchRecent', 'ngProgressFactory',
    'RecentHistorySaveService', 'downloadFactory', '$timeout', 'appConstant', 'CommonValidationService',
    'valueAddedServicesFactory', 'PortService', 'roleConstant', 'AutoCompleteService',
    'AgentByPort',
    function($rootScope, $scope, $modal, $location, ngDialog, EmployeeList,
        EmployeeListOnLocation, SalesmanListOnLocation, SalesmanList, ServiceList, Notification,
        TosSearchKeyword, PortSearchKeyword, CommodityService, EnquiryAdd,
        PartiesList, enquiryService, EnquiryUpdate, cloneService, EnquiryStatusChange,
        PortByTransportMode, quotationService, PartyMasterService, StateList, CityList, EnquiryView,
        $stateParams, $state, $window, EnquiryLogSearchRecent, ngProgressFactory,
        RecentHistorySaveService, downloadFactory, $timeout, appConstant, CommonValidationService,
        valueAddedServicesFactory, PortService, roleConstant, AutoCompleteService,
        AgentByPort) {

        $scope.$AutoCompleteService = AutoCompleteService;
        $scope.$roleConstant = roleConstant;

        $scope.moveCursor = function(id, tab) {
            $rootScope.navigateToNextField(id);
        }

        $scope.isDimensionFound = function(enquiryDetail) {
            return enquiryService.isDimensionFound(enquiryDetail);
        }

        $scope.isValueAddedFound = function(enquiryDetail) {
            return enquiryService.isValueAddedFound(enquiryDetail);
        }

        $scope.isRateRequestFound = function(enquiryDetail) {
            return enquiryService.isRateRequestFound(enquiryDetail);
        }

        $scope.isPickupDeliveryFound = function(enquiryDetail) {
            return enquiryService.isPickupDeliveryFound(enquiryDetail);
        }


        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('top-enquiry-panel'));
        $scope.contained_progressbar.setAbsolute();
        $scope.contained_progressbar.start();
        $scope.enquiryCustomer = {};
        $scope.enqC = "";



        $scope.disableSubmitBtn = false;

        $scope.enableSubmitBtnFn = function() {
            $timeout(function() {
                console.log("Enable btn ,", new Date());
                $scope.disableSubmitBtn = false;
            }, 300);
        }

        //Advanced popup
        var myOtherModalAdvanced = $modal({
            scope: $scope,
            templateUrl: 'app/components/sales/enquiry/view/advanced-view-popup.html',
            show: false,
            keyboard: true
        });
        $scope.getAdvancedPopUp = function() {
            console.log("Advanced popup ");
            myOtherModalAdvanced.$promise.then(myOtherModalAdvanced.show);
        };

        var customerModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/sales/enquiry/view/customer-details.html',
            show: false,
            keyboard: true
        });
        $scope.showCustomerModal = function() {
            console.log("Customer Modal");
            $scope.errorMap = new Map();
            customerModal.$promise.then(customerModal.show);
        };

        $scope.selectedEnquiryCustomerCountry = function(nextField) {

            if ($scope.validateEnquiryCustomer(1)) {
                $scope.navigateToNextField(nextField);
            }

        }

        $scope.ajaxCityEvent = function(keyword, countryMaster) {
            if ($scope.validateEnquiryCustomer(1)) {
                return AutoCompleteService.getCityList(keyword, countryMaster);
            }
        }

        $scope.selectedEnquiryCustomerCity = function(nextFieldId) {
            if ($scope.validateEnquiryCustomer(3)) {
                if ($scope.enquiryLog.enquiryCustomer.cityMaster.stateMaster != undefined && $scope.enquiryLog.enquiryCustomer.cityMaster.stateMaster != null) {
                    $scope.enquiryLog.enquiryCustomer.stateMaster = $scope.enquiryLog.enquiryCustomer.cityMaster.stateMaster;
                }
                $scope.navigateToNextField(nextFieldId);
            }
        };

        $scope.ajaxStateEvent = function(keyword, countryMaster) {
            if ($scope.validateEnquiryCustomer(1)) {
                return AutoCompleteService.getStateList(keyword, countryMaster);
            }
        }

        $scope.selectedEnquiryCustomerState = function(nextFieldId) {
            if ($scope.validateEnquiryCustomer(4)) {
                $scope.navigateToNextField(nextFieldId);
            }
        };

        $scope.saveEnquiryCustomer = function() {

            if ($scope.validateEnquiryCustomer(0)) {
                console.log("Saving Enquiry Customer Details ");
                customerModal.$promise.then(customerModal.hide);
                $rootScope.navigateToNextField("salesCoordinator");
            }

        }

        $scope.cancelEnquiryCustomer = function() {
            console.log("Cancel Enquiry Customer Details ");
            customerModal.$promise.then(customerModal.hide);
            $rootScope.navigateToNextField("salesCoordinator");
        }



        var attachmentModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/sales/enquiry/view/attachment_popup.html',
            backdrop: 'static',
            show: false,
            keyboard: true
        });

        $scope.openAttachmentPopUp = function(index) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_ATTACHMENT_VIEW)) {
                if ($scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList == undefined ||
                    $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList == null ||
                    $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList.length == 0) {
                    $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList = [];
                    if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_ATTACHMENT_CREATE, true) || $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_ATTACHMENT_MODIFY, true)) {
                        $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList.push({});
                    }
                }
                attachmentModal.$promise.then(attachmentModal.show);
                $rootScope.navigateToNextField('EnqSerAttattachRefNo0');
            }
        }

        $scope.cancelAttachmentPopUp = function() {
            attachmentModal.$promise.then(attachmentModal.hide);

            if ($scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList != undefined &&
                $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList != null &&
                $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList.length != 0) {
                var attachmentList = [];
                for (var i = 0; i < $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList.length; i++) {
                    if ($scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList[i].id != undefined &&
                        $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList[i].id != null) {
                        attachmentList.push($scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList[i]);
                    }
                }

                $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList = [];
                $scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].enquiryAttachmentList = attachmentList;
            }
        }


        $scope.saveAttachmentPopUp = function() {

            if (!$scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.selectedTabIndex].isAttachmentCheck()) {
                Notification.error($rootScope.nls['ERR250']);
                return false;
            }

            attachmentModal.$promise.then(attachmentModal.hide);
        }


        $scope.downloadAttach = function(param) {

            if (param.data.id != null && param.data.file == null) {
                console.log("API CALL")
                downloadFactory.downloadAttachment('/api/v1/enquirylog/files/', param.data.id, param.data.fileName);
            } else {
                console.log("NO API CALL", param);
                saveAs(param.data.tmpFile, param.data.fileName);
            }
        }

        var remarksModal = $modal({
            scope: $scope,

            templateUrl: 'app/components/sales/enquiry/view/remarks_popup.html',
            backdrop: 'static',
            selectedTabIndex: $scope.selectedTabIndex,
            show: false,
            keyboard: true

        });

        $scope.openRemarksPopUp = function(index) {
            $scope.selectedTabIndex = index;
            if ($scope.enquiryLog.id != undefined) {
                if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_REMARKS_MODIFY)) {
                    remarksModal.$promise.then(remarksModal.show);
                    $rootScope.navigateToNextField('notes0');
                }
            } else {
                remarksModal.$promise.then(remarksModal.show);
                $rootScope.navigateToNextField('notes0');
            }
        }

        $scope.cancelRemarksPopUp = function() {
            console.log("cencelRemarksPopUp is called... ");
            remarksModal.$promise.then(remarksModal.hide);
        }

        $scope.saveRemarksPopUp = function() {
            console.log("saveRemarksPopUp is called... ");
            remarksModal.$promise.then(remarksModal.hide);
        }

        var rateRequestModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/sales/enquiry/view/rateRequest_Popup.html',
            backdrop: 'static',
            show: false,
            keyboard: true
        });

        $scope.openRateRequestPopUp = function () {
            rateRequestModal.$promise.then(rateRequestModal.show);
            setTimeout(function() {
                angular.element("#rateRequestSection").trigger("click");
            }, 300);
        }

        $scope.yesRateRequestPopUp = function () {
            console.log("yesRateRequestPopUp is called... ");
            rateRequestModal.$promise.then(rateRequestModal.hide);
            $scope.isRRPopupShown = true;
        }

        $scope.noRateRequestPopUp = function () {
            console.log("noRateRequestPopUp is called... ");
            rateRequestModal.$promise.then(rateRequestModal.hide);
        }

        var oldEnquiry;
        $scope.init = function() {
            console.log("Init is called.........");
            $scope.isRRPopupShown = false;
            $scope.enquiryLog = enquiryService.createEnquiry();
            $scope.enquiryLog.serviceAccessRights = [];
            $scope.enquiryLog.selectedTabIndex = 0;
            $scope.addNewService();
            $rootScope.navigateToNextField("partyName");
            oldEnquiry = JSON.stringify($scope.enquiryLog);
            //console.log("oldEnquiry:"+oldEnquiry);
        };

        $scope.enquiryTableCheck = {};


        $scope.initDateFormat = function() {

            $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";

            $scope.directiveDatePickerOptsLeft = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOptsLeft.widgetParent = "body";
            $scope.directiveDatePickerOptsLeft.widgetPositioning = {
                horizontal: 'right'
            };

            $scope.directiveDateTimePickerOpts = {
                format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                useCurrent: false,
                sideBySide: true,
                viewDate: moment()
            };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";



            $scope.dateTimePickerOptionsLeft = {
                format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                useCurrent: false,
                sideBySide: true,
                viewDate: moment()
            };
            $scope.dateTimePickerOptionsLeft.widgetParent = "body";
            $scope.dateTimePickerOptionsLeft.widgetPositioning = {
                horizontal: 'right'
            };

        }


        $scope.serviceEnquiryRolesView = function(tab, enqDetail) {

            if (tab == 'pickUpDelivery') {
                $scope.pickUpDeliveryEdit = false;
                if ($scope.enquiryLog.id != null) {
                    if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_PICKUP_DELIVERY_MODIFY)) {
                        $scope.pickUpDeliveryEdit = true;
                    } else {
                        $scope.pickUpDeliveryEdit = false;
                    }
                } else {
                    $scope.pickUpDeliveryEdit = true;
                }
            }
            if (tab == 'valueAddedService') {
                $scope.valueAddedServiceEdit = false;
                if ($scope.enquiryLog.id != null) {
                    if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_VALUE_ADDED_SERVICE_MODIFY)) {
                        $scope.valueAddedServiceEdit = true;
                    } else {
                        $scope.valueAddedServiceEdit = false;
                    }
                } else {
                    $scope.valueAddedServiceEdit = true;
                }
            }

            if (tab === 'rateRequestRole') {
                fetchDefaultAgent(enqDetail);
            }

        }

        $scope.validateService = function(validateCode, enqDetail, index) {

            $scope.errorMap = new Map();


            if (validateCode == 0 || validateCode == 10) {
                if (enqDetail.importExport == null || enqDetail.importExport == undefined || enqDetail.importExport == "null") {
                    $scope.errorMap.put("transport" + index, $rootScope.nls["ERR8844"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 11) {
                if (enqDetail.service == null || enqDetail.service == undefined || enqDetail.service == "null") {
                    $scope.errorMap.put("service" + index, $rootScope.nls["ERR8845"]);
                    return false;
                }
            }


            if (validateCode == 0 || validateCode == 1) {}

            if (validateCode == 0 || validateCode == 2) {
                if (enqDetail.fromPortMaster == null ||
                    enqDetail.fromPortMaster.id == null ||
                    enqDetail.fromPortMaster.id.trim == "") {
                    $scope.errorMap.put("origin" + index, $rootScope.nls["ERR8792"]);
                    return false;
                }


                if (enqDetail.fromPortMaster.status == "Hide") {
                    enqDetail.fromPortMaster = null;
                    $scope.errorMap.put("origin" + index, $rootScope.nls["ERR8798"]);
                    return false;
                }

                if (enqDetail.fromPortMaster.status == "Block") {
                    enqDetail.fromPortMaster = null;
                    $scope.errorMap.put("origin" + index, $rootScope.nls["ERR8799"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 3) {
                if (enqDetail.toPortMaster == null || enqDetail.toPortMaster == undefined ||
                    enqDetail.toPortMaster.id == null ||
                    enqDetail.toPortMaster == undefined) {
                    $scope.errorMap.put("destination" + index, $rootScope.nls["ERR8793"]);
                    return false;
                }

                if (enqDetail.toPortMaster.status == "Hide") {
                    enqDetail.toPortMaster = null;
                    $scope.errorMap.put("destination" + index, $rootScope.nls["ERR8800"]);
                    return false;

                }
                if (enqDetail.toPortMaster.status == "Block") {
                    enqDetail.toPortMaster = null;
                    $scope.errorMap.put("destination" + index, $rootScope.nls["ERR8801"]);
                    return false;

                }

            }

            if (validateCode == 0 || validateCode == 4) {


                /*if(enqDetail.tosMaster == null || enqDetail.tosMaster == undefined
                        || enqDetail.tosMaster.id==null
                        || enqDetail.tosMaster.id==undefined) {
                    console.log("Tos is required..")
                    $scope.errorMap.put("tos",$rootScope.nls["ERR8810"]);
                    return false;
                }*/


                if (enqDetail.tosMaster != null && enqDetail.tosMaster.id != null && enqDetail.tosMaster.id != "") {

                    if (enqDetail.tosMaster.status == "Block") {
                        enqDetail.tosMaster = null;
                        $scope.errorMap.put("tos" + index, $rootScope.nls["ERR8802"]);
                        return false

                    }

                    if (enqDetail.tosMaster.status == "Hide") {
                        enqDetail.tosMaster = null;
                        $scope.errorMap.put("tos" + index, $rootScope.nls["ERR8805"]);
                        return false

                    }

                } else {
                    enqDetail.tosMaster = null;
                }
            }

            if (validateCode == 0 || validateCode == 5) {

                if (enqDetail.toPortMaster != undefined && enqDetail.toPortMaster != null && enqDetail.toPortMaster.portCode != null &&
                    enqDetail.fromPortMaster != undefined && enqDetail.fromPortMaster != null && enqDetail.fromPortMaster.portCode != null) {
                    if (enqDetail.toPortMaster.portCode == enqDetail.fromPortMaster.portCode) {
                        $scope.errorMap.put("tofromdifferent" + index, $rootScope.nls["ERR8803"]);
                        return false;
                    }
                }


            }




            if (validateCode == 0 || validateCode == 9) {

                if (enqDetail.commodityMaster != null && enqDetail.commodityMaster.id != null) {

                    if (enqDetail.commodityMaster.status == "Block" || enqDetail.commodityMaster.status == "Hide") {
                        $scope.errorMap.put("commodity" + index, $rootScope.nls["ERR8804"]);
                        enqDetail.commodityMaster = null;
                        return false;
                    }
                } else {
                    enqDetail.commodityMaster = null;
                }
            }

            if (validateCode == 0 || validateCode == 12) {
                if (enqDetail.pickupAddress != undefined && enqDetail.pickupAddress != null && enqDetail.pickupAddress.city != null && enqDetail.pickupAddress.city.status == 'Block') {
                    $scope.errorMap.put("pickupCity" + index, $rootScope.nls["ERR8845"]);
                    return false;
                } else if (enqDetail.pickupAddress != undefined && enqDetail.pickupAddress != null && enqDetail.pickupAddress.city != null && enqDetail.pickupAddress.city.status == 'Hide') {
                    $scope.errorMap.put("pickupCity" + index, $rootScope.nls["ERR8848"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 13) {
                if (enqDetail.pickupAddress != undefined && enqDetail.pickupAddress != null && enqDetail.pickupAddress.state != null && enqDetail.pickupAddress.state.status == 'Block') {
                    $scope.errorMap.put("pickupState" + index, $rootScope.nls["ERR8846"]);
                    return false;
                } else if (enqDetail.pickupAddress != undefined && enqDetail.pickupAddress != null && enqDetail.pickupAddress.state != null && enqDetail.pickupAddress.state.status == 'Hide') {
                    $scope.errorMap.put("pickupState" + index, $rootScope.nls["ERR8849"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 14) {
                if (enqDetail.pickupAddress != undefined && enqDetail.pickupAddress != null && enqDetail.pickupAddress.country != null && enqDetail.pickupAddress.country.status == 'Block') {
                    $scope.errorMap.put("pickupCountry" + index, $rootScope.nls["ERR8847"]);
                    return false;
                } else if (enqDetail.pickupAddress != undefined && enqDetail.pickupAddress != null && enqDetail.pickupAddress.country != null && enqDetail.pickupAddress.country.status == 'Hide') {
                    $scope.errorMap.put("pickupCountry" + index, $rootScope.nls["ERR8850"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 15) {
                if (enqDetail.deliveryAddress != undefined && enqDetail.deliveryAddress != null && enqDetail.deliveryAddress.city != null && enqDetail.deliveryAddress.city.status == 'Block') {
                    $scope.errorMap.put("deliveryCity" + index, $rootScope.nls["ERR8845"]);
                    return false;
                } else if (enqDetail.deliveryAddress != undefined && enqDetail.deliveryAddress != null && enqDetail.deliveryAddress.city != null && enqDetail.deliveryAddress.city.status == 'Hide') {
                    $scope.errorMap.put("deliveryCity" + index, $rootScope.nls["ERR8848"]);
                    return false;
                }

            }
            if (validateCode == 0 || validateCode == 16) {
                if (enqDetail.deliveryAddress != undefined && enqDetail.deliveryAddress != null && enqDetail.deliveryAddress.state != null && enqDetail.deliveryAddress.state.status == 'Block') {
                    $scope.errorMap.put("deliveryState" + index, $rootScope.nls["ERR8846"]);
                    return false;
                } else if (enqDetail.deliveryAddress != undefined && enqDetail.deliveryAddress != null && enqDetail.deliveryAddress.state != null && enqDetail.deliveryAddress.state.status == 'Hide') {
                    $scope.errorMap.put("deliveryState" + index, $rootScope.nls["ERR8849"]);
                    return false;
                }

            }
            if (validateCode == 0 || validateCode == 17) {
                if (enqDetail.deliveryAddress != undefined && enqDetail.deliveryAddress != null && enqDetail.deliveryAddress.country != null && enqDetail.deliveryAddress.country.status == 'Block') {
                    $scope.errorMap.put("deliveryCountry" + index, $rootScope.nls["ERR8847"]);
                    return false;
                } else if (enqDetail.deliveryAddress != undefined && enqDetail.deliveryAddress != null && enqDetail.deliveryAddress.country != null && enqDetail.deliveryAddress.country.status == 'Hide') {
                    $scope.errorMap.put("deliveryCountry" + index, $rootScope.nls["ERR8850"]);
                    return false;
                }

            }


            if ($scope.validateVolumeGross(0, enqDetail, index)) {
                return true;
            } else {
                return false;
            }



        };

        $scope.focusFirst = function(id) {
            if (id != undefined && id != null)
                $rootScope.navigateToNextField(id);
        }

        $scope.cancel = function() {
            if ($scope.enquiryForm != undefined && $scope.enquiryForm.$pristine) {
                if ($stateParams != undefined && $stateParams.bkState != undefined && $stateParams.bkState != null) {
                    if ($stateParams.bkParam != undefined && $stateParams.bkParam == "activiti") {
                        $state.go($stateParams.bkState, {
                            submitAction: 'Cancelled'
                        });
                    }
                } else {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.salesEnquiry", params);
                }
            } else {
                $scope.cancelContinue();
            }
        };


        $scope.cancelContinue = function() {

            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(
                function(value) {
                    if (value == 1 && $scope.enquiryLog.id == null) {
                        $scope.saveEnquiry();
                    } else if (value == 1 && $scope.enquiryLog.id != null) {
                        $scope.updateEnquiry();
                    } else {
                        if ($stateParams != undefined && $stateParams.bkState != undefined && $stateParams.bkState != null) {

                            if ($stateParams.bkParam != undefined && $stateParams.bkParam == "activiti") {
                                $state.go($stateParams.bkState, {
                                    submitAction: 'Cancelled'
                                });
                            }

                        } else {
                            var params = {};
                            params.submitAction = 'Cancelled';
                            $state.go("layout.salesEnquiry", params);
                        }
                    }
                });
        }


        $scope.showResponse = function(enquiryObj, isAdded) {
            $rootScope.mainpreloder = false;
            var newScope = $scope.$new();
            var enquiryNo = enquiryObj.enquiryNo;
            newScope.errorMessage = isAdded == true ? $rootScope.nls["ERR212"] : $rootScope.nls["ERR213"];
            if (newScope.errorMessage != undefined) {
                newScope.errorMessage = newScope.errorMessage.replace("%s", enquiryNo);
            }
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                var params = {};
                params.submitAction = 'Saved';
                $state.go("layout.salesEnquiry", params);
            }, function(value) {
                console.log("ok cancelled");
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go("layout.salesEnquiry", params);
            });

        }




        $scope.$on('addSalesEnquiryEventMenuNavi', function(events, moveToScreen) {
            //console.log("Scope enquiryLog data :: ", $scope.enquiryLog);
            //console.log("Scope State data :: ", $state);
            //console.log("Scope State $rootScope.naviGotoStateName:: ", $rootScope.naviGotoStateName);
            $scope.move(moveToScreen);

        })
        $scope.$on('editSalesEnquiryEventMenuNavi', function(events, moveToScreen) {
            //console.log("Scope enquiryLog data :: ", $scope.enquiryLog);
            //console.log("Scope State data :: ", $state);
            //console.log("Scope State $rootScope.naviGotoStateName:: ", $rootScope.naviGotoStateName);
            $scope.move(moveToScreen);

        })

        $scope.move = function(moveToScreen) {
            if ($scope.enquiryform.$dirty) {
                ngDialog.openConfirm({
                    template: '<p>Do you want to Save your changes?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.moveToScreen = moveToScreen;
                        if ($scope.enquiryLog.id != null) {
                            $scope.updateEnquiry();
                        } else {
                            $scope.saveEnquiry();
                        }
                    } else if (value == 2) {
                        $state.go(moveToScreen);
                    } else if (value == 3) {
                        //$state.go(args);
                        console.log("cancelled");
                    } else {
                        console.log("nothing selected")
                    }
                }, function(error) {
                    console.log('error occurred');
                });
            } else {
                console.log("No Dirty" + moveToScreen);
                $state.go(moveToScreen);
            }

        }

        $scope.saveEnquiry = function() {
            if (!$scope.disableSubmitBtn) {
                $scope.disableSubmitBtn = true;
                $rootScope.clientMessage = null;
                if ($scope.validateEnquiryLog(0)) {

                    for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {

                        if (!$scope.validateService(0, $scope.enquiryLog.enquiryDetailList[i], i)) {
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        /*if(!$scope.enquiryLog.enquiryDetailList[i].isAttachmentCheck()) {
                            $rootScope.clientMessage = $rootScope.nls['ERR250'];
                            console.log("Attachment is invalid");
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.Tabs = "Tab5";
                            $scope.enableSubmitBtnFn();
                            return false;
                        }*/

                        if (!$scope.enquiryLog.enquiryDetailList[i].isDimensionCheck()) {
                            console.log("Dimension Validation Failed");
                            $rootScope.clientMessage = $rootScope.nls["ERR8340"];
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.Tabs = "Tab1";
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if (!$scope.enquiryLog.enquiryDetailList[i].isRateRequestCheck()) {
                            /*Notification.error($rootScope.nls["ERR8832"]);*/
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.Tabs = "RateRequest";
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                    }
                    for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                        for (var j = 0; j < $scope.enquiryLog.enquiryDetailList.length; j++) {
                            if (i != j &&
                                $scope.enquiryLog.enquiryDetailList[i].service == $scope.enquiryLog.enquiryDetailList[j].service &&
                                $scope.enquiryLog.enquiryDetailList[i].fromPortMaster.id == $scope.enquiryLog.enquiryDetailList[j].fromPortMaster.id &&
                                $scope.enquiryLog.enquiryDetailList[i].toPortMaster.id == $scope.enquiryLog.enquiryDetailList[j].toPortMaster.id) {
                                $rootScope.clientMessage = $rootScope.nls['ERR8808'];
                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }
                    }


                    $scope.tmpObject = cloneService.clone($scope.enquiryLog);

                    $scope.tmpObject.status = "Active";

                    for (var i = 0; i < $scope.tmpObject.enquiryDetailList.length; i++) {
                        $scope.tmpObject.enquiryDetailList[i].hazardous = $scope.tmpObject.enquiryDetailList[i].hazardous ? 'Yes' : 'No';
                        $scope.tmpObject.enquiryDetailList[i].clearance = $scope.tmpObject.enquiryDetailList[i].clearance ? 'Yes' : 'No';
                    }

                    if ($scope.tmpObject.loggedOn != null) {
                        $scope.tmpObject.loggedOn = $rootScope.sendApiStartDateTime($scope.tmpObject.loggedOn);
                    }

                    if ($scope.tmpObject.receivedOn != null)
                        $scope.tmpObject.receivedOn = $rootScope.sendApiDateAndTime($scope.tmpObject.receivedOn);

                    if ($scope.tmpObject.quoteBy != null)
                        $scope.tmpObject.quoteBy = $rootScope.sendApiStartDateTime($scope.tmpObject.quoteBy);

                    $scope.tmpObject.locationMaster = $rootScope.userProfile.selectedUserLocation;
                    $scope.tmpObject.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
                    $scope.tmpObject.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;

                    for (var i = 0; i < $scope.tmpObject.enquiryDetailList.length; i++) {

                        if ($scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList != null && $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList.length > 0) {

                            $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedServiceList = [];

                            for (var j = 0; j < $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList.length; j++) {
                                if ($scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList[j].selected != undefined && $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList[j].selected) {
                                    $scope.enquiryValueAddedService = {};
                                    $scope.enquiryValueAddedService.valueAddedServices = $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList[j];
                                    $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedServiceList.push($scope.enquiryValueAddedService);
                                }
                            }
                        } else {
                            $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedServiceList = null;
                        }

                        //loop for rate request list  
                        if ($scope.tmpObject.enquiryDetailList[i].rateRequestList != undefined && $scope.tmpObject.enquiryDetailList[i].rateRequestList != null) {

                            for (var s = 0; s < $scope.tmpObject.enquiryDetailList[i].rateRequestList.length; s++) {

                                if ($scope.tmpObject.enquiryDetailList[i].rateRequestList[s] != undefined && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s] != null && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].responseDate != undefined) {

                                    $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].responseDate = $rootScope.sendApiDateAndTime($scope.tmpObject.enquiryDetailList[i].rateRequestList[s].responseDate);
                                }

                                if ($scope.tmpObject.enquiryDetailList[i].rateRequestList[s] != undefined && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList != undefined &&
                                    $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList != null && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.length > 0) {
                                    var rateChargeLength = $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.length;
                                    for (var k = 0; k < $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.length; k++) {
                                        if ($scope.isEmptyRow($scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList[k])) {
                                            if (rateChargeLength == 1 || rateChargeLength == 0) {
                                                $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList = null;
                                                break;
                                            } else {
                                                $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.splice(k, 1);
                                            }
                                        } else {


                                        }
                                    }
                                } else {
                                    $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList = null;
                                }

                            }
                        }
                    }
                    $rootScope.mainpreloder = true;

                    var enquiry = createEnquiryRequest($scope.tmpObject);

                    EnquiryAdd.save(enquiry).$promise.then(function(data) {
                            if (data.responseCode == "ERR0") {
                                console.log("Enquiry  added Successfully")
                                $scope.showResponse(data.responseObject, true);
                            } else {
                                console.log("added Failed " + data.responseDescription)
                                $rootScope.mainpreloder = false;
                            }
                            $scope.enableSubmitBtnFn();
                            $rootScope.mainpreloder = false;
                        },
                        function(error) {
                            console.log("added Failed : " + error)
                            $rootScope.mainpreloder = false;
                            $scope.enableSubmitBtnFn();
                        });
                } else {
                    console.log("validation failed");
                    $rootScope.mainpreloder = false;
                    $scope.enableSubmitBtnFn();
                }

            }

        };

        createEnquiryRequest = function(enquiry) {

            if (enquiry.partyMaster) {
                enquiry.partyMaster = {
                    "id": enquiry.partyMaster.id,
                    "versionLock": enquiry.partyMaster.versionLock
                };
            }
            if (enquiry.salesCoOrdinator) {
                enquiry.salesCoOrdinator = {
                    "id": enquiry.salesCoOrdinator.id,
                    "versionLock": enquiry.salesCoOrdinator.versionLock
                };

            }
            if (enquiry.salesman) {
                enquiry.salesman = {
                    "id": enquiry.salesman.id,
                    "versionLock": enquiry.salesman.versionLock
                };
            }
            if (enquiry.loggedBy) {
                enquiry.loggedBy = {
                    "id": enquiry.loggedBy.id,
                    "versionLock": enquiry.loggedBy.versionLock

                };
            }

            if (enquiry.locationMaster) {
                enquiry.locationMaster = {
                    "id": enquiry.locationMaster.id,
                    "versionLock": enquiry.locationMaster.versionLock
                };
            }

            if (enquiry.countryMaster) {
                enquiry.countryMaster = {
                    "id": enquiry.countryMaster.id,
                    "versionLock": enquiry.countryMaster.versionLock
                };
            }

            if (enquiry.companyMaster) {
                enquiry.companyMaster = {
                    "id": enquiry.companyMaster.id,
                    "versionLock": enquiry.companyMaster.versionLock
                };
            }


            if (enquiry.enquiryDetailList && enquiry.enquiryDetailList.length > 0) {
                for (var i = 0; i < enquiry.enquiryDetailList.length; i++) {

                    if (enquiry.enquiryDetailList[i].tosMaster) {
                        enquiry.enquiryDetailList[i].tosMaster = {
                            "id": enquiry.enquiryDetailList[i].tosMaster.id,
                            "versionLock": enquiry.enquiryDetailList[i].tosMaster.versionLock
                        };
                    }
                    if (enquiry.enquiryDetailList[i].fromPortMaster) {
                        enquiry.enquiryDetailList[i].fromPortMaster = {
                            "id": enquiry.enquiryDetailList[i].fromPortMaster.id,
                            "versionLock": enquiry.enquiryDetailList[i].fromPortMaster.versionLock
                        };
                    }
                    if (enquiry.enquiryDetailList[i].toPortMaster) {
                        enquiry.enquiryDetailList[i].toPortMaster = {
                            "id": enquiry.enquiryDetailList[i].toPortMaster.id,
                            "versionLock": enquiry.enquiryDetailList[i].toPortMaster.versionLock
                        };
                    }

                    if (enquiry.enquiryDetailList[i].commodityMaster) {
                        enquiry.enquiryDetailList[i].commodityMaster = {
                            "id": enquiry.enquiryDetailList[i].commodityMaster.id,
                            "versionLock": enquiry.enquiryDetailList[i].commodityMaster.versionLock
                        };
                    }


                    if (enquiry.enquiryDetailList[i].rateRequestList && enquiry.enquiryDetailList[i].rateRequestList.length > 0) {
                        for (var j = 0; j < enquiry.enquiryDetailList[i].rateRequestList.length; j++) {
                            if (enquiry.enquiryDetailList[i].rateRequestList[j].vendor) {
                                enquiry.enquiryDetailList[i].rateRequestList[j].vendor = {
                                    "id": enquiry.enquiryDetailList[i].rateRequestList[j].vendor.id,
                                    "versionLock": enquiry.enquiryDetailList[i].rateRequestList[j].vendor.versionLock
                                };
                            }
                        }

                    }


                }
            }




            return enquiry;
        }

        $scope.updateEnquiry = function() {
            if (!$scope.disableSubmitBtn) {
                $scope.disableSubmitBtn = true;
                if ($scope.validateEnquiryLog(0)) {

                    $rootScope.clientMessage = null;


                    for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                        if (!$scope.validateService(0, $scope.enquiryLog.enquiryDetailList[i], i)) {
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        // For Attachment Inline Table

                        /* if($scope.enquiryLog.enquiryDetailList[i].isAttachmentCheck() == false) {
                            $rootScope.clientMessage = $rootScope.nls['ERR250'];
                            console.log("Attachment is invalid");
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.Tabs = "Tab5";
                             $scope.enableSubmitBtnFn();
                             return false;
                         }*/


                        if ($scope.enquiryLog.enquiryDetailList[i].isDimensionCheck() == false) {
                            console.log("Dimension Validation Failed");
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.Tabs = "Tab1";
                            $scope.enableSubmitBtnFn();
                            $rootScope.clientMessage = $rootScope.nls["ERR8340"];
                            return false;

                        }

                        if (!$scope.enquiryLog.enquiryDetailList[i].isRateRequestCheck()) {
                            /* Notification.error($rootScope.nls["ERR8832"]);*/
                            $scope.enquiryLog.selectedTabIndex = i;
                            $scope.Tabs = "RateRequest";
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                    }

                    for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                        for (var j = 0; j < $scope.enquiryLog.enquiryDetailList.length; j++) {
                            if (i != j &&
                                $scope.enquiryLog.enquiryDetailList[i].serviceMaster.id == $scope.enquiryLog.enquiryDetailList[j].serviceMaster.id &&
                                $scope.enquiryLog.enquiryDetailList[i].fromPortMaster.id == $scope.enquiryLog.enquiryDetailList[j].fromPortMaster.id &&
                                $scope.enquiryLog.enquiryDetailList[i].toPortMaster.id == $scope.enquiryLog.enquiryDetailList[j].toPortMaster.id
                            ) {
                                $rootScope.clientMessage = $rootScope.nls['ERR8808'];
                                console.log("duplicate servie")
                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }

                    }

                    $scope.tmpObject = cloneService.clone($scope.enquiryLog);

                    for (var i = 0; i < $scope.tmpObject.enquiryDetailList.length; i++) {
                        $scope.tmpObject.enquiryDetailList[i].hazardous = $scope.tmpObject.enquiryDetailList[i].hazardous ? 'Yes' : 'No';
                        $scope.tmpObject.enquiryDetailList[i].clearance = $scope.tmpObject.enquiryDetailList[i].clearance ? 'Yes' : 'No';

                        //loop for rate request list  
                        if ($scope.tmpObject.enquiryDetailList[i].rateRequestList != undefined && $scope.tmpObject.enquiryDetailList[i].rateRequestList.length > 0 && $scope.tmpObject.enquiryDetailList[i].rateRequestList != null) {

                            for (var s = 0; s < $scope.tmpObject.enquiryDetailList[i].rateRequestList.length; s++) {

                                if ($scope.tmpObject.enquiryDetailList[i].rateRequestList[s] != undefined && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s] != null && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].responseDate != undefined) {

                                    $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].responseDate = $rootScope.sendApiDateAndTime($scope.tmpObject.enquiryDetailList[i].rateRequestList[s].responseDate);
                                }

                                if ($scope.tmpObject.enquiryDetailList[i].rateRequestList[s] != undefined && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList != undefined &&
                                    $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList != null && $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.length > 0) {

                                    var rateChargeLength = $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.length;
                                    for (var k = 0; k < $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.length; k++) {
                                        if ($scope.isEmptyRow($scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList[k])) {
                                            if (rateChargeLength == 1 || rateChargeLength == 0) {
                                                $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList = null;
                                                break;
                                            } else {
                                                $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList.splice(k, 1);
                                            }
                                        } else {


                                        }
                                    }
                                } else {
                                    $scope.tmpObject.enquiryDetailList[i].rateRequestList[s].chargeList = null;
                                }

                            }
                        }
                    }
                    if ($scope.tmpObject.enquiryDetailList.length > 0) {

                        if ($scope.tmpObject.loggedOn != null) {
                            $scope.tmpObject.loggedOn = $rootScope.sendApiStartDateTime($scope.tmpObject.loggedOn);
                        }

                        $scope.tmpObject.receivedOn = $rootScope.sendApiDateAndTime($scope.tmpObject.receivedOn);

                        if ($scope.tmpObject.quoteBy != null)
                            $scope.tmpObject.quoteBy = $rootScope.sendApiStartDateTime($scope.tmpObject.quoteBy);

                        if ($scope.tmpObject.locationMaster == null || $scope.tmpObject.locationMaster.id == null) {
                            $scope.tmpObject.locationMaster = $rootScope.userProfile.selectedUserLocation;
                        }
                        if ($scope.tmpObject.companyMaster == null || $scope.tmpObject.companyMaster.id == null) {
                            $scope.tmpObject.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
                        }
                        if ($scope.tmpObject.countryMaster == null || $scope.tmpObject.countryMaster.id == null) {
                            $scope.tmpObject.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
                        }

                        for (var i = 0; i < $scope.tmpObject.enquiryDetailList.length; i++) {

                            if ($scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList != null && $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList.length > 0) {

                                $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedServiceList = [];

                                for (var j = 0; j < $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList.length; j++) {
                                    if ($scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList[j].selected != undefined && $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList[j].selected) {
                                        $scope.enquiryValueAddedService = {};
                                        $scope.enquiryValueAddedService.valueAddedServices = $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedTempServiceList[j];
                                        $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedServiceList.push($scope.enquiryValueAddedService);
                                    }
                                }
                            } else {
                                $scope.tmpObject.enquiryDetailList[i].enquiryValueAddedServiceList = null;
                            }
                        }
                        $rootScope.mainpreloder = true;
                        var enquiry = createEnquiryRequest($scope.tmpObject);
                        EnquiryUpdate.update({ id: enquiry.id }, enquiry).$promise.then(
                            function(data) {
                                if (data.responseCode == "ERR0") {
                                    console.log("Enquiry  updated Successfully");
                                    $scope.showResponse(data.responseObject, false);
                                } else if (data.responseCode === 'ERR20') {
                                    $rootScope.showResponseForConcurrentException('Enquiry', $scope.tmpObject.id, $scope.tmpObject.enquiryNo, 'layout.salesEnquiry', 'layout.salesEnquiry', { submitAction: 'Updated' });
                                } else {
                                    console.log("added Failed " + data.responseDescription);
                                    $rootScope.mainpreloder = false;
                                    $scope.enableSubmitBtnFn();
                                }
                            },
                            function(error) {
                                console.log("added Failed : " + error);
                                $rootScope.mainpreloder = false;
                                $scope.enableSubmitBtnFn();
                            });
                    } else {
                        console.log("No Service Added");
                        $rootScope.mainpreloder = false;
                        $scope.enableSubmitBtnFn();
                    }

                } else {
                    console.log("Validation Failed");
                    $rootScope.mainpreloder = false;
                    $scope.enableSubmitBtnFn();
                }
                $scope.enableSubmitBtnFn();
            }
        };



        //for rate request charge    
        $scope.isEmptyRow = function(obj) {
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }

            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;

                }
            }
            return isempty;
        }




        $scope.addNewService = function() {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_SERVICE_CREATE)) {

                if ($scope.enquiryLog.enquiryDetailList.length > 0 ? $scope.validateEnquiryLog(0) : true) {

                    if ($scope.enquiryLog.enquiryDetailList == undefined || $scope.enquiryLog.enquiryDetailList.length == 0) {
                        $scope.enquiryLog.enquiryDetailList = [];
                    }

                    if ($scope.enquiryLog.enquiryDetailList != null && $scope.enquiryLog.enquiryDetailList.length > 0) {
                        for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                            if (!$scope.validateService(0, $scope.enquiryLog.enquiryDetailList[i], i)) {
                                $scope.enquiryLog.selectedTabIndex = i;
                                return false;
                            }
                        }
                    }

                    var enquiryDetail = enquiryService.getNewEnquiryDetail();
                    console.log("enquiryDetail " + enquiryDetail);
                    $scope.enquiryLog.enquiryDetailList.push(enquiryDetail);
                    $scope.enquiryLog.serviceAccessRights[$scope.enquiryLog.enquiryDetailList.length - 1] = {};
                    $scope.enquiryLog.serviceAccessRights[$scope.enquiryLog.enquiryDetailList.length - 1].ishavingRole = true;
                    $scope.enquiryLog.selectedTabIndex = $scope.enquiryLog.enquiryDetailList.length - 1;
                    $timeout(function() {
                        $scope.clickTabIndex(enquiryDetail, $scope.enquiryLog.selectedTabIndex, 'service');
                    }, 100);
                }

                if ($scope.enquiryForm != undefined) {
                    $scope.enquiryForm.$pristine = false;
                }

            }

        }




        $scope.deleteTabIndex = function(index) {
            if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_SERVICE_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR214"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).
                then(function(value) {
                    $scope.enquiryLog.enquiryDetailList.splice(index, 1);
                    $scope.enquiryLog.selectedTabIndex = $scope.enquiryLog.enquiryDetailList.length - 1;
                    $scope.clickTabIndex($scope.enquiryLog.enquiryDetailList[$scope.enquiryLog.enquiryDetailList.length - 1], $scope.enquiryLog.selectedTabIndex, 'service');
                }, function(value) {
                    console.log("Service Deletion cancelled");
                });
            }
        }
        $scope.employeeRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }
        }


        $scope.onEnquiryCustomerBlur = function() {


            if ($scope.enquiryLog.partyMaster != null && $scope.enquiryLog.partyMaster.id == undefined) {
                $scope.enqC = "";
                if ($scope.enquiryLog.partyMaster instanceof Object && $scope.enquiryLog.partyMaster.partyName != null && $scope.enquiryLog.partyMaster.partyName.trim().length > 0) {
                    $scope.enqC = $scope.enquiryLog.partyMaster.partyName;
                } else if (typeof $scope.enquiryLog.partyMaster === 'string') {
                    $scope.enqC = $scope.enquiryLog.partyMaster;
                }
                $scope.enquiryLog.partyMaster = {
                    partyName: $scope.enqC
                };
                if ($scope.enquiryLog.enquiryCustomer === undefined || $scope.enquiryLog.enquiryCustomer == null || $scope.enquiryLog.enquiryCustomer.name == undefined) {
                    $scope.enquiryLog.enquiryCustomer = {};
                }
                $scope.enquiryLog.enquiryCustomer.name = $scope.enqC;
                //$rootScope.navigateToNextField("iconUser");
            }

            // By pressing tab from customer, popup won't be shown
            /*if($scope.enquiryLog.partyMaster != null && $scope.enquiryLog.partyMaster.id == undefined 
                    && $scope.enquiryLog.partyMaster.partyName != null 
                    && $scope.enquiryLog.partyMaster.partyName.trim().length>0) {
                    $scope.showCustomerModal();
            }*/
            if ($scope.enquiryLog.partyMaster == undefined || $scope.enquiryLog.partyMaster == null || $scope.enquiryLog.partyMaster.partyName == "") {
                $scope.enquiryLog.salesman = null;
            }

        }
        $scope.selectedParty = function(value) {
            $scope.enquiryLog.enquiryCustomer = null;
            if ($scope.validateEnquiryLog(1)) {
                $scope.enquiryLog.salesman = null;
                // Select the salesman mapped in the party with service 00
                if ($scope.enquiryLog.partyMaster.partyServiceList != null && $scope.enquiryLog.partyMaster.partyServiceList.length != undefined && $scope.enquiryLog.partyMaster.partyServiceList.length > 0) {
                    for (var i = 0; i < $scope.enquiryLog.partyMaster.partyServiceList.length; i++) {
                        if ($scope.enquiryLog.partyMaster.partyServiceList[i].serviceMaster.serviceCode == $rootScope.appMasterData['default.party.salesman.service.code']) {
                            $scope.enquiryLog.salesman = $scope.enquiryLog.partyMaster.partyServiceList[i].salesman;
                            break;
                        }
                    }
                }
                if ($scope.enquiryLog.quoteBy == undefined || $scope.enquiryLog.quoteBy == null || $scope.enquiryLog.quoteBy == "") {
                    $scope.enquiryLog.quoteBy = $rootScope.dateToString(new Date());
                }
                $scope.navigateToNextField(value);
            } else {
                $scope.enquiryLog.salesman = null;
            }
        };

        $scope.routePartyEvent = function(fromObjModel) {
            $rootScope.nav_src_bkref_key = new Date().toISOString();
            $state.go("layout.addParty", {
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                forObj: fromObjModel
            });
        }



        $scope.ajaxSalesCoordinatorEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.searchDto.userLocation = $rootScope.userProfile.selectedUserLocation.countryMaster;
            $scope.searchDto.isSalesman = false;
            return EmployeeListOnLocation.fetch({
                "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.salesCordinatorList = data.responseObject.searchResult;
                        return $scope.salesCordinatorList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Emplyoees');
                }
            );

        }



        $scope.selectedSalesCordinator = function(value) {
            if ($scope.validateEnquiryLog(5)) {
                $scope.navigateToNextField(value);
            }
        };




        $scope.selectedSalesEmplyoee = function(value) {
            if ($scope.validateEnquiryLog(6)) {
                $scope.navigateToNextField(value);
            }
        };




        $scope.ajaxServiceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return ServiceList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.serviceList = data.responseObject.searchResult;
                        return $scope.serviceList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching  Services');
                }
            );

        }



        $scope.selectedService = function(value, enqDetail, index) {
            if ($scope.validateService(1, enqDetail, index)) {
                $scope.navigateToNextField(value, index);
            }
        };




        $scope.ajaxPortDestinationEvent = function(keyword, enqDetail, index) {

            var transportMode = null;
            var id = null;

            if (enqDetail != undefined && enqDetail.service != undefined && enqDetail.service != null) {
                if (enqDetail.service == 'Air') {
                    transportMode = enqDetail.service;
                } else {
                    transportMode = 'Ocean';
                }
            }

            if (enqDetail.rateRequestList.length > 0 && enqDetail.rateRequestList[0].isMailSent && !$scope.isRRPopupShown) {
                $scope.openRateRequestPopUp();
                return;
            }
            return PortService.getPortListByTransportMode(keyword, transportMode);


        }


        $scope.selectedToPort = function(value, enqDetail, index) {
            if ($scope.validateService(3, enqDetail, index)) {
                if ($scope.validateService(5, enqDetail, index)) {
                    $scope.navigateToNextField(value, index);
                    fetchDefaultAgent(enqDetail);
                }
            }
        };



        $scope.ajaxPortOriginEvent = function(keyword, enqDetail, index) {

            var transportMode = null;

            if (enqDetail != undefined && enqDetail.service != undefined && enqDetail.service != null) {
                if (enqDetail.service == 'Air') {
                    transportMode = enqDetail.service;
                } else {
                    transportMode = 'Ocean';
                }
            }

            if (enqDetail.rateRequestList.length > 0 && enqDetail.rateRequestList[0].isMailSent && !$scope.isRRPopupShown) {
                $scope.openRateRequestPopUp();
                return;
            }

            return PortService.getPortListByTransportMode(keyword, transportMode);

        }


        $scope.selectedFromPort = function(value, enqDetail, index) {
            if ($scope.validateService(2, enqDetail, index)) {
                if ($scope.validateService(5, enqDetail, index)) {
                    $scope.navigateToNextField(value, index);
                    fetchDefaultAgent(enqDetail);
                }
            }
        };

        var fetchDefaultAgent = function (enqDetail, index) {
            var portCode;
            if (enqDetail.fromPortMaster) {
                portCode = enqDetail.fromPortMaster.portCode;
            } else {
                return;
            }
            if (enqDetail.importExport === "Export") {
                if (enqDetail.toPortMaster) {
                    portCode = enqDetail.toPortMaster.portCode;
                } else {
                    return;
                }
            }
            console.log('portCode ', portCode);
            $scope.defaultAgent = null;
            if (portCode) {
                AgentByPort.fetch({
                    "portCode": portCode
                }).$promise.then(
                    function (data) {
                        if (data.responseCode == "ERR0") {
                            var agentExport = data.responseObject;
                            if (agentExport != undefined &&
                                agentExport != null &&
                                agentExport.id != undefined) {

                                if (typeof (agentExport) === "object") {
                                    $scope.defaultAgent = agentExport;
                                }
                                //$scope.getPartyAddress(document.agent, document.agentAddress);
                                setDefaultAgent(enqDetail);
                            }
                        }
                    },
                    function (errResponse) {
                        console.error('Error while fetching agent  based on service port');
                    }

                );
            }
        }

        var setDefaultAgent = function (enqDetail, index) {
            var objRateRequest = {};
            if (enqDetail.rateRequestList == undefined || enqDetail.rateRequestList == null || enqDetail.rateRequestList.length == 0) {
                if ($scope.defaultAgent != undefined && $scope.defaultAgent != null && $scope.defaultAgent.partyAddressList != null && $scope.defaultAgent.partyAddressList.length > 0) {
                    objRateRequest.vendor = $scope.defaultAgent;
                    $scope.rateRequestList = [];
                    $scope.rateRequestList.push(objRateRequest);
                    angular.forEach($scope.defaultAgent.partyAddressList, function (data, index) {
                        if (data.addressType == 'Primary') {
                            if (data.contactPerson != undefined && data.contactPerson != null) {
                                objRateRequest.contactPerson = data.contactPerson;
                            }
                            if (data.email != undefined && data.email != null) {
                                objRateRequest.emailId = data.email;
                            }
                            objRateRequest.rateAccepted = 'No';
                            return
                        }
                    });
                } else {
                    $scope.rateRequestList.push({});
                }
            } else if (enqDetail.rateRequestList.length == 1) {
                objRateRequest = {};
                if ($scope.defaultAgent && $scope.defaultAgent.partyAddressList != null && $scope.defaultAgent.partyAddressList.length > 0) {
                    objRateRequest.vendor = $scope.defaultAgent;
                    if (!enqDetail.rateRequestList[0].vendor) {
                        enqDetail.rateRequestList[0] = objRateRequest;
                    }
                    angular.forEach($scope.defaultAgent.partyAddressList, function (data, index) {
                        if (data.addressType == 'Primary') {
                            if (data.contactPerson != undefined && data.contactPerson != null) {
                                objRateRequest.contactPerson = data.contactPerson;
                            }
                            if (data.email != undefined && data.email != null) {
                                objRateRequest.emailId = data.email;
                            }
                            objRateRequest.rateAccepted = 'No';
                            return
                        }
                    });
                }
            }
        }


        $scope.selectedTos = function(value, enqDetail, index) {
            if ($scope.validateService(4, enqDetail, index)) {
                $scope.navigateToNextField(value, index);
            }
        };




        $scope.ajaxCommodityEvent = function(keyword) {
            return CommodityService.getCommodityList(keyword);
        }


        $scope.selectedCommodity = function(value, enqDetail, index, event) {

            if ($scope.validateService(9, enqDetail, index)) {
                if ($scope.isDimensionFound(enqDetail)) {
                    $scope.openDimensionTab(index, event);
                } else {
                    $scope.navigateToNextField(value, index);
                }
            }
        }

        $scope.openTab = function(index, event) {

            if (!event.shiftKey && event.keyCode == 9) {
                $scope.dimensionTab = true;
                $rootScope.navigateToNextField('EnqSer' + index + 'noOfPiece0', 'dimension');
            } else {
                $rootScope.navigateToNextField('grossweightt' + index);
            }
        }

        $scope.openDimensionTab = function(index, event) {

            $scope.dimensionTab = true;
            $rootScope.navigateToNextField('EnqSer' + index + 'noOfPiece0', 'dimension');

        }

        $scope.changeDimensionDef = function(index, event) {

            if ($scope.dimensionTab) {
                $scope.dimensionTab = false;
            } else {
                $scope.openDimensionTab(index, event);
                $scope.dimensionTab = true;
            }
        }

        $scope.serialNo = function(index) {
            var index = index + 1;
            return index;
        }

        //Validation for enquiry log,enquiry service and enquiry dimension

        $scope.validateEnquiryLog = function(validateCode) {

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                if ($scope.enquiryLog.id != null && $scope.enquiryLog.partyCreated == true) {

                    if ($scope.enquiryLog.partyMaster != undefined &&
                        $scope.enquiryLog.partyMaster != null &&
                        $scope.enquiryLog.partyMaster != "" &&
                        $scope.enquiryLog.partyMaster.id != undefined) {

                        if ($scope.enquiryLog.partyMaster.status == "Block") {
                            $scope.errorMap.put("partyMaster", $rootScope.nls["ERR7016"]);
                            $scope.enquiryLog.partyMaster = null;
                            return false
                        }
                    } else {
                        $scope.errorMap.put("partyMaster", $rootScope.nls["ERR7003"]);
                        $scope.enquiryLog.partyMaster = null;
                        return false
                    }

                } else if ($scope.enquiryLog.id != null && $scope.enquiryLog.partyCreated == false) {
                    if ($scope.enquiryLog.enquiryCustomer == null ||
                        $scope.enquiryLog.enquiryCustomer.name == undefined || $scope.enquiryLog.enquiryCustomer.name == null ||
                        $scope.enquiryLog.enquiryCustomer.name.trim().length == 0) {
                        $scope.errorMap.put("partyMaster", $rootScope.nls["ERR7003"]);
                        $scope.enquiryLog.partyMaster = null;
                        return false
                    } else {
                        console.log("Validations of Enquiry Customer Starts......")
                        if ($scope.validateEnquiryCustomer(0)) {

                        } else {
                            $scope.errorMap.put("partyMaster", $rootScope.nls["ERR6097"]);
                            return false
                        }
                        console.log("Validations of Enquiry Customer Ends......")
                    }
                } else {


                    if ($scope.enquiryLog.partyMaster != undefined &&
                        $scope.enquiryLog.partyMaster != null &&
                        $scope.enquiryLog.partyMaster != "" &&
                        $scope.enquiryLog.partyMaster.id != undefined) {

                        $scope.enquiryLog.enquiryCustomer = null;
                        if ($scope.enquiryLog.partyMaster.status == "Block") {
                            $scope.errorMap.put("partyMaster", $rootScope.nls["ERR7016"]);
                            $scope.enquiryLog.partyMaster = null;
                            return false
                        }
                    } else {

                        if ($scope.enquiryLog.enquiryCustomer == null ||
                            $scope.enquiryLog.enquiryCustomer.name == undefined || $scope.enquiryLog.enquiryCustomer.name == null ||
                            $scope.enquiryLog.enquiryCustomer.name.trim().length == 0) {
                            $scope.errorMap.put("partyMaster", $rootScope.nls["ERR7003"]);
                            $scope.enquiryLog.partyMaster = null;
                            return false
                        } else {
                            console.log("Validations of Enquiry Customer Starts......")
                            if ($scope.validateEnquiryCustomer(0)) {

                            } else {
                                Notification.error($rootScope.nls["ERR6097"]);
                                return false
                            }
                            console.log("Validations of Enquiry Customer Ends......")
                        }
                    }


                }

            }


            if (validateCode == 0 || validateCode == 5) {
                if ($scope.enquiryLog.salesCoOrdinator == undefined ||
                    $scope.enquiryLog.salesCoOrdinator == null ||
                    $scope.enquiryLog.salesCoOrdinator.id == undefined ||
                    $scope.enquiryLog.salesCoOrdinator.id == "" ||
                    $scope.enquiryLog.salesCoOrdinator.id == null) {
                    $scope.errorMap.put("salesCoOrdinator", $rootScope.nls["ERR7009"]);
                    return false;
                } else if ($scope.enquiryLog.salesCoOrdinator.status != null) {
                    if ($scope.enquiryLog.salesCoOrdinator.status == "Block") {
                        console.log($rootScope.nls["ERR7009"]);
                        $scope.errorMap.put("salesCoOrdinator", $rootScope.nls["ERR7020"]);
                        return false

                    }
                }

            }

            if (validateCode == 0 || validateCode == 6) {

                if ($scope.enquiryLog.salesman != null &&
                    $scope.enquiryLog.salesman != "" &&
                    $scope.enquiryLog.salesman != undefined &&
                    $scope.enquiryLog.salesman.id != null) {
                    if ($scope.enquiryLog.salesman.status != null) {
                        if ($scope.enquiryLog.salesman.status == "Block") {
                            $scope.enquiryLog.salesman = null;
                            $scope.errorMap.put("salesman", $rootScope.nls["ERR7021"]);
                            return false

                        }

                    }
                } else {
                    $scope.enquiryLog.salesman = null;
                }
            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.enquiryLog.loggedOn == null && $scope.enquiryLog.loggedOn == undefined) {
                    $scope.errorMap.put("loggedOn", $rootScope.nls["ERR7017"]);
                    console.log($rootScope.nls["ERR7017"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 4) {
                if ($scope.enquiryLog.receivedOn == undefined || $scope.enquiryLog.receivedOn == null) {
                    console.log("received on DATE", $scope.enquiryLog.receivedOn);
                    console.log($rootScope.nls["ERR7006"]);
                    $scope.errorMap.put("receivedOn", $rootScope.nls["ERR7006"]);
                    return false;
                }



                if (validateCode == 0 || validateCode == 3) {}


                if ($scope.enquiryLog.receivedOn != undefined &&
                    $scope.enquiryLog.receivedOn != null &&
                    $scope.enquiryLog.loggedOn != undefined &&
                    $scope.enquiryLog.loggedOn != null) {
                    var receivedOn = $rootScope.convertToDate($scope.enquiryLog.receivedOn);
                    var loggedOn = $rootScope.convertToDate($scope.enquiryLog.loggedOn);

                    if (receivedOn > loggedOn) {
                        $scope.errorMap.put("receivedOn", $rootScope.nls["ERR8806"]);
                        return false;
                    }
                }


                if ($scope.enquiryLog.receivedOn != undefined &&
                    $scope.enquiryLog.receivedOn != null &&
                    $scope.enquiryLog.quoteBy != undefined &&
                    $scope.enquiryLog.quoteBy != null) {



                    var receivedOn = $rootScope.convertToDate($scope.enquiryLog.receivedOn);
                    var loggedOn = $rootScope.convertToDate($scope.enquiryLog.quoteBy);

                    if (receivedOn > loggedOn) {
                        $scope.errorMap.put("quoteBy", $rootScope.nls["ERR8812"]);
                        return false;
                    }
                }

            }




            $scope.errorMap = new Map();
            return true;

        };


        $scope.validateEnquiryCustomer = function(validateCode) {
            $scope.errorMap = new Map();



            /*if($scope.enquiryLog.enquiryCustomer == null) {
                $scope.errorMap.put("partyName",$rootScope.nls["ERR3507"]);
                return false;
            }*/

            /*if (validateCode == 0 || validateCode == 8) {
                if ($rootScope.appMasterData['party.name.special.char.allowed'].toUpperCase() == "FALSE") {
                    if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_NAME,$scope.enquiryLog.enquiryCustomer.name)){
                        $scope.errorMap.put("partyName",$rootScope.nls["ERR3507"]);
                        return false;
                    }
                }
            }*/

            if (validateCode == 0 || validateCode == 1) {

                if ($scope.enquiryLog.enquiryCustomer.countryMaster == undefined ||
                    $scope.enquiryLog.enquiryCustomer.countryMaster == null ||
                    $scope.enquiryLog.enquiryCustomer.countryMaster.id == undefined ||
                    $scope.enquiryLog.enquiryCustomer.countryMaster.id == null ||
                    $scope.enquiryLog.enquiryCustomer.countryMaster.id == "") {

                    console.log("Country is Mandatory..........")
                    $scope.errorMap.put("enquiryCustomerCountry", $rootScope.nls["ERR36"]);
                    return false;

                } else {

                    if ($scope.enquiryLog.enquiryCustomer.countryMaster.status == 'Block') {
                        console.log("Selected Country is Blocked.");
                        $scope.enquiryLog.enquiryCustomer.countryMaster = null;
                        $scope.errorMap.put("enquiryCustomerCountry", $rootScope.nls["ERR3522"]);
                        return false;
                    }

                    if ($scope.enquiryLog.enquiryCustomer.countryMaster.status == 'Hide') {
                        console.log("Selected Country is Hidden.");
                        $scope.enquiryLog.enquiryCustomer.countryMaster = null;
                        $scope.errorMap.put("enquiryCustomerCountry", $rootScope.nls["ERR3523"]);
                        return false;
                    }
                    console.log("Country Selected.........." + $scope.enquiryLog.enquiryCustomer.countryMaster.countryCode)
                }
            }

            if (validateCode == 0 || validateCode == 2) {

                if ($scope.enquiryLog.enquiryCustomer.addressLine1 == undefined || $scope.enquiryLog.enquiryCustomer.addressLine1 == null ||
                    $scope.enquiryLog.enquiryCustomer.addressLine1.trim().length == 0) {
                    $scope.errorMap.put("enquiryCustomerAddrLine1", $rootScope.nls["ERR3701"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 3) {

                if ($scope.enquiryLog.enquiryCustomer.cityMaster == undefined ||
                    $scope.enquiryLog.enquiryCustomer.cityMaster == null ||
                    $scope.enquiryLog.enquiryCustomer.cityMaster.id == undefined ||
                    $scope.enquiryLog.enquiryCustomer.cityMaster.id == null ||
                    $scope.enquiryLog.enquiryCustomer.cityMaster.id == "") {

                    $scope.enquiryLog.enquiryCustomer.cityMaster = null;
                    $scope.errorMap.put("enquiryCustomerCity", $rootScope.nls["ERR3717"]);
                    return false;
                } else {
                    if ($scope.enquiryLog.enquiryCustomer.cityMaster.status == 'Block') {
                        console.log("Selected City is Blocked.");
                        $scope.enquiryLog.enquiryCustomer.cityMaster = null;
                        $scope.errorMap.put("enquiryCustomerCity", $rootScope.nls["ERR3719"]);
                        return false;
                    }

                    if ($scope.enquiryLog.enquiryCustomer.cityMaster.status == 'Hide') {
                        console.log("Selected City is Hidden.");
                        $scope.enquiryLog.enquiryCustomer.cityMaster = null;
                        $scope.errorMap.put("enquiryCustomerCity", $rootScope.nls["ERR3718"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 4) {

                if ($scope.enquiryLog.enquiryCustomer.stateMaster == undefined ||
                    $scope.enquiryLog.enquiryCustomer.stateMaster == null ||
                    $scope.enquiryLog.enquiryCustomer.stateMaster.id == undefined ||
                    $scope.enquiryLog.enquiryCustomer.stateMaster.id == null ||
                    $scope.enquiryLog.enquiryCustomer.stateMaster.id == "") {

                    $scope.enquiryLog.enquiryCustomer.stateMaster = null;
                    $scope.errorMap.put("enquiryCustomerState", $rootScope.nls["ERR3706"]);
                    return false;
                } else {
                    if ($scope.enquiryLog.enquiryCustomer.stateMaster.status == 'Block') {
                        console.log("Selected state is Blocked.");
                        $scope.enquiryLog.enquiryCustomer.stateMaster = null;
                        $scope.errorMap.put("enquiryCustomerState", $rootScope.nls["ERR3707"]);
                        return false;
                    }

                    if ($scope.enquiryLog.enquiryCustomer.stateMaster.status == 'Hide') {
                        console.log("Selected state is Hidden.");
                        $scope.enquiryLog.enquiryCustomer.stateMaster = null;
                        $scope.errorMap.put("enquiryCustomerState", $rootScope.nls["ERR3708"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 5) {
                if ($scope.enquiryLog.enquiryCustomer.email != null && $scope.enquiryLog.enquiryCustomer.email.length != 0) {
                    if (!CommonValidationService.checkMultipleMail($scope.enquiryLog.enquiryCustomer.email)) {
                        $scope.errorMap.put("enquiryCustomerEmail", $rootScope.nls["ERR3714"]);
                        return false;
                    }
                }
            }




            return true;

        }


        $scope.validateVolumeGross = function(validateCode, enqDetail, index) {

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                if (enqDetail.grossWeight != null && enqDetail.grossWeight != undefined && enqDetail.grossWeight != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT, enqDetail.grossWeight)) {
                        $scope.errorMap.put("grossweightt" + index, $rootScope.nls["ERR8360"]);
                        return false;
                    }

                    if (enqDetail.grossWeight < 0 || enqDetail.grossWeight > 99999999999.99) {
                        $scope.errorMap.put("grossweightt" + index, $rootScope.nls["ERR8360"]);
                        return false;
                    }
                }

            }


            if (validateCode == 0 || validateCode == 2) {
                if (enqDetail.volWeight != null && enqDetail.volWeight != undefined && enqDetail.volWeight != "") {

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT, enqDetail.volWeight)) {
                        $scope.errorMap.put("volumeweightt" + index, $rootScope.nls["ERR8364"]);
                        return false;
                    }

                    if (enqDetail.volWeight < 0 || enqDetail.volWeight >= 99999999999.99) {
                        $scope.errorMap.put("volumeweightt" + index, $rootScope.nls["ERR8364"]);
                        return false;
                    }
                }

            }

            return true;

        }


        $scope.singlePickerOpts = {
            singleDatePicker: true,
            eventHandlers: {
                'show.daterangepicker': function(ev, picker) {
                    $scope.niceObject.hide()
                },
                'hide.daterangepicker': function(ev, picker) {
                    $scope.niceObject.show()
                }

            }
        };


        $scope.calculateWeight = function(param, enqDetail) {
            if (param.removeFlag) {

                if (enqDetail.dimensionUnit) { //Pound to Kg
                    enqDetail.volWeight = Math.round(param.volumeWeight * 100) / 100;
                    enqDetail.grossWeight = Math.round((param.grossWeight * 0.45359237) * 100) / 100;
                } else {
                    enqDetail.volWeight = param.volumeWeight;
                    enqDetail.grossWeight = param.grossWeight;
                }
                enqDetail.dimensionVolWeight = param.volumeWeight;
                enqDetail.dimensionGrossWeight = param.grossWeight;
                enqDetail.dimensionGrossWeightInKg = param.dimensionGrossWeightInKg;
                enqDetail.dimensionTotalPieces = param.totalPieces;
            }
            if (param.volumeWeight >= 0 && param.grossWeight >= 0 && param.totalPieces >= 0 && !param.removeFlag) {
                enqDetail.dimensionVolWeight = param.volumeWeight;
                enqDetail.dimensionGrossWeight = param.grossWeight;
                enqDetail.dimensionTotalPieces = param.totalPieces;
                enqDetail.dimensionGrossWeightInKg = param.dimensionGrossWeightInKg;

                if (enqDetail.dimensionUnit) { //Pound to Kg
                    enqDetail.volWeight = Math.round(param.volumeWeight * 100) / 100;
                    enqDetail.grossWeight = Math.round((param.grossWeight * 0.45359237) * 100) / 100;
                } else {
                    enqDetail.volWeight = param.volumeWeight;
                    enqDetail.grossWeight = param.grossWeight;
                }

            }

            if (enqDetail.volWeight >= 0) {
                enqDetail.volWeight = $rootScope.roundOffWeight(enqDetail.volWeight);
            }

            if (enqDetail.grossWeight >= 0) {
                enqDetail.grossWeight = $rootScope.roundOffWeight(enqDetail.grossWeight);
            }


        }

        $scope.navigateToEditParty = function(fromObjModel) {
            if ($scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null && $scope.enquiryLog.partyMaster != "" && $scope.enquiryLog.partyMaster.id != undefined) {
                $rootScope.nav_src_bkref_key = new Date().toISOString();
                var stateRouteParam = {
                    partyId: $scope.enquiryLog.partyMaster.id,
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    forObj: fromObjModel
                };
                $state.go("layout.editParty", stateRouteParam);
            } else {
                console.log("Party not selected...");
            }
        }
        $scope.viewEnquiryDetailPage = function(id) {
            console.log("View Enquiry Detail page of " + id);
            $rootScope.nav_src_bkref_key = new Date().toISOString();

            $state.go("layout.viewSalesEnquiry", {
                enquiryId: id,
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                bkState: $state.current.name,
                bkParam: JSON.stringify($stateParams),
                showActionButton: 1
            });
        }

        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }
        }

        $scope.countryRender = function(item) {
            return {
                label: item.countryName,
                item: item
            }
        }
        $scope.stateRender = function(item) {
            return {
                label: item.stateName,
                item: item
            }
        }
        $scope.cityRender = function(item) {
            return {
                label: item.cityName,
                item: item
            }
        }
        $scope.employeeRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }
        }
        $scope.portRender = function(item) {
            return {
                label: item.portName,
                item: item
            }
        }
        $scope.serviceRender = function(item) {
            return {
                label: item.serviceName,
                item: item
            }
        }
        $scope.tosRender = function(item) {
            return {
                label: item.tosName,
                item: item
            }
        }
        $scope.commodityRender = function(item) {
            return {
                label: item.hsName,
                item: item
            }
        }



        /* Pickup Delivery */

        $scope.pickUpDeliveryValidation = function(code, enquiryDetail, index) {
            var flag = false;
            flag = $scope.validateService(code, enquiryDetail, index);
            return flag
        }

        $scope.selectedPickupCity = function(enquiryDetail, nextField, index) {

            if ($scope.pickUpDeliveryValidation(12, enquiryDetail, index)) {
                if (enquiryDetail.pickupAddress.city != null && enquiryDetail.pickupAddress.city.status == 'Active') {
                    enquiryDetail.pickupAddress.state = enquiryDetail.pickupAddress.city.stateMaster;
                    enquiryDetail.pickupAddress.country = enquiryDetail.pickupAddress.city.countryMaster;
                }
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedPickupState = function(enquiryDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(13, enquiryDetail, index)) {
                if (enquiryDetail.pickupAddress.state != null && enquiryDetail.pickupAddress.state.status == 'Active') {
                    enquiryDetail.pickupAddress.country = enquiryDetail.pickupAddress.state.countryMaster;
                }

                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedPickupCountry = function(enquiryDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(14, enquiryDetail, index)) {
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedDeliveryCity = function(enquiryDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(15, enquiryDetail, index)) {
                if (enquiryDetail.deliveryAddress.city != null && enquiryDetail.deliveryAddress.city.status == 'Active') {
                    enquiryDetail.deliveryAddress.state = enquiryDetail.deliveryAddress.city.stateMaster;
                    enquiryDetail.deliveryAddress.country = enquiryDetail.deliveryAddress.city.countryMaster;
                }
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedDeliveryState = function(enquiryDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(16, enquiryDetail, index)) {
                if (enquiryDetail.deliveryAddress.state != null && enquiryDetail.deliveryAddress.state.status == 'Active') {
                    enquiryDetail.deliveryAddress.country = enquiryDetail.deliveryAddress.state.countryMaster;
                }
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedDeliveryCountry = function(enquiryDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(17, enquiryDetail, index)) {
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.showPartyAddress = false;
        $scope.whichColumn = null;
        $scope.partyAddressConfig = {
            search: false,
            address: true
        }

        $scope.goToPickup = function(enqDetail) {
            $scope.panelTitle = "Pick up Address";
            $scope.partyAddressCustomer = [];
            if ($scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null && $scope.enquiryLog.partyMaster.partyAddressList != undefined && $scope.enquiryLog.partyMaster.partyAddressList != null) {
                for (var i = 0; i < $scope.enquiryLog.partyMaster.partyAddressList.length; i++) {
                    $scope.partyAddressCustomer.push($scope.enquiryLog.partyMaster.partyAddressList[i]);
                }
            }
            $scope.showPartyAddress = true;
            $scope.selectedItem = -1;
            $scope.whichColumn = "PICKUP";
            $scope.enquDetail = enqDetail;
        }

        $scope.selectedPartyAddress = function(obj) {

            if ($scope.whichColumn == "PICKUP") {
                if ($scope.enquDetail.pickupAddress == undefined || $scope.enquDetail.pickupAddress == null) {
                    $scope.enquDetail.pickupAddress = {};
                }

                $scope.enquDetail.pickupAddress.addressLine1 = obj.addressLine1;
                $scope.enquDetail.pickupAddress.addressLine2 = obj.addressLine2;
                $scope.enquDetail.pickupAddress.addressLine3 = obj.addressLine3;
                $scope.enquDetail.pickupAddress.city = obj.cityMaster;
                $scope.enquDetail.pickupAddress.poBox = obj.zipCode;
                $scope.enquDetail.pickupAddress.state = obj.stateMaster;
                if (obj.stateMaster != null && obj.stateMaster != undefined && obj.stateMaster.id != undefined) {
                    $scope.enquDetail.pickupAddress.country = obj.stateMaster.countryMaster;
                } else {
                    $scope.enquDetail.pickupAddress.country = null;
                }
                $rootScope.navigateToNextField('deliveryAddress.addressLine1' + $scope.enquiryLog.selectedTabIndex);
            } else if ($scope.whichColumn == "DELIVERY") {
                if ($scope.enquDetail.deliveryAddress == undefined || $scope.enquDetail.deliveryAddress == null) {
                    $scope.enquDetail.deliveryAddress = {};
                }

                $scope.enquDetail.deliveryAddress.addressLine1 = obj.addressLine1;
                $scope.enquDetail.deliveryAddress.addressLine2 = obj.addressLine2;
                $scope.enquDetail.deliveryAddress.addressLine3 = obj.addressLine3;
                $scope.enquDetail.deliveryAddress.city = obj.cityMaster;
                $scope.enquDetail.deliveryAddress.poBox = obj.zipCode;
                $scope.enquDetail.deliveryAddress.state = obj.stateMaster;
                if (obj.stateMaster != null && obj.stateMaster != undefined && obj.stateMaster.id != undefined) {
                    $scope.enquDetail.deliveryAddress.country = obj.stateMaster.countryMaster;
                } else {
                    $scope.enquDetail.deliveryAddress.country = null;
                }
                $rootScope.navigateToNextField('partyName');
            }

            $scope.showPartyAddress = false;
            $scope.whichColumn = null;
            if ($scope.enquiryForm != undefined) {
                $scope.enquiryForm.$pristine = false;
            }

        };

        $scope.cancelPartyAddress = function() {
            if ($scope.whichColumn == "PICKUP") {
                $rootScope.navigateToNextField('pickup' + $scope.enquiryLog.selectedTabIndex);
            } else if ($scope.whichColumn == "DELIVERY") {
                $rootScope.navigateToNextField('delivery' + $scope.enquiryLog.selectedTabIndex);
            }
            $scope.showPartyAddress = false;
            $scope.whichColumn = null;
        }

        $scope.goToDelivery = function(enqDetail) {
            $scope.panelTitle = "Delivery Address";
            $scope.partyAddressCustomer = [];
            if ($scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null && $scope.enquiryLog.partyMaster.partyAddressList != undefined && $scope.enquiryLog.partyMaster.partyAddressList != null) {
                for (var i = 0; i < $scope.enquiryLog.partyMaster.partyAddressList.length; i++) {
                    $scope.partyAddressCustomer.push($scope.enquiryLog.partyMaster.partyAddressList[i]);
                }
            }
            $scope.showPartyAddress = true;
            $scope.selectedItem = -1;
            $scope.whichColumn = "DELIVERY";
            $scope.enquDetail = enqDetail;
        }



        $scope.navigateToNextField = function(id, index) {
            console.log("navigateToNextField is called.", id);
            if (id != undefined && index != undefined) {
                var value = id + index;
                if (document.getElementById(value))
                    document.getElementById(value).focus();
            } else {
                if (document.getElementById(id))
                    document.getElementById(id).focus();
            }

        }

        $scope.serviceTabArr = [{
            "tabTitle": "New Service"
        }];



        $scope.clickTabIndex = function(obj, index, focus) {
            if (obj.id == undefined || obj.id == null) {
                if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_SERVICE_CREATE)) {
                    $scope.enquiryLog.selectedTabIndex = index;
                    $scope.navigateToNextField(focus, index);
                }
            } else {
                if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_SERVICE_MODIFY)) {
                    $scope.enquiryLog.selectedTabIndex = index;
                    $scope.navigateToNextField(focus, index);
                }
            }
        };


        $scope.enquirySelectFocus = function(id) {
            console.log("called." + id);
            $scope.enquiryLog.selectedTabIndex = 0;
            $rootScope.navigateToNextField(id);
        }


        //For Cancelling the enquiry   
        $scope.enquiryCancelled = function(enq) {
            if (enq.status == 'Cancel') {
                Notification.warning("Already Enquiry was cancelled");
                return;
            }
            if (enq.statusChange == true) {
                ngDialog.openConfirm({
                    template: '<p>Do you want to cancel the enquiry?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.saveEnquiryStatus("Cancelled", enq);
                        $scope.spinner = true;
                    } else {
                        enq.statusChange = false;
                    }
                })
            }

        }


        $scope.saveEnquiryStatus = function(status, enq) {

            $scope.enquiryDto = {};
            $scope.enquiryDto.enqId = enq.id;
            $scope.enquiryDto.enquiryStatus = 'Cancel';
            EnquiryStatusChange.status($scope.enquiryDto).$promise
                .then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.enquiryLog = data.responseObject;
                        $scope.enquiryLog.statusChange = true;
                        $scope.spinner = false;
                        Notification.success($rootScope.nls["ERR7025"]);
                        $state.go("layout.salesEnquiry");
                    } else {
                        enq.statusChange = false;
                        $scope.spinner = false;
                        Notification.error($rootScope.nls["ERR7026"]);
                    }
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                }, function(error) {
                    Notification.error("Problem in enquiry while change the status");
                    $scope.spinner = false;
                    enq.statusChange = false;
                    Notification.error($rootScope.nls["ERR7026"]);
                });
        }

        //for enabling the cancelled check box 
        $scope.checkStatus = function(dataObj) {
            if (dataObj != undefined) {
                dataObj.statusChange = dataObj.status == "Cancel" ? true : false;
            }
        }

        $scope.checkLengthOfRate = function(obj) {
            if (obj != undefined && (obj.rateRequestList == null || obj.rateRequestList == undefined || obj.rateRequestList.length == 0)) {
                obj.rateRequestList.push({});
            }
        }




        $scope.downloadAttach = function(param) {
            if (param.data.id != null && param.data.file == null) {
                console.log("API CALL")
                downloadFactory.downloadAttachment('/api/v1/enquirylog/files/', param.data.id, param.data.fileName);
            } else {
                console.log("NO API CALL", param);
                saveAs(param.data.tmpFile, param.data.fileName);
            }
        }

        $scope.hasAccessWithFocus = function(feature, focus, tabName) {

            if (feature == null || $scope.hasAccess(feature)) {
                $scope.Tabs = tabName;
                console.log("$scope.Tabs " + $scope.Tabs);
                $rootScope.navigateToNextField(focus);
            }
        }


        /* Recent Enquiry Code Begins */


        $scope.topEnquiryConfig = {
            ajax: true,
            showTopEnquiryFlag: false
        };


        $scope.showTopRecords = function() {

            $scope.searchDto = {};

            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = 5;

            if ($scope.enquiryLog.partyMaster != null) {
                $scope.searchDto.partyId = $scope.enquiryLog.partyMaster.id;
                $scope.topEnquiryConfig.title = $scope.enquiryLog.partyMaster.partyName;

                $scope.topRecords = [];
                EnquiryLogSearchRecent.query($scope.searchDto).$promise.then(function(data, status) {

                    $scope.totalRecord = data.totalRecord;
                    $scope.recentEnquiryList = data.searchResult;
                });

                $scope.contained_progressbar.complete();
                $scope.topEnquiryConfig.showTopEnquiryFlag = true;
            }
        }

        $scope.closeRecentList = function() {
            $scope.topEnquiryConfig.showTopEnquiryFlag = false;
        }

        $scope.changeDimension = function(enquiryDetail) {
            if (enquiryDetail.dimensionUnit != undefined) {
                if (enquiryDetail.dimensionUnit == false) {
                    enquiryDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                } else {
                    enquiryDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                }
            }
            $scope.changeVolume(enquiryDetail);
        };

        $scope.changeVolume = function(enquiryDetail) {
            var grossWeight = 0.0;
            var volWeight = 0.0;
            var noOfPiece = 0;

            if (enquiryDetail.enquiryDimensionList == undefined || enquiryDetail.enquiryDimensionList == null || enquiryDetail.enquiryDimensionList.length == 0) {
                enquiryDetail.dimensionVolWeight = 0.0;
                enquiryDetail.dimensionGrossWeight = 0.0;
                enquiryDetail.enquiryDimensionList = [];
                return;
            }

            enquiryDetail.chargebleWeight = 0.0
            enquiryDetail.grossWeight = 0.0;
            enquiryDetail.volumeWeight = 0.0;
            enquiryDetail.totalNoOfPieces = 0;

            for (var i = 0; i < enquiryDetail.enquiryDimensionList.length; i++) {

                enquiryDetail.enquiryDimensionList[i].volWeight = (enquiryDetail.enquiryDimensionList[i].noOfPiece * (enquiryDetail.enquiryDimensionList[i].length * enquiryDetail.enquiryDimensionList[i].width * enquiryDetail.enquiryDimensionList[i].height)) / enquiryDetail.dimensionUnitValue;
                enquiryDetail.enquiryDimensionList[i].volWeight = Math.round(enquiryDetail.enquiryDimensionList[i].volWeight * 100) / 100;
                volWeight = volWeight + parseFloat(enquiryDetail.enquiryDimensionList[i].volWeight);
                grossWeight = grossWeight + parseFloat(enquiryDetail.enquiryDimensionList[i].grossWeight);
                noOfPiece = parseInt(noOfPiece) + parseInt(enquiryDetail.enquiryDimensionList[i].noOfPiece);
            }

            enquiryDetail.dimensionVolWeight = Math.round(volWeight * 100) / 100;
            enquiryDetail.dimensionGrossWeight = Math.round(grossWeight * 100) / 100;
            enquiryDetail.totalNoOfPieces = parseInt(noOfPiece);

            if (enquiryDetail.dimensionUnit) { //Pound to Kg

                enquiryDetail.volWeight = Math.round(volWeight * 100) / 100;
                enquiryDetail.grossWeight = Math.round((grossWeight * 0.45359237) * 100) / 100;

            } else {
                enquiryDetail.volWeight = Math.round(volWeight * 100) / 100;
                enquiryDetail.grossWeight = Math.round(grossWeight * 100) / 100;
            }


            if (enquiryDetail.volWeight >= 0) {
                enquiryDetail.volWeight = $rootScope.roundOffWeight(enquiryDetail.volWeight);
            }

            if (enquiryDetail.grossWeight >= 0) {
                enquiryDetail.grossWeight = $rootScope.roundOffWeight(enquiryDetail.grossWeight);
            }
        }


        /* Recent Enquiry Code Ends */
        $scope.editEnquiryGetdata = function(enqId) {
            EnquiryView.get({
                id: enqId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Get Enquiry for Edit", data.responseObject.id);
                    $scope.enquiryLog = data.responseObject;
                    $scope.checkStatus($scope.enquiryLog);
                    $scope.enquiryLog.selectedTabIndex = 0;

                    if ($scope.enquiryLog.partyMaster != null && $scope.enquiryLog.partyMaster.id != null) {
                        $scope.enquiryLog.partyCreated = true;
                    } else {
                        $scope.enquiryLog.partyCreated = false;
                    }

                    $scope.enquiryLog.loggedOn = $rootScope.dateToString($scope.enquiryLog.loggedOn);
                    $scope.enquiryLog.receivedOn = $rootScope.dateAndTimeToString($scope.enquiryLog.receivedOn);
                    $scope.enquiryLog.quoteBy = $rootScope.dateToString($scope.enquiryLog.quoteBy);

                    $scope.enquiryLog.serviceAccessRights = [];
                    valueAddedServicesFactory.getAllValue.query().$promise.then(function(data) {
                        var isRole = $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_SERVICE_MODIFY);
                        for (var i = 0; i < $scope.enquiryLog.enquiryDetailList.length; i++) {
                            if (isRole) {
                                $scope.enquiryLog.serviceAccessRights[i] = {};
                                $scope.enquiryLog.serviceAccessRights[i].ishavingRole = true;
                            } else {
                                $scope.enquiryLog.serviceAccessRights[i] = {};
                                $scope.enquiryLog.serviceAccessRights[i].ishavingRole = false;
                            }

                            $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedTempServiceList = [];

                            $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedTempServiceList = JSON.parse(JSON.stringify(data.responseObject.searchResult));


                            if ($scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedServiceList != null && $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedServiceList.length > 0) {

                                for (var j = 0; j < $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedTempServiceList.length; j++) {

                                    for (var k = 0; k < $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedServiceList.length; k++) {

                                        if (($scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedServiceList[k].valueAddedServices.id == $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedTempServiceList[j].id)) {

                                            $scope.enquiryLog.enquiryDetailList[i].enquiryValueAddedTempServiceList[j].selected = true;
                                            break
                                        }
                                    }

                                }
                            }

                            $scope.enquiryLog.enquiryDetailList[i].dimensionUnitValue = $scope.enquiryLog.enquiryDetailList[i].dimensionUnit ? $rootScope.appMasterData['inch.to.pounds'] : $rootScope.appMasterData['centimeter.to.kilos'];
                            $scope.enquiryLog.enquiryDetailList[i].hazardous = $scope.enquiryLog.enquiryDetailList[i].hazardous == 'Yes' ? true : false;
                            $scope.enquiryLog.enquiryDetailList[i].clearance = $scope.enquiryLog.enquiryDetailList[i].clearance == 'Yes' ? true : false;


                            if ($scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList == undefined || $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList == null || $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList.length == 0) {
                                $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList = [];
                                $scope.enquiryLog.enquiryDetailList[i].enquiryDimensionList.push({})
                            }
                            if ($scope.enquiryLog.enquiryDetailList[i].enquiryAttachmentList == undefined || $scope.enquiryLog.enquiryDetailList[i].enquiryAttachmentList == null || $scope.enquiryLog.enquiryDetailList[i].enquiryAttachmentList.length == 0) {
                                $scope.enquiryLog.enquiryDetailList[i].enquiryAttachmentList = [];
                            }


                            if ($scope.enquiryLog.enquiryDetailList[i].rateRequestList != undefined && $scope.enquiryLog.enquiryDetailList[i].rateRequestList != null && $scope.enquiryLog.enquiryDetailList[i].rateRequestList.length > 0) {
                                for (var s = 0; s < $scope.enquiryLog.enquiryDetailList[i].rateRequestList.length; s++) {
                                    if ($scope.enquiryLog.enquiryDetailList[i].rateRequestList[s] != undefined && $scope.enquiryLog.enquiryDetailList[i].rateRequestList[s] != null && $scope.enquiryLog.enquiryDetailList[i].rateRequestList[s].responseDate != undefined) {
                                        $scope.enquiryLog.enquiryDetailList[i].rateRequestList[s].responseDate = $rootScope.dateAndTimeToString($scope.enquiryLog.enquiryDetailList[i].rateRequestList[s].responseDate);
                                    }
                                }
                            } else {
                                $scope.enquiryLog.enquiryDetailList[i].rateRequestList = [{}];
                            }


                        }
                        oldEnquiry = JSON.stringify($scope.enquiryLog);
                        $rootScope.navigateToNextField("partyName");
                        if ($scope.enquiryForm != undefined) {
                            $scope.enquiryForm.$setPristine();
                        }
                    }, function(response) {
                        console.log("Value  Added Service ");
                    });


                    if ($scope.enquiryLog != undefined && $scope.enquiryLog != null && $scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null &&
                        $scope.enquiryLog.partyMaster.partyName != undefined && $scope.enquiryLog.partyMaster.partyName != null) {
                        $rootScope.subTitle = $scope.enquiryLog.partyMaster.partyName;
                    } else {
                        $rootScope.subTitle = "Unknown Party"
                    }

                    $rootScope.serviceCodeForUnHistory = "";
                    $rootScope.orginAndDestinationUnHistory = "";
                    if ($scope.enquiryLog != undefined && $scope.enquiryLog != null &&
                        $scope.enquiryLog.enquiryDetailList != undefined && $scope.enquiryLog.enquiryDetailList != null &&
                        $scope.enquiryLog.enquiryDetailList.length > 0 && $scope.enquiryLog.enquiryDetailList[0] != null) {

                        if ($scope.enquiryLog.enquiryDetailList[0].serviceMaster != undefined && $scope.enquiryLog.enquiryDetailList[0].serviceMaster != null) {
                            $rootScope.serviceCodeForUnHistory = $scope.enquiryLog.enquiryDetailList[0].serviceMaster.serviceCode;
                        }

                        if ($scope.enquiryLog.enquiryDetailList[0].fromPortMaster != undefined && $scope.enquiryLog.enquiryDetailList[0].fromPortMaster != null) {
                            $rootScope.orginAndDestinationUnHistory = $scope.enquiryLog.enquiryDetailList[0].fromPortMaster.portCode;
                        }
                        if ($scope.enquiryLog.enquiryDetailList[0].toPortMaster != undefined && $scope.enquiryLog.enquiryDetailList[0].toPortMaster != null) {
                            $rootScope.orginAndDestinationUnHistory = $rootScope.orginAndDestinationUnHistory + " > " + $scope.enquiryLog.enquiryDetailList[0].toPortMaster.portCode;
                        }
                    }

                    var rHistoryObj = {
                        'title': 'Enquiry Edit # ' + $stateParams.enquiryId,
                        'subTitle': $rootScope.subTitle,
                        'stateName': $state.current.name,
                        'stateParam': JSON.stringify($stateParams),
                        'stateCategory': 'Enquiry',
                        'serviceType': 'AIR',
                        'serviceCode': $rootScope.serviceCodeForUnHistory,
                        'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                        'showService': true
                    }

                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    console.log("enquiry view Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("enquiry view Failed : " + error)
            });
        }




        $scope.$on('addSalesEnquiryEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.enquiryLog;
            $rootScope.category = "Enquiry";
            $rootScope.unfinishedFormTitle = "Enquiry (New)";
            if ($scope.enquiryLog != undefined && $scope.enquiryLog != null && $scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null &&
                $scope.enquiryLog.partyMaster.partyName != undefined && $scope.enquiryLog.partyMaster.partyName != null) {
                $rootScope.subTitle = $scope.enquiryLog.partyMaster.partyName;
            } else {
                $rootScope.subTitle = "Unknown Party"
            }
        });

        $scope.$on('editSalesEnquiryEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.enquiryLog;
            $rootScope.category = "Enquiry";
            $rootScope.unfinishedFormTitle = "Enquiry Edit # " + $scope.enquiryLog.enquiryNo;
            if ($scope.enquiryLog != undefined && $scope.enquiryLog != null && $scope.enquiryLog.partyMaster != undefined && $scope.enquiryLog.partyMaster != null &&
                $scope.enquiryLog.partyMaster.partyName != undefined && $scope.enquiryLog.partyMaster.partyName != null) {
                $rootScope.subTitle = $scope.enquiryLog.partyMaster.partyName;
            } else {
                $rootScope.subTitle = "Unknown Party"
            }
        });


        $scope.reloadEvent = function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.enquiryLog);
            localStorage.isEnquiryReloaded = "YES";
            e.preventDefault();
        };

        $scope.$on('addSalesEnquiryEventReload', $scope.reloadEvent);
        $scope.$on('editSalesEnquiryEventReload', $scope.reloadEvent);

        $scope.isReloaded = localStorage.isEnquiryReloaded;

        $scope.action = $stateParams.action;
        if ($stateParams.action == "ADD") {
            $scope.initDateFormat();
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEnquiryReloaded = "NO";
                    $scope.enquiryLog = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.enquiryLog = $rootScope.selectedUnfilledFormData;
                    if ($stateParams.forObj != undefined && $stateParams.forObj != null && $rootScope.naviPartyMaster != undefined &&
                        $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != undefined && $rootScope.naviPartyMaster.id != null) {
                        $scope.enquiryLog.partyMaster = $rootScope.naviPartyMaster;
                    }
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEnquiryReloaded = "NO";
                    $scope.enquiryLog = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.init();
                }
            }
            $scope.dimensionTab = false;
            $rootScope.serviceCodeForUnHistory = "";
            $rootScope.orginAndDestinationUnHistory = "";
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesEnquiry"
            }, {
                label: "Enquiry",
                state: "layout.salesEnquiry"
            }, {
                label: "Add Enquiry",
                state: null
            }];
        } else {
            $scope.initDateFormat();
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEnquiryReloaded = "NO";
                    $scope.enquiryLog = JSON.parse(localStorage.reloadFormData);
                    $scope.checkStatus($scope.enquiryLog);
                } else {
                    $scope.enquiryLog = $rootScope.selectedUnfilledFormData;
                    $scope.checkStatus($scope.enquiryLog);
                    if ($stateParams.forObj != undefined && $stateParams.forObj != null && $rootScope.naviPartyMaster != undefined &&
                        $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != undefined && $rootScope.naviPartyMaster.id != null) {
                        $scope.enquiryLog.partyMaster = $rootScope.naviPartyMaster;
                    }
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEnquiryReloaded = "NO";
                    $scope.enquiryLog = JSON.parse(localStorage.reloadFormData);
                    $scope.checkStatus($scope.enquiryLog);
                } else {
                    $scope.editEnquiryGetdata($stateParams.enquiryId);
                }
            }
            $scope.dimensionTab = false;
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesEnquiry"
            }, {
                label: "Enquiry",
                state: "layout.salesEnquiry"
            }, {
                label: "Edit Enquiry",
                state: null
            }];
        }




    }
]);