app.factory("EnquiryLogSearch",['$resource',  function($resource) {
    return $resource("/api/v1/enquirylog/get/search/:searchFlag", {}, {
        query: {
            method: 'POST',
            params: {
                searchFlag: ''
            },
            isArray: false
        }
    });
}]);


app.factory("EnquiryLogSearchRecent",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/get/search/recent", {}, {
        query: {
            method: 'POST',
            isArray: false
        }
    });
}]);

app.factory("EnquiryAdd",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/create", {}, {
        query: {
            method: 'POST'
        }
    });
}]);

app.factory("EnquiryUpdate",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/:id/update", {}, {
        update: {
            method: 'POST'
        }
    });
}]);

app.factory("EnquiryCreateCustomer",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/create/customer/:id", {}, {
        get: {
            method: 'GET',
            params: {
                id: ''
            },
            isArray: false
        }
    });
}]);


app.factory("EnquiryView",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/get/id/:id", {}, {
        get: {
            method: 'GET',
            params: {
                id: ''
            },
            isArray: false
        }
    });
}]);

app.factory("EnquiryViewByEnquiryNo",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/get/enquiryNo/:enquiryNo", {}, {
        get: {
            method: 'GET',
            params: {
                enquiryNo: ''
            },
            isArray: false
        }
    });
}]);


app.factory("EnquiryStatusChange",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/statuschange", {}, {
        status: {
            method: 'POST'
        }
    });
}]);


app.factory("EnquiryLogRemove",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/delete/:id", {}, {
        remove: {
            method: 'DELETE',
            params: {
                id: ''
            },
            isArray: false
        }
    });
}]);


app.factory("EnquiryAttachmentList",['$resource', function($resource) {
    return $resource("/api/v1/enquirylog/files/all", {}, {
        get: {
            method: 'POST'
        }
    });
}]);

app.factory("isQuotationCreatedFromEnquiry",['$resource', function($resource) {
    return $resource("/api/v1/quotation/isquotationcreatedfromenquiry/:enquiryNo", {}, {
        get: {
            method: 'GET',
            params: {
                enquiryNo: ''
            },
            isArray: false
        }
    });
}]);