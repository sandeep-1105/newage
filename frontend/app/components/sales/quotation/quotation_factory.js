app.factory("QuotationSearch",['$resource', function($resource) {
		return $resource("/api/v1/quotation/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
}]);

app.factory("QuotationView",['$resource', function($resource) {
	return $resource("/api/v1/quotation/get/id/:id", {}, {
		get : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}]);

app.factory("isClientApproved",['$resource', function($resource) {
	return $resource("/api/v1/quotation/get/isClientApproved", {}, {
		get : {
			method : 'GET',
			isArray : false
		}
	});
}]);

app.factory("QuotationViewByQuotationNo",['$resource', function($resource) {
	return $resource("/api/v1/quotation/get/quotationNo/:quotationNumber", {}, {
		get : {
			method : 'GET',
			params : {
				quotationNumber : ''
			},
			isArray : false
		}
	});
}]);

app.factory("QuotationAdd",['$resource', function($resource) {
	return $resource("/api/v1/quotation/create", {}, {
		query : {
			method : 'POST'
		}
	});
}]);

app.factory("QuotationUpdate",['$resource', function($resource) {
	return $resource("/api/v1/quotation/update", {}, {
		update : {
			method : 'POST'
		}
	});
}]);

app.factory("QuotationUpdateGroup",['$resource', function($resource) {
	return $resource("/api/v1/quotation/update/group", {}, {
		update : {
			method : 'POST'
		}
	});
}]);

app.factory("QuotationCopy",['$resource', function($resource) {
	return $resource("/api/v1/quotation/copyquote", {}, {
		update : {
			method : 'POST'
		}
	});
}]);


app.factory("QuotationRemove",['$resource', function($resource) {
	return $resource("/api/v1/quotation/delete/:id", {}, {
		remove : {
			method : 'DELETE',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}]);

app.factory("QuotationAttachment",['$resource', function($resource) {
	return $resource("/api/v1/quotation/files/:id", {}, {
		get : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : false,
			responseType: 'arraybuffer'
		}
	});
}]);

app.factory("QuotationApprove",['$resource', function($resource) {
	return $resource("/api/v1/quotation/statusupdate", {}, {
		update : {
			method : 'POST'
		}
	});
}]);

app.factory("QuotationCustomerApproval",['$resource', function($resource) {
	return $resource("/api/v1/quotation/customer/approval", {}, {
		approve : {
			method : 'POST'
		}
	});
}]);

app.factory("GetSurcharge",['$resource', function($resource) {
	return $resource("/api/v1/quotation/getsurcharge/chargeid/:chargeId", {}, {
		get : {
			method : 'GET',
			params : {
				chargeId : ''
			},
			isArray : false
		}
	});
}]);

app.factory("isShipmentCreatedFromQuotation",['$resource', function($resource) {
	return $resource("/api/v1/shipment/isshipmentcreatedfromquotation/:quotationNo", {}, {
		get : {
			method : 'GET',
			params : {
				quotationNo : ''
			},
			isArray : false
		}
	});
}]);



