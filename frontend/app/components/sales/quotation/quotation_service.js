app.factory('quotationService', function() {
    var savedData = {}
    var loc = null;

    function set(data) {
        try {
            savedData = data;
            localStorage.savedData = JSON.stringify(savedData);
        } catch (e) {
            var storageSize = Math.round(JSON.stringify(localStorage).length / 1024);
            console.log("LIMIT REACHED: (" + i + ") " + storageSize + "K");
        }
    }

    function get() {

        savedData = localStorage.savedData == null ? null : JSON.parse(localStorage.savedData);
        return savedData;
    }

    return {
        set: set,
        get: get,
        setLoc: setLoc,
        getLoc: getLoc,
    }

    function setLoc(data) {
        loc = data;
    }

    function getLoc() {

        return loc;
    }

});