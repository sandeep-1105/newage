/**
 * Created by hmspl on 12/4/16.
 */
app.controller('quotationCtrl', ['$rootScope', '$scope', '$location', '$modal', 'NavigationService',
    'QuotationCopy', 'quotationService', 'ngDialog', 'QuotationSearch', 'QuotationView', 'QuotationApprove',
    'AgentByServicePort', 'ValidateUtil', 'PartiesList', 'QuotationRemove', 'QuotationViewByQuotationNo',
    'cloneService', 'RecentHistorySaveService', 'GoToUnfilledFormNavigationService',
    '$http', 'downloadFactory', 'downloadMultipleFactory', '$sce', '$window', '$timeout', '$state',
    '$stateParams', 'PackGetByCode', 'QuotationUpdateGroup', 'roleConstant', 'Notification', 'EmptyRowChecker',
    'isShipmentCreatedFromQuotation','isClientApproved',
    function ($rootScope, $scope, $location, $modal, NavigationService,
        QuotationCopy, quotationService, ngDialog, QuotationSearch, QuotationView, QuotationApprove,
        AgentByServicePort, ValidateUtil, PartiesList, QuotationRemove, QuotationViewByQuotationNo,
        cloneService, RecentHistorySaveService, GoToUnfilledFormNavigationService,
        $http, downloadFactory, downloadMultipleFactory, $sce, $window, $timeout, $state,
        $stateParams, PackGetByCode, QuotationUpdateGroup, roleConstant, Notification, EmptyRowChecker,
        isShipmentCreatedFromQuotation,isClientApproved) {
        $scope.contentFileURL = "";

        $scope.isOverDimension = false;


        $scope.tabArray = [{
            tab: "Pending",
            displayName: "Waiting for Manager Approval",
            totalCount: 0
        }, {
            tab: "Approved",
            displayName: "Waiting for Customer Approval",
            totalCount: 0
        }, {
            tab: "ClientApproved",
            displayName: "Ready for Shipment",
            totalCount: 0
        }, {
            tab: "Rejected",
            displayName: "Rejected",
            totalCount: 0
        }, {
            tab: "All",
            displayName: "All",
            totalCount: 0

        }];

        $scope.searchOption = ['Reference No', 'Shipment ID', 'Sales Co-ordinator', 'Salesman', 'Created By'];




        $scope.quotationHeadArr = [{
            "name": "#",
            "width": " inl_sno",
            "model": "no",
            "search": false,
        }, {
            "name": "Service",
            "width": "w100px",
            "model": "serviceName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchServiceName"

        }, {
            "name": $rootScope.appMasterData['Party_Label'],
            "width": " inl_l",
            "model": "customerName",
            "search": true,
            "wrap_cell": true,
            "type": "color-text",
            "sort": true,
            "key": "searchCustomerName"

        }, {
            "name": "Quotation No",
            "width": "w100px",
            "model": "quotationNo",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchQuotationNo"

        }, {
            "name": "Origin",
            "width": "w150px",
            "model": "originName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchPortOrigin",
            "prefWidth": "150"
        }, {
            "name": "Destination",
            "width": "w150px",
            "model": "destinationName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchPortDestination",
            "prefWidth": "150"
        }, {
            "name": "Valid From",
            "width": "w103px",
            "model": "validFrom",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "key": "searchValidFrom"
        }, {
            "name": "Expires On",
            "width": "w103px",
            "model": "expiresOn",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "key": "searchValidTo"
        }, {
            "name": "Shipper",
            "width": "w100px",
            "model": "shipperName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchShipper",
            "prefWidth": "150"
        }]




        $scope.init = function () {

            $scope.tabChange("Pending");
            $scope.isNavigation = false;
            $scope.showDetail = true;
            $scope.searchDto = {};
            $scope.searchDto.status = 'Pending';
            if ($stateParams.status != null && $stateParams.status != undefined) {
                $scope.searchDto.status = $stateParams.status;
            }
            //$scope.search();
            getIndividualCount();

        }



        $scope.tabChange = function (status) {
            console.log("TabChange is called : " + status);
            $scope.detailTab = status;
            $scope.searchDto.status = status;
            $scope.search();
        }

        $scope.searchBy = function (clearFlag) {
            $scope.page = 0;
            if ($scope.quotationHeadArr.length == 9) {
                $scope.quotationHeadArr.splice($scope.quotationHeadArr.length - 1, 1);
            }
            $scope.navigateToNextField('searchValueAll');

            if ($scope.searchDto.searchName === undefined || $scope.searchDto.searchName === null) {
                $scope.searchDto.searchValue = "";
                $scope.searchValueAllFlag = false;
                $scope.quotationHeadArr.push({
                    "name": "Shipper",
                    "width": "w100px",
                    "model": "shipperName",
                    "search": true,
                    "wrap_cell": true,
                    "type": "text",
                    "sort": true,
                    "key": "searchShipper",
                    "prefWidth": "150"
                });
            } else {
                $scope.searchValueAllFlag = true;
                $scope.detailTab = 'All';
                $scope.searchDto.status = null;

                if (clearFlag) {
                    $scope.searchDto.searchValue = "";
                }

                if ($scope.searchDto.searchName === "Reference No") {

                    $scope.quotationHeadArr.push({
                        "name": "Reference No",
                        "width": "w150px",
                        "model": "referenceNumber",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "key": "searchRefNo",
                        "prefWidth": "150"
                    });
                } else if ($scope.searchDto.searchName === "Shipment ID") {
                    $scope.quotationHeadArr.push({
                        "name": "Shipment ID",
                        "width": "w150px",
                        "model": "shipmentUid",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "key": "shipmentId",
                        "prefWidth": "150"
                    });
                } else if ($scope.searchDto.searchName === "Shipper") {
                    $scope.quotationHeadArr.push({
                        "name": "Shipper",
                        "width": "w150px",
                        "model": "shipperName",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "key": "searchShipper",
                        "prefWidth": "150"
                    });

                } else if ($scope.searchDto.searchName === "Sales Co-ordinator") {
                    $scope.quotationHeadArr.push({
                        "name": "Sales Co-ordinator",
                        "width": "w150px",
                        "model": "salesCoOrdinator",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "key": "searchSalesCordinator",
                        "prefWidth": "150"
                    });

                } else if ($scope.searchDto.searchName === "Salesman") {
                    $scope.quotationHeadArr.push({
                        "name": "Salesman",
                        "width": "w150px",
                        "model": "routedBy",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "key": "searchSalesman",
                        "prefWidth": "150"
                    });
                } else {
                    $scope.quotationHeadArr.push({
                        "name": "Created By",
                        "width": "w150px",
                        "model": "createdBy",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "key": "searchLoggedBy",
                        "prefWidth": "150"
                    });
                }

            }
            $scope.search();
        }

        //

        $scope.buyRateAccess = $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CHARGE_BUY_RATE_VIEW, true);

        var routedByDisplayed = $rootScope.appMasterData['quotation.routed.enable'];
        if (routedByDisplayed != undefined && routedByDisplayed.toLowerCase() == 'true' || routedByDisplayed == true) {
            $scope.routedByFlag = true;
        } else {
            $scope.routedByFlag = false;
        }

        $scope.$roleConstant = roleConstant;


        //reportOptionModal code starts
        var reportOptionModal;
        $scope.getReportOptionModal = function (reportName) {
            $scope.quotation.reportName = reportName; //setting report name for grouping
            if ($scope.reportList != undefined && $scope.reportList != null) {
                $scope.allRepoVarModel.allReportSelected = false;
                $scope.isGenerateAll = false;
                for (var i = 0; i < $scope.reportList.length; i++) {
                    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
                }
            }
            if (reportName == 'QUOTATION_WITH_ESTIMATION') {
                if (!$rootScope.roleAccess(roleConstant.SALE_QUOTATION_REPORT_QUOTATION_WITH_ESTIMATED_DOWNLOAD)) {
                    return;
                }
            } else {
                if ($scope.quotation.quoteType == 'GENERAL') {
                    if (!$rootScope.roleAccess(roleConstant.SALE_QUOTATION_REPORT_QUOTATION_GENERAL_DOWNLOAD)) {
                        return;
                    }
                } else if ($scope.quotation.quoteType == 'LUMPSUM') {
                    if (!$rootScope.roleAccess(roleConstant.SALE_QUOTATION_REPORT_QUOTATION_LUMPSUM_DOWNLOAD)) {
                        return;
                    }
                } else if ($scope.quotation.quoteType == 'PERKG') {
                    if (!$rootScope.roleAccess(roleConstant.SALE_QUOTATION_REPORT_QUOTATION_PERKG_DOWNLOAD)) {
                        return;
                    }
                }

            }
            $scope.reportName = reportName;
            $scope.selectMultiCarrierModel(0);
            reportOptionModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/sales/quotation/view/report_option_popup.html',
                show: false
            });
            reportOptionModal.$promise.then(reportOptionModal.show)
        };


        $scope.base64PDF = "";
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 10;
        $scope.searchDto = {};
        $scope.showActionButton = true;



        //general notes popup
        var addgeneralModal;
        $scope.generalModel = function () {

            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_VIEW)) {
                addgeneralModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/sales/quotation/view/general_notes_popup_view.html',
                    show: false
                });
                addgeneralModal.$promise.then(addgeneralModal.show)
            }
        };

        //attachment button
        var addattachModal;
        $scope.attachModel = function () {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_ATTACHMENT_VIEW)) {
                addattachModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/sales/quotation/view/quotation_attachment_popup_view.html',
                    show: false
                });
                addattachModal.$promise.then(addattachModal.show)
            }
        };

        //report button
        var addreportModal;
        $scope.reportModel = function () {
            addreportModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/sales/quotation/view/quotation_report_popup_view.html',
                show: false
            });
            addreportModal.$promise.then(addreportModal.show)
        };

        //Multicarrier
        var addCarrierModal = null;
        $scope.multiCarrierList = null;

        $scope.addMultiCarrierModel = function () {

            $scope.multiCarrierList = null;

            var newScope = $scope.$new();

            $scope.multiCarrierList = angular.copy($scope.quotationDetail.quotationCarrierList);
            $scope.selectedCarrierIndex = 0;
            addCarrierModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/sales/quotation/view/quotation_add_multi_carrier_tabpopup_view.html',
                show: false
            });

            addCarrierModal.$promise.then(addCarrierModal.show);

        };

        $scope.selectMultiCarrierModel = function (index) {
            $scope.selectedCarrierIndex = index;
        }

        $scope.listConfig = {
            search: true,

        }

        $scope.historyModal = function () {
            //$scope.$emit('$destroy',{"state":true});
            var myOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/sales/quotation/view/charge_history.html',
                show: false
            });
            myOtherModal.$promise.then(myOtherModal.show);
        };

        $scope.panelCopyTitle = "Select Customer";



        $scope.sortSelection = {
            sortKey: "quotationNo",
            sortOrder: "desc"
        }


        $scope.changepage = function (param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }

        $scope.limitChange = function (item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.rowSelect = function (data, index) {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_VIEW)) {
                $scope.selectedRowIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
                var param = {
                    quotationId: data.id,
                    selectedPageNumber: $scope.selectedRowIndex,
                    totalRecord: $scope.totalRecord
                };
                console.log("State Parameters : ", param);
                $state.go("layout.salesQuotationView", $scope.searchDtoToStateParams(param));
            }
        }

        $scope.actionArr = ["Inserted", "Updated", "Deleted", "All"];

        $scope.view = function (quotationId) {
            QuotationView.get({
                id: quotationId
            }, function (data) {
                $scope.populateQuotationData(data);
            }, function (error) {
                console.log("Quotation view Failed : " + error)
            });
        }

        $scope.viewByNo = function (quotationNo) {
            QuotationViewByQuotationNo.get({
                quotationNumber: quotationNo
            }, function (data) {
                $rootScope.setNavigate1("Sales");
                $rootScope.setNavigate2("Quotation");
                $scope.populateQuotationData(data);
            }, function (error) {
                console.log("Quotation view Failed : " + error)
            });
        }

        $scope.populateQuotationData = function (data) {


            if (data.responseCode == "ERR0") {
                console.log("Quotation view Successfully")

                $scope.quotation = data.responseObject;
                $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                $scope.showDetail = false;
                $scope.showHistory = false;
                $scope.openDefaultTabs();
                $scope.enableQuotationEditFlag();
                /*$scope.populateAttachment();*/

                if ($scope.quotation != undefined && $scope.quotation != null && $scope.quotation.customer != undefined && $scope.quotation.customer != null &&
                    $scope.quotation.customer.partyName != undefined && $scope.quotation.customer.partyName != null) {
                    $rootScope.subTitle = $scope.quotation.customer.partyName;
                } else {
                    $rootScope.subTitle = "Unknown Party"
                }
                $rootScope.serviceCodeForUnHistory = "";
                $rootScope.orginAndDestinationUnHistory = "";
                if ($scope.quotation != undefined && $scope.quotation != null &&
                    $scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null &&
                    $scope.quotation.quotationDetailList.length > 0 && $scope.quotation.quotationDetailList[0] != null) {

                    if ($scope.quotation.quotationDetailList[0].serviceMaster != undefined && $scope.quotation.quotationDetailList[0].serviceMaster != null) {
                        $rootScope.serviceCodeForUnHistory = $scope.quotation.quotationDetailList[0].serviceMaster.serviceCode;
                    }

                    if ($scope.quotation.quotationDetailList[0].origin != undefined && $scope.quotation.quotationDetailList[0].origin != null) {
                        $rootScope.orginAndDestinationUnHistory = $scope.quotation.quotationDetailList[0].origin.portCode;
                    }
                    if ($scope.quotation.quotationDetailList[0].destination != undefined && $scope.quotation.quotationDetailList[0].destination != null) {
                        $rootScope.orginAndDestinationUnHistory = $rootScope.orginAndDestinationUnHistory + " > " + $scope.quotation.quotationDetailList[0].destination.portCode;
                    }
                }

                $rootScope.unfinishedFormTitle = "Quotation View # " + $scope.quotation.quotationNo;
                var rHistoryObj = {
                    'title': 'Quotation View # ' + $scope.quotation.quotationNo,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'Quotation',
                    'serviceType': 'AIR',
                    'serviceCode': $rootScope.serviceCodeForUnHistory,
                    'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                    'showService': true
                }
                RecentHistorySaveService.form(rHistoryObj);
            } else {
                console.log("Quotation view Failed " +
                    data.responseDescription)
            }

            if (($stateParams.fromEnqId != null && $stateParams.fromEnqId != undefined && $stateParams.fromEnqId != "") ||
                ($stateParams.fromShipmentId != null && $stateParams.fromShipmentId != undefined && $stateParams.fromShipmentId != "") ||
                ($stateParams.showActionButton != null && $stateParams.showActionButton != undefined && $stateParams.showActionButton != "0")) {
                $scope.showActionButton = false;
            }
            $scope.isOverDimension = checkOverDimension();
        }

        var checkOverDimension = function(){
            var thresholdLength = 317;
            var thresholdWidth = 317;
            var thresholdHeight = 158;
            if($scope.quotationDetail.dimensionUnit){
                thresholdLength = 125;
                thresholdWidth = 125;
                thresholdHeight = 62;
        }
            for (var i = 0; i < $scope.quotationDetail.quotationDimensionList.length; i++) {
                var currentRowData = $scope.quotationDetail.quotationDimensionList[i];
                if(currentRowData.length > thresholdLength || currentRowData.width > thresholdWidth || currentRowData.height > thresholdHeight){
                    return true;
                }
            }
            return false;
        }
        
        $scope.changeSearch = function (param) {
            console.log("param", param);
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.search();

        }

        $scope.changeHeaderSearch = function () {
            $scope.search();
        }

        $scope.sortChange = function (param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();

        }

        $scope.singlePageNavigation = function (val) {
            if ($stateParams != undefined && $stateParams != null) {

                if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                    return;

                var stateParameters = {};
                $scope.searchDto = {};
                $scope.searchDto.recordPerPage = 1;
                $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

                $scope.searchDto.searchLoggedOn = {};
                if ($stateParams.searchLoggedOnStartDate != undefined && $stateParams.searchLoggedOnStartDate != null) {
                    stateParameters.searchLoggedOnStartDate = $scope.searchDto.searchLoggedOn.startDate = $stateParams.searchLoggedOnStartDate;
                }
                if ($stateParams.searchLoggedOnEndDate != undefined && $stateParams.searchLoggedOnEndDate != null) {
                    stateParameters.searchLoggedOnEndDate = $scope.searchDto.searchLoggedOn.endDate = $stateParams.searchLoggedOnEndDate;
                }

                $scope.searchDto.searchValidTo = {};
                if ($stateParams.searchValidToStartDate != undefined && $stateParams.searchValidToStartDate != null) {
                    stateParameters.searchValidToStartDate = $scope.searchDto.searchValidTo.startDate = $stateParams.searchValidToStartDate;
                }
                if ($stateParams.searchValidToEndDate != undefined && $stateParams.searchValidToEndDate != null) {
                    stateParameters.searchValidToEndDate = $scope.searchDto.searchValidTo.endDate = $stateParams.searchValidToEndDate;
                }

                $scope.searchDto.searchValidFrom = {};
                if ($stateParams.searchValidFromStartDate != undefined && $stateParams.searchValidFromStartDate != null) {
                    stateParameters.searchValidFromStartDate = $scope.searchDto.searchValidFrom.startDate = $stateParams.searchValidFromStartDate;
                }
                if ($stateParams.searchValidFromEndDate != undefined && $stateParams.searchValidFromEndDate != null) {
                    stateParameters.searchValidFromEndDate = $scope.searchDto.searchValidFrom.endDate = $stateParams.searchValidFromEndDate;
                }

                if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                    stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
                }
                if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                    stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
                }
                if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                    stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
                }
                if ($stateParams.status != undefined && $stateParams.status != null) {
                    stateParameters.status = $scope.searchDto.status = $stateParams.status;
                }
                if ($stateParams.transportMode != undefined && $stateParams.transportMode != null) {
                    stateParameters.transportMode = $scope.searchDto.transportMode = $stateParams.transportMode;
                }
                if ($stateParams.searchLoggedBy != undefined && $stateParams.searchLoggedBy != null) {
                    stateParameters.searchLoggedBy = $scope.searchDto.searchLoggedBy = $stateParams.searchLoggedBy;
                }
                if ($stateParams.searchSalesman != undefined && $stateParams.searchSalesman != null) {
                    stateParameters.searchSalesman = $scope.searchDto.searchSalesman = $stateParams.searchSalesman;
                }
                if ($stateParams.searchSalesCordinator != undefined && $stateParams.searchSalesCordinator != null) {
                    stateParameters.searchSalesCordinator = $scope.searchDto.searchSalesCordinator = $stateParams.searchSalesCordinator;
                }
                if ($stateParams.searchShipper != undefined && $stateParams.searchShipper != null) {
                    stateParameters.searchShipper = $scope.searchDto.searchShipper = $stateParams.searchShipper;
                }
                if ($stateParams.searchPortDestination != undefined && $stateParams.searchPortDestination != null) {
                    stateParameters.searchPortDestination = $scope.searchDto.searchPortDestination = $stateParams.searchPortDestination;
                }
                if ($stateParams.searchPortOrigin != undefined && $stateParams.searchPortOrigin != null) {
                    stateParameters.searchPortOrigin = $scope.searchDto.searchPortOrigin = $stateParams.searchPortOrigin;
                }
                if ($stateParams.searchQuotationNo != undefined && $stateParams.searchQuotationNo != null) {
                    stateParameters.searchQuotationNo = $scope.searchDto.searchQuotationNo = $stateParams.searchQuotationNo;
                }
                if ($stateParams.searchCustomerName != undefined && $stateParams.searchCustomerName != null) {
                    stateParameters.searchCustomerName = $scope.searchDto.searchCustomerName = $stateParams.searchCustomerName;
                }
                if ($stateParams.searchServiceName != undefined && $stateParams.searchServiceName != null) {
                    stateParameters.searchServiceName = $scope.searchDto.searchServiceName = $stateParams.searchServiceName;
                }
                if ($stateParams.importExport != undefined && $stateParams.importExport != null) {
                    stateParameters.importExport = $scope.searchDto.importExport = $stateParams.importExport;
                }
                if ($stateParams.searchRefNo != undefined && $stateParams.searchRefNo != null) {
                    stateParameters.searchRefNo = $scope.searchDto.searchRefNo = $stateParams.searchRefNo;
                }
            } else {
                return;
            }
            QuotationSearch.query($scope.searchDto).$promise.then(function (
                data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;

                stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
                stateParameters.quotationId = data.responseObject.searchResult[0].id;
                stateParameters.totalRecord = $scope.totalRecord;
                $state.go("layout.salesQuotationView", stateParameters);
            });
        }
        $scope.searchDtoToStateParams = function (param) {
            if (param == undefined || param == null) {
                param = {};
            }
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                if ($scope.searchDto != undefined && $scope.searchDto != null) {
                    //Page Details
                    //        		param.selectedPageNumber = param.enqIdx; 
                    param.recordPerPage = 1;
                    //Sorting Details
                    if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                        param.sortByColumn = $scope.searchDto.sortByColumn;
                    }
                    if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                        param.orderByType = $scope.searchDto.orderByType;
                    }
                    //Reference No
                    if ($scope.searchDto.searchRefNo != undefined && $scope.searchDto.searchRefNo != null) {
                        param.searchRefNo = $scope.searchDto.searchRefNo;
                    }
                    //QUotation Valid From Date Between
                    if ($scope.searchDto.searchValidFrom != undefined && $scope.searchDto.searchValidFrom != null) {
                        param.searchValidFromStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidFrom.startDate);
                        param.searchValidFromEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidFrom.endDate);
                    }
                    //QUotation Valid To Date Between
                    if ($scope.searchDto.searchValidTo != undefined && $scope.searchDto.searchValidTo != null) {
                        param.searchValidToStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidTo.startDate);
                        param.searchValidToEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidTo.endDate);
                    }
                    //QUotation Logged On Date Between
                    if ($scope.searchDto.searchLoggedOn != undefined && $scope.searchDto.searchLoggedOn != null) {
                        param.searchLoggedOnStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchLoggedOn.startDate);
                        param.searchLoggedOnEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchLoggedOn.endDate);
                    }
                    if ($scope.searchDto.importExport != undefined && $scope.searchDto.importExport != null) {
                        param.importExport = $scope.searchDto.importExport;
                    }
                    if ($scope.searchDto.status != undefined && $scope.searchDto.status != null) {
                        param.status = $scope.searchDto.status;
                    }
                    if ($scope.searchDto.transportMode != undefined && $scope.searchDto.transportMode != null) {
                        param.transportMode = $scope.searchDto.transportMode;
                    }
                    if ($scope.searchDto.searchLoggedBy != undefined && $scope.searchDto.searchLoggedBy != null) {
                        param.searchLoggedBy = $scope.searchDto.searchLoggedBy;
                    }
                    if ($scope.searchDto.searchSalesman != undefined && $scope.searchDto.searchSalesman != null) {
                        param.searchSalesman = $scope.searchDto.searchSalesman;
                    }
                    if ($scope.searchDto.searchSalesCordinator != undefined && $scope.searchDto.searchSalesCordinator != null) {
                        param.searchSalesCordinator = $scope.searchDto.searchSalesCordinator;
                    }
                    if ($scope.searchDto.searchShipper != undefined && $scope.searchDto.searchShipper != null) {
                        param.searchShipper = $scope.searchDto.searchShipper;
                    }
                    if ($scope.searchDto.searchPortDestination != undefined && $scope.searchDto.searchPortDestination != null) {
                        param.searchPortDestination = $scope.searchDto.searchPortDestination;
                    }
                    if ($scope.searchDto.searchPortOrigin != undefined && $scope.searchDto.searchPortOrigin != null) {
                        param.searchPortOrigin = $scope.searchDto.searchPortOrigin;
                    }
                    if ($scope.searchDto.searchQuotationNo != undefined && $scope.searchDto.searchQuotationNo != null) {
                        param.searchQuotationNo = $scope.searchDto.searchQuotationNo;
                    }
                    if ($scope.searchDto.searchCustomerName != undefined && $scope.searchDto.searchCustomerName != null) {
                        param.searchCustomerName = $scope.searchDto.searchCustomerName;
                    }
                    if ($scope.searchDto.searchServiceName != undefined && $scope.searchDto.searchServiceName != null) {
                        param.searchServiceName = $scope.searchDto.searchServiceName;
                    }
                }

            }
            return param;
        }


        $scope.cancel = function () {
            if ($stateParams.nav_src_bkref_key != undefined && $stateParams.nav_src_bkref_key != null && $stateParams.nav_src_bkref_key != "") {
                if ($rootScope.unfinishedFormHistoryList != undefined &&
                    $rootScope.unfinishedFormHistoryList != null &&
                    $rootScope.unfinishedFormHistoryList.length > 0) {
                    for (var rchIndex = $rootScope.unfinishedFormHistoryList.length; rchIndex > 0; rchIndex--) {
                        if ($rootScope.unfinishedFormHistoryList[rchIndex - 1].nav_src_bkref_key != undefined && $rootScope.unfinishedFormHistoryList[rchIndex - 1].nav_src_bkref_key != null &&
                            $rootScope.unfinishedFormHistoryList[rchIndex - 1].nav_src_bkref_key == $stateParams.nav_src_bkref_key) {
                            var controlParams = {
                                submitAction: "Cancelled"
                            };
                            GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[rchIndex - 1], controlParams);
                            break;
                        }
                    }
                }

                // If coming from all other pages
                if ($stateParams.bkParam != undefined && $stateParams.bkParam != null) {
                    if ($stateParams.bkParam == 'activiti') {
                        $state.go($stateParams.bkState, {
                            activeTab: $stateParams.bkTab
                        });
                    }
                }
            } else if ($stateParams.fromEnqId != undefined && $stateParams.fromEnqId != null) {
                $state.go('layout.viewSalesEnquiry', {
                    enquiryId: $stateParams.fromEnqId
                });
            } else {
                var param = {};
                $scope.searchDto = {};
                $scope.searchDto.status = $stateParams.status;
                $state.go("layout.salesQuotation", $scope.searchDtoToStateParams(param));

            }
        }

        $scope.nextToFocus = function (nextFieldId) {
            if (nextFieldId != undefined) {
                $rootScope.navigateToNextField(nextFieldId);
            }

        };

        $scope.search = function () {
            $scope.quotation = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;

            $scope.tmpSearchDto = angular.copy($scope.searchDto);


            if ($scope.tmpSearchDto.searchValidFrom != null) {
                $scope.tmpSearchDto.searchValidFrom.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchValidFrom.startDate);
                $scope.tmpSearchDto.searchValidFrom.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchValidFrom.endDate);
            }

            if ($scope.tmpSearchDto.searchValidTo != null) {
                $scope.tmpSearchDto.searchValidTo.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchValidTo.startDate);
                $scope.tmpSearchDto.searchValidTo.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchValidTo.endDate);
            }

            if ($scope.tmpSearchDto.searchLoggedOn != null) {
                $scope.tmpSearchDto.searchLoggedOn.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchLoggedOn.startDate);
                $scope.tmpSearchDto.searchLoggedOn.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchLoggedOn.endDate);
            }

            $scope.searchRecord($scope.tmpSearchDto);
        }

        $scope.searchRecord = function (searchObj) {
            var resultArr = [];
            QuotationSearch.query(searchObj).$promise.then(function (data, status) {

                $scope.totalRecord = data.responseObject.totalRecord;
                var resultArr = [];

                angular.forEach(data.responseObject.searchResult, function (item, index) {
                    var tempObj = item;

                    if (tempObj.whoRouted == 'Self') {
                        tempObj.routedBy = tempObj.salesman;
                    } else {
                        tempObj.routedBy = tempObj.agentName;
                    }

                    if (tempObj.customerColorCode != null) {
                        tempObj.changeColor = true;
                        tempObj.color = tempObj.customerColorCode;
                    }

                    resultArr.push(tempObj);
                });

                $scope.quotationArr = resultArr;
            });
        }


        var getIndividualCount = function () {
            var tmpSearchDto = angular.copy($scope.searchDto);
            tmpSearchDto.selectedPageNumber = -1;
            tmpSearchDto.recordPerPage = -1;

            tmpSearchDto.status = "All";

            if (tmpSearchDto.searchValidFrom != null) {
                tmpSearchDto.searchValidFrom.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchValidFrom.startDate);
                tmpSearchDto.searchValidFrom.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchValidFrom.endDate);
            }

            if (tmpSearchDto.searchValidTo != null) {
                tmpSearchDto.searchValidTo.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchValidTo.startDate);
                tmpSearchDto.searchValidTo.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchValidTo.endDate);
            }

            if (tmpSearchDto.searchLoggedOn != null) {
                tmpSearchDto.searchLoggedOn.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchLoggedOn.startDate);
                tmpSearchDto.searchLoggedOn.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchLoggedOn.endDate);
            }

            isClientApproved.get().$promise.then(function (data, status) {
                $scope.clientApproved = data.responseObject.value;
            });

            QuotationSearch.query(tmpSearchDto).$promise.then(function (data, status) {

                angular.forEach(data.responseObject.searchResult, function (item, index) {

                    switch (item.approved) {
                        case "Pending":
                            updateTabTotalCount("Pending");
                            updateTabTotalCount("All");
                            break;

                        case "Approved":
                        if($scope.clientApproved==="true"){
                            updateTabTotalCount("Approved");
                          }
                        else{
                            updateTabTotalCount("ClientApproved");
                           }
                            updateTabTotalCount("All");
                            break;

                        case "Rejected":
                            updateTabTotalCount("Rejected");
                            updateTabTotalCount("All");
                            break;

                        case "Gained":
                            updateTabTotalCount("All");
                            break;

                        case "ClientApproved":
                            updateTabTotalCount("ClientApproved");
                            updateTabTotalCount("All");
                            break;

                    }
                });

            });
        }

        var updateTabTotalCount = function (tabName) {
            $scope.tabArray.forEach(function (item) {
                if (item.tab === tabName) item.totalCount = item.totalCount + 1;
            })
        }


        $scope.addQuotation = function () {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_CREATE)) {
                console.log("Add Quotation button is pressed....");

                $state.go("layout.salesQuotationAdd");
            }
        }

        $scope.editQuotation = function () {
            console.log("Edit Quotation button is pressed......", $scope.quotation.id);
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_MODIFY)) {
                var param = {
                    quotationId: $scope.quotation.id
                };

                console.log("State Parameters : ", param);
                var ishistoryFounded = false;
                if ($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0) {
                    for (var iIndex = 0; iIndex < $rootScope.unfinishedFormHistoryList.length; iIndex++) {
                        var tit = "Quotation Edit # " + $scope.quotation.id;
                        console.log($rootScope.unfinishedFormHistoryList[iIndex].title, "  ==   ", tit);
                        if ($rootScope.unfinishedFormHistoryList[iIndex].title == tit) {
                            var unSavedData = $rootScope.unfinishedFormHistoryList[iIndex];
                            GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[iIndex], undefined); // $rootScope.unfinishedFormHistoryList.push(stateDataObject);
                            ishistoryFounded = true;
                        }
                        if (ishistoryFounded) {
                            break;
                        }
                    }
                }
                if (!ishistoryFounded) {

                    $state.go("layout.salesQuotationEdit", param);
                }
            }
        }
        var myOtherModal = null;
        $scope.showModal = function () {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_COPY_QUOTE_CREATE)) {
                $scope.errorMap = new Map();
                $scope.copyQuote = {};
                $scope.copyQuote.quotation = $scope.quotation;
                myOtherModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/sales/quotation/view/copy_quote.html',
                    show: false
                });
                myOtherModal.$promise.then(myOtherModal.show);
            }
        };

        $scope.saveCopyQuote = function () {
            $scope.errorMap = new Map();

            if ($scope.copyQuoteValidate(0)) {

                $scope.tmpObject = cloneService.clone($scope.copyQuote);
                $scope.tmpObject.validFrom = $rootScope.sendApiStartDateTime($scope.copyQuote.validFrom);
                $scope.tmpObject.validTo = $rootScope.sendApiStartDateTime($scope.copyQuote.validTo);
                QuotationCopy.save($scope.tmpObject).$promise

                    .then(
                        function (data) {

                            if (data.responseCode == "ERR0") {

                                console.log("Quotation Copied Successfully");
                                $scope.view(data.responseObject, true);
                                myOtherModal.$promise.then(myOtherModal.hide);

                                var params = {};
                                params.quotationId = data.responseObject;
                                $state.go("layout.salesQuotationEdit", params);
                            } else {
                                console
                                    .log("added Failed " +
                                        data.responseDescription)
                            }
                        },
                        function (error) {
                            console
                                .log("added Failed : " +
                                    error)

                        });

            }
            return false;
        };

        $scope.partyRender = function (item) {
            return {
                label: item.partyName,
                item: item
            }
        }

        $scope.copyQuoteValidate = function (validateCode) {

            console.log("copyQuoteValidate" + validateCode);

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.copyQuote.party == undefined ||
                    $scope.copyQuote.party == null ||
                    $scope.copyQuote.party.id == "") {
                    $scope.errorMap.put("customer", $rootScope.nls["ERR22006"]);
                    return false;
                } else {

                    if ($scope.copyQuote.party.isDefaulter) {
                        $scope.copyQuote.party = null;
                        $scope.errorMap.put("customer", $rootScope.nls["ERR22161"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusBlocked($scope.copyQuote.party.status)) {
                        $scope.copyQuote.party = null;
                        $scope.errorMap.put("customer", $rootScope.nls["ERR22007"]);
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.copyQuote.party.status)) {
                        $scope.copyQuote.party = null;
                        $scope.errorMap.put("customer", $rootScope.nls["ERR22008"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 2) {

                if ($scope.copyQuote.validFrom == undefined ||
                    $scope.copyQuote.validFrom == null ||
                    $scope.copyQuote.validFrom.trim == "") {
                    $scope.errorMap.put("validFrom", $rootScope.nls["ERR22004"]);
                    return false;
                } else {
                    var validFrom = $rootScope.convertToDate($scope.copyQuote.validFrom);
                    $scope.copyQuote.validTo = $rootScope.dateToString(validFrom.add('days', $rootScope.appMasterData['quotation.valid.days']));
                }


                if ($scope.copyQuote.validTo != null) {
                    var validFrom = $rootScope.convertToDate($scope.copyQuote.validFrom);
                    var validTo = $rootScope.convertToDate($scope.copyQuote.validTo);
                    if (validTo < validFrom) {
                        $scope.errorMap.put("validFrom", $rootScope.nls["ERR22136"]);
                        console.log($rootScope.nls["ERR22136"]);
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 3) {

                if ($scope.copyQuote.validTo == undefined ||
                    $scope.copyQuote.validTo == null ||
                    $scope.copyQuote.validTo.trim == "") {
                    $scope.errorMap.put("validTo", $rootScope.nls["ERR22005"]);
                    return false;
                }

                if ($scope.copyQuote.validFrom != null) {
                    var validFrom = $rootScope.convertToDate($scope.copyQuote.validFrom);
                    var validTo = $rootScope.convertToDate($scope.copyQuote.validTo);
                    if (validTo < validFrom) {
                        $scope.errorMap.put("validTo", $rootScope.nls["ERR22136"]);
                        console.log($rootScope.nls["ERR22136"]);
                        return false;
                    }
                }
            }

            return true;
        };




        $scope.showCopyCustomerList = false;
        $scope.copycustomerlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "partyName",
                seperator: false
            },
            {
                "title": "partyCode",
                seperator: true
            }
            ]
        }
        $scope.showCopyCustomer = function () {
            $scope.selectedItem = -1;
            $scope.panelCopyTitle = "Select Party"

            $scope.showCopyCustomerList = true;
        };




        $scope.ajaxCopyCustomerEvent = function (object) {
            console.log("ajaxCopyCustomerEvent is called", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            $scope.copyCustomerList = {};
            $scope.copyCustomerList.data = [];
            return PartiesList.query($scope.searchDto).$promise.then(function (data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.copyCustomerList.data = data.responseObject.searchResult;
                    console.log("Number of Parties : ", $scope.copyCustomerList.data.length);
                    return $scope.copyCustomerList.data;
                    //$scope.showCopyCustomerList=true;
                }
            },
                function (errResponse) {
                    console.error('Error while fetching Countries');
                }
            );
        }



        $scope.selectedCopyCustomer = function (nextFieldId) {

            console.log("Select Customer.....");
            //$scope.copyQuote.party= obj;
            if ($scope.copyQuoteValidate(1)) {
                if (nextFieldId != undefined)
                    $rootScope.navigateToNextField(nextFieldId);
            }
            //$scope.cancelCopyCustomer();
        };

        $scope.cancelCopyCustomer = function () {
            console.log("Cancel Customer.....");
            $scope.showCopyCustomerList = false;
        }
        $scope.charge = {
            page: 0,
            limit: 10,
            totalRecord: 20
        }

        $scope.setLabel = function (data) {
            if (data == 'insert') {
                return 'label-success'
            } else if (data == 'update') {
                return 'label-warning'
            } else if (data == 'delete') {
                return 'label-danger'
            }
        }

        $scope.chargedatepickeropts = {
            "opens": "left",
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                format: "DD-MM-YYYY",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range'
            },
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()]
            },
            eventHandlers: {
                'apply.daterangepicker': function (ev, picker) {
                    console.log("ev", ev);
                    console.log("ev", JSON.stringify(ev.model));
                    console.log("picker", JSON.stringify(picker));
                    $scope.changeFunction();
                    //$scope.changeSearch();
                }
            }
        };

        $scope.attachConfig = {
            "isEdit": false, //false for view only mode
            "isDelete": false, //
            "columnDefs": [{
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                width: "inl_l"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file"
            }
            ]
        }

        $scope.downloadAttach = function (param) {
            if (param.data.id != null && param.data.file == null) {
                console.log("API CALL")
                downloadFactory.downloadAttachment('/api/v1/quotation/files/', param.data.id, param.data.fileName);
            } else if (param.data.id == null && param.data.file != null) {
                console.log("NO API CALL BUT Byte array found")
                var blob = new Blob([param.data.file], {
                    type: param.data.fileContentType + ";charset=utf-8"
                });
                saveAs(blob, param.data.fileName);
            } else {
                console.log("NO API CALL")
                saveAs(param.data.tmpFile, param.data.fileName);
            }
        }

        //Download Sales Quotation Without Estimation
        $scope.downloadSalesQuoteWithOutEstimation = function (salesQuoteId, reportActionType) {
            if (reportActionType === 'eMail') {
                console.log("eMail is not integerated..");
                return false;
            }
            $scope.reportDownloadRequest = {};
            $scope.reportDownloadRequest.quotationId = salesQuoteId;
            $scope.reportDownloadRequest.downloadOption = reportActionType;
            $scope.reportDownloadRequest.downloadFileType = $scope.downloadFIleType;
            console.log("Sales Quotaion Id -:: ", salesQuoteId, " ::- Report Type -:: ", reportActionType);
            console.log("API CALL");
            $http({
                url: $rootScope.baseURL + '/api/v1/quotation/report/quotation',
                method: "POST",
                responseType: 'arraybuffer',
                data: $scope.reportDownloadRequest,
            }).success(function (data, status, headers, config) {
                console.log("hiddenElement ", data);
                var blobResponseType = 'application/pdf';
                var downloadReportFileName = "SalesQuotation_" + salesQuoteId + ".pdf";
                if ($scope.reportDownloadRequest.downloadFileType === "PDF") {
                    blobResponseType = 'application/pdf';
                    downloadReportFileName = "SalesQuotation_" + salesQuoteId + ".pdf";
                } else if ($scope.reportDownloadRequest.downloadFileType === "XLS") {
                    blobResponseType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    downloadReportFileName = "SalesQuotation_" + salesQuoteId + ".xls";
                } else if ($scope.reportDownloadRequest.downloadFileType === "RTF") {
                    blobResponseType = 'application/rtf';
                    downloadReportFileName = "SalesQuotation_" + salesQuoteId + ".rtf";
                }
                console.log("Blob ResponseType :: ", blobResponseType, "DownloadReport FileName :: ", downloadReportFileName);


                if (reportActionType === "Download") {
                    var blob = new Blob([data], {});
                    saveAs(blob, downloadReportFileName);
                } else if (reportActionType === "Print") {
                    console.log("PDf Print..");
                    var file = new Blob([data], {
                        type: blobResponseType
                    });
                    var fileURL = URL.createObjectURL(file);
                    $scope.contentFileURL = $sce.trustAsResourceUrl(fileURL);

                    var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                        'resizable,screenX=50,screenY=50,width=850,height=1050';
                    var printWindow = window.open($scope.contentFileURL, "PDF", winparams);
                    console.log("Focusing ... ");
                    printWindow.focus();
                    printWindow.print();
                    console.log("Focused ... ");
                } else {
                    var file = new Blob([data], {
                        type: blobResponseType
                    });
                    var fileURL = URL.createObjectURL(file);
                    $scope.contentFileURL = $sce.trustAsResourceUrl(fileURL);
                    $window.open($scope.contentFileURL);
                }
            }).error(function (data, status, headers, config) {
                console.log("Problem while downloading.....");
            });
        }

        $scope._arrayBufferToBase64 = function (buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;
            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }
        //Download Sales Quotation With Estimation
        $scope.downloadSalesQuoteWithEstimation = function (salesQuoteId, reportActionType) {
            if (reportActionType === 'eMail') {
                console.log("eMail Integeration is not Implemented..");
                return false;
            }
            $scope.reportDownloadRequest = {};
            $scope.reportDownloadRequest.quotationId = salesQuoteId;
            $scope.reportDownloadRequest.downloadOption = reportActionType;
            $scope.reportDownloadRequest.downloadFileType = $scope.downloadFIleType;
            console.log("Sales Quotaion Id -:: ", salesQuoteId, " ::- Report Type -:: ", reportActionType);
            console.log("API CALL");
            $http({
                url: $rootScope.baseURL + '/api/v1/quotation/report/quotation/withestimation',
                method: "POST",
                responseType: 'arraybuffer',
                data: $scope.reportDownloadRequest
            }).success(function (data, status, headers, config) {
                console.log("hiddenElement ", data);
                var blobResponseType = 'application/pdf';
                var downloadReportFileName = "SalesQuotation_With_Estimation_" + salesQuoteId + ".pdf";
                if ($scope.reportDownloadRequest.downloadFileType === "PDF") {
                    blobResponseType = 'application/pdf';
                    downloadReportFileName = "SalesQuotation_With_Estimation_" + salesQuoteId + ".pdf";
                } else if ($scope.reportDownloadRequest.downloadFileType === "XLS") {
                    blobResponseType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    downloadReportFileName = "SalesQuotation_With_Estimation_" + salesQuoteId + ".xls";
                } else if ($scope.reportDownloadRequest.downloadFileType === "RTF") {
                    blobResponseType = 'application/rtf';
                    downloadReportFileName = "SalesQuotation_With_Estimation_" + salesQuoteId + ".rtf";
                }
                console.log("Blob ResponseType :: ", blobResponseType, "DownloadReport FileName :: ", downloadReportFileName);


                if (reportActionType === "Download") {
                    var blob = new Blob([data], {});
                    saveAs(blob, downloadReportFileName);
                } else if (reportActionType === "Print") {
                    console.log("PDf Print..");
                    var file = new Blob([data], {
                        type: blobResponseType
                    });
                    var fileURL = URL.createObjectURL(file);
                    $scope.contentFileURL = $sce.trustAsResourceUrl(fileURL);

                    var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                        'resizable,screenX=50,screenY=50,width=850,height=1050';
                    var printWindow = window.open($scope.contentFileURL, "PDF", winparams);
                    console.log("Focusing ... ");
                    printWindow.focus();
                    printWindow.print();
                    console.log("Focused ... ");
                } else {
                    var file = new Blob([data], {
                        type: blobResponseType
                    });
                    var fileURL = URL.createObjectURL(file);
                    $scope.contentFileURL = $sce.trustAsResourceUrl(fileURL);
                    $window.open($scope.contentFileURL);

                }
            }).error(function (data, status, headers, config) {
                console.log("Problem while downloading.....");
            });
        }

        /*$scope.populateAttachment = function() {
        	console.log("Populating Attachment....");
        	$scope.attachConfig.data = [];
        	if($scope.quotation.quotationAttachementList != null && $scope.quotation.quotationAttachementList.length != 0){
        		
        		for(var i = 0; i < $scope.quotation.quotationAttachementList.length; i++) {
        				
        			var displayObject = new Object();
        			displayObject.id =  $scope.quotation.quotationAttachementList[i].id;
        			displayObject.refNo =  $scope.quotation.quotationAttachementList[i].refNo;
        			displayObject.fileName =  $scope.quotation.quotationAttachementList[i].fileName;
        			displayObject.file =  $scope.quotation.quotationAttachementList[i].file;
        			displayObject.fileContentType =  $scope.quotation.quotationAttachementList[i].fileContentType;
        			displayObject.systemTrack = $scope.quotation.quotationAttachementList[i].systemTrack;
        			
        			
        			$scope.attachConfig.data.push(displayObject);
        		}
        	}		
        }*/

        /*Report Related Codes Starts Here*/
        $scope.downloadFileType = "PDF";
        $scope.generate = {};
        $scope.generate.typeArr = ["Download", "Preview", "Print", "eMail"];
        $scope.reportList = [];
        var allRepoVar = {};
        $scope.allRepoVarModel = {};
        $scope.allRepoVarModel.allReportSelected = false;
        $scope.reportLoadingImage = {};
        $scope.reportLoadingImage.status = false;
        $scope.reportNameList = ["QUOTATION_WITHOUT_ESTIMATION", "QUOTATION_WITH_ESTIMATION"];

        $scope.clickOnQuotationTab = function (tab) {
            if (tab == "Services" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VIEW)) {
                $scope.detailTab = tab;
            }
            if (tab == "chargeBtn" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CHARGE_VIEW)) {
                $scope.moreAddressTab = tab;
            }
            if (tab == "dimensionBtn" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_DIMENSION_VIEW)) {
                $scope.moreAddressTab = tab;
            }
            if (tab == "pickupBtn" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_PICKUP_DELIVERY_VIEW)) {
                $scope.moreAddressTab = tab;
            }
            if (tab == "notesBtn" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_VIEW)) {
                $scope.moreAddressTab = tab;
            }
            if (tab == "valueAdded" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VALUE_ADDED_SERVICE_VIEW)) {
                $scope.moreAddressTab = tab;
            }

            if (tab == "notes" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_VIEW)) {
                $scope.detailTab = tab;
            }
            if (tab == "attachMent" && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_ATTACHMENT_VIEW)) {
                $scope.detailTab = tab;
            }


        }

        $scope.onClickReportTab = function () {
            $rootScope.mainpreloder = true;
            if ($scope.hasAccess('QUOTATION_REPORTS_TAB_VIEW')) {
                $scope.reportList = [];

                $scope.allReportSelected = false;
                $scope.reportLoadingImage.status = false;
                if ($scope.quotation != null && $scope.quotation.id != null && $scope.quotation.id > 0) {
                    for (var i = 0; i < $scope.reportNameList.length; i++) {
                        var localReportName = "";
                        if ($scope.reportNameList[i] === 'QUOTATION_WITHOUT_ESTIMATION') {
                            if ($scope.quotation.quoteType === 'LUMPSUM') {
                                localReportName = 'QUOTATION_FOR_LUMPSUM';
                            } else if ($scope.quotation.quoteType === 'PERKG') {
                                localReportName = 'QUOTATION_FOR_PERKG';
                            } else {
                                localReportName = 'QUOTATION_WITHOUT_ESTIMATION';
                            }
                        } else {
                            localReportName = $scope.reportNameList[i];
                        }
                        if (localReportName != "") {
                            $scope.assignDefaultReports(localReportName, $scope.quotation.id);
                        }
                    }
                }

                $scope.reportModel();
            }

            $rootScope.mainpreloder = false;

        }
        $scope.selectAllReports = function () {
            for (var i = 0; i < $scope.reportList.length; i++) {
                $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
            }
            if (!$scope.allRepoVarModel.allReportSelected) {
                $scope.isGenerateAll = false;
            }
        }
        $scope.assignDefaultReports = function (repo, id) {
            $scope.reportDetailData = {
                isSelected: false,
                reportName: repo,
                resourceId: id
            };
            $scope.reportList.push($scope.reportDetailData);
            $rootScope.mainpreloder = false;
        }
        $scope.chkReportStatus = function (chkFlag, detObj) {
            console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
        }

        $scope.isSingle = false;
        $scope.isMultiple = false;
        //$scope.downOption='Download';
        //$scope.downType='PDF';



        $scope.isGenerateAll = false;

        $scope.generateAll = function () {
            var flag = false;
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    flag = true
                    break;
                }
            }
            if (!flag) {
                $scope.isGenerateAll = false;
                Notification.warning("Please select atleast one.......)")
            } else {
                $scope.isGenerateAll = true;
                $scope.myReportModal = {};
                $scope.myReportModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/report/report_opt.html',
                    show: false
                });
                $rootScope.mainpreloder = false;
                $scope.myReportModal.$promise.then($scope.myReportModal.show);
            }
        }


        $scope.generateAllReports = function () {
            if ($scope.reportList == undefined || $scope.reportList == null || $scope.reportList.length == 0) {
                //Notification.warning("Please select atleast one.......)")
            } else {
                //Using for mail sending for download reports 
                $scope.emailReportList = [];
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        $scope.emailReport = {};
                        $scope.emailReport.resourceId = $scope.reportList[i].resourceId;
                        $scope.emailReport.reportName = $scope.reportList[i].reportName;
                        $scope.emailReport.downloadOption = $scope.downloadOption;
                        $scope.emailReport.downloadFileType = $scope.downloadFileType;
                        $scope.emailReportList.push($scope.emailReport);
                    }
                }

            }
            if ($scope.emailReportList != undefined && $scope.emailReportList.length > 0) {
                $scope.isMultiple = true;
                $scope.isSingle = false;
            } else {
                $scope.isSingle = true;
                $scope.isMultiple = false;
            }
            var flag = false;
            if ($scope.isSingle) {
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        flag = true
                        $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
                        $timeout(function () {
                            $scope.reportLoadingImage.status = false;
                        }, 1000);
                    }
                    if (flag) {
                        break;
                    }
                }
                if (!flag) {
                    Notification.warning("Please select atleast one.......)");
                    return;
                }
            } else if ($scope.isMultiple) {
                $scope.downloadGenericCallMultiple($scope.emailReportList, $scope.downloadOption, $scope.isMultiple, "PDF", $scope.reportList[0].resourceId)
                console.log("Nothig selected");
            }
        }

        $rootScope.mainpreloder = false;
        $scope.openModal = function (resourceRefId, reportRefName) {

            $scope.changeReportDownloadFormat("PDF");
            $scope.dataResourceId = resourceRefId;
            $scope.dataReportName = reportRefName;
            $scope.myReportModal = {};
            $scope.myReportModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/report/report_opt.html',
                show: false
            });
            $rootScope.mainpreloder = false;
            $scope.myReportModal.$promise.then($scope.myReportModal.show);
        }


        $scope.changeReportDownloadFormat = function (dType) {
            console.log("Download type Changed :: ", dType);
            $scope.downloadFileType = dType;
            if (dType === 'PDF') {
                $scope.generate.TypeArr = ["Download", "Preview",
                    "Print"
                ];
            } else if (dType === 'eMail') {
                $scope.generate.TypeArr = ["eMail"];
            } else {
                $scope.generate.TypeArr = ["Download"];
            }
        }

        $scope.updateQuotation = function (quotationId, quotationDetail) {
            $rootScope.mainpreloder = true;
            QuotationUpdateGroup.save($scope.quotation).$promise.then(function (data) {
                if (data.responseCode == "ERR0") {
                    QuotationView.get({
                        id: data.responseObject.id
                    }, function (data) {
                        $scope.populateQuotationData(data);

                        reportOptionModal.$promise.then(reportOptionModal.hide);
                        $scope.openModal(quotationId, $scope.reportName);
                    }, function (error) {
                        console.log("Quotation view Failed : " + error)
                    });

                } else {
                    $rootScope.mainpreloder = false;
                    console.log("Quotation update Failed " + data.responseDescription)
                }
            },
                function (error) {
                    $rootScope.mainpreloder = false;
                    console.log("Quotation update Failed : " + error)
                });
        }

        /*$scope.downloadGenericCall = function(id, downOption, repoName) {
    	var reportDownloadRequest = {
    		resourceId : id,
    		downloadOption : downOption,// "Download","Preview","Print"
    		downloadFileType : $scope.downloadFileType,//PDF
    		reportName : repoName, //'QUOTATION_WITH_ESTIMATION','QUOTATION_WITHOUT_ESTIMATION'
    		single : true
    	};
    	$rootScope.mainpreloder = true;
    	downloadFactory.download(reportDownloadRequest);
    }
		
    $scope.downloadGenericCallMultiple=function(emailReportList,downOption,multiple,type,id){
    	var reportDownloadRequest = {
    		emailReportList:emailReportList,
    		downloadOption : downOption,// "Download","Preview","Print"
    		//downloadOption : $scope.downOption,
    		downloadFileType : type,//PDF
    		isMultiple : multiple,
    		resourceId:id,
    		screenType :'Quotation'
    	};
    	downloadMultipleFactory.download(reportDownloadRequest);
    }*/



        $scope.downloadGenericCall = function (id, downOption, repoName) {
            $scope.downloadOption = downOption;
            if ($scope.isGenerateAll) {
                $scope.generateAllReports();
            } else {
                var reportDownloadRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: repoName, //'QUOTATION_WITH_ESTIMATION','QUOTATION_WITHOUT_ESTIMATION'
                    single: true
                };
                if (downOption === 'eMail') {
                    var flag = checkMail();
                    if (!flag) {
                        Notification.error($rootScope.nls["ERR275"]);
                        return false;
                    } else {
                        $rootScope.mainpreloder = true;
                        downloadFactory.download(reportDownloadRequest);
                        $timeout(function () {
                            $rootScope.mainpreloder = false;
                            $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                        }, 3000);
                    }
                } else {
                    $rootScope.mainpreloder = true;
                    downloadFactory.download(reportDownloadRequest);
                }
            }
        }

        function checkMail() {
            var flag = false;
            if ($scope.quotation != null && $scope.quotation != undefined) {
                if ($scope.quotation.customer != null && $scope.quotation.customer != undefined) {
                    if ($scope.quotation.customer.partyAddressList != null && $scope.quotation.customer.partyAddressList != undefined && $scope.quotation.customer.partyAddressList.length > 0) {

                        for (var i = 0; i < $scope.quotation.customer.partyAddressList.length; i++) {
                            if ($scope.quotation.customer.partyAddressList[0].addressType == "Primary") {
                                var emailList = $scope.quotation.customer.partyAddressList[0].email;
                                if (emailList == null || emailList == undefined) {
                                    flag = false;
                                } else {
                                    flag = true;
                                    break;
                                }
                            } else {
                                flag = false;
                            }
                        }
                    }
                }
            }
            return flag;
        }

        $scope.downloadGenericCallMultiple = function (emailReportList, downOption, multiple, type, id) {
            var reportDownloadRequest = {
                emailReportList: emailReportList,
                downloadOption: downOption, // "Download","Preview","Print"
                //downloadOption : $scope.downOption,
                downloadFileType: type, //PDF
                isMultiple: multiple,
                resourceId: id,
                screenType: 'Quotation'
            };
            if (downOption === 'eMail') {
                var flag = checkMail();
                if (!flag) {
                    Notification.error($rootScope.nls["ERR275"]);
                    return false;
                } else {
                    $rootScope.mainpreloder = true;
                    downloadMultipleFactory.download(reportDownloadRequest);
                    $timeout(function () {
                        $rootScope.mainpreloder = false;
                        $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                    }, 3000);
                }
            } else {
                $rootScope.mainpreloder = false;
                for (var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
                    var reportFileName = '';
                    reportFileName = reportDownloadRequest.emailReportList[i].reportName;
                    var reportDownloaddRequest = {
                        resourceId: id,
                        downloadOption: downOption, // "Download","Preview","Print"
                        downloadFileType: $scope.downloadFileType, //PDF
                        reportName: reportFileName, //'QUOTATION_WITH_ESTIMATION','QUOTATION_WITHOUT_ESTIMATION'
                        single: true
                    };
                    $rootScope.mainpreloder = true;
                    downloadFactory.download(reportDownloaddRequest);
                }
            }
        }


        var goToShipmentModel;

        $scope.quotationToShipmentCarrier = function (quotation) {

            if (quotation.quotationDetailList[0].quotationCarrierId == undefined &&
                quotation.quotationDetailList[0].quotationCarrierId == null) {

                if (quotation.quotationDetailList[0].quotationCarrierList.length > 1) {
                    $scope.quotationCarrierList = angular.copy(quotation.quotationDetailList[0].quotationCarrierList);

                    goToShipmentModel = $modal({
                        scope: $scope,
                        templateUrl: 'app/components/sales/quotation/view/quotation_carrier_selection.html',
                        show: false
                    });
                    goToShipmentModel.$promise.then(goToShipmentModel.show);
                } else {
                    $scope.quotationToShipment(quotation.quotationDetailList[0].quotationCarrierList[0].id);
                }

            } else {
                $scope.quotationToShipment(quotation.quotationDetailList[0].quotationCarrierId);
            }

        };

        /*Report Related Codes Ends Here*/
        $scope.quotationToShipment = function (quotationCarrierId) {
            isShipmentCreatedFromQuotation.get({
                quotationNo: $scope.quotation.quotationNo
            }, function (data) {
                if (data.responseCode == "ERR0") {
                    console.log("Creating Shipment from Quotation....")
                    $rootScope.clientMessage = null;

                    if ($scope.quotation.customer.isDefaulter) {
                        $rootScope.clientMessage = $rootScope.nls["ERR22161"];
                        return false
                    }

                    if ($scope.quotation.shipper != undefined &&
                        $scope.quotation.shipper != null &&
                        $scope.quotation.shipper.id != null) {
                        if ($scope.quotation.shipper.isDefaulter) {
                            $rootScope.clientMessage = $rootScope.nls["ERR22162"];
                            return false
                        }
                    }


                    $scope.shipment = {};
                    $scope.shipment.id = -1;
                    $scope.shipment.shipmentUid = null;
                    $scope.shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
                    $scope.shipment.createdBy = $scope.quotation.loggedBy;

                    $scope.shipment.location = $rootScope.userProfile.selectedUserLocation;
                    $scope.shipment.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
                    $scope.shipment.country = $rootScope.userProfile.selectedUserLocation.countryMaster;

                    $scope.shipment.shipmentServiceList = [];
                    if ($scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                        for (var i = 0; i < $scope.quotation.quotationDetailList.length; i++) {
                            var shipmentServiceDetail = {};
                            if ($scope.quotation.quotationDetailList[i].hazardous == 'Yes' || $scope.quotation.quotationDetailList[i].hazardous == true) {
                                shipmentServiceDetail.hazardous = true;
                            } else {
                                shipmentServiceDetail.hazardous = false;
                            }

                            shipmentServiceDetail.processInstanceId = $scope.quotation.quotationDetailList[0].processInstanceId;
                            shipmentServiceDetail.taskId = $scope.quotation.quotationDetailList[0].taskId;

                            shipmentServiceDetail.party = $scope.quotation.customer;
                            shipmentServiceDetail.partyAddress = $scope.quotation.customerAddress;
                            //shipmentServiceDetail.partyAddress.id = null;
                            shipmentServiceDetail.tosMaster = $scope.quotation.quotationDetailList[0].tosMaster;
                            shipmentServiceDetail.commodity = $scope.quotation.quotationDetailList[0].commodityMaster;
                            shipmentServiceDetail.directShipment = false;
                            shipmentServiceDetail.salesman = $scope.quotation.salesman;
                            shipmentServiceDetail.agent = $scope.quotation.agent;
                            if ($scope.routedByFlag) {
                                shipmentServiceDetail.whoRouted = $scope.quotation.whoRouted == 'Self' ? false : true;
                            } else {
                                if ($scope.quotation.salesman == undefined || $scope.quotation.salesman == null || $scope.quotation.salesman.id == undefined || $scope.quotation.salesman.id == null) {
                                    shipmentServiceDetail.whoRouted = true;
                                } else {
                                    shipmentServiceDetail.whoRouted = false;
                                }
                            }

                            shipmentServiceDetail.location = $rootScope.userProfile.selectedUserLocation;
                            shipmentServiceDetail.serviceReqDate = new Date();
                            shipmentServiceDetail.quotationUid = $scope.quotation.quotationNo;
                            shipmentServiceDetail.quotationCarrierId = quotationCarrierId;
                            shipmentServiceDetail.shipmentUid = null;
                            shipmentServiceDetail.serviceMaster = $scope.quotation.quotationDetailList[i].serviceMaster;
                            shipmentServiceDetail.customerService = $scope.quotation.loggedBy;
                            shipmentServiceDetail.customerServicePersonEmail = $scope.quotation.loggedBy.email;


                            shipmentServiceDetail.origin = $scope.quotation.quotationDetailList[i].origin;
                            shipmentServiceDetail.pol = $scope.quotation.quotationDetailList[i].origin;
                            shipmentServiceDetail.pod = $scope.quotation.quotationDetailList[i].destination;
                            shipmentServiceDetail.destination = $scope.quotation.quotationDetailList[i].destination;

                            shipmentServiceDetail.ppcc = false;
                            shipmentServiceDetail.holdShipment = false;
                            shipmentServiceDetail.overDimension = false;
                            shipmentServiceDetail.isMinimumShipment = false;
                            shipmentServiceDetail.isAgreed = false;
                            shipmentServiceDetail.isMarksNo = true;

                            shipmentServiceDetail.salesman = $scope.quotation.salesman;
                            shipmentServiceDetail.agent = $scope.quotation.agent;
                            if ($scope.routedByFlag) {
                                shipmentServiceDetail.whoRouted = $scope.quotation.whoRouted == 'Self' ? false : true;
                            } else {
                                if ($scope.quotation.salesman == undefined || $scope.quotation.salesman == null || $scope.quotation.salesman.id == undefined || $scope.quotation.salesman.id == null) {
                                    shipmentServiceDetail.whoRouted = true;
                                } else {
                                    shipmentServiceDetail.whoRouted = false;
                                }
                            }

                            /*shipmentServiceDetail.bookedPieces=$scope.quotation.quotationDetailList[i].totalNoOfPieces;
					shipmentServiceDetail.bookedGrossWeightUnitKg = $scope.quotation.quotationDetailList[i].grossWeight;
					shipmentServiceDetail.bookedVolumeWeightUnitKg  = $scope.quotation.quotationDetailList[i].volumeWeight;
					shipmentServiceDetail.bookedGrossWeightUnitPound = $rootScope.roundOffWeight(Math.round(( shipmentServiceDetail.bookedGrossWeightUnitKg * 2.20462) * 100) / 100);
					shipmentServiceDetail.bookedVolumeWeightUnitPound  = $rootScope.roundOffWeight(Math.round((shipmentServiceDetail.bookedVolumeWeightUnitKg * 2.20462) * 100) / 100);
					shipmentServiceDetail.bookedChargeableUnit = $scope.quotation.quotationDetailList[i].chargebleWeight;

					
					
					if(shipmentServiceDetail.bookedChargeableUnit>0){
						var conversionUnit = null;
						
						if(shipmentServiceDetail.serviceMaster.transportMode==='Air'){
							conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
						}else if(shipmentServiceDetail.serviceMaster.transportMode==='Road'){
							conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
						}else if(shipmentServiceDetail.serviceMaster.transportMode==='Ocean'){
							conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
						}
						
						if(conversionUnit!=null){
							shipmentServiceDetail.bookedVolumeUnitCbm = parseFloat(shipmentServiceDetail.bookedChargeableUnit)/parseFloat(conversionUnit);
							shipmentServiceDetail.bookedVolumeUnitCbm = $rootScope.weightFormat(shipmentServiceDetail.bookedVolumeUnitCbm,shipmentServiceDetail.location.countryMaster.locale);
							
						}
					}else{
						shipmentServiceDetail.bookedVolumeUnitCbm = 0.0;
					}
					
					shipmentServiceDetail.dimensionVolWeight = $scope.quotation.quotationDetailList[i].dimensionVolWeight;
					shipmentServiceDetail.dimensionGrossWeight = $scope.quotation.quotationDetailList[i].dimensionGrossWeight;
					shipmentServiceDetail.dimensionTotalPiece = $scope.quotation.quotationDetailList[i].totalNoOfPieces;
					shipmentServiceDetail.dimensionUnit = $scope.quotation.quotationDetailList[i].dimensionUnit;
					shipmentServiceDetail.dimensionUnitValue = $scope.quotation.quotationDetailList[i].dimensionUnitValue;
	*
	*/

                            shipmentServiceDetail.pickUpDeliveryPoint = {};
                            shipmentServiceDetail.pickUpDeliveryPoint.isOurPickUp = false;
                            shipmentServiceDetail.pickUpDeliveryPoint.transporter = {};
                            shipmentServiceDetail.pickUpDeliveryPoint.pickupPoint = {};
                            shipmentServiceDetail.pickUpDeliveryPoint.pickUpPlace = {};
                            shipmentServiceDetail.pickUpDeliveryPoint.deliveryPlace = {};
                            if ($scope.quotation.quotationDetailList[0].pickupAddress != null) {
                                shipmentServiceDetail.pickUpDeliveryPoint.isOurPickUp = true;
                                shipmentServiceDetail.pickUpDeliveryPoint.pickupPoint = $scope.quotation.customer;
                                shipmentServiceDetail.pickUpDeliveryPoint.pickUpPlace = $scope.quotation.quotationDetailList[0].pickupAddress;
                                if (shipmentServiceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode == undefined) {
                                    shipmentServiceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode = shipmentServiceDetail.pickUpDeliveryPoint.pickUpPlace.poBox;
                                }
                            }
                            if ($scope.quotation.quotationDetailList[0].deliveryAddress != null) {
                                shipmentServiceDetail.pickUpDeliveryPoint.deliveryPlace = $scope.quotation.quotationDetailList[0].deliveryAddress;
                                if (shipmentServiceDetail.pickUpDeliveryPoint.deliveryPlace.zipCode == undefined) {
                                    shipmentServiceDetail.pickUpDeliveryPoint.deliveryPlace.zipCode = shipmentServiceDetail.pickUpDeliveryPoint.deliveryPlace.poBox;
                                }
                            }


                            shipmentServiceDetail.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
                            shipmentServiceDetail.country = $rootScope.userProfile.selectedUserLocation.countryMaster;


                            /*				// Assigning Default Package in Shipment Service Detail configured in appMasterData..
                            				shipmentServiceDetail.packMaster = {}
                            	            var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
                            	            if (defaultPackageCode != undefined && defaultPackageCode != null) {
                            	                console.log("Default Package is assigned....");
                            	                PackGetByCode.get({
                            	                    code: defaultPackageCode
                            	                }, function(data) {
                            	                    if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                            	                        console.log("Successful while getting pack.", data)
                            	                        shipmentServiceDetail.packMaster = data.responseObject;
                            	                    }
                            	                }, function(error) {
                            	                    console.log("Error while getting pack.", error)
                            	                });
                            	            } else {
                            	                console.log("Default Package Code is not defined in Default Master Data....");
                            	            }
                            */
                            shipmentServiceDetail.documentList = [];
                            var document = {};

                            document.origin = $scope.quotation.quotationDetailList[i].origin;
                            document.pol = $scope.quotation.quotationDetailList[i].origin;
                            document.pod = $scope.quotation.quotationDetailList[i].destination;
                            document.destination = $scope.quotation.quotationDetailList[i].destination;


                            document.noOfPieces = $scope.quotation.quotationDetailList[i].totalNoOfPieces;
                            document.dimensionUnit = $scope.quotation.quotationDetailList[i].dimensionUnit;
                            document.dimensionUnitValue = $scope.quotation.quotationDetailList[i].dimensionUnitValue;

                            document.grossWeight = $scope.quotation.quotationDetailList[i].grossWeight;
                            document.volumeWeight = $scope.quotation.quotationDetailList[i].volumeWeight;
                            document.chargebleWeight = $scope.quotation.quotationDetailList[i].chargebleWeight;
                            document.volumeWeightInPound = $rootScope.roundOffWeight(Math.round((document.volumeWeight * 2.20462) * 100) / 100);
                            document.grossWeightInPound = $rootScope.roundOffWeight(Math.round((document.volumeWeight * 2.20462) * 100) / 100);


                            if (document.chargebleWeight > 0) {
                                var conversionUnit = null;

                                if (shipmentServiceDetail.serviceMaster != undefined && shipmentServiceDetail.serviceMaster.id != undefined && shipmentServiceDetail.serviceMaster.transportMode === 'Air') {
                                    conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
                                } else if (shipmentServiceDetail.serviceMaster != undefined && shipmentServiceDetail.serviceMaster != null && shipmentServiceDetail.serviceMaster.transportMode === 'Road') {
                                    conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
                                } else if (shipmentServiceDetail.serviceMaster != undefined && shipmentServiceDetail.serviceMaster != null && shipmentServiceDetail.serviceMaster.transportMode === 'Ocean') {
                                    conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
                                }

                                if (conversionUnit != null) {
                                    document.bookedVolumeUnitCbm = parseFloat(document.volumeWeight) / parseFloat(conversionUnit);
                                    document.bookedVolumeUnitCbm = $rootScope.roundOffWeight(document.bookedVolumeUnitCbm);
                                }
                            } else {
                                document.bookedVolumeUnitCbm = 0.0;
                            }


                            document.company = $scope.quotation.companyMaster;
                            document.country = $scope.quotation.countryMaster;
                            document.location = $scope.quotation.locationMaster;

                            if ($scope.quotation.quotationDetailList[i].serviceMaster != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.id != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.importExport == 'Export') {
                                document.shipper = $scope.quotation.customer;
                                document.shipperAddress = $scope.quotation.customerAddress;
                                //document.shipperAddress.id = null;

                                document.consignee = {};
                                document.consigneeAddress = {};

                            } else if ($scope.quotation.quotationDetailList[i].serviceMaster != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.id != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.importExport == 'Import') {
                                document.consignee = $scope.quotation.customer;
                                document.consigneeAddress = $scope.quotation.customerAddress;
                                document.consigneeAddress.id = null;

                                document.shipper = {};
                                document.shipperAddress = {};
                            }




                            document.firstNotify = {};
                            document.firstNotifyAddress = {};

                            document.secondNotify = {};
                            document.secondNotifyAddress = {};

                            document.forwarder = {};
                            document.forwarderAddress = {};

                            document.agent = {};
                            document.agentAddress = {};


                            document.issuingAgent = {};
                            document.issuingAgentAddress = {};

                            if ($rootScope.userProfile.selectedUserLocation.tmpPartyMaster != undefined &&
                                $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != null &&
                                $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != undefined &&
                                $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != null) {

                                document.issuingAgent = $rootScope.userProfile.selectedUserLocation.tmpPartyMaster;
                                $scope.getPartyAddress(document.issuingAgent, document.issuingAgentAddress);
                            }

                            if ($scope.quotation.quotationDetailList[i].serviceMaster != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.id != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.importExport == 'Export' &&
                                $scope.quotation.quotationDetailList[i].destination != undefined &&
                                $scope.quotation.quotationDetailList[i].destination.id != undefined) {

                                AgentByServicePort.fetch({
                                    "serviceId": $scope.quotation.quotationDetailList[i].serviceMaster.id,
                                    "portId": $scope.quotation.quotationDetailList[i].destination.id
                                }).$promise.then(
                                    function (data) {
                                        if (data.responseCode == "ERR0") {
                                            var agentExport = data.responseObject;
                                            if (agentExport != undefined &&
                                                agentExport != null &&
                                                agentExport.id != undefined) {

                                                document.agent = agentExport.agent;
                                                $scope.getPartyAddress(document.agent, document.agentAddress);
                                            }
                                        }
                                    },
                                    function (errResponse) {
                                        console.error('Error while fetching agent  based on service port');
                                    }

                                );

                            } else if ($scope.quotation.quotationDetailList[i].serviceMaster != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.id != undefined &&
                                $scope.quotation.quotationDetailList[i].serviceMaster.importExport == 'Import' &&
                                $scope.quotation.quotationDetailList[i].origin != undefined &&
                                $scope.quotation.quotationDetailList[i].origin.id != undefined) {

                                AgentByServicePort.fetch({
                                    "serviceId": $scope.quotation.quotationDetailList[i].serviceMaster.id,
                                    "portId": $scope.quotation.quotationDetailList[i].origin.id
                                }).$promise.then(
                                    function (data) {
                                        if (data.responseCode == "ERR0") {
                                            var agentExport = data.responseObject;
                                            if (agentExport != undefined &&
                                                agentExport != null &&
                                                agentExport.id != undefined) {

                                                document.agent = agentExport.agent;
                                                $scope.getPartyAddress(document.agent, document.agentAddress);
                                            }
                                        }
                                    },
                                    function (errResponse) {
                                        console.error('Error while fetching agent  based on service port');
                                    }

                                );

                            }

                            document.carrier = $scope.quotation.quotationDetailList[i].carrierMaster;


                            // Assigning Default Package in Shipment Service Detail configured in appMasterData..
                            shipmentServiceDetail.packMaster = {}
                            var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
                            if (defaultPackageCode != undefined && defaultPackageCode != null) {
                                console.log("Default Package is assigned....");
                                PackGetByCode.get({
                                    code: defaultPackageCode
                                }, function (data) {
                                    if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                                        console.log("Successful while getting pack.", data)
                                        shipmentServiceDetail.packMaster = data.responseObject;
                                        document.packMaster = shipmentServiceDetail.packMaster;
                                    }
                                }, function (error) {
                                    console.log("Error while getting pack.", error)
                                });
                            } else {
                                console.log("Default Package Code is not defined in Default Master Data....");
                            }

                            if ($scope.quotation.quotationDetailList[i].quotationDimensionList != null && $scope.quotation.quotationDetailList[i].quotationDimensionList.length != 0) {
                                document.dimensionList = [];
                                for (var j = 0; j < $scope.quotation.quotationDetailList[i].quotationDimensionList.length; j++) {
                                    var dimension = {};
                                    dimension.length = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].length;
                                    dimension.noOfPiece = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].noOfPiece;
                                    dimension.width = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].width;
                                    dimension.height = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].height;
                                    dimension.volWeight = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].volWeight;
                                    dimension.grossWeight = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].grossWeight;
                                    dimension.grossWeightKg = $scope.quotation.quotationDetailList[i].quotationDimensionList[j].grossWeightKg;
                                    document.dimensionList.push(dimension);
                                }
                                document.dimensionVolumeWeight = $scope.quotation.quotationDetailList[i].dimensionVolWeight;
                                document.dimensionGrossWeight = $scope.quotation.quotationDetailList[i].dimensionGrossWeight;
                                document.dimensionGrossWeightInKg = $scope.quotation.quotationDetailList[i].dimensionGrossWeightInKg;
                                document.grossWeightInPound = $scope.quotation.quotationDetailList[i].dimensionGrossWeight;

                                if (document.dimensionUnit || document.dimensionUnit == 'true') {
                                    var weight = Math.round((parseFloat($scope.quotation.quotationDetailList[i].dimensionVolWeight) * 2.20462) * 100) / 100;
                                    document.volumeWeightInPound = weight;

                                } else {
                                    document.volumeWeightInPound = $scope.quotation.quotationDetailList[i].dimensionVolWeight;
                                }

                            } else {
                                document.dimensionList = [];
                                document.dimensionList.push({});
                            }


                            shipmentServiceDetail.dimensionUnit = document.dimensionUnit;
                            shipmentServiceDetail.dimensionUnitValue = document.dimensionUnitValue;
                            shipmentServiceDetail.bookedPieces = document.noOfPieces;
                            shipmentServiceDetail.bookedGrossWeightUnitKg = document.grossWeight;
                            shipmentServiceDetail.bookedVolumeWeightUnitKg = document.volumeWeight;
                            shipmentServiceDetail.bookedChargeableUnit = document.chargebleWeight;
                            shipmentServiceDetail.bookedGrossWeightUnitPound = document.grossWeightInPound;
                            shipmentServiceDetail.bookedVolumeWeightUnitPound = document.volumeWeightInPound;
                            shipmentServiceDetail.bookedVolumeUnitCbm = document.bookedVolumeUnitCbm;

                            shipmentServiceDetail.documentList.push(document);

                            for (var m = 0; m < $scope.quotation.quotationDetailList[i].quotationCarrierList.length; m++) {

                                if ($scope.quotation.quotationDetailList[i].quotationCarrierList[m].id != quotationCarrierId) {
                                    continue;
                                }

                                shipmentServiceDetail.shipmentChargeList = [];

                                var chargeList = $scope.quotation.quotationDetailList[i].quotationCarrierList[m].quotationChargeList;

                                if (chargeList != null && chargeList.length != 0) {
                                    for (var j = 0; j < chargeList.length; j++) {
                                        var charge = {};
                                        charge.chargeMaster = chargeList[j].chargeMaster;
                                        charge.chargeName = chargeList[j].chargeName;
                                        charge.unitMaster = chargeList[j].unitMaster;

                                        if (charge.chargeMaster.chargeType == "Origin") {
                                            charge.ppcc = "Prepaid";
                                        } else if (charge.chargeMaster.chargeType == "Destination" || charge.chargeMaster.chargeType == "Freight") {
                                            charge.ppcc = "Collect";
                                        } else if (charge.chargeMaster.chargeType == "Other") {
                                            if ($scope.quotation.quotationDetailList[i].serviceMaster != undefined && $scope.quotation.quotationDetailList[i].serviceMaster.id != undefined && $scope.quotation.quotationDetailList[i].serviceMaster.importExport == 'Import') {
                                                charge.ppcc = 'Prepaid';
                                            } else {
                                                charge.ppcc = 'Collect';
                                            }
                                        }
                                        if (shipmentServiceDetail.serviceMaster != undefined && shipmentServiceDetail.serviceMaster != null &&
                                            shipmentServiceDetail.serviceMaster.importExport === 'Import') {
                                            charge.ppcc = 'Prepaid';
                                        } else {
                                            charge.ppcc = 'Collect';
                                        }

                                        charge.numberOfUnit = chargeList[j].numberOfUnit;

                                        charge.grossSaleCurrency = chargeList[j].currencyMaster;
                                        charge.grossSaleAmount = chargeList[j].sellRateAmountPerUnit;
                                        charge.localGrossSaleRoe = chargeList[j].roe;
                                        charge.grossSaleMinimum = chargeList[j].sellRateMinAmount;

                                        charge.netSaleCurrency = chargeList[j].currencyMaster;
                                        charge.netSaleAmount = chargeList[j].sellRateAmountPerUnit;
                                        charge.localNetSaleRoe = chargeList[j].roe;
                                        charge.netSaleMinimum = chargeList[j].sellRateMinAmount;

                                        charge.declaredSaleCurrency = chargeList[j].currencyMaster;
                                        charge.declaredSaleAmount = chargeList[j].buyRateCostPerUnit;
                                        charge.localDeclaredSaleRoe = chargeList[j].roe;
                                        charge.declaredSaleMinimum = chargeList[j].buyRateMinCost;

                                        charge.rateCurrency = chargeList[j].currencyMaster;
                                        charge.rateAmount = chargeList[j].sellRateAmountPerUnit;
                                        charge.localRateAmountRoe = chargeList[j].roe;
                                        charge.rateMinimum = chargeList[j].sellRateMinAmount;

                                        charge.costCurrency = chargeList[j].currencyMaster;
                                        charge.costAmount = chargeList[j].buyRateCostPerUnit;
                                        charge.localCostAmountRoe = chargeList[j].roe;
                                        charge.costMinimum = chargeList[j].buyRateMinCost;

                                        charge.actualChargeable = chargeList[j].actualchargeable;

                                        charge.quotationId = $scope.quotation.id;
                                        shipmentServiceDetail.shipmentChargeList.push(charge);


                                    }
                                } else {
                                    shipmentServiceDetail.shipmentChargeList = [{}];
                                }

                                shipmentServiceDetail.isAllInclusiveCharges = $scope.quotation.quotationDetailList[i].quotationCarrierList[m].isAllInclusiveCharges;
                                shipmentServiceDetail.carrier = $scope.quotation.quotationDetailList[i].quotationCarrierList[m].carrierMaster;

                                break;
                            }




                            shipmentServiceDetail.attachConfig = {
                                "isEdit": true, //false for view only mode
                                "isDelete": true, //
                                "page": "shipment",
                                "columnDefs": [

                                    {
                                        "name": "Document Name",
                                        "model": "documentMaster.documentName",
                                        "type": "text"
                                    },
                                    {
                                        "name": "Reference #",
                                        "model": "refNo",
                                        "type": "text"
                                    },
                                    {
                                        "name": "Customs",
                                        "model": "customs",
                                        "type": "text"
                                    },
                                    {
                                        "name": "Unprotected File Name",
                                        "model": "unprotectedFileName",
                                        "type": "file"
                                    },
                                    {
                                        "name": "Protected File Name",
                                        "model": "protectedFileName",
                                        "type": "file"
                                    },
                                    {
                                        "name": "Actions",
                                        "model": "action",
                                        "type": "action"
                                    }
                                ],
                                "data": []
                            }



                            shipmentServiceDetail.eventList = [];
                            shipmentServiceDetail.shipmentServiceTriggerList = [];
                            shipmentServiceDetail.connectionList = [{
                                connectionStatus: 'Planned'
                            }];
                            shipmentServiceDetail.shipmentAttachmentList = [];
                            shipmentServiceDetail.referenceList = [];
                            shipmentServiceDetail.authenticatedDocList = [{}];

                            $scope.shipment.shipmentServiceList.push(shipmentServiceDetail);
                        }
                    }
                    console.log("Shipment to be created ", $scope.shipment);
                    $rootScope.createShipment = $scope.shipment;
                    var params = {};
                    params.action = 'CREATE';
                    params.fromScreen = $state.current.name;
                    params.fromState = $state.current.name;
                    params.fromStateParams = JSON.stringify($stateParams);

                    $state.go('layout.addNewShipment', params);
                } else {
                    Notification.warning($rootScope.nls["ERR90700"]);
                }
            }, function (error) {
                console.log("enquiryToQuotation view Failed : " + error)
            });
        }

        $scope.getPartyAddress = function (obj, mapToAddrss) {
            if (obj != null && obj.id != null) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressMapping(obj.partyAddressList[i], obj.countryMaster, mapToAddrss);
                    }
                }
            }

        }


        $scope.addressMapping = function (partyAddress, countryMaster, mapToAddrss) {

            if (mapToAddrss == null || mapToAddrss == undefined) {
                mapToAddrss = {};
            }

            mapToAddrss.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox + ", " + partyAddress.addressLine1 : partyAddress.addressLine1;
            mapToAddrss.addressLine2 = partyAddress.addressLine2;
            mapToAddrss.addressLine3 = partyAddress.addressLine3;

            var addressLine4 = "";
            if (partyAddress.cityMaster !== null) {
                addressLine4 = addressLine4 + partyAddress.cityMaster.cityName;
            }

            if (partyAddress.zipCode != null) {
                if (addressLine4 != "")

                    addressLine4 = addressLine4 + " - ";
                addressLine4 = addressLine4 + partyAddress.zipCode;
            }

            if (partyAddress.stateMaster != null) {
                if (addressLine4 != "")
                    addressLine4 = addressLine4 + ", ";
                addressLine4 = addressLine4 + partyAddress.stateMaster.stateName;

            }

            if (countryMaster != null) {
                if (addressLine4 != "")
                    addressLine4 = addressLine4 + ", ";
                addressLine4 = addressLine4 + countryMaster.countryName;

            }

            if (mapToAddrss.addressLine3 == null || mapToAddrss.addressLine3 == '') {
                mapToAddrss.addressLine3 = addressLine4;
                mapToAddrss.addressLine4 = null;
            } else {
                mapToAddrss.addressLine4 = addressLine4;
            }

            mapToAddrss.phone = partyAddress.phone;
            mapToAddrss.mobile = partyAddress.mobileNo;
            mapToAddrss.email = partyAddress.email;


        }

        $scope.approve = function () {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_APPROVE_MODIFY)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR224"];
                newScope.errorMessage1 = {};
                newScope.errorMessage2 = {};
                if (newScope.errorMessage != undefined) {
                    var tmp = newScope.errorMessage.split('?');
                    newScope.errorMessage1 = tmp[0] + '?';
                    newScope.errorMessage2 = tmp[1];
                }
                console.log(newScope.errorMessage1);
                console.log(newScope.errorMessage2);

                ngDialog.openConfirm({
                    template: '<p>{{errorMessage1}}<br>{{errorMessage2}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    closeByDocument: false,
                    className: 'ngdialog-theme-default'
                }).
                    then(function (value) {
                        $scope.apporveApiCall("Approved");
                    }, function (value) {
                        console.log("approve quotation cancelled");

                    });



            }

        }

        $scope.lost = function () {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_LOST_MODIFY)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR225"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).
                    then(function (value) {
                        $scope.apporveApiCall("Rejected");
                    }, function (value) {
                        console.log("approve quotation cancelled");

                    });
            }
        }

        $scope.apporveApiCall = function (status) {

            $scope.tmpObj = {};
            $scope.tmpObj.quotationId = $scope.quotation.id;
            $scope.tmpObj.approved = status;
            $scope.tmpObj.employeeId = $rootScope.userProfile.employee.id;
            $scope.tmpObj.approvedDate = $rootScope.sendApiDateAndTime(new Date());

            QuotationApprove.update($scope.tmpObj).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        console.log("Quotation Sataus changed Successfully")
                        $scope.view(data.responseObject, false);
                    } else {
                        console.log("Quotation Sataus changed Failed " + data.responseDescription)
                    }
                },
                function (error) {
                    console.log("Quotation Sataus changed Failed : " + error)
                });

        };

        $scope.toggleHistory = function () {
            console.log("toggle History is called..");
            $scope.showHistory = !$scope.showHistory;
        }

        $scope.closeHistory = function () {
            console.log("Close History is called..");
            $scope.showHistory = false;
        }

        $scope.openDefaultTabs = function () {
            console.log("openDefaultTabs is called.....");
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VIEW)) {
                $scope.detailTab = 'Services';
                $scope.moreAddressTab = 'chargeBtn'
            }
        }


        $scope.showChargeHistory = function (id) {
            /*var searchDto={};
		searchDto.selectedPageNumber =$scope.page;
		searchDto.recordPerPage =$scope.limit;*/
            var auditArr = [];
            $http({
                method: 'GET',
                url: '/api/v1/quotation/getquotation/chargehistory/' + id
            }).then(function (response) {
                if (response.data.responseCode == "ERR0") {
                    $scope.auditArr = [];
                    $scope.auditArr = response.data.responseObject;
                    var chargeModal;
                    chargeModal = $modal({
                        scope: $scope,
                        backdrop: 'static',
                        templateUrl: '/app/shared/inlineEditTables/charge_quotation/charge_quotation_history.html',
                        show: false
                    });
                    chargeModal.$promise.then(chargeModal.show)
                } else if (response.data.responseCode == "ERR0700012") {
                    Notification.error($rootScope.nls["ERR0700012"]);
                }
            });
        }

        $scope.enableQuotationEditFlag = function () {
            $scope.editQuotationFlag = false;

            if ($scope.quotation.approved == 'Pending') {
                $scope.editQuotationFlag = true;
            } else if ($scope.quotation.approvedBy.id == $rootScope.userProfile.employee.id &&
                $scope.quotation.approved == 'Approved') {
                $scope.editQuotationFlag = true;
            } else if (($rootScope.appMasterData['quotation.client.approval.required'].toUpperCase() == 'FALSE') &&
                $scope.quotation.approvedBy.id == $rootScope.userProfile.employee.id &&
                $scope.quotation.approved == 'Gained') {
                $scope.editQuotationFlag = true;
            } else if ($scope.quotation.approved == 'Rejected' &&
                $scope.quotation.approvedBy.id == $rootScope.userProfile.employee.id) {
                $scope.editQuotationFlag = true;
            } else {
                $scope.editQuotationFlag = false;
            }
        }


        $scope.isObjFound = function (obj) {

            if (obj == undefined || obj == null || obj.length == undefined || EmptyRowChecker.isEmptyRow(obj[0])) {
                return false;
            } else {
                return true;
            }
        }

        $scope.isValueAddedFound = function (obj) {

            if (obj == undefined || obj == null || obj.length == undefined || EmptyRowChecker.isEmptyRow(obj[0])) {
                return false;
            } else {
                return true;
            }
        }

        $scope.isPickUpDelFound = function (obj) {

            if (obj == undefined || obj == null || obj.pickupAddress == undefined || obj.pickupAddress == null ||
                obj.deliveryAddress == undefined || obj.deliveryAddress == null || EmptyRowChecker.isEmptyRow(obj.pickupAddress) || EmptyRowChecker.isEmptyRow(obj.deliveryAddress)) {
                return false;
            } else {
                return true;
            }
        }


        $scope.accessCheck = function (value, nextIdFocus) {

            if (value == 'pickupBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_PICKUP_DELIVERY_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.pickupBtn = false;
                }

            }
            if (value == 'chargeBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CHARGE_VIEW)) {
                    //$rootScope.navigateToNextField(nextIdFocus);
                    $scope.selectMultiCarrierModel(0)
                    return true;
                } else {
                    $scope.chargeBtn = false;
                }

            }
            if (value == 'dimensionBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_DIMENSION_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.dimensionBtn = false;
                }

            }

            if (value == 'notesBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.notesBtn = false;
                }

            }

            if (value == 'valueAdded') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VALUE_ADDED_SERVICE_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.valueAdded = false;
                }

            }
        }



        switch ($stateParams.action) {
            case "VIEW":
                console.log("I am In Quotation View Page");
                $rootScope.unfinishedFormTitle = "Quotation View # " + $stateParams.quotationId;
                $rootScope.unfinishedData = undefined;

                if ($stateParams.quotationId != undefined && $stateParams.quotationId != null) {
                    $scope.view($stateParams.quotationId);
                } else if ($stateParams.quotationNo != undefined && $stateParams.quotationNo != null) {
                    $scope.viewByNo($stateParams.quotationNo);
                }

                if ($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
                    $scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
                }
                if ($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
                    $scope.totalRecord = parseInt($stateParams.totalRecord);
                }




                $rootScope.breadcrumbArr = [{
                    label: "Sales",
                    state: "layout.salesQuotation"
                }, {
                    label: "Quotation",
                    state: "layout.salesQuotation"
                }, {
                    label: "View Quotation",
                    state: null
                }];
                break;
            case "SEARCH":
                console.log("I am In Quotation List Page");
                $scope.init();
                $rootScope.breadcrumbArr = [{
                    label: "Sales",
                    state: "layout.salesQuotation"
                }, {
                    label: "Quotation",
                    state: null
                }];
                break;
            default:

        }

    }
]);