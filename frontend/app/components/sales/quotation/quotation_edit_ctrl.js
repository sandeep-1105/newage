/**
 * Created by hmspl on 5/5/16.
 */
app.controller("quotationEditCtrl", ['$rootScope', 'ngDialog', '$http', '$scope', '$location', '$modal',
    'quotationService', 'EmptyRowChecker', 'PartiesList', 'EmployeeList', 'SalesmanListOnLocation', 'GeneralNote',
    'ServiceByTransportMode', 'PortSearchKeyword', 'TosSearchKeyword', 'CommoditySearchKeyword', 'CarrierByTransportMode',
    'ChargeSearchKeyword', 'CurrencyMasterSearchKeyword', 'UnitSearchKeyword',
    'QuotationAdd', 'QuotationUpdate', 'ValidateUtil', 'discardService', 'cloneService', 'PortByTransportMode',
    'PeriodList', 'RecentHistorySaveService', 'QuotationAttachment', 'PartyMasterService', 'QuotationView', '$state',
    '$stateParams', 'appMetaFactory', '$timeout', 'appConstant', 'CommonValidationService', 'addressJoiner',
    'QuotationSearch', 'Notification', 'ngProgressFactory', 'valueAddedServicesFactory', 'UnitMasterGetByCode',
    'AutoCompleteService', 'roleConstant', 'GetSurcharge', 'downloadFactory',
    function($rootScope, ngDialog, $http, $scope, $location, $modal,
        quotationService, EmptyRowChecker, PartiesList, EmployeeList, SalesmanListOnLocation, GeneralNote,
        ServiceByTransportMode, PortSearchKeyword, TosSearchKeyword, CommoditySearchKeyword, CarrierByTransportMode,
        ChargeSearchKeyword, CurrencyMasterSearchKeyword, UnitSearchKeyword,
        QuotationAdd, QuotationUpdate, ValidateUtil, discardService, cloneService, PortByTransportMode,
        PeriodList, RecentHistorySaveService, QuotationAttachment, PartyMasterService, QuotationView, $state,
        $stateParams, appMetaFactory, $timeout, appConstant, CommonValidationService, addressJoiner,
        QuotationSearch, Notification, ngProgressFactory, valueAddedServicesFactory, UnitMasterGetByCode,
        AutoCompleteService, roleConstant, GetSurcharge, downloadFactory) {
        $scope.$AutoCompleteService = AutoCompleteService;
        $scope.$roleConstant = roleConstant;
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('top-quotations-panel'));
        $scope.contained_progressbar.setAbsolute();
        $scope.contained_progressbar.start();

        var routedByDisplayed = $rootScope.appMasterData['quotation.routed.enable'];
        if (routedByDisplayed != undefined && routedByDisplayed.toLowerCase() == 'true' || routedByDisplayed == true) {
            $scope.routedByFlag = true;
        } else {
            $scope.routedByFlag = false;
        }


        $scope.init = function() {

            console.log("Quotation Edit Controller is Activated.................................");


            $scope.isOverDimension = false;
            $scope.firstFocus = true;
            $scope.limit = 100;
            $scope.showEditDimension = false;
            $scope.showEditCharge = false;
            $scope.tableDimensionState = true;
            if ($scope.quotation.id == null) {
                $scope.quotation.locationMaster = $rootScope.userProfile.selectedUserLocation;
                $scope.quotation.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
                $scope.quotation.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                $scope.quotation.customer = {};

                $scope.quotation.shipper = {};
                $scope.quotation.salesCoordinator = {};
                $scope.quotation.salesman = {};
                $scope.quotation.loggedBy = $rootScope.userProfile.employee;
                $scope.quotation.whoRouted = false;
                $scope.quotation.approvedBy = {};
                $scope.quotation.customerAddress = {};
                $scope.quotation.shipperAddress = {};
                $scope.quotation.loggedOn = $rootScope.dateToString(new Date());

                $scope.quotation.quotationDetailList = [];
                $scope.quotation.generalNoteList = [];

                $scope.quotation.quoteType = 'GENERAL';
                $scope.quotation.validFrom = $rootScope.dateToString(new Date());
                $scope.quotation.validTo = $rootScope.dateToString(moment().add('days', $rootScope.appMasterData['quotation.valid.days']));


                valueAddedServicesFactory.getAllValue.query().$promise.then(function(data, status) {
                    $scope.quotationDetail = {};
                    $scope.quotationDetail.serviceMaster = {};
                    $scope.quotationDetail.origin = {};
                    $scope.quotationDetail.destination = {};
                    $scope.quotationDetail.tosMaster = {};
                    $scope.quotationDetail.carrierMaster = {};
                    $scope.quotationDetail.commodityMaster = {};
                    $scope.quotationDetail.frequency = {};
                    $scope.quotationDetail.hazardous = false;

                    if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                        $scope.quotationDetail.dimensionUnit = true;
                        $scope.quotationDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                    } else {
                        $scope.quotationDetail.dimensionUnit = false;
                        $scope.quotationDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                    }

                    $scope.quotationDetail.quotationCarrierList = [];
                    $scope.quotationDetail.quotationCarrierList.push({});
                    $scope.quotationDetail.quotationCarrierList[0].quotationChargeList = [];
                    $scope.quotationDetail.quotationCarrierList[0].quotationChargeList.push({});
                    $scope.quotationDetail.quotationDimensionList = [];
                    $scope.quotationDetail.quotationDimensionList.push({});
                    $scope.quotationDetail.quotationContainerList = [];
                    $scope.quotationDetail.serviceNoteList = [];
                    $scope.quotationDetail.quotationValueAddedTmpServiceList = [];

                    $scope.fnDimensionUnit();
                    $scope.quotation.quotationAttachementList = [];
                    //$scope.quotation.quotationAttachementList.push({});
                    $scope.quotationDetail.volumeWeight = "";
                    $scope.quotationDetail.grossWeight = "";
                    $scope.quotationDetail.chargebleWeight = "";

                    $scope.quotationDetail.quotationValueAddedTmpServiceList = data.responseObject.searchResult;

                }, function(response) {
                    console.log("Error found in Quotation Value Added");
                });




            } else {

                if ($scope.quotation.quotationAttachementList == undefined || $scope.quotation.quotationAttachementList == null || $scope.quotation.quotationAttachementList.length == 0) {
                    $scope.quotation.quotationAttachementList = [];
                    //$scope.quotation.quotationAttachementList.push({});
                }

                if ($scope.quotation.id == -1) { //Enquiry to Quotation, we are passing id = -1, 
                    $scope.quotation.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
                    $scope.quotation.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                    $scope.quotation.customerAddress = {};
                    $scope.quotation.shipperAddress = {};
                    $scope.getPartyAddress('CUSTOMER', $scope.quotation.customer);
                    $scope.getPartyAddress('SHIPPER', $scope.quotation.shipper);
                }

                $scope.quotation.whoRouted = $scope.quotation.whoRouted == 'Self' ? false : true;
                $scope.quotationDetail = $scope.quotation.quotationDetailList[0];


                if ($scope.quotation != undefined && $scope.quotationDetail != undefined && $scope.quotationDetail != null) {

                    if ($scope.quotationDetail.quotationCarrierList == undefined || $scope.quotationDetail.quotationCarrierList == null || $scope.quotationDetail.quotationCarrierList.length == 0) {
                        $scope.quotationDetail.quotationCarrierList = [];
                        $scope.quotationDetail.quotationCarrierList.push({});
                        $scope.quotationDetail.quotationCarrierList[0].quotationChargeList = [];
                        $scope.quotationDetail.quotationCarrierList[0].quotationChargeList.push({});
                    }

                    if ($scope.quotationDetail.quotationDimensionList == undefined || $scope.quotationDetail.quotationDimensionList == null || $scope.quotationDetail.quotationDimensionList.length == 0) {
                        $scope.quotationDetail.quotationDimensionList = [];
                        $scope.quotationDetail.quotationDimensionList.push({});
                    }
                }


                if ($scope.quotation != undefined && $scope.quotation != null && $scope.quotation.generalNoteList != undefined &&
                    $scope.quotation.generalNoteList != null) {
                    if ($scope.quotation.generalNoteList.length == 0) {
                        $scope.addNote();
                    } else {
                        console.log('not added geneal notes');
                    }
                }
                if ($scope.quotationDetail != undefined && $scope.quotationDetail != null &&
                    $scope.quotationDetail.serviceNoteList != undefined && $scope.quotationDetail.serviceNoteList != null) {
                    if ($scope.quotationDetail.serviceNoteList.length == 0) {
                        $scope.addAirNote();
                    } else {
                        console.log('not added air note');
                    }
                }

                $scope.quotationDetail.dimensionUnit = $scope.quotation.quotationDetailList[0].dimensionUnit;
                $scope.quotationDetail.hazardous = $scope.quotation.quotationDetailList[0].hazardous == 'No' ? false : true;

                //$scope.fnCalculateChargeableWeight();
                $scope.isOverDimension = checkOverDimension();

            }


            // Open the default tabs
            $scope.openDefaultTabs();


            //get default note while creating
            $scope.getDefaultNote();


            if ($scope.quotation.headerNote == null || $scope.quotation.headerNote == "") {
                $scope.quotation.headerNote = "Thanks for your enquiry. Please find the rates as per our discussion.";
            }

            if ($scope.quotation.footerNote == null || $scope.quotation.footerNote == "") {
                $scope.quotation.footerNote = "Regards,\n" + $rootScope.userProfile.employee.employeeName;
            }


            $timeout(function() {
                console.log("Setting Old Data", new Date());
                $scope.oldData = JSON.stringify($scope.quotation);
            }, 300);

        }

        $scope.openTab = function(event, tabName) {
            if (!event.shiftKey && event.keyCode == 9) {
                if (tabName == 'charge') {
                    $scope.chargeBtn = true;
                    $rootScope.navigateToNextField('-1chargeCodeField0');
                } else if (tabName == 'notes') {
                    $scope.notesBtn = true;
                    $scope.createDefaultAirNote();
                    $rootScope.navigateToNextField('airNote0');
                }
            }
        }

        $scope.pupulateQuotationChargeList = function(obj) {

            if (obj.selected) {

                for (m = 0; m < $scope.quotationDetail.quotationCarrierList.length; m++) {

                    var chargeList = $scope.quotationDetail.quotationCarrierList[m].quotationChargeList;

                    if (chargeList != undefined && chargeList.length > 0) {
                        for (var i = 0; i < chargeList.length; i++) {
                            if (EmptyRowChecker.isEmptyRow(chargeList[i])) {
                                chargeList.splice(i, 1);
                            }
                        }
                    }


                    if (obj.valueAddedChargeList != undefined && obj.valueAddedChargeList.length != 0) {
                        for (var i = 0; i < obj.valueAddedChargeList.length; i++) {

                            var isFound = false;

                            if (chargeList != undefined &&
                                chargeList != null &&
                                chargeList.length != 0) {
                                for (var k = 0; k < chargeList.length; k++) {

                                    if (chargeList[k].chargeMaster != undefined &&
                                        chargeList[k].chargeMaster.id != null &&
                                        chargeList[k].chargeMaster.id == obj.valueAddedChargeList[i].chargeMaster.id) {
                                        chargeList[k].valueAddedId = obj.id;
                                        isFound = true;
                                        break;
                                    }

                                }
                            }

                            if (isFound) {
                                continue;
                            }

                            var quotationCharge = {};
                            quotationCharge.chargeMaster = obj.valueAddedChargeList[i].chargeMaster;
                            $scope.makeQuotationCharge(quotationCharge);
                            quotationCharge.valueAddedId = obj.id;
                            if (chargeList) chargeList.push(quotationCharge);
                        }
                    }

                }


            } else {

                for (m = 0; m < $scope.quotationDetail.quotationCarrierList.length; m++) {

                    var chargeList = $scope.quotationDetail.quotationCarrierList[m].quotationChargeList;

                    var quoteChargeList = [];
                    if (chargeList != undefined && chargeList.length > 0) {
                        for (var i = 0; i < chargeList.length; i++) {
                            if (chargeList[i].valueAddedId == undefined ||
                                chargeList[i].valueAddedId == null ||
                                chargeList[i].valueAddedId != obj.id) {
                                quoteChargeList.push(chargeList[i]);
                            }
                        }
                    }
                    $scope.quotationDetail.quotationCarrierList[m].quotationChargeList = quoteChargeList;
                }


            }

        }


        $scope.makeQuotationCharge = function(charge) {
            if (charge.chargeMaster != undefined && charge.chargeMaster != null) {
                charge.chargeName = charge.chargeMaster.chargeName;
                if (charge.chargeMaster.calculationType != undefined && charge.chargeMaster.calculationType != null) {
                    var unitCode;
                    if (charge.chargeMaster.calculationType == 'Percentage') {
                        unitCode = 'PER';
                    } else if (charge.chargeMaster.calculationType == 'Shipment') {
                        unitCode = 'SHPT';
                    } else if (charge.chargeMaster.calculationType == 'Unit') {
                        unitCode = 'KG';
                        charge.actualchargeable = 'CHARGEABLE';
                    } else if (charge.chargeMaster.calculationType == 'Document') {
                        unitCode = 'DOC';
                    } else {

                    }
                    UnitMasterGetByCode.get({
                        unitCode: unitCode
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            charge.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : ", error)
                    });
                }
            }
            charge.currencyMaster = $scope.quotation.localCurrency;
            var sellRate = null;
            if ($rootScope.baseCurrenyRate[charge.currencyMaster.id] != null && $scope.quotation.localCurrency.id != charge.currencyMaster.id) {
                sellRate = $rootScope.baseCurrenyRate[charge.currencyMaster.id].csell;
            } else if (charge.currencyMaster.id == $scope.quotation.localCurrency.id) {
                sellRate = 1;
            }
            charge.roe = sellRate;

            $scope.isSurchargeAvailable(charge);
        };

        $scope.isSurchargeAvailable = function(charge) {
            charge.surChargeFlag = false;
            GetSurcharge.get({
                chargeId: charge.chargeMaster.id
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    if (data.responseObject == true) {
                        charge.surChargeFlag = true;
                        if ($scope.quotation.customer != null && $scope.quotation.customer != undefined && $scope.quotation.customer != '') {
                            if (($scope.quotation.customer.partyDetail != null &&
                                    $scope.quotation.customer.partyDetail != undefined) &&
                                $scope.quotation.customer.partyDetail.billingFscScOnGrossWeight == true) {
                                charge.actualchargeable = 'ACTUAL';
                            } else {
                                charge.actualchargeable = 'CHARGEABLE';
                            }
                        }

                    }

                } else {
                    console.log("GetSurcharge Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("GetSurcharge Failed : " + error)
            });

        }


        $scope.cancel = function() {

            if ($scope.oldData != JSON.stringify($scope.quotation)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    closeByDocument: false,
                    className: 'ngdialog-theme-default'
                }).then(
                    function(value) {
                        if (value == 1 && $scope.quotation.id == null) {
                            $scope.saveQuotation();

                        } else if (value == 1 && $scope.quotation.id != null) {

                            $scope.updateQuotation();
                        } else if (value == 2) {
                            var params = {};
                            params.submitAction = 'Cancelled';
                            if ($stateParams != undefined && $stateParams.bkState != undefined && $stateParams.bkState != null) {

                                if ($stateParams.bkParam != undefined && $stateParams.bkParam != null) {
                                    if ($stateParams.bkParam == 'activiti') {
                                        $state.go($stateParams.bkState, params);
                                    }
                                }

                            } else {
                                if ($scope.quotation.id == -1) {
                                    $state.go('layout.salesEnquiry', params);
                                } else {
                                    $state.go('layout.salesQuotation', params);
                                }
                            }
                        } else {
                            console.log("Quotation cancel was cancelled..")
                        }

                    });

            } else {

                var params = {};
                params.submitAction = 'Cancelled';

                if ($stateParams != undefined && $stateParams.bkState != undefined && $stateParams.bkState != null) {

                    if ($stateParams.bkParam != undefined && $stateParams.bkParam != null) {
                        if ($stateParams.bkParam == 'activiti') {
                            $state.go($stateParams.bkState, params);
                        }
                    }

                } else {
                    if ($scope.quotation.id == -1) {
                        $state.go('layout.salesEnquiry', params);
                    } else {
                        $state.go('layout.salesQuotation', params);
                    }
                }
            }
        }

        $scope.showResponse = function(quotation, isAdded) {
            $rootScope.mainpreloder = false;
            var newScope = $scope.$new();
            var quotationNo = quotation.quotationNo;
            newScope.errorMessage = isAdded == true ? $rootScope.nls["ERR215"] : $rootScope.nls["ERR216"];
            if (newScope.errorMessage != undefined) {
                newScope.errorMessage = newScope.errorMessage.replace("%s", quotationNo);
            }

            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                var param = {
                    quotationId: quotation.id,
                    submitAction: 'Saved'
                };
                $state.go('layout.salesQuotationView', param);
            }, function(value) {
                console.log("deleted cancelled");
            });

        }

        $scope.$on('addSalesQuotationEventMenuNavi', function(events, moveToScreen) {
            $scope.move(moveToScreen);
        });


        $scope.$on('editSalesQuotationEventMenuNavi', function(events, moveToScreen) {
            $scope.move(moveToScreen);
        });

        $scope.move = function(moveToScreen) {

            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"]
            if ($scope.quotationForm.$dirty) {
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-primary btn-property cancel-btn " ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-primary btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.moveToScreen = moveToScreen;
                        if ($scope.quotation.id != null) {
                            $scope.updateQuotation();
                        } else {
                            $scope.saveQuotation();
                        }
                    } else if (value == 2) {
                        $state.go(moveToScreen);
                    } else if (value == 3) {
                        //$state.go(args);
                        console.log("cancelled");
                    } else {
                        console.log("nothing selected")
                    }
                }, function(error) {
                    console.log('error occurred');
                });
            } else {
                console.log("No Dirty" + moveToScreen);
                $state.go(moveToScreen);
            }

        }

        var addCarrierModal = null;
        $scope.multiCarrierList = null;

        $scope.addMultiCarrierModel = function() {
            $scope.multiCarrierList = null;
            var newScope = $scope.$new();
            $scope.multiCarrierList = angular.copy($scope.quotationDetail.quotationCarrierList);
            $scope.selectedCarrierIndex = 0;
            addCarrierModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/sales/quotation/view/quotation_add_multi_carrier_tabpopup.html',
                show: false
            });
            $rootScope.navigateToNextField('0carrier');
            addCarrierModal.$promise.then(addCarrierModal.show);
        };

        $scope.selectMultiCarrierModel = function(index) {
            $scope.selectedCarrierIndex = index;
        }


        $scope.saveMultiCarrierModel = function() {

            var flag = false;
            for (var i = 0; i < $scope.multiCarrierList.length; i++) {
                flag = false
                if ($scope.multiCarrierList[i].ischargeCheck()) {
                    flag = true;
                }
            }

            if (flag) {
                $scope.quotationDetail.quotationCarrierList = angular.copy($scope.multiCarrierList);
                addCarrierModal.$promise.then(addCarrierModal.hide);
            }


        };

        $scope.addCarrierRow = function(tmpObj) {

            var obj = {};
            obj.quotationChargeList = [];
            obj.quotationChargeList.push({});

            tmpObj.push(obj);

            $scope.selectMultiCarrierModel(tmpObj.length - 1);

        }

        $scope.deleteCarrierRow = function(tmpObj, index) {

            tmpObj.splice(index, 1);

            if (tmpObj == undefined || tmpObj == null || tmpObj.length == 0) {
                $scope.addCarrierRow(tmpObj);
            } else {
                $scope.selectMultiCarrierModel(index - 1);
            }

        }

        $scope.quotationRateCalculation = function(quotationCarrier) {


            var lumpSum = 0.0;
            var lumpSumBuyAmt = 0.0;

            for (var i = 0; i < quotationCarrier.quotationChargeList.length; i++) {
                var charge = quotationCarrier.quotationChargeList[i];

                if (charge.unitMaster != undefined && charge.unitMaster != null && charge.roe != undefined && charge.roe != null) {

                    charge.estimatedUnit = 1;

                    if (charge.unitMaster.unitType == 'Unit') {
                        if (charge.actualchargeable == 'CHARGEABLE') {
                            charge.estimatedUnit = $scope.quotationDetail.chargebleWeight;
                        } else if (charge.actualchargeable == 'ACTUAL') {
                            charge.estimatedUnit = $scope.quotationDetail.grossWeight;
                        }
                    }


                    if (charge.sellRateAmountPerUnit != undefined && charge.sellRateAmountPerUnit != null) {
                        charge.sellRateAmount = charge.estimatedUnit * charge.sellRateAmountPerUnit;
                    }

                    if (charge.sellRateMinAmount != undefined && charge.sellRateMinAmount != null) {
                        if (charge.sellRateAmount <= charge.sellRateMinAmount) {
                            charge.sellRateAmount = charge.sellRateMinAmount;
                        }
                    }

                    if (charge.sellRateAmount != undefined && charge.sellRateAmount != null) {
                        charge.sellRateLocalAmount = charge.roe * charge.sellRateAmount;
                    }

                    if (charge.buyRateCostPerUnit != undefined && charge.buyRateCostPerUnit != null) {
                        charge.buyRateCostAmount = charge.estimatedUnit * charge.buyRateCostPerUnit;
                    }

                    if (charge.buyRateMinCost != undefined && charge.buyRateMinCost != null) {
                        if (charge.buyRateCostAmount <= charge.buyRateMinCost) {
                            charge.buyRateCostAmount = charge.buyRateMinCost;
                        }
                    }

                    if (charge.buyRateCostAmount != undefined && charge.buyRateCostAmount != null) {
                        charge.buyRateCostLocalAmount = charge.roe * charge.buyRateCostAmount;
                    }

                    if (charge.sellRateAmount != undefined && charge.sellRateAmount != null) {
                        charge.sellRateAmount = $rootScope.currency(charge.currencyMaster, charge.sellRateAmount);
                    }

                    if (charge.sellRateLocalAmount != undefined && charge.sellRateLocalAmount != null) {
                        charge.sellRateLocalAmount = $rootScope.currency($scope.quotation.localCurrency, charge.sellRateLocalAmount);
                    }

                    if (charge.buyRateCostAmount != undefined && charge.buyRateCostAmount != null) {
                        charge.buyRateCostAmount = $rootScope.currency(charge.currencyMaster, charge.buyRateCostAmount);
                    }

                    if (charge.buyRateCostLocalAmount != undefined && charge.buyRateCostLocalAmount != null) {
                        charge.buyRateCostLocalAmount = $rootScope.currency($scope.quotation.localCurrency, charge.buyRateCostLocalAmount);
                    }

                    lumpSum = lumpSum + charge.sellRateLocalAmount;
                    lumpSumBuyAmt = lumpSumBuyAmt + charge.buyRateCostLocalAmount;
                } else {
                    charge.sellRateAmount = null;
                    charge.sellRateLocalAmount = null;
                    charge.buyRateAmount = null;
                    charge.buyRateLocalAmount = null;

                }

            }
            if (lumpSum > 0) {
                quotationCarrier.lumpSum = $rootScope.currency($scope.quotation.localCurrency, lumpSum);
                quotationCarrier.perKg = $rootScope.currency($scope.quotation.localCurrency, (lumpSum / $scope.quotationDetail.chargebleWeight));
            }

            if (lumpSumBuyAmt > 0) {
                quotationCarrier.lumpSumBuyAmt = $rootScope.currency($scope.quotation.localCurrency, lumpSumBuyAmt);
                quotationCarrier.perKgBuy = $rootScope.currency($scope.quotation.localCurrency, (lumpSumBuyAmt / $scope.quotationDetail.chargebleWeight));
            }
        }


        $scope.saveQuotation = function() {
            $rootScope.clientMessage = null;

            if ($scope.quotationDimension.isDimensionCheck()) {

                for (var i = 0; i < $scope.quotationDetail.quotationCarrierList.length; i++) {

                    if ($scope.validateQuotationCarrier(0, $scope.quotationDetail.quotationCarrierList[i], i) == false) {
                        return false;
                    }

                    var flag = false;
                    if (angular.isFunction($scope.quotationDetail.quotationCarrierList[i].ischargeCheck)) {
                        if ($scope.quotationDetail.quotationCarrierList[i].ischargeCheck()) {
                            flag = true;
                        }
                    } else {
                        flag = true;
                    }

                    if (flag) {

                        if ($scope.validateQuotation(0)) {

                            console.log("Validation Quotation is done....")

                            if ($scope.validateQuotationDetail(0)) {

                                console.log("Validation Quotation Detail is done....")

                                if ($scope.validateGeneralNotes(0)) {

                                    console.log("General Notes Validations Finished....................");

                                    /*if($scope.attachConfig.isAttachmentCheck()) {
                            	
                                    console.log("Attachment is Validations Finished....................");
                        	
                                } else {
                                    console.log("Error while validating Attachment....");
                                    $rootScope.clientMessage = $rootScope.nls['ERR250'];
                                    console.log("Attachment is invalid");
                                    $scope.setDetailTab('attachMent', 'attachRefNo');
                                    $scope.enableSubmitBtnFn();
                                    return false;
                                }*/
                                } else {
                                    console.log("Error while validating General Note....");
                                    return;
                                }
                            } else {
                                console.log("Validation Quotation Detail is not done...." + $scope.errorMap.entrys.length)
                                return;
                            }
                        } else {
                            console.log("Validation Quotation is not done...." + $scope.errorMap.entrys.length)
                            return;
                        }
                    } else {
                        $scope.moreAddressTab = 'chargeBtn';
                        //$rootScope.clientMessage=$rootScope.nls["ERR22169"];
                        return false;
                    }

                }

            } else {
                if ($scope.quotationDetail.quotationDimensionList != null && $scope.quotationDetail.quotationDimensionList.length != 0) {
                    $rootScope.clientMessage = $rootScope.nls["ERR8340"];
                    $scope.moreAddressTab = 'dimensionBtn';
                    return false;
                }
            }




            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);

            if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                for (var i = 0; i < $scope.quotation.quotationDetailList[0].quotationCarrierList.length; i++) {
                    if ($scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList == undefined || $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList == null || $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList.length == 0) {
                        $scope.moreAddressTab = 'chargeBtn';
                        $rootScope.clientMessage = $rootScope.nls["ERR22163"];
                        $scope.chargeBtn = true;
                        $rootScope.navigateToNextField('0mchargeCodeField0');
                        $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList = [];
                        $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList.push({});
                        return false;
                    }
                }


            }

            for (var i = 0; i < $scope.quotation.quotationDetailList.length; i++) {
                if ($scope.quotation.quotationDetailList[i].serviceNoteList != null && $scope.quotation.quotationDetailList[i].serviceNoteList != undefined &&
                    $scope.quotation.quotationDetailList[i].serviceNoteList.length > 0) {
                    var lastIndex = $scope.quotation.quotationDetailList[i].serviceNoteList.length - 1;
                    if ($scope.quotation.quotationDetailList[i].serviceNoteList.length > 0 &&
                        ($scope.quotation.quotationDetailList[i].serviceNoteList[lastIndex].note == undefined ||
                            $scope.quotation.quotationDetailList[i].serviceNoteList[lastIndex].note == null ||
                            $scope.quotation.quotationDetailList[i].serviceNoteList[lastIndex].note == '')) {
                        $scope.quotation.quotationDetailList[i].serviceNoteList.splice(lastIndex, 1);
                    }
                }
            }
            $scope.tmpObject = cloneService.clone($scope.quotation);

            $scope.tmpObject.whoRouted = $scope.tmpObject.whoRouted ? 'Agent' : 'Self';

            if ($scope.tmpObject.validFrom != null)
                $scope.tmpObject.validFrom = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.validFrom));

            if ($scope.tmpObject.validTo != null)
                $scope.tmpObject.validTo = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.validTo));

            if ($scope.tmpObject.loggedOn != null)
                $scope.tmpObject.loggedOn = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.loggedOn));


            for (var i = 0; i < $scope.tmpObject.quotationDetailList.length; i++) {
                $scope.tmpObject.quotationDetailList[i].hazardous = $scope.tmpObject.quotationDetailList[i].hazardous ? 'Yes' : 'No';
            }

            $scope.tmpObject.approved = 'Pending';
            $scope.tmpObject.locationMaster = $rootScope.userProfile.selectedUserLocation;
            $scope.tmpObject.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
            $scope.tmpObject.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;

            for (var i = 0; i < $scope.tmpObject.quotationDetailList.length; i++) {


                if ($scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList != null &&
                    $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList.length > 0) {

                    $scope.tmpObject.quotationDetailList[i].quotationValueAddedServiceList = [];

                    for (var j = 0; j < $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList.length; j++) {
                        if ($scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList[j].selected != undefined &&
                            $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList[j].selected) {
                            $scope.quotationValueAddedService = {};
                            $scope.quotationValueAddedService.valueAddedServices = $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList[j];
                            $scope.tmpObject.quotationDetailList[i].quotationValueAddedServiceList.push($scope.quotationValueAddedService);
                        }
                    }


                }
            }




            $rootScope.mainpreloder = true;
            modifyRequest();
            QuotationAdd.save($scope.tmpObject).$promise.then(function(data) {
                    if (data.responseCode === "ERR0") {
                        console.log("Quotation added Successfully")
                        $scope.showResponse(data.responseObject, true);
                    } else {
                        console.log("added Failed " + data.responseDescription)
                        $rootScope.mainpreloder = false;
                    }
                },
                function(error) {
                    $rootScope.mainpreloder = false;
                    console.log("added Failed : " + error)
                });
        };


        var modifyRequest = function() {
            if ($scope.tmpObject.locationMaster) {
                $scope.tmpObject.locationMaster = {
                    "id": $scope.tmpObject.locationMaster.id,
                    "versionLock": $scope.tmpObject.locationMaster.versionLock
                };
            }
            if ($scope.tmpObject.companyMaster) {
                $scope.tmpObject.companyMaster = {
                    "id": $scope.tmpObject.companyMaster.id,
                    "versionLock": $scope.tmpObject.companyMaster.versionLock
                };
            }
            if ($scope.tmpObject.countryMaster) {
                $scope.tmpObject.countryMaster = {
                    "id": $scope.tmpObject.countryMaster.id,
                    "versionLock": $scope.tmpObject.countryMaster.versionLock
                };
            }
            if ($scope.tmpObject.localCurrency) {
                $scope.tmpObject.localCurrency = {
                    "id": $scope.tmpObject.localCurrency.id,
                    "versionLock": $scope.tmpObject.localCurrency.versionLock

                };
            }
            if ($scope.tmpObject.customer) {
                $scope.tmpObject.customer = {
                    "id": $scope.tmpObject.customer.id,
                    "versionLock": $scope.tmpObject.customer.versionLock
                };
            }

            if ($scope.tmpObject.customerAddress) {
                if ($scope.tmpObject.customerAddress.city) {
                    $scope.tmpObject.customerAddress.city = {
                        "id": $scope.tmpObject.customerAddress.city.id,
                        "versionLock": $scope.tmpObject.customerAddress.city.versionLock
                    };
                }
                if ($scope.tmpObject.customerAddress.state) {
                    $scope.tmpObject.customerAddress.state = {
                        "id": $scope.tmpObject.customerAddress.state.id,
                        "versionLock": $scope.tmpObject.customerAddress.state.versionLock
                    };
                }
                if ($scope.tmpObject.customerAddress.country) {
                    $scope.tmpObject.customerAddress.country = {
                        "id": $scope.tmpObject.customerAddress.country.id,
                        "versionLock": $scope.tmpObject.customerAddress.country.versionLock
                    };
                }
            }

            if ($scope.tmpObject.salesCoordinator) {
                $scope.tmpObject.salesCoordinator = {
                    "id": $scope.tmpObject.salesCoordinator.id,
                    "versionLock": $scope.tmpObject.salesCoordinator.versionLock
                };
            }
            if ($scope.tmpObject.salesman) {
                $scope.tmpObject.salesman = {
                    "id": $scope.tmpObject.salesman.id,
                    "versionLock": $scope.tmpObject.salesman.versionLock
                };
            }

            if ($scope.tmpObject.loggedBy) {
                $scope.tmpObject.loggedBy = {
                    "id": $scope.tmpObject.loggedBy.id,
                    "versionLock": $scope.tmpObject.loggedBy.versionLock
                };
            }
            if ($scope.tmpObject.shipper) {
                $scope.tmpObject.shipper = {
                    "id": $scope.tmpObject.shipper.id,
                    "versionLock": $scope.tmpObject.shipper.versionLock

                };
            }
            if ($scope.tmpObject.agent) {
                $scope.tmpObject.agent = {
                    "id": $scope.tmpObject.agent.id,
                    "versionLock": $scope.tmpObject.agent.versionLock

                };
            }

            if ($scope.tmpObject.shipperAddress) {
                if ($scope.tmpObject.shipperAddress.city) {
                    $scope.tmpObject.shipperAddress.city = {
                        "id": $scope.tmpObject.shipperAddress.city.id,
                        "versionLock": $scope.tmpObject.shipperAddress.city.versionLock
                    };
                }
                if ($scope.tmpObject.shipperAddress.state) {
                    $scope.tmpObject.shipperAddress.state = {
                        "id": $scope.tmpObject.shipperAddress.state.id,
                        "versionLock": $scope.tmpObject.shipperAddress.state.versionLock
                    };
                }
                if ($scope.tmpObject.shipperAddress.country) {
                    $scope.tmpObject.shipperAddress.country = {
                        "id": $scope.tmpObject.shipperAddress.country.id,
                        "versionLock": $scope.tmpObject.shipperAddress.country.versionLock
                    };
                }
            }

            if ($scope.tmpObject.quotationDetailList && $scope.tmpObject.quotationDetailList.length > 0) {
                for (var i = 0; i < $scope.tmpObject.quotationDetailList.length; i++) {
                    if ($scope.tmpObject.quotationDetailList[i].serviceMaster) {
                        $scope.tmpObject.quotationDetailList[i].serviceMaster = {
                            "id": $scope.tmpObject.quotationDetailList[i].serviceMaster.id,
                            "versionLock": $scope.tmpObject.quotationDetailList[i].serviceMaster.versionLock
                        };
                    }

                    if ($scope.tmpObject.quotationDetailList[i].origin) {
                        $scope.tmpObject.quotationDetailList[i].origin = {
                            "id": $scope.tmpObject.quotationDetailList[i].origin.id,
                            "versionLock": $scope.tmpObject.quotationDetailList[i].origin.versionLock
                        };
                    }
                    if ($scope.tmpObject.quotationDetailList[i].destination) {
                        $scope.tmpObject.quotationDetailList[i].destination = {
                            "id": $scope.tmpObject.quotationDetailList[i].destination.id,
                            "versionLock": $scope.tmpObject.quotationDetailList[i].destination.versionLock
                        };
                    }
                    if ($scope.tmpObject.quotationDetailList[i].tosMaster) {
                        $scope.tmpObject.quotationDetailList[i].tosMaster = {
                            "id": $scope.tmpObject.quotationDetailList[i].tosMaster.id,
                            "versionLock": $scope.tmpObject.quotationDetailList[i].tosMaster.versionLock
                        };
                    }
                    if ($scope.tmpObject.quotationDetailList[i].commodityMaster) {
                        $scope.tmpObject.quotationDetailList[i].commodityMaster = {
                            "id": $scope.tmpObject.quotationDetailList[i].commodityMaster.id,
                            "versionLock": $scope.tmpObject.quotationDetailList[i].commodityMaster.versionLock
                        };
                    }
                    if ($scope.tmpObject.quotationDetailList[i].pickupAddress) {
                        if ($scope.tmpObject.quotationDetailList[i].pickupAddress.city) {
                            $scope.tmpObject.quotationDetailList[i].pickupAddress.city = {
                                "id": $scope.tmpObject.quotationDetailList[i].pickupAddress.city.id,
                                "versionLock": $scope.tmpObject.quotationDetailList[i].pickupAddress.city.versionLock
                            };
                            $scope.tmpObject.quotationDetailList[i].pickupAddress.state = {
                                "id": $scope.tmpObject.quotationDetailList[i].pickupAddress.state.id,
                                "versionLock": $scope.tmpObject.quotationDetailList[i].pickupAddress.state.versionLock
                            };
                            $scope.tmpObject.quotationDetailList[i].pickupAddress.country = {
                                "id": $scope.tmpObject.quotationDetailList[i].pickupAddress.country.id,
                                "versionLock": $scope.tmpObject.quotationDetailList[i].pickupAddress.country.versionLock
                            };
                        }
                    }
                    if ($scope.tmpObject.quotationDetailList[i].deliveryAddress) {
                        if ($scope.tmpObject.quotationDetailList[i].deliveryAddress.city) {
                            $scope.tmpObject.quotationDetailList[i].deliveryAddress.city = {
                                "id": $scope.tmpObject.quotationDetailList[i].deliveryAddress.city.id,
                                "versionLock": $scope.tmpObject.quotationDetailList[i].deliveryAddress.city.versionLock
                            };
                            $scope.tmpObject.quotationDetailList[i].deliveryAddress.state = {
                                "id": $scope.tmpObject.quotationDetailList[i].deliveryAddress.state.id,
                                "versionLock": $scope.tmpObject.quotationDetailList[i].deliveryAddress.state.versionLock
                            };
                            $scope.tmpObject.quotationDetailList[i].deliveryAddress.country = {
                                "id": $scope.tmpObject.quotationDetailList[i].deliveryAddress.country.id,
                                "versionLock": $scope.tmpObject.quotationDetailList[i].deliveryAddress.country.versionLock
                            };
                        }
                    }
                    if ($scope.tmpObject.quotationDetailList[i].quotationCarrierList) {
                        for (var j = 0; j < $scope.tmpObject.quotationDetailList[i].quotationCarrierList.length; j++) {
                            if ($scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].carrierMaster) {
                                $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].carrierMaster = {
                                    "id": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].carrierMaster.id,
                                    "versionLock": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].carrierMaster.versionLock
                                };
                            }
                            for (var k = 0; k < $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList.length; k++) {
                                if ($scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].chargeMaster) {
                                    $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].chargeMaster = {
                                        "id": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].chargeMaster.id,
                                        "versionLock": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].chargeMaster.versionLock
                                    };
                                }
                                if ($scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].unitMaster) {
                                    $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].unitMaster = {
                                        "id": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].unitMaster.id,
                                        "versionLock": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].unitMaster.versionLock
                                    };
                                }
                                if ($scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].currencyMaster) {
                                    $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].currencyMaster = {
                                        "id": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].currencyMaster.id,
                                        "versionLock": $scope.tmpObject.quotationDetailList[i].quotationCarrierList[j].quotationChargeList[k].currencyMaster.versionLock
                                    };
                                }

                            }

                        }
                    }
                }

            }

        }


        $scope.updateQuotation = function() {

            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);

            $rootScope.clientMessage = null;


            if ($scope.quotationDimension.isDimensionCheck()) {
                for (var i = 0; i < $scope.quotationDetail.quotationCarrierList.length; i++) {

                    var flag = false;
                    if (angular.isFunction($scope.quotationDetail.quotationCarrierList[i].ischargeCheck)) {
                        if ($scope.quotationDetail.quotationCarrierList[i].ischargeCheck()) {
                            flag = true;
                        }
                    } else {
                        flag = true;
                    }

                    if (flag) {

                        if ($scope.validateQuotation(0)) {
                            console.log("Validation Quotation is done....")

                            if ($scope.validateQuotationDetail(0)) {

                                console.log("Validation Quotation Detail is done....")
                                if ($scope.validateGeneralNotes(0)) {
                                    console.log("General Notes Validations Finished....................");
                                    /*if($scope.attachConfig.isAttachmentCheck()) {
                                                    console.log("Attachment is Validations Finished....................");
                                                } else {
                                                    console.log("Error while validating Attachment....");
                                                    $rootScope.clientMessage = $rootScope.nls['ERR250'];
                                                    return false;
                                                }*/
                                } else {
                                    console.log("Error while validating General Note....");
                                    return;
                                }
                            } else {
                                console.log("Validation Quotation is not done...." + $scope.errorMap.entrys.length)
                                return;
                            }
                        } else {
                            console.log("Validation Quotation is not done...." + $scope.errorMap.entrys.length)
                            return;
                        }

                    } else {

                        $scope.moreAddressTab = 'chargeBtn';
                        $rootScope.clientMessage = $rootScope.nls["ERR90556"];
                        return false;
                    }
                }


            } else {
                if ($scope.quotationDetail.quotationDimensionList != null && $scope.quotationDetail.quotationDimensionList.length != 0) {
                    $scope.errorMap = new Map();
                    $rootScope.clientMessage = $rootScope.nls["ERR8340"];
                    console.log("Dimension Validation Failed");
                    $scope.moreAddressTab = 'dimensionBtn';
                    return false;
                }
            }

            if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {


                for (var i = 0; i < $scope.quotation.quotationDetailList[0].quotationCarrierList.length; i++) {
                    if ($scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList == undefined || $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList == null || $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList.length == 0) {
                        $scope.moreAddressTab = 'chargeBtn';
                        $rootScope.clientMessage = $rootScope.nls["ERR22163"];
                        $scope.chargeBtn = true;
                        $rootScope.navigateToNextField('0mchargeCodeField0');
                        $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList = [];
                        $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList.push({});
                        return false;
                    }
                }

            }

            for (var i = 0; i < $scope.quotation.quotationDetailList.length; i++) {
                if ($scope.quotation.quotationDetailList[i].serviceNoteList != null && $scope.quotation.quotationDetailList[i].serviceNoteList != undefined &&
                    $scope.quotation.quotationDetailList[i].serviceNoteList.length > 0) {
                    var lastIndex = $scope.quotation.quotationDetailList[i].serviceNoteList.length - 1;
                    if ($scope.quotation.quotationDetailList[i].serviceNoteList.length > 0 &&
                        ($scope.quotation.quotationDetailList[i].serviceNoteList[lastIndex].note == undefined ||
                            $scope.quotation.quotationDetailList[i].serviceNoteList[lastIndex].note == null ||
                            $scope.quotation.quotationDetailList[i].serviceNoteList[lastIndex].note == '')) {
                        $scope.quotation.quotationDetailList[i].serviceNoteList.splice(lastIndex, 1);
                    }
                }
            }


            /*	 $scope.associateQuotationAttach();*/

            $scope.tmpObject = cloneService.clone($scope.quotation);

            $scope.tmpObject.whoRouted = $scope.tmpObject.whoRouted ? 'Agent' : 'Self';

            if ($scope.tmpObject.validFrom != null)
                $scope.tmpObject.validFrom = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.validFrom));

            if ($scope.tmpObject.validTo != null)
                $scope.tmpObject.validTo = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.validTo));

            if ($scope.tmpObject.loggedOn != null)
                $scope.tmpObject.loggedOn = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.loggedOn));


            for (var i = 0; i < $scope.tmpObject.quotationDetailList.length; i++) {
                $scope.tmpObject.quotationDetailList[i].hazardous = $scope.tmpObject.quotationDetailList[i].hazardous ? 'Yes' : 'No';
            }


            if ($scope.tmpObject.approvedDate != null)
                $scope.tmpObject.approvedDate = $rootScope.sendApiStartDateTime($rootScope.convertToDateTime($scope.tmpObject.approvedDate), true);


            if ($scope.tmpObject.locationMaster == null || $scope.tmpObject.locationMaster.id == null) {
                $scope.tmpObject.locationMaster = $rootScope.userProfile.selectedUserLocation;
            }
            if ($scope.tmpObject.companyMaster == null || $scope.tmpObject.companyMaster.id == null) {
                $scope.tmpObject.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
            }
            if ($scope.tmpObject.countryMaster == null || $scope.tmpObject.countryMaster.id == null) {
                $scope.tmpObject.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
            }

            for (var i = 0; i < $scope.tmpObject.quotationDetailList.length; i++) {


                if ($scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList != null &&
                    $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList.length > 0) {

                    $scope.tmpObject.quotationDetailList[i].quotationValueAddedServiceList = [];

                    for (var j = 0; j < $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList.length; j++) {
                        if ($scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList[j].selected != undefined &&
                            $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList[j].selected) {
                            $scope.quotationValueAddedService = {};
                            $scope.quotationValueAddedService.valueAddedServices = $scope.tmpObject.quotationDetailList[i].quotationValueAddedTmpServiceList[j];
                            $scope.tmpObject.quotationDetailList[i].quotationValueAddedServiceList.push($scope.quotationValueAddedService);
                        }
                    }


                }
            }
            $rootScope.mainpreloder = true;
            modifyRequest();
            QuotationUpdate.save($scope.tmpObject).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("Quotation added Successfully")
                        $scope.showResponse(data.responseObject, false);
                    } else if (data.responseCode === 'ERR20') {
                        $rootScope.showResponseForConcurrentException('Quotation', $scope.tmpObject.id, $scope.tmpObject.quotationNo, 'layout.salesQuotationView', 'layout.salesQuotationView', { quotationId: $scope.tmpObject.id, submitAction: 'Saved' });
                    } else {
                        console.log("added Failed " + data.responseDescription)
                        $rootScope.mainpreloder = false;
                    }
                },
                function(error) {
                    $rootScope.mainpreloder = false;
                    console.log("added Failed : " + error)
                });

        };

        /* List of Customers select Picker */
        $scope.showCustomerList = false;

        $scope.customerlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.routePartyEvent = function(fromObjModel, isAdd, partyMaster) {
            $rootScope.nav_src_bkref_key = new Date().toISOString();
            if (isAdd) {
                $state.go("layout.addParty", {
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    forObj: fromObjModel
                });
            } else {
                if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                    $rootScope.nav_src_bkref_key = new Date().toISOString();
                    var stateRouteParam = {
                        partyId: partyMaster.id,
                        nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                        forObj: fromObjModel
                    };
                    $state.go("layout.editParty", stateRouteParam);
                } else {
                    console.log("Party not selected...");
                }
            }
        }

        $scope.showCustomer = function(partyName) {

            console.log("Showing List of Customer.....");
            $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
            $scope.selectedItem = -1;


        };

        $scope.ajaxCustomerEvent = function(object) {
            console.log("ajaxCustomerEvent is called", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            //$scope.showCustomerList=true;


            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.customerList = data.responseObject.searchResult;
                        //$scope.showCustomerList=true;
                        return $scope.customerList;

                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Countries');
                }
            );


        }



        $scope.selectedCustomer = function(id) {
            if ($scope.validateQuotation(1)) {
                $scope.getPartyAddress('CUSTOMER', $scope.quotation.customer);
                $scope.setSalesman();
                $scope.navigateToNextField(id);
            }
        };

        $scope.getPartyAddress = function(type, obj) {
            if (type == 'CUSTOMER') {
                if (($scope.quotation.customer != null && $scope.quotation.customer.id != null)) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            $scope.addressMapping('CUSTOMER', obj.partyAddressList[i], obj.countryMaster);
                        }
                    }
                }

            } else if (type == 'SHIPPER') {
                if (($scope.quotation.shipper != null && $scope.quotation.shipper.id != null)) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            $scope.addressMapping('SHIPPER', obj.partyAddressList[i], obj.countryMaster);
                        }
                    }
                }
            }

        }
        $scope.cancelCustomer = function() {
            $scope.showCustomerList = false;
        }



        /*List of Shipper Select Picker*/

        $scope.showShipperList = false;

        $scope.shipperlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showShipper = function(partyName) {
            console.log("Showing list of shippers.....");

            $scope.selectedItem = -1;

            $scope.panelTitle = "Shipper"


        };

        $scope.ajaxShipperEvent = function(object) {

            console.log("ajaxShipperEvent is called", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.shipperList = data.responseObject.searchResult;
                        //$scope.showShipperList=true;
                        return $scope.shipperList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Shipper');
                }
            );



        }



        $scope.selectedShipper = function(id) {

            if ($scope.validateQuotation(6)) {
                $scope.navigateToNextField(id);
            }

            $scope.getPartyAddress('SHIPPER', $scope.quotation.shipper);
        };

        $scope.cancelShipper = function() {
            $scope.showShipperList = false;
        }

        $scope.addressMapping = function(type, partyAddress, countryMaster) {
            if (type == 'CUSTOMER') {

                if ($scope.quotation.customerAddress == undefined || $scope.quotation.customerAddress == null) {
                    $scope.quotation.customerAddress = {};
                }

                $scope.quotation.customerAddress = addressJoiner.joinAddressLineByLine($scope.quotation.customerAddress, $scope.quotation.customer, partyAddress);
                $scope.quotation.attention = partyAddress.contactPerson;
            } else if (type == 'SHIPPER') {
                if ($scope.quotation.shipperAddress == undefined || $scope.quotation.shipperAddress == null) {
                    $scope.quotation.shipperAddress = {};
                }
                $scope.quotation.shipperAddress = addressJoiner.joinAddressLineByLine($scope.quotation.shipperAddress, $scope.quotation.shipper, partyAddress);
            }
        }




        /* Showing List of Logged By Users... Select Picker */

        $scope.showLoggedByList = false;

        $scope.loggedBylistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "employeeName",
                    seperator: false
                },
                {
                    "title": "employeeCode",
                    seperator: true
                }
            ]
        }

        $scope.showLoggedBy = function(partyName) {
            console.log("Showing List of Logged By Users....");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Logged By"

        };

        $scope.ajaxLoggedByEvent = function(object) {
            console.log("ajaxLoggedByEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


            return EmployeeList.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.loggedByList = data.responseObject.searchResult;
                        //$scope.showLoggedByList=true;
                        return $scope.loggedByList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching LoggedBy');
                }
            );

        }



        $scope.selectedLoggedBy = function(id) {
            console.log("Selected Logged By ");
            if ($scope.validateQuotation(12)) {
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelLoggedBy = function() {
            $scope.showLoggedByList = false;
        }

        $scope.setFocus = function(num) {
            if (num == 1) {
                document.quotationForm.pickup.focus();
            } else if (num == 2) {
                document.quotationForm.delivery.focus();
            }
        }

        /* Sales Coordinator List Picker */

        $scope.showSalesCoordinatorList = false;

        $scope.salesCoordinatorlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "employeeName",
                    seperator: false
                },
                {
                    "title": "employeeCode",
                    seperator: true
                }
            ]
        }

        $scope.showSalesCoordinator = function(partyName) {
            console.log("Showing List of Sales coordinator");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Sales Coordinatior";

        };

        $scope.ajaxSalesCoordinator = function(object) {
            console.log("ajaxSalesCoordinator ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            //$scope.showSalesCoordinatorList=true;

            return EmployeeList.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.salesCoordinatorList = data.responseObject.searchResult;
                        //$scope.showSalesCoordinatorList=true;
                        return $scope.salesCoordinatorList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching SalesCoordinator');
                }
            );

        }


        $scope.selectedSalesCoordinator = function(id) {
            if ($scope.validateQuotation(19)) {
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelSalesCoordinator = function() {
            $scope.showSalesCoordinatorList = false;
        }


        /*Salesman Select Picker*/

        $scope.showSalesmanList = false;

        $scope.salesmanlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "employeeName",
                    seperator: false
                },
                {
                    "title": "employeeCode",
                    seperator: true
                }
            ]
        }

        $scope.showSalesman = function(partyName) {
            console.log("Showing List of Salesman");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Salesman"

        };

        $scope.ajaxSalesmanEvent = function(object) {

            console.log("ajaxSalesmanEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return SalesmanListOnLocation.fetch({
                "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
            }, $scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.salesmanList = data.responseObject.searchResult;
                        //$scope.showSalesmanList=true;
                        return $scope.salesmanList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Salesman');
                }
            );

        }


        $scope.selectedSalesman = function(id) {
            if ($scope.validateQuotation(18)) {
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelSalesman = function() {
            $scope.showSalesmanList = false;
        }



        $scope.ajaxAgentEvent = function(object) {
            console.log("ajaxAgentEvent is running.......")

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.agentList = data.responseObject.searchResult;
                        return $scope.agentList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedAgent = function(partyobj) {

            if ($scope.validateQuotation(18)) {
                $scope.navigateToNextField(id);
            }

        };


        /* Pickup Delivery */

        $scope.pickUpDeliveryValidation = function(code) {
            var flag = false;
            flag = $scope.validateQuotationDetail(code);
            return flag
        }

        $scope.selectedPickupCity = function(quotationDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(12)) {
                if (quotationDetail.pickupAddress.city != null && quotationDetail.pickupAddress.city.status == 'Active') {
                    quotationDetail.pickupAddress.state = quotationDetail.pickupAddress.city.stateMaster;
                    quotationDetail.pickupAddress.country = quotationDetail.pickupAddress.city.countryMaster;
                }
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedPickupState = function(quotationDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(13)) {
                if (quotationDetail.pickupAddress.state != null && quotationDetail.pickupAddress.state.status == 'Active') {
                    quotationDetail.pickupAddress.country = quotationDetail.pickupAddress.state.countryMaster;
                }

                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedPickupCountry = function(quotationDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(14)) {
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedDeliveryCity = function(quotationDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(15)) {
                if (quotationDetail.deliveryAddress.city != null && quotationDetail.deliveryAddress.city.status == 'Active') {
                    quotationDetail.deliveryAddress.state = quotationDetail.deliveryAddress.city.stateMaster;
                    quotationDetail.deliveryAddress.country = quotationDetail.deliveryAddress.city.countryMaster;
                }
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedDeliveryState = function(quotationDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(16)) {
                if (quotationDetail.deliveryAddress.state != null && quotationDetail.deliveryAddress.state.status == 'Active') {
                    quotationDetail.deliveryAddress.country = quotationDetail.deliveryAddress.state.countryMaster;
                }
                $scope.navigateToNextField(nextField, index);
            }
        }

        $scope.selectedDeliveryCountry = function(quotationDetail, nextField, index) {
            if ($scope.pickUpDeliveryValidation(17)) {
                $scope.navigateToNextField(nextField, index);
            }
        }



        $scope.showPartyAddressCustomerList = false;

        $scope.partyAddressCustomerlistConfig = {
            search: true,
            showCode: true,
            columns: [{
                    "title": "addressType",
                    seperator: false
                },
                {
                    "title": "contactPerson",
                    seperator: true
                }
            ]

        }

        $scope.partyAddresslistShipperConfig = {
            search: false,
            address: true
        }

        $scope.partyAddresslistCustomerConfig = {
            search: false,
            address: true
        }

        $scope.showPartyAddressCustomer = function() {

            console.log("Showing Customers address list...");

            if ($scope.validateQuotation(1)) {
                $scope.panelTitle = "Party Address";
                $scope.partyAddressCustomer = [];
                $scope.showPartyAddressCustomerList = true;
                $scope.selectedItem = -1;
                $scope.isPickUp = false;
            }
        };

        $scope.goToPickup = function(quotationDetail) {


            if ($scope.validateQuotation(1)) {
                $scope.panelTitle = "Pick up Address";
                $scope.partyAddressCustomer = [];
                $scope.showPartyAddressCustomerList = true;
                $scope.selectedItem = -1;
                $scope.quotationDetail = quotationDetail;
                $scope.isPickUp = true;
            }
        };



        $scope.selectedPartyAddressCustomer = function(obj) {

            if ($scope.isPickUp) {
                /*var addressMaster = {};
                addressMaster = addressJoiner.joinAddressLineByLine(addressMaster, $scope.quotation.customer, obj);
                $scope.quotationDetail.pickUpAddress = addressMaster.fullAddress;*/
                if ($scope.quotationDetail.pickupAddress == undefined || $scope.quotationDetail.pickupAddress == null) {
                    $scope.quotationDetail.pickupAddress = {};
                }

                $scope.quotationDetail.pickupAddress.addressLine1 = obj.addressLine1;
                $scope.quotationDetail.pickupAddress.addressLine2 = obj.addressLine2;
                $scope.quotationDetail.pickupAddress.addressLine3 = obj.addressLine3;
                if (obj.cityMaster != null) {
                    $scope.quotationDetail.pickupAddress.city = obj.cityMaster;
                }
                if (obj.poBox != null && obj.poBox != undefined && obj.poBox != '') {
                    $scope.quotationDetail.pickupAddress.poBox = obj.poBox;
                } else {
                    if (obj.zipCode != null && obj.zipCode != undefined && obj.zipCode != '') {
                        $scope.quotationDetail.pickupAddress.poBox = obj.zipCode;
                    }
                }
                if (obj.stateMaster != null) {
                    $scope.quotationDetail.pickupAddress.state = obj.stateMaster;
                    $scope.quotationDetail.pickupAddress.country = obj.stateMaster.countryMaster;
                }

            } else {
                $scope.addressMapping('CUSTOMER', obj, $scope.quotation.customer.countryMaster);

            }
            $scope.showPartyAddressCustomerList = false;
            $rootScope.navigateToNextField('delivery')
        };

        $scope.cancelPartyAddressCustomer = function() {
            $scope.showPartyAddressCustomerList = false;
            $rootScope.navigateToNextField('pickup')
        }

        $scope.showPartyAddressShipperList = false;

        $scope.partyAddressShipperlistConfig = {
            search: true,
            showCode: true,
            columns: [{
                    "title": "addressType",
                    seperator: false
                },
                {
                    "title": "contactPerson",
                    seperator: true
                }
            ]
        }
        $scope.showPartyAddressShipper = function() {
            if ($scope.validateQuotation(6)) {
                if ($scope.quotation.shipper != undefined && $scope.quotation.shipper.id != undefined) {
                    $scope.panelTitle = "Shipper Address";
                    $scope.showPartyAddressShipperList = true;
                    $scope.selectedItem = -1;
                    $scope.isDelivery = false;
                } else {
                    console.log("Shipper is null.");
                }

            }
        };

        $scope.goToDelivery = function(quotationDetail) {

            if ($scope.validateQuotation(6)) {
                $scope.panelTitle = "Delivery Address";
                $scope.showPartyAddressShipperList = true;
                $scope.selectedItem = -1;
                $scope.quotationDetail = quotationDetail;
                $scope.isDelivery = true;
            }
        };

        $scope.selectedPartyAddressShipper = function(obj) {

            if ($scope.isDelivery) {
                /*var addressMaster = {};
                addressMaster = addressJoiner.joinAddressLineByLine(addressMaster, $scope.quotation.shipper, obj);
                $scope.quotationDetail.deliveryAddress = addressMaster.fullAddress;*/


                if ($scope.quotationDetail.deliveryAddress == undefined || $scope.quotationDetail.deliveryAddress == null) {
                    $scope.quotationDetail.deliveryAddress = {};
                }

                $scope.quotationDetail.deliveryAddress.addressLine1 = obj.addressLine1;
                $scope.quotationDetail.deliveryAddress.addressLine2 = obj.addressLine2;
                $scope.quotationDetail.deliveryAddress.addressLine3 = obj.addressLine3;
                if (obj.cityMaster != null) {
                    $scope.quotationDetail.deliveryAddress.city = obj.cityMaster;
                }

                if (obj.poBox != null && obj.poBox != undefined && obj.poBox != '') {
                    $scope.quotationDetail.deliveryAddress.poBox = obj.poBox;
                } else {
                    if (obj.zipCode != null && obj.zipCode != undefined && obj.zipCode != '') {
                        $scope.quotationDetail.deliveryAddress.poBox = obj.zipCode;
                    }
                }
                if (obj.stateMaster != null) {
                    $scope.quotationDetail.deliveryAddress.state = obj.stateMaster;
                    $scope.quotationDetail.deliveryAddress.country = obj.stateMaster.countryMaster;
                }
            } else {
                $scope.addressMapping('SHIPPER', obj, $scope.quotation.shipper.countryMaster);
            }
            $scope.showPartyAddressShipperList = false;
            $rootScope.navigateToNextField('partyMaster')

        };

        $scope.cancelPartyAddressShipper = function() {
            $scope.showPartyAddressShipperList = false;
            $rootScope.navigateToNextField('delivery')
        }




        $scope.getDefaultNote = function() {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_DEFAULT_NOTE_LIST)) {
                GeneralNote.fetch({
                    locationId: $scope.quotation.locationMaster.id,
                    category: 'Quotation',
                    subCategory: 'General',
                    serviceMasterId: -1
                }, function(data) {
                    if (data.responseCode == 'ERR0') {

                        if ($scope.quotation.generalNoteList != null && $scope.quotation.generalNoteList.length != 0) {
                            for (var i = 0; i < data.responseObject.length; i++) {
                                for (var j = 0; j < $scope.quotation.generalNoteList.length; j++) {

                                    if ($scope.quotation.generalNoteList[j].note == undefined) {
                                        continue;
                                    }
                                    if ($scope.quotation.generalNoteList[j].note.toUpperCase() == data.responseObject[i].note.toUpperCase()) {
                                        $scope.quotation.generalNoteList.splice(j, 1);
                                    }

                                }
                            }
                        }

                        for (var i = 0; i < data.responseObject.length; i++) {
                            $scope.gNote = {};
                            $scope.gNote.note = data.responseObject[i].note;
                            if ($scope.quotation.generalNoteList == null || $scope.quotation.generalNoteList.length == 0) {
                                $scope.quotation.generalNoteList = [];
                            }
                            $scope.quotation.generalNoteList.push($scope.gNote);
                        }

                        if ($scope.quotation.generalNoteList == null || $scope.quotation.generalNoteList == undefined ||
                            $scope.quotation.generalNoteList.length == 0) {
                            $scope.addNote();
                        }

                    } else {
                        console.log("GeneralNote Failed " +
                            data.responseDescription)
                    }
                }, function(error) {
                    console.log("GeneralNote Failed : " + error)
                });


            }
        }

        $scope.insertNote = function() {
            if ($scope.quotation.generalNoteList == undefined || $scope.quotation.generalNoteList == null || $scope.quotation.generalNoteList.length == 0) {
                $scope.quotation.generalNoteList = [];
                $scope.quotation.generalNoteList.push({});
                $rootScope.navigateToNextField('gNote0');
            } else {

                if ($scope.quotation.generalNoteList[$scope.quotation.generalNoteList.length - 1] != undefined &&
                    $scope.quotation.generalNoteList[$scope.quotation.generalNoteList.length - 1] != null
                ) {

                    if ($scope.quotation.generalNoteList[$scope.quotation.generalNoteList.length - 1].note != undefined &&
                        $scope.quotation.generalNoteList[$scope.quotation.generalNoteList.length - 1].note != null &&
                        $scope.quotation.generalNoteList[$scope.quotation.generalNoteList.length - 1].note != "") {
                        $scope.quotation.generalNoteList.push({});
                    }

                    $rootScope.navigateToNextField('gNote' + ($scope.quotation.generalNoteList.length - 1));
                }

            }



        }
        $scope.addNote = function() {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_CREATE)) {
                $scope.insertNote();
            }
        }

        $scope.deleteNote = function(index) {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_DELETE)) {
                $scope.quotation.generalNoteList.splice(index, 1);

                if ($scope.quotation.generalNoteList == undefined || $scope.quotation.generalNoteList == null || $scope.quotation.generalNoteList.length == 0) {
                    $scope.insertNote();
                } else {
                    $rootScope.navigateToNextField('gNote' + ($scope.quotation.generalNoteList.length - 1));
                }
            }
        }




        $scope.validateQuotation = function(validateCode) {

            $scope.firstFocus = false;
            $scope.errorMap = new Map();
            console.log("Quotation Validation Begins " + validateCode);


            if (validateCode == 0 || validateCode == 1) {
                if ($scope.quotation.customer == null ||
                    $scope.quotation.customer.id == undefined ||
                    $scope.quotation.customer.id == null ||
                    $scope.quotation.customer.id == "") {

                    console.log("Customer is Mandatory");

                    $scope.errorMap.put("customer", $rootScope.nls["ERR22006"]);

                    return false;

                } else {

                    if ($scope.quotation.customer.isDefaulter) {
                        $scope.quotation.customer = null;
                        $scope.quotation.customerAddress.addressLine1 = "";
                        $scope.quotation.customerAddress.addressLine2 = "";
                        $scope.quotation.customerAddress.addressLine3 = "";
                        $scope.quotation.customerAddress.addressLine4 = "";
                        $scope.quotation.customerAddress.phone = "";
                        $scope.quotation.customerAddress.mobile = "";
                        $scope.quotation.customerAddress.email = "";
                        $scope.errorMap.put("customer", $rootScope.nls["ERR22161"]);
                        return false
                    }

                    if (ValidateUtil.isStatusBlocked($scope.quotation.customer.status)) {
                        $scope.quotation.customer = null;
                        $scope.quotation.customerAddress.addressLine1 = "";
                        $scope.quotation.customerAddress.addressLine2 = "";
                        $scope.quotation.customerAddress.addressLine3 = "";
                        $scope.quotation.customerAddress.addressLine4 = "";
                        $scope.quotation.customerAddress.phone = "";
                        $scope.quotation.customerAddress.mobile = "";
                        $scope.quotation.customerAddress.email = "";
                        $scope.errorMap.put("customer", $rootScope.nls["ERR22007"]);
                        return false
                    }

                    if (ValidateUtil.isStatusHidden($scope.quotation.customer.status)) {
                        $scope.quotation.customer = null;
                        $scope.quotation.customerAddress.addressLine1 = "";
                        $scope.quotation.customerAddress.addressLine2 = "";
                        $scope.quotation.customerAddress.addressLine3 = "";
                        $scope.quotation.customerAddress.addressLine4 = "";
                        $scope.quotation.customerAddress.phone = "";
                        $scope.quotation.customerAddress.mobile = "";
                        $scope.quotation.customerAddress.email = "";
                        $scope.errorMap.put("customer", $rootScope.nls["ERR22008"]);
                        return false
                    }

                }
            }




            if (validateCode == 0 || validateCode == 2) {

                if ($scope.quotation.customerAddress.addressLine1 == null ||
                    $scope.quotation.customerAddress.addressLine1 == "" ||
                    $scope.quotation.customerAddress.addressLine1 == undefined) {
                    $scope.showPartyAddressModel = true;
                    console.log($rootScope.nls["ERR22027"]);
                    $scope.errorMap.put("quotation.customerAddress.addressLine1", $rootScope.nls["ERR22027"]);
                    return false;
                }
            }



            if (validateCode == 0 || validateCode == 3) {
                /*
                                if($scope.quotation.customerAddress.addressLine2  == null
                                        || $scope.quotation.customerAddress.addressLine2  =="" 
                                        || $scope.quotation.customerAddress.addressLine2 ==undefined) {
                                    console.log($rootScope.nls["ERR22028"]);
                                    $scope.errorMap.put("quotation.customerAddress.addressLine2",$rootScope.nls["ERR22028"]);
                                    return false;
                                } 
                            */
            }



            if (validateCode == 0 || validateCode == 6) {
                if ($scope.quotation.shipper == null ||
                    $scope.quotation.shipper.id == undefined ||
                    $scope.quotation.shipper.id == null ||
                    $scope.quotation.shipper.id == "") {
                    $scope.quotation.shipper = null;
                    /*As discussed with mani we are removed
                     * 
                     * console.log($rootScope.nls["ERR22009"]);
                    $scope.errorMap.put("shipper",$rootScope.nls["ERR22009"]);
                    return false;*/
                } else {

                    if ($scope.quotation.shipper.isDefaulter) {
                        $scope.quotation.shipper = null;
                        $scope.quotation.shipperAddress.addressLine1 = "";
                        $scope.quotation.shipperAddress.addressLine2 = "";
                        $scope.quotation.shipperAddress.addressLine3 = "";
                        $scope.quotation.shipperAddress.addressLine4 = "";
                        $scope.quotation.shipperAddress.phone = "";
                        $scope.quotation.shipperAddress.mobile = "";
                        $scope.quotation.shipperAddress.email = "";
                        $scope.errorMap.put("shipper", $rootScope.nls["ERR22162"]);
                        return false
                    }

                    if (ValidateUtil.isStatusBlocked($scope.quotation.shipper.status)) {
                        $scope.quotation.shipper = null;
                        $scope.quotation.shipperAddress.addressLine1 = "";
                        $scope.quotation.shipperAddress.addressLine2 = "";
                        $scope.quotation.shipperAddress.addressLine3 = "";
                        $scope.quotation.shipperAddress.addressLine4 = "";
                        $scope.quotation.shipperAddress.phone = "";
                        $scope.quotation.shipperAddress.mobile = "";
                        $scope.quotation.shipperAddress.email = "";

                        console.log($rootScope.nls["ERR22010"]);
                        $scope.errorMap.put("shipper", $rootScope.nls["ERR22010"]);
                        return false
                    }

                    if (ValidateUtil.isStatusHidden($scope.quotation.shipper.status)) {
                        $scope.quotation.shipper = null;
                        $scope.quotation.shipperAddress.addressLine1 = "";
                        $scope.quotation.shipperAddress.addressLine2 = "";
                        $scope.quotation.shipperAddress.addressLine3 = "";
                        $scope.quotation.shipperAddress.addressLine4 = "";
                        $scope.quotation.shipperAddress.phone = "";
                        $scope.quotation.shipperAddress.mobile = "";
                        $scope.quotation.shipperAddress.email = "";
                        console.log($rootScope.nls["ERR22011"]);
                        $scope.errorMap.put("shipper", $rootScope.nls["ERR22011"]);
                        return false
                    }

                }

            }



            if (validateCode == 0 || validateCode == 7) {

                if ($scope.quotation.shipperAddress.addressLine1 == null ||
                    $scope.quotation.shipperAddress.addressLine1 == "" || $scope.quotation.shipperAddress.addressLine1 == undefined) {
                    /*As discussed with mani we are removed
                     * $scope.errorMap.put("quotation.shipperAddress.addressLine1",$rootScope.nls["ERR22030"]);
                    console.log($rootScope.nls["ERR22030"]);
                    return false;*/
                }
            }

            if (validateCode == 0 || validateCode == 8) {
                if ($scope.quotation.shipperAddress.addressLine2 == null ||
                    $scope.quotation.shipperAddress.addressLine2 == "" ||
                    $scope.quotation.shipperAddress.addressLine2 == undefined) {
                    /*As discussed with mani we are removed
                     * $scope.errorMap.put("quotation.shipperAddress.addressLine2",$rootScope.nls["ERR22031"]);
                    console.log($rootScope.nls["ERR22031"]);
                    return false;*/
                }
            }


            if (validateCode == 0 || validateCode == 10) {
                if ($scope.quotation.shipperAddress.addressLine4 == null ||
                    $scope.quotation.shipperAddress.addressLine4 == "" || $scope.quotation.shipperAddress.addressLine4 == undefined) {
                    /*As discussed with mani we are removed
                     * $scope.errorMap.put("quotation.shipperAddress.addressLine4",$rootScope.nls["ERR22099"]);
                    console.log($rootScope.nls["ERR22099"]);
                    return false;*/
                }
            }


            if (validateCode == 0 || validateCode == 11) {
                if ($scope.quotation.loggedOn == undefined ||
                    $scope.quotation.loggedOn == null ||
                    $scope.quotation.loggedOn.trim == "") {
                    console.log($rootScope.nls["ERR22101"]);
                    $scope.errorMap.put("loggedOn", $rootScope.nls["ERR22101"]);
                    return false;
                }


                // Valid From should be less than logged on
                if ($scope.quotation.validFrom != null) {
                    var validFrom = $rootScope.convertToDate($scope.quotation.validFrom);
                    var loggedOn = $rootScope.convertToDate($scope.quotation.loggedOn);

                    if (loggedOn > validFrom) {
                        $scope.errorMap.put("loggedOn", $rootScope.nls["ERR22155"]);
                        console.log($rootScope.nls["ERR22155"]);
                        return false;
                    }
                }


                if ($scope.quotation.validTo != null) {
                    var validTo = $rootScope.convertToDate($scope.quotation.validTo);
                    var loggedOn = $rootScope.convertToDate($scope.quotation.loggedOn);

                    if (loggedOn > validTo) {
                        $scope.errorMap.put("loggedOn", $rootScope.nls["ERR22150"]);
                        console.log($rootScope.nls["ERR22150"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 12) {
                if ($scope.quotation.loggedBy == null || $scope.quotation.loggedBy.id == undefined ||
                    $scope.quotation.loggedBy.id == null ||
                    $scope.quotation.loggedBy.id == "") {
                    console.log($rootScope.nls["ERR22090"]);
                    $scope.errorMap.put("loggedBy", $rootScope.nls["ERR22090"]);
                    return false;
                } else if ($scope.quotation.loggedBy.employementStatus != null) {

                    if (ValidateUtil.isEmployeeResigned($scope.quotation.loggedBy.employementStatus)) {
                        console.log("Selected Logged By User is Resigned");
                        $scope.quotation.loggedBy = null;
                        console.log($rootScope.nls["ERR22036"]);
                        $scope.errorMap.put("loggedBy", $rootScope.nls["ERR22036"]);
                        return false

                    }
                    if (ValidateUtil.isEmployeeTerminated($scope.quotation.loggedBy.employementStatus)) {
                        console.log("Selected Logged By User is Terminated");
                        $scope.quotation.loggedBy = null;
                        console.log($rootScope.nls["ERR22037"]);
                        $scope.errorMap.put("loggedBy", $rootScope.nls["ERR22037"]);
                        return false

                    }
                }
            }



            if (validateCode == 0 || validateCode == 13) {
                // Reserved for Enquiry Number
            }

            if (validateCode == 0 || validateCode == 14) {
                // Reserved for Booking Number	    		
            }



            if (validateCode == 0 || validateCode == 15) {

                // Valid From is Mandatory
                if ($scope.quotation.validFrom == undefined ||
                    $scope.quotation.validFrom == null ||
                    $scope.quotation.validFrom.trim == "") {
                    $scope.errorMap.put("validFrom", $rootScope.nls["ERR22004"]);
                    console.log($rootScope.nls["ERR22004"]);
                    return false;
                }

                // Valid from should be less than valid to.
                if ($scope.quotation.validTo != null) {
                    var validFrom = $rootScope.convertToDate($scope.quotation.validFrom);
                    var validTo = $rootScope.convertToDate($scope.quotation.validTo);


                    if (validFrom > validTo) {
                        $scope.errorMap.put("validFrom", $rootScope.nls["ERR22131"]);
                        console.log($rootScope.nls["ERR22131"]);
                        return false;
                    }
                }


                // Valid From should be less than logged on
                if ($scope.quotation.loggedOn != null) {
                    var validFrom = $rootScope.convertToDate($scope.quotation.validFrom);
                    var loggedOn = $rootScope.convertToDate($scope.quotation.loggedOn);

                    if (loggedOn > validFrom) {
                        $scope.errorMap.put("validFrom", $rootScope.nls["ERR22130"]);
                        console.log($rootScope.nls["ERR22130"]);
                        return false;
                    }
                }

            }




            if (validateCode == 0 || validateCode == 16) {

                // validTo date is mandatory
                if ($scope.quotation.validTo == undefined ||
                    $scope.quotation.validTo == null ||
                    $scope.quotation.validTo.trim == "") {
                    console.log($rootScope.nls["ERR22005"]);
                    $scope.errorMap.put("validTo", $rootScope.nls["ERR22005"]);
                    return false;
                }


                if ($scope.quotation.validFrom != null) {

                    var validFrom = $rootScope.convertToDate($scope.quotation.validFrom);
                    var validTo = $rootScope.convertToDate($scope.quotation.validTo);

                    if (validTo < validFrom) {
                        console.log($rootScope.nls["ERR22151"]);
                        $scope.errorMap.put("validTo", $rootScope.nls["ERR22151"]);
                        return false;
                    }
                }


                if ($scope.quotation.loggedOn != null) {
                    var validTo = $rootScope.convertToDate($scope.quotation.validTo);
                    var loggedOn = $rootScope.convertToDate($scope.quotation.loggedOn);

                    if (loggedOn > validTo) {
                        $scope.errorMap.put("validTo", $rootScope.nls["ERR22150"]);
                        console.log($rootScope.nls["ERR22150"]);
                        return false;
                    }
                }



            }




            if (validateCode == 0 || validateCode == 17) {
                if ($scope.quotation.attention == undefined ||
                    $scope.quotation.attention == null ||
                    $scope.quotation.attention == "") {
                    /*console.log($rootScope.nls["ERR22038"]);
                    $scope.errorMap.put("attention",$rootScope.nls["ERR22038"]);
                    return false;*/
                } else {
                    if ($scope.quotation.attention != null) {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CONTACT_PERSON, $scope.quotation.attention)) {
                            console.log("Invalid Attention...");
                            $scope.errorMap.put("attention", $rootScope.nls["ERR22039"]);
                            return false;
                        }
                    }
                }
            }




            if (validateCode == 0 || validateCode == 18) {

                // Routed By Flag is true
                // Routed By Column is mandatory (Salesman or Agent)
                if ($scope.routedByFlag) {
                    if ($scope.quotation.whoRouted) {
                        if ($scope.quotation.agent == undefined || $scope.quotation.agent == null || $scope.quotation.agent.id == null) {
                            $scope.quotation.agent = null;
                            console.log($rootScope.nls["ERR22165"]);
                            $scope.errorMap.put("salesman", $rootScope.nls["ERR22165"]);
                            return false;
                        } else {

                        }
                        $scope.quotation.salesman = null;

                    } else {
                        if ($scope.quotation.salesman == undefined || $scope.quotation.salesman == null || $scope.quotation.salesman.id == null) {
                            $scope.quotation.salesman = null;
                            console.log($rootScope.nls["ERR22164"]);
                            $scope.errorMap.put("salesman", $rootScope.nls["ERR22164"]);
                            return false;
                        } else {
                            if (!ValidateUtil.isEmployeeSalesman($scope.quotation.salesman.isSalesman)) {
                                $scope.quotation.salesman = null;
                                console.log($rootScope.nls["ERR22014"]);
                                $scope.errorMap.put("salesman", $rootScope.nls["ERR22014"]);
                                return false
                            }

                            if (ValidateUtil.isEmployeeResigned($scope.quotation.salesman.employementStatus)) {
                                $scope.quotation.salesman = null;
                                console.log($rootScope.nls["ERR22015"]);
                                $scope.errorMap.put("salesman", $rootScope.nls["ERR22015"]);
                                return false
                            }
                            if (ValidateUtil.isEmployeeTerminated($scope.quotation.salesman.employementStatus)) {
                                $scope.quotation.salesman = null;
                                console.log($rootScope.nls["ERR22016"]);
                                $scope.errorMap.put("salesman", $rootScope.nls["ERR22016"]);
                                return false
                            }
                        }

                        $scope.quotation.agent = null;
                    }

                } else {
                    // Routed By Flag is false (Salesman is displayed and is not mandatory.)
                    if ($scope.quotation.salesman != null && $scope.quotation.salesman.id != null && $scope.quotation.salesman.employementStatus != null) {
                        if (!ValidateUtil.isEmployeeSalesman($scope.quotation.salesman.isSalesman)) {
                            $scope.quotation.salesman = null;
                            console.log($rootScope.nls["ERR22014"]);
                            $scope.errorMap.put("salesman", $rootScope.nls["ERR22014"]);
                            return false
                        }

                        if (ValidateUtil.isEmployeeResigned($scope.quotation.salesman.employementStatus)) {
                            $scope.quotation.salesman = null;
                            console.log($rootScope.nls["ERR22015"]);
                            $scope.errorMap.put("salesman", $rootScope.nls["ERR22015"]);
                            return false
                        }
                        if (ValidateUtil.isEmployeeTerminated($scope.quotation.salesman.employementStatus)) {
                            $scope.quotation.salesman = null;
                            console.log($rootScope.nls["ERR22016"]);
                            $scope.errorMap.put("salesman", $rootScope.nls["ERR22016"]);
                            return false
                        }
                    }

                }



            }

            if (validateCode == 0 || validateCode == 19) {
                if ($scope.quotation.salesCoordinator == null || $scope.quotation.salesCoordinator.id == undefined ||
                    $scope.quotation.salesCoordinator.id == null ||
                    $scope.quotation.salesCoordinator.id == "") {
                    console.log($rootScope.nls["ERR22017"]);
                    $scope.errorMap.put("salesCoordinator", $rootScope.nls["ERR22017"]);
                    return false;
                } else if ($scope.quotation.salesCoordinator.employementStatus != null) {

                    if (ValidateUtil.isEmployeeResigned($scope.quotation.salesCoordinator.employementStatus)) {
                        $scope.quotation.salesCoordinator = null;
                        console.log($rootScope.nls["ERR22020"]);
                        $scope.errorMap.put("salesCoordinator", $rootScope.nls["ERR22020"]);
                        return false

                    }
                    if (ValidateUtil.isEmployeeTerminated($scope.quotation.salesCoordinator.employementStatus)) {
                        $scope.quotation.salesCoordinator = null;
                        console.log($rootScope.nls["ERR22021"]);
                        $scope.errorMap.put("salesCoordinator", $rootScope.nls["ERR22021"]);
                        return false

                    }
                }
            }


            if (validateCode == 0 || validateCode == 20) {

            }

            return true;
        }


        // Validating General Notes Tab...

        $scope.validateGeneralNotes = function(validateCode) {

            $scope.errorMap = new Map();
            console.log("Validating General Notes.... " + validateCode);

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.quotation.headerNote == undefined ||
                    $scope.quotation.headerNote == null ||
                    $scope.quotation.headerNote == "") {
                    $scope.detailTab = 'notes';
                    $scope.errorMap.put("headerNote", $rootScope.nls["ERR22102"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.quotation.footerNote == undefined ||
                    $scope.quotation.footerNote == null ||
                    $scope.quotation.footerNote == "") {
                    $scope.detailTab = 'notes';
                    $scope.errorMap.put("footerNote", $rootScope.nls["ERR22103"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 3) {
                if ($scope.quotation.generalNoteList != null && $scope.quotation.generalNoteList.length != 0) {

                    for (var i = 0; i < $scope.quotation.generalNoteList.length; i++) {
                        if ($scope.quotation.generalNoteList[i].note == undefined ||
                            $scope.quotation.generalNoteList[i].note == null ||
                            $scope.quotation.generalNoteList[i].note == "" ||
                            $scope.quotation.generalNoteList[i].note.trim == "") {
                            $scope.detailTab = 'notes';
                            $scope.quotation.generalNoteList.splice(i, 1);
                            //$scope.errorMap.put("gNote"+i,$rootScope.nls["ERR22104"]);
                            //return false;
                        }

                    }

                }
            }

            return true;

        }

        /* Service List Picker */

        $scope.showServiceMasterList = false;

        $scope.serviceMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "serviceName",
                    seperator: false
                },
                {
                    "title": "serviceCode",
                    seperator: true
                }
            ]
        }

        $scope.showServiceMaster = function(name) {
            console.log("Showing List of Services...");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Service";

        };




        $scope.ajaxServiceEvent = function(object) {
            console.log("ajaxServiceEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


            return ServiceByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.serviceMasterList = data.responseObject.searchResult;
                        console.log("serviceMasterList ", $scope.serviceMasterList);
                        //$scope.showServiceMasterList=true;
                        return $scope.serviceMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching ServiceMaster');
                }


            );

        }



        $scope.selectedServiceMaster = function(id) {
            if ($scope.validateQuotationDetail(1)) {

                //get default air note while creating
                $scope.getDefaultAirNote();

                $scope.navigateToNextField(id);

            }
        };

        $scope.cancelServiceMaster = function() {
            $scope.showServiceMasterList = false;
        }

        $scope.showOriginList = false;
        $scope.originlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showOrigin = function(name) {
            console.log("Showing List of Origins.....");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Origin";

        };

        $scope.selectedOrigin = function(id) {
            if ($scope.validateQuotationDetail(2)) {
                $scope.validateQuotationDetail(4);
                $scope.navigateToNextField(id);
            }
        };


        $scope.ajaxOriginEvent = function(object) {
            console.log("ajaxOriginEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.originList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            for (var i = 0; i < resultList.length; i++) {
                                $scope.originList.push(resultList[i]);
                            }
                        }
                        //$scope.showOriginList=true;
                        return $scope.originList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Origin List');
                }

            );


        }




        $scope.cancelOrigin = function() {
            $scope.showOriginList = false;
        }


        /* List of Destination - List Picker */

        $scope.showDestinationList = false;

        $scope.destinationlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDestination = function(name) {
            console.log("Showing List of Destination");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Destination";

        };

        $scope.ajaxDestination = function(object) {

            console.log("ajaxDestination ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            //if($scope.validateQuotationDetail(2)) {

            return PortByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.destinationList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            for (var i = 0; i < resultList.length; i++) {
                                if (resultList[i].id != $scope.quotationDetail.origin.id) {
                                    $scope.destinationList.push(resultList[i]);
                                }
                            }
                            return $scope.destinationList;
                        }
                        //$scope.showDestinationList=true;

                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Origin List');
                }
            );
            //}


        }



        $scope.selectedDestination = function(id) {
            if ($scope.validateQuotationDetail(3)) {
                $scope.validateQuotationDetail(4);
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelDestination = function() {
            $scope.showDestinationList = false;
        }


        /*List of Tos - Select Picker*/

        $scope.showTosList = false;

        $scope.toslistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "tosName",
                    seperator: false
                },
                {
                    "title": "tosCode",
                    seperator: true
                }
            ]
        }

        $scope.showTos = function(name) {
            console.log("Showing List of TOS..");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Tos";

        };

        $scope.ajaxTosEvent = function(object) {
            console.log("ajaxTosEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.tosList = data.responseObject.searchResult;
                        //$scope.showTosList=true;
                        return $scope.tosList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Tos');
                }
            );

        }


        $scope.selectedTos = function(id) {
            if ($scope.validateQuotationDetail(8)) {
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelTos = function() {
            $scope.showTosList = false;
        }



        $scope.showCarrierMasterList = false;

        $scope.carrierMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "carrierName",
                    seperator: false
                },
                {
                    "title": "carrierCode",
                    seperator: true
                }
            ]
        }

        $scope.showCarrierMaster = function(name) {
            console.log("showing list of carriers......");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Carrier";

        };

        $scope.ajaxCarrierEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;



            return CarrierByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.carrierMasterList = data.responseObject.searchResult;
                        //$scope.showCarrierMasterList=true;
                        return $scope.carrierMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier List Based on Air');
                }
            );

        }



        $scope.selectedCarrierMaster = function(id, quotationCarrier, index) {
            if ($scope.validateQuotationCarrier(5, quotationCarrier, index)) {
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelCarrierMaster = function() {
            $scope.showCarrierMasterList = false;
        }


        /* Commodity List Picker */

        $scope.showCommodityMasterList = false;

        $scope.commodityMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "hsName",
                    seperator: false
                },
                {
                    "title": "hsCode",
                    seperator: true
                }
            ]
        }

        $scope.showCommodityMaster = function(name) {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Commodity";

        };

        $scope.ajaxCommodityEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            CommoditySearchKeyword.fetch($scope.searchDto).$promise.then(function(
                    data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.commodityMasterList = data.responseObject.searchResult;
                        //$scope.showCommodityMasterList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching CommodityMaster');
                }
            );
        }



        $scope.selectedCommodityMaster = function(obj) {
            $scope.quotationDetail.commodityMaster = obj;
            $scope.validateQuotationDetail(7);
            $scope.cancelCommodityMaster();
        };

        $scope.cancelCommodityMaster = function() {
            $scope.showCommodityMasterList = false;
        }


        $scope.showFrequencyList = false;

        $scope.frequencylistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "frequencyName",
                    seperator: false
                },
                {
                    "title": "frequencyCode",
                    seperator: true
                }
            ]
        }

        $scope.showFrequency = function() {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Frequency";


        };

        $scope.ajaxFrequencyEvent = function(object) {

            console.log("showing list of frequency...", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PeriodList.get().$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.frequencyList = data.responseObject;
                        //$scope.showFrequencyList=true;
                        return $scope.frequencyList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Frequency');
                }
            );

        }

        $scope.selectedFrequency = function(id, quotationCarrier, index) {
            if ($scope.validateQuotationCarrier(7, quotationCarrier, index)) {
                $scope.navigateToNextField(id);
            }
        };

        $scope.cancelFrequency = function() {
            console.log("Canceling Frequency List..");
            $scope.showFrequencyList = false;
        }


        $scope.getDefaultAirNote = function() {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_DEFAULT_AIR_NOTE_LIST)) {
                if ($scope.quotationDetail.serviceMaster == undefined || $scope.quotationDetail.serviceMaster.id == undefined) {
                    Notification.warning($rootScope.nls["ERR23102"]);
                    return false;
                }
                GeneralNote.fetch({
                    locationId: $scope.quotation.locationMaster.id,
                    category: 'Quotation',
                    subCategory: 'Service',
                    serviceMasterId: $scope.quotationDetail.serviceMaster.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {

                        if ($scope.quotationDetail.serviceNoteList != null && $scope.quotationDetail.serviceNoteList.length != 0) {
                            for (var i = 0; i < data.responseObject.length; i++) {
                                for (var j = 0; j < $scope.quotationDetail.serviceNoteList.length; j++) {

                                    if ($scope.quotationDetail.serviceNoteList[j].note == undefined) {
                                        continue;
                                    }

                                    if ($scope.quotationDetail.serviceNoteList[j].note.toUpperCase() == data.responseObject[i].note.toUpperCase()) {
                                        $scope.quotationDetail.serviceNoteList.splice(j, 1);
                                    }

                                }
                            }
                        }

                        for (var i = 0; i < data.responseObject.length; i++) {
                            $scope.gNote = {};
                            $scope.gNote.note = data.responseObject[i].note;

                            if ($scope.quotationDetail.serviceNoteList == undefined || $scope.quotationDetail.serviceNoteList == null || $scope.quotationDetail.serviceNoteList.length == 0) {
                                $scope.quotationDetail.serviceNoteList = [];
                                $scope.quotationDetail.serviceNoteList.push($scope.gNote);
                            } else {
                                $scope.quotationDetail.serviceNoteList.splice(0, 0, $scope.gNote);
                            }
                            /*if($scope.quotationDetail.serviceNoteList==null || $scope.quotationDetail.serviceNoteList.length==0){
                                $scope.quotationDetail.serviceNoteList = [];
                            }
                            $scope.quotationDetail.serviceNoteList.push($scope.gNote);*/
                        }

                        if ($scope.quotationDetail.serviceNoteList == null || $scope.quotationDetail.serviceNoteList == undefined ||
                            $scope.quotationDetail.serviceNoteList.length == 0) {
                            $scope.addAirNote();
                        }

                    } else {
                        console.log("GeneralNote Failed " +
                            data.responseDescription)
                    }
                }, function(error) {
                    console.log("GeneralNote Failed : " + error)
                });


            }
        }


        $scope.createDefaultAirNote = function() {
            if ($scope.quotationDetail.serviceNoteList == null || $scope.quotationDetail.serviceNoteList.length == 0) {
                $scope.quotationDetail.serviceNoteList = [];
                $scope.quotationDetail.serviceNoteList.push({});
            }
        }

        $scope.addAirNote = function() {
            if ($scope.quotationDetail.serviceNoteList == null || $scope.quotationDetail.serviceNoteList.length == 0) {
                $scope.quotationDetail.serviceNoteList = [];
                $scope.quotationDetail.serviceNoteList.push({});
            }
            if ($scope.quotationDetail.serviceNoteList[$scope.quotationDetail.serviceNoteList.length - 1] != null &&
                $scope.quotationDetail.serviceNoteList[$scope.quotationDetail.serviceNoteList.length - 1].note != null) {
                $scope.quotationDetail.serviceNoteList.push({});
            }

        }

        $scope.deleteAirNote = function(index) {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_DELETE)) {
                $scope.quotationDetail.serviceNoteList.splice(index, 1);
            }
        }



        $scope.fnDimensionUnit = function() {
            if ($scope.quotationDetail.dimensionUnit != undefined) {
                if ($scope.quotationDetail.dimensionUnit == false) {
                    $scope.quotationDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                } else {
                    $scope.quotationDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                }
                $scope.changeVolume();
            }
        };



        $scope.dimensionAddShow = function() {
            console.log("Add Dimension button is pressed....");
            $scope.firstFocusInDimension = true;
            $scope.editIndex = null;
            $scope.showEditDimension = true;
            $scope.quotationDimension = {};
            $scope.errorMap = new Map();
            if ($scope.quotationDetail.quotationDimensionList == null)
                $scope.quotationDetail.quotationDimensionList = [];

            discardService.set($scope.quotationDimension);
        }

        $scope.editIndex = null;
        $scope.dimensionEditShow = function(index) {
            console.log("Edit Dimension button is pressed....");
            $scope.firstFocusInDimension = true;
            $scope.editIndex = index;
            $scope.errorMap = new Map();
            $scope.showEditDimension = true;
            $scope.quotationDimension = cloneService.clone($scope.quotationDetail.quotationDimensionList[index]);
            discardService.set($scope.quotationDimension);
        }


        $scope.dimensionRemoveShow = function(index) {
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected Dimension  ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                $scope.quotationDetail.quotationDimensionList.splice(index, 1);
                $scope.changeVolume();
                $scope.showEditDimension = false;
            }, function(value) {
                console.log("Dimension Deletion cancelled");
            });
        }

        $scope.discardDimension = function() {
            if (!discardService.compare($scope.quotationDimension)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR201"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.saveDimension();
                    } else {
                        $scope.closeDimension();
                    }
                });

            } else {
                $scope.closeDimension();
            }
        }

        $scope.closeDimension = function() {

            $scope.showEditDimension = false;
            $scope.showEditContainerDetails = false;
        }

        $scope.saveDimension = function() {
            if ($scope.validateDimension(0)) {
                if ($scope.editIndex == null) {
                    if ($scope.quotationDetail.quotationDimensionList == null)
                        $scope.quotationDetail.quotationDimensionList = [];
                    $scope.quotationDetail.quotationDimensionList
                        .push($scope.quotationDimension);
                } else {
                    $scope.quotationDetail.quotationDimensionList
                        .splice($scope.editIndex, 1, $scope.quotationDimension);
                }
                $scope.showEditDimension = false;
            } else {

                console.log("Dimension Validation Failed");
            }
            $scope.changeVolume();

        }

        $scope.caluculateVolumeWeight = function() {

            if ($scope.quotationDimension.noOfPiece > 0 && $scope.quotationDimension.length > 0 &&
                $scope.quotationDimension.width > 0 && $scope.quotationDimension.height > 0) {

                $scope.quotationDimension.volWeight = ($scope.quotationDimension.noOfPiece * ($scope.quotationDimension.length * $scope.quotationDimension.width * $scope.quotationDimension.height)) / $scope.quotationDetail.dimensionUnitValue;
                $scope.quotationDimension.volWeight = Math.round($scope.quotationDimension.volWeight * 100) / 100;
            } else {
                $scope.quotationDimension.volWeight = "";
            }

        }

        $scope.calculateWeight = function(param) {

            if ($scope.quotationDetail.quotationDimensionList == undefined ||
                $scope.quotationDetail.quotationDimensionList == null ||
                $scope.quotationDetail.quotationDimensionList.length == 0) {
                $scope.quotationDetail.quotationDimensionList = [];
                if (param.removeFlag != undefined && !param.removeFlag) {
                    $scope.quotationDetail.chargebleWeight = 0.0
                    $scope.quotationDetail.grossWeight = 0.0;
                    $scope.quotationDetail.volumeWeight = 0.0;
                }
                return;
            }
            if (param.removeFlag) {

                if ($scope.quotationDetail.dimensionUnit) { //Pound to Kg
                    $scope.quotationDetail.volumeWeight = Math.round(param.volumeWeight * 100) / 100;
                    $scope.quotationDetail.grossWeight = Math.round((param.grossWeight * 0.45359237) * 100) / 100;
                } else {
                    $scope.quotationDetail.volumeWeight = param.volumeWeight;
                    $scope.quotationDetail.grossWeight = param.grossWeight;
                }
                $scope.quotationDetail.dimensionVolWeight = param.volumeWeight;
                $scope.quotationDetail.dimensionGrossWeight = param.grossWeight;
                $scope.quotationDetail.dimensionGrossWeightInKg = param.dimensionGrossWeightInKg;
                $scope.quotationDetail.totalNoOfPieces = param.totalPieces;
            }
            if (param.volumeWeight >= 0 && param.grossWeight >= 0 && param.totalPieces >= 0 && !param.removeFlag) {
                $scope.quotationDetail.dimensionVolWeight = param.volumeWeight;
                $scope.quotationDetail.dimensionGrossWeight = param.grossWeight;
                $scope.quotationDetail.totalNoOfPieces = param.totalPieces;
                $scope.quotationDetail.dimensionGrossWeightInKg = param.dimensionGrossWeightInKg;

                if ($scope.quotationDetail.dimensionUnit) { //Pound to Kg
                    $scope.quotationDetail.volumeWeight = Math.round(param.volumeWeight * 100) / 100;
                    $scope.quotationDetail.grossWeight = Math.round((param.grossWeight * 0.45359237) * 100) / 100;
                } else {
                    $scope.quotationDetail.volumeWeight = param.volumeWeight;
                    $scope.quotationDetail.grossWeight = param.grossWeight;
                }

            }

            if ($scope.quotationDetail.volumeWeight >= 0) {
                $scope.quotationDetail.volumeWeight = $rootScope.roundOffWeight($scope.quotationDetail.volumeWeight);
            }

            if ($scope.quotationDetail.grossWeight >= 0) {
                $scope.quotationDetail.grossWeight = $rootScope.roundOffWeight($scope.quotationDetail.grossWeight);
            }

            $scope.fnCalculateChargeableWeight();
            $scope.isOverDimension = checkOverDimension();
        }

        var checkOverDimension = function(){
            var thresholdLength = 317;
            var thresholdWidth = 317;
            var thresholdHeight = 158;

            if(!$scope.quotationDetail) return;

            if($scope.quotationDetail.dimensionUnit){
                thresholdLength = 125;
                thresholdWidth = 125;
                thresholdHeight = 62;
            }
            for (var i = 0; i < $scope.quotationDetail.quotationDimensionList.length; i++) {
                var currentRowData = $scope.quotationDetail.quotationDimensionList[i];
                if(currentRowData.length > thresholdLength || currentRowData.width > thresholdWidth || currentRowData.height > thresholdHeight){
                    return true;
                }
            }
            return false;
        }

        $scope.changeVolume = function() {
            var grossWeight = 0.0;
            var volWeight = 0.0;
            var noOfPiece = 0;

            if ($scope.quotationDetail.quotationDimensionList == undefined || $scope.quotationDetail.quotationDimensionList == null || $scope.quotationDetail.quotationDimensionList.length == 0) {
                $scope.quotationDetail.dimensionVolWeight = 0.0;
                $scope.quotationDetail.dimensionGrossWeight = 0.0;
                $scope.quotationDetail.quotationDimensionList = [];
                return;
            }

            $scope.quotationDetail.chargebleWeight = 0.0
            $scope.quotationDetail.grossWeight = 0.0;
            $scope.quotationDetail.volumeWeight = 0.0;
            $scope.quotationDetail.totalNoOfPieces = 0;

            for (var i = 0; i < $scope.quotationDetail.quotationDimensionList.length; i++) {

                $scope.quotationDetail.quotationDimensionList[i].volWeight = ($scope.quotationDetail.quotationDimensionList[i].noOfPiece * ($scope.quotationDetail.quotationDimensionList[i].length * $scope.quotationDetail.quotationDimensionList[i].width * $scope.quotationDetail.quotationDimensionList[i].height)) / $scope.quotationDetail.dimensionUnitValue;
                $scope.quotationDetail.quotationDimensionList[i].volWeight = Math.round($scope.quotationDetail.quotationDimensionList[i].volWeight * 100) / 100;
                volWeight = volWeight + parseFloat($scope.quotationDetail.quotationDimensionList[i].volWeight);
                grossWeight = grossWeight + parseFloat($scope.quotationDetail.quotationDimensionList[i].grossWeight);
                noOfPiece = parseInt(noOfPiece) + parseInt($scope.quotationDetail.quotationDimensionList[i].noOfPiece);
            }

            $scope.quotationDetail.dimensionVolWeight = Math.round(volWeight * 100) / 100;
            $scope.quotationDetail.dimensionGrossWeight = Math.round(grossWeight * 100) / 100;
            $scope.quotationDetail.totalNoOfPieces = parseInt(noOfPiece);

            if ($scope.quotationDetail.dimensionUnit) { //Pound to Kg

                $scope.quotationDetail.volumeWeight = Math.round(volWeight * 100) / 100;
                $scope.quotationDetail.grossWeight = Math.round((grossWeight * 0.45359237) * 100) / 100;

            } else {
                $scope.quotationDetail.volumeWeight = Math.round(volWeight * 100) / 100;
                $scope.quotationDetail.grossWeight = Math.round(grossWeight * 100) / 100;
            }


            if ($scope.quotationDetail.volumeWeight >= 0) {
                $scope.quotationDetail.volumeWeight = $rootScope.roundOffWeight($scope.quotationDetail.volumeWeight);
            }

            if ($scope.quotationDetail.grossWeight >= 0) {
                $scope.quotationDetail.grossWeight = $rootScope.roundOffWeight($scope.quotationDetail.grossWeight);
            }

            $scope.fnCalculateChargeableWeight();
            $scope.isOverDimension = checkOverDimension();
        }

        $scope.fnCalculateChargeableWeight = function() {

            var volumeWeight = 0;
            var grossWeight = 0;

            if ($scope.quotationDetail.volumeWeight != null && $scope.quotationDetail.volumeWeight != undefined &&
                $scope.quotationDetail.volumeWeight != '' && isNaN($scope.quotationDetail.volumeWeight) == false) {
                volumeWeight = parseFloat($scope.quotationDetail.volumeWeight);
            }

            if ($scope.quotationDetail.grossWeight != null && $scope.quotationDetail.grossWeight != undefined &&
                $scope.quotationDetail.grossWeight != '' && isNaN($scope.quotationDetail.grossWeight) == false) {
                grossWeight = parseFloat($scope.quotationDetail.grossWeight);
            }


            if (volumeWeight >= 0 && grossWeight > 0) {
                if (volumeWeight < grossWeight) {
                    $scope.quotationDetail.chargebleWeight = grossWeight;
                } else {
                    $scope.quotationDetail.chargebleWeight = volumeWeight;
                }
            }

            if (volumeWeight < 0 || volumeWeight === null || volumeWeight === "" || grossWeight < 0

                ||
                grossWeight === null || grossWeight === "") {

                $scope.quotationDetail.chargebleWeight = "";

            }

        }

        $scope.validateDimension = function(validate) {

            $scope.errorMap = new Map();

            if (validate == 0 || validate == 1) {
                if ($scope.quotationDimension.noOfPiece == undefined || $scope.quotationDimension.noOfPiece == "" || $scope.quotationDimension.noOfPiece == null) {
                    $scope.errorMap.put("noOfPiece", $rootScope.nls["ERR22062"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp.test($scope.quotationDimension.noOfPiece)) {
                        $scope.errorMap.put("noOfPiece", $rootScope.nls["ERR22105"]);
                        return false;

                    }
                }


            }
            if (validate == 0 || validate == 2) {
                if ($scope.quotationDimension.length == undefined || $scope.quotationDimension.length == "" || $scope.quotationDimension.length == null) {
                    $scope.errorMap.put("length", $rootScope.nls["ERR22064"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.quotationDimension.length)) {
                        $scope.errorMap.put("length", $rootScope.nls["ERR22106"]);
                        return false;

                    }
                }
            }

            if (validate == 0 || validate == 3) {
                if ($scope.quotationDimension.width == undefined || $scope.quotationDimension.width == "" || $scope.quotationDimension.width == null) {
                    $scope.errorMap.put("width", $rootScope.nls["ERR22066"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.quotationDimension.width)) {

                        $scope.errorMap.put("width", $rootScope.nls["ERR22107"]);
                        return false;

                    }
                }
            }
            if (validate == 0 || validate == 4) {
                if ($scope.quotationDimension.height == undefined || $scope.quotationDimension.height == "" || $scope.quotationDimension.height == null) {
                    $scope.errorMap.put("height", $rootScope.nls["ERR22068"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.quotationDimension.height)) {

                        $scope.errorMap.put("height", $rootScope.nls["ERR22108"]);
                        return false;

                    }
                }
            }

            if (validate == 0 || validate == 6) {
                if ($scope.quotationDimension.grossWeight == undefined || $scope.quotationDimension.grossWeight == "" || $scope.quotationDimension.grossWeight == null) {
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22070"]);
                    return false;
                } else {


                    if ($scope.quotationDimension.grossWeight < 0 || $scope.quotationDimension.grossWeight > 9999999999.99) {
                        $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                        return false;
                    }
                    var regexp = new RegExp("^[+]?([0-9]{0,10}(?:[\.][0-9]{0,2})?|\.[0-9]+)$");
                    if (!regexp
                        .test($scope.quotationDimension.grossWeight)) {
                        $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                        return false;

                    }
                }
            }

            return true;

        }

        $scope.validateQuotationCarrier = function(validateCode, quotationCarrier, index) {
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 5) {
                if (quotationCarrier.carrierMaster != null && quotationCarrier.carrierMaster.id != undefined && quotationCarrier.carrierMaster.id != null) {

                    if (quotationCarrier.carrierMaster.status == "Block") {
                        quotationCarrier.carrierMaster = null;
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                        $scope.errorMap.put(index + "carrierMaster", $rootScope.nls["ERR22049"]);
                        return false

                    }
                    if (quotationCarrier.carrierMaster.status == "Hide") {
                        quotationCarrier.carrierMaster = null;
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                        $scope.errorMap.put(index + "carrierMaster", $rootScope.nls["ERR22050"]);
                        return false

                    }
                } else {
                    quotationCarrier.carrierMaster = null; // which is for empty auto complete giving empty string so we are making null
                }
                console.log("Carrier validation done...");
            }


            if (validateCode == 0 || validateCode == 6) {
                if (quotationCarrier.transitday == undefined ||
                    quotationCarrier.transitday == null ||
                    quotationCarrier.transitday == "") {
                    // Transit days is optional

                } else {
                    var regexp = new RegExp("^[0-9]{0,3}$");
                    if (!regexp.test(quotationCarrier.transitday)) {
                        $scope.errorMap.put(index + "transitday", $rootScope.nls["ERR22110"]);
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                        return false;
                    }
                }
                console.log("Transit validation done...");
            }

            if (validateCode == 0 || validateCode == 7) {
                if (quotationCarrier.frequency != null &&
                    quotationCarrier.frequency.id != undefined && quotationCarrier.frequency.id != null) {

                    if (quotationCarrier.frequency.status == "Block") {
                        quotationCarrier.frequency = null;
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                        console.log("Frequency Blocked", $rootScope.nls["ERR22140"]);
                        $scope.errorMap.put(index + "frequency", $rootScope.nls["ERR22140"]);
                        return false

                    }
                    if (quotationCarrier.frequency.status == "Hide") {
                        quotationCarrier.frequency = null;
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                        console.log("Frequency Hidden ", $rootScope.nls["ERR22141"]);
                        $scope.errorMap.put(index + "frequency", $rootScope.nls["ERR22141"]);
                        return false
                    }

                } else {
                    quotationCarrier.frequency = null;
                }
                console.log("frequency validation done...");
            }

            return true;

        }

        $scope.validateQuotationDetail = function(validateCode) {

            $scope.errorMap = new Map();

            console.log("Quotation Detail Validation Begins ", validateCode);

            if (validateCode == 0 || validateCode == 1) {

                if ($scope.quotationDetail.serviceMaster == null || $scope.quotationDetail.serviceMaster.id == undefined ||
                    $scope.quotationDetail.serviceMaster.id == null ||
                    $scope.quotationDetail.serviceMaster.id == "") {
                    /*$scope.errorMap.put("serviceMaster","Service is Mandatory");
                    return false;*/
                    $scope.quotationDetail.serviceMaster = null; // for empty string of auto complete clear
                } else if ($scope.quotationDetail.serviceMaster.status != null) {

                    if ($scope.quotationDetail.serviceMaster.status == "Block") {
                        $scope.quotationDetail.serviceMaster = null;
                        $scope.detailTab = 'Services';
                        $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR22040"]);
                        return false

                    }
                    if ($scope.quotationDetail.serviceMaster.status == "Hide") {
                        $scope.quotationDetail.serviceMaster = null;
                        $scope.detailTab = 'Services';
                        $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR22041"]);
                        return false

                    }

                }

                console.log("Service Master validation done...");
            }


            if (validateCode == 0 || validateCode == 2) {
                if ($scope.quotationDetail.origin == null || $scope.quotationDetail.origin.id == undefined ||
                    $scope.quotationDetail.origin.id == null ||
                    $scope.quotationDetail.origin.id == "") {
                    $scope.detailTab = 'Services';
                    $scope.errorMap.put("origin", $rootScope.nls["ERR22042"]);
                    return false;
                } else if ($scope.quotationDetail.origin.status != null) {

                    if ($scope.quotationDetail.origin.status == "Block") {
                        $scope.quotationDetail.origin = null;
                        $scope.detailTab = 'Services';
                        $scope.errorMap.put("origin", $rootScope.nls["ERR22043"]);
                        return false

                    }
                    if ($scope.quotationDetail.origin.status == "Hide") {
                        $scope.quotationDetail.origin = null;
                        $scope.detailTab = 'Services';
                        $scope.errorMap.put("origin", $rootScope.nls["ERR22044"]);
                        return false

                    }

                }
                console.log("Origin validation done...");
            }

            if (validateCode == 0 || validateCode == 3) {
                if ($scope.quotationDetail.destination == null || $scope.quotationDetail.destination.id == undefined ||
                    $scope.quotationDetail.destination.id == null ||
                    $scope.quotationDetail.destination.id == "") {
                    $scope.errorMap.put("destination", $rootScope.nls["ERR22045"]);
                    $scope.detailTab = 'Services';
                    return false;
                } else if ($scope.quotationDetail.destination.status != null) {

                    if ($scope.quotationDetail.destination.status == "Block") {
                        $scope.quotationDetail.destination = null;
                        $scope.detailTab = 'Services';
                        $scope.errorMap.put("destination", $rootScope.nls["ERR22046"]);
                        return false

                    }
                    if ($scope.quotationDetail.destination.status == "Hide") {
                        $scope.quotationDetail.destination = null;
                        $scope.detailTab = 'Services';
                        $scope.errorMap.put("destination", $rootScope.nls["ERR22047"]);
                        return false

                    }

                }
                console.log("Destination validation done...");
            }

            if (validateCode == 0 || validateCode == 4) {
                if ($scope.quotationDetail.origin != null && $scope.quotationDetail.origin.portCode != null &&
                    $scope.quotationDetail.destination != null && $scope.quotationDetail.destination.portCode) {
                    if ($scope.quotationDetail.origin.portCode == $scope.quotationDetail.destination.portCode) {
                        $scope.errorMap.put("similarPorts", $rootScope.nls["ERR22048"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 8) {

                if ($scope.quotationDetail.tosMaster == undefined ||
                    $scope.quotationDetail.tosMaster == null ||
                    $scope.quotationDetail.tosMaster.id == null) {
                    $scope.detailTab = 'Services';
                    $scope.errorMap.put("tosMaster", $rootScope.nls["ERR22051"]);
                    return false;
                }

                if ($scope.quotationDetail.tosMaster.id != null) {

                    if ($scope.quotationDetail.tosMaster.status == "Block") {
                        $scope.quotationDetail.tosMaster = null;
                        $scope.errorMap.put("tosMaster", $rootScope.nls["ERR22052"]);
                        $scope.detailTab = 'Services';
                        return false

                    }
                    if ($scope.quotationDetail.tosMaster.status == "Hide") {
                        $scope.quotationDetail.tosMaster = null;
                        $scope.errorMap.put("tosMaster", $rootScope.nls["ERR22053"]);
                        $scope.detailTab = 'Services';
                        return false
                    }

                }
                console.log("Tos validation done...");
            }



            if (validateCode == 0 || validateCode == 9) {
                /*
                                if($scope.quotationDetail.commodityMaster!=null && $scope.quotationDetail.commodityMaster.id !=null){
                                	
                                    if($scope.quotationDetail.commodityMaster.status=="Block"){
                                        $scope.quotationDetail.commodityMaster = null;
                                        $scope.detailTab = 'Services';
                                        $scope.errorMap.put("commodityMaster",$rootScope.nls["ERR22054"]);
                                        return false
                                    	
                                    }
                                    if($scope.quotationDetail.commodityMaster.status=="Hide"){
                                        $scope.quotationDetail.commodityMaster = null;
                                        $scope.detailTab = 'Services';
                                        $scope.errorMap.put("commodityMaster",$rootScope.nls["ERR22055"]);
                                        return false
                                    }
                                	
                            }
                                console.log("Commodity validation done...");
                            */
            }




            if (validateCode == 0 || validateCode == 10) {
                if ($scope.quotationDetail.grossWeight == undefined || $scope.quotationDetail.grossWeight == null || $scope.quotationDetail.grossWeight == "") {
                    $scope.detailTab = 'Services';
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                    return false;
                } else {
                    if ($scope.quotationDetail.quotationDimensionList != undefined && $scope.quotationDetail.quotationDimensionList.length == 0) {
                        if ($scope.quotationDetail.grossWeight < 0 || $scope.quotationDetail.grossWeight > 9999999999.99) {
                            $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                            return false;
                        }
                    }

                    if ($scope.quotationDetail.quotationDimensionList != undefined && $scope.quotationDetail.quotationDimensionList.length == 0) {
                        if ($scope.quotationDetail.grossWeight != null && $scope.quotationDetail.grossWeight != undefined && $scope.quotationDetail.grossWeight != "") {
                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG, $scope.quotationDetail.grossWeight)) {
                                $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                                return false;
                            }
                        }
                    }
                }
                console.log("Gross Weight validation done...");
            }


            if (validateCode == 0 || validateCode == 11) {

                if (isNaN($scope.quotationDetail.volumeWeight)) {
                    $scope.quotationDetail.volumeWeight = 0.0;
                }

                if ($scope.quotationDetail.volumeWeight != undefined &&
                    $scope.quotationDetail.volumeWeight != null &&
                    $scope.quotationDetail.volumeWeight >= 0) {

                    if ($scope.quotationDetail.quotationDimensionList != undefined && $scope.quotationDetail.quotationDimensionList.length == 0) {
                        if ($scope.quotationDetail.volumeWeight < 0 || $scope.quotationDetail.volumeWeight > 9999999999.99) {
                            $scope.errorMap.put("volumeWeight", $rootScope.nls["ERR22112"]);
                            return false;
                        }
                    }

                    if ($scope.quotationDetail.quotationDimensionList != undefined && $scope.quotationDetail.quotationDimensionList.length == 0) {
                        if ($scope.quotationDetail.volumeWeight != null && $scope.quotationDetail.volumeWeight != undefined && $scope.quotationDetail.volumeWeight != "") {
                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT, $scope.quotationDetail.volumeWeight)) {
                                $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22112"]);
                                return false;
                            }
                        }
                    }

                } else {
                    $scope.detailTab = 'Services';
                    $scope.errorMap.put("volumeWeight", $rootScope.nls["ERR22112"]);
                    return false;
                }
                console.log("Volume Weight validation done...");
            }

            if (validateCode == 0 || validateCode == 12) {
                if ($scope.quotationDetail.pickupAddress != undefined && $scope.quotationDetail.pickupAddress != null && $scope.quotationDetail.pickupAddress.city != null && $scope.quotationDetail.pickupAddress.city.status == 'Block') {
                    $scope.errorMap.put("pickupCity", $rootScope.nls["ERR8845"]);
                    return false;
                } else if ($scope.quotationDetail.pickupAddress != undefined && $scope.quotationDetail.pickupAddress != null && $scope.quotationDetail.pickupAddress.city != null && $scope.quotationDetail.pickupAddress.city.status == 'Hide') {
                    $scope.errorMap.put("pickupCity", $rootScope.nls["ERR8848"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 13) {
                if ($scope.quotationDetail.pickupAddress != undefined && $scope.quotationDetail.pickupAddress != null && $scope.quotationDetail.pickupAddress.state != null && $scope.quotationDetail.pickupAddress.state.status == 'Block') {
                    $scope.errorMap.put("pickupState", $rootScope.nls["ERR8846"]);
                    return false;
                } else if ($scope.quotationDetail.pickupAddress != undefined && $scope.quotationDetail.pickupAddress != null && $scope.quotationDetail.pickupAddress.state != null && $scope.quotationDetail.pickupAddress.state.status == 'Hide') {
                    $scope.errorMap.put("pickupState", $rootScope.nls["ERR8849"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 14) {
                if ($scope.quotationDetail.pickupAddress != undefined && $scope.quotationDetail.pickupAddress != null && $scope.quotationDetail.pickupAddress.country != null && $scope.quotationDetail.pickupAddress.country.status == 'Block') {
                    $scope.errorMap.put("pickupCountry", $rootScope.nls["ERR8847"]);
                    return false;
                } else if ($scope.quotationDetail.pickupAddress != undefined && $scope.quotationDetail.pickupAddress != null && $scope.quotationDetail.pickupAddress.country != null && $scope.quotationDetail.pickupAddress.country.status == 'Hide') {
                    $scope.errorMap.put("pickupCountry", $rootScope.nls["ERR8850"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 15) {
                if ($scope.quotationDetail.deliveryAddress != undefined && $scope.quotationDetail.deliveryAddress != null && $scope.quotationDetail.deliveryAddress.city != null && $scope.quotationDetail.deliveryAddress.city.status == 'Block') {
                    $scope.errorMap.put("deliveryCity", $rootScope.nls["ERR8845"]);
                    return false;
                } else if ($scope.quotationDetail.deliveryAddress != undefined && $scope.quotationDetail.deliveryAddress != null && $scope.quotationDetail.deliveryAddress.city != null && $scope.quotationDetail.deliveryAddress.city.status == 'Hide') {
                    $scope.errorMap.put("deliveryCity", $rootScope.nls["ERR8848"]);
                    return false;
                }

            }
            if (validateCode == 0 || validateCode == 16) {
                if ($scope.quotationDetail.deliveryAddress != undefined && $scope.quotationDetail.deliveryAddress != null && $scope.quotationDetail.deliveryAddress.state != null && $scope.quotationDetail.deliveryAddress.state.status == 'Block') {
                    $scope.errorMap.put("deliveryState", $rootScope.nls["ERR8846"]);
                    return false;
                } else if ($scope.quotationDetail.deliveryAddress != undefined && $scope.quotationDetail.deliveryAddress != null && $scope.quotationDetail.deliveryAddress.state != null && $scope.quotationDetail.deliveryAddress.state.status == 'Hide') {
                    $scope.errorMap.put("deliveryState", $rootScope.nls["ERR8849"]);
                    return false;
                }

            }
            if (validateCode == 0 || validateCode == 17) {
                if ($scope.quotationDetail.deliveryAddress != undefined && $scope.quotationDetail.deliveryAddress != null && $scope.quotationDetail.deliveryAddress.country != null && $scope.quotationDetail.deliveryAddress.country.status == 'Block') {
                    $scope.errorMap.put("deliveryCountry", $rootScope.nls["ERR8847"]);
                    return false;
                } else if ($scope.quotationDetail.deliveryAddress != undefined && $scope.quotationDetail.deliveryAddress != null && $scope.quotationDetail.deliveryAddress.country != null && $scope.quotationDetail.deliveryAddress.country.status == 'Hide') {
                    $scope.errorMap.put("deliveryCountry", $rootScope.nls["ERR8850"]);
                    return false;
                }

            }

            /*if (validateCode == 0 || validateCode == 10) {
                    if ($scope.quotationDetail.chargebleWeight == undefined
                            || $scope.quotationDetail.chargebleWeight == null
                            || $scope.quotationDetail.chargebleWeight == "") {
                        $scope.errorMap.put("chargebleWeight",$rootScope.nls["ERR22111"]);
                        return false;
                    } 
                    else
                        {
                        var regexp = new RegExp("^[0-9]{0,10}(.[0-9]{0,3})$");
                        if (!regexp
                                .test($scope.quotationDetail.chargebleWeight)) {
                            $scope.errorMap.put("chargebleweight",$rootScope.nls["ERR22111"]);
                            return false;
                        }
                }
                    console.log("Chargeble Weight validation done...");
                }*/

            if (validateCode == 0) {
                for (var i = 0; i < $scope.quotationDetail.quotationCarrierList.length; i++) {
                    $scope.quotationRateCalculation($scope.quotationDetail.quotationCarrierList[i]);
                }
                if ($scope.isPickUpDelFound($scope.quotationDetail)) {


                } else {
                    $scope.quotationDetail.pickupAddress = null;
                    $scope.quotationDetail.deliveryAddress = null;
                }
            }


            return true;
        }



        $scope.validateAirNotesTab = function(validateCode) {
            if (validateCode == 0 || validateCode == 8) {
                if ($scope.quotationDetail.serviceNoteList != null && $scope.quotationDetail.serviceNoteList.length != 0) {

                    for (var i = 0; i < $scope.quotationDetail.serviceNoteList.length; i++) {
                        if ($scope.quotationDetail.serviceNoteList[i].note == undefined ||
                            $scope.quotationDetail.serviceNoteList[i].note == null ||
                            $scope.quotationDetail.serviceNoteList[i].note == "" ||
                            $scope.quotationDetail.serviceNoteList[i].note.trim == "") {
                            $scope.errorMap.put("sNote" + i, $rootScope.nls["ERR22104"]);
                            return false;
                        }

                    }

                }
                console.log("Service Air Note validation done...");
            }
        }

        $scope.charge = {};
        $scope.goToAddCharge = function() {
            console.log("Add Charge Button is pressed......");
            $scope.charge = {};
            $scope.charge.chargeMaster = {};
            $scope.charge.currencyMaster = {};
            $scope.charge.unitMaster = {};
            $scope.charge.actualchargeable = false;
            $scope.charge.isNote = false;
            $scope.errorMap = new Map();
            $scope.showEditCharge = true;
            $scope.editIndex = null;
            $scope.firstFocusInCharge = true;
            discardService.set($scope.charge);
        }

        $scope.chargeEditShow = function(index) {
            console.log("Edit Charge Button is pressed......");
            $scope.editIndex = index;
            $scope.errorMap = new Map();
            $scope.showEditCharge = true;
            $scope.charge = cloneService.clone($scope.quotationDetail.quotationChargeList[index]);
            $scope.firstFocusInCharge = true;
            $scope.charge.actualchargeable = $scope.charge.actualchargeable == 'ACTUAL' ? true : false;
            $scope.charge.isNote = $scope.charge.note != null ? true : false;
            discardService.set($scope.charge);
        }

        $scope.discardCharge = function() {
            if (!discardService.compare($scope.charge)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR201"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.saveCharge();
                    } else {
                        $scope.closeCharge();
                    }
                });

            } else {
                $scope.closeCharge();
            }
        }

        $scope.closeCharge = function() {
            $scope.showEditCharge = false;
            $scope.editIndex = null;

        }

        $scope.saveCharge = function() {
            console.log("Save Charge is Called.....");

            if ($scope.validateQuotationCharge(0)) {

                $scope.charge.actualchargeable = $scope.charge.actualchargeable ? 'ACTUAL' : 'CHARGEABLE';

                if ($scope.editIndex == null) {
                    if ($scope.quotationDetail.quotationChargeList == null)
                        $scope.quotationDetail.quotationChargeList = [];
                    $scope.quotationDetail.quotationChargeList.push($scope.charge);
                } else {
                    $scope.quotationDetail.quotationChargeList.splice($scope.editIndex, 1, $scope.charge);
                }
                $scope.showEditCharge = false;
                $scope.editIndex = null;
            } else {
                console.log("Charge Validation Failed");
            }
        }


        $scope.chargeRemoveShow = function(index) {
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected Charge  ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                $scope.quotationDetail.quotationChargeList.splice(index, 1);
                $scope.showEditCharge = false;
                $scope.editIndex = null;
            }, function(value) {
                console.log("Charge Deletion cancelled");
            });
        }


        /* Showing Charge Lov */

        $scope.showChargeMasterList = false;

        $scope.chargeMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "chargeName",
                    seperator: false
                },
                {
                    "title": "chargeCode",
                    seperator: true
                }
            ]
        }


        $scope.showChargeMaster = function(partyName) {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Charge";

        };


        $scope.ajaxChargeEvent = function(object) {

            console.log("ajaxChargeEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.chargeMasterList = data.responseObject.searchResult;
                        $scope.showChargeMasterList = true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Charge');
                }
            );
        }


        $scope.selectedChargeMaster = function(obj) {
            $scope.charge.chargeName = "";
            console.log("Selected Charge ", obj);
            $scope.charge.chargeMaster = obj;
            if ($scope.validateQuotationCharge(1))
                $scope.charge.chargeName = obj.chargeName;
            $scope.cancelChargeMaster();
        };

        $scope.cancelChargeMaster = function() {
            $scope.showChargeMasterList = false;
        }




        $scope.showCurrencyMasterList = false;
        $scope.currencyMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "currencyName",
                    seperator: false
                },
                {
                    "title": "currencyCode",
                    seperator: true
                }
            ]
        }

        $scope.showCurrencyMaster = function(currencyName) {
            console.log("Showing List of Currency....");
            $scope.panelTitle = "Currency";
            $scope.selectedItem = -1;

        };

        $scope.ajaxCurrencyMasterEvent = function(object) {

            console.log("ajaxCurrencyMasterEvent is called ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


            CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.currencyMasterList = data.responseObject.searchResult;
                        $scope.showCurrencyMasterList = true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Currency');
                }
            );
        }

        $scope.selectedCurrencyMaster = function(obj) {
            console.log("Selected Currency ", obj);
            $scope.charge.currencyMaster = obj;
            $scope.validateQuotationCharge(2);
            $scope.cancelCurrencyMaster();
        };

        $scope.cancelCurrencyMaster = function() {
            console.log("Currency List Cancelled.....");
            $scope.showCurrencyMasterList = false;
        }


        $scope.showUnitMasterList = false;
        $scope.unitMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "unitName",
                    seperator: false
                },
                {
                    "title": "unitCode",
                    seperator: true
                }
            ]
        }

        $scope.showUnitMaster = function(partyName) {
            console.log("Showing List of Units.....");
            $scope.panelTitle = "Unit";
            $scope.selectedItem = -1;

        };


        $scope.ajaxUnitMasterEvent = function(object) {
            console.log("ajaxUnitMasterEvent is called ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.unitMasterList = data.responseObject.searchResult;
                        $scope.showUnitMasterList = true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Charge');
                }
            );
        }

        $scope.selectedUnitMaster = function(obj) {
            console.log("Selected Unit ", obj);
            $scope.charge.unitMaster = obj;
            $scope.validateQuotationCharge(3);
            $scope.cancelUnitMaster();
        };

        $scope.cancelUnitMaster = function() {
            console.log("Canceling Unit Master List....");
            $scope.showUnitMasterList = false;
        }

        $scope.validateQuotationCharge = function(validateCode) {
            $scope.errorMap = new Map();
            $scope.firstFocusInCharge = false;
            if (validateCode == 0 || validateCode == 1) {
                if ($scope.charge.chargeMaster == null ||
                    $scope.charge.chargeMaster.id == undefined ||
                    $scope.charge.chargeMaster.id == null ||
                    $scope.charge.chargeMaster.id == "") {

                    $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22072"]);

                    return false;
                } else if ($scope.charge.chargeMaster.status != null) {

                    if (ValidateUtil.isStatusBlocked($scope.charge.chargeMaster.status)) {

                        $scope.charge.chargeMaster = null;

                        console.log("Selected Charge is Blocked");

                        $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22073"]);

                        return false
                    }

                    if (ValidateUtil.isStatusHidden($scope.charge.chargeMaster.status)) {

                        $scope.charge.chargeMaster = null;

                        console.log("Selected Charge is Hidden");

                        $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22074"]);

                        return false
                    }

                }


            }



            if (validateCode == 0 || validateCode == 2) {
                if ($scope.charge.currencyMaster == null || $scope.charge.currencyMaster.id == undefined ||
                    $scope.charge.currencyMaster.id == null ||
                    $scope.charge.currencyMaster.id == "") {

                    $scope.errorMap.put("currencyMaster", $rootScope.nls["ERR22077"]);

                    return false;

                } else if ($scope.charge.currencyMaster.status != null) {

                    if (ValidateUtil.isStatusBlocked($scope.charge.currencyMaster.status)) {

                        $scope.charge.currencyMaster = null;

                        console.log("Selected currencyMaster is Blocked");

                        $scope.errorMap.put("currencyMaster", $rootScope.nls["ERR22078"]);

                        return false
                    }

                    if (ValidateUtil.isStatusHidden($scope.charge.currencyMaster.status)) {

                        $scope.charge.currencyMaster = null;

                        console.log("Selected currencyMaster is Hidden");

                        $scope.errorMap.put("currencyMaster", $rootScope.nls["ERR22078"]);

                        return false
                    }

                }
            }


            if (validateCode == 0 || validateCode == 3) {
                if ($scope.charge.unitMaster == null || $scope.charge.unitMaster.id == undefined ||
                    $scope.charge.unitMaster.id == null ||
                    $scope.charge.unitMaster.id == "") {

                    $scope.errorMap.put("unitMaster", $rootScope.nls["ERR22080"]);

                    return false;
                } else if ($scope.charge.unitMaster.status != null) {

                    if (ValidateUtil.isStatusBlocked($scope.charge.unitMaster.status)) {

                        $scope.charge.unitMaster = null;

                        console.log("Selected unitMaster is Blocked");

                        $scope.errorMap.put("unitMaster", $rootScope.nls["ERR22081"]);

                        return false
                    }

                    if (ValidateUtil.isStatusHidden($scope.charge.unitMaster.status)) {

                        $scope.charge.unitMaster = null;

                        console.log("Selected unitMaster is Hidden");

                        $scope.errorMap.put("unitMaster", $rootScope.nls["ERR22082"]);

                        return false
                    }

                }


            }



            if (validateCode == 0 || validateCode == 4) {
                if ($scope.charge.noOfMinUnit != undefined && $scope.charge.noOfMinUnit != null && $scope.charge.noOfMinUnit != "") {
                    if ($scope.charge.noOfMinUnit == 0) {
                        $scope.errorMap.put("noOfMinUnit", $rootScope.nls["ERR22113"]);
                        return false;
                    } else {
                        try {
                            if (isNaN(parseFloat($scope.charge.noOfMinUnit))) {
                                $scope.errorMap.put("noOfMinUnit", $rootScope.nls["ERR22114"]);
                                return false;
                            }
                        } catch (err) {
                            $scope.errorMap.put("noOfMinUnit", $rootScope.nls["ERR22114"]);
                            return false;
                        }

                        var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                        if (!regexp
                            .test($scope.charge.noOfMinUnit)) {
                            $scope.errorMap.put("noOfMinUnit", $rootScope.nls["ERR22114"]);
                            return false;
                        }


                        if ($scope.charge.noOfMinUnit != undefined && $scope.charge.noOfMinUnit != null && $scope.charge.noOfMinUnit != "")
                            if ($scope.charge.noOfMinUnit < 0 || $scope.charge.noOfMinUnit >= 99999999999.99) {
                                $scope.errorMap.put("noOfMinUnit", $rootScope.nls["ERR22114"]);
                                return false;
                            }


                    }


                } else {
                    $scope.charge.noOfMinUnit = null;
                }
            }


            if (validateCode == 0 || validateCode == 5) {
                if ($scope.charge.noOfMaxUnit != undefined && $scope.charge.noOfMaxUnit != null && $scope.charge.noOfMaxUnit != "") {
                    if ($scope.charge.noOfMaxUnit == 0) {
                        $scope.errorMap.put("noOfMaxUnit", $rootScope.nls["ERR22115"]);
                        return false;
                    } else {

                        try {
                            if (isNaN(parseFloat($scope.charge.noOfMaxUnit))) {
                                $scope.errorMap.put("noOfMaxUnit", $rootScope.nls["ERR22116"]);
                                return false;
                            }
                        } catch (err) {
                            $scope.errorMap.put("noOfMaxUnit", $rootScope.nls["ERR22116"]);
                            return false;
                        }

                        var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");
                        if (!regexp
                            .test($scope.charge.noOfMaxUnit)) {
                            $scope.errorMap.put("noOfMaxUnit", $rootScope.nls["ERR22116"]);
                            return false;
                        }
                        if ($scope.charge.noOfMaxUnit != undefined && $scope.charge.noOfMaxUnit != null && $scope.charge.noOfMaxUnit != "")
                            if ($scope.charge.noOfMaxUnit < 0 || $scope.charge.noOfMaxUnit >= 99999999999.999) {
                                $scope.errorMap.put("noOfMaxUnit", $rootScope.nls["ERR22116"]);
                                return false;
                            }


                    }
                } else {
                    $scope.charge.noOfMaxUnit = null;
                }

            }


            if (validateCode == 0 || validateCode == 6) {



                if ($scope.charge.sellRateAmountPerUnit == undefined || $scope.charge.sellRateAmountPerUnit == null ||
                    $scope.charge.sellRateAmountPerUnit == "") {

                    $scope.errorMap.put("sellRateAmountPerUnit", $rootScope.nls["ERR22132"]);
                    return false;

                }




                if ($scope.charge.sellRateAmountPerUnit != null) {
                    if ($scope.charge.sellRateAmountPerUnit <= 0) {
                        $scope.errorMap.put("sellRateAmountPerUnit", $rootScope.nls["ERR22117"]);
                        return false;
                    } else {
                        try {
                            if (isNaN(parseFloat($scope.charge.sellRateAmountPerUnit))) {
                                $scope.errorMap.put("sellRateAmountPerUnit", $rootScope.nls["ERR22118"]);
                                return false;
                            }
                        } catch (err) {
                            $scope.errorMap.put("sellRateAmountPerUnit", $rootScope.nls["ERR22118"]);
                            return false;
                        }

                        var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");
                        if (!regexp
                            .test($scope.charge.sellRateAmountPerUnit)) {
                            $scope.errorMap.put("sellRateAmountPerUnit", $rootScope.nls["ERR22118"]);
                            return false;
                        }

                        if ($scope.charge.sellRateAmountPerUnit != null && $scope.charge.sellRateAmountPerUnit != undefined && $scope.charge.sellRateAmountPerUnit != "")
                            if ($scope.charge.sellRateAmountPerUnit < 0 || $scope.charge.sellRateAmountPerUnit > 99999999999.999) {
                                $scope.errorMap.put("sellRateAmountPerUnit", $rootScope.nls["ERR22118"]);
                                return false;
                            }


                    }
                }

            }


            if (validateCode == 0 || validateCode == 7) {

                /* 
                 if ($scope.charge.sellRateMinAmount ==undefined || $scope.charge.sellRateMinAmount ==null || $scope.charge.sellRateMinAmount =="") {
                     
                     $scope.errorMap.put("sellRateMinAmount",$rootScope.nls["ERR22133"]);
                        return false;
                 
                 }*/

                if ($scope.charge.sellRateMinAmount != null && $scope.charge.sellRateMinAmount != undefined && $scope.charge.sellRateMinAmount != "") {
                    if ($scope.charge.sellRateMinAmount != null) {
                        if ($scope.charge.sellRateMinAmount <= 0) {
                            $scope.errorMap.put("sellRateMinAmount", $rootScope.nls["ERR22119"]);
                            return false;
                        } else {

                            try {
                                if (isNaN(parseFloat($scope.charge.sellRateMinAmount))) {
                                    $scope.errorMap.put("sellRateMinAmount", $rootScope.nls["ERR22120"]);
                                    return false;
                                }
                            } catch (err) {
                                $scope.errorMap.put("sellRateMinAmount", $rootScope.nls["ERR22120"]);
                                return false;
                            }

                            var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");
                            if (!regexp
                                .test($scope.charge.sellRateMinAmount)) {
                                $scope.errorMap.put("sellRateMinAmount", $rootScope.nls["ERR22120"]);
                                return false;
                            }

                            if ($scope.charge.sellRateMinAmount != null && $scope.charge.sellRateMinAmount != undefined && $scope.charge.sellRateMinAmount != "")
                                if ($scope.charge.sellRateMinAmount < 0 || $scope.charge.sellRateMinAmount > 99999999999.999) {
                                    $scope.errorMap.put("sellRateMinAmount", $rootScope.nls["ERR22120"]);
                                    return false;
                                }



                        }
                    }

                }




            }


            if (validateCode == 0 || validateCode == 8) {



                /*if ($scope.charge.buyRateCostPerUnit ==undefined || $scope.charge.buyRateCostPerUnit ==null || $scope.charge.buyRateCostPerUnit =="") {
                                 
                                 $scope.errorMap.put("buyRateCostPerUnit",$rootScope.nls["ERR22134"]);
                                    return false;
                             
                             }*/

                if ($scope.charge.buyRateCostPerUnit != null && $scope.charge.buyRateCostPerUnit != undefined && $scope.charge.buyRateCostPerUnit != "") {
                    if ($scope.charge.buyRateCostPerUnit != undefined && $scope.charge.buyRateCostPerUnit != null && $scope.charge.buyRateCostPerUnit != "") {
                        if ($scope.charge.buyRateCostPerUnit <= 0) {
                            $scope.errorMap.put("buyRateCostPerUnit", $rootScope.nls["ERR22121"]);
                            return false;
                        } else {

                            try {
                                if (isNaN(parseFloat($scope.charge.buyRateCostPerUnit))) {
                                    $scope.errorMap.put("buyRateCostPerUnit", $rootScope.nls["ERR22122"]);
                                    return false;
                                }
                            } catch (err) {
                                $scope.errorMap.put("buyRateCostPerUnit", $rootScope.nls["ERR22122"]);
                                return false;
                            }

                            var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");
                            if (!regexp
                                .test($scope.charge.buyRateCostPerUnit)) {
                                $scope.errorMap.put("buyRateCostPerUnit", $rootScope.nls["ERR22122"]);
                                return false;
                            }

                            if ($scope.charge.buyRateCostPerUnit != null && $scope.charge.buyRateCostPerUnit != undefined && $scope.charge.buyRateCostPerUnit != "")
                                if ($scope.charge.buyRateCostPerUnit < 0 || $scope.charge.buyRateCostPerUnit > 99999999999.999) {
                                    $scope.errorMap.put("buyRateCostPerUnit", $rootScope.nls["ERR22122"]);
                                    return false;
                                }

                        }
                    } else {
                        $scope.charge.buyRateCostPerUnit = null;
                    }
                }


            }


            if (validateCode == 0 || validateCode == 9) {



                /*if ($scope.charge.buyRateMinCost ==undefined || $scope.charge.buyRateMinCost ==null || $scope.charge.buyRateMinCost =="") {
                                 
                                 $scope.errorMap.put("buyRateMinCost",$rootScope.nls["ERR22135"]);
                                    return false;
                             
                             }*/



                if ($scope.charge.buyRateMinCost != undefined && $scope.charge.buyRateMinCost != null && $scope.charge.buyRateMinCost != "") {
                    if ($scope.charge.buyRateMinCost <= 0) {
                        $scope.errorMap.put("buyRateMinCost", $rootScope.nls["ERR22123"]);
                        return false;
                    } else {

                        try {
                            if (isNaN(parseFloat($scope.charge.buyRateMinCost))) {
                                $scope.errorMap.put("buyRateMinCost", $rootScope.nls["ERR22124"]);
                                return false;
                            }
                        } catch (err) {
                            $scope.errorMap.put("buyRateMinCost", $rootScope.nls["ERR22124"]);
                            return false;
                        }

                        var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");
                        if (!regexp
                            .test($scope.charge.buyRateMinCost)) {
                            $scope.errorMap.put("buyRateMinCost", $rootScope.nls["ERR22124"]);
                            return false;
                        }

                        if ($scope.charge.buyRateMinCost != null && $scope.charge.buyRateMinCost != undefined && $scope.charge.buyRateMinCost != "")
                            if ($scope.charge.buyRateMinCost < 0 || $scope.charge.buyRateMinCost > 99999999999.999) {
                                $scope.errorMap.put("buyRateMinCost", $rootScope.nls["ERR22124"]);
                                return false;
                            }

                    }
                } else {
                    $scope.charge.buyRateMinCost = null;
                }

            }


            if (validateCode == 0 || validateCode == 10) {
                if ($scope.charge.note != undefined ||
                    $scope.charge.note != null ||
                    $scope.charge.note != "") {

                    var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,4000}");
                    if (!regexp
                        .test($scope.charge.note)) {
                        $scope.errorMap.put("note", $rootScope.nls["ERR22125"]);
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 11) {


                if ($scope.charge.chargeName != undefined &&
                    $scope.charge.chargeName != null &&
                    $scope.charge.chargeName != "") {
                    var regexp = new RegExp("^[ 0-9a-zA-Z-'.,&@:;?!()$#*_]{0,100}$");
                    if (!regexp
                        .test($scope.charge.chargeName)) {
                        $scope.errorMap.put("chargeName", $rootScope.nls["ERR22076"]);
                        return false;
                    }
                } else {
                    $scope.errorMap.put("chargeName", $rootScope.nls["ERR22075"]);
                    return false;
                }

            }
            return true;

        }




        $scope.validateQuotationDetails = function(validateCode) {



            // Carrier is optional
            // Frequency is optional


            if (validateCode == 0 || validateCode == 8) {
                if ($scope.quotationDetail.headerNote != undefined ||
                    $scope.quotationDetail.headerNote != null ||
                    $scope.quotationDetail.headerNote != "") {

                    var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,4000}");
                    if (!regexp
                        .test($scope.quotationDetail.headerNote)) {
                        $scope.errorMap.put("headerNote", $rootScope.nls["ERR22126"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 9) {
                if ($scope.quotationDetail.footerNote != undefined ||
                    $scope.quotationDetail.footerNote != null ||
                    $scope.quotationDetail.footerNote != "") {

                    var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,4000}");
                    if (!regexp
                        .test($scope.quotationDetail.footerNote)) {
                        $scope.errorMap.put("footerNote", $rootScope.nls["ERR22127"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 10) {
                if ($scope.quotationDetail.generalNotes != undefined ||
                    $scope.quotationDetail.generalNotes != null ||
                    $scope.quotationDetail.generalNotes != "") {

                    var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,4000}");
                    if (!regexp
                        .test($scope.quotationDetail.generalNotes)) {
                        $scope.errorMap.put("generalNotes", $rootScope.nls["ERR22128"]);
                        return false;
                    }

                }

            }
            if (validateCode == 0 || validateCode == 11) {
                if ($scope.quotationDetail.airNotes != undefined ||
                    $scope.quotationDetail.airNotes != null ||
                    $scope.quotationDetail.airNotes != "") {

                    var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,4000}");
                    if (!regexp
                        .test($scope.quotationDetail.airNotes)) {
                        $scope.errorMap.put("airNotes", $rootScope.nls["ERR22129"]);
                        return false;
                    }

                }

            }

            return true;
        }

        $scope.validateQuotationDimension = function(validateCode) {
            $scope.errorMap = new Map();
            $scope.firstFocusInDimension = false;
            if (validate == 0 || validate == 1) {
                if ($scope.quotationDimension.noOfPiece == undefined || $scope.quotationDimension.noOfPiece == "" || $scope.quotationDimension.noOfPiece == null) {
                    $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR22062"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp.test($scope.quotationDimension.noOfPiece)) {
                        $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR22105"]);
                        return false;

                    }
                }


            }
            if (validate == 0 || validate == 2) {
                if ($scope.quotationDimension.length == undefined || $scope.quotationDimension.length == "" || $scope.quotationDimension.length == null) {
                    $scope.errorMap.put("length", $rootScope.nls["ERR22064"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.quotationDimension.length)) {
                        $scope.errorMap.put("length", $rootScope.nls["ERR22106"]);
                        return false;

                    }
                }
            }

            if (validate == 0 || validate == 3) {
                if ($scope.quotationDimension.width == undefined || $scope.quotationDimension.width == "" || $scope.quotationDimension.width == null) {
                    $scope.errorMap.put("width", $rootScope.nls["ERR22066"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.quotationDimension.width)) {

                        $scope.errorMap.put("width", $rootScope.nls["ERR22107"]);
                        return false;

                    }
                }
            }
            if (validate == 0 || validate == 4) {
                if ($scope.quotationDimension.height == undefined || $scope.quotationDimension.height == "" || $scope.quotationDimension.height == null) {
                    $scope.errorMap.put("height", $rootScope.nls["ERR22068"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,10}$");
                    if (!regexp
                        .test($scope.quotationDimension.height)) {

                        $scope.errorMap.put("height", $rootScope.nls["ERR22108"]);
                        return false;

                    }
                }
            }

            if (validate == 0 || validate == 5) {
                if ($scope.quotationDimension.grossWeight == undefined || $scope.quotationDimension.grossWeight == "" || $scope.quotationDimension.grossWeight == null) {
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22070"]);
                    return false;
                } else {
                    if ($scope.quotationDimension.grossWeight < 0 || $scope.quotationDimension.grossWeight > 9999999999.999) {
                        $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                        return false;
                    }
                    var regexp = new RegExp("^[0-9]{0,10}(.[0-9]{0,3})$");
                    if (!regexp
                        .test($scope.quotationDimension.grossWeight)) {
                        $scope.errorMap.put("grossWeight", $rootScope.nls["ERR22109"]);
                        return false;

                    }
                }
            }

            return true;

        }

        $scope.downloadAttach = function(param) {
            if (param.data.id != null && param.data.file == null) {
                console.log("API CALL")
                downloadFactory.downloadAttachment('/api/v1/quotation/files/', param.data.id, param.data.fileName);
            }
            /*else if(param.data.id==null && param.data.file!=null){
                    console.log("NO API CALL BUT Byte array found")	
                     var blob = new Blob([param.data.file], {type: param.data.fileContentType+";charset=utf-8"});
                     saveAs(blob, param.data.fileName);
                }*/
            else {
                console.log("NO API CALL")
                saveAs(param.data.tmpFile, param.data.fileName);
            }
        }


        $scope.attachConfig = {
            "isEdit": true,
            "isDelete": true,
            "tableSize": "col-xs-8",
            "columnDefs": [

                    {
                        "name": "Reference No",
                        "model": "refNo",
                        "type": "text",
                        "width": "inl_lp"
                    },
                    {
                        "name": "File Name",
                        "model": "fileName",
                        "type": "file"
                    },
                    {
                        "name": "Actions",
                        "model": "action",
                        "type": "action",
                        "width": "inl_l"
                    }
                ]
                //,
                //"fieldData":[
                //	{"label":"Reference No","elemData":{"type":"text"},"scope":"attachMentData.refNo","validation":["required","number"]},
                //	{"label":"File Name","elemData":{"type":"text","disabled":true},"scope":"attachMentData.file.name"},
                //	{"label":"File","elemData":{"type":"file"},"scope":"attachMentData.file","validation":["required"]},
                //]
        }


        /*$scope.manipulateAttach = function(param){
    
            console.log("manipulateAttach param", param);
        	
            var displayObject = new Object();
        	
            displayObject.id =  param.data.id;
            displayObject.refNo =  param.data.refNo;
            displayObject.fileName =  param.data.fileName;
            displayObject.file =  param.data.file;
            displayObject.fileContentType =  param.data.fileContentType;
            displayObject.tmpFile =  param.data.tmpFile;
            displayObject.systemTrack = param.data.systemTrack; 
        	
            if(param.index == null) {
                $scope.attachConfig.data.push(param.data);
            } else {
                $scope.attachConfig.data.splice(param.index, 1, param.data);
            }
        	
        }*/

        /*$scope.removeAttach = function(param){
            $scope.attachConfig.data.splice(param.index,1);
        }*/

        /*$scope.downloadAttach = function(param){
            if(param.data.id!=null && param.data.file==null){
                console.log("API CALL")	
                $http({
                    url: $rootScope.baseURL+'/api/v1/quotation/files/'+param.data.id,
                    method: "GET",
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    console.log("hiddenElement ",data)
                    var blob = new Blob([data], {});
                    saveAs(blob, param.data.fileName);
    
                }).error(function (data, status, headers, config) {
                    //upload failed
                });
            	
    
            }else if(param.data.id==null && param.data.file!=null){
                console.log("NO API CALL BUT Byte array found")	
                 var blob = new Blob([param.data.file], {});
            	
                var dt = new Date();
            	
                var tmp = {};
                tmp.lastModified = dt.getTime();
                tmp.lastModifiedDate = dt;
                tmp.name = param.data.fileName;
                tmp.size = param.data.fileSize;
                tmp.type = param.data.fileContentType;
                tmp.webkitRelativePath = "";
            	
                 saveAs(tmp, param.data.fileName);
            } else{
            	
                console.log("NO API CALL", param);	
            	
                saveAs(param.data.tmpFile, param.data.fileName);
            	
            }
        	
    	
        }*/

        /*$scope.populateAttachment = function() {
            console.log("Populating Attachment....");
            $scope.attachConfig.data = [];
            if($scope.quotation.quotationAttachementList != null && $scope.quotation.quotationAttachementList.length != 0){
            	
                for(var i = 0; i < $scope.quotation.quotationAttachementList.length; i++) {
                    	
                    var displayObject = new Object();
                    displayObject.id =  $scope.quotation.quotationAttachementList[i].id;
                    displayObject.refNo =  $scope.quotation.quotationAttachementList[i].refNo;
                    displayObject.fileName =  $scope.quotation.quotationAttachementList[i].fileName;
                    displayObject.file =  $scope.quotation.quotationAttachementList[i].file;
                    displayObject.fileContentType =  $scope.quotation.quotationAttachementList[i].fileContentType;
                    displayObject.systemTrack = $scope.quotation.quotationAttachementList[i].systemTrack;
                    displayObject.versionLock = $scope.quotation.quotationAttachementList[i].versionLock;
                	
                    $scope.attachConfig.data.push(displayObject);
                }
            }		
        }
    	
        $scope.associateQuotationAttach = function() {
            console.log("associateQuotationAttach....");
        	
            $scope.quotation.quotationAttachementList = [];
        	
    
            if($scope.attachConfig.data!= null && $scope.attachConfig.data.length != 0){
            	
                for(var i = 0; i < $scope.attachConfig.data.length; i++) {
                	
                    var displayObject = new Object();
                    displayObject.id =  $scope.attachConfig.data[i].id;
                    displayObject.refNo =  $scope.attachConfig.data[i].refNo;
                    displayObject.fileName =  $scope.attachConfig.data[i].fileName;
                    displayObject.file =  $scope.attachConfig.data[i].file;
                    displayObject.fileContentType =  $scope.attachConfig.data[i].fileContentType;
                    displayObject.systemTrack = $scope.attachConfig.data[i].systemTrack;
                    displayObject.versionLock = $scope.attachConfig.data[i].versionLock;
                	
                    $scope.quotation.quotationAttachementList.push(displayObject);
                }
            }		
        	
        	
        	
        	
        }*/
        /*$scope.tableDimensionStatee = function(param){
        	
            $scope.errorMap = new Map();
            console.log("data",param);
            $scope.tableDimensionState=param;
        }*/




        $scope.clearShipperAddress = function() {
            $scope.quotation.shipper = {};
            $scope.quotation.shipperAddress.addressLine1 = "";
            $scope.quotation.shipperAddress.addressLine2 = "";
            $scope.quotation.shipperAddress.addressLine3 = "";
            $scope.quotation.shipperAddress.addressLine4 = "";
            $scope.quotation.shipperAddress.phone = "";
            $scope.quotation.shipperAddress.mobile = "";
            $scope.quotation.shipperAddress.email = "";
        }

        $scope.clearCustomerAddress = function() {
            $scope.quotation.customer = {};
            $scope.quotation.customerAddress.addressLine1 = "";
            $scope.quotation.customerAddress.addressLine2 = "";
            $scope.quotation.customerAddress.addressLine3 = "";
            $scope.quotation.customerAddress.addressLine4 = "";
            $scope.quotation.customerAddress.phone = "";
            $scope.quotation.customerAddress.mobile = "";
            $scope.quotation.customerAddress.email = "";
        }


        $scope.loggedByRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }

        }
        $scope.salesCoordinatorRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }

        }
        $scope.salesManRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }

        }
        $scope.serviceNameRender = function(item) {
            return {
                label: item.serviceName,
                item: item
            }

        }
        $scope.orginRender = function(item) {
            return {
                label: item.portName,
                item: item
            }

        }
        $scope.destinationRender = function(item) {
            return {
                label: item.portName,
                item: item
            }

        }
        $scope.carrierRender = function(item) {
            return {
                label: item.carrierName,
                item: item
            }

        }
        $scope.frequencyRender = function(item) {
            return {
                label: item.frequencyName,
                item: item
            }

        }
        $scope.tosRender = function(item) {
            return {
                label: item.tosName,
                item: item
            }

        }
        $scope.shipperRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }
        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }

        $scope.isObjFound = function(obj) {

            if (obj == undefined || obj == null || obj.length == undefined || EmptyRowChecker.isEmptyRow(obj[0])) {
                return false;
            } else {
                return true;
            }
        }

        $scope.isValueAddedFound = function(obj) {

            if (obj == undefined || obj == null || obj.length == undefined) {
                return false;
            } else {
                var isFound = false;
                for (var i = 0; i < obj.length; i++) {
                    if (obj[i].selected != undefined && obj[i].selected) {
                        isFound = true
                        break
                    } else {

                    }
                }
                return isFound;
            }
        }

        $scope.isPickUpDelFound = function(obj) {

            if (obj == undefined || obj == null || obj.pickupAddress == undefined || obj.pickupAddress == null ||
                obj.deliveryAddress == undefined || obj.deliveryAddress == null || EmptyRowChecker.isEmptyRow(obj.pickupAddress) || EmptyRowChecker.isEmptyRow(obj.deliveryAddress)) {
                return false;
            } else {
                return true;
            }
        }




        $scope.setSalesman = function() {

            var flag = false;

            if ($scope.quotation.customer.partyServiceList != null && $scope.quotation.customer.partyServiceList.length != undefined && $scope.quotation.customer.partyServiceList.length > 0) {
                for (var i = 0; i < $scope.quotation.customer.partyServiceList.length; i++) {
                    if ($scope.quotation.locationMaster.id == $scope.quotation.customer.partyServiceList[i].locationMaster.id) {
                        if ($scope.quotation.customer.partyServiceList[i].serviceMaster.serviceCode == $rootScope.appMasterData['default.party.salesman.service.code']) {
                            $scope.quotation.salesman = $scope.quotation.customer.partyServiceList[i].salesman;
                            flag = true;
                            break;
                        }
                    }
                }
            }

            if (flag == false) {
                $scope.quotation.salesman = null;
            }




        }

        $scope.openDefaultTabs = function() {
            console.log("openDefaultTabs is called.....");

            if ($scope.quotation.id == undefined && $scope.quotation.id == null) {
                for (var i = 0; i <= $scope.quotation.quotationDetailList.length; i++) {
                    if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CREATE)) {
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                    }

                }
            } else {
                for (var i = 0; i <= $scope.quotation.quotationDetailList.length; i++) {
                    if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_MODIFY)) {
                        $scope.detailTab = 'Services';
                        $scope.moreAddressTab = 'chargeBtn';
                    }

                }
            }


        }




        $scope.navigateToNextField = function(id) {
            console.log("navigateToNextField is called ", id);
            var focElement = document.getElementById(id);
            if (focElement != null) {
                focElement.focus();
            }
        }

        $scope.setDetailTab = function(tabName, focusId) {
            if ($scope.quotation.id == undefined && $scope.quotation.id == null) {
                for (var i = 0; i <= $scope.quotation.quotationDetailList.length; i++) {

                    if (tabName == 'Services' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CREATE)) {
                        $scope.detailTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                        $scope.moreAddressTab = 'chargeBtn';
                    }
                    if (tabName == 'chargeBtn') {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'dimensionBtn') {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'pickupBtn') {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'airNote' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_CREATE)) {
                        $scope.moreAddressTab = 'notesBtn';
                        $rootScope.navigateToNextField(focusId);
                    }
                    if (tabName == 'valueAdded' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VALUE_ADDED_SERVICE_LIST)) {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'notes') {
                        $scope.detailTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }
                    if (tabName == 'attachMent') {
                        $scope.detailTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }
                }



            } else {

                for (var i = 0; i <= $scope.quotation.quotationDetailList.length; i++) {

                    if (tabName == 'Services' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_MODIFY)) {
                        $scope.detailTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }
                    if (tabName == 'chargeBtn') {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'dimensionBtn') {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'pickupBtn' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_PICKUP_DELIVERY_MODIFY)) {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                    if (tabName == 'airNote' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_MODIFY)) {
                        $scope.moreAddressTab = 'notesBtn';
                        $rootScope.navigateToNextField(focusId);
                    }
                    if (tabName == 'valueAdded' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VALUE_ADDED_SERVICE_MODIFY)) {
                        $scope.moreAddressTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }
                    if (tabName == 'notes' && $rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_MODIFY)) {
                        $scope.detailTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }
                    if (tabName == 'attachMent') {
                        $scope.detailTab = tabName;
                        $rootScope.navigateToNextField(focusId);
                    }

                }




            }



        }

        $scope.nextToFocus = function(id) {
            console.log("called." + id);
            $rootScope.navigateToNextField(id);
        }




        $scope.accessCheck = function(value, nextIdFocus) {

            if (value == 'pickupBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_PICKUP_DELIVERY_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.pickupBtn = false;
                }

            }
            if (value == 'chargeBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CHARGE_VIEW)) {
                    $scope.selectedCarrierIndex = 0;
                    $rootScope.navigateToNextField('0mccarrier');
                    return true;
                } else {
                    $scope.chargeBtn = false;
                }

            }
            if (value == 'dimensionBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_DIMENSION_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.dimensionBtn = false;
                }

            }

            if (value == 'notesBtn') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_AIR_NOTE_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.notesBtn = false;
                }

            }

            if (value == 'valueAdded') {
                if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_VALUE_ADDED_SERVICE_VIEW)) {
                    $rootScope.navigateToNextField(nextIdFocus);
                    return true;
                } else {
                    $scope.valueAdded = false;
                }

            }
        }



        /* Showing Top Records of a Quotation */

        $scope.topQuotationsConfig = {
            ajax: true,
            showTopQuotationsFlag: false
        }


        $scope.showTopRecords = function() {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_RECENT_QUOTATION_LIST)) {
                console.log("showTopRecords() is called");
                $scope.searchDto = {};
                $scope.searchDto.status = "All";
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = 5;

                $scope.searchDto.orderByType = null;
                $scope.searchDto.sortByColumn = null;
                if ($scope.quotation.customer != null && $scope.quotation.customer.id != null) {
                    $scope.searchDto.searchCustomerName = $scope.quotation.customer.partyName;
                    $scope.searchRecord($scope.searchDto);
                }

            }
        }

        $scope.isSurchargeAvailable = function(charge) {
            if (charge == null || charge == undefined || charge.chargeMaster == null || charge.chargeMaster == undefined) {
                return;
            }
            charge.surChargeFlag = false;
            GetSurcharge.get({
                chargeId: charge.chargeMaster.id
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    if (data.responseObject == true) {
                        charge.surChargeFlag = true;
                        if ($scope.quotation.customer != null && $scope.quotation.customer != undefined && $scope.quotation.customer != '') {
                            if (($scope.quotation.customer.partyDetail != null &&
                                    $scope.quotation.customer.partyDetail != undefined) &&
                                $scope.quotation.customer.partyDetail.billingFscScOnGrossWeight == true) {
                                charge.actualchargeable = 'ACTUAL';
                            } else {
                                charge.actualchargeable = 'CHARGEABLE';
                            }
                        }

                    }

                } else {
                    console.log("GetSurcharge Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("GetSurcharge Failed : " + error)
            });

        }

        $scope.getChargeDisableFlag = function() {
            if ($scope.quotation != undefined && $scope.quotation.quotationDetailList[0] != null &&
                $scope.quotation.quotationDetailList[0].quotationCarrierList != null &&
                $scope.quotation.quotationDetailList[0].quotationCarrierList != undefined &&
                $scope.quotation.quotationDetailList[0].quotationCarrierList.length > 0) {

                for (var i = 0; i < $scope.quotation.quotationDetailList[0].quotationCarrierList.length; i++) {

                    for (var j = 0; j < $scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList.length; j++) {
                        $scope.isSurchargeAvailable($scope.quotation.quotationDetailList[0].quotationCarrierList[i].quotationChargeList[j]);
                    }
                }
            }
        }


        $scope.salesQuotationViewPage = function(id) {
            console.log("View Quotation Detail page of " + id);
            $rootScope.nav_src_bkref_key = new Date().toISOString();

            $state.go("layout.salesQuotationView", {
                quotationId: id,
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                bkState: $state.current.name,
                bkParam: JSON.stringify($stateParams),
                showActionButton: 1
            });
        }


        //general notes popup
        var addgeneralModal;
        $scope.generalModel = function() {

            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_GENERAL_NOTES_VIEW)) {
                addgeneralModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/sales/quotation/view/general_notes_popup.html',
                    show: false
                });
                addgeneralModal.$promise.then(addgeneralModal.show);
                $rootScope.navigateToNextField('headerNote');
            }
        };


        //attachment button
        var addattachModal;
        $scope.attachModel = function() {
            if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_ATTACHMENT_VIEW)) {
                addattachModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/sales/quotation/view/quotation_attachment_popup.html',
                    show: false
                });
                if ($scope.quotation.quotationAttachementList == undefined || $scope.quotation.quotationAttachementList == null || $scope.quotation.quotationAttachementList.length == undefined || $scope.quotation.quotationAttachementList.length == 0) {
                    $scope.quotation.quotationAttachementList = [];
                    $scope.quotation.quotationAttachementList.push({});
                } else {

                }
                $scope.quotationAttachmentListBackUp = cloneService.clone($scope.quotation.quotationAttachementList);
                addattachModal.$promise.then(addattachModal.show);
                $rootScope.navigateToNextField('0attachRefNo0');
            }
        };

        $scope.saveAttachment = function() {

            $scope.quotation.quotationAttachementList = $scope.quotationAttachmentListBackUp;

            if ($scope.attachConfig.isAttachmentCheck() == false) {
                Notification.error($rootScope.nls["ERR250"]);
                return false;
            }
            addattachModal.$promise.then(addattachModal.hide);
        }

        $scope.cancelAttachment = function() {
            if (EmptyRowChecker.isEmptyRow($scope.quotation.quotationAttachementList[0])) {
                $scope.quotation.quotationAttachementList = null;
            }
            addattachModal.$promise.then(addattachModal.hide);
        }


        $scope.searchRecord = function(searchObj) {
            QuotationSearch.query(searchObj).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                $scope.quotationArr = data.responseObject.searchResult;
                console.log($scope.quotationArr);
                $scope.topQuotationsConfig.title = $scope.quotation.customer.partyName;
                $scope.topRecords = [];
                angular.forEach($scope.quotationArr, function(item) {
                    var dataObj = {};
                    dataObj.id = item.id;
                    dataObj.quotationNo = item.quotationNo;
                    dataObj.customerName = item.customerName;
                    dataObj.status = item.approved;
                    dataObj.serviceCode = item.serviceCode
                    dataObj.originName = item.originCode;
                    dataObj.destinationName = item.destinationCode;
                    $scope.topRecords.push(dataObj);
                });
                $scope.contained_progressbar.complete();
                $scope.topQuotationsConfig.showTopQuotationsFlag = true;
            });
        }

        $scope.editInit = function(quotationId) {
            QuotationView.get({
                id: quotationId
            }, function(data) {
                if (data.responseCode == "ERR0") {


                    console.log("Quotation view Successfully ", data.responseObject)
                    $scope.quotation = data.responseObject;
                    $scope.getChargeDisableFlag();
                    if ($scope.quotation != undefined && $scope.quotation != null && $scope.quotation.shipper != undefined && $scope.quotation.shipper != null &&
                        $scope.quotation.shipper.partyName != undefined && $scope.quotation.shipper.partyName != null) {
                        $rootScope.subTitle = $scope.quotation.shipper.partyName;
                    } else {
                        $rootScope.subTitle = "Unknown Shipper"
                    }
                    $rootScope.serviceCodeForUnHistory = "";
                    $rootScope.orginAndDestinationUnHistory = "";

                    valueAddedServicesFactory.getAllValue.query().$promise.then(function(data) {

                        if ($scope.quotation != undefined && $scope.quotation != null &&
                            $scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null &&
                            $scope.quotation.quotationDetailList.length > 0 && $scope.quotation.quotationDetailList[0] != null) {

                            if ($scope.quotation.quotationDetailList[0].serviceMaster != undefined && $scope.quotation.quotationDetailList[0].serviceMaster != null) {
                                $rootScope.serviceCodeForUnHistory = $scope.quotation.quotationDetailList[0].serviceMaster.serviceCode;
                            }

                            if ($scope.quotation.quotationDetailList[0].origin != undefined && $scope.quotation.quotationDetailList[0].origin != null) {
                                $rootScope.orginAndDestinationUnHistory = $scope.quotation.quotationDetailList[0].origin.portCode;
                            }
                            if ($scope.quotation.quotationDetailList[0].destination != undefined && $scope.quotation.quotationDetailList[0].destination != null) {
                                $rootScope.orginAndDestinationUnHistory = $rootScope.orginAndDestinationUnHistory + " > " + $scope.quotation.quotationDetailList[0].destination.portCode;
                            }
                        }


                        for (var i = 0; i < $scope.quotation.quotationDetailList.length; i++) {

                            $scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList = [];

                            $scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList = JSON.parse(JSON.stringify(data.responseObject.searchResult));


                            if ($scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList != null &&
                                $scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList.length > 0) {

                                for (var j = 0; j < $scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList.length; j++) {

                                    for (var k = 0; k < $scope.quotation.quotationDetailList[i].quotationValueAddedServiceList.length; k++) {

                                        if (($scope.quotation.quotationDetailList[i].quotationValueAddedServiceList[k].valueAddedServices.id == $scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList[j].id)) {

                                            $scope.quotation.quotationDetailList[i].quotationValueAddedTmpServiceList[j].selected = true;
                                            break
                                        }
                                    }

                                }
                            }

                        }




                    }, function(response) {
                        console.log("Error found in Quoation Value  Added Service ");
                    });
                    var rHistoryObj = {
                        'title': 'Quotation Edit # ' + $stateParams.quotationId,
                        'subTitle': $rootScope.subTitle,
                        'stateName': $state.current.name,
                        'stateParam': JSON.stringify($stateParams),
                        'stateCategory': 'Quotation',
                        'serviceType': 'AIR',
                        'serviceCode': $rootScope.serviceCodeForUnHistory,
                        'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                        'showService': true
                    }
                    RecentHistorySaveService.form(rHistoryObj);


                    $scope.quotation.loggedOn = $rootScope.dateToString($scope.quotation.loggedOn);
                    $scope.quotation.validFrom = $rootScope.dateToString($scope.quotation.validFrom);
                    $scope.quotation.validTo = $rootScope.dateToString($scope.quotation.validTo);
                    if ($scope.quotation.approvedDate != null) {
                        $scope.quotation.approvedDate = $rootScope.dateToString($scope.quotation.approvedDate, true);
                    } else {
                        $scope.quotation.approvedDate = null;
                    }
                    $scope.init();
                } else {
                    console.log("Quotation view Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("Quotation view Failed : " + error)
            });
        }


        $scope.$on('addSalesQuotationEventUnload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            console.log("On Add Quote Unload ", confirmation.message);
        });

        $scope.$on('addSalesQuotationEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);
            $scope.quotation.quotationAttachementList = [];
            $scope.quotation.quotationAttachementList = $scope.attachConfig.data;
            //	    quotationDetail.serviceMaster
            localStorage.reloadFormData = JSON.stringify($scope.quotation);
            localStorage.isQuoteReloaded = "YES";
            console.log("$scope.quotation Before Reload Scope Obj :: ", $scope.quotation);
            console.log("$scope.quotation Before Reload Session Object:: ", localStorage.reloadFormData);
            e.preventDefault();
        });
        $scope.$on('editSalesQuotationEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);
            $scope.quotation.quotationAttachementList = [];
            $scope.quotation.quotationAttachementList = $scope.attachConfig.data;
            localStorage.reloadFormData = JSON.stringify($scope.quotation);
            localStorage.isQuoteReloaded = "YES";
            e.preventDefault();
        });

        $scope.$on('createSalesQuotationEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);
            $scope.quotation.quotationAttachementList = [];
            $scope.quotation.quotationAttachementList = $scope.attachConfig.data;
            localStorage.reloadFormData = JSON.stringify($scope.quotation);
            localStorage.isQuoteReloaded = "YES";
            e.preventDefault();
        });

        $scope.$on('addSalesQuotationEvent', function(events, args) {

            console.log("addSalesQuotationEvent", $scope.quotation);

            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);
            $scope.quotation.quotationAttachementList = [];
            $scope.quotation.quotationAttachementList = $scope.attachConfig.data;

            $rootScope.unfinishedData = $scope.quotation;
            $rootScope.unfinishedFormTitle = "Quotation (New)";
            $rootScope.category = "Quotation";

            $rootScope.serviceCodeForUnHistory = "";
            $rootScope.orginAndDestinationUnHistory = "";

            if ($scope.quotation != undefined && $scope.quotation != null && $scope.quotation.customer != undefined && $scope.quotation.customer != null &&
                $scope.quotation.customer.partyName != undefined && $scope.quotation.customer.partyName != null) {
                $rootScope.subTitle = $scope.quotation.customer.partyName;
            } else {
                $rootScope.subTitle = "Unknown " + $rootScope.appMasterData['Party_Label'];
            }
            console.log("addSalesQuotationEvent Broadcast Listener :: ", args);
        });



        $scope.$on('editSalesQuotationEvent', function(events, args) {

            console.log("editSalesQuotationEvent", $scope.quotation);


            $scope.quotation.quotationDetailList = [];
            $scope.quotation.quotationDetailList.push($scope.quotationDetail);
            $scope.quotation.quotationAttachementList = [];
            $scope.quotation.quotationAttachementList = $scope.attachConfig.data;

            $rootScope.serviceCodeForUnHistory = "";
            $rootScope.orginAndDestinationUnHistory = "";


            $rootScope.unfinishedData = $scope.quotation;
            $rootScope.category = "Quotation";
            $rootScope.unfinishedFormTitle = "Quotation Edit # " + $scope.quotation.id;

            if ($scope.quotation != undefined && $scope.quotation != null && $scope.quotation.customer != undefined && $scope.quotation.customer != null &&
                $scope.quotation.customer.partyName != undefined && $scope.quotation.customer.partyName != null) {
                $rootScope.subTitle = $scope.quotation.customer.partyName;
            } else {
                $rootScope.subTitle = "Unknown " + $rootScope.appMasterData['Party_Label'];
            }

            console.log("editSalesQuotationEvent Broadcast Listener :: ", args);
        })

        $scope.$on('createSalesQuotationEvent', function(events, args) {

            console.log("editSalesQuotationEvent", $scope.quotation);

            $rootScope.unfinishedData = $scope.quotation;
            $rootScope.category = "Quotation";
            $rootScope.unfinishedFormTitle = "Quotation (New)";

            if ($scope.quotation != undefined && $scope.quotation != null && $scope.quotation.customer != undefined && $scope.quotation.customer != null &&
                $scope.quotation.customer.partyName != undefined && $scope.quotation.customer.partyName != null) {
                $rootScope.subTitle = $scope.quotation.customer.partyName;
            } else {
                $rootScope.subTitle = "Unknown " + $rootScope.appMasterData['Party_Label'];
            }

            console.log("createSalesQuotationEvent Broadcast Listener :: ", args);
        })

        //Get the Party master From Root Scope and then assign the master values to appropriate Object
        $scope.populatePartyMaster = function() {
            console.log("$stateParams.forObj -- ", $stateParams.forObj, "  || $rootScope.naviPartyMaster -- ", $rootScope.naviPartyMaster);

            if ($stateParams.forObj != undefined && $stateParams.forObj != null && $rootScope.naviPartyMaster != undefined &&
                $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != undefined && $rootScope.naviPartyMaster.id != null) {

                if ($stateParams.forObj == "quotation.customer") {
                    $scope.quotation.customer = $rootScope.naviPartyMaster;
                    $scope.getPartyAddress('CUSTOMER', $scope.quotation.customer);
                } else if ($stateParams.forObj == "quotation.shipper") {
                    $scope.quotation.shipper = $rootScope.naviPartyMaster;
                    $scope.getPartyAddress('SHIPPER', $scope.quotation.shipper);
                }

            }
        }
        $scope.isReloaded = localStorage.isQuoteReloaded;
        if ($stateParams.action == "CREATE") {
            console.log("I am In Quotation CREATE Page");

            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isQuoteReloaded = "NO";
                    $scope.quotation = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.quotation = $rootScope.selectedUnfilledFormData;
                    $scope.populatePartyMaster();
                }

                if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                    $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                    $scope.quotation.quotationDetailList = [];
                    $scope.setDetailTab('Services', 'service');
                    $scope.getChargeDisableFlag();
                }
                if ($scope.quotation.quotationAttachementList != undefined && $scope.quotation.quotationAttachementList != null && $scope.quotation.quotationAttachementList.length > 0) {
                    //				    $scope.attachConfig.data = $scope.quotation.quotationAttachementList;
                    for (var inde = 0; inde < $scope.quotation.quotationAttachementList.length; inde++) {
                        if ($scope.attachConfig == undefined) {
                            $scope.attachConfig = {
                                "isEdit": true,
                                "isDelete": true,
                                "tableSize": "col-xs-8",
                                "columnDefs": [

                                        {
                                            "name": "Reference No",
                                            "model": "refNo",
                                            "type": "text",
                                            "width": "inl_lp"
                                        },
                                        {
                                            "name": "File Name",
                                            "model": "fileName",
                                            "type": "file"
                                        },
                                        {
                                            "name": "Actions",
                                            "model": "action",
                                            "type": "action",
                                            "width": "inl_l"
                                        }
                                    ]
                                    //,
                                    //"fieldData":[
                                    //	{"label":"Reference No","elemData":{"type":"text"},"scope":"attachMentData.refNo","validation":["required","number"]},
                                    //	{"label":"File Name","elemData":{"type":"text","disabled":true},"scope":"attachMentData.file.name"},
                                    //	{"label":"File","elemData":{"type":"file"},"scope":"attachMentData.file","validation":["required"]},
                                    //]
                            }
                        }

                        if ($scope.attachConfig.data == undefined) {
                            $scope.attachConfig.data = [];
                        }

                        $scope.attachConfig.data.push($scope.quotation.quotationAttachementList[inde]);
                    }
                    $scope.quotation.quotationAttachementList = [];
                } else {
                    //$scope.quotation.quotationAttachementList = [{}];
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isQuoteReloaded = "NO";
                    $scope.quotation = JSON.parse(localStorage.reloadFormData);

                    if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                        $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                        $scope.quotation.quotationDetailList = [];
                    }
                    if ($scope.quotation.quotationAttachementList != undefined && $scope.quotation.quotationAttachementList != null && $scope.quotation.quotationAttachementList.length > 0) {
                        $scope.attachConfig.data = $scope.quotation.quotationAttachementList;
                        $scope.quotation.quotationAttachementList = [];
                    }

                    $scope.getChargeDisableFlag();

                } else {
                    $scope.quotation = $rootScope.createQuotation;
                    console.log("Quotation From enquiry ", $scope.quotation);
                    $scope.init()
                    $scope.getChargeDisableFlag();
                    /*$timeout(function() {
                            appMetaFactory.pricing.findByOriginAndDestination({
                                        enquiryId : $stateParams.enquiryId,
                                        originId : $scope.quotation.quotationDetailList[0].origin.id,
                                        destinationId : $scope.quotation.quotationDetailList[0].destination.id
                                    }, function(data) {
                                        if (data.responseCode =="ERR0"){
                                            $scope.quotation.quotationDetailList[0].quotationChargeList = data.responseObject;
                                            if($scope.quotation.quotationDetailList[0].quotationChargeList == undefined || $scope.quotation.quotationDetailList[0].quotationChargeList == null || $scope.quotation.quotationDetailList[0].quotationChargeList.length == 0) {
                                                $scope.quotation.quotationDetailList[0].quotationChargeList=[{}];
                                            } 
                                        } else {
                                            console.log(data.responseDescription);
                                        }
                                    }, function(error) {
                                        console.log(error);
                                    });
                        },2000);*/

                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesQuotation"
            }, {
                label: "Quotation",
                state: "layout.salesQuotation"
            }, {
                label: "Edit Quotation",
                state: null
            }];

        } else if ($stateParams.action == "ADD") {
            console.log("I am In Quotation ADD Page");

            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isQuoteReloaded = "NO";
                    $scope.quotation = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.quotation = $rootScope.selectedUnfilledFormData;

                    $scope.populatePartyMaster();
                }
                if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                    $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                    $scope.quotation.quotationDetailList = [];
                    $scope.getChargeDisableFlag();
                }
                if ($scope.quotation.quotationAttachementList != undefined && $scope.quotation.quotationAttachementList != null && $scope.quotation.quotationAttachementList.length >= 0) {
                    //			    $scope.attachConfig.data = $scope.quotation.quotationAttachementList;
                    for (var inde = 0; inde < $scope.quotation.quotationAttachementList.length; inde++) {
                        if ($scope.attachConfig == undefined) {
                            $scope.attachConfig = {
                                "isEdit": true,
                                "isDelete": true,
                                "tableSize": "col-xs-8",
                                "columnDefs": [

                                    {
                                        "name": "Reference No",
                                        "model": "refNo",
                                        "type": "text",
                                        "width": "inl_lp"
                                    },
                                    {
                                        "name": "File Name",
                                        "model": "fileName",
                                        "type": "file"
                                    },
                                    {
                                        "name": "Actions",
                                        "model": "action",
                                        "type": "action",
                                        "width": "inl_l"
                                    }
                                ],
                                "data": []
                            }
                        }

                        if ($scope.attachConfig.data == undefined) {
                            $scope.attachConfig.data = [];
                        }

                        $scope.attachConfig.data.push($scope.quotation.quotationAttachementList[inde]);
                    }
                    $scope.quotation.quotationAttachementList = [];
                } else {
                    //$scope.quotation.quotationAttachementList = [{}];
                }
                $scope.getChargeDisableFlag();
                $scope.openDefaultTabs();
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isQuoteReloaded = "NO";
                    $scope.quotation = JSON.parse(localStorage.reloadFormData);

                    if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                        $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                        $scope.quotation.quotationDetailList = [];
                        $scope.getChargeDisableFlag();
                    }

                    $scope.oldData = "";
                } else {
                    $scope.quotation = {};
                    $scope.init();

                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesQuotation"
            }, {
                label: "Quotation",
                state: "layout.salesQuotation"
            }, {
                label: "Add Quotation",
                state: null
            }];
        } else {
            console.log("I am In  Edit Quotation ID :: ", $stateParams.quotationId);
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isQuoteReloaded = "NO";
                    $scope.quotation = JSON.parse(localStorage.reloadFormData);

                } else {
                    $scope.quotation = $rootScope.selectedUnfilledFormData;
                    $scope.populatePartyMaster();
                }
                if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                    $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                    $scope.quotation.quotationDetailList = [];
                    $scope.getChargeDisableFlag();
                }
                if ($scope.quotation.quotationAttachementList != undefined && $scope.quotation.quotationAttachementList != null && $scope.quotation.quotationAttachementList.length >= 0) {
                    //			    $scope.attachConfig.data = $scope.quotation.quotationAttachementList;
                    for (var inde = 0; inde < $scope.quotation.quotationAttachementList.length; inde++) {
                        if ($scope.attachConfig == undefined) {
                            $scope.attachConfig = {
                                "isEdit": true,
                                "isDelete": true,
                                "tableSize": "col-xs-8",
                                "columnDefs": [

                                    {
                                        "name": "Reference No",
                                        "model": "refNo",
                                        "type": "text",
                                        "width": "inl_lp"
                                    },
                                    {
                                        "name": "File Name",
                                        "model": "fileName",
                                        "type": "file"
                                    },
                                    {
                                        "name": "Actions",
                                        "model": "action",
                                        "type": "action",
                                        "width": "inl_l"
                                    }
                                ],
                                "data": []
                            }
                        }
                        if ($scope.attachConfig.data == undefined) {
                            $scope.attachConfig.data = [];
                        }
                        $scope.attachConfig.data.push($scope.quotation.quotationAttachementList[inde]);
                    }
                    $scope.quotation.quotationAttachementList = [];
                } else {
                    //$scope.quotation.quotationAttachementList = [{}];
                }
                $scope.getChargeDisableFlag();
                $scope.openDefaultTabs();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isQuoteReloaded = "NO";
                    $scope.quotation = JSON.parse(localStorage.reloadFormData);

                    if ($scope.quotation.quotationDetailList != undefined && $scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                        $scope.quotationDetail = $scope.quotation.quotationDetailList[0];
                        $scope.quotation.quotationDetailList = [];
                        $scope.getChargeDisableFlag();
                    }
                } else {
                    $scope.editInit($stateParams.quotationId)
                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Sales",
                state: "layout.salesQuotation"
            }, {
                label: "Quotation",
                state: "layout.salesQuotation"
            }, {
                label: "Edit Quotation",
                state: null
            }];
        }


    }
]);