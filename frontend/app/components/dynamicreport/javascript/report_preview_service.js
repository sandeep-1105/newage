/**
 *  @Author                 Date                   Version                             Reason 
 *  
 *  K.SathishKumar         14/06/2017                 0.1                       Used to call api for download,print options for preview reports
 *     
 *
 */
app.service('previewReportService',['$rootScope', 'downloadFactory', '$state', '$stateParams',
    function($rootScope, downloadFactory, $state, $stateParams) {

    /*
     * @Params
     * ReportName
     * TYPE
     * ID
     * Num--Document No
     */

    this.printDownload = function(reportName, type, id, doId, isDetail, Screen, routingObj) {
        console.log("Check which state..." + isDetail);
        //var reportDetailData = {resourceRefId:id, isDocument:true,isService:false,documentNo:num, reportName : reportName, reportDisplayName : 'HAWB| Document # '+num, key : reportName};

        try {
            $rootScope.mainpreloder = true;
            var reportDownloadRequest = {
                resourceId: id,
                doId: doId,
                downloadOption: type, // "Download","Preview","Print"
                downloadFileType: 'PDF', //PDF
                reportName: reportName,
                single: true
            };
            var successOpeartion = downloadFactory.download(reportDownloadRequest)
            var stateCount = 0;
            if ($stateParams.count != undefined && $stateParams.count != null) {
                stateCount = $stateParams.count + 1;
            }
            successOpeartion.then(function(data) {
                $rootScope.mainpreloder = false;
                navigate(reportName, type, id, doId, isDetail, Screen, routingObj, stateCount);
            }, function(error) {
                $rootScope.mainpreloder = false;
                navigate(reportName, type, id, doId, isDetail, Screen, routingObj, stateCount);
                return;
            });
        } catch (e) {
            $rootScope.mainpreloder = false;
            navigate(reportName, type, id, doId, isDetail, Screen, routingObj, stateCount);
            console.log(e);
        }

    }


    this.reportGenerationNavigation = function(reportName, type, id, doId, isDetail, Screen, routingObj) {
        try {
            navigate(reportName, type, id, doId, isDetail, Screen, routingObj, 0);
        } catch (e) {
            console.log(e);
        }
    }



    function navigate(reportName, type, id, doId, isDetail, Screen, routingObj, stateCount) {

        if (Screen == 'MasterShipment') {
            if (reportName == 'SHIPMENT_MAWB') {
                if (isDetail) {
                    $state.go('layout.editAirConsol', {
                        consolId: routingObj.id,
                        forMawb: routingObj.forMawb,
                        count: stateCount
                    });
                } else {
                    $state.go('layout.viewNewAirConsol', {
                        consolId: routingObj.id,
                        forMawb: routingObj.forMawb,
                        count: stateCount
                    });
                }
            } else {
                //Delivery order
                if (isDetail) {
                    $state.go('layout.editAirConsol', {
                        consolId: routingObj.id,
                        count: stateCount
                    });
                } else {
                    $state.go('layout.viewNewAirConsol', {
                        consolId: routingObj.id,
                        count: stateCount
                    });
                }
            }
        }
        if (Screen == 'Shipment') {
            if (!isDetail) {

                $state.go('layout.viewCrmShipment', {
                    shipmentUid: routingObj.shipmentUid,
                    documentId: routingObj.documentId,
                    forHawb: routingObj.forHawb,
                    count: stateCount,
                    serviceId: routingObj.serviceId
                });

            } else {

                $state.go("layout.editNewShipment", {
                    shipmentId: routingObj.id,
                    submitAction: 'Saved',
                    count: stateCount
                });
            }
        }

        if (Screen == 'AirDocument') {

            if (routingObj.docType == "HAWB") {
                $state.go("layout.airDocumentsView", {
                    id: routingObj.id,
                    docType: 'HAWB',
                    docNo: routingObj.docNo,
                    count: stateCount
                });
            } else if (routingObj.docType == "MAWB") {
                $state.go("layout.airDocumentsView", {
                    id: routingObj.id,
                    docType: 'MAWB',
                    docNo: routingObj.docNo,
                    count: stateCount
                });
            } else if (routingObj.docType == "DO") {
                $state.go("layout.airDocumentsView", {
                    id: routingObj.id,
                    docType: 'DO',
                    docNo: routingObj.docNo,
                    count: stateCount
                });
            }

        }

    }
}]);