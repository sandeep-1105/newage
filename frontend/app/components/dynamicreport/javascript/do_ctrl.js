/**
 * Do-Controller for upating ,Downloaing, and printing the Delivery Order--For Consol Ddocument
 * @Author
 * K.Sathish Kumar  14-09-2016
 *
 */
app.controller('doController',['$rootScope', '$scope', '$modal', 'ngDialog', 'LogoGetByLocation', 'ChangeProfileServer', 
        'ConsolGet', '$state', 'ShipmentGetFromUID', 'Notification', 'ConsolDocumentUpdate',
        '$timeout', 'ConsolEdit', 'ValidateUtil', 'cloneService', '$location', '$window', '$sce', 
        'SequenceFactoryForDO', '$stateParams', 'appMetaFactory', 'previewReportService', 
    function($rootScope, $scope, $modal, ngDialog, LogoGetByLocation, ChangeProfileServer, 
        ConsolGet, $state, ShipmentGetFromUID, Notification, ConsolDocumentUpdate,
        $timeout, ConsolEdit, ValidateUtil, cloneService, $location, $window, $sce, 
        SequenceFactoryForDO, $stateParams, appMetaFactory, previewReportService) {

    $scope.disableSubmitBtn = false;
    $scope.autoAttachMentDto = {};

    $scope.doOPeration = function(opname) {
        $rootScope.mainpreloder = true;
        if ($scope.showDetail != undefined && $scope.showDetail) {
            $scope.updateDo(false, opname);
        } else {
            $scope.updateDo(true, opname);
        }
    }

    //which will update the values of service-document and consol document
    $scope.updateDo = function(isEdit, opName) {

        if ($scope.validateDeliveryOrder(0)) {
            $scope.deliveryOrderDto = {};
            $scope.deliveryOrderDto.consolCloneDO = $scope.consolCloneDO;
            $scope.deliveryOrderDto.documentDetailForDO = $scope.documentDetailForDO;
            if (!isEdit) {
                $scope.deliveryOrderDto.documentDetailForDO.eta = $rootScope.sendApiDateAndTime($scope.deliveryOrderDto.documentDetailForDO.eta);
                ConsolDocumentUpdate.save($scope.deliveryOrderDto).$promise.then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.checkForDo(opName, isEdit);
                        } else {
                            Notification.warning($rootScope.nls["ERR403"]);
                            $rootScope.mainpreloder = false;
                            $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
                        }
                    },
                    function(error) {
                        $rootScope.mainpreloder = false;
                        $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
                    });
            } else {
                $scope.editOpName = opName;
                $scope.updateConsol('DO');
            }
        } else {
            Notification.warning($rootScope.nls["ERR404"]);
            $rootScope.mainpreloder = false;
        }
    }

    $scope.$on('consol-update-for-do', function(event, args) {
        console.log("listener updation consol");
        $scope.deliveryOrderDto = {};
        $scope.deliveryOrderDto.args = args;
        if (args != null) {
            $scope.deliveryOrderDto.consolCloneDO = $scope.getTempObject($scope.deliveryOrderDto.args.state);
            if ($scope.deliveryOrderDto.consolCloneDO != undefined && $scope.deliveryOrderDto.consolCloneDO != null && $scope.deliveryOrderDto.consolCloneDO.shipmentLinkList != null) {
                for (var i = 0; i < $scope.deliveryOrderDto.consolCloneDO.shipmentLinkList.length; i++) {
                    if ($scope.deliveryOrderDto.consolCloneDO.shipmentLinkList[i].service != undefined && $scope.deliveryOrderDto.consolCloneDO.shipmentLinkList[i].service.documentList != null) {
                        for (var j = 0; j < $scope.deliveryOrderDto.consolCloneDO.shipmentLinkList[i].service.documentList.length; j++) {
                            if ($scope.doShow != undefined && $scope.doShow.documentId != undefined && $scope.doShow.documentId != null) {
                                if ($scope.deliveryOrderDto.consolCloneDO.shipmentLinkList[i].service.documentList[j].id == $scope.doShow.documentId) {
                                    $scope.deliveryOrderDto.documentDetailForDO = $scope.deliveryOrderDto.consolCloneDO.shipmentLinkList[i].service.documentList[j]; // service document object for delivery order 
                                }
                            } else {
                                $scope.deliveryOrderDto.documentDetailForDO = $scope.documentDetailForDO;
                            }
                        }
                    }
                }
            }
            $scope.deliveryOrderDto.documentDetailForDO.eta = $rootScope.sendApiDateAndTime($scope.deliveryOrderDto.documentDetailForDO.eta);
        } else {
            $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
        }
        ConsolDocumentUpdate.save($scope.deliveryOrderDto).$promise.then(
            function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.checkForDo($scope.editOpName, true);
                } else {
                    Notification.warning($rootScope.nls["ERR403"]);
                    $rootScope.mainpreloder = false;
                    $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
                }
            },
            function(error) {
                Notification.warning($rootScope.nls["ERR404"]);
                $rootScope.mainpreloder = false;
                $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
            });

    });


    $scope.checkForDo = function(opName, isEdit) {

        var routingObj = {};
        if ($scope.screen != undefined && $scope.screen == 'AirDocument') {
            routingObj.id = $scope.documentDetailForDO.id;
            routingObj.docType = 'DO';
            routingObj.docNo = $scope.documentDetailForDO != undefined ? $scope.documentDetailForDO.doNumber : null;
        } else {
            routingObj.id = $scope.consolCloneDO.id;
        }
        if (opName == 'download') {
            SequenceFactoryForDO.fetch({
                'documentId': $scope.doShow.documentId
            }, function(data) {
                previewReportService.printDownload('MASTER_DO', 'Download', $scope.consolCloneDO.id, $scope.doId, isEdit, $scope.screen, routingObj);
                $scope.routeToPage(isEdit);
            });
        } else if (opName == 'print') {
            SequenceFactoryForDO.fetch({
                'documentId': $scope.doShow.documentId
            }, function(data) {
                previewReportService.printDownload('MASTER_DO', 'Print', $scope.consolCloneDO.id, $scope.doId, isEdit, $scope.screen, routingObj);
                $scope.routeToPage(isEdit);
            });
        } else if (opName == 'update') {
            $scope.routeToPage(isEdit, opName);
        } else {
            console.log("Nothing Happen");
            $scope.routeToPage(isEdit);
        }
        $rootScope.mainpreloder = false;
    }


    //for routing depand up on edit and detail page
    $scope.routeToPage = function(isEdit, opName) {
        $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);

        if ($scope.screen != undefined && $scope.screen == 'AirDocument') {

            return;
        }


        var params = {};
        var stateCount = 0;
        if ($stateParams.count != undefined && $stateParams.count != null) {
            stateCount = $stateParams.count + 1;
        }
        var doShowJson = JSON.stringify($scope.doShow);
        localStorage.setItem('isDoShow', doShowJson)
        var stateCount = 0;
        if ($stateParams.count != undefined && $stateParams.count != null) {
            stateCount = $stateParams.count + 1;
        }
        if (opName != undefined && isEdit && opName == 'update') {
            $state.go('layout.editAirConsol', {
                consolId: $scope.consolCloneDO.id,
                count: stateCount
            });
        } else if (opName != undefined && opName == 'update') {
            $state.go('layout.viewNewAirConsol', {
                consolId: $scope.consolCloneDO.id,
                count: stateCount
            });
        }
    }


    $scope.diasbleSubmitBtnFn = function() {
        $rootScope.mainpreloder = true;
        $scope.disableSubmitBtn = true;
        $timeout(function() {
            $rootScope.mainpreloder = false;
            $scope.disableSubmitBtn = false;
        }, 3000);
    }



    //Delivery Order
    $scope.previewReportDeliveryOrder = function(consol, serviceId, shipmentUid, documentId, type, showDetail, screen) {

        if (consol != undefined && consol.consolDocument != undefined && (consol.consolDocument.isHold || consol.consolDocument.isHold == 'Yes')) {
            Notification.error($rootScope.nls["ERR96561"]);
            return;
        }

        $scope.showDetail = showDetail;
        $scope.screen = 'MasterShipment';
        $scope.doId = documentId;
        var consoleId = consol ? consol.id : null;
        $scope.doShow = {
            doId: documentId,
            consolId: consoleId,
            serviceId: serviceId,
            documentId: documentId,
            shipmentUid: shipmentUid,
            type: type,
            isEdit: $scope.showDetail
        }
        if (!$scope.showDetail) {
            if ($scope.validateConsol(0) && $scope.validateConsolOthers()) {
                $scope.consolDoContinue(consol, serviceId, shipmentUid, documentId, type, $scope.showDetail);
            }
        } else {
            $scope.consolDoContinue(consol, serviceId, shipmentUid, documentId, type, $scope.showDetail);
        }
    }


    $scope.consolDoContinue = function(consol, serviceId, shipmentUid, documentId, type, showDetail) {


        // which is used for showing preview report on update/print and download
        $scope.doId = documentId;;
        $rootScope.clientMessage = null;
        $scope.isEdit = showDetail;
        $scope.docType = type;
        $scope.deliveryOrder = {};
        $scope.partyNoifyFlag = false;
        /*if(consol!=undefined && consol.shipmentLinkList!=undefined && consol.shipmentLinkList!=null &&consol.shipmentLinkList.length>0){*/
        $scope.serviceId = [];
        if (serviceId != undefined && serviceId != null) {
            $scope.serviceId.push(serviceId);
        } else {
            console.log("problem in getting")
        }
        //checking incoice for particular service
        appMetaFactory.reportInvoiceValidation.findByServiceId({
            serviceId: $scope.serviceId,
        }).$promise.then(function(
            data, status) {
            if (data.responseCode == "ERR0") {
                if (!$scope.isEdit) {
                    $scope.consolCloneDO = $scope.consol;
                    $scope.setValuesforPreview($scope.consolCloneDO, documentId);
                } else {
                    ConsolGet.get({
                            id: consol.id
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                $scope.consolCloneDO = data.responseObject;
                                $scope.setValuesforPreview($scope.consolCloneDO, documentId);
                            }
                        },
                        function(error) {
                            console.log("problem while getting");
                        });
                }
            } else if (data.responseCode == "ERR0149") {
                return false;
            } else {
                console.log("unknown response");
            }
        }, function(error) {
            console.log("error while fetching service id");
            return false;
        });
        /*}else{
        			$rootScope.clientMessage = $rootScope.nls["ERR96524"];
        			return false;
        		}//end of preview Reports 
        	*/

    }

    $scope.setValuesforPreview = function(consolCloneDO, documentId) {
        $scope.consolCloneDO = consolCloneDO;
        if ($scope.consolCloneDO != undefined && $scope.consolCloneDO != null && $scope.consolCloneDO.shipmentLinkList != null) {
            for (var i = 0; i < $scope.consolCloneDO.shipmentLinkList.length; i++) {
                if ($scope.consolCloneDO.shipmentLinkList[i].service != undefined && $scope.consolCloneDO.shipmentLinkList[i].service.documentList != null) {
                    for (var j = 0; j < $scope.consolCloneDO.shipmentLinkList[i].service.documentList.length; j++) {
                        if ($scope.consolCloneDO.shipmentLinkList[i].service.documentList[j].id == documentId) {
                            $scope.documentDetailForDO = $scope.consolCloneDO.shipmentLinkList[i].service.documentList[j]; // service document object for delivery order 
                        }
                    }
                }
            }
        } else {
            Notification.warning($rootScope.nls["ERR96524"]);
        }

        if ($scope.documentDetailForDO != undefined && ($scope.documentDetailForDO.canIssuedDate == undefined || $scope.documentDetailForDO.canIssuedDate == null || $scope.documentDetailForDO.canIssuedBy == undefined ||
                $scope.documentDetailForDO.canIssuedBy == null || $scope.documentDetailForDO.canIssuedBy.id == undefined)) {
            Notification.error($rootScope.nls["ERR96562"]);
            return;
        }

        if ($rootScope.userProfile.selectedUserLocation.companyMaster != undefined && $rootScope.userProfile.selectedUserLocation.companyMaster != null) {
            $scope.deliveryOrder.companyName = $rootScope.userProfile.selectedUserLocation.companyMaster.companyName;
            LogoGetByLocation.get({
                    locationid: $scope.consolCloneDO.location.id,
                }, function(data) {
                    if (data.responseCode == 'ERR0' && data.responseObject.encodedLogo != null) {
                        $scope.encodedLogo = data.responseObject.encodedLogo;
                    } else {
                        console.log("Logo is not avialable")
                        $scope.encodedLogo = null;
                    }
                },
                function(error) {
                    console.log("Consol get Failed : " + error)
                });
        }
        if ($rootScope.userProfile.selectedUserLocation != undefined && $rootScope.userProfile.selectedUserLocation != null) {

            $scope.deliveryOrder.companyAddress = "";
            if ($rootScope.userProfile.selectedUserLocation.addressLine1 != undefined && $rootScope.userProfile.selectedUserLocation.addressLine1 != null)
                $scope.deliveryOrder.companyAddress = $rootScope.userProfile.selectedUserLocation.addressLine1;

            if ($rootScope.userProfile.selectedUserLocation.addressLine2 != undefined && $rootScope.userProfile.selectedUserLocation.addressLine2 != null)
                $scope.deliveryOrder.companyAddress = $scope.deliveryOrder.companyAddress + "," + $rootScope.userProfile.selectedUserLocation.addressLine2 + "\n";

            if ($rootScope.userProfile.selectedUserLocation.addressLine3 != undefined && $rootScope.userProfile.selectedUserLocation.addressLine3 != null)
                $scope.deliveryOrder.companyAddress = $scope.deliveryOrder.companyAddress + $rootScope.userProfile.selectedUserLocation.addressLine3;

        } else {
            console.log("Location is not avialable")
        }

        if ($scope.documentDetailForDO.firstNotify != undefined && $scope.documentDetailForDO.firstNotify != null) {

            $scope.deliveryOrder.partyAddress = {};

            if ($scope.documentDetailForDO.firstNotifyAddress != undefined && $scope.documentDetailForDO.firstNotifyAddress != null && $scope.documentDetailForDO.firstNotifyAddress.addressLine1 != null)
                $scope.deliveryOrder.partyAddress = $scope.documentDetailForDO.firstNotifyAddress.addressLine1;

            if ($scope.documentDetailForDO.firstNotifyAddress != undefined && $scope.documentDetailForDO.firstNotifyAddress != null && $scope.documentDetailForDO.firstNotifyAddress.addressLine2 != null)
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.firstNotifyAddress.addressLine2;

            if ($scope.documentDetailForDO.firstNotifyAddress != undefined && $scope.documentDetailForDO.firstNotifyAddress != null && $scope.documentDetailForDO.firstNotifyAddress.addressLine3 != null)
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.firstNotifyAddress.addressLine3;

            if ($scope.documentDetailForDO.firstNotifyAddress.cityMaster != undefined && $scope.documentDetailForDO.firstNotifyAddress.cityMaster !== null) {
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.firstNotifyAddress.cityMaster.cityName;
            }
            if ($scope.documentDetailForDO.firstNotifyAddress.zipCode != undefined && $scope.documentDetailForDO.firstNotifyAddress.zipCode != null) {
                $scope.deliveryOrder.partyAddress = +"," + $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.firstNotifyAddress.zipCode;
            }
            if ($scope.documentDetailForDO.firstNotifyAddress.stateMaster != undefined && $scope.documentDetailForDO.firstNotifyAddress.stateMaster != null) {
                $scope.deliveryOrder.partyAddress = +"," + $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.firstNotifyAddress.stateMaster.stateName;
            }
            $scope.partyNoifyFlag = true;
        }

        if ($scope.documentDetailForDO.consignee != undefined && $scope.documentDetailForDO.consignee != null && !$scope.partyNoifyFlag) {
            $scope.deliveryOrder.partyAddress = {};
            if ($scope.documentDetailForDO.consigneeAddress != undefined && $scope.documentDetailForDO.consigneeAddress != null && $scope.documentDetailForDO.consigneeAddress.addressLine1 != null)
                $scope.deliveryOrder.partyAddress = $scope.documentDetailForDO.consigneeAddress.addressLine1;
            if ($scope.documentDetailForDO.consigneeAddress != undefined && $scope.documentDetailForDO.consigneeAddress != null && $scope.documentDetailForDO.consigneeAddress.addressLine2 != null)
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.consigneeAddress.addressLine2;
            if ($scope.documentDetailForDO.consigneeAddress != undefined && $scope.documentDetailForDO.consigneeAddress != null && $scope.documentDetailForDO.consigneeAddress.addressLine3 != null)
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.consigneeAddress.addressLine3;
            if ($scope.documentDetailForDO.consigneeAddress.cityMaster != undefined && $scope.documentDetailForDO.consigneeAddress.cityMaster != null) {
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.consigneeAddress.cityMaster.cityName;
            }
            if ($scope.documentDetailForDO.consigneeAddress.zipCode != undefined && $scope.documentDetailForDO.consigneeAddress.zipCode != null) {
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.consigneeAddress.zipCode;
            }
            if ($scope.documentDetailForDO.consigneeAddress.stateMaster != undefined && $scope.documentDetailForDO.consigneeAddress.stateMaster != null) {
                $scope.deliveryOrder.partyAddress = $scope.deliveryOrder.partyAddress + "," + $scope.documentDetailForDO.consigneeAddress.stateMaster.stateName;
            }
        }
        $scope.documentDetailForDO.doIssuedDate = $rootScope.dateToString(new Date());
        if ($scope.documentDetailForDO.eta != undefined && $scope.documentDetailForDO.eta != null) {
            $scope.documentDetailForDO.eta = $rootScope.dateToString($scope.documentDetailForDO.eta);
        }
        $scope.deliveryModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/dynamicreport/view/do.html',
            backdrop: 'static',
            show: false
        });

        $scope.deliveryModal.$promise.then($scope.deliveryModal.show);
        $timeout(function() {
            $scope.isError = true;
        }, 500);

    }

    $scope.getTempObject = function(baseObject) {
        var tmpObj = cloneService.clone(baseObject);
        tmpObj.ppcc = tmpObj.ppcc == false ? 'Prepaid' : 'Collect';


        tmpObj.eta = $rootScope.sendApiDateAndTime(tmpObj.eta);
        tmpObj.etd = $rootScope.sendApiDateAndTime(tmpObj.etd);
        tmpObj.atd = $rootScope.sendApiDateAndTime(tmpObj.atd);
        tmpObj.ata = $rootScope.sendApiDateAndTime(tmpObj.ata);

        if (tmpObj.serviceMaster != undefined && tmpObj.serviceMaster.importExport == "Import") {
            tmpObj.whoRouted = tmpObj.whoRouted == false ? 'Self' : 'Agent';
        }

        tmpObj.consolDocument.mawbIssueDate = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.mawbIssueDate);
        tmpObj.consolDocument.mawbGeneratedDate = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.mawbGeneratedDate);
        tmpObj.consolDocument.doIssuedDate = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.doIssuedDate);
        tmpObj.consolDocument.dimensionUnit = tmpObj.consolDocument.dimensionUnit == false ? 'CENTIMETERORKILOS' : 'INCHORPOUNDS';
        tmpObj.directShipment = tmpObj.directShipment == false ? 'No' : 'Yes';

        if (tmpObj.consolDocument.eta != null)
            tmpObj.consolDocument.eta = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.eta);
        if (tmpObj.consolDocument.etd != null)
            tmpObj.consolDocument.etd = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.etd);


        if (tmpObj.pickUpDeliveryPoint == undefined) {
            tmpObj.pickUpDeliveryPoint = {};
            tmpObj.pickUpDeliveryPoint.isOurPickUp = 'No';
        } else {
            tmpObj.pickUpDeliveryPoint.isOurPickUp = tmpObj.pickUpDeliveryPoint.isOurPickUp ? 'Yes' : 'No';
        }


        if (tmpObj.pickUpDeliveryPoint != undefined && tmpObj.pickUpDeliveryPoint != null &&
            tmpObj.pickUpDeliveryPoint != "") {

            if (tmpObj.pickUpDeliveryPoint.pickUpFollowUp != null)
                tmpObj.pickUpDeliveryPoint.pickUpFollowUp = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.pickUpFollowUp);

            if (tmpObj.pickUpDeliveryPoint.pickUpPlanned != null)
                tmpObj.pickUpDeliveryPoint.pickUpPlanned = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.pickUpPlanned);

            if (tmpObj.pickUpDeliveryPoint.pickUpActual != null)
                tmpObj.pickUpDeliveryPoint.pickUpActual = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.pickUpActual);

            if (tmpObj.pickUpDeliveryPoint.deliveryExpected != null)
                tmpObj.pickUpDeliveryPoint.deliveryExpected = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.deliveryExpected);

            if (tmpObj.pickUpDeliveryPoint.deliveryActual != null)
                tmpObj.pickUpDeliveryPoint.deliveryActual = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.deliveryActual);

            if (tmpObj.pickUpDeliveryPoint.doorDeliveryExpected != null)
                tmpObj.pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.doorDeliveryExpected);

            if (tmpObj.pickUpDeliveryPoint.doorDeliveryActual != null)
                tmpObj.pickUpDeliveryPoint.doorDeliveryActual = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.doorDeliveryActual);

        }

        // for dynamic column
        if (tmpObj.consolDocument != undefined && tmpObj.consolDocument != null &&
            tmpObj.consolDocument != "") {}

        if (tmpObj.connectionList != null && tmpObj.connectionList.length != undefined) {

            for (var i = 0; i < tmpObj.connectionList.length; i++) {
                tmpObj.connectionList[i].eta = $rootScope.sendApiDateAndTime(tmpObj.connectionList[i].eta);
                tmpObj.connectionList[i].etd = $rootScope.sendApiDateAndTime(tmpObj.connectionList[i].etd);
            }

        }

        if (tmpObj.eventList != undefined && tmpObj.eventList != null && tmpObj.eventList.length != 0) {
            for (var j = 0; j < tmpObj.eventList.length; j++) {

                tmpObj.eventList[j].eventDate = $rootScope.sendApiStartDateTime(tmpObj.eventList[j].eventDate);
                if (tmpObj.eventList[j].followUpRequired == true || tmpObj.eventList[j].followUpRequired == 'Yes') {
                    tmpObj.eventList[j].followUpRequired = 'Yes';
                    tmpObj.eventList[j].followUpDate = $rootScope.sendApiDateAndTime(tmpObj.eventList[j].followUpDate);
                } else {
                    tmpObj.eventList[j].followUpRequired = 'No';
                    tmpObj.eventList[j].followUpDate = null;
                }

                tmpObj.eventList[j].isCompleted = tmpObj.eventList[j].isCompleted ? 'Yes' : 'No';

            }
        }
        if (tmpObj.serviceMaster.importExport == 'Export' && tmpObj.etd != null) {
            tmpObj.consolDocument.documentReqDate = $rootScope.sendApiDateAndTime($rootScope.dateAndTimeToString(tmpObj.etd));
        } else if (tmpObj.serviceMaster.importExport == 'Import' && tmpObj.eta != null) {
            tmpObj.consolDocument.documentReqDate = $rootScope.sendApiDateAndTime($rootScope.dateAndTimeToString(tmpObj.eta));
        } else {
            tmpObj.consolDocument.documentReqDate = $rootScope.sendApiDateAndTime($rootScope.dateAndTimeToString(new Date()));
        }

        return tmpObj;
    }




    $scope.showResponseDO = function(consol, isAdded) {
        var newScope = $scope.$new();
        if ($scope.showDetail) {
            newScope.consolUid = consol;
        } else {
            newScope.consolUid = consol.consolUid;
        }
        newScope.opt = isAdded == true ? 'saved' : 'updated';
        ngDialog.openConfirm({
            template: '<p>ConolShipment  {{consolUid}} successfully {{opt}}.</p> ' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                '</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            var params = {};
            var stateCount = 0;
            var forDo = true;
            if ($stateParams.count != undefined && $stateParams.count != null) {
                stateCount = $stateParams.count + 1;
            }
            $state.go('layout.viewAirConsol', {
                consolId: $scope.consolCloneDO.id,
                forDo: forDo,
                count: stateCount
            });
        }, function(value) {
            $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
        });
        $scope.deliveryModal.$promise.then($scope.deliveryModal.hide);
    }


    // Validation of delivery order
    $scope.validateDeliveryOrder = function(code) {

        $scope.errorMap = new Map();

        if (code == 0 || code == 1) {
            if ($scope.consolCloneDO.consolDocument.carrierDoNo == undefined || $scope.consolCloneDO.consolDocument.carrierDoNo == null || $scope.consolCloneDO.consolDocument.carrierDoNo == "") {
                $scope.errorMap.put("carrierDoNo", $rootScope.nls["ERR95915"]);
                return false;
            }
        }
        if (code == 0 || code == 2) {
            if ($scope.documentDetailForDO.doIssuedDate == undefined || $scope.documentDetailForDO.doIssuedDate == null || $scope.documentDetailForDO.doIssuedDate == "") {
                $scope.errorMap.put("masterDoissedDate", $rootScope.nls["ERR95916"]);
                return false;
            }

            /*  if($scope.consolDocumentCloneDO.doIssuedDate!=undefined && $scope.consolDocumentCloneDO.doIssuedDate!=null && $scope.consolDocumentCloneDO.doIssuedDate!=""){
 			   if(!(moment().isSame(moment($scope.consolDocumentCloneDO.doIssuedDate),'day'))){
 			    $scope.errorMap.put("masterDoissedDate",$rootScope.nls["ERR95912"]);
				      return false;
 			    }
			   }*/
        }

        if (code == 0 || code == 4) {

            /*if($scope.consolDocumentCloneDO.doIssuedDate!=undefined && $scope.consolDocumentCloneDO.doIssuedDate!=null && $scope.consolDocumentCloneDO.doIssuedDate!=""){
			   if(!(moment().isSame(moment($scope.consolDocumentCloneDO.doIssuedDate),'day'))){
			    $scope.errorMap.put("masterDoissedDate",$rootScope.nls["ERR95912"]);
				      return false;
			    }
			   }*/

        }

        if (code == 0 || code == 7) {
            if ($scope.consolCloneDO.consolDocument.igmNo == undefined || $scope.consolCloneDO.consolDocument.igmNo == null || $scope.consolCloneDO.consolDocument.igmNo == "") {
                $scope.errorMap.put("igmNo", $rootScope.nls["ERR95918"]);
                return false;
            }

        }

        if (code == 0 || code == 8) {
            if ($scope.documentDetailForDO.referenceNo == undefined || $scope.documentDetailForDO.referenceNo == null || $scope.documentDetailForDO.referenceNo == "") {
                $scope.errorMap.put("referenceNo", $rootScope.nls["ERR95919"]);
                return false;
            }

        }
        if (code == 0 || code == 9) {
            if ($scope.documentDetailForDO.routeNo == undefined || $scope.documentDetailForDO.routeNo == null || $scope.documentDetailForDO.routeNo == "") {
                $scope.errorMap.put("flight", $rootScope.nls["ERR95920"]);
                return false;
            }
            if ($scope.documentDetailForDO.routeNo == 0) {
                console.log($rootScope.nls["ERR90066"]);
                $scope.errorMap.put("flight", $rootScope.nls["ERR90066"]);
                return false;
            }
            var regexp = new RegExp("^[A-Za-z0-9]{0,10}$");
            if (!regexp.test($scope.documentDetailForDO.routeNo)) {
                console.log($rootScope.nls["ERR90066"]);
                $scope.errorMap.put("flight", $rootScope.nls["ERR90066"]);
                return false;
            }
        }

        if (code == 0 || code == 11) {
            if ($scope.documentDetailForDO.grossWeight == undefined ||
                $scope.documentDetailForDO.grossWeight == null ||
                $scope.documentDetailForDO.grossWeight == "") {
                $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90475"]);
                return false;
            } else {
                if ($scope.documentDetailForDO.grossWeight != null && $scope.documentDetailForDO.grossWeight != undefined && $scope.documentDetailForDO.grossWeight != "")
                    if ($scope.documentDetailForDO.grossWeight <= 0 || $scope.documentDetailForDO.grossWeight > 99999999999.99) {
                        $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90476"]);
                        return false;
                    }
                var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,2})?|\.[0-9]+)$");
                if (!regexp
                    .test($scope.documentDetailForDO.grossWeight)) {
                    $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90476"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }
            }
            if ($scope.errorMap.get('grossWeightKg') != null)
                $scope.errorMap.remove('grossWeightKg');
        }


        if (code == 0) {
            if ($scope.documentDetailForDO.mawbNo == undefined || $scope.documentDetailForDO.mawbNo == null || $scope.documentDetailForDO.mawbNo == '') {
                $rootScope.clientMessage = $rootScope.nls["ERR90569"];
                return false;

            } else {

                if ($scope.consolCloneDO.carrier == undefined || $scope.consolCloneDO.carrier == null || $scope.consolCloneDO.carrier.id == null) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90063"];
                    return false;
                }
                var check = $rootScope.validateMawb($scope.consolCloneDO.carrier, $scope.documentDetailForDO.mawbNo);
                if (check != "S") {
                    if (check != "F") {
                        $rootScope.clientMessage = $rootScope.nls["ERR90068"] + ". Check Digit(" + check + ")";
                    } else {
                        $rootScope.clientMessage = $rootScope.nls["ERR90068"];
                    }

                    return false;
                }

            }
        }
        if (code == 0 || code == 13) {
            if ($scope.documentDetailForDO.noOfPieces == undefined || $scope.documentDetailForDO.noOfPieces == "" || $scope.documentDetailForDO.noOfPieces == null) {
                $scope.errorMap.put("docPieces", $rootScope.nls["ERR96118"]);
                return false;
            } else {
                if ($scope.documentDetailForDO.noOfPieces == 0) {
                    console.log($rootScope.nls["ERR90097"]);
                    $scope.errorMap.put("docPieces", $rootScope.nls["ERR90097"]);
                    return false;
                }
                var regexp = new RegExp("^[0-9]{0,9}$");
                if (!regexp.test($scope.documentDetailForDO.noOfPieces)) {
                    $scope.errorMap.put("docPieces", $rootScope.nls["ERR90097"]);
                    $scope.Tabs = 'documents';
                    return false;

                }
            }
        }

        return true;

    } //validate end


    try {
        var isDoShow = JSON.parse(localStorage.getItem('isDoShow'));
        if (isDoShow != undefined && isDoShow != null) {
            localStorage.removeItem('isDoShow');
            ConsolGet.get({
                    id: isDoShow.consolId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.previewReportDeliveryOrder(data.responseObject, isDoShow.serviceId, isDoShow.shipmentUid, isDoShow.documentId, isDoShow.type, isDoShow.isEdit);
                    }
                },
                function(error) {
                    console.log("Consol get Failed : " + error)
                });
        }
    } catch (e) {
        console.log("exception in DO show")
    }

}]);