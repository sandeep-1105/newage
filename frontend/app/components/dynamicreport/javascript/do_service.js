/**
 * 
 */
app.service('doService', ['updateAttachEvent', function(updateAttachEvent) {
    //after do donwload update attach and event update
    function attachAndEventUpdate(serviceId, dataUrl) {

        var attachEventDto = {};
        var attchEventObj = [];

        if (serviceId != undefined && serviceId != null && serviceId.length > 0) {
            for (var i = 0; i < serviceId.length; i++) {
                attachEventDto.sid = serviceId[i].sId;
                attachEventDto.serviceUid = serviceId[i].serviceUid;
                attachEventDto.documentObj = dataUrl.replace(/^data:image\/[a-z]+;base64,/, "");
                attchEventObj.splice(i, 1, attachEventDto);
            }
            updateAttachEvent.update(attchEventObj).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {

                    console.log("updateAttachEvent.update success called");

                } else {}
            }, function(error) {
                console.log("updateAttachEvent.update error called");

            });

        }

    }

    return {
        attachAndEventUpdate: attachAndEventUpdate
    }

}]);