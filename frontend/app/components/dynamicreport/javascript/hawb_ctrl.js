app.controller('hawbController', ['$rootScope', '$timeout', '$scope', '$http', '$location', 'PartiesList', 'PortByTransportMode',
    'ServiceByTransportMode', 'ChargeSearchKeyword', 'CurrencySearchExclude',
    'ShipmentAsyncSearch', 'ShipmentGetFromUID', 'PortGroupList', 'ngDialog', 'consolService', 'NavigationService',
    '$state', '$stateParams', '$modal', 'ShipmentGetByUid', 'ShipmentUpdate', 'appMetaFactory',
    'ShipmentStatusChange', 'discardService', 'cloneService', 'ShipmentDocumentIdList', 'Notification',
    '$sce', '$window', 'ValidateUtil', 'addressJoiner', 'AutoCompleteService', 'uiShipmentValidationService',
    'uiShipmentDataService', 'roleConstant', 'previewReportService',
    function($rootScope, $timeout, $scope, $http, $location, PartiesList, PortByTransportMode,
        ServiceByTransportMode, ChargeSearchKeyword, CurrencySearchExclude,
        ShipmentAsyncSearch, ShipmentGetFromUID, PortGroupList, ngDialog, consolService, NavigationService,
        $state, $stateParams, $modal, ShipmentGetByUid, ShipmentUpdate, appMetaFactory,
        ShipmentStatusChange, discardService, cloneService, ShipmentDocumentIdList, Notification,
        $sce, $window, ValidateUtil, addressJoiner, AutoCompleteService, uiShipmentValidationService,
        uiShipmentDataService, roleConstant, previewReportService) {

        $scope.disableSubmitBtn = false;
        $scope.rateMergedValue = false;
        $scope.$AutoCompleteService = AutoCompleteService;
        var rateMergedValueHawb = $rootScope.appMasterData['booking.rates.merged'];
        if (rateMergedValueHawb.toLowerCase() == 'true' || rateMergedValueHawb == true) {
            $scope.rateMergedValue = true;
        } else {
            $scope.rateMergedValue = false;
        }
        $scope.navigateToNextField = function(id) {
            if (id != undefined && id != null && id != "")
                document.getElementById(id).focus();
        }
        $scope.diasbleSubmitBtnFn = function() {
            $scope.disableSubmitBtn = true;
            $timeout(function() {
                $scope.disableSubmitBtn = false;
            }, 3000);
        }

        $scope.closeDynamicDocument = function() {
            $scope.errorMap = {};
        }

        // for invoice check we added newly shipmentid
        $scope.previewReport = function(shipment, shipmentUid, documentdetailId, type, serviceid, isEdit, screen) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_HAWB_PREVIEW_HAWB_REPORT_VIEW)) {

                try {
                    $scope.screen = 'Shipment';
                    $scope.isEdit = isEdit;
                    if ($scope.isEdit) {
                        if (!$scope.validateServices()) {
                            return;
                        }
                    }
                    $scope.diasbleSubmitBtnFn();
                    $rootScope.mainpreloder = true;
                    $scope.serviceId = [];
                    $scope.servId = serviceid;
                    // STEP-1---Invoice Validation
                    if (serviceid != undefined && serviceid != null) {
                        $scope.serviceId.push(serviceid);
                        appMetaFactory.reportInvoiceValidation.findByServiceId({
                            serviceId: $scope.serviceId,
                        }).$promise.then(function(
                            data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.errorMap = new Map();
                                //view page
                                if (!$scope.isEdit) {
                                    ShipmentGetByUid.get({
                                            shipmentUid: shipmentUid
                                        }, function(data) {
                                            if (data.responseCode == 'ERR0') {
                                                $scope.shipment = data.responseObject;
                                                $scope.shipmentClone = cloneService.clone($scope.shipment);
                                                $scope.previewReportHAWB(shipmentUid, documentdetailId, $scope.servId);
                                            } else {
                                                Notification.error(data);
                                                console.log("Error Exists while retriveing shipment");
                                                $rootScope.mainpreloder = false;
                                            }
                                        },
                                        function(error) {
                                            console.log("Shipment get Failed : " + error)
                                            $rootScope.mainpreloder = false;
                                        });
                                } else {
                                    $scope.shipmentClone = cloneService.clone(shipment);
                                    $scope.previewReportHAWB(shipmentUid, documentdetailId, $scope.servId);
                                }
                            } else if (data.responseCode == "ERR0149") {
                                $rootScope.mainpreloder = false;
                                return false;
                            } else {
                                console.log("unknown response");
                                $rootScope.mainpreloder = false;
                            }
                        }, function(error) {
                            console.log("error while fetching service id");
                            $rootScope.mainpreloder = false;
                            return false;
                        });
                    } else {
                        $rootScope.mainpreloder = false;
                    }

                } catch (e) {
                    $rootScope.mainpreloder = false;
                    $scope.spinner = false;
                    console.log(e);
                }
            }
        }; // end of preview report


        $scope.previewReportHAWB = function(shipmentUid, documentdetailId, svid) {

                for (var i = 0; i < $scope.shipmentClone.shipmentServiceList.length; i++) {
                    if ($scope.shipmentClone.shipmentServiceList[i].id == svid) {
                        $scope.shipmentServiceDetailClone = $scope.shipmentClone.shipmentServiceList[i];
                        for (var j = 0; j < $scope.shipmentServiceDetailClone.documentList.length; j++) {
                            if ($scope.shipmentServiceDetailClone.documentList[j].id == documentdetailId)
                                $scope.documentDetailClone = $scope.shipmentServiceDetailClone.documentList[j];
                        }
                        break;
                    }
                }


                $scope.isOnlyConsoleConnection = false;
                if ($scope.shipmentServiceDetailClone.connectionList.length > 0) {
                    $scope.isOnlyConsoleConnection = false;
                } else {
                    $scope.isOnlyConsoleConnection = true;
                }

                var extraConnList = new Array();
                for (var i = 0; i < $scope.shipmentServiceDetailClone.connectionList.length; i++) {
                    if (i < 2) {
                        continue;
                    } else {
                        extraConnList.push(" " + $scope.shipmentServiceDetailClone.connectionList[i].flightVoyageNo + "|" +
                            $rootScope.dateToString($scope.shipmentServiceDetailClone.connectionList[i].etd) + "|" +
                            $scope.shipmentServiceDetailClone.connectionList[i].podCode + "|" +
                            $scope.shipmentServiceDetailClone.connectionList[i].carrierCode);
                    }
                }

                if ($scope.shipmentServiceDetailClone.connectionList.length > 2) {
                    $scope.documentDetailClone.handlingInformation = extraConnList.join();
                }




                // STEP-2---Checking is service is received and generated(based on status)
                if ($scope.shipmentServiceDetailClone.lastUpdatedStatus.serviceStatus != "Received" && $scope.shipmentServiceDetailClone.lastUpdatedStatus.serviceStatus != "Generated") {
                    Notification.error($rootScope.nls["ERR90568"]);
                    $rootScope.mainpreloder = false;
                    return false;
                }

                if (!$rootScope.hawbRated_Unrated) {
                    $scope.shipmentServiceDetailClone.isAgreed = true;
                } else {
                    $scope.shipmentServiceDetailClone.isAgreed = false;
                }

                //default in preview report showing 6 charges
                if ($scope.shipmentServiceDetailClone.isAgreed) {
                    $scope.shipmentServiceDetailClone.shipmentChargeListClone = [];
                    $scope.shipmentServiceDetailClone.shipmentChargeListClone = new Array(6);
                }

                $scope.documentDetailClone.hawbReceivedOn = $rootScope.dateToString(new Date());
                $scope.documentDetailClone.hawbReceivedBy = $rootScope.userProfile.employee;

                if ($scope.shipmentServiceDetailClone.tosMaster != null) {

                    $scope.documentDetailClone.chgs = $scope.shipmentServiceDetailClone.tosMaster.freightPPCC.charAt(0);

                    if ($scope.documentDetailClone.chgs == 'P') {
                        $scope.documentDetailClone.wtValPrepaid = $scope.shipmentServiceDetailClone.tosMaster.freightPPCC.charAt(0);
                        $scope.documentDetailClone.othersPrepaid = $scope.shipmentServiceDetailClone.tosMaster.freightPPCC.charAt(0);
                        $scope.documentDetailClone.wtValCollect = null;
                        $scope.documentDetailClone.othersCollect = null;
                    } else {
                        $scope.documentDetailClone.wtValPrepaid = null;
                        $scope.documentDetailClone.othersPrepaid = null;
                        $scope.documentDetailClone.wtValCollect = $scope.shipmentServiceDetailClone.tosMaster.freightPPCC.charAt(0);
                        $scope.documentDetailClone.othersCollect = $scope.shipmentServiceDetailClone.tosMaster.freightPPCC.charAt(0);
                    }
                }

                if ($scope.shipmentServiceDetailClone.isAgreed) {
                    $scope.documentDetailClone.totalAmountClone = "AS AGREED"; // Total AMount
                    $scope.documentDetailClone.ratePerChargeClone = null;

                    if ($scope.documentDetailClone.chgs == 'P') {
                        $scope.documentDetailClone.preWeightChargeClone = "AS AGREED";
                        $scope.documentDetailClone.totalPrepaidClone = "AS AGREED";
                    }
                    if ($scope.documentDetailClone.chgs == 'C') {
                        $scope.documentDetailClone.collWeightChargeClone = "AS AGREED";
                        $scope.documentDetailClone.totalCollectClone = "AS AGREED";
                    }
                }


                //ACCOUNT AND NOTIFY

                $scope.documentDetailClone.accountandNotify = ""
                if ($scope.documentDetailClone.accoutingInformation != undefined && $scope.documentDetailClone.accoutingInformation != null) {
                    $scope.documentDetailClone.accountandNotify = $scope.documentDetailClone.accoutingInformation
                }

                if ($scope.documentDetailClone.firstNotify != undefined && $scope.documentDetailClone.firstNotify != null) {

                    $scope.documentDetailClone.accountandNotify = $scope.documentDetailClone.accountandNotify + '\n' +

                        $scope.documentDetailClone.firstNotify.partyName + '\n' + $scope.documentDetailClone.firstNotify.partyAddressList[0].addressLine1 + '\n' +

                        $scope.documentDetailClone.firstNotify.partyAddressList[0].addressLine2 + '\n' + ""

                    if ($scope.documentDetailClone.firstNotify.partyAddressList[0].addressLine3 != undefined && $scope.documentDetailClone.firstNotify.partyAddressList[0].addressLine3 != null)
                        $scope.documentDetailClone.accountandNotify = $scope.documentDetailClone.accountandNotify + $scope.documentDetailClone.firstNotify.partyAddressList[0].addressLine3 + '\n' + $scope.getPhoneandFax($scope.documentDetailClone.firstNotify);
                }

                //set aes
                if ($scope.shipmentServiceDetailClone.aesNo != undefined && $scope.shipmentServiceDetailClone.aesNo != null && $scope.shipmentServiceDetailClone.aesNo != "") {
                    $scope.documentDetailClone.accountandNotify = $scope.documentDetailClone.accountandNotify + '\n' + "AES No:" + $scope.shipmentServiceDetailClone.aesNo;
                }

                $scope.dimensionLabel = " PCS - L  X  W  X  H" + (($scope.documentDetailClone.dimensionUnit == "Inch/Pounds" || $scope.documentDetailClone.dimensionUnit == true) ? '(inch)' : '(cms)');

                $scope.documentDetailClone.dimensionClone = "";
                if ($scope.documentDetailClone.commodityDescription != undefined && $scope.documentDetailClone.commodityDescription != null)
                    $scope.documentDetailClone.dimensionClone = $scope.documentDetailClone.commodityDescription;

                if ($scope.documentDetailClone != undefined && $scope.documentDetailClone != null && $scope.documentDetailClone.dimensionList.length > 0) {
                    $scope.documentDetailClone.dimensionClone = $scope.documentDetailClone.dimensionClone + '\n' + $scope.dimensionLabel;
                    if (!isEmptyRow($scope.documentDetailClone.dimensionList[0])) {
                        for (var i = 0; i < $scope.documentDetailClone.dimensionList.length; i++) {
                            $scope.documentDetailClone.dimensionClone = $scope.documentDetailClone.dimensionClone + '\n' + "  " +
                                $scope.documentDetailClone.dimensionList[i].noOfPiece + " " + "-" + ' ' + $scope.documentDetailClone.dimensionList[i].length + ' ' + "X" + ' ' + $scope.documentDetailClone.dimensionList[i].width + ' ' + "X" +
                                '  ' + $scope.documentDetailClone.dimensionList[i].height + '\n' + " ";
                        }
                    } else {
                        $scope.documentDetailClone.dimensionClone = null;
                    }
                }
                //clone connection List
                $scope.cloneConnectionList = {};
                if ($scope.shipmentServiceDetailClone.routeNo != null && $scope.shipmentServiceDetailClone.routeNo != "" && $scope.shipmentServiceDetailClone.etd != null && $scope.shipmentServiceDetailClone.etd != "") {
                    var etdDate = $rootScope.dateToString($scope.shipmentServiceDetailClone.etd);
                    $scope.shipmentServiceDetailClone.flightanddate = $scope.shipmentServiceDetailClone.routeNo + "/" + etdDate;
                }
                if ($scope.shipmentServiceDetailClone.connectionList != null && $scope.shipmentServiceDetailClone.connectionList.length > 0) {
                    var length = $scope.shipmentServiceDetailClone.connectionList.length;

                    $scope.cloneConnectionList.pod = $scope.shipmentServiceDetailClone.connectionList[0].pod;
                    $scope.cloneConnectionList.carrierMaster = $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster;

                    if ($scope.shipmentServiceDetailClone.connectionList.length > 1) {
                        if ($scope.shipmentServiceDetailClone.connectionList[1] != undefined && $scope.shipmentServiceDetailClone.connectionList[1].pod != undefined && $scope.shipmentServiceDetailClone.connectionList[1].pod.id != undefined)
                            $scope.cloneConnectionList.pod1 = $scope.shipmentServiceDetailClone.connectionList[1].pod;

                        if ($scope.shipmentServiceDetailClone.connectionList[1] != undefined && $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster != undefined && $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster.id != undefined)
                            $scope.cloneConnectionList.carrierMaster1 = $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster;
                    }
                    if ($scope.shipmentServiceDetailClone.connectionList[0] != undefined && $scope.shipmentServiceDetailClone.connectionList[0].flightVoyageNo != undefined) {
                        var etdDate = $rootScope.dateToString($scope.shipmentServiceDetailClone.connectionList[0].etd);;
                        $scope.cloneConnectionList.flightanddate0 = $scope.shipmentServiceDetailClone.connectionList[0].flightVoyageNo + "/" + etdDate;
                    }

                    if ($scope.shipmentServiceDetailClone.connectionList[1] != undefined && $scope.shipmentServiceDetailClone.connectionList[1].flightVoyageNo != undefined) {
                        var etdDate = $rootScope.dateToString($scope.shipmentServiceDetailClone.connectionList[1].etd);;
                        $scope.cloneConnectionList.flightanddate = $scope.shipmentServiceDetailClone.connectionList[1].flightVoyageNo + "/" + etdDate;
                    }

                }

                if ($scope.shipmentServiceDetailClone.mawbNo != null && $scope.shipmentServiceDetailClone.mawbNo != undefined) {
                    $scope.documentDetailClone.mawbNo = $scope.shipmentServiceDetailClone.mawbNo;
                }
                if ($scope.documentDetailClone.mawbNo != undefined && $scope.documentDetailClone.mawbNo != null && $scope.documentDetailClone.mawbNo != "") {
                    var MawbNo = $scope.documentDetailClone.mawbNo.match(/(.{1,3})/g);
                    var mawbwithoutPrefix = MawbNo[1] + MawbNo[2] + MawbNo[3]; // having the values of after slicing the carrier num    
                    mawbwithoutPrefix = mawbwithoutPrefix.match(/(.{1,4})/g); //having the vli
                    $scope.documentDetailClone.mawbNowithoutPrefix1 = mawbwithoutPrefix[0] + " " + mawbwithoutPrefix[1];
                    $scope.documentDetailClone.mawbNowithoutPrefix2 = MawbNo[0] + "|" + mawbwithoutPrefix[0] + " " + mawbwithoutPrefix[1];
                }

                $scope.hawbModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/dynamicreport/view/awb.html',
                    backdrop: 'static',
                    show: false
                });

                $scope.docType = 'HAWB';
                $scope.isError = false;
                $scope.agreedChange();
                $scope.getAccounts(0);

                if ($scope.documentDetailClone.issCarrierAgentSign == undefined || $scope.documentDetailClone.issCarrierAgentSign == null || $scope.documentDetailClone.issCarrierAgentSign == "") {
                    $scope.documentDetailClone.issCarrierAgentSign = $rootScope.userProfile.employee.employeeName;
                }
                if ($scope.documentDetailClone.shipperSignature == undefined || $scope.documentDetailClone.shipperSignature == null || $scope.documentDetailClone.shipperSignature == "") {
                    $scope.documentDetailClone.shipperSignature = $rootScope.userProfile.employee.employeeName;
                }
                $scope.hawbModal.$promise.then($scope.hawbModal.show);
                $timeout(function() {
                    $scope.isError = true;
                }, 500);

                $rootScope.mainpreloder = false;

            } //end of preview report



        $scope.choosePartyAddress = function(addressKey) {
            if (addressKey == 'shipper') {
                if ($scope.documentDetailClone.shipper != undefined && $scope.documentDetailClone.shipper != null && $scope.documentDetailClone.shipper.status != 'Block' && ($scope.documentDetailClone.shipperAddress == undefined || $scope.documentDetailClone.shipperAddress == null)) {
                    $scope.documentDetailClone.shipperAddress = {};
                    $scope.getPartyAddress($scope.documentDetailClone.shipper, $scope.documentDetailClone.shipperAddress);
                } else if ($scope.documentDetailClone.shipper != undefined && $scope.documentDetailClone.shipper != null && $scope.documentDetailClone.shipper != null && $scope.documentDetailClone.shipper.status != 'Block') {
                    $scope.getPartyAddress($scope.documentDetailClone.shipper, $scope.documentDetailClone.shipperAddress);
                }
            }

            if (addressKey == 'consignee') {
                if ($scope.documentDetailClone.consignee != undefined && $scope.documentDetailClone.consignee != null && $scope.documentDetailClone.consignee.status != 'Block' && ($scope.documentDetailClone.consigneeAddress == undefined || $scope.documentDetailClone.consigneeAddress == null)) {
                    $scope.documentDetailClone.consigneeAddress = {};
                    $scope.getPartyAddress($scope.documentDetailClone.consignee, $scope.documentDetailClone.consigneeAddress);
                } else if ($scope.documentDetailClone.consignee != undefined && $scope.documentDetailClone.consignee != null && $scope.documentDetailClone.consignee.status != 'Block') {
                    $scope.getPartyAddress($scope.documentDetailClone.consignee, $scope.documentDetailClone.consigneeAddress);
                }
            }

            if (addressKey == 'issuingAgent') {
                if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent.status != 'Block' && ($scope.documentDetailClone.issuingAgentAddress == undefined || $scope.documentDetailClone.issuingAgentAddress == null)) {
                    $scope.documentDetailClone.issuingAgentAddress = {};
                    $scope.getPartyAddress($scope.documentDetailClone.issuingAgent, $scope.documentDetailClone.issuingAgentAddress);
                } else if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent.status != 'Block') {
                    $scope.getPartyAddress($scope.documentDetailClone.issuingAgent, $scope.documentDetailClone.issuingAgentAddress);
                }
            }



            if (addressKey == 'firstNotify') {
                if ($scope.documentDetailClone.firstNotify != undefined && $scope.documentDetailClone.firstNotify != null && $scope.documentDetailClone.firstNotify.status != 'Block' && ($scope.documentDetailClone.firstNotifyAddress == undefined || $scope.documentDetailClone.firstNotifyAddress == null)) {
                    $scope.documentDetailClone.firstNotifyAddress = {};
                    $scope.getPartyAddress($scope.documentDetailClone.firstNotify, $scope.documentDetail.firstNotifyAddress);
                } else if ($scope.documentDetailClone.firstNotify != undefined && $scope.documentDetailClone.firstNotify != null && $scope.documentDetailClone.firstNotify.status != 'Block') {
                    $scope.getPartyAddress($scope.documentDetailClone.firstNotify, $scope.documentDetail.firstNotifyAddress);
                }
            }

            if (addressKey == 'secondNotify') {
                if ($scope.documentDetailClone.secondNotify != undefined && $scope.documentDetailClone.secondNotify != null && $scope.documentDetailClone.secondNotify.status != 'Block' && ($scope.documentDetailClone.secondNotifyAddress == undefined || $scope.documentDetailClone.secondNotifyAddress == null)) {
                    $scope.documentDetailClone.secondNotifyAddress = {};
                    $scope.getPartyAddress($scope.documentDetailClone.secondNotify, $scope.documentDetailClone.secondNotifyAddress);
                } else if ($scope.documentDetailClone.secondNotify != undefined && $scope.documentDetailClone.secondNotify != null && $scope.documentDetailClone.secondNotify.status != 'Block') {
                    $scope.getPartyAddress($scope.documentDetailClone.secondNotify, $scope.documentDetailClone.secondNotifyAddress);
                }
            }
        }




        //get party and address for the concern party
        $scope.getPartyAddress = function(obj, mapToAddrss) {
            if (obj != null && obj.id != null) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss, obj, obj.partyAddressList[i]) + $scope.getPhoneandFax(obj);
                    }
                }
            }
        }

        //get phone and Fax for particular party
        $scope.getPhoneandFax = function(partyObj) {
            $scope.partyPhandFax = "";
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].phone != undefined && partyObj.partyAddressList[i].phone != null) {
                    if ($scope.partyPhandFax.includes("PH")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].phone;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "  " + "PH:" + partyObj.partyAddressList[i].phone;
                    }
                }
            }
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].fax != undefined && partyObj.partyAddressList[i].fax != null) {
                    if ($scope.partyPhandFax.includes("FAX")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].fax;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "  " + "FAX:" + partyObj.partyAddressList[i].fax;

                    }
                }

            }
            return $scope.partyPhandFax;
        }



        $scope.agreedChange = function() {
                if ($scope.shipmentServiceDetailClone.isAgreed || $scope.shipmentServiceDetailClone.isAgreed == 'Yes') {

                    $scope.documentDetailClone.ratePerChargeClone = null;
                    $scope.documentDetailClone.otherChargesDuePreAgentClone = null;
                    $scope.documentDetailClone.otherChargesDueCollAgentClone = null;
                    $scope.documentDetailClone.otherChargesDuePreCarrierClone = null;
                    $scope.documentDetailClone.otherChargesDueCollCarrierClone = null;
                    $scope.documentDetailClone.preValuationChargeClone = null;
                    $scope.documentDetailClone.collValuationChargeClone = null;
                    $scope.documentDetailClone.collectTaxClone = null;
                    $scope.documentDetailClone.prepaidTaxClone = null;
                    $scope.documentDetailClone.totalAmountClone = "AS AGREED";
                    $scope.shipmentServiceDetailClone.shipmentChargeListClone = [];
                    $scope.shipmentServiceDetailClone.shipmentChargeListClone = new Array(6);
                    if ($scope.documentDetailClone.chgs == 'P') {
                        $scope.documentDetailClone.preWeightChargeClone = "AS AGREED";
                        $scope.documentDetailClone.totalPrepaidClone = "AS AGREED";
                        $scope.documentDetailClone.collWeightChargeClone = null;
                        $scope.documentDetailClone.totalCollectClone = null;
                    }

                    if ($scope.documentDetailClone.chgs == 'C') {
                        $scope.documentDetailClone.collWeightChargeClone = "AS AGREED";
                        $scope.documentDetailClone.totalCollectClone = "AS AGREED";
                        $scope.documentDetailClone.preWeightChargeClone = null;
                        $scope.documentDetailClone.totalPrepaidClone = null;
                    }

                } else {
                    $scope.documentDetailClone.ratePerChargeClone = $scope.documentDetailClone.ratePerCharge;
                    $scope.documentDetailClone.preWeightChargeClone = null;
                    $scope.documentDetailClone.collWeightChargeClone = null;
                    $scope.documentDetailClone.otherChargesDuePreAgentClone = 0.0;
                    $scope.documentDetailClone.otherChargesDueCollAgentClone = 0.0;
                    $scope.documentDetailClone.otherChargesDuePreCarrierClone = 0.0;
                    $scope.documentDetailClone.otherChargesDueCollCarrierClone = 0.0;
                    if ($scope.shipmentServiceDetailClone.shipmentChargeList != null && $scope.shipmentServiceDetailClone.shipmentChargeList.length > 0) {
                        if (!isEmptyRow($scope.shipmentServiceDetailClone.shipmentChargeList[0])) {
                            for (var c = 0; c < $scope.shipmentServiceDetailClone.shipmentChargeList.length; c++) {
                                if ($scope.shipmentServiceDetailClone.shipmentChargeList[c].ppcc != undefined && $scope.shipmentServiceDetailClone.shipmentChargeList[c].ppcc == 'Prepaid' || $scope.shipmentServiceDetailClone.shipmentChargeList[c].ppcc == false) {
                                    if ($scope.shipmentServiceDetailClone.shipmentChargeList[c].due != undefined && $scope.shipmentServiceDetailClone.shipmentChargeList[c].due == 'Agent') {
                                        if ($scope.rateMergedValue || $scope.rateMergedValue == 'true') {
                                            $scope.documentDetailClone.otherChargesDuePreAgentClone = parseFloat($scope.documentDetailClone.otherChargesDuePreAgentClone) +
                                                parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalRateAmount);
                                        } else {
                                            $scope.documentDetailClone.otherChargesDuePreAgentClone = parseFloat($scope.documentDetailClone.otherChargesDuePreAgentClone) +
                                                parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalNetSale);
                                        }

                                    } else {
                                        if ($scope.shipmentServiceDetailClone.shipmentChargeList[c].due != undefined) {
                                            if ($scope.rateMergedValue || $scope.rateMergedValue == 'true') {
                                                $scope.documentDetailClone.otherChargesDuePreCarrierClone = parseFloat($scope.documentDetailClone.otherChargesDuePreCarrierClone) +
                                                    parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalRateAmount);
                                            } else {
                                                $scope.documentDetailClone.otherChargesDuePreCarrierClone = parseFloat($scope.documentDetailClone.otherChargesDuePreCarrierClone) +
                                                    parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalNetSale);
                                            }
                                        }

                                    }
                                } else if ($scope.shipmentServiceDetailClone.shipmentChargeList[c].ppcc != undefined && $scope.shipmentServiceDetailClone.shipmentChargeList[c].ppcc == 'Collect' || $scope.shipmentServiceDetailClone.shipmentChargeList[c].ppcc == false) {
                                    if ($scope.shipmentServiceDetailClone.shipmentChargeList[c].due != undefined && $scope.shipmentServiceDetailClone.shipmentChargeList[c].due == 'Agent') {
                                        if ($scope.rateMergedValue || $scope.rateMergedValue == 'true') {
                                            $scope.documentDetailClone.otherChargesDueCollAgentClone = parseFloat($scope.documentDetailClone.otherChargesDueCollAgentClone) +
                                                parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalRateAmount);
                                        } else {
                                            $scope.documentDetailClone.otherChargesDueCollAgentClone = parseFloat($scope.documentDetailClone.otherChargesDueCollAgentClone) +
                                                parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalNetSale);
                                        }
                                    } else {
                                        if ($scope.shipmentServiceDetailClone.shipmentChargeList[c].due != undefined) {
                                            if ($scope.rateMergedValue || $scope.rateMergedValue == 'true') {
                                                $scope.documentDetailClone.otherChargesDueCollCarrierClone = parseFloat($scope.documentDetailClone.otherChargesDueCollCarrierClone) +
                                                    parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalRateAmount);
                                            } else {
                                                $scope.documentDetailClone.otherChargesDueCollCarrierClone = parseFloat($scope.documentDetailClone.otherChargesDueCollCarrierClone) +
                                                    parseFloat($scope.shipmentServiceDetailClone.shipmentChargeList[c].localTotalNetSale);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $scope.calculateTotalAmount();
                    $scope.shipmentServiceDetailClone.shipmentChargeListClone = $scope.shipmentServiceDetailClone.shipmentChargeList;
                    $scope.documentDetailClone.otherChargesDuePreAgentClone = $scope.isZero($scope.documentDetailClone.otherChargesDuePreAgentClone);
                    $scope.documentDetailClone.otherChargesDueCollAgentClone = $scope.isZero($scope.documentDetailClone.otherChargesDueCollAgentClone);
                    $scope.documentDetailClone.otherChargesDuePreCarrierClone = $scope.isZero($scope.documentDetailClone.otherChargesDuePreCarrierClone);
                    $scope.documentDetailClone.otherChargesDueCollCarrierClone = $scope.isZero($scope.documentDetailClone.otherChargesDueCollCarrierClone);
                }

                if ($scope.shipmentServiceDetailClone.isMarksNo == false || $scope.shipmentServiceDetailClone.isMarksNo == 'No') {
                    $scope.documentDetailClone.marksAndNoClone = null;
                } else {
                    $scope.documentDetailClone.marksAndNoClone = $scope.documentDetailClone.marksAndNo;
                }


            } //agreed change complete


        $scope.calculateTotalAmount = function() {
            if (($scope.shipmentServiceDetailClone.isAgreed == "No" || $scope.shipmentServiceDetailClone.isAgreed == false) && $scope.documentDetailClone.ratePerChargeClone != undefined && $scope.documentDetailClone.ratePerChargeClone != null && $scope.documentDetailClone.chargebleWeight != undefined && $scope.documentDetailClone.chargebleWeight != null) {
                $scope.documentDetailClone.totalAmountClone = parseFloat($scope.documentDetailClone.ratePerChargeClone) * parseFloat($scope.documentDetailClone.chargebleWeight);
                $scope.documentDetailClone.totalAmountClone = $rootScope.currency($scope.shipmentServiceDetailClone.localCurrency, parseFloat($scope.documentDetailClone.totalAmountClone));
            }
            if (($scope.shipmentServiceDetailClone.isAgreed == false || $scope.shipmentServiceDetailClone.isAgreed == "No") && $scope.documentDetailClone.ratePerChargeClone == null) {
                $scope.documentDetailClone.totalAmountClone = null;
            }
            if ($scope.documentDetailClone.chgs == 'P') {
                $scope.documentDetailClone.preWeightChargeClone = $scope.documentDetailClone.totalAmountClone;
            } else {
                $scope.documentDetailClone.collWeightChargeClone = $scope.documentDetailClone.totalAmountClone;
            }
            $scope.caluclatePrepaid();
            $scope.caluclateCollect();
        }


        $scope.caluclatePrepaid = function() {

            if ($scope.shipmentServiceDetailClone.isAgreed == 'No' || $scope.shipmentServiceDetailClone.isAgreed == false) {
                $scope.documentDetailClone.totalPrepaidClone = parseFloat($scope.documentDetailClone.preWeightChargeClone == null ? 0 : $scope.documentDetailClone.preWeightChargeClone) +
                    parseFloat($scope.documentDetailClone.preValuationCharge == null ? 0 : $scope.documentDetailClone.preValuationCharge) +
                    parseFloat($scope.documentDetailClone.prepaidTax == null ? 0 : $scope.documentDetailClone.prepaidTax) +
                    parseFloat($scope.documentDetailClone.otherChargesDuePreAgentClone == null ? 0 : $scope.documentDetailClone.otherChargesDuePreAgentClone) +
                    parseFloat($scope.documentDetailClone.otherChargesDuePreCarrierClone == null ? 0 : $scope.documentDetailClone.otherChargesDuePreCarrierClone)
            }

            if ($scope.documentDetailClone.totalPrepaidClone == 0) {
                $scope.documentDetailClone.totalPrepaidClone = null;
            }
        }

        $scope.caluclateCollect = function() {
            if ($scope.shipmentServiceDetailClone.isAgreed == 'No' || $scope.shipmentServiceDetailClone.isAgreed == false) {
                $scope.documentDetailClone.totalCollectClone = parseFloat($scope.documentDetailClone.collWeightChargeClone == null ? 0 : $scope.documentDetailClone.collWeightChargeClone) +
                    parseFloat($scope.documentDetailClone.collValuationCharge == null ? 0 : $scope.documentDetailClone.collValuationCharge) +
                    parseFloat($scope.documentDetailClone.collectTax == null ? 0 : $scope.documentDetailClone.collectTax) +
                    parseFloat($scope.documentDetailClone.otherChargesDueCollAgentClone == null ? 0 : $scope.documentDetailClone.otherChargesDueCollAgentClone) +
                    parseFloat($scope.documentDetailClone.otherChargesDueCollCarrierClone == null ? 0 : $scope.documentDetailClone.otherChargesDueCollCarrierClone)
            }
            if ($scope.documentDetailClone.totalCollectClone == 0) {

                $scope.documentDetailClone.totalCollectClone = null;
            }
        }

        //Accounts code for Parties
        $scope.getAccounts = function(code) {

            if (code == 0 || code == 1) {

                $scope.documentDetailClone.shipperAccount = null;
                if ($scope.documentDetailClone.shipper != undefined && $scope.documentDetailClone.shipper != null && $scope.documentDetailClone.shipper.partyAccountList != null && $scope.documentDetailClone.shipper.partyAccountList.length > 0) {

                    $scope.documentDetailClone.shipperAccount = cloneService.clone($scope.documentDetailClone.shipper.partyAccountList[0].accountMaster.accountCode);
                } else {
                    $scope.documentDetailClone.shipperAccount = null;
                }

            }

            if (code == 0 || code == 2) {

                $scope.documentDetailClone.consigneeAccount = null;
                if ($scope.documentDetailClone.consignee != undefined && $scope.documentDetailClone.consignee != null && $scope.documentDetailClone.consignee.partyAccountList != null && $scope.documentDetailClone.consignee.partyAccountList.length > 0) {

                    $scope.documentDetailClone.consigneeAccount = $scope.documentDetailClone.consignee.partyAccountList[0].accountMaster.accountCode;

                } else {

                    $scope.documentDetailClone.consigneeAccount = null;
                }
            }

            if (code == 0 || code == 3) {

                $scope.documentDetailClone.issuingAgentAccount = null;
                if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent.partyAccountList != null && $scope.documentDetailClone.issuingAgent.partyAccountList.length > 0) {

                    $scope.documentDetailClone.issuingAgentAccount = $scope.documentDetailClone.issuingAgent.partyAccountList[0].accountMaster.accountCode;

                } else {

                    $scope.documentDetailClone.issuingAgentAccount = null;
                }

            }
        }

        $scope.isZero = function(obj) {

            if (obj == 0 || parseFloat(obj) == 0) {
                obj = null;
            }
            return obj;
        }



        function isEmptyRow(obj) {
            //return (Object.getOwnPropertyNames(obj).length === 0);
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }

            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;

                }
            }
            return isempty;
        }


        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                if ($scope.shipmentServiceDetailClone.shipmentChargeListClone != undefined && $scope.shipmentServiceDetailClone.shipmentChargeListClone[i] != undefined) {
                    input.push($scope.shipmentServiceDetailClone.shipmentChargeListClone[i]);
                }
            }
            return input;
        };

        //Renders

        $scope.groupColDestination = [{
            name: 'Port Code',
            width: '150px',
            valueField: 'portCode'
        }, {
            name: 'Port Name',
            width: '120px',
            valueField: 'portName'
        }];


        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }
        $scope.serviceNameRender = function(item) {
            return {
                label: item.serviceName,
                item: item
            }

        }
        $scope.PortNameRender = function(item) {
            return {
                label: item.portName,
                item: item
            }

        }
        $scope.carrierNameRender = function(item) {
            return {
                label: item.carrierName,
                item: item
            }

        }

        $scope.carrierCodeRender = function(item) {
            return {
                label: item.carrierCode,
                item: item
            }

        }

        $scope.currencyRender = function(item) {
            return {
                label: item.currencyCode,
                item: item
            }
        }

        $scope.chargeRender = function(item) {
            return {
                label: item.chargeCode,
                item: item
            }
        }

        $scope.PortCodeRender = function(item) {
            return {
                label: item.portCode,
                item: item
            }

        }



        //validation for DYnamic Document

        $scope.validateDynamicDocument = function(validateCode, nextId) {

                if ($scope.errorMap != undefined) {
                    $scope.errorMap = new Map();

                }
                if (validateCode == 0 || validateCode == 1) {

                    if ($scope.documentDetailClone.carrier != undefined && $scope.documentDetailClone.carrier != null) {

                        if ($scope.documentDetailClone.carrier.transportMode != "Air") {

                            if ($scope.documentDetailClone.carrier.carrierNo != null) {
                                var regexp = new RegExp("^[0-9A-Z ]{0,10}$");
                                if (!regexp
                                    .test($scope.documentDetailClone.carrier.carrierNo)) {
                                    $scope.errorMap.put("carrierNo", $rootScope.nls["ERR1213"]);
                                    return false;
                                }
                            }
                        } else {
                            if ($scope.documentDetailClone.carrier.carrierNo == "undefined" || $scope.documentDetailClone.carrier.carrierNo == null || $scope.documentDetailClone.carrier.carrierNo == "") {
                                $scope.errorMap.put("carrierNo", $rootScope.nls["ERR1214"]);
                                return false;
                            } else {
                                var regexp = new RegExp("^[0-9]{3}$");
                                if (!regexp
                                    .test($scope.documentDetailClone.carrier.carrierNo)) {
                                    $scope.errorMap.put("carrierNo", $rootScope.nls["ERR1234"]);
                                    return false;
                                }
                            }
                        }
                    }

                    if ($scope.errorMap.get('carrierNo') != null)
                        $scope.errorMap.remove('carrierNo');
                }



                if (validateCode == 0 || validateCode == 2) {

                    if ($scope.documentDetailClone.pol == null || $scope.documentDetailClone.pol.id == null || $scope.documentDetailClone.pol.id.trim == "") {
                        console.log($rootScope.nls["ERR90041"]);
                        $scope.errorMap.put("pol", $rootScope.nls["ERR90041"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.pol.status)) {
                        $scope.documentDetailClone.pol = null;
                        $scope.errorMap.put("pol", $rootScope.nls["ERR90042"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden($scope.documentDetailClone.pol.status)) {
                        $scope.documentDetailClone.pol = null;
                        $scope.errorMap.put("pol", $rootScope.nls["ERR90043"]);
                        return false;
                    }
                    if ($scope.errorMap.get('pol') != null)
                        $scope.errorMap.remove('pol');
                    $scope.navigateToNextField(nextId);
                }
                if (validateCode == 0 || validateCode == 3) {
                    if ($scope.documentDetailClone.shipper == undefined || $scope.documentDetailClone.shipper == null || $scope.documentDetailClone.shipper == "" || $scope.documentDetailClone.shipper.id == null) {
                        $scope.documentDetailClone.shipperAddress.addressLine1 = null;
                        $scope.documentDetailClone.shipperAddress.addressLine2 = null;
                        $scope.documentDetailClone.shipperAddress.addressLine3 = null;
                        $scope.documentDetailClone.shipperAddress.addressLine4 = null;
                        $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90106"]);
                        $scope.showPartyAddressList = false;
                        return false;
                    } else {
                        if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.shipper.status)) {
                            $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90107"]);
                            $scope.documentDetailClone.shipper = null;
                            $scope.documentDetailClone.shipperAddress.addressLine1 = null;
                            $scope.documentDetailClone.shipperAddress.addressLine2 = null;
                            $scope.documentDetailClone.shipperAddress.addressLine3 = null;
                            $scope.documentDetailClone.shipperAddress.addressLine4 = null;
                            return false
                        }
                        if (ValidateUtil.isStatusHidden($scope.documentDetailClone.shipper.status)) {
                            $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90108"]);
                            $scope.documentDetailClone.shipper = {};
                            $scope.documentDetailClone.shipperAddress.addressLine1 = null;
                            $scope.documentDetailClone.shipperAddress.addressLine2 = null;
                            $scope.documentDetailClone.shipperAddress.addressLine3 = null;
                            $scope.documentDetailClone.shipperAddress.addressLine4 = null;
                            return false
                        }
                    }
                    if ($scope.errorMap.get('documentShipper') != null)
                        $scope.errorMap.remove('documentShipper');
                    $scope.navigateToNextField(nextId);
                }

                if (validateCode == 0 || validateCode == 4) {
                    if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent.id != undefined && $scope.documentDetailClone.issuingAgent.id != null && $scope.documentDetailClone.issuingAgent != "") {

                        if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.issuingAgent.status)) {
                            $scope.errorMap.put("partyIssuingAgent", $rootScope.nls["ERR90185"]);
                            $scope.documentDetailClone.issuingAgent = null;
                            $scope.documentDetailClone.issuingAgentAddress = {};
                            return false
                        }
                        if (ValidateUtil.isStatusHidden($scope.documentDetailClone.issuingAgent.status)) {
                            $scope.errorMap.put("partyIssuingAgent", $rootScope.nls["ERR90186"]);
                            $scope.documentDetailClone.issuingAgent = {};
                            $scope.documentDetailClone.issuingAgentAddress = {};
                            return false
                        }
                    } else {
                        $scope.documentDetailClone.issuingAgent = null;
                        $scope.documentDetailClone.issuingAgentAddress = {};
                    }

                    if ($scope.errorMap.get('partyIssuingAgent') != null)
                        $scope.errorMap.remove('partyIssuingAgent');
                    $scope.navigateToNextField(nextId);
                }

                if (validateCode == 0 || validateCode == 5) {

                    if ($scope.documentDetailClone.consignee == undefined || $scope.documentDetailClone.consignee == null || $scope.documentDetailClone.consignee == "" || $scope.documentDetailClone.consignee.id == null) {
                        $scope.documentDetailClone.consigneeAddress = {};
                        $scope.errorMap.put("consignee", $rootScope.nls["ERR90132"]);
                        $scope.showPartyAddressList = false;
                        return false;
                    } else {
                        if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.consignee.status)) {
                            $scope.errorMap.put("consignee", $rootScope.nls["ERR90133"]);
                            $scope.documentDetailClone.consignee = null;
                            $scope.documentDetailClone.consigneeAddress = {};
                            return false
                        }
                        if (ValidateUtil.isStatusHidden($scope.documentDetailClone.consignee.status)) {
                            $scope.errorMap.put("consignee", $rootScope.nls["ERR90134"]);
                            $scope.documentDetail.consignee = {};
                            $scope.documentDetail.consigneeAddress = {};
                            return false

                        }
                    }
                    if ($scope.errorMap.get('consignee') != null)
                        $scope.errorMap.remove('consignee');
                    $scope.navigateToNextField(nextId);
                }

                if (validateCode == 0 || validateCode == 6) {

                    if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent.partyDetail != undefined && $scope.documentDetailClone.issuingAgent.partyDetail.iataCode != null && $scope.documentDetailClone.issuingAgent.partyDetail.iataCode != "") {
                        var regexp = new RegExp("^[0-9a-zA-Z ]{0,30}$");
                        if (!regexp.test($scope.documentDetailClone.issuingAgent.partyDetail.accountNumber)) {
                            $scope.errorMap.put("iataCode", $rootScope.nls["ERR3756"]);
                            return false;
                        }
                    }
                    if ($scope.errorMap.get('iataCode') != null)
                        $scope.errorMap.remove('iataCode');

                }
                if (validateCode == 0 || validateCode == 10) {

                    if ($scope.documentDetailClone.pod == null || $scope.documentDetailClone.pod.id == null || $scope.documentDetailClone.pod.id.trim == "") {
                        console.log($rootScope.nls["ERR90041"]);
                        $scope.errorMap.put("pod", $rootScope.nls["ERR90044"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.pod.status)) {
                        $scope.documentDetailClone.pod = null;
                        $scope.errorMap.put("pod", $rootScope.nls["ERR90045"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden($scope.documentDetailClone.pod.status)) {
                        $scope.documentDetailClone.pod = null;
                        $scope.errorMap.put("pod", $rootScope.nls["ERR90046"]);
                        return false;
                    }
                    if ($scope.errorMap.get('pod') != null)
                        $scope.errorMap.remove('pod');
                    $scope.navigateToNextField(nextId);

                }



                if (validateCode == 0 || validateCode == 11) {

                    if ($scope.documentDetailClone.carrier != undefined && $scope.documentDetailClone.carrier != null && $scope.documentDetailClone.carrier != "" && $scope.documentDetailClone.carrier.id != null) {


                        if ($scope.documentDetailClone.carrier.status != null) {
                            if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.carrier.status)) {
                                console.log($rootScope.nls["ERR90064"]);
                                $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90064"]);
                                $scope.documentDetailClone.carrier = null;
                                return false
                            }
                            if (ValidateUtil.isStatusHidden($scope.documentDetailClone.carrier.status)) {
                                console.log($rootScope.nls["ERR90065"]);
                                $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90065"]);
                                $scope.documentDetailClone.carrier = null;
                                return false

                            }
                        }

                    }
                    if ($scope.errorMap.get('shipmentCarrier') != null)
                        $scope.errorMap.remove('shipmentCarrier');
                    $scope.navigateToNextField(nextId);
                }




                if (validateCode == 0 || validateCode == 12) {

                    if ($scope.shipmentServiceDetailClone.connectionList.length == 1) {

                        if ($scope.shipmentServiceDetailClone.connectionList[0].pod != undefined && $scope.shipmentServiceDetailClone.connectionList[0].pod != null && $scope.shipmentServiceDetailClone.connectionList[0].pod != "" && $scope.shipmentServiceDetailClone.connectionList[0].pod.id != undefined && $scope.shipmentServiceDetailClone.connectionList[0].pod.id != null) {


                            if ($scope.shipmentServiceDetailClone.connectionList[0].pod.status != null) {
                                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceDetailClone.connectionList[0].pod.status)) {
                                    $scope.errorMap.put("pod", $rootScope.nls["ERR90045"]);
                                    $scope.shipmentServiceDetailClone.connectionList[0].pod = null;
                                    $scope.cloneConnectionList.pod = null;
                                    return false
                                }
                                if (ValidateUtil.isStatusHidden($scope.shipmentServiceDetailClone.connectionList[0].pod.status)) {
                                    $scope.errorMap.put("pod", $rootScope.nls["ERR90046"]);
                                    $scope.shipmentServiceDetailClone.connectionList[0].pod = null;
                                    $scope.cloneConnectionList.pod = null;
                                    return false

                                }
                            }

                        }
                        if ($scope.errorMap.get('pod') != null)
                            $scope.errorMap.remove('pod');

                    }
                    $scope.navigateToNextField(nextId);

                }

                if (validateCode == 0 || validateCode == 13) {

                    if ($scope.shipmentServiceDetailClone.connectionList.length == 1) {

                        if ($scope.shipmentServiceDetailClone.connectionList[0].carrierMaster != undefined && $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster != null && $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster != "" && $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster.id != null) {


                            if ($scope.shipmentServiceDetailClone.connectionList[0].carrierMaster.status != null) {
                                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceDetailClone.connectionList[0].carrierMaster.status)) {
                                    console.log($rootScope.nls["ERR90064"]);
                                    $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90064"]);
                                    $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster = null;
                                    $scope.cloneConnectionList.carrier = null;
                                    return false
                                }
                                if (ValidateUtil.isStatusHidden($scope.shipmentServiceDetailClone.connectionList[0].carrierMaster.status)) {
                                    console.log($rootScope.nls["ERR90065"]);
                                    $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90065"]);
                                    $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster = null;
                                    $scope.cloneConnectionList.carrier = null;
                                    return false

                                }
                            }

                        }
                        if ($scope.errorMap.get('shipmentCarrier') != null)
                            $scope.errorMap.remove('shipmentCarrier');

                    }
                    $scope.navigateToNextField(nextId);
                }

                if (validateCode == 0 || validateCode == 14) {

                    if ($scope.shipmentServiceDetailClone.connectionList.length > 1) {
                        if ($scope.shipmentServiceDetailClone.connectionList[1].pod != undefined && $scope.shipmentServiceDetailClone.connectionList[1].pod != null && $scope.shipmentServiceDetailClone.connectionList[1].pod != "" && $scope.shipmentServiceDetailClone.connectionList[1].pod.id != null) {


                            if ($scope.shipmentServiceDetailClone.connectionList[1].pod.status != null) {
                                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceDetailClone.connectionList[1].pod.status)) {
                                    console.log($rootScope.nls["ERR90064"]);
                                    $scope.errorMap.put("pod", $rootScope.nls["ERR90045"]);
                                    $scope.shipmentServiceDetailClone.connectionList[1].pod = null;
                                    $scope.cloneConnectionList.pod1 = null;
                                    return false
                                }
                                if (ValidateUtil.isStatusHidden($scope.shipmentServiceDetailClone.connectionList[1].pod.status)) {
                                    console.log($rootScope.nls["ERR90065"]);
                                    $scope.errorMap.put("pod", $rootScope.nls["ERR90046"]);
                                    $scope.shipmentServiceDetailClone.connectionList[1].pod = null;
                                    $scope.cloneConnectionList.pod1 = null;
                                    return false

                                }
                            }

                        }
                        if ($scope.errorMap.get('pod') != null)
                            $scope.errorMap.remove('pod');
                        $scope.navigateToNextField(nextId);
                    }
                }

                if (validateCode == 0 || validateCode == 15) {

                    if ($scope.shipmentServiceDetailClone.connectionList.length > 1) {
                        if ($scope.shipmentServiceDetailClone.connectionList[1].carrierMaster != undefined && $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster != null && $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster != "" && $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster.id != null) {


                            if ($scope.shipmentServiceDetailClone.connectionList[1].carrierMaster.status != null) {
                                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceDetailClone.connectionList[1].carrierMaster.status)) {
                                    console.log($rootScope.nls["ERR90064"]);
                                    $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90064"]);
                                    $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster = null;
                                    $scope.cloneConnectionList.carrier1 = null;
                                    return false
                                }
                                if (ValidateUtil.isStatusHidden($scope.shipmentServiceDetailClone.connectionList[1].carrierMaster.status)) {
                                    console.log($rootScope.nls["ERR90065"]);
                                    $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90065"]);
                                    $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster = null;
                                    $scope.cloneConnectionList.carrier1 = null;
                                    return false

                                }
                            }

                        }
                        if ($scope.errorMap.get('shipmentCarrier') != null)
                            $scope.errorMap.remove('shipmentCarrier');
                    }
                    $scope.navigateToNextField(nextId);
                }
                if (validateCode == 0 || validateCode == 16) {


                    if ($scope.shipmentServiceDetailClone.localCurrency != undefined && $scope.shipmentServiceDetailClone.localCurrency != null && $scope.shipmentServiceDetailClone.localCurrency != "") {

                        if ($scope.shipmentServiceDetailClone.localCurrency.status == "Block") {
                            $scope.shipmentServiceDetailClone.localCurrency = null;
                            $scope.errorMap.put("consolCurrency", $rootScope.nls["ERR95048"]);
                            return false;
                        }
                        if ($scope.shipmentServiceDetailClone.localCurrency.status == "Hide") {
                            $scope.shipmentServiceDetailClone.localCurrency = null;
                            $scope.errorMap.put("consolCurrency", $rootScope.nls["ERR95049"]);
                            return false;

                        }
                    }
                    if ($scope.errorMap.get('consolCurrency') != null)
                        $scope.errorMap.remove('consolCurrency');
                    $scope.navigateToNextField(nextId);
                }


                if (validateCode == 0 || validateCode == 17) {
                    if ($scope.documentDetailClone.destination == null || $scope.documentDetailClone.destination.id == null || $scope.documentDetailClone.destination.id.trim == "") {
                        $scope.errorMap.put("destination", $rootScope.nls["ERR90014"]);
                        return false;
                    }
                    if (ValidateUtil.isStatusBlocked($scope.documentDetailClone.destination.status)) {
                        $scope.documentDetailClone.destination = null;
                        $scope.errorMap.put("destination", $rootScope.nls["ERR90039"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden($scope.documentDetailClone.destination.status)) {
                        $scope.documentDetailClone.destination = null;
                        $scope.errorMap.put("destination", $rootScope.nls["ERR90040"]);
                        return false;
                    }
                    if ($scope.errorMap.get('destination') != null)
                        $scope.errorMap.remove('destination');
                    $scope.navigateToNextField(nextId);
                }


                if (validateCode == 0 || validateCode == 18) {
                    if ($scope.documentDetailClone.noOfPieces == undefined || $scope.documentDetailClone.noOfPieces == "" || $scope.documentDetailClone.noOfPieces == null) {
                        $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR90209"]);
                        return false;
                    } else {
                        var regexp = new RegExp("^[0-9]{0,9}$");
                        if (!regexp.test($scope.documentDetailClone.noOfPieces)) {
                            $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR90209"]);
                            return false;

                        }
                    }

                    if ($scope.errorMap.get('noOfPieces') != null)
                        $scope.errorMap.remove('noOfPieces');
                }

                if (validateCode == 0 || validateCode == 19) {

                    if ($scope.documentDetailClone.grossWeight == undefined || $scope.documentDetailClone.grossWeight == null || $scope.documentDetailClone.grossWeight == "") {
                        $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90475"]);
                        return false;
                    } else {
                        if ($scope.documentDetailClone.grossWeight != null && $scope.documentDetailClone.grossWeight != undefined && $scope.documentDetailClone.grossWeight != "")
                            if ($scope.documentDetailClone.grossWeight <= 0 || $scope.documentDetailClone.grossWeight > 99999999999.99) {
                                $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90476"]);
                                return false;
                            }

                        var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,3})?|\.[0-9]+)$");
                        if (!regexp
                            .test($scope.documentDetailClone.grossWeight)) {
                            $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90476"]);
                            $scope.shipmentTab = 'documents';
                            return false;
                        }
                    }
                    /*else{
            if ($scope.documentDetailClone.grossWeightInPound == undefined
                    || $scope.documentDetailClone.grossWeightInPound == null
                    || $scope.documentDetailClone.grossWeightInPound == "") {
                $scope.errorMap.put("grossWeightKg",$rootScope.nls["ERR90477"]);
                return false;
            }
            else
            {
            if($scope.documentDetailClone.grossWeightInPound!= null && $scope.documentDetailClone.grossWeightInPound!=undefined  && $scope.documentDetailClone.grossWeightInPound!="")
                if ($scope.documentDetailClone.grossWeightInPound<=0 || $scope.documentDetailClone.grossWeightInPound>99999999999.99) {
                    $scope.errorMap.put("grossWeightKg",$rootScope.nls["ERR90478"]);
                    return false;
                }

            var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,3})?|\.[0-9]+)$");
            if (!regexp
                    .test($scope.documentDetailClone.grossWeightInPound)) {
                $scope.errorMap.put("grossWeightKg",$rootScope.nls["ERR90479"]);
                $scope.shipmentTab='documents';
                return false;
            }
        }*/

                    if ($scope.errorMap.get('grossWeightKg') != null)
                        $scope.errorMap.remove('grossWeightKg');
                    /*$scope.documentDetailClone.grossWeight=Math.round($scope.documentDetailClone.grossWeight* 1000) / 1000;
        $scope.documentDetailClone.grossWeightInPound=Math.round(($scope.documentDetailClone.grossWeight * 2.20462) * 1000) / 1000;*/
                }

                if (validateCode == 0 || validateCode == 20) {

                    if ($scope.documentDetailClone.rateClass != null && $scope.documentDetailClone.rateClass != undefined && $scope.documentDetailClone.rateClass != "") {
                        var regexp = new RegExp("^[0-9A-Za-z]{0,10}$");
                        if (!regexp
                            .test($scope.documentDetailClone.rateClass)) {
                            $scope.errorMap.put("rateclass", $rootScope.nls["ERR90204"]);
                            return false;
                        }
                    }
                    if ($scope.errorMap.get('rateclass') != null)
                        $scope.errorMap.remove('rateclass');
                }



                if (validateCode == 0 || validateCode == 21) {

                    if ($scope.documentDetailClone.chargebleWeight == undefined || $scope.documentDetailClone.chargebleWeight == null || $scope.documentDetailClone.chargebleWeight == "") {
                        $scope.errorMap.put("chargebleweight", $rootScope.nls["ERR22111"]);
                        return false;
                    } else {
                        if ($scope.documentDetailClone.chargebleWeight != null && $scope.documentDetailClone.chargebleWeight != undefined && $scope.documentDetailClone.chargebleWeight != "")
                            if ($scope.documentDetailClone.chargebleWeight <= 0 || $scope.documentDetailClone.chargebleWeight > 99999999999.999) {
                                $scope.errorMap.put("chargebleweight", $rootScope.nls["ERR22111"]);
                                return false;
                            }

                        var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,3})?|\.[0-9]+)$");
                        if (!regexp
                            .test($scope.documentDetailClone.chargebleWeight)) {
                            $scope.errorMap.put("chargebleweight", $rootScope.nls["ERR22111"]);
                            return false;
                        }


                    }
                    if ($scope.errorMap.get('chargebleweight') != null)
                        $scope.errorMap.remove('chargebleweight');
                }

                if (validateCode == 0 || validateCode == 22) {

                    if ($scope.shipmentServiceDetailClone.isAgreed == false && $scope.documentDetailClone.ratePerChargeClone != undefined && $scope.documentDetailClone.ratePerChargeClone != null && $scope.documentDetailClone.ratePerChargeClone != "") {

                        if ($scope.documentDetailClone.ratePerChargeClone <= 0 || $scope.documentDetailClone.ratePerChargeClone > 9999999999.999) {
                            $scope.errorMap.put("ratePerCharge", $rootScope.nls["ERR90556"]);
                            return false;
                        }

                        var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,3})?|\.[0-9]+)$");

                        if (!regexp
                            .test($scope.documentDetailClone.ratePerChargeClone)) {
                            $scope.errorMap.put("ratePerCharge", $rootScope.nls["ERR90556"]);
                            return false;
                        }
                    }
                    if ($scope.errorMap.get('ratePerCharge') != null)
                        $scope.errorMap.remove('ratePerCharge');
                }


                if (validateCode == 0 || validateCode == 23) {
                    if ($scope.shipmentServiceDetailClone.isAgreed == false && $scope.documentDetailClone.totalAmountClone != undefined && $scope.documentDetailClone.totalAmountClone != null && $scope.documentDetailClone.totalAmountClone != "") {
                        var totalAmount = parseFloat($scope.documentDetailClone.totalAmountClone);
                        var calculateAmount = 0.0;
                        if ($scope.documentDetailClone.ratePerChargeClone != undefined && $scope.documentDetailClone.ratePerChargeClone != null && $scope.documentDetailClone.chargebleWeight != undefined && $scope.documentDetailClone.chargebleWeight != null) {
                            calculateAmount = parseFloat($scope.documentDetailClone.ratePerChargeClone) * parseFloat($scope.documentDetailClone.chargebleWeight);
                        }

                        if (totalAmount != calculateAmount) {
                            $scope.errorMap.put("totalAmount", $rootScope.nls["ERR96516"]);
                            return false;
                        }
                    }
                    if ($scope.errorMap.get('totalAmount') != null)
                        $scope.errorMap.remove('totalAmount');
                }

                if (validateCode == 0 || validateCode == 24) {

                    if ($scope.shipmentCharge != undefined && $scope.shipmentCharge.chargeMaster != undefined && $scope.shipmentCharge.chargeMaster != null && $scope.shipmentCharge.chargeMaster.status != null) {

                        if (ValidateUtil.isStatusBlocked($scope.shipmentCharge.chargeMaster.status)) {

                            $scope.shipmentCharge.chargeMaster = null;

                            console.log("Selected Charge is Blocked");

                            $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22073"]);

                            return false
                        }

                        if (ValidateUtil.isStatusHidden($scope.shipmentCharge.chargeMaster.status)) {

                            $scope.shipmentCharge.chargeMaster = null;

                            console.log("Selected Charge is Hidden");

                            $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22074"]);

                            return false
                        }

                    }
                    if ($scope.errorMap.get('chargeMaster') != null)
                        $scope.errorMap.remove('chargeMaster');
                    $scope.navigateToNextField(nextId);
                }

                if (validateCode == 0 || validateCode == 25) {

                    if ($scope.documentDetailClone.shipper != undefined && $scope.documentDetailClone.shipper != null && $scope.documentDetailClone.shipper != "" &&
                        ($scope.documentDetailClone.shipperAddress.addressLine1 == null || $scope.documentDetailClone.shipperAddress.addressLine1 == undefined ||
                            $scope.documentDetailClone.shipperAddress.addressLine1 == "")) {
                        $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90110"]);
                        return false;
                    }
                    if ($scope.errorMap.get('documentShipper') != null)
                        $scope.errorMap.remove('documentShipper');
                }

                if (validateCode == 0 || validateCode == 26) {

                    if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent != "" &&
                        ($scope.documentDetailClone.issuingAgentAddress.addressLine1 == null || $scope.documentDetailClone.issuingAgentAddress.addressLine1 == undefined ||
                            $scope.documentDetailClone.issuingAgentAddress.addressLine1 == "")) {
                        $scope.errorMap.put("partyIssuingAgent", $rootScope.nls["ERR90188"]);
                        return false;
                    }
                    if ($scope.errorMap.get('partyIssuingAgent') != null)
                        $scope.errorMap.remove('partyIssuingAgent');
                }


                if (validateCode == 0 || validateCode == 27) {

                    if ($scope.documentDetailClone.consignee != undefined && $scope.documentDetailClone.consignee != null && $scope.documentDetailClone.consignee != "" &&
                        ($scope.documentDetailClone.consigneeAddress.addressLine1 == null || $scope.documentDetailClone.consigneeAddress.addressLine1 == undefined ||
                            $scope.documentDetailClone.consigneeAddress.addressLine1 == "")) {
                        $scope.errorMap.put("consignee", $rootScope.nls["ERR05546"]);
                        return false;
                    }
                    if ($scope.errorMap.get('consignee') != null)
                        $scope.errorMap.remove('consignee');
                }




                return true;

            } //end block of validation




        $scope.update = function() {
            $scope.downloadFlag = false;
            $scope.printFlag = false;
            if (!$scope.isEdit) {
                $scope.updateDynamicHawbDocDetail();
            } else {
                $scope.updateDynamicHawbDocEdit();
            }
        }

        $scope.updateDynamicHawbDocEdit = function() {
            $rootScope.mainpreloder = true;
            if ($scope.validateDynamicDocument(0)) {
                if (!$scope.shipmentServiceDetailClone.isAgreed || $scope.shipmentServiceDetailClone.isAgreed == 'No') {
                    $scope.documentDetailClone.ratePerCharge = $scope.documentDetailClone.ratePerChargeClone;
                    $scope.documentDetailClone.marksAndNo = $scope.documentDetailClone.marksAndNoClone;
                    $scope.documentDetailClone.totalAmount = $scope.documentDetailClone.totalAmountClone;
                    $scope.documentDetailClone.preWeightCharge = $scope.documentDetailClone.preWeightChargeClone;
                    $scope.documentDetailClone.collWeightCharge = $scope.documentDetailClone.collWeightChargeClone;
                    $scope.documentDetailClone.totalPrepaid = $scope.documentDetailClone.totalPrepaidClone;
                    $scope.documentDetailClone.totalCollect = $scope.documentDetailClone.totalCollectClone;
                    $scope.documentDetailClone.otherChargesDuePreAgent = $scope.documentDetailClone.otherChargesDuePreAgentClone;
                    $scope.documentDetailClone.otherChargesDueCollAgent = $scope.documentDetailClone.otherChargesDueCollAgentClone;
                    $scope.documentDetailClone.otherChargesDuePreCarrier = $scope.documentDetailClone.otherChargesDuePreCarrierClone;
                    $scope.documentDetailClone.otherChargesDueCollCarrier = $scope.documentDetailClone.otherChargesDueCollCarrierClone;
                    $scope.documentDetailClone.preValuationCharge = $scope.documentDetailClone.preValuationChargeClone;
                    $scope.documentDetailClone.collValuationCharge = $scope.documentDetailClone.collValuationChargeClone;
                    $scope.documentDetailClone.collectTax = $scope.documentDetailClone.collectTaxClone;
                    $scope.documentDetailClone.prepaidTax = $scope.documentDetailClone.prepaidTaxClone;
                }
                // for empty strings
                if ($scope.documentDetailClone.issCarrierAgentSign == undefined || $scope.documentDetailClone.issCarrierAgentSign == null || $scope.documentDetailClone.issCarrierAgentSign == "") {
                    $scope.documentDetailClone.issCarrierAgentSign = null;
                }
                if ($scope.documentDetailClone.shipperSignature == undefined || $scope.documentDetailClone.shipperSignature == null || $scope.documentDetailClone.shipperSignature == "") {
                    $scope.documentDetailClone.shipperSignature = null;
                }
                $scope.tmpObj = {};
                $scope.tmpObj = angular.copy($scope.shipmentClone);
                ShipmentUpdate.save(uiShipmentDataService.convertToSaveObject($scope.tmpObj)).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        var routingObj = {};
                        routingObj.forHawb = true;
                        routingObj.id = $scope.shipmentClone.id;
                        routingObj.shipmentUid = $scope.shipmentClone.shipmentUid;
                        routingObj.documentId = $scope.documentDetailClone.id;
                        routingObj.serviceId = $scope.shipmentServiceDetailClone.id;
                        if ($scope.downloadFlag) {
                            previewReportService.printDownload('HAWB', 'Download', $scope.documentDetailClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                            $scope.routeTo();
                            $rootScope.mainpreloder = false;
                        } else if ($scope.printFlag) {
                            //$scope.print();
                            previewReportService.printDownload('HAWB', 'Print', $scope.documentDetailClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                            $scope.routeTo();
                            $rootScope.mainpreloder = false;
                        } else {
                            Notification.success($rootScope.nls["ERR401"]);
                            $rootScope.mainpreloder = false;
                            $scope.routeTo('update');
                        }
                    } else {
                        Notification.warning($rootScope.nls["ERR403"]);
                        $rootScope.mainpreloder = false;
                    }

                }, function(error) {
                    Notification.warning($rootScope.nls["ERR403"]);
                    $rootScope.mainpreloder = false;
                });
            } else {
                $rootScope.mainpreloder = false;
            }
        }



        $scope.downloadDynamicHawb = function() {
            $scope.downloadFlag = true;
            $scope.printFlag = false;
            if (!$scope.isEdit) {
                $scope.updateDynamicHawbDocDetail();
            } else {
                $scope.updateDynamicHawbDocEdit();
            }
        }


        $scope.printDynamicHawb = function() {
            $scope.downloadFlag = false;
            $scope.printFlag = true;
            if (!$scope.isEdit) {
                $scope.updateDynamicHawbDocDetail();
            } else {
                $scope.updateDynamicHawbDocEdit();
            }
        }


        $scope.routeTo = function(op) {
            $rootScope.mainpreloder = false;
            $scope.hawbModal.$promise.then($scope.hawbModal.hide);

            if ($scope.screen != undefined && $scope.screen == 'AirDocument') {
                return;
            }
            var params = {};
            $scope.hawbShow = {
                shipmentUid: $scope.shipmentClone.shipmentUid,
                documentdetailId: $scope.documentDetailClone.id,
                type: 'HAWB',
                isEdit: $scope.isEdit,
                serviceid: $scope.shipmentServiceDetailClone.id
            }
            var hawbShowJson = JSON.stringify($scope.hawbShow);
            localStorage.setItem('isHawbShow', hawbShowJson)
            var serviceId = $scope.shipmentServiceDetailClone.id;
            if (op != undefined && op == 'update') {

                var stateCount = 0;
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }

                if ($scope.isEdit) {
                    $state.go("layout.editNewShipment", {
                        shipmentId: $scope.shipmentClone.id,
                        submitAction: 'Saved',
                        count: stateCount
                    });
                } else {
                    $state.go('layout.viewCrmShipment', {
                        shipmentUid: $scope.shipmentClone.shipmentUid,
                        documentId: $scope.documentDetailClone.id,
                        forHawb: true,
                        count: stateCount,
                        serviceid: $scope.shipmentServiceDetailClone.id
                    });
                }



            }

        }




        //upddate detail
        $scope.updateDynamicHawbDocDetail = function() {
            $rootScope.mainpreloder = true;
            if ($scope.validateDynamicDocument(0)) {
                if (!$scope.shipmentServiceDetailClone.isAgreed || $scope.shipmentServiceDetailClone.isAgreed == 'No') {
                    $scope.documentDetailClone.ratePerCharge = $scope.documentDetailClone.ratePerChargeClone;
                    $scope.documentDetailClone.marksAndNo = $scope.documentDetailClone.marksAndNoClone;
                    $scope.documentDetailClone.totalAmount = $scope.documentDetailClone.totalAmountClone;
                    $scope.documentDetailClone.preWeightCharge = $scope.documentDetailClone.preWeightChargeClone;
                    $scope.documentDetailClone.collWeightCharge = $scope.documentDetailClone.collWeightChargeClone;
                    $scope.documentDetailClone.totalPrepaid = $scope.documentDetailClone.totalPrepaidClone;
                    $scope.documentDetailClone.totalCollect = $scope.documentDetailClone.totalCollectClone;
                    $scope.documentDetailClone.otherChargesDuePreAgent = $scope.documentDetailClone.otherChargesDuePreAgentClone;
                    $scope.documentDetailClone.otherChargesDueCollAgent = $scope.documentDetailClone.otherChargesDueCollAgentClone;
                    $scope.documentDetailClone.otherChargesDuePreCarrier = $scope.documentDetailClone.otherChargesDuePreCarrierClone;
                    $scope.documentDetailClone.otherChargesDueCollCarrier = $scope.documentDetailClone.otherChargesDueCollCarrierClone;
                    $scope.documentDetailClone.preValuationCharge = $scope.documentDetailClone.preValuationChargeClone;
                    $scope.documentDetailClone.collValuationCharge = $scope.documentDetailClone.collValuationChargeClone;
                    $scope.documentDetailClone.collectTax = $scope.documentDetailClone.collectTaxClone;
                    $scope.documentDetailClone.prepaidTax = $scope.documentDetailClone.prepaidTaxClone;
                }
                // for empty strings
                if ($scope.documentDetailClone.issCarrierAgentSign == undefined || $scope.documentDetailClone.issCarrierAgentSign == null || $scope.documentDetailClone.issCarrierAgentSign == "") {
                    $scope.documentDetailClone.issCarrierAgentSign = null;
                }
                if ($scope.documentDetailClone.shipperSignature == undefined || $scope.documentDetailClone.shipperSignature == null || $scope.documentDetailClone.shipperSignature == "") {
                    $scope.documentDetailClone.shipperSignature = null;
                }


                $scope.documentDetailClone.hawbReceivedOn = $rootScope.sendApiDateAndTime($scope.documentDetailClone.hawbReceivedOn);
                $scope.tmpObj = {};
                $scope.tmpObj = angular.copy($scope.shipmentClone);
                ShipmentUpdate.update($scope.tmpObj).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {

                        var routingObj = {};
                        if ($scope.screen != undefined && $scope.screen == 'AirDocument') {
                            routingObj.id = $scope.documentDetailClone != undefined ? $scope.documentDetailClone.id : null;
                            routingObj.docType = 'HAWB';
                            routingObj.docNo = $scope.documentDetailClone != undefined ? $scope.documentDetailClone.documentNo : null;
                        } else {
                            routingObj.id = $scope.shipmentClone.id;
                            routingObj.shipmentUid = $scope.shipmentClone.shipmentUid;
                            routingObj.documentId = $scope.documentDetailClone.id;
                            routingObj.serviceId = $scope.shipmentServiceDetailClone.id;
                            routingObj.forHawb = true;
                        }

                        if ($scope.downloadFlag) {
                            previewReportService.printDownload('HAWB', 'Download', $scope.documentDetailClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                            $scope.routeTo();
                            $rootScope.mainpreloder = false;
                        } else if ($scope.printFlag) {
                            previewReportService.printDownload('HAWB', 'Print', $scope.documentDetailClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                            $scope.routeTo();
                            $rootScope.mainpreloder = false;
                        } else {
                            Notification.success($rootScope.nls["ERR401"]);
                            $rootScope.mainpreloder = false;
                            $scope.routeTo('update');
                        }
                    } else {
                        Notification.warning($rootScope.nls["ERR403"]);
                        $rootScope.mainpreloder = false;
                    }

                }, function(error) {
                    Notification.warning($rootScope.nls["ERR403"]);
                    $rootScope.mainpreloder = false;
                });
            } else {
                Notification.warning($rootScope.nls["ERR404"]);
                $rootScope.mainpreloder = false;
            }
        }


        $scope.addressRearrange = function() {
            var shipperAddress3 = document.getElementById('shipperAddress3').value;
            var shipperAddress4 = document.getElementById('shipperAddress4').value;

            if (shipperAddress3 == undefined || shipperAddress3 == '') {
                document.getElementById('shipperAddress3').value = shipperAddress4;
                document.getElementById('shipperAddress4').value = "";
            }

            var issuingAgentAddress3 = document.getElementById('issuingAgentAddress3').value;
            var issuingAgentAddress4 = document.getElementById('issuingAgentAddress4').value;

            if (issuingAgentAddress3 == undefined || issuingAgentAddress3 == '') {
                document.getElementById('issuingAgentAddress3').value = issuingAgentAddress4;
                document.getElementById('issuingAgentAddress4').value = "";
            }

            var consigneeAddres3 = document.getElementById('consigneeAddres3').value;
            var consigneeAddres4 = document.getElementById('consigneeAddres4').value;

            if (consigneeAddres3 == undefined || consigneeAddres3 == '') {
                document.getElementById('consigneeAddres3').value = consigneeAddres4;
                document.getElementById('consigneeAddres4').value = "";
            }

            var issuingAgentAddress13 = document.getElementById('issuingAgentAddress13').value;
            var issuingAgentAddress14 = document.getElementById('issuingAgentAddress14').value;

            if (issuingAgentAddress13 == undefined || issuingAgentAddress13 == '') {
                document.getElementById('issuingAgentAddress13').value = issuingAgentAddress14;
                document.getElementById('issuingAgentAddress14').value = "";
            }


        }



        $scope.updateList = function(listObject, listKey) {

            if (listKey == 'pod-0') {
                $scope.shipmentServiceDetailClone.connectionList[0].pod = listObject;
            }
            if (listKey == 'carrier') {
                $scope.shipmentServiceDetailClone.connectionList[0].carrierMaster = listObject;
            }
            if (listKey == 'pod-1') {
                $scope.shipmentServiceDetailClone.connectionList[1].pod = listObject;
            }
            if (listKey == 'carrier1') {
                $scope.shipmentServiceDetailClone.connectionList[1].carrierMaster = listObject;
            }

            if (listKey == 'flightanddate') {
                var res = listObject.split("/");
                $scope.shipmentServiceDetailClone.connectionList[1].flightVoyageNo = res[0];
                $scope.shipmentServiceDetailClone.connectionList[1].eta = res[1];
            }

            if (listKey == 'flightanddate0') {
                var res = listObject.split("/");
                $scope.shipmentServiceDetailClone.connectionList[0].flightVoyageNo = res[0];
                $scope.shipmentServiceDetailClone.connectionList[0].eta = res[1];
            }


        }

        $scope.updateflightanddate = function(listObject) {
            var res = listObject.split("/");
            $scope.shipmentServiceDetailClone.routeNo = res[0];
            if ($scope.isEdit != undefined && !$scope.isEdit) {
                $scope.shipmentServiceDetailClone.etd = $rootScope.sendApiDate(res[1]);
            } else {
                $scope.shipmentServiceDetailClone.etd = res[1];
            }
        }



        try {

            try {
                var isHawbShow = JSON.parse(localStorage.getItem('isHawbShow'));
            } catch (e) {
                console.log("exception", e);
            }

            if (isHawbShow != undefined && isHawbShow != null) {
                localStorage.removeItem('isHawbShow');
                ShipmentGetByUid.get({
                        shipmentUid: isHawbShow.shipmentUid
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            var shipment = data.responseObject;
                            if (isHawbShow.isEdit || isHawbShow.isEdit == 'true') {
                                for (var i = 0; i < shipment.shipmentServiceList.length; i++) {
                                    if (shipment.shipmentServiceList[i].shipmentUid == isHawbShow.shipmentUid) {
                                        shipment.shipmentServiceList[i].etd = $rootScope.dateToString(shipment.shipmentServiceList[i].etd);
                                        if (shipment.shipmentServiceList[i].connectionList != undefined && shipment.shipmentServiceList[i].connectionList != null && shipment.shipmentServiceList[i].connectionList.length > 0) {
                                            var length = (shipment.shipmentServiceList[i].connectionList.length - 1);
                                            shipment.shipmentServiceList[i].connectionList[length].etd = $rootScope.dateToString(shipment.shipmentServiceList[i].connectionList[length].etd)
                                        }
                                    }
                                }
                            }
                            $scope.previewReport(shipment, isHawbShow.shipmentUid, isHawbShow.documentdetailId, isHawbShow.type, isHawbShow.serviceid, isHawbShow.isEdit);
                        } else {
                            console.log("Error Exists while retriveing shipment");
                            $rootScope.mainpreloder = false;
                        }
                    },
                    function(error) {
                        console.log("Shipment get Failed : " + error)
                        $rootScope.mainpreloder = false;
                    });
            }
        } catch (e) {
            console.log("exception in hawb show")
            $rootScope.mainpreloder = false;
        }

    }
]);