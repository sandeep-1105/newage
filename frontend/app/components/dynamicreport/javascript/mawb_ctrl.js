/**
 * 
 */
app.controller('mawbController', ['$rootScope', '$http', '$timeout', '$scope', '$modal', 'ngDialog', 'Notification',
    'PartiesList', 'PortByTransportMode', 'ServiceByTransportMode', 'CurrencySearchExclude',
    'ValidateUtil', 'PortGroupList', 'AutoCompleteService',
    'CarrierByTransportMode', 'ConsolEdit', 'consolShipmentDocumentIdList', 'cloneService',
    'ChargeSearchKeyword', '$state', '$stateParams', 'ConsolGet', 'addressJoiner', 'GetCarrier',
    'roleConstant', 'appMetaFactory', 'previewReportService',
    function ($rootScope, $http, $timeout, $scope, $modal, ngDialog, Notification,
        PartiesList, PortByTransportMode, ServiceByTransportMode, CurrencySearchExclude,
        ValidateUtil, PortGroupList, AutoCompleteService,
        CarrierByTransportMode, ConsolEdit, consolShipmentDocumentIdList, cloneService,
        ChargeSearchKeyword, $state, $stateParams, ConsolGet, addressJoiner, GetCarrier,
        roleConstant, appMetaFactory, previewReportService) {

        $scope.closeDynamicDocument = function () {
            $scope.errorMap = {};
        }
        $scope.$AutoCompleteService = AutoCompleteService;


        $scope.groupColDestination = [{
            name: 'Port Code',
            width: '150px',
            valueField: 'portCode'
        }, {
            name: 'Port Name',
            width: '120px',
            valueField: 'portName'
        }];


        //for preview Report - 
        $scope.previewReportBefore = function (object, consolId, type, isEdit) {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DOCUMENT_PREVIEW_MAWB_REPORT_VIEW)) {
                $scope.mainpreloder = true
                $rootScope.clientMessage = null;
                if (object.consolDocument.issuingAgent == undefined ||
                    object.consolDocument.issuingAgent == null ||
                    object.consolDocument.issuingAgent.id == undefined ||
                    object.consolDocument.issuingAgent.id == null) {
                    console.log("$rootScope.nls['ERR95910'] : " + $rootScope.nls['ERR95910'])
                    $rootScope.clientMessage = $rootScope.nls['ERR95910'];
                    return;
                }
                if (object.consolDocument.issuingAgent.partyDetail == undefined ||
                    object.consolDocument.issuingAgent.partyDetail == null ||
                    object.consolDocument.issuingAgent.partyDetail.iataCode == undefined ||
                    object.consolDocument.issuingAgent.partyDetail.iataCode == null) {
                    console.log("$rootScope.nls['ERR95911'] : " + $rootScope.nls['ERR95911'])
                    $rootScope.clientMessage = $rootScope.nls['ERR95911'];
                    return;
                }
                $scope.previewReport(object, type, isEdit)

            }
        }

        $scope.previewReport = function (consol, type, isEdit, screen) {

            try {
                $scope.screen = 'MasterShipment';
                $scope.isEdit = isEdit;
                $scope.docType = type;
                if ($scope.isEdit) {
                    $scope.consolClone = angular.copy(consol);
                    $scope.previewConsolReport();
                } else {
                    ConsolGet.get({
                        id: consol.id
                    }, function (data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.consolClone = angular.copy(data.responseObject);
                            $scope.previewConsolReport();
                        } else {
                            Notification.warning($rootScope.nls["ERR404"]);
                        }
                    },
                        function (error) {
                            Notification.warning($rootScope.nls["ERR404"]);
                        });
                }
            } catch (e) {
                console.log(e)
                $scope.mainpreloder = false;
            }
        }

        $scope.previewConsolReport = function () {
            $scope.isOnlyConsoleConnection = false;
            if($scope.consolClone.connectionList.length>0){
                $scope.isOnlyConsoleConnection = false;
            }
            else{
                $scope.isOnlyConsoleConnection = true;
            }
            $scope.consolDocumentClone = $scope.consolClone.consolDocument;
            $scope.consolDocumentClone.mawbIssueDate = $rootScope.dateToString(new Date());
            var extraConnList = new Array();
            for(var i=0 ;i<$scope.consolClone.connectionList.length;i++){
                if (i<2) {
                     continue;
                    }
                    else{
                        extraConnList.push(" "+$scope.consolClone.connectionList[i].flightVoyageNo + "|"+
                                           $rootScope.dateToString($scope.consolClone.connectionList[i].etd)+ "|"+
                                           $scope.consolClone.connectionList[i].podCode+ "|"+
                                           $scope.consolClone.connectionList[i].carrierCode)
                                    ;
                    }
            }

            if($scope.consolClone.connectionList.length>2){
                $scope.consolDocumentClone.handlingInformation = extraConnList.join();
            }
           

            if ($scope.consolClone.iataRate != undefined && $scope.consolClone.iataRate != null && $scope.consolClone.iataRate.trim != "") {
                $scope.consolClone.consolDocument.isAgreed = false;
            }

            if ($scope.consolDocumentClone.isAgreed == 'Yes' || $scope.consolDocumentClone.isAgreed == true) {
                $scope.consolDocumentClone.isAgreed = true;
                if ($scope.consolClone.chargeList != null && $scope.consolClone.chargeList == 0) {
                    $scope.consolClone.chargeList[1, 2, 3, 4, 5, 6];
                }
            } else {
                $scope.consolDocumentClone.isAgreed = false;
            }

            if ($scope.consolDocumentClone.isMarksNo == undefined || $scope.consolDocumentClone.isMarksNo == 'Yes' || $scope.consolDocumentClone.isMarksNo == true) {
                $scope.consolDocumentClone.isMarksNo = true;
            } else {
                $scope.consolDocumentClone.isMarksNo = false;
            }

            $scope.mawbModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/dynamicreport/view/mawb.html',
                backdrop: 'static',
                show: false
            });

            /*if($scope.consolDocumentClone.shipper!=undefined && $scope.consolDocumentClone.shipper!=null && $scope.consolDocumentClone.shipperAddress!=undefined)
                    $scope.getPartyAddress('shipper');
                if($scope.consolDocumentClone.consignee!=undefined && $scope.consolDocumentClone.consignee!=null && $scope.consolDocumentClone.consigneeAddress!=undefined)
                    $scope.getPartyAddress('consignee');
                if($scope.consolDocumentClone.issuingAgent!=undefined && $scope.consolDocumentClone.issuingAgent!=null && $scope.consolDocumentClone.issuingAgentAddress!=undefined)
                    $scope.getPartyAddress('issuingAgent');
                if($scope.consolDocumentClone.firstNotify!=undefined && $scope.consolDocumentClone.firstNotify!=null && $scope.consolDocumentClone.firstNotifyAddress!=undefined)
                    $scope.getPartyAddress('firstNotify');
                if($scope.consolDocumentClone.secondNotify!=undefined && $scope.consolDocumentClone.secondNotify!=null && $scope.consolDocumentClone.secondNotifyAddress!=undefined)
                    $scope.getPartyAddress('secondNotify');
    */


            $scope.choosePartyAddress = function (addressKey) {
                if (addressKey == 'shipper') {
                    if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper.status != 'Block' && ($scope.consolDocumentClone.shipperAddress == undefined || $scope.consolDocumentClone.shipperAddress == null)) {
                        $scope.consolDocumentClone.shipperAddress = {};
                        $scope.getPartyAddress($scope.consolDocumentClone.shipper, $scope.consolDocumentClone.shipperAddress);
                    } else if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper.status != 'Block') {
                        $scope.getPartyAddress($scope.consolDocumentClone.shipper, $scope.consolDocumentClone.shipperAddress);
                    }
                }

                if (addressKey == 'consignee') {
                    if ($scope.consolDocumentClone.consignee != undefined && $scope.consolDocumentClone.consignee != null && $scope.consolDocumentClone.consignee.status != 'Block' && ($scope.consolDocumentClone.consigneeAddress == undefined || $scope.consolDocumentClone.consigneeAddress == null)) {
                        $scope.consolDocumentClone.consigneeAddress = {};
                        $scope.getPartyAddress($scope.consolDocumentClone.consignee, $scope.consolDocumentClone.consigneeAddress);
                    } else if ($scope.consolDocumentClone.consignee != undefined && $scope.consolDocumentClone.consignee != null && $scope.consolDocumentClone.consignee.status != 'Block') {
                        $scope.getPartyAddress($scope.consolDocumentClone.consignee, $scope.consolDocumentClone.consigneeAddress);
                    }
                }

                if (addressKey == 'issuingAgent') {
                    if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.status != 'Block' && ($scope.consolDocumentClone.issuingAgentAddress == undefined || $scope.consolDocumentClone.issuingAgentAddress == null)) {
                        $scope.consolDocumentClone.issuingAgentAddress = {};
                        $scope.getPartyAddress($scope.consolDocumentClone.issuingAgent, $scope.consolDocumentClone.issuingAgentAddress);
                    } else if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.status != 'Block') {
                        $scope.getPartyAddress($scope.consolDocumentClone.issuingAgent, $scope.consolDocumentClone.issuingAgentAddress);
                    }
                }



                if (addressKey == 'firstNotify') {
                    if ($scope.consolDocumentClone.firstNotify != undefined && $scope.consolDocumentClone.firstNotify != null && $scope.consolDocumentClone.firstNotify.status != 'Block' && ($scope.consolDocumentClone.firstNotifyAddress == undefined || $scope.consolDocumentClone.firstNotifyAddress == null)) {
                        $scope.consolDocumentClone.firstNotifyAddress = {};
                        $scope.getPartyAddress($scope.consolDocumentClone.firstNotify, $scope.documentDetail.firstNotifyAddress);
                    } else if ($scope.consolDocumentClone.firstNotify != undefined && $scope.consolDocumentClone.firstNotify != null && $scope.consolDocumentClone.firstNotify.status != 'Block') {
                        $scope.getPartyAddress($scope.consolDocumentClone.firstNotify, $scope.documentDetail.firstNotifyAddress);
                    }
                }

                if (addressKey == 'secondNotify') {
                    if ($scope.consolDocumentClone.secondNotify != undefined && $scope.consolDocumentClone.secondNotify != null && $scope.consolDocumentClone.secondNotify.status != 'Block' && ($scope.consolDocumentClone.secondNotifyAddress == undefined || $scope.consolDocumentClone.secondNotifyAddress == null)) {
                        $scope.consolDocumentClone.secondNotifyAddress = {};
                        $scope.getPartyAddress($scope.consolDocumentClone.secondNotify, $scope.consolDocumentClone.secondNotifyAddress);
                    } else if ($scope.consolDocumentClone.secondNotify != undefined && $scope.consolDocumentClone.secondNotify != null && $scope.consolDocumentClone.secondNotify.status != 'Block') {
                        $scope.getPartyAddress($scope.consolDocumentClone.secondNotify, $scope.consolDocumentClone.secondNotifyAddress);
                    }
                }

            }


            $scope.getissuingAgentAddress();

            $scope.getPartyAddress = function (obj, mapToAddrss) {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss, obj, obj.partyAddressList[i]) + $scope.getPhoneandFax(obj);
                        }
                    }
                }
            }

            $scope.getPhoneandFax = function (partyObj) {
                $scope.partyPhandFax = "";
                for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                    if (partyObj.partyAddressList[i].phone != undefined && partyObj.partyAddressList[i].phone != null) {
                        if ($scope.partyPhandFax.includes("PH")) {
                            $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].phone;
                        } else {
                            $scope.partyPhandFax = $scope.partyPhandFax + "  " + "PH:" + partyObj.partyAddressList[i].phone;
                        }
                    }
                }
                for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                    if (partyObj.partyAddressList[i].fax != undefined && partyObj.partyAddressList[i].fax != null) {
                        if ($scope.partyPhandFax.includes("FAX")) {
                            $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].fax;
                        } else {
                            $scope.partyPhandFax = $scope.partyPhandFax + "  " + "FAX:" + partyObj.partyAddressList[i].fax;

                        }
                    }

                }
                return $scope.partyPhandFax;
            }


            if ($scope.consolClone.ppcc != null) {

                if ($scope.consolClone.ppcc == false || $scope.consolClone.ppcc == 'Prepaid') {
                    $scope.consolDocumentClone.chgs = 'P'
                } else {
                    $scope.consolDocumentClone.chgs = 'C'
                }
                if ($scope.consolDocumentClone.chgs == 'P') {
                    $scope.consolDocumentClone.wtValPrepaid = $scope.consolDocumentClone.chgs
                    $scope.consolDocumentClone.othersPrepaid = $scope.consolDocumentClone.chgs
                    $scope.consolDocumentClone.wtValCollect = null;
                    $scope.consolDocumentClone.othersCollect = null;
                } else {
                    $scope.consolDocumentClone.wtValPrepaid = null;
                    $scope.consolDocumentClone.othersPrepaid = null;
                    $scope.consolDocumentClone.wtValCollect = $scope.consolDocumentClone.chgs
                    $scope.consolDocumentClone.othersCollect = $scope.consolDocumentClone.chgs
                }
            }
            /*  if($scope.consolClone.consolDocument.isAgreed==true || $scope.consolClone.consolDocument.isAgreed=='Yes'){
                             $scope.consolDocumentClone.totalAmountClone="AS AGREED";  // Total AMount
                             $scope.consolClone.iataRateClone=null;
                             if($scope.consolDocumentClone.chgs=='P'){
                                 $scope.consolDocumentClone.preWeightChargeClone="AS AGREED";
                                 $scope.consolDocumentClone.totalPrepaidClone="AS AGREED";
                                 $scope.consolDocumentClone.collWeightChargeClone=null;
                                 $scope.consolDocumentClone.totalCollectClone=null;
                             }
                             if($scope.consolDocumentClone.chgs=='C'){
                                 $scope.consolDocumentClone.collWeightChargeClone="AS AGREED";
                                 $scope.consolDocumentClone.totalCollectClone="AS AGREED";    
                                 $scope.consolDocumentClone.preWeightChargeClone=null;
                                 $scope.consolDocumentClone.totalPrepaidClone=null;
                             }
            }*/

            if ($scope.consolDocumentClone != undefined && $scope.consolDocumentClone != null) {

                $scope.consolDocumentClone.accountandNotify = ""
                if ($scope.consolDocumentClone.accountingInformation != undefined && $scope.consolDocumentClone.accountingInformation != null) {
                    $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountingInformation;
                }

                if ($scope.consolDocumentClone.firstNotify != undefined && $scope.consolDocumentClone.firstNotify != null) {

                    $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + '\n' +

                        $scope.consolDocumentClone.firstNotify.partyName + '\n' + $scope.consolDocumentClone.firstNotify.partyAddressList[0].addressLine1 + '\n' +

                        $scope.consolDocumentClone.firstNotify.partyAddressList[0].addressLine2 + '\n' + ""

                    if ($scope.consolDocumentClone.firstNotify.partyAddressList[0].addressLine3 != undefined && $scope.consolDocumentClone.firstNotify.partyAddressList[0].addressLine3 != null)
                        $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + $scope.consolDocumentClone.firstNotify.partyAddressList[0].addressLine3 + '\n' + $scope.getPhoneandFax($scope.consolDocumentClone.firstNotify);

                }

                if ($scope.consolClone != undefined && $scope.consolClone != null) {

                    if ($scope.consolClone.ppcc != undefined && $scope.consolClone.ppcc != null) {
                        if ($scope.consolClone.ppcc == true || $scope.consolClone.ppcc == 'Collect') {
                            $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + '\n' + "Freight : " + 'Collect';
                        } else if ($scope.consolClone.ppcc == false || $scope.consolClone.ppcc == 'Prepaid') {
                            $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + '\n' + "Freight : " + 'Prepaid';
                        } else {

                        }
                    }
                    if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.id != undefined) {

                        if ($scope.consolDocumentClone.issuingAgent.partyAddressList != undefined && $scope.consolDocumentClone.issuingAgent.partyAddressList != null && $scope.consolDocumentClone.issuingAgent.partyAddressList.length > 0) {

                            var isValidTsa = false;
                            var tsaIndex;
                            for (var i = 0; i < $scope.consolDocumentClone.issuingAgent.partyAddressList.length; i++) {
                                if ($scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationNo == undefined ||
                                    $scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationNo == null ||
                                    $scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationNo == "" ||
                                    $scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationDate == undefined ||
                                    $scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationDate == null ||
                                    $scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationDate == "") {
                                    isValidTsa = false;
                                    break;
                                } else {
                                    var todayDate = moment().hour(0).minute(0).second(0).millisecond(0);
                                    var tsaDate;
                                    if (typeof $scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationDate === 'string') {
                                        tsaDate = new Date($scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationDate);
                                        tsaDate = moment(tsaDate);
                                    } else {
                                        tsaDate = moment($scope.consolDocumentClone.issuingAgent.partyAddressList[i].partyCountryField.knownShipperValidationDate);
                                    }
                                    if (tsaDate < todayDate) {
                                        isValidTsa = false;
                                        break;
                                    } else {
                                        isValidTsa = true;
                                        tsaIndex = i;
                                    }
                                }
                            }

                            if ($scope.consolClone.origin.portGroupMaster.countryMaster.countryCode == 'US' && $scope.consolClone.serviceMaster.importExport == 'Export') {
                                if (isValidTsa) {
                                    $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + '\t' + "TSA No:" + $scope.consolDocumentClone.issuingAgent.partyAddressList[tsaIndex].partyCountryField.knownShipperValidationNo;
                                } else {
                                    Notification.error($rootScope.nls["ERR96545"]);
                                    return;
                                }
                            }
                        }

                    }


                    if ($scope.consolClone.directShipment != undefined && $scope.consolClone.directShipment != null && ($scope.consolClone.directShipment == 'Yes' || $scope.consolClone.directShipment == true)) {
                        if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper.partyDetail.spotNo != undefined && $scope.consolDocumentClone.issuingAgent.partyDetail.spotNo != null) {
                            $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + '\t' + "Spot No:" + $scope.consolDocumentClone.issuingAgent.partyDetail.spotNo;
                        }
                    } else if ($scope.consolClone.directShipment != undefined && $scope.consolClone.directShipment != null && ($scope.consolClone.directShipment == 'No' || $scope.consolClone.directShipment == false)) {
                        if ($scope.consolDocumentClone.agent != undefined && $scope.consolDocumentClone.agent != null && $scope.consolDocumentClone.agent.partyDetail.spotNo != undefined && $scope.consolDocumentClone.agent.partyDetail.spotNo != null) {
                            $scope.consolDocumentClone.accountandNotify = $scope.consolDocumentClone.accountandNotify + '\t' + "Spot No:" + $scope.consolDocumentClone.agent.partyDetail.spotNo;
                        }
                    } else {


                    }
                }
            }

            if ($scope.consolDocumentClone != undefined && $scope.consolDocumentClone != null) {

                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent != "" && $scope.consolDocumentClone.issuingAgentAddress != null) {
                    $scope.consolDocumentClone.issuingAgentAddress.addressLine = "";

                    $scope.consolDocumentClone.issuingAgentAddress.addressLine =
                        $scope.consolDocumentClone.issuingAgentAddress.addressLine + '\n' + $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine1 + '\n' +

                        $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine2 + '\n' + ""

                    if ($scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3 != undefined && $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3 != null)
                        $scope.consolDocumentClone.issuingAgentAddress.addressLine = $scope.consolDocumentClone.issuingAgentAddress.addressLine + $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3 + '\n' + $scope.getPhoneandFax($scope.consolDocumentClone.issuingAgent);

                }
            }
            $scope.dimensionLabel = " PCS - L  X   W   X   H" + (($scope.consolDocumentClone.dimensionUnit == true || $scope.consolDocumentClone.dimensionUnit == "INCHORPOUNDS") ? '(inch)' : '(cms)');

            $scope.consolDocumentClone.dimensionClone = "";
            if ($scope.consolDocumentClone.commodityDescription != undefined && $scope.consolDocumentClone.commodityDescription != null)
                $scope.consolDocumentClone.dimensionClone = $scope.consolDocumentClone.commodityDescription;
            if ($scope.consolDocumentClone != undefined && $scope.consolDocumentClone != null && $scope.consolDocumentClone.dimensionList.length > 0) {
                $scope.consolDocumentClone.dimensionClone = $scope.consolDocumentClone.dimensionClone + '\n' + $scope.dimensionLabel;
                if (!isEmptyRow($scope.consolDocumentClone.dimensionList[0])) {
                    for (var i = 0; i < $scope.consolDocumentClone.dimensionList.length; i++) {
                        $scope.consolDocumentClone.dimensionClone = $scope.consolDocumentClone.dimensionClone + '\n' + " " + $scope.consolDocumentClone.dimensionList[i].noOfPiece + "   " + "-" + ' ' + $scope.consolDocumentClone.dimensionList[i].length + ' ' + "X" + ' ' + $scope.consolDocumentClone.dimensionList[i].width + ' ' + "X" + ' ' + $scope.consolDocumentClone.dimensionList[i].height + '\n';
                    }
                } else {
                    $scope.consolDocumentClone.dimensionClone = null;
                }
            }
            $scope.getAccounts(0); //Get accounts for shipper,consignee,Issuing Agent
            if ($scope.consolDocumentClone.issCarrierAgentSign == undefined || $scope.consolDocumentClone.issCarrierAgentSign == null || $scope.consolDocumentClone.issCarrierAgentSign == "") {
                $scope.consolDocumentClone.issCarrierAgentSign = $rootScope.userProfile.employee.employeeName;
            }
            $scope.consolDocumentClone.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;



            if ($scope.consolDocumentClone.routeNo != null && $scope.consolDocumentClone.routeNo != "" && $scope.consolClone.etd != null && $scope.consolClone.etd != "") {

                var etdDate = "";
                if ($scope.isEdit) {
                    etdDate = $scope.consolClone.etd;
                } else {
                    etdDate = $rootScope.dateToString($scope.consolClone.etd);
                }
                $scope.consolClone.flightanddate = $scope.consolDocumentClone.routeNo + "/" + etdDate;
            }

            

            if ($scope.consolClone.connectionList != null && $scope.consolClone.connectionList.length > 0) {

                var length = $scope.consolClone.connectionList.length;
                $scope.cloneConnectionList = {};
                $scope.cloneConnectionList.pod = $scope.consolClone.connectionList[0].pod;
                $scope.cloneConnectionList.carrierMaster = $scope.consolClone.connectionList[0].carrierMaster;

                if ($scope.consolClone.connectionList != null && $scope.consolClone.connectionList.length > 1) {

                    if ($scope.consolClone.connectionList[1] != undefined && $scope.consolClone.connectionList[1].pod != undefined)
                        $scope.cloneConnectionList.pod1 = $scope.consolClone.connectionList[1].pod;

                    if ($scope.consolClone.connectionList[1] != undefined && $scope.consolClone.connectionList[1].carrierMaster != undefined)
                        $scope.cloneConnectionList.carrierMaster1 = $scope.consolClone.connectionList[1].carrierMaster;
                }

                if ($scope.consolClone.connectionList[0] != undefined && $scope.consolClone.connectionList[0].flightVoyageNo != undefined) {
                    var etdDate = "";
                    if ($scope.isEdit) {
                        etdDate = $scope.consolClone.connectionList[0].etd;
                    } else {
                        etdDate = $rootScope.dateToString($scope.consolClone.connectionList[0].etd);
                    }
    
                    $scope.cloneConnectionList.flightanddate0 = $scope.consolClone.connectionList[0].flightVoyageNo + "/" + etdDate;
                }

                if ($scope.consolClone.connectionList[1] != undefined && $scope.consolClone.connectionList[1].flightVoyageNo != undefined) {
                    var etdDate = "";
                    if ($scope.isEdit) {
                        etdDate = $scope.consolClone.connectionList[1].etd;
                    } else {
                        etdDate = $rootScope.dateToString($scope.consolClone.connectionList[1].etd);
                    }

                    $scope.cloneConnectionList.flightanddate = $scope.consolClone.connectionList[1].flightVoyageNo + "/" + etdDate;
                }
            }
            if ($scope.consolDocumentClone.mawbNo != undefined && $scope.consolDocumentClone.mawbNo != null && $scope.consolDocumentClone.mawbNo != "") {
                var MawbNo = $scope.consolDocumentClone.mawbNo.match(/(.{1,3})/g);

                var mawbwithoutPrefix = MawbNo[1] + MawbNo[2] + MawbNo[3]; // having the values of after slicing the carrier num	
                mawbwithoutPrefix = mawbwithoutPrefix.match(/(.{1,4})/g); //having the vli
                $scope.consolDocumentClone.mawbNowithoutPrefix1 = mawbwithoutPrefix[0] + " " + mawbwithoutPrefix[1];
                $scope.consolDocumentClone.mawbNowithoutPrefix2 = MawbNo[0] + "-" + mawbwithoutPrefix[0] + " " + mawbwithoutPrefix[1];
                $scope.consolDocumentClone.mawbNowithoutPrefix3 = MawbNo[0] + "|" + mawbwithoutPrefix[0] + " " + mawbwithoutPrefix[1];

            }
            $scope.agreedChange();
            $scope.isError = false;
            $scope.mainpreloder = false;
            $scope.mawbModal.$promise.then($scope.mawbModal.show);
            $timeout(function () {
                $scope.isError = true;
            }, 500);

        }



        //operation methods

        //update
        $scope.update = function () {
            $scope.mainpreloder = true;
            $scope.printFlag = false;
            $scope.downloadFlag = false;
            if ($scope.isEdit != undefined && !$scope.isEdit) {
                $scope.updateMAWBConsolDetail();
            } else {
                $scope.updateMAWBConsolEdit();
            }
        }

        //download
        $scope.downloadMAWB = function () {
            $scope.mainpreloder = true;
            $scope.downloadFlag = true;
            $scope.printFlag = false;
            if ($scope.isEdit != undefined && !$scope.isEdit) {
                $scope.updateMAWBConsolDetail();
            } else {
                $scope.updateMAWBConsolEdit();
            }
        }
        //print
        $scope.printDynamicMawb = function () {
            $scope.mainpreloder = true;
            $scope.downloadFlag = false;
            $scope.printFlag = true;
            if ($scope.isEdit != undefined && !$scope.isEdit) {
                $scope.updateMAWBConsolDetail();
            } else {
                $scope.updateMAWBConsolEdit();
            }
        }

        //which will execute after update consol from consol_ctl.js (in edit mode)
        $scope.$on('consol-update-for-mawb', function () {

            var routingObj = {};
            routingObj.id = $scope.consolClone.id;
            if ($scope.screen != undefined && $scope.screen == 'AirDocument') {
                routingObj.docType = 'MAWB';
                routingObj.docNo = $scope.consolClone != undefined ? $scope.consolClone.consolDocument.documentNo : null;
            } else {
                routingObj.forMawb = true;
            }

            if ($scope.downloadFlag != undefined && $scope.downloadFlag) {
                previewReportService.printDownload('SHIPMENT_MAWB', 'Download', $scope.consolClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                $scope.routeTo();
            } else if ($scope.printFlag != undefined && $scope.printFlag) {
                previewReportService.printDownload('SHIPMENT_MAWB', 'Print', $scope.consolClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                $scope.routeTo();
            } else if ($scope.downloadFlag != undefined && $scope.printFlag != undefined && !$scope.printFlag && !$scope.downloadFlag) {
                Notification.success($rootScope.nls["ERR401"]);
                $scope.routeTo('update');
            } else {
                console.log("nothing to do");
                $scope.routeTo();
                previewReportService.printDownload('SHIPMENT_MAWB', 'Download', $scope.consolClone.id, null, $scope.isEdit, $scope.screen, routingObj);
            }
        });




        $scope.routeTo = function (opName) {

            $scope.mawbModal.$promise.then($scope.mawbModal.hide);

            if ($scope.screen != undefined && $scope.screen == 'AirDocument') {

                return;
            }

            $scope.mawbShow = {
                consolId: $scope.consolClone.id,
                type: 'MAWB',
                isEdit: $scope.isEdit
            }
            var mawbShowJson = JSON.stringify($scope.mawbShow);
            localStorage.setItem('isMawbShow', mawbShowJson)

            var stateCount = 0;
            if ($stateParams.count != undefined && $stateParams.count != null) {
                stateCount = $stateParams.count + 1;
            }
            if ($scope.isEdit && opName == 'update') {
                $scope.mainpreloder = true;
                $state.go('layout.editAirConsol', {
                    consolId: $scope.consolClone.id,
                    count: stateCount
                });
            } else if (opName == 'update') {
                $scope.mainpreloder = true;
                $state.go('layout.viewNewAirConsol', {
                    consolId: $scope.consolClone.id,
                    count: stateCount
                });
            }
        }



        $scope.calculateTotalAmount = function () {
            if (($scope.consolClone.consolDocument.isAgreed == "No" || $scope.consolClone.consolDocument.isAgreed == false) && $scope.consolClone.iataRateClone != undefined && $scope.consolClone.iataRateClone != null && $scope.consolDocumentClone.chargebleWeight != undefined && $scope.consolDocumentClone.chargebleWeight != null) {
                $scope.consolDocumentClone.totalAmountClone = parseFloat($scope.consolClone.iataRateClone) * parseFloat($scope.consolDocumentClone.chargebleWeight);
                $scope.consolDocumentClone.totalAmountClone = $rootScope.currency($scope.consolClone.currency, parseFloat($scope.consolDocumentClone.totalAmountClone));
            }
            if (($scope.consolClone.consolDocument.isAgreed == false || $scope.consolClone.consolDocument.isAgreed == "No") && $scope.consolClone.iataRateClone == null) {
                $scope.consolDocumentClone.totalAmountClone = null;
            }
            if ($scope.consolDocumentClone.chgs == 'P') {
                $scope.consolDocumentClone.preWeightChargeClone = $scope.consolDocumentClone.totalAmountClone;
            } else {
                $scope.consolDocumentClone.collWeightChargeClone = $scope.consolDocumentClone.totalAmountClone;
            }
            $scope.caluclatePrepaid();
            $scope.caluclateCollect();
        }



        $scope.agreedChange = function () {

            if ($scope.consolClone.consolDocument.isAgreed || $scope.consolClone.consolDocument.isAgreed == 'Yes') {
                $scope.consolDocumentClone.totalPrepaidClone = null;
                $scope.consolDocumentClone.collWeightChargeClone = null;
                $scope.consolDocumentClone.totalCollectClone = null;
                $scope.consolClone.iataRateClone = null;
                $scope.consolDocumentClone.totalAmountClone = "AS AGREED";
                $scope.consolDocumentClone.otherChargesDuePreAgentClone = null;
                $scope.consolDocumentClone.otherChargesDueCollAgentClone = null;
                $scope.consolDocumentClone.otherChargesDuePreCarrierClone = null;
                $scope.consolDocumentClone.otherChargesDueCollCarrierClone = null;
                $scope.consolDocumentClone.preValuationChargeClone = null;
                $scope.consolDocumentClone.collValuationChargeClone = null;
                $scope.consolDocumentClone.collectTaxClone = null;
                $scope.consolDocumentClone.prepaidTaxClone = null;
                $scope.consolClone.chargeListClone = [];
                $scope.consolClone.chargeListClone = new Array(6);
                if ($scope.consolDocumentClone.chgs == 'P') {
                    $scope.consolDocumentClone.preWeightChargeClone = "AS AGREED";
                    $scope.consolDocumentClone.totalPrepaidClone = "AS AGREED";
                    $scope.consolDocumentClone.collWeightChargeClone = null;
                    $scope.consolDocumentClone.totalCollectClone = null;

                }
                if ($scope.consolDocumentClone.chgs == 'C') {
                    $scope.consolDocumentClone.collWeightChargeClone = "AS AGREED";
                    $scope.consolDocumentClone.totalCollectClone = "AS AGREED";
                    $scope.consolDocumentClone.preWeightChargeClone = null;
                    $scope.consolDocumentClone.totalPrepaidClone = null;

                }
            } else {
                $scope.consolDocumentClone.preWeightChargeClone = null;
                $scope.consolDocumentClone.totalPrepaidClone = null;
                $scope.consolDocumentClone.collWeightChargeClone = null;
                $scope.consolDocumentClone.totalCollectClone = null;
                $scope.consolClone.iataRateClone = $scope.consolClone.iataRate;
                $scope.consolDocumentClone.otherChargesDuePreAgentClone = 0.0;
                $scope.consolDocumentClone.otherChargesDueCollAgentClone = 0.0;
                $scope.consolDocumentClone.otherChargesDuePreCarrierClone = 0.0;
                $scope.consolDocumentClone.otherChargesDueCollCarrierClone = 0.0;
                if ($scope.consolClone.chargeList != null && $scope.consolClone.chargeList.length > 0) {
                    if (!isEmptyRow($scope.consolClone.chargeList[0])) {
                        for (var c = 0; c < $scope.consolClone.chargeList.length; c++) {
                            if ($scope.consolClone.chargeList[c].ppcc != undefined && ($scope.consolClone.chargeList[c].ppcc == 'Prepaid' || $scope.consolClone.chargeList[c].ppcc == false)) {
                                if ($scope.consolClone.chargeList[c].due != undefined && $scope.consolClone.chargeList[c].due == 'Agent') {
                                    $scope.consolDocumentClone.otherChargesDuePreAgentClone = parseFloat($scope.consolDocumentClone.otherChargesDuePreAgentClone) + parseFloat($scope.consolClone.chargeList[c].localAmount);
                                } else {
                                    if ($scope.consolClone.chargeList[c].due != undefined)
                                        $scope.consolDocumentClone.otherChargesDuePreCarrierClone = parseFloat($scope.consolDocumentClone.otherChargesDuePreCarrierClone) + parseFloat($scope.consolClone.chargeList[c].localAmount);
                                }
                            } else if ($scope.consolClone.chargeList[c].ppcc != undefined && $scope.consolClone.chargeList[c].ppcc == 'Collect' || $scope.consolClone.chargeList[c].ppcc == false) {

                                if ($scope.consolClone.chargeList[c].due != undefined && $scope.consolClone.chargeList[c].due == 'Agent') {
                                    $scope.consolDocumentClone.otherChargesDueCollAgentClone = parseFloat($scope.consolDocumentClone.otherChargesDueCollAgentClone) + parseFloat($scope.consolClone.chargeList[c].localAmount);
                                } else {
                                    if ($scope.consolClone.chargeList[c].due != undefined)
                                        $scope.consolDocumentClone.otherChargesDueCollCarrierClone = parseFloat($scope.consolDocumentClone.otherChargesDueCollCarrierClone) + parseFloat($scope.consolClone.chargeList[c].localAmount);
                                }
                            }
                        }
                    }
                }
                $scope.calculateTotalAmount();
                $scope.consolDocumentClone.otherChargesDuePreAgentClone = $scope.isZero($scope.consolDocumentClone.otherChargesDuePreAgentClone);
                $scope.consolDocumentClone.otherChargesDueCollAgentClone = $scope.isZero($scope.consolDocumentClone.otherChargesDueCollAgentClone);
                $scope.consolDocumentClone.otherChargesDuePreCarrierClone = $scope.isZero($scope.consolDocumentClone.otherChargesDuePreCarrierClone);
                $scope.consolDocumentClone.otherChargesDueCollCarrierClone = $scope.isZero($scope.consolDocumentClone.otherChargesDueCollCarrierClone);
                $scope.consolClone.chargeListClone = $scope.consolClone.chargeList
            }
            if ($scope.consolClone.isMarksNo == false || $scope.consolClone.isMarksNo == 'No') {
                $scope.consolDocumentClone.isMarksNo = false;
            } else {
                $scope.consolDocumentClone.marksAndNoClone = $scope.consolDocumentClone.marksAndNo;
            }
        };

        $scope.isZero = function (obj) {

            if (obj == 0 || parseFloat(obj) == 0) {
                obj = null;
            }
            return obj;

        }



        $scope.updateList = function (listObject, listKey) {

            var length = $scope.consolClone.connectionList.length;
            if (listKey == 'pod-0') {
                if ($scope.consolClone.connectionList[0] != undefined && $scope.consolClone.connectionList[0].pod != undefined) {
                    $scope.consolClone.connectionList[0].pod = listObject;
                }
                $scope.validateDynamicMAWBDocument(10, 'connCarrier');
            }
            if (listKey == 'carrier') {
                if ($scope.consolClone.connectionList[0] != undefined && $scope.consolClone.connectionList[0].carrierMaster != undefined) {
                    $scope.consolClone.connectionList[0].carrierMaster = listObject;
                    $scope.validateDynamicMAWBDocument(11, 'connPod1');
                }
            }
            if (listKey == 'pod-1') {
                if ($scope.consolClone.connectionList[1] != undefined && $scope.consolClone.connectionList[1].carrierMaster != undefined) {
                    $scope.consolClone.connectionList[1].pod = listObject;
                    $scope.validateDynamicMAWBDocument(12, 'connCarrier1');
                }
            }
            if (listKey == 'carrier1') {
                if ($scope.consolClone.connectionList[1] != undefined && $scope.consolClone.connectionList[1].carrierMaster != undefined) {
                    $scope.consolClone.connectionList[1].carrierMaster = listObject;
                    $scope.validateDynamicMAWBDocument(13, 'locCurrency');
                }
            }

            if (listKey == 'flightanddate') {
                if ($scope.consolClone.connectionList[1] != undefined) {
                    var res = listObject.split("/");
                    $scope.consolClone.connectionList[1].flightVoyageNo = res[0];
                    if ($scope.isEdit != undefined && !$scope.isEdit) {
                        $scope.consolClone.connectionList[1].etd = $rootScope.dateToString(res[1]);
                    } else {
                        $scope.consolClone.connectionList[1].etd = res[1];
                    }
                }
            }

            if (listKey == 'flightanddate0') {
                if ($scope.consolClone.connectionList[0] != undefined) {
                    var res = listObject.split("/");
                    $scope.consolClone.connectionList[0].flightVoyageNo = res[0];
                    if ($scope.isEdit != undefined && !$scope.isEdit) {
                        $scope.consolClone.connectionList[0].etd = $rootScope.dateToString(res[1]);
                    } else {
                        $scope.consolClone.connectionList[0].etd = res[1];
                    }
                }
            }

        }

        $scope.updateflightanddate = function (listObject) {
            var res = listObject.split("/");
            $scope.consolDocumentClone.routeNo = res[0];
            if ($scope.isEdit != undefined && !$scope.isEdit) {
                $scope.consolClone.etd = $rootScope.sendApiDate(res[1]);
            } else {
                $scope.consolClone.etd = res[1];
            }
        }


        $scope.caluclatePrepaid = function () {
            if ($scope.consolClone.consolDocument.isAgreed == 'No' || $scope.consolClone.consolDocument.isAgreed == false) {
                $scope.consolDocumentClone.totalPrepaidClone = parseFloat($scope.consolDocumentClone.preWeightChargeClone == null ? 0 : $scope.consolDocumentClone.preWeightChargeClone) +
                    parseFloat($scope.consolDocumentClone.preValuationCharge == null ? 0 : $scope.consolDocumentClone.preValuationCharge) +
                    parseFloat($scope.consolDocumentClone.prepaidTax == null ? 0 : $scope.consolDocumentClone.prepaidTax) +
                    parseFloat($scope.consolDocumentClone.otherChargesDuePreAgentClone == null ? 0 : $scope.consolDocumentClone.otherChargesDuePreAgentClone) +
                    parseFloat($scope.consolDocumentClone.otherChargesDuePreCarrierClone == null ? 0 : $scope.consolDocumentClone.otherChargesDuePreCarrierClone)
            }
            if ($scope.consolDocumentClone.totalPrepaidClone != null && $scope.consolDocumentClone.totalPrepaidClone == 0) {
                $scope.consolDocumentClone.totalPrepaidClone = null;
            }
        }

        $scope.caluclateCollect = function () {
            if ($scope.consolClone.consolDocument.isAgreed == 'No' || $scope.consolClone.consolDocument.isAgreed == false) {
                $scope.consolDocumentClone.totalCollectClone = parseFloat($scope.consolDocumentClone.collWeightChargeClone == null ? 0 : $scope.consolDocumentClone.collWeightChargeClone) +
                    parseFloat($scope.consolDocumentClone.collValuationCharge == null ? 0 : $scope.consolDocumentClone.collValuationCharge) +
                    parseFloat($scope.consolDocumentClone.collectTax == null ? 0 : $scope.consolDocumentClone.collectTax) +
                    parseFloat($scope.consolDocumentClone.otherChargesDueCollAgentClone == null ? 0 : $scope.consolDocumentClone.otherChargesDueCollAgentClone) +
                    parseFloat($scope.consolDocumentClone.otherChargesDueCollCarrierClone == null ? 0 : $scope.consolDocumentClone.otherChargesDueCollCarrierClone)
            }
            if ($scope.consolDocumentClone.totalCollectClone == 0) {
                $scope.consolDocumentClone.totalCollectClone = null;
            }
        }

        $scope.getAccounts = function (code) {

            if (code == 0 || code == 1) {
                $scope.consolDocumentClone.shipperAccount = null;
                if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper.partyAccountList != null && $scope.consolDocumentClone.shipper.partyAccountList.length > 0) {

                    $scope.consolDocumentClone.shipperAccount = cloneService.clone($scope.consolDocumentClone.shipper.partyAccountList[0].accountMaster.accountCode);
                } else {
                    $scope.consolDocumentClone.shipperAccount = null;
                }

            }

            if (code == 0 || code == 2) {
                $scope.consolDocumentClone.consigneeAccount = null;
                if ($scope.consolDocumentClone.consignee != undefined && $scope.consolDocumentClone.consignee != null && $scope.consolDocumentClone.consignee.partyAccountList != null && $scope.consolDocumentClone.consignee.partyAccountList.length > 0) {

                    $scope.consolDocumentClone.consigneeAccount = $scope.consolDocumentClone.consignee.partyAccountList[0].accountMaster.accountCode;

                } else {

                    $scope.consolDocumentClone.consigneeAccount = null;
                }
            }

            if (code == 0 || code == 3) {

                $scope.consolDocumentClone.issuingAgentAccount = null;

                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.partyAccountList != null && $scope.consolDocumentClone.issuingAgent.partyAccountList.length > 0) {

                    $scope.consolDocumentClone.issuingAgentAccount = $scope.consolDocumentClone.issuingAgent.partyAccountList[0].accountMaster.accountCode;

                } else {

                    $scope.consolDocumentClone.issuingAgentAccount = null;
                }

            }


        }




        // Ajax Calls

        //Ajax calls for dynamic document


        // Preview Document Auto complete  Vlaues
        $scope.ajaxPartyEvent = function (object) {
            console.log("Ajax Party Event method : " + object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PartiesList.query($scope.searchDto).$promise.then(function (data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partyList = data.responseObject.searchResult;
                    //$scope.showPartyList=true;
                    return $scope.partyList;
                }
            },
                function (errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.ajaxPortOfDocDischarge = function (object) {
            console.log("ajaxPortOfDischarge ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocDischargeList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consolDocumentClone.pol == undefined || $scope.consolDocumentClone.pol == null) {
                                $scope.portOfDocDischargeList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consolDocumentClone.pol.id) {
                                        $scope.portOfDocDischargeList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.portOfDocDischargeList;
                        //$scope.showPortOfDocDischargeList=true;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );

        }


        $scope.ajaxPortOfDocDischargeConn = function (object) {
            console.log("ajaxPortOfDischarge ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocDischargeList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            $scope.portOfDocDischargeList = resultList;
                        }
                        return $scope.portOfDocDischargeList;
                        //$scope.showPortOfDocDischargeList=true;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );
        }



        $scope.ajaxPortOfDocLoading = function (object) {
            console.log("ajaxPortOfLoadingEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function (data) {

                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocLoadingList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consolDocumentClone.pod == undefined || $scope.consolDocumentClone.pod == null) {
                                $scope.portOfDocLoadingList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consolDocumentClone.pod.id) {
                                        $scope.portOfDocLoadingList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.portOfDocLoadingList;
                        //$scope.showPortOfDocLoadingList=true;
                    }



                },
                function (errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );

        }

        $scope.ajaxServiceEvent = function (object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ServiceList.fetch($scope.searchDto).$promise.then(function (data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceMasterList = data.responseObject.searchResult;
                    console.log("serviceMasterList ", $scope.serviceMasterList);
                    //$scope.showServiceMasterList=true;
                    return $scope.serviceMasterList;
                }
            },
                function (errResponse) {
                    console.error('Error while fetching ServiceMaster');
                }
            );

        }

        $scope.ajaxCurrencyEvent = function (object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencySearchExclude.fetch({
                "excludeCurrencyCode": $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode
            }, $scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.currencyList = data.responseObject.searchResult;
                        /*$scope.showCurrencyList = true;*/
                        return $scope.currencyList;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Currency');
                }
            );


        }

        $scope.ajaxOriginEvent = function (object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortGroupList.fetch($scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.originList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            /*if($scope.shipment.destination==undefined || $scope.shipment.destination ==null) {
                                $scope.originList=resultList;
                            }
                            else
                                {
                            for(var i = 0; i < resultList.length; i++) {
    
                                    if(resultList[i].id != $scope.shipment.destination.id) {
                                    $scope.originList.push(resultList[i]);
                                    }
    
                            }
                                }*/

                            $scope.originList = resultList;
                        }
                        return $scope.originList;
                        //$scope.showOriginList=true;
                    }



                },
                function (errResponse) {

                    console.error('Error while fetching Origin List');
                }
            );
        }




        $scope.ajaxDestination = function (object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortGroupList.fetch($scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.destinationList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            /*if($scope.shipment.origin==undefined || $scope.shipment.origin ==null) {
                                $scope.destinationList=resultList;
                            }
                            else
                                {
                            for(var i = 0; i < resultList.length; i++) {
    
                                    if(resultList[i].id != $scope.shipment.origin.id) {
                                    $scope.destinationList.push(resultList[i]);
                                    }
    
                            }
                                }*/

                            $scope.destinationList = resultList;
                        }
                        return $scope.destinationList;
                        //$scope.showDestinationList=true;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Origin List');
                }
            );

        }


        $scope.ajaxCarrierEvent = function (object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CarrierByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.carrierMasterList = data.responseObject.searchResult;
                        //$scope.showCarrierMasterList=true;
                        return $scope.carrierMasterList;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Carrier List Based on Air');
                }
            );

        }


        $scope.ajaxTosEvent = function (object) {
            console.log("ajaxTosEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function (data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.tosList = data.responseObject.searchResult;
                    //$scope.showTosList=true;
                    return $scope.tosList
                }
            },
                function (errResponse) {
                    console.error('Error while fetching Tos');
                }
            );;

        }


        $scope.ajaxDocDestination = function (object) {

            console.log("ajaxDestination ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.docDestinationList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            $scope.docDestinationList = resultList;
                        }
                        return $scope.docDestinationList;
                        //$scope.showDocDestinationList=true;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Destination List');
                }
            );

        }


        $scope.partyRender = function (item) {
            return {
                label: item.partyName,
                item: item
            }

        }
        $scope.serviceNameRender = function (item) {
            return {
                label: item.serviceName,
                item: item
            }

        }
        $scope.PortNameRender = function (item) {
            return {
                label: item.portName,
                item: item
            }

        }
        $scope.carrierNameRender = function (item) {
            return {
                label: item.carrierName,
                item: item
            }

        }

        $scope.currencyRender = function (item) {
            return {
                label: item.currencyCode,
                item: item
            }
        }
        $scope.PortCodeRender = function (item) {
            return {
                label: item.portCode,
                item: item
            }

        }
        $scope.carrierCodeRender = function (item) {
            return {
                label: item.carrierCode,
                item: item
            }

        }


        $scope.changeCollPre = function (code) {
            if (code == 0) {
                if ($scope.consolDocumentClone.chgs == 'C' || $scope.consolDocumentClone.chgs == 'c' || $scope.consolDocumentClone.chgs == 'CC' || $scope.consolDocumentClone.chgs == 'cc') {
                    $scope.consolDocumentClone.wtValCollect = $scope.consolDocumentClone.chgs;
                    $scope.consolDocumentClone.othersCollect = $scope.consolDocumentClone.chgs;
                } else {
                    $scope.consolDocumentClone.wtValPrepaid = $scope.consolDocumentClone.chgs;
                    $scope.consolDocumentClone.othersPrepaid = $scope.consolDocumentClone.chgs;
                }
            }
        }




        //validate Dynamic Document


        $scope.validateDynamicMAWBDocument = function (validateCode, nextId) {


            $scope.errorMap = new Map();

            // Carrier NO Validation

            if (validateCode == 0 || validateCode == 1) {

                if ($scope.consolClone.carrier == undefined || $scope.consolClone.carrier == null || $scope.consolClone.carrier == "" || $scope.consolClone.carrier.id == undefined) {
                    $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR9006"]);
                    return false;
                } else {
                    if (ValidateUtil.isStatusBlocked($scope.consolClone.carrier.status)) {
                        $scope.consolClone.carrier = null;
                        $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR9006"]);
                        return false;
                    }
                }
                if ($scope.errorMap.get('shipmentCarrier') != null)
                    $scope.errorMap.remove('shipmentCarrier');
                $scope.navigateToNextField(nextId);
            }


            if (validateCode == 0 || validateCode == 2) {

                if ($scope.consolClone.pol == null ||
                    $scope.consolClone.pol.id == null ||
                    $scope.consolClone.pol.id.trim == "") {
                    $scope.errorMap.put("pol", $rootScope.nls["ERR90041"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consolClone.pol.status)) {
                    $scope.consolClone.pol = null;
                    $scope.errorMap.put("pol", $rootScope.nls["ERR90042"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consolClone.pol.status)) {
                    $scope.consolClone.pol = null;
                    $scope.errorMap.put("pol", $rootScope.nls["ERR90043"]);
                    return false;
                }
                if ($scope.errorMap.get('pol') != null)
                    $scope.errorMap.remove('pol');

                $scope.navigateToNextField(nextId);
            }


            if (validateCode == 0 || validateCode == 3) {
                if ($scope.consolDocumentClone.shipper == undefined || $scope.consolDocumentClone.shipper == null || $scope.consolDocumentClone.shipper.id == undefined || $scope.consolDocumentClone.shipper.id == null) {
                    $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90106"]);
                    $scope.consolDocumentClone.shipperAddress.addressLine1 = null;
                    $scope.consolDocumentClone.shipperAddress.addressLine2 = null;
                    $scope.consolDocumentClone.shipperAddress.addressLine3 = null;
                    $scope.consolDocumentClone.shipperAddress.addressLine4 = null;
                    $scope.showPartyAddressList = false;
                    return false;
                } else {
                    if (ValidateUtil.isStatusBlocked($scope.consolDocumentClone.shipper.status)) {
                        $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90107"]);
                        $scope.consolDocumentClone.shipper = {};
                        $scope.consolDocumentClone.shipperAddress.addressLine1 = null;
                        $scope.consolDocumentClone.shipperAddress.addressLine2 = null;
                        $scope.consolDocumentClone.shipperAddress.addressLine3 = null;
                        $scope.consolDocumentClone.shipperAddress.addressLine4 = null;
                        return false
                    }
                    if (ValidateUtil.isStatusHidden($scope.consolDocumentClone.shipper.status)) {
                        $scope.errorMap.put("documentShipper", $rootScope.nls["ERR90108"]);
                        $scope.consolDocumentClone.shipper = {};
                        $scope.consolDocumentClone.shipperAddress.addressLine1 = null;
                        $scope.consolDocumentClone.shipperAddress.addressLine2 = null;
                        $scope.consolDocumentClone.shipperAddress.addressLine3 = null;
                        $scope.consolDocumentClone.shipperAddress.addressLine4 = null;
                        return false
                    }
                }
                if ($scope.errorMap.get('documentShipper') != null)
                    $scope.errorMap.remove('documentShipper');
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 4) {

                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.id != undefined && $scope.consolDocumentClone.issuingAgent.id != null) {


                    if (ValidateUtil.isStatusBlocked($scope.consolDocumentClone.issuingAgent.status)) {
                        $scope.errorMap.put("partyIssuingAgent", $rootScope.nls["ERR90185"]);
                        $scope.consolDocumentClone.issuingAgent = {};
                        $scope.consolDocumentClone.issuingAgentAddress = {};
                        return false
                    }
                    if (ValidateUtil.isStatusHidden($scope.consolDocumentClone.issuingAgent.status)) {
                        $scope.errorMap.put("partyIssuingAgent", $rootScope.nls["ERR90186"]);
                        $scope.consolDocumentClone.issuingAgent = {};
                        $scope.consolDocumentClone.issuingAgentAddress = {};
                        return false

                    }
                } else {
                    $scope.consolDocumentClone.issuingAgent = null;
                    $scope.consolDocumentClone.issuingAgentAddress = {};
                }
                if ($scope.errorMap.get('partyIssuingAgent') != null)
                    $scope.errorMap.remove('partyIssuingAgent');
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 5) {

                if ($scope.consolClone.carrier != undefined &&
                    $scope.consolClone.carrier != null &&
                    $scope.consolClone.carrier != "" &&
                    $scope.consolClone.carrier.id != null) {


                    if ($scope.consolClone.carrier.status != null) {
                        if (ValidateUtil.isStatusBlocked($scope.consolClone.carrier.status)) {
                            console.log($rootScope.nls["ERR90064"]);
                            $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90064"]);
                            $scope.consolClone.carrier = null;
                            return false
                        }
                        if (ValidateUtil.isStatusHidden($scope.consolClone.carrier.status)) {
                            console.log($rootScope.nls["ERR90065"]);
                            $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90065"]);
                            $scope.consolClone.carrier = null;
                            return false

                        }
                    }

                }
                if ($scope.errorMap.get('shipmentCarrier') != null)
                    $scope.errorMap.remove('shipmentCarrier');
                $scope.navigateToNextField(nextId);
            }


            if (validateCode == 0 || validateCode == 6) {
                if ($scope.consolDocumentClone.consignee == undefined || $scope.consolDocumentClone.consignee == null || $scope.consolDocumentClone.consignee.id == undefined || $scope.consolDocumentClone.consignee.id == null) {
                    $scope.consolDocumentClone.consigneeAddress = {};
                    $scope.errorMap.put("consignee", $rootScope.nls["ERR90132"]);
                    $scope.showPartyAddressList = false;
                    return false;
                } else {
                    if (ValidateUtil.isStatusBlocked($scope.consolDocumentClone.consignee.status)) {
                        $scope.errorMap.put("consignee", $rootScope.nls["ERR90133"]);
                        $scope.consolDocumentClone.consignee = {};
                        $scope.consolDocumentClone.consigneeAddress = {};
                        return false
                    }
                    if (ValidateUtil.isStatusHidden($scope.consolDocumentClone.consignee.status)) {
                        $scope.errorMap.put("consignee", $rootScope.nls["ERR90134"]);
                        $scope.consolDocumentClone.consignee = {};
                        $scope.consolDocumentClone.consigneeAddress = {};
                        return false

                    }
                }
                if ($scope.errorMap.get('consignee') != null)
                    $scope.errorMap.remove('consignee');
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 7) {
                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent.partyDetail.iataCode != null && $scope.consolDocumentClone.issuingAgent.partyDetail.iataCode != "") {
                    var regexp = new RegExp("^[0-9A-Za-z ]{0,30}$");
                    if (!regexp.test($scope.consolDocumentClone.issuingAgent.partyDetail.iataCode)) {
                        $scope.errorMap.put("iataCode", $rootScope.nls["ERR3756"]);
                        return false;
                    }
                }
                if ($scope.errorMap.get('iataCode') != null)
                    $scope.errorMap.remove('iataCode');
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 8) {

                if ($scope.consolDocumentClone.firstNotify != undefined && $scope.consolDocumentClone.firstNotify.id != undefined && $scope.consolDocumentClone.firstNotify != null && $scope.consolDocumentClone.firstNotify != "") {
                    if ($scope.consolDocumentClone.firstNotify.status != null) {
                        if (ValidateUtil.isStatusBlocked($scope.consolDocumentClone.firstNotify.status)) {
                            $scope.errorMap.put("consolDocFirstNotify", $rootScope.nls["ERR95846"]);
                            $scope.consolDocumentClone.firstNotify = {};
                            $scope.consolDocumentClone.firstNotifyAddress = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consolDocumentClone.firstNotify.status)) {
                            $scope.errorMap.put("consolDocFirstNotify", $rootScope.nls["ERR95847"]);
                            $scope.consolDocumentClone.firstNotify = {};
                            $scope.consolDocumentClone.firstNotifyAddress = {};
                            return false;
                        }

                    }

                }
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 9) {
                if ($scope.consolClone.pod == null ||
                    $scope.consolClone.pod.id == null ||
                    $scope.consolClone.pod.id.trim == "") {
                    $scope.errorMap.put("pod", $rootScope.nls["ERR90044"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consolClone.pod.status)) {
                    $scope.consolClone.pod = null;
                    $scope.errorMap.put("pod", $rootScope.nls["ERR90045"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consolClone.pod.status)) {
                    $scope.consolClone.pod = null;
                    $scope.errorMap.put("pod", $rootScope.nls["ERR90046"]);
                    return false;
                }
                if ($scope.errorMap.get('pod') != null)
                    $scope.errorMap.remove('pod');
                $scope.navigateToNextField(nextId);
            }



            if (validateCode == 0 || validateCode == 10) {

                if ($scope.consolClone.connectionList != null && $scope.consolClone.connectionList.length > 0) {

                    if ($scope.consolClone.connectionList[0].pod != undefined && $scope.consolClone.connectionList[0].pod != null && $scope.consolClone.connectionList[0].pod.id != undefined &&
                        $scope.consolClone.connectionList[0].pod.id != null &&
                        $scope.consolClone.connectionList[0].pod.id.trim != "") {

                        if (ValidateUtil.isStatusBlocked($scope.cloneConnectionList.pod.status)) {
                            $scope.consolClone.connectionList[0].pod = null;
                            $scope.cloneConnectionList.pod = null;
                            $scope.errorMap.put("connectionPod", $rootScope.nls["ERR90045"]);
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.cloneConnectionList.pod.status)) {
                            $scope.cloneConnectionList.pod = null;
                            $scope.consolClone.connectionList[0].pod = null;
                            $scope.errorMap.put("connectionPod", $rootScope.nls["ERR90046"]);
                            return false;
                        }
                        if ($scope.errorMap.get('connectionPod') != null)
                            $scope.errorMap.remove('connectionPod');

                    }
                }
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 11) {

                if ($scope.consolClone.connectionList != null && $scope.consolClone.connectionList.length > 0) {

                    if ($scope.consolClone.connectionList[0].carrierMaster != null &&
                        $scope.consolClone.connectionList[0].carrierMaster != undefined &&
                        $scope.consolClone.connectionList[0].carrierMaster.trim != "") {
                        if (ValidateUtil.isStatusBlocked($scope.cloneConnectionList.carrierMaster.status)) {
                            $scope.consolClone.connectionList[0].carrierMaster = null;
                            $scope.cloneConnectionList.carrierMaster = null;
                            $scope.errorMap.put("connctionCarrier", $rootScope.nls["ERR90064"]);
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.cloneConnectionList.carrierMaster.status)) {
                            $scope.cloneConnectionList.carrierMaster = null;
                            $scope.consolClone.connectionList[0].carrierMaster = null;
                            $scope.errorMap.put("connctionCarrier", $rootScope.nls["ERR90064"]);
                            return false;
                        }
                        if ($scope.errorMap.get('connctionCarrier') != null)
                            $scope.errorMap.remove('connctionCarrier');

                    }
                }
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 12) {

                if ($scope.consolClone.connectionList != null && $scope.consolClone.connectionList.length > 1) {

                    if ($scope.consolClone.connectionList[1].pod != null &&
                        $scope.consolClone.connectionList[1].pod != undefined &&
                        $scope.consolClone.connectionList[1].pod != "") {
                        if (ValidateUtil.isStatusBlocked($scope.consolClone.connectionList[1].pod.status)) {
                            $scope.consolClone.connectionList[1].pod = null;
                            $scope.cloneConnectionList.pod1 = null;
                            $scope.errorMap.put("connectionPod1", $rootScope.nls["ERR90045"]);
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consolClone.connectionList[1].pod.status)) {
                            $scope.cloneConnectionList.pod1 = null;
                            $scope.consolClone.connectionList[1].pod = null;
                            $scope.errorMap.put("connectionPod1", $rootScope.nls["ERR90046"]);
                            return false;
                        }
                        if ($scope.errorMap.get('connectionPod1') != null)
                            $scope.errorMap.remove('connectionPod1');
                    }
                }
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 13) {

                if ($scope.consolClone.connectionList != null && $scope.consolClone.connectionList.length > 1) {

                    if ($scope.consolClone.connectionList[1].carrierMaster != undefined && $scope.consolClone.connectionList[1].carrierMaster != null &&
                        $scope.consolClone.connectionList[1].carrierMaster != null &&
                        $scope.consolClone.connectionList[1].carrierMaster != "") {
                        if (ValidateUtil.isStatusBlocked($scope.consolClone.connectionList[1].carrierMaster.status)) {
                            $scope.consolClone.connectionList[1].carrierMaster = null;
                            $scope.cloneConnectionList.carrierMaster1 = null;
                            $scope.errorMap.put("connctionCarrier1", $rootScope.nls["ERR90064"]);
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consolClone.connectionList[1].carrierMaster.status)) {
                            $scope.cloneConnectionList.carrierMaster1 = null;
                            $scope.consolClone.connectionList[1].carrierMaster = null;
                            $scope.errorMap.put("connctionCarrier1", $rootScope.nls["ERR90064"]);
                            return false;
                        }
                        if ($scope.errorMap.get('connctionCarrier1') != null)
                            $scope.errorMap.remove('connctionCarrier1');

                    }
                }
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 14) {

                if ($scope.consolDocumentClone.localCurrency != undefined &&
                    $scope.consolDocumentClone.localCurrency != null &&
                    $scope.consolDocumentClone.localCurrency != "") {

                    if ($scope.consolDocumentClone.localCurrency.status == "Block") {
                        $scope.consolDocumentClone.localCurrency = null;
                        $scope.errorMap.put("consolCurrency", $rootScope.nls["ERR95048"]);
                        return false;
                    }
                    if ($scope.consolDocumentClone.localCurrency.status == "Hide") {
                        $scope.consolDocumentClone.localCurrency = null;
                        $scope.errorMap.put("consolCurrency", $rootScope.nls["ERR95049"]);
                        return false;

                    }
                }
                if ($scope.errorMap.get('consolCurrency') != null)
                    $scope.errorMap.remove('consolCurrency');
                $scope.navigateToNextField(nextId);
            }


            if (validateCode == 0 || validateCode == 15) {
                if ($scope.consolClone.destination == null ||
                    $scope.consolClone.destination.id == null ||
                    $scope.consolClone.destination.id.trim == "") {
                    $scope.errorMap.put("consolDestination", $rootScope.nls["ERR95033"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consolClone.destination.status)) {
                    $scope.consolDocumentClone.destination = null;
                    $scope.errorMap.put("consolDestination", $rootScope.nls["ERR95034"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consolClone.destination.status)) {
                    $scope.consolDocumentClone.destination = null;
                    $scope.errorMap.put("consolDestination", $rootScope.nls["ERR95036"]);
                    return false;
                }
                if ($scope.errorMap.get('consolDestination') != null)
                    $scope.errorMap.remove('consolDestination');
                $scope.navigateToNextField(nextId);
            }


            if (validateCode == 0 || validateCode == 16) {
                if ($scope.consolDocumentClone.noOfPieces == undefined || $scope.consolDocumentClone.noOfPieces == "" || $scope.consolDocumentClone.noOfPieces == null) {
                    $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR90209"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{0,9}$");
                    if (!regexp.test($scope.consolDocumentClone.noOfPieces)) {
                        $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR90209"]);
                        return false;

                    }
                }

                if ($scope.errorMap.get('noOfPieces') != null)
                    $scope.errorMap.remove('noOfPieces');
                $scope.navigateToNextField(nextId);
            }

            if (validateCode == 0 || validateCode == 17) {


                if ($scope.consolDocumentClone.grossWeight == undefined ||
                    $scope.consolDocumentClone.grossWeight == null ||
                    $scope.consolDocumentClone.grossWeight == "") {
                    $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90475"]);
                    return false;
                } else {
                    if ($scope.consolDocumentClone.grossWeight != null && $scope.consolDocumentClone.grossWeight != undefined && $scope.consolDocumentClone.grossWeight != "")
                        if ($scope.consolDocumentClone.grossWeight <= 0 || $scope.consolDocumentClone.grossWeight > 99999999999.99) {
                            $scope.errorMap.put("grossWeightKg", $rootScope.nls["ERR90476"]);
                            return false;
                        }
                }

                if ($scope.errorMap.get('grossWeightKg') != null)
                    $scope.errorMap.remove('grossWeightKg');
                //$scope.consolClone.grossWeight = Math.round($scope.consolClone.grossWeight * 1000) / 1000;
                //$scope.consolClone.grossWeightInPound = Math.round(($scope.consolClone.grossWeight * 2.20462) * 1000) / 1000;
            }

            if (validateCode == 0 || validateCode == 18) {

                if ($scope.consolDocumentClone.rateClass != null && $scope.consolDocumentClone.rateClass != undefined && $scope.consolDocumentClone.rateClass != "") {
                    var regexp = new RegExp("^[0-9A-Za-z]{0,10}$");
                    if (!regexp
                        .test($scope.consolDocumentClone.rateClass)) {
                        $scope.errorMap.put("rateclass", $rootScope.nls["ERR90204"]);
                        return false;
                    }
                }
                if ($scope.errorMap.get('rateclass') != null)
                    $scope.errorMap.remove('rateclass');
            }

            if (validateCode == 0 || validateCode == 19) {

                if ($scope.consolDocumentClone.chargebleWeight == undefined ||
                    $scope.consolDocumentClone.chargebleWeight == null ||
                    $scope.consolDocumentClone.chargebleWeight == "") {
                    $scope.errorMap.put("chargebleweight", $rootScope.nls["ERR22111"]);
                    return false;
                } else {
                    if ($scope.consolDocumentClone.chargebleWeight != null && $scope.consolDocumentClone.chargebleWeight != undefined && $scope.consolDocumentClone.chargebleWeight != "")
                        if ($scope.consolDocumentClone.chargebleWeight <= 0 || $scope.consolDocumentClone.chargebleWeight > 99999999999.999) {
                            $scope.errorMap.put("chargebleweight", $rootScope.nls["ERR22111"]);
                            return false;
                        }

                    var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,3})?|\.[0-9]+)$");
                    if (!regexp
                        .test($scope.consolDocumentClone.chargebleWeight)) {
                        $scope.errorMap.put("chargebleweight", $rootScope.nls["ERR22111"]);
                        return false;
                    }

                }
                if ($scope.errorMap.get('chargebleweight') != null)
                    $scope.errorMap.remove('chargebleweight');
            }

            if (validateCode == 0 || validateCode == 20) {

                if (($scope.consolClone.consolDocument.isAgreed == false || $scope.consolClone.consolDocument.isAgreed == 'No') && $scope.consolClone.iataRateClone != undefined &&
                    $scope.consolClone.iataRateClone != null &&
                    $scope.consolClone.iataRateClone != "") {
                    if ($scope.consolClone.iataRateClone <= 0 || $scope.consolClone.iataRateClone > 9999999999.999) {
                        $scope.errorMap.put("ratePerCharge", $rootScope.nls["ERR90556"]);
                        return false;
                    }
                    var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,3})?|\.[0-9]+)$");

                    if (!regexp
                        .test($scope.consolClone.iataRateClone)) {
                        $scope.errorMap.put("ratePerCharge", $rootScope.nls["ERR90556"]);
                        return false;
                    }
                }
                if ($scope.errorMap.get('ratePerCharge') != null)
                    $scope.errorMap.remove('ratePerCharge');
            }


            if (validateCode == 0 || validateCode == 22) {
                if (($scope.consolClone.consolDocument.isAgreed == 'No' || $scope.consolClone.consolDocument.isAgreed == false) && $scope.consolDocumentClone.totalAmountClone != undefined &&
                    $scope.consolDocumentClone.totalAmountClone != null &&
                    $scope.consolDocumentClone.totalAmountClone != "") {
                    var totalAmount = $scope.consolDocumentClone.totalAmountClone;
                    var calculateAmount = 0.0;
                    if ($scope.consolClone.iataRateClone != undefined && $scope.consolClone.iataRateClone != null && $scope.consolDocumentClone.chargebleWeight != undefined && $scope.consolDocumentClone.chargebleWeight != null) {
                        calculateAmount = parseFloat($scope.consolClone.iataRateClone) * parseFloat($scope.consolDocumentClone.chargebleWeight);

                    }
                    if (totalAmount != calculateAmount) {
                        $scope.errorMap.put("totalAmount", $rootScope.nls["ERR96516"]);
                        return false;
                    }
                }
                if ($scope.errorMap.get('totalAmount') != null)
                    $scope.errorMap.remove('totalAmount');
            }

            if (validateCode == 0 || validateCode == 23) {

                if ($scope.consolCharge != undefined && $scope.consolCharge.chargeMaster != undefined && $scope.consolCharge.chargeMaster != null && $scope.consolCharge.chargeMaster.status != null) {

                    if (ValidateUtil.isStatusBlocked($scope.consolCharge.chargeMaster.status)) {

                        $scope.consolCharge.chargeMaster = null;

                        console.log("Selected Charge is Blocked");

                        $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22073"]);

                        return false
                    }
                    if (ValidateUtil.isStatusHidden($scope.consolCharge.chargeMaster.status)) {
                        $scope.consolCharge.chargeMaster = null;
                        console.log("Selected Charge is Hidden");
                        $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR22074"]);
                        return false
                    }
                }
                if ($scope.errorMap.get('chargeMaster') != null)
                    $scope.errorMap.remove('chargeMaster');
                $scope.navigateToNextField(nextId);
            }


            if (validateCode == 0 || validateCode == 25) {

                if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper != "" &&
                    ($scope.consolDocumentClone.shipperAddress.addressLine1 == null || $scope.consolDocumentClone.shipperAddress.addressLine1 == undefined ||
                        $scope.consolDocumentClone.shipperAddress.addressLine1 == "")) {
                    $scope.errorMap.put("documentShipper", $rootScope.nls["ERR95809"]);
                    return false;
                }
                if ($scope.errorMap.get('documentShipper') != null)
                    $scope.errorMap.remove('documentShipper');
            }

            if (validateCode == 0 || validateCode == 26) {

                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent != "" &&
                    ($scope.consolDocumentClone.issuingAgentAddress.addressLine1 == null || $scope.consolDocumentClone.issuingAgentAddress.addressLine1 == undefined ||
                        $scope.consolDocumentClone.issuingAgentAddress.addressLine1 == "")) {
                    $scope.errorMap.put("partyIssuingAgent", $rootScope.nls["ERR95888"]);
                    return false;
                }
                if ($scope.errorMap.get('partyIssuingAgent') != null)
                    $scope.errorMap.remove('partyIssuingAgent');
            }


            if (validateCode == 0 || validateCode == 27) {

                if ($scope.consolDocumentClone.consignee != undefined && $scope.consolDocumentClone.consignee != null && $scope.consolDocumentClone.consignee != "" &&
                    ($scope.consolDocumentClone.consigneeAddress.addressLine1 == null || $scope.consolDocumentClone.consigneeAddress.addressLine1 == undefined ||
                        $scope.consolDocumentClone.consigneeAddress.addressLine1 == "")) {
                    $scope.errorMap.put("consignee", $rootScope.nls["ERR95836"]);
                    return false;
                }
                if ($scope.errorMap.get('consignee') != null)
                    $scope.errorMap.remove('consignee');
            }


            if (validateCode == 0 || validateCode == 28) {

                if ($scope.consolClone.firstcarrierNo == "undefined" ||
                    $scope.consolClone.firstcarrierNo == null ||
                    $scope.consolClone.firstcarrierNo == "") {
                    $scope.errorMap.put("firstCarrierNo", $rootScope.nls["ERR1214"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[0-9]{3}$");
                    if (!regexp
                        .test($scope.consolClone.firstcarrierNo)) {
                        $scope.errorMap.put("firstCarrierNo", $rootScope.nls["ERR1234"]);
                        return false;
                    }
                }

                if ($scope.errorMap.get('firstCarrierNo') != null)
                    $scope.errorMap.remove('firstCarrierNo');
                $scope.navigateToNextField(nextId);
            }




            return true;

        } //validation ending



        $scope.getissuingAgentAddress = function () {

            if ($scope.consolDocumentClone != undefined && $scope.consolDocumentClone != null && $scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgentAddress != null) {
                $scope.consolDocumentClone.issuingAgentAddress.addressLine = "";

                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent != "") {

                    $scope.consolDocumentClone.issuingAgentAddress.addressLine =
                        $scope.consolDocumentClone.issuingAgentAddress.addressLine + '\n' + $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine1 + '\n' +

                        $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine2 + '\n' + ""

                    if ($scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3 != undefined && $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3 != null)
                        $scope.consolDocumentClone.issuingAgentAddress.addressLine = $scope.consolDocumentClone.issuingAgentAddress.addressLine + $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3 + '\n' + $scope.getPhoneandFax($scope.consolDocumentClone.issuingAgent);

                }
            }

        }


        //get the first carrier based on the carrier no
        $scope.getFirstCarrier = function () {
            var regexp = new RegExp("^[0-9]{3}$");
            if ($scope.consolClone.firstcarrierNo != undefined && $scope.consolClone.firstcarrierNo != null && $scope.consolClone.firstcarrierNo != "") {
                if (!regexp
                    .test($scope.consolClone.firstcarrierNo)) { } else {
                    GetCarrier.get({
                        carrierNo: $scope.consolClone.firstcarrierNo
                    }, function (data) {
                        if (data.responseCode == "ERR0" && data.responseObject != null) {
                            $scope.consolClone.firstCarrier = data.responseObject;
                        } else {
                            $scope.consolClone.firstCarrier = data.responseObject;
                            $scope.errorMap.put("firstCarrierNo", $rootScope.nls["ERR90063"]);
                        }
                    }, function (error) {
                        console.log("Error while getting firstCarrier.", error)
                        $scope.errorMap.put("firstCarrierNo", $rootScope.nls["ERR90063"]);
                    });
                }
            }
            if ($scope.errorMap.get('firstCarrierNo') != null)
                $scope.errorMap.remove('firstCarrierNo');
        }


        $scope.diasbleSubmitBtnFn = function () {
            $scope.disableSubmitBtn = true;
            $timeout(function () {
                $scope.disableSubmitBtn = false;
            }, 3000);
        }

        $scope.updateMAWBConsolEdit = function () {

            try {

                if ($scope.validateDynamicMAWBDocument(0)) {

                    if ($scope.consolClone.consolDocument.isAgreed == false || $scope.consolClone.consolDocument.isAgreed == 'No') {
                        if ($scope.consolClone.iataRateClone != undefined && $scope.consolClone.iataRateClone != null) {
                            $scope.consolClone.iataRate = $scope.consolClone.iataRateClone;
                        }

                        if ($scope.consolDocumentClone.marksAndNoClone != null) {
                            $scope.consolDocumentClone.marksAndNo = $scope.consolDocumentClone.marksAndNoClone;
                        }

                        if ($scope.consolDocumentClone.totalAmountClone != null) {
                            $scope.consolDocumentClone.totalAmount = $scope.consolDocumentClone.totalAmountClone;
                        }
                        if ($scope.consolDocumentClone.preWeightChargeClone != null) {
                            $scope.consolDocumentClone.preWeightCharge = $scope.consolDocumentClone.preWeightChargeClone;
                        }
                        if ($scope.consolDocumentClone.collWeightChargeClone != null) {
                            $scope.consolDocumentClone.collWeightCharge = $scope.consolDocumentClone.collWeightChargeClone;
                        }
                        if ($scope.consolDocumentClone.totalPrepaidClone != null) {
                            $scope.consolDocumentClone.totalPrepaid = $scope.consolDocumentClone.totalPrepaidClone;
                        }
                        if ($scope.consolDocumentClone.totalCollectClone != null) {

                            $scope.consolDocumentClone.totalCollect = $scope.consolDocumentClone.totalCollectClone;
                        }
                        if ($scope.consolDocumentClone.otherChargesDuePreAgentClone != null) {
                            $scope.consolDocumentClone.otherChargesDuePreAgent = $scope.consolDocumentClone.otherChargesDuePreAgentClone;
                        }
                        if ($scope.consolDocumentClone.otherChargesDueCollAgentClone != null) {
                            $scope.consolDocumentClone.otherChargesDueCollAgent = $scope.consolDocumentClone.otherChargesDueCollAgentClone;
                        }
                        if ($scope.consolDocumentClone.otherChargesDuePreCarrierClone) {
                            $scope.consolDocumentClone.otherChargesDuePreCarrier = $scope.consolDocumentClone.otherChargesDuePreCarrierClone;
                        }
                        if ($scope.consolDocumentClone.otherChargesDueCollCarrierClone != null) {
                            $scope.consolDocumentClone.otherChargesDueCollCarrier = $scope.consolDocumentClone.otherChargesDueCollCarrierClone;
                        }
                        $scope.consolDocumentClone.preValuationCharge = $scope.consolDocumentClone.preValuationChargeClone;
                        $scope.consolDocumentClone.prepaidTax = $scope.consolDocumentClone.prepaidTaxClone;
                        $scope.consolDocumentClone.collValuationCharge = $scope.consolDocumentClone.collValuationChargeClone;
                        $scope.consolDocumentClone.collectTax = $scope.consolDocumentClone.collectTaxClone;
                    }

                    if ($scope.consolClone.consolDocument.isAgreed == false || $scope.consolClone.consolDocument.isAgreed == 'No') {
                        $scope.consolClone.chargeList = $scope.consolClone.chargeListClone;
                    }
                    $scope.updateConsol('MAWB', $scope.consolClone);
                } else {

                    $scope.mainpreloder = false;
                    Notification.warning($rootScope.nls["ERR404"]);
                }
            } catch (e) {
                $scope.mainpreloder = false;
                $scope.spinner = false;
                console.log("exception occured in preview report" + e);
            }
        }




        $scope.updateMAWBConsolDetail = function () {

            if ($scope.validateDynamicMAWBDocument(0)) {

                $scope.mainpreloder = true;
                if ($scope.consolClone.consolDocument.isAgreed == false || $scope.consolClone.consolDocument.isAgreed == 'No') {

                    if ($scope.consolClone.iataRateClone != undefined && $scope.consolClone.iataRateClone != null) {
                        $scope.consolClone.iataRate = $scope.consolClone.iataRateClone;
                    }

                    if ($scope.consolDocumentClone.marksAndNoClone != null) {
                        $scope.consolDocumentClone.marksAndNo = $scope.consolDocumentClone.marksAndNoClone;
                    }

                    if ($scope.consolDocumentClone.totalAmountClone != null) {
                        $scope.consolDocumentClone.totalAmount = $scope.consolDocumentClone.totalAmountClone;
                    }
                    if ($scope.consolDocumentClone.preWeightChargeClone != null) {
                        $scope.consolDocumentClone.preWeightCharge = $scope.consolDocumentClone.preWeightChargeClone;
                    }
                    if ($scope.consolDocumentClone.collWeightChargeClone != null) {
                        $scope.consolDocumentClone.collWeightCharge = $scope.consolDocumentClone.collWeightChargeClone;
                    }
                    if ($scope.consolDocumentClone.totalPrepaidClone != null) {
                        $scope.consolDocumentClone.totalPrepaid = $scope.consolDocumentClone.totalPrepaidClone;
                    }
                    if ($scope.consolDocumentClone.totalCollectClone != null) {

                        $scope.consolDocumentClone.totalCollect = $scope.consolDocumentClone.totalCollectClone;
                    }
                    if ($scope.consolDocumentClone.otherChargesDuePreAgentClone != null) {
                        $scope.consolDocumentClone.otherChargesDuePreAgent = $scope.consolDocumentClone.otherChargesDuePreAgentClone;
                    }
                    if ($scope.consolDocumentClone.otherChargesDueCollAgentClone != null) {
                        $scope.consolDocumentClone.otherChargesDueCollAgent = $scope.consolDocumentClone.otherChargesDueCollAgentClone;
                    }
                    if ($scope.consolDocumentClone.otherChargesDuePreCarrierClone) {
                        $scope.consolDocumentClone.otherChargesDuePreCarrier = $scope.consolDocumentClone.otherChargesDuePreCarrierClone;
                    }
                    if ($scope.consolDocumentClone.otherChargesDueCollCarrierClone != null) {
                        $scope.consolDocumentClone.otherChargesDueCollCarrier = $scope.consolDocumentClone.otherChargesDueCollCarrierClone;
                    }
                    $scope.consolDocumentClone.preValuationCharge = $scope.consolDocumentClone.preValuationChargeClone;
                    $scope.consolDocumentClone.prepaidTax = $scope.consolDocumentClone.prepaidTaxClone;
                    $scope.consolDocumentClone.collValuationCharge = $scope.consolDocumentClone.collValuationChargeClone;
                    $scope.consolDocumentClone.collectTax = $scope.consolDocumentClone.collectTaxClone;

                }
                $scope.consolDocumentClone.mawbIssueDate = $rootScope.sendApiStartDateTime($scope.consolDocumentClone.mawbIssueDate);

                if ($scope.consolClone.consolDocument.isAgreed == false || $scope.consolClone.consolDocument.isAgreed == 'No') {
                    $scope.consolClone.chargeList = $scope.consolClone.chargeListClone;
                }
                $scope.tmpObj = {};
                $scope.tmpObj = angular.copy($scope.consolClone);
                ConsolEdit.update($scope.tmpObj).$promise.then(
                    function (data) {
                        if (data.responseCode == 'ERR0') {

                            var routingObj = {};
                            routingObj.id = $scope.consolClone.id;
                            if ($scope.screen != undefined && $scope.screen == 'AirDocument') {
                                routingObj.docType = 'MAWB';
                                routingObj.docNo = $scope.consolClone != undefined ? $scope.consolClone.consolDocument.documentNo : null;
                            } else {
                                routingObj.forMawb = true;
                            }
                            if ($scope.downloadFlag) {
                                previewReportService.printDownload('SHIPMENT_MAWB', 'Download', $scope.consolClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                                $scope.routeTo();
                                //$scope.download();
                            } else if ($scope.printFlag) {
                                previewReportService.printDownload('SHIPMENT_MAWB', 'Print', $scope.consolClone.id, null, $scope.isEdit, $scope.screen, routingObj);
                                $scope.routeTo();
                                //$scope.print();
                            } else {
                                Notification.success($rootScope.nls["ERR401"]);
                                $scope.routeTo('update');
                            }
                        } else {
                            console.log("Consol updated Failed " +
                                data.responseDescription)

                            $scope.mainpreloder = false;
                            Notification.warning($rootScope.nls["ERR403"]);
                            $scope.mawbModal.$promise.then($scope.mawbModal.hide);
                        }
                    },
                    function (error) {
                        $scope.mainpreloder = false;

                        Notification.warning($rootScope.nls["ERR403"]);
                        $scope.mawbModal.$promise.then($scope.mawbModal.hide);
                    });
            } else {
                Notification.warning("validation fails");
                $scope.mainpreloder = false;
                //$scope.mawbModal.$promise.then($scope.mawbModal.hide);

            }
        }

        $scope.addressRearrange = function () {
            var shipperAddress3 = document.getElementById('shipperAddress3').value;
            var shipperAddress4 = document.getElementById('shipperAddress4').value;

            if (shipperAddress3 == undefined || shipperAddress3 == '') {
                document.getElementById('shipperAddress3').value = shipperAddress4;
                document.getElementById('shipperAddress4').value = "";
            }

            var issuingAgentAddress3 = document.getElementById('issuingAgentAddress3').value;
            var issuingAgentAddress4 = document.getElementById('issuingAgentAddress4').value;

            if (issuingAgentAddress3 == undefined || issuingAgentAddress3 == '') {
                document.getElementById('issuingAgentAddress3').value = issuingAgentAddress4;
                document.getElementById('issuingAgentAddress4').value = "";
            }

            var consigneeAddres3 = document.getElementById('consigneeAddres3').value;
            var consigneeAddres4 = document.getElementById('consigneeAddres4').value;

            if (consigneeAddres3 == undefined || consigneeAddres3 == '') {
                document.getElementById('consigneeAddres3').value = consigneeAddres4;
                document.getElementById('consigneeAddres4').value = "";
            }

            var issuingAgentAddress13 = document.getElementById('issuingAgentAddress13').value;
            var issuingAgentAddress14 = document.getElementById('issuingAgentAddress14').value;

            if (issuingAgentAddress13 == undefined || issuingAgentAddress13 == '') {
                document.getElementById('issuingAgentAddress13').value = issuingAgentAddress14;
                document.getElementById('issuingAgentAddress14').value = "";
            }


            /*if($scope.consolDocumentClone.dimensionUnit){
                var grossWeight=document.getElementById('grossweight').value;
                if(!isNaN(parseFloat(grossWeight)) && grossWeight!=undefined && grossWeight!=null && grossWeight!=''){
                    document.getElementById('grossweight').value = parseFloat($scope.consolDocumentClone.grossWeightInPound).toFixed(2);
                }
            }else{
                var grossWeight=document.getElementById('grossweight').value;
                if(!isNaN(parseFloat(grossWeight)) &&  grossWeight!=undefined && grossWeight!=null && grossWeight!=''){
                    document.getElementById('grossweight').value = parseFloat($scope.consolDocumentClone.grossWeight).toFixed(2);
                }
            }
        	
    
            if($scope.consolDocumentClone.dimensionUnit){
                var grossWeight=document.getElementById('totGross').value;
                if(!isNaN(parseFloat(grossWeight)) &&  grossWeight!=undefined && grossWeight!=null && grossWeight!=''){
                    document.getElementById('totGross').value = parseFloat($scope.consolDocumentClone.grossWeightInPound).toFixed(2);
                }
            }else{
                var grossWeight=document.getElementById('totGross').value;
                if(!isNaN(parseFloat(grossWeight))&&  grossWeight!=undefined && grossWeight!=null && grossWeight!=''){
                    document.getElementById('totGross').value = parseFloat($scope.consolDocumentClone.grossWeight).toFixed(2);
                }
            }
        	
        	
        	
        	
        	
        	
        	
        	
            var chargebleWeight=document.getElementById('chargeWeight').value;
            if(!isNaN(parseFloat(chargebleWeight)) &&  chargebleWeight!=undefined && chargebleWeight!=null && chargebleWeight!=''){
                document.getElementById('chargeWeight').value = parseFloat($scope.consolDocumentClone.chargebleWeight).toFixed(2);
            }
        	
        	
            var iataRate = document.getElementById('ratePerCharge').value;
            if(!isNaN(parseFloat(iataRate)) &&  iataRate!=undefined && iataRate!=null && iataRate!=''){
                document.getElementById('ratePerCharge').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(iataRate));
            }
        	
            var totalAmount = document.getElementById('totalAmount').value;
            if(!isNaN(parseFloat(totalAmount)) && totalAmount!=undefined && totalAmount!=null && totalAmount!=''){
                document.getElementById('totalAmount').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(totalAmount));
            }
        	
            var totalAmount1 = document.getElementById('totalAmount1').value;
            if(!isNaN(parseFloat(totalAmount1)) && totalAmount1!=undefined && totalAmount1!=null && totalAmount1!=''){
                document.getElementById('totalAmount1').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(totalAmount1));
            }
        	
            var preWeightChargeClone = document.getElementById('preWeightChargeClone').value;
            if(!isNaN(parseFloat(preWeightChargeClone)) && preWeightChargeClone!=undefined && preWeightChargeClone!=null && preWeightChargeClone!=''){
                document.getElementById('preWeightChargeClone').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(preWeightChargeClone));
            }
        	
            var collWeightChargeClone = document.getElementById('collWeightChargeClone').value;
            if(!isNaN(parseFloat(collWeightChargeClone)) && collWeightChargeClone!=undefined && collWeightChargeClone!=null && collWeightChargeClone!=''){
                document.getElementById('collWeightChargeClone').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(collWeightChargeClone));
            }
        	
            var preValuationCharge = document.getElementById('preValuationCharge').value;
            if(!isNaN(parseFloat(preValuationCharge)) &&  preValuationCharge!=undefined && preValuationCharge!=null && preValuationCharge!=''){
                document.getElementById('preValuationCharge').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(preValuationCharge));
            }
        	
            var collValuationCharge = document.getElementById('collValuationCharge').value;
            if(!isNaN(parseFloat(collValuationCharge)) && collValuationCharge!=undefined && collValuationCharge!=null && collValuationCharge!=''){
                document.getElementById('collValuationCharge').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(collValuationCharge));
            }
        	
            var prepaidTax = document.getElementById('prepaidTax').value;
            if(prepaidTax!=undefined && prepaidTax!=null &&!isNaN(parseFloat(prepaidTax))&&prepaidTax!=''){
                document.getElementById('prepaidTax').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(prepaidTax));
            }
        	
            var collectTax = document.getElementById('collectTax').value;
            if(collectTax!=undefined && collectTax!=null && !isNaN(parseFloat(collectTax))&& collectTax!=''){
                document.getElementById('collectTax').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(collectTax));
            }
        	
            var otherChargesDuePreAgent = document.getElementById('otherChargesDuePreAgent').value;
            if(otherChargesDuePreAgent!=undefined && otherChargesDuePreAgent!=null && !isNaN(parseFloat(otherChargesDuePreAgent))&&  otherChargesDuePreAgent!=''){
                document.getElementById('otherChargesDuePreAgent').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(otherChargesDuePreAgent));
            }
        	
            var otherChargesDueCollAgent = document.getElementById('otherChargesDueCollAgent').value;
            if(otherChargesDueCollAgent!=undefined && otherChargesDueCollAgent!=null && !isNaN(parseFloat(otherChargesDueCollAgent))&& otherChargesDueCollAgent!=''){
                document.getElementById('otherChargesDueCollAgent').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(otherChargesDueCollAgent));
            }
        	
            var otherChargesDuePreCarrier = document.getElementById('otherChargesDuePreCarrier').value;
            if(otherChargesDuePreCarrier!=undefined && otherChargesDuePreCarrier!=null && !isNaN(parseFloat(otherChargesDuePreCarrier))&& otherChargesDuePreCarrier!=''){
                document.getElementById('otherChargesDuePreCarrier').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(otherChargesDuePreCarrier));
            }
        	
            var otherChargesDueCollCarrier = document.getElementById('otherChargesDueCollCarrier').value;
            if(otherChargesDueCollCarrier!=undefined && otherChargesDueCollCarrier!=null &&  !isNaN(parseFloat(otherChargesDueCollCarrier))&&  otherChargesDueCollCarrier!=''){
                document.getElementById('otherChargesDueCollCarrier').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(otherChargesDueCollCarrier));
            }
        	
            var totalPrepaidClone = document.getElementById('totalPrepaidClone').value;
            if(totalPrepaidClone!=undefined && totalPrepaidClone!=null &&!isNaN(parseFloat(totalPrepaidClone))&&   totalPrepaidClone!=''){
                document.getElementById('totalPrepaidClone').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(totalPrepaidClone));
            }
        	
            var totalCollectClone = document.getElementById('totalCollectClone').value;
            if(totalCollectClone!=undefined && totalCollectClone!=null &&!isNaN(parseFloat(totalCollectClone))&& totalCollectClone!=''){
                document.getElementById('totalCollectClone').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(totalCollectClone));
            }
            */

            /*var currencyConversionRates = document.getElementById('currencyConversionRates').value;
            if(currencyConversionRates!=undefined && currencyConversionRates!=null && !isNaN(parseFloat(currencyConversionRates))&&  currencyConversionRates!=''){
                document.getElementById('currencyConversionRates').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(currencyConversionRates));
            }
        	
            var chargesInDestCurrency = document.getElementById('chargesInDestCurrency').value;
            if(chargesInDestCurrency!=undefined && chargesInDestCurrency!=null && !isNaN(parseFloat(chargesInDestCurrency)) &&chargesInDestCurrency!=''){
                document.getElementById('chargesInDestCurrency').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(chargesInDestCurrency));
            }
        	
            var chargesAtDestination = document.getElementById('chargesAtDestination').value;
            if(chargesAtDestination!=undefined && chargesAtDestination!=null && !isNaN(parseFloat(chargesAtDestination)) && chargesAtDestination!=''){
                document.getElementById('chargesAtDestination').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(chargesAtDestination));
            }
        	
            var totalCollectCharges = document.getElementById('totalCollectCharges').value;
            if(totalCollectCharges!=undefined && totalCollectCharges!=null && !isNaN(parseFloat(totalCollectCharges)) && totalCollectCharges!=''){
                document.getElementById('totalCollectCharges').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(totalCollectCharges));
            }
        	
            if(document.getElementById('locChargeLeft0')!=null){
                var locChargeLeft0 = document.getElementById('locChargeLeft0').value;
                if(locChargeLeft0!=undefined && locChargeLeft0!=null &&!isNaN(parseFloat(locChargeLeft0) ) &&locChargeLeft0!=''){
                    document.getElementById('locChargeLeft0').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(locChargeLeft0));
                }
            }*/

            /*	if(document.getElementById('locChargeLeft1')!=null){
                    var locChargeLeft1 = document.getElementById('locChargeLeft1').value;
                    if(locChargeLeft1!=undefined && locChargeLeft1!=null && !isNaN(parseFloat(locChargeLeft1))  && locChargeLeft1!=''){
                        document.getElementById('locChargeLeft1').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(locChargeLeft1));
                    }
                }
            	
                if(document.getElementById('locChargeLeft2')!=null){
                    var locChargeLeft2 = document.getElementById('locChargeLeft2').value;
                    if(locChargeLeft2!=undefined && locChargeLeft2!=null && !isNaN(parseFloat(locChargeLeft2))  && locChargeLeft2!=''){
                        document.getElementById('locChargeLeft2').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(locChargeLeft2));
                    }
                }
            	
                if(document.getElementById('locChargeRight0')!=null){
                    var locChargeRight0 = document.getElementById('locChargeRight0').value;
                    if(locChargeRight0!=undefined && locChargeRight0!=null && !isNaN(parseFloat(locChargeRight0))  && locChargeRight0!=''){
                        document.getElementById('locChargeRight0').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(locChargeRight0));
                    }
                }
            	
                if(document.getElementById('locChargeRight1')!=null){
                    var locChargeRight1 = document.getElementById('locChargeRight1').value;
                    if(locChargeRight1!=undefined && locChargeRight1!=null &&  !isNaN(parseFloat(locChargeRight1))  &&  locChargeRight1!=''){
                        document.getElementById('locChargeRight1').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(locChargeRight1));
                    }
                }
                if(document.getElementById('locChargeRight2')!=null){
                    var locChargeRight2 = document.getElementById('locChargeRight2').value;
                    if(locChargeRight2!=undefined && locChargeRight2!=null && !isNaN(parseFloat(locChargeRight2))  && locChargeRight2!=''){
                        document.getElementById('locChargeRight2').value = $rootScope.currencyFormat($scope.consolClone.currency,parseFloat(locChargeRight2));
                    }
                }*/

        }




        //Ajax calls

        $scope.ajaxPortOfDocLoadingDynamic = function (object) {
            console.log("ajaxPortOfLoadingEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocLoadingList = [];
                        var resultList = data.responseObject.searchResult;
                        return resultList;
                        //$scope.showPortOfDocLoadingList=true;
                    }



                },
                function (errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );

        }

        $scope.ajaxDestinationDynamic = function (object) {
            /*if($scope.shipment.origin==undefined || $scope.shipment.origin ==null) {
                    $scope.destinationList=resultList;
                    }
                    else
                        {
                    for(var i = 0; i < resultList.length; i++) {
    
                            if(resultList[i].id != $scope.shipment.origin.id) {
                            $scope.destinationList.push(resultList[i]);
                            }
    
                    }
                        }*/
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortGroupList.fetch($scope.searchDto).$promise.then(
                function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.destinationList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            $scope.destinationList = resultList;
                        }
                        return $scope.destinationList;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Origin List');
                }
            );
        }


        $scope.getPhoneandFax = function (partyObj) {
            $scope.partyPhandFax = "";
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].phone != undefined && partyObj.partyAddressList[i].phone != null) {
                    if ($scope.partyPhandFax.includes("PH")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].phone;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "  " + "PH:" + partyObj.partyAddressList[i].phone;
                    }
                }
            }
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].fax != undefined && partyObj.partyAddressList[i].fax != null) {
                    if ($scope.partyPhandFax.includes("FAX")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].fax;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "  " + "FAX:" + partyObj.partyAddressList[i].fax;

                    }
                }

            }
            return $scope.partyPhandFax;
        }


        $scope.range = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                if ($scope.consolClone.chargeListClone != undefined && $scope.consolClone.chargeListClone[i] != undefined) {

                    var obj = $scope.consolClone.chargeListClone[i];

                    input.push(obj);
                }
            }
            return input;
        }


        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }




        $scope.ajaxChargeEvent = function (object) {
            console.log("ajaxChargeEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function (data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.chargeMasterList = data.responseObject.searchResult;
                    return $scope.chargeMasterList;
                    /* $scope.showChargeMasterList=true; */
                }
            },
                function (errResponse) {
                    console.error('Error while fetching Charge');
                }
            );
        }

        $scope.navigateToNextField = function (id) {
            if (id != undefined && id != null && id != "")
                document.getElementById(id).focus();
        }


        $scope.PortCodeRender = function (item) {
            return {
                label: item.portCode,
                item: item
            }

        }

        $scope.carrierNameRender = function (item) {
            return {
                label: item.carrierName,
                item: item
            }

        }

        $scope.carrierCodeRender = function (item) {
            return {
                label: item.carrierCode,
                item: item
            }
        }

        function isEmptyRow(obj) {
            //return (Object.getOwnPropertyNames(obj).length === 0);
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }

            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;

                }
            }
            return isempty;
        }


        try {
            var isMawbShow = JSON.parse(localStorage.getItem('isMawbShow'));
            if (isMawbShow != undefined && isMawbShow != null) {
                localStorage.removeItem('isMawbShow');
                ConsolGet.get({
                    id: isMawbShow.consolId
                }, function (data) {
                    if (data.responseCode == 'ERR0') {
                        var consol = data.responseObject;
                        if (isMawbShow.isEdit || isMawbShow.isEdit == 'true') {
                            consol.etd = $rootScope.dateToString(consol.etd);
                            if (consol.connectionList != undefined &&
                                consol.connectionList != null &&
                                consol.connectionList.length > 0) {
                                var length = (consol.connectionList.length - 1);
                                consol.connectionList[length].etd = $rootScope.dateToString(consol.connectionList[length].etd)
                            }
                        }
                        $scope.previewReport(consol, 'MAWB', isMawbShow.isEdit);
                    } else {
                        console.log("Consol get Failed :")
                        $scope.mainpreloder = false;
                        Notification.warning($rootScope.nls["ERR404"]);
                    }
                },
                    function (error) {
                        console.log("Consol get Failed : ")
                        Notification.warning($rootScope.nls["ERR404"]);
                        $scope.mainpreloder = false;
                    });
            }
        } catch (e) {
            $scope.mainpreloder = false;
            console.log("exception in hawb show")
        }


    }]);