app.controller('iataCtrl',['$scope', '$location', '$window', 'ngDialog', '$state', '$modal', 'IataRateView', '$stateParams', 
        'cloneService', 'IataRateSearch', '$rootScope', 'RecentHistorySaveService', 'downloadFactory', 'roleConstant',
    function($scope, $location, $window, ngDialog, $state, $modal, IataRateView, $stateParams, 
        cloneService, IataRateSearch, $rootScope, RecentHistorySaveService, downloadFactory, roleConstant) {

    $scope.$roleConstant = roleConstant;
    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.iataratesairHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false
        },

        {
            "name": "Carrier",
            "width": "col-xs-4half",
            "model": "carrierMaster.carrierName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchCarrier"

        },
        {
            "name": "Airport of Loading",
            "width": "col-xs-3",
            "model": "pol.portName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchPol"
        },
        {
            "name": "Valid From",
            "width": "col-xs-2",
            "model": "validFromDate",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "searchValidFromDate"
        },
        {
            "name": "Valid To",
            "width": "col-xs-2",
            "model": "validToDate",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "searchValidToDate"
        }
    ];

    $scope.sortSelection = {
        sortKey: "carrierMaster.carrierName",
        sortOrder: "asc"
    };
    $scope.page = 0;
    $scope.limit = 10;



    $scope.cancel = function() {
        $scope.showDetail = false;
        $scope.showLogs = false;
        $scope.showHistory = false;
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };
    $scope.back = function() {
        $scope.showDetail = false;
    };
    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_CREATE)) {
            $rootScope.clientMessage = null;
            $state.go("layout.iataRatesAirAdd");
        }
    };

    $scope.attachConfig = {
        "isEdit": false, //false for view only mode
        "isDelete": true, //
        "page": "",
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file"
            }
        ]

    };

    $scope.init = function() {

        $scope.setSearch('active');
        $scope.IataRateMaster = {};
    }


    $scope.editIataRate = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_MODIFY)) {
            console.log("Edit editIataRate Button is Pressed....")
            var param = {
                iataRateId: $scope.iataRateMaster.id
            };
            console.log("State Parameters :: ", param);
            $state.go("layout.iataRatesAirEdit", param);
        }
    }

    $scope.tabViewRoles = function(tab) {
        if (tab == 'IATARates' && $rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_IATA_RATES_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'Attachments' && $rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_ATTACHMENT_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'Remarks' && $rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_REMARKS_VIEW)) {
            $scope.Tabs = tab;
        }

    }

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_VIEW)) {
            $rootScope.clientMessage = null;
            $scope.detailPageFlag = true;
            $scope.selectedRowIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            //  $scope.selectedRecordIndex = index;
            var param = {
                iataRateId: data.id,
                iataRateIdx: index,
                searchFlag: $scope.searchFlag,
                selectedPageNumber: $scope.selectedRowIndex,
                totalRecord: $scope.totalRecord
            };

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }

            /********************* ux fix ends here *****************************/

            $state.go("layout.iataRatesAirView", $scope.searchDtoToStateParams(param));
        }
    };

    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.iataRateIdx;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                //searchCarrier
                if ($scope.searchDto.searchCarrier != undefined && $scope.searchDto.searchCarrier != null) {
                    param.searchCarrier = $scope.searchDto.searchCarrier;
                }
                //pol
                if ($scope.searchDto.searchPol != undefined && $scope.searchDto.searchPol != null) {
                    param.searchPol = $scope.searchDto.searchPol;
                }
                //Valid from Date
                if ($scope.searchDto.searchValidFromDate != undefined && $scope.searchDto.searchValidFromDate != null) {
                    param.searchValidFromDate = $rootScope.sendApiDateAndTime($scope.searchDto.searchValidFromDate);

                }
                //Valid from Date
                if ($scope.searchDto.searchValidToDate != undefined && $scope.searchDto.searchValidToDate != null) {
                    param.searchValidToDate = $rootScope.sendApiDateAndTime($scope.searchDto.searchValidToDate);

                }


            }

        }
        return param;
    }

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;

            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchCarrier != undefined && $stateParams.searchCarrier != null) {
                stateParameters.searchCarrier = $scope.searchDto.searchCarrier = $stateParams.recordPerPage;
            }
            if ($stateParams.searchPol != undefined && $stateParams.searchPol != null) {
                stateParameters.searchPol = $scope.searchDto.recordPerPage = $stateParams.searchPol;
            }
            if ($stateParams.searchValidFromDate != undefined && $stateParams.searchValidFromDate != null) {
                stateParameters.searchValidFromDate = $scope.searchDto.searchValidFromDate = $stateParams.recordPerPage;
            }
            if ($stateParams.searchValidToDate != undefined && $stateParams.searchValidToDate != null) {
                stateParameters.searchValidToDate = $scope.searchDto.searchValidToDate = $stateParams.recordPerPage;
            }



        } else {
            return;
        }
        IataRateSearch.query($scope.searchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;

            stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.iataRateId = data.responseObject.searchResult[0].id;
            stateParameters.totalRecord = $scope.totalRecord;
            $state.go("layout.iataRatesAirView", stateParameters);
        });
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search($scope.page, $scope.limit);
    }

    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search($scope.page, $scope.limit);
    }

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search($scope.page, $scope.limit);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search($scope.page, $scope.limit);
    }
    $scope.back = function() {
        $scope.setSearch('active');
        $scope.iataRateMaster = {};
        $state.go('layout.iataRatesAir');
    };

    $scope.setSearch = function(value) {
        $scope.searchFlag = value;
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search($scope.page, $scope.limit);
    }

    //  $scope.attachData= [{"sno":"1","refNo":"DOC1","fileName":"Packaging List.png"}];

    //attachment

    $scope.attachConfig = {
        "isEdit": false,
        "isDelete": false,
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                "width": "w300px"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file",
                "width": "w200px"
            },

        ]

    }


    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/iatarate/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }


    $scope.populateAttachment = function() {
        console.log("Populating Attachment....");
        $scope.attachConfig.data = [];
        if ($scope.iataRateMaster.attachmentList != null && $scope.iataRateMaster.attachmentList.length != 0) {

            for (var i = 0; i < $scope.iataRateMaster.attachmentList.length; i++) {

                var displayObject = new Object();
                displayObject.id = $scope.iataRateMaster.attachmentList[i].id;
                displayObject.refNo = $scope.iataRateMaster.attachmentList[i].refNo;
                displayObject.fileName = $scope.iataRateMaster.attachmentList[i].fileName;
                displayObject.file = $scope.iataRateMaster.attachmentList[i].file;
                displayObject.fileContentType = $scope.iataRateMaster.attachmentList[i].fileContentType;
                displayObject.systemTrack = $scope.iataRateMaster.attachmentList[i].systemTrack;
                displayObject.versionLock = $scope.iataRateMaster.attachmentList[i].versionLock;

                $scope.attachConfig.data.push(displayObject);
            }
        }
    }
    $scope.search = function(selectedPageNumber, recordPerPage) {
        $scope.iataRateMaster = {};
        $scope.searchDto.selectedPageNumber = selectedPageNumber;
        $scope.searchDto.recordPerPage = recordPerPage;
        $scope.tmpSearchDto = cloneService.clone($scope.searchDto);



        if ($scope.tmpSearchDto.searchValidFromDate != null) {
            $scope.tmpSearchDto.searchValidFromDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchValidFromDate.startDate);
            $scope.tmpSearchDto.searchValidFromDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchValidFromDate.endDate);
        }

        if ($scope.tmpSearchDto.searchValidToDate != null) {
            $scope.tmpSearchDto.searchValidToDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchValidToDate.startDate);
            $scope.tmpSearchDto.searchValidToDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchValidToDate.endDate);
        }



        $scope.serviceArr = [];
        IataRateSearch.query({
            "searchFlag": $scope.searchFlag
        }, $scope.tmpSearchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.iataratesArr = resultArr;
        });
    }

    $scope.viewById = function(id) {
        IataRateView.get({
            id: id
        }, function(data) {
            $rootScope.setNavigate1("CRM");
            $rootScope.setNavigate2("IATA Rates - Air");
            $scope.populateIataRateData(data);
        }, function(error) {
            console.log("IATA Rates - Air view Failed : " + error)
        });
    }

    $scope.populateIataRateData = function(data) {
        if (data.responseCode == "ERR0") {
            console.log("iataRate view Successfully")
            $scope.iataRateMaster = data.responseObject;

            $rootScope.setNavigate1("CRM");
            $rootScope.setNavigate2("IATA Rates - Air");

            if ($scope.iataRateMaster != undefined && $scope.iataRateMaster != null && $scope.iataRateMaster.carrierMaster != undefined && $scope.iataRateMaster.carrierMaster != null &&
                $scope.iataRateMaster.carrierMaster.carrierName != undefined && $scope.iataRateMaster.carrierMaster.carrierName != null) {
                $rootScope.subTitle = $scope.iataRateMaster.carrierMaster.carrierName;
            }

            $rootScope.category = "IATA Rates - Air";
            $rootScope.unfinishedFormTitle = "IATA Rates - Air View # " + $scope.iataRateMaster.id;
            var rHistoryObj = {
                'title': 'IATA Rates - Air View #' + $scope.iataRateMaster.id,
                'subTitle': $rootScope.subTitle,
                'stateName': $state.current.name,
                'stateParam': JSON.stringify($stateParams),
                'stateCategory': 'IATA Rates - Air'

            }

            RecentHistorySaveService.form(rHistoryObj);


        } else {
            console.log("IATA Rates - Air view Failed " + data.responseDescription)
        }

    }


    switch ($stateParams.action) {
        case "VIEW":
            $rootScope.unfinishedFormTitle = "Iata Rate View # " + $stateParams.iataRateId;
            $rootScope.unfinishedData = undefined;
            $scope.selectedTabIndex = 0;

            if ($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
                $scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
            }
            if ($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
                $scope.totalRecord = parseInt($stateParams.totalRecord);
            }
            if ($stateParams.iataRateId != undefined && $stateParams.iataRateId != null) {
                $scope.viewById($stateParams.iataRateId);
            }


            // $scope.view($stateParams.enquiryId);
            $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.iataRatesAir"
            }, {
                label: "IATA Rates - Air",
                state: "layout.iataRatesAir"
            }, {
                label: "View IATA Rates - Air",
                state: null
            }];
            break;
        case "SEARCH":
            console.log("I am In IATA Rates - Air List Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.iataRatesAir"
            }, {
                label: "IATA Rates - Air",
                state: null
            }];
            break;
        default:

    }

}]);