/**
 * Created by saravanan on 31/10/16.
 */
app.controller('iataEditCtrl',['$rootScope', '$state', '$stateParams', '$timeout', '$scope', '$http',
    '$location', '$modal', 'ngDialog', 'ngProgressFactory', 'CurrencyMasterSearchKeyword', 
    'CarrierSearchKeyword', 'PortByTransportMode', 'IataRateSave', 'IataRateEdit', 'cloneService', 
    'IataRateView', 'appConstant', 'CommonValidationService', 'roleConstant',
    function($rootScope, $state, $stateParams, $timeout, $scope, $http,
    $location, $modal, ngDialog, ngProgressFactory, CurrencyMasterSearchKeyword, 
    CarrierSearchKeyword, PortByTransportMode, IataRateSave, IataRateEdit, cloneService, 
    IataRateView, appConstant, CommonValidationService, roleConstant) {

    $scope.$roleConstant = roleConstant;
    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";

    $scope.contained_progressbar = ngProgressFactory
        .createInstance();
    $scope.contained_progressbar.setParent(document
        .getElementById('iatarate-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.disableSubmitBtn = false;
    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            console.log("Enable btn ,", new Date());
            $scope.disableSubmitBtn = false;
        }, 300);
    }


    $scope.attachConfig = {
        "isEdit": true, //false for view only mode
        "isDelete": true, //
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file"
            },
            {
                "name": "Actions",
                "model": "action",
                "type": "action"
            }
        ]

    };

    $scope.attachData = [{
        "sno": "1",
        "refNo": "DOC1",
        "fileName": "Packaging List.png"
    }];


    $scope.hasTempAccess = function(param) {
        return true;
    }

    $scope.tabClick = function(tab) {
        if ($scope.iataRateMaster.id != null) {
            if (tab == 'Remarks' && $rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_REMARKS_MODIFY)) {
                $scope.Tabs = tab;
            }
        } else {
            if (tab == 'Remarks') {
                $scope.Tabs = tab;
            }
        }

    }
    //IATA BULK UPLOAD
    var myTotalOtherModal = null;
    $scope.iataRateUploadModel = function() {
        console.log("BulkUpload Button is pressed....!");
        $scope.errorMap = new Map();
        var newScope = $scope.$new();
        $scope.uploadText = true;
        $scope.data = {};
        myTotalOtherModal = $modal({
            scope: newScope,
            templateUrl: '/app/components/crm/iata_rates_air/views/iata_rates_air_upload.html',
            show: false,
            backdrop: 'static'
        });

        myTotalOtherModal.$promise.then(myTotalOtherModal.show);


    }
    $scope.uploadFile = function(value) {
        if ($scope.validateUploadFile($scope.data.file)) {
            $scope.spinner = true;
            $scope.data.fileName = $scope.data.file.name;
            var reader = new FileReader();

            reader.onload = function(event) {
                var contents = event.target.result;
                var uploadedFile = btoa(contents);
                $scope.file = uploadedFile;
            };
            reader.readAsBinaryString($scope.data.file);

            $timeout(function() {
                $scope.spinner = false;
                console.log(' uploading  IATA_RATE Air');
                myTotalOtherModal.$promise.then(myTotalOtherModal.hide);
            }, 5000);
        } else {
            console.log('invalid file format');
        }
    }

    $scope.enableUploadFn = function() {
        $timeout(function() {
            $scope.disableUploadBtn = false;
        }, 1000);
    }

    $scope.chooseFile = function() {
        $scope.disableUploadBtn = true;
        if ($scope.validateUploadFile($scope.data.file)) {
            $scope.uploadText = false;
            $scope.isBusy = true;
            $scope.data.fileName = $scope.data.file.name;
            $timeout(function() {
                $scope.isBusy = false;
                $scope.enableUploadFn();
            }, 5000);
        } else {
            $scope.enableUploadFn();
            console.log("Not valid file format", $scope.data.file.type);
        }

    };

    $scope.validateUploadFile = function(file) {
        $scope.errorMap = new Map();
        if (file == null && file == undefined) {
            $scope.errorMap.put('fileUpload', $rootScope.nls["ERR1297"]);
            return false;
        } else {
            if (file.type == "text/csv" || file.name.includes('.csv')) {

                var allowedSize = $rootScope.appMasterData['file.size.iata.rate.bulk.upload'];

                if (allowedSize != undefined) {

                    allowedSize = allowedSize * 1024 * 1024;

                    if (file.size <= 0) {

                        $scope.errorMap.put("fileUpload", $rootScope.nls["ERR1299"]);

                        return false;
                    } else if (file.size > allowedSize) {

                        $scope.errorMap.put("fileUpload", $rootScope.nls["ERR1300"]);

                        return false;
                    }

                }
            } else {
                $scope.errorMap.put('fileUpload', $rootScope.nls["ERR1298"]);
                return false;
            }
        }

        return true;
    }

    $scope.cancel = function() {
        $scope.isDirty = $scope.iataRateForm.$dirty;
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1 && $scope.iataRateMaster.id == null) {
                        $scope.save();
                    } else if (value == 1 && $scope.iataRateMaster.id != null) {
                        $scope.update();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go("layout.iataRatesAir");
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.iataRatesAir");
        }
    };


    $scope.init = function() {
        if ($scope.iataRateMaster == null ||
            $scope.iataRateMaster == undefined ||
            $scope.iataRateMaster == "") {
            $scope.iataRateMaster = {};
            $scope.iataRateMaster.currencyMaster = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.iataRateMaster.iataRateChargeList = [];
            $scope.iataRateMaster.iataRateChargeList.push({});
            $scope.iataRateCharge = {};
        } else {
            if ($scope.iataRateMaster != undefined && $scope.iataRateMaster.id != undefined && $scope.iataRateMaster.iataRateChargeList.length != 0) {
                var editRole = $rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_IATA_RATES_MODIFY);
                for (var i = 0; i < $scope.iataRateMaster.iataRateChargeList.length; i++) {
                    if ($scope.iataRateMaster.iataRateChargeList[i].id != null) {
                        if (editRole) {
                            $scope.iataRateMaster.iataRateChargeList[i].checkRolesToDisable = false;
                        } else {
                            $scope.iataRateMaster.iataRateChargeList[i].checkRolesToDisable = true;
                        }
                    }
                }
            }

            $scope.iataRateMaster.validFromDate = $rootScope.dateToString($scope.iataRateMaster.validFromDate);
            $scope.iataRateMaster.validToDate = $rootScope.dateToString($scope.iataRateMaster.validToDate);
        }




    }

    $scope.ajaxCarrierEvent = function(object) {
        console.log("Ajax Carrier Rate Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CarrierSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.carrierList = data.responseObject.searchResult;
                    console.log("$scope.carrierList ", $scope.carrierList);
                    return $scope.carrierList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier');
            });
    };

    $scope.carrierRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }
    };

    $scope.selectedCarrier = function(id) {
        if ($scope.validateIataRateMaster(1)) {
            if (id != undefined && id != null)
                $rootScope.navigateToNextField(id);
        } else {
            console.log('found carrier error..');
        }

    }

    /*    $scope.ajaxPolEvent = function(object) {
        	console.log(" Ajax pol Event method : " , object);
        	
        	$scope.searchDto={};
    		$scope.searchDto.keyword=object==null?"":object;
    		$scope.searchDto.selectedPageNumber = 0;
    		$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

    		return PortByTransportMode.fetch($scope.searchDto).$promise.then(function(data, status) {
    			if (data.responseCode =="ERR0"){
    				$scope.polList = data.responseObject.searchResult;
    				console.log("$scope.polList ", $scope.polList);
    				return $scope.polList;
    			}
    		},
    		function(errResponse){
    			console.error('Error while fetching pol list');
    		});
        };
    */
    // To display the pol
    $scope.ajaxPolEvent = function(object, iataObj) {
        console.log("Displaying List of Ports....");
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        if (iataObj == undefined || iataObj.carrierMaster == undefined || iataObj.carrierMaster == null || iataObj.carrierMaster == "") {
            Notification.error("Please select Carrier first");
        } else {
            return PortByTransportMode.fetch({
                "transportMode": iataObj.carrierMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        var resultList = data.responseObject.searchResult;
                        console.log("List of Ports : ", resultList);
                        if (resultList != null && resultList.length != 0) {
                            $scope.polList = resultList;
                        }
                        return $scope.polList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );
        }

    }




    $scope.PortRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    };

    $scope.selectedPol = function(id) {
        if ($scope.validateIataRateMaster(2)) {
            if (id != undefined && id != null)
                $rootScope.navigateToNextField(id);
        } else {
            console.log('found pol error..');
        }

    }


    $scope.ajaxCurrencyMasterEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.currencyList = data.responseObject.searchResult;
                    return $scope.currencyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Pack List');
            }
        );
    }

    $scope.currencyRender = function(item) {
        return {
            label: item.currencyCode,
            item: item
        }
    }
    $scope.ajaxPodEvent = function(object) {
        console.log(" Ajax pod Event method : ", object);
        $rootScope.clientMessage = null;
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.podList = data.responseObject.searchResult;

                    if ($scope.iataRateMaster.pol != undefined && $scope.iataRateMaster.pol != null && $scope.iataRateMaster.pol != "") {
                        $scope.podList = $scope.filterPort($scope.podList, $scope.iataRateMaster.pol.id);
                    } else {
                        $rootScope.clientMessage = $rootScope.nls['ERR05034'];
                        return;
                    }
                    console.log("$scope.podList ", $scope.podList);
                    return $scope.podList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching pod list');
            });
    };


    $scope.filterPort = function(array, id) {
        for (var i in array) {
            if (array[i].id == id) {
                array.splice(i, 1);
                break;
            }
        }
        return array;
    }

    $scope.selectedPod = function(obj, nextIdValue, index) {

        $rootScope.navigateToNextField(nextIdValue + index);

    }



    $scope.save = function() {
        console.log("Save Method is called.");


        if ($scope.validateIataRateMaster(0)) {

            if ($scope.callValidateIataRateCharge() == false) {
                $scope.Tabs = 'IATARates';
                console.log("charge validation failed");
                $scope.enableSubmitBtnFn();
                return false;
            }
            if (!$scope.iataRateAttach.isAttachmentCheck()) {
                $rootScope.clientMessage = $rootScope.nls['ERR250'];
                console.log("Attachment is invalid");
                $scope.Tabs = 'Attachments';
                $scope.enableSubmitBtnFn();
                return false;
            }
            $scope.tmpObj = cloneService.clone($scope.iataRateMaster);
            $scope.tmpObj.validFromDate = $rootScope.sendApiStartDateTime($scope.tmpObj.validFromDate);
            $scope.tmpObj.validToDate = $rootScope.sendApiEndDateTime($scope.tmpObj.validToDate);

            $scope.contained_progressbar.start();
            IataRateSave.save($scope.tmpObj).$promise
                .then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {

                            var params = {};
                            params.submitAction = 'Saved';
                            $rootScope.successDesc = $rootScope.nls["ERR400"];
                            $state.go("layout.iataRatesAir");


                        } else {
                            console.log("Iata rate added Failed ", data.responseDescription)
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    },
                    function(error) {
                        console.log("Iata rate added Failed : ", error)
                    });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.selectedCurrency = function(id) {
        if ($scope.validateIataRateMaster(5)) {
            if (id != undefined && id != null)
                $rootScope.navigateToNextField(id);
        }

    }
    $scope.focousPod = function() {
        $rootScope.navigateToNextField('currency');
        $scope.Tabs = 'IATARates';
    }
    $scope.update = function() {
        console.log("Update Method is called.");


        if ($scope.validateIataRateMaster(0)) {

            if ($scope.callValidateIataRateCharge() == false) {
                $scope.Tabs = 'IATARates';
                console.log("charge validation failed");
                $scope.enableSubmitBtnFn();
                return false;
            }

            if (!$scope.iataRateAttach.isAttachmentCheck()) {
                $rootScope.clientMessage = $rootScope.nls['ERR250'];
                console.log("Attachment is invalid");
                $scope.Tabs = 'Attachments';
                $scope.enableSubmitBtnFn();
                return false;
            }
            $scope.tmpObj = cloneService.clone($scope.iataRateMaster);
            $scope.tmpObj.validFromDate = $rootScope.sendApiStartDateTime($scope.tmpObj.validFromDate);
            $scope.tmpObj.validToDate = $rootScope.sendApiEndDateTime($scope.tmpObj.validToDate);
            $scope.contained_progressbar.start();
            IataRateEdit.save($scope.tmpObj).$promise
                .then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {

                            var params = {};
                            params.submitAction = 'Saved';
                            $rootScope.successDesc = $rootScope.nls["ERR401"];
                            $state.go("layout.iataRatesAir");


                        } else {
                            console.log("Iata rate added Failed ", data.responseDescription)
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    },
                    function(error) {
                        console.log("Iata rate added Failed : ", error)
                    });
        } else {
            console.log("Invalid Form Submission.");
        }

    }


    $scope.callValidateIataRateCharge = function() {
        var isValid = true;
        var index = $scope.iataRateMaster.iataRateChargeList.length;
        var fineArr = [];
        $scope.secondArr = [];
        angular.forEach($scope.iataRateMaster.iataRateChargeList, function(dataObj, index) {
            $scope.secondArr[index] = {};
            var validResult = validateIataRateCharge(dataObj, index, "row");
            if (validResult) {
                fineArr.push(1);
                $scope.secondArr[index].errRow = false;

            } else {
                $scope.secondArr[index].errRow = true;
                fineArr.push(0);
                isValid = false;
            }

        });

        return isValid;
    }

    //add Iata rate charge 
    $scope.addNewIataRateCharge = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_IATA_RATES_CREATE)) {
            var index = $scope.iataRateMaster.iataRateChargeList.length;
            var fineArr = [];
            $scope.secondArr = [];
            angular.forEach($scope.iataRateMaster.iataRateChargeList, function(dataObj, index) {
                $scope.secondArr[index] = {};
                var validResult = validateIataRateCharge(dataObj, index, "row");
                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArr[index].errRow = false;

                } else {
                    $scope.secondArr[index].errRow = true;
                    fineArr.push(0);
                }
                //console.log();
            });
            if (fineArr.indexOf(0) < 0) {
                $scope.iataRateMaster.iataRateChargeList.push({});
                $scope.secondArr.push({
                    "errRow": false
                });
                $scope.indexFocus = null;
                if ($scope.tableState) {
                    $scope.tableState({
                        param: true
                    });
                }
            } else {
                if ($scope.tableState) {
                    $scope.tableState({
                        param: false
                    });
                }
            }
            $rootScope.navigateToNextField("pod" + index);
        }
    };


    //show errors
    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });
    var errorOnRowIndex = null;
    $scope.errorShow = function(errorObj, index) {

        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };


    $scope.validateIataRateMaster = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.iataRateMaster.carrierMaster == null ||
                $scope.iataRateMaster.carrierMaster == undefined ||
                $scope.iataRateMaster.carrierMaster == "" ||
                $scope.iataRateMaster.carrierMaster.id == undefined ||
                $scope.iataRateMaster.carrierMaster.id == null ||
                $scope.iataRateMaster.carrierMaster.id == "") {
                $scope.errorMap.put("carrierMaster", $rootScope.nls["ERR05006"]);
                console.log("Carrier is mandatory...");
                return false;
            } else {

                if ($scope.iataRateMaster.carrierMaster.status == "Block") {
                    $scope.iataRateMaster.carrierMaster = null;
                    $scope.errorMap.put("carrierMaster", $rootScope.nls["ERR05007"]);
                    return false

                }
                if ($scope.iataRateMaster.carrierMaster.status == "Hide") {
                    $scope.iataRateMaster.carrierMaster = null;
                    $scope.errorMap.put("carrierMaster", $rootScope.nls["ERR05008"]);
                    return false

                }

            }

            console.log("Carrier validation done...");

        }
        if (validateCode == 0 || validateCode == 2) {
            if ($scope.iataRateMaster.pol == null ||
                $scope.iataRateMaster.pol == undefined ||
                $scope.iataRateMaster.pol == "" ||
                $scope.iataRateMaster.pol.id == undefined ||
                $scope.iataRateMaster.pol.id == null ||
                $scope.iataRateMaster.pol.id == "") {
                console.log("Pol is mandatory...");
                $scope.errorMap.put("pol", $rootScope.nls["ERR05003"]);
                return false;
            } else if ($scope.iataRateMaster.pol.status != null) {

                if ($scope.iataRateMaster.pol.status == "Block") {
                    $scope.iataRateMaster.pol = null;

                    $scope.errorMap.put("pol", $rootScope.nls["ERR05004"]);
                    return false

                }
                if ($scope.iataRateMaster.pol.status == "Hide") {
                    $scope.iataRateMaster.pol = null;

                    $scope.errorMap.put("pol", $rootScope.nls["ERR05005"]);
                    return false

                }

            }
            console.log("pol validation done...");
        }

        if (validateCode == 0 || validateCode == 3) {
            if ($scope.iataRateMaster.validFromDate == undefined || $scope.iataRateMaster.validFromDate == null) {
                console.log("validFromDate is mandatory...");
                $scope.errorMap.put("validFromDate", $rootScope.nls["ERR05001"]);
                return false;
            }


        }

        if (validateCode == 0 || validateCode == 4) {
            if ($scope.iataRateMaster.validToDate == undefined || $scope.iataRateMaster.validToDate == null) {
                console.log("validToDate is mandatory...");
                $scope.errorMap.put("validToDate", $rootScope.nls["ERR05002"]);
                return false;
            }
            if ($scope.iataRateMaster.validFromDate != undefined &&
                $scope.iataRateMaster.validFromDate != null) {

                var validFromDate = moment($scope.iataRateMaster.validFromDate, $rootScope.userProfile.selectedUserLocation.dateFormat).format("DD-MM-YYYY") + " 00:00:00";

                var validToDate = moment($scope.iataRateMaster.validToDate, $rootScope.userProfile.selectedUserLocation.dateFormat).format("DD-MM-YYYY") + " 00:00:00";

                if ($rootScope.convertToDate(validFromDate) > $rootScope.convertToDate(validToDate)) {
                    $scope.errorMap.put("validToDate", $rootScope.nls["ERR05000"]);
                    return false;
                }
            }

        }

        if (validateCode == 0 || validateCode == 5) {

            if ($scope.iataRateMaster.currencyMaster.currencyCode == undefined || $scope.iataRateMaster.currencyMaster.currencyCode == null ||
                $scope.iataRateMaster.currencyMaster.currencyCode == "") {
                console.log("currencyMaster is mandatory...");
                $scope.errorMap.put("currencyErr", $rootScope.nls["ERR05031"]);
                return false;

            }

            if ($scope.iataRateMaster.currencyMaster.status == 'Block') {
                console.log("currencyMaster is block...");
                $scope.errorMap.put("currencyErr", $rootScope.nls["ERR05032"]);
                return false;
            } else if ($scope.iataRateMaster.currencyMaster.status == 'Hide') {
                console.log("currencyMaster is hidden...");
                $scope.errorMap.put("currencyErr", $rootScope.nls["ERR05033"]);
                return false;
            }



        }



        return true;
    }
    var validateIataRateCharge = function(dataObj, index, type) {

        var errorArr = [];
        if (type == "row") {
            $scope.secondArr[index].errTextArr = [];
        }

        //Airport of loading

        if (dataObj.pod == null || dataObj.pod.id == undefined ||
            dataObj.pod.id == null || dataObj.pod.id == "") {
            console.log("Pod is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].pod = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05013"]);
            }

            errorArr.push(0);
        } else {

            if (dataObj.pod.status == 'Block') {
                console.log("Selected Pod is Blocked.");
                if (type == "row") {
                    $scope.secondArr[index].pod = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05014"]);
                }

                errorArr.push(0);

            } else if (dataObj.pod.status == 'Hide') {
                console.log("Selected Pod is Hidden.");
                if (type == "row") {
                    $scope.secondArr[index].pod = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05015"]);
                }

                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].pod = false;

                }

                errorArr.push(1);
            }


        }

        //min amount
        if (dataObj.minAmount != undefined && dataObj.minAmount != null && dataObj.minAmount != "") {

            if (isNaN(parseFloat(dataObj.minAmount))) {
                if (type == "row") {
                    $scope.secondArr[index].minAmount = true;
                    console.log("Minimum amount is invalid");
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05016"]);
                }
                errorArr.push(0);

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.minAmount)) {
                if (type == "row") {
                    $scope.secondArr[index].minAmount = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05016"]);
                }
                errorArr.push(0);
            } else if (dataObj.minAmount == 0) {
                if (type == "row") {
                    $scope.secondArr[index].minAmount = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05028"]);
                    console.log("Minimum amount cannot be zero");
                }
                errorArr.push(0);

            } else if (dataObj.minAmount < 0 || dataObj.minAmount >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].minAmount = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05016"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].minAmount = false;

                }
                errorArr.push(1);
            }

        } else {
            dataObj.minAmount = null;
        }

        //normal
        if (dataObj.normal != undefined && dataObj.normal != null && dataObj.normal != "") {

            if (isNaN(parseFloat(dataObj.normal))) {
                if (type == "row") {
                    $scope.secondArr[index].normal = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05017"]);
                }
                errorArr.push(0);

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.normal)) {
                if (type == "row") {
                    $scope.secondArr[index].normal = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05017"]);
                }
                errorArr.push(0);
            } else if (dataObj.normal == 0) {
                if (type == "row") {
                    $scope.secondArr[index].normal = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05029"]);
                }
                errorArr.push(0);

            } else if (dataObj.normal < 0 || dataObj.normal >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].normal = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05017"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].normal = false;

                }
                errorArr.push(1);
            }
        } else {
            dataObj.normal = null;
        }


        //plus45
        if (dataObj.plus45 != undefined && dataObj.plus45 != null && dataObj.plus45 != "") {

            if (isNaN(parseFloat(dataObj.plus45))) {
                if (type == "row") {
                    $scope.secondArr[index].plus45 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05018"]);
                }
                errorArr.push(0);

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.plus45)) {
                if (type == "row") {
                    $scope.secondArr[index].plus45 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05018"]);
                }
                errorArr.push(0);
            } else if (dataObj.plus45 == 0) {
                if (type == "row") {
                    $scope.secondArr[index].plus45 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05019"]);
                }
                errorArr.push(0);

            } else if (dataObj.plus45 < 0 || dataObj.plus45 >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].plus45 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05018"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].plus45 = false;

                }
                errorArr.push(1);
            }
        } else {
            dataObj.plus45 = null;
        }

        //plus100
        if (dataObj.plus100 != undefined && dataObj.plus100 != null && dataObj.plus100 != "") {

            if (isNaN(parseFloat(dataObj.plus100))) {
                if (type == "row") {
                    $scope.secondArr[index].plus100 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05020"]);
                }
                errorArr.push(0);

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.plus100)) {
                if (type == "row") {
                    $scope.secondArr[index].plus100 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05020"]);
                }
                errorArr.push(0);
            } else if (dataObj.plus100 == 0) {
                if (type == "row") {
                    $scope.secondArr[index].plus100 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05021"]);
                }
                errorArr.push(0);

            } else if (dataObj.plus100 < 0 || dataObj.plus100 >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].plus100 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05020"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].plus100 = false;

                }
                errorArr.push(1);
            }
        } else {
            dataObj.plus100 = null;
        }

        //plus300
        if (dataObj.plus300 != undefined && dataObj.plus300 != null && dataObj.plus300 != "") {

            if (isNaN(parseFloat(dataObj.plus300))) {
                if (type == "row") {
                    $scope.secondArr[index].plus300 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05022"]);
                }
                errorArr.push(0);
            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.plus300)) {
                if (type == "row") {
                    $scope.secondArr[index].plus300 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05022"]);
                }
                errorArr.push(0);
            } else if (dataObj.plus300 == 0) {
                if (type == "row") {
                    $scope.secondArr[index].plus300 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05023"]);
                }
                errorArr.push(0);

            } else if (dataObj.plus300 < 0 || dataObj.plus300 >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].plus300 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05022"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].plus300 = false;

                }
                errorArr.push(1);
            }
        } else {
            dataObj.plus300 = null;
        }

        //plus500
        if (dataObj.plus500 != undefined && dataObj.plus500 != null && dataObj.plus500 != "") {

            if (isNaN(parseFloat(dataObj.plus500))) {
                if (type == "row") {
                    $scope.secondArr[index].plus500 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05024"]);
                }
                errorArr.push(0);

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.plus500)) {
                if (type == "row") {
                    $scope.secondArr[index].plus500 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05024"]);
                }
                errorArr.push(0);
            } else if (dataObj.plus500 == 0) {
                if (type == "row") {
                    $scope.secondArr[index].plus500 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05025"]);
                }
                errorArr.push(0);

            } else if (dataObj.plus500 < 0 || dataObj.plus500 >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].plus500 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05024"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].plus500 = false;

                }
                errorArr.push(1);
            }
        } else {
            dataObj.plus500 = null;
        }

        //plus1000
        if (dataObj.plus1000 != undefined && dataObj.plus1000 != null && dataObj.plus1000 != "") {

            if (isNaN(parseFloat(dataObj.plus1000))) {
                if (type == "row") {
                    $scope.secondArr[index].plus1000 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05026"]);
                }
                errorArr.push(0);

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataObj.plus1000)) {
                if (type == "row") {
                    $scope.secondArr[index].plus1000 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05026"]);
                }
                errorArr.push(0);
            } else if (dataObj.plus1000 == 0) {
                if (type == "row") {
                    $scope.secondArr[index].plus1000 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05027"]);
                }
                errorArr.push(0);

            } else if (dataObj.plus1000 < 0 || dataObj.plus1000 >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].plus1000 = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05026"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].plus1000 = false;

                }
                errorArr.push(1);
            }

        } else {
            dataObj.plus1000 = null;
        }

        if ((dataObj.normal != undefined && dataObj.normal != null) &&
            (dataObj.minAmount != undefined && dataObj.minAmount != null) &&
            (dataObj.plus45 != undefined && dataObj.plus45 != null) &&
            (dataObj.plus100 != undefined && dataObj.plus100 != null) &&
            (dataObj.plus300 != undefined && dataObj.plus300 != null) &&
            (dataObj.plus500 != undefined && dataObj.plus500 != null) &&
            (dataObj.plus100 != undefined && dataObj.plus100 != null)) {

            if (dataObj.minAmount == dataObj.normal && dataObj.minAmount == dataObj.plus45 &&
                dataObj.minAmount == dataObj.plus100 && dataObj.minAmount == dataObj.plus300 &&
                dataObj.minAmount == dataObj.plus500 && dataObj.minAmount == dataObj.plus100) {


                if (type == "row") {
                    $scope.secondArr[index].errorArr = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05035"]);
                }

                errorArr.push(0);
            }

        }




        if (errorArr.indexOf(0) < 0) {
            return true
        } else {
            return false;
        }
    }

    $scope.deleteIataRate = function() {
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR05037"];
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            IataRateRemove.remove({
                id: $scope.iataRateMaster.id
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("IataRate deleted Successfully");
                    $scope.init();

                } else {
                    console.log("IataRate deleted Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("IataRate deleted Failed : " + error)
            });
        }, function(value) {
            console.log("IataRate cancelled");

        });
    }
    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print"];

    $scope.manipulateAttach = function(param) {

        console.log("manipulateAttach param", param);

        if (param.index == null) {
            $scope.attachConfig.data.push(param.data);
        } else {
            $scope.attachConfig.data.splice(param.index, 1, param.data);
        }

    }

    $scope.removeAttach = function(param) {
        $scope.attachConfig.data.splice(param.index, 1);
    }

    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/iatarate/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }




    $scope.deleteIataRateCharge = function(index) {
        if ($rootScope.roleAccess(roleConstant.CRM_IATA_RATES_AIR_IATA_RATES_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR05038"];
            ngDialog
                .openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' + '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        $scope.iataRateMaster.iataRateChargeList
                            .splice(index, 1);
                        if ($scope.iataRateMaster.iataRateChargeList.length == 0) {

                        }
                    },
                    function(value) {
                        console
                            .log(" Iata rate charge delete cancelled");
                    });
        }
    }

    // History Start

    $scope.getIataRateById = function(id) {
        console.log("getIataRateById ", id);

        IataRateView.get({
            id: id
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting IataRate.", data)
                $scope.iataRateMaster = data.responseObject;
                $scope.init();
            }

        }, function(error) {
            console.log("Error while getting IataRate.", error)
        });

    }


    //On leave the Unfilled pack Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('iataRatesAirAddEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.iataRateMaster;
        $rootScope.iataRate = "Master";
        $rootScope.unfinishedFormTitle = "Iata Rate (New)";
        if ($scope.iataRateMaster != undefined && $scope.iataRateMaster != null && $scope.iataRateMaster.carrierMaster != undefined &&
            $scope.iataRateMaster.carrierMaster != null && $scope.iataRateMaster.carrierMaster != "") {
            $rootScope.subTitle = $scope.iataRateMaster.carrierMaster.carrierName;
        } else {
            $rootScope.subTitle = "Unknown Iata Rate"
        }
    })

    $scope.$on('iataRatesAirEditEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.iataRateMaster;
        $rootScope.iataRate = "Master";
        $rootScope.unfinishedFormTitle = "Iata Rate Edit";
        if ($scope.iataRateMaster != undefined && $scope.iataRateMaster != null && $scope.iataRateMaster.carrierMaster != undefined &&
            $scope.iataRateMaster.carrierMaster != null && $scope.iataRateMaster.carrierMaster != "") {
            $rootScope.subTitle = $scope.iataRateMaster.carrierMaster.carrierName;
        } else {
            $rootScope.subTitle = "Unknown Iata Rate"
        }
    })


    $scope.$on('iataRatesAirAddEventReload', function(e, confirmation) {
        console.log("addPackEventReload is called ", $scope.iataRateMaster);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.iataRateMaster);
        localStorage.isIataRateReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('iataRatesAirEditEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.iataRateMaster);
        localStorage.isIataRateReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isIataRateReloaded;

    if ($stateParams.action == "ADD") {

        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isIataRateReloaded = "NO";
                $scope.iataRateMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.iataRateMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isIataRateReloaded = "NO";
                $scope.iataRateMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.iataRatesAir"
            },
            {
                label: "IATA Rates – Air",
                state: "layout.iataRatesAir"
            },
            {
                label: "Add IATA Rates – Air",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isIataRateReloaded = "NO";
                $scope.iataRateMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.iataRateMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isIataRateReloaded = "NO";
                $scope.iataRateMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {

                $scope.getIataRateById($stateParams.iataRateId);

            }
        }
        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.iataRatesAir"
            },
            {
                label: "IATA Rates – Air",
                state: "layout.iataRatesAir"
            },
            {
                label: "Edit IATA Rates – Air",
                state: null
            }
        ];
    }


    // History End

}]);