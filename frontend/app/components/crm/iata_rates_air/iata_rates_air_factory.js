(function() {

		app.factory("IataRateSearch",['$resource', function($resource) {
			return $resource("/api/v1/iatarate/search", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("IataRateView",['$resource', function($resource) {
			return $resource("/api/v1/iatarate/get/id/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		app.factory("IataRateSave",['$resource', function($resource) {
			return $resource("/api/v1/iatarate/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		
		app.factory("IataRateEdit",['$resource', function($resource) {
			return $resource("/api/v1/iatarate/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
	
		app.factory("IataRateRemove",['$resource', function($resource) {
			return $resource("/api/v1/iatarate/delete/id/:id", {}, {
				get : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		
})();
