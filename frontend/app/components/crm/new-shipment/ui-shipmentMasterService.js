app.service('uiShipmentDataService',['$rootScope', 'uiShipmentFactory', '$timeout', '$q', 
                                     function($rootScope, uiShipmentFactory, $timeout, $q) {

	this.addInitialization = function() {

        var shipment = {};
        shipment.shipmentServiceList = [];
        shipment.createdBy = $rootScope.userProfile.employee;
        shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
        return this.addNewShipmentService(shipment);
        
	}
	
	this.addNewShipmentService = function(shipment) {
		//Validate Shipment to add New Service
		/*if(validate(shipment)) {
			
		}*/
        var shipmentServiceDetail = {};
        shipmentServiceDetail.serviceReqDate = new Date();
        shipmentServiceDetail.pickUpDeliveryPoint = {};
        shipmentServiceDetail.pickUpDeliveryPoint.transporter = {};
        shipmentServiceDetail.pickUpDeliveryPoint.pickupPoint = {};
        shipmentServiceDetail.pickUpDeliveryPoint.pickupFrom = {};
        shipmentServiceDetail.pickUpDeliveryPoint.deliveryPoint = {};
        shipmentServiceDetail.pickUpDeliveryPoint.deliveryFrom = {};
        shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryPoint = {};
        shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryFrom = {};
        //shipmentServiceDetail.whoRouted = vm.shipment.whoRouted;
        shipmentServiceDetail.customerService = $rootScope.userProfile.employee;
        shipmentServiceDetail.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;

        if($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS')=='true'){
        	shipmentServiceDetail.dimensionUnit=true;
        	shipmentServiceDetail.dimensionUnitValue=$rootScope.appMasterData['inch.to.pounds'];
		}else{
			shipmentServiceDetail.dimensionUnit=false;
			shipmentServiceDetail.dimensionUnitValue=$rootScope.appMasterData['centimeter.to.kilos'];
		}
        
        shipmentServiceDetail.ppcc = false;
        shipmentServiceDetail.location = $rootScope.userProfile.selectedUserLocation;
        shipmentServiceDetail.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
        shipmentServiceDetail.documentList = [];
        shipmentServiceDetail.shipmentChargeList = [];
        shipmentServiceDetail.connectionList = [{connectionStatus : 'Planned'}];
        shipmentServiceDetail.shipmentAttachmentList = [{}];
        shipmentServiceDetail.eventList = [{isCompleted : 'Yes',followUpRequired : 'No'}];
        //Reference List initialize with one default empty object
        shipmentServiceDetail.referenceList = [{}];
        //Boe list initialize 
        shipmentServiceDetail.billOfEntryList = [{}];
        //Trigger List initialize with one default empty object
        shipmentServiceDetail.shipmentServiceTriggerList = [];
        //Authenticated Document List initialize with one default empty object
        shipmentServiceDetail.authenticatedDocList=[{}];
        // Attachment Configuration for each shipment service
        shipmentServiceDetail.attachConfig = {
            "isEdit": true,
            "isDelete": true,
            "page": "shipment",
            "columnDefs": [
                {"name": "Document Name", "model": "documentMaster.documentName", "type": "text"}, 
                {"name": "Reference #", "model": "refNo", "type": "text"}, 
                {"name": "Customs", "model": "customs", "type": "text"}, 
                {"name": "Unprotected File Name", "model": "unprotectedFileName", "type": "file"}, 
                {"name": "Protected File Name", "model": "protectedFileName", "type": "file"}, 
                {"name": "Actions", "model": "action", "type": "action"}
            ]
        };
        // Assigning Default Package in Shipment Service Detail configured in appMasterData..
        shipmentServiceDetail.packMaster = {};
        /*var pkgData = function() {
            var deferred = $q.defer();
            setTimeout(function() {
              // Reject 3 out of 10 times to simulate 
              // some business logic.
              if (Math.random() > 0.7) deferred.reject('hell');
              else deferred.resolve('world');
            }, 1000);
            return deferred.promise;
          };

          wait().then(function(rest) {
            $scope.name += rest;
          })
          .catch(function(fallback) {
            $scope.name += fallback.toUpperCase() + '!!';
          });*/
      /* 
		*/
        shipment.shipmentServiceList.push(shipmentServiceDetail);
        return shipment;
	};
	
	
    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.shipment"
            }, {
                label: "Shipment",
                state: "layout.shipment"
            }, {
                label: "Add Shipment",
                state: null
            }];
    };
    
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.shipment"
            }, {
                label: "Shipment",
                state: "layout.shipment"
            }, {
                label: "Edit Shipment",
                state: null
            }];
    };

    this.getDefaultPackage = function(defaultPackageCode) {
    	var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
        uiShipmentFactory.PackGetByCode.query( {code: defaultPackageCode}, function(data) {
            if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                console.log("Successful while getting pack. have to 1st");
                shipmentServiceDetail.packMaster = data.responseObject;
            }
        }, function(error) {
            console.log("Error while getting pack.", error)
        });
    } 
}]);