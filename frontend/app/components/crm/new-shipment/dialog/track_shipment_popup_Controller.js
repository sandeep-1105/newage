(function () {
	app.controller("trackShipmentPopupCtrl", [
		'$scope', 'TrackShipmentPopupFactory',
		function ($scope, TrackShipmentPopupFactory) {

			$scope.sendMail = function (shipmentUid) {
				console.log("inside send mail for track shipment pop up "+shipmentUid);
				var emailBody = angular.element("#modal-body");
				
				html2canvas(emailBody[0]).then(function(canvas) {
					console.log("inside html2Canvas ");
					var data = canvas.toDataURL();
					TrackShipmentPopupFactory.query({
						"shipmentUid": shipmentUid,
						"mailData": data.split(",")[1]}).$promise.then(function (data, status) {
						if (data.responseCode == "ERR0") {
							console.log("Mail Sent Successfully");
						}
					},
						function (errResponse) {
							console.error('Error while Sending mail');
						}
					);
				},function(error){
					console.log("Error " + error);
				});


			}


		}//end
	]);
})();