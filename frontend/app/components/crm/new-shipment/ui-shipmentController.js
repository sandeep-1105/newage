(function() {
    app.controller("uiShipmentCtrl", [
        '$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'Notification', '$timeout', 'NgMap',
        'uiShipmentDataService', 'uiShipmentFactory', 'RecentHistorySaveService', 'uiShipmentValidationService', 'AutoCompleteService',
        'ShipmentAdd', 'cloneService', 'FlightPlanSearchForShipment', 'GetNextMawb', 'ValidateUtil', 'addressJoiner', 'EmptyRowChecker', 'CommonValidationService', 'uiShipmentCalculationService',
        'ShipmentGet', 'ShipmentUpdate', 'ShipmentServiceInvoiceList', 'ProvisionalViewByShipmentServiceUid', 'ShipmentStatusChange', 'CopyShipmentSearch', 'ShipmentGetFromUID', 'appMetaFactory', 'commentMasterFactory',
        'downloadMultipleFactory', 'downloadFactory', 'ShipmentDocumentIdList', 'AesSearch', 'AesAddAll', 'ServiceGetByUid', 'roleConstant', '$http', 'ServiceSignOffStatus', 'uiShipmentAccountsService', 'ShipmentTrack', 'SequenceFactoryForCAN', 'purchaseOrderFactory',

        function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, Notification, $timeout, NgMap,
            uiShipmentDataService, uiShipmentFactory, RecentHistorySaveService, uiShipmentValidationService, AutoCompleteService,
            ShipmentAdd, cloneService, FlightPlanSearchForShipment, GetNextMawb, ValidateUtil, addressJoiner, EmptyRowChecker,
            CommonValidationService, uiShipmentCalculationService, ShipmentGet, ShipmentUpdate, ShipmentServiceInvoiceList, ProvisionalViewByShipmentServiceUid,
            ShipmentStatusChange, CopyShipmentSearch, ShipmentGetFromUID, appMetaFactory, commentMasterFactory, downloadMultipleFactory, downloadFactory, ShipmentDocumentIdList, AesSearch, AesAddAll, ServiceGetByUid, roleConstant, $http, ServiceSignOffStatus, uiShipmentAccountsService, ShipmentTrack, SequenceFactoryForCAN, purchaseOrderFactory) {
            $scope.$AutoCompleteService = AutoCompleteService;
            $scope.$uiShipmentAccountsService = uiShipmentAccountsService;
            $scope.$roleConstant = roleConstant;
            $scope.$uiShipmentDataService = uiShipmentDataService;
            $scope.$uiShipmentValidationService = uiShipmentValidationService;

            $scope.btnGrp = {};
            $scope.btnGrpFilter = {};
            $scope.btnGrp.TypeArr = ["Invoice", "Credit Note", "All"];
            $scope.btnGrpFilter.TypeArr = ["Customer", "Agent", "All"];

            $rootScope.hawbRated_Unrated = false

            $scope.invoiceList = [];
            var selectListConfig = {
                search: false,
                address: true
            };

            $scope.setActiveObject = function() {
                var pageRelaodActiveObj = {
                    serviceIdx: $scope.selectedShipmentServiceIndex,
                    currenctTab: $scope.shipmentTab
                };
                localStorage.pageRelaodActiveObj = JSON.stringify(pageRelaodActiveObj);
            }

            $scope.restoreActiveObject = function() {
                try {
                    var pageRelaodActiveObj = JSON.parse(localStorage.pageRelaodActiveObj);
                } catch (e) {
                    console.log("Exception in restore active Object");
                }
                if (pageRelaodActiveObj == undefined) {
                    $scope.changeShipmentServiceView(0, 'serviceRouting');
                } else {
                    if (pageRelaodActiveObj.serviceIdx == undefined) {
                        pageRelaodActiveObj.serviceIdx = 0;
                    }
                    $scope.changeShipmentServiceView(pageRelaodActiveObj.serviceIdx, pageRelaodActiveObj.currenctTab);
                }
            };
            $scope.transportAddressListConfig = $scope.shipmentPartyAddressListConfig = $scope.partyAddressListConfig = $scope.pickUpPlaceAddressListConfig = selectListConfig;
            $scope.errorMap = new Map();
            $scope.showTransportAddressList = false;
            $scope.showPartyAddressList = false;
            $scope.showShipmentPartyAddressList = false;
            $scope.showTransportAddressList = false;
            $scope.showPickUpPlaceAddressList = false;

            $scope.initDateFormat = function() {
                $scope.dateTimePickerOptionsLeft = {
                    format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                    useCurrent: false,
                    sideBySide: true,
                    viewDate: moment()
                };
                $scope.dateTimePickerOptionsLeft.widgetParent = "body";
                $scope.dateTimePickerOptionsLeft.widgetPositioning = {
                    horizontal: 'right'
                };

                $scope.directiveDateTimePickerOpts = {
                    format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                    useCurrent: false,
                    sideBySide: true,
                    viewDate: moment()
                };
                $scope.directiveDateTimePickerOpts.widgetParent = "body";
                $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
                $scope.directiveDatePickerOpts.widgetParent = "body";
            }

            $scope.focusAndMessageShower = function(resObj, nextField) {

                $scope.errorMap = new Map();
                console.log("resObj :: ", resObj);
                if (resObj.error == false) {
                    if (nextField != undefined) {
                        $rootScope.navigateToNextField(nextField);
                    }
                    return false;
                } else {
                    $scope.errorMap.put(resObj.errElement, resObj.errMessage);
                    $scope.changeShipmentServiceView(resObj.serviceIdx, resObj.tab, 'error', resObj.errElement);
                    return true;
                }
            }

            $scope.getSerivceSignOffStatus = function(fromState) {
                var serviceId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;
                ServiceSignOffStatus.get({
                    serviceId: serviceId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (data.responseObject != null && data.responseObject.isSignOff == true) {

                            if (fromState == 'INV') {
                                if ($rootScope.roleAccess(roleConstant.FINANCE_INVOICE_CREATE)) {
                                    uiShipmentAccountsService.createInvoice($scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                                }
                            } else if (fromState == 'CREDIT_NOTE_COST') {
                                if ($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_COST_CREATE)) {
                                    uiShipmentAccountsService.createCreditNoteCost($scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                                }
                            } else if (fromState == 'CREDIT_NOTE_REVENUE') {
                                if ($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_CREATE)) {
                                    uiShipmentAccountsService.createCreditNoteRevenue($scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                                }
                            }

                        } else {
                            console.log($rootScope.nls["ERR40027"]);
                            $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                        }
                    } else {
                        console.log($rootScope.nls["ERR40027"]);
                        $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                    }
                }, function(error) {
                    console.log("error in getSerivceSignOffStatus method : " + error);
                });
            }
            $scope.openTab = function(tabName, index, shipmentServiceDetail, id) {

                if ($scope.shipment.id == undefined) {
                    if (tabName != undefined && tabName != null) {
                        if (tabName == 'serviceRouting' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_SERVICE_ROUTING_CREATE)) {
                            $scope.shipmentTab = tabName;
                        } else {
                            $scope.shipmentTab = null;
                        }
                        if (tabName == 'cargoDetails' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_CARGO_DETAILS_CREATE)) {
                            $scope.shipmentTab = tabName;
                            if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                $rootScope.navigateToNextField(id);
                            }
                        }
                        if (tabName == 'hawb' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_HAWB_CREATE)) {
                            $scope.shipmentTab = tabName;
                            if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                $rootScope.navigateToNextField(id);
                            }
                        }
                        if (tabName == 'pickupDelivery' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_PICKUP_DELIVERY_CREATE)) {
                            $scope.shipmentTab = tabName;
                            $scope.mapShowingFunctionality();
                        }
                        if (tabName == 'aes' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_AES_RATES_CREATE)) {
                            $scope.shipmentTab = tabName;
                        }
                        if (tabName == 'others' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_CREATE)) {
                            $scope.shipmentTab = tabName;
                        }

                        if (id != undefined) {
                            $rootScope.navigateToNextField(id);
                        }
                    }
                } else {

                    if (tabName != undefined && tabName != null) {
                        if (tabName == 'serviceRouting' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_SERVICE_ROUTING_MODIFY)) {
                            $scope.shipmentTab = tabName;
                        } else {
                            $scope.shipmentTab = null;
                        }
                        if (tabName == 'cargoDetails' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_CARGO_DETAILS_MODIFY)) {
                            $scope.shipmentTab = tabName;
                            if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                $rootScope.navigateToNextField(id);
                            }
                        }
                        if (tabName == 'hawb' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_HAWB_MODIFY)) {
                            $scope.shipmentTab = tabName;
                            if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                $rootScope.navigateToNextField(id);
                            }
                        }
                        if (tabName == 'pickupDelivery' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_PICKUP_DELIVERY_MODIFY)) {
                            $scope.shipmentTab = tabName;
                            $scope.mapShowingFunctionality();
                        }
                        if (tabName == 'aes' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_AES_RATES_MODIFY)) {
                            $scope.shipmentTab = tabName;
                        }
                        if (tabName == 'others' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_MODIFY)) {
                            $scope.shipmentTab = tabName;

                            $scope.shipmentServiceDetail = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex];

                            if ($scope.shipmentServiceDetail != undefined && $scope.shipmentServiceDetail.id != undefined && $scope.shipmentServiceDetail.authenticatedDocList.length != 0) {

                                var editRole = $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_DOCUMENT_MODIFY);
                                for (var i = 0; i < $scope.shipmentServiceDetail.authenticatedDocList.length; i++) {
                                    if ($scope.shipmentServiceDetail.authenticatedDocList[i].id != null) {
                                        if (editRole) {
                                            $scope.shipmentServiceDetail.authenticatedDocList[i].checkRolesToDisable = false;
                                        } else {
                                            $scope.shipmentServiceDetail.authenticatedDocList[i].checkRolesToDisable = true;
                                        }
                                    }
                                }
                            }

                        }
                        if (id != undefined) {
                            $rootScope.navigateToNextField(id);
                        }
                    }
                }

            }

            $scope.makeAccordianActive = function() {

            }

            //Shipment Cargo starts

            $scope.selectedCustomerService = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Booking person Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 101), nextField)) {
                    console.log("Yes Booking person Error");
                } else {
                    console.log("Booking person - No Error Occurs");
                }
            }



            $scope.selectedShipmentServiceDivision = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Division Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 103), nextField)) {
                    console.log("Yes Division Error");
                } else {
                    console.log("Division - No Error Occurs");
                }
            }


            $scope.selectedShipmentServiceProject = function(shipmentServiceDetail, serviceIDX, nextField) {

                console.log("After Project Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 104), nextField)) {
                    console.log("Yes Project Error");
                } else {
                    console.log("Project - No Error Occurs");
                }
            }


            $scope.selectedShipmentServicePack = function(shipmentServiceDetail, serviceIDX, nextField) {

                console.log("After Pack Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 105), nextField)) {
                    console.log("Yes Pack Error");
                } else {
                    console.log("Pack - No Error Occurs");
                }
            }

            $scope.cargoValidationTextBox = function(shipmentServiceDetail, index, errorCode) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, errorCode), undefined)
            }


            //Shipment Cargo ends


            //Shipment Cargo starts

            $scope.selectedCustomerService = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Booking person Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 101), nextField)) {
                    console.log("Yes Booking person Error");
                } else {
                    console.log("Booking person - No Error Occurs");
                }
            }


            $scope.selectedShipmentServiceDetailCoLoader = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Booking person Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 102), nextField)) {
                    console.log("Yes Booking person Error");
                } else {
                    console.log("Booking person - No Error Occurs");
                }
            }


            $scope.selectedShipmentServiceDivision = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Division Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 103), nextField)) {
                    console.log("Yes Division Error");
                } else {
                    console.log("Division - No Error Occurs");
                }
            }


            $scope.selectedShipmentServiceProject = function(shipmentServiceDetail, serviceIDX, nextField) {

                console.log("After Project Selected ");
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 104), nextField)) {
                    console.log("Yes Project Error");
                } else {
                    console.log("Project - No Error Occurs");
                }
            }



            $scope.cargoValidationTextBox = function(shipmentServiceDetail, index, errorCode) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, errorCode), undefined)
            }


            //Shipment Cargo ends





            //Aes No validation Text Box
            $scope.aesNoValidation = function(sd, index, code) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, sd, index, code), undefined)
            }

            $scope.selectedBrokerageParty = function(sd, index, code) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, sd, index, code), 'shipment.serviceDetail.brokeragePercentage.' + index)
            }

            $scope.selectedBrokeragePercentage = function(sd, index, code) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, sd, index, code), undefined)
            }

            $scope.selectedHoldNote = function(sd, index, code) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, sd, index, code), 'shipment.serviceDetail.holdNote.' + index)
            }

            $scope.selectedHoldReleaseNote = function(sd, index, code) {
                $scope.errorMap = new Map();
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, sd, index, code), 'shipment.serviceDetail.holdNote.' + index)
            }

            //render 

            $scope.partyRender = function(item) {
                return {
                    label: item.partyName,
                    item: item
                }

            }



            //Rates       


            $scope.viewRate = function(tmpObj) {

                if (tmpObj.isRatesCheck()) {
                    var newScope = $scope.$new();

                    if (tmpObj.localCurrency != null) {
                        newScope.rateMerged = $rootScope.appMasterData['booking.rates.merged'];
                        if (newScope.rateMerged.toLowerCase() == 'true' || newScope.rateMerged == true) {
                            newScope.rateMerged = true;
                        } else {
                            newScope.rateMerged = false;
                        }
                        newScope.localCurrency = tmpObj.localCurrency.currencyCode;
                        newScope.clientGrossRate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.clientGrossRate);
                        newScope.clientNetRate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.clientNetRate);
                        newScope.declaredCost = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.declaredCost);
                        newScope.rate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.rate);
                        newScope.cost = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.cost);
                    }
                    var rateModal = $modal({
                        scope: newScope,
                        templateUrl: 'viewlocalRate.html',
                        show: false
                    });
                    rateModal.$promise.then(rateModal.show);
                }
            };

            //default check and get po items
            $scope.hidePO = function(serv) {
                if (serv != undefined && (serv.purchaseOrder == undefined || serv.purchaseOrder.id == undefined)) {
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].poNo = null;
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = null;
                }
                poModel.$promise.then(poModel.hide);
            }

            $scope.savePurchaseOrder = function() {
                poModel.$promise.then(poModel.hide);
            }

            $scope.moreinfoPopup = function(parentindex, index) {
                $scope.poIndex = index;
                $scope.poParentIndex = parentindex;

                var myPoMoreModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/crm/new-shipment/dialog/shipment_po_moreinfo_popup.html',
                    show: false
                });
                myPoMoreModal.$promise.then(myPoMoreModal.show);
            };

            var poModel = $modal({
                scope: $scope,
                // backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_purchase_order_popup.html',
                show: false
            });

            $scope.showPurchaseOrderPopup = function(service, index) {

                if (service != undefined && service.purchaseOrder != undefined && service.purchaseOrder != null && service.purchaseOrder.id != undefined) {
                    //$scope.checkAndGetPurchaseOrder(service.poNo,index); 
                } else if (service) {
                    service.purchaseOrder = {};
                    service.purchaseOrder = null;
                }
                poModel.$promise.then(poModel.show)
            }

            $scope.checkAndGetPurchaseOrder = function(poNo, index) {
                if (poNo == "" || poNo == undefined || poNo == null) {
                    $rootScope.navigateToNextField("ponoid");
                    return;
                }
                if (poNo != undefined && poNo != null && poNo != "") {
                    $scope.spinner = true;
                    purchaseOrderFactory.findBypoNo.poNo({ poNo: poNo }).$promise.then(function(data) {
                        if (data.responseCode == "ERR0") {
                            if (data.responseObject != null) {} else {
                                Notification.error("Not matched any purchase order based on given PO No.");
                            }
                            $scope.spinner = false;
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = {};
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = data.responseObject;
                        }
                    }, function(error) {
                        console.log("error while fetching findBypoNo");
                        $scope.spinner = false;
                        $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = {};
                        $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = null;
                        return false;
                    });
                } else {
                    Notification.error("Not matched any purchase order based on given PO No.");
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = {};
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].purchaseOrder = null;
                    $scope.spinner = false;
                }
            }


            //boe button
            var boeModal;
            $scope.addboemodel = function() {

                boeModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/crm/new-shipment/dialog/shipment_boe_popup.html',
                    show: false
                });
                boeModal.$promise.then(boeModal.show)
            };
            //boe note button
            var noteBoeModal;
            $scope.addnoteboemodel = function() {

                noteBoeModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/crm/new-shipment/dialog/shipment_boe_notes_popup.html',
                    show: false
                });
                noteBoeModal.$promise.then(noteBoeModal.show)
            };


            $scope.dirtyIndex = function(index) {
                $scope.indexFocus = index;
            };


            $scope.selectedShipmentServiceMaster = function(shipmentServiceDetail, serviceIDX, nextField, code) {
                console.log("After Service Selected : ", shipmentServiceDetail);
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 1), nextField)) {
                    console.log("Yes Service Master Error");
                } else {
                    console.log("Service Master No Error Occurs");
                }
            }
            $scope.selectedShipmentCarrierMaster = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Selected : ", shipmentServiceDetail);
                $scope.errorMap = new Map();
                console.log($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 1), nextField));
            }
            $scope.selectedShipmentServiceParty = function(shipmentServiceDetail, serviceIDX, nextField) {

                console.log("After Service Party Selected : ", nextField);
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 2), nextField)) {
                    console.log("Yes Party Master Error");
                } else {
                    console.log("Party Master No Error Occurs");
                }
            }


            $scope.selectedShipmentServiceTos = function(shipmentServiceDetail, serviceIDX, nextField) {
                $scope.selectPrepaidCollect(shipmentServiceDetail);
                console.log("After Service TOS Selected : ", nextField);
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 3), nextField)) {}
            }
            $scope.selectPrepaidCollect = function(serviceDetail) {
                if (serviceDetail.tosMaster != null && serviceDetail.tosMaster != undefined) {
                    serviceDetail.ppcc = serviceDetail.tosMaster.freightPPCC == 'Prepaid' ? false : true;
                } else {
                    serviceDetail.ppcc = false;
                }
            }
            $scope.selectedShipmentServiceCommodityMaster = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service TOS Selected : ", shipmentServiceDetail);
                console.log("After Service TOS Selected : ", nextField);
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 4), nextField)) {}
            }
            $scope.selectedShipmentServiceSalesMan = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service SalesMan Selected : ", shipmentServiceDetail);
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 5), nextField);
            }
            $scope.selectedShipmentServiceAgent = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Agent Selected : ", shipmentServiceDetail);
                $scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 5), nextField);
            }


            /* Selected Origin */
            $scope.selectedShipmentServiceOrigin = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Origin Selected : ", shipmentServiceDetail);
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 6), nextField)) {
                    if (shipmentServiceDetail.pol == undefined || shipmentServiceDetail.pol == null) {
                        shipmentServiceDetail.pol = shipmentServiceDetail.origin;
                    }

                    uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "origin");
                }
            }

            $scope.selectedShipmentServicePol = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Pol Selected : ", shipmentServiceDetail);
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 7), nextField)) {
                    uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "pol");
                }
            }

            $scope.selectedShipmentServicePod = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Pod Selected : ", shipmentServiceDetail);
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 8), nextField)) {
                    if (shipmentServiceDetail.destination == undefined || shipmentServiceDetail.destination == null) {
                        shipmentServiceDetail.destination = shipmentServiceDetail.pod;
                    }
                    uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "pod");
                }
            }

            $scope.selectedShipmentServiceDestination = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Destination Selected : ", shipmentServiceDetail);
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 9), nextField)) {
                    uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "destination");
                }
            }


            $scope.getCarrierList = function(shipmentServiceDetail, serviceIDX, keyword) {

                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 1))) {
                    $rootScope.navigateToNextFieldWithOutTime('shipment.serviceDetail.serviceMaster.' + serviceIDX);
                } else {
                    return AutoCompleteService.getCarrierList(shipmentServiceDetail.serviceMaster.transportMode, keyword);
                }

            }

            $scope.navigateToNextFieldWithOutTime = function(id) {
                if (id != undefined && id != null) {
                    var docElement = document.getElementById(id);
                    if (docElement != undefined && docElement != null) {
                        docElement.focus();
                    }
                }
            }


            $scope.selectedShipmentServiceCarrier = function(shipmentServiceDetail, serviceIDX, nextField) {
                console.log("After Service Carrier Selected : ", shipmentServiceDetail);
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 10), nextField)) {
                    uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "carrier");
                }
            }

            $scope.selectedShipmentServiceRouteNo = function(shipmentServiceDetail, serviceIDX, nextField) {

                if (!event.shiftKey && event.keyCode == 9) {
                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 11), nextField)) {
                        uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "routeNo");
                    }
                }
            }

            $scope.selectedShipmentServiceMawb = function(shipmentServiceDetail, serviceIDX, nextField, event) {

                if (!event.shiftKey && event.keyCode == 9) {
                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIDX, 12), nextField)) {
                        uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "mawbNo");
                    }
                }

            }


            $scope.selectedShipmentServiceEtd = function(shipmentServiceDetail, serviceIDX, nextField) {
                if (shipmentServiceDetail.eta == undefined || shipmentServiceDetail.eta == null || shipmentServiceDetail.eta == "") {
                    shipmentServiceDetail.eta = shipmentServiceDetail.etd;
                }
                uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "etd");
                $rootScope.navigateToNextField(nextField)
            }

            $scope.selectedShipmentServiceEta = function(shipmentServiceDetail, serviceIDX, nextField) {
                uiShipmentDataService.assignedDocumentToService(shipmentServiceDetail, "eta");
                if (uiShipmentDataService.checkIsTranshipment(shipmentServiceDetail)) {
                    $rootScope.navigateToNextField(nextField);
                } else {
                    $scope.shipmentTab = 'cargoDetails';
                    $rootScope.navigateToNextField('shipment.serviceDetail.bookingPerson.' + serviceIDX);
                }

            }

            //Document Select Event
            $scope.selectedAjaxShipperEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 300), nextFocus)) {
                    console.log("Yes Shipper Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'shipper');
                    console.log("Shipper No Error Occurs");
                }
            }

            $scope.selectedAjaxCongineeEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 304), nextFocus)) {
                    console.log("Yes consignee Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'consignee');
                    console.log("consignee No Error Occurs");
                }
            }

            $scope.selectedAjaxFirstNotifyEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {

                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 306), nextFocus)) {
                    console.log("Yes FirstNotify Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'firstNotify');
                    console.log("FirstNotify No Error Occurs");
                }
            }

            $scope.selectedAjaxSecondNotifyEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 307), nextFocus)) {
                    console.log("Yes FirstNotify Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'secondNotify');
                    console.log("FirstNotify No Error Occurs");
                }
            }

            $scope.selectedAjaxForwarderEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 302), nextFocus)) {
                    console.log("Yes forwarder Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'forwarder');
                    console.log("forwarder No Error Occurs");
                }
            }

            $scope.selectedAjaxAgentEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 309), nextFocus)) {
                    console.log("Yes agent Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'agent');
                    console.log("agent No Error Occurs");
                }
            }

            $scope.selectedAjaxIssueAgentEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 310), nextFocus)) {
                    console.log("Yes issuingAgent Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'issuingAgent');
                    console.log("issuingAgent No Error");
                }
            }

            $scope.selectedAjaxChaAgentEvent = function(documentDetail, serviceIdx, docIdx, nextFocus) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 311), nextFocus)) {
                    console.log("Yes CHAAgent Error");
                } else {
                    uiShipmentDataService.getPrimaryAddress(documentDetail, 'chaAgent');
                    console.log("CHAAgent No Error");
                }
            }

            $scope.selectedDocumentPackMaster = function(documentDetail, serviceIdx, docIdx, nextFocus, next2Focus) {
                $scope.errorMap = new Map();
                var focus;
                if (documentDetail.dimensionList != undefined && documentDetail.dimensionList != null && documentDetail.dimensionList.length > 0 && EmptyRowChecker.isEmptyRow(documentDetail.dimensionList[0])) {
                    focus = nextFocus;
                } else {
                    focus = next2Focus;
                }

                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, 312), focus)) {} else {
                    uiShipmentDataService.assignedDocumentToService($scope.shipment.shipmentServiceList[serviceIdx], "packMaster");
                }
            };

            $scope.validateFields = function(documentDetail, serviceIdx, docIdx, code, shipmentService, isBookedVolumeUnitCbm) {
                $scope.errorMap = new Map();
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService(documentDetail, serviceIdx, docIdx, code, shipmentService), undefined)) {

                } else {
                    if (code === 313 || code === 315 || code === 316 || code === 317 || code === 318 || code === 319) {
                        $scope.changeDocumentDimension(documentDetail, $scope.shipment.shipmentServiceList[serviceIdx], isBookedVolumeUnitCbm);
                    }
                }
            }

            $scope.change_HawbRatedUnrated = function(val) {
                $rootScope.hawbRated_Unrated = val;
            }

            $scope.changeDocumentDimension = function(documentDetail, service) {

                uiShipmentCalculationService.calculateDocumentWeight(documentDetail, service)

            }

            $scope.isDimensionFound = function(service) {
                return uiShipmentCalculationService.isDimensionFound(service);

            }

            $scope.isDocumentDimensionFound = function(document) {
                return uiShipmentCalculationService.isDocumentDimensionFound(document);

            }
            $scope.chargableWeightCalulcate = function(service, whichValue) {
                return uiShipmentCalculationService.chargableWeightCalulcate(service, whichValue);

            }

            $scope.calculateWeight = function(param, documentDetail, service) {
                $scope.errorMap = new Map()
                uiShipmentCalculationService.calculateWeight(param, documentDetail, service);
            }


            $scope.cancelToList = function() {

                //            	console.log("New Data ", JSON.stringify($scope.shipment));
                if ($scope.oldData != JSON.stringify($scope.shipment)) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    }).then(
                        function(value) {
                            if (value == 1 && $scope.shipment != undefined &&
                                ($scope.shipment.id == null || $scope.shipment.id == undefined || $scope.shipment.id == -1)) {
                                $scope.saveShipment();
                            } else if (value == 1 &&
                                $scope.shipment != undefined && $scope.shipment.id != null) {
                                $scope.isCreateConsol = null;
                                $scope.saveShipment();
                            } else if (value == 2) {
                                if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {
                                    var params = {};
                                    params.forPurpose = "createdShipmentFromConsol";
                                    params.shipmentUid = $scope.shipment.shipmentUid;
                                    $state.go($stateParams.fromScreen, params);
                                    console.log("navigate to consol screen");
                                } else if ($stateParams.fromState != undefined && $stateParams.fromState != null &&
                                    ($stateParams.fromState != "" && $stateParams.fromState.length > 0) &&
                                    $stateParams.fromStateParams != undefined && $stateParams.fromStateParams != null) {
                                    $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                                } else if ($stateParams.fromScreen != undefined && $stateParams.fromScreen != null) {
                                    if ($stateParams.forPurpose != null && $stateParams.forPurpose != undefined && $stateParams.forPurpose == 'activiti') {
                                        $state.go($stateParams.fromScreen, { submitAction: 'Cancelled' });
                                    } else {
                                        var params = { submitAction: 'Cancelled' }
                                        $state.go('layout.crmShipment', params);
                                    }
                                } else {
                                    var params = { submitAction: 'Cancelled' }
                                    $state.go('layout.crmShipment', params);
                                }
                            } else {
                                console.log("cancelled");
                            }
                        });
                } else {
                    var params = {};
                    if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {
                        params.forPurpose = "createdShipmentFromConsol";
                        params.shipmentUid = $scope.shipment.shipmentUid;
                        $state.go($stateParams.fromScreen, params);
                        console.log("navigate to consol screen");
                    } else if ($stateParams.fromScreen != undefined && $stateParams.fromScreen != null) {

                        if ($stateParams.forPurpose != null && $stateParams.forPurpose != undefined && $stateParams.forPurpose == 'activiti') {
                            $state.go($stateParams.fromScreen, { submitAction: 'Cancelled' });
                        } else {
                            var params = { submitAction: 'Cancelled' }
                            $state.go('layout.crmShipment', params);
                        }
                    } else if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
                        if (window.history != undefined)
                            window.history.back();
                    } else {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go('layout.crmShipment');
                    }
                }
            }


            $scope.checkIsItFromConsol = function(shipment, params) {
                if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {
                    console.log("shipment creation from consol import");
                    if (shipment != undefined && shipment != null && shipment.shipmentServiceList != undefined && shipment.shipmentServiceList != null &&
                        shipment.shipmentServiceList.length > 0) {
                        params.forPurpose = "createdShipmentFromConsol";
                        params.shipmentUid = shipment.shipmentUid;

                        /*if($rootScope.shipmentFromConsol.shipmentLinkList!=undefined 
                        	   && $rootScope.shipmentFromConsol.shipmentLinkList!=null
                        	   && $rootScope.shipmentFromConsol.shipmentLinkList.length==0){

                        		$rootScope.shipmentFromConsol.consolDocument.noOfPieces=0;
                        		$rootScope.shipmentFromConsol.consolDocument.grossWeight=0.0;
                        		$rootScope.shipmentFromConsol.consolDocument.volumeWeight=0.0;
                        		$rootScope.shipmentFromConsol.consolDocument.chargebleWeight=0.0;
                        		$rootScope.shipmentFromConsol.consolDocument.grossWeightInPound=0.0;
                        		$rootScope.shipmentFromConsol.consolDocument.volumeWeightInPound=0.0;

                        		  if($rootScope.shipmentFromConsol.consolDocument.dimensionList!=undefined 
                        		   && $rootScope.shipmentFromConsol.consolDocument.dimensionList!=null
                        		   && $rootScope.shipmentFromConsol.consolDocument.dimensionList.length!=0){
                        				$rootScope.shipmentFromConsol.consolDocument.dimensionList = [];
                        		  }
                        	}else{
                        		
                        	}*/

                        if ($rootScope.shipmentFromConsol.shipmentLinkList == undefined ||
                            $rootScope.shipmentFromConsol.shipmentLinkList == null ||
                            $rootScope.shipmentFromConsol.shipmentLinkList.length == 0) {
                            $rootScope.shipmentFromConsol.shipmentLinkList = [];
                            if ($rootScope.shipmentFromConsol != undefined && $rootScope.shipmentFromConsol.consolDocument.dimensionList != null &&
                                $rootScope.shipmentFromConsol.consolDocument.dimensionList.length > 0) {
                                $rootScope.shipmentFromConsol.consolDocument.dimensionList = []; //making empty dimension..will add while in link
                            }
                        }
                        $scope.tmpShipmentLink = {};
                        $scope.tmpShipmentLink.service = shipment.shipmentServiceList[0];
                        $rootScope.shipmentFromConsol.shipmentLinkList.push($scope.tmpShipmentLink);
                        $rootScope.shipmentFromConsol.consolDocument.mawbNo = shipment.shipmentServiceList[0].mawbNo;
                        $state.go($stateParams.fromScreen, params);
                        return true;
                    }
                }
                return false;
            }

            $scope.showResponse = function(shipment, isAdded) {
                var newScope = $scope.$new();
                newScope.shipmentUid = shipment.shipmentUid;
                newScope.opt = isAdded == true ? 'saved' : 'updated';

                ngDialog.openConfirm({
                    template: '<p>Shipment  {{shipmentUid}} successfully {{opt}}.</p> ' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok </button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default top_9999'
                }).then(function(value) {
                    if ($scope.hawbModal != undefined && $scope.hawbModal != null) {
                        $scope.hawbModal.$promise.then($scope.hawbModal.hide);
                    }
                    var params = {};
                    if ($scope.checkIsItFromConsol(shipment, params)) {
                        console.log("navigate to consol ")
                    } else {
                        var stateCount = 0;
                        if ($stateParams.count != undefined && $stateParams.count != null) {
                            stateCount = $stateParams.count + 1;
                        }
                        $state.go("layout.editNewShipment", { shipmentId: shipment.id, submitAction: 'Saved', count: stateCount });
                    }
                }, function(value) {
                    $state.go("layout.crmShipment", { submitAction: 'Cancelled' });
                });
            }

            $scope.validateServices = function() {

                for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {

                    if ($scope.shipment.shipmentServiceList[i].lastUpdatedStatus != undefined && $scope.shipment.shipmentServiceList[i].lastUpdatedStatus != null &&
                        $scope.shipment.shipmentServiceList[i].lastUpdatedStatus.serviceStatus == 'Cancelled') {
                        continue;
                    }

                    /*	if($scope.shipment.shipmentServiceList[i].location.id!=$rootScope.userProfile.selectedUserLocation.id){
                    		continue;
                    	}*/

                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, $scope.shipment.shipmentServiceList[i], i, 0), undefined)) {
                        uiShipmentDataService.assignedDocumentToService($scope.shipment.shipmentServiceList[i], "all");
                    } else {
                        return false;
                    }

                    if ($scope.shipment.shipmentServiceList[i].isConnectionCheck() == false) {
                        Notification.error($rootScope.nls["ERR90560"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                        $scope.shipmentTab = 'serviceRouting';
                        return false;
                    }

                    var connectionRequired = $rootScope.appMasterData['connection.required'];
                    var connectionRequiredFlag = false;
                    if (connectionRequired.toLowerCase() == 'true' || connectionRequired == true) {
                        if ($scope.shipment.shipmentServiceList[i].origin != null && $scope.shipment.shipmentServiceList[i].origin.id != null) {
                            if ($scope.shipment.shipmentServiceList[i].pol != null && $scope.shipment.shipmentServiceList[i].pol.id != null) {
                                if ($scope.shipment.shipmentServiceList[i].origin.id != $scope.shipment.shipmentServiceList[i].pol.id) {
                                    connectionRequiredFlag = true;
                                }
                            }
                        }

                        if ($scope.shipment.shipmentServiceList[i].destination != null && $scope.shipment.shipmentServiceList[i].destination.id != null) {
                            if ($scope.shipment.shipmentServiceList[i].pod != null && $scope.shipment.shipmentServiceList[i].pod.id != null) {
                                if ($scope.shipment.shipmentServiceList[i].destination.id != $scope.shipment.shipmentServiceList[i].pod.id) {
                                    connectionRequiredFlag = true;
                                }
                            }
                        }

                        if (connectionRequiredFlag) {
                            if ($scope.shipment.shipmentServiceList[i].connectionList == undefined || $scope.shipment.shipmentServiceList[i].connectionList == null || $scope.shipment.shipmentServiceList[i].connectionList.length == 0) {
                                Notification.error($rootScope.nls["ERR90364"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1))
                                $scope.shipment.shipmentServiceList[i].connectionList = [{}];
                                return false;
                            }
                        }
                    }

                    if ($scope.shipment.shipmentServiceList[i].documentList != undefined && $scope.shipment.shipmentServiceList[i].documentList != null &&
                        $scope.shipment.shipmentServiceList[i].documentList.length != 0) {
                        for (var dIdx = 0; dIdx < $scope.shipment.shipmentServiceList[i].documentList.length; dIdx++) {
                            if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService($scope.shipment.shipmentServiceList[i].documentList[dIdx], i, dIdx, 0, $scope.shipment.shipmentServiceList[i]), undefined)) {
                                return false;
                            } else {
                                if ($scope.shipment.shipmentServiceList[i].isDimensionStatus() == false) {
                                    $scope.openTab('cargoDetails', i, $scope.shipment.shipmentServiceList[i]);
                                    Notification.error($rootScope.nls["ERR8340"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1))
                                    return false;
                                }
                            }
                        }
                    } else {
                        Notification.error($rootScope.nls["ERR90558"]);
                        $scope.openTab('hawb', i, $scope.shipment.shipmentServiceList[i]);
                        return false;
                    }

                    if ($scope.shipment.shipmentServiceList[i].isRatesCheck() == false) {
                        Notification.error($rootScope.nls["ERR90561"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                        return false;
                    }

                    if ($scope.shipment.shipmentServiceList[i].authenticatedDocList != undefined && $scope.shipment.shipmentServiceList[i].authenticatedDocList != null && $scope.shipment.shipmentServiceList[i].authenticatedDocList.length > 0) {
                        for (var j = 0; j < $scope.shipment.shipmentServiceList[i].authenticatedDocList.length; j++) {
                            if (EmptyRowChecker.isEmptyRow($scope.shipment.shipmentServiceList[i].authenticatedDocList[j])) {
                                $scope.shipment.shipmentServiceList[i].authenticatedDocList.splice(j, 1);
                                delete $scope.shipment.shipmentServiceList[i].authenticatedDocList[j];
                            }
                        }
                    }

                    if ($scope.callValidateAuthenticatedDocs($scope.shipment.shipmentServiceList[i]) == false) {
                        Notification.error($rootScope.nls["ERR05571"]);
                        return false;
                    }

                    if ($scope.validatePickUpDelivery($scope.shipment.shipmentServiceList[i].pickUpDeliveryPoint, 0, $scope.selectedShipmentServiceIndex)) {
                        return false;
                    }
                }
                return true;
            }


            $scope.saveShipment = function() {

                //validate services method used for both in shipment validation and hawb validation..if any changes..check hawb update too

                if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {

                    for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {
                        if ($scope.shipment.shipmentServiceList[i].serviceMaster != undefined &&
                            $scope.shipment.shipmentServiceList[i].serviceMaster != null &&
                            $scope.shipment.shipmentServiceList[i].serviceMaster.id != null &&
                            $scope.shipment.shipmentServiceList[i].serviceMaster.transportMode == 'Air' &&
                            $scope.shipment.shipmentServiceList[i].serviceMaster.importExport == 'Import' &&
                            ($scope.shipment.shipmentServiceList[i].serviceMaster.serviceType == undefined ||
                                $scope.shipment.shipmentServiceList[i].serviceMaster.serviceType == null)) {

                            $scope.shipment.shipmentServiceList[i].isFreehandShipment = true;

                        }
                    }

                }

                if (!$scope.validateServices()) {
                    $scope.spinner = false;
                    return;
                }


                if ($scope.shipment.id == null || $scope.shipment.id == -1) {
                    $scope.saveToDatabase();
                } else {
                    $scope.updateToDatabase();
                }
            }

            var modifyRequest = function(modifiedObject) {

                if (modifiedObject.createdBy) {
                    modifiedObject.createdBy = {
                        "versionLock": modifiedObject.createdBy.versionLock,
                        "id": modifiedObject.createdBy.id
                    }
                }
                if (modifiedObject.companyMaster) {
                    modifiedObject.companyMaster = {
                        "versionLock": modifiedObject.companyMaster.versionLock,
                        "id": modifiedObject.companyMaster.id
                    }
                }
                if (modifiedObject.location) {
                    modifiedObject.location = {
                        "id": modifiedObject.location.id,
                        "versionLock": modifiedObject.location.versionLock,
                        "countryMaster": {
                            "versionLock": modifiedObject.location.countryMaster.versionLock,
                            "id": modifiedObject.location.countryMaster.id
                        }
                    }
                }
                if (modifiedObject.country) {
                    modifiedObject.country = {
                        "versionLock": modifiedObject.country.versionLock,
                        "id": modifiedObject.country.id
                    }
                }

                if (modifiedObject.company) {
                    modifiedObject.company = {
                        "versionLock": modifiedObject.company.versionLock,
                        "id": modifiedObject.company.id
                    }
                }

                return modifiedObject;
            }


            $scope.saveToDatabase = function() {
                var tmpObj = cloneService.clone($scope.shipment);

                tmpObj = modifyRequest(tmpObj);
                $rootScope.mainpreloder = true;
                ShipmentAdd.save(uiShipmentDataService.convertToSaveObject(tmpObj)).$promise.then(function(data) {
                    if (data.responseCode === 'ERR0') {
                        $scope.showResponse(data.responseObject, true);
                    } else {
                        console.log("Shipment added Failed ", data.responseDescription)
                    }
                    $rootScope.mainpreloder = false;
                }, function(error) {
                    console.log("Shipment added Failed : " + error)
                    $rootScope.mainpreloder = false;
                });
            }


            $scope.updateToDatabase = function() {
                var shipment = cloneService.clone($scope.shipment);
                shipment = modifyRequest(shipment);
                $rootScope.mainpreloder = true;
                ShipmentUpdate.save(uiShipmentDataService.convertToSaveObject(shipment)).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.showResponse(data.responseObject, false);
                    } else if (data.responseCode === 'ERR20') {
                        $rootScope.showResponseForConcurrentException('Shipment', shipment.id, shipment.shipmentUid, 'layout.editNewShipment', 'layout.crmShipment', { shipmentId: shipment.id, submitAction: 'Updated' });
                    } else {
                        console.log("Shipment added Failed ", data.responseDescription)
                    }
                    $rootScope.mainpreloder = false;
                }, function(error) {
                    console.log("Shipment added Failed : " + error)
                    $rootScope.mainpreloder = false;
                });
            }

            $scope.changeShipmentServiceView = function(index, tabName, reason, id) {

                try {

                    $scope.selectedShipmentServiceIndex = index;

                    var shipmentServiceDetail = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex];

                    if ($scope.shipment.id == undefined) {
                        if (tabName != undefined && tabName != null) {
                            if (tabName == 'serviceRouting' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_SERVICE_ROUTING_CREATE)) {
                                $scope.shipmentTab = tabName;
                            } else {
                                $scope.shipmentTab = null;
                            }
                            if (tabName == 'cargoDetails' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_CARGO_DETAILS_CREATE)) {
                                $scope.shipmentTab = tabName;

                                if (reason != undefined && reason == 'error') {

                                    uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                    if (id == undefined) {
                                        $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                                    } else {
                                        $rootScope.navigateToNextField(id);
                                    }


                                } else {

                                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                        uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                        if (id == undefined) {
                                            $rootScope.navigateToNextField('shipment.serviceDetail.bookingPerson.' + index);
                                        } else {
                                            $rootScope.navigateToNextField(id);
                                        }
                                    }
                                }
                            }
                            if (tabName == 'hawb' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_HAWB_CREATE)) {
                                $scope.shipmentTab = tabName;

                                if (reason != undefined && reason == 'error') {

                                    uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                    if (id == undefined) {
                                        $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                                    } else {
                                        $rootScope.navigateToNextField(id);
                                    }


                                } else {

                                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                        uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                        if (id == undefined) {
                                            $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                                        } else {
                                            $rootScope.navigateToNextField(id);
                                        }
                                    }
                                }


                            }

                            if (tabName == 'pickupDelivery' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_PICKUP_DELIVERY_CREATE)) {
                                $scope.shipmentTab = tabName;
                            }
                            if (tabName == 'aes' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_AES_RATES_CREATE)) {
                                $scope.shipmentTab = tabName;
                            }
                            if (tabName == 'others' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_CREATE)) {
                                $scope.shipmentTab = tabName;
                            }

                        }
                    } else {
                        if (tabName != undefined && tabName != null) {
                            if (tabName == 'serviceRouting' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_SERVICE_ROUTING_MODIFY)) {
                                $scope.shipmentTab = tabName;
                            } else {
                                $scope.shipmentTab = null;
                            }
                            if (tabName == 'cargoDetails' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_CARGO_DETAILS_MODIFY)) {
                                $scope.shipmentTab = tabName;

                                if (reason != undefined && reason == 'error') {

                                    uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                    if (id == undefined) {
                                        $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                                    } else {
                                        $rootScope.navigateToNextField(id);
                                    }


                                } else {

                                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                        uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                        if (id == undefined) {
                                            $rootScope.navigateToNextField('shipment.serviceDetail.bookingPerson.' + index);
                                        } else {
                                            $rootScope.navigateToNextField(id);
                                        }
                                    }
                                }

                            }
                            if (tabName == 'hawb' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_HAWB_MODIFY)) {
                                $scope.shipmentTab = tabName;

                                if (reason != undefined && reason == 'error') {
                                    uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                    if (id == undefined) {
                                        $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                                    } else {
                                        $rootScope.navigateToNextField(id);
                                    }


                                } else {

                                    if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, index, 1000), undefined)) {
                                        uiShipmentDataService.populateHAWB(shipmentServiceDetail, $scope.shipment.isFromConsolForImport, shipmentServiceDetail.documentList[0]);
                                        if (id == undefined) {
                                            $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                                        } else {
                                            $rootScope.navigateToNextField(id);
                                        }
                                    }
                                }

                            }
                            if (tabName == 'pickupDelivery' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_PICKUP_DELIVERY_MODIFY)) {
                                $scope.shipmentTab = tabName;
                            }
                            if (tabName == 'aes' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_AES_RATES_MODIFY)) {
                                $scope.shipmentTab = tabName;
                            }
                            if (tabName == 'others' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_MODIFY)) {
                                $scope.shipmentTab = tabName;
                            }

                        }

                    }


                    if (reason == undefined || reason != 'error') {
                        $rootScope.navigateToNextField('shipment.serviceDetail.serviceMaster.' + index);
                    }
                    //                $scope.openTab($scope.shipmentTab, index, $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex]);

                } catch (e) {
                    console.log("exception in change shipment service view" + e);
                }


            }



            $scope.addNewShipmentService = function(noOfServices) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_CREATE)) {
                    var isServiceError = false;
                    if ($scope.shipment.shipmentServiceList != undefined && $scope.shipment.shipmentServiceList != null) {
                        for (var sIdx = 0; sIdx < $scope.shipment.shipmentServiceList.length; sIdx++) {
                            if ($scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, $scope.shipment.shipmentServiceList[sIdx], sIdx, 0), undefined)) {
                                isServiceError = true;
                                break;
                            } else {
                                if ($scope.shipment.shipmentServiceList[sIdx].documentList != undefined && $scope.shipment.shipmentServiceList[sIdx].documentList != null &&
                                    $scope.shipment.shipmentServiceList[sIdx].documentList.length != 0) {
                                    for (var dIdx = 0; dIdx < $scope.shipment.shipmentServiceList[sIdx].documentList.length; dIdx++) {
                                        if ($scope.focusAndMessageShower(uiShipmentValidationService.validateDocumentService($scope.shipment.shipmentServiceList[sIdx].documentList[dIdx], sIdx, dIdx, 0, $scope.shipment.shipmentServiceList[sIdx]), undefined)) {
                                            isServiceError = true;
                                            break;
                                        }
                                    }
                                } else {
                                    isServiceError = true;
                                    Notification.error($rootScope.nls["ERR90558"]);
                                    $scope.openTab('hawb', sIdx, $scope.shipment.shipmentServiceList[sIdx])
                                }
                            }
                        }
                    }
                    if (!isServiceError) {
                        $scope.shipment = uiShipmentDataService.addNewShipmentService($scope.shipment);
                        if (noOfServices != undefined) {
                            $scope.changeShipmentServiceView(noOfServices, "serviceRouting");
                        }
                    }

                }
            }


            $scope.nextTabNav = function(event, index, shipmentServiceDetail, tabName, id) {
                if (!event.shiftKey) {
                    $scope.openTab(tabName, index, shipmentServiceDetail, id);
                }
            }

            //shipment flight schedule code starts
            var flightScheduleModal;
            $scope.flightmodel = function() {
                var shipmentServiceDetail = $scope.getShipmentService();
                $scope.flightSchedule = {};
                $scope.searchDto = {};
                $scope.searchDto.orderByType = 'asc';
                $scope.searchDto.sortByColumn = 'etd';
                if (shipmentServiceDetail.carrier != null && shipmentServiceDetail.carrier != undefined &&
                    shipmentServiceDetail.carrier.id != null) {
                    $scope.searchDto.carrier = shipmentServiceDetail.carrier.carrierName;
                }
                $scope.flightScheduleSearchResult = [];
                $scope.selectedFlightScheduleData = null;
                $scope.flightScheduleLimitArray = [10, 15, 20];
                $scope.flightSchedulePage = 0;
                $scope.flightScheduleLimit = 10;
                $scope.flightScheduleTotalRecord = 0;
                $scope.flightScheduleSearch();
                flightScheduleModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/crm/shipment/shipment_flight_schedule_popup.html',
                    show: false
                });
                $scope.navigateToNextField("flightSchedule.service");
                flightScheduleModal.$promise.then(flightScheduleModal.show)
            };

            $scope.datepickeropts = {
                clearLabel: 'Clear',
                locale: {
                    applyClass: 'btn-green',
                    applyLabel: "Apply",
                    fromLabel: "From",
                    //format: $rootScope.userProfile.selectedUserLocation.dateFormat,
                    format: $rootScope.userProfile.selectedUserLocation.dateFormat ? $rootScope.userProfile.selectedUserLocation.dateFormat : "DD-MM-YYYY",
                    toLabel: "To",
                    //cancelLabel: 'Clear',
                    //customRangeLabel: 'Custom range'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()]
                },
                eventHandlers: {
                    'apply.daterangepicker': function(ev, picker) {
                        $timeout(function() {

                            $scope.flightScheduleSearch();
                        }, 2);
                    },
                    'cancel.daterangepicker': function(ev, picker) {
                        $timeout(function() {
                            $scope.searchDto.eta = null;
                            $scope.searchDto.etd = null;
                            $scope.flightScheduleSearch();
                        }, 2);

                    }
                }
            };



            $scope.flightScheduleSearch = function() {
                console.log("flightScheduleSearch is called.");
                var shipmentServiceDetail = $scope.getShipmentService();
                if ($scope.searchDto == undefined || $scope.searchDto == null) {
                    $scope.searchDto = {};
                }

                $scope.searchDto.selectedPageNumber = $scope.flightSchedulePage;
                $scope.searchDto.recordPerPage = $scope.flightScheduleLimit;

                if ($scope.searchDto.eta != null) {
                    $scope.searchDto.eta.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.eta.startDate);
                    $scope.searchDto.eta.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.eta.endDate);
                } else {
                    $scope.searchDto.eta = null;
                }

                if ($scope.searchDto.etd != null) {
                    $scope.searchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.startDate);
                    $scope.searchDto.etd.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.etd.endDate);
                } else {
                    $scope.searchDto.etd = null;
                }

                console.log("$scope.searchDto ", $scope.searchDto);


                FlightPlanSearchForShipment.query($scope.searchDto).$promise.then(function(data, status) {
                    $scope.flightScheduleTotalRecord = data.responseObject.totalRecord;
                    console.log("$scope.flightScheduleTotalRecord ", $scope.flightScheduleTotalRecord);
                    var tempArr = [];
                    var resultArr = [];
                    tempArr = data.responseObject.searchResult;
                    var tempObj = {};
                    angular.forEach(tempArr, function(item, index) {
                        tempObj = item;
                        tempObj.no = (index + 1) + ($scope.flightSchedulePage * $scope.flightScheduleLimit);
                        resultArr.push(tempObj);
                        tempObj = {};
                    });

                    $scope.flightScheduleSearchResult = resultArr;

                });


            }

            $scope.toggleFlightPlan = function(flightSchedule) {
                if ($scope.flightDetail == 'child' + flightSchedule.id) {
                    $scope.flightDetail = null;
                    $scope.flightSchedule = null;
                } else {
                    $scope.flightDetail = 'child' + flightSchedule.id;
                    $scope.flightSchedule = flightSchedule;
                }
            }

            $scope.flightScheduleChangePage = function(param) {
                console.log("flightScheduleChangePage ", param);
                $scope.flightSchedulePage = param.page;
                $scope.flightScheduleLimit = param.size;
                $scope.flightScheduleSearch();

            }

            $scope.flightScheduleLimitChange = function(item) {
                console.log("flightScheduleLimitChange ", item);
                $scope.flightSchedulePage = 0;
                $scope.flightScheduleLimit = item;
                $scope.flightScheduleSearch();
            }

            $scope.flightScheduleSortChange = function(sortKey) {

                $scope.searchDto.sortByColumn = sortKey;
                if ($scope.searchDto.orderByType == 'asc') {
                    $scope.searchDto.orderByType = 'desc';
                } else {
                    $scope.searchDto.orderByType = 'asc';
                }
                $scope.flightScheduleSearch();

            }


            $scope.selectedFlightSchedule = function() {
                flightScheduleModal.hide();
                if ($scope.flightSchedule != null && $scope.flightSchedule != undefined) {
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].carrier = $scope.flightSchedule.carrierMaster;
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].routeNo = $scope.flightSchedule.flightNo;
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eta = $rootScope.dateAndTimeToString($scope.flightSchedule.eta);
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].etd = $rootScope.dateAndTimeToString($scope.flightSchedule.etd);
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].scheduleUid = $scope.flightSchedule.scheduleId;
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].vesselId = $scope.flightSchedule.id;
                }

                $rootScope.navigateToNextField('shipment.serviceDetail.mawb.' + $scope.selectedShipmentServiceIndex);
            }

            $scope.getShipmentService = function() {
                return $scope.shipmentService = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex];
            }

            $scope.cancelFlightSchedule = function() {
                $scope.flightSchedule = {};
                $rootScope.navigateToNextField('mawb' + $scope.selectedTabIndex);
            };

            $scope.newFlightSchedule = function(flightSchedule) {

                $scope.flightSchedule = flightSchedule;

            };



            //shipment flight schedule code ends

            //attachment button
            var attachmentModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_attachment_popup.html',
                show: false
            });
            $scope.showAttachmentPopUp = function(shipmentServiceDetail, index) {
                $scope.shipmentAttachmentListBackUp = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList);
                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList == undefined || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList == null || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList.length == 0) {
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList = [{}];
                }
                $scope.shipmentAttachmentListData = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList);
                $rootScope.navigateToNextField(index + 'attachDocument0');
                attachmentModal.$promise.then(attachmentModal.show);
            };




            $scope.saveShipmentServiceAttachment = function() {

                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentAttachmentList = $scope.shipmentAttachmentListData;

                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].isAttachmentCheck() == false) {
                    Notification.error($rootScope.nls["ERR250"] + "  " + $rootScope.nls["ERR90572"] + "-" + ($scope.selectedShipmentServiceIndex + 1));
                    console.log("Attachment is invalid");
                    return false;
                }

                attachmentModal.$promise.then(attachmentModal.hide);
            }

            $scope.downloadAttach = function(param) {
                console.log("download ", param);

                if (param.data.id != null && (param.data.file == null)) {
                    console.log("API CALL")
                    console.log("download ", param);
                    if (param.data.id != null && param.data.file == null) {
                        console.log("API CALL")
                        $http({
                            url: $rootScope.baseURL + '/api/v1/shipment/files/' + param.data.id + '/' + param.type + '/false',
                            method: "GET",
                            responseType: 'arraybuffer'
                        }).success(function(data, status, headers, config) {
                            console.log("hiddenElement ", data)
                            var blob = new Blob([data], {});
                            console.log("blob ", blob);
                            saveAs(blob, param.data.fileName);
                        }).error(function(data, status, headers, config) {
                            //upload failed
                        });
                    }
                } else {
                    console.log("NO API CALL", param);
                    saveAs(param.data.tmpFile, param.data.fileName);
                }
            }


            $scope.cancelPopup = function(popupService) {
                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex][popupService] = $scope[popupService + "BackUp"];
                if (popupService === "shipmentAttachmentList") {
                    attachmentModal.$promise.then(attachmentModal.hide);
                } else if (popupService === "eventList") {
                    for (i = 0; i < $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.length; i++) {
                        if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventMaster == undefined ||
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventMaster == null ||
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventDate == undefined ||
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventDate == null ||
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventDate == ""
                        ) {
                            $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.splice(i, 1);
                        }
                    }
                    addeventModal.$promise.then(addeventModal.hide);
                } else {
                    if (popupService === "referenceList") {
                        referenceModel.$promise.then(referenceModel.hide);
                    } else {
                        referenceModel.$promise.then(boeModel.hide);
                    }
                }
            }



            /*Bill Of Entry Modal*/
            var boeModel = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_boe_popup.html',
                show: false
            });



            $scope.showBillOfEntryPopup = function(shipmentServiceDetail, index) {


                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList == undefined || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList == null || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList.length == 0) {
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList = [];
                    //$scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList.push({});
                }
                $scope.billOfEntryListData = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList);
                if ($scope.billOfEntryListData != undefined && $scope.billOfEntryListData != null && $scope.billOfEntryListData.length != undefined && $scope.billOfEntryListData.length == 0) {
                    $scope.billOfEntryListData.push({});
                }
                $rootScope.navigateToNextField(index + 'billOfEntry0');
                boeModel.$promise.then(boeModel.show)
            };



            $scope.hideBillOfEntryPopup = function() {

                $scope.billOfEntryListData = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList);
                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].isBoeCheck() == false) {

                }
                boeModel.$promise.then(boeModel.hide)
            };




            $scope.saveShipmentServiceBillOfEntry = function() {

                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].isBoeCheck() == false) {
                    Notification.error($rootScope.nls["ERR70111"] + "  " + $rootScope.nls["ERR90572"] + "-" + ($scope.selectedShipmentServiceIndex + 1));
                    console.log("isBoeCheck failed :" + $scope.selectedShipmentServiceIndex);
                    return false;
                }
                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].billOfEntryList = cloneService.clone($scope.billOfEntryListData);
                boeModel.$promise.then(boeModel.hide);
            }





            /*Trigger Modal*/
            var triggerModel = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_trigger_popup.html',
                show: false
            });

            $scope.showTriggerPopUp = function(shipmentServiceDetail, index) {
                /*	
                	if($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList == undefined  || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList == null || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList.length == 0) {
                		$scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList = [{}];
                	}
                	$scope.shipmentServiceTriggerListData = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList); 
                	*/
                //$rootScope.navigateToNextField('statusDate');
                triggerModel.$promise.then(triggerModel.show)
            };

            $scope.closeTriggerPopUp = function() {
                console.log("closing Trigger PopUp ", $scope.shipment);
                triggerModel.$promise.then(triggerModel.hide);
            }


            $scope.saveShipmentServiceTrigger = function() {
                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList = $scope.shipmentServiceTriggerListData;

                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].isStatusCheck() == false) {
                    Notification.error($rootScope.nls["ERR90562"] + "  " + $rootScope.nls["ERR90572"] + "-" + ($scope.selectedShipmentServiceIndex + 1));
                    console.log("Trigger is invalid");
                    return false;
                }

                triggerModel.$promise.then(triggerModel.hide);
            }


            $scope.editStatusUpdateObject = function(shipmentServiceDetail, statusUpdateObject, objectIndex) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_TRIGGER_UPDATE_MODIFY)) {
                    console.log("editStatusUpdateObject ", statusUpdateObject, objectIndex);
                    shipmentServiceDetail.currentObject(statusUpdateObject, objectIndex);
                }
            }

            $scope.deleteStatusUpdateObject = function(statusUpdateObject, index) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_TRIGGER_UPDATE_DELETE)) {
                    console.log("deleteStatusUpdateObject ", statusUpdateObject, index);
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].shipmentServiceTriggerList.splice(index, 1);
                }

            }

            /*Event Modal*/
            /*open Event Modal*/
            var addeventModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_event_popup.html',
                show: false
            });;
            $scope.eventmodel = function(shipmentServiceDetail, index) {
                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList == undefined ||
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList == null ||
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.length == 0) {
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList = [];
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.push({ isCompleted: 'Yes', followUpRequired: 'No' });
                }
                $scope.eventListBackUp = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList);
                $scope.eventTmpList = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList);
                $rootScope.navigateToNextField(index + 'eventMaster0');
                addeventModal.$promise.then(addeventModal.show);
            };

            /*Cancel event modal */

            $scope.cancelEventModal = function() {
                    addeventModal.$promise.then(addeventModal.hide);
                }
                /*Save event modal */
            $scope.saveEventModal = function() {

                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList = cloneService.clone($scope.eventTmpList);
                    if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].isEventCheck()) {
                        if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.length > 0) {
                            for (i = 0; i < $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.length; i++) {
                                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventMaster == undefined ||
                                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventMaster == null ||
                                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventDate == undefined ||
                                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventDate == null ||
                                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList[i].eventDate == ""
                                ) {
                                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].eventList.splice(i, 1);
                                }
                            }
                        }
                        addeventModal.$promise.then(addeventModal.hide);
                    }

                }
                //Fetch Mawb
            $scope.fetchMawb = function(shipmentServiceDetail, serviceIdx) {
                console.log("FetchMAWB is called");
                //Validation Carrier And Origin to Fetch Mawb
                if ($scope.focusAndMessageShower(uiShipmentValidationService.validateCarrier(shipmentServiceDetail, serviceIdx), undefined)) {
                    return;
                }
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validateShipmentService($scope.shipment, shipmentServiceDetail, serviceIdx, 6), undefined)) {

                    if (shipmentServiceDetail.mawbNo == undefined || shipmentServiceDetail.mawbNo == null || shipmentServiceDetail.mawbNo == "") {
                        GetNextMawb.fetch({
                            "carrierId": shipmentServiceDetail.carrier.id,
                            "portId": shipmentServiceDetail.origin.id
                        }).$promise.then(
                            function(data) {
                                if (data.responseCode == "ERR0") {
                                    if (data.responseObject != null) {
                                        shipmentServiceDetail.mawbNo = data.responseObject.mawbNo;
                                        shipmentServiceDetail.mawbDate = $rootScope.sendApiDateAndTime(new Date());
                                        //TODO Later
                                        //                                        $scope.updateMoreInfoToDocument('MAWB', 'etdMoreInfoShipment' + $scope.selectedTabIndex)
                                        if (data.responseObject.remindNo != null) {
                                            Notification.error($rootScope.nls["ERR100023"]);
                                        }
                                        $rootScope.navigateToNextField('shipment.serviceDetail.etd.' + serviceIdx);

                                    } else {
                                        Notification.error($rootScope.nls["ERR100022"]);
                                        $rootScope.navigateToNextField('shipment.serviceDetail.mawb.' + serviceIdx)
                                    }
                                }
                            },
                            function(errResponse) {
                                console.error('Error while fetching mawb no');
                            }
                        );
                    } else {
                        Notification.error($rootScope.nls["ERR100026"]);
                        $rootScope.navigateToNextField('shipment.serviceDetail.etd.' + serviceIdx);
                        //$rootScope.navigateToNextField('etdMoreInfoShipment' + serviceIdx);
                    }
                }

            }

            /*Accounts Modal*/
            var cfsReceiveModel = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_cfs.html',
                show: false
            });

            $scope.showCfsPopup = function() {
                $scope.cfsReceiveEntryList = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].cfsReceiveEntryList;
                cfsReceiveModel.$promise.then(cfsReceiveModel.show);

            };

            $scope.validateForCfs = function(service, index) {

                if (!$rootScope.isGoodsReceived(service)) {

                    if (service.carrier == undefined || service.carrier == "" || service.carrier == null || service.carrier.id == undefined) {
                        cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                        Notification.error($rootScope.nls["ERR90063"])
                        uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.carrier." + index, $rootScope.nls["ERR90063"], index, "serviceRouting", undefined);
                        return false;
                    }

                    if (service.routeNo == undefined || service.routeNo == "" || service.routeNo == null) {
                        cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                        Notification.error($rootScope.nls["ERR90067"])
                        uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.flightNo." + index, $rootScope.nls["ERR90067"], index, "serviceRouting", undefined);
                        return false;
                    }

                    if (service.eta == undefined || service.eta == null || service.eta == '') {
                        cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                        Notification.error($rootScope.nls["ERR90095"])
                        uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.eta." + index, $rootScope.nls["ERR90095"], index, "serviceRouting", undefined);
                        return false;
                    }

                    if (service.etd == undefined || service.etd == null || service.etd == '') {
                        cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                        Notification.error($rootScope.nls["ERR90094"])
                        uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.etd." + index, $rootScope.nls["ERR90094"], index, "serviceRouting", undefined);
                        return false;
                    }

                }
                return true;
            }

            $scope.createCFS = function(serviceUid) {

                if ($scope.validateForCfs($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex], $scope.selectedShipmentServiceIndex)) {
                    if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_CFS_CREATE)) {
                        var param = {
                            shipmentUid: $scope.shipment.shipmentUid,
                            serviceUid: $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].serviceUid,
                            fromState: $state.current.name,
                            fromStateParams: $scope.shipment.id
                        };
                        cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                        $state.go("layout.createAirCFS", param);
                    }
                }
            }

            $scope.viewCFS = function(id) {

                console.log("CFS ID " + id);

                var params = {
                    cfsId: id,
                    fromState: $state.current.name,
                    fromStateParams: $scope.shipment.id
                }
                cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                $state.go('layout.viewAirCFS', params);
            }

            /*Accounts Modal*/
            var accountsModel = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_account.html',
                show: false
            });

            $scope.showAccountsPopup = function() {
                if ($scope.shipment.id == undefined) {
                    if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_ACCOUNTS_CREATE)) {
                        var serviceId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;
                        var serviceUid = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].serviceUid;
                        $scope.invoiceList = [];
                        $scope.provisionalObj = {};
                        $scope.getAccountData(serviceId);
                        $scope.getProvisionalData(serviceUid);
                        accountsModel.$promise.then(accountsModel.show);
                    }
                } else {
                    if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_ACCOUNTS_MODIFY)) {
                        var serviceId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;
                        var serviceUid = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].serviceUid;
                        $scope.invoiceList = [];
                        $scope.provisionalObj = {};
                        $scope.getAccountData(serviceId);
                        $scope.getProvisionalData(serviceUid);
                        accountsModel.$promise.then(accountsModel.show);
                    }
                }
            };

            $scope.goToShipmentService = function(shipmentUid) {

                var param = {
                    shipmentUid: shipmentUid,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify($stateParams)
                }
                accountsModel.$promise.then(accountsModel.show);
                $state.go('layout.viewCrmShipment', param);
            }
            $scope.goToConsol = function(consolUid) {
                var param = {
                    consolUid: consolUid,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify($stateParams)
                }
                accountsModel.$promise.then(accountsModel.show);
                $state.go('layout.viewAirConsol', param);
            }

            $scope.getProvisionalData = function(serviceUid) {
                $scope.provisionSpinner = true;
                ProvisionalViewByShipmentServiceUid.get({
                    serviceuid: serviceUid
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.provisionalObj = data.responseObject;
                        $scope.provisionSpinner = false;
                    }
                }, function(error) {
                    console.log("ProvisionalViewByShipmentServiceUid Failed : " + error);
                    $scope.provisionSpinner = false;
                });
            }

            $scope.getAccountData = function(serviceId) {
                $scope.invoiceSpinner = true;
                var searchDto = {};
                searchDto.param1 = serviceId;
                searchDto.param2 = null;
                return ShipmentServiceInvoiceList.fetch(searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.invoiceList = data.responseObject;
                            $scope.invoiceSpinner = false;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching invoicelist');
                        $scope.invoiceSpinner = false;
                    }
                );
            }



            $scope.filterCustomerAgent = function(type) {

                if (type == 'Customer')
                    $scope.customerAgent = 'Customer';
                if (type == 'Agent')
                    $scope.customerAgent = 'Agent';
                if (type == 'All')
                    $scope.customerAgent = undefined;

            }


            $scope.filterInvoiceCreditNote = function(type) {

                if (type == 'Invoice')
                    $scope.documentTypeCode = 'INV';
                if (type == 'Credit Note')
                    $scope.documentTypeCode = 'CRN';
                if (type == 'All')
                    $scope.documentTypeCode = undefined;

            }



            $scope.refmodel = function(provisionalItem) {
                var newScope = $scope.$new();
                newScope.provisionalItem = provisionalItem;
                var myTotalOtherModal = $modal({ scope: newScope, templateUrl: 'app/components/finance/provisional_cost/view/refpopup.html', show: false });
                myTotalOtherModal.$promise.then(myTotalOtherModal.show);
            };

            $scope.provisionalFromShipment = function(object) {
                if (object != null && object.id != null) {
                    if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_MODIFY)) {
                        uiShipmentAccountsService.addOrEditProvisional(object, $scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                    }
                } else {
                    if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_CREATE)) {
                        uiShipmentAccountsService.addOrEditProvisional(null, $scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                    }
                }
            }


            $scope.getInvoiceCrnView = function(invoiceCreditNote) {
                uiShipmentAccountsService.getInvoiceCrnView(invoiceCreditNote, $scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
            }

            /*Refer Modal*/
            var referenceModel = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_reference_popup.html',
                show: false
            });


            $scope.showReferencePopup = function(shipmentServiceDetail, index) {
                $scope.referenceListBackUp = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList);
                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList == undefined || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList == null || $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList.length == 0) {
                    $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList = [{}];
                }
                $scope.referenceListData = cloneService.clone($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList);
                $rootScope.navigateToNextField(index + 'referenceTypeMaster0');
                referenceModel.$promise.then(referenceModel.show)
            };

            $scope.saveShipmentServiceReference = function() {

                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].referenceList = $scope.referenceListData;

                if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].isReferenceCheck() == false) {
                    Notification.error($rootScope.nls["ERR90573"] + "  " + $rootScope.nls["ERR90572"] + "-" + ($scope.selectedShipmentServiceIndex + 1));
                    console.log("isReferenceCheck failed :" + $scope.selectedShipmentServiceIndex);
                    return false;
                }

                attachmentModal.$promise.then(referenceModel.hide);
            }


            $scope.getShipmentService = function() {
                return $scope.shipmentService = $scope.shipment.shipmentServiceList[0];
            }



            $scope.shipmentServiceStatusCancelled = function(service) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MARK_AS_CANCELLED_VIEW)) {
                    ngDialog.openConfirm({
                        template: '<p>Do you want to cancel the shipment Service?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {
                            $scope.saveShipmentStatus("Cancelled", true, service.id);
                        }
                    });
                }
            }

            $scope.saveShipmentStatus = function(status, isPartial, serviceId) {
                console.log("Save statusMethod is called.");

                $rootScope.mainpreloder = true;
                $scope.shipmentDto = {};
                $scope.shipmentDto.shipmentId = $scope.shipment.id;
                $scope.shipmentDto.shipmentStatus = isPartial ? null : status,
                    $scope.shipmentDto.serviceId = isPartial ? serviceId : null,
                    $scope.shipmentDto.serviceStatus = isPartial ? status : null
                ShipmentStatusChange.status($scope.shipmentDto).$promise
                    .then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.shipment = data.responseObject;
                            uiShipmentDataService.editInitialization($scope.shipment)
                            $rootScope.mainpreloder = false;
                        } else {
                            $rootScope.mainpreloder = false;
                            console.log("Service added Failed ", data.responseDescription)
                        }
                        angular.element(".panel-body").animate({ scrollTop: 0 }, "slow");
                    }, function(error) {
                        $rootScope.mainpreloder = false;
                        console.log("Service added Failed : ", error)
                    });

            }

            $scope.serviceToConsol = function(shipment, service) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_ATTACH_TO_MASTER_VIEW)) {
                    if (uiShipmentDataService.serviceToConsol(shipment, service)) {
                        uiShipmentDataService.goToConsol(shipment, service);
                    }
                }
            }

            $scope.setStatus = function(data) {
                if (data == 'Active') {
                    return 'activetype';
                } else if (data == 'Block') {
                    return 'blockedtype';
                } else if (data == 'Hide') {
                    return 'hiddentype';
                }
            }

            $scope.validate = function(valElement, code) {
                    $scope.errorMap = new Map();
                    var validationResponse = countryMasterValidationService.validate($scope.shipment, code);
                    if (validationResponse.error == true) {
                        $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
                    } else {
                        $scope.errorMap.put(valElement, null);
                        validationResponse.error = false;
                    }
                }
                /**
                 * Cancel Add or Edit Country Master.
                 */

            $scope.cancel = function(objId) {
                if ($scope.countryForm != undefined) {
                    $scope.goTocancel($scope.countryForm.$dirty, objId);
                } else {
                    $scope.goTocancel(false, objId);
                }
            }

            /**
             * Common Validation Focus Functionality - starts here" 
             */
            $scope.validationErrorAndFocus = function(valResponse, elemId) {
                $scope.errorMap = new Map();
                if (valResponse.error == true) {
                    $scope.errorMap.put(valResponse.errElement,
                        valResponse.errMessage);
                }
                $scope.navigateToNextField(elemId);
            }

            $scope.deleteShipmentService = function(index) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_DELETE)) {
                    console.log("Delete shipment service is called.....", index);
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR214"];
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    }).
                    then(function(value) {
                        $scope.shipment.shipmentServiceList.splice(index, 1);
                        $scope.initClickTabIndex($scope.shipment);
                    }, function(value) {
                        console.log("Shipment service Deletion cancelled");
                    });
                }
            }

            /*Service tab started*/

            $scope.initClickTabIndex = function(obj) {
                var flag = false;
                $scope.selectedShipmentServiceIndex = 0;
                if (obj.shipmentServiceList != undefined && obj.shipmentServiceList != null && obj.shipmentServiceList.length != 0) {

                    for (var i = 0; i < obj.shipmentServiceList.length; i++) {
                        if (obj.shipmentServiceList[i].location.id == $rootScope.userProfile.selectedUserLocation.id) {
                            $scope.selectedShipmentServiceIndex = i;
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag) {
                    $scope.changeShipmentServiceView($scope.selectedShipmentServiceIndex, 'serviceRouting')
                }
            };

            $scope.clickTabIndex = function(index) {
                $scope.selectedShipmentServiceIndex = index;
                if ($scope.shipment.isFromConsolForImport != undefined && ($scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport)) {
                    $scope.shipmentTab = 'hawb';
                    $rootScope.navigateToNextField('shipment.serviceDetail.' + index + '.document.shipper.' + 0);
                } else {
                    $scope.shipmentTab = 'serviceRouting';
                    $rootScope.navigateToNextField('shipment.serviceDetail.serviceMaster.' + index);
                }

            };

            /*
             * Code for EDIT Section
             * */










            /**
             * Recent History - starts here
             */
            $scope.isReloaded = localStorage.isShipmentDataReloaded;
            $scope.$on('addNewShipmentEvent', function(events, args) {
                $rootScope.unfinishedData = $scope.shipment;
                $rootScope.unfinishedFormTitle = "Shipment (New)";
                $rootScope.category = "Shipment";
                if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.party != undefined && $scope.shipment.party != null &&
                    $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
                    $rootScope.subTitle = $scope.shipment.party.partyName;
                } else {
                    $rootScope.subTitle = "Unknown Party"
                }
                console.log("addCrmShipmentEvent Broadcast Listener :: ", args);
            })

            $scope.$on('editNewShipmentEvent', function(events, args) {
                //console.log("Scope Shipment data :: ", $scope.shipment);
                $rootScope.unfinishedData = $scope.shipment;
                $rootScope.category = "Shipment";
                $rootScope.unfinishedFormTitle = "Shipment Edit # " + $scope.shipment.shipmentUid;
                if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.party != undefined && $scope.shipment.party != null &&
                    $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
                    $rootScope.subTitle = $scope.shipment.party.partyName;
                } else {
                    $rootScope.subTitle = "Unknown Party"
                }
                console.log("editNewShipmentEvent Broadcast Listener :: ", args);
            })

            $scope.$on('addNewShipmentEventReload', function(e,
                confirmation) {

                $scope.setActiveObject();
                confirmation.message = "";
                localStorage.reloadFormData = JSON.stringify($scope.shipment);
                localStorage.isShipmentDataReloaded = "YES";
                e.preventDefault();
            })

            $scope.$on('editNewShipmentEventReload', function(e,
                confirmation) {
                confirmation.message = "";
                $scope.setActiveObject();
                localStorage.reloadFormData = JSON.stringify($scope.shipment);
                localStorage.isShipmentDataReloaded = "YES";
                e.preventDefault();

            })

            $scope.timeoutFocus = function() {

                $timeout(function() {
                    $rootScope.navigateToNextField("shipment.serviceDetail.serviceMaster.0");
                    $scope.oldData = JSON.stringify($scope.shipment);
                }, 1000);
            }


            $scope.getShipmentForEdit = function() {
                console.log("getShipmentForEdit is called ", $stateParams.shipmentId);
                $rootScope.mainpreloder = true;
                ShipmentGet.get({ id: $stateParams.shipmentId }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.shipment = data.responseObject;
                        try {
                            uiShipmentDataService.saveToHistory($scope.shipment, $state, $stateParams);
                        } catch (e) {
                            console.log(e);
                        }
                        uiShipmentDataService.editInitialization($scope.shipment);
                        $scope.changeShipmentServiceView(0, 'serviceRouting');
                        $scope.initClickTabIndex($scope.shipment);
                        $scope.timeoutFocus();
                        $rootScope.mainpreloder = false;
                    } else {
                        $rootScope.mainpreloder = false;
                    }

                }, function(error) {
                    console.log("Shipment get Failed : ", error);
                    $rootScope.mainpreloder = false;
                });
            }

            $scope.selectedPartyAddress = function(obj) {

                var documentDetail = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].documentList[$scope.selectedDocumentIndex];
                if ($scope.partyAddressVal == 'shipperAddress') {
                    documentDetail.shipperAddress = addressJoiner.joinAddressLineByLine(documentDetail.shipperAddress, documentDetail.shipper, obj);
                }
                if ($scope.partyAddressVal == 'forwarderAddress') {
                    documentDetail.forwarderAddress = addressJoiner.joinAddressLineByLine(documentDetail.forwarderAddress, documentDetail.forwarder, obj);
                }
                if ($scope.partyAddressVal == 'consigneeAddress') {
                    documentDetail.consigneeAddress = addressJoiner.joinAddressLineByLine(documentDetail.consigneeAddress, documentDetail.consignee, obj);
                }
                if ($scope.partyAddressVal == 'firstNotifyAddress') {
                    documentDetail.firstNotifyAddress = addressJoiner.joinAddressLineByLine(documentDetail.firstNotifyAddress, documentDetail.firstNotify, obj);
                }
                if ($scope.partyAddressVal == 'secondNotifyAddress') {
                    documentDetail.secondNotifyAddress = addressJoiner.joinAddressLineByLine(documentDetail.secondNotifyAddress, documentDetail.secondNotify, obj);
                }
                if ($scope.partyAddressVal == 'agentAddress') {
                    documentDetail.agentAddress = addressJoiner.joinAddressLineByLine(documentDetail.agentAddress, documentDetail.agent, obj);
                }
                if ($scope.partyAddressVal == 'issuingAgentAddress') {
                    documentDetail.issuingAgentAddress = addressJoiner.joinAddressLineByLine(documentDetail.issuingAgentAddress, documentDetail.issuingAgent, obj);
                }
                if ($scope.partyAddressVal == 'chaAgentAddress') {
                    documentDetail.chaAgentAddress = addressJoiner.joinAddressLineByLine(documentDetail.chaAgentAddress, documentDetail.chaAgent, obj);
                }

                $scope.closeAuthenticatedAddressBar();
            };

            $scope.mapShowingFunctionality = function() {

            }
            NgMap.getMap().then(function(map) {
                $scope.map = map;
            });
            $scope.googleMapModal = function(sourcePlace, destinationPlace) {
                var src = "";
                $scope.sourceMapPlace = "";
                $scope.destinationMapPlace = "";
                if (sourcePlace != undefined && sourcePlace != null) {
                    if (sourcePlace.addressLine1 != undefined && sourcePlace.addressLine1 != null && sourcePlace.addressLine1.length > 0) {
                        src = sourcePlace.addressLine1;
                    }
                    if (sourcePlace.addressLine2 != undefined && sourcePlace.addressLine2 != null && sourcePlace.addressLine2.length > 0) {
                        if (src.length > 0) {
                            src = src + ", ";
                        }
                        src = src + sourcePlace.addressLine2;
                    }
                    if (sourcePlace.addressLine3 != undefined && sourcePlace.addressLine3 != null && sourcePlace.addressLine3.length > 0) {
                        if (src.length > 0) {
                            src = src + ", ";
                        }
                        src = src + sourcePlace.addressLine1;
                    }
                    if (sourcePlace.city != undefined && sourcePlace.city != null && sourcePlace.city.cityName != undefined &&
                        sourcePlace.city.cityName != null && sourcePlace.city.cityName.length > 0) {
                        if (src.length > 0) {
                            src = src + ", ";
                        }
                        src = src + sourcePlace.city.cityName;
                    }
                    if (sourcePlace.zipCode != undefined && sourcePlace.zipCode != null && sourcePlace.zipCode.length > 0) {
                        if (src.length > 0) {
                            src = src + ", ";
                        }
                        src = src + sourcePlace.zipCode;
                    }
                }

                var dest = "";
                if (destinationPlace != undefined && destinationPlace != null) {
                    if (destinationPlace.addressLine1 != undefined && destinationPlace.addressLine1 != null && destinationPlace.addressLine1.length > 0) {
                        dest = destinationPlace.addressLine1;
                    }
                    if (destinationPlace.addressLine2 != undefined && destinationPlace.addressLine2 != null && destinationPlace.addressLine2.length > 0) {
                        if (dest.length > 0) {
                            dest = dest + ", ";
                        }
                        dest = dest + destinationPlace.addressLine2;
                    }
                    if (destinationPlace.addressLine3 != undefined && destinationPlace.addressLine3 != null && destinationPlace.addressLine3.length > 0) {
                        if (dest.length > 0) {
                            dest = dest + ", ";
                        }
                        dest = dest + destinationPlace.addressLine1;
                    }
                    if (destinationPlace.city != undefined && destinationPlace.city != null && destinationPlace.city.cityName != undefined &&
                        destinationPlace.city.cityName != null && destinationPlace.city.cityName.length > 0) {
                        if (dest.length > 0) {
                            dest = dest + ", ";
                        }
                        dest = dest + destinationPlace.city.cityName;
                    }
                    if (destinationPlace.zipCode != undefined && destinationPlace.zipCode != null && destinationPlace.zipCode.length > 0) {
                        if (dest.length > 0) {
                            dest = dest + ", ";
                        }
                        dest = dest + destinationPlace.zipCode;
                    }
                }
                $scope.sourceMapPlace = src;
                $scope.destinationMapPlace = dest;

                $timeout(function() {
                    $window.dispatchEvent(new Event("resize"));
                }, 100);
            }


            $scope.partyMasterActionResponseFn = function() {
                if ($scope.shipment != null && $stateParams.forObj != undefined && $stateParams.forObj != null) {
                    var isNaviPartyMasterEmpty = true;
                    var stateServiceIndex = 0;
                    var stateDocumentIndex = 0;

                    if ($stateParams.serIndx != undefined && $stateParams.serIndx != null) {
                        $scope.selectedShipmentServiceIndex = stateServiceIndex = parseInt($stateParams.serIndx);
                    }
                    $scope.shipmentTab = $stateParams.selectedTab;
                    console.log("Service Index", stateServiceIndex, " : DOC Index -- ", stateDocumentIndex);
                    if ($rootScope.naviPartyMaster != undefined && $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != null) {
                        isNaviPartyMasterEmpty = false;
                    }
                    if ($stateParams.forObj == "shipmentServiceDetail.party" && stateServiceIndex > -1) {
                        if ($scope.shipment.shipmentServiceList != undefined && $scope.shipment.shipmentServiceList != null &&
                            $scope.shipment.shipmentServiceList.length > 0) {
                            if (isNaviPartyMasterEmpty) {
                                //$scope.shipment.shipmentServiceList[stateServiceIndex].party = {};
                            } else {
                                $scope.shipment.shipmentServiceList[stateServiceIndex].party = $rootScope.naviPartyMaster;
                            }
                        }
                        $scope.activeViewTabAndService(stateServiceIndex, "general");
                        $scope.focusFieldAfter2Sec('partyService' + stateServiceIndex);
                    } else if ($stateParams.forObj == "shipment.party") {
                        if (isNaviPartyMasterEmpty) {
                            // $scope.shipment.party = {};
                        } else {
                            $scope.shipment.party = $rootScope.naviPartyMaster;
                            $scope.getShipmentPartyAddress($scope.shipment.party);
                        }
                        //				$scope.focusFieldAfter2Sec('shipmentParty');
                    } else if ($stateParams.forObj == "documentDetail.shipper") {

                        console.log("$scope.shipment.shipmentServiceList ::: ", $scope.shipment.shipmentServiceList);
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].shipper = {};
                        } else {
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].shipper = $rootScope.naviPartyMaster;
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "shipper");
                        }
                        $scope.setActiveTabAndFocusAfter2Sec('shipment.serviceDetail.' + stateServiceIndex + '.document.shipper.' + stateDocumentIndex, $stateParams.selectedTab);
                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $scope.focusFieldAfter2Sec('docShipper' + stateServiceIndex + "_" + stateDocumentIndex);
                    } else if ($stateParams.forObj == "documentDetail.forwarder") {
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].forwarder = {};
                        } else {
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].forwarder = $rootScope.naviPartyMaster;
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "forwarder");
                        }

                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $rootScope.navigateToNextField('docForwarder' + stateServiceIndex + "_" + stateDocumentIndex);
                    } else if ($stateParams.forObj == "documentDetail.consignee") {
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].consignee = {};
                        } else {
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].consignee = $rootScope.naviPartyMaster;
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "consignee");
                        }

                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $scope.focusFieldAfter2Sec('docConsignee' + stateServiceIndex + "_" + stateDocumentIndex);
                    } else if ($stateParams.forObj == "documentDetail.firstNotify") {
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].firstNotify = {};
                        } else {
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].firstNotify = $rootScope.naviPartyMaster;
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "firstNotify");
                        }

                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $scope.focusFieldAfter2Sec('docFirstNotify' + stateServiceIndex + "_" + stateDocumentIndex);
                    } else if ($stateParams.forObj == "documentDetail.secondNotify") {
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].secondNotify = {};
                        } else {
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "secondNotify");
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].secondNotify = $rootScope.naviPartyMaster;
                        }

                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $scope.focusFieldAfter2Sec('docSecondNotify' + stateServiceIndex + "_" + stateDocumentIndex);
                    } else if ($stateParams.forObj == "documentDetail.agent") {
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].agent = {};
                        } else {
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].agent = $rootScope.naviPartyMaster;
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "agent");
                        }

                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $scope.focusFieldAfter2Sec('docAgent' + stateServiceIndex + "_" + stateDocumentIndex);
                    } else if ($stateParams.forObj == "documentDetail.issuingAgent") {
                        if (isNaviPartyMasterEmpty) {
                            //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].issuingAgent = {};
                        } else {
                            $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].issuingAgent = $rootScope.naviPartyMaster;
                            $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "issuingAgent");
                        }
                        //	    	            $scope.activeViewTabAndService(stateServiceIndex, "documents");
                        //	    	            $scope.focusFieldAfter2Sec('docIssueAgent' + stateServiceIndex + "_" + stateDocumentIndex);
                    }


                }
            }


            $scope.checkforTsa = function(shipper, index) {
                if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.shipmentServiceList[index] != undefined &&
                    $scope.shipment.shipmentServiceList[index].serviceMaster.importExport == 'Export') {
                    if (ValidateUtil.isTSAExpired(shipper, $rootScope.partyAddressType)) {
                        Notification.warning($rootScope.nls["ERR5131"]);
                    }
                }
            }


            $scope.choosePartyAddress = function(documentDetail, addressKey, pIndex, index) {

                if (addressKey == 'shipper') {
                    if (documentDetail.shipperAddress == undefined || documentDetail.shipperAddress == null) {
                        documentDetail.shipperAddress = {};
                    }
                    if (documentDetail.shipper == undefined || documentDetail.shipper == null) {
                        documentDetail.shipperAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.shipper, documentDetail.shipperAddress);
                    $scope.checkforTsa(documentDetail.shipper, index);

                } else if (addressKey == 'forwarder') {
                    if (documentDetail.forwarderAddress == undefined || documentDetail.forwarderAddress == null) {
                        documentDetail.forwarderAddress = {};
                    }

                    if (documentDetail.forwarder == undefined || documentDetail.forwarder == null) {
                        documentDetail.forwarderAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.forwarder, documentDetail.forwarderAddress);
                } else if (addressKey == 'consignee') {
                    if (documentDetail.consigneeAddress == undefined || documentDetail.consigneeAddress == null) {
                        documentDetail.consigneeAddress = {};
                    }

                    if (documentDetail.consignee == undefined || documentDetail.consignee == null) {
                        documentDetail.consigneeAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.consignee, documentDetail.consigneeAddress);
                } else if (addressKey == 'firstNotify') {
                    if (documentDetail.firstNotifyAddress == undefined || documentDetail.firstNotifyAddress == null) {
                        documentDetail.firstNotifyAddress = {};
                    }

                    if (documentDetail.firstNotify == undefined || documentDetail.firstNotify == null) {
                        documentDetail.firstNotifyAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.firstNotify, documentDetail.firstNotifyAddress);
                } else if (addressKey == 'secondNotify') {
                    if (documentDetail.secondNotifyAddress == undefined || documentDetail.secondNotifyAddress == null) {
                        documentDetail.secondNotifyAddress = {};
                    }

                    if (documentDetail.secondNotify == undefined || documentDetail.secondNotify == null) {
                        documentDetail.secondNotifyAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.secondNotify, documentDetail.secondNotifyAddress);
                } else if (addressKey == 'agent') {
                    if (documentDetail.agentAddress == undefined || documentDetail.agentAddress == null) {
                        documentDetail.agentAddress = {};
                    }

                    if (documentDetail.agent == undefined || documentDetail.agent == null) {
                        documentDetail.agentAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.agent, documentDetail.agentAddress);
                } else if (addressKey == 'issuingAgent') {
                    if (documentDetail.issuingAgentAddress == undefined || documentDetail.issuingAgentAddress == null) {
                        documentDetail.issuingAgentAddress = {};
                    }
                    if (documentDetail.issuingAgent == undefined || documentDetail.issuingAgent == null) {
                        documentDetail.issuingAgentAddress = {};
                    }
                    $scope.getPartyAddress(documentDetail.issuingAgent, documentDetail.issuingAgentAddress);
                }


            }



            $scope.getPartyAddress = function(obj, mapToAddrss) {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss, obj, obj.partyAddressList[i]);
                        }
                    }
                }

            }
            $scope.focusFieldAfter2Sec = function(fieldName) {

                $timeout(function() {
                    $rootScope.navigateToNextField(fieldName);
                }, 3000);
            }



            $scope.setActiveTabAndFocusAfter2Sec = function(fieldName, selectedTab) {

                $timeout(function() {
                    $rootScope.navigateToNextField(fieldName);

                    $scope.shipmentTab = $stateParams.selectedTab;
                }, 3000);
            }































            $scope.partyChargedOption = function(service) {
                if (angular.uppercase($rootScope.appMasterData['quotation.to.shipment.change.party']) == 'TRUE') {
                    if (service.quotationUid != null) {
                        return true;
                    }
                }

                return false;
            }

            /* Others Tab Start */

            var authenticatedMoreInfoModal;

            $scope.authenticatedDocumentMorePopUp = function(authenticatedDoc, parent, child) {
                var newScope = $scope.$new();
                newScope.authenticatedDoc = authenticatedDoc;
                newScope.parent = parent;
                newScope.child = child;
                authenticatedMoreInfoModal = $modal({ scope: newScope, backDrop: 'static', templateUrl: 'app/components/crm/new-shipment/dialog/auth_doc_edit_moreinfo_popup.html', show: false });
                authenticatedMoreInfoModal.$promise.then(authenticatedMoreInfoModal.show);


            };


            /* Authenticated Documents */


            $scope.addNewAuthDocs = function(shipmentService) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_DOCUMENT_CREATE)) {
                    if (shipmentService.authenticatedDocList == undefined || shipmentService.authenticatedDocList == null || shipmentService.authenticatedDocList.length == 0) {
                        shipmentService.authenticatedDocList = [];
                        shipmentService.authenticatedDocList.push({});
                    }


                    var fineArr = [];
                    $scope.secondArr = [];
                    angular.forEach(shipmentService.authenticatedDocList, function(dataObj, index) {
                        $scope.secondArr[index] = {};
                        $scope.secondArr[index] = uiShipmentValidationService.validateAuthenticatedDocs(dataObj, index);
                        if ($scope.secondArr[index].errorFlag) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            fineArr.push(0);
                            $scope.secondArr[index].errRow = true;
                        }
                    });


                    if (fineArr.indexOf(0) < 0) {
                        shipmentService.authenticatedDocList.push({});
                        $scope.secondArr.push({ "errRow": false });
                    }


                }
            };



            $scope.deleteAuthenticatedDoc = function(shipmentServiceDetail, index) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_DOCUMENT_DELETE)) {
                    shipmentServiceDetail.authenticatedDocList.splice(index, 1);
                }
            }

            $scope.selectedAuthDocsShipperEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
                console.log("selectedAuthDocsShipperEvent")
                if ($scope.validateAuthDocsMore(1, authenticatedDoc, pIndex, index)) {
                    uiShipmentDataService.getPrimaryAddress(authenticatedDoc, 'shipper');
                    $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
                }
            }

            $scope.selectedAuthConsigneEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
                console.log("selectedAuthConsigneEvent")
                if ($scope.validateAuthDocsMore(3, authenticatedDoc, pIndex, index)) {
                    uiShipmentDataService.getPrimaryAddress(authenticatedDoc, 'consignee');
                    $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
                }
            }

            $scope.selectedAuthFirstNotifyEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
                console.log("selectedAuthFirstNotifyEvent")
                if ($scope.validateAuthDocsMore(5, authenticatedDoc, pIndex, index)) {
                    uiShipmentDataService.getPrimaryAddress(authenticatedDoc, 'firstNotify');
                    $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
                }
            }

            $scope.selectedAuthForwarderEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
                console.log("selectedAuthForwarderEvent")
                if ($scope.validateAuthDocsMore(7, authenticatedDoc, pIndex, index)) {
                    uiShipmentDataService.getPrimaryAddress(authenticatedDoc, 'forwarder');
                    $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
                }
            }



            $scope.saveAuthenticatedMoreInfo = function(validateCode, authenticatedDoc, pIndex, index) {
                    if ($scope.validateAuthDocsMore(validateCode, authenticatedDoc, pIndex, index)) {
                        authenticatedMoreInfoModal.$promise.then(authenticatedMoreInfoModal.hide);
                    }
                }
                //validateAuthDocsMore
            $scope.validateAuthDocsMore = function(validateCode, authenticatedDoc, pIndex, index) {

                $scope.errorMap = new Map();

                if (validateCode == 0 || validateCode == 1) {
                    if (authenticatedDoc.shipper == undefined ||
                        authenticatedDoc.shipper == null ||
                        authenticatedDoc.shipper == "" ||
                        authenticatedDoc.shipper.id == undefined) {
                        // $scope.errorMap.put("authDocShipper" + pIndex + "_" + index, $rootScope.nls["ERR05516"]);
                        // $scope.shipmentTab = 'AuthenticatedDocs';
                        //return false;
                        authenticatedDoc.shipperAddress = null;
                    } else {

                        if (ValidateUtil.isStatusBlocked(authenticatedDoc.shipper.status)) {
                            $scope.errorMap.put("authDocShipper" + pIndex + "_" + index, $rootScope.nls["ERR05517"]);
                            authenticatedDoc.shipper = {};
                            authenticatedDoc.shipperAddress = {};
                            return false

                        }
                        if (ValidateUtil.isStatusHidden(authenticatedDoc.shipper.status)) {
                            $scope.errorMap.put("authDocShipper" + pIndex + "_" + index, $rootScope.nls["ERR05518"]);
                            authenticatedDoc.shipper = {};
                            authenticatedDoc.shipperAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false

                        }
                    }

                }

                if (validateCode == 0 || validateCode == 2) {
                    if (authenticatedDoc.shipper != null && authenticatedDoc.shipper != undefined && authenticatedDoc.shipper.id != null) {
                        if (authenticatedDoc.shipperAddress.addressLine1 == undefined || authenticatedDoc.shipperAddress.addressLine1 == null || authenticatedDoc.shipperAddress.addressLine1 == "") {
                            $scope.errorMap.put("authenticatedDoc.shipperAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05520"]);
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false;
                        }
                        /*if (authenticatedDoc.shipperAddress.addressLine2 == null || authenticatedDoc.shipperAddress.addressLine2 == "" || authenticatedDoc.shipperAddress.addressLine2 == undefined) {
                        	$scope.errorMap.put("authenticatedDoc.shipperAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05522"]);
                        	$scope.shipmentTab = 'AuthenticatedDocs';
                        	return false;
                        } */
                    }
                }
                if (validateCode == 0 || validateCode == 3) {

                    if (authenticatedDoc.consignee == undefined || authenticatedDoc.consignee == null || authenticatedDoc.consignee == "" || authenticatedDoc.consignee.id == undefined) {
                        // $scope.errorMap.put("authConsignee" + pIndex + "_" + index, $rootScope.nls["ERR05542"]);
                        // $scope.shipmentTab = 'AuthenticatedDocs';
                        // return false;
                    } else {
                        if (ValidateUtil.isStatusBlocked(authenticatedDoc.consignee.status)) {
                            $scope.errorMap.put("authConsignee" + pIndex + "_" + index, $rootScope.nls["ERR05543"]);
                            authenticatedDoc.consignee = {};
                            authenticatedDoc.consigneeAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false
                        }
                        if (ValidateUtil.isStatusHidden(authenticatedDoc.consignee.status)) {
                            $scope.errorMap.put("authConsignee" + pIndex + "_" + index, $rootScope.nls["ERR05544"]);
                            authenticatedDoc.consignee = {};
                            authenticatedDoc.consigneeAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false
                        }
                    }
                }

                if (validateCode == 0 || validateCode == 4) {
                    if (authenticatedDoc.consignee != null && authenticatedDoc.consignee != undefined && authenticatedDoc.consignee.id != null) {
                        if (authenticatedDoc.consigneeAddress.addressLine1 == undefined || authenticatedDoc.consigneeAddress.addressLine1 == null || authenticatedDoc.consigneeAddress.addressLine1 == "") {
                            $scope.errorMap.put("authenticatedDoc.consigneeAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05546"]);
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false;
                        }

                        /*if (authenticatedDoc.consigneeAddress.addressLine2 == null || authenticatedDoc.consigneeAddress.addressLine2 == "" || authenticatedDoc.consigneeAddress.addressLine2 == undefined) {
                        	$scope.errorMap.put("authenticatedDoc.consigneeAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05548"]);
                        	$scope.shipmentTab = 'AuthenticatedDocs';
                        	return false;
                        }*/
                    }
                }
                if (validateCode == 0 || validateCode == 5) {
                    if (authenticatedDoc.firstNotify == undefined ||
                        authenticatedDoc.firstNotify == null ||
                        authenticatedDoc.firstNotify == "" ||
                        authenticatedDoc.firstNotify.id == undefined) {
                        //$scope.errorMap.put("authFirstNotify"+pIndex+"_"+index,$rootScope.nls["ERR05555"]);
                        //$scope.shipmentTab='documents';
                        //return false;

                    } else {
                        if (ValidateUtil.isStatusBlocked(authenticatedDoc.firstNotify.status)) {
                            $scope.errorMap.put("authFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR05556"]);
                            authenticatedDoc.firstNotify = {};
                            authenticatedDoc.firstNotifyAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false
                        }
                        if (ValidateUtil.isStatusHidden(authenticatedDoc.firstNotify.status)) {
                            $scope.errorMap.put("authFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR05557"]);
                            authenticatedDoc.firstNotify = {};
                            authenticatedDoc.firstNotifyAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false
                        }
                    }

                }

                if (validateCode == 0 || validateCode == 6) {
                    if (authenticatedDoc.firstNotify != null && authenticatedDoc.firstNotify != undefined && authenticatedDoc.firstNotify.id != null) {
                        if (authenticatedDoc.firstNotifyAddress.addressLine1 == undefined || authenticatedDoc.firstNotifyAddress.addressLine1 == null || authenticatedDoc.firstNotifyAddress.addressLine1 == "") {
                            $scope.errorMap.put("authenticatedDoc.firstNotifyAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05559"]);
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false;
                        }

                        /* if (authenticatedDoc.firstNotifyAddress.addressLine2 == null || authenticatedDoc.firstNotifyAddress.addressLine2 == "" || authenticatedDoc.firstNotifyAddress.addressLine2 == undefined) {
                        	 $scope.errorMap.put("authenticatedDoc.firstNotifyAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05561"]);
                        	 $scope.shipmentTab = 'AuthenticatedDocs';
                        	 return false;
                         } */
                    }
                }

                //Forwarder
                if (validateCode == 0 || validateCode == 7) {


                    if (authenticatedDoc.forwarder == undefined || authenticatedDoc.forwarder == null || authenticatedDoc.forwarder == "" || authenticatedDoc.forwarder.id == undefined) {
                        // $scope.errorMap.put("authForwarder" + pIndex + "_" + index, $rootScope.nls["ERR05555"]);
                        // $scope.shipmentTab = 'AuthenticatedDocs';
                        //return false;
                    } else {
                        if (ValidateUtil.isStatusBlocked(authenticatedDoc.forwarder.status)) {
                            $scope.errorMap.put("authForwarder" + pIndex + "_" + index, $rootScope.nls["ERR05530"]);
                            authenticatedDoc.forwarder = {};
                            authenticatedDoc.forwarderAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false

                        }

                        if (ValidateUtil.isStatusHidden(authenticatedDoc.forwarder.status)) {
                            $scope.errorMap.put("authForwarder" + pIndex + "_" + index, $rootScope.nls["ERR05531"]);
                            authenticatedDoc.forwarder = null;
                            authenticatedDoc.forwarderAddress = {};
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false

                        }
                    }
                }


                if (validateCode == 0 || validateCode == 8) {

                    if (authenticatedDoc.forwarder != null && authenticatedDoc.forwarder != undefined && authenticatedDoc.forwarder.id != null) {

                        if (authenticatedDoc.forwarderAddress.addressLine1 == undefined || authenticatedDoc.forwarderAddress.addressLine1 == null ||
                            authenticatedDoc.forwarderAddress.addressLine1 == "") {
                            $scope.errorMap.put("authenticatedDoc.forwarderAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05533"]);
                            $scope.shipmentTab = 'AuthenticatedDocs';
                            return false;
                        }

                        /*if (authenticatedDoc.forwarderAddress.addressLine2 == undefined || authenticatedDoc.forwarderAddress.addressLine2 == null || authenticatedDoc.forwarderAddress.addressLine2 == "") {
                        	$scope.errorMap.put("authenticatedDoc.forwarderAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05535"]);
                        	$scope.shipmentTab = 'AuthenticatedDocs';
                        	return false;
                        }*/
                    }
                }
                return true;
            }


            $scope.closeAuthenticatedAddressBar = function() {
                $scope.showAuthenticatedPartyAddress = false;
                console.log("Closed...............................");
            }
            $scope.showPartyAddress = function(addressKey, documentDetail, pIndex, index) {
                $scope.panelAuthenticatedTitle = "Party Address";
                $scope.selectedItem = -1;
                $scope.errorMap = new Map();
                $scope.selectedDocumentIndex = index;
                $scope.showAuthenticatedPartyAddress = true;
                $scope.authenticatedPartyAddressList = [];
                $scope.partyAddressVal = addressKey;

                if ($scope.partyAddressVal == 'shipperAddress') {
                    for (var i = 0; i < documentDetail.shipper.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.shipper.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'forwarderAddress') {
                    for (var i = 0; i < documentDetail.forwarder.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.forwarder.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'consigneeAddress') {
                    for (var i = 0; i < documentDetail.consignee.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.consignee.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'firstNotifyAddress') {
                    for (var i = 0; i < documentDetail.firstNotify.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.firstNotify.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'secondNotifyAddress') {
                    for (var i = 0; i < documentDetail.secondNotify.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.secondNotify.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'agentAddress') {
                    for (var i = 0; i < documentDetail.agent.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.agent.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'issuingAgentAddress') {
                    for (var i = 0; i < documentDetail.issuingAgent.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.issuingAgent.partyAddressList[i]);
                    }
                } else if ($scope.partyAddressVal == 'chaAgentAddress') {
                    for (var i = 0; i < documentDetail.chaAgent.partyAddressList.length; i++) {
                        $scope.authenticatedPartyAddressList.push(documentDetail.chaAgent.partyAddressList[i]);
                    }
                }
            };




            $scope.selectedAuthDocsPartyAddress = function(obj, authenticatedDoc) {

                if ($scope.partyAddressVal == 'shipperAddress') {
                    authenticatedDoc.shipperAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.shipperAddress, authenticatedDoc.shipper, obj);
                }
                if ($scope.partyAddressVal == 'consigneeAddress') {
                    authenticatedDoc.consigneeAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.consigneeAddress, authenticatedDoc.consignee, obj);
                }
                if ($scope.partyAddressVal == 'firstNotifyAddress') {
                    authenticatedDoc.firstNotifyAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.firstNotifyAddress, authenticatedDoc.firstNotify, obj);
                }
                if ($scope.partyAddressVal == 'forwarderAddress') {
                    authenticatedDoc.forwarderAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.forwarderAddress, authenticatedDoc.forwarder, obj);
                }

                $scope.closeAuthenticatedAddressBar();
            };


            var myOtherModalAuthDoc = $modal({ scope: $scope, templateUrl: 'errorPopUpAuthDoc.html', show: false, keyboard: true });
            var errorOnRowIndex = null;
            $scope.errorShowAuthDocs = function(errorObj, index) {
                console.log("errorshow", errorObj, index);
                $scope.errList = errorObj.errTextArr;
                // Show when some event occurs (use $promise property to ensure the template has been loaded)
                errorOnRowIndex = index;
                myOtherModalAuthDoc.$promise.then(myOtherModalAuthDoc.show);
            };



            $scope.callValidateAuthenticatedDocs = function(shipmentService) {

                var isValid = true;

                if (shipmentService.authenticatedDocList == undefined || shipmentService.authenticatedDocList == null || shipmentService.authenticatedDocList.length == 0) {
                    return isValid;
                }

                $scope.secondArr = [];

                angular.forEach(shipmentService.authenticatedDocList, function(dataObj, index) {

                    $scope.secondArr[index] = uiShipmentValidationService.validateAuthenticatedDocs(dataObj, index);
                    if ($scope.secondArr[index].errorFlag) {
                        $scope.secondArr[index].errRow = false;
                    } else {
                        $scope.secondArr[index].errRow = true;
                        isValid = false;
                    }

                });

                return isValid;
            }


            /* Authenticated Documents Ends*/





            //pick up delivery started

            $scope.changePickUpToggle = function(serviceDetail) {
                $scope.errorMap = new Map();
                if (serviceDetail.pickUpDeliveryPoint.isOurPickUp == false || serviceDetail.pickUpDeliveryPoint.isOurPickUp == 'No') {
                    serviceDetail.pickUpDeliveryPoint = {};
                } else {
                    //Need to fetch from document shipper and forwarder if our pick up yes
                    if (serviceDetail.documentList.length == 0) {
                        console.log("pick up to document");
                        $scope.shipmentTab = 'documents';
                        $rootScope.clientMessage = $rootScope.nls["ERR90558"];
                        serviceDetail.pickUpDeliveryPoint.isOurPickUp = false;
                        return false;
                    } else {
                        if (serviceDetail != undefined && serviceDetail.documentList != undefined && serviceDetail.documentList != null) {
                            if (serviceDetail.documentList[0].forwarder != undefined) {
                                serviceDetail.pickUpDeliveryPoint.pickupPoint = serviceDetail.documentList[0].forwarder;
                                if (serviceDetail.documentList[0].forwarderAddress != undefined && serviceDetail.documentList[0].forwarderAddress != null) {
                                    var partyAddress = serviceDetail.documentList[0].forwarderAddress;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace = {};
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine1 = partyAddress.addressLine1;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine2 = partyAddress.addressLine2;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine3 = partyAddress.addressLine3;
                                    if (partyAddress.zipCode == null || undefined) {
                                        serviceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.poBox;
                                    } else {
                                        serviceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.zipCode;
                                    }
                                    serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = partyAddress.phone;
                                    serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = partyAddress.mobileNo;
                                    serviceDetail.pickUpDeliveryPoint.pickUpEmail = partyAddress.email;
                                    serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = partyAddress.contactPerson;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.city = partyAddress.cityMaster;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.state = partyAddress.stateMaster;
                                }
                            }
                            if (serviceDetail != undefined && serviceDetail.documentList[0].forwarder == undefined && serviceDetail.documentList[0].shipper != undefined) {
                                serviceDetail.pickUpDeliveryPoint.pickupPoint = serviceDetail.documentList[0].shipper;
                                if (serviceDetail.documentList[0].shipperAddress != undefined && serviceDetail.documentList[0].shipperAddress != null) {
                                    var partyAddress = serviceDetail.documentList[0].shipperAddress;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace = {};
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine1 = partyAddress.addressLine1;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine2 = partyAddress.addressLine2;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine3 = partyAddress.addressLine3;
                                    if (partyAddress.zipCode == null || undefined) {
                                        serviceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.poBox;
                                    } else {
                                        serviceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.zipCode;
                                    }
                                    serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = partyAddress.phone;
                                    serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = partyAddress.mobileNo;
                                    serviceDetail.pickUpDeliveryPoint.pickUpEmail = partyAddress.email;
                                    serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = partyAddress.contactPerson;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.city = partyAddress.cityMaster;
                                    serviceDetail.pickUpDeliveryPoint.pickUpPlace.state = partyAddress.stateMaster;
                                }
                            }
                        }
                    }
                }
            }

            $scope.clearPickUpPlace = function(pickUpDeliveryPoint) {
                if (pickUpDeliveryPoint.pickupPoint == null || pickUpDeliveryPoint.pickupPoint == undefined || pickUpDeliveryPoint.pickupPoint.id == null) {
                    pickUpDeliveryPoint.pickUpContactPerson = null;
                    pickUpDeliveryPoint.pickUpEmail = null;
                    pickUpDeliveryPoint.pickUpMobileNo = null;
                    pickUpDeliveryPoint.pickUpPhoneNo = null;
                    pickUpDeliveryPoint.pickUpPlace = null;
                    pickUpDeliveryPoint.pickupPointCode = null;
                }
            }

            $scope.clearDeliveryPlace = function(pickUpDeliveryPoint) {
                if (pickUpDeliveryPoint.deliveryPoint == null || pickUpDeliveryPoint.deliveryPoint == undefined || pickUpDeliveryPoint.deliveryPoint.id == null) {
                    pickUpDeliveryPoint.deliveryContactPerson = null;
                    pickUpDeliveryPoint.deliveryEmail = null;
                    pickUpDeliveryPoint.deliveryMobileNo = null;
                    pickUpDeliveryPoint.deliveryPhoneNo = null;
                    pickUpDeliveryPoint.deliveryPlace = null;
                    pickUpDeliveryPoint.deliveryPointCode = null;
                }
            }

            $scope.clearDoorDeliveryPlace = function(pickUpDeliveryPoint) {
                if (pickUpDeliveryPoint.doorDeliveryPoint == null || pickUpDeliveryPoint.doorDeliveryPoint == undefined || pickUpDeliveryPoint.doorDeliveryPoint.id == null) {
                    pickUpDeliveryPoint.doorDeliveryContactPerson = null;
                    pickUpDeliveryPoint.doorDeliveryEmail = null;
                    pickUpDeliveryPoint.doorDeliveryMobileNo = null;
                    pickUpDeliveryPoint.doorDeliveryPhoneNo = null;
                    pickUpDeliveryPoint.doorDeliveryPlace = null;
                    pickUpDeliveryPoint.doorDeliveryPointCode = null;
                }
            }

            $scope.onTransporterBlur = function(shipmentServiceDetail) {
                $timeout(function() {
                    if (shipmentServiceDetail.pickUpDeliveryPoint.transporter != undefined && shipmentServiceDetail.pickUpDeliveryPoint.transporter != null &&
                        shipmentServiceDetail.pickUpDeliveryPoint.transporter.id == undefined) {
                        $scope.enqC = "";
                        if (shipmentServiceDetail.pickUpDeliveryPoint.transporter instanceof Object && shipmentServiceDetail.pickUpDeliveryPoint.transporter.partyName != null && shipmentServiceDetail.pickUpDeliveryPoint.transporter.partyName.trim().length > 0) {
                            $scope.enqC = shipmentServiceDetail.pickUpDeliveryPoint.transporter.partyName;
                        } else if (typeof shipmentServiceDetail.pickUpDeliveryPoint.transporter === 'string') {
                            $scope.enqC = shipmentServiceDetail.pickUpDeliveryPoint.transporter;
                        }
                        shipmentServiceDetail.pickUpDeliveryPoint.transporter = { partyName: $scope.enqC };
                        shipmentServiceDetail.pickUpDeliveryPoint.transporterName = $scope.enqC;
                    } else {
                        shipmentServiceDetail.pickUpDeliveryPoint.transporterName = null;
                    }
                }, 300);
            }

            $scope.selectedShipmentTransporter = function(serviceDetail, index, nextField) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 1, index), nextField)) {
                    for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList.length; i++) {
                        if (serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList[i].addressType == 'Primary') {
                            if (serviceDetail.pickUpDeliveryPoint.transporterAddress == undefined) {
                                serviceDetail.pickUpDeliveryPoint.transporterAddress = {};
                            }
                            serviceDetail.pickUpDeliveryPoint.transporterAddress = addressJoiner.joinAddressLineByLine({}, serviceDetail.pickUpDeliveryPoint.transporter, serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList[i]).fullAddress;
                            break;
                        }
                    }
                }
            };

            $scope.selectedShipmentPickUpPoint = function(serviceDetail, index, nextField) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 2, index), nextField)) {
                    var tmpPartyAddress = null;

                    for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList.length; i++) {
                        if (serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i].addressType == 'Warehouse') {
                            tmpPartyAddress = serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i];
                            break;
                        } else if (serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i].addressType == 'Primary') {
                            tmpPartyAddress = serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i];
                        }
                    }
                    $scope.addressPickUpMapping(serviceDetail, tmpPartyAddress, serviceDetail.pickUpDeliveryPoint.pickupPoint);

                }

            };


            $scope.selectedPickUpFrom = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 3, index), nextFocus)) {

                }

            };


            $scope.selectedShipmentDeliveryPoint = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 9, index), nextFocus)) {
                    var tmpPartyAddress = serviceDetail.pickUpDeliveryPoint.deliveryPoint.addressMaster;
                    $scope.addressDeliveryMapping(serviceDetail, tmpPartyAddress, serviceDetail.pickUpDeliveryPoint.deliveryPoint);
                }
            }

            $scope.selectedShipmentDoorDeliveryPoint = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 16, index), nextFocus)) {
                    var tmpPartyAddress = serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint.addressMaster;
                    $scope.addressDoorDeliveryMapping(serviceDetail, tmpPartyAddress, serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint);
                }
            }

            $scope.addressPickUpMapping = function(serviceDetail, partyAddress, partyMaster) {
                serviceDetail.pickUpDeliveryPoint.pickUpPlace = {};
                serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine1 = partyAddress.addressLine1;
                serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine2 = partyAddress.addressLine2;
                serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine3 = partyAddress.addressLine3;
                serviceDetail.pickUpDeliveryPoint.pickUpPlace.state = partyAddress.stateMaster;
                serviceDetail.pickUpDeliveryPoint.pickUpPlace.city = partyAddress.cityMaster;
                serviceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.zipCode;
                serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = partyAddress.phone;
                serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = partyAddress.mobileNo;
                serviceDetail.pickUpDeliveryPoint.pickUpEmail = partyAddress.email;
                serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = partyAddress.contactPerson;

            }


            $scope.addressDeliveryMapping = function(serviceDetail, address, cfsMaster) {
                serviceDetail.pickUpDeliveryPoint.deliveryPlace = {};
                serviceDetail.pickUpDeliveryPoint.deliveryPlace.addressLine1 = address.addressLine1;
                serviceDetail.pickUpDeliveryPoint.deliveryPlace.addressLine2 = address.addressLine2;
                serviceDetail.pickUpDeliveryPoint.deliveryPlace.addressLine3 = address.addressLine3;
                serviceDetail.pickUpDeliveryPoint.deliveryPlace.state = address.state;
                serviceDetail.pickUpDeliveryPoint.deliveryPlace.city = address.city;
                serviceDetail.pickUpDeliveryPoint.deliveryPlace.zipCode = address.zipCode;
                serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo = address.phone;
                serviceDetail.pickUpDeliveryPoint.deliveryMobileNo = address.mobile;
                serviceDetail.pickUpDeliveryPoint.deliveryEmail = address.email;
                serviceDetail.pickUpDeliveryPoint.deliveryContactPerson = address.contact;

            }

            $scope.addressDoorDeliveryMapping = function(serviceDetail, address, portMaster) {
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace = {};
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.addressLine1 = address.addressLine1;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.addressLine2 = address.addressLine2;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.addressLine3 = address.addressLine3;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.state = address.stateMaster;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.city = address.cityMaster;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.zipCode = address.zipCode;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryPhoneNo = address.phone;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryMobileNo = address.mobileNo;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryEmail = address.email;
                serviceDetail.pickUpDeliveryPoint.doorDeliveryContactPerson = address.contact;

            }
            $scope.showTransportAddressList = false;
            $scope.transportAddressListConfig = {
                search: false,
                address: true
            }
            $scope.showTransportPartyAddress = function(serviceDetail) {
                $scope.panelTitle = "Transporter Address";
                $scope.showTransportAddressList = true;
                $scope.selectedItem = -1;
                $scope.transportAddressListShipment = [];
                if (serviceDetail.pickUpDeliveryPoint.transporter != null && serviceDetail.pickUpDeliveryPoint.transporter.id != null) {
                    for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList.length; i++) {
                        $scope.transportAddressListShipment.push(serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList[i]);
                    }
                }


            };



            $scope.selectedTransportPartyAddress = function(obj) {
                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].pickUpDeliveryPoint.transporterAddress = addressJoiner.joinAddressLineByLine({}, $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].pickUpDeliveryPoint.transporter, obj).fullAddress;
                $scope.cancelTransportPartyAddress();
                $rootScope.navigateToNextField('ourPickUp' + $scope.selectedShipmentServiceIndex);

            };

            $scope.cancelTransportPartyAddress = function() {
                $scope.showTransportAddressList = false;
                $rootScope.navigateToNextField('transporterAddress' + $scope.selectedShipmentServiceIndex);
            }


            $scope.showPickUpPlaceAddressList = false;
            $scope.pickUpPlaceAddressListConfig = {
                search: false,
                address: true
            }

            $scope.showPickUpPlacePartyAddress = function(serviceDetail) {
                $scope.panelTitle = "Pickup Place Address";
                $scope.showPickUpPlaceAddressList = true;
                $scope.selectedItem = -1;
                $scope.pickUpPlaceAddressListShipment = [];
                if (serviceDetail.pickUpDeliveryPoint.pickupPoint != null && serviceDetail.pickUpDeliveryPoint.pickupPoint.id != null) {
                    for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList.length; i++) {
                        $scope.pickUpPlaceAddressListShipment.push(serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i]);
                    }
                }


            };
            /*   $scope.routingAndRoutingStatus = function(serviceDetail) {
            	   console.log("ee called..");
            	   if((serviceDetail.carrier != null && serviceDetail.carrier != undefined && serviceDetail.carrier != "") 
            			   || (serviceDetail.routeNo != null && serviceDetail.routeNo != undefined && serviceDetail.routeNo != "") 
            			   || (serviceDetail.mawbNo != null && serviceDetail.mawbNo != undefined && serviceDetail.mawbNo != "")
            			   || (serviceDetail.etd != null && serviceDetail.etd != undefined && serviceDetail.etd != "")
            			   || (serviceDetail.eta != null && serviceDetail.eta != undefined && serviceDetail.eta != "")) {
            		   return true;
            	   }
            	   return false;
               }*/


            $scope.packAndDimensionStatus = function(serviceDetail) {

            }
            $scope.hawbCustomerStatus = function(serviceDetail) {

            }
            $scope.hawbOtherInfoStatus = function(serviceDetail) {

            }
            $scope.pickupStatus = function(serviceDetail) {

            }
            $scope.deliveryToCfsStatus = function(serviceDetail) {

            }

            $scope.doorDeliveryStatus = function(serviceDetail) {

            }

            $scope.selectedPickUpPlacePartyAddress = function(obj) {

                $scope.addressPickUpMapping($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex], obj, $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].pickUpDeliveryPoint.pickupPoint);
                $scope.cancelPickUpPlacePartyAddress();
                $rootScope.navigateToNextField('pickUpContactPerson' + $scope.selectedShipmentServiceIndex);

            };


            $scope.cancelPickUpPlacePartyAddress = function() {
                $scope.showPickUpPlaceAddressList = false;
                $rootScope.navigateToNextField('PickupPlace' + $scope.selectedShipmentServiceIndex);
            }

            $scope.validatePickUpDelivery = function(pickUpDeliveryPoint, code, index, nextFocus) {
                return $scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(pickUpDeliveryPoint, code, index), nextFocus);
            }

            $scope.selectedPickUpState = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 4, index), nextFocus)) {}
            }

            $scope.selectedDeliveryFrom = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 10, index), nextFocus)) {}
            };

            $scope.selectedDeliveryState = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 11, index), nextFocus)) {}
            }

            $scope.selectedDoorDeliveryState = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 18, index), nextFocus)) {}
            }

            $scope.selectedPickUpCity = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 4, index), nextFocus)) {}
            }
            $scope.selectedDeliveryCity = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 11, index), nextFocus)) {}
            }
            $scope.selectedDoorDeliveryCity = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 18, index), nextFocus)) {}
            }

            $scope.selectedDoorDeliveryPoint = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 16, index), nextFocus)) {}
            };

            $scope.selectedDoorDeliveryFrom = function(serviceDetail, index, nextFocus) {
                if (!$scope.focusAndMessageShower(uiShipmentValidationService.validatePickUpDelivery(serviceDetail.pickUpDeliveryPoint, 17, index), nextFocus)) {}
            };


            //pick up delivery ends
            var notesModel = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_notes_popup.html',
                show: false
            });


            $scope.showNotesPopup = function() {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_NOTES_MODIFY) && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_NOTES_VIEW)) {
                    $scope.object = {};
                    $scope.object.notes = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].notes;
                    $scope.object.internalNote = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].internalNote;
                    $rootScope.navigateToNextField('note');
                    notesModel.$promise.then(notesModel.show);
                }
            };

            $scope.saveNote = function(object) {
                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].notes = object.notes;
                $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].internalNote = object.internalNote;
            }


            //sign unoff popup
            var signUnoffModal;
            $scope.showUnSignOffPopup = function() {
                $scope.shipmentServiceDetail = {};
                $scope.shipmentServiceDetail.shipmentAttachmentList = [{}];

                signUnoffModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/crm/new-shipment/dialog/shipment_signunoff_popup.html',

                    show: false
                });
                $scope.navigateToNextField("flightSchedule.service");
                signUnoffModal.$promise.then(signUnoffModal.show)
            };
            //sign off popup

            var signOffModel;
            //Sign Off Modal
            $scope.serviceSignOffModal = function(serviceObject, index) {
                $scope.serviceObject = serviceObject;
                $scope.shipmentServiceSignOffIndex = index;
                if ($scope.serviceObject.shipmentServiceSignOff == undefined || $scope.serviceObject.shipmentServiceSignOff == null) {
                    $scope.serviceObject.shipmentServiceSignOff = {};
                    $scope.serviceObject.shipmentServiceSignOff.signOffBy = $rootScope.userProfile.employee;
                    $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
                    $scope.serviceObject.shipmentServiceSignOff.shipmentUid = serviceObject.shipmentUid;
                    $scope.serviceObject.shipmentServiceSignOff.serviceUid = serviceObject.serviceUid;
                    $scope.serviceObject.shipmentServiceSignOff.consolUid = serviceObject.consolUid;
                    $scope.serviceObject.shipmentServiceSignOff.signOffUnsignOff = "SignOff";
                    $scope.serviceObject.shipmentServiceSignOff.shipmentServiceDetail = serviceObject.id;


                } else {
                    $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString($scope.serviceObject.shipmentServiceSignOff.signOffDate);
                }
                signOffModel = $modal({ scope: $scope, templateUrl: 'app/components/crm/shipment/views/service_signoff_popup.html', show: false });
                signOffModel.$promise.then(signOffModel.show);
            };


            var signOffModel;
            //Sign Off Modal
            $scope.serviceSignOffModal = function(serviceObject, index) {
                $scope.serviceObject = serviceObject;
                $scope.shipmentServiceSignOffIndex = index;
                if ($scope.serviceObject.shipmentServiceSignOff == undefined || $scope.serviceObject.shipmentServiceSignOff == null) {
                    $scope.serviceObject.shipmentServiceSignOff = {};
                    $scope.serviceObject.shipmentServiceSignOff.signOffBy = $rootScope.userProfile.employee;
                    $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
                    $scope.serviceObject.shipmentServiceSignOff.shipmentUid = serviceObject.shipmentUid;
                    $scope.serviceObject.shipmentServiceSignOff.serviceUid = serviceObject.serviceUid;
                    $scope.serviceObject.shipmentServiceSignOff.consolUid = serviceObject.consolUid;
                    $scope.serviceObject.shipmentServiceSignOff.signOffUnsignOff = "SignOff";
                    $scope.serviceObject.shipmentServiceSignOff.shipmentServiceDetail = serviceObject.id;


                } else {
                    $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateToString($scope.serviceObject.shipmentServiceSignOff.signOffDate);
                }
                signOffModel = $modal({ scope: $scope, templateUrl: 'app/components/crm/shipment/views/service_signoff_popup.html', show: false });
                signOffModel.$promise.then(signOffModel.show);
            };


            /*Salesman Select Picker*/
            $scope.ajaxEmployeeEvent = function(object) {
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                return EmployeeList.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.customerServiceList = data.responseObject.searchResult;
                            console.log("$scope.customerServiceList", $scope.customerServiceList);
                            return $scope.customerServiceList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Customer Services');
                    }
                );

            }
            $scope.ajaxCommentEvent = function(object) {
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                return commentMasterFactory.keyword.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.commentList = data.responseObject.searchResult;
                            console.log("$scope.commentList", $scope.commentList);
                            return $scope.commentList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Customer Services');
                    }
                );

            }

            $scope.commentRender = function(item) {
                return {
                    label: item.commentCode,
                    item: item
                }
            }

            $scope.selectedCommentMaster = function() {
                $scope.serviceObject.shipmentServiceSignOff.description = $scope.serviceObject.shipmentServiceSignOff.commentMaster.description;
            }

            $scope.saveServiceSignOff = function(signOffObject) {
                console.log("Sign Off Object", signOffObject);
                // close the model
                signOffModel.$promise.then(signOffModel.hide);

                signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
                if (signOffObjectCopy.signOffDate != null)
                    signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);

                signOffObjectCopy.signOffType = signOffObjectCopy.signOffType ? 'Yes' : 'No';

                console.log("signOffObjectCopy ", signOffObjectCopy);

                appMetaFactory.shipmentService.signOff(signOffObjectCopy).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("Sign off Successful", data.responseObject)

                        $scope.shipment.shipmentServiceList[$scope.shipmentServiceSignOffIndex].shipmentServiceSignOff = data.responseObject;
                    } else {
                        console.lolg("Sign off Failed", data.responseDescription);
                    }
                }, function(error) {
                    console.lolg("Sign off Failed", error);
                });
            }


            /* Copy Shipment Code */


            var myCopyShipmentPopupModel = null;
            $scope.getShipmentsPopup = function() {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_COPY_SHIPMENT_CREATE)) {
                    console.log("Shipment Popup..........................");

                    $scope.copyShipment = {};
                    $scope.copyShipmentSearchResult = [];
                    $scope.selectedCopyShipmentData = null;
                    $scope.copyShipmentLimitArray = [5, 10, 15, 20];
                    $scope.copyShipmentPage = 0;
                    $scope.copyShipmentLimit = 5;
                    $scope.copyShipmentTotalRecord = 0;
                    myCopyShipmentPopupModel = $modal({ scope: $scope, backdrop: 'static', show: false, templateUrl: 'app/components/crm/new-shipment/dialog/copy_shipment_popup.html' });
                    $scope.navigateToNextField("copy_shipment_service");
                    myCopyShipmentPopupModel.$promise.then(myCopyShipmentPopupModel.show);

                }
            }

            $scope.validateCopyShipmentSearch = function(validateCode) {
                $scope.errorMap = new Map();
                if (validateCode == 1 || validateCode == 0) {

                    if ($scope.copyShipment.service == undefined || $scope.copyShipment.service == null || $scope.copyShipment.service.id == null) {
                        $scope.errorMap.put("copyShipment.service", $rootScope.nls["ERR90599-1"]);
                        return false;
                    } else {
                        if (ValidateUtil.isStatusBlocked($scope.copyShipment.service.status)) {
                            $scope.errorMap.put("copyShipment.service", $rootScope.nls["ERR90599-2"]);
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.copyShipment.service.status)) {
                            $scope.errorMap.put("copyShipment.service", $rootScope.nls["ERR90599-3"]);
                            return false;
                        }
                    }
                }


                if (validateCode == 2 || validateCode == 0) {
                    if ($scope.copyShipment.party == undefined || $scope.copyShipment.party == null || $scope.copyShipment.party.id == null) {
                        $scope.errorMap.put("copyShipment.party", $rootScope.nls["ERR90599-4"]);
                        return false;
                    } else {
                        if (ValidateUtil.isStatusBlocked($scope.copyShipment.party.status)) {
                            $scope.errorMap.put("copyShipment.party", $rootScope.nls["ERR90599-5"]);
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.copyShipment.service.status)) {
                            $scope.errorMap.put("copyShipment.party", $rootScope.nls["ERR90599-6"]);
                            return false;
                        }
                    }
                }

                return true;
            }

            $scope.copyShipmentSearch_selectedService = function(nextFieldId) {
                if ($scope.validateCopyShipmentSearch(1)) {
                    if (nextFieldId != undefined && nextFieldId != null) {
                        $scope.navigateToNextField(nextFieldId);
                    }
                }
            }

            $scope.copyShipmentSearch_selectedParty = function(nextFieldId) {
                if ($scope.validateCopyShipmentSearch(2)) {
                    if (nextFieldId != undefined && nextFieldId != null) {
                        $scope.navigateToNextField(nextFieldId);
                    }
                }
            }

            $scope.copyShipment_search = function() {
                console.log("Search is called.");

                if ($scope.validateCopyShipmentSearch(0)) {

                    $scope.spinner = true;
                    $scope.copyShipment.selectedPageNumber = $scope.copyShipmentPage;
                    $scope.copyShipment.recordPerPage = $scope.copyShipmentLimit;

                    $scope.copyShipment.shipmentDate = {};

                    if ($scope.copyShipment.shipmentDateTemp != null && $scope.copyShipment.shipmentDateTemp != "") {
                        $scope.copyShipment.shipmentDate.startDate = $rootScope.sendApiStartDateTime($scope.copyShipment.shipmentDateTemp);
                        $scope.copyShipment.shipmentDate.endDate = $rootScope.sendApiEndDateTime($scope.copyShipment.shipmentDateTemp);
                    }

                    $scope.copyShipment.serviceId = $scope.copyShipment.service.id;
                    $scope.copyShipment.partyId = $scope.copyShipment.party.id;


                    console.log("$scope.copyShipment ", $scope.copyShipment);
                    CopyShipmentSearch.query($scope.copyShipment).$promise.then(function(data, status) {

                        $scope.copyShipmentTotalRecord = data.responseObject.totalRecord;
                        console.log("$scope.copyShipmentTotalRecord ", $scope.copyShipmentTotalRecord);
                        $scope.copyShipmentSearchResult = data.responseObject.searchResult;
                        $scope.spinner = false;
                    });

                }
            }

            $scope.copyShipmentChangePage = function(param) {
                console.log("copyShipmentChangePage ", param);
                $scope.copyShipmentPage = param.page;
                $scope.copyShipmentLimit = param.size;
                $scope.copyShipment_search();
            }

            $scope.copyShipmentLimitChange = function(item) {
                console.log("copyShipmentLimitChange ", item);
                $scope.copyShipmentPage = 0;
                $scope.copyShipmentLimit = item;
                $scope.copyShipment_search();
            }

            $scope.shipmentCopy = function(documentId, resultObject) {

                if (!$scope.validateCopyShipmentSearch(0)) {
                    return;
                }

                if (documentId == undefined || documentId == null || resultObject == undefined || resultObject == null || resultObject.legnth == 0) {
                    console.log("Not Selected any shipment");
                    Notification.error($rootScope.nls["ERR90642"]);
                    return;
                } else {
                    myCopyShipmentPopupModel.$promise.then(myCopyShipmentPopupModel.hide);
                    $scope.spinner = true;
                }

                var data = null;
                for (var i = 0; i < resultObject.length; i++) {
                    if (documentId == resultObject[i].id) {
                        data = resultObject[i];
                        break;
                    }
                }
                if (!data) return false;
                var copyShipmentObject = {
                    documentId: data.id,
                    serviceId: data.shipmentServiceDetailId,
                    shipmentUid: data.shipmentUid
                };

                console.log("Copy Shipment Object ", copyShipmentObject);

                ShipmentGetFromUID.get({
                        shipmentuid: copyShipmentObject.shipmentUid
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $rootScope.copyShipmentData = uiShipmentDataService.copyShipment(data.responseObject, copyShipmentObject);
                            var stateCount = 0;
                            if ($stateParams.count != undefined && $stateParams.count != null) {
                                stateCount = $stateParams.count + 1;
                            }
                            $scope.spinner = false;
                            $state.go('layout.addNewShipment', { count: stateCount, forCopyShipment: true });
                            $rootScope.navigateToNextField('shipmentParty');
                        } else {
                            $scope.shipment = null;
                            $scope.spinner = false;
                        }
                    },
                    function(error) {
                        console.log("Shipment get Failed : " + error)
                        $scope.shipment = null;
                        $scope.spinner = false;
                    });
            }


            $scope.reportData = {};


            $scope.documentIdList = [];
            $scope.reportList = [];
            $scope.reportCountList = [];
            $scope.reportDetailData = {};
            var allRepoVar = {};
            $scope.allRepoVarModel = {};
            $scope.allRepoVarModel.allReportSelected = false;
            $scope.reportLoadingImage = {};
            $scope.reportLoadingImage.status = false;


            $scope.reportPopUp = function(sd, resourceRefId) {

                try {
                    if ($scope.oldData != JSON.stringify($scope.shipment)) {
                        var newScope = $scope.$new();
                        newScope.errorMessage = $rootScope.nls["ERR0700016"];
                        ngDialog.openConfirm({
                                template: '<p>{{errorMessage}}</p>' +
                                    '<div class="ngdialog-footer">' +
                                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)" style="float:both">Ok</button>' +
                                    '</div>',
                                plain: true,
                                scope: newScope,
                                className: 'ngdialog-theme-default'
                            })
                            .then(
                                function(value) {
                                    $scope.continueToReports(sd, resourceRefId);
                                });
                    } else {
                        $scope.continueToReports(sd, resourceRefId);
                    }
                } catch (e) {

                }
            }


            $scope.continueToReports = function(sd, resourceRefId) {

                $scope.serviceObject = angular.copy(sd);
                //if($scope.hasAccess('CRM_SHIPMENT_SERVICE_REPORTS_VIEW')) {
                $rootScope.mainpreloder = true;
                $scope.reportList = [];
                //console.log("reportListMaking  Tab :: ",resourceRefId);
                $scope.moreAddressTab = 'reports';
                ShipmentDocumentIdList.get({
                        serviceId: resourceRefId
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.documentIdList = data.responseObject;
                            console.log("$scope.documentIdList -- ", $scope.documentIdList);
                            if ($scope.documentIdList.length > 0) {
                                for (var i = 0; i < $scope.documentIdList.length; i++) {
                                    if (sd.serviceMaster.importExport == 'Import') {
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "CARGO_ARRICAL_NOTICE", reportDisplayName: 'Cargo Arrival Notice | Document # ' + $scope.documentIdList[i].documentNo, key: 'cargoArrivalNotice' };
                                        $scope.reportList.push($scope.reportDetailData);
                                    } else {
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "HAWB", reportDisplayName: 'HAWB| Document # ' + $scope.documentIdList[i].documentNo, key: 'hawb' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }

                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "HAWB_DRAFT", reportDisplayName: 'HAWB DRAFT| Document # ' + $scope.documentIdList[i].documentNo, key: 'hawbDraft' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }

                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "BOOKING_CONFIRMATION", reportDisplayName: 'Shipment Confirmation | Document # ' + $scope.documentIdList[i].documentNo, key: 'shipmentConfirmation' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "TRUCKING_INSTRUCTION", reportDisplayName: 'Trucking Instruction | Document # ' + $scope.documentIdList[i].documentNo, key: 'truckingInstruction' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "SHIPPING_INSTRUCTION", reportDisplayName: 'Shipping Instruction | Document # ' + $scope.documentIdList[i].documentNo, key: 'shippingInstruction' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "CERTIFICATE_OF_ORIGIN", reportDisplayName: 'Certificate Of Origin | Document # ' + $scope.documentIdList[i].documentNo, key: 'certificateOfOrigin' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "STATEMENT_OF_CHARGE", reportDisplayName: 'Statement Of Charge | Document # ' + $scope.documentIdList[i].documentNo, key: 'statementOfCharge' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "SUBJOB_COST_SHEET", reportDisplayName: ' Consol Profitability | Document # ' + $scope.documentIdList[i].documentNo, key: 'consolProfitability' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }

                                        if ((sd.directShipment == 'No' || sd.directShipment == false) && $scope.checkCargoRecieved(sd)) {
                                            for (var i = 0; i < $scope.documentIdList.length; i++) {
                                                $scope.reportDetailData = {};
                                                $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "DOCK_RECEIPT", reportDisplayName: ' Dock Receipt | Document # ' + $scope.documentIdList[i].documentNo, key: 'dockReceipt' };
                                                $scope.reportList.push($scope.reportDetailData);
                                            }
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "EXPORT_JOB_CARD_HAWB", reportDisplayName: ' Export Job Card(HAWB) | Document # ' + $scope.documentIdList[i].documentNo, key: 'exportJobCard' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "FORWARDERS_CARGO_RECEIPT", reportDisplayName: ' Forwarders Cargo Receipt | Document # ' + $scope.documentIdList[i].documentNo, key: 'forwardersCargoReceipt' };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }

                                    }


                                }
                                $scope.assignDefaultReports(sd, resourceRefId);
                            } else {
                                $scope.assignDefaultReports(sd, resourceRefId);
                            }

                        } else {
                            $scope.assignDefaultReports(sd, resourceRefId);

                        }
                    },
                    function(error) {
                        $scope.assignDefaultReports(sd, resourceRefId);
                        console.log("Shipment get Failed : " + error)
                        $rootScope.mainpreloder = false;
                    });

            }

            $scope.checkCargoRecieved = function(sd) {
                for (var i = 0; i < sd.eventList.length; i++) {
                    if (sd.eventList[i].eventMaster.eventMasterType != null && sd.eventList[i].eventMaster.eventMasterType == 'CARGO_RECEIVED') {
                        return true;
                    }
                }
                return false;
            }
            $scope.assignDefaultReports = function(sd, resourceRefId) {
                if (sd.serviceMaster.importExport == 'Export') {
                    $scope.reportDetailData = { resourceRefId: resourceRefId, isDocument: false, isService: true, documentNo: '', reportName: "COMMERCIAL_INVOICE", reportDisplayName: 'Commercial Invoice ', key: 'commercialInvoice' };
                    $scope.reportData = {};
                    $scope.reportList.push($scope.reportDetailData);
                }

                $rootScope.mainpreloder = false;
                $scope.myOtherModalLists = $modal({
                    scope: $scope,
                    templateUrl: '/app/components/crm/new-shipment/dialog/report.html',
                    show: false
                });
                $rootScope.mainpreloder = false;
                $scope.myOtherModalLists.$promise.then($scope.myOtherModalLists.show);
            }

            //which will close pop up and refresh the page
            $scope.closeReports = function() {


                $scope.myOtherModalLists.$promise.then($scope.myOtherModalLists.hide);
                var stateCount = 0;
                if ($stateParams != undefined && $stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }
                $state.go("layout.editNewShipment", {
                    shipmentId: $scope.shipment.id,
                    submitAction: 'EDIT',
                    count: stateCount
                });
            }

            $scope.selectAllReports = function() {
                for (var i = 0; i < $scope.reportList.length; i++) {
                    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
                }
                if (!$scope.allRepoVarModel.allReportSelected) {
                    $scope.isGenerateAll = false;
                }
            }


            $scope.chkReportStatus = function(chkFlag, detObj) {
                console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
            }

            $scope.isSingle = false;
            $scope.isMultiple = false;

            $scope.isGenerateAll = false;

            $scope.generateAll = function() {
                $scope.dataReportName = null;
                var flag = false;
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        flag = true
                        break;
                    }
                }
                if (!flag) {
                    $scope.isGenerateAll = false;
                    Notification.warning("Please select atleast one.......)")
                } else {
                    $scope.isGenerateAll = true;
                    $scope.myReportModal = {};
                    $scope.myReportModal = $modal({
                        scope: $scope,
                        templateUrl: 'app/components/report/report_opt.html',
                        show: false
                    });
                    $rootScope.mainpreloder = false;
                    $scope.myReportModal.$promise.then($scope.myReportModal.show);
                }
            }


            $scope.generateAllReports = function() {
                if ($scope.reportList == undefined || $scope.reportList == null || $scope.reportList.length == 0) {} else {
                    //Using for mail sending for download reports 
                    $scope.emailReportList = [];
                    for (var i = 0; i < $scope.reportList.length; i++) {
                        if ($scope.reportList[i].isSelected) {
                            $scope.emailReport = {};
                            $scope.emailReport.resourceId = $scope.reportList[i].resourceRefId;
                            $scope.emailReport.reportName = $scope.reportList[i].reportName;
                            $scope.emailReport.downloadOption = $scope.downloadOption;
                            $scope.emailReport.downloadFileType = $scope.downloadFileType;
                            $scope.emailReportList.push($scope.emailReport);
                        }
                    }

                }
                if ($scope.emailReportList != undefined && $scope.emailReportList.length > 0) {
                    $scope.isMultiple = true;
                    $scope.isSingle = false;
                } else {
                    $scope.isSingle = true;
                    $scope.isMultiple = false;
                }
                var flag = false;
                if ($scope.isSingle) {
                    for (var i = 0; i < $scope.reportList.length; i++) {
                        if ($scope.reportList[i].isSelected) {
                            flag = true
                            $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
                            $timeout(function() {
                                $scope.reportLoadingImage.status = false;
                            }, 1000);
                        }
                        if (flag) {
                            break;
                        }
                    }
                    if (!flag) {
                        Notification.warning("Please select atleast one.......)");
                        return;
                    }
                } else if ($scope.isMultiple) {
                    $scope.downloadGenericCallMultiple($scope.emailReportList, $scope.downloadOption, $scope.isMultiple, "PDF", $scope.reportList[0].resourceRefId)
                    console.log("Nothig selected");
                }
            }


            /*Report Related Codes Starts Here*/
            $scope.downloadFileType = "PDF";
            $scope.generate = {};
            $scope.generate.typeArr = ["Download", "Preview", "Print", "eMail"];

            $scope.reportModal = function(resourceRefId, reportRefName, key) {
                if ($scope.reportList != undefined && $scope.reportList != null) {
                    $scope.allRepoVarModel.allReportSelected = false;
                    $scope.isGenerateAll = false;
                    for (var i = 0; i < $scope.reportList.length; i++) {
                        $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
                    }
                }

                if (key == 'cargoArrivalNotice') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_CARGO_ARRICAL_NOTICE_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'hawb') {
                    if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_HAWB_DOWNLOAD)) {
                        //invoice checking
                        $scope.serviceId = [];
                        if ($scope.shipment != undefined && $scope.shipment.shipmentServiceList != undefined && $scope.shipment.shipmentServiceList.length > 0) {
                            $scope.servId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;

                            // STEP-1---Invoice Validation
                            if ($scope.servId != undefined && $scope.servId != null) {
                                $scope.serviceId.push($scope.servId);
                            }
                            if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].lastUpdatedStatus.serviceStatus != "Received" && $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].lastUpdatedStatus.serviceStatus != "Generated") {
                                Notification.error($rootScope.nls["ERR90568"]);
                                $rootScope.mainpreloder = false;
                                return false;
                            }
                            appMetaFactory.reportInvoiceValidation.findByServiceId({
                                serviceId: $scope.serviceId,
                            }).$promise.then(function(
                                data, status) {
                                if (data.responseCode == "ERR0") {
                                    $scope.spinner = false;
                                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                                }
                            }, function(error) {
                                console.log("error while fetching service id");
                                $scope.reportPopUpOpen(resourceRefId, reportRefName);
                                $scope.spinner = false;
                                return false;
                            });

                        } else {
                            $scope.reportPopUpOpen(resourceRefId, reportRefName);
                        }
                    } else {
                        return;
                    }
                }

                if (key == 'hawbDraft') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_HAWB_DRAFT_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'shipmentConfirmation') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_BOOKING_CONFIRMATION_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'truckingInstruction') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_TRUCKING_INSTRUCTION_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'shippingInstruction') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_SHIPPING_INSTRUCTION_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'certificateOfOrigin') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_CERTIFICATE_OF_ORIGIN_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'statementOfCharge') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_STATEMENT_OF_CHARGE_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }

                if (key == 'consolProfitability') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_SUBJOB_COST_SHEET_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }

                if (key == 'exportJobCard') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_EXPORT_JOB_CARD_HAWB_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }

                if (key == 'forwardersCargoReceipt') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_FORWARDERS_CARGO_RECEIPT_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'dockReceipt') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_DOCK_RECEIPT_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
                if (key == 'commercialInvoice') {
                    if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_COMMERCIAL_INVOICE_DOWNLOAD)) {
                        return;
                    }
                    $scope.reportPopUpOpen(resourceRefId, reportRefName);
                }
            };

            $scope.reportPopUpOpen = function(resourceRefId, reportRefName) {

                console.log("Service ID - ", resourceRefId, " -- Name --", reportRefName);
                $scope.changeReportDownloadFormat("PDF");
                $scope.dataResourceId = resourceRefId;
                $scope.dataReportName = reportRefName;
                $scope.reportData = {};
                $scope.myReportModal = {}
                $scope.myReportModal = $modal({
                    scope: $scope,
                    templateUrl: '/app/components/report/report_opt.html',
                    show: false
                });
                $scope.myReportModal.$promise.then($scope.myReportModal.show);

            }

            $scope.changeReportDownloadFormat = function(dType) {
                console.log("Download type Changed :: ", dType);
                $scope.downloadFileType = dType;
                if (dType === 'PDF') {
                    $scope.generate.TypeArr = ["Download", "Preview", "Print"];
                } else if (dType === 'eMail') {
                    $scope.generate.TypeArr = ["eMail"]
                } else {
                    $scope.generate.TypeArr = ["Download"];
                }
            }

            $scope.downloadGenericCall = function(id, downOption, repoName) {
                $scope.downloadOption = downOption;
                if ($scope.isGenerateAll) {
                    $scope.generateAllReports();
                } else {
                    var reportDownloadRequest = {
                        resourceId: id,
                        downloadOption: downOption, // "Download","Preview","Print"
                        downloadFileType: $scope.downloadFileType, //PDF
                        reportName: repoName, //HAWB,BOOKIG_CONFIRMATION,
                        single: true,
                        parameter1: $scope.reportData.parameter1,
                        parameter2: $scope.reportData.parameter2
                    };
                    if (reportDownloadRequest.reportName == 'CARGO_ARRICAL_NOTICE') {
                        SequenceFactoryForCAN.fetch({ 'documentId': id }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                if (downOption === 'eMail') {
                                    if (!flag) {
                                        Notification.error($rootScope.nls["ERR275"]);
                                        return false;
                                    } else {
                                        $rootScope.mainpreloder = true;
                                        downloadFactory.download(reportDownloadRequest);
                                        $timeout(function() {
                                            $rootScope.mainpreloder = false;
                                            $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                                        }, 3000);
                                    }
                                }
                                $rootScope.mainpreloder = true;
                                downloadFactory.download(reportDownloadRequest);
                            }
                        });
                    } else {
                        //$rootScope.mainpreloder = true;
                        if (downOption === 'eMail') {
                            var flag = checkMail();
                            if (!flag) {
                                Notification.error($rootScope.nls["ERR275"]);
                                return false;
                            } else {
                                $rootScope.mainpreloder = true;
                                downloadFactory.download(reportDownloadRequest);
                                $timeout(function() {
                                    $rootScope.mainpreloder = false;
                                    $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                                }, 3000);
                            }
                        } else {
                            $rootScope.mainpreloder = true;
                            downloadFactory.download(reportDownloadRequest);
                        }
                    }
                }

            }


            function checkMail() {
                var flag = false;
                if ($scope.serviceObject != null && $scope.serviceObject != undefined) {
                    if ($scope.serviceObject.party != null && $scope.serviceObject.party != undefined) {
                        if ($scope.serviceObject.party.partyAddressList != null && $scope.serviceObject.party.partyAddressList != undefined && $scope.serviceObject.party.partyAddressList.length > 0) {

                            for (var i = 0; i < $scope.serviceObject.party.partyAddressList.length; i++) {
                                if ($scope.serviceObject.party.partyAddressList[0].addressType == "Primary") {
                                    var emailList = $scope.serviceObject.party.partyAddressList[0].email;
                                    if (emailList == null || emailList == undefined) {
                                        flag = false;
                                    } else {
                                        flag = true;
                                        break;
                                    }
                                } else {
                                    flag = false;
                                }
                            }
                        }
                    }
                }
                return flag;
            }

            $scope.downloadGenericCallMultiple = function(emailReportList, downOption, multiple, type, id) {
                    var reportDownloadRequest = {
                        emailReportList: emailReportList,
                        downloadOption: downOption, // "Download","Preview","Print"
                        //downloadOption : $scope.downOption,
                        downloadFileType: type, //PDF
                        isMultiple: multiple,
                        resourceId: id,
                        screenType: 'Shipment'
                    };
                    if (downOption === 'eMail') {
                        var flag = checkMail();
                        if (!flag) {
                            Notification.error($rootScope.nls["ERR275"]);
                            return false;
                        } else {
                            $rootScope.mainpreloder = true;
                            downloadMultipleFactory.download(reportDownloadRequest);
                            $timeout(function() {
                                $rootScope.mainpreloder = false;
                                $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                            }, 3000);
                        }
                    } else {
                        $rootScope.mainpreloder = false;
                        for (var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
                            var reportFileName = '';
                            reportFileName = reportDownloadRequest.emailReportList[i].reportName;
                            var reportDownloaddRequest = {
                                resourceId: id,
                                downloadOption: downOption, // "Download","Preview","Print"
                                downloadFileType: $scope.downloadFileType, //PDF
                                reportName: reportFileName,
                                single: true
                            };
                            $rootScope.mainpreloder = true;
                            downloadFactory.download(reportDownloaddRequest);
                        }
                    }
                }
                //Report Related code End Here		

            //validate email and mobile
            $scope.validatePartyEmailAndMobile = function(obj, EP, id, err, index, tab) {
                if (EP == 'Email') {
                    if (!$scope.focusAndMessageShower(ValidateUtil.isValidMailNew(obj, id, err, index, tab), undefined)) {}
                } else {
                    if (!$scope.focusAndMessageShower(ValidateUtil.isValidMobile(obj, id, err, index, tab), undefined)) {}
                }
            }

            //AES code

            $scope.aesSearchforService = function(service) {
                if (service.serviceUid != undefined && service.serviceUid != null) {
                    $scope.searchDto = {};
                    $scope.searchDto.serviceUid = service.serviceUid;
                    AesSearch.query($scope.searchDto).$promise.then(function(data, status) {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        var tempArr = [];
                        $scope.finalArr = [];
                        var tempArr = data.responseObject.searchResult;
                        var tempObj = {};
                        angular.forEach(tempArr, function(item, index) {
                            tempObj = item;
                            tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                            if (tempObj.id != undefined && tempObj.id != null) {
                                tempObj.status = 'Generated'
                                tempObj.shipperName = tempObj.shipperName;
                                tempObj.consigneeName = tempObj.consigneeName;
                            }
                            $scope.finalArr.push(tempObj);
                        });

                        if ($scope.finalArr != null && $scope.finalArr.length > 0) {} else {
                            var tempObj = {};
                            tempObj.serviceUid = service.serviceUid;
                            tempObj.consolUid = service.consolUid;
                            tempObj.status = 'Active';
                            if (service.documentList != null && service.documentList[0] != undefined && service.documentList[0].length != 0) {
                                tempObj.hawbNo = service.documentList[0].hawbNo;
                                tempObj.shipperName = service.documentList[0].shipper.partyName;
                                tempObj.consigneeName = service.documentList[0].consignee.partyName;
                            }
                            tempObj.mawbNo = service.mawbNo;
                            tempObj.shipmentUid = service.shipmentUid;
                            $scope.finalArr.push(tempObj);
                        }



                        return $scope.finalArr;
                    });
                } else {
                    return;
                }
            }

            $scope.aesGenerateView = function(aesFile) {
                $scope.spineer = true;
                var fromScreen = {};
                fromScreen = $state.current.name;
                if (aesFile != undefined && aesFile != null) {
                    ServiceGetByUid.get({
                        serviceUid: aesFile.serviceUid
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.serviceObj = data.responseObject;
                            if ($scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || $scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                                $scope.spinner = false;
                                $state.go('layout.aesView', {
                                    aesId: aesFile.id,
                                    fromScreen: $state.current.name,
                                    shipmentUid: $scope.serviceObj.shipmentUid,
                                    shipmentId: $scope.shipment.id
                                });
                            } else {
                                $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                                $scope.spinner = false;
                                return false;
                            }
                        }

                    }, function(error) {
                        $scope.spinner = false;
                        console.log("Consol get Failed : " + error)
                    });
                }
            }

            $scope.aesGenerateAdd = function(aesFile) {
                console.log("aesGenerateAdd method called");
                $rootScope.clientMessage = null;
                var fromScreen = {};
                fromScreen = $state.current.name;
                if (aesFile != undefined && aesFile != null) {
                    ServiceGetByUid.get({
                        serviceUid: aesFile.serviceUid
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.serviceObj = data.responseObject;
                            if ($scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || $scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                                var navAesObj = {
                                    selectedPageNumber: $scope.detailTab == 'active' ? $scope.activeSearchDto.selectedPageNumber : $scope.searchDto.selectedPageNumber,
                                    status: $scope.detailTab,
                                    totalRecord: $scope.detailTab == 'active' ? $scope.totalRecordView : $scope.totalRecord,
                                    shipmentId: $scope.shipment.id,
                                    serviceUid: $scope.serviceObj.serviceUid,
                                    shipmentUid: $scope.serviceObj.shipmentUid,
                                    consolUid: $scope.serviceObj.consolUid,
                                    etd: $rootScope.dateAndTimeToString($scope.serviceObj.etd),
                                    carrier: $scope.serviceObj.carrier,
                                    shipper: $scope.serviceObj.documentList[0].shipper,
                                    consignee: $scope.serviceObj.documentList[0].consignee,
                                    fromState: $state.current.name,
                                    fromScreen: fromScreen,
                                    mawbNo: $scope.serviceObj.mawbNo,
                                    hawbNo: $scope.serviceObj.documentList[0].hawbNo
                                };
                                if ($scope.serviceObj.serviceMaster.transportMode == 'Air') {
                                    navAesObj.iataOrScacCode = $scope.serviceObj.carrier.iataCode;
                                } else if ($scope.serviceObj.serviceMaster.transportMode == 'Ocean') {
                                    navAesObj.iataOrScacCode = $scope.serviceObj.carrier.scacCode;
                                }
                                $scope.spinner = false;
                                $state.go("layout.addAes", { navAesObj: navAesObj });
                            } else {
                                $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                                $scope.spinner = false;
                                return false;
                            }
                        } else {
                            $scope.spinner = false;
                            return false;
                        }
                    }, function(error) {
                        console.log("Consol get Failed : " + error)
                        $scope.spinner = false;
                    });
                } else {
                    $scope.spinner = false;
                }
            };

            $scope.generateAllAes = function() {
                $scope.spinner = true;
                var count = 0;
                var aesArr = [];
                if ($scope.finalArr != null && $scope.finalArr != undefined && $scope.finalArr.length > 0) {
                    $scope.aesGenerateAdd($scope.finalArr[0]);
                }

            }

            $scope.aesGenerateAll = function(aesArr) {
                console.log("Aes files need to genarate : ", aesArr);
                AesAddAll.save(aesArr).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Aes Saved Successfully");
                        $scope.spinner = false;
                        Notification.success("Successfully generated ");
                    } else {
                        $scope.spinner = false;
                        console.log("Aes Saving Failed ", data.responseDescription);
                    }
                }, function(error) {
                    $scope.spinner = false;
                    console.log("Aes Saving Failed : ", error)
                });
            }

            //Go to shipment link

            $scope.goToShipmentViewByServiceUid = function(serviceuid) {
                console.log("State  : ", $state)
                console.log("State Param  : ", $stateParams)
                console.log("serviceuid ", serviceuid)
                ShipmentIdByServiceUid.get({
                        serviceuid: serviceuid
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            var param = {
                                shipmentId: data.responseObject,
                                fromState: $state.current.name,
                                fromStateParams: JSON.stringify($stateParams)

                            }
                            $state.go("layout.viewCrmShipment", param);
                        }
                    },
                    function(error) {
                        console.log("Shipment get Failed : " + error)
                    });

            }

            //aes coding


            //shipment milestone code started here

            var addtrackModal;
            $scope.trackmodel = function(shipmentId) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_TRACK_SHIPMENT_VIEW)) {
                    $scope.trackShipment(shipmentId);
                    addtrackModal = $modal({
                        scope: $scope,
                        backdrop: 'static',
                        templateUrl: 'app/components/crm/new-shipment/dialog/track-shipment-popup.html',
                        show: false
                    });
                    $scope.navigateToNextField("flightSchedule.service");
                }
            };


            $scope.trackShipment = function(shipmentId) {
                    ShipmentTrack.get({
                            shipmentId: shipmentId
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                addtrackModal.$promise.then(addtrackModal.show);
                                $scope.shipmentTrackList = uiShipmentDataService.prepareToolTipData(data.responseObject);
                            } else {
                                Notification.error($rootScope.nls[data.responseCode]);
                            }
                        },
                        function(error) {
                            console.log("track Shipment Failed : " + error)
                        });
                }
                //shipment milestone code ends here

            $scope.clearPickupFromCode = function(pickUpDeliveryPoint) {
                if (pickUpDeliveryPoint.pickupFrom == null || pickUpDeliveryPoint.pickupFrom == undefined || pickUpDeliveryPoint.pickupFrom.id == null)
                    pickUpDeliveryPoint.pickupFromCode = null;
            }

            $scope.clearDeliveryFromCode = function(pickUpDeliveryPoint) {
                if (pickUpDeliveryPoint.deliveryFrom == null || pickUpDeliveryPoint.deliveryFrom == undefined || pickUpDeliveryPoint.deliveryFrom.id == null)
                    pickUpDeliveryPoint.deliveryFromCode = null;
            }

            $scope.clearDoorDeliveryFromCode = function(pickUpDeliveryPoint) {
                if (pickUpDeliveryPoint.doorDeliveryFrom == null || pickUpDeliveryPoint.doorDeliveryFrom == undefined || pickUpDeliveryPoint.doorDeliveryFrom.id == null)
                    pickUpDeliveryPoint.doorDeliveryFromCode = null;
            }



            var signOffModel;
            //Sign Off Modal
            $scope.serviceSignOffModal = function(serviceObject, index) {

                if ($scope.oldData != JSON.stringify($scope.shipment)) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR267"];
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Ok</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    }).then(
                        function(value) {

                        });
                } else {
                    $scope.serviceObject = angular.copy(serviceObject);
                    $scope.shipmentServiceSignOffIndex = index;
                    if ($scope.serviceObject.shipmentServiceSignOff == undefined || $scope.serviceObject.shipmentServiceSignOff == null) {
                        $scope.serviceObject.shipmentServiceSignOff = {};
                        $scope.serviceObject.shipmentServiceSignOff.signOffBy = $rootScope.userProfile.employee;
                        $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
                        $scope.serviceObject.shipmentServiceSignOff.shipmentUid = serviceObject.shipmentUid;
                        $scope.serviceObject.shipmentServiceSignOff.serviceUid = serviceObject.serviceUid;
                        $scope.serviceObject.shipmentServiceSignOff.isSignOff = true;
                        $scope.serviceObject.shipmentServiceSignOff.consolUid = serviceObject.consolUid;
                        $scope.serviceObject.shipmentServiceSignOff.shipmentServiceDetail = serviceObject.id;
                    } else {
                        $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString($scope.serviceObject.shipmentServiceSignOff.signOffDate);
                    }
                    signOffModel = $modal({ scope: $scope, templateUrl: 'app/components/crm/shipment/views/service_signoff_popup.html', show: false });
                    signOffModel.$promise.then(signOffModel.show);
                }

            };

            /*Salesman Select Picker*/
            $scope.ajaxEmployeeEvent = function(object) {
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                return EmployeeList.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.customerServiceList = data.responseObject.searchResult;
                            console.log("$scope.customerServiceList", $scope.customerServiceList);
                            return $scope.customerServiceList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Customer Services');
                    }
                );

            }
            $scope.ajaxCommentEvent = function(object) {
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                return commentMasterFactory.keyword.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.commentList = data.responseObject.searchResult;
                            console.log("$scope.commentList", $scope.commentList);
                            return $scope.commentList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Customer Services');
                    }
                );

            }

            $scope.selectedCommentMaster = function() {
                $scope.serviceObject.shipmentServiceSignOff.description = $scope.serviceObject.shipmentServiceSignOff.commentMaster.description;
            }


            $scope.saveServiceSignOff = function(signOffObject) {
                console.log("Sign Off Object", signOffObject);
                // close the model

                signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
                if (signOffObjectCopy.signOffDate != null)
                    signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);

                appMetaFactory.shipmentService.signOff(signOffObjectCopy).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        signOffModel.$promise.then(signOffModel.hide);

                        var localRefStateParam = { shipmentId: $scope.shipment.id, submitAction: 'Saved' };
                        if ($stateParams.count != undefined && $stateParams.count != null) {
                            localRefStateParam.count = $stateParams.count + 1;
                        } else {
                            localRefStateParam.count = 0;
                        }

                        console.log("State Parameters :: ", localRefStateParam);
                        $state.go("layout.editNewShipment", localRefStateParam);

                    } else {
                        Notification.error($rootScope.nls[data.responseCode]);
                        console.log("Sign off Failed", data.responseDescription);
                    }
                }, function(error) {
                    console.log("Sign off Failed", error);
                });

            }


            var unSignOffModel;
            //Sign Off Modal
            $scope.serviceUnSignOffModal = function(serviceObject, index) {

                if ($scope.oldData != JSON.stringify($scope.shipment)) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR268"];
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Ok</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    }).then(
                        function(value) {

                        });
                } else {
                    if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_UNSIGN_OFF_MODIFY)) {

                        $scope.serviceObject = serviceObject;

                        $scope.serviceObject.shipmentServiceSignOff.signOffBy = $rootScope.userProfile.employee;
                        $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());

                        $scope.shipmentServiceSignOffIndex = index;
                        unSignOffModel = $modal({ scope: $scope, templateUrl: 'app/components/crm/shipment/views/service_unsignoff_popup.html', show: false });
                        unSignOffModel.$promise.then(unSignOffModel.show);

                    }
                }

            };

            $scope.saveServiceUnSignOff = function(signOffObject) {
                // close the model


                signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
                if (signOffObjectCopy.signOffDate != null)
                    signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);

                signOffObjectCopy.isSignOff = false;

                appMetaFactory.shipmentServiceUnSignOff.unSignOff(signOffObjectCopy).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        unSignOffModel.$promise.then(unSignOffModel.hide);
                        var localRefStateParam = { shipmentId: $scope.shipment.id, submitAction: 'Saved' };
                        if ($stateParams.count != undefined && $stateParams.count != null) {
                            localRefStateParam.count = $stateParams.count + 1;
                        } else {
                            localRefStateParam.count = 0;
                        }

                        console.log("State Parameters :: ", localRefStateParam);
                        $state.go("layout.editNewShipment", localRefStateParam);
                    } else {

                        Notification.error($rootScope.nls[data.responseCode]);

                        console.log("Sign off Failed", data.responseDescription);
                    }
                }, function(error) {
                    console.log("Sign off Failed", error);
                });
            }


            /**
             * State Identification Handling Here.
             * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
             */
            //



            $scope.isReloaded = localStorage.isShipmentDataReloaded;
            switch ($stateParams.action) {
                case "ADD":
                    $scope.initDateFormat();
                    if ($stateParams.fromHistory === 'Yes') {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            $scope.shipment = $rootScope.selectedUnfilledFormData;
                            $scope.partyMasterActionResponseFn();
                        }
                    } else if ($stateParams.forCopyShipment) {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            $scope.shipment = $rootScope.copyShipmentData;
                            $scope.changeShipmentServiceView(0, 'serviceRouting');
                        }
                    } else {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                            $scope.restoreActiveObject();
                        } else {
                            //							$scope.addInit();
                            $scope.shipment = uiShipmentDataService.addInitialization();
                            $scope.changeShipmentServiceView(0, 'serviceRouting');
                            $log.debug("Shipment Data :: ", $scope.shipment);
                        }
                    }
                    $scope.showCopyShipment = true;
                    $scope.timeoutFocus();
                    $rootScope.breadcrumbArr = uiShipmentDataService.getAddBreadCrumb();
                    break;
                case "CREATE":
                    $scope.initDateFormat();
                    if ($scope.fromHistory === 'Yes') {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            $scope.shipment = $rootScope.selectedUnfilledFormData;
                            $scope.partyMasterActionResponseFn();
                        }
                    } else {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            $scope.shipment = $rootScope.createShipment;
                            $scope.changeShipmentServiceView(0, 'serviceRouting');
                        }
                    }
                    $scope.showCopyShipment = false;
                    $scope.timeoutFocus();
                    $rootScope.breadcrumbArr = uiShipmentDataService.getAddBreadCrumb();
                    break;

                case "EDIT":
                    $scope.initDateFormat();
                    if ($scope.fromHistory === 'Yes') {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            $scope.shipment = $rootScope.selectedUnfilledFormData;
                            $scope.partyMasterActionResponseFn();
                        }
                    } else {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isShipmentDataReloaded = "NO";
                            try {
                                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                                $scope.restoreActiveObject();
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            $scope.getShipmentForEdit();
                        }
                    }
                    $scope.showCopyShipment = false;
                    $rootScope.breadcrumbArr = uiShipmentDataService.getEditBreadCrumb();
                    break;
                default:
            }


        } //end
    ]);
})();