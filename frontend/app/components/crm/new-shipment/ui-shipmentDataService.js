app.service('uiShipmentDataService', ['$rootScope', 'uiShipmentFactory', '$timeout', '$state', '$stateParams', 'addressJoiner', 'AgentByServicePort', 'RecentHistorySaveService', 'PackGetByCode', 'MandatoryReferenceTypeByKey', 'Notification', 'roleConstant', 'ValidateUtil',
    function($rootScope, uiShipmentFactory, $timeout, $state, $stateParams, addressJoiner, AgentByServicePort, RecentHistorySaveService, PackGetByCode, MandatoryReferenceTypeByKey, Notification, roleConstant, ValidateUtil) {

        var that = this;
        this.assignPrimaryAddress = function(obj, mapToAddrss) {
            if (obj != null && obj.id != null && obj.partyAddressList != null && obj.partyAddressList != undefined) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss, obj, obj.partyAddressList[i]);
                    }
                }
            }
        }



        this.getAttachmentHeaderData = function() {
            return {
                "isEdit": true,
                "isDelete": true,
                "page": "shipment",
                "columnDefs": [{
                        "name": "Document Name",
                        "model": "documentMaster.documentName",
                        "type": "text"
                    },
                    {
                        "name": "Reference #",
                        "model": "refNo",
                        "type": "text"
                    },
                    {
                        "name": "Customs",
                        "model": "customs",
                        "type": "text"
                    },
                    {
                        "name": "Unprotected File Name",
                        "model": "unprotectedFileName",
                        "type": "file"
                    },
                    {
                        "name": "Protected File Name",
                        "model": "protectedFileName",
                        "type": "file"
                    },
                    {
                        "name": "Actions",
                        "model": "action",
                        "type": "action"
                    }
                ]
            };
        }

        this.addNewShipmentService = function(shipment) {
            //Validate Shipment to add New Service
            /*if(validate(shipment)) {
            	
            }*/
            var shipmentServiceDetail = {};
            shipmentServiceDetail.serviceReqDate = new Date();
            shipmentServiceDetail.isClearance = false;
            shipmentServiceDetail.pickUpDeliveryPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.transporter = {};
            shipmentServiceDetail.pickUpDeliveryPoint.pickupPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.pickupFrom = {};
            shipmentServiceDetail.pickUpDeliveryPoint.deliveryPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.deliveryFrom = {};
            shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryFrom = {};
            //shipmentServiceDetail.whoRouted = vm.shipment.whoRouted;
            shipmentServiceDetail.customerService = $rootScope.userProfile.employee;
            shipmentServiceDetail.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;

            if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                shipmentServiceDetail.dimensionUnit = true;
                shipmentServiceDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            } else {
                shipmentServiceDetail.dimensionUnit = false;
                shipmentServiceDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            }
            shipmentServiceDetail.whoRouted = false;
            shipmentServiceDetail.directShipment = false;
            shipmentServiceDetail.ppcc = false;
            shipmentServiceDetail.holdShipment = false;
            shipmentServiceDetail.hazardous = false;
            shipmentServiceDetail.overDimension = false;
            shipmentServiceDetail.isMinimumShipment = false;
            shipmentServiceDetail.isAgreed = false;
            shipmentServiceDetail.isMarksNo = true;
            shipmentServiceDetail.location = $rootScope.userProfile.selectedUserLocation;
            shipmentServiceDetail.company = $rootScope.userProfile.selectedUserLocation.companyMaster;

            shipmentServiceDetail.documentList = [];

            var documentDetail = {};
            documentDetail.dimensionList = [{}];
            documentDetail.shipperAddress = {};
            documentDetail.forwarderAddress = {};
            documentDetail.consigneeAddress = {};
            documentDetail.firstNotifyAddress = {};
            documentDetail.secondNotifyAddress = {};
            documentDetail.agentAddress = {};
            documentDetail.issuingAgentAddress = {};
            documentDetail.documentReqDate = $rootScope.sendApiDateAndTime(new Date());
            if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                documentDetail.dimensionUnit = true;
                documentDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            } else {
                documentDetail.dimensionUnit = false;
                documentDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            }

            if (shipment.isFromConsolForImport != undefined && (shipment.isFromConsolForImport == 'true' || shipment.isFromConsolForImport)) {
                documentDetail.issuingAgent = null;
                documentDetail.issuingAgentAddress = null;
            } else if ($rootScope.userProfile.selectedUserLocation.tmpPartyMaster != undefined && $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != null &&
                $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != undefined && $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != null) {
                documentDetail.issuingAgent = $rootScope.userProfile.selectedUserLocation.tmpPartyMaster;
                this.assignPrimaryAddress(documentDetail.issuingAgent, documentDetail.issuingAgentAddress);
            }

            shipmentServiceDetail.documentList.push(documentDetail);

            shipmentServiceDetail.shipmentChargeList = [{
                "actualChargeable": "CHARGEABLE"
            }];
            shipmentServiceDetail.connectionList = [{
                connectionStatus: 'Planned'
            }];
            shipmentServiceDetail.shipmentAttachmentList = [];
            //Reference List initialize with one default empty object
            shipmentServiceDetail.referenceList = [];

            shipmentServiceDetail.billOfEntryList = [];

            //Trigger List initialize with one default empty object
            shipmentServiceDetail.shipmentServiceTriggerList = [];
            //Authenticated Document List initialize with one default empty object
            shipmentServiceDetail.authenticatedDocList = [{}];
            // Attachment Configuration for each shipment service
            shipmentServiceDetail.attachConfig = {
                "isEdit": true,
                "isDelete": true,
                "page": "shipment",
                "columnDefs": [{
                        "name": "Document Name",
                        "model": "documentMaster.documentName",
                        "type": "text"
                    },
                    {
                        "name": "Reference #",
                        "model": "refNo",
                        "type": "text"
                    },
                    {
                        "name": "Customs",
                        "model": "customs",
                        "type": "text"
                    },
                    {
                        "name": "Unprotected File Name",
                        "model": "unprotectedFileName",
                        "type": "file"
                    },
                    {
                        "name": "Protected File Name",
                        "model": "protectedFileName",
                        "type": "file"
                    },
                    {
                        "name": "Actions",
                        "model": "action",
                        "type": "action"
                    }
                ]
            };

            // Assigning Default Package in Shipment Service Detail configured in appMasterData..
            shipmentServiceDetail.packMaster = {}
            var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
            if (defaultPackageCode != undefined && defaultPackageCode != null) {
                console.log("Default Package is assigned....");
                PackGetByCode.get({
                    code: defaultPackageCode
                }, function(data) {
                    if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                        console.log("Successful while getting pack.", data)
                        shipmentServiceDetail.packMaster = data.responseObject;
                        for (var i = 0; i < shipmentServiceDetail.documentList.length; i++) {
                            shipmentServiceDetail.documentList[i].packMaster = shipmentServiceDetail.packMaster;
                        }

                    }
                }, function(error) {
                    console.log("Error while getting pack.", error)
                });
            } else {
                console.log("Default Package Code is not defined in Default Master Data....");
            }
            shipment.shipmentServiceList.push(shipmentServiceDetail);
            return shipment;
        };


        this.addInitialization = function() {

            var shipment = {};
            shipment.shipmentServiceList = [];
            shipment.createdBy = $rootScope.userProfile.employee;
            shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
            return this.addNewShipmentService(shipment);
        }

        this.getAddBreadCrumb = function() {
            return [{
                label: "CRM",
                state: "layout.crmShipment"
            }, {
                label: "Shipment",
                state: "layout.crmShipment"
            }, {
                label: "Add Shipment",
                state: null
            }];
        };

        this.getEditBreadCrumb = function() {
            return [{
                label: "CRM",
                state: "layout.crmShipment"
            }, {
                label: "Shipment",
                state: "layout.crmShipment"
            }, {
                label: "Edit Shipment",
                state: null
            }];
        };

        this.convertToSaveObject = function(shipmentObj) {
            $rootScope.mainpreloder = true;

            if (shipmentObj.id == undefined || shipmentObj.id == null || shipmentObj.id == -1) {

                shipmentObj.createdBy = {
                    "versionLock": shipmentObj.createdBy.versionLock,
                    "id": shipmentObj.createdBy.id
                };

                shipmentObj.companyMaster = {
                    "versionLock": $rootScope.userProfile.selectedUserLocation.companyMaster.versionLock,
                    "id": $rootScope.userProfile.selectedUserLocation.companyMaster.id
                };
                shipmentObj.location = {
                    "versionLock": $rootScope.userProfile.selectedUserLocation.versionLock,
                    "id": $rootScope.userProfile.selectedUserLocation.id,
                    "countryMaster": {
                        "versionLock": $rootScope.userProfile.selectedUserLocation.countryMaster.versionLock,
                        "id": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                    }
                };
                shipmentObj.country = {
                    "versionLock": $rootScope.userProfile.selectedUserLocation.countryMaster.versionLock,
                    "id": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                };
            }


            for (var i = 0; i < shipmentObj.shipmentServiceList.length; i++) {

                if (shipmentObj.shipmentServiceList[i].serviceMaster) {
                    shipmentObj.shipmentServiceList[i].serviceMaster = {
                        "versionLock": shipmentObj.shipmentServiceList[i].serviceMaster.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].serviceMaster.id
                    }
                }


                if (shipmentObj.shipmentServiceList[i].tosMaster) {
                    shipmentObj.shipmentServiceList[i].tosMaster = {
                        "versionLock": shipmentObj.shipmentServiceList[i].tosMaster.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].tosMaster.id
                    }
                }

                if (shipmentObj.shipmentServiceList[i].commodity) {
                    shipmentObj.shipmentServiceList[i].commodity = {
                        "versionLock": shipmentObj.shipmentServiceList[i].commodity.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].commodity.id
                    }
                }


                if (shipmentObj.shipmentServiceList[i].customerService) {
                    if (shipmentObj.shipmentServiceList[i].customerService.locationMaster) {
                        shipmentObj.shipmentServiceList[i].customerService.locationMaster = {
                            "versionLock": shipmentObj.shipmentServiceList[i].customerService.locationMaster.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].customerService.locationMaster.id
                        }

                    }

                    if (shipmentObj.shipmentServiceList[i].customerService.countryMaster) {
                        shipmentObj.shipmentServiceList[i].customerService.countryMaster = {
                            "versionLock": shipmentObj.shipmentServiceList[i].customerService.countryMaster.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].customerService.countryMaster.id

                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].customerService.companyMaster) {
                        shipmentObj.shipmentServiceList[i].customerService.companyMaster = {
                            "versionLock": shipmentObj.shipmentServiceList[i].customerService.companyMaster.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].customerService.companyMaster.id
                        }

                    }

                    if (shipmentObj.shipmentServiceList[i].customerService.employeeProfessionalDetail) {
                        shipmentObj.shipmentServiceList[i].customerService.employeeProfessionalDetail = {
                            "versionLock": shipmentObj.shipmentServiceList[i].customerService.employeeProfessionalDetail.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].customerService.employeeProfessionalDetail.id
                        }

                    }


                    // shipmentObj.shipmentServiceList[i].customerService.id = shipmentObj.shipmentServiceList[i].customerService.id;
                    // shipmentObj.shipmentServiceList[i].customerService.employeeCode = shipmentObj.shipmentServiceList[i].customerService.employeeCode;
                    // shipmentObj.shipmentServiceList[i].customerService.employementStatus = shipmentObj.shipmentServiceList[i].customerService.employementStatus;

                }

                if (shipmentObj.shipmentServiceList[i].origin) {
                    shipmentObj.shipmentServiceList[i].origin = {
                        "versionLock": shipmentObj.shipmentServiceList[i].origin.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].origin.id
                    }
                }
                if (shipmentObj.shipmentServiceList[i].pol) {
                    shipmentObj.shipmentServiceList[i].pol = {
                        "versionLock": shipmentObj.shipmentServiceList[i].pol.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].pol.id
                    }
                }
                if (shipmentObj.shipmentServiceList[i].pod) {
                    shipmentObj.shipmentServiceList[i].pod = {
                        "versionLock": shipmentObj.shipmentServiceList[i].pod.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].pod.id
                    }
                }
                if (shipmentObj.shipmentServiceList[i].coLoader) {
                    shipmentObj.shipmentServiceList[i].coLoader = {
                        "versionLock": shipmentObj.shipmentServiceList[i].coLoader.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].coLoader.id
                    }
                }

                if (shipmentObj.shipmentServiceList[i].packMaster) {
                    shipmentObj.shipmentServiceList[i].packMaster = {
                        "versionLock": shipmentObj.shipmentServiceList[i].packMaster.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].packMaster.id
                    }
                }

                if (shipmentObj.shipmentServiceList[i].carrier) {
                    shipmentObj.shipmentServiceList[i].carrier = {
                        "versionLock": shipmentObj.shipmentServiceList[i].carrier.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].carrier.id
                    }
                }

                if (shipmentObj.shipmentServiceList[i].localCurrency) {
                    shipmentObj.shipmentServiceList[i].localCurrency = {
                        "versionLock": shipmentObj.shipmentServiceList[i].localCurrency.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].localCurrency.id,
                        "countryMaster": {
                            "versionLock": shipmentObj.shipmentServiceList[i].localCurrency.countryMaster.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].localCurrency.countryMaster.id
                        }
                    }
                }



                if (shipmentObj.shipmentServiceList[i].origin) {
                    shipmentObj.shipmentServiceList[i].origin = {
                        "versionLock": shipmentObj.shipmentServiceList[i].origin.versionLock,
                        "id": shipmentObj.shipmentServiceList[i].origin.id
                    }
                }
                // if (shipmentObj.shipmentServiceList[i].lastUpdatedStatus) {
                //     shipmentObj.shipmentServiceList[i].lastUpdatedStatus = {
                //         "versionLock": shipmentObj.shipmentServiceList[i].lastUpdatedStatus.versionLock,
                //         "id": shipmentObj.shipmentServiceList[i].lastUpdatedStatus.id,
                //         "serviceStatus": shipmentObj.shipmentServiceList[i].lastUpdatedStatus.serviceStatus

                //     }
                // }


                if (shipmentObj.shipmentServiceList[i].documentList != undefined && shipmentObj.shipmentServiceList[i].documentList != null && shipmentObj.shipmentServiceList[i].documentList.length != 0) {
                    for (var j = 0; j < shipmentObj.shipmentServiceList[i].documentList.length; j++) {

                        if (shipmentObj.shipmentServiceList[i].documentList[j].id == undefined || shipmentObj.shipmentServiceList[i].documentList[j].id == null) {
                            shipmentObj.shipmentServiceList[i].documentList[j].company = {
                                "versionLock": $rootScope.userProfile.selectedUserLocation.companyMaster.versionLock,
                                "id": $rootScope.userProfile.selectedUserLocation.companyMaster.id
                            };

                            shipmentObj.shipmentServiceList[i].documentList[j].location = {
                                "versionLock": $rootScope.userProfile.selectedUserLocation.versionLock,
                                "id": $rootScope.userProfile.selectedUserLocation.id,
                                "countryMaster": {
                                    "versionLock": $rootScope.userProfile.selectedUserLocation.countryMaster.versionLock,
                                    "id": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                                }
                            };

                            shipmentObj.shipmentServiceList[i].documentList[j].country = {
                                "versionLock": $rootScope.userProfile.selectedUserLocation.countryMaster.versionLock,
                                "id": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                            };

                        }


                        shipmentObj.shipmentServiceList[i].documentList[j].documentReqDate = $rootScope.sendApiDateAndTime(new Date());
                        shipmentObj.shipmentServiceList[i].documentList[j].eta = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].documentList[j].eta);
                        shipmentObj.shipmentServiceList[i].documentList[j].etd = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].documentList[j].etd);


                        if (shipmentObj.shipmentServiceList[i].documentList[j].hawbReceivedOn != undefined && shipmentObj.shipmentServiceList[i].documentList[j].hawbReceivedOn != null)
                            shipmentObj.shipmentServiceList[i].documentList[j].hawbReceivedOn = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].documentList[j].hawbReceivedOn);

                        if (shipmentObj.shipmentServiceList[i].documentList[j].doIssuedDate != undefined && shipmentObj.shipmentServiceList[i].documentList[j].doIssuedDate != null)
                            shipmentObj.shipmentServiceList[i].documentList[j].doIssuedDate = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].documentList[j].doIssuedDate);

                        if (shipmentObj.shipmentServiceList[i].documentList[j].canIssuedDate != undefined && shipmentObj.shipmentServiceList[i].documentList[j].canIssuedDate != null)
                            shipmentObj.shipmentServiceList[i].documentList[j].canIssuedDate = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].documentList[j].canIssuedDate);


                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].shipper) {
                            shipmentObj.shipmentServiceList[i].documentList[j].shipper = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].shipper.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].shipper.id
                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].shipperAddress.country.id

                                }
                            }

                        }


                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].consignee) {
                            shipmentObj.shipmentServiceList[i].documentList[j].consignee = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].consignee.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].consignee.id
                            }
                        }


                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].consigneeAddress.country.id

                                }
                            }

                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].firstNotify) {
                            shipmentObj.shipmentServiceList[i].documentList[j].firstNotify = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].firstNotify.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].firstNotify.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].firstNotifyAddress.country.id

                                }
                            }

                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].secondNotify) {
                            shipmentObj.shipmentServiceList[i].documentList[j].secondNotify = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].secondNotify.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].secondNotify.id

                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].secondNotifyAddress.country.id

                                }
                            }

                        }
                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].forwarder) {
                            shipmentObj.shipmentServiceList[i].documentList[j].forwarder = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].forwarder.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].forwarder.id

                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].forwarderAddress.country.id

                                }
                            }

                        }



                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].agent) {
                            shipmentObj.shipmentServiceList[i].documentList[j].agent = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].agent.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].agent.id
                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].agentAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].agentAddress.country.id

                                }
                            }

                        }


                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].issuingAgent) {
                            shipmentObj.shipmentServiceList[i].documentList[j].issuingAgent = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgent.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgent.id
                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].issuingAgentAddress.country.id

                                }
                            }

                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].chaAgent) {
                            shipmentObj.shipmentServiceList[i].documentList[j].chaAgent = {
                                "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].chaAgent.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].documentList[j].chaAgent.id
                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress) {
                            if (shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.city) {
                                shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.city.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.state) {
                                shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.state.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].chaAgentAddress.country.id

                                }
                            }



                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].origin) {
                                shipmentObj.shipmentServiceList[i].documentList[j].origin = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].origin.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].origin.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].pol) {
                                shipmentObj.shipmentServiceList[i].documentList[j].pol = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].pol.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].pol.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].pod) {
                                shipmentObj.shipmentServiceList[i].documentList[j].pod = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].pod.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].pod.id
                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].destination) {
                                shipmentObj.shipmentServiceList[i].documentList[j].destination = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].destination.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].destination.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].packMaster) {
                                shipmentObj.shipmentServiceList[i].documentList[j].packMaster = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].packMaster.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].packMaster.id
                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].company) {
                                shipmentObj.shipmentServiceList[i].documentList[j].company = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].company.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].company.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].country) {
                                shipmentObj.shipmentServiceList[i].documentList[j].country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].country.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].location) {
                                shipmentObj.shipmentServiceList[i].documentList[j].location = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].location.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].location.id

                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].documentList[j] && shipmentObj.shipmentServiceList[i].documentList[j].carrier) {
                                shipmentObj.shipmentServiceList[i].documentList[j].carrier = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].documentList[j].carrier.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].documentList[j].carrier.id
                                }
                            }


                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != undefined && shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != null) {
                        if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter != undefined && shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter != null &&
                            shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter.id == undefined) {
                            shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName = shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter.partyName;
                            shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter = null;
                        } else {
                            shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName = null;
                        }

                        if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != undefined && shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != null &&
                            shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != "") {

                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp);
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned);
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual);
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected);
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual);
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected);
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual != null)
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual);

                        }
                        if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint) {
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.countryMaster) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.countryMaster = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.countryMaster.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.countryMaster.id
                                };
                            }
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.partyDetail) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.partyDetail = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.partyDetail.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.partyDetail.id
                                };
                            }


                            var partyAddressList = shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.partyAddressList;
                            if (partyAddressList) {
                                for (var j = 0; j < partyAddressList.length; j++) {
                                    if (partyAddressList[j].stateMaster) {
                                        partyAddressList[j].stateMaster = {
                                            "versionLock": partyAddressList[j].stateMaster.versionLock,
                                            "id": partyAddressList[j].stateMaster.id
                                        }
                                    }
                                    if (partyAddressList[j].cityMaster) {
                                        partyAddressList[j].cityMaster = {
                                            "versionLock": partyAddressList[j].cityMaster.versionLock,
                                            "id": partyAddressList[j].cityMaster.id
                                        }
                                    }
                                    if (partyAddressList[j].partyCountryField) {
                                        partyAddressList[j].partyCountryField = {
                                            "versionLock": partyAddressList[j].partyCountryField.versionLock,
                                            "id": partyAddressList[j].partyCountryField.id
                                        }
                                    }

                                }
                            }

                            var partyServiceList = shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickupPoint.partyServiceList;

                            if (partyServiceList) {
                                for (var j = 0; j < partyServiceList.length; j++) {
                                    if (partyServiceList[j].serviceMaster) {
                                        partyServiceList[j].serviceMaster = {
                                            "versionLock": partyServiceList[j].serviceMaster.versionLock,
                                            "id": partyServiceList[j].serviceMaster.id
                                        }
                                    }
                                    if (partyServiceList[j].tosMaster) {
                                        partyServiceList[j].tosMaster = {
                                            "versionLock": partyServiceList[j].tosMaster.versionLock,
                                            "id": partyServiceList[j].tosMaster.id
                                        }
                                    }
                                    if (partyServiceList[j].locationMaster) {
                                        partyServiceList[j].locationMaster = {
                                            "versionLock": partyServiceList[j].locationMaster.versionLock,
                                            "id": partyServiceList[j].locationMaster.id
                                        }
                                    }
                                    if (partyServiceList[j].salesman) {
                                        partyServiceList[j].salesman = {
                                            "versionLock": partyServiceList[j].salesman.versionLock,
                                            "id": partyServiceList[j].salesman.id
                                        }
                                    }
                                    if (partyServiceList[j].customerService) {
                                        partyServiceList[j].customerService = {
                                            "versionLock": partyServiceList[j].customerService.versionLock,
                                            "id": partyServiceList[j].customerService.id
                                        }
                                    }


                                }

                            }


                        }

                        if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace) {
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.state) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.state.id
                                };
                            }
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.city) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.city.id
                                };
                            }
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.country) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.country = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.country.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.country.id
                                };
                            }

                        }

                        if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace) {
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.state) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.state = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.state.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.state.id
                                };
                            }
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.city) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.city = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.city.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.city.id
                                };
                            }

                        }

                        if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPoint) {
                            if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPoint.portMaster) {
                                shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPoint.portMaster = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPoint.portMaster.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPoint.portMaster.id
                                };
                            }

                        }

                    }
                    // if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint) {
                    //     shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint = {
                    //         "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.id,
                    //         "pickUpPlace": {
                    //             "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlace.id
                    //         },
                    //         "deliveryPlace": {
                    //             "id": shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryPlace.id
                    //         }
                    //     }
                    // }

                    if (shipmentObj.shipmentServiceList[i].id == undefined || shipmentObj.shipmentServiceList[i].id == null) {
                        shipmentObj.shipmentServiceList[i].company = {
                            "versionLock": $rootScope.userProfile.selectedUserLocation.companyMaster.versionLock,
                            "id": $rootScope.userProfile.selectedUserLocation.companyMaster.id
                        }


                        shipmentObj.shipmentServiceList[i].location = {
                            "versionLock": $rootScope.userProfile.selectedUserLocation.versionLock,
                            "id": $rootScope.userProfile.selectedUserLocation.id,
                            "countryMaster": {
                                "versionLock": $rootScope.userProfile.selectedUserLocation.countryMaster.versionLock,
                                "id": $rootScope.userProfile.selectedUserLocation.countryMaster.id
                            }
                        }


                        shipmentObj.shipmentServiceList[i].country = {
                            "versionLock": $rootScope.userProfile.selectedUserLocation.countryMaster.versionLock,
                            "id": $rootScope.userProfile.selectedUserLocation.countryMaster.id

                        }

                    } else {
                        if (shipmentObj.shipmentServiceList[i].company) {
                            shipmentObj.shipmentServiceList[i].company = {
                                "versionLock": shipmentObj.shipmentServiceList[i].company.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].company.id
                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].location) {
                            shipmentObj.shipmentServiceList[i].location = {
                                "versionLock": shipmentObj.shipmentServiceList[i].location.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].location.id,
                                "countryMaster": {
                                    "versionLock": shipmentObj.shipmentServiceList[i].location.countryMaster.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].location.countryMaster.id
                                }
                            }
                        }

                        if (shipmentObj.shipmentServiceList[i].country) {
                            shipmentObj.shipmentServiceList[i].country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].country.id
                            }
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].party) {
                        shipmentObj.shipmentServiceList[i].party = {
                            "versionLock": shipmentObj.shipmentServiceList[i].party.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].party.id
                        }
                    }


                    if (shipmentObj.shipmentServiceList[i].partyAddress) {
                        if (shipmentObj.shipmentServiceList[i].partyAddress.city) {
                            shipmentObj.shipmentServiceList[i].partyAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].partyAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].partyAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].partyAddress.state) {
                            shipmentObj.shipmentServiceList[i].partyAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].partyAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].partyAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].partyAddress.country) {
                            shipmentObj.shipmentServiceList[i].partyAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].partyAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].partyAddress.country.id

                            }
                        }

                    }

                    if (shipmentObj.shipmentServiceList[i].firstNotify) {
                        shipmentObj.shipmentServiceList[i].firstNotify = {
                            "versionLock": shipmentObj.shipmentServiceList[i].firstNotify.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].firstNotify.id
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].firstNotifyAddress) {
                        if (shipmentObj.shipmentServiceList[i].firstNotifyAddress.city) {
                            shipmentObj.shipmentServiceList[i].firstNotifyAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].firstNotifyAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].firstNotifyAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].firstNotifyAddress.state) {
                            shipmentObj.shipmentServiceList[i].firstNotifyAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].firstNotifyAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].firstNotifyAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].firstNotifyAddress.country) {
                            shipmentObj.shipmentServiceList[i].firstNotifyAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].firstNotifyAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].firstNotifyAddress.country.id

                            }
                        }

                    }
                    if (shipmentObj.shipmentServiceList[i].secondNotify) {
                        shipmentObj.shipmentServiceList[i].secondNotify = {
                            "versionLock": shipmentObj.shipmentServiceList[i].secondNotify.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].secondNotify.id
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].secondNotifyAddress) {
                        if (shipmentObj.shipmentServiceList[i].secondNotifyAddress.city) {
                            shipmentObj.shipmentServiceList[i].secondNotifyAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].secondNotifyAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].secondNotifyAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].secondNotifyAddress.state) {
                            shipmentObj.shipmentServiceList[i].secondNotifyAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].secondNotifyAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].secondNotifyAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].secondNotifyAddress.country) {
                            shipmentObj.shipmentServiceList[i].secondNotifyAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].secondNotifyAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].secondNotifyAddress.country.id

                            }
                        }

                    }


                    if (shipmentObj.shipmentServiceList[i].agent) {
                        shipmentObj.shipmentServiceList[i].agent = {
                            "versionLock": shipmentObj.shipmentServiceList[i].agent.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].agent.id
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].salesman) {
                        shipmentObj.shipmentServiceList[i].salesman = {
                            "versionLock": shipmentObj.shipmentServiceList[i].salesman.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].salesman.id
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].salesmanAddress) {
                        if (shipmentObj.shipmentServiceList[i].salesmanAddress.city) {
                            shipmentObj.shipmentServiceList[i].salesmanAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].salesmanAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].salesmanAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].salesmanAddress.state) {
                            shipmentObj.shipmentServiceList[i].salesmanAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].salesmanAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].salesmanAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].salesmanAddress.country) {
                            shipmentObj.shipmentServiceList[i].salesmanAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].salesmanAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].salesmanAddress.country.id

                            }
                        }

                    }

                    if (shipmentObj.shipmentServiceList[i].agentAddress) {
                        if (shipmentObj.shipmentServiceList[i].agentAddress.city) {
                            shipmentObj.shipmentServiceList[i].agentAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].agentAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].agentAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].agentAddress.state) {
                            shipmentObj.shipmentServiceList[i].agentAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].agentAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].agentAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].agentAddress.country) {
                            shipmentObj.shipmentServiceList[i].agentAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].agentAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].agentAddress.country.id

                            }
                        }

                    }


                    if (shipmentObj.shipmentServiceList[i].destination) {
                        shipmentObj.shipmentServiceList[i].destination = {
                            "versionLock": shipmentObj.shipmentServiceList[i].destination.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].destination.id
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].shipper) {
                        shipmentObj.shipmentServiceList[i].shipper = {
                            "versionLock": shipmentObj.shipmentServiceList[i].shipper.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].shipper.id
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].shipperAddress) {
                        if (shipmentObj.shipmentServiceList[i].shipperAddress.city) {
                            shipmentObj.shipmentServiceList[i].shipperAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].shipperAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].shipperAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].shipperAddress.state) {
                            shipmentObj.shipmentServiceList[i].shipperAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].shipperAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].shipperAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].shipperAddress.country) {
                            shipmentObj.shipmentServiceList[i].shipperAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].shipperAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].shipperAddress.country.id

                            }
                        }

                    }


                    if (shipmentObj.shipmentServiceList[i].consignee) {
                        shipmentObj.shipmentServiceList[i].consignee = {
                            "versionLock": shipmentObj.shipmentServiceList[i].consignee.versionLock,
                            "id": shipmentObj.shipmentServiceList[i].consignee.id
                        }
                    }


                    if (shipmentObj.shipmentServiceList[i].consigneeAddress) {
                        if (shipmentObj.shipmentServiceList[i].consigneeAddress.city) {
                            shipmentObj.shipmentServiceList[i].consigneeAddress.city = {
                                "versionLock": shipmentObj.shipmentServiceList[i].consigneeAddress.city.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].consigneeAddress.city.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].consigneeAddress.state) {
                            shipmentObj.shipmentServiceList[i].consigneeAddress.state = {
                                "versionLock": shipmentObj.shipmentServiceList[i].consigneeAddress.state.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].consigneeAddress.state.id

                            }
                        }
                        if (shipmentObj.shipmentServiceList[i].consigneeAddress.country) {
                            shipmentObj.shipmentServiceList[i].consigneeAddress.country = {
                                "versionLock": shipmentObj.shipmentServiceList[i].consigneeAddress.country.versionLock,
                                "id": shipmentObj.shipmentServiceList[i].consigneeAddress.country.id

                            }
                        }

                    }

                    shipmentObj.shipmentServiceList[i].eta = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].eta);
                    shipmentObj.shipmentServiceList[i].etd = $rootScope.sendApiDateAndTime(shipmentObj.shipmentServiceList[i].etd);
                    shipmentObj.shipmentServiceList[i].serviceReqDate = $rootScope.sendApiDateAndTime(new Date());

                    if (shipmentObj.shipmentServiceList[i].connectionList != undefined && shipmentObj.shipmentServiceList[i].connectionList != null && shipmentObj.shipmentServiceList[i].connectionList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].connectionList.length; j++) {
                            shipmentObj.shipmentServiceList[i].connectionList[j].eta = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].connectionList[j].eta);
                            shipmentObj.shipmentServiceList[i].connectionList[j].etd = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].connectionList[j].etd);

                            if (shipmentObj.shipmentServiceList[i].connectionList[j].pol) {
                                shipmentObj.shipmentServiceList[i].connectionList[j].pol = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].connectionList[j].pol.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].connectionList[j].pol.id
                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].connectionList[j].pod) {
                                shipmentObj.shipmentServiceList[i].connectionList[j].pod = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].connectionList[j].pod.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].connectionList[j].pod.id
                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].connectionList[j].carrierMaster) {
                                shipmentObj.shipmentServiceList[i].connectionList[j].carrierMaster = {
                                    "versionLock": shipmentObj.shipmentServiceList[i].connectionList[j].carrierMaster.versionLock,
                                    "id": shipmentObj.shipmentServiceList[i].connectionList[j].carrierMaster.id
                                }
                            }

                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].eventList != undefined && shipmentObj.shipmentServiceList[i].eventList != null && shipmentObj.shipmentServiceList[i].eventList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].eventList.length; j++) {
                            shipmentObj.shipmentServiceList[i].eventList[j].eventDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].eventList[j].eventDate);
                            if (shipmentObj.shipmentServiceList[i].eventList[j].followUpDate != undefined &&
                                shipmentObj.shipmentServiceList[i].eventList[j].followUpDate != null) {
                                shipmentObj.shipmentServiceList[i].eventList[j].followUpDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].eventList[j].followUpDate);
                            } else {
                                shipmentObj.shipmentServiceList[i].eventList[j].followUpDate = null;
                            }
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].authenticatedDocList != undefined && shipmentObj.shipmentServiceList[i].authenticatedDocList != null && shipmentObj.shipmentServiceList[i].authenticatedDocList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].authenticatedDocList.length; j++) {
                            shipmentObj.shipmentServiceList[i].authenticatedDocList[j].date = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].authenticatedDocList[j].date);
                        }
                    }


                    if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList != undefined && shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList != null && shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList.length; j++) {

                            if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j]) {
                                if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date != null) {
                                    shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date);
                                }
                                if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate != null) {
                                    shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate);
                                }
                                if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerMaster) {
                                    shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerMaster = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerMaster.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerMaster.id
                                    }
                                }
                                if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerTypeMaster) {
                                    shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerTypeMaster = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerTypeMaster.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[j].triggerTypeMaster.id
                                    }
                                }

                            }
                        }
                    }




                    if (shipmentObj.shipmentServiceList[i].authenticatedDocList != undefined && shipmentObj.shipmentServiceList[i].authenticatedDocList != null && shipmentObj.shipmentServiceList[i].authenticatedDocList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].authenticatedDocList.length; j++) {

                            if (shipmentObj.shipmentServiceList[i].authenticatedDocList[j]) {
                                if (shipmentObj.shipmentServiceList[i].authenticatedDocList[j].date != null) {
                                    shipmentObj.shipmentServiceList[i].authenticatedDocList[j].date = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].authenticatedDocList[j].date);
                                }
                                if (shipmentObj.shipmentServiceList[i].authenticatedDocList[j].origin) {
                                    shipmentObj.shipmentServiceList[i].authenticatedDocList[j].origin = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].authenticatedDocList[j].origin.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].authenticatedDocList[j].origin.id
                                    }
                                }
                                if (shipmentObj.shipmentServiceList[i].authenticatedDocList[j].destination) {
                                    shipmentObj.shipmentServiceList[i].authenticatedDocList[j].destination = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].authenticatedDocList[j].destination.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].authenticatedDocList[j].destination.id
                                    }
                                }
                            }
                        }
                    }


                    if (shipmentObj.shipmentServiceList[i].billOfEntryList != undefined && shipmentObj.shipmentServiceList[i].billOfEntryList != null && shipmentObj.shipmentServiceList[i].billOfEntryList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].billOfEntryList.length; j++) {
                            if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate != undefined &&
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate != null) {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate);
                            } else {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate = null;
                            }
                            if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate != undefined &&
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate != null) {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate);
                            } else {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate = null;
                            }
                            if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate != undefined &&
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate != null) {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate);
                            } else {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate = null;
                            }
                            if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate != undefined &&
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate != null) {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate = $rootScope.sendApiStartDateTime(shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate);
                            } else {
                                shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate = null;
                            }
                        }
                    }

                    if (shipmentObj.shipmentServiceList[i].shipmentChargeList != undefined && shipmentObj.shipmentServiceList[i].shipmentChargeList != null && shipmentObj.shipmentServiceList[i].shipmentChargeList.length != 0) {
                        for (var j = 0; j < shipmentObj.shipmentServiceList[i].shipmentChargeList.length; j++) {
                            if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].location) {
                                shipmentObj.shipmentServiceList[i].shipmentChargeList[j].location = {
                                    "location": {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].location.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].location.id
                                    }
                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].company) {
                                shipmentObj.shipmentServiceList[i].shipmentChargeList[j].company = {
                                    "company": {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].company.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].company.id
                                    },
                                }
                            }
                            if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].country) {
                                shipmentObj.shipmentServiceList[i].shipmentChargeList[j].country = {
                                    "country": {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].country.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].country.id
                                    },
                                }
                            }

                            if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j]) {
                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].unitMaster) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].unitMaster = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].unitMaster.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].unitMaster.id
                                    }
                                }
                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].grossSaleCurrency) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].grossSaleCurrency = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].grossSaleCurrency.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].grossSaleCurrency.id
                                    }
                                }

                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].netSaleCurrency) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].netSaleCurrency = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].netSaleCurrency.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].netSaleCurrency.id
                                    }
                                }

                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].declaredSaleCurrency) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].declaredSaleCurrency = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].declaredSaleCurrency.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].declaredSaleCurrency.id
                                    }
                                }
                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].rateCurrency) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].rateCurrency = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].rateCurrency.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].rateCurrency.id
                                    }
                                }

                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].costCurrency) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].costCurrency = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].costCurrency.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].costCurrency.id
                                    }
                                }

                                if (shipmentObj.shipmentServiceList[i].shipmentChargeList[j].localCurrency) {
                                    shipmentObj.shipmentServiceList[i].shipmentChargeList[j].localCurrency = {
                                        "versionLock": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].localCurrency.versionLock,
                                        "id": shipmentObj.shipmentServiceList[i].shipmentChargeList[j].localCurrency.id
                                    }
                                }
                            }

                        }
                    }

                }
            }
            return shipmentObj;
        }


        // To assign the primary address of the party once party is selected.
        this.getPrimaryAddress = function(object, addressKey) {

            if (addressKey == 'shipper') {
                if (object.shipperAddress == undefined || object.shipperAddress == null) {
                    object.shipperAddress = {};
                }
                if (object.shipper == undefined || object.shipper == null) {
                    object.shipperAddress = {};
                }

                this.assignPrimaryAddress(object.shipper, object.shipperAddress);

            } else if (addressKey == 'forwarder') {
                if (object.forwarderAddress == undefined || object.forwarderAddress == null) {
                    object.forwarderAddress = {};
                }

                if (object.forwarder == undefined || object.forwarder == null) {
                    object.forwarderAddress = {};
                }
                this.assignPrimaryAddress(object.forwarder, object.forwarderAddress);
            } else if (addressKey == 'consignee') {
                if (object.consigneeAddress == undefined || object.consigneeAddress == null) {
                    object.consigneeAddress = {};
                }

                if (object.consignee == undefined || object.consignee == null) {
                    object.consigneeAddress = {};
                }
                this.assignPrimaryAddress(object.consignee, object.consigneeAddress);
            } else if (addressKey == 'firstNotify') {
                if (object.firstNotifyAddress == undefined || object.firstNotifyAddress == null) {
                    object.firstNotifyAddress = {};
                }

                if (object.firstNotify == undefined || object.firstNotify == null) {
                    object.firstNotifyAddress = {};
                }
                this.assignPrimaryAddress(object.firstNotify, object.firstNotifyAddress);
            } else if (addressKey == 'secondNotify') {
                if (object.secondNotifyAddress == undefined || object.secondNotifyAddress == null) {
                    object.secondNotifyAddress = {};
                }

                if (object.secondNotify == undefined || object.secondNotify == null) {
                    object.secondNotifyAddress = {};
                }
                this.assignPrimaryAddress(object.secondNotify, object.secondNotifyAddress);
            } else if (addressKey == 'agent') {
                if (object.agentAddress == undefined || object.agentAddress == null) {
                    object.agentAddress = {};
                }

                if (object.agent == undefined || object.agent == null) {
                    object.agentAddress = {};
                }
                this.assignPrimaryAddress(object.agent, object.agentAddress);
            } else if (addressKey == 'issuingAgent') {
                if (object.issuingAgentAddress == undefined || object.issuingAgentAddress == null) {
                    object.issuingAgentAddress = {};
                }
                if (object.issuingAgent == undefined || object.issuingAgent == null) {
                    object.issuingAgentAddress = {};
                }
                this.assignPrimaryAddress(object.issuingAgent, object.issuingAgentAddress);
            } else if (addressKey == 'chaAgent') {
                if (object.chaAgentAddress == undefined || object.chaAgentAddress == null) {
                    object.chaAgentAddress = {};
                }
                if (object.chaAgent == undefined || object.chaAgent == null) {
                    object.chaAgentAddress = {};
                }
                this.assignPrimaryAddress(object.chaAgent, object.chaAgentAddress);
            }

        }


        this.chkAndClear = function(shipment, forObject, serviceIndex, docIndex) {

            switch (forObject) {
                case "documentDetail.shipper":
                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipper != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipper.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipper = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipperAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipperAddress = this.clearAddress();
                        }
                    }
                    break;

                case "documentDetail.consignee":
                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consignee != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consignee.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consignee = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consigneeAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consigneeAddress = this.clearAddress();
                        }
                    }
                    break;

                case "documentDetail.firstNotify":
                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotify != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotify.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotify = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotifyAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotifyAddress = this.clearAddress();
                        }
                    }
                    break;

                case "documentDetail.secondNotify":
                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotify != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotify.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotify = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotifyAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotifyAddress = this.clearAddress();
                        }
                    }
                    break;

                case "documentDetail.forwarder":
                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarder != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarder.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarder = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarderAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarderAddress = this.clearAddress();
                        }
                    }
                    break;

                case "documentDetail.agent":

                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agent != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agent.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agent = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agentAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agentAddress = this.clearAddress();
                        }
                    }
                    break;
                case "documentDetail.issuingAgent":

                    if (serviceIndex > -1 && docIndex > -1) {
                        if (shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgent != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgent.id != null) {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgent = null;
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgentAddress = this.clearAddress();
                        } else {
                            shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgentAddress = this.clearAddress();
                        }
                    }
                    break;
                default:

            }
        }

        this.clearAddress = function() {
            address = {};
            return address;
        }


        //for chargeble weight calculation

        /*	this.chargableWeightCalulcate = function (service, whichValue){
            	
                if(service.bookedVolumeWeightUnitPound!=undefined && service.bookedVolumeWeightUnitPound!=null && whichValue=='volumePound'){
                    try{
                        var weight = $rootScope.roundOffWeight(((parseFloat(service.bookedVolumeWeightUnitPound) * 0.453592) * 100)/100);
                        service.bookedVolumeWeightUnitKg = weight;
                    	
                    }catch(err){
                        service.bookedVolumeWeightUnitPound = 0.0;
                        service.bookedVolumeWeightUnitKg = 0.0;
                    }
                }
            	
                if(service.bookedGrossWeightUnitPound!=undefined && service.bookedGrossWeightUnitPound!=null && whichValue=='grossPound'){
                    try{
                        var weight = $rootScope.roundOffWeight(((parseFloat(service.bookedGrossWeightUnitPound) * 0.453592) * 100)/100);
                        service.bookedGrossWeightUnitKg = weight;
                    }catch(err){
                        service.bookedGrossWeightUnitPound = 0.0;
                        service.bookedGrossWeightUnitKg = 0.0;
                    }
                }
            	
            	
                if(service.bookedVolumeWeightUnitKg!=undefined && service.bookedVolumeWeightUnitKg!=null && whichValue=='volumekg'){
                    try{
                        var weight = Math.round((parseFloat(service.bookedVolumeWeightUnitKg) * 2.20462) * 100) / 100;
                        service.bookedVolumeWeightUnitPound = weight;
                    }catch(err){
                        service.bookedVolumeWeightUnitKg = 0.0;
                        service.bookedVolumeWeightUnitPound = 0.0;
                    }
                }
            	
                if(service.bookedGrossWeightUnitKg!=undefined && service.bookedGrossWeightUnitKg!=null && whichValue=='grossKg'){
                    try{
                    	
                        var weight = Math.round((parseFloat(service.bookedGrossWeightUnitKg) * 2.20462) * 100) / 100;
                        service.bookedGrossWeightUnitPound = weight;
                    }catch(err){
                        service.bookedGrossWeightUnitKg = 0.0;
                        service.bookedGrossWeightUnitPound = 0.0;
                    }
                }
            	
        	
            	
                this.calculateChargeWeight(service);
            }*/

        /*	this.calculateChargeWeight = function(service){
                var volumeKg = parseFloat(service.bookedVolumeWeightUnitKg);
                var grossKg = parseFloat(service.bookedGrossWeightUnitKg);
            	
                if(volumeKg>0 && grossKg>0){
                	
                    if(volumeKg>grossKg){
                        service.bookedChargeableUnit = volumeKg;
                    }else{
                        service.bookedChargeableUnit = grossKg;
                    }
                	
                }else if(volumeKg<=0 && grossKg>0){
                    service.bookedChargeableUnit = grossKg;
                }else if(volumeKg>0 && grossKg<=0){
                    service.bookedChargeableUnit = volumeKg;
                }else{
                    service.bookedChargeableUnit = 0.0;
                }
            	
            	
                if(service.bookedChargeableUnit>0){
                    var conversionUnit = null;
                	
                    if(service.serviceMaster.transportMode==='Air'){
                        conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
                    }else if(service.serviceMaster.transportMode==='Road'){
                        conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
                    }else if(service.serviceMaster.transportMode==='Ocean'){
                        conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
                    }
                	
                    if(conversionUnit!=null){
                        service.bookedVolumeUnitCbm = parseFloat(service.bookedChargeableUnit)/parseFloat(conversionUnit);
        //				service.bookedVolumeUnitCbm = $rootScope.weightFormat(service.bookedVolumeUnitCbm,service.location.countryMaster.locale);
                        service.bookedVolumeUnitCbm  = $rootScope.roundOffWeight(service.bookedVolumeUnitCbm);
                    }
                }else{
                    service.bookedVolumeUnitCbm = 0.0;
                }
            	
            }*/

        this.assignedDocumentToService = function(service, whichField) {

            if (whichField === "packMaster" || whichField === "all") {
                service.packMaster = service.documentList[0].packMaster;
            }

            if (whichField === "weightCalculate" || whichField === "all") {
                service.bookedPieces = service.documentList[0].noOfPieces;
                service.dimensionUnit = service.documentList[0].dimensionUnit;
                service.dimensionUnitValue = service.documentList[0].dimensionUnitValue;
                service.bookedGrossWeightUnitKg = service.documentList[0].grossWeight;
                service.bookedVolumeWeightUnitKg = service.documentList[0].volumeWeight;
                service.bookedChargeableUnit = service.documentList[0].chargebleWeight;
                service.bookedGrossWeightUnitPound = service.documentList[0].grossWeightInPound;
                service.bookedVolumeWeightUnitPound = service.documentList[0].volumeWeightInPound;
                service.bookedVolumeUnitCbm = service.documentList[0].bookedVolumeUnitCbm;
            }

            if (whichField === "carrier" || whichField === "all") {
                service.documentList[0].carrier = service.carrier;
            }

            if (whichField === "etd" || whichField === "all") {
                service.documentList[0].etd = service.etd;
            }
            if (whichField === "eta" || whichField === "all") {
                service.documentList[0].eta = service.eta;
            }
            if (whichField === "mawbNo" || whichField === "all") {
                service.documentList[0].mawbNo = service.mawbNo;
            }
            if (whichField === "routeNo" || whichField === "all") {
                service.documentList[0].routeNo = service.routeNo;
            }
            if (whichField === "origin" || whichField === "all") {
                service.documentList[0].origin = service.origin;
            }
            if (whichField === "pol" || whichField === "all") {
                service.documentList[0].pol = service.pol;
            }
            if (whichField === "pod" || whichField === "all") {
                service.documentList[0].pod = service.pod;
            }
            if (whichField === "destination" || whichField === "all") {
                service.documentList[0].destination = service.destination;
            }

        }

        this.populateHAWB = function(shipmentServiceDetail, isFromConsolForImport, documentDetail) {

            if (documentDetail.shipper == undefined || documentDetail.shipper == null ||
                documentDetail.shipper.id == undefined || documentDetail.shipper.id == null) {
                if (shipmentServiceDetail.party != undefined && shipmentServiceDetail.party.id != null) {
                    if (shipmentServiceDetail.serviceMaster != undefined && shipmentServiceDetail.serviceMaster != null &&
                        shipmentServiceDetail.serviceMaster.id != null && shipmentServiceDetail.serviceMaster.importExport == 'Export') {
                        documentDetail.shipper = shipmentServiceDetail.party;
                        that.assignPrimaryAddress(documentDetail.shipper, documentDetail.shipperAddress);
                    }
                }
            }

            if (documentDetail.consignee == undefined || documentDetail.consignee == null ||
                documentDetail.consignee.id == undefined || documentDetail.consignee.id == null) {
                if (shipmentServiceDetail.party != undefined && shipmentServiceDetail.party.id != null) {
                    if (shipmentServiceDetail.serviceMaster != undefined && shipmentServiceDetail.serviceMaster != null &&
                        shipmentServiceDetail.serviceMaster.id != null && shipmentServiceDetail.serviceMaster.importExport == 'Import') {
                        documentDetail.consignee = shipmentServiceDetail.party;
                        that.assignPrimaryAddress(documentDetail.consignee, documentDetail.consigneeAddress);
                    }
                }
            }

            if (documentDetail.agent == undefined || documentDetail.agent == null ||
                documentDetail.agent.id == undefined || documentDetail.agent.id == null) {
                var serviceMasterId = null;
                var portMasterId = null;

                if (shipmentServiceDetail.serviceMaster.transportMode === 'Air' && shipmentServiceDetail.serviceMaster.importExport === 'Import' &&
                    (shipmentServiceDetail.serviceMaster.serviceType === undefined || shipmentServiceDetail.serviceMaster.serviceType === null)) {
                    serviceMasterId = shipmentServiceDetail.serviceMaster.id;
                    portMasterId = shipmentServiceDetail.origin.id;
                } else {
                    serviceMasterId = shipmentServiceDetail.serviceMaster.id;
                    portMasterId = shipmentServiceDetail.destination.id;
                }

                AgentByServicePort.fetch({
                    "serviceId": serviceMasterId,
                    "portId": portMasterId
                }).$promise.then(function(data) {

                    if (data.responseCode == "ERR0") {

                        documentDetail.alreadySetted = true;
                        var agentExport = data.responseObject;
                        if (agentExport != undefined && agentExport != null && agentExport.id != undefined) {
                            documentDetail.agent = agentExport.agent;
                            documentDetail.agentAddress = {};
                            that.assignPrimaryAddress(documentDetail.agent, documentDetail.agentAddress);
                        } else {
                            if (shipmentServiceDetail.whoRouted == true && (documentDetail.agent == undefined || documentDetail.agent == null || documentDetail.agent.id == null)) {
                                documentDetail.agent = shipmentServiceDetail.agent;
                                documentDetail.agentAddress = {};
                                that.assignPrimaryAddress(documentDetail.agent, documentDetail.agentAddress);
                            }
                        }


                    }
                }, function(errResponse) {
                    console.error('Error while fetching agent  based on service port');
                });
            }


        }





        this.saveToHistory = function(shipment, state, stateParams) {

            $rootScope.category = "Shipment";
            $rootScope.serviceCodeForUnHistory = "";
            $rootScope.orginAndDestinationUnHistory = "";

            if (shipment != undefined && shipment != null) {
                if (shipment.party != null && shipment.party.partyName != undefined && shipment.party.partyName != null) {
                    $rootScope.subTitle = shipment.party.partyName;
                } else {
                    $rootScope.subTitle = "Unkonwn Party";
                }

                if (shipment.origin != undefined && shipment.origin != null) {
                    $rootScope.serviceCodeForUnHistory = shipment.origin.portGroupCode;
                }

                if (shipment.destination != undefined && shipment.destination != null) {
                    $rootScope.orginAndDestinationUnHistory = shipment.destination.portGroupCode;
                }

            } else {

                $rootScope.subTitle = "Unknown Party";
                $rootScope.serviceCodeForUnHistory = "";
                $rootScope.orginAndDestinationUnHistory = "";

            }
            var shipment_id = shipment ? shipment.shipmentUid : null;
            var rHistoryObj = {
                'title': 'Shipment Edit # ' + shipment_id,
                'subTitle': $rootScope.subTitle,
                'stateName': state.current.name,
                'stateParam': JSON.stringify(stateParams),
                'stateCategory': 'Shipment',
                'serviceType': 'AIR',
                'serviceCode': $rootScope.serviceCodeForUnHistory,
                'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                'showService': true
            }

            RecentHistorySaveService.form(rHistoryObj);
            console.log("History Saved.");
        }

        this.editInitialization = function(shipmentObj) {

            for (var i = 0; i < shipmentObj.shipmentServiceList.length; i++) {

                shipmentObj.shipmentServiceList[i].serviceReqDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].serviceReqDate);
                shipmentObj.shipmentServiceList[i].eta = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].eta);
                shipmentObj.shipmentServiceList[i].etd = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].etd);

                if (shipmentObj.shipmentServiceList[i].documentList != undefined && shipmentObj.shipmentServiceList[i].documentList != null) {
                    for (var j = 0; j < shipmentObj.shipmentServiceList[i].documentList.length; j++) {
                        if (shipmentObj.shipmentServiceList[i].documentList[j].dimensionList == undefined || shipmentObj.shipmentServiceList[i].documentList[j].dimensionList == null ||
                            shipmentObj.shipmentServiceList[i].documentList[j].dimensionList.length == 0) {
                            shipmentObj.shipmentServiceList[i].documentList[j].dimensionList.push({});
                        }

                        shipmentObj.shipmentServiceList[i].documentList[j].documentReqDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].documentList[j].documentReqDate);
                        shipmentObj.shipmentServiceList[i].documentList[j].etd = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].documentList[j].etd);
                        shipmentObj.shipmentServiceList[i].documentList[j].eta = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].documentList[j].eta);

                        shipmentObj.shipmentServiceList[i].documentList[j].canIssuedDate = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].documentList[j].canIssuedDate);
                        shipmentObj.shipmentServiceList[i].documentList[j].doIssuedDate = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].documentList[j].doIssuedDate);
                        shipmentObj.shipmentServiceList[i].documentList[j].hawbReceivedOn = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].documentList[j].hawbReceivedOn);
                    }
                }

                if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != undefined && shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != null) {
                    if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter != undefined &&
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter != null &&
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter.id != undefined &&
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter.id != null) {

                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName = null;
                    } else if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName != undefined &&
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName != null &&
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName.trim().length > 0) {
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter = {};
                        shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporter.partyName = shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.transporterName;
                    }

                }


                if (shipmentObj.shipmentServiceList[i].connectionList == undefined || shipmentObj.shipmentServiceList[i].connectionList == null || shipmentObj.shipmentServiceList[i].connectionList.length == 0) {
                    shipmentObj.shipmentServiceList[i].connectionList = [{
                        connectionStatus: 'Planned'
                    }];
                } else {
                    for (var j = 0; j < shipmentObj.shipmentServiceList[i].connectionList.length; j++) {
                        shipmentObj.shipmentServiceList[i].connectionList[j].etd = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].connectionList[j].etd);
                        shipmentObj.shipmentServiceList[i].connectionList[j].eta = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].connectionList[j].eta);
                    }
                }

                if (shipmentObj.shipmentServiceList[i].shipmentAttachmentList == undefined || shipmentObj.shipmentServiceList[i].shipmentAttachmentList == null || shipmentObj.shipmentServiceList[i].shipmentAttachmentList.length == 0) {
                    shipmentObj.shipmentServiceList[i].shipmentAttachmentList = [];
                }

                if (shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != undefined && shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint != null) {
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp);
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned);
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual);
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected);
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual);
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected);
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual = $rootScope.dateAndTimeToString(shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual);

                } else {
                    shipmentObj.shipmentServiceList[i].pickUpDeliveryPoint = {
                        isOurPickUp: false
                    };
                }

                if (shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList == undefined || shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList == null || shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList.length == 0) {
                    shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList = [];
                } else {
                    for (var k = 0; k < shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList.length; k++) {
                        shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[k].date = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[k].date);
                        shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[k].followUpDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].shipmentServiceTriggerList[k].followUpDate);
                    }
                }

                if (shipmentObj.shipmentServiceList[i].eventList == undefined || shipmentObj.shipmentServiceList[i].eventList == null ||
                    shipmentObj.shipmentServiceList[i].eventList.length == 0) {
                    shipmentObj.shipmentServiceList[i].eventList = [];
                } else {
                    for (var j = 0; j < shipmentObj.shipmentServiceList[i].eventList.length; j++) {
                        shipmentObj.shipmentServiceList[i].eventList[j].eventDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].eventList[j].eventDate);
                        shipmentObj.shipmentServiceList[i].eventList[j].followUpDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].eventList[j].followUpDate);
                    }
                }


                if (shipmentObj.shipmentServiceList[i].billOfEntryList == undefined || shipmentObj.shipmentServiceList[i].billOfEntryList == null ||
                    shipmentObj.shipmentServiceList[i].billOfEntryList.length == 0) {
                    shipmentObj.shipmentServiceList[i].billOfEntryList = [];
                } else {
                    for (var j = 0; j < shipmentObj.shipmentServiceList[i].billOfEntryList.length; j++) {
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate);
                        }
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate);
                        }
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate);
                        }
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate);
                        }
                    }
                }

                if (shipmentObj.shipmentServiceList[i].referenceList == undefined || shipmentObj.shipmentServiceList[i].referenceList == null || shipmentObj.shipmentServiceList[i].referenceList.length == 0) {
                    shipmentObj.shipmentServiceList[i].referenceList = [];
                }

                if (shipmentObj.shipmentServiceList[i].billOfEntryList == undefined || shipmentObj.shipmentServiceList[i].billOfEntryList == null || shipmentObj.shipmentServiceList[i].billOfEntryList.length == 0) {
                    shipmentObj.shipmentServiceList[i].billOfEntryList = [];
                }

                if (shipmentObj.shipmentServiceList[i].authenticatedDocList == undefined || shipmentObj.shipmentServiceList[i].authenticatedDocList == null || shipmentObj.shipmentServiceList[i].authenticatedDocList.length == 0) {
                    shipmentObj.shipmentServiceList[i].authenticatedDocList = [{}];
                } else {
                    for (var k = 0; k < shipmentObj.shipmentServiceList[i].authenticatedDocList.length; k++) {
                        shipmentObj.shipmentServiceList[i].authenticatedDocList[k].date = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].authenticatedDocList[k].date);
                    }
                }

                shipmentObj.shipmentServiceList[i].attachConfig = {
                    "isEdit": true,
                    "isDelete": true,
                    "page": "shipment",
                    "columnDefs": [{
                            "name": "Document Name",
                            "model": "documentMaster.documentName",
                            "type": "text"
                        },
                        {
                            "name": "Reference #",
                            "model": "refNo",
                            "type": "text"
                        },
                        {
                            "name": "Customs",
                            "model": "customs",
                            "type": "text"
                        },
                        {
                            "name": "Unprotected File Name",
                            "model": "unprotectedFileName",
                            "type": "file"
                        },
                        {
                            "name": "Protected File Name",
                            "model": "protectedFileName",
                            "type": "file"
                        },
                        {
                            "name": "Actions",
                            "model": "action",
                            "type": "action"
                        }
                    ]
                }
            }

        }

        // Checking Mandatory in Reference Tab if any Reference type master is marked as mandatory.
        this.checkMandatoryReference = function(shipment, service, key) {

            try {

                MandatoryReferenceTypeByKey.get({
                    key: 'Shipment'
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        var listOfMandatoryReferences = data.responseObject;
                        console.log("Successful while getting Mandatory ReferenceTypeMasters ", listOfMandatoryReferences);

                        if (listOfMandatoryReferences != null && listOfMandatoryReferences.length > 0) {
                            console.log("Number of Mandatory References " + listOfMandatoryReferences.length);
                            var flag = true;
                            for (var i = 0; i < listOfMandatoryReferences.length; i++) {
                                var found = false;
                                if (service.referenceList != null && service.referenceList.length > 0) {
                                    for (var j = 0; j < service.referenceList.length; j++) {
                                        if (service.referenceList[j].referenceTypeMaster != null && listOfMandatoryReferences[i].id == service.referenceList[j].referenceTypeMaster.id) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if (!found) {
                                    flag = false;
                                    Notification.error($rootScope.nls["ERR90525"].replace("%s", listOfMandatoryReferences[i].referenceTypeName));
                                    break;
                                }
                            }

                            if (flag) {
                                that.consolContinueAfterReference(shipment, service, key);
                            }
                        } else {
                            console.log("Number of Mandatory References is 0");
                            that.consolContinueAfterReference(shipment, service, key);
                        }
                    }
                }, function(error) {
                    console.log("Error while getting Mandatory ReferenceTypeMasters ", error)
                    that.consolContinueAfterReference(shipment, service, key);
                });
            } catch (e) {
                console.log("exception in api" + e);
                that.consolContinueAfterReference(shipment, service, key);
            }

        }


        this.serviceToConsol = function(shipment, service) {

            if (service.holdShipment || service.holdShipment == 'Yes') {
                $rootScope.clientMessage = $rootScope.nls["ERR90614"];
                return false;
            }


            if (service.party.isDefaulter) {
                $rootScope.clientMessage = $rootScope.nls["ERR90584"];
                return false;
            }

            if (service.party.isDefaulter) {
                $rootScope.clientMessage = $rootScope.nls["ERR90584"];
                return false;
            }

            if (service.brokerageParty != undefined && service.brokerageParty != null &&
                service.brokerageParty.id != null && service.brokerageParty.isDefaulter) {
                $rootScope.clientMessage = $rootScope.nls["ERR90585"];
                return false;
            }

            if (service.pickUpDeliveryPoint != undefined &&
                service.pickUpDeliveryPoint != null &&
                service.pickUpDeliveryPoint.id != null &&
                service.pickUpDeliveryPoint.transporter != undefined &&
                service.pickUpDeliveryPoint.transporter != null &&
                service.pickUpDeliveryPoint.transporter.id != null &&
                service.pickUpDeliveryPoint.transporter.isDefaulter) {
                $rootScope.clientMessage = $rootScope.nls["ERR90593"];
                return false;
            }

            if (service.pickUpDeliveryPoint != undefined &&
                service.pickUpDeliveryPoint != null &&
                service.pickUpDeliveryPoint.id != null &&
                service.pickUpDeliveryPoint.pickupPoint != undefined &&
                service.pickUpDeliveryPoint.pickupPoint != null &&
                service.pickUpDeliveryPoint.pickupPoint.id != null &&
                service.pickUpDeliveryPoint.pickupPoint.isDefaulter) {
                $rootScope.clientMessage = $rootScope.nls["ERR90594"];
                return false;
            }

            for (var i = 0; i < service.documentList.length; i++) {
                if (service.documentList[i].shipper != undefined && service.documentList[i].shipper != null && service.documentList[i].shipper.id != null && service.documentList[i].shipper.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90586"];
                    return false;
                }
                if (service.documentList[i].forwarder != undefined && service.documentList[i].forwarder != null && service.documentList[i].forwarder.id != null && service.documentList[i].forwarder.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90591"];
                    return false;
                }
                if (service.documentList[i].consignee != undefined && service.documentList[i].consignee != null && service.documentList[i].consignee.id != null && service.documentList[i].consignee.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90587"];
                    return false;
                }
                if (service.documentList[i].firstNotify != undefined && service.documentList[i].firstNotify != null && service.documentList[i].firstNotify.id != null && service.documentList[i].firstNotify.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90588"];
                    return false;
                }
                if (service.documentList[i].secondNotify != undefined && service.documentList[i].secondNotify != null && service.documentList[i].secondNotify.id != null && service.documentList[i].secondNotify.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90589"];
                    return false;
                }
                if (service.documentList[i].agent != undefined && service.documentList[i].agent != null && service.documentList[i].agent.id != null && service.documentList[i].agent.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90590"];
                    return false;
                }
                if (service.documentList[i].issuingAgent != undefined && service.documentList[i].issuingAgent != null && service.documentList[i].issuingAgent.id != null && service.documentList[i].issuingAgent.isDefaulter) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90592"];
                    return false;
                }

            }

            if (service.serviceMaster.importExport == 'Export') {
                if (!$rootScope.isGoodsReceived(service)) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90568"];
                    return false;
                }
            }


            if (service.mawbNo == undefined || service.mawbNo == null || service.mawbNo == '') {
                $rootScope.clientMessage = $rootScope.nls["ERR90569"];
                return false;

            } else {

                if (service.carrier == undefined || service.carrier == null || service.carrier.id == null) {
                    $rootScope.clientMessage = $rootScope.nls["ERR90063"];
                    return false;
                }
                var check = $rootScope.validateMawb(service.carrier, service.mawbNo);
                if (check != "S") {
                    if (check != "F") {
                        $rootScope.clientMessage = $rootScope.nls["ERR90216"] + "(" + check + ")";
                    } else {
                        $rootScope.clientMessage = $rootScope.nls["ERR90068"];
                    }

                    return false;
                }
            }

            if (service.etd == undefined || service.etd == null || service.etd == '') {
                $rootScope.clientMessage = $rootScope.nls["ERR90094"];
                return false;
            }

            if (service.eta == undefined || service.eta == null || service.eta == '') {
                $rootScope.clientMessage = $rootScope.nls["ERR90095"];
                return false;
            }

            return true;

        }




        this.goToConsol = function(shipment, service, key) {

            this.checkMandatoryReference(shipment, service, key);

        }

        this.consolContinueAfterReference = function(shipment, service, key) {
            console.log("goto Consol");
            var consol = {};
            consol.createdBy = $rootScope.userProfile.employee;
            consol.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
            consol.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
            consol.location = $rootScope.userProfile.selectedUserLocation;
            consol.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            consol.masterDate = $rootScope.dateToString(new Date());
            consol.directShipment = service.directShipment;
            consol.consolDocument = {};
            consol.consolDocument.mawbNo = service.mawbNo;
            consol.consolDocument.mawbGeneratedDate = $rootScope.dateAndTimeToString(service.mawbDate);
            consol.serviceMaster = service.serviceMaster;

            if (service.serviceMaster.importExport == 'Import') {
                consol.whoRouted = service.whoRouted == 'Self' ? false : true;
                if (consol.whoRouted) {
                    consol.routedAgent = service.agent;
                    consol.routedSalesman = null;
                } else {
                    consol.routedSalesman = service.salesman;
                    consol.routedAgent = null;
                }
            } else {
                consol.whoRouted = null;
                consol.routedAgent = null;
                consol.routedSalesman = null;
                consol.routedSalesman = null;
                consol.routedAgent = null;
            }

            if (service.shipmentChargeList != undefined && service.shipmentChargeList != null && service.shipmentChargeList.length != 0) {

                if (service.shipmentChargeList.length == 1) {

                    if (service.shipmentChargeList[0].chargeMaster == undefined || service.shipmentChargeList[0].chargeMaster == null || service.shipmentChargeList[0].chargeMaster.id == undefined) {

                        service.shipmentChargeList = null;
                    }
                }
            } else {
                service.shipmentChargeList = null;
            }

            consol.ppcc = service.ppcc;
            consol.jobStatus = 'Active';
            consol.isJobCompleted = false;
            consol.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            consol.currency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            consol.currencyRate = $rootScope.baseCurrenyRate[consol.currency.id] == undefined || $rootScope.baseCurrenyRate[consol.currency.id] == null ? 1 : $rootScope.baseCurrenyRate[consol.currency.id].csell;
            consol.origin = service.origin;
            consol.pol = service.pol;
            consol.pod = service.pod;
            consol.destination = service.destination;
            //consol.currency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            //consol.currencyRate = $rootScope.baseCurrenyRate[consol.currency.id].csell;
            consol.consolDocument.noOfPieces = service.bookedPieces;
            //consol.volume = 
            consol.consolDocument.grossWeight = service.bookedGrossWeightUnitKg;
            consol.consolDocument.volumeWeight = service.bookedVolumeWeightUnitKg;
            consol.consolDocument.chargebleWeight = service.bookedChargeableUnit;
            consol.consolDocument.grossWeightInPound = service.bookedGrossWeightUnitPound;
            consol.consolDocument.volumeWeightInPound = service.bookedVolumeWeightUnitPound;
            if (service.bookedVolumeUnitCbm != null) {
                consol.consolDocument.volume = service.bookedVolumeUnitCbm;
            }
            consol.carrier = service.carrier;
            consol.etd = $rootScope.dateAndTimeToString(service.etd);
            consol.eta = $rootScope.dateAndTimeToString(service.eta);
            consol.consolDocument.routeNo = service.routeNo;

            consol.consolDocument.isAgreed = (service.isAgreed == 'Yes' || service.isAgreed) ? true : false;
            consol.consolDocument.isMarksNo = (service.isMarksNo == 'Yes' || service.isMarksNo) ? true : false;

            consol.shipmentLinkList = [];

            var shipmentLink = {};
            shipmentLink.company = service.company;
            shipmentLink.country = service.country;
            shipmentLink.location = service.location;
            shipmentLink.shipmentUid = service.shipmentUid;
            shipmentLink.service = service;
            shipmentLink.service.isAgreed = (shipmentLink.service.isAgreed || shipmentLink.service.isAgreed == 'Yes') ? true : false;
            shipmentLink.service.isMarksNo = (shipmentLink.service.isMarksNo || shipmentLink.service.isMarksNo == 'Yes') ? true : false;
            shipmentLink.serviceUid = service.serviceUid;

            consol.shipmentLinkList.push(shipmentLink);

            consol.connectionList = [];

            for (var i = 0; i < service.connectionList.length; i++) {
                var consolConnection = {};
                consolConnection.move = service.connectionList[i].move;
                consolConnection.etd = $rootScope.dateAndTimeToString(service.connectionList[i].etd);
                consolConnection.eta = $rootScope.dateAndTimeToString(service.connectionList[i].eta);
                consolConnection.pol = service.connectionList[i].pol;
                consolConnection.pod = service.connectionList[i].pod;
                consolConnection.carrierMaster = service.connectionList[i].carrierMaster;
                consolConnection.flightVoyageNo = service.connectionList[i].flightVoyageNo;
                consolConnection.connectionStatus = service.connectionList[i].connectionStatus;

                consol.connectionList.push(consolConnection);

            }

            if (consol.connectionList == undefined || consol.connectionList == null || consol.connectionList.length == 0) {
                consol.connectionList.push({
                    connectionStatus: 'Planned'
                });
            }



            consol.eventList = [];
            for (var i = 0; i < service.eventList.length; i++) {
                var event = {};
                event.id = -1; //which is for disabling the event in consol add screen.
                event.eventMaster = service.eventList[i].eventMaster;
                event.eventDate = $rootScope.dateToString(service.eventList[i].eventDate);
                event.followUpRequired = service.eventList[i].followUpRequired;
                event.assignedTo = service.eventList[i].assignedTo;
                event.followUpDate = $rootScope.dateToString(service.eventList[i].followUpDate);
                event.isCompleted = service.eventList[i].isCompleted;
                consol.eventList.push(event);
            }

            /*if(consol.eventList==undefined || consol.eventList==null || consol.eventList.length==0){
                consol.eventList.push({isCompleted : 'Yes',followUpRequired : 'No'});
            }*/

            consol.pickUpDeliveryPoint = {};

            /* As discussed with suresh (NES-2167) removed the pickup delivery information
             * 
        	
            if(service.pickUpDeliveryPoint!=undefined && service.pickUpDeliveryPoint!=null){
                consol.pickUpDeliveryPoint.transporter = service.pickUpDeliveryPoint.transporter;
            	
                consol.pickUpDeliveryPoint.transporterAddress = service.pickUpDeliveryPoint.transporterAddress;
                if(service.pickUpDeliveryPoint.transporterAddress!=undefined && service.pickUpDeliveryPoint.transporterAddress!=null && service.pickUpDeliveryPoint.transporterAddress!=""){
                    consol.pickUpDeliveryPoint.transporterAddress.id = null;
                    consol.pickUpDeliveryPoint.transporterAddress.versionLock =  0;
                }else{
                    service.pickUpDeliveryPoint.transporterAddress=null;
                }
            	
                consol.pickUpDeliveryPoint.isOurPickUp = service.pickUpDeliveryPoint.isOurPickUp; 
                consol.pickUpDeliveryPoint.pickUpClientRefNo = service.pickUpDeliveryPoint.pickUpClientRefNo;
                consol.pickUpDeliveryPoint.pickupPoint = service.pickUpDeliveryPoint.pickupPoint;
                consol.pickUpDeliveryPoint.pickupFrom = service.pickUpDeliveryPoint.pickupFrom;
                consol.pickUpDeliveryPoint.pickUpPlace = service.pickUpDeliveryPoint.pickUpPlace;
            	
                if(service.pickUpDeliveryPoint.pickUpPlace!=undefined && service.pickUpDeliveryPoint.pickUpPlace!=null && service.pickUpDeliveryPoint.pickUpPlace!=""){
                    consol.pickUpDeliveryPoint.pickUpPlace.id = null;
                    consol.pickUpDeliveryPoint.pickUpPlace.versionLock =  0;
                }
            	
                consol.pickUpDeliveryPoint.pickUpContactPerson = service.pickUpDeliveryPoint.pickUpContactPerson;
                consol.pickUpDeliveryPoint.pickUpMobileNo = service.pickUpDeliveryPoint.pickUpMobileNo;
                consol.pickUpDeliveryPoint.pickUpPhoneNo = service.pickUpDeliveryPoint.pickUpPhoneNo;
                consol.pickUpDeliveryPoint.pickUpEmail = service.pickUpDeliveryPoint.pickUpEmail;
                consol.pickUpDeliveryPoint.pickUpFax = service.pickUpDeliveryPoint.pickUpFax;
                consol.pickUpDeliveryPoint.pickUpFollowUp =$rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.pickUpFollowUp);
                consol.pickUpDeliveryPoint.pickUpPlanned =$rootScope.dateAndTimeToString( service.pickUpDeliveryPoint.pickUpPlanned);
                consol.pickUpDeliveryPoint.pickUpActual = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.pickUpActual);
            	
                consol.pickUpDeliveryPoint.deliveryPoint = service.pickUpDeliveryPoint.deliveryPoint;
                consol.pickUpDeliveryPoint.deliveryFrom = service.pickUpDeliveryPoint.deliveryFrom;
                consol.pickUpDeliveryPoint.deliveryPlace = service.pickUpDeliveryPoint.deliveryPlace;
            	
            	
                if(service.pickUpDeliveryPoint.deliveryPlace!=undefined && service.pickUpDeliveryPoint.deliveryPlace!=null &&  service.pickUpDeliveryPoint.deliveryPlace!=""){
                    consol.pickUpDeliveryPoint.deliveryPlace.id = null;
                    consol.pickUpDeliveryPoint.deliveryPlace.versionLock =  0;
                }
            	
                consol.pickUpDeliveryPoint.deliveryContactPerson = service.pickUpDeliveryPoint.deliveryContactPerson;
                consol.pickUpDeliveryPoint.deliveryMobileNo = service.pickUpDeliveryPoint.deliveryMobileNo;
                consol.pickUpDeliveryPoint.deliveryPhoneNo = service.pickUpDeliveryPoint.deliveryPhoneNo;
                consol.pickUpDeliveryPoint.deliveryEmail = service.pickUpDeliveryPoint.deliveryEmail;
                consol.pickUpDeliveryPoint.deliveryExpected =$rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.deliveryExpected);
                consol.pickUpDeliveryPoint.deliveryActual =$rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.deliveryActual);
            	
            	
                consol.pickUpDeliveryPoint.doorDeliveryPoint = service.pickUpDeliveryPoint.doorDeliveryPoint;
                consol.pickUpDeliveryPoint.doorDeliveryFrom = service.pickUpDeliveryPoint.doorDeliveryFrom;
                consol.pickUpDeliveryPoint.doorDeliveryPlace = service.pickUpDeliveryPoint.doorDeliveryPlace;
            	
            	
                if(service.pickUpDeliveryPoint.doorDeliveryPlace!=undefined && service.pickUpDeliveryPoint.doorDeliveryPlace!=null &&  service.pickUpDeliveryPoint.doorDeliveryPlace!=""){
                    consol.pickUpDeliveryPoint.doorDeliveryPlace.id = null;
                    consol.pickUpDeliveryPoint.doorDeliveryPlace.versionLock =  0;
                }
            	
                consol.pickUpDeliveryPoint.doorDeliveryContactPerson = service.pickUpDeliveryPoint.doorDeliveryContactPerson;
                consol.pickUpDeliveryPoint.doorDeliveryMobileNo = service.pickUpDeliveryPoint.doorDeliveryMobileNo;
                consol.pickUpDeliveryPoint.doorDeliveryPhoneNo = service.pickUpDeliveryPoint.doorDeliveryPhoneNo;
                consol.pickUpDeliveryPoint.doorDeliveryEmail = service.pickUpDeliveryPoint.doorDeliveryEmail;
                consol.pickUpDeliveryPoint.doorDeliveryExpected =$rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.doorDeliveryExpected);
                consol.pickUpDeliveryPoint.doorDeliveryActual =$rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.doorDeliveryActual);
 
          }*/


            // Attachments no need to copy...
            //consol.consolAttachmentList = [{}];

            consol.consolDocument.documentReqDate = $rootScope.dateToString(new Date());
            consol.consolDocument.documentNo = service.documentList[0].documentNo;

            if (consol.directShipment == 'Yes' || consol.directShipment == true) {

                consol.agent = null;
                consol.agentAddress = null;

                consol.consolDocument.shipper = service.documentList[0].shipper;
                consol.consolDocument.shipperAddress = service.documentList[0].shipperAddress;
                if (consol.consolDocument.shipperAddress != null) {
                    consol.consolDocument.shipperAddress.id = null;
                }

                consol.consolDocument.consignee = service.documentList[0].consignee;
                consol.consolDocument.consigneeAddress = service.documentList[0].consigneeAddress;
                if (consol.consolDocument.consigneeAddress != null) {
                    consol.consolDocument.consigneeAddress.id = null;
                }

            } else {

                consol.agent = service.documentList[0].agent;;
                consol.agentAddress = service.documentList[0].agentAddress;;
                if (consol.agentAddress != null)
                    consol.agentAddress.id = null;

                consol.consolDocument.shipper = service.documentList[0].issuingAgent;
                consol.consolDocument.shipperAddress = service.documentList[0].issuingAgentAddress;
                if (consol.consolDocument.shipperAddress != null)
                    consol.consolDocument.shipperAddress.id = null;

                consol.consolDocument.consignee = service.documentList[0].agent;
                consol.consolDocument.consigneeAddress = service.documentList[0].agentAddress;
                if (consol.consolDocument.consigneeAddress != null)
                    consol.consolDocument.consigneeAddress.id = null;

            }

            consol.consolDocument.forwarder = service.documentList[0].forwarder;
            consol.consolDocument.forwarderAddress = service.documentList[0].forwarderAddress;
            if (consol.consolDocument.forwarderAddress != null) {
                consol.consolDocument.forwarderAddress.id = null;
            }

            consol.consolDocument.firstNotify = service.documentList[0].firstNotify;
            consol.consolDocument.firstNotifyAddress = service.documentList[0].firstNotifyAddress;
            if (consol.consolDocument.firstNotifyAddress != null) {
                consol.consolDocument.firstNotifyAddress.id = null;
            }

            consol.consolDocument.secondNotify = service.documentList[0].secondNotify;
            consol.consolDocument.secondNotifyAddress = service.documentList[0].secondNotifyAddress;
            if (consol.consolDocument.secondNotifyAddress != null) {
                consol.consolDocument.secondNotifyAddress.id = null;
            }

            consol.consolDocument.agent = service.documentList[0].agent;
            consol.consolDocument.agentAddress = service.documentList[0].agentAddress;
            if (consol.consolDocument.agentAddress != null) {
                consol.consolDocument.agentAddress.id = null;
            }

            consol.consolDocument.issuingAgent = service.documentList[0].issuingAgent;
            consol.consolDocument.issuingAgentAddress = service.documentList[0].issuingAgentAddress;
            if (consol.consolDocument.issuingAgentAddress != null) {
                consol.consolDocument.issuingAgentAddress.id = null;
            }


            consol.consolDocument.packMaster = service.documentList[0].packMaster;
            consol.consolDocument.packDescription = service.documentList[0].packDescription;
            consol.consolDocument.dimensionUnit = service.documentList[0].dimensionUnit;
            if (service.bookedVolumeUnitCbm != null) {
                consol.consolDocument.volume = service.bookedVolumeUnitCbm;
            }
            consol.consolDocument.dimensionUnitValue = service.documentList[0].dimensionUnitValue;

            consol.consolDocument.handlingInformation = service.documentList[0].handlingInformation;
            consol.consolDocument.accountingInformation = service.documentList[0].accoutingInformation;
            consol.consolDocument.commodityDescription = service.documentList[0].commodityDescription;
            consol.consolDocument.marksAndNo = service.documentList[0].marksAndNo;
            consol.consolDocument.rateClass = service.documentList[0].rateClass;

            consol.consolDocument.company = service.documentList[0].company;
            consol.consolDocument.country = service.documentList[0].country;
            consol.consolDocument.location = service.documentList[0].location;


            consol.consolDocument.dimensionList = [];


            for (var i = 0; i < service.documentList.length; i++) {

                for (var j = 0; j < service.documentList[i].dimensionList.length; j++) {
                    var consDim = {};
                    consDim.noOfPiece = service.documentList[i].dimensionList[j].noOfPiece;
                    consDim.length = service.documentList[i].dimensionList[j].length;
                    consDim.width = service.documentList[i].dimensionList[j].width;
                    consDim.height = service.documentList[i].dimensionList[j].height;
                    consDim.volWeight = service.documentList[i].dimensionList[j].volWeight;
                    consDim.grossWeight = service.documentList[i].dimensionList[j].grossWeight;
                    consDim.grossWeightKg = service.documentList[i].dimensionList[j].grossWeightKg;
                    consDim.serviceId = service.id;
                    consDim.serviceDocumentId = service.documentList[i].id;
                    consol.consolDocument.dimensionList.push(consDim);
                }
                consol.consolDocument.dimGrossWeight = service.documentList[i].dimensionGrossWeight;
                consol.consolDocument.dimVolumeWeight = service.documentList[i].dimensionVolumeWeight
                consol.consolDocument.dimNoOfPieces = service.documentList[i].dimensionNoOfPieces;
            }


            if (service.documentList[0].ratePerCharge != undefined && service.documentList[0].ratePerCharge != null) {
                consol.consolDocument.ratePerCharge = service.documentList[0].ratePerCharge;
            }

            consol.chargeList = [{}];

            //is used for getting shipment using mawb no-air import consol
            if (key != undefined && key == 'forConsol') {
                return consol;
            }

            //consol.referenceList = [{}];
            $rootScope.createMasterShipment = consol;
            var params = {};
            params.fromState = "fromShipment"
            $state.go('layout.createAirConsol', params);

        }







        this.configureData = function(shipment) {

            shipment.whoRouted = shipment.whoRouted == 'Agent' ? true : false;
            shipment.directShipment = shipment.directShipment == 'Yes' ? true : false;
            for (var i = 0; i < shipment.shipmentServiceList.length; i++) {
                shipment.shipmentServiceList[i].whoRouted = shipment.shipmentServiceList[i].whoRouted == 'Agent' ? true : false;
                shipment.shipmentServiceList[i].ppcc = shipment.shipmentServiceList[i].ppcc == 'Prepaid' ? false : true;

                shipment.shipmentServiceList[i].serviceReqDate = $rootScope.dateToString(shipment.shipmentServiceList[i].serviceReqDate);
                shipment.shipmentServiceList[i].eta = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].eta);
                shipment.shipmentServiceList[i].etd = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].etd);

                if (shipment.shipmentServiceList[i].documentList != undefined && shipment.shipmentServiceList[i].documentList != null &&
                    shipment.shipmentServiceList[i].documentList.length > 0) {
                    for (var j = 0; j < shipment.shipmentServiceList[i].documentList.length; j++) {
                        if (shipment.shipmentServiceList[i].documentList[j].dimensionList == undefined ||
                            shipment.shipmentServiceList[i].documentList[j].dimensionList == null ||
                            shipment.shipmentServiceList[i].documentList[j].dimensionList.length == 0) {
                            shipment.shipmentServiceList[i].documentList[j].dimensionList = [];
                            shipment.shipmentServiceList[i].documentList[j].dimensionList.push({});
                        }

                        shipment.shipmentServiceList[i].documentList[j].documentReqDate = $rootScope.dateToString(shipment.shipmentServiceList[i].documentList[j].documentReqDate);
                        shipment.shipmentServiceList[i].documentList[j].etd = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].etd);
                        shipment.shipmentServiceList[i].documentList[j].eta = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].eta);

                        shipment.shipmentServiceList[i].documentList[j].canIssuedDate = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].canIssuedDate);
                        shipment.shipmentServiceList[i].documentList[j].doIssuedDate = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].doIssuedDate);
                        shipment.shipmentServiceList[i].documentList[j].hawbReceivedOn = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].hawbReceivedOn);


                    }
                }

                if (shipment.shipmentServiceList[i].connectionList == undefined || shipment.shipmentServiceList[i].connectionList == null || shipment.shipmentServiceList[i].connectionList.length == 0) {
                    shipment.shipmentServiceList[i].connectionList = [];
                    shipment.shipmentServiceList[i].connectionList.push({
                        connectionStatus: 'Planned'
                    });
                } else {
                    for (var j = 0; j < shipment.shipmentServiceList[i].connectionList.length; j++) {
                        shipment.shipmentServiceList[i].connectionList[j].etd = $rootScope.dateToString(shipment.shipmentServiceList[i].connectionList[j].etd);
                        shipment.shipmentServiceList[i].connectionList[j].eta = $rootScope.dateToString(shipment.shipmentServiceList[i].connectionList[j].eta);
                    }

                }

                if (shipment.shipmentServiceList[i].shipmentAttachmentList == undefined || shipment.shipmentServiceList[i].shipmentAttachmentList == null || shipment.shipmentServiceList[i].shipmentAttachmentList.length == 0) {
                    shipment.shipmentServiceList[i].shipmentAttachmentList = [];
                    shipment.shipmentServiceList[i].shipmentAttachmentList.push({});
                }

                if (shipment.shipmentServiceList[i].pickUpDeliveryPoint != undefined && shipment.shipmentServiceList[i].pickUpDeliveryPoint != null) {
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = shipment.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp;
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp);
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned);
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual);
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected);
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual);
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected);
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual);

                } else {
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint = {};
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.transporter = {};
                    shipment.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = false;
                }

                if (shipment.shipmentServiceList[i].shipmentServiceTriggerList == undefined || shipment.shipmentServiceList[i].shipmentServiceTriggerList == null || shipment.shipmentServiceList[i].shipmentServiceTriggerList.length == 0) {
                    shipment.shipmentServiceList[i].shipmentServiceTriggerList = [];
                    shipment.shipmentServiceList[i].shipmentServiceTriggerList.push({});
                } else {
                    for (var k = 0; k < shipment.shipmentServiceList[i].shipmentServiceTriggerList.length; k++) {
                        shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].date = $rootScope.dateToString(shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].date);
                        shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].followUpDate = $rootScope.dateToString(shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].followUpDate);
                    }
                }

                if (shipment.shipmentServiceList[i].eventList == undefined || shipment.shipmentServiceList[i].eventList == null || shipment.shipmentServiceList[i].eventList.length == 0) {
                    shipment.shipmentServiceList[i].eventList = [];
                    shipment.shipmentServiceList[i].eventList.push({
                        isCompleted: 'Yes',
                        followUpRequired: 'No'
                    });
                } else {
                    for (var j = 0; j < shipment.shipmentServiceList[i].eventList.length; j++) {
                        shipment.shipmentServiceList[i].eventList[j].eventDate = $rootScope.dateToString(shipment.shipmentServiceList[i].eventList[j].eventDate);
                        shipment.shipmentServiceList[i].eventList[j].followUpDate = $rootScope.dateToString(shipment.shipmentServiceList[i].eventList[j].followUpDate);
                    }
                }

                if (shipment.shipmentServiceList[i].referenceList == undefined || shipment.shipmentServiceList[i].referenceList == null || shipment.shipmentServiceList[i].referenceList.length == 0) {
                    shipment.shipmentServiceList[i].referenceList = [];
                    shipment.shipmentServiceList[i].referenceList.push({});
                }

                if (shipment.shipmentServiceList[i].billOfEntryList == undefined || shipment.shipmentServiceList[i].billOfEntryList == null || shipment.shipmentServiceList[i].billOfEntryList.length == 0) {
                    shipment.shipmentServiceList[i].billOfEntryList = [];
                    shipment.shipmentServiceList[i].billOfEntryList.push({});
                } else {

                    for (var j = 0; j < shipment.shipmentServiceList[i].billOfEntryList.length; j++) {
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].boeDate);
                        }
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].processDate);
                        }
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].receivedDate);
                        }
                        if (shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate !== undefined && shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate != null && shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate == "") {
                            shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate = $rootScope.dateToString(shipmentObj.shipmentServiceList[i].billOfEntryList[j].ackDate);
                        }
                    }
                }


                if (shipment.shipmentServiceList[i].authenticatedDocList == undefined || shipment.shipmentServiceList[i].authenticatedDocList == null || shipment.shipmentServiceList[i].authenticatedDocList.length == 0) {
                    shipment.shipmentServiceList[i].authenticatedDocList = [];
                    shipment.shipmentServiceList[i].authenticatedDocList.push({});
                } else {
                    for (var k = 0; k < shipment.shipmentServiceList[i].authenticatedDocList.length; k++) {
                        shipment.shipmentServiceList[i].authenticatedDocList[k].date = $rootScope.dateToString(shipment.shipmentServiceList[i].authenticatedDocList[k].date);
                    }
                }

                shipment.shipmentServiceList[i].attachConfig = {
                    "isEdit": true,
                    "isDelete": true,
                    "page": "shipment",
                    "columnDefs": [

                        {
                            "name": "Document Name",
                            "model": "documentMaster.documentName",
                            "type": "text"
                        }, {
                            "name": "Reference #",
                            "model": "refNo",
                            "type": "text"
                        }, {
                            "name": "Customs",
                            "model": "customs",
                            "type": "text"
                        }, {
                            "name": "Unprotected File Name",
                            "model": "unprotectedFileName",
                            "type": "file"
                        }, {
                            "name": "Protected File Name",
                            "model": "protectedFileName",
                            "type": "file"
                        }, {
                            "name": "Actions",
                            "model": "action",
                            "type": "action"
                        }
                    ]
                }


            }
            return shipment;
        }


        this.checkIsTranshipment = function(serviceObj) {

            var isTranshipment = false;
            if (serviceObj != undefined && serviceObj.origin != undefined && serviceObj.origin != null && serviceObj.origin.id != undefined &&
                serviceObj.pol != undefined && serviceObj.pol != null && serviceObj.pol.id != undefined && serviceObj.origin.id != serviceObj.pol.id) {
                isTranshipment = true;
            }

            if (serviceObj != undefined && serviceObj.destination != undefined && serviceObj.destination != null && serviceObj.destination.id != undefined &&
                serviceObj.pod != undefined && serviceObj.pod != null && serviceObj.pod.id != undefined && serviceObj.destination.id != serviceObj.pod.id) {
                isTranshipment = true;
            }
            return isTranshipment;

        }



        this.copyShipment = function(shipmentData, copyShipmentObject) {


            var shipment = shipmentData;
            shipment.id = null;
            shipment.versionLock = 0;
            shipment.shipmentUid = null;
            shipment.shipmentReqDate = null;
            shipment.lastUpdatedStatus = null;
            shipment.shipmentStatusList = null;
            shipment.directShipment = false;
            if (shipment.partyAddress != undefined && shipment.partyAddress != null) {
                shipment.partyAddress.id = null;
            }
            shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
            if (shipment.project != undefined && shipment.project != null) {
                shipment.project = null;
                shipment.projectCode = null;
            }
            if (shipment.commodity != undefined && shipment.commodity != null) {
                shipment.commodity = null;
                shipment.commodityCode = null;
            }
            if (shipment.shipmentServiceList != undefined &&
                shipment.shipmentServiceList != null &&
                shipment.shipmentServiceList.length > 0) {

                var tempService = null;
                for (var i = 0; i < shipment.shipmentServiceList.length; i++) {

                    if (shipment.shipmentServiceList[i].id === copyShipmentObject.serviceId) {
                        shipment.shipmentServiceList[i].aesFile = null;
                        shipment.shipmentServiceList[i].whoRouted = shipment.shipmentServiceList[i].whoRouted == 'Self' ? false : true;
                        shipment.shipmentServiceList[i].ppcc = shipment.shipmentServiceList[i].ppcc == 'Prepaid' ? false : true;
                        shipment.shipmentServiceList[i].dimensionUnit = false;
                        shipment.shipmentServiceList[i].serviceReqDate = $rootScope.sendApiDateAndTime(new Date());
                        shipment.shipmentServiceList[i].isMinimumShipment = false;
                        shipment.shipmentServiceList[i].id = null;
                        shipment.shipmentServiceList[i].taskId = null;
                        shipment.shipmentServiceList[i].processInstanceId = null;
                        shipment.shipmentServiceList[i].versionLock = 0;
                        shipment.shipmentServiceList[i].systemTrack = null;
                        shipment.shipmentServiceList[i].serviceUid = null;
                        shipment.shipmentServiceList[i].shipmentUid = null;
                        shipment.shipmentServiceList[i].consolUid = null;
                        shipment.shipmentServiceList[i].quotationUid = null;
                        shipment.shipmentServiceList[i].mawbNo = null;
                        shipment.shipmentServiceList[i].mawbDate = null;
                        shipment.shipmentServiceList[i].pickUpDeliveryPoint = {};
                        shipment.shipmentServiceList[i].serviceStatusList = null;
                        shipment.shipmentServiceList[i].lastUpdatedStatus = null;
                        shipment.shipmentServiceList[i].shipmentServiceTriggerList = null;
                        shipment.shipmentServiceList[i].shipmentAttachmentList = null;
                        shipment.shipmentServiceList[i].connectionList = null;
                        shipment.shipmentServiceList[i].eventList = null;
                        shipment.shipmentServiceList[i].referenceList = null;
                        shipment.shipmentServiceList[i].billOfEntryList = null;
                        shipment.shipmentServiceList[i].hawbNo = null;
                        shipment.shipmentServiceList[i].bookedPieces = null;
                        shipment.shipmentServiceList[i].bookedGrossWeightUnitKg = null;
                        shipment.shipmentServiceList[i].bookedVolumeWeightUnitKg = null;
                        shipment.shipmentServiceList[i].bookedVolumeUnitCbm = null;
                        shipment.shipmentServiceList[i].bookedChargeableUnit = null;
                        shipment.shipmentServiceList[i].bookedGrossWeightUnitPound = null;
                        shipment.shipmentServiceList[i].bookedVolumeWeightUnitPound = null;
                        shipment.shipmentServiceList[i].exportRef = null;
                        shipment.shipmentServiceList[i].importRef = null;
                        shipment.shipmentServiceList[i].carrier = null;

                        shipment.shipmentServiceList[i].carrier = null;
                        shipment.shipmentServiceList[i].carrierCode = null;
                        shipment.shipmentServiceList[i].etd = null;
                        shipment.shipmentServiceList[i].eta = null;

                        shipment.shipmentServiceList[i].routeNo = null;
                        shipment.shipmentServiceList[i].mawbNo = null;
                        shipment.shipmentServiceList[i].mawbDate = null;
                        shipment.shipmentServiceList[i].coLoader = null;
                        shipment.shipmentServiceList[i].coLoaderCode = null;

                        shipment.shipmentServiceList[i].division = null;
                        shipment.shipmentServiceList[i].divisionCode = null;


                        shipment.shipmentServiceList[i].connectionList = [{
                            connectionStatus: 'Planned'
                        }];
                        shipment.shipmentServiceList[i].shipmentAttachmentList = [];
                        shipment.shipmentServiceList[i].eventList = [];
                        shipment.shipmentServiceList[i].referenceList = [];
                        shipment.shipmentServiceList[i].billOfEntryList = [];
                        shipment.shipmentServiceList[i].shipmentServiceTriggerList = [];
                        shipment.shipmentServiceList[i].authenticatedDocList = [{}];
                        shipment.shipmentServiceList[i].cfsReceiveEntryList = [];
                        shipment.shipmentServiceList[i].attachConfig = this.getAttachmentHeaderData();

                        shipment.shipmentServiceList[i].shipmentServiceSignOff = null;

                        if (shipment.shipmentServiceList[i].shipmentChargeList != undefined &&
                            shipment.shipmentServiceList[i].shipmentChargeList != null &&
                            shipment.shipmentServiceList[i].shipmentChargeList.length > 0) {
                            for (var j = 0; j < shipment.shipmentServiceList[i].shipmentChargeList.length; j++) {
                                shipment.shipmentServiceList[i].shipmentChargeList[j].id = null;
                            }
                        }

                        if (shipment.shipmentServiceList[i].documentList != undefined &&
                            shipment.shipmentServiceList[i].documentList != null &&
                            shipment.shipmentServiceList[i].documentList.length > 0) {

                            for (var j = 0; j < shipment.shipmentServiceList[i].documentList.length; j++) {
                                shipment.shipmentServiceList[i].documentList[j].id = null;
                                shipment.shipmentServiceList[i].documentList[j].versionLock = 0;
                                shipment.shipmentServiceList[i].documentList[j].systemTrack = null;
                                shipment.shipmentServiceList[i].documentList[j].documentReqDate = null;
                                shipment.shipmentServiceList[i].documentList[j].shipmentUid = null;
                                shipment.shipmentServiceList[i].documentList[j].serviceUid = null;
                                shipment.shipmentServiceList[i].documentList[j].documentNo = null;
                                shipment.shipmentServiceList[i].documentList[j].mawbNo = null;
                                shipment.shipmentServiceList[i].documentList[j].hawbNo = null;
                                shipment.shipmentServiceList[i].documentList[j].dimensionUnit = false;
                                shipment.shipmentServiceList[i].documentList[j].grossWeight = null;
                                shipment.shipmentServiceList[i].documentList[j].chargebleWeight = null;
                                shipment.shipmentServiceList[i].documentList[j].volumeWeightInPound = null;
                                shipment.shipmentServiceList[i].documentList[j].grossWeightInPound = null;
                                shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm = null;
                                shipment.shipmentServiceList[i].documentList[j].commodityDescription = null;
                                shipment.shipmentServiceList[i].documentList[j].rateClass = null;
                                shipment.shipmentServiceList[i].documentList[j].marksAndNo = null;
                                shipment.shipmentServiceList[i].documentList[j].blReceivedPerson = null;
                                shipment.shipmentServiceList[i].documentList[j].blReceivedPersonCode = null;
                                shipment.shipmentServiceList[i].documentList[j].blReceivedDate = null;
                                shipment.shipmentServiceList[i].documentList[j].eta = null;
                                shipment.shipmentServiceList[i].documentList[j].etd = null;
                                shipment.shipmentServiceList[i].documentList[j].chargebleWeight = null;
                                shipment.shipmentServiceList[i].documentList[j].carrier = null;
                                shipment.shipmentServiceList[i].documentList[j].noOfPieces = null;
                                shipment.shipmentServiceList[i].documentList[j].commodityDescription = null;
                                shipment.shipmentServiceList[i].documentList[j].hawbReceivedBy = null;
                                shipment.shipmentServiceList[i].documentList[j].hawbReceivedOn = null;
                                shipment.shipmentServiceList[i].documentList[j].volumeWeight = null;
                                shipment.shipmentServiceList[i].documentList[j].routeNo = null;


                                if (shipment.shipmentServiceList[i].documentList[j].shipper != undefined && shipment.shipmentServiceList[i].documentList[j].shipper != null && shipment.shipmentServiceList[i].documentList[j].shipper.id != null && shipment.shipmentServiceList[i].documentList[j].shipperAddress.id != null) {
                                    shipment.shipmentServiceList[i].documentList[j].shipperAddress.id = null
                                }

                                if (shipment.shipmentServiceList[i].documentList[j].forwarder != undefined && shipment.shipmentServiceList[i].documentList[j].forwarder != null && shipment.shipmentServiceList[i].documentList[j].forwarder.id != null && shipment.shipmentServiceList[i].documentList[j].forwarderAddress.id != null) {
                                    shipment.shipmentServiceList[i].documentList[j].forwarderAddress.id = null
                                }

                                if (shipment.shipmentServiceList[i].documentList[j].consignee != undefined && shipment.shipmentServiceList[i].documentList[j].consignee != null && shipment.shipmentServiceList[i].documentList[j].consignee.id != null && shipment.shipmentServiceList[i].documentList[j].consigneeAddress.id != null) {
                                    shipment.shipmentServiceList[i].documentList[j].consigneeAddress.id = null;
                                }


                                shipment.shipmentServiceList[i].documentList[j].firstNotify = null;
                                shipment.shipmentServiceList[i].documentList[j].firstNotifyAddress = null

                                shipment.shipmentServiceList[i].documentList[j].secondNotify = null;
                                shipment.shipmentServiceList[i].documentList[j].secondNotifyAddress = null

                                shipment.shipmentServiceList[i].documentList[j].agent = null;
                                shipment.shipmentServiceList[i].documentList[j].agentAddress = null

                                shipment.shipmentServiceList[i].documentList[j].agentAddress
                                shipment.shipmentServiceList[i].documentList[j].issuingAgent = null;
                                shipment.shipmentServiceList[i].documentList[j].issuingAgentAddress = null;

                                shipment.shipmentServiceList[i].documentList[j].dimensionList = [{}];

                            }

                        }


                        tempService = shipment.shipmentServiceList[i];
                    } else {
                        console.log("Removing the service....");
                        shipment.shipmentServiceList.splice(i, 1);
                    }
                }

                shipment.shipmentServiceList = [];
                shipment.shipmentServiceList.push(tempService);
            }

            return shipment;
        }

        this.routePartyEvent = function(fromObjModel, isAdd, partyMaster, isService, serviceIndex, docIndex, selectedTab) {
            $rootScope.nav_src_bkref_key = new Date().toISOString();
            var param = {
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                forObj: fromObjModel,
                category: "Shipment"
            };
            if (isService) {
                param.serIndx = serviceIndex;
                param.docIndx = docIndex;
                param.selectedTab = selectedTab;
            }

            if (isAdd) {
                $state.go("layout.addParty", param);
            } else {
                if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                    param.partyId = partyMaster.id;
                    $state.go("layout.editParty", param);
                } else {
                    console.log("Party not selected...");
                }
            }
        }

        // shipment milestone code starts here
        this.prepareToolTipData = function(shipmentTrackList) {
                if (shipmentTrackList == null || shipmentTrackList == undefined) {
                    return;
                }
                for (var i = 0; i < shipmentTrackList.length; i++) {
                    if (shipmentTrackList[i].labelName == 'Booking Date') {
                        shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>Customer : </td><td class='left-align'>&nbsp" + shipmentTrackList[i].customerName + "</td></tr><tr><td class='right-align'>Service : </td><td class='left-align'>&nbsp" + shipmentTrackList[i].serviceName + "</td></tr></table>";
                    }
                    if (shipmentTrackList[i].labelName == 'Pickup Date') {
                        var str1 = '';
                        var str2 = '';
                        if (shipmentTrackList[i].date != null && shipmentTrackList[i].date != undefined) {
                            str1 = "Pickup&nbspDate : </td><td class='left-align'>&nbsp" + $rootScope.dateToString(shipmentTrackList[i].date) + "</td></tr><tr><td class='right-align'>";
                            if (shipmentTrackList[i].pickUpPoint != null && shipmentTrackList[i].pickUpPoint != undefined)
                                str2 = "Pickup&nbsppoint : </td><td class='left-align'>&nbsp" + shipmentTrackList[i].pickUpPoint;
                            shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>" + str1 + str2 + "</td></tr></table>";
                        }
                    }
                    if (shipmentTrackList[i].labelName == 'Cargo Received Date') {
                        var str1 = '';
                        var str2 = '';
                        if (shipmentTrackList[i].date != null && shipmentTrackList[i].date != undefined) {
                            str1 = "Received Date :</td><td class='left-align'>&nbsp " + $rootScope.dateToString(shipmentTrackList[i].date) + "</td></tr><tr><td class='right-align'>";
                            if (shipmentTrackList[i].recievedPoint != null && shipmentTrackList[i].recievedPoint != undefined)
                                str2 = "Receive point :</td><td class='left-align'>&nbsp " + shipmentTrackList[i].recievedPoint;
                            shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>" + str1 + str2 + "</td></tr></table>";
                        }
                    }
                    if (shipmentTrackList[i].labelName == 'ETD') {
                        var str1 = '';
                        var str2 = '';
                        var str0 = '';
                        if (shipmentTrackList[i].date != null && shipmentTrackList[i].date != undefined) {
                            str2 = "ETD date : </td><td class='left-align'>&nbsp" + $rootScope.dateAndTimeToString(shipmentTrackList[i].date);
                            if (shipmentTrackList[i].carrierName != null && shipmentTrackList[i].carrierName != undefined) {
                                str0 = "Carrier : </td><td class='left-align'>&nbsp" + shipmentTrackList[i].carrierName + "</td></tr><tr><td class='right-align'>";
                            }
                            if (shipmentTrackList[i].carrierNo != null && shipmentTrackList[i].carrierNo != undefined) {
                                str1 = "Carrier No : </td><td class='left-align'>&nbsp" + shipmentTrackList[i].carrierNo + "</td></tr><tr><td class='right-align'>";
                            }
                            shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>" + str0 + str1 + str2 + "</td></tr></table>";
                        }
                    }
                    if (shipmentTrackList[i].labelName == 'ETA') {
                        var str1 = '';
                        var str2 = '';
                        var str0 = '';
                        if (shipmentTrackList[i].date != null && shipmentTrackList[i].date != undefined) {
                            str2 = "ETA date : </td><td class='left-align'>&nbsp" + $rootScope.dateAndTimeToString(shipmentTrackList[i].date);
                            if (shipmentTrackList[i].carrierName != null && shipmentTrackList[i].carrierName != undefined) {
                                str0 = "Carrier :</td><td class='left-align'>&nbsp" + shipmentTrackList[i].carrierName + "</td></tr><tr><td class='right-align'>";
                            }
                            if (shipmentTrackList[i].carrierNo != null && shipmentTrackList[i].carrierNo != undefined) {
                                str1 = "Carrier No :</td><td class='left-align'>&nbsp" + shipmentTrackList[i].carrierNo + "</td></tr><tr><td class='right-align'>";
                            }
                            shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>" + str0 + str1 + str2 + "</td></tr></table>";
                        }
                    }
                    if (shipmentTrackList[i].labelName == 'CAN Date') {
                        var str1 = '';
                        var str2 = '';
                        if (shipmentTrackList[i].date != null && shipmentTrackList[i].date != undefined) {
                            str1 = "CAN Date :</td><td class='left-align'>&nbsp" + $rootScope.dateToString(shipmentTrackList[i].date) + "</td></tr><tr><td class='right-align'>";
                            if (shipmentTrackList[i].noticeSentTo != null && shipmentTrackList[i].noticeSentTo != undefined)
                                str2 = "Notice sent to :</td><td class='left-align'>&nbsp" + shipmentTrackList[i].noticeSentTo;
                            shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>" + str1 + str2 + "</td></tr></table>";
                        }
                    }
                    if (shipmentTrackList[i].labelName == 'Delivery Date') {
                        var str1 = '';
                        var str2 = '';
                        if (shipmentTrackList[i].date != null && shipmentTrackList[i].date != undefined) {
                            str1 = "Delivery Date :</td><td class='left-align'>&nbsp " + $rootScope.dateToString(shipmentTrackList[i].date) + "</td></tr><tr><td class='right-align'>";
                            if (shipmentTrackList[i].deliveryPoint != null && shipmentTrackList[i].deliveryPoint != undefined)
                                str2 = "Delivery point :</td><td class='left-align'>&nbsp " + shipmentTrackList[i].deliveryPoint;
                            shipmentTrackList[i].toolTip = "<table><tr><td class='right-align'>" + str1 + str2 + "</td></tr></table>";
                        }
                    }

                }
                return shipmentTrackList;

            }
            // shipment milestone code ends here

        this.goToJobLedger = function(shipmentServiceDetail, consol) {
            if (consol == null || consol == undefined || consol.id == null) {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_JOB_LEDGER_VIEW)) {
                    return;
                }
            }

            if (shipmentServiceDetail == null || shipmentServiceDetail == undefined || shipmentServiceDetail.id == null) {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_MORE_JOB_LEDGER_VIEW)) {
                    return;
                }
            }
            var shipmentArr = "";
            var consolUid = null;
            var serviceName = null;
            var shipmentUid = null;
            var serviceUid = null;
            if (consol != null && consol != undefined && consol.id != null && consol.shipmentLinkList != null && consol.shipmentLinkList.length > 0) {
                for (var i = 0; i < consol.shipmentLinkList.length; i++) {
                    if (i == 0) {
                        shipmentArr = consol.shipmentLinkList[0].shipmentUid;
                    } else {
                        shipmentArr = shipmentArr + "," + consol.shipmentLinkList[i].shipmentUid;
                    }
                }
                consolUid = consol.consolUid;
                serviceName = consol.serviceMaster.serviceName;
            } else if (shipmentServiceDetail) {
                shipmentArr = shipmentServiceDetail.shipmentUid;
                shipmentUid = shipmentServiceDetail.shipmentUid;
                //consolUid=shipmentServiceDetail.consolUid;
                consolUid = null;
                serviceName = shipmentServiceDetail.serviceMaster.serviceName;
                serviceUid = shipmentServiceDetail.serviceUid;
            }
            var param = {
                serviceName: serviceName,
                shipmentUid: shipmentUid,
                consolUid: consolUid,
                shipmentArr: shipmentArr,
                serviceUid: serviceUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
            $state.go('layout.jobledger', param);
        }


        this.isPickUpDeliveyModified = function(obj, type) {
            if (obj == undefined || obj == null) {
                return false;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]] && k[i].startsWith(type, 0)) {
                    if (angular.isObject(obj[k[i]])) {
                        if (obj[k[i]] != null && obj[k[i]] != undefined && obj[k[i]].id != null) {
                            return true
                        }
                    } else {
                        if (obj[k[i]] != null && obj[k[i]] != undefined && obj[k[i]] != '') {
                            return true
                        }
                    }
                }
            }
            return false;
        }

        this.connectionStatus = function(serviceDetail) {
            if (serviceDetail.connectionList != null && serviceDetail.connectionList != undefined && serviceDetail.connectionList.length > 0) {
                if (serviceDetail.connectionList.length > 1) {
                    return true;
                }
                for (var lIdx = 0; lIdx < serviceDetail.connectionList.length; lIdx++) {
                    if (serviceDetail.connectionList[lIdx] != null && serviceDetail.connectionList[lIdx] != undefined) {

                        if ((serviceDetail.connectionList[lIdx].move != null && serviceDetail.connectionList[lIdx].move != undefined && serviceDetail.connectionList[lIdx].move != "") ||
                            (serviceDetail.connectionList[lIdx].pol != null && serviceDetail.connectionList[lIdx].pol != undefined && serviceDetail.connectionList[lIdx].pol != "") ||
                            (serviceDetail.connectionList[lIdx].pod != null && serviceDetail.connectionList[lIdx].pod != undefined && serviceDetail.connectionList[lIdx].pod != "") ||
                            (serviceDetail.connectionList[lIdx].etd != null && serviceDetail.connectionList[lIdx].etd != undefined && serviceDetail.connectionList[lIdx].etd != "") ||
                            (serviceDetail.connectionList[lIdx].eta != null && serviceDetail.connectionList[lIdx].eta != undefined && serviceDetail.connectionList[lIdx].eta != "") ||
                            (serviceDetail.connectionList[lIdx].carrierMaster != null && serviceDetail.connectionList[lIdx].carrierMaster != undefined && serviceDetail.connectionList[lIdx].carrierMaster != "")) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        this.dimensionStatus = function(serviceDetail) {
            var dimensionList = serviceDetail.documentList[0].dimensionList;
            if (dimensionList != null && dimensionList != undefined && dimensionList.length > 0) {
                if (dimensionList.length > 1) {
                    return true;
                }
                for (var lIdx = 0; lIdx < dimensionList.length; lIdx++) {
                    if (dimensionList[lIdx] != null && dimensionList[lIdx] != undefined) {
                        if ((dimensionList[lIdx].noOfPiece != null && dimensionList[lIdx].noOfPiece != undefined && dimensionList[lIdx].noOfPiece != "") ||
                            (dimensionList[lIdx].length != null && dimensionList[lIdx].length != undefined && dimensionList[lIdx].length != "") ||
                            (dimensionList[lIdx].width != null && dimensionList[lIdx].width != undefined && dimensionList[lIdx].width != "") ||
                            (dimensionList[lIdx].height != null && dimensionList[lIdx].height != undefined && dimensionList[lIdx].height != "") ||
                            (dimensionList[lIdx].volWeight != null && dimensionList[lIdx].volWeight != undefined && dimensionList[lIdx].volWeight != "") ||
                            (dimensionList[lIdx].grossWeight != null && dimensionList[lIdx].grossWeight != undefined && dimensionList[lIdx].grossWeight != "")) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        this.routingAndCarrierStatus = function(serviceDetail) {
            if ((serviceDetail.carrier != null && serviceDetail.carrier != undefined && serviceDetail.carrier != "" && serviceDetail.carrier.id != null) ||
                (serviceDetail.routeNo != null && serviceDetail.routeNo != undefined && serviceDetail.routeNo != "") ||
                (serviceDetail.mawbNo != null && serviceDetail.mawbNo != undefined && serviceDetail.mawbNo != "") ||
                (serviceDetail.etd != null && serviceDetail.etd != undefined && serviceDetail.etd != "") ||
                (serviceDetail.eta != null && serviceDetail.eta != undefined && serviceDetail.eta != "")) {
                return true;
            }
            return false;
        }

        this.packAndDimensionStatus = function(serviceDetail) {
            if ((serviceDetail.documentList[0].noOfPieces != null && serviceDetail.documentList[0].noOfPieces != undefined && serviceDetail.documentList[0].noOfPieces != "") ||
                (serviceDetail.documentList[0].grossWeightInPound != null && serviceDetail.documentList[0].grossWeightInPound != undefined && serviceDetail.documentList[0].grossWeightInPound != "") ||
                (serviceDetail.documentList[0].volumeWeightInPound != null && serviceDetail.documentList[0].volumeWeightInPound != undefined && serviceDetail.documentList[0].volumeWeightInPound != "") ||
                (serviceDetail.documentList[0].ratePerCharge != null && serviceDetail.documentList[0].ratePerCharge != undefined && serviceDetail.documentList[0].ratePerCharge != "")
            ) {
                return true;
            }
            return false;
        }

        this.hawbCustomerInfoStatus = function(documentDetail) {
            if ((documentDetail.shipper != null && documentDetail.shipper != undefined && documentDetail.shipper != "" && documentDetail.shipper.id != null) ||
                (documentDetail.consignee != null && documentDetail.consignee != undefined && documentDetail.consignee != "" && documentDetail.consignee.id != null) ||
                (documentDetail.firstNotify != null && documentDetail.firstNotify != undefined && documentDetail.firstNotify != "" && documentDetail.firstNotify.id != null) ||
                (documentDetail.secondNotify != null && documentDetail.secondNotify != undefined && documentDetail.secondNotify != "" && documentDetail.secondNotify.id != null) ||
                (documentDetail.forwarder != null && documentDetail.forwarder != undefined && documentDetail.forwarder != "" && documentDetail.forwarder.id != null) ||
                (documentDetail.agent != null && documentDetail.agent != undefined && documentDetail.agent != "" && documentDetail.agent.id != null) ||
                (documentDetail.issuingAgent != null && documentDetail.issuingAgent != undefined && documentDetail.issuingAgent != "" && documentDetail.issuingAgent.id != null)
            ) {
                return true;
            }
            return false;
        }
        this.hawbOtherInfoStatus = function(documentDetail) {
            if ((documentDetail.handlingInformation != null && documentDetail.handlingInformation != undefined && documentDetail.handlingInformation != "") ||
                (documentDetail.manifestHawbNo != null && documentDetail.manifestHawbNo != undefined && documentDetail.manifestHawbNo != "") ||
                (documentDetail.accoutingInformation != null && documentDetail.accoutingInformation != undefined && documentDetail.accoutingInformation != "") ||
                (documentDetail.commodityDescription != null && documentDetail.commodityDescription != undefined && documentDetail.commodityDescription != "") ||
                (documentDetail.marksAndNo != null && documentDetail.marksAndNo != undefined && documentDetail.marksAndNo != "") ||
                (documentDetail.rateClass != null && documentDetail.rateClass != undefined && documentDetail.rateClass != "")
            ) {
                return true;
            }
            return false;
        }
    }
])