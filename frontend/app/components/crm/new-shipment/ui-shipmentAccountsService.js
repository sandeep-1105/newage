app.service('uiShipmentAccountsService',['$rootScope', '$timeout','$state','RecentHistorySaveService','ProvisionalViewByShipmentServiceUid','ShipmentServiceInvoiceList','Notification','$stateParams','ServiceSignOffStatus',
	function($rootScope, $timeout,$state, RecentHistorySaveService,ProvisionalViewByShipmentServiceUid,ShipmentServiceInvoiceList,Notification,$stateParams,ServiceSignOffStatus) {

		
		this.createInvoice = function(shipment,accountsModel,selectedShipmentServiceIndex) {
			var shipmentServiceDetail = shipment.shipmentServiceList[selectedShipmentServiceIndex];
			
			var flag = false;
			
			if(shipmentServiceDetail.serviceMaster.importExport ==="Export"){
				
				if(shipmentServiceDetail.lastUpdatedStatus.serviceStatus=='Received' 
					|| shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Generated') {
					flag = true;
			}
		}else{
			flag = true;
		}
		
		if(flag) {
			
			var param = {shipmentId : shipment.id,shipmentServiceId : shipmentServiceDetail.serviceUid,
				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
				accountsModel.$promise.then(accountsModel.hide);
				$state.go('layout.invoiceCreate', param);
				
			} else {
				console.log("Can't create invoice, if shipment service is not received or generated")
				Notification.error($rootScope.nls["ERR40015"])
				return false;
				
			}
			
			
		}	    
		this.createCreditNoteCost = function(shipment,accountsModel,selectedShipmentServiceIndex) {
			
			var shipmentServiceDetail = shipment.shipmentServiceList[selectedShipmentServiceIndex];
			
			var flag = false;
			
			if(shipmentServiceDetail.serviceMaster.importExport ==="Export"){
				
				if(shipmentServiceDetail.lastUpdatedStatus.serviceStatus=='Received' 
					|| shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Generated') {
					flag = true;
			}
			
		}else{
			flag = true;
		}
		
		if(flag) {
			
			var param = {shipmentId : shipment.id,shipmentServiceId : shipmentServiceDetail.serviceUid,
				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
				accountsModel.$promise.then(accountsModel.hide);
				$state.go('layout.creditNoteCostCreate', param);
			}
			else
			{
				console.log("Can't create credit note cost, if shipment service is not received or generated")
				Notification.error($rootScope.nls["ERR40016"]);
				return false;
				
			}
		}
		
		this.createCreditNoteRevenue = function(shipment,accountsModel,selectedShipmentServiceIndex) {
			
			var shipmentServiceDetail =shipment.shipmentServiceList[selectedShipmentServiceIndex];
			
			var param = {shipmentUid : shipment.shipmentUid,shipmentServiceId : shipmentServiceDetail.id,
				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
				accountsModel.$promise.then(accountsModel.hide);
				$state.go('layout.addCreditNoteRevenue', param);
				
			}
			
			this.addOrEditProvisional = function(object,shipment,accountsModel,selectedShipmentServiceIndex) {
				var shipmentServiceDetail = shipment.shipmentServiceList[selectedShipmentServiceIndex];
				
				ServiceSignOffStatus.get({
					serviceId: shipmentServiceDetail.id
				}, function(data) {
					if (data.responseCode == 'ERR0') {
						if(data.responseObject!=null && data.responseObject.isSignOff==true){
							console.log($rootScope.nls["ERR90633"]);
							Notification.error($rootScope.nls["ERR90633"]);
						}
						else{
							if(shipmentServiceDetail.serviceMaster.importExport=="Export"){
								
								if(shipmentServiceDetail.lastUpdatedStatus.serviceStatus=='Received' || shipmentServiceDetail.lastUpdatedStatus.serviceStatus=='Generated')
								{
									console.log("addOrEditProvisional Button is Pressed.....");
									var param = {shipmentId :shipment.id,shipmentServiceId : shipmentServiceDetail.serviceUid,
										fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
										accountsModel.$promise.then(accountsModel.hide);
										if(object!=undefined && object != null && object.id!=null){
											param.provisionalId=object.id;
											$state.go('layout.editProvisional', param);
										}
										else{
											$state.go('layout.addProvisional', param);
										}
									}
									else
									{
										console.log("Can't create or edit provisional cost, if shipment is not received or generated")
										Notification.error($rootScope.nls["ERR96676"])
										return false;
									}
								}else{
									var param = {shipmentId : shipment.id,shipmentServiceId : shipmentServiceDetail.serviceUid,
										fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
										accountsModel.$promise.then(accountsModel.hide);
										if(object!=undefined && object != null && object.id!=null){
											param.provisionalId=object.id;
											$state.go('layout.editProvisional', param);
										}
										else{
											$state.go('layout.addProvisional', param);
										}
									}
								}
							}
							
						}, function(error) {
							console.log("error in addOrEditProvisional method : " + error);
						});
			}
			
			
			
			this.getInvoiceCrnView=function(invoiceCreditNote,shipment,accountsModel,selectedShipmentServiceIndex)
			{
				var shipmentServiceDetail = shipment.shipmentServiceList[selectedShipmentServiceIndex];
				var params = {};
				if(invoiceCreditNote.documentTypeCode=='INV')
				{
					
					var param = {shipmentUid : shipment.shipmentUid,invoiceId:invoiceCreditNote.id,shipmentServiceId:shipmentServiceDetail.id,
						fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}

						accountsModel.$promise.then(accountsModel.hide);
						$state.go('layout.viewInvoice', param);
					}
					else
					{
						if(invoiceCreditNote.documentTypeCode=='CRN' && invoiceCreditNote.adjustmentInvoiceNo!=null)
						{
							var param = {shipmentUid :shipment.shipmentUid,creditNoteRevenueId:invoiceCreditNote.id,
								fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}

								accountsModel.$promise.then(accountsModel.hide);
								$state.go('layout.viewCreditNoteRevenue', param);
							}
							else
							{
								var param = {shipmentUid :shipment.shipmentUid,creditNoteCostId:invoiceCreditNote.id,
									fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}

									accountsModel.$promise.then(accountsModel.hide);
									$state.go('layout.viewCreditNoteCost', param);
								}
							}
							
						}
					}]);