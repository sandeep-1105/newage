app.service('uiShipmentCalculationService', ['$rootScope', '$state', 'uiShipmentDataService', function($rootScope, $state, uiShipmentDataService) {




    /*	this.isDimensionFound = function (service){
    		
    		var flag=false;
    		
    		if(service.documentList!=undefined 
    				&& service.documentList!=null
    				&& service.documentList.length!=0){
    			for(var i=0;i<service.documentList.length;i++){
    				if(service.documentList[i].dimensionList!=undefined
    						&& service.documentList[i].dimensionList!=null
    						&& service.documentList[i].dimensionList.length!=0){
    					if(service.documentList[i].dimensionList.length==1){
    						var keyNames = Object.keys(service.documentList[i].dimensionList[0]);
    						if(keyNames.length==0){
    							continue;
    						}
    					}
    					flag=true;
    					break;
    				}
    			}
    		}
    		
    		return flag;
    	}*/

    /*	this.chargableWeightCalulcate = function (service, whichValue){
    		
    		if(service.bookedVolumeWeightUnitPound!=undefined && service.bookedVolumeWeightUnitPound!=null && whichValue=='volumePound'){
    			try{
    				var weight = $rootScope.roundOffWeight(((parseFloat(service.bookedVolumeWeightUnitPound) * 0.453592) * 100)/100);
    				service.bookedVolumeWeightUnitKg = weight;
    				
    			}catch(err){
    				service.bookedVolumeWeightUnitPound = 0.0;
    				service.bookedVolumeWeightUnitKg = 0.0;
    			}
    		}
    		
    		if(service.bookedGrossWeightUnitPound!=undefined && service.bookedGrossWeightUnitPound!=null && whichValue=='grossPound'){
    			try{
    				var weight = $rootScope.roundOffWeight(((parseFloat(service.bookedGrossWeightUnitPound) * 0.453592) * 100)/100);
    				service.bookedGrossWeightUnitKg = weight;
    			}catch(err){
    				service.bookedGrossWeightUnitPound = 0.0;
    				service.bookedGrossWeightUnitKg = 0.0;
    			}
    		}
    		
    		
    		if(service.bookedVolumeWeightUnitKg!=undefined && service.bookedVolumeWeightUnitKg!=null && whichValue=='volumekg'){
    			try{
    				var weight = Math.round((parseFloat(service.bookedVolumeWeightUnitKg) * 2.20462) * 100) / 100;
    				service.bookedVolumeWeightUnitPound = weight;
    			}catch(err){
    				service.bookedVolumeWeightUnitKg = 0.0;
    				service.bookedVolumeWeightUnitPound = 0.0;
    			}
    		}
    		
    		if(service.bookedGrossWeightUnitKg!=undefined && service.bookedGrossWeightUnitKg!=null && whichValue=='grossKg'){
    			try{
    				
    				var weight = Math.round((parseFloat(service.bookedGrossWeightUnitKg) * 2.20462) * 100) / 100;
    				service.bookedGrossWeightUnitPound = weight;
    			}catch(err){
    				service.bookedGrossWeightUnitKg = 0.0;
    				service.bookedGrossWeightUnitPound = 0.0;
    			}
    		}
    		
    	
    		
    		this.calculateChargeWeight(service);
    	}*/

    /*this.calculateChargeWeight = function(service){
		var volumeKg = parseFloat(service.bookedVolumeWeightUnitKg);
		var grossKg = parseFloat(service.bookedGrossWeightUnitKg);
		
		if(volumeKg>0 && grossKg>0){
			
			if(volumeKg>grossKg){
				service.bookedChargeableUnit = volumeKg;
			}else{
				service.bookedChargeableUnit = grossKg;
			}
			
		}else if(volumeKg<=0 && grossKg>0){
			service.bookedChargeableUnit = grossKg;
		}else if(volumeKg>0 && grossKg<=0){
			service.bookedChargeableUnit = volumeKg;
		}else{
			service.bookedChargeableUnit = 0.0;
		}
		
		
		if(service.bookedChargeableUnit>0){
			var conversionUnit = null;
			
			if(service.serviceMaster.transportMode==='Air'){
				conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
			}else if(service.serviceMaster.transportMode==='Road'){
				conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
			}else if(service.serviceMaster.transportMode==='Ocean'){
				conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
			}
			
			if(conversionUnit!=null){
				service.bookedVolumeUnitCbm = parseFloat(service.bookedChargeableUnit)/parseFloat(conversionUnit);
//				service.bookedVolumeUnitCbm = $rootScope.weightFormat(service.bookedVolumeUnitCbm,service.location.countryMaster.locale);
				service.bookedVolumeUnitCbm  = $rootScope.roundOffWeight(service.bookedVolumeUnitCbm);
			}
		}else{
			service.bookedVolumeUnitCbm = 0.0;
		}
		
	}*/

    this.isDocumentDimensionFound = function(document) {

        var flag = false;

        if (document.dimensionList != undefined &&
            document.dimensionList != null &&
            document.dimensionList.length != 0) {

            if (document.dimensionList.length == 1) {
                var keyNames = Object.keys(document.dimensionList[0]);
                if (keyNames.length == 0) {
                    flag = false
                } else {
                    flag = true;
                }
            } else {
                flag = true;
            }
        }

        return flag;
    }

    this.calculateDocumentWeight = function(documentDetail, service, isBookedVolumeUnitCbm) {

        if (documentDetail.dimensionUnit == false) {

            documentDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];

            if (documentDetail.volumeWeight != undefined &&
                documentDetail.volumeWeight != null &&
                documentDetail.volumeWeight != '') {
                documentDetail.volumeWeightInPound = Math.round((documentDetail.volumeWeight * 2.20462) * 100) / 100;
            }

            if (documentDetail.grossWeight != undefined &&
                documentDetail.grossWeight != null &&
                documentDetail.grossWeight != '') {
                documentDetail.grossWeightInPound = Math.round((documentDetail.grossWeight * 2.20462) * 100) / 100;
            }

        } else {
            documentDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];

            if (documentDetail.volumeWeightInPound != undefined &&
                documentDetail.volumeWeightInPound != null &&
                documentDetail.volumeWeightInPound != '' && !isNaN(documentDetail.volumeWeightInPound)) {
                documentDetail.volumeWeight = $rootScope.roundOffWeight(Math.round((documentDetail.volumeWeightInPound * 0.45359237) * 100) / 100);

            } else {
                documentDetail.volumeWeight = null;
            }

            if (documentDetail.grossWeightInPound != undefined &&
                documentDetail.grossWeightInPound != null &&
                documentDetail.grossWeightInPound != '' && !isNaN(documentDetail.grossWeightInPound)) {
                documentDetail.grossWeight = $rootScope.roundOffWeight(Math.round((documentDetail.grossWeightInPound * 0.45359237) * 100) / 100);
            } else {
                documentDetail.grossWeight = null;
            }

        }

        this.calculateDocChargeWeight(documentDetail, service, isBookedVolumeUnitCbm);
    }

    this.calculateWeight = function(param, documentDetail, service) {

        if (param.grossWeight > 0.0 && param.volumeWeight > 0.0) {
            if (documentDetail.dimensionUnit) {
                documentDetail.grossWeight = $rootScope.roundOffWeight(param.dimensionGrossWeightInKg);
                documentDetail.grossWeightInPound = param.grossWeight;
            } else {
                documentDetail.grossWeight = $rootScope.roundOffWeight(param.grossWeight);
                documentDetail.grossWeightInPound = Math.round((documentDetail.grossWeight * 2.20462) * 100) / 100;
            }

            documentDetail.dimensionVolumeWeight = param.volumeWeight;
            documentDetail.dimensionGrossWeight = param.grossWeight;
            documentDetail.dimensionGrossWeightInKg = param.dimensionGrossWeightInKg;
            documentDetail.dimensionNoOfPieces = param.totalPieces;


            documentDetail.volumeWeight = $rootScope.roundOffWeight(param.volumeWeight);
            documentDetail.volumeWeightInPound = Math.round((documentDetail.volumeWeight * 2.20462) * 100) / 100;
            documentDetail.noOfPieces = param.totalPieces;
            this.calculateDocChargeWeight(documentDetail, service);
        }

    }

    this.calculateDocChargeWeight = function(document, service, isBookedVolumeUnitCbm) {
        if (document.volumeWeight == undefined || document.volumeWeight == null || document.volumeWeight == '' || isNaN(document.volumeWeight)) {
            document.volumeWeight = 0;
        }

        if (document.grossWeight == undefined || document.grossWeight == null || document.grossWeight == '' || isNaN(document.grossWeight)) {
            document.grossWeight = 0;
        }

        var volumeKg = parseFloat(document.volumeWeight);
        var grossKg = parseFloat(document.grossWeight);

        if (volumeKg >= 0 && grossKg > 0) {

            if (volumeKg > grossKg) {
                document.chargebleWeight = volumeKg;
            } else {
                document.chargebleWeight = grossKg;
            }

        } else if (volumeKg < 0 && grossKg > 0) {
            document.chargebleWeight = grossKg;
        } else if (volumeKg > 0 && grossKg <= 0) {
            document.chargebleWeight = volumeKg;
        } else {
            document.chargebleWeight = 0.0;
        }

        if (document.chargebleWeight > 0) {
            var conversionUnit = null;

            if (service.serviceMaster.transportMode === 'Air') {
                conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
            } else if (service.serviceMaster.transportMode === 'Road') {
                conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
            } else if (service.serviceMaster.transportMode === 'Ocean') {
                conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
            }

            if (conversionUnit != null && !isBookedVolumeUnitCbm) {
                document.bookedVolumeUnitCbm = parseFloat(document.volumeWeight) / parseFloat(conversionUnit);
                document.bookedVolumeUnitCbm = $rootScope.roundOffWeight(document.bookedVolumeUnitCbm);
            } else {
                document.volumeWeight = parseFloat(document.bookedVolumeUnitCbm) * parseFloat(conversionUnit);
            }
        } else {
            document.bookedVolumeUnitCbm = 0.0;
        }
        if (document.volumeWeight == 0) {
            document.volumeWeight = null;
        }

        uiShipmentDataService.assignedDocumentToService(service, "weightCalculate");
    }

    /*	this.finalWeight = function(service){
    		
    		var noOfPieces = 0;
    		var grossWeight = 0.0;
    		var volumeWeight = 0.0;
    		var grossPound = 0.0;
    		var volumePound = 0.0;
    		var volumeInCbm = 0.0;
    		
    		
    		for(var i=0;i<service.documentList.length;i++){
    			var doc = service.documentList[i];
    			
    			try{
    				noOfPieces = parseInt(noOfPieces) + parseInt(doc.noOfPieces);
    			}catch(err){
    				
    			}
    			
    			try{
    				grossWeight = parseFloat(grossWeight) + parseFloat(doc.grossWeight);
    			}catch(err){
    				
    			}
    			
    			try{
    				volumeWeight = parseFloat(volumeWeight) + parseFloat(doc.volumeWeight);
    			}catch(err){
    				
    			}
    			try{
    				grossPound = parseFloat(grossPound) + parseFloat(doc.grossWeightInPound);
    			}catch(err){
    				
    			}
    			try{
    				volumePound = parseFloat(volumePound) + parseFloat(doc.volumeWeightInPound);
    			}catch(err){
    				
    			}
    			try{
    				volumeInCbm = parseFloat(volumeInCbm) + parseFloat(doc.bookedVolumeUnitCbm);
    			}catch(err){
    				
    			}
    			
    		}
    		
    		service.bookedPieces = noOfPieces;
    		service.bookedGrossWeightUnitKg = grossWeight;
    		service.bookedVolumeWeightUnitKg = volumeWeight;
    		service.bookedGrossWeightUnitPound = grossPound;
    		service.bookedVolumeWeightUnitPound = volumePound;
    		service.bookedVolumeUnitCbm = volumeInCbm;
    		
    		this.calculateChargeWeight(service);
    	}*/

}]);