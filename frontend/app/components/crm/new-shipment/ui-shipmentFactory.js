app.factory('uiShipmentFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/countrymaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/countrymaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/countrymaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/countrymaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		PackGetByCode : $resource('/api/v1/packmaster/get/code/:code', {}, {
			 query : {method : 'GET', params : {code : ''}, isArray : false}
		}),
		
		ShipmentDocumentIdList : $resource("/api/v1/shipment/report/shipment/:serviceId", {}, {
			 query : {method : 'GET', params : {code : ''}, isArray : false}
		}),
		
		isConsolCreatedFromShipment : $resource("/api/v1/consol/isconsolcreatedfromshipment/:serviceUid", {}, {
			 query : {method : 'GET', params : {serviceUid : ''}, isArray : false}
		}),
		GetShipmentByMawbNo : $resource("/api/v1/shipment/getShipmentbymawb/:mawbNo/:serviceId", {}, {
			 fetch : {method : 'GET', params : {mawbNo:' ', serviceId : ''}, isArray : false}
		})
	};
}])

