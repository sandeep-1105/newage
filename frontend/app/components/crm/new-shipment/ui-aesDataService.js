/**
 * AES DATA SERVICE IS USED FOR SEARCH,GENERATE AES IN SERVICE,CONSOL...
 * 
 */
app.service('uiAesDataService', ['$rootScope', 'uiShipmentFactory', '$timeout', '$state', '$stateParams',

    function($rootScope, $state, $stateParams, uiShipmentFactory, $timeout, AesSearch, ServiceGetByUid, ShipmentIdByServiceUid) {


        this.aesSearch = function(service) {
            if (service.serviceUid != undefined && service.serviceUid != null) {
                var searchDto = {};
                searchDto.serviceUid = service.serviceUid;
                AesSearch.query(searchDto).$promise.then(function(data, status) {
                    var totalRecord = data.responseObject.totalRecord;
                    var tempArr = [];
                    var finalArr = [];
                    tempArr = data.responseObject.searchResult;
                    var tempObj = {};
                    angular.forEach(tempArr, function(item, index) {
                        tempObj = item;
                        tempObj.no = (index + 1) + (page * limit);
                        if (tempObj.id != undefined && tempObj.id != null) {
                            tempObj.status = 'Update'
                        }
                        finalArr.push(tempObj);
                    });
                    if (finalArr != null && finalArr.length > 0) {

                    } else {
                        var tempObj = {};
                        tempObj.serviceUid = service.serviceUid;
                        tempObj.consolUid = service.consolUid;
                        tempObj.status = 'Generate';
                        if (service.documentList != null && service.documentList[0] != undefined && service.documentList[0].length != 0) {
                            tempObj.hawbNo = service.documentList[0].hawbNo;
                        }
                        tempObj.mawbNo = service.mawbNo;
                        tempObj.shipmentUid = service.shipmentUid;
                        finalArr.push(tempObj);
                    }
                    return finalArr;
                });

            } else {
                return null;
            }
        }


        //Aes View Page
        this.aesGenerateView = function(aesFile) {
            var fromScreen = {};
            fromScreen = $state.current.name;
            if (aesFile != undefined && aesFile != null) {
                ServiceGetByUid.get({
                    serviceUid: aesFile.serviceUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var serviceObj = data.responseObject;
                        if (serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                            $state.go('layout.aesView', {
                                aesId: aesFile.id,
                                fromScreen: $state.current.name,
                                shipmentUid: serviceObj.shipmentUid,
                                shipmentId: shipment.id
                            });
                        } else {
                            $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                            return false;
                        }
                    }

                }, function(error) {
                    console.log("Consol get Failed : " + error)
                });
            }
        }

        //Aes Add Page
        this.aesGenerateAdd = function(aesFile) {
            $rootScope.clientMessage = null;
            var fromScreen = {};
            fromScreen = $state.current.name;
            if (aesFile != undefined && aesFile != null) {
                ServiceGetByUid.get({
                    serviceUid: aesFile.serviceUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        serviceObj = data.responseObject;
                        if (serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                            var navAesObj = {
                                selectedPageNumber: detailTab == 'active' ? activeSearchDto.selectedPageNumber : searchDto.selectedPageNumber,
                                status: detailTab,
                                totalRecord: detailTab == 'active' ? totalRecordView : totalRecord,
                                shipmentId: shipment.id,
                                serviceUid: serviceObj.serviceUid,
                                shipmentUid: serviceObj.shipmentUid,
                                consolUid: serviceObj.consolUid,
                                fromState: $state.current.name,
                                fromScreen: fromScreen,
                                mawbNo: serviceObj.mawbNo,
                                hawbNo: serviceObj.documentList[0].hawbNo
                            };
                            $state.go("layout.addAes", {
                                navAesObj: navAesObj
                            });
                        } else {
                            $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                            return false;
                        }
                    }
                }, function(error) {
                    console.log("Consol get Failed : " + error)
                });
            }
        };


        //Go to shipment link
        this.goToShipmentViewByServiceUid = function(serviceuid) {
            ShipmentIdByServiceUid.get({
                    serviceuid: serviceuid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var param = {
                            shipmentId: data.responseObject,
                            fromState: $state.current.name,
                            fromStateParams: JSON.stringify($stateParams)

                        }
                        $state.go("layout.viewCrmShipment", param);
                    }
                },
                function(error) {
                    console.log("Shipment get Failed : " + error)
                });

        }




    }
]);