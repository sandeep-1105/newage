app.service('uiShipmentValidationService', ['$rootScope','uiShipmentDataService','ValidateUtil', 'CommonValidationService', 'appConstant', 'Notification',
    function($rootScope, uiShipmentDataService , ValidateUtil, CommonValidationService, appConstant, Notification) {
       this.validateShipmentService = function(shipment, shipmentServiceObj, index, code) {
          
		// SERVICE ROUTING VALIDATION STARTS HERE
		if(code == 0 || code == 1 || code == 1000) {
            if (shipmentServiceObj.serviceMaster == null || shipmentServiceObj.serviceMaster.id == undefined ||
                shipmentServiceObj.serviceMaster.id == null ||
                shipmentServiceObj.serviceMaster.id == "") {
                return this.validationResponse(true, "shipment.serviceDetail.serviceMaster."+index, $rootScope.nls["ERR90031"], index, "serviceRouting", undefined);
        } else {
            if (ValidateUtil.isStatusBlocked(shipmentServiceObj.serviceMaster.status)) {
                shipmentServiceObj.serviceMaster = null;
                return this.validationResponse(true, "shipment.serviceDetail.serviceMaster."+index, $rootScope.nls["ERR90032"], index, "serviceRouting", undefined);
            }
            if (ValidateUtil.isStatusHidden(shipmentServiceObj.serviceMaster.status)) {
                shipmentServiceObj.serviceMaster = null;
                return this.validationResponse(true, "shipment.serviceDetail.serviceMaster."+index, $rootScope.nls["ERR90033"], index, "serviceRouting", undefined);
            }

            for (var i = 0; i < shipment.shipmentServiceList.length; i++) {
                if (shipment.shipmentServiceList[i].lastUpdatedStatus != undefined &&
                    (shipment.shipmentServiceList[i].lastUpdatedStatus.serviceStatus == 'Cancelled' ||
                        shipment.shipmentServiceList[i].lastUpdatedStatus.serviceStatus == 'Closed')) {
                    continue;
            }
            if (index != i) {
                continue;
            } else if (shipment.shipmentServiceList[i].serviceMaster != undefined && shipment.shipmentServiceList[i].serviceMaster.id != shipmentServiceObj.serviceMaster.id) {
                return this.validationResponse(true, "shipment.serviceDetail.serviceMaster."+index, $rootScope.nls["ERR90033"], index, "serviceRouting", undefined);
            }
        }
    }
}

if (code == 0 || code == 2 || code == 1000) {

    if (shipmentServiceObj.party == undefined || shipmentServiceObj.party == null || shipmentServiceObj.party == "" ||
      shipmentServiceObj.party.id == null) {

        shipmentServiceObj.party = null;
    return this.validationResponse(true, "shipment.serviceDetail.party."+index, $rootScope.nls["ERR90002"], index, "serviceRouting", undefined);

} else {
   if (shipmentServiceObj.party.isDefaulter) {
    shipmentServiceObj.party = null;
    return this.validationResponse(true, "shipment.serviceDetail.party."+index, $rootScope.nls["ERR90584"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusBlocked(shipmentServiceObj.party.status)) {
    shipmentServiceObj.party = null;
    return this.validationResponse(true, "shipment.serviceDetail.party."+index, $rootScope.nls["ERR90003"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusHidden(shipmentServiceObj.party.status)) {
    shipmentServiceObj.party = null;
    return this.validationResponse(true, "shipment.serviceDetail.party."+index, $rootScope.nls["ERR90004"], index, "serviceRouting", undefined);
}
}
}

if (code == 0 || code == 3) {
    if (shipmentServiceObj.tosMaster==undefined || shipmentServiceObj.tosMaster == null ||
      shipmentServiceObj.tosMaster.id == null || shipmentServiceObj.tosMaster.id.trim == "") {
       shipmentServiceObj.tosMaster = null;
   return this.validationResponse(true, "shipment.serviceDetail.tos."+index, $rootScope.nls["ERR90015"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusBlocked(shipmentServiceObj.tosMaster.status)) {
   shipmentServiceObj.tosMaster = null;
   return this.validationResponse(true, "shipment.serviceDetail.tos."+index, $rootScope.nls["ERR90016"], index, "serviceRouting", undefined);
}
if (ValidateUtil.isStatusHidden(shipmentServiceObj.tosMaster.status)) {
   shipmentServiceObj.tosMaster = null;
   return this.validationResponse(true, "shipment.serviceDetail.tos."+index, $rootScope.nls["ERR90017"], index, "serviceRouting", undefined);
}
}

if (code == 0 || code == 4) {
    if (shipmentServiceObj.commodity==undefined || shipmentServiceObj.commodity.id == undefined  || shipmentServiceObj.commodity == null || shipmentServiceObj.commodity.id == null || shipmentServiceObj.commodity.id.trim == "") { 
       shipmentServiceObj.commodity=null;
       
       
   } else {

    if (ValidateUtil.isStatusBlocked(shipmentServiceObj.commodity.status)) {
       shipmentServiceObj.commodity = null; 
       return this.validationResponse(true, "shipment.serviceDetail.commodity."+index, $rootScope.nls["ERR90022"], index, "serviceRouting", undefined);
   }

   if (ValidateUtil.isStatusHidden(shipmentServiceObj.commodity.status)) {
       shipmentServiceObj.commodity = null;
       return this.validationResponse(true, "shipment.serviceDetail.commodity."+index, $rootScope.nls["ERR90023"], index, "serviceRouting", undefined);
   }
}
}

if(code == 0 || code == 5) {


    if (shipmentServiceObj.whoRouted == true && shipmentServiceObj.directShipment == false) {
        if (shipmentServiceObj.agent == null ||
          shipmentServiceObj.agent.id == null ||
          shipmentServiceObj.agent.id.trim == "") {
            return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90028"], index, "serviceRouting", undefined);
    }

    if (ValidateUtil.isStatusBlocked(shipmentServiceObj.agent.status)) {
       shipmentServiceObj.agent = null;
       return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90029"], index, "serviceRouting", undefined);
   }
   if (ValidateUtil.isStatusHidden(shipmentServiceObj.agent.status)) {
       shipmentServiceObj.agent = null;
       return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90030"], index, "serviceRouting", undefined);
   }
}

if (shipmentServiceObj.whoRouted == true && shipmentServiceObj.directShipment == true) {
    if (shipmentServiceObj.agent != null &&
      shipmentServiceObj.agent.id != null &&
      shipmentServiceObj.agent.id.trim != "") {
        if (ValidateUtil.isStatusBlocked(shipmentServiceObj.agent.status)) {
           shipmentServiceObj.agent = null;
           return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90029"], index, "serviceRouting", undefined);
       }
       if (ValidateUtil.isStatusHidden(shipmentServiceObj.agent.status)) {
           shipmentServiceObj.agent = null;
           return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90030"], index, "serviceRouting", undefined);
       }
   }
}

if (shipmentServiceObj.whoRouted == false) {

    if (shipmentServiceObj.salesman ==undefined || shipmentServiceObj.salesman == null ||
      shipmentServiceObj.salesman.id == null ||
      shipmentServiceObj.salesman.id.trim == "") {
        return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90024"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isEmployeeResigned(shipmentServiceObj.salesman.employementStatus)) {
   shipmentServiceObj.salesman = null;
   return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90025"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isEmployeeTerminated(shipmentServiceObj.salesman.employementStatus)) {
   shipmentServiceObj.salesman = null;
   return this.validationResponse(true, "shipment.serviceDetail.routedby."+index, $rootScope.nls["ERR90026"], index, "serviceRouting", undefined);
}

}

}





if(code == 0 || code == 6 || code == 1000) {

   if (shipmentServiceObj.origin == null ||
     shipmentServiceObj.origin.id == null ||
     shipmentServiceObj.origin.id.trim == "") {
    return this.validationResponse(true, "shipment.serviceDetail.origin."+index, $rootScope.nls["ERR90013"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusBlocked(shipmentServiceObj.origin.status)) {
   shipmentServiceObj.origin = null;
   return this.validationResponse(true, "shipment.serviceDetail.origin."+index, $rootScope.nls["ERR90037"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusHidden(shipmentServiceObj.origin.status)) {
   shipmentServiceObj.origin = null;
   return this.validationResponse(true, "shipment.serviceDetail.origin."+index, $rootScope.nls["ERR90038"], index, "serviceRouting", undefined);
}

}



if(code == 0 || code == 7 || code == 1000) {
 if (shipmentServiceObj.pol == null || shipmentServiceObj.pol.id == null || shipmentServiceObj.pol.id.trim == "") {
    shipmentServiceObj.pol = null;
    return this.validationResponse(true, "shipment.serviceDetail.pol."+index, $rootScope.nls["ERR90041"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusBlocked(shipmentServiceObj.pol.status)) {
  shipmentServiceObj.pol = null;
  return this.validationResponse(true, "shipment.serviceDetail.pol."+index, $rootScope.nls["ERR90042"], index, "serviceRouting", undefined);
}

if (ValidateUtil.isStatusHidden(shipmentServiceObj.pol.status)) {
  shipmentServiceObj.pol = null;
  return this.validationResponse(true, "shipment.serviceDetail.pol."+index, $rootScope.nls["ERR90043"], index, "serviceRouting", undefined);
}
}


if(code == 0 || code == 8 || code == 1000) {
   
   if (shipmentServiceObj.pod == null || shipmentServiceObj.pod.id == null || shipmentServiceObj.pod.id.trim == "") {
       return this.validationResponse(true, "shipment.serviceDetail.pod."+index, $rootScope.nls["ERR90044"], index, "serviceRouting", undefined);
   } else {
      if (ValidateUtil.isStatusBlocked(shipmentServiceObj.pod.status)) {
         shipmentServiceObj.pod = null;
         return this.validationResponse(true, "shipment.serviceDetail.pod."+index, $rootScope.nls["ERR90045"], index, "serviceRouting", undefined);
     }

     if (ValidateUtil.isStatusHidden(shipmentServiceObj.pod.status)) {
      shipmentServiceObj.pod = null;
      return this.validationResponse(true, "shipment.serviceDetail.pod."+index, $rootScope.nls["ERR90046"], index, "serviceRouting", undefined);
  }
}
}

if(code == 0 || code == 9 || code == 1000) {
   
   
   if (shipmentServiceObj.destination == null || shipmentServiceObj.destination.id == null || shipmentServiceObj.destination.id.trim == "") {
    return this.validationResponse(true, "shipment.serviceDetail.destination."+index, $rootScope.nls["ERR90014"], index, "serviceRouting", undefined);
} else {

    if (ValidateUtil.isStatusBlocked(shipmentServiceObj.destination.status)) {
       shipmentServiceObj.destination = null;
       return this.validationResponse(true, "shipment.serviceDetail.destination."+index, $rootScope.nls["ERR90039"], index, "serviceRouting", undefined);
   }

   if (ValidateUtil.isStatusHidden(shipmentServiceObj.destination.status)) {
       shipmentServiceObj.destination = null;
       return this.validationResponse(true, "shipment.serviceDetail.destination."+index, $rootScope.nls["ERR90040"], index, "serviceRouting", undefined);
   }
}

}






if(code == 0 || code == 10) {
    if (shipmentServiceObj.carrier != undefined && shipmentServiceObj.carrier != null && shipmentServiceObj.carrier.id != null) {
       if (shipmentServiceObj.carrier.status != null) {
          if (ValidateUtil.isStatusBlocked(shipmentServiceObj.carrier.status)) {
             shipmentServiceObj.carrier = null;
             return this.validationResponse(true, "shipment.serviceDetail.carrier."+index, $rootScope.nls["ERR90064"], index, "serviceRouting", undefined);
         }
         if (ValidateUtil.isStatusHidden(shipmentServiceObj.carrier.status)) {
           shipmentServiceObj.carrier = null;
           return this.validationResponse(true, "shipment.serviceDetail.carrier."+index, $rootScope.nls["ERR90065"], index, "serviceRouting", undefined);
       }
   }
}else{
   if($rootScope.isGoodsReceived(shipmentServiceObj)){
      
      if (shipmentServiceObj.carrier == undefined ||  shipmentServiceObj.carrier == "" ||  shipmentServiceObj.carrier == null || shipmentServiceObj.carrier.id==undefined) {
       return this.validationResponse(true, "shipment.serviceDetail.carrier."+index, $rootScope.nls["ERR90063"], index, "serviceRouting", undefined);
   }	
}else{
  shipmentServiceObj.carrier=null;	
}
}
}



if (code == 0 || code == 11) {
    if (shipmentServiceObj.routeNo != undefined && shipmentServiceObj.routeNo != "" && shipmentServiceObj.routeNo != null) {

       if (shipmentServiceObj.routeNo == 0) {
        return this.validationResponse(true, "shipment.serviceDetail.flightNo."+index, $rootScope.nls["ERR90066"], index, "serviceRouting", undefined);
    }
    if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_FLIGHT_NUMBER,shipmentServiceObj.routeNo)){
       return this.validationResponse(true, "shipment.serviceDetail.flightNo."+index, $rootScope.nls["ERR90066"], index, "serviceRouting", undefined);
   }
}else{
   if($rootScope.isGoodsReceived(shipmentServiceObj)){
      
      if (shipmentServiceObj.routeNo == undefined ||  shipmentServiceObj.routeNo == "" ||  shipmentServiceObj.routeNo == null) {
       return this.validationResponse(true, "shipment.serviceDetail.flightNo."+index, $rootScope.nls["ERR90067"], index, "serviceRouting", undefined);
   }	
}else{
  shipmentServiceObj.routeNo=null;	
}
}
}




if(code == 0 || code == 12) {

    if (shipmentServiceObj.mawbNo != undefined && shipmentServiceObj.mawbNo != "" && shipmentServiceObj.mawbNo != null) {
        if (shipmentServiceObj.carrier == undefined || shipmentServiceObj.carrier == null || shipmentServiceObj.carrier == "" || shipmentServiceObj.carrier.id == undefined) {
            return this.validationResponse(true, "shipment.serviceDetail.mawb."+index, $rootScope.nls["ERR90063"], index, "serviceRouting", undefined);
        }
        if (isNaN(shipmentServiceObj.mawbNo)==true) {
            return this.validationResponse(true, "shipment.serviceDetail.mawb."+index, $rootScope.nls["ERR90068"], index, "serviceRouting", undefined);
        }
        
        if (shipmentServiceObj.mawbNo == 0) {
            return this.validationResponse(true, "shipment.serviceDetail.mawb."+index, $rootScope.nls["ERR90068"], index, "serviceRouting", undefined);
        }
        if (shipmentServiceObj.mawbNo.length!=11) {
            return this.validationResponse(true, "shipment.serviceDetail.mawb."+index, $rootScope.nls["ERR96523"], index, "serviceRouting", undefined);
        }
        var check = $rootScope.validateMawb(shipmentServiceObj.carrier, shipmentServiceObj.mawbNo);
        if (check != "S") {
           var eMsg = "";
           if (check != "F") {
               eMsg = $rootScope.nls["ERR90216"] + "(" + check + ")";
           } else {
               eMsg = $rootScope.nls["ERR90068"];
           }
           Notification.error(eMsg);
           return this.validationResponse(true, "shipment.serviceDetail.mawb."+index, eMsg, index, "serviceRouting", undefined);
       }
   }
   
}

        //ETD Validation
        if(code==0 || code==13){
        	if($rootScope.isGoodsReceived(shipmentServiceObj)){
        		if(shipmentServiceObj.etd==undefined || shipmentServiceObj.etd==null || shipmentServiceObj.etd==''){
        			return this.validationResponse(true, "shipment.serviceDetail.etd."+index, $rootScope.nls["ERR90094"], index, "serviceRouting", undefined);
               }
           }
       }

       //ETA Validation
       if(code==0 || code==14){
           
           if($rootScope.isGoodsReceived(shipmentServiceObj)){
              if(shipmentServiceObj.eta==undefined || shipmentServiceObj.eta==null || shipmentServiceObj.eta==''){
                 return this.validationResponse(true, "shipment.serviceDetail.eta."+index, $rootScope.nls["ERR90095"], index, "serviceRouting", undefined);
             }
         }
         
     }
        //Comparsion of eta and etd
        if(code==0 || code==15){
        	
         if (shipmentServiceObj.eta != null && shipmentServiceObj.etd != null) {
          var etd = $rootScope.convertToDate(shipmentServiceObj.etd);
          var eta = $rootScope.convertToDate(shipmentServiceObj.eta);
          if (etd > eta) {
              return this.validationResponse(true, "shipment.serviceDetail.etd."+index, $rootScope.nls["ERR90096"], index, "serviceRouting", undefined);
          }
      }
  }
  
  
  

//Cargo Validation Starts

if(code==0 || code==101){
 
    if (shipmentServiceObj.customerService == undefined ||
        shipmentServiceObj.customerService == null ||
        shipmentServiceObj.customerService == "" ||
        shipmentServiceObj.customerService.id == undefined) {
       return this.validationResponse(true, "shipment.serviceDetail.bookingPerson."+index, $rootScope.nls["ERR90034"], index, "cargoDetails", undefined);
} else if (shipmentServiceObj.customerService.status != null) {
    if (ValidateUtil.isEmployeeResigned(shipmentServiceObj.customerService.employementStatus)) {
       shipmentServiceObj.customerService = null;
       return this.validationResponse(true, "shipment.serviceDetail.bookingPerson."+index, $rootScope.nls["ERR90035"], index, "cargoDetails", undefined);
   }

   if (ValidateUtil.isEmployeeTerminated(shipmentServiceObj.customerService.employementStatus)) {
    shipmentServiceObj.customerService = null;
    return this.validationResponse(true, "shipment.serviceDetail.bookingPerson."+index, $rootScope.nls["ERR90036"], index, "cargoDetails", undefined);
}
}else{
   console.log("nothing..");
}
}


if(code==0 || code==102){
 
   if (shipmentServiceObj.coLoader != undefined &&
      shipmentServiceObj.coLoader != null &&
      shipmentServiceObj.coLoader != "" &&
      shipmentServiceObj.coLoader.id != null) {

      if (shipmentServiceObj.coLoader.status != null) {
         if (shipmentServiceObj.coLoader.isDefaulter) {
            shipmentServiceObj.coLoader = null;
            return this.validationResponse(true, "shipment.serviceDetail.coLoader."+index, $rootScope.nls["ERR90611"], index, "cargoDetails", undefined);
        }
        
        if (ValidateUtil.isStatusBlocked(shipmentServiceObj.coLoader.status)) {
         shipmentServiceObj.coLoader = null;
         return this.validationResponse(true, "shipment.serviceDetail.coLoader."+index, $rootScope.nls["ERR90048"], index, "cargoDetails", undefined);
     }

     if (ValidateUtil.isStatusHidden(shipmentServiceObj.coLoader.status)) {
         shipmentServiceObj.coLoader = null;
         return this.validationResponse(true, "shipment.serviceDetail.coLoader."+index, $rootScope.nls["ERR90049"], index, "cargoDetails", undefined);
     }
 }
}else{
 shipmentServiceObj.coLoader=null;
}
}




if(code==0 || code==103){
 
 if (shipmentServiceObj.division != undefined &&
   shipmentServiceObj.division != null &&
   shipmentServiceObj.division != "" &&
   shipmentServiceObj.division.id != null) {
   if (shipmentServiceObj.division.status != null) {
       if (ValidateUtil.isStatusBlocked(shipmentServiceObj.division.status)) {
           shipmentServiceObj.division = null;
           return this.validationResponse(true, "shipment.serviceDetail.division."+index, $rootScope.nls["ERR90054"], index, "cargoDetails", undefined);
       }
       if (ValidateUtil.isStatusHidden(shipmentServiceObj.division.status)) {
           shipmentServiceObj.division = null;
           return this.validationResponse(true, "shipment.serviceDetail.division."+index, $rootScope.nls["ERR90055"], index, "cargoDetails", undefined);
       }
   }
}else{
  shipmentServiceObj.division=null;
}

}

if(code==0 || code==104){
 
 if (shipmentServiceObj.project != undefined &&
   shipmentServiceObj.project != null &&
   shipmentServiceObj.project != "" &&
   shipmentServiceObj.project.id != null) {
   if (shipmentServiceObj.project.status != null) {
       if (ValidateUtil.isStatusBlocked(shipmentServiceObj.project.status)) {
           shipmentServiceObj.project = null;
           return this.validationResponse(true, "shipment.serviceDetail.project."+index, $rootScope.nls["ERR90018"], index, "cargoDetails", undefined);
       }
       if (ValidateUtil.isStatusHidden(shipmentServiceObj.project.status)) {
           shipmentServiceObj.project = null;
           return this.validationResponse(true, "shipment.serviceDetail.project."+index, $rootScope.nls["ERR90019"], index, "cargoDetails", undefined);
       }
   }
}else{
  shipmentServiceObj.project=null;
}

}

if(code==0 || code==105){
 
 if(shipmentServiceObj.holdShipment){
    if(shipmentServiceObj.holdNote==undefined
      || shipmentServiceObj.holdNote==null
      || shipmentServiceObj.holdNote==''){
       shipmentServiceObj.holdNote = null;
   return this.validationResponse(true, "shipment.serviceDetail.holdNote."+index, $rootScope.nls["ERR90217"], index, "cargoDetails", undefined);
}
}else{
    if(shipmentServiceObj.holdNote!=undefined
      && shipmentServiceObj.holdNote!=null
      && shipmentServiceObj.holdNote!=''){
       
       if(shipmentServiceObj.holdReleaseNote==undefined
         || shipmentServiceObj.holdReleaseNote==null
         || shipmentServiceObj.holdReleaseNote==''){
          shipmentServiceObj.holdReleaseNote = null;
      return this.validationResponse(true, "shipment.serviceDetail.holdReleaseNote."+index, $rootScope.nls["ERR90218"], index, "cargoDetails", undefined);
  }
  
}
}



}


		/*if(code==0 || code==106){
			
			
			 if (shipmentServiceObj.bookedPieces != null && shipmentServiceObj.bookedPieces != undefined && shipmentServiceObj.bookedPieces != "") {
	            	if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,shipmentServiceObj.bookedPieces)){
	            		return this.validationResponse(true, "shipment.serviceDetail.serNoOfPieces."+index, $rootScope.nls["ERR90097"], index, "cargoDetails", undefined);
					}
	            }
	            if (shipmentServiceObj.bookedPieces != null && shipmentServiceObj.bookedPieces != undefined && shipmentServiceObj.bookedPieces != "") {
	                if (shipmentServiceObj.bookedPieces < 0 || shipmentServiceObj.bookedPieces >= 99999999999) {
	                	return this.validationResponse(true, "shipment.serviceDetail.serNoOfPieces."+index, $rootScope.nls["ERR90097"], index, "cargoDetails", undefined);
	                }
	            }
			
          }*/
          
		/*if(code==0 || code==107){
			
			if (shipmentServiceObj.bookedGrossWeightUnitPound != null && shipmentServiceObj.bookedGrossWeightUnitPound != undefined && shipmentServiceObj.bookedGrossWeightUnitPound != "") {
                if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_POUND,shipmentServiceObj.bookedGrossWeightUnitPound)){
                	return this.validationResponse(true, "shipment.serviceDetail.grossPound."+index, $rootScope.nls["ERR90575"], index, "cargoDetails", undefined);
				}
            }
            if (shipmentServiceObj.bookedGrossWeightUnitPound != null && shipmentServiceObj.bookedGrossWeightUnitPound != undefined && shipmentServiceObj.bookedGrossWeightUnitPound != "") {
                if (shipmentServiceObj.bookedGrossWeightUnitPound < 0 || shipmentServiceObj.bookedGrossWeightUnitPound >= 99999999999.999) {
                	return this.validationResponse(true, "shipment.serviceDetail.grossPound."+index, $rootScope.nls["ERR90575"], index, "cargoDetails", undefined);
                }
            }
            uiShipmentDataService.chargableWeightCalulcate(shipmentServiceObj,'grossPound');

        }*/
        
		/*if(code==0 || code==108){
			
            if (shipmentServiceObj.bookedVolumeWeightUnitPound != null && shipmentServiceObj.bookedVolumeWeightUnitPound != undefined && shipmentServiceObj.bookedVolumeWeightUnitPound != "") {
                if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_POUND,shipmentServiceObj.bookedVolumeWeightUnitPound)){
                	return this.validationResponse(true, "shipment.serviceDetail.volumePound."+index, $rootScope.nls["ERR90576"], index, "cargoDetails", undefined);
				}
            }
            if (shipmentServiceObj.bookedVolumeWeightUnitPound != null && shipmentServiceObj.bookedVolumeWeightUnitPound != undefined && shipmentServiceObj.bookedVolumeWeightUnitPound != "") {
                if (shipmentServiceObj.bookedVolumeWeightUnitPound < 0 || shipmentServiceObj.bookedVolumeWeightUnitPound >= 99999999999.999) {
                    return this.validationResponse(true, "shipment.serviceDetail.volumePound."+index, $rootScope.nls["ERR90576"], index, "cargoDetails", undefined);
                }
            }
            uiShipmentDataService.chargableWeightCalulcate(shipmentServiceObj,'volumePound');
        }*/
        
		/*if(code==0 || code==109){
			
			  if (shipmentServiceObj.bookedGrossWeightUnitKg != null && shipmentServiceObj.bookedGrossWeightUnitKg != undefined && shipmentServiceObj.bookedGrossWeightUnitKg != "") {
	                if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG,shipmentServiceObj.bookedGrossWeightUnitKg)){
	                	 return this.validationResponse(true, "shipment.serviceDetail.grossKg."+index, $rootScope.nls["ERR90098"], index, "cargoDetails", undefined);
					}
	            }
	            if (shipmentServiceObj.bookedGrossWeightUnitKg != null && shipmentServiceObj.bookedGrossWeightUnitKg != undefined && shipmentServiceObj.bookedGrossWeightUnitKg != ""){
	            	if (shipmentServiceObj.bookedGrossWeightUnitKg < 0 || shipmentServiceObj.bookedGrossWeightUnitKg >= 99999999999.99) {
	                    return this.validationResponse(true, "shipment.serviceDetail.grossKg."+index, $rootScope.nls["ERR90098"], index, "cargoDetails", undefined);
	                }
	            	
	            }
	            uiShipmentDataService.chargableWeightCalulcate(shipmentServiceObj,'grossKg');
          }*/
          
          
          
	/*	if(code==0 || code==110){
			
			 if (shipmentServiceObj.bookedVolumeWeightUnitKg != null && shipmentServiceObj.bookedVolumeWeightUnitKg != undefined && shipmentServiceObj.bookedVolumeWeightUnitKg != "") {
	                if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG,shipmentServiceObj.bookedVolumeWeightUnitKg)){
	                	 return this.validationResponse(true, "shipment.serviceDetail.volumeKg."+index, $rootScope.nls["ERR90213"], index, "cargoDetails", undefined);
					}
	            }
	            if (shipmentServiceObj.bookedVolumeWeightUnitKg != null && shipmentServiceObj.bookedVolumeWeightUnitKg != undefined && shipmentServiceObj.bookedVolumeWeightUnitKg != "") {
	                if (shipmentServiceObj.bookedVolumeWeightUnitKg < 0 || shipmentServiceObj.bookedVolumeWeightUnitKg >= 99999999999.99) {
	                	 return this.validationResponse(true, "shipment.serviceDetail.volumeKg."+index, $rootScope.nls["ERR90213"], index, "cargoDetails", undefined);
	                }
	            }
	            uiShipmentDataService.chargableWeightCalulcate(shipmentServiceObj,'volumekg');

          }*/
          
          
		/*if(code==0 || code==112){
			
			 if (!isNaN(parseFloat(shipmentServiceObj.bookedVolumeUnitCbm))) {
		            if (parseFloat(shipmentServiceObj.bookedVolumeUnitCbm)!=null && shipmentServiceObj.bookedVolumeUnitCbm != null && shipmentServiceObj.bookedVolumeUnitCbm != undefined && shipmentServiceObj.bookedVolumeUnitCbm != "") {
		                if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM,shipmentServiceObj.bookedVolumeUnitCbm)){
		                	 return this.validationResponse(true, "shipment.serviceDetail.volumecbm." +index, $rootScope.nls["ERR90559"], index, "cargoDetails", undefined);
						}
		            }
		            if (parseFloat(shipmentServiceObj.bookedVolumeUnitCbm)!=null && shipmentServiceObj.bookedVolumeUnitCbm != null && shipmentServiceObj.bookedVolumeUnitCbm != undefined && shipmentServiceObj.bookedVolumeUnitCbm != "") {
		                if (shipmentServiceObj.bookedVolumeUnitCbm < 0 || shipmentServiceObj.bookedVolumeUnitCbm >= 99999999999.999) {
		                	return this.validationResponse(true, "shipment.serviceDetail.volumecbm." +index, $rootScope.nls["ERR90559"], index, "cargoDetails", undefined);
		                }
		            }
		        }else{
		        	shipmentServiceObj.bookedVolumeUnitCbm=null;
		        }
			
			
          }*/
          
          
          
          
		//Cargo Validation ends
		
	//AES AND RATE VALIDATION
  if(code==0 || code==500){
     
     if(shipmentServiceObj.aesNo!=undefined && shipmentServiceObj.aesNo!=null &&shipmentServiceObj.aesNo!=""){
      if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NO,shipmentServiceObj.aesNo)){
        return this.validationResponse(true,"shipment.serviceDetail.aes."+index, $rootScope.nls["ERR2012010"], index, "aes", undefined);
    }
}else{
  shipmentServiceObj.aesNo=null;
}

}

if(code==0 || code==501){
 
  if(shipmentServiceObj.brokerageParty != undefined &&
    shipmentServiceObj.brokerageParty != null && shipmentServiceObj.brokerageParty != "" &&
    shipmentServiceObj.brokerageParty.id != null && shipmentServiceObj.brokerageParty.status != null) {
     
   if (shipmentServiceObj.brokerageParty.isDefaulter) {
      shipmentServiceObj.brokerageParty = null;
      return this.validationResponse(true,"shipment.serviceDetail.brokerageParty."+index, $rootScope.nls["ERR90585"], index, "aes", undefined);
  }
  
  if (ValidateUtil.isStatusBlocked(shipmentServiceObj.brokerageParty.status)) {
   shipmentServiceObj.brokerageParty = null;
   return this.validationResponse(true,"shipment.serviceDetail.brokerageParty."+index, $rootScope.nls["ERR90564"], index, "aes", undefined);
}

if (ValidateUtil.isStatusHidden(shipmentServiceObj.brokerageParty.status)) {
   shipmentServiceObj.brokerageParty = null;
   return this.validationResponse(true,"shipment.serviceDetail.brokerageParty."+index, $rootScope.nls["ERR90565"], index, "aes", undefined);
}
}else{
   shipmentServiceObj.brokerageParty=null;
}
}


if(code==0 || code==502){
 
 if(shipmentServiceObj.brokeragePercentage != undefined &&
   shipmentServiceObj.brokeragePercentage != null && shipmentServiceObj.brokeragePercentage != "") {
   if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_SHIPMENT_BROKERAGE_PERCENTAGE,shipmentServiceObj.brokeragePercentage)){
     return this.validationResponse(true,"shipment.serviceDetail.brokeragePercentage."+index, $rootScope.nls["ERR90566"], index, "aes", undefined);
 }
}else{
  shipmentServiceObj.brokeragePercentage=null;
}
}


		// SERVICE ROUTING VALIDATION ENDS HERE
		return this.validationSuccesResponse();
	}
	
	this.validateDocumentService = function(documentDetail, serviceIdx, docIdx, code, shipmentService) {
		if(code == 0 || code == 312) {
            if (documentDetail.packMaster != undefined && documentDetail.packMaster != null &&
                documentDetail.packMaster != "" && documentDetail.packMaster.id != null) {

                if (documentDetail.packMaster.status != null) {
                    if (ValidateUtil.isStatusBlocked(documentDetail.packMaster.status)) {
                        documentDetail.packMaster = null;
                        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.packMaster."+docIdx, $rootScope.nls["ERR90057"], serviceIdx, "cargoDetails", undefined);
                    }
                    if (ValidateUtil.isStatusHidden(documentDetail.packMaster.status)) {
                        documentDetail.packMaster = null;
                        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.packMaster."+docIdx, $rootScope.nls["ERR90058"], serviceIdx, "cargoDetails", undefined);
                    }
                }
            }
        }

        if(code == 0 || code == 313) {
        	/*if(shipmentService.serviceMaster.importExport=='Export'){*/
        		if (documentDetail.noOfPieces == undefined || documentDetail.noOfPieces == "" || documentDetail.noOfPieces == null) {
                    return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.noOfPieces."+docIdx, $rootScope.nls["ERR90473"], serviceIdx, "cargoDetails", undefined);
                } else {
                    if (documentDetail.noOfPieces == 0) {
                        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.noOfPieces."+docIdx, $rootScope.nls["ERR90474"], serviceIdx, "cargoDetails", undefined);
                    }
                    if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,documentDetail.noOfPieces)){
                        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.noOfPieces."+docIdx, $rootScope.nls["ERR90474"], serviceIdx, "cargoDetails", undefined);
                    }
                }
                /*}*/
            }
            

            if(code == 0 || code == 314) {
               
               if(isNaN(documentDetail.ratePerCharge)){
                  documentDetail.ratePerCharge=null;
              }
              if (documentDetail.ratePerCharge == undefined || documentDetail.ratePerCharge == null || documentDetail.ratePerCharge == "") {
               
               
              } else {
                if (documentDetail.ratePerCharge <= 0 || documentDetail.ratePerCharge > 9999999999.999999) {
                    return this.validationResponse(true,'shipment.serviceDetail.'+serviceIdx+'.document.0.ratePerCharge', $rootScope.nls["ERR90556"], serviceIdx, "cargoDetails", undefined);
                }
                
                if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_RATE_CLASS,documentDetail.ratePerCharge)){
                    return this.validationResponse(true,'shipment.serviceDetail.'+serviceIdx+'.document.0.ratePerCharge', $rootScope.nls["ERR90556"], serviceIdx, "cargoDetails", undefined);
                }
            }
        }
        
        if(code == 0 || code == 315) {
         
         /*if(shipmentService.serviceMaster.importExport=='Export'){*/
          if(documentDetail.dimensionUnit) {
             if(isNaN(documentDetail.grossWeightInPound)){
                documentDetail.grossWeightInPound = null;
            }
            if (documentDetail.grossWeightInPound == undefined || documentDetail.grossWeightInPound == null || documentDetail.grossWeightInPound == "") {
             documentDetail.grossWeight=0;
             if (documentDetail.volumeWeightInPound == undefined || documentDetail.volumeWeightInPound == null || documentDetail.volumeWeightInPound == "") {
                documentDetail.chargebleWeight=0;
            }
            return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.grossWeightLBS."+docIdx, $rootScope.nls["ERR90477"], serviceIdx, "cargoDetails", undefined);
        } else {
           if (documentDetail.grossWeightInPound <= 0 || documentDetail.grossWeightInPound > 99999999999.99) {
              return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.grossWeightLBS."+docIdx, $rootScope.nls["ERR90478"], serviceIdx, "cargoDetails", undefined);
          }
          if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_POUND,documentDetail.grossWeightInPound)){
             return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.grossWeightLBS."+docIdx, $rootScope.nls["ERR90478"], serviceIdx, "cargoDetails", undefined);
         }
//		                $scope.changeDocumentDimension(documentDetail,$scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
}
console.log("Gross Weight in Pound validation done...");
}
/*}*/

}

if(code == 0 || code == 316) {
 /*if(shipmentService.serviceMaster.importExport=='Export'){*/
  if(documentDetail.dimensionUnit) {
     if(isNaN(documentDetail.volumeWeightInPound)){
        documentDetail.volumeWeightInPound = null;
    }
    if (documentDetail.volumeWeightInPound == undefined || documentDetail.volumeWeightInPound == null || documentDetail.volumeWeightInPound == "") {
        if (documentDetail.grossWeightInPound == undefined || documentDetail.grossWeightInPound == null || documentDetail.grossWeightInPound == "") {
            documentDetail.chargebleWeight=0;
        }
    } else {
        if (documentDetail.volumeWeightInPound <= 0 || documentDetail.volumeWeightInPound > 99999999999.99) {
           return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.volumeWeightLBS."+docIdx, $rootScope.nls["ERR90482"], serviceIdx, "cargoDetails", undefined);
       }
       if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_POUND,documentDetail.volumeWeightInPound)){
           return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.volumeWeightLBS."+docIdx, $rootScope.nls["ERR90482"], serviceIdx, "cargoDetails", undefined);
       }
//           	                $scope.changeDocumentDimension(documentDetail,$scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
}
console.log("Volume Weight pound validation done...");
}
/*}*/
}
if(code == 0 || code == 317) {
 
 if(isNaN(documentDetail.bookedVolumeUnitCbm)){
    documentDetail.bookedVolumeUnitCbm = 0.0;
}

if (documentDetail.bookedVolumeUnitCbm != null && documentDetail.bookedVolumeUnitCbm != undefined && documentDetail.bookedVolumeUnitCbm != "") {
    if (documentDetail.bookedVolumeUnitCbm <= 0 || documentDetail.bookedVolumeUnitCbm > 9999999999.999) {
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.volumeCBM."+docIdx, $rootScope.nls["ERR90559"], serviceIdx, "cargoDetails", undefined);
    }
    if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM,documentDetail.bookedVolumeUnitCbm)){
       return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.volumeCBM."+docIdx, $rootScope.nls["ERR90559"], serviceIdx, "cargoDetails", undefined);
   }
}


}
if(code == 0 || code == 318) {
 /*if(shipmentService.serviceMaster.importExport=='Export'){*/
    if(isNaN(documentDetail.grossWeight)){
        documentDetail.grossWeight = null;
    }
    if (documentDetail.grossWeight == undefined || documentDetail.grossWeight == null || documentDetail.grossWeight == "") {
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.grossWeightKg."+docIdx, $rootScope.nls["ERR90475"], serviceIdx, "cargoDetails", undefined);
    } else {
       
        if (documentDetail.grossWeight <= 0 || documentDetail.grossWeight > 99999999999.99) {
            return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.grossWeightKg."+docIdx, $rootScope.nls["ERR90476"], serviceIdx, "cargoDetails", undefined);
        }
        
        if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG,documentDetail.grossWeight)){
          return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.grossWeightKg."+docIdx, $rootScope.nls["ERR90476"], serviceIdx, "cargoDetails", undefined);
      }
      
//                        $scope.changeDocumentDimension(documentDetail,$scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
}
/*}*/
console.log("Gross Weight in Kg validation done...");
}
if(code == 0 || code == 319) {
 /*if(shipmentService.serviceMaster.importExport=='Export'){*/
     if(isNaN(documentDetail.volumeWeight)){
        documentDetail.volumeWeight = null;
    }
    if (documentDetail.volumeWeight != undefined && documentDetail.volumeWeight != null && documentDetail.volumeWeight != "") {
        
       if (documentDetail.volumeWeight <= 0 || documentDetail.volumeWeight > 99999999999.99) {
           return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.volumeWeightKg."+docIdx, $rootScope.nls["ERR90480"], serviceIdx, "cargoDetails", undefined);
       }
       if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG,documentDetail.volumeWeight)){
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.volumeWeightKg."+docIdx, $rootScope.nls["ERR90480"], serviceIdx, "cargoDetails", undefined);
    }
}
/* 	}*/
}

if(code == 0 || code == 300) {
    if (documentDetail.shipper == undefined || documentDetail.shipper.id == undefined || documentDetail.shipper == null || documentDetail.shipper == "") {
        return this.validationResponse(true, "shipment.serviceDetail."+serviceIdx+".document.shipper."+docIdx, $rootScope.nls["ERR90106"], serviceIdx, "hawb", undefined);

    } else {
       if (documentDetail.shipper.isDefaulter) {
        documentDetail.shipper = {};
        documentDetail.shipperAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.shipper."+docIdx, $rootScope.nls["ERR90586"], serviceIdx, "hawb", undefined);
    }
    
    if (ValidateUtil.isStatusBlocked(documentDetail.shipper.status)) {
        documentDetail.shipper = {};
        documentDetail.shipperAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.shipper."+docIdx, $rootScope.nls["ERR90107"], serviceIdx, "hawb", undefined);
    }
    
    if (ValidateUtil.isStatusHidden(documentDetail.shipper.status)) {
        documentDetail.shipper = {};
        documentDetail.shipperAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.shipper."+docIdx, $rootScope.nls["ERR90108"], serviceIdx, "hawb", undefined);
    }

}
}
if(code == 0 || code == 301) {
 
}
if(code == 0 || code == 302) {
    if (documentDetail.forwarder != undefined && documentDetail.forwarder != null && documentDetail.forwarder != "") {
      if (documentDetail.forwarder.isDefaulter) {
       documentDetail.forwarder = {};
       documentDetail.forwarderAddress = {};
       return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.forwarder."+docIdx, $rootScope.nls["ERR90591"], serviceIdx, "hawb", undefined);
   }
   
   if (ValidateUtil.isStatusBlocked(documentDetail.forwarder.status)) {
    documentDetail.forwarder = {};
    documentDetail.forwarderAddress = {};
    return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.forwarder."+docIdx, $rootScope.nls["ERR90120"], serviceIdx, "hawb", undefined);
}

if (ValidateUtil.isStatusHidden(documentDetail.forwarder.status)) {
    documentDetail.forwarder = null;
    documentDetail.forwarderAddress = {};
    return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.forwarder."+docIdx, $rootScope.nls["ERR90121"], serviceIdx, "hawb", undefined);
}
}
}
if(code == 0 || code == 303) {
 
}
if(code == 0 || code == 304) {

    if (documentDetail.consignee == undefined || documentDetail.consignee.id == undefined || documentDetail.consignee == null || documentDetail.consignee == "") {
       return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.consignee."+docIdx, $rootScope.nls["ERR90132"], serviceIdx, "hawb", undefined);
   } else {
     
       if (documentDetail.consignee.isDefaulter) {
        documentDetail.consignee = {};
        documentDetail.consigneeAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.consignee."+docIdx, $rootScope.nls["ERR90587"], serviceIdx, "hawb", undefined);
    }
    if (ValidateUtil.isStatusBlocked(documentDetail.consignee.status)) {
        documentDetail.consignee = {};
        documentDetail.consigneeAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.consignee."+docIdx, $rootScope.nls["ERR90133"], serviceIdx, "hawb", undefined);

    }
    if (ValidateUtil.isStatusHidden(documentDetail.consignee.status)) {
        documentDetail.consignee = {};
        documentDetail.consigneeAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.consignee."+docIdx, $rootScope.nls["ERR90134"], serviceIdx, "hawb", undefined);
    }
}


}
if(code == 0 || code == 305) {
 
}
if(code == 0 || code == 306) {
   if (documentDetail.firstNotify == undefined || documentDetail.firstNotify.id == undefined || documentDetail.firstNotify == null || documentDetail.firstNotify == "") {
      documentDetail.firstNotifyAddress = {};
  } else {
   
   if (documentDetail.firstNotify.isDefaulter) {
    documentDetail.firstNotify = {};
    documentDetail.firstNotifyAddress = {};
    return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.firstNotify."+docIdx, $rootScope.nls["ERR90588"], serviceIdx, "hawb", undefined);
}

if (ValidateUtil.isStatusBlocked(documentDetail.firstNotify.status)) {
    documentDetail.firstNotify = {};
    documentDetail.firstNotifyAddress = {};
    return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.firstNotify."+docIdx, $rootScope.nls["ERR90146"], serviceIdx, "hawb", undefined);

}

if (ValidateUtil.isStatusHidden(documentDetail.firstNotify.status)) {
    documentDetail.firstNotify = {};
    documentDetail.firstNotifyAddress = {};
    return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.firstNotify."+docIdx, $rootScope.nls["ERR90147"], serviceIdx, "hawb", undefined);
}
}
}

if(code == 0 || code == 307) {
    if (documentDetail.secondNotify == undefined || documentDetail.secondNotify.id == undefined ||
        documentDetail.secondNotify == null || documentDetail.secondNotify == "") {

    } else {
       if (documentDetail.secondNotify.isDefaulter) {
        documentDetail.secondNotify = {};
        documentDetail.secondNotifyAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.secondNotify."+docIdx, $rootScope.nls["ERR90589"], serviceIdx, "hawb", undefined);
    }
    
    if (ValidateUtil.isStatusBlocked(documentDetail.secondNotify.status)) {
        documentDetail.secondNotify = {};
        documentDetail.secondNotifyAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.secondNotify."+docIdx, $rootScope.nls["ERR90159"], serviceIdx, "hawb", undefined);
    }

    if (ValidateUtil.isStatusHidden(documentDetail.secondNotify.status)) {
        documentDetail.secondNotify = {};
        documentDetail.secondNotifyAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.secondNotify."+docIdx, $rootScope.nls["ERR90160"], serviceIdx, "hawb", undefined);
    }
}
}

if(code == 0 || code == 308) {
 
}
if(code == 0 || code == 309) {
    if (documentDetail.agent == undefined || documentDetail.agent.id == undefined || documentDetail.agent == null || documentDetail.agent == "") {
    } else {
       if (documentDetail.agent.isDefaulter) {
        documentDetail.agent = {};
        documentDetail.agentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.agent."+docIdx, $rootScope.nls["ERR90590"], serviceIdx, "hawb", undefined);
    }
    
    if (ValidateUtil.isStatusBlocked(documentDetail.agent.status)) {
        documentDetail.agent = {};
        documentDetail.agentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.agent."+docIdx, $rootScope.nls["ERR90172"], serviceIdx, "hawb", undefined);
    }

    if (ValidateUtil.isStatusHidden(documentDetail.agent.status)) {
        documentDetail.agent = {};
        documentDetail.agentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.agent."+docIdx, $rootScope.nls["ERR90173"], serviceIdx, "hawb", undefined);
    }
}
}

if(code == 0 || code == 310) {
    if (documentDetail.issuingAgent == undefined || documentDetail.issuingAgent.id == undefined || documentDetail.issuingAgent == null || documentDetail.issuingAgent == "") {
    } else {
       if (documentDetail.issuingAgent.isDefaulter) {
        documentDetail.issuingAgent = {};
        documentDetail.issuingAgentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.issuingAgent."+docIdx, $rootScope.nls["ERR90592"], serviceIdx, "hawb", undefined);
    }
    
    if (ValidateUtil.isStatusBlocked(documentDetail.issuingAgent.status)) {
        documentDetail.issuingAgent = {};
        documentDetail.issuingAgentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.issuingAgent."+docIdx, $rootScope.nls["ERR90185"], serviceIdx, "hawb", undefined);
    }

    if (ValidateUtil.isStatusHidden(documentDetail.issuingAgent.status)) {
        documentDetail.issuingAgent = {};
        documentDetail.issuingAgentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.issuingAgent."+docIdx, $rootScope.nls["ERR90186"], serviceIdx, "hawb", undefined);
    }
}
}

if(code == 0 || code == 311) {
    if (documentDetail.chaAgent == undefined || documentDetail.chaAgent.id == undefined || documentDetail.chaAgent == null || documentDetail.chaAgent == "") {
    } else {
       if (documentDetail.chaAgent.isDefaulter) {
        documentDetail.chaAgent = {};
        documentDetail.chaAgentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.chaAgent."+docIdx, $rootScope.nls["ERR90702"], serviceIdx, "hawb", undefined);
    }
    
    if (ValidateUtil.isStatusBlocked(documentDetail.chaAgent.status)) {
        documentDetail.chaAgent = {};
        documentDetail.chaAgentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.chaAgent."+docIdx, $rootScope.nls["ERR90703"], serviceIdx, "hawb", undefined);
    }

    if (ValidateUtil.isStatusHidden(documentDetail.chaAgent.status)) {
        documentDetail.chaAgent = {};
        documentDetail.chaAgentAddress = {};
        return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.chaAgent."+docIdx, $rootScope.nls["ERR90704"], serviceIdx, "hawb", undefined);
    }
}
}


if(code == 0 || code == 313) {
 
}

if(code == 0 ) {
 if($rootScope.isGoodsReceived(shipmentService)){
  
  if (documentDetail.commodityDescription == undefined ||  documentDetail.commodityDescription == "" ||  documentDetail.commodityDescription == null) {
     return this.validationResponse(true,"shipment.serviceDetail."+serviceIdx+".document.commodityDescription."+docIdx, $rootScope.nls["ERR90716"], serviceIdx, "hawb", undefined);
 }	
}
}

return this.validationSuccesResponse();
}



this.validateAuthenticatedDocs = function(dataObj,index) {
   var errorObject = {errorFlag : false, errTextArr : []};
   var errorArr = [];
        //documnetNo
        if (dataObj.documentNo==null || dataObj.documentNo == undefined || dataObj.documentNo == "") {
          console.log("documentNo is Mandatory");
          errorObject.documentNo = true;
          errorObject.errTextArr.push($rootScope.nls["ERR05500"]);
          errorArr.push(0);
      }
      
        //date
        if (dataObj.date==null || dataObj.date == undefined || dataObj.date == "") {
            console.log("Date is Mandatory")
            errorObject.date = true;
            errorObject.errTextArr.push($rootScope.nls["ERR05503"]);
            errorArr.push(0);
        }
        
        //origin
        if (dataObj.origin==null || dataObj.origin==undefined || dataObj.origin=="" || dataObj.origin.id == undefined || dataObj.origin.id == null || dataObj.origin.id == "") {
            console.log("Origin is Mandatory")
            errorObject.origin = true;
            errorObject.errTextArr.push($rootScope.nls["ERR05504"]);
            errorArr.push(0);
        } else {
            if(dataObj.origin.status == 'Block') {
                console.log("Selected Origin is Blocked.");
                errorObject.origin = true;
                errorObject.errTextArr.push($rootScope.nls["ERR05505"]);
                errorArr.push(0);
            } else if(dataObj.origin.status == 'Hide') {
                console.log("Selected Origin is Hidden.");
                errorObject.origin = true;
                errorObject.errTextArr.push($rootScope.nls["ERR05506"]);
                errorArr.push(0);
            } else {
            	errorObject.origin = false;
              errorArr.push(1);
          }
      }
      
        //destination
        if (dataObj.destination==null || dataObj.destination==undefined || dataObj.destination=="" || dataObj.destination.id == undefined || dataObj.destination.id == null || dataObj.destination.id == "") {
          console.log("Destination is Mandatory")
          errorObject.destination = true;
          errorObject.errTextArr.push($rootScope.nls["ERR05509"]);
          errorArr.push(0);
      } else {
          if(dataObj.destination.status == 'Block') {
              console.log("Selected Destination is Blocked.");
              errorObject.destination = true;
              errorObject.errTextArr.push($rootScope.nls["ERR05510"]);
              errorArr.push(0);
          } else if(dataObj.destination.status == 'Hide') {
              console.log("Selected Origin is Hidden.");
              errorObject.destination = true;
              errorObject.errTextArr.push($rootScope.nls["ERR05511"]);
              errorArr.push(0);
          } else {
             errorObject.destination = false;
             errorArr.push(1);
         }
     }
     
        // noOfPiece
        if (dataObj.noOfPiece==undefined && dataObj.noOfPiece==null && dataObj.noOfPiece=="" ) {
            console.log("No Of Piece is Mandatory")
            errorObject.noOfPiece = true;
            errorObject.errTextArr.push($rootScope.nls["ERR05570"]);
            errorArr.push(0);
        } else {
        	if(isNaN(parseFloat(dataObj.noOfPiece))){
        		errorObject.noOfPiece = true;
        		errorObject.errTextArr.push($rootScope.nls["ERR05513"]);
             errorArr.push(0);
         } else if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,dataObj.noOfPiece)){
          errorObject.noOfPiece = true;
          errorObject.errTextArr.push($rootScope.nls["ERR05513"]);
          errorArr.push(0);
      } else if(dataObj.noOfPiece == 0) {
          errorObject.noOfPiece = true;
          errorObject.errTextArr.push($rootScope.nls["ERR05512"]);
          errorObject.errorFlag = true;
      } else if (dataObj.noOfPiece<0 || dataObj.noOfPiece>=99999999999) {
          errorObject.noOfPiece = true;
          errorObject.errTextArr.push($rootScope.nls["ERR05513"]);
          errorArr.push(0);
      } else {
          errorObject.noOfPiece = false;
          errorArr.push(1);
      }
  }

        //gross weight
        if (dataObj.grossWeight==undefined && dataObj.grossWeight==null && dataObj.grossWeight=="" ) {
            console.log("Gross Weight is Mandatory")
            errorObject.grossWeight = true;
            errorObject.errTextArr.push($rootScope.nls["ERR05568"]);
            errorArr.push(0);
        } else {
            if(isNaN(parseFloat(dataObj.grossWeight))) {
               errorObject.grossWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05501"]);
               errorArr.push(0);
           } else if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG,dataObj.grossWeight)){
               errorObject.grossWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05501"]);
               errorArr.push(0);
           } else if(dataObj.grossWeight == 0) {
               errorObject.grossWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05508"]);
               errorArr.push(0);
           } else if (dataObj.grossWeight<0 || dataObj.grossWeight>=99999999999.99) {
               errorObject.grossWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05501"]);
               errorArr.push(0);
           } else {
               errorObject.grossWeight = false;
               errorArr.push(1);
           }
       }
       
       
        //volumne weight
        if (dataObj.volWeight==undefined && dataObj.volWeight==null && dataObj.volWeight=="" ) {
            console.log("Volume Weight is Mandatory")
            errorObject.volWeight = true;
            errorObject.errTextArr.push($rootScope.nls["ERR05569"]);
            errorArr.push(0);
        } else {
            if(isNaN(parseFloat(dataObj.volWeight))){
               errorObject.volWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05502"]);
               errorArr.push(0);
           } else if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG,dataObj.volWeight)){
               errorObject.volWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05502"]);
               errorArr.push(0);
           } else if(dataObj.volWeight == 0) {
               errorObject.volWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05507"]);
               errorArr.push(0);
           } else if (dataObj.volWeight<0 || dataObj.volWeight>=99999999999.99) {
               errorObject.volWeight = true;
               errorObject.errTextArr.push($rootScope.nls["ERR05502"]);
               errorArr.push(0);
           } else {
               errorObject.volWeight = false;
               errorArr.push(1);
           }
       }

       if(errorArr.indexOf(0)<0){
           errorObject.errorFlag = true;
       }else{
           errorObject.errorFlag = false;
       }
       return errorObject;
   }
   
   this.validateCarrier = function(serviceDetail, index) {
      
      
       
    if (serviceDetail.carrier == undefined ||  serviceDetail.carrier == null ||  serviceDetail.carrier == "" || serviceDetail.carrier.id == null) {
        return this.validationResponse(true, "shipment.serviceDetail.carrier."+index, $rootScope.nls["ERR90063"], index, "serviceRouting", undefined);
    } else if (serviceDetail.carrier.status != null) {
        if (ValidateUtil.isStatusBlocked(serviceDetail.carrier.status)) {
            return this.validationResponse(true, "shipment.serviceDetail.carrier."+index, $rootScope.nls["ERR90064"], index, "serviceRouting", undefined);
        }

        if (ValidateUtil.isStatusHidden(serviceDetail.carrier.status)) {
            return this.validationResponse(true, "shipment.serviceDetail.carrier."+index, $rootScope.nls["ERR90065"], index, "serviceRouting", undefined);
        }
    }
    return this.validationSuccesResponse( );
    
}

this.validatePickUpDelivery = function(pickUpDeliveryPoint, code,index) {
  
  if(pickUpDeliveryPoint==undefined || pickUpDeliveryPoint==null){
     return this.validationSuccesResponse();
 }
 
 if(code == 0 || code == 1) {
     if(pickUpDeliveryPoint!=undefined && pickUpDeliveryPoint.transporter != null && pickUpDeliveryPoint.transporter != undefined && pickUpDeliveryPoint.transporter!= "" ){
         
        if(pickUpDeliveryPoint.transporter.isDefaulter){
           return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.transporter."+index, $rootScope.nls["ERR90593"], index, "pickupDelivery", undefined);
       }
       if(pickUpDeliveryPoint.transporter.status=="Block"){
           return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.transporter."+index, $rootScope.nls["ERR90455"], index, "pickupDelivery", undefined);
       }
       else if(pickUpDeliveryPoint.transporter.status=="Hide"){
           return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.transporter."+index, $rootScope.nls["ERR90456"], index, "pickupDelivery", undefined);
       }
   }
}

if(code == 0 || code == 2) {
 if(pickUpDeliveryPoint.pickupPoint != null && pickUpDeliveryPoint.pickupPoint != undefined && pickUpDeliveryPoint.pickupPoint!= "" ){
    if(pickUpDeliveryPoint.pickupPoint.isDefaulter){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPoint."+index, $rootScope.nls["ERR90594"], index, "pickupDelivery", undefined);
   }
   if(pickUpDeliveryPoint.pickupPoint.status=="Block"){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPoint."+index, $rootScope.nls["ERR90484"], index, "pickupDelivery", undefined);
   }
   else if(pickUpDeliveryPoint.pickupPoint.status=="Hide"){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPoint."+index, $rootScope.nls["ERR90485"], index, "pickupDelivery", undefined);
   }
}
if(pickUpDeliveryPoint.isOurPickUp==true || pickUpDeliveryPoint.isOurPickUp=='Yes'){
    if(pickUpDeliveryPoint.pickupPoint == null || pickUpDeliveryPoint.pickupPoint == undefined || pickUpDeliveryPoint.pickupPoint== "" ){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPoint."+index,$rootScope.nls["ERR90641"], index, "pickupDelivery", undefined);
   }
}
}

if(code == 0 || code == 3) {
			/*if(pickUpDeliveryPoint.pickupFrom != null && pickUpDeliveryPoint.pickupFrom != undefined && pickUpDeliveryPoint.pickupFrom!= "" ){
				if(pickUpDeliveryPoint.pickupFrom.status=="Block"){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupFrom."+index, $rootScope.nls["ERR90487"], index, "pickupDelivery", undefined);
				}
				else if(pickUpDeliveryPoint.pickupFrom.status=="Hide"){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupFrom."+index, $rootScope.nls["ERR90488"], index, "pickupDelivery", undefined);
				}
			}
			
			if(pickUpDeliveryPoint.isOurPickUp==true || pickUpDeliveryPoint.isOurPickUp=='Yes'){
				if(pickUpDeliveryPoint.pickupFrom == null || pickUpDeliveryPoint.pickupFrom == undefined || pickUpDeliveryPoint.pickupFrom== "" ){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupFrom."+index,$rootScope.nls["ERR90640"], index, "pickupDelivery", undefined);
				}
			}*/
		}
		
		if(code == 0 || code == 4) {
			if(pickUpDeliveryPoint.pickUpPlace != null && pickUpDeliveryPoint.pickUpPlace != undefined && pickUpDeliveryPoint.pickUpPlace!= "" ){
				if(pickUpDeliveryPoint.pickUpPlace.state != null && pickUpDeliveryPoint.pickUpPlace.state != undefined && pickUpDeliveryPoint.pickUpPlace.state!= "" ){
					if(pickUpDeliveryPoint.pickUpPlace.state.status=="Block"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPlace.state."+index, $rootScope.nls["ERR1817"], index, "pickupDelivery", undefined);
					}
					else if(pickUpDeliveryPoint.pickUpPlace.state.status=="Hide"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPlace.state."+index, $rootScope.nls["ERR1818"], index, "pickupDelivery", undefined);
					}
				}
				if(pickUpDeliveryPoint.pickUpPlace.city != null && pickUpDeliveryPoint.pickUpPlace.city != undefined && pickUpDeliveryPoint.pickUpPlace.city!= "" ){
					if(pickUpDeliveryPoint.pickUpPlace.city.status=="Block"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPlace.city."+index, $rootScope.nls["ERR90624"], index, "pickupDelivery", undefined);
					}
					else if(pickUpDeliveryPoint.pickUpPlace.city.status=="Hide"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickupPlace.city."+index, $rootScope.nls["ERR90625"], index, "pickupDelivery", undefined);
					}
				}
			}
		}
		
		if(code == 0 || code == 5) {
			if(pickUpDeliveryPoint.pickUpContactPerson != null && pickUpDeliveryPoint.pickUpContactPerson != undefined && pickUpDeliveryPoint.pickUpContactPerson!= "" ){
				if (!ValidateUtil.isContactPerson(pickUpDeliveryPoint.pickUpContactPerson)) {
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickUpContactPerson."+index, $rootScope.nls["ERR90490"], index, "pickupDelivery", undefined);
                }
            }
        }
        
        if(code == 0 || code == 6) {
         if(pickUpDeliveryPoint.pickUpMobileNo != null && pickUpDeliveryPoint.pickUpMobileNo != undefined && pickUpDeliveryPoint.pickUpMobileNo!= "" ){
           if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,pickUpDeliveryPoint.pickUpMobileNo)){
               return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickUpMobileNo."+index, $rootScope.nls["ERR90491"], index, "pickupDelivery", undefined);
           }
       }
   }
   
   if(code == 0 || code == 7) {
     if(pickUpDeliveryPoint.pickUpPhoneNo != null && pickUpDeliveryPoint.pickUpPhoneNo != undefined && pickUpDeliveryPoint.pickUpPhoneNo!= "" ){
       if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,pickUpDeliveryPoint.pickUpPhoneNo)){
           return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo."+index, $rootScope.nls["ERR90492"], index, "pickupDelivery", undefined);
       }
   }
}

if(code == 0 || code == 8) {
 if(pickUpDeliveryPoint.pickUpEmail != null && pickUpDeliveryPoint.pickUpEmail != undefined && pickUpDeliveryPoint.pickUpEmail!= "" ){
    if(ValidateUtil.isValidMail(pickUpDeliveryPoint.pickUpEmail)){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.pickUpEmail."+index, $rootScope.nls["ERR90492"], index, "pickupDelivery", undefined);
   }
}
}


if(code == 0 || code == 9) {
 if(pickUpDeliveryPoint.deliveryPoint != null && pickUpDeliveryPoint.deliveryPoint != undefined && pickUpDeliveryPoint.deliveryPoint!= "" ){
    if(pickUpDeliveryPoint.deliveryPoint.status=="Block"){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPoint."+index, $rootScope.nls["ERR90496"], index, "pickupDelivery", undefined);
   }
   else if(pickUpDeliveryPoint.deliveryPoint.status=="Hide"){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPoint."+index, $rootScope.nls["ERR90497"], index, "pickupDelivery", undefined);
   }
}
}

if(code == 0 || code == 10) {
			/* if(pickUpDeliveryPoint.deliveryFrom != null && pickUpDeliveryPoint.deliveryFrom != undefined && pickUpDeliveryPoint.deliveryFrom!= "" ){
				if(pickUpDeliveryPoint.deliveryFrom.status=="Block"){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryFrom."+index, $rootScope.nls["ERR90499"], index, "pickupDelivery", undefined);
				}
				else if(pickUpDeliveryPoint.deliveryFrom.status=="Hide"){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryFrom."+index, $rootScope.nls["ERR90500"], index, "pickupDelivery", undefined);
				}
			}*/
		}
		
		if(code == 0 || code == 11) {
			if(pickUpDeliveryPoint.deliveryPlace != null && pickUpDeliveryPoint.deliveryPlace != undefined && pickUpDeliveryPoint.deliveryPlace!= "" ){
				if(pickUpDeliveryPoint.deliveryPlace.state != null && pickUpDeliveryPoint.deliveryPlace.state != undefined && pickUpDeliveryPoint.deliveryPlace.state!= "" ){
					if(pickUpDeliveryPoint.deliveryPlace.state.status=="Block"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPlace.state."+index, $rootScope.nls["ERR1817"], index, "pickupDelivery", undefined);
					}
					else if(pickUpDeliveryPoint.deliveryPlace.state.status=="Hide"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPlace.state."+index, $rootScope.nls["ERR1818"], index, "pickupDelivery", undefined);
					}
				}
				if(pickUpDeliveryPoint.deliveryPlace.city != null && pickUpDeliveryPoint.deliveryPlace.city != undefined && pickUpDeliveryPoint.deliveryPlace.city!= "" ){
					if(pickUpDeliveryPoint.deliveryPlace.city.status=="Block"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPlace.city."+index, $rootScope.nls["ERR90624"], index, "pickupDelivery", undefined);
					}
					else if(pickUpDeliveryPoint.deliveryPlace.city.status=="Hide"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPlace.city."+index, $rootScope.nls["ERR90625"], index, "pickupDelivery", undefined);
					}
				}
			}
		}
		
		if(code == 0 || code == 12) {
			if(pickUpDeliveryPoint.deliveryContactPerson != null && pickUpDeliveryPoint.deliveryContactPerson != undefined && pickUpDeliveryPoint.deliveryContactPerson!= "" ){
				if (!ValidateUtil.isContactPerson(pickUpDeliveryPoint.deliveryContactPerson)) {
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryContactPerson."+index, $rootScope.nls["ERR90502"], index, "pickupDelivery", undefined);
                }
            }
        }
        
        if(code == 0 || code == 13) {
         if(pickUpDeliveryPoint.deliveryMobileNo != null && pickUpDeliveryPoint.deliveryMobileNo != undefined && pickUpDeliveryPoint.deliveryMobileNo!= "" ){
           if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,pickUpDeliveryPoint.deliveryMobileNo)){
               return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryMobileNo."+index, $rootScope.nls["ERR90503"], index, "pickupDelivery", undefined);
           }
       }
   }
   
   if(code == 0 || code == 14) {
     if(pickUpDeliveryPoint.deliveryPhoneNo != null && pickUpDeliveryPoint.deliveryPhoneNo != undefined && pickUpDeliveryPoint.deliveryPhoneNo!= "" ){
       if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,pickUpDeliveryPoint.deliveryPhoneNo)){
           return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo."+index, $rootScope.nls["ERR90504"], index, "pickupDelivery", undefined);
       }
   }
}

if(code == 0 || code == 15) {
 if(pickUpDeliveryPoint.deliveryEmail != null && pickUpDeliveryPoint.deliveryEmail != undefined && pickUpDeliveryPoint.deliveryEmail!= "" ){
    if(ValidateUtil.isValidMail(pickUpDeliveryPoint.deliveryEmail)){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.deliveryEmail."+index, $rootScope.nls["ERR90506"], index, "pickupDelivery", undefined);
   }
}
}

if(code == 0 || code == 16) {
 if(pickUpDeliveryPoint.doorDeliveryPoint != null && pickUpDeliveryPoint.doorDeliveryPoint != undefined && pickUpDeliveryPoint.doorDeliveryPoint!= "" ){
    if(pickUpDeliveryPoint.doorDeliveryPoint.status=="Block"){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint."+index, $rootScope.nls["ERR90508"], index, "pickupDelivery", undefined);
   }
   else if(pickUpDeliveryPoint.doorDeliveryPoint.status=="Hide"){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint."+index, $rootScope.nls["ERR90509"], index, "pickupDelivery", undefined);
   }
}
}

if(code == 0 || code == 17) {
			/* if(pickUpDeliveryPoint.doorDeliveryFrom != null && pickUpDeliveryPoint.doorDeliveryFrom != undefined && pickUpDeliveryPoint.doorDeliveryFrom!= "" ){
				if(pickUpDeliveryPoint.doorDeliveryFrom.status=="Block"){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom."+index, $rootScope.nls["ERR90511"], index, "pickupDelivery", undefined);
				}
				else if(pickUpDeliveryPoint.doorDeliveryFrom.status=="Hide"){
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom."+index, $rootScope.nls["ERR90512"], index, "pickupDelivery", undefined);
				}
			}*/
		}
		
		if(code == 0 || code == 18) {
			if(pickUpDeliveryPoint.doorDeliveryPlace != null && pickUpDeliveryPoint.doorDeliveryPlace != undefined && pickUpDeliveryPoint.doorDeliveryPlace!= "" ){
				if(pickUpDeliveryPoint.doorDeliveryPlace.state != null && pickUpDeliveryPoint.doorDeliveryPlace.state != undefined && pickUpDeliveryPoint.doorDeliveryPlace.state!= "" ){
					if(pickUpDeliveryPoint.doorDeliveryPlace.state.status=="Block"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.state."+index, $rootScope.nls["ERR1817"], index, "pickupDelivery", undefined);
					}
					else if(pickUpDeliveryPoint.doorDeliveryPlace.state.status=="Hide"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.state."+index, $rootScope.nls["ERR1818"], index, "pickupDelivery", undefined);
					}
				}
				if(pickUpDeliveryPoint.doorDeliveryPlace.city != null && pickUpDeliveryPoint.doorDeliveryPlace.city != undefined && pickUpDeliveryPoint.doorDeliveryPlace.city!= "" ){
					if(pickUpDeliveryPoint.doorDeliveryPlace.city.status=="Block"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.city."+index, $rootScope.nls["ERR90624"], index, "pickupDelivery", undefined);
					}
					else if(pickUpDeliveryPoint.doorDeliveryPlace.city.status=="Hide"){
						return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace.city."+index, $rootScope.nls["ERR90625"], index, "pickupDelivery", undefined);
					}
				}
			}
		}
		
		if(code == 0 || code == 19) {
			if(pickUpDeliveryPoint.doorDeliveryContactPerson != null && pickUpDeliveryPoint.doorDeliveryContactPerson != undefined && pickUpDeliveryPoint.doorDeliveryContactPerson!= "" ){
				if (!ValidateUtil.isContactPerson(pickUpDeliveryPoint.doorDeliveryContactPerson)) {
					return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryContactPerson."+index, $rootScope.nls["ERR90514"], index, "pickupDelivery", undefined);
                }
            }
        }
        
        if(code == 0 || code == 20) {
         if(pickUpDeliveryPoint.doorDeliveryMobileNo != null && pickUpDeliveryPoint.doorDeliveryMobileNo != undefined && pickUpDeliveryPoint.doorDeliveryMobileNo!= "" ){
           if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,pickUpDeliveryPoint.doorDeliveryMobileNo)){
               return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryMobileNo."+index, $rootScope.nls["ERR90515"], index, "pickupDelivery", undefined);
           }
       }
   }
   
   if(code == 0 || code == 21) {
     if(pickUpDeliveryPoint.doorDeliveryPhoneNo != null && pickUpDeliveryPoint.doorDeliveryPhoneNo != undefined && pickUpDeliveryPoint.doorDeliveryPhoneNo!= "" ){
       if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,pickUpDeliveryPoint.doorDeliveryPhoneNo)){
           return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryPhoneNo."+index, $rootScope.nls["ERR90516"], index, "pickupDelivery", undefined);
       }
   }
}

if(code == 0 || code == 22) {
 if(pickUpDeliveryPoint.doorDeliveryEmail != null && pickUpDeliveryPoint.doorDeliveryEmail != undefined && pickUpDeliveryPoint.doorDeliveryEmail!= "" ){
    if(ValidateUtil.isValidMail(pickUpDeliveryPoint.doorDeliveryEmail)){
       return this.validationResponse(true, "shipment.serviceDetail.pickUpDeliveryPoint.doorDeliveryEmail."+index, $rootScope.nls["ERR90518"], index, "pickupDelivery", undefined);
   }
}
}



return this.validationSuccesResponse();
}












this.validationSuccesResponse = function() {
   return {error : false};
}

this.validationResponse = function(err, elem, message, idx, tabName, accordian) {
  return {error : err, errElement : elem, errMessage : message, serviceIdx : idx, tab : tabName, accordian: accordian};
}

}]);