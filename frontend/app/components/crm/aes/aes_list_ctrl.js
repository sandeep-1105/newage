/**
 * Created by hmspl on 18/10/16.
 */
app.controller('aes_list_ctrl',['$rootScope', '$scope', '$http', '$location', '$window', 'AesSearch', 'AesPendingSearch', '$stateParams', '$state',
    'ngDialog', 'roleConstant',
    function($rootScope, $scope, $http, $location, $window, AesSearch, AesPendingSearch, $stateParams, $state,
    ngDialog, roleConstant) {
    $scope.deskTopView = true;


    $scope.setTab = function(tab) {

        $scope.detailTab = tab;
        if (tab === 'pending') {
            $scope.pagePending = 0;
            $scope.limitPending = 10;
            $scope.limitArrPending = [10, 15, 20];
            $scope.totalRecordPending = 0;
            $scope.aesPendingSearch();

        } else {
            $scope.page = 0;
            $scope.limit = 10;
            $scope.limitArr = [10, 15, 20];
            $scope.totalRecord = 0;
            $scope.aesListSearch();
        }

    }




    $scope.init = function() {
        console.log("Init is called....");




        $scope.setTab('pending');
    }

    $scope.sortSelection = {
        sortKey: "status",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.aesListSearch();
    }


    $scope.changeSearch = function(param) {
        $scope.page = 0;
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.aesListSearch();
        console.log("change search", $scope.searchDto);
    }
    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.aesListSearch();
    }


    $scope.aesListSearch = function() {
        $scope.searchDto = {};
        console.log("Search method is called. aes...........................!");
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        if ($scope.searchDto.etd != undefined && $scope.searchDto.etd != null) {
            $scope.searchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.startDate);
            $scope.searchDto.etd.endDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.endDate);
        }
        AesSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            $scope.aesFileList = data.responseObject.searchResult;
        });
    }


    // AES 	Start

    $scope.aesHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,
        },
        {
            "name": "Service ID",
            "width": "w75px",
            "prefWidth": "75",
            "model": "serviceUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "serviceUid"
        },
        {
            "name": "Shipment ID",
            "width": "w75px",
            "prefWidth": "75",
            "model": "shipmentUid",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "shipmentUid"
        },
        {
            "name": "Master ID",
            "width": "w75px",
            "prefWidth": "75",
            "model": "masterUid",
            "search": true,
            "type": "link",
            "sort": true,
            "key": "masterUid"
        },
        {
            "name": "Shipper",
            "width": "w75px",
            "prefWidth": "75",
            "model": "shipperName",
            "search": true,
            "type": "text",
            //"data":$rootScope.enum['PPCC'],
            "sort": true,
            "key": "shipper"
        },
        {
            "name": "Consignee",
            "width": "w75px",
            "prefWidth": "75",
            "model": "consigneeName",
            "search": true,
            "sort": true,
            "type": "text",
            //"data":$rootScope.enum['LovStatus'],
            "key": "consignee"
        },
        {
            "name": "ETD",
            "width": "w75px",
            "prefWidth": "75",
            "model": "etd",
            "search": true,
            "sort": true,
            "type": "date-range",
            "key": "etd"
        },
        {
            "name": "Last Action",
            "width": "w75px",
            "prefWidth": "75",
            "model": "aesAction",
            "search": true,
            "sort": true,
            "type": "drop",
            "data": $rootScope.enum['AesAction'],
            "key": "aesAction"
        },
        {
            "name": "AES Status",
            "width": "w75px",
            "prefWidth": "75",
            "model": "aesStatus",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "aesStatus"
        }
    ]



    //Aes pending search




    $scope.changepage = function(param) {

        if ($scope.detailTab == 'pending') {
            $scope.pagePending = param.page;
            $scope.limitPending = param.size;
            $scope.aesPendingSearch();
        } else {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.aesListSearch();
        }

    }


    $scope.aesPendingSearch = function() {

        $scope.shipmentSearchDto = {};
        $scope.shipmentSearchDto.selectedPageNumber = $scope.pagePending;
        $scope.shipmentSearchDto.recordPerPage = $scope.limitPending;

        console.log("Search method is called. aes...........................!");

        if ($scope.shipmentSearchDto.etd != undefined && $scope.shipmentSearchDto.etd != null) {
            $scope.shipmentSearchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.shipmentSearchDto.etd.startDate);
            $scope.shipmentSearchDto.etd.endDate = $rootScope.sendApiStartDateTime($scope.shipmentSearchDto.etd.endDate);
        }



        AesPendingSearch.search($scope.shipmentSearchDto).$promise.then(function(data, status) {
            if (data.responseObject != null) {
                $scope.totalRecordPending = data.responseObject.totalRecord;
                $scope.aesPendingFileList = data.responseObject.searchResult;
            }
        });
    }

    //Aes Pending Array

    $scope.aesPendingHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,
        },
        {
            "name": "Service ID",
            "width": "w75px",
            "prefWidth": "75",
            "model": "serviceUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "serviceUid"
        },
        {
            "name": "Shipment ID",
            "width": "w75px",
            "prefWidth": "75",
            "model": "shipmentUid",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "shipmentUid"
        },
        {
            "name": "Master ID",
            "width": "w75px",
            "prefWidth": "75",
            "model": "masterUid",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "consolUid"
        }, {
            "name": "MAWB No",
            "width": "w75px",
            "prefWidth": "75",
            "model": "mawbNo",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "mawbNo"
        },
        {
            "name": "Origin",
            "width": "w75px",
            "prefWidth": "75",
            "model": "origin",
            "search": true,
            "type": "text",
            //"data":$rootScope.enum['PPCC'],
            "sort": true,
            "key": "origin"
        },
        {
            "name": "Destination",
            "width": "w75px",
            "prefWidth": "75",
            "model": "destination",
            "search": true,
            "sort": true,
            "type": "text",
            //"data":$rootScope.enum['LovStatus'],
            "key": "destination"
        },
        {
            "name": "ETD",
            "width": "w75px",
            "prefWidth": "75",
            "model": "etd",
            "search": true,
            "sort": true,
            "type": "date-range",
            "key": "etd"
        }
    ]




    $scope.sortPendingSelection = {
        sortKey: "serviceUid",
        sortOrder: "asc"
    }

    $scope.limitPendingChange = function(item) {
        $scope.pagePending = 0;
        $scope.limitPending = item;
        $scope.cancel();
        $scope.aesPendingSearch();
    }

    $scope.changeSearchPending = function(param) {
        $scope.pagePending = 0;
        for (var attrname in param) {
            $scope.shipmentSearchDto[attrname] = param[attrname];
        }
        $scope.cancel();
        $scope.aesPendingSearch();
    }
    $scope.sortChangePending = function(param) {
        $scope.shipmentSearchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.shipmentSearchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.aesPendingSearch();
    }

    $scope.rowSelect = function(obj) {
        if ($rootScope.roleAccess(roleConstant.CRM_AUTOMATED_EXPORT_SYSTEM_VIEW)) {

            if (obj != undefined && obj != null) {

                if (obj.aesAction == undefined || obj.aesAction == null) {

                    if (obj.serviceUid != null && obj.shipmentUid != null) {
                        var navAesObj = {
                            status: $scope.detailTab,
                            consolUid: obj.consolUid,
                            serviceUid: obj.serviceUid,
                            shipmentUid: obj.shipmentUid,
                            fromScreen: $state.current.name,
                            mawbNo: obj.mawbNo,
                            hawbNo: obj.hawbNo
                        };
                        $state.go("layout.addAes", {
                            navAesObj: navAesObj
                        });
                    } else {
                        Notification.error("Shipment is Mandatory for AES-Generation");
                    }
                } else {
                    var navAesObj = {
                        status: $scope.detailTab,
                        aesId: obj.id,
                        fromScreen: $state.current.name,
                        consolUid: obj.consolUid,
                        shipmentUid: obj.shipmentUid,
                        fromState: $state.current.name,
                        serviceUid: obj.serviceUid
                    };
                    $state.go("layout.editAes", {
                        navAesObj: navAesObj
                    });
                }
            } else {
                Notification.error("Something went wrong....");
            }

        }
    }


    $scope.actionClick = function(obj) {
        if (obj != null) {
            if (obj.data.serviceUid != null && obj.data.shipmentUid != null) {
                var navAesObj = {
                    status: $scope.detailTab,
                    consolUid: obj.data.consolUid,
                    serviceUid: obj.data.serviceUid,
                    shipmentUid: obj.data.shipmentUid,
                    fromScreen: $state.current.name,
                    mawbNo: obj.data.mawbNo,
                    hawbNo: obj.data.hawbNo
                };
                $state.go("layout.addAes", {
                    navAesObj: navAesObj
                });
            } else {
                Notification.error("Shipment is Mandatory for AES-Generation");
            }
        } else {
            Notification.error("Something went wrong....");
        }
    }

    $scope.cancel = function() {


    }


    $scope.serialNo = function(index) {
        index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
            (index + 1);
        return index;
    }




    switch ($stateParams.action) {
        case "SEARCH":
            console.log("Search....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "CRM",
                    state: "layout.aesList"
                },
                {
                    label: "AES",
                    state: "layout.aesList"
                }
            ];
            break;
        case "VIEW":
            break;
    }

}]);