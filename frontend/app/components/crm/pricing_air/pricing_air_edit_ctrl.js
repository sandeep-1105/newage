/**
 * Created by Praveen on 16/11/16.
 */
app.controller('pricingEditCtrl', ['$rootScope', '$scope', '$location', '$stateParams', '$window', '$timeout', 'ngDialog', '$state', '$modal',
    'cloneService', 'PortByTransportMode', 'ChargeSearchKeyword', 'UnitSearchKeyword', 'CurrencyMasterSearchKeyword', 'CarrierByTransportMode',
    'PricingAirSave', 'PricingAirView', 'PricingAirEdit', 'UnitMasterGetByCode', 'appConstant', 'CommonValidationService',
    'divsionService', 'CommodityService', 'partyService', 'carrierMasterService', 'TosSearchKeyword', 'downloadFactory', 'roleConstant', 'Notification',
    function($rootScope, $scope, $location, $stateParams, $window, $timeout, ngDialog, $state, $modal,
        cloneService, PortByTransportMode, ChargeSearchKeyword, UnitSearchKeyword, CurrencyMasterSearchKeyword, CarrierByTransportMode,
        PricingAirSave, PricingAirView, PricingAirEdit, UnitMasterGetByCode, appConstant, CommonValidationService,
        divsionService, CommodityService, partyService, carrierMasterService, TosSearchKeyword, downloadFactory, roleConstant, Notification) {

        $scope.$roleConstant = roleConstant;
        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";

        $scope.init = function() {

            if ($scope.pricingMaster == undefined || $scope.pricingMaster == null) {
                $scope.pricingMaster = {};
                $scope.pricingMaster.whoRouted = false;
            } else {
                $scope.pricingMaster.whoRouted = $scope.pricingMaster.whoRouted == 'Agent' ? true : false;

                if ($scope.pricingMaster != undefined && $scope.pricingMaster.id != null && $scope.pricingMaster.generalOriginPriceList.length != 0) {

                    var editAccess = $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_ORIGIN_CHARGES_MODIFY);

                    for (var i = 0; i < $scope.pricingMaster.generalOriginPriceList.length; i++) {
                        if ($scope.pricingMaster.generalOriginPriceList[i].id != null) {
                            if (editAccess) {
                                $scope.pricingMaster.generalOriginPriceList[i].checkRolesToDisable = false;
                            } else {
                                $scope.pricingMaster.generalOriginPriceList[i].checkRolesToDisable = true;
                            }
                        }

                    }

                }
            }

            if ($scope.pricingMaster.generalFreightPriceList == null)
                $scope.pricingMaster.generalFreightPriceList = [];

            if ($scope.pricingMaster.generalOriginPriceList == null)
                $scope.pricingMaster.generalOriginPriceList = [];

            if ($scope.pricingMaster.generalDestinationPriceList == null)
                $scope.pricingMaster.generalDestinationPriceList = [];

            if ($scope.pricingMaster.hazardousOriginPriceList == null)
                $scope.pricingMaster.hazardousOriginPriceList = [];
            if ($scope.pricingMaster.hazardousFreightPriceList == null)
                $scope.pricingMaster.hazardousFreightPriceList = [];

            if ($scope.pricingMaster.hazardousDestinationPriceList == null)
                $scope.pricingMaster.hazardousDestinationPriceList = [];
            $scope.openDefaultTab('general', 'generalChargeName0');
            $scope.openDefaultTab('Origin', 'generalChargeName0');
            $scope.addEmptyList();

        };

        $scope.addEmptyList = function() {

            if ($scope.pricingMaster.generalOriginPriceList != undefined &&
                $scope.pricingMaster.generalOriginPriceList != null &&
                $scope.pricingMaster.generalOriginPriceList.length == 0) {
                $scope.pricingMaster.generalOriginPriceList.push({});
            }
            if ($scope.pricingMaster.generalFreightPriceList != undefined &&
                $scope.pricingMaster.generalFreightPriceList != null &&
                $scope.pricingMaster.generalFreightPriceList.length == 0) {
                $scope.pricingMaster.generalFreightPriceList.push({});
            }
            if ($scope.pricingMaster.generalDestinationPriceList != undefined &&
                $scope.pricingMaster.generalDestinationPriceList != null &&
                $scope.pricingMaster.generalDestinationPriceList.length == 0) {
                $scope.pricingMaster.generalDestinationPriceList.push({});
            }
            if ($scope.pricingMaster.hazardousOriginPriceList != undefined &&
                $scope.pricingMaster.hazardousOriginPriceList != null &&
                $scope.pricingMaster.hazardousOriginPriceList.length == 0) {
                $scope.pricingMaster.hazardousOriginPriceList.push({});
            }
            if ($scope.pricingMaster.hazardousFreightPriceList != undefined &&
                $scope.pricingMaster.hazardousFreightPriceList != null &&
                $scope.pricingMaster.hazardousFreightPriceList.length == 0) {
                $scope.pricingMaster.hazardousFreightPriceList.push({});
            }
            if ($scope.pricingMaster.hazardousDestinationPriceList != undefined &&
                $scope.pricingMaster.hazardousDestinationPriceList != null &&
                $scope.pricingMaster.hazardousDestinationPriceList.length == 0) {
                $scope.pricingMaster.hazardousDestinationPriceList.push({});
            }
            if ($scope.pricingMaster.attachmentList != null && $scope.pricingMaster.attachmentList.length == 0) {
                $scope.pricingMaster.attachmentList.push({});
            } else {

            }
        }

        $scope.attachConfig = {
            "isEdit": true, //false for view only mode
            "isDelete": true, //
            "columnDefs": [

                {
                    "name": "Reference No",
                    "model": "refNo",
                    "type": "text"
                },
                {
                    "name": "File Name",
                    "model": "fileName",
                    "type": "file"
                },
                {
                    "name": "Actions",
                    "model": "action",
                    "type": "action"
                }
            ]

        };

        /*
            $scope.pricingAir = {};
            $scope.pricingAir = ["Origin", "Freight", "Destination"];
            $scope.pricingState = "Origin";

            $scope.hasTempAccess = function (param) {
                return true;
            }*/

        $scope.editmorepopup = function(obj) {

            var newScope = $scope.$new();
            newScope.pricingCarrier = obj;
            var myOtherModal = $modal({
                scope: newScope,
                templateUrl: 'app/components/crm/pricing_air/views/moreinfo_edit_popup.html',
                backdrop: 'static',
                show: false
            });
            myOtherModal.$promise.then(myOtherModal.show);

        };

        $scope.downloadAttach = function(param) {

            if (param.data.id != null && param.data.file == null) {

                console.log("API CALL")

                downloadFactory.downloadAttachment(
                    '/api/v1/pricing/files/', param.data.id,
                    param.data.fileName);

            }
        }

        $scope.cancel = function() {

            $scope.isDirty = $scope.pricingAirDataForm.$dirty;
            if ($scope.isDirty) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    closeByDocument: false,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(
                    function(value) {
                        if (value == 1) {
                            $scope.saveOrUpdate();
                        } else if (value == 2) {
                            var params = {};
                            params.submitAction = 'Cancelled';
                            $state.go("layout.pricingAir");
                        } else {
                            console.log("cancelled");
                        }
                    });
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go("layout.pricingAir");
            }
        };

        $scope.openDefaultTab = function(tab, nextId) {
            if (nextId != undefined && nextId != null)
                $rootScope.navigateToNextField(nextId);
            if ($scope.pricingMaster != undefined && $scope.pricingMaster.id != null) {
                if (tab == 'general' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_MODIFY)) {
                    $scope.Tabs = tab;
                }
            } else {
                if (tab == 'general' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_CREATE)) {
                    $scope.Tabs = tab;
                }
            }
            if (tab == 'Origin') {
                $scope.pricingState = tab;
            }

        }

        $scope.clickOnTab = function(tab, nextId) {

            if (nextId != undefined && nextId != null)
                $rootScope.navigateToNextField(nextId);
            if ($scope.pricingMaster != undefined && $scope.pricingMaster.id != null) {
                if (tab == 'general' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_MODIFY)) {
                    $scope.Tabs = tab;
                }
            } else {
                if (tab == 'general' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_CREATE)) {
                    $scope.Tabs = tab;
                }
            }

            if (tab == 'Origin') {
                $scope.pricingState = tab;

            }
            if (tab == 'Freight') {
                $scope.pricingState = tab;
                if ($scope.pricingMaster != undefined && $scope.pricingMaster.id != null && $scope.pricingMaster.generalFreightPriceList.length != 0) {
                    var editAccess = $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_FREIGHT_CHARGES_MODIFY);
                    for (var i = 0; i < $scope.pricingMaster.generalFreightPriceList.length; i++) {
                        if ($scope.pricingMaster.generalFreightPriceList[i].id != null) {
                            if (editAccess) {
                                $scope.pricingMaster.generalFreightPriceList[i].checkRolesToDisable = false;
                            } else {
                                $scope.pricingMaster.generalFreightPriceList[i].checkRolesToDisable = true;
                            }
                        }

                    }

                }


            }
            if (tab == 'Destination') {
                $scope.pricingState = tab;
                if ($scope.pricingMaster != undefined && $scope.pricingMaster.id != null && $scope.pricingMaster.generalDestinationPriceList.length != 0) {
                    var editAccess = $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_DESTINATION_CHARGES_MODIFY);
                    for (var i = 0; i < $scope.pricingMaster.generalDestinationPriceList.length; i++) {
                        if ($scope.pricingMaster.generalDestinationPriceList[i].id != null) {
                            if (editAccess) {
                                $scope.pricingMaster.generalDestinationPriceList[i].checkRolesToDisable = false;
                            } else {
                                $scope.pricingMaster.generalDestinationPriceList[i].checkRolesToDisable = true;
                            }
                        }

                    }

                }

            }
            if (tab == 'attachments') {
                $scope.Tabs = tab;
            }

        }
        $scope.ajaxPricingAirCurrencyEvent = function(object) {

            console.log("ajaxPricingAirCurrencyEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.currencyList = data.responseObject.searchResult;
                        return $scope.currencyList;
                        // $scope.showRateCurrencyList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Currency List');
                }
            );
        }

        $scope.pricingAirCurrencyRender = function(item) {
            return {
                label: item.currencyCode,
                item: item
            }
        };


        $scope.ajaxPricingAirUnitEvent = function(object) {
            console.log("Ajax PricingAirUnit Event method : ", object)
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.unitList = data.responseObject.searchResult;
                        console.log("$scope.unitList ", $scope.unitList);
                        return $scope.unitList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching unit');
                });
        };

        $scope.pricingAirUnitRender = function(item) {
            return {
                label: item.unitName,
                item: item
            }
        };
        $scope.ajaxPricingAirChargeEvent = function(object) {
            console.log("Ajax PricingAirCharge Event method : ", object)
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.chargeList = data.responseObject.searchResult;
                        console.log("$scope.chargeList ", $scope.chargeList);
                        return $scope.chargeList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier Rate');
                });

        };
        $scope.pricingAirChargeRender = function(item) {
            return {
                label: item.chargeName,
                item: item
            }
        };



        $scope.ajaxPortEvent = function(object, type, obj) {

            $rootScope.clientMessage = null;
            console.log(" Ajax PricingAirOrigin Event method : ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": "Air"
            }, $scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.portList = data.responseObject.searchResult;
                        if (obj != undefined && obj != null) {
                            if (obj.origin != undefined && obj.origin != null && obj.origin != "") {
                                $scope.portList = $scope.filterPort($scope.portList, obj.origin.id);
                            }
                            if (obj.destination != undefined && obj.destination != null && obj.destination != "") {
                                $scope.portList = $scope.filterPort($scope.portList, obj.destination.id);
                            }
                            if (obj.transit != undefined && obj.transit != null && obj.transit != "") {
                                $scope.portList = $scope.filterPort($scope.portList, obj.transit.id);
                            }
                        }

                        return $scope.portList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching origin');
                });

        };
        $scope.filterPort = function(array, id) {
            for (var i in array) {
                if (array[i].id == id) {
                    array.splice(i, 1);
                    break;
                }
            }
            return array;
        }



        $scope.PortRender = function(item) {
            return {
                label: item.portName,
                item: item
            }
        };


        $scope.ajaxPricingAirCarrierEvent = function(object) {
            console.log("Ajax Carrier Rate Event method : ", object)
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CarrierByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.carrierList = data.responseObject.searchResult;
                        console.log("$scope.carrierList ", $scope.carrierList);
                        return $scope.carrierList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier');
                });
        };

        $scope.carrierRender = function(item) {
            return {
                label: item.carrierName,
                item: item
            }
        };

        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'errorPopUp.html',
            show: false,
            keyboard: true
        });

        var errorOnRowIndex = null;

        $scope.errorShow = function(errorObj, index) {
            $scope.errList = errorObj.errTextArr;
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };



        $scope.addGeneralOriginCharge = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_ORIGIN_CHARGES_CREATE)) {
                console.log("addGeneralOriginCharge Row is clicked");
                $scope.firstFocus = true;
                if ($scope.validation('general', 'Origin')) {
                    $scope.pricingMaster.generalOriginPriceList.push({});
                    console.log("validation,,,")
                } else {

                    return false;
                }
            }
        }


        $scope.addGeneralFreightCharge = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_FREIGHT_CHARGES_CREATE)) {
                console.log("addGeneralFreightCharge Row is clicked");
                $scope.firstFocus = true;
                if ($scope.validation('general', 'Freight')) {
                    $scope.pricingMaster.generalFreightPriceList.push({});
                } else {
                    return false;
                }
            }
        }

        $scope.addGeneralDestinationCharge = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_DESTINATION_CHARGES_CREATE)) {
                $scope.firstFocus = true;
                if ($scope.validation('general', 'Destination')) {
                    $scope.pricingMaster.generalDestinationPriceList.push({});
                } else {
                    return false;
                }
            }
        }

        $scope.addHazardousOrigin = function() {
            console.log("addGeneralFreightCharge Row is clicked");
            $scope.firstFocus = true;
            if ($scope.validation('hazardous', 'Origin')) {
                $scope.pricingMaster.hazardousOriginPriceList.push({});
            } else {
                return false;
            }
        }
        $scope.addHazardousFreight = function() {
            console.log("addHazardousFreight Row is clicked");
            $scope.firstFocus = true;
            if ($scope.validation('hazardous', 'Freight')) {
                $scope.pricingMaster.hazardousFreightPriceList.push({});
            } else {
                return false;
            }

        }
        $scope.addHazardousDestination = function() {
            console.log("addHazardousDestination Row is clicked");
            $scope.firstFocus = true;
            if ($scope.validation('hazardous', 'Destination')) {
                $scope.pricingMaster.hazardousDestinationPriceList.push({});

            }
            return false;
        }

        $scope.removeGeneralOriginCharge = function(index) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_ORIGIN_CHARGES_DELETE)) {
                console.log("remove removeGeneralOriginCharge  is called ", index);
                $scope.pricingMaster.generalOriginPriceList.splice(index, 1);
            }
        }
        $scope.removeGeneralFreightCharge = function(index) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_FREIGHT_CHARGES_DELETE)) {
                console.log("remove removeGeneralFreightCharge..", index);
                $scope.pricingMaster.generalFreightPriceList.splice(index, 1);
            }
        }
        $scope.removeGeneralDestinationCharge = function(index) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_DESTINATION_CHARGES_DELETE)) {
                console.log("remove removeGeneralDestinationCharge..", index);
                $scope.pricingMaster.generalDestinationPriceList.splice(index, 1);
            }
        }
        $scope.removeHazardousOrigin = function(index) {
            console.log("remove removeHazardousOrigin..", index);
            $scope.pricingMaster.hazardousOriginPriceList.splice(index, 1);

        }
        $scope.removeHazardousFreight = function(index) {
            console.log("remove removeHazardousFreight..", index);
            $scope.pricingMaster.hazardousFreightPriceList.splice(index, 1);

        }
        $scope.removeHazardousDestination = function(index) {
            console.log("remove removeHazardousDestination..", index);
            $scope.pricingMaster.hazardousDestinationPriceList.splice(index, 1);

        }


        $scope.navigateInPricingAir = function(id) {
            if (id != undefined && id != null)
                $rootScope.navigateToNextField(id);
        }

        $scope.selectOriginPort = function(id) {
            console.log("$scope.pricingPortPair ======>", $scope.pricingPortPair)

            for (var i = 0; i < $scope.pricingMaster.generalOriginPriceList.length; i++) {
                $scope.pricingMaster.generalOriginPriceList[i].currencyMaster = $scope.pricingMaster.origin.portGroupMaster.countryMaster.currencyMaster.countryMaster.currencyMaster;

            }
            if ($scope.validatePricingAir(1)) {
                if (id != undefined && id != null)
                    $rootScope.navigateToNextField(id);
            } else {
                console.log('found origin port error..');
            }

        }
        $scope.selectDestinationPort = function(id) {
            for (var i = 0; i < $scope.pricingMaster.generalDestinationPriceList.length; i++) {
                $scope.pricingMaster.generalDestinationPriceList[i].currencyMaster = $scope.pricingMaster.destination.portGroupMaster.countryMaster.currencyMaster.countryMaster.currencyMaster;
            }
            if ($scope.validatePricingAir(2)) {
                if (id != undefined && id != null)
                    $rootScope.navigateToNextField(id);
            } else {
                console.log('found destination port error');
            }
        }

        $scope.selectTransitPort = function(id) {
            if ($scope.validatePricingAir(3)) {
                if (id != undefined && id != null)
                    $rootScope.navigateToNextField(id);
            } else {
                console.log('error found');
            }
        }
        $scope.selectCharge = function(id, data) {
            if (id != undefined && id != null) {
                $scope.setDefaultUnit(data);
                $scope.navigateToNextField(id)
            }



        }

        $scope.setDefaultUnit = function(data) {
            if (data.chargeMaster.calculationType != undefined && data.chargeMaster.calculationType != null) {
                if (data.chargeMaster.calculationType == 'Percentage') {
                    UnitMasterGetByCode.get({
                        unitCode: 'PER'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            data.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });

                }
                if (data.chargeMaster.calculationType == 'Shipment') {
                    UnitMasterGetByCode.get({
                        unitCode: 'SHPT'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            data.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });

                }
                if (data.chargeMaster.calculationType == 'Unit') {
                    UnitMasterGetByCode.get({
                        unitCode: 'KG'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            data.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });

                }
                if (data.chargeMaster.calculationType == 'Document') {
                    UnitMasterGetByCode.get({
                        unitCode: 'DOC'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            data.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });
                }
            }
        }




        $scope.validatePricingAir = function(validateCode) {
            console.log("validatePricingAir", validateCode);
            $scope.errorMap = new Map();

            //port validation
            if (validateCode == 0 || validateCode == 1) {

                if ($scope.pricingMaster.origin == undefined ||
                    $scope.pricingMaster.origin == null ||
                    $scope.pricingMaster.origin == "") {
                    // $scope.errorMap.put("originName", $rootScope.nls["ERR4000"]);
                    //return false;
                } else if ($scope.pricingMaster.origin.status == "Block") {
                    $scope.errorMap.put("originName", $rootScope.nls["ERR4001"]);
                    return false;
                } else if ($scope.pricingMaster.origin.status == "Hide") {
                    $scope.errorMap.put("originName", $rootScope.nls["ERR4002"]);
                    return false;
                }
                if ($scope.pricingMaster.transit != undefined && $scope.pricingMaster.transit != null && $scope.pricingMaster.origin != undefined && $scope.pricingMaster.origin != null) {
                    if ($scope.pricingMaster.origin.portCode == $scope.pricingMaster.transit.portCode) {
                        $scope.errorMap.put("originName", $rootScope.nls["ERR4010"]);
                        return false;
                    }

                }
                if ($scope.pricingMaster.destination != undefined && $scope.pricingMaster.destination != null && $scope.pricingMaster.origin != undefined && $scope.pricingMaster.origin != null) {
                    if ($scope.pricingMaster.origin.portCode == $scope.pricingMaster.destination.portCode) {
                        $scope.errorMap.put("originName", $rootScope.nls["ERR4009"]);
                        return false;
                    }
                }


            }
            if (validateCode == 0 || validateCode == 2) {

                if ($scope.pricingMaster.destination == undefined ||
                    $scope.pricingMaster.destination == null ||
                    $scope.pricingMaster.destination == "") {
                    // $scope.errorMap.put("destinationName", $rootScope.nls["ERR4003"]);
                    //return false;
                } else if ($scope.pricingMaster.destination.status == "Block") {
                    $scope.errorMap.put("destinationName", $rootScope.nls["ERR4004"]);
                    return false;
                } else if ($scope.pricingMaster.destination.status == "Hide") {
                    $scope.errorMap.put("destinationName", $rootScope.nls["ERR4005"]);
                    return false;
                }
                if ($scope.pricingMaster.origin != undefined && $scope.pricingMaster.origin != null && $scope.pricingMaster.destination != undefined && $scope.pricingMaster.destination != null) {
                    if ($scope.pricingMaster.origin.portCode == $scope.pricingMaster.destination.portCode) {
                        $scope.errorMap.put("destinationName", $rootScope.nls["ERR4009"]);
                        return false;
                    }
                }
                if ($scope.pricingMaster.transit != undefined && $scope.pricingMaster.transit != null && $scope.pricingMaster.destination != undefined && $scope.pricingMaster.destination != null) {
                    if ($scope.pricingMaster.destination.portCode == $scope.pricingMaster.transit.portCode) {
                        $scope.errorMap.put("destinationName", $rootScope.nls["ERR4011"]);
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 3) {
                /* if ($scope.pricingMaster.transit == undefined
                   || $scope.pricingMaster.transit == null
                   || $scope.pricingMaster.transit == "") {
                   $scope.errorMap.put("transitPort", $rootScope.nls["ERR4006"]);
                   return false;
                 }*/
                if ($scope.pricingMaster != undefined && $scope.pricingMaster != null) {

                    if ($scope.pricingMaster.transit != undefined && $scope.pricingMaster.transit != null && $scope.pricingMaster.transit != "") {
                        if ($scope.pricingMaster.transit.status == "Block") {
                            $scope.errorMap.put("transitPort", $rootScope.nls["ERR4007"]);
                            return false;
                        } else if ($scope.pricingMaster.transit.status == "Hide") {
                            $scope.errorMap.put("transitPort", $rootScope.nls["ERR4008"]);
                            return false;
                        }


                        if ($scope.pricingMaster.origin != undefined && $scope.pricingMaster.origin != null) {

                            if ($scope.pricingMaster.origin.portCode != undefined && $scope.pricingMaster.origin.portCode != null &&
                                $scope.pricingMaster.origin.portCode != "" && $scope.pricingMaster.transit.portCode != undefined &&
                                $scope.pricingMaster.transit.portCode != null && $scope.pricingMaster.transit.portCode != "") {
                                if ($scope.pricingMaster.origin.portCode == $scope.pricingMaster.transit.portCode) {
                                    $scope.errorMap.put("transitPort", $rootScope.nls["ERR4010"]);
                                    return false;
                                }
                            }
                        }


                        if ($scope.pricingMaster.destination != undefined && $scope.pricingMaster.destination != null) {
                            if ($scope.pricingMaster.destination.portCode != undefined && $scope.pricingMaster.destination.portCode != null &&
                                $scope.pricingMaster.destination.portCode != "" && $scope.pricingMaster.transit.portCode != undefined &&
                                $scope.pricingMaster.transit.portCode != null && $scope.pricingMaster.transit.portCode != "") {
                                if ($scope.pricingMaster.destination.portCode == $scope.pricingMaster.transit.portCode) {
                                    $scope.errorMap.put("transitPort", $rootScope.nls["ERR4011"]);
                                    return false;
                                }
                            }
                        }
                    } else {
                        $scope.pricingMaster.transit = null;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 4) {

                if (($scope.pricingMaster.origin == undefined ||
                        $scope.pricingMaster.origin == null ||
                        $scope.pricingMaster.origin == "" ||
                        $scope.pricingMaster.origin.id == undefined
                    ) && ($scope.pricingMaster.destination == undefined ||
                        $scope.pricingMaster.destination == null ||
                        $scope.pricingMaster.destination == "" || $scope.pricingMaster.destination.id == undefined)) {
                    $scope.errorMap.put("originName", $rootScope.nls["ERR2000249"]);
                    return false;
                }
            }
            return true;
        }


        $scope.isEmptyRow = function(obj) {
            angular.forEach(obj, function(data, index) {
                if ($scope.checkEmpty(data, index)) {
                    obj.splice(index, 1);
                }
            });
        }

        $scope.checkEmpty = function(obj) {

            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;
                }
            }
            return isempty;
        }

        $scope.validation = function(maintab, subtab) {

            $rootScope.clientMessage = null;
            if (maintab == 'general' && subtab == 'Origin') {

                $scope.isEmptyRow($scope.pricingMaster.generalOriginPriceList);
                $scope.generalOrigin = true;
                $scope.errorArr = [];
                $scope.errorArr.push(1);
                $scope.errorGenOrgArray = [];
                angular.forEach($scope.pricingMaster.generalOriginPriceList, function(dataObj, index) {
                    $scope.errorGenOrgArray[index] = {};
                    $scope.errorGenOrgArray[index].errTextArr = [];
                    if ($scope.finalValidate(maintab, subtab, dataObj, index)) {
                        $scope.generalOrigin = true;

                    } else {
                        $scope.generalOrigin = false;
                        return false;
                    }
                });
                if (!$scope.generalOrigin) {
                    $scope.Tabs = maintab;
                    $scope.pricingState = subtab;
                    if ($scope.isSSR_OK)
                        $rootScope.clientMessage = $rootScope.nls['ERR4096'];
                    return false;
                }


            }


            if (maintab == 'general' && subtab == 'Freight') {
                $scope.isEmptyRow($scope.pricingMaster.generalFreightPriceList);
                $scope.generalFreight = true;
                $scope.errorArr = [];
                $scope.errorArr.push(1);
                $scope.errorGenFreightArray = [];
                angular.forEach($scope.pricingMaster.generalFreightPriceList, function(dataObj, index) {
                    $scope.errorGenFreightArray[index] = {};
                    $scope.errorGenFreightArray[index].errTextArr = [];
                    $scope.errorArr = [];

                    if ($scope.finalValidateFreight(maintab, subtab, dataObj, index)) {
                        $scope.generalFreight = true;

                    } else {
                        $scope.generalFreight = false;
                        return false;
                    }
                });
                if (!$scope.generalFreight) {
                    $scope.Tabs = maintab;
                    $scope.pricingState = subtab;
                    $rootScope.clientMessage = $rootScope.nls['ERR4097'];
                    return false;
                }
            }


            if (maintab == 'general' && subtab == 'Destination') {

                $scope.isEmptyRow($scope.pricingMaster.generalDestinationPriceList);
                $scope.generalDestination = true;
                $scope.errorArr = [];
                $scope.errorArr.push(1);
                $scope.errorGenDestArray = [];
                angular.forEach($scope.pricingMaster.generalDestinationPriceList, function(dataObj, index) {
                    $scope.errorGenDestArray[index] = {};
                    $scope.errorGenDestArray[index].errTextArr = [];
                    if ($scope.finalValidate(maintab, subtab, dataObj, index)) {
                        $scope.generalDestination = true;
                    } else {
                        $scope.generalDestination = false;
                        return false;
                    }
                });
                if (!$scope.generalDestination) {
                    $scope.Tabs = maintab;
                    $scope.pricingState = subtab;
                    $rootScope.clientMessage = $rootScope.nls['ERR4092'];
                    return false;
                }
            }

            if (maintab == 'hazardous' && subtab == 'Origin') {

                $scope.isEmptyRow($scope.pricingMaster.hazardousOriginPriceList);
                $scope.hazardousOrigin = true;
                $scope.errorArr = [];
                $scope.errorArr.push(1);
                $scope.errorHazOrgArray = [];
                angular.forEach($scope.pricingMaster.hazardousOriginPriceList, function(dataObj, index) {
                    $scope.errorHazOrgArray[index] = {};
                    $scope.errorHazOrgArray[index].errTextArr = [];
                    if ($scope.finalValidate(maintab, subtab, dataObj, index)) {
                        $scope.hazardousOrigin = true;

                    } else {
                        $scope.hazardousOrigin = false;
                        return false;
                    }
                });
                if (!$scope.hazardousOrigin) {
                    $scope.Tabs = maintab;
                    $scope.pricingState = subtab;
                    $rootScope.clientMessage = $rootScope.nls['ERR4093'];
                    return false;
                }
            }



            if (maintab == 'hazardous' && subtab == 'Freight') {

                $scope.isEmptyRow($scope.pricingMaster.hazardousFreightPriceList);
                $scope.hazardousFreight = true;
                $scope.errorArr = [];
                $scope.errorArr.push(1);
                $scope.errorHazFreightArray = [];
                angular.forEach($scope.pricingMaster.hazardousFreightPriceList, function(dataObj, index) {
                    $scope.errorHazFreightArray[index] = {};
                    $scope.errorHazFreightArray[index].errTextArr = [];
                    if ($scope.finalValidateFreight(maintab, subtab, dataObj, index)) {
                        $scope.hazardousFreight = true;
                    } else {
                        $scope.hazardousFreight = false;
                        return false;
                    }
                });
                if (!$scope.hazardousFreight) {
                    $scope.Tabs = maintab;
                    $scope.pricingState = subtab;
                    $rootScope.clientMessage = $rootScope.nls['ERR4094'];
                    return false;
                }
            }


            if (maintab == 'hazardous' && subtab == 'Destination') {

                $scope.isEmptyRow($scope.pricingMaster.hazardousDestinationPriceList);
                $scope.hazardousDestination = true;
                $scope.errorArr = [];
                $scope.errorArr.push(1);
                $scope.errorHazDestArray = [];
                angular.forEach($scope.pricingMaster.hazardousDestinationPriceList, function(dataObj, index) {
                    $scope.errorHazDestArray[index] = {};
                    $scope.errorHazDestArray[index].errTextArr = [];
                    if ($scope.finalValidate(maintab, subtab, dataObj, index)) {
                        $scope.hazardousDestination = true;

                    } else {
                        $scope.hazardousDestination = false;
                        return false;
                    }
                });
                if (!$scope.hazardousDestination) {
                    $scope.Tabs = maintab;
                    $scope.pricingState = subtab;
                    $rootScope.clientMessage = $rootScope.nls['ERR4095'];
                    return false;
                }
            }



            return true;

        }


        /************************************************************************************************************************- 
  ------------------------------Freight Charge Validation--------------------------------------------------------------------
  ****************************************************************************************************************************/



        $scope.finalValidateFreight = function(maintab, subtab, dataobj, index) {


            //charge validation    
            if (dataobj.chargeMaster == null ||
                dataobj.chargeMaster == undefined ||
                dataobj.chargeMaster == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Freight') {
                    $scope.errorGenFreightArray[index].errRow = true;
                    $scope.errorGenFreightArray[index].genFreightCharge = true;
                    $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4012"]);

                }
                if (maintab == 'hazardous' && subtab == 'Freight') {
                    $scope.errorHazFreightArray[index].errRow = true;
                    $scope.errorHazFreightArray[index].hazFreightCharge = true;
                    $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4012"]);
                }

            } else {

                if (dataobj.chargeMaster.status == 'Block') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightCharge = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4013"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightCharge = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4013"]);
                    }


                }

                if (dataobj.chargeMaster.status == 'Hide') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightCharge = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4014"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightCharge = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4014"]);
                    }


                }
            }
            //Currency validation
            if (dataobj.currencyMaster == null ||
                dataobj.currencyMaster == undefined ||
                dataobj.currencyMaster == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Freight') {
                    $scope.errorGenFreightArray[index].errRow = true;
                    $scope.errorGenFreightArray[index].genFreightCurrency = true;
                    $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4018"]);

                }
                if (maintab == 'hazardous' && subtab == 'Freight') {
                    $scope.errorHazFreightArray[index].errRow = true;
                    $scope.errorHazFreightArray[index].hazFreightCurrency = true;
                    $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4018"]);
                }

            } else {

                if (dataobj.chargeMaster.status == 'Block') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightCurrency = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4019"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightCurrency = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4019"]);
                    }
                }

                if (dataobj.chargeMaster.status == 'Hide') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightCurrency = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4020"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightCurrency = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4020"]);
                    }
                }
            }

            //Carrier validation
            if (dataobj.carrierMaster == null ||
                dataobj.carrierMaster == undefined ||
                dataobj.carrierMaster == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Freight') {
                    $scope.errorGenFreightArray[index].errRow = true;
                    $scope.errorGenFreightArray[index].genFreightCarrier = true;
                    $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4021"]);

                }
                if (maintab == 'hazardous' && subtab == 'Freight') {
                    $scope.errorHazFreightArray[index].errRow = true;
                    $scope.errorHazFreightArray[index].hazFreightCarrier = true;
                    $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4021"]);
                }
            } else {
                if (dataobj.carrierMaster.status == 'Block') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightCarrier = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4022"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightCarrier = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4022"]);
                    }
                }

                if (dataobj.carrierMaster.status == 'Hide') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightCarrier = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4023"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightCarrier = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4023"]);
                    }
                }
            }

            //ValidFrom date validation
            if (dataobj.validFrom == null ||
                dataobj.validFrom == undefined ||
                dataobj.validFrom == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Freight') {
                    $scope.errorGenFreightArray[index].errRow = true;
                    $scope.errorGenFreightArray[index].genFreightValidFrom = true;
                    $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4024"]);

                }
                if (maintab == 'hazardous' && subtab == 'Freight') {
                    $scope.errorHazFreightArray[index].errRow = true;
                    $scope.errorHazFreightArray[index].hazFreightValidFrom = true;
                    $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4024"]);
                }

            } else {
                if (dataobj.validTo != undefined && dataobj.validTo != null && dataobj.validTo != "") {

                    var validFrom = $rootScope.convertToDateTime(dataobj.validFrom);
                    var validTo = $rootScope.convertToDateTime(dataobj.validTo);

                    if (validFrom >= validTo) {

                        $scope.errorArr.splice(0, 1, 0);

                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightValidFrom = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4061"]);
                        } else if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightValidFrom = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4061"]);
                        }
                    }

                }

            }

            //ValidTo date validation    
            if (dataobj.validTo == null ||
                dataobj.validTo == undefined ||
                dataobj.validTo == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Freight') {
                    $scope.errorGenFreightArray[index].errRow = true;
                    $scope.errorGenFreightArray[index].genFreightValidTo = true;
                    $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4025"]);

                }
                if (maintab == 'hazardous' && subtab == 'Freight') {
                    $scope.errorHazFreightArray[index].errRow = true;
                    $scope.errorHazFreightArray[index].hazFreightValidTo = true;
                    $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4025"]);
                }

            }




            /************************************************************************************************************************- 
            ------------------------------- More Info Validation--------------------------------------------------------------------
            ****************************************************************************************************************************/



            //StdSell price minimum validation

            if (dataobj.standardSellPrice != undefined && dataobj.standardSellPrice != null) {
                if (dataobj.standardSellPrice.minimum != null && dataobj.standardSellPrice.minimum != undefined && dataobj.standardSellPrice.minimum != "") {

                    if (dataobj.standardSellPrice.minimum == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightMinimum = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4051"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightMinimum = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4051"]);
                        }

                    }


                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.minimum)) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightMinimum = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4057"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightMinimum = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4057"]);
                        }
                    }
                } else {
                    dataobj.standardSellPrice.minimum = null;
                }
                //minus45 validation         
                /* if(dataobj.minus45==null 
              || dataobj.minus45==undefined
              ||dataobj.minus45==""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].genFreightMinus45 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4029"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].hazFreightMinus45 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4029"]); 
           }
           
        }   */

                if (dataobj.standardSellPrice.minus45 != undefined && dataobj.standardSellPrice.minus45 != null &&
                    dataobj.standardSellPrice.minus45 != "") {

                    if (dataobj.standardSellPrice.minus45 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightMinus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4116"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightMinus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4116"]);
                        }

                    }


                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.minus45)) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightMinus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4110"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightMinus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4110"]);
                        }

                    }
                } else {

                    dataobj.standardSellPrice.minus45 = null;
                }

                //plus45 validation                    

                /*if(dataobj.plus45==null 
              || dataobj.plus45==undefined
              ||dataobj.plus45==""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].genFreightPlus45 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4030"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].hazFreightPlus45 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4030"]); 
           }
           
        }   */

                if (dataobj.standardSellPrice.plus45 != undefined && dataobj.standardSellPrice.plus45 != null && dataobj.standardSellPrice.plus45 != "") {
                    if (dataobj.standardSellPrice.plus45 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4117"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4117"]);
                        }

                    }


                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.plus45)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4111"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4111"]);
                        }
                    }
                } else {
                    dataobj.standardSellPrice.plus45 = null;
                }

                //plus100 validation

                /* if(dataobj.plus100==null 
              || dataobj.plus100==undefined
              ||dataobj.plus100==""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].genFreightPlus100 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4031"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].hazFreightPlus100 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4031"]); 
           }
           
        }    */

                if (dataobj.standardSellPrice.plus100 != undefined && dataobj.standardSellPrice.plus100 != null && dataobj.standardSellPrice.plus100 != "") {
                    if (dataobj.standardSellPrice.plus100 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus100 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4118"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus100 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4118"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.plus100)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus100 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4112"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus100 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4112"]);
                        }
                    }
                } else {
                    dataobj.standardSellPrice.plus100 = null;
                }

                //      plus250 validation


                /* if(dataobj.plus250==null 
              || dataobj.plus250==undefined
              ||dataobj.plus250==""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].genFreightPlus250 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4032"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].hazFreightPlus250 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4032"]); 
           }
           
        }   */

                if (dataobj.standardSellPrice.plus300 != undefined && dataobj.standardSellPrice.plus300 != null && dataobj.standardSellPrice.plus300 != "") {
                    if (dataobj.standardSellPrice.plus300 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus250 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4119"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus250 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4119"]);
                        }

                    }


                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.plus300)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus250 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4113"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus250 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4113"]);
                        }
                    }
                } else {
                    dataobj.standardSellPrice.plus300 = null;
                }


                //plus500 validation

                /* if(dataobj.plus500==null 
              || dataobj.plus500==undefined
              ||dataobj.plus500==""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].genFreightPlus500 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4033"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].hazFreightPlus500 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4033"]); 
           }
           
        }   */

                if (dataobj.standardSellPrice.plus500 != undefined && dataobj.standardSellPrice.plus500 != null && dataobj.standardSellPrice.plus500 != "") {
                    if (dataobj.standardSellPrice.plus500 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus500 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4120"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus500 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4120"]);
                        }
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.plus500)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus500 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4114"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus500 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4114"]);
                        }
                    }
                } else {
                    dataobj.standardSellPrice.plus500 = null;
                }

                //plus1000 validation


                /*  if(dataobj.plus1000==null 
              || dataobj.plus1000==undefined
              ||dataobj.plus1000==""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].genFreightPlus1000 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4034"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].hazFreightPlus1000 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4034"]); 
           }
           
        }      */

                if (dataobj.standardSellPrice.plus1000 != undefined && dataobj.standardSellPrice.plus1000 != null && dataobj.standardSellPrice.plus1000 != "") {
                    if (dataobj.standardSellPrice.plus1000 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus1000 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4121"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus1000 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4121"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.standardSellPrice.plus1000)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].genFreightPlus1000 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4115"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].hazFreightPlus1000 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4115"]);
                        }
                    }
                } else {
                    dataobj.standardSellPrice.plus1000 = null;
                }
            }
            //fuel surcharge validation


            /*  if(dataobj.fuelSurcharge==null 
                  || dataobj.fuelSurcharge==undefined
                  ||dataobj.fuelSurcharge==""){
               $scope.errorArr.splice(0,1,0);
               if( maintab=='general' && subtab=='Freight'){
                 $scope.errorGenFreightArray[index].errRow = true;
                 $scope.errorGenFreightArray[index].genFreightFuelSurcharge = true;
                 $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4035"]); 

               }
               if(maintab=='hazardous' && subtab=='Freight') {
                 $scope.errorHazFreightArray[index].errRow = true;
                 $scope.errorHazFreightArray[index].hazFreightFuelSurcharge = true;
                 $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4035"]); 
               }
               
            }   */

            if (dataobj.fuelSurcharge != undefined && dataobj.fuelSurcharge != null && dataobj.fuelSurcharge != "") {
                if (dataobj.fuelSurcharge == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightFuelSurcharge = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4064"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightFuelSurcharge = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4064"]);
                    }

                }


                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.fuelSurcharge)) {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightFuelSurcharge = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4062"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightFuelSurcharge = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4062"]);
                    }

                }
            } else {
                dataobj.fuelSurcharge = null;
            }

            //security surcharge

            /*  if(dataobj.securitySurcharge==null 
                  || dataobj.securitySurcharge==undefined
                  ||dataobj.securitySurcharge==""){
               $scope.errorArr.splice(0,1,0);
               if( maintab=='general' && subtab=='Freight'){
                 $scope.errorGenFreightArray[index].errRow = true;
                 $scope.errorGenFreightArray[index].genFreightSecuritySurcharge = true;
                 $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4036"]); 

               }
               if(maintab=='hazardous' && subtab=='Freight') {
                 $scope.errorHazFreightArray[index].errRow = true;
                 $scope.errorHazFreightArray[index].hazFreightSecuritySurcharge = true;
                 $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4036"]); 
               }
               
            } */

            if (dataobj.securitySurcharge != undefined && dataobj.securitySurcharge != null && dataobj.securitySurcharge != "") {
                if (dataobj.securitySurcharge == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightSecuritySurcharge = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4065"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightSecuritySurcharge = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4065"]);
                    }

                }


                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.securitySurcharge)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].genFreightSecuritySurcharge = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4063"]);

                    }
                    if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].hazFreightSecuritySurcharge = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4063"]);
                    }
                }
            } else {
                dataobj.securitySurcharge = null
            }

            /*------------------------------Min Sell Price Starts----------------------------------------------------*/

            if (dataobj.minimumSellPrice != undefined && dataobj.minimumSellPrice != null && dataobj.minimumSellPrice != "") {
                if (dataobj.minimumSellPrice.minimum != null && dataobj.minimumSellPrice.minimum != undefined && dataobj.minimumSellPrice.minimum != "") {

                    if (dataobj.minimumSellPrice.minimum == 0) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightMinimum = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4049"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightMinimum = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4049"]);
                        }
                    }


                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.minimum)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightMinimum = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4055"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightMinimum = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4055"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.minimum = null;
                }

                //minus45 validation

                /* if(dataobj.minimumSellPrice.minus45 == null 
              || dataobj.minimumSellPrice.minus45 == undefined
              ||dataobj.minimumSellPrice.minus45 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].minFreightMinus45 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4066"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].minFreightMinus45 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4066"]); 
           }
           
        } */

                if (dataobj.minimumSellPrice.minus45 != undefined && dataobj.minimumSellPrice.minus45 != null && dataobj.minimumSellPrice.minus45 != "") {
                    if (dataobj.minimumSellPrice.minus45 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightMinus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4128"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightMinus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4128"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.minus45)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightMinus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4122"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightMinus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4122"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.minus45 = null;
                }
                //plus45 validation

                /*  if(dataobj.minimumSellPrice.plus45 == null 
              || dataobj.minimumSellPrice.plus45 == undefined
              ||dataobj.minimumSellPrice.plus45 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].minFreightPlus45 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4067"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].minFreightPlus45 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4067"]); 
           }
           
        } */

                if (dataobj.minimumSellPrice.plus45 != undefined && dataobj.minimumSellPrice.plus45 != null && dataobj.minimumSellPrice.plus45 != "") {
                    if (dataobj.minimumSellPrice.plus45 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4129"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4129"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.plus45)) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4123"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4123"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.plus45 = null;
                }

                //plus100 validation

                /* if(dataobj.minimumSellPrice.plus100 == null 
              || dataobj.minimumSellPrice.plus100 == undefined
              ||dataobj.minimumSellPrice.plus100 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].minFreightPlus100 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4068"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].minFreightPlus100 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4068"]); 
           }
           
        } */

                if (dataobj.minimumSellPrice.plus100 != undefined && dataobj.minimumSellPrice.plus100 != null &&
                    dataobj.minimumSellPrice.plus100 != "") {
                    if (dataobj.minimumSellPrice.plus100 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus100 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4130"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus100 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4130"]);
                        }
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.plus100)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus100 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4124"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus100 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4124"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.plus100 = null;
                }

                //plus250 validation

                /* if(dataobj.minimumSellPrice.plus250 == null 
              || dataobj.minimumSellPrice.plus250 == undefined
              ||dataobj.minimumSellPrice.plus250 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].minFreightPlus250 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4069"]); 

           }
           if(maintab=='hazardous' && subtab == 'Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].minFreightPlus250 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4069"]); 
           }
           
        } */

                if (dataobj.minimumSellPrice.plus300 != undefined && dataobj.minimumSellPrice.plus300 != null &&
                    dataobj.minimumSellPrice.plus300 != "") {
                    if (dataobj.minimumSellPrice.plus300 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus250 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4131"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus250 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4131"]);
                        }
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.plus300)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus250 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4125"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus250 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4125"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.plus300 = null;
                }

                //plus500 validation

                /* if(dataobj.minimumSellPrice.plus500 == null 
              || dataobj.minimumSellPrice.plus500 == undefined
              ||dataobj.minimumSellPrice.plus500 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].minFreightPlus500 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4070"]); 

           }
           if(maintab=='hazardous' && subtab == 'Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].minFreightPlus500 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4070"]); 
           }
           
        } */

                if (dataobj.minimumSellPrice.plus500 != undefined && dataobj.minimumSellPrice.plus500 != null &&
                    dataobj.minimumSellPrice.plus500 != "") {
                    if (dataobj.minimumSellPrice.plus500 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus500 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4132"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus500 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4132"]);
                        }
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.plus500)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus500 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4126"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus500 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4126"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.plus500 = null;
                }

                //plus1000 validation

                /*if(dataobj.minimumSellPrice.plus1000 == null 
              || dataobj.minimumSellPrice.plus1000 == undefined
              ||dataobj.minimumSellPrice.plus1000 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].minFreightPlus1000 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4071"]); 

           }
           if(maintab=='hazardous' && subtab == 'Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].minFreightPlus1000 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4071"]); 
           }
           
        } */

                if (dataobj.minimumSellPrice.plus1000 != undefined && dataobj.minimumSellPrice.plus1000 != null &&
                    dataobj.minimumSellPrice.plus1000 != "") {
                    if (dataobj.minimumSellPrice.plus1000 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus1000 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4133"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus1000 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4133"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.plus1000)) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].minFreightPlus1000 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4127"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].minFreightPlus1000 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4127"]);
                        }
                    }
                } else {
                    dataobj.minimumSellPrice.plus1000 = null;
                }

            } else {
                dataobj.minimumSellPrice = null;
            }
            //-------------------------------Cost validation------------------------------------------------------------------------------------------

            if (dataobj.cost != undefined && dataobj.cost != null && dataobj.cost != "") {

                if (dataobj.cost.minimum != null &&
                    dataobj.cost.minimum != undefined &&
                    dataobj.cost.minimum != "") {
                    if (dataobj.cost.minimum == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightMinimum = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4053"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightMinimum = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4053"]);
                        }
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.cost.minimum)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightMinimum = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4059"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightMinimum = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4059"]);
                        }
                    }
                } else {
                    dataobj.cost.minimum = null;
                }

                //minus45 validation

                /*  if(dataobj.cost.minus45 == null 
              || dataobj.cost.minus45 == undefined
              ||dataobj.cost.minus45 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].costFreightMinus45 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4066"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].costFreightMinus45 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4066"]); 
           }
           
        } */

                if (dataobj.cost.minus45 != undefined && dataobj.cost.minus45 != null && dataobj.cost.minus45 != "") {
                    if (dataobj.cost.minus45 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightMinus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4140"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightMinus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4140"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.minus45)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightMinus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4134"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightMinus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4134"]);
                        }
                    }
                } else {
                    dataobj.cost.minus45 = null;
                }

                //plus45 validation

                /*   if(dataobj.cost.plus45 == null 
                      || dataobj.cost.plus45 == undefined
                      ||dataobj.cost.plus45 == ""){
                   $scope.errorArr.splice(0,1,0);
                   if( maintab=='general' && subtab=='Freight'){
                     $scope.errorGenFreightArray[index].errRow = true;
                     $scope.errorGenFreightArray[index].costFreightPlus45 = true;
                     $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4067"]); 

                   }
                   if(maintab=='hazardous' && subtab=='Freight') {
                     $scope.errorHazFreightArray[index].errRow = true;
                     $scope.errorHazFreightArray[index].costFreightPlus45 = true;
                     $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4067"]); 
                   }
                   
                } */

                if (dataobj.cost.plus45 != undefined && dataobj.cost.plus45 != null && dataobj.cost.plus45 != "") {
                    if (dataobj.cost.plus45 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4141"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4141"]);
                        }
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minimumSellPrice.plus45)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus45 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4135"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus45 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4135"]);
                        }
                    }
                } else {
                    dataobj.cost.plus45 = null;
                }

                //plus100 validation

                /*if(dataobj.cost.plus100 == null 
              || dataobj.cost.plus100 == undefined
              ||dataobj.cost.plus100 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].costFreightPlus100 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4068"]); 

           }
           if(maintab=='hazardous' && subtab=='Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].costFreightPlus100 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4068"]); 
           }
           
        }   */

                if (dataobj.cost.plus100 != undefined && dataobj.cost.plus100 != null && dataobj.cost.plus100 != "") {
                    if (dataobj.cost.plus100 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus100 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4142"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus100 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4142"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.cost.plus100)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus100 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4136"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus100 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4136"]);
                        }
                    }
                } else {
                    dataobj.cost.plus100 = null;
                }

                //plus250 validation

                /*  if(dataobj.cost.plus250 == null 
              || dataobj.cost.plus250 == undefined
              ||dataobj.cost.plus250 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].costFreightPlus250 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4069"]); 

           }
           if(maintab=='hazardous' && subtab == 'Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].costFreightPlus250 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4069"]); 
           }
           
        } */

                if (dataobj.cost.plus300 != undefined && dataobj.cost.plus300 != null && dataobj.cost.plus300 != "") {
                    if (dataobj.cost.plus300 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus250 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4143"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus250 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4143"]);
                        }

                    }

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.cost.plus300)) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus250 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4137"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus250 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4137"]);
                        }
                    }
                } else {
                    dataobj.cost.plus300 = null;
                }
                //plus500 validation

                /* if(dataobj.cost.plus500 == null 
              || dataobj.cost.plus500 == undefined
              ||dataobj.cost.plus500 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].costFreightPlus500 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4070"]); 

           }
           if(maintab=='hazardous' && subtab == 'Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].costFreightPlus500 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4070"]); 
           }
           
        }       
           */
                if (dataobj.cost.plus500 != undefined && dataobj.cost.plus500 != null && dataobj.cost.plus500 != "") {
                    if (dataobj.cost.plus500 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus500 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4144"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus500 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4144"]);
                        }

                    }


                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.cost.plus500)) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus500 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4138"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus500 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4138"]);
                        }
                    }
                } else {
                    dataobj.cost.plus500 = null;
                }

                //plus1000 validation

                /*if(dataobj.cost.plus1000 == null 
              || dataobj.cost.plus1000 == undefined
              ||dataobj.cost.plus1000 == ""){
           $scope.errorArr.splice(0,1,0);
           if( maintab=='general' && subtab=='Freight'){
             $scope.errorGenFreightArray[index].errRow = true;
             $scope.errorGenFreightArray[index].costFreightPlus1000 = true;
             $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4071"]); 

           }
           if(maintab=='hazardous' && subtab == 'Freight') {
             $scope.errorHazFreightArray[index].errRow = true;
             $scope.errorHazFreightArray[index].costFreightPlus1000 = true;
             $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4071"]); 
           }
           
        } */

                if (dataobj.cost.plus1000 != undefined && dataobj.cost.plus1000 != null && dataobj.cost.plus1000 != "") {
                    if (dataobj.cost.plus1000 == 0) {

                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus1000 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4145"]);

                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus1000 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4145"]);
                        }

                    }
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.cost.plus1000)) {
                        $scope.errorArr.splice(0, 1, 0);
                        if (maintab == 'general' && subtab == 'Freight') {
                            $scope.errorGenFreightArray[index].errRow = true;
                            $scope.errorGenFreightArray[index].costFreightPlus1000 = true;
                            $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4139"]);
                        }
                        if (maintab == 'hazardous' && subtab == 'Freight') {
                            $scope.errorHazFreightArray[index].errRow = true;
                            $scope.errorHazFreightArray[index].costFreightPlus1000 = true;
                            $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4139"]);
                        }
                    }
                } else {
                    dataobj.cost.plus1000 = null;
                }
            } else {
                dataobj.cost = null;
            }
            //---------------------------------Fuel Surcharge Validation---------------------------------------------------------

            /*  if(dataobj.fuelValidFrom == null 
                  || dataobj.fuelValidFrom == undefined
                  ||dataobj.fuelValidFrom == ""){
               $scope.errorArr.splice(0,1,0);
               if( maintab=='general' && subtab=='Freight'){
                 $scope.errorGenFreightArray[index].errRow = true;
                 $scope.errorGenFreightArray[index].freightFuelValidFrom = true;
                 $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4084"]); 

               }
               if(maintab=='hazardous' && subtab == 'Freight') {
                 $scope.errorHazFreightArray[index].errRow = true;
                 $scope.errorHazFreightArray[index].freightFuelValidFrom = true;
                 $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4084"]); 
               }
               
            } */
            if (dataobj.fuelValidFrom != undefined &&
                dataobj.fuelValidFrom != null &&
                dataobj.fuelValidFrom != "" &&
                dataobj.fuelValidTo != undefined &&
                dataobj.fuelValidTo != null &&
                dataobj.fuelValidTo != "") {

                var validFrom = $rootScope.convertToDateTime(dataobj.fuelValidFrom);
                var validTo = $rootScope.convertToDateTime(dataobj.fuelValidTo);

                if (validFrom >= validTo) {

                    $scope.errorArr.splice(0, 1, 0);

                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].freightFuelValidFrom = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4090"]);
                    } else if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].freightFuelValidFrom = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4090"]);
                    }
                }
            } else {
                dataobj.fuelValidFrom = null;
                dataobj.fuelValidTo = null;
            }

            /* if(dataobj.fuelValidTo == null 
                  || dataobj.fuelValidTo == undefined
                  ||dataobj.fuelValidTo == ""){
               $scope.errorArr.splice(0,1,0);
               if( maintab=='general' && subtab=='Freight'){
                 $scope.errorGenFreightArray[index].errRow = true;
                 $scope.errorGenFreightArray[index].freightFuelValidTo = true;
                 $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4085"]); 

               }
               if(maintab=='hazardous' && subtab == 'Freight') {
                 $scope.errorHazFreightArray[index].errRow = true;
                 $scope.errorHazFreightArray[index].freightFuelValidTo = true;
                 $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4085"]); 
               }
               
            }  */

            /*   if(dataobj.securityChargeValidFrom == null 
          || dataobj.securityChargeValidFrom == undefined
          ||dataobj.securityChargeValidFrom == ""){
       $scope.errorArr.splice(0,1,0);
       if( maintab=='general' && subtab=='Freight'){
         $scope.errorGenFreightArray[index].errRow = true;
         $scope.errorGenFreightArray[index].freightSecurityValidFrom = true;
         $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4086"]); 

       }
       if(maintab=='hazardous' && subtab == 'Freight') {
         $scope.errorHazFreightArray[index].errRow = true;
         $scope.errorHazFreightArray[index].freightSecurityValidFrom = true;
         $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4086"]); 
       }
       
    } */
            if (dataobj.securityChargeValidFrom != undefined &&
                dataobj.securityChargeValidFrom != null &&
                dataobj.securityChargeValidFrom != "" &&
                dataobj.securityChargeValidTo != undefined &&
                dataobj.securityChargeValidTo != null &&
                dataobj.securityChargeValidTo != "") {

                var validFrom = $rootScope.convertToDateTime(dataobj.securityChargeValidFrom);
                var validTo = $rootScope.convertToDateTime(dataobj.securityChargeValidTo);

                if (validFrom >= validTo) {

                    $scope.errorArr.splice(0, 1, 0);

                    if (maintab == 'general' && subtab == 'Freight') {
                        $scope.errorGenFreightArray[index].errRow = true;
                        $scope.errorGenFreightArray[index].freightSecurityValidFrom = true;
                        $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4091"]);
                    } else if (maintab == 'hazardous' && subtab == 'Freight') {
                        $scope.errorHazFreightArray[index].errRow = true;
                        $scope.errorHazFreightArray[index].freightFuelValidFrom = true;
                        $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4091"]);
                    }

                }


            } else {
                dataobj.securityChargeValidFrom = null;
                dataobj.securityChargeValidTo = null;
            }

            /*  if(dataobj.securityChargeValidTo == null 
            || dataobj.securityChargeValidTo == undefined
            ||dataobj.securityChargeValidTo == ""){
         $scope.errorArr.splice(0,1,0);
         if( maintab=='general' && subtab=='Freight'){
           $scope.errorGenFreightArray[index].errRow = true;
           $scope.errorGenFreightArray[index].freightSecurityValidTo = true;
           $scope.errorGenFreightArray[index].errTextArr.push($rootScope.nls["ERR4087"]); 

         }
         if(maintab=='hazardous' && subtab == 'Freight') {
           $scope.errorHazFreightArray[index].errRow = true;
           $scope.errorHazFreightArray[index].freightSecurityValidTo = true;
           $scope.errorHazFreightArray[index].errTextArr.push($rootScope.nls["ERR4087"]); 
         }
         
      }          
        */

            if ($scope.errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }
        }

        $scope.finalValidate = function(maintab, subtab, dataobj, index) {

            //charge validation
            if (dataobj.chargeMaster == null ||
                dataobj.chargeMaster == undefined ||
                dataobj.chargeMaster == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgCharge = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4012"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestCharge = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4012"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgCharge = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4012"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestCharge = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4012"]);
                }

            } else {

                if (dataobj.chargeMaster.status == 'Block') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCharge = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4013"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCharge = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4013"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCharge = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4013"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCharge = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4013"]);
                    }

                }

                if (dataobj.chargeMaster.status == 'Hide') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCharge = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4014"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCharge = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4014"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCharge = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4014"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCharge = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4014"]);
                    }

                }
            } // charge validation ends


            //unit validation  
            if (dataobj.unitMaster == null ||
                dataobj.unitMaster == undefined ||
                dataobj.unitMaster == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgUnit = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4015"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestUnit = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4015"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgUnit = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4015"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestUnit = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4015"]);
                }

            } else {


                if (dataobj.unitMaster.status == 'Block') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCharge = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4016"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCharge = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4016"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCharge = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4016"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCharge = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4016"]);
                    }

                }

                if (dataobj.unitMaster.status == 'Hide') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgUnit = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4017"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestUnit = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4017"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgUnit = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4017"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestUnit = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4017"]);
                    }

                }




            }


            //currency validation

            if (dataobj.currencyMaster == null ||
                dataobj.currencyMaster == undefined ||
                dataobj.currencyMaster == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgCurrency = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4018"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestCurrency = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4018"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgCurrency = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4018"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestCurrency = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4018"]);
                }

            } else {

                if (dataobj.currencyMaster.status == 'Block') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCharge = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4019"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCharge = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4019"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCharge = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4019"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCharge = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4019"]);
                    }

                }

                if (dataobj.currencyMaster.status == 'Hide') {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCurrency = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4020"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCurrency = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4020"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCurrency = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4020"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCurrency = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4020"]);
                    }

                }


            }


            //valid fromdate validation
            if (dataobj.validFromDate == null ||
                dataobj.validFromDate == undefined ||
                dataobj.validFromDate == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgValidFrom = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4024"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestValidFrom = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4024"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgValidFrom = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4024"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestValidFrom = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4024"]);
                }

            } else {

                if (dataobj.validToDate != undefined && dataobj.validToDate != null && dataobj.validToDate != "") {

                    var validFromDate = $rootScope.convertToDateTime(dataobj.validFromDate);
                    var validToDate = $rootScope.convertToDateTime(dataobj.validToDate);
                    if (validFromDate >= validToDate) {

                        $scope.errorArr.splice(0, 1, 0);

                        if (maintab == 'general' && subtab == 'Origin') {
                            $scope.errorGenOrgArray[index].errRow = true;
                            $scope.errorGenOrgArray[index].genOrgValidFrom = true;
                            $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4061"]);

                        } else if (maintab == 'general' && subtab == 'Destination') {
                            $scope.errorGenDestArray[index].errRow = true;
                            $scope.errorGenDestArray[index].genDestValidFrom = true;
                            $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4061"]);
                        } else if (maintab == 'hazardous' && subtab == 'Origin') {
                            $scope.errorHazOrgArray[index].errRow = true;
                            $scope.errorHazOrgArray[index].hazOrgValidFrom = true;
                            $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4061"]);
                        } else if (maintab == 'hazardous' && subtab == 'Destination') {
                            $scope.errorHazDestArray[index].errRow = true;
                            $scope.errorHazDestArray[index].hazDestValidFrom = true;
                            $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4061"]);
                        }



                    }

                }

            }


            //valid fromTo validation      


            if (dataobj.validToDate == null ||
                dataobj.validToDate == undefined ||
                dataobj.validToDate == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgValidTo = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4025"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestValidTo = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4025"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgValidTo = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4025"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestValidTo = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4025"]);
                }

            }


            if (dataobj.minSelInAmount != undefined && dataobj.minSelInAmount != null && dataobj.minSelInAmount != "") {
                if (dataobj.minSelInAmount == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgMinSelInAmount = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4050"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestMinSelInAmount = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4050"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgMinSelInAmount = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4050"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestMinSelInAmount = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4050"]);
                    }
                }

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minSelInAmount)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgMinSelInAmount = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4056"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestMinSelInAmount = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4056"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgMinSelInAmount = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4056"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestMinSelInAmount = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4056"]);
                    }

                }

            } else {
                dataobj.minSelInAmount = null;
            }


            //StdellPrice minimum validation


            if (dataobj.stdSelInMinimum == null || dataobj.stdSelInMinimum == undefined || dataobj.stdSelInMinimum == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgStdSelInMinimum = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4026"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestStdSelInMinimum = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4026"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgStdSelInMinimum = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4026"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestStdSelInMinimum = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4026"]);
                }

            } else {


                if (dataobj.stdSelInMinimum == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgStdSelInMinimum = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4051"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestStdSelInMinimum = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4051"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgStdSelInMinimum = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4051"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestStdSelInMinimum = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4051"]);
                    }
                }



                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.stdSelInMinimum)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgStdSelInMinimum = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4057"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestStdSelInMinimum = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4057"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgStdSelInMinimum = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4057"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestStdSelInMinimum = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4057"]);
                    }
                }
            }



            //StdSellPriceAmount Validation

            if (dataobj.stdSelInAmount == null ||
                dataobj.stdSelInAmount == undefined ||
                dataobj.stdSelInAmount == "") {
                $scope.errorArr.splice(0, 1, 0);
                if (maintab == 'general' && subtab == 'Origin') {
                    $scope.errorGenOrgArray[index].errRow = true;
                    $scope.errorGenOrgArray[index].genOrgStdSelInAmount = true;
                    $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4027"]);

                } else if (maintab == 'general' && subtab == 'Destination') {
                    $scope.errorGenDestArray[index].errRow = true;
                    $scope.errorGenDestArray[index].genDestStdSelInAmount = true;
                    $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4027"]);
                } else if (maintab == 'hazardous' && subtab == 'Origin') {
                    $scope.errorHazOrgArray[index].errRow = true;
                    $scope.errorHazOrgArray[index].hazOrgStdSelInAmount = true;
                    $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4027"]);
                } else if (maintab == 'hazardous' && subtab == 'Destination') {
                    $scope.errorHazDestArray[index].errRow = true;
                    $scope.errorHazDestArray[index].hazDestStdSelInAmount = true;
                    $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4027"]);
                }

            } else {
                if (dataobj.stdSelInAmount == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgStdSelInAmount = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4052"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestStdSelInAmount = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4052"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgStdSelInAmount = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4052"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestStdSelInAmount = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4052"]);
                    }
                }


                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.stdSelInAmount)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgStdSelInAmount = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4058"]);
                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestStdSelInAmount = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4058"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgStdSelInAmount = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4058"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestStdSelInAmount = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4058"]);
                    }
                }
            }


            if (dataobj.costInAmount != null && dataobj.costInAmount != undefined && dataobj.costInAmount != "") {
                if (dataobj.costInAmount == 0) {
                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCostInAmount = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4054"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCostInAmount = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4054"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCostInAmount = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4054"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCostInAmount = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4054"]);
                    }
                }

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.costInAmount)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCostInAmount = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4060"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCostInAmount = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4060"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCostInAmount = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4060"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCostInAmount = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4060"]);
                    }
                }
            } else {

                dataobj.costInAmount = null;
            }



            //MinSellPrice minimum validation


            if (dataobj.minSelInMinimum != null &&
                dataobj.minSelInMinimum != undefined && dataobj.minSelInMinimum != ""
            ) {



                if (dataobj.minSelInMinimum == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgMinSelInMinimum = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4049"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestMinSelInMinimum = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4049"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgMinSelInMinimum = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4049"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestMinSelInMinimum = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4049"]);
                    }
                }



                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.minSelInMinimum)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgMinSelInMinimum = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4055"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestMinSelInMinimum = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4055"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgMinSelInMinimum = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4055"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestMinSelInMinimum = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4055"]);
                    }
                }
            } else {

                dataobj.minSelInMinimum = null;
            }




            //Cost minimum validation


            if (dataobj.costInMinimum != null &&
                dataobj.costInMinimum != undefined && dataobj.costInMinimum != ""
            ) {

                if (dataobj.costInMinimum == 0) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCostInMinimum = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4053"]);

                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCostInMinimum = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4053"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCostInMinimum = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4053"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCostInMinimum = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4053"]);
                    }
                }
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX, dataobj.costInMinimum)) {

                    $scope.errorArr.splice(0, 1, 0);
                    if (maintab == 'general' && subtab == 'Origin') {
                        $scope.errorGenOrgArray[index].errRow = true;
                        $scope.errorGenOrgArray[index].genOrgCostInMinimum = true;
                        $scope.errorGenOrgArray[index].errTextArr.push($rootScope.nls["ERR4059"]);
                    } else if (maintab == 'general' && subtab == 'Destination') {
                        $scope.errorGenDestArray[index].errRow = true;
                        $scope.errorGenDestArray[index].genDestCostInMinimum = true;
                        $scope.errorGenDestArray[index].errTextArr.push($rootScope.nls["ERR4059"]);
                    } else if (maintab == 'hazardous' && subtab == 'Origin') {
                        $scope.errorHazOrgArray[index].errRow = true;
                        $scope.errorHazOrgArray[index].hazOrgCostInMinimum = true;
                        $scope.errorHazOrgArray[index].errTextArr.push($rootScope.nls["ERR4059"]);
                    } else if (maintab == 'hazardous' && subtab == 'Destination') {
                        $scope.errorHazDestArray[index].errRow = true;
                        $scope.errorHazDestArray[index].hazDestCostInMinimum = true;
                        $scope.errorHazDestArray[index].errTextArr.push($rootScope.nls["ERR4059"]);
                    }
                }
            } else {

            }
            if (dataobj.stdSelInAmount < dataobj.costInAmount || dataobj.stdSelInAmount < dataobj.minSelInAmount) {
                Notification.error("SSR amount cannot be less than Cost amount OR MSR amount !");
                $scope.isSSR_OK = false;
                return false;
            }

            if ($scope.errorArr.indexOf(0) < 0) {
                $scope.isSSR_OK = true;
                return true
            } else {
                return false;
            }

        }


        $scope.saveOrUpdate = function(flag) {

            if ($scope.validatePricingAir(0) && $scope.validation('general', 'Origin') &&
                $scope.validation('general', 'Freight') &&
                $scope.validation('general', 'Destination') &&
                $scope.validation('hazardous', 'Origin') &&
                $scope.validation('hazardous', 'Freight') &&
                $scope.validation('hazardous', 'Destination')) {
                if ($scope.pricingMaster != undefined && ($scope.pricingMaster.generalOriginPriceList == null || $scope.pricingMaster.generalOriginPriceList.length == 0) &&
                    ($scope.pricingMaster.generalDestinationPriceList == null || $scope.pricingMaster.generalDestinationPriceList.length == 0) &&
                    ($scope.pricingMaster.generalFreightPriceList == null || $scope.pricingMaster.generalFreightPriceList.length == 0) &&
                    ($scope.pricingMaster.hazardousOriginPriceList == null || $scope.pricingMaster.hazardousOriginPriceList.length == 0) &&
                    ($scope.pricingMaster.hazardousDestinationPriceList == null || $scope.pricingMaster.hazardousDestinationPriceList.length == 0) &&
                    ($scope.pricingMaster.hazardousFreightPriceList == null || $scope.pricingMaster.hazardousFreightPriceList.length == 0)
                ) {
                    $rootScope.clientMessage = $rootScope.nls["ERR4109"];
                    $scope.init();
                    return;
                }


                if (!$scope.attachConfig.isAttachmentCheck()) {
                    $rootScope.clientMessage = $rootScope.nls['ERR250'];
                    console.log("Attachment is invalid");
                    return false;
                }


                $scope.tmpPricingAirObj = $scope.prepareToSave($scope.pricingMaster);
                $scope.tmpPricingAirObj.whoRouted = $scope.tmpPricingAirObj.whoRouted == false ? 'Self' : 'Agent';
                if (flag) {
                    PricingAirSave.save($scope.tmpPricingAirObj).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("CarrierRate Saved successfully..");
                            var params = {};
                            params.submitAction = 'Saved';
                            $rootScope.successDesc = $rootScope.nls["ERR400"];
                            $state.go("layout.pricingAir", params);
                        } else {
                            console.log("pricingMaster Saving Failed " + data.responseDescription)
                            $rootScope.clientMessage = $rootScope.nls[data.responseCode];
                        }
                    }, function(error) {
                        console.log("pricingMaster Saving Failed : " + error)
                        $scope.init();
                    });

                } else {
                    PricingAirEdit.update($scope.tmpPricingAirObj).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("PricingAir update successfully..");
                            var params = {};
                            params.submitAction = 'Saved';
                            $rootScope.successDesc = $rootScope.nls["ERR401"];
                            $state.go("layout.pricingAir", params);
                        } else {
                            console.log("pricingMaster Update Failed " + data.responseDescription)
                            $rootScope.clientMessage = $rootScope.nls[data.responseCode];
                        }
                    }, function(error) {
                        console.log("pricingMaster Update Failed : " + error)
                        $scope.init();
                    });
                }
            }
        }


        $scope.prepareToSave = function(pricingMaster) {

            console.log("Save savePricingAir method is called.", pricingMaster);

            if (pricingMaster != undefined && pricingMaster != null) {

                $scope.tmpPricingAirObj = cloneService.clone(pricingMaster);

                $scope.tmpGenOrgChargeDataList = $scope.tmpPricingAirObj.generalOriginPriceList;
                for (var i = 0; i < $scope.tmpGenOrgChargeDataList.length; i++) {
                    $scope.tmpGenOrgChargeDataList[i].validFromDate = $rootScope.sendApiStartDateTime($scope.tmpGenOrgChargeDataList[i].validFromDate);
                    $scope.tmpGenOrgChargeDataList[i].validToDate = $rootScope.sendApiEndDateTime($scope.tmpGenOrgChargeDataList[i].validToDate);
                }
                $scope.tmpGenFrieightDataList = $scope.tmpPricingAirObj.generalFreightPriceList;
                for (var i = 0; i < $scope.tmpGenFrieightDataList.length; i++) {
                    $scope.tmpGenFrieightDataList[i].validFrom = $rootScope.sendApiStartDateTime($scope.tmpGenFrieightDataList[i].validFrom);
                    $scope.tmpGenFrieightDataList[i].validTo = $rootScope.sendApiEndDateTime($scope.tmpGenFrieightDataList[i].validTo);
                    $scope.tmpGenFrieightDataList[i].fuelValidFrom = $rootScope.sendApiStartDateTime($scope.tmpGenFrieightDataList[i].fuelValidFrom);
                    $scope.tmpGenFrieightDataList[i].fuelValidTo = $rootScope.sendApiEndDateTime($scope.tmpGenFrieightDataList[i].fuelValidTo);
                    $scope.tmpGenFrieightDataList[i].securityChargeValidFrom = $rootScope.sendApiStartDateTime($scope.tmpGenFrieightDataList[i].securityChargeValidFrom);
                    $scope.tmpGenFrieightDataList[i].securityChargeValidTo = $rootScope.sendApiEndDateTime($scope.tmpGenFrieightDataList[i].securityChargeValidTo);
                }

                $scope.tmpGenDestinationDataList = $scope.tmpPricingAirObj.generalDestinationPriceList;
                for (var i = 0; i < $scope.tmpGenDestinationDataList.length; i++) {
                    $scope.tmpGenDestinationDataList[i].validFromDate = $rootScope.sendApiStartDateTime($scope.tmpGenDestinationDataList[i].validFromDate);
                    $scope.tmpGenDestinationDataList[i].validToDate = $rootScope.sendApiEndDateTime($scope.tmpGenDestinationDataList[i].validToDate);

                }
                $scope.tmpHazOrgChargeDataList = $scope.tmpPricingAirObj.hazardousOriginPriceList;
                for (var i = 0; i < $scope.tmpHazOrgChargeDataList.length; i++) {
                    $scope.tmpHazOrgChargeDataList[i].validFromDate = $rootScope.sendApiStartDateTime($scope.tmpHazOrgChargeDataList[i].validFromDate);
                    $scope.tmpHazOrgChargeDataList[i].validToDate = $rootScope.sendApiEndDateTime($scope.tmpHazOrgChargeDataList[i].validToDate);

                }
                $scope.tmpHazFreightDataList = $scope.tmpPricingAirObj.hazardousFreightPriceList;
                for (var i = 0; i < $scope.tmpHazFreightDataList.length; i++) {
                    $scope.tmpHazFreightDataList[i].validFrom = $rootScope.sendApiStartDateTime($scope.tmpHazFreightDataList[i].validFrom);
                    $scope.tmpHazFreightDataList[i].validTo = $rootScope.sendApiEndDateTime($scope.tmpHazFreightDataList[i].validTo);
                    $scope.tmpHazFreightDataList[i].fuelValidFrom = $rootScope.sendApiStartDateTime($scope.tmpHazFreightDataList[i].fuelValidFrom);
                    $scope.tmpHazFreightDataList[i].fuelValidTo = $rootScope.sendApiEndDateTime($scope.tmpHazFreightDataList[i].fuelValidTo);
                    $scope.tmpHazFreightDataList[i].securityChargeValidFrom = $rootScope.sendApiStartDateTime($scope.tmpHazFreightDataList[i].securityChargeValidFrom);
                    $scope.tmpHazFreightDataList[i].securityChargeValidTo = $rootScope.sendApiEndDateTime($scope.tmpHazFreightDataList[i].securityChargeValidTo);

                }
                $scope.tmpHazDestinationDataList = $scope.tmpPricingAirObj.hazardousDestinationPriceList;
                for (var i = 0; i < $scope.tmpHazDestinationDataList.length; i++) {
                    $scope.tmpHazDestinationDataList[i].validFromDate = $rootScope.sendApiStartDateTime($scope.tmpHazDestinationDataList[i].validFromDate);
                    $scope.tmpHazDestinationDataList[i].validToDate = $rootScope.sendApiEndDateTime($scope.tmpHazDestinationDataList[i].validToDate);

                }


                return $scope.tmpPricingAirObj;


            }
        }

        $scope.editDate = function() {


            $scope.tmpGenOrgChargeDataList = $scope.pricingMaster.generalOriginPriceList;
            for (var i = 0; i < $scope.tmpGenOrgChargeDataList.length; i++) {
                $scope.tmpGenOrgChargeDataList[i].validFromDate = $rootScope.dateAndTimeToString($scope.tmpGenOrgChargeDataList[i].validFromDate);
                $scope.tmpGenOrgChargeDataList[i].validToDate = $rootScope.dateAndTimeToString($scope.tmpGenOrgChargeDataList[i].validToDate);
            }
            $scope.tmpGenFrieightDataList = $scope.pricingMaster.generalFreightPriceList;
            for (var i = 0; i < $scope.tmpGenFrieightDataList.length; i++) {
                $scope.tmpGenFrieightDataList[i].validFrom = $rootScope.dateAndTimeToString($scope.tmpGenFrieightDataList[i].validFrom);
                $scope.tmpGenFrieightDataList[i].validTo = $rootScope.dateAndTimeToString($scope.tmpGenFrieightDataList[i].validTo);
                $scope.tmpGenFrieightDataList[i].fuelValidFrom = $rootScope.dateAndTimeToString($scope.tmpGenFrieightDataList[i].fuelValidFrom);
                $scope.tmpGenFrieightDataList[i].fuelValidTo = $rootScope.dateAndTimeToString($scope.tmpGenFrieightDataList[i].fuelValidTo);
                $scope.tmpGenFrieightDataList[i].securityChargeValidFrom = $rootScope.dateAndTimeToString($scope.tmpGenFrieightDataList[i].securityChargeValidFrom);
                $scope.tmpGenFrieightDataList[i].securityChargeValidTo = $rootScope.dateAndTimeToString($scope.tmpGenFrieightDataList[i].securityChargeValidTo);
            }

            $scope.tmpGenDestinationDataList = $scope.pricingMaster.generalDestinationPriceList;
            for (var i = 0; i < $scope.tmpGenDestinationDataList.length; i++) {
                $scope.tmpGenDestinationDataList[i].validFromDate = $rootScope.dateAndTimeToString($scope.tmpGenDestinationDataList[i].validFromDate);
                $scope.tmpGenDestinationDataList[i].validToDate = $rootScope.dateAndTimeToString($scope.tmpGenDestinationDataList[i].validToDate);

            }
            $scope.tmpHazOrgChargeDataList = $scope.pricingMaster.hazardousOriginPriceList;
            for (var i = 0; i < $scope.tmpHazOrgChargeDataList.length; i++) {
                $scope.tmpHazOrgChargeDataList[i].validFromDate = $rootScope.dateAndTimeToString($scope.tmpHazOrgChargeDataList[i].validFromDate);
                $scope.tmpHazOrgChargeDataList[i].validToDate = $rootScope.dateAndTimeToString($scope.tmpHazOrgChargeDataList[i].validToDate);

            }
            $scope.tmpHazFreightDataList = $scope.pricingMaster.hazardousFreightPriceList;
            for (var i = 0; i < $scope.tmpHazFreightDataList.length; i++) {
                $scope.tmpHazFreightDataList[i].validFrom = $rootScope.dateAndTimeToString($scope.tmpHazFreightDataList[i].validFrom);
                $scope.tmpHazFreightDataList[i].validTo = $rootScope.dateAndTimeToString($scope.tmpHazFreightDataList[i].validTo);
                $scope.tmpHazFreightDataList[i].fuelValidFrom = $rootScope.dateAndTimeToString($scope.tmpHazFreightDataList[i].fuelValidFrom);
                $scope.tmpHazFreightDataList[i].fuelValidTo = $rootScope.dateAndTimeToString($scope.tmpHazFreightDataList[i].fuelValidTo);
                $scope.tmpHazFreightDataList[i].securityChargeValidFrom = $rootScope.sendApiStartDateTime($scope.tmpHazFreightDataList[i].securityChargeValidFrom);
                $scope.tmpHazFreightDataList[i].securityChargeValidTo = $rootScope.dateAndTimeToString($scope.tmpHazFreightDataList[i].securityChargeValidTo);

            }
            $scope.tmpHazDestinationDataList = $scope.pricingMaster.hazardousDestinationPriceList;
            for (var i = 0; i < $scope.tmpHazDestinationDataList.length; i++) {
                $scope.tmpHazDestinationDataList[i].validFromDate = $rootScope.dateAndTimeToString($scope.tmpHazDestinationDataList[i].validFromDate);
                $scope.tmpHazDestinationDataList[i].validToDate = $rootScope.dateAndTimeToString($scope.tmpHazDestinationDataList[i].validToDate);

            }
        }

        $scope.getPricingAirById = function(id) {
            PricingAirView.get({
                id: id
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.pricingMaster = data.responseObject;
                    $scope.init();
                    $scope.editDate();
                    console.log("Successful while getting Pricing Air.", data)

                }

            }, function(error) {
                console.log("Error while getting Pricing Air.", error)
            });
        }


        //Adding pop up with new columns which is present in DC as per Demo Fb--SathishRobert
        $scope.showMoreInfo = function(index, state, pricingMaster) {
            if (index == undefined || state == undefined || pricingMaster == undefined) {
                Notification.error("oops...some thing went wrong");
            } else {
                if (state == 'Origin') {
                    $scope.pricingPortPair = $scope.pricingMaster.generalOriginPriceList[index];
                } else if (state == 'Destination') {
                    $scope.pricingPortPair = $scope.pricingMaster.generalDestinationPriceList[index];
                } else if (state == 'Freight') {
                    $scope.pricingPortPair = $scope.pricingMaster.generalDestinationPriceList[index];
                } else {
                    console.log("pricing port pair nothing selected");
                }
            }
            $scope.setFalse($scope.pricingPortPair.isClearance);
            $scope.setFalse($scope.pricingPortPair.isForwarderOrDirect);
            $scope.setFalse($scope.pricingPortPair.isPersonalEffect);
            $scope.setFalse($scope.pricingPortPair.isCoload);
            $scope.setFalse($scope.pricingPortPair.isTransit);
            $scope.setFalse($scope.pricingPortPair.isOurTransport);
            $scope.selectedItemIdx = index;
            var myOtherModal = $modal({
                scope: $scope,
                templateUrl: '/app/components/crm/pricing_air/popup/pricing_air_more_info_popup.html',
                backdrop: 'static',
                show: false
            });
            myOtherModal.$promise.then(myOtherModal.show);
        }


        $scope.setFalse = function(value) {
            if (value == undefined || value == null) {
                value = false;
            }
        }


        $scope.divsionServiceCall = function(object) {
            return divsionService.ajaxDivisionEvent(object);
        }

        $scope.commodityServiceCall = function(object) {
            return CommodityService.getCommodityList(object);
        }

        $scope.partyServiceCall = function(object) {
            return partyService.getPartyList(object);
        }

        $scope.carrierServiceCall = function(object) {
            return carrierMasterService.getCarrierList(object, 'Air');
        }

        $scope.ajaxTosEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.tosList = data.responseObject.searchResult;
                    return $scope.tosList;
                }
            }, function(errResponse) {
                console.error('Error while fetching Tos');
            });
        }

        $scope.copyOfOrginDest = function(copyObj, listObj) {

            var tmpObj = angular.copy(copyObj);
            tmpObj.id = null;
            tmpObj.systemTrack = null;
            tmpObj.versionLock = 0;
            if (listObj == undefined || listObj == null || listObj.length == 0) {
                listObj = [];
            } else {
                var tmpIndex = listObj.length - 1;
                if ($scope.isEmptyRow(listObj)) {
                    listObj.splice(tmpIndex, 1);

                    if (listObj == undefined || listObj == null || listObj.length == 0) {
                        listObj = [];
                    }
                }
            }
            listObj.push(tmpObj);
        }

        $scope.copyOfFreight = function(copyObj, listObj) {

            var tmpObj = angular.copy(copyObj);
            tmpObj.id = null;
            tmpObj.systemTrack = null;
            tmpObj.versionLock = 0;

            if (tmpObj.minimumSellPrice != undefined && tmpObj.minimumSellPrice != null) {
                tmpObj.minimumSellPrice.id = null;
                tmpObj.minimumSellPrice.systemTrack = null;
                tmpObj.minimumSellPrice.versionLock = 0;
            }

            if (tmpObj.standardSellPrice != undefined && tmpObj.standardSellPrice != null) {
                tmpObj.standardSellPrice.id = null;
                tmpObj.standardSellPrice.systemTrack = null;
                tmpObj.standardSellPrice.versionLock = 0;
            }

            if (tmpObj.cost != undefined && tmpObj.cost != null) {
                tmpObj.cost.id = null;
                tmpObj.cost.systemTrack = null;
                tmpObj.cost.versionLock = 0;
            }

            if (listObj == undefined || listObj == null || listObj.length == 0) {
                listObj = [];
            } else {
                var tmpIndex = listObj.length - 1;
                if ($scope.isEmptyRow(listObj)) {
                    listObj.splice(tmpIndex, 1);

                    if (listObj == undefined || listObj == null || listObj.length == 0) {
                        listObj = [];
                    }
                }
            }
            listObj.push(tmpObj);
        }

        $scope.$on('addPricingAirEvent', function(events, args) {

            $rootScope.unfinishedData = $scope.pricingMaster;
            $rootScope.category = "CRM";
            $rootScope.unfinishedFormTitle = "Pricing Air (New)";
            if ($scope.pricingMaster != undefined &&
                $scope.pricingMaster != null &&
                $scope.pricingMaster.origin != undefined &&
                $scope.pricingMaster.origin != null &&
                $scope.pricingMaster.origin.portName != undefined &&
                $scope.pricingMaster.origin.portName != null &&
                $scope.pricingMaster.origin.portName != "") {
                $rootScope.subTitle = $scope.pricingMaster.origin.portName;
            } else {
                $rootScope.subTitle = "Unknown Pricing Air"
            }




        })

        $scope.$on('editPricingAirEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.pricingMaster;
            $rootScope.category = "CRM";
            $rootScope.unfinishedFormTitle = "Pricing Air Edit";
            $rootScope.subTitle = "Pricing Air";
        })


        $scope.$on('addPricingAirEventReload', function(e, confirmation) {
            confirmation.message = "";
            sessionStorage.reloadFormData = JSON.stringify($scope.pricingMaster);
            sessionStorage.isPricingMasterReloaded = "YES";
            e.preventDefault();
        })
        $scope.$on('editPricingAirEventReload', function(e, confirmation) {
            console.log('editPricingAirEventReload');
            confirmation.message = "";
            sessionStorage.reloadFormData = JSON.stringify($scope.pricingMaster);
            sessionStorage.isPricingMasterReloaded = "YES";
            e.preventDefault();

        })

        $scope.isReloaded = sessionStorage.isPricingMasterReloaded;
        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    sessionStorage.isPricingMasterReloaded = "NO";

                    $scope.pricingMaster = JSON.parse(sessionStorage.reloadFormData);
                } else {
                    console.log("Add History NotReload...")
                    $scope.pricingMaster = $rootScope.selectedUnfilledFormData;
                }
                $scope.init();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    sessionStorage.isPricingMasterReloaded = "NO";
                    $scope.pricingMaster = JSON.parse(sessionStorage.reloadFormData);
                    console.log("Add NotHistory Reload...")
                } else {
                    $scope.pricingMaster = {};
                    $scope.init();
                    console.log("Add NotHistory NotReload...")

                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "CRM",
                    state: "layout.pricingAir"
                },
                {
                    label: "Pricing Air",
                    state: "layout.pricingAir"
                },
                {
                    label: "Add Pricing Air",
                    state: null
                }
            ];
        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    sessionStorage.isPricingMasterReloaded = "NO";
                    $scope.pricingMaster = JSON.parse(sessionStorage.reloadFormData);
                } else {
                    $scope.pricingMaster = $rootScope.selectedUnfilledFormData;
                }
                $scope.init();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    sessionStorage.isPricingMasterReloaded = "NO";
                    $scope.pricingMaster = JSON.parse(sessionStorage.reloadFormData);
                    $scope.init();
                } else {
                    $scope.getPricingAirById($stateParams.id);
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "CRM",
                    state: "layout.pricingAir"
                },
                {
                    label: "Pricing Air",
                    state: "layout.pricingAir"
                },
                {
                    label: "Edit Pricing Air",
                    state: null
                }
            ];

        }

    }
]);
