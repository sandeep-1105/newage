app.controller('pricingCtrl',['$rootScope', '$stateParams', '$scope', '$location', '$window',
        '$timeout', 'ngDialog', '$state', '$modal', 'PricingAirView',
        'PricingAirSearch', 'RecentHistorySaveService',
        'downloadFactory', 'PricingAirBulkFileUpload', 'uiShipmentDataService', 'PackGetByCode',
        'CarrierByTransportMode', 'PrepareShipment', 'roleConstant',
    function($rootScope, $stateParams, $scope, $location, $window,
        $timeout, ngDialog, $state, $modal, PricingAirView,
        PricingAirSearch, RecentHistorySaveService,
        downloadFactory, PricingAirBulkFileUpload, uiShipmentDataService, PackGetByCode,
        CarrierByTransportMode, PrepareShipment, roleConstant) {

        $scope.$roleConstant = roleConstant;

        /***********************************************************
         * ux - by Muthu reason - ipad compatibility fix
         * 
         **********************************************************/
        $scope.deskTopView = true;
        $scope.searchDto = {};

        /**
         * ******************* ux fix ends here
         * ****************************
         */
        $scope.pricingAirHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false
            },

            {
                "name": "Origin",
                "width": "col-xs-3",
                "model": "originName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchPortOrigin"

            }, {
                "name": "Destination",
                "width": "col-xs-3",
                "model": "destinationName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchPortDestination"
            }, {
                "name": "Transit Port",
                "width": "col-xs-3",
                "model": "transitName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchTransitPort"
            }, {
                "name": "Routed By",
                "model": "whoRouted",
                "search": true,
                "wrap_cell": true,
                "type": "drop",
                "data": $rootScope.enum['WhoRouted'],
                "sort": true,
                "key": "searchWhoRouted"
            }
        ];

        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;

        $scope.init = function() {
            console.log("Pricing Air Init method called.");
            $scope.searchDto = {};
            $scope.search();
        };

        $scope.searchDto.orderByType = "ASC";

        $scope.downloadAttach = function(param) {

            if (param.data.id != null && param.data.file == null) {

                console.log("API CALL")

                downloadFactory.downloadAttachment(
                    '/api/v1/pricing/files/', param.data.id,
                    param.data.fileName);

            }
        }

        $scope.attachConfig = {
            "isEdit": false, // false for view only mode
            "isDelete": true, //
            "page": "",
            "columnDefs": [

                {
                    "name": "Reference No",
                    "model": "refNo",
                    "type": "text"
                }, {
                    "name": "File Name",
                    "model": "fileName",
                    "type": "file"
                }
            ]

        };

        $scope.rowSelect = function(data, index) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_VIEW)) {
                $rootScope.clientMessage = null;
                $scope.detailPageFlag = true;
                $scope.selectedRowIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) +
                    index;
                // $scope.selectedRecordIndex = index;
                var param = {
                    id: data.id,
                    index: index,
                    selectedPageNumber: $scope.selectedRowIndex,
                    totalRecord: $scope.totalRecord
                };

                /*******************************************************
                 * ux - by Muthu reason - ipad compatibility fix
                 * 
                 ******************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;
                } else {
                    $scope.deskTopView = true;
                }

                /**
                 * ******************* ux fix ends here
                 * ****************************
                 */

                $state.go("layout.viewPricingAir", $scope
                    .searchDtoToStateParams(param));
            }
        };

        $scope.searchDtoToStateParams = function(param) {
            if (param == undefined || param == null) {
                param = {};
            }
            if ($scope.searchDto != undefined &&
                $scope.searchDto != null) {
                if ($scope.searchDto != undefined &&
                    $scope.searchDto != null) {
                    param.recordPerPage = 1;
                    if ($scope.searchDto.sortByColumn != undefined &&
                        $scope.searchDto.sortByColumn != null) {
                        param.sortByColumn = $scope.searchDto.sortByColumn;
                    }
                    if ($scope.searchDto.orderByType != undefined &&
                        $scope.searchDto.orderByType != null) {
                        param.orderByType = $scope.searchDto.orderByType;
                    }
                    if ($scope.searchDto.searchPortOrigin != undefined &&
                        $scope.searchDto.searchPortOrigin != null) {
                        param.searchPortOrigin = $scope.searchDto.searchPortOrigin;
                    }
                    if ($scope.searchDto.searchPortDestination != undefined &&
                        $scope.searchDto.searchPortDestination != null) {
                        param.searchPortDestination = $scope.searchDto.searchPortDestination;
                    }
                    if ($scope.searchDto.searchTransitPort != undefined &&
                        $scope.searchDto.searchTransitPort != null) {
                        param.searchTransitPort = $scope.searchDto.searchTransitPort;
                    }

                }

            }
            return param;
        }

        $scope.singlePageNavigation = function(val) {
            if ($stateParams != undefined && $stateParams != null) {

                if ((parseInt($stateParams.selectedPageNumber) + val) < 0 ||
                    (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                    return;

                var stateParameters = {};
                $scope.searchDto.recordPerPage = 1;
                $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) +
                    val;

                if ($stateParams.sortByColumn != undefined &&
                    $stateParams.sortByColumn != null) {
                    stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
                }
                if ($stateParams.orderByType != undefined &&
                    $stateParams.orderByType != null) {
                    stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
                }
                if ($stateParams.recordPerPage != undefined &&
                    $stateParams.recordPerPage != null) {
                    stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
                }
                if ($stateParams.searchPortOrigin != undefined &&
                    $stateParams.searchPortOrigin != null) {
                    stateParameters.searchPortOrigin = $scope.searchDto.searchPortOrigin = $stateParams.searchPortOrigin;
                }
                if ($stateParams.searchPortDestination != undefined &&
                    $stateParams.searchPortDestination != null) {
                    stateParameters.searchPortDestination = $scope.searchDto.searchPortDestination = $stateParams.searchPortDestination;
                }
                if ($stateParams.searchTransitPort != undefined &&
                    $stateParams.searchTransitPort != null) {
                    stateParameters.searchTransitPort = $scope.searchDto.searchTransitPort = $stateParams.searchTransitPort;
                }

            } else {
                return;
            }
            PricingAirSearch.query($scope.searchDto).$promise
                .then(function(data, status) {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
                    stateParameters.id = data.responseObject.searchResult[0].id;
                    stateParameters.totalRecord = $scope.totalRecord;
                    $state.go("layout.viewPricingAir",
                        stateParameters);
                });
        }

        $scope.openDefaultTab = function(tab) {
            if (tab == 'general' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_VIEW)) {
                $scope.Tabs = tab;
            }
            if (tab == 'Origin' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_ORIGIN_CHARGES_VIEW)) {
                $scope.pricingState = tab;
            }
        }

        $scope.clickOnTab = function(tab) {

            if (tab == 'general' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_VIEW)) {
                $scope.Tabs = tab;
            }
            if (tab == 'Origin' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_ORIGIN_CHARGES_VIEW)) {
                $scope.pricingState = tab;
            }
            if (tab == 'Freight' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_FREIGHT_CHARGES_VIEW)) {
                $scope.pricingState = tab;
            }
            if (tab == 'Destination' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_GENERAL_DESTINATION_CHARGES_VIEW)) {
                $scope.pricingState = tab;
            }
            if (tab == 'attachments' && $rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_ATTACHMENT_VIEW)) {
                $scope.Tabs = tab;
            }

        }



        $scope.editPricingAir = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_MODIFY)) {
                console
                    .log("Edit editPricingAir Button is Pressed....")
                var param = {
                    id: $scope.pricingMaster.id
                };
                console.log("State Parameters :: ", param);
                $state.go("layout.pricingAirEdit", param);
            }
        }

        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }

        $scope.sortSelection = {
            sortKey: "origin.portName",
            sortOrder: "asc"
        }

        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder
                .toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        };

        $scope.changepage = function(param) {
            console.log("called change page")
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }
        $scope.changeSearch = function(param) {

            console.log("Change Search is called ", param);

            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }

            $scope.page = 0;
            $scope.search();
        }

        $scope.search = function() {
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.pricingArr = [];
            PricingAirSearch.query($scope.searchDto).$promise
                .then(function(data, status) {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    var tempArr = [];
                    var resultArr = [];
                    tempArr = data.responseObject.searchResult;
                    console.log("tempArr", tempArr);
                    var tempObj = {};
                    angular.forEach(tempArr, function(item,
                        index) {
                        tempObj = item;
                        tempObj.no = (index + 1) +
                            ($scope.page * $scope.limit);
                        resultArr.push(tempObj);
                        tempObj = {};
                    });
                    $scope.pricingArr = resultArr;
                    console
                        .log('pricingArr',
                            $scope.pricingArr);
                });
        }

        $scope.cancel = function() {
            $scope.detailPageFlag = false;
            $scope.showLogs = false;
            $scope.showHistory = false;
            /*******************************************************
             * ux - by Muthu reason - ipad compatibility fix
             * 
             ******************************************************/
            $scope.deskTopView = true;

            /**
             * ******************* ux fix ends here
             * ****************************
             */
        };
        $scope.back = function() {
            $state.go("layout.pricingAir");
        };
        $scope.add = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_CREATE)) {
                $rootScope.clientMessage = null;
                $state.go("layout.pricingAirAdd");
            }
        };

        /*
         * $scope.pricing = { "status" : "Active" };
         */

        $scope.attachConfig = {
            "isEdit": false, // false for view only mode
            "isDelete": true, //
            "page": "",
            "columnDefs": [

                {
                    "name": "Reference No",
                    "model": "refNo",
                    "type": "text"
                }, {
                    "name": "File Name",
                    "model": "fileName",
                    "type": "file"
                }
            ]

        };

        $scope.attachData = [{
            "sno": "1",
            "refNo": "DOC1",
            "fileName": "Packaging List.png"
        }];

        $scope.moreinfoPopup = function(obj) {
            var newScope = $scope.$new();
            newScope.pricingCarrier = obj;
            newScope.directiveDatePickerOpts = angular
                .copy($rootScope.datePickerOptions);
            newScope.directiveDatePickerOpts.widgetParent = "body";
            var myOtherModal = $modal({
                scope: newScope,
                templateUrl: 'app/components/crm/pricing_air/views/moreinfo_popup.html',
                backdrop: 'static',
                show: false
            });
            myOtherModal.$promise.then(myOtherModal.show);
        };
        /*
         * $scope.noteViewPopup = function(){ var noteModal =
         * $modal({scope: $scope, templateUrl:
         * 'app/components/crm/pricing_air/views/notes_view_popup.html',
         * show: false}); noteModal.$promise.then(noteModal.show);
         *  }
         */

        $scope.view = function(id) {
            console.log("View Pricing Air is called ", id);
            $rootScope.mainpreloder = true;
            PricingAirView
                .get({
                        id: id
                    },
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            console
                                .log("Pricing Air view Successful");
                            $scope.pricingMaster = data.responseObject;
                            $rootScope.mainpreloder = false;
                            var rHistoryObj = {
                                'title': 'Pricing Air View #' +
                                    $stateParams.id,
                                'subTitle': $rootScope.subTitle,
                                'stateName': $state.current.name,
                                'stateParam': JSON
                                    .stringify($stateParams),
                                'stateCategory': 'PricingAirGroup',
                                'serviceType': 'AIR'
                            }
                            RecentHistorySaveService
                                .form(rHistoryObj);
                            console.log(
                                "Viewing Pricing Air ",
                                $scope.pricingMaster);
                        } else {
                            console
                                .log(
                                    "Carrier Pricing Air View Failed ",
                                    data.responseDescription)
                            $rootScope.mainpreloder = false;
                        }
                    },
                    function(error) {
                        console
                            .log(
                                "Pricing AirView Failed : ",
                                error);
                        $rootScope.mainpreloder = false;
                    });
        }

        $scope.download = function(object) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_DOWNLOAD)) {
                var id, fileName;
                if (object == null) {
                    id = -1;
                    fileName = 'air_pricing_template.csv';

                } else {
                    id = object.id;
                    fileName = object.origin.portName +
                        '_' +
                        object.destination.portName +
                        (object.transit == null ||
                            object.transit == undefined ? '' :
                            ('_' + object.transit.portName)) +
                        '.csv';
                }
                downloadFactory.downloadAttachment(
                    '/api/v1/pricing/download/template/', id,
                    fileName);
            }
        }

        // Bulk upload
        var myTotalOtherModal = null;
        $scope.pricingAirUploadmodel = function(id) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_UPLOAD_CREATE)) {
                console.log("BulkUpload Button is pressed....!");
                $scope.errorMap = new Map();
                var newScope = $scope.$new();
                $scope.uploadText = true;
                $scope.data = {};
                $scope.data.id = id;
                myTotalOtherModal = $modal({
                    scope: newScope,
                    templateUrl: '/app/components/crm/pricing_air/views/pricing_air_upload.html',
                    show: false,
                    backdrop: 'static'
                });

                myTotalOtherModal.$promise.then(myTotalOtherModal.show);
            }
        };

        $scope.uploadFile = function(value) {
            if ($scope.validateUploadFile($scope.data.file)) {
                $scope.spinner = true;
                $scope.data.fileName = $scope.data.file.name;
                var reader = new FileReader();

                reader.onload = function(event) {
                    var contents = event.target.result;
                    var uploadedFile = btoa(contents);
                    $scope.file = uploadedFile;
                };
                reader.readAsBinaryString($scope.data.file);

                $timeout(
                    function() {

                        $scope.fileUploadDto = {};

                        $scope.fileUploadDto.fileName = $scope.data.file.name;
                        $scope.fileUploadDto.fileType = $scope.data.file.type;
                        $scope.fileUploadDto.file = $scope.file;
                        $scope.fileUploadDto.pricingMasterId = $scope.data.id;
                        return PricingAirBulkFileUpload
                            .upload($scope.fileUploadDto).$promise
                            .then(
                                function(data, status) {
                                    if (data.responseCode == "ERR0") {
                                        $rootScope.successDesc = $rootScope.nls["ERR05319"];
                                        myTotalOtherModal.$promise
                                            .then(myTotalOtherModal.hide);
                                        $scope.spinner = false;
                                        if (value != -1) {
                                            $scope.oldData = JSON
                                                .stringify($stateParams);
                                            var tempSParam = JSON
                                                .parse($scope.oldData);
                                            if (tempSParam.refCount == undefined ||
                                                tempSParam.refCount == null) {
                                                tempSParam.refCount = 1;
                                            } else {
                                                tempSParam.refCount = tempSParam.refCount + 1;
                                            }
                                            $state
                                                .go(
                                                    "layout.viewPricingAir",
                                                    tempSParam);
                                        } else {
                                            $state
                                                .go("layout.pricingAir");
                                            $scope
                                                .init();
                                        }
                                    } else {
                                        myTotalOtherModal.$promise
                                            .then(myTotalOtherModal.hide);
                                        $scope.spinner = false;
                                    }
                                },
                                function(errResponse) {
                                    console
                                        .log(
                                            'Error while uploading  Pricing Air',
                                            errResponse);
                                    myTotalOtherModal.$promise
                                        .then(myTotalOtherModal.hide);
                                    $scope.spinner = false;
                                });

                    }, 5000);
            } else {
                console.log('invalid file format');
            }
        }

        $scope.enableUploadFn = function() {
            $timeout(function() {
                $scope.disableUploadBtn = false;
            }, 1000);
        }

        $scope.chooseFile = function() {
            $scope.disableUploadBtn = true;
            if ($scope.validateUploadFile($scope.data.file)) {
                $scope.uploadText = false;
                $scope.isBusy = true;
                $scope.data.fileName = $scope.data.file.name;
                $timeout(function() {
                    $scope.isBusy = false;
                    $scope.enableUploadFn();
                }, 5000);
            } else {
                $scope.enableUploadFn();
                console.log("Not valid file format",
                    $scope.data.file.type);
            }

        };

        $scope.validateUploadFile = function(file) {
            $scope.errorMap = new Map();
            if (file == null && file == undefined) {
                $scope.errorMap.put('fileUpload',
                    $rootScope.nls["ERR1297"]);
                return false;
            } else {
                if (file.type == "text/csv" ||
                    file.name.includes('.csv')) {

                    var allowedSize = $rootScope.appMasterData['file.size.carrier.rate.bulk.upload'];

                    if (allowedSize != undefined) {

                        allowedSize = allowedSize * 1024 * 1024;

                        if (file.size <= 0) {

                            $scope.errorMap.put("fileUpload",
                                $rootScope.nls["ERR1299"]);

                            return false;
                        } else if (file.size > allowedSize) {

                            $scope.errorMap.put("fileUpload",
                                $rootScope.nls["ERR1300"]);

                            return false;
                        }

                    }
                } else {
                    $scope.errorMap.put('fileUpload',
                        $rootScope.nls["ERR1298"]);
                    return false;
                }
            }

            return true;
        }

        $scope.createShipmentPopup = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/pricing_air/views/pricing_createShipment_popup.html',
            backdrop: 'static',
            show: false,
            keyboard: true
        });
        $scope.showPopup = function(pricingMaster) {
            if ($rootScope.roleAccess(roleConstant.CRM_PRICING_AIR_CREATE_SHIPMENT_CREATE)) {
                $scope.createShipmentPopup.$promise
                    .then($scope.createShipmentPopup.show);
            }
        };



        $scope.createShipment = function(carrier) {
            if (carrier == null || carrier == undefined || carrier.id == null || carrier.id == undefined) {
                return;
            }
            $scope.searchDto = {};
            $scope.searchDto.param1 = carrier.id;
            $scope.searchDto.param2 = $scope.pricingMaster.id;
            PrepareShipment.query($scope.searchDto).$promise
                .then(function(data, status) {
                    var shipment = data.responseObject;
                    shipment.party = null;
                    //							    shipment.whoRouted = shipment.whoRouted == 'Self' ? false : true;
                    shipment.partyAddress = {};
                    shipment.createdBy = $rootScope.userProfile.employee;
                    shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
                    shipment.whoRouted = false;
                    shipment.directShipment = false;
                    shipment.location = $rootScope.userProfile.selectedUserLocation;
                    shipment.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
                    shipment.country = $rootScope.userProfile.selectedUserLocation.countryMaster;

                    shipment.shipmentServiceList[0].attachConfig = {
                        "isEdit": true, //false for view only mode
                        "isDelete": true, //
                        "page": "shipment",
                        "columnDefs": [

                            {
                                "name": "Document Name",
                                "model": "documentMaster.documentName",
                                "type": "text"
                            },
                            {
                                "name": "Reference #",
                                "model": "refNo",
                                "type": "text"
                            },
                            {
                                "name": "Customs",
                                "model": "customs",
                                "type": "text"
                            },
                            {
                                "name": "Unprotected File Name",
                                "model": "unprotectedFileName",
                                "type": "file"
                            },
                            {
                                "name": "Protected File Name",
                                "model": "protectedFileName",
                                "type": "file"
                            },
                            {
                                "name": "Actions",
                                "model": "action",
                                "type": "action"
                            }
                        ],
                        "data": []
                    }
                    //shipment.shipmentServiceList[0].whoRouted = shipment.shipmentServiceList[0].whoRouted == 'Self' ? false : true;
                    shipment.shipmentServiceList[0].serviceReqDate = $rootScope.dateToString(new Date());
                    shipment.shipmentServiceList[0].carrier = carrier;

                    shipment.shipmentServiceList[0].packMaster = {}
                    var documentDetail = {};
                    documentDetail.dimensionList = [{}];
                    documentDetail.shipperAddress = {};
                    documentDetail.forwarderAddress = {};
                    documentDetail.consigneeAddress = {};
                    documentDetail.firstNotifyAddress = {};
                    documentDetail.secondNotifyAddress = {};
                    documentDetail.agentAddress = {};
                    documentDetail.issuingAgentAddress = {};
                    documentDetail.documentReqDate = $rootScope.sendApiDateAndTime(new Date());
                    if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                        documentDetail.dimensionUnit = true;
                        documentDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                    } else {
                        documentDetail.dimensionUnit = false;
                        documentDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                    }

                    if (shipment.isFromConsolForImport != undefined && (shipment.isFromConsolForImport == 'true' || shipment.isFromConsolForImport)) {
                        documentDetail.issuingAgent = null;
                        documentDetail.issuingAgentAddress = null;
                    } else if ($rootScope.userProfile.selectedUserLocation.tmpPartyMaster != undefined && $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != null &&
                        $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != undefined && $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != null) {
                        documentDetail.issuingAgent = $rootScope.userProfile.selectedUserLocation.tmpPartyMaster;
                        uiShipmentDataService.assignPrimaryAddress(documentDetail.issuingAgent, documentDetail.issuingAgentAddress);
                    }
                    shipment.shipmentServiceList[0].documentList = [];
                    shipment.shipmentServiceList[0].documentList.push(documentDetail);

                    var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
                    if (defaultPackageCode != undefined && defaultPackageCode != null) {
                        console.log("Default Package is assigned....");
                        PackGetByCode.get({
                            code: defaultPackageCode
                        }, function(data) {
                            if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                                console.log("Successful while getting pack.", data)
                                shipment.shipmentServiceList[0].documentList[0].packMaster = shipment.shipmentServiceList[0].packMaster = data.responseObject;
                            }
                        }, function(error) {
                            console.log("Error while getting pack.", error)
                        });
                    } else {
                        console.log("Default Package Code is not defined in Default Master Data....");
                    }

                    shipment.shipmentServiceList[0].pickUpDeliveryPoint = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.isOurPickUp = false;
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.transporter = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.pickupPoint = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.pickupFrom = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.deliveryPoint = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.deliveryFrom = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.doorDeliveryPoint = {};
                    shipment.shipmentServiceList[0].pickUpDeliveryPoint.doorDeliveryFrom = {};

                    shipment.shipmentServiceList[0].location = $rootScope.userProfile.selectedUserLocation;
                    shipment.shipmentServiceList[0].company = $rootScope.userProfile.selectedUserLocation.companyMaster;
                    shipment.shipmentServiceList[0].country = $rootScope.userProfile.selectedUserLocation.countryMaster;
                    shipment.shipmentServiceList[0].eventList = [{
                        isCompleted: 'Yes',
                        followUpRequired: 'No'
                    }];
                    shipment.shipmentServiceList[0].shipmentServiceTriggerList = [];
                    shipment.shipmentServiceList[0].connectionList = [{
                        connectionStatus: 'Planned'
                    }];
                    shipment.shipmentServiceList[0].shipmentAttachmentList = [];
                    shipment.shipmentServiceList[0].referenceList = [];
                    shipment.shipmentServiceList[0].authenticatedDocList = [];


                    shipment.shipmentServiceList[0].whoRouted = false;
                    shipment.shipmentServiceList[0].directShipment = false;
                    shipment.shipmentServiceList[0].ppcc = false;
                    shipment.shipmentServiceList[0].holdShipment = false;
                    shipment.shipmentServiceList[0].hazardous = false;
                    shipment.shipmentServiceList[0].overDimension = false;
                    shipment.shipmentServiceList[0].isMinimumShipment = false;
                    shipment.shipmentServiceList[0].isAgreed = false;
                    shipment.shipmentServiceList[0].isMarksNo = true;

                    shipment.shipmentServiceList[0].customerService = $rootScope.userProfile.employee;
                    shipment.shipmentServiceList[0].localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;

                    $rootScope.createShipment = shipment;

                    $scope.createShipmentPopup.$promise
                        .then($scope.createShipmentPopup.hide);
                    /*var params = {};
                    params.fromState="fromPricingair";
                    $state.go('layout.createCrmShipment',params);*/

                    //								$rootScope.createShipment = $scope.shipment;
                    var params = {};
                    params.action = 'CREATE';
                    params.fromScreen = $state.current.name;
                    $state.go('layout.addNewShipment', params)
                });
        }

        $scope.ajaxCarrierDynamicEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CarrierByTransportMode.fetch({
                    "transportMode": 'Air'
                }, $scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $rootScope.carrierMasterList = data.responseObject.searchResult;
                            return $rootScope.carrierMasterList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Carrier List Based on Air');
                    });

        }

        switch ($stateParams.action) {
            case "VIEW":
                console.log("PricingAir View Page", $stateParams.id);
                $rootScope.unfinishedFormTitle = "Pricing Air # " +
                    $stateParams.id;
                $rootScope.unfinishedData = undefined;

                if ($stateParams.id != undefined &&
                    $stateParams.id != null) {
                    $scope.view($stateParams.id);
                }
                if ($stateParams.selectedPageNumber != undefined &&
                    $stateParams.selectedPageNumber != null) {
                    $scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
                }
                if ($stateParams.totalRecord != undefined &&
                    $stateParams.totalRecord != null) {
                    $scope.totalRecord = parseInt($stateParams.totalRecord);
                }
                $rootScope.breadcrumbArr = [{
                    label: "CRM",
                    state: "layout.pricingAir"
                }, {
                    label: "Pricing Air",
                    state: "layout.pricingAir"
                }, {
                    label: "View Pricing Air",
                    state: null
                }];
                break;
            case "SEARCH":
                console.log("PricingAir Search Page");
                $scope.init();
                $rootScope.breadcrumbArr = [{
                    label: "CRM",
                    state: "layout.pricingAir"
                }, {
                    label: "Pricing Air",
                    state: "layout.pricingAir"
                }];
                break;
            default:
        }

    }]);