(function() {

		app.factory("PricingAirSearch",['$resource', function($resource) {
			return $resource("/api/v1/pricing/search", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("PricingAirView",['$resource', function($resource) {
			return $resource("/api/v1/pricing/get/id/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		app.factory("PricingPortPairView",['$resource', function($resource) {
			return $resource("/api/v1/pricing/pricingport/get/id/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		app.factory("PricingAirSave",['$resource', function($resource) {
			return $resource("/api/v1/pricing/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		
		
		
		app.factory("PricingAirSaveList",['$resource', function($resource) {
			return $resource("/api/v1/pricing/createlist", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		
		
		
		app.factory("PricingAirEdit",['$resource', function($resource) {
			return $resource("/api/v1/pricing/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
		
		app.factory("PricingPortUpdate",['$resource', function($resource) {
			return $resource("/api/v1/pricing/pricingportpair/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
	
		app.factory("PricingAirBulkFileUpload",['$resource', function($resource) {
			return $resource("/api/v1/pricing/upload", {}, {
				upload : {
					method : 'POST'
				}
			});
		}]);
		
		
		app.factory("PrepareShipment",['$resource', function($resource) {
			return $resource("/api/v1/pricing/prepareShipment", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		
		
})();
