/**
 *  Default charge Factory which is used for Calling API 
 */

app.factory('SearchCharge',['$resource', function($resource){
	
	return $resource("/api/v1/pricing/defaultchargesearch",{},{
		query : {
			method : 'POST'
		}	
	});
	
}]);


app.factory('DefaultChargeAdd',['$resource', function($resource){
	
	return $resource("/api/v1/defaultcharge/create",{},{
		save:{
				method : 'POST'
		}
	});
}]);


app.factory("DefaultChargeRemove",['$resource', function($resource) {
	return $resource("/api/v1/defaultcharge/delete/:id", {}, {
		remove : {
			method : 'DELETE',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}]);

app.factory("DCFileUpload",['$resource', function($resource) {
	return $resource("/api/v1/defaultcharge/upload", {}, {
		upload : {
			method : 'POST'
		}
	});
}]);


app.factory("DefaultChargeView",['$resource', function($resource) {
	return $resource("/api/v1/defaultcharge/get/id/:id", {}, {
		get : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}]);
