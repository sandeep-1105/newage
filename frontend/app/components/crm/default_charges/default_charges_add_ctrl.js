app.controller('defaultChargeOperational',['$rootScope', '$scope', '$http', '$timeout', '$location', '$modal', '$stateParams', '$state', 
		'ServiceList','ServiceByTransportMode',
    'ngDialog', 'ChargeSearchKeyword', 'UnitSearchKeyword', 'PartiesList', 'CommodityList', 
    'CarrierByTransportMode', 'PortByTransportMode', 'CurrencyMasterSearchKeyword', 'DCFileUpload',
    'DivisionListByCompany', 'TosSearchKeyword', 'DefaultChargeAdd', 'DefaultChargeView', 
    'UnitMasterGetByCode', 'defaultService', 'PricingPortUpdate', 'Notification', 'PricingAirSaveList', 'compareService',
	function($rootScope, $scope, $http, $timeout, $location, $modal, $stateParams, $state, 
		ServiceList,ServiceByTransportMode,
    ngDialog, ChargeSearchKeyword, UnitSearchKeyword, PartiesList, CommodityList, 
    CarrierByTransportMode, PortByTransportMode, CurrencyMasterSearchKeyword, DCFileUpload,
    DivisionListByCompany, TosSearchKeyword, DefaultChargeAdd, DefaultChargeView, 
    UnitMasterGetByCode, defaultService, PricingPortUpdate, Notification, PricingAirSaveList, compareService) {

    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";

    $scope.isEmpty = false;
    $scope.popupspinner = false;
    $scope.pricingMasterList = [];
    $scope.disableUploadBtn = false;
    $scope.enableUploadFn = function() {
        $timeout(function() {
            $scope.disableUploadBtn = false;
        }, 4000);
    }



    $scope.bulkuploadmodel = function(bulkItem) {
        $scope.uploadText = true;
        $scope.data = {};
        $scope.popupspinner = false;
        $scope.errorMap = new Map();
        var newScope = $scope.$new();
        newScope.bulkItem = bulkItem;
        $scope.myTotalDCModal = $modal({
            scope: newScope,
            templateUrl: '/app/components/crm/default_charges/popup/bulkupload.html',
            backdrop: 'static',
            show: false
        });
        $scope.myTotalDCModal.$promise.then($scope.myTotalDCModal.show);
    };

    $scope.chooseFile = function() {
        console.log('choose File.....', $scope.data.file);
        $scope.disableUploadBtn = true;
        if ($scope.validateUploadFile($scope.data.file)) {
            $scope.uploadText = false;
            $scope.isBusy = true;
            $scope.data.fileName = $scope.data.file.name;
            $timeout(function() {
                $scope.isBusy = false;
                $scope.enableUploadFn();
            }, 4000);
            console.log("selected file is: ", $scope.data.file.name);
        } else {
            $scope.enableUploadFn();
            console.log("Not valid file format", $scope.data.file.type);
        }

    };

    $scope.validateUploadFile = function(file) {
        console.log("validateUploadFile is running");
        $scope.errorMap = new Map();
        if (file == null && file == undefined) {
            $scope.errorMap.put('fileUpload', $rootScope.nls["ERR1297"]);
            return false;
        } else {
            if (file.type == "text/csv" || file.name.includes('.csv')) {
                console.log('selected file type is :', file.type);

                var allowedSize = $rootScope.appMasterData['file.size.carrier.rate.bulk.upload'];

                if (allowedSize != undefined) {

                    console.log("Allowed File Size : " + allowedSize + " MB");

                    console.log("Uploaded File Size : ", file.size);

                    allowedSize = allowedSize * 1024 * 1024;

                    if (file.size <= 0) {

                        console.log("Empty file uploaded...")

                        $scope.errorMap.put("fileUpload", $rootScope.nls["ERR1299"]);

                        return false;
                    } else if (file.size > allowedSize) {

                        console.log("Selected File is Larger than the allowed size")

                        $scope.errorMap.put("fileUpload", $rootScope.nls["ERR1300"]);

                        return false;
                    }

                }
            } else {
                $scope.errorMap.put('fileUpload', $rootScope.nls["ERR1298"]);
                return false;
            }
        }

        return true;
    }


    $scope.uploadFile = function() {
        if ($scope.validateUploadFile($scope.data.file)) {

            $scope.popupspinner = true;
            $scope.data.fileName = $scope.data.file.name;

            var reader = new FileReader();

            reader.onload = function(event) {
                var contents = event.target.result;
                var uploadedFile = btoa(contents);
                $scope.file = uploadedFile;
            };
            reader.readAsBinaryString($scope.data.file);

            $timeout(function() {

                $scope.fileUploadDto = {};

                $scope.fileUploadDto.fileName = $scope.data.file.name;
                $scope.fileUploadDto.fileType = $scope.data.file.type;
                $scope.fileUploadDto.file = $scope.file;

                return DCFileUpload.upload($scope.fileUploadDto).$promise.then(function(data, status) {
                        if (data.responseCode == "ERR0") {
                            if ($scope.pricingMasterList == undefined || $scope.pricingMasterList == null || $scope.pricingMasterList.length == 0) {
                                $scope.pricingMasterList = [];
                            }
                            for (var i = 0; i < $scope.pricingMasterList.length; i++) {
                                if ($scope.checkMasterEmpty($scope.pricingMasterList[i])) {
                                    $scope.pricingMasterList.splice(i, 1);
                                }
                            }
                            $scope.pricingMasterList = $scope.pricingMasterList.concat(angular.copy(data.responseObject));
                            $scope.setDefaultAddValues($scope.pricingMasterList);
                            if ($scope.pricingMasterList.length == 0) {
                                $scope.pricingMasterList.push({});
                            } else {
                                /*for(var i=0;i<$scope.pricingPortPairList.length;i++){
                                	$scope.pricingPortPairList[i].validFrom =$rootScope.convertToDate(moment($scope.pricingPortPairList[i].validFrom).format("DD-MM-YYYY"));
                                	$scope.pricingPortPairList[i].validTo= $rootScope.convertToDate(moment($scope.pricingPortPairList[i].validTo).format("DD-MM-YYYY"));
                                }*/

                            }
                            $scope.myTotalDCModal.$promise.then($scope.myTotalDCModal.hide);
                            $scope.popupspinner = false;
                        } else {
                            console.log('Error while uploading  DC ', data.responseObject);
                            $scope.pricingPortPairList = [];
                            $scope.pricingPortPairList.push({});
                            $scope.myTotalDCModal.$promise.then($scope.myTotalDCModal.hide);
                            $scope.popupspinner = false;
                        }
                    },
                    function(errResponse) {
                        console.log('Error while uploading DC', errResponse);
                        $scope.pricingPortPairList = [];
                        $scope.pricingPortPairList.push({});
                        $scope.myTotalDCModal.$promise.then($scope.myTotalDCModal.hide);
                        $scope.popupspinner = false;
                    });

            }, 5000);
        } else {
            console.log('invalid file format');
            $scope.popupspinner = false;
        }
    }




    $scope.default_editmodel = function(index) {

        $scope.selectedItemIdx = index;
        if ($scope.pricingPortPairList != undefined && $scope.pricingPortPairList != null && $scope.pricingPortPairList[index] != undefined) {

            if ($scope.pricingPortPairList[index].validFrom != undefined) {
                if (typeof $scope.pricingPortPairList[index].validFrom == "string") {
                    $scope.pricingPortPairList[index].validFrom = $rootScope.convertToDate($scope.pricingPortPairList[index].validFrom);
                }
            }
            if ($scope.pricingPortPairList[index].validTo != undefined) {
                if (typeof $scope.pricingPortPairList[index].validTo == "string") {
                    $scope.pricingPortPairList[index].validTo = $rootScope.convertToDate($scope.pricingPortPairList[index].validTo);
                }
            }
        }
        var newScope = $scope.$new(false, $scope);
        var myTotalOtherModal = $modal({
            scope: newScope,
            backdrop: 'static',
            templateUrl: '/app/components/crm/default_charges/popup/default_popup.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);
    };


    $scope.cancelDc = function(op) {

        if (op == 'save') {
            //$scope.isDirty = ($scope.defaultChargeAddForm!=undefined?$scope.defaultChargeAddForm.$dirty:true);

            if ($scope.oldData != JSON.stringify($scope.pricingMasterList)) {
                $scope.isDirty = true;
            } else {
                $scope.isDirty = false;
            }
        } else {
            if ($scope.oldData != JSON.stringify($scope.pricingPortPair)) {
                $scope.isDirty = true;
            } else {
                $scope.isDirty = false;
            }
        }
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        if (value == 1 && op == 'save') {
                            $scope.savePricingPort();
                        } else if (value == 1 && op == 'update') {
                            $scope.updatePricingPort();
                        } else if (value == 2) {
                            var params = {};
                            params.submitAction = 'Cancelled';
                            $state.go("layout.defaultCharges", params);
                        } else {
                            console.log("cancelled");
                        }
                    });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.defaultCharges", params);
        }
    };

    $scope.addDCRow = function() {

        if ($scope.pricingMasterList == undefined || $scope.pricingMasterList == null || $scope.pricingMasterList.length == 0) {
            $scope.pricingMasterList = [];
            $scope.pricingMasterList.push({});
            $scope.pricingMasterList[0].pricingPortPairList = [];
            $scope.pricingMasterList[0].pricingPortPairList.push({});
            $scope.setDefaultAddValues($scope.pricingMasterList);
            return;
        }
        if ($scope.validatePricingMasterList('ADD')) {
            $scope.pricingMasterList.push({});
            $scope.pricingMasterList[$scope.pricingMasterList.length - 1].pricingPortPairList = [];
            $scope.pricingMasterList[$scope.pricingMasterList.length - 1].pricingPortPairList.push({});
            $scope.setDefaultAddValues($scope.pricingMasterList);
            $rootScope.navigateToNextField(($scope.pricingMasterList.length - 1) + 'dcService');
        } else {

            console.log("Valid")
        }

    }

    //moreinfo button
    var moreinModal;
    $scope.moreinfomodel = function(pindex, index) {

        $scope.parentIndex = pindex;
        $scope.pricingIndex = index;
        moreinModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/default_charges/popup/defaultcharge_moreinfo_edit_popup.html',
            show: false
        });
        moreinModal.$promise.then(moreinModal.show)
    };

    $scope.saveMoreInfo = function() {
        moreinModal.$promise.then(moreinModal.hide)
    }

    $scope.defaultObjP = {};
    $scope.defaultObj = {};
    $scope.defaultObjP.whoRouted = 'Self';
    $scope.defaultObj.hazardous = false;
    $scope.defaultObj.isClearance = false;
    $scope.defaultObj.isCoload = false;
    $scope.defaultObj.isForwarderOrDirect = false;
    $scope.defaultObj.isOurTransport = false;
    $scope.defaultObj.isPersonalEffect = false;
    $scope.defaultObj.isTransit = false;
    $scope.defaultObj.validFromDate = $rootScope.dateToString(new Date());
    $scope.defaultObj.validToDate = $rootScope.dateToString(moment().add('days', $rootScope.appMasterData['defaultcharge.valid.days']));


    $scope.setDefaultAddValues = function(obj) {
        if (obj == undefined || obj == null || obj.length == undefined) {
            obj = [];
            obj.push({});
            obj.pricingPortPairList = [];
            obj.pricingPortPairList.push({})
        }

        for (var k = 0; k < obj.length; k++) {


            if (obj[k] != undefined && obj[k].whoRouted == undefined || obj[k].whoRouted == null || obj[k].whoRouted == "")
                obj[k].whoRouted = 'Self';

            for (var i = 0; i < obj[k].pricingPortPairList.length; i++) {


                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].validFromDate == undefined || obj[k].pricingPortPairList[i].validFromDate == null || obj[k].pricingPortPairList[i].validFromDate == "")
                    obj[k].pricingPortPairList[i].validFromDate = $rootScope.dateToString(new Date());


                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].validToDate == undefined || obj[k].pricingPortPairList[i].validToDate == null || obj[k].pricingPortPairList[i].validToDate == "")
                    obj[k].pricingPortPairList[i].validToDate = $rootScope.dateToString(moment().add('days', $rootScope.appMasterData['defaultcharge.valid.days']));

                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].hazardous == undefined || obj[k].pricingPortPairList[i].hazardous == null || obj[k].pricingPortPairList[i].hazardous == "")
                    obj[k].pricingPortPairList[i].hazardous = false;

                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].isClearance == undefined || obj[k].pricingPortPairList[i].isClearance == null || obj[k].pricingPortPairList[i].isClearance == "")
                    obj[k].pricingPortPairList[i].isClearance = false;

                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].isForwarderOrDirect == undefined || obj[k].pricingPortPairList[i].isForwarderOrDirect == null || obj[k].pricingPortPairList[i].isForwarderOrDirect == "")
                    obj[k].pricingPortPairList[i].isForwarderOrDirect = false;

                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].isPersonalEffect == undefined || obj[k].pricingPortPairList[i].isPersonalEffect == null || obj[k].pricingPortPairList[i].isPersonalEffect == "")
                    obj[k].pricingPortPairList[i].isPersonalEffect = false;


                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].isCoload == undefined || obj[k].pricingPortPairList[i].isCoload == null || obj[k].pricingPortPairList[i].isCoload == "")
                    obj[k].pricingPortPairList[i].isCoload = false;

                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].isTransit == undefined || obj[k].pricingPortPairList[i].isTransit == null || obj[k].pricingPortPairList[i].isTransit == "")
                    obj[k].pricingPortPairList[i].isTransit = false;

                if (obj[k].pricingPortPairList[i] != undefined && obj[k].pricingPortPairList[i].isOurTransport == undefined || obj[k].pricingPortPairList[i].isOurTransport == null || obj[k].pricingPortPairList[i].isOurTransport == "")
                    obj[k].pricingPortPairList[i].isOurTransport = false;


            }
        }
    }

    $scope.setDefaultValues = function(obj, index) {
        $scope.selectedAjaxAdd('dcCurrency', index);
        $scope.setUnitByCharge(obj);
    }


    $scope.setUnitByCharge = function(obj) {


        if (obj.chargeMaster != undefined && obj.chargeMaster != null && obj.chargeMaster != "") {

            if (obj.chargeMaster.calculationType != undefined && obj.chargeMaster.calculationType != null) {
                if (obj.chargeMaster.calculationType == 'Percentage') {
                    UnitMasterGetByCode.get({
                        unitCode: 'PER'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            obj.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });

                }
                if (obj.chargeMaster.calculationType == 'Shipment') {
                    UnitMasterGetByCode.get({
                        unitCode: 'SHPT'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            obj.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });

                }
                if (obj.chargeMaster.calculationType == 'Unit') {
                    UnitMasterGetByCode.get({
                        unitCode: 'KG'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            obj.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });

                }
                if (obj.chargeMaster.calculationType == 'Document') {
                    UnitMasterGetByCode.get({
                        unitCode: 'DOC'
                    }).$promise.then(function(resObj) {
                        if (resObj.responseCode == "ERR0") {
                            obj.unitMaster = resObj.responseObject;
                        }
                    }, function(error) {
                        console.log("unit Get Failed : " + error)
                    });
                }
            }

        } else {
            obj.currencyMaster = "";
            obj.unitMaster = "";
        }
        return obj;

    }



    $scope.selectedAjaxEdit = function(key, validateCode, index) {
        if (true) { //if($scope.validateDC(validateCode)){
            if (key != undefined && key != null)
                var indexKey = key;
            $rootScope.navigateToNextField(indexKey);
        }
    }


    $scope.selectedAjaxAdd = function(key, validateCode, index) {
        if (key != undefined && key != null && index != undefined) {
            var indexKey = index + key;
            $rootScope.navigateToNextField(indexKey);
        }
    }


    $scope.copyPP = function(index) {

        $scope.pricingMasterList[index + 1] = angular.copy($scope.pricingMasterList[index]);
        $rootScope.navigateToNextField(index + 1 + 'dcService');
    }

    $scope.deletePP = function(index) {
        $scope.pricingMasterList.splice(index, 1);
        $rootScope.navigateToNextField(index - 1 + 'dcService');
    }


    $scope.serviceRender = function(item) {
        return {
            label: item.serviceName,
            item: item
        }
    };

    $scope.chargeRender = function(item) {
        return {
            label: item.chargeName,
            item: item
        }
    };

    $scope.currencyRender = function(item) {
        return {
            label: item.currencyCode,
            item: item
        }
    };

    $scope.unitRender = function(item) {
        return {
            label: item.unitName,
            item: item
        }
    };


    $scope.PortRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    };


    $scope.tosRender = function(item) {
        return {
            label: item.tosName,
            item: item
        }

    }

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }
    }

    $scope.divisionRender = function(item) {
        return {
            label: item.divisionName,
            item: item
        }

    }

    $scope.carrierRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }
    };

    $scope.commodityRender = function(item) {
        return {
            label: item.hsName,
            item: item
        }
    }

    $scope.ajaxServiceEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ServiceByTransportMode.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceMasterList = data.responseObject.searchResult;
                    return $scope.serviceMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ServiceMaster');
            }
        );
    }

    $scope.ajaxChargeEvent = function(object) {
        console.log("ajaxChargeEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.chargeMasterList = data.responseObject.searchResult;

                    if ($scope.chargeMasterList != null && $scope.chargeMasterList.length > 0) {

                        angular.forEach($scope.chargeMasterList, function(chargeObj, index) {

                            if (chargeObj.chargeType == 'Freight' || chargeObj.chargeType == 'Other') {

                                $scope.chargeMasterList.splice(index, 1);
                            }

                        });
                    }
                    return $scope.chargeMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Charge');
            }
        );
    }




    $scope.ajaxUnitEvent = function(object) {

        console.log("ajaxUnitEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.unitMasterList = data.responseObject.searchResult;
                    return $scope.unitMasterList;
                    // $scope.showUnitMasterList=true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Unit List');
            }
        );

    }

    $scope.ajaxCurrencyEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.currencyList = data.responseObject.searchResult;
                    return $scope.currencyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }

    $scope.ajaxPortEvent = function(object, type, obj, cobj) {
        $rootScope.clientMessage = null;
        if (cobj.serviceMaster != null && cobj.serviceMaster != undefined && cobj.serviceMaster != " " && cobj.serviceMaster.id != undefined) {
            console.log(" Ajax PricingAirOrigin Event method : ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": cobj.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.portList = data.responseObject.searchResult;
                        console.log("$scope.originList ", $scope.originList);
                        if (obj.origin != undefined && obj.origin != null && obj.origin != "") {
                            $scope.portList = $scope.filterPort($scope.portList, obj.origin.id);
                        }
                        if (obj.transitPort != undefined && obj.transitPort != null && obj.transitPort != "") {
                            $scope.portList = $scope.filterPort($scope.portList, obj.transitPort.id);
                        }
                        if (obj.destination != undefined && obj.destination != null && obj.destination != "") {
                            $scope.portList = $scope.filterPort($scope.portList, obj.destination.id);
                        }
                        return $scope.portList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching origin');
                });
        } else {
            $rootScope.clientMessage = $rootScope.nls['ERR2000238'];
        }
    };
    $scope.filterPort = function(array, id) {
        for (var i in array) {
            if (array[i].id == id) {
                array.splice(i, 1);
                break;
            }
        }
        return array;
    }

    $scope.ajaxCarrierEvent = function(object) {
        console.log("Ajax Carrier Rate Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CarrierByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.carrierList = data.responseObject.searchResult;
                    console.log("$scope.carrierList ", $scope.carrierList);
                    return $scope.carrierList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier');
            });
    };

    $scope.ajaxPartyEvent = function(object, type) {
        console.log("Ajax Party Event method : " + object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partyList = data.responseObject.searchResult;
                    //$scope.showPartyList=true;
                    return $scope.partyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );
    }
    $scope.ajaxCommodityEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CommodityList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.commodityList = data.responseObject.searchResult;
                    return $scope.commodityList;
                    //$scope.showCommodityList=true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching commodity List');
            }
        );
    }

    $scope.ajaxDivisionEvent = function(object) {
        console.log("ajaxStateEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return DivisionListByCompany
            .fetch({
                "companyId": $rootScope.userProfile.selectedUserLocation.companyMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return $scope.divisionList = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });
    }

    $scope.ajaxTosEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
            if (data.responseCode == "ERR0") {
                $scope.tosList = data.responseObject.searchResult;
                return $scope.tosList;
            }
        }, function(errResponse) {
            console.error('Error while fetching Tos');
        });
    }


    $scope.getDC = function(id) {
        DefaultChargeView.get({
            id: id
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.pricingPortPair = data.responseObject;
                $scope.pricingPortPair.validFrom = $rootScope.dateToString($scope.pricingPortPair.validFrom);
                $scope.pricingPortPair.validTo = $rootScope.dateToString($scope.pricingPortPair.validTo);

                if ($scope.pricingPortPair.isForwarderOrDirect != undefined && $scope.pricingPortPair.isForwarderOrDirect != null) {
                    $scope.pricingPortPair.isForwarderOrDirect = $scope.pricingPortPair.isForwarderOrDirect == 'Direct' ? true : false;
                } else {
                    $scope.pricingPortPair.isForwarderOrDirect = false;
                }
            } else {
                console.log("DC view Failed : " + error)
            }
        }, function(error) {
            console.log("DC view Failed : " + error)
        });
    }

    $scope.isEmptyRow = function(index, obj) {
        if ($scope.checkEmpty(obj)) {
            if ($scope.pricingPortPairList == null || $scope.pricingPortPairList == undefined || $scope.pricingPortPairList.length == 1) {
                $rootScope.clientMessage = $rootScope.nls['ERR2000242'];
                return true;
            }
            $scope.pricingPortPairList.splice(index, 1);
            $scope.isEmpty = true;
            return true;
        } else {
            return false;
        }
    }




    $scope.checkEmpty = function(obj) {
        //return (Object.getOwnPropertyNames(obj).length === 0);
        var isempty = true; //  empty
        if (!obj) {
            return isempty;
        }

        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]] != $scope.defaultObj[k[i]] || obj[k[i]] != $scope.defaultObjP[k[i]]) {
                isempty = false; // not empty
                break;
            }
        }
        return isempty;
    }

    $scope.checkMasterEmpty = function(obj) {
        if (obj.destination == undefined && obj.origin == undefined && obj.transit == undefined &&
            obj.pricingPortPairList[0].serviceMaster == undefined && obj.pricingPortPairList[0].chargeMaster == undefined &&
            obj.pricingPortPairList[0].currencyMaster == undefined && obj.pricingPortPairList[0].unitMaster == undefined &&
            obj.pricingPortPairList[0].stdSelInAmount == undefined && obj.pricingPortPairList[0].stdSelInMinimum == undefined &&
            obj.pricingPortPairList[0].costInAmount == undefined && obj.pricingPortPairList[0].costInMinimum == undefined &&
            obj.pricingPortPairList[0].tosMaster == undefined && obj.pricingPortPairList[0].ppcc == undefined) {
            return true
        }
    }


    $scope.savePricingPort = function() {

        var respone = compareService.compareArray($scope.pricingMasterList)

        if (!respone.isDuplicate) {

            if ($scope.validatePricingMasterList('SAVE')) { //validation need to do

                $scope.pricingMasterListDto = {};
                $scope.pricingMasterListDto.pricingMasterList = [];
                $scope.pricingMasterListDto.pricingMasterList = angular.copy($scope.pricingMasterList);

                angular.forEach($scope.pricingMasterListDto.pricingMasterList, function(pricingMaster, index) {

                    angular.forEach($scope.pricingMasterListDto.pricingMasterList[index].pricingPortPairList, function(pricingPortPair, index1) {

                        convertObject(pricingPortPair, $scope.pricingMasterListDto.pricingMasterList[index]);
                    });

                });
                PricingAirSaveList.save($scope.pricingMasterListDto).$promise.then(function(data) {
                    if (data != null && data.responseCode == "ERR0") {
                        Notification.success($rootScope.nls["ERR400"]);
                        $state.go("layout.defaultCharges");
                    } else {
                        //Notification.error($rootScope.nls["ERR1"]);
                    }
                }, function(error) {
                    console.log("Default charge master save Failed : " + error)
                    // Notification.error($rootScope.nls["ERR1"]);
                });
            } else {
                console.log("validation fails in default charges..");
                // Notification.error($rootScope.nls["ERR2000243"]);
            }
        } else {
            console.log("Comparsion fails in default charges..");
            var respDesc = $rootScope.nls["ERR2000247"];
            if (respDesc != undefined && respDesc != null) {
                respDesc = respDesc.replace("%s", parseInt(respone.start) + 1);
                respDesc = respDesc.replace("%d", parseInt(respone.end) + 1);
            } else {
                respDesc = "Duplicate Found";
            }
            Notification.error(respDesc);
        }
    }



    function convertObject(pricingPortPair, pricingMaster) {

        if (pricingPortPair == undefined || pricingPortPair == null || pricingPortPair == "") {
            Notification.error("Please create atleast one object");
        } else {
            //pricingPortPair.importExport=pricingMaster.serviceMaster.importExport;

            if (pricingPortPair.validFromDate == undefined || pricingPortPair.validFromDate == null || pricingPortPair.validFromDate == "") {
                pricingPortPair.validFromDate = $rootScope.dateToString(new Date());
            }
            if (pricingPortPair.validToDate == undefined || pricingPortPair.validToDate == null || pricingPortPair.validToDate == "") {
                pricingPortPair.validToDate = $rootScope.dateToString(moment().add('days', $rootScope.appMasterData['defaultcharge.valid.days']));
            }
            pricingPortPair.validFromDate = $rootScope.sendApiDateAndTime(pricingPortPair.validFromDate);
            pricingPortPair.validToDate = $rootScope.sendApiDateAndTime(pricingPortPair.validToDate);
        }
    }

    //which is updating dc ..in pricing port pair table			 
    $scope.updatePricingPort = function() {

        //validation
        $scope.errorMap = new Map();
        $scope.errorArr = [];
        $scope.errorArr.push(1);
        $scope.errorDc = [];
        $scope.errorDc[0] = {};
        $scope.errorDc[0].errTextArr = [];

        if ($scope.validateDc(0, $scope.pricingPortPair.pricingMaster, $scope.pricingPortPair, 0)) {

            $scope.pricingPortPair.validFromDate = $rootScope.sendApiDateAndTime($scope.pricingPortPair.validFromDate);
            $scope.pricingPortPair.validToDate = $rootScope.sendApiDateAndTime($scope.pricingPortPair.validToDate);


            PricingPortUpdate.update($scope.pricingPortPair).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("PricingAir update successfully..");
                    var params = {};
                    params.submitAction = 'Saved';
                    $state.go("layout.defaultCharges", params);
                    Notification.success($rootScope.nls["ERR401"]);
                } else {
                    console.log("pricingMaster Update Failed " + data.responseDescription)
                    // Notification.error("Failed to update");
                }
            }, function(error) {
                console.log("pricingMaster Update Failed : " + error)
                //Notification.error("Failed to update");
            });
        } else {
            //Notification.error($rootScope.nls["ERR2000243"]);
            console.log("vaidation fails..")
        }
    }

    /*$scope.deleteDc = function(index,list) {
    	if(list!=undefined && list!=null && list.length>1){
    		list.splice(index,1);
    	}
    }*/




    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });



    $scope.validatePricingMasterList = function(op, parentObj, childObj, indexValue) {

        $scope.errorMap = new Map();
        $scope.errorArr = [];
        $scope.errorArr.push(1);
        $scope.errorDc = [];

        if (op == 'ADD' && parentObj == undefined && childObj == undefined) {

            angular.forEach($scope.pricingMasterList, function(pricingMaster, index) {

                angular.forEach($scope.pricingMasterList[index].pricingPortPairList, function(pricingPortPair, index1) {

                    $scope.errorDc[index] = {};
                    $scope.errorDc[index].errTextArr = [];
                    $scope.validateDc(0, pricingMaster, pricingPortPair, index);

                });

            });
        } else if (op == 'SAVE' && parentObj == undefined && childObj == undefined) {

            if ($scope.pricingMasterList != undefined && $scope.pricingMasterList != null && $scope.pricingMasterList.length > 0) {


                angular.forEach($scope.pricingMasterList, function(pricingMaster, index) {

                    angular.forEach($scope.pricingMasterList[index].pricingPortPairList, function(pricingPortPair, index1) {

                        if (!$scope.checkEmpty(pricingPortPair) || !$scope.checkEmpty(pricingMaster)) {
                            $scope.errorDc[index] = {};
                            $scope.errorDc[index].errTextArr = [];
                            $scope.validateDc(0, pricingMaster, pricingPortPair, index);
                        } else {
                            if ($scope.pricingMasterList.length == 1) {
                                Notification.error("Please add atleast one for saving...");
                                return;
                            } else {
                                $scope.pricingMasterList.splice(index, 1);
                                return;
                            }
                        }
                    });
                });
            }
        } else {
            $scope.errorDc[indexValue] = {};
            $scope.errorDc[indexValue].errTextArr = [];
            $scope.validateDc(0, parentObj, childObj, indexValue);
        }
        if ($scope.errorArr != undefined && $scope.errorArr.indexOf(0) < 0) {
            return true
        } else {
            return false;
        }
    }

    //validatons for edit and add

    $scope.validateDc = function(validateCode, parentObj, childObj, index) {

        var index = index;

        if (validateCode == 0 || validateCode == 1) {

            if (childObj.serviceMaster == undefined ||
                childObj.serviceMaster == null ||
                childObj.serviceMaster.id == undefined) {
                $scope.errorDc[index].dcService = true;
                $scope.insertErrorKey(index, 'ERR2000201');
            } else {
                if (childObj.serviceMaster.status == "Block") {
                    $scope.errorDc[index].dcService = true;
                    $scope.insertErrorKey(index, 'ERR2000202');
                    childObj.serviceMaster = null;
                    //$scope.errorDc[index].dcService = true;
                    //$scope.insertErrorKey(index,'ERR2000202');
                }
            }
        }


        if (validateCode == 0 || validateCode == 2) {

            if (childObj.chargeMaster == undefined ||
                childObj.chargeMaster == null ||
                childObj.chargeMaster.id == undefined) {
                $scope.errorDc[index].dcCharge = true;
                $scope.insertErrorKey(index, 'ERR2000204');
            } else {
                if (childObj.chargeMaster.status == "Block") {
                    $scope.errorDc[index].dcCharge = true;
                    $scope.insertErrorKey(index, 'ERR2000205');
                    childObj.chargeMaster = null;
                }
            }
        } // charge ended


        if (validateCode == 0 || validateCode == 3) {

            if (childObj.currencyMaster == undefined ||
                childObj.currencyMaster == null ||
                childObj.currencyMaster.id == undefined) {
                $scope.errorDc[index].dcCurrency = true;
                $scope.insertErrorKey(index, 'ERR2000207');
            } else {
                if (childObj.currencyMaster.status == "Block") {
                    $scope.errorDc[index].dcCurrency = true;
                    childObj.currencyMaster = null;
                    $scope.insertErrorKey(index, 'ERR2000208');
                }
            }
        } // currency ended

        if (validateCode == 0 || validateCode == 4) {

            if (childObj.unitMaster == undefined ||
                childObj.unitMaster == null ||
                childObj.unitMaster.id == undefined) {
                $scope.errorDc[index].dcUnit = true;
                $scope.insertErrorKey(index, 'ERR2000210');
            } else {
                if (childObj.unitMaster.status == "Block") {
                    $scope.errorDc[index].dcUnit = true;
                    $scope.insertErrorKey(index, 'ERR2000211');
                    childObj.unitMaster = null;
                }
            }
        } //unit ended


        if (validateCode == 0 || validateCode == 5) {

            if (childObj.stdSelInAmount == undefined ||
                childObj.stdSelInAmount == null ||
                childObj.stdSelInAmount == "" || isNaN(childObj.stdSelInAmount)) {
                $scope.errorDc[index].dcRA = true;
                $scope.insertErrorKey(index, 'ERR2000213');
            } else {
                if (childObj.stdSelInAmount <= 0 || childObj.stdSelInAmount > 9999999999.999999) {

                    $scope.errorDc[index].dcRA = true;
                    $scope.insertErrorKey(index, 'ERR2000214');
                }

            }

            if (childObj.costInAmount == undefined ||
                childObj.costInAmount == null ||
                childObj.costInAmount == "" || isNaN(childObj.costInAmount)) {
                $scope.errorDc[index].dcCA = true;
                $scope.insertErrorKey(index, 'ERR2000244');
            } else {
                if (childObj.costInAmount <= 0 || childObj.costInAmount > 9999999999.999999) {

                    $scope.errorDc[index].dcCA = true;
                    $scope.insertErrorKey(index, 'ERR2000245');
                }

            }
        } //AMount per unit ended

        if (validateCode == 0 || validateCode == 6) {

            if (childObj.stdSelInMinimum === undefined ||
                childObj.stdSelInMinimum === null ||
                childObj.stdSelInMinimum === "" || isNaN(childObj.stdSelInMinimum)) {
                /* $scope.errorDc[index].dcAPT = true;
		 		     $scope.insertErrorKey(index,'ERR2000213');*/
                childObj.stdSelInMinimum = null;
            } else {
                if (childObj.stdSelInMinimum < 0 || childObj.stdSelInMinimum > 9999999999.999999) {
                    $scope.errorDc[index].dcRMA = true;
                    $scope.insertErrorKey(index, 'ERR2000215');
                }
            }
            if (childObj.costInMinimum === undefined ||
                childObj.costInMinimum === null ||
                childObj.costInMinimum === "" || isNaN(childObj.costInMinimum)) {
                childObj.costInMinimum = null;
                /* $scope.errorDc[index].dcAPT = true;
		 		     $scope.insertErrorKey(index,'ERR2000213');*/
            } else {
                if (childObj.costInMinimum < 0 || childObj.costInMinimum > 9999999999.999999) {
                    $scope.errorDc[index].dcCMA = true;
                    $scope.insertErrorKey(index, 'ERR2000246');
                }

            }

        } // min amount

        if (validateCode == 0 || validateCode == 7) {

            if (parentObj == undefined || parentObj.origin == undefined ||
                parentObj.origin == null ||
                parentObj.origin.id == undefined) {} else {
                if (parentObj.origin.status == "Block") {
                    $scope.errorDc[index].dcOrigin = true;
                    parentObj.origin = null;
                    $scope.insertErrorKey(index, 'ERR2000218');
                }
            }
        } //origin ended

        if (validateCode == 0 || validateCode == 8) {

            if (parentObj == undefined || parentObj.transit == undefined ||
                parentObj.transit == null ||
                parentObj.transit.id == undefined) {} else {
                if (parentObj.transit.status == "Block") {
                    $scope.errorDc[index].dcTP = true;
                    parentObj.transit = null;
                    $scope.insertErrorKey(index, 'ERR2000222');
                }
            }
        } //tp ended
        if (validateCode == 0 || validateCode == 9) {

            if (parentObj == undefined || parentObj.destination == undefined ||
                parentObj.destination == null ||
                parentObj.destination.id == undefined) {} else {
                if (parentObj.destination.status == "Block") {
                    parentObj.destination = null;
                    $scope.errorDc[index].dcDestination = true;
                    $scope.insertErrorKey(index, 'ERR2000220');
                }
            }
        } //destination ended


        if (validateCode == 0) {

            if (parentObj != undefined && parentObj.origin != undefined && parentObj.destination != undefined && parentObj.origin.id != undefined && parentObj.destination.id != undefined) {

                if (parentObj.origin.id === parentObj.destination.id) {
                    $scope.errorDc[index].origin = true;
                    $scope.insertErrorKey(index, 'ERR4009');

                }
            }

            if (parentObj != undefined && parentObj.origin != undefined && parentObj.transit != undefined && parentObj.origin.id != undefined && parentObj.transit.id != undefined) {

                if (parentObj.origin.id === parentObj.transit.id) {
                    $scope.errorDc[index].origin = true;
                    $scope.insertErrorKey(index, 'ERR4010');

                }
            }

            if (parentObj != undefined && parentObj.transit != undefined && parentObj.destination != undefined && parentObj.transit.id != undefined && parentObj.destination.id != undefined) {

                if (parentObj.transit.id === parentObj.destination.id) {
                    $scope.errorDc[index].destination = true;
                    $scope.insertErrorKey(index, 'ERR4011');
                }
            }

            if (parentObj != undefined && ((parentObj.origin == undefined || parentObj.origin == null || parentObj.origin.id == undefined) && (parentObj.destination == undefined || parentObj.destination == null || parentObj.destination.id == undefined))) {
                $scope.errorDc[index].origin = true;
                $scope.insertErrorKey(index, 'ERR2000249');
            }

        }


        if (validateCode == 0 || validateCode == 10) {

            if (childObj.tosMaster == undefined ||
                childObj.tosMaster == null ||
                childObj.tosMaster.id == undefined) {} else {
                if (childObj.tosMaster.status == "Block") {
                    childObj.tosMaster = null;
                    $scope.errorDc[index].dcTosM = true;
                    $scope.insertErrorKey(index, 'ERR2000239');
                }
            }
        } //tos ended


        if (validateCode == 0 || validateCode == 11) {
            if (childObj.validFrom == undefined ||
                childObj.validFrom == null || childObj.validFrom == "") {} else {
                if (childObj.validTo != undefined &&
                    childObj.validTo != null &&
                    childObj.validTo != "") {
                    var validTo = moment(childObj.validTo).format("DD-MM-YYYY");;
                    var validFrom = moment(childObj.validFrom).format("DD-MM-YYYY");
                    if (validTo < validFrom) {
                        $scope.errorMap.put("dcDate", $rootScope.nls["ERR2000241"]);
                        return false;
                    }
                }


            }
        } //valid from validation

        if (validateCode == 0 || validateCode == 13) {

            if (childObj.divisionMaster == undefined ||
                childObj.divisionMaster == null ||
                childObj.divisionMaster.id == undefined) {} else {
                if (childObj.divisionMaster.status == "Block") {
                    childObj.divisionMaster = null;
                    $scope.errorMap.put("dcDIVM", $rootScope.nls["ERR2000216"]);
                    return false;
                }
            }
        } //divison ended

        if (validateCode == 0 || validateCode == 14) {

            if (childObj.agent == undefined ||
                childObj.agent == null ||
                childObj.agent.id == undefined) {} else {
                if (childObj.agent.status == "Block") {
                    childObj.agent = null;
                    $scope.errorMap.put("dcAgent", $rootScope.nls["ERR2000231"]);
                    return false;
                }

                if (childObj.agent.isDefaulter) {
                    childObj.agent = null;
                    $scope.errorMap.put("dcAgent", $rootScope.nls["ERR2000230"]);
                    return false;
                }


            }
        } // agent validation ends

        if (validateCode == 0 || validateCode == 15) {

            if (childObj.commodityMaster == undefined ||
                childObj.commodityMaster == null ||
                childObj.commodityMaster.id == undefined) {} else {
                if (childObj.commodityMaster.status == "Block") {
                    childObj.commodityMaster = null;
                    $scope.errorMap.put("dcCOMM", $rootScope.nls["ERR2000233"]);
                    return false;
                }
            }
        } //commodity master

        if (validateCode == 0 || validateCode == 16) {

            if (childObj.chargeMaster != undefined && childObj.chargeMaster.id != undefined && childObj.chargeMaster.chargeType == 'Freight' && (childObj.carrierMaster == undefined ||
                    childObj.carrierMaster == null ||
                    childObj.carrierMaster.id == undefined)) {
                $scope.errorMap.put("dcCRM", $rootScope.nls["ERR2000243"]);
            } else {
                if (childObj.carrierMaster != undefined && childObj.carrierMaster.id != undefined && childObj.carrierMaster.status == "Block") {
                    childObj.carrierMaster = null;
                    $scope.errorMap.put("dcCRM", $rootScope.nls["ERR2000235"]);
                    return false;
                }
            }
        } //carrier master ends


        if (validateCode == 0 || validateCode == 17) {

            if (childObj.shipper == undefined ||
                childObj.shipper == null ||
                childObj.shipper.id == undefined) {} else {
                if (childObj.shipper.status == "Block") {
                    childObj.shipper = null;
                    $scope.errorMap.put("dcShipper", $rootScope.nls["ERR2000225"]);
                    return false;
                }
                if (childObj.shipper.isDefaulter) {
                    childObj.shipper = null;
                    $scope.errorMap.put("dcShipper", $rootScope.nls["ERR2000224"]);
                    return false;
                }

            }
        } // shipper ends

        if (validateCode == 0 || validateCode == 18) {

            if (childObj.consignee == undefined ||
                childObj.consignee == null ||
                childObj.consignee.id == undefined) {} else {
                if (childObj.consignee.status == "Block") {
                    childObj.consignee = null;
                    $scope.errorMap.put("dcConsignee", $rootScope.nls["ERR2000228"]);
                    return false;
                }

                if (childObj.consignee.isDefaulter) {
                    childObj.consignee = null;
                    $scope.errorDc[index].dcConsignee = true;
                    $scope.errorMap.put("dcConsignee", $rootScope.nls["ERR2000227"]);
                    return false;
                }
            }
        } //consignee ends

        if ($scope.errorArr.indexOf(0) < 0) {
            return true
        } else {
            return false;
        }

    } //validation ended

    $scope.insertErrorKey = function(index, errorcode) {
        $scope.errorArr[0] = 0;
        $scope.errorDc[index].errRow = true;
        var err = $rootScope.nls[errorcode];
        $scope.errorDc[index].errTextArr.push(err);
    }


    $scope.$on('addDefaultChargesEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.pricingMasterList);
        localStorage.isDCReloaded = "YES";
        return "Do you want to Reload";
    });

    $scope.$on('editDefaultChargesEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.pricingPortPair);
        localStorage.isDCReloaded = "YES";
        return "Do you want to Reload";
    });

    $scope.$on('addDefaultChargesEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.pricingMasterList;
        $rootScope.unfinishedFormTitle = "DefaultCharges (New)";
        $rootScope.category = "DefaultCharges";
    })

    $scope.$on('editDefaultChargesEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.pricingPortPair;
        $rootScope.category = "Default Charges";
        $rootScope.unfinishedFormTitle = "DefaultCharges Edit # ";
    })


    // Navigation 
    $scope.isReloaded = localStorage.isDCReloaded;
    $scope.oldData = {};
    if ($stateParams.action == "ADD") {
        $scope.pricingMasterList = [];
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isDCReloaded = "NO";
                $scope.pricingMasterList = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.pricingMasterList = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isDCReloaded = "NO";
                $scope.pricingMasterList = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.pricingMasterList.push({});
                $scope.pricingMasterList[0].pricingPortPairList = [];
                $scope.pricingMasterList[0].pricingPortPairList.push({});
                $scope.setDefaultAddValues($scope.pricingMasterList);
            }
        }
        $rootScope.navigateToNextField(($scope.pricingMasterList.length - 1) + 'dcService');
        $rootScope.breadcrumbArr = [{
            label: "CRM",
            state: "layout.defaultCharges"
        }, {
            label: "DefaultCharge",
            state: "layout.defaultCharges"
        }, {
            label: "Add DefaultCharge",
            state: null
        }];
        $scope.oldData = JSON.stringify($scope.pricingMasterList);
    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isDCReloaded = "NO";
                $scope.pricingPortPair = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.pricingPortPair = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isDCReloaded = "NO";
                $scope.pricingPortPair = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.pricingPortPair = defaultService.get();
                $scope.pricingPortPair.validToDate = $rootScope.dateToString($scope.pricingPortPair.validToDate);
                $scope.pricingPortPair.validFromDate = $rootScope.dateToString($scope.pricingPortPair.validFromDate);
            }
        }
        $rootScope.navigateToNextField('dcService');
        $rootScope.breadcrumbArr = [{
            label: "CRM",
            state: "layout.defaultCharges"
        }, {
            label: "DefaultCharge",
            state: "layout.defaultCharges"
        }, {
            label: "Edit DefaultCharge",
            state: null
        }];
        $scope.oldData = JSON.stringify($scope.pricingPortPair);
    }

}]);