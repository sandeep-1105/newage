app.controller('defaultChrgeCtrl',['$rootScope','$scope', '$state', '$http', '$location', '$window', 'Notification',
        'ngDialog', 'SearchCharge', '$stateParams', 'DefaultChargeRemove', 'DefaultChargeView', 
        'PricingPortPairView', 'PricingAirView', 'defaultService', 'roleConstant',
    function($rootScope,$scope, $state, $http, $location, $window, Notification,
        ngDialog, SearchCharge, $stateParams,DefaultChargeRemove, DefaultChargeView, 
        PricingPortPairView, PricingAirView, defaultService, roleConstant) {

        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.selectedRecordIndex = 0;
        $scope.precollecArr = [{
            "title": "Prepaid",
            "value": "Prepaid"
        }, {
            "title": "Collect",
            "value": "Collect"
        }];
        console.log("Default Charge controller loaded..");
        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.pricingAirSearchDto = {};
        $scope.deskTopView = true;
        $scope.mainpreloder = false;
        $scope.showDetailCharge = false;
        $scope.btnGrp = {};
        $scope.btnGrp.TypeServiceArr = ["Export", "Import", "Both"];
        $scope.pricingAirSearchDto.searchImportExport = 'Both';

        /********************* ux fix ends here *****************************/

        $scope.init = function() {
            console.log("Default Charges Init method called.");
            $scope.showDetailCharge = false;
            $scope.searchDC();
        };

        //add default charges
        $scope.addDC = function() {
            $state.go('layout.addDefaultCharges');
        }

        $scope.defaultChargeHeadArr = [{
                "name": "#",
                "width": "w50px",
                "prefWidth": "50",
                "model": "no",
                "search": false,
            },
            {
                "name": "Service",
                "width": "w125px",
                "prefWidth": "125",
                "model": "serviceName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchServiceName"
            },
            {
                "name": "Charge",
                "width": "w125px",
                "prefWidth": "125",
                "model": "chargeName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchCharge"
            },
            {
                "name": "Currency",
                "width": "w100px",
                "prefWidth": "100",
                "model": "currencyName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchCurrency"
            },
            {
                "name": "Unit",
                "width": "w100px",
                "prefWidth": "100",
                "model": "unitName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchUnit"
            },
            {
                "name": " Revenue <br> " +
                    "Min Amount",
                "width": "w125px",
                "prefWidth": "125",
                "model": "stdSelInMinimum",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchMinAmount"
            },
            {
                "name": " Revenue<br>" +
                    "Amount per Unit",
                "width": "w125px",
                "prefWidth": "125",
                "model": "stdSelInAmount",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchAmountPerUnit"
            },
            {
                "name": "  Cost <br> " +
                    "Min Amount",
                "width": "w125px",
                "prefWidth": "125",
                "model": "costInMinimum",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchCostInMinimum"
            },
            {
                "name": " Cost <br>" +
                    "Amount per Unit",
                "width": "w125px",
                "prefWidth": "125",
                "model": "costInAmount",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchCostInAmount"
            },
            {
                "name": "Origin",
                "width": "w100px",
                "prefWidth": "100",
                "model": "originName",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchPortOrigin"
            },
            {
                "name": "Transit	Port",
                "width": "w75px",
                "prefWidth": "75",
                "model": "transitName",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchTransitPort"
            },
            {
                "name": "Destination",
                "width": "w100px",
                "prefWidth": "100",
                "model": "destinationName",
                "search": true,
                "sort": true,
                "type": "text",
                "key": "searchPortDestination"
            },
            /*{
            	"name":"TOS",
            	"width":"w75px",
            	"prefWidth":"75",
            	"model":"tosMaster.tosName",
            	"search":true,
            	"type":"text",
            	"sort":true,
            	"key":"searchTosCode"
            },
            {
            	"name":"Prepaid / Collect",
            	"width":"w100px",
            	"prefWidth":"100",
            	"model":"ppcc",
            	"search":true,
            	"sort":true,
            	"data":$rootScope.enum['PPCC'],
            	"type":"drop",
            	"key":"searchPpcc"
            },*/
            /*{
            	"name":"Vaild From",
            	"width":"w100px",
            	"prefWidth":"100",
            	"model":"validFrom",
            	"search":true,
            	"sort":true,
            	"type":"date-range",
            	"key":"searchValidFrom"
            },
            {
            	"name":"Valid To",
            	"width":"w100px",
            	"prefWidth":"100",
            	"model":"validTo",
            	"search":true,
            	"sort":true,
            	"type":"date-range",
            	"key":"searchValidTo"
            }*/
        ]




        $scope.sortSelection = {
            sortKey: "serviceName",
            sortOrder: "asc"
        }

        $scope.changeSearch = function(param) {
            console.log("changeSearch is called ", param);

            for (var attrname in param) {
                $scope.pricingAirSearchDto[attrname] = param[attrname];
            }

            $scope.searchDC();
            console.log("change searchDC", $scope.searchDCDto);
        }


        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.searchDC();
        }


        $scope.sortChange = function(param) {
            $scope.pricingAirSearchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.pricingAirSearchDto.sortByColumn = param.sortKey;
            $scope.searchDC();
        }
        $scope.changepage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.searchDC();
        }

        $scope.searchDC = function() {
            if ($scope.pricingAirSearchDto == undefined || $scope.pricingAirSearchDto == null) {
                $scope.pricingAirSearchDto = {};
            }
            $scope.pricingAirSearchDto.selectedPageNumber = $scope.page;
            $scope.pricingAirSearchDto.recordPerPage = $scope.limit;

            /*	if($scope.pricingAirSearchDto.searchValidFrom!=null && $scope.pricingAirSearchDto.searchValidFrom != ""){
            		$scope.pricingAirSearchDto.searchValidFrom.startDate = $rootScope.sendApiStartDateTime($scope.pricingAirSearchDto.searchValidFrom.startDate);
            		$scope.pricingAirSearchDto.searchValidFrom.endDate = $rootScope.sendApiEndDateTime($scope.pricingAirSearchDto.searchValidFrom.endDate);
            	}
            	if($scope.pricingAirSearchDto.searchValidTo!=null && $scope.pricingAirSearchDto.searchValidTo != ""){
            		$scope.pricingAirSearchDto.searchValidTo.startDate = $rootScope.sendApiStartDateTime($scope.pricingAirSearchDto.searchValidTo.startDate);
            		$scope.pricingAirSearchDto.searchValidTo.endDate = $rootScope.sendApiEndDateTime($scope.pricingAirSearchDto.searchValidTo.endDate);
            	}*/


            SearchCharge.query($scope.pricingAirSearchDto).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                console.log($scope.totalRecord);
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    /*								tempObj.stdSelInMinimum = $rootScope.currencyFormat(tempObj.currencyMaster,tempObj.stdSelInMinimum);
                    								tempObj.stdSelInAmount = $rootScope.currencyFormat(tempObj.currencyMaster,tempObj.stdSelInAmount);
                    								tempObj.costInMinimum = $rootScope.currencyFormat(tempObj.currencyMaster,tempObj.costInMinimum);
                    								tempObj.costInAmount = $rootScope.currencyFormat(tempObj.currencyMaster,tempObj.costInAmount);
                    */
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.defaultChargeArr = resultArr;
            });
        }

        $scope.clickOnTab = function(tab) {
            $scope.detailTab = tab;
            $scope.pricingAirSearchDto = {};
            $scope.searchDC();
            $scope.showDetailCharge = false;
        }

        $scope.view = function(data) {
            var id = data.id;
            var pricingMasterId = data.pricingMasterId;
            PricingPortPairView.get({
                id: id
            }, function(data) {
                $rootScope.mainpreloder = true;
                if (data.responseCode == 'ERR0') {
                    console.log("DC rowselect")
                    $scope.Logs = false;
                    $rootScope.mainpreloder = false;
                    $scope.pricingPortPair = data.responseObject;
                    if ($scope.pricingPortPair == null) {
                        Notification.error("Problem while found Pricing air");
                        return;
                    }
                    PricingAirView
                        .get({
                                id: pricingMasterId
                            },
                            function(data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.pricingMaster = data.responseObject;
                                    $rootScope.mainpreloder = false;
                                    if ($scope.pricingMaster == null) {
                                        Notification.error("Problem while found Pricing air");
                                        return;
                                    }
                                    $scope.pricingPortPair.pricingMaster = $scope.pricingMaster;
                                    $scope.pricingPortPair.screen = $scope.detailTab;
                                    $scope.showHistory = false;
                                    $scope.showDetailCharge = true;
                                    $scope.deskTopView = false;
                                } else {
                                    $rootScope.mainpreloder = false;
                                }
                            },
                            function(error) {
                                $rootScope.mainpreloder = false;
                                console
                                    .log(
                                        "Pricing AirView Failed : ",
                                        error);
                            });
                } else {
                    console.log("DC view Failed : " + error)
                    $rootScope.mainpreloder = false;
                }
            }, function(error) {
                console.log("DC view Failed : " + error)
                $rootScope.mainpreloder = false;
            });

        }

        /*$scope.view = function(selectedMaster) {
        	console.log("View Default Charge Called");
        	$scope.defaultCharge = selectedMaster;
        };*/

        $scope.rowSelect = function(data, index) {
            if ($rootScope.roleAccess(roleConstant.CRM_DEFAULT_CHARGES_VIEW)) {
                $scope.selectedRecordIndex = parseInt($scope.pricingAirSearchDto.recordPerPage) * parseInt($scope.pricingAirSearchDto.selectedPageNumber) + parseInt(index);
                $scope.pricingAirSearchDto.selectedPageNumber = $scope.selectedRecordIndex;
                $scope.view(data);
            }
        }
        $scope.singlePageNavigation = function(val) {
            if ((parseInt($scope.selectedRecordIndex) + val) < 0 || (parseInt($scope.selectedRecordIndex) + val) >= parseInt($scope.totalRecord))
                return;
            $scope.mainpreloder = true;
            $scope.pricingAirSearchDto.recordPerPage = 1;
            $scope.pricingAirSearchDto.selectedPageNumber = parseInt($scope.pricingAirSearchDto.selectedPageNumber) + val;
            SearchCharge.query($scope.pricingAirSearchDto).$promise.then(function(
                data, status) {
                $scope.selectedRecordIndex = parseInt($scope.pricingAirSearchDto.selectedPageNumber);
                $scope.totalRecord = data.responseObject.totalRecord;
                $scope.view(data.responseObject.searchResult[0]);
            });
        }

        $scope.backToScreen = function() {
            $scope.showDetailCharge = false;
            var stateCount = 0;
            $scope.selectedRecordIndex = 0;
            if ($stateParams.count != undefined && $stateParams.count != null) {
                stateCount = $stateParams.count + 1;
            }
            $state.go('layout.defaultCharges', {
                count: stateCount
            });
        }

        $scope.editDc = function(obj) {
            if ($rootScope.roleAccess(roleConstant.CRM_DEFAULT_CHARGES_MODIFY)) {
                console.log("Edit Dc Called");
                defaultService.set(obj);
                $state.go("layout.editDefaultCharges");
            }
        };


        $scope.deleteDC = function(obj) {
            if ($rootScope.roleAccess(roleConstant.CRM_DEFAULT_CHARGES_DELETE)) {
                ngDialog.openConfirm({
                        template: '<p>Are you sure you want to delete selected Default Charge?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    })
                    .then(function(value) {
                        DefaultChargeRemove.remove({
                            id: obj.id
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                console.log("DC deleted Successfully")
                                $rootScope.successDesc = $rootScope.nls["ERR402"];
                                $scope.backToScreen();
                            } else {
                                console.log("DC deleted Failed " + data.responseDescription)
                            }
                        }, function(error) {
                            console.log("DC deleted Failed : " + error)
                        });
                    }, function(value) {
                        console.log("deleted cancelled");
                    });

            }
        }




        $scope.addDC = function() {
            console.log("move to add page--DC")
            $state.go('layout.addDefaultCharges');
        }




        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/




        $scope.uploadImage = function(file) {

            if (file != null || file != undefined) {
                var fileName = file.name;
                var extention = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
                console.log("file extention ", extention);
                var valid = false;
                if (extention == 'jpeg' || extention == 'jpg' || extention == 'png' || extention == 'gif') {
                    valid = true;
                }
                var reader = new FileReader();
                reader.onload = function(event) {
                    var contents = event.target.result;
                    var uploadedFile = btoa(contents);
                    console.log("File contents: " + uploadedFile);
                    $scope.encodedLogo = uploadedFile;
                    $scope.uploadText = false;
                    $scope.isBusy = true;
                    $timeout(function() {
                        $scope.isBusy = false;
                        $scope.convertToByteArray(file);
                    }, 1000);

                };
                reader.readAsBinaryString(file);
            }
        }

        switch ($stateParams.action) {
            case "VIEW":
                $scope.view($stateParams.id);
                $rootScope.breadcrumbArr = [{
                        label: "CRM",
                        state: "layout.defaultCharges"
                    },
                    {
                        label: "Default Charge",
                        state: "layout.defaultCharges"
                    },
                    {
                        label: "View Default Charge",
                        state: null
                    }
                ];
                break;
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "CRM",
                        state: "layout.defaultCharges"
                    },
                    {
                        label: "Default Charge",
                        state: "layout.defaultCharges"
                    }
                ];
                break;
            default:

        }

    }]);