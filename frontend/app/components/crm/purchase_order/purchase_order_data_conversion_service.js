app.service('PODataConversionService', function($rootScope) {
	this.convertObjectToData = function(purchaseOrder) {
		if(purchaseOrder.poDate != undefined && purchaseOrder.poDate != null && purchaseOrder.poDate != "") {
			purchaseOrder.poDate = $rootScope.sendApiStartDateTime(purchaseOrder.poDate);
		}
		
		purchaseOrder.docReceivedDate = $rootScope.sendApiStartDateTime(purchaseOrder.docReceivedDate);
		if(purchaseOrder.purchaseOrderItemList != undefined && purchaseOrder.purchaseOrderItemList != null && purchaseOrder.purchaseOrderItemList.length > 0) {
			for(var index = 0; index < purchaseOrder.purchaseOrderItemList.length; index++) {
				purchaseOrder.purchaseOrderItemList[index].pickUpFrom = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderItemList[index].pickUpFrom);
				purchaseOrder.purchaseOrderItemList[index].pickUpWithin = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderItemList[index].pickUpWithin);
				purchaseOrder.purchaseOrderItemList[index].deliveryFrom = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderItemList[index].deliveryFrom);
				purchaseOrder.purchaseOrderItemList[index].deliveryWithin = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderItemList[index].deliveryWithin);
				purchaseOrder.purchaseOrderItemList[index].supplierConfDate = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderItemList[index].supplierConfDate);
			}
		}
		if(purchaseOrder.purchaseOrderVehicleInfoList != undefined && purchaseOrder.purchaseOrderVehicleInfoList != null && purchaseOrder.purchaseOrderVehicleInfoList.length > 0) {
			for(var index = 0; index < purchaseOrder.purchaseOrderVehicleInfoList.length; index++) {
				purchaseOrder.purchaseOrderVehicleInfoList[index].pickupDate = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderVehicleInfoList[index].pickupDate);
				purchaseOrder.purchaseOrderVehicleInfoList[index].deliveryDate = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderVehicleInfoList[index].deliveryDate);
			}
		}
		console.log("rerrege ",purchaseOrder.purchaseOrderRemarkFollowUpList);
		if(purchaseOrder.purchaseOrderRemarkFollowUpList != undefined && purchaseOrder.purchaseOrderRemarkFollowUpList != null && purchaseOrder.purchaseOrderRemarkFollowUpList.length > 0) {
			for(var index = 0; index < purchaseOrder.purchaseOrderRemarkFollowUpList.length; index++) {
				purchaseOrder.purchaseOrderRemarkFollowUpList[index].date = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderRemarkFollowUpList[index].date);
				purchaseOrder.purchaseOrderRemarkFollowUpList[index].followDate = $rootScope.sendApiStartDateTime(purchaseOrder.purchaseOrderRemarkFollowUpList[index].followDate);
			}
		}
		
		
		
		return purchaseOrder;
	}
	
	this.convertToEditData = function(purchaseOrder) {
		if(purchaseOrder.poDate != undefined && purchaseOrder.poDate != null && purchaseOrder.poDate != "") {
			purchaseOrder.poDate = $rootScope.dateToString(purchaseOrder.poDate);
		}
		purchaseOrder.docReceivedDate = $rootScope.dateToString(purchaseOrder.docReceivedDate);
		
		if(purchaseOrder.purchaseOrderItemList != undefined && purchaseOrder.purchaseOrderItemList != null && purchaseOrder.purchaseOrderItemList.length > 0) {
			for(var index = 0; index < purchaseOrder.purchaseOrderItemList.length; index++) {
				purchaseOrder.purchaseOrderItemList[index].pickUpFrom = $rootScope.dateToString(purchaseOrder.purchaseOrderItemList[index].pickUpFrom);
				purchaseOrder.purchaseOrderItemList[index].pickUpWithin = $rootScope.dateToString(purchaseOrder.purchaseOrderItemList[index].pickUpWithin);
				purchaseOrder.purchaseOrderItemList[index].deliveryFrom = $rootScope.dateToString(purchaseOrder.purchaseOrderItemList[index].deliveryFrom);
				purchaseOrder.purchaseOrderItemList[index].deliveryWithin = $rootScope.dateToString(purchaseOrder.purchaseOrderItemList[index].deliveryWithin);
				purchaseOrder.purchaseOrderItemList[index].supplierConfDate = $rootScope.dateToString(purchaseOrder.purchaseOrderItemList[index].supplierConfDate);
			}
		}
		if(purchaseOrder.purchaseOrderVehicleInfoList != undefined && purchaseOrder.purchaseOrderVehicleInfoList != null && purchaseOrder.purchaseOrderVehicleInfoList.length > 0) {
			for(var index = 0; index < purchaseOrder.purchaseOrderVehicleInfoList.length; index++) {
				purchaseOrder.purchaseOrderVehicleInfoList[index].pickupDate = $rootScope.dateToString(purchaseOrder.purchaseOrderVehicleInfoList[index].pickupDate);
				purchaseOrder.purchaseOrderVehicleInfoList[index].deliveryDate = $rootScope.dateToString(purchaseOrder.purchaseOrderVehicleInfoList[index].deliveryDate);
			}
		}
console.log("Reload ",purchaseOrder.purchaseOrderRemarkFollowUpList);
		if(purchaseOrder.purchaseOrderRemarkFollowUpList != undefined && purchaseOrder.purchaseOrderRemarkFollowUpList != null && purchaseOrder.purchaseOrderRemarkFollowUpList.length > 0) {
			for(var index = 0; index < purchaseOrder.purchaseOrderRemarkFollowUpList.length; index++) {
				purchaseOrder.purchaseOrderRemarkFollowUpList[index].date = $rootScope.dateToString(purchaseOrder.purchaseOrderRemarkFollowUpList[index].date);
				purchaseOrder.purchaseOrderRemarkFollowUpList[index].followDate = $rootScope.dateToString(purchaseOrder.purchaseOrderRemarkFollowUpList[index].followDate);
			}
		}
		
		return purchaseOrder;
	}

});

