app.factory('purchaseOrderFactory', function($resource) {

	return {
		common : $resource('/api/v1/purchaseorder/:id', {}, {
			create : {method : 'POST'},
			update : {method : 'PUT'},
			findById : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		search : $resource('/api/v1/purchaseorder/search', {}, {
			query : {method : 'POST'}
		}),
		findBypoNo : $resource('/api/v1/purchaseorder/getbypono/:poNo', {}, {
			poNo : {method : 'GET', params :{poNo : ''}, isArray : false}
		})
	};
})