app.service('POValidationService', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(purchaseOrder, code) {
		//Validate Buyer's Consolidation
		if(code == 0 || code == 1) {
			if (purchaseOrder.buyer == null || purchaseOrder.buyer.id == undefined || purchaseOrder.buyer.id == null || purchaseOrder.buyer.id == "") {
				return this.validationResponse(true, "bcMaster", "none", -1, $rootScope.nls["ERR03020"], purchaseOrder);
			} else if(purchaseOrder.buyer.status != null) {
	            if (purchaseOrder.buyer.status == "Block") {
	            	purchaseOrder.buyer = null;
					return this.validationResponse(true, "bcMaster", "none", -1, $rootScope.nls["ERR03021"], purchaseOrder);
	            }
	        }
		}
		//PO date
		if(code == 0 || code == 9) {
			if (purchaseOrder.poDate == undefined || purchaseOrder.poDate == null || purchaseOrder.poDate == "") {
				return this.validationResponse(true, "purchaseOrderDate", "none", -1, $rootScope.nls["ERR03009"], purchaseOrder);
			}
		}
		//Validate Supplier's Consolidation
		if(code == 0 || code == 2) {
			if (purchaseOrder.supplier == null || purchaseOrder.supplier.id == undefined || purchaseOrder.supplier.id == null || purchaseOrder.supplier.id == "") {
				return this.validationResponse(true, "poSupplier", "none", -1, $rootScope.nls["ERR03023"], purchaseOrder);
			} else if(purchaseOrder.supplier.status != null) {
	            if (purchaseOrder.supplier.status == "Block") {
	            	purchaseOrder.supplier = null;
					return this.validationResponse(true, "poSupplier", "none", -1, $rootScope.nls["ERR03024"], purchaseOrder);
	            }
	        }
		}
		//Validate Origin Agent Consolidation
		if(code == 0 || code == 3) {
			if (purchaseOrder.originAgent == null || purchaseOrder.originAgent.id == undefined || purchaseOrder.originAgent.id == null || purchaseOrder.originAgent.id == "") {
				return this.validationResponse(true, "poOriginAgent", "none", -1, $rootScope.nls["ERR03026"], purchaseOrder);
			} else {
					if(purchaseOrder.originAgent.status != null) {
		            if (purchaseOrder.originAgent.status == "Block") {
		            	purchaseOrder.originAgent = null;
						return this.validationResponse(true, "poOriginAgent", "none", -1, $rootScope.nls["ERR03027"], purchaseOrder);
		            }
		        }
			}
		}

		//Validate Destination Agent Consolidation
		if(code == 0 || code == 4) {
			if (purchaseOrder.destinationAgent == null || purchaseOrder.destinationAgent.id == undefined || purchaseOrder.destinationAgent.id == null || purchaseOrder.destinationAgent.id == "") {
				return this.validationResponse(true, "poDestinationAgent", "none", -1, $rootScope.nls["ERR03029"], purchaseOrder);
			} else {
				if(purchaseOrder.destinationAgent.status != null) {
		            if (purchaseOrder.destinationAgent.status == "Block") {
		            	purchaseOrder.destinationAgent = null;
						return this.validationResponse(true, "poDestinationAgent", "none", -1, $rootScope.nls["ERR03030"], purchaseOrder);
		            }
		        }
			}
		}

		//Validate Origin Port
		if(code == 0 || code == 5) {
			if (purchaseOrder.origin == null || purchaseOrder.origin.id == undefined || purchaseOrder.origin.id == null || purchaseOrder.origin.id == "") {
				return this.validationResponse(true, "poOrigin", "none", -1, $rootScope.nls["ERR03017"], purchaseOrder);
			} else if(purchaseOrder.origin.status != null) {
	            if (purchaseOrder.origin.status == "Block") {
	            	purchaseOrder.origin = null;
					return this.validationResponse(true, "poOrigin", "none", -1, $rootScope.nls["ERR03018"], purchaseOrder);
	            }
	        }
		}
		//Validate Destination Port
		if(code == 0 || code == 6) {

			if (purchaseOrder.destination == null || purchaseOrder.destination.id == undefined || purchaseOrder.destination.id == null || purchaseOrder.destination.id == "") {
				return this.validationResponse(true, "poDestination", "none", -1, $rootScope.nls["ERR03014"], purchaseOrder);
			} else if(purchaseOrder.destination.status != null) {
	            if (purchaseOrder.destination.status == "Block") {
	            	purchaseOrder.destination = null;
					return this.validationResponse(true, "poDestination", "none", -1, $rootScope.nls["ERR03015"], purchaseOrder);
	            }
	        }
		}
		
		//Check Origin and destination are same
		if (code == 0 || code == 5|| code == 6) {
			if(purchaseOrder.origin != null && purchaseOrder.origin != undefined && purchaseOrder.origin.portCode != null && 
				purchaseOrder.destination != null && purchaseOrder.destination != undefined && purchaseOrder.destination.portCode != null
					&& (purchaseOrder.origin.portCode == purchaseOrder.destination.portCode)) {
					purchaseOrder.destination = null;
					return this.validationResponse(true, "poSimilarPorts", "none", -1, $rootScope.nls["ERR03042"], purchaseOrder);
			}
		}
		

		//Validate Term of Shipment
		if(code == 0 || code == 7) {
			if (purchaseOrder.tosMaster != null && purchaseOrder.tosMaster.id != undefined && purchaseOrder.tosMaster.id != null && purchaseOrder.tosMaster.id != "") {
				if(purchaseOrder.tosMaster.status != null) {
		            if (purchaseOrder.tosMaster.status == "Block") {
		            	purchaseOrder.tosMaster = null;
						return this.validationResponse(true, "poTos", "none", -1, $rootScope.nls["ERR03012"], purchaseOrder);
		            }
		        }
			}
		}
		/*//PO no
		if(code == 0 || code == 8) {
			if (purchaseOrder.poNo == undefined || purchaseOrder.poNo == null || purchaseOrder.poNo == "") {
				return this.validationResponse(true, "purchaseOrderNumber", "none", -1, $rootScope.nls["ERR03070"], purchaseOrder);
			}
		}*/
		
		//Others Tab Validation
		if(code == 0 || code == 10) {
			if (purchaseOrder.supplierReferenceNo != undefined &&purchaseOrder.supplierReferenceNo != null && purchaseOrder.supplierReferenceNo != ""
				&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_FIFTY,purchaseOrder.supplierReferenceNo)) {
				return this.validationResponse(true, "poSupplierReferenceNo", "tab2", -1, $rootScope.nls["ERR030101"], purchaseOrder);
			}
		}

		if(code == 0 || code == 11) {
			if (purchaseOrder.supplierRemark != undefined &&purchaseOrder.supplierRemark != null && purchaseOrder.supplierRemark != ""
				&& !CommonValidationService.checkLength(purchaseOrder.supplierRemark,1,4000)) {
				return this.validationResponse(true, "poSupplierRemark", "tab2", -1, $rootScope.nls["ERR030102"], purchaseOrder);
			}
		}
		if(code == 0 || code == 12) {
			if (purchaseOrder.specialInstruction != undefined &&purchaseOrder.specialInstruction != null && purchaseOrder.specialInstruction != ""
				&& !CommonValidationService.checkLength(purchaseOrder.specialInstruction,1,4000)) {
				return this.validationResponse(true, "poSpecialInstruction", "tab2", -1, $rootScope.nls["ERR030103"], purchaseOrder);
			}
		}
		if(code == 0 || code == 13) {
			if (purchaseOrder.receivedFrom != undefined &&purchaseOrder.receivedFrom != null && purchaseOrder.receivedFrom != ""
				&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_HUNDRED,purchaseOrder.receivedFrom)) {
				return this.validationResponse(true, "poReceivedFrom", "tab2", -1, $rootScope.nls["ERR030104"], purchaseOrder);
			}
		}
		if(code == 0 || code == 14) {
			if (purchaseOrder.deliveryTo != undefined &&purchaseOrder.deliveryTo != null && purchaseOrder.deliveryTo != ""
				&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_HUNDRED,purchaseOrder.deliveryTo)) {
				return this.validationResponse(true, "poDeliveryTo", "tab2", -1, $rootScope.nls["ERR030105"], purchaseOrder);
			}
		}
		/*if(code == 0 || code == 15) {
			if (purchaseOrder.supplierReferenceNo != undefined &&purchaseOrder.supplierReferenceNo != null && purchaseOrder.supplierReferenceNo != ""
				&& !CommonValidationService.checkRegExp(purchaseOrder.supplierReferenceNo, appConstant.Reg_Exp_PO_ALPHA_FIFTY)) {
				return this.validationResponse(true, "poSupplierReferenceNo", "tab2", -1, $rootScope.nls["ERR030106"], purchaseOrder);
			}
		}*/
		if(code == 0 || code == 16) {
			if (purchaseOrder.familyProductCode != undefined &&purchaseOrder.familyProductCode != null && purchaseOrder.familyProductCode != ""
				&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_HUNDRED,purchaseOrder.familyProductCode)) {
				return this.validationResponse(true, "poFamilyProductCode", "tab2", -1, $rootScope.nls["ERR030107"], purchaseOrder);
			}
		}
		if(code == 0 || code == 17) {
			if (purchaseOrder.bccEmail != undefined &&purchaseOrder.bccEmail != null && purchaseOrder.bccEmail != "") {
				/*if(!CommonValidationService.checkRegExp(appConstant.alphaNumeric500CharsExpr.purchaseOrder.bccEmail)) {
					return this.validationResponse(true, "poBCeMail", "tab2", -1, $rootScope.nls["ERR030108"], purchaseOrder);
				} else*/
				if(!CommonValidationService.checkMultipleMail(purchaseOrder.bccEmail)){
					return this.validationResponse(true, "poBCeMail", "tab2", -1, $rootScope.nls["ERR030108"], purchaseOrder);
				}
			}
		}
		if(code == 0 || code == 18) {
			if (purchaseOrder.supplierEmail != undefined &&purchaseOrder.supplierEmail != null && purchaseOrder.supplierEmail != "") {
				/*if(!CommonValidationService.checkRegExp(purchaseOrder.supplierEmail, appConstant.alphaNumeric500CharsExpr)) {
					return this.validationResponse(true, "poSupplierMail", "tab2", -1, $rootScope.nls["ERR030109"], purchaseOrder);
				} else*/
				if(!CommonValidationService.checkMultipleMail(purchaseOrder.supplierEmail)){
					return this.validationResponse(true, "poSupplierMail", "tab2", -1, $rootScope.nls["ERR030109"], purchaseOrder);
				}
			}
		}

		if(code == 0 || code == 19) {
			if (purchaseOrder.originAgentEmail != undefined &&purchaseOrder.originAgentEmail != null && purchaseOrder.originAgentEmail != "") {
				/*if(!CommonValidationService.checkRegExp(purchaseOrder.originAgentEmail, appConstant.alphaNumeric500CharsExpr)) {
					return this.validationResponse(true, "poOriginAgenteMail", "tab2", -1, $rootScope.nls["ERR030110"], purchaseOrder);
				} else */
				if(!CommonValidationService.checkMultipleMail(purchaseOrder.originAgentEmail)){
					return this.validationResponse(true, "poOriginAgenteMail", "tab2", -1, $rootScope.nls["ERR030110"], purchaseOrder);
				}
			}
		}

		if(code == 0 || code == 20) {
			if (purchaseOrder.destinationAgentEmail != undefined &&purchaseOrder.destinationAgentEmail != null && purchaseOrder.destinationAgentEmail != "") {
				/*if(!CommonValidationService.checkRegExp(purchaseOrder.destinationAgentEmail, appConstant.alphaNumeric500CharsExpr)) {
					return this.validationResponse(true, "poDestinationAgenteMail", "tab2", -1, $rootScope.nls["ERR030111"], purchaseOrder);
				} else*/ 
				if(!CommonValidationService.checkMultipleMail(purchaseOrder.destinationAgentEmail)){
					return this.validationResponse(true, "poDestinationAgenteMail", "tab2", -1, $rootScope.nls["ERR030111"], purchaseOrder);
				}
			}
		}

		if(code == 0 || code == 21) {
			if (purchaseOrder.processingPersonEmail != undefined &&purchaseOrder.processingPersonEmail != null && purchaseOrder.processingPersonEmail != "") {
				/*if(!CommonValidationService.checkRegExp(purchaseOrder.processingPersonEmail, appConstant.alphaNumeric500CharsExpr)) {
					return this.validationResponse(true, "poProcessingPersoneMail", "tab2", -1, $rootScope.nls["ERR030112"], purchaseOrder);
				} else */
				if(!CommonValidationService.checkMultipleMail(purchaseOrder.processingPersonEmail)){
					return this.validationResponse(true, "poProcessingPersoneMail", "tab2", -1, $rootScope.nls["ERR030112"], purchaseOrder);
				}
			}
		}

		return this.validationSuccesResponse(purchaseOrder);
	}

	this.validateItem = function(code, poItem, index) {
		/*if(CommonValidationService.checkPiecesFormat("10")){
			console.log("Valid");
		} else {
			console.log("InValid");
		}*/
		var errorArr = [];

		if (code == 0 || code == 1) {
            if (poItem.itemNo == undefined || poItem.itemNo == null && poItem.itemNo == "" ) {
            	errorArr.push(this.validationResponse(true, "poItemNo"+ index, "tab1", index, $rootScope.nls["ERR03043"], poItem));
            } else if(!CommonValidationService.checkLength(poItem.itemNo,1,250)){
            	errorArr.push(this.validationResponse(true, "poItemNo"+ index, "tab1", index, $rootScope.nls["ERR03074"], poItem));
            }
        }

		if (code == 0 || code == 2) {
            if (poItem.description == undefined || poItem.description == null || poItem.description == "" ) {
            	errorArr.push(this.validationResponse(true, "poItemDescription"+ index, "tab1", index, $rootScope.nls["ERR03044"], poItem));
            } else if(!CommonValidationService.checkLength(poItem.description,1,4000)){
            	errorArr.push(this.validationResponse(true, "poItemDescription"+ index, "tab1", index, $rootScope.nls["ERR03075"], poItem));
            }
        }
		if (code == 0 || code == 3) {
            if (poItem.noOfPiece == undefined || poItem.noOfPiece == null || poItem.noOfPiece == ""){
            	errorArr.push(this.validationResponse(true, "poItemNoOfPiece"+ index, "tab1", index, $rootScope.nls["ERR03076"], poItem));
            } else if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,poItem.noOfPiece) || !CommonValidationService.checkLength(poItem.noOfPiece,1,999999999)) {
            	errorArr.push(this.validationResponse(true, "poItemNoOfPiece"+ index, "tab1", index, $rootScope.nls["ERR03045"], poItem));
            }
        }
        if (code == 0 || code == 4) {
        	
            if (poItem.packMaster != undefined && poItem.packMaster != null && poItem.packMaster != "" && poItem.packMaster.id != null) {

                if (poItem.packMaster.status != null) {
                    if (ValidateUtil.isStatusBlocked(poItem.packMaster.status)) {
                    	poItem.packMaster = null;
                    	errorArr.push(this.validationResponse(true, "poItemPack"+ index, "tab1", index, $rootScope.nls["ERR03012"], poItem));
                    }
                }
            }
        }
        
		if (code == 0 || code == 5) {
            if (poItem.pieceRCount == undefined || poItem.pieceRCount == null || poItem.pieceRCount == ""){
            	errorArr.push(this.validationResponse(true, "poItemPcsAndCnt"+ index, "tab1", index, $rootScope.nls["ERR03077"], poItem));
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,poItem.pieceRCount) || !CommonValidationService.checkLength(poItem.pieceRCount,1,999999999)) {
            	errorArr.push(this.validationResponse(true, "poItemPcsAndCnt"+ index, "tab1", index, $rootScope.nls["ERR03047"], poItem));
            }
        }
        
		if (code == 0 || code == 6) {
            if (poItem.orderPiece === undefined || poItem.orderPiece === null && poItem.orderPiece == "" ){
            	errorArr.push(this.validationResponse(true, "poItemPieces"+ index, "tab1", index, $rootScope.nls["ERR03078"], poItem));
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,poItem.orderPiece) || !CommonValidationService.checkLength(poItem.orderPiece,1,999999999)) {
            	errorArr.push(this.validationResponse(true, "poItemPieces"+ index, "tab1", index, $rootScope.nls["ERR03048"], poItem));
            }
        }
        
		if (code == 0 || code == 7) {
            if (poItem.orderVolume != undefined && poItem.orderVolume != null && poItem.orderVolume != "" && poItem.orderVolume != null
            	&& !CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM,poItem.orderVolume)){
            	errorArr.push(this.validationResponse(true, "poItemVolume"+ index, "tab1", index, $rootScope.nls["ERR03049"], poItem));
            }
        }
		if (code == 0 || code == 8) {
            if (poItem.orderWeight != undefined && poItem.orderWeight != null && poItem.orderWeight != "" && poItem.orderWeight != null
            	&& !CommonValidationService.checkWeightFormat(poItem.orderWeight)) {
            	errorArr.push(this.validationResponse(true, "poItemWeight"+ index, "tab1", index, $rootScope.nls["ERR03050"], poItem));
            }
        }
		if (code == 0 || code == 9) {
            if (poItem.netWeight != undefined && poItem.netWeight != null && poItem.netWeight != "" && poItem.netWeight != null
            	&& !CommonValidationService.checkWeightFormat(poItem.netWeight)){
            	errorArr.push(this.validationResponse(true, "poItemNetWeight"+ index, "tab1", index, $rootScope.nls["ERR03051"], poItem));
            }
        }
		if (code == 0 || code == 10) {
            if (poItem.amountPerUnit != undefined && poItem.amountPerUnit != null && poItem.amountPerUnit != "" && poItem.amountPerUnit != null
            	&& !CommonValidationService.checkWeightFormat(poItem.amountPerUnit)) {
            	errorArr.push(this.validationResponse(true, "poItemAmountPerUnit"+ index, "tab1", index, $rootScope.nls["ERR03053"], poItem));
            }
        }
		if (code == 0 || code == 10) {
            if (poItem.totalAmount != undefined && poItem.totalAmount != null && poItem.totalAmount != "" && poItem.totalAmount != null
            	&& !CommonValidationService.checkWeightFormat(poItem.totalAmount)){
            	errorArr.push(this.validationResponse(true, "poItemTotalAmount"+ index, "tab1", index, $rootScope.nls["ERR03054"], poItem));
            }
        }
/*
		if (code == 0 || code == 11) {
            if (poItem.pickUpFrom == undefined || poItem.pickUpFrom == null || poItem.pickUpFrom == ""){
            	errorArr.push(this.validationResponse(true, "poItemOriginPickUpFrom"+ index, "tab1", index, $rootScope.nls["ERR03079"], poItem));
            }
        }

		if (code == 0 || code == 12) {
            if (poItem.pickUpWithin == undefined || poItem.pickUpWithin == null || poItem.pickUpWithin == ""){
            	errorArr.push(this.validationResponse(true, "poItemOriginPickUpWithIn"+ index, "tab1", index, $rootScope.nls["ERR03080"], poItem));
            }
        }

		if (code == 0 || code == 11 || code == 12) {
			if (poItem.pickUpFrom != undefined && poItem.pickUpFrom != null && poItem.pickUpFrom != "" && poItem.pickUpWithin != undefined && poItem.pickUpWithin != null && poItem.pickUpWithin != ""){
	
	            if (poItem.pickUpFrom != null && poItem.pickUpWithin != null) {
	                var fromD = $rootScope.convertToDate(poItem.pickUpFrom);
	
	                var toD = $rootScope.convertToDate(poItem.pickUpWithin);
	                if (fromD > toD) {
	                	errorArr.push(this.validationResponse(true, "poItemOriginPickUpWithIn"+ index, "tab1", index, $rootScope.nls["ERR03055"], poItem));
	                }
	
	            }
			}
		}
		if (code == 0 || code == 13) {
            if (poItem.deliveryFrom == undefined || poItem.deliveryFrom == null || poItem.deliveryFrom == ""){
            	errorArr.push(this.validationResponse(true, "poItemFinalDeliveryFrom"+ index, "tab1", index, $rootScope.nls["ERR03081"], poItem));
            }
        }

		if (code == 0 || code == 14) {
            if (poItem.deliveryWithin == undefined || poItem.deliveryWithin == null || poItem.deliveryWithin == ""){
            	errorArr.push(this.validationResponse(true, "poItemFinalDeliveryWithIn"+ index, "tab1", index, $rootScope.nls["ERR03082"], poItem));
            }
        }
		

		if (code == 0 || code == 13 || code == 14) {
			if (poItem.deliveryFrom != undefined && poItem.deliveryFrom != null && poItem.deliveryFrom != "" && poItem.deliveryWithin != undefined && poItem.deliveryWithin != null && poItem.deliveryWithin != ""){
	
	            if (poItem.deliveryFrom != null && poItem.deliveryWithin != null) {
	                var fromD = $rootScope.convertToDate(poItem.deliveryFrom);
	
	                var toD = $rootScope.convertToDate(poItem.deliveryWithin);
	                if (fromD > toD) {
	                	errorArr.push(this.validationResponse(true, "poFinalDeliveryWithIn"+ index, "tab1", index, $rootScope.nls["ERR03056"], poItem));
	                }
	
	            }
			}
		}
		*/

		if (code == 0 || code == 15) {
            if (poItem.qcStatus != undefined && poItem.qcStatus != null && poItem.qcStatus != "" &&
            	!CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_FIFTY,poItem.qcStatus)){
            	errorArr.push(this.validationResponse(true, "poItemQcStatus"+ index, "tab1", index, $rootScope.nls["ERR03057"], poItem));
            }
        }
/*
		if (code == 0 || code == 21) {
            if (poItem.supplierConfDate != undefined && poItem.supplierConfDate != null && poItem.supplierConfDate != ""){
            	errorArr.push(this.validationResponse(true, "poItemConfDate"+ index, "tab1", index, $rootScope.nls["ERR03084"], poItem));
            	
            }else if(!CommonValidationService.checkRegExp(poItem.supplierConfDate, appConstant.alphaNumeric30CharsExpr)) {
            	errorArr.push(this.validationResponse(true, "poItemConfDate"+ index, "tab1", index, $rootScope.nls["ERR03084"], poItem));
            }
        }
*/
		if (code == 0 || code == 16) {
            if (poItem.ref1 != undefined && poItem.ref1 != null && poItem.ref1 != "" &&
            	!CommonValidationService.checkRegExp( appConstant.Reg_Exp_PO_ALPHA_THIRTY,poItem.ref1)){
            	errorArr.push(this.validationResponse(true, "poItemRef1"+ index, "tab1", index, $rootScope.nls["ERR03085"], poItem));
            }
        }


		if (code == 0 || code == 17) {
            if (poItem.poItemRef2 != undefined && poItem.ref2 != null && poItem.ref2 != "" &&
            	!CommonValidationService.checkRegExp( appConstant.Reg_Exp_PO_ALPHA_THIRTY,poItem.ref2)){
            	errorArr.push(this.validationResponse(true, "poItemRef2"+ index, "tab1", index, $rootScope.nls["ERR03086"], poItem));
            }
        }


		if (code == 0 || code == 18) {
            if (poItem.ref3 != undefined && poItem.ref3 != null && poItem.ref3 != "" &&
            	!CommonValidationService.checkRegExp( appConstant.Reg_Exp_PO_ALPHA_THIRTY,poItem.ref3)){
            	errorArr.push(this.validationResponse(true, "poItemRef3"+ index, "tab1", index, $rootScope.nls["ERR03087"], poItem));
            }
        }


		if (code == 0 || code == 19) {
            if (poItem.ref4 != undefined && poItem.ref4 != null && poItem.ref4 != "" &&
            	!CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_THIRTY,poItem.ref4)){
            	errorArr.push(this.validationResponse(true, "poItemRef4"+ index, "tab1", index, $rootScope.nls["ERR03088"], poItem));
            }
        }

		

		if (code == 0 || code == 20) {
            if (poItem.ref5 != undefined && poItem.ref5 != null && poItem.ref5 != "" &&
            	!CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_THIRTY,poItem.ref5)){
            	errorArr.push(this.validationResponse(true, "poItemRef5"+ index, "tab1", index, $rootScope.nls["ERR03089"], poItem));
            }
        }


		//Validate Currency Master
		if(code == 0 || code == 21) {
			if (poItem.currencyMaster == null || poItem.currencyMaster.id == undefined || poItem.currencyMaster.id == null || poItem.currencyMaster.id == "") {
				errorArr.push(this.validationResponse(true, "poItemCurrency"+ index, "tab1", index, $rootScope.nls["ERR03090"], poItem));
			} else if(poItem.currencyMaster.status != null) {
	            if (poItem.currencyMaster.status == "Block") {
	            	poItem.currencyMaster = null;
	            	errorArr.push(this.validationResponse(true, "poItemCurrency"+ index, "tab1", index, $rootScope.nls["ERR03052"], poItem));
	            }
	        }
		}
		if(code == 0 || code == 22) {
			if (poItem.amountPerUnit == null || poItem.amountPerUnit == undefined || poItem.amountPerUnit == null || poItem.amountPerUnit == "") {
				errorArr.push(this.validationResponse(true, "poItemAmountPerUnit"+ index, "tab1", index, $rootScope.nls["ERR03091"], poItem));
			} else if(poItem.amountPerUnit <= 0) {
            	errorArr.push(this.validationResponse(true, "poItemAmountPerUnit"+ index, "tab1", index, $rootScope.nls["ERR03092"], poItem));
	        } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX,poItem.amountPerUnit)){
            	errorArr.push(this.validationResponse(true, "poItemAmountPerUnit"+ index, "tab1", index, $rootScope.nls["ERR03092"], poItem));
            }
		}
		if(code == 0 || code == 23) {
			if (poItem.totalAmount == null || poItem.totalAmount == undefined || poItem.totalAmount == null || poItem.totalAmount == "") {
				errorArr.push(this.validationResponse(true, "poItemTotalAmount"+ index, "tab1", index, $rootScope.nls["ERR03093"], poItem));
			} else if(poItem.totalAmount <= 0) {
            	errorArr.push(this.validationResponse(true, "poItemTotalAmount"+ index, "tab1", index, $rootScope.nls["ERR03094"], poItem));
	        } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AMOUNT_TEN_SIX,poItem.totalAmount)){
            	errorArr.push(this.validationResponse(true, "poItemTotalAmount"+ index, "tab1", index, $rootScope.nls["ERR03094"], poItem));
            }
		}

		return errorArr;
	}
	

	this.validateVehicleInfo = function(code, poVehicleInfo, index) {
		var errorArr = [];

		if (code == 0 || code == 1) {
            if (poVehicleInfo.vehicleNo != undefined && poVehicleInfo.vehicleNo != null && poVehicleInfo.vehicleNo != "" &&
            	!CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_FIFTY,poVehicleInfo.vehicleNo)){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoVehicleNo"+ index, "tab4", index, $rootScope.nls["ERR03057"], poVehicleInfo));
            }
        }
		if (code == 0 || code == 2) {
            if (poVehicleInfo.vehicleType != undefined && poVehicleInfo.vehicleType != null && poVehicleInfo.vehicleType != "" &&
            	!CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_FIFTY,poVehicleInfo.vehicleType)){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoVehicleType"+ index, "tab4", index, $rootScope.nls["ERR03058"], poVehicleInfo));
            }
        }
		if (code == 0 || code == 3) {
            if (poVehicleInfo.driverName != undefined && poVehicleInfo.driverName != null && poVehicleInfo.driverName != "" &&
            	!CommonValidationService.checkRegExp(appConstant.Reg_Exp_PO_ALPHA_HUNDRED,poVehicleInfo.driverName)){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoDriverName"+ index, "tab4", index, $rootScope.nls["ERR03059"], poVehicleInfo));
            }
        }

		if (code == 0 || code == 7) {
            if (poVehicleInfo.pickupDate == undefined || poVehicleInfo.pickupDate == null || poVehicleInfo.pickupDate == ""){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoPickUpDate"+ index, "tab4", index, $rootScope.nls["ERR03060"], poVehicleInfo));
            }
        }

		if (code == 0 || code == 8) {
            if (poVehicleInfo.deliveryDate == undefined || poVehicleInfo.deliveryDate == null || poVehicleInfo.deliveryDate == ""){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoDeliveryDate"+ index, "tab4", index, $rootScope.nls["ERR03061"], poVehicleInfo));
            }
        }
		

		if (code == 0 || code == 8 || code == 9) {
			if (poVehicleInfo.pickupDate != undefined && poVehicleInfo.pickupDate != null && poVehicleInfo.pickupDate != "" && poVehicleInfo.deliveryDate != undefined && poVehicleInfo.deliveryDate != null && poVehicleInfo.deliveryDate != ""){
	
	            if (poVehicleInfo.pickupDate != null && poVehicleInfo.deliveryDate != null) {
	                var fromD = $rootScope.convertToDate(poVehicleInfo.pickupDate);
	
	                var toD = $rootScope.convertToDate(poVehicleInfo.deliveryDate);
	                if (fromD > toD) {
	                	errorArr.push(this.validationResponse(true, "poVehicleInfoDeliveryDate"+ index, "tab4", index, $rootScope.nls["ERR03062"], poVehicleInfo));
	                }
	
	            }
			}
		}
		if (code == 0 || code == 4) {
            if (poVehicleInfo.noOfPiece === undefined || poVehicleInfo.noOfPiece == null || poVehicleInfo.noOfPiece == ""){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoNoOfPiece"+ index, "tab4", index, $rootScope.nls["ERR03095"], poVehicleInfo));
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,poVehicleInfo.noOfPiece) || !CommonValidationService.checkLength(poVehicleInfo.noOfPiece,1,999999999)) {
            	errorArr.push(this.validationResponse(true, "poVehicleInfoNoOfPiece"+ index, "tab4", index, $rootScope.nls["ERR03063"], poVehicleInfo));
            }
        }
		if (code == 0 || code == 5) {
            if (poVehicleInfo.netWeightKg === undefined || poVehicleInfo.netWeightKg === null || poVehicleInfo.netWeightKg == ""){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoNetWeightKg"+ index, "tab4", index, $rootScope.nls["ERR03096"], poVehicleInfo));
            } else if (!CommonValidationService.checkWeightFormat(poVehicleInfo.netWeightKg)) {
            	errorArr.push(this.validationResponse(true, "poVehicleInfoNetWeightKg"+ index, "tab4", index, $rootScope.nls["ERR03064"], poVehicleInfo));
            }
        }
		if (code == 0 || code == 6) {
            if (poVehicleInfo.volumeCBM === undefined || poVehicleInfo.volumeCBM === null || poVehicleInfo.volumeCBM == ""){
            	errorArr.push(this.validationResponse(true, "poVehicleInfoVolumeCBM"+ index, "tab4", index, $rootScope.nls["ERR03097"], poVehicleInfo));
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM,poVehicleInfo.volumeCBM)) {
            	errorArr.push(this.validationResponse(true, "poVehicleInfoVolumeCBM"+ index, "tab4", index, $rootScope.nls["ERR03057"], poVehicleInfo));
            }
        }
		
        return errorArr;
	}

	this.validateRemarkFollow = function(code, poRemarkFollow, index) {
		var errorArr = [];

		if (code == 0 || code == 1) {
            if (poRemarkFollow.date == undefined || poRemarkFollow.date == null || poRemarkFollow.date == ""){
            	errorArr.push(this.validationResponse(true, "poRemarkFollowDate"+ index, "tab5", index, $rootScope.nls["ERR03099"], poRemarkFollow));
            }
        }
		if (code == 0 || code == 2) {
            if (poRemarkFollow.type == undefined || poRemarkFollow.type == null || poRemarkFollow.type == ""){
            	errorArr.push(this.validationResponse(true, "poRemarkFollowUpType"+ index, "tab5", index, $rootScope.nls["ERR03098"], poRemarkFollow));
            }
        }
		if (code == 0 || code == 3) {
			if (poRemarkFollow.followDate == undefined || poRemarkFollow.followDate == null || poRemarkFollow.followDate == ""){
				errorArr.push(this.validationResponse(true, "poRemarkFollowUpDate"+ index, "tab5", index, $rootScope.nls["ERR030100"], poRemarkFollow));
			}
		}
 
		if (code == 0 || code == 4) {
            if (poRemarkFollow.remark != undefined && poRemarkFollow.remark != null && poRemarkFollow.remark != "" &&
            	!CommonValidationService.checkLength(poRemarkFollow.remark,1,4000)){
            	errorArr.push(this.validationResponse(true, "poRemarkFollowUpRemark"+ index, "tab5", index, $rootScope.nls["ERR03069"], poRemarkFollow));
            }
        }
        return errorArr;
	}
	
	this.validationSuccesResponse = function(purchaseOrder) {
	    return {error : false, obj : purchaseOrder};
	}
	/*this.validationResponse = function(err, elem, message, purchaseOrder) {
		return {error : err, errElement : elem, errMessage : message, obj : purchaseOrder};
	}*/
	this.validationResponse = function(err, elem, tab, index, message, purchaseOrder) {
		return {error : err, errElement : elem, tab : tab, index :index, errMessage : message, obj : purchaseOrder};
	}
	
});