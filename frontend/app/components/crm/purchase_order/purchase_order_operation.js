app.controller('PurchaseOrderEditCtrl',['$rootScope', '$scope', '$location', '$window', 'ngDialog', '$timeout',
    '$state', '$stateParams', '$modal', 'POValidationService', 'CommonValidationService', 'PODataConversionService', 
    'cloneService', 'RecentHistorySaveService', 'purchaseOrderFactory', 'buyerConsolidationFactory',
    'PartiesList', 'PortByTransportMode', 'TosSearchKeyword', 'PackList', 'CurrencyMasterSearchKeyword', 'roleConstant',
    function($rootScope, $scope, $location, $window, ngDialog, $timeout,
    $state, $stateParams, $modal, POValidationService, CommonValidationService, PODataConversionService, 
    cloneService, RecentHistorySaveService, purchaseOrderFactory, buyerConsolidationFactory,
    PartiesList, PortByTransportMode, TosSearchKeyword, PackList, CurrencyMasterSearchKeyword, roleConstant) {
    $scope.$roleConstant = roleConstant;
    $scope.errorMap = new Map();
    $scope.disableSubmitBtn = false;

    $scope.diasbleSubmitBtnFn = function() {
        $scope.disableSubmitBtn = true;
        $timeout(function() {
            $scope.disableSubmitBtn = false;
        }, 2000);
    }

    $scope.submitPO = function(isUpdate) {
        $scope.diasbleSubmitBtnFn();
        var validationResponse = POValidationService.validate($scope.purchaseOrder, 0);
        console.log("Validation Response -- ", validationResponse);
        if (validationResponse.error == true) {
            console.log("Validation Response 2-- ", validationResponse);
            $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
        } else {
            var childError = false;
            console.log("1 - ", childError);
            if ($scope.validateItem(true)) {
                childError = true;
            }
            console.log("2 - ", childError);
            if (!childError && $scope.validateVehicleInfo(true)) {
                childError = true;
            }
            if (!childError && $scope.validateRemarkFollow(true)) {
                childError = true;
            }
            if (!childError) {
                console.log("3 - ", childError);
                $scope.tmpObj = cloneService.clone($scope.purchaseOrder);
                $scope.tmpObj = PODataConversionService.convertObjectToData($scope.tmpObj);
                if (isUpdate) {
                    purchaseOrderFactory.common.update($scope.tmpObj).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.showResponse(data.responseObject, false);

                        } else {
                            $scope.addEmptyRows();
                            console.log("PO Update Failed " + data.responseDescription)
                        }

                    }, function(error) {
                        $scope.addEmptyRows();
                        console.log("PO Update Failed : " + error)
                    });

                } else {
                    purchaseOrderFactory.common.create($scope.tmpObj).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.showResponse(data.responseObject, true);
                        } else {
                            $scope.addEmptyRows();
                            console.log("PO added Failed " + data.responseDescription)
                        }

                    }, function(error) {
                        $scope.addEmptyRows();
                        console.log("PO added Failed : " + error)
                    });
                }

            }
        }
    }



    $scope.showResponse = function(po, isAdded) {
        var newScope = $scope.$new();
        if (po != null) {
            newScope.poNo = po.poNo;
            newScope.opt = isAdded == true ? 'saved' : 'updated';
            ngDialog.openConfirm({
                template: '<p>Purchase order  {{poNo}} successfully {{opt}}.</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok </button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default top_9999'
            }).then(function(value) {
                $state.go('layout.PurchaseOrder', {
                    submitAction: "Saved"
                });
            }, function(value) {
                $state.go('layout.PurchaseOrder', {
                    submitAction: "Saved"
                });
            });
        } else {
            $state.go('layout.PurchaseOrder', {
                submitAction: "Saved"
            });
        }

    }

    $scope.cancel = function() {
        $state.go('layout.PurchaseOrder', {
            submitAction: "Cancelled"
        });
    };

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }
    $scope.editmorepopup = function(index) {
        $scope.selectedItemIdx = index;
        console.log("Date ", $scope.purchaseOrder.purchaseOrderItemList[$scope.selectedItemIdx].supplierConfDate);
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/purchase_order/views/moreinfo_edit_popup.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
    };
    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";

    $scope.attEdit = true;

    $scope.attachConfig = {
        "isEdit": true, // false for view only mode
        "isDelete": true, //
        "page": "noDoc",
        "columnDefs": [{
            "name": "Document Name",
            "model": "documentMaster.documentName",
            "type": "text"
        }, {
            "name": "Reference No",
            "model": "refNo",
            "type": "text"
        }, {
            "name": "Customs",
            "model": "customs",
            "type": "text"
        }, {
            "name": "Unprotected File Name",
            "model": "unProcFile",
            "type": "file"
        }, {
            "name": "Protected File Name",
            "model": "procFile",
            "type": "file"
        }, {
            "name": "Actions",
            "model": "action",
            "type": "action"
        }]
    }



    $scope.clickOnTabView = function(tab) {
        if (tab == 'items') {
            $scope.Tabs = tab;
        }
        if (tab == 'attachments') {
            $scope.Tabs = tab;
        }
        if (tab == 'vehicleInfo') {
            $scope.Tabs = tab;
        }
        if (tab == 'remarksFollowUp') {
            $scope.Tabs = tab;
        }
        if ($scope.purchaseOrder.id != null && $scope.purchaseOrder.id != undefined) {
            if (tab == 'others' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_OTHERS_MODIFY)) {
                $scope.Tabs = tab;
            }
            if (tab == 'vehicleInfo') {
                $scope.Tabs = tab;

                if ($scope.purchaseOrder.purchaseOrderVehicleInfoList != undefined && $scope.purchaseOrder.purchaseOrderVehicleInfoList.length != 0) {

                    var editRoleForVehicle = $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_VEHICLE_INFO_MODIFY);

                    for (var i = 0; i < $scope.purchaseOrder.purchaseOrderVehicleInfoList.length; i++) {

                        if ($scope.purchaseOrder.purchaseOrderVehicleInfoList[i].id != null) {
                            if (editRoleForVehicle) {
                                $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].checkRolesToDisable = false;
                            } else {
                                $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].checkRolesToDisable = true;
                            }
                        }
                    }

                }

            }
            if (tab == 'remarksFollowUp') {
                $scope.Tabs = tab;

                var editRoleForRemarks = $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_REMARKS_FOLLOW_UP_MODIFY);

                for (var i = 0; i < $scope.purchaseOrder.purchaseOrderRemarkFollowUpList.length; i++) {

                    if ($scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].id != null) {
                        if (editRoleForRemarks) {
                            $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].checkRolesToDisable = false;
                        } else {
                            $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].checkRolesToDisable = true;
                        }
                    }
                }

            }
        } else {
            if (tab == 'others' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_OTHERS_CREATE)) {
                $scope.Tabs = tab;
            }
        }
    }

    // Buyer's Consolidation - dropdown starts Here
    $scope.ajaxBuyerEvent = function(keyword) {
        console.log("$scope.searchBuyer -- ", $scope.searchBuyer);
        $scope.searchBuyer = {};
        $scope.searchBuyer.keyword = keyword == null ? "" : keyword.trim();
        $scope.searchBuyer.selectedPageNumber = 0;
        $scope.searchBuyer.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return buyerConsolidationFactory.search.keyword($scope.searchBuyer).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                } else {
                    console.log("added Failed ", data.responseDescription)
                }
            },
            function(error) {
                console.log("added Failed : ", error)
            });
    }

    $scope.validateFocusAfterSelect = function(obj, code, sameFieldId, nextField) {
        var respError = POValidationService.validate(obj, code);
        if (respError.error) {
            $scope.validationErrorAndFocus(respError, sameFieldId);
        } else {
            $scope.validationErrorAndFocus(respError, nextField);
        }

    }

    $scope.selectedBuyer = function(elemId) {
        console.log("selectedBuyer Function Called ", elemId);
        $scope.validateFocusAfterSelect($scope.purchaseOrder, 1, "bcMaster", elemId);
    }

    $scope.buyerRender = function(item) {
        return {
            label: item.bcName,
            item: item
        }
    }
    // Buyer's Consolidation - dropdown starts Here


    //Party Master Lov - Starts Here
    $scope.ajaxPartyEvent = function(object) {
        console.log("ajaxCustomerEvent is called", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PartiesList.query($scope.searchDto).$promise.then(
            function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            });
    }

    $scope.focusFieldAfter2Sec = function(fieldName) {

        $timeout(function() {
            $rootScope.navigateToNextField(fieldName);
        }, 3000);
    }
    $scope.partyMasterActionResponseFn = function() {
        if ($scope.purchaseOrder != null && $stateParams.forObj != undefined && $stateParams.forObj != null) {
            var isNaviPartyMasterEmpty = true;
            if ($rootScope.naviPartyMaster != undefined && $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != null) {
                isNaviPartyMasterEmpty = false;
            }
            if ($stateParams.forObj == "purchaseOrder.supplier") {
                if (isNaviPartyMasterEmpty) {
                    //If initialize required means initialize party object here
                } else {
                    $scope.purchaseOrder.supplier = $rootScope.naviPartyMaster;
                }
                //$scope.focusFieldAfter2Sec("");
            } else if ($stateParams.forObj == "purchaseOrder.originAgent") {
                if (isNaviPartyMasterEmpty) {
                    //If initialize required means initialize party object here
                } else {
                    $scope.purchaseOrder.originAgent = $rootScope.naviPartyMaster;
                }
                //$scope.focusFieldAfter2Sec("");
            } else if ($stateParams.forObj == "purchaseOrder.destinationAgent") {
                if (isNaviPartyMasterEmpty) {
                    //If initialize required means initialize party object here
                } else {
                    $scope.purchaseOrder.destinationAgent = $rootScope.naviPartyMaster;
                }
                //$scope.focusFieldAfter2Sec("");
            }
        }
    }
    $scope.routePartyEvent = function(fromObjModel, isAdd, partyMaster) {
        $rootScope.nav_src_bkref_key = new Date().toISOString();
        if (isAdd) {
            $state.go("layout.addParty", {
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                forObj: fromObjModel
            });
        } else {
            if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                $rootScope.nav_src_bkref_key = new Date().toISOString();
                var stateRouteParam = {
                    partyId: partyMaster.id,
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    forObj: fromObjModel
                };
                $state.go("layout.editParty", stateRouteParam);
            } else {
                console.log("Party not selected...");
            }
        }
    }

    $scope.selectedParty = function(elemId) {
        $scope.validationErrorAndFocus(POValidationService.validate($scope.purchaseOrder, 2), elemId);
    }
    $scope.selectedOriginAgent = function(elemId) {
        $scope.validationErrorAndFocus(POValidationService.validate($scope.purchaseOrder, 3), elemId);
    }
    $scope.selectedDestinationAgent = function(elemId) {
        $scope.validationErrorAndFocus(POValidationService.validate($scope.purchaseOrder, 4), elemId);
    }

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }
    }

    //Party Master Lov - Ends Here
    //Port Master Lov - starts  here

    $scope.selectedDestination = function(elemId) {
        $scope.validateFocusAfterSelect($scope.purchaseOrder, 6, "poDestination", elemId);
    };

    $scope.selectedOrigin = function(elemId) {
        $scope.validateFocusAfterSelect($scope.purchaseOrder, 5, "poOrigin", elemId);
    };

    $scope.ajaxPortEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.originList = [];
                    var resultList = data.responseObject.searchResult;
                    if (resultList != null && resultList.length != 0) {
                        for (var i = 0; i < resultList.length; i++) {
                            $scope.originList.push(resultList[i]);
                        }
                    }
                    return $scope.originList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Port Master List');
            });
    }


    $scope.portRender = function(item) {
        return {
            label: item.portName,
            item: item
        }

    }
    //Port Master Lov - ends  here

    //Terms Of Shipment Master Lov - starts  here
    $scope.ajaxTosEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
            if (data.responseCode == "ERR0") {
                $scope.tosList = data.responseObject.searchResult;
                return $scope.tosList;
            }
        }, function(errResponse) {
            console.error('Error while fetching Tos');
        });
    }

    $scope.selectedTos = function(elemId) {
        console.log("$scope.Tabs", $scope.Tabs)
        if ($scope.Tabs === "tab1") {
            elemId = "poItemNo0";
        } else if ($scope.Tabs === "tab2") {
            elemId = "poSupplierReferenceNo";
        } else if ($scope.Tabs === "tab3") {
            elemId = "poAttachmentdateFormat";
        } else if ($scope.Tabs === "tab4") {
            elemId = "poVehicleInfoVehicleNo0";
        } else if ($scope.Tabs === "tab5") {
            elemId = "poRemarkFollowUpDate0";
        }
        $scope.validateFocusAfterSelect($scope.purchaseOrder, 7, "poTos", elemId);
    };

    $scope.tosRender = function(item) {
        return {
            label: item.tosName,
            item: item
        }
    }
    //Terms Of Shipment Master Lov - ends  here

    //Pack Master Lov - Starts Here

    $scope.ajaxPackEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PackList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.packList = data.responseObject.searchResult;
                    return $scope.packList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Pack List');
            }
        );
    }

    $scope.selectedPack = function(poItem, index, elemId) {
        //$scope.validationErrorAndFocus(POValidationService.validateItem(1, poItem, index), elemId);
        $scope.navigateToNextField(elemId + index);
    };

    $scope.packRender = function(item) {
        return {
            label: item.packName,
            item: item
        }
    }
    //Pack Master Lov - Ends Here

    //Currency Master Lov - Starts Here

    $scope.ajaxCurrencyMasterEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.currencyList = data.responseObject.searchResult;
                    return $scope.currencyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Pack List');
            }
        );
    }

    $scope.selectedCurrencyMaster = function(poItem, index, elemId) {
        $scope.navigateToNextField(elemId + index);
    };

    $scope.currencyRender = function(item) {
        return {
            label: item.currencyCode,
            item: item
        }
    }
    //Currency Master Lov - Starts Here

    //Input Focus Navigation - Starts Here

    $scope.actAfterFiveHunMilliSec = function(id) {
        $timeout(function() {
            $scope.navigateToNextField(id);
        }, 500);
    }

    $scope.navigateToNextField = function(id) {
        var focElement = document.getElementById(id);
        console.log("id - ", id, " - ", focElement);
        if (focElement != null) {
            focElement.focus();
        }
    }
    //Input Focus Navigation - Ends Here

    //Common Validation Focus Functionality - starts here
    $scope.validationErrorAndFocus = function(valResponse, elemId) {
        $scope.errorMap = new Map();
        if (valResponse.error == true) {
            if (valResponse.tab == "none") {
                $scope.purchaseOrder = valResponse.obj;
            } else if (valResponse.tab == "tab1") {
                $scope.purchaseOrder.purchaseOrderItemList[valResponse.index] = valResponse.obj;
                $scope.Tabs = "tab1";
            }
            $scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
            console.log("$scope.valResponse -- ", valResponse);
            console.log("$scope.errorMap -- ", $scope.errorMap);
        }
        $scope.navigateToNextField(elemId);
    }

    //show errors

    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });

    var errorOnRowIndex = null;

    $scope.errorShow = function(errorArr, index) {
        $scope.errList = errorArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };
    $scope.validateItem = function(isSubmit) {
        var isError = false,
            sliceArrIndx = [];


        for (var i = 0; i < $scope.purchaseOrder.purchaseOrderItemList.length; i++) {
            if (CommonValidationService.checkObjectIsEmpty($scope.purchaseOrder.purchaseOrderItemList[i])) {
                if (isSubmit) {
                    sliceArrIndx.push(i);
                } else {
                    $scope.purchaseOrder.purchaseOrderItemList[i].error = true;
                    $scope.purchaseOrder.purchaseOrderItemList[i].errorList = [$rootScope.nls["ERR03043"], $rootScope.nls["ERR03044"], $rootScope.nls["ERR03076"], $rootScope.nls["ERR03077"], $rootScope.nls["ERR03078"], $rootScope.nls["ERR03079"], $rootScope.nls["ERR03080"], $rootScope.nls["ERR03081"], $rootScope.nls["ERR03082"], $rootScope.nls["ERR03057"], $rootScope.nls["ERR03090"], $rootScope.nls["ERR03043"], $rootScope.nls["ERR03044"], $rootScope.nls["ERR03076"], $rootScope.nls["ERR03077"], $rootScope.nls["ERR03078"], $rootScope.nls["ERR03079"], $rootScope.nls["ERR03080"], $rootScope.nls["ERR03081"], $rootScope.nls["ERR03082"], $rootScope.nls["ERR03084"], $rootScope.nls["ERR03090"]];

                    $scope.errorMap.put("poItemNo" + i, $rootScope.nls["ERR03043"]);
                    $scope.errorMap.put("poItemDescription" + i, $rootScope.nls["ERR03044"]);
                    $scope.errorMap.put("poItemNoOfPiece" + i, $rootScope.nls["ERR03076"]);
                    $scope.errorMap.put("poItemPcsAndCnt" + i, $rootScope.nls["ERR03077"]);
                    $scope.errorMap.put("poItemPieces" + i, $rootScope.nls["ERR03078"]);
                    $scope.errorMap.put("poItemOriginPickUpFrom" + i, $rootScope.nls["ERR03079"]);
                    $scope.errorMap.put("poItemOriginPickUpWithIn" + i, $rootScope.nls["ERR03080"]);
                    $scope.errorMap.put("poItemFinalDeliveryFrom" + i, $rootScope.nls["ERR03081"]);
                    $scope.errorMap.put("poItemFinalDeliveryWithIn" + i, $rootScope.nls["ERR03082"]);
                    $scope.errorMap.put("poItemConfDate" + i, $rootScope.nls["ERR03084"]);
                    $scope.errorMap.put("poItemCurrency" + i, $rootScope.nls["ERR03090"]);
                    $scope.errorMap.put("poItemAmountPerUnit" + i, $rootScope.nls["ERR03091"]);
                    $scope.errorMap.put("poItemTotalAmount" + i, $rootScope.nls["ERR03093"]);
                    isError = true;
                }
            } else {
                console.log("NonEmpty Object");
                var validationResponse = POValidationService.validateItem(0, $scope.purchaseOrder.purchaseOrderItemList[i], i);
                $scope.purchaseOrder.purchaseOrderItemList[i].error = false;
                $scope.purchaseOrder.purchaseOrderItemList[i].errorList = [];
                if (validationResponse instanceof Array) {
                    for (var j = 0; j < validationResponse.length; j++) {
                        if (validationResponse[j].error != undefined && validationResponse[j].error == true) {
                            $scope.purchaseOrder.purchaseOrderItemList[i].error = true;
                            console.log("validationResponse[j].errMessage ------------ ", validationResponse[j].errMessage);
                            $scope.purchaseOrder.purchaseOrderItemList[i].errorList.push(validationResponse[j].errMessage);
                            isError = true;
                            console.log("Chk in isError -- ", isError);
                            $scope.errorMap.put(validationResponse[j].errElement, "Error");
                        }
                    }
                }
            }
        }
        if (isSubmit && sliceArrIndx.length > 0) {
            for (var i = 0; i < sliceArrIndx.length; i++) {
                $scope.purchaseOrder.purchaseOrderItemList.splice(sliceArrIndx[i], 1);
            }
        }
        console.log("Chk in isError -- ", isError);
        return isError;
    }

    $scope.validateVehicleInfo = function(isSubmit) {
        var isError = false,
            sliceArrIndx = [];

        for (var i = 0; i < $scope.purchaseOrder.purchaseOrderVehicleInfoList.length; i++) {

            if (CommonValidationService.checkObjectIsEmpty($scope.purchaseOrder.purchaseOrderVehicleInfoList[i])) {
                if (isSubmit) {
                    sliceArrIndx.push(i);
                } else {
                    $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].error = true;
                    $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].errorList = [$rootScope.nls["ERR03060"], $rootScope.nls["ERR03061"], $rootScope.nls["ERR03095"], $rootScope.nls["ERR03096"], $rootScope.nls["ERR03097"]];


                    $scope.errorMap.put("poVehicleInfoPickUpDate" + i, $rootScope.nls["ERR03060"]);
                    $scope.errorMap.put("poVehicleInfoDeliveryDate" + i, $rootScope.nls["ERR03061"]);
                    $scope.errorMap.put("poVehicleInfoNoOfPiece" + i, $rootScope.nls["ERR03095"]);
                    $scope.errorMap.put("poVehicleInfoNetWeightKg" + i, $rootScope.nls["ERR03096"]);
                    $scope.errorMap.put("poVehicleInfoVolumeCBM" + i, $rootScope.nls["ERR03097"]);

                    isError = true;
                }
            } else {
                var validationResponse = POValidationService.validateVehicleInfo(0, $scope.purchaseOrder.purchaseOrderVehicleInfoList[i], i);
                $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].error = false;
                $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].errorList = [];
                if (validationResponse instanceof Array) {
                    for (var j = 0; j < validationResponse.length; j++) {
                        if (validationResponse[j].error != undefined && validationResponse[j].error == true) {
                            $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].error = true;
                            $scope.purchaseOrder.purchaseOrderVehicleInfoList[i].errorList.push(validationResponse[j].errMessage);
                            isError = true;
                            console.log("Chk in isError -- ", isError);
                            $scope.errorMap.put(validationResponse[j].errElement, "Error");
                        }
                    }
                }
            }
        }

        if (isSubmit && sliceArrIndx.length > 0) {
            for (var i = 0; i < sliceArrIndx.length; i++) {
                $scope.purchaseOrder.purchaseOrderVehicleInfoList.splice(sliceArrIndx[i], 1);
            }
        }
        console.log("Chk in isError -- ", isError);
        return isError;
    }

    $scope.validateRemarkFollow = function(isSubmit) {
        var isError = false,
            sliceArrIndx = [];

        for (var i = 0; i < $scope.purchaseOrder.purchaseOrderRemarkFollowUpList.length; i++) {

            if (CommonValidationService.checkObjectIsEmpty($scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i])) {
                if (isSubmit) {
                    sliceArrIndx.push(i);
                } else {
                    $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].error = true;
                    $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].errorList = [$rootScope.nls["ERR03099"], $rootScope.nls["ERR03098"], $rootScope.nls["ERR030100"]];

                    $scope.errorMap.put("poRemarkFollowDate" + i, $rootScope.nls["ERR03099"]);
                    $scope.errorMap.put("poRemarkFollowUpType" + i, $rootScope.nls["ERR03098"]);
                    $scope.errorMap.put("poRemarkFollowUpDate" + i, $rootScope.nls["ERR030100"]);

                    isError = true;
                }
            } else {
                var validationResponse = POValidationService.validateRemarkFollow(0, $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i], i);
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].error = false;
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].errorList = [];
                if (validationResponse instanceof Array) {
                    for (var j = 0; j < validationResponse.length; j++) {
                        if (validationResponse[j].error != undefined && validationResponse[j].error == true) {
                            $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].error = true;
                            $scope.purchaseOrder.purchaseOrderRemarkFollowUpList[i].errorList.push(validationResponse[j].errMessage);
                            isError = true;
                            console.log("Chk in isError -- ", isError);
                            $scope.errorMap.put(validationResponse[j].errElement, "Error");
                        }
                    }
                }
            }
        }
        console.log("Chk in isError -- ", isError);


        if (isSubmit && sliceArrIndx.length > 0) {
            for (var i = 0; i < sliceArrIndx.length; i++) {
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList.splice(sliceArrIndx[i], 1);
            }
        }
        return isError;
    }

    $scope.removeItem = function(index) {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_ITEMS_DELETE)) {
            $scope.errorMap = new Map();
            if ($scope.purchaseOrder.purchaseOrderItemList.length > 1) {
                $scope.purchaseOrder.purchaseOrderItemList.splice(index, 1);
            }
        }
    }
    $scope.removeVehicle = function(index) {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_VEHICLE_INFO_DELETE)) {
            $scope.errorMap = new Map();
            if ($scope.purchaseOrder.purchaseOrderVehicleInfoList.length > 1) {
                $scope.purchaseOrder.purchaseOrderVehicleInfoList.splice(index, 1);
            }
        }
    }
    $scope.removeRemark = function(index) {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_REMARKS_FOLLOW_UP_DELETE)) {
            $scope.errorMap = new Map();
            if ($scope.purchaseOrder.purchaseOrderRemarkFollowUpList.length > 1) {
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList.splice(index, 1);
            }
        }
    }

    //Common Validation Focus Functionality - ends here
    $scope.addNewItem = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_ITEMS_CREATE)) {
            $scope.errorMap = new Map();
            console.log("Date ", $scope.purchaseOrder.purchaseOrderItemList[0].supplierConfDate);
            if (!$scope.validateItem(false)) {
                $scope.purchaseOrder.purchaseOrderItemList.push({});
            }
        }
    }
    $scope.addNewVehicle = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_VEHICLE_INFO_CREATE)) {
            $scope.errorMap = new Map();

            if (!$scope.validateVehicleInfo(false)) {
                $scope.purchaseOrder.purchaseOrderVehicleInfoList.push({});
            }
        }
    }
    $scope.addNewRemark = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_REMARKS_FOLLOW_UP_CREATE)) {
            $scope.errorMap = new Map();

            if (!$scope.validateRemarkFollow(false)) {
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList.push({});
            }
        }
    }
    $scope.addInit = function() {

        $scope.purchaseOrder = {};
        $scope.purchaseOrder.status = "Active";

        $scope.purchaseOrder.transportMode = "Air";
        $scope.purchaseOrder.consolRdirect = false;
        $scope.purchaseOrder.companyMaster = $rootScope.userProfile.selectedCompany;
        $scope.purchaseOrder.locationMaster = $rootScope.userProfile.selectedUserLocation;
        $scope.purchaseOrder.poDate = $rootScope.dateToString(new Date());
        $scope.addEmptyRows();
        console.log("$scope.purchaseOrder -- ", $scope.purchaseOrder);
    }


    $scope.editInit = function(id) {

        console.log("Edit Init method is called ", id);

        purchaseOrderFactory.common.findById({
            id: id
        }).$promise.then(function(data) {

            if (data.responseCode == 'ERR0') {
                console.log("Server Message : ", data.responseDescription);

                $scope.purchaseOrder = PODataConversionService.convertToEditData(data.responseObject);
                $scope.addEmptyRows();
                console.log("Purchase Order ", $scope.purchaseOrder);
                if ($scope.purchaseOrder != undefined && $scope.purchaseOrder != null) {
                    var editRoleForItems = $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_ITEMS_MODIFY);
                    for (var i = 0; i < $scope.purchaseOrder.purchaseOrderItemList.length; i++) {
                        if ($scope.purchaseOrder.purchaseOrderItemList[i].id != null) {
                            if (editRoleForItems) {
                                $scope.purchaseOrder.purchaseOrderItemList[i].checkRolesToDisable = false;
                            } else {
                                $scope.purchaseOrder.purchaseOrderItemList[i].checkRolesToDisable = true;
                            }

                        }
                    }
                }
                if ($scope.purchaseOrder != undefined && $scope.purchaseOrder != null && $scope.purchaseOrder.buyer != undefined && $scope.purchaseOrder.buyer != null &&
                    $scope.purchaseOrder.buyer.bcName != undefined && $scope.purchaseOrder.buyer.bcName != null) {
                    $rootScope.subTitle = $scope.purchaseOrder.buyer.bcName;
                } else {
                    $rootScope.subTitle = "Unknown Buyer"
                }
                var rHistoryObj = {
                    'title': "PurchaseOrder Edit # " + $scope.purchaseOrder.poNo,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'PurchaseOrder',
                    'serviceType': 'AIR',
                    'showService': false
                }

                RecentHistorySaveService.form(rHistoryObj);
            } else {
                console.log("Exception while getting Purchase Order by Id ", data.responseDescription);
            }

        }, function(error) {
            console.log("Error ", error);
        });

    };

    $scope.addEmptyRows = function() {
        if ($scope.purchaseOrder != undefined && $scope.purchaseOrder != null) {
            if ($scope.purchaseOrder.purchaseOrderItemList == undefined || $scope.purchaseOrder.purchaseOrderItemList == null ||
                $scope.purchaseOrder.purchaseOrderItemList.length == 0) {
                $scope.purchaseOrder.purchaseOrderItemList = [{}];
            }
            if ($scope.purchaseOrder.purchaseOrderVehicleInfoList == undefined || $scope.purchaseOrder.purchaseOrderVehicleInfoList == null ||
                $scope.purchaseOrder.purchaseOrderVehicleInfoList.length == 0) {
                $scope.purchaseOrder.purchaseOrderVehicleInfoList = [{}];
            }
            if ($scope.purchaseOrder.purchaseOrderRemarkFollowUpList == undefined || $scope.purchaseOrder.purchaseOrderRemarkFollowUpList == null ||
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList.length == 0) {
                $scope.purchaseOrder.purchaseOrderRemarkFollowUpList = [{}];
            }
        }

    }


    //On leave the Unfilled Enquiry Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js
    //This block is listener for Add Sales Enquiry Event
    /*
    		'data' : $rootScope.unfinishedData,
    		'title' : $rootScope.unfinishedFormTitle,*/
    $scope.$on('addPurchaseOrderEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.purchaseOrder;
        $rootScope.category = "PurchaseOrder";
        $rootScope.unfinishedFormTitle = "Purchase Order (New)";
        if ($scope.purchaseOrder != undefined && $scope.purchaseOrder != null && $scope.purchaseOrder.buyer != undefined && $scope.purchaseOrder.buyer != null &&
            $scope.purchaseOrder.buyer.bcName != undefined && $scope.purchaseOrder.buyer.bcName != null) {
            $rootScope.subTitle = $scope.purchaseOrder.buyer.bcName;
        } else {
            $rootScope.subTitle = "Unknown Buyer"
        }
    });

    $scope.$on('editPurchaseOrderEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.purchaseOrder;
        $rootScope.category = "PurchaseOrder";
        $rootScope.unfinishedFormTitle = "PurchaseOrder Edit # " + $scope.purchaseOrder.poNo;
        if ($scope.purchaseOrder != undefined && $scope.purchaseOrder != null && $scope.purchaseOrder.buyer != undefined && $scope.purchaseOrder.buyer != null &&
            $scope.purchaseOrder.buyer.bcName != undefined && $scope.purchaseOrder.buyer.bcName != null) {
            $rootScope.subTitle = $scope.purchaseOrder.buyer.bcName;
        } else {
            $rootScope.subTitle = "Unknown Buyer"
        }
    });

    $scope.$on('addPurchaseOrderEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify(PODataConversionService.convertObjectToData($scope.purchaseOrder));
        localStorage.isPOReloaded = "YES";
        e.preventDefault();
    });

    $scope.$on('editPurchaseOrderEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify(PODataConversionService.convertObjectToData($scope.purchaseOrder));
        localStorage.isPOReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isPOReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPOReloaded = "NO";
                $scope.purchaseOrder = JSON.parse(localStorage.reloadFormData);
                //default focus for this page 
            } else {
                $scope.purchaseOrder = $rootScope.selectedUnfilledFormData;
                $scope.partyMasterActionResponseFn();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPOReloaded = "NO";
                $scope.purchaseOrder = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.addInit();
            }
        }

        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.PurchaseOrder"
            },
            {
                label: "Purchase Order",
                state: "layout.PurchaseOrder"
            },
            {
                label: "Add Purchase Order",
                state: null
            }
        ];
    } else if ($stateParams.action == "EDIT") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPOReloaded = "NO";
                $scope.purchaseOrder = JSON.parse(localStorage.reloadFormData);
                //default focus for this page 
            } else {
                $scope.purchaseOrder = $rootScope.selectedUnfilledFormData;
                $scope.partyMasterActionResponseFn();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPOReloaded = "NO";
                $scope.purchaseOrder = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.editInit($stateParams.id);
            }
        }

        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.PurchaseOrder"
            },
            {
                label: "Purchase Order",
                state: "layout.PurchaseOrder"
            },
            {
                label: "Edit Purchase Order",
                state: null
            }
        ];
    }

}]);