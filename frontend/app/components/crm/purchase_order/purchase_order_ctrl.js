app.controller('PurchaseOrderCtrl',['$rootScope', '$scope', '$window', 'ngDialog', '$state', '$stateParams',
    '$modal', '$http', 'purchaseOrderFactory', 'RecentHistorySaveService', 'roleConstant',
    function($rootScope, $scope, $window, ngDialog, $state, $stateParams,
    $modal, $http, purchaseOrderFactory, RecentHistorySaveService, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;



    /********************* ux fix ends here *****************************/
    $scope.purchaseOrderHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false
        },

        {
            "name": "BC",
            "width": "w150px",
            "prefWidth": "150",
            "model": "buyer",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "buyerName"

        },
        {
            "name": "Supplier",
            "width": "w150px",
            "prefWidth": "150",
            "model": "supplier",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "supplierName"
        },
        {
            "name": "PO No",
            "width": "w100px",
            "prefWidth": "100",
            "model": "poNo",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "poNo"

        },
        {
            "name": "PO Date",
            "width": "w100px",
            "prefWidth": "100",
            "model": "poDate",
            "search": true,
            "type": "date-range",
            "sort": true,
            "key": "poDate"
        },

        {
            "name": "Origin",
            "width": "w125px",
            "prefWidth": "125",
            "model": "origin",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "origin"
        },
        {
            "name": "Destination",
            "width": "w125px",
            "prefWidth": "125",
            "model": "destination",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "destination"
        },
        {
            "name": "Transport Mode",
            "width": "w100px",
            "prefWidth": "100",
            "model": "transportMode",
            "search": true,
            "type": "drop",
            "sort": true,
            "key": "transportMode"
        },
        {
            "name": "TOS",
            "width": "w100px",
            "prefWidth": "100",
            "model": "tos",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "tos"
        },

        {
            "name": "Status",
            "width": "w100px",
            "prefWidth": "100",
            "model": "status",
            "search": true,
            "type": "drop",
            "data": $rootScope.enum['LovStatus'],
            "key": "status"
        }
    ];

    $scope.sortSelection = {
        sortKey: "po.id",
        sortOrder: "desc"
    };

    $scope.searchData = {};

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 0;

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.enquirysearch($scope.page, $scope.limit);
    }
    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search($scope.page, $scope.limit);
    }

    $scope.clickOnTabView = function(tab) {
        if (tab == 'items' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_ITEMS_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'attachments' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_ATTACHMENT_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'vehicleInfo' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_VEHICLE_INFO_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'remarksFollowUp' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_REMARKS_FOLLOW_UP_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'Others' && $rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_OTHERS_VIEW)) {
            $scope.Tabs = tab;
        }

    }

    $scope.rowSelect = function(data, index) {

        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_VIEW)) {

            //          $scope.selectedRecordIndex = index;
            var sParam = JSON.parse(JSON.stringify($scope.searchData));

            if (sParam.poDate != null) {
                sParam.poDateStartDate = $rootScope.sendApiStartDateTime(sParam.poDate.startDate);
                sParam.poDateStartDate = $rootScope.sendApiEndDateTime(sParam.poDate.endDate);
                sParam.poDate = null;
            }

            $scope.selectedRecordIndex = ($scope.searchData.recordPerPage * $scope.searchData.selectedPageNumber) + index;
            sParam.id = data.id;
            sParam.selectedPageNumber = $scope.selectedRecordIndex;
            sParam.totalRecord = $scope.totalRecord;
            sParam.recordPerPage = 1;

            $state.go("layout.viewPurchaseOrder", sParam);

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }

            /********************* ux fix ends here *****************************/

        }
    };
    $scope.cancel = function() {
        $scope.showDetail = false;
        $scope.showLogs = false;
        $scope.showHistory = false;
        $scope.chargeMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };
    $scope.back = function() {
        /*
        var listParam = JSON.parse(JSON.stringify($stateParams));
        listParam.id = undefined;
        listParam.selectedPageNumber = undefined;
        listParam.recordPerPage = undefined;
        listParam.totalRecord = undefined;
        listParam.fromView = 1;*/
        $state.go("layout.PurchaseOrder");
    };
    $scope.addpurchaseorder = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_CREATE)) {
            $state.go("layout.addPurchaseOrder");
        }
    };


    $scope.editPO = function() {
        if ($rootScope.roleAccess(roleConstant.CRM_PURCHASE_ORDER_MODIFY)) {
            $state.go("layout.editPurchaseOrder", {
                id: $stateParams.id
            });
        }
    };




    $scope.moreinfoPopup = function(index) {
        $scope.selectedItemIdx = index;

        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/purchase_order/views/moreinfo_popup.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
    };


    $scope.po = {}; 
    $scope.po = {
        "status": "Active"
    };


    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            console.log("attrname - ", attrname, " ; Value - ", param[attrname]);
            $scope.searchData[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
    }


    $scope.sortChange = function(param) {
        console.log("Sort Param ,", param);
        switch (param.sortKey) {
            case "status":
                $scope.searchData.sortByColumn = "po.status";
                break;
            case "tos":
                $scope.searchData.sortByColumn = "po.tosMaster.tosName";
                break;
            case "transportMode":
                $scope.searchData.sortByColumn = "po.transportMode";
                break;
            case "destination":
                $scope.searchData.sortByColumn = "po.destination.portName";
                break;
            case "origin":
                $scope.searchData.sortByColumn = "po.origin.portName ";
                break;
            case "poDate":
                $scope.searchData.sortByColumn = "po.poDate";
                break;
            case "poNo":
                $scope.searchData.sortByColumn = "po.poNo";
                break;
            case "supplierName":
                $scope.searchData.sortByColumn = "po.supplier.partyName";
                break;
            case "buyerName":
                $scope.searchData.sortByColumn = "po.buyer.partyName";
                break;
            default:
                $scope.searchData.sortByColumn = "po.id";
        }
        $scope.searchData.orderByType = param.sortOrder.toUpperCase();
        $scope.search();
    };

    $scope.search = function() {
        /*if($stateParams.fromView != undefined && $stateParams.fromView != null && $stateParams.fromView === "1") {
            $scope.searchData = JSON.parse(JSON.stringify($stateParams));
        }*/
        $scope.searchData.selectedPageNumber = $scope.page;
        $scope.searchData.recordPerPage = $scope.limit;

        $scope.tmpSearchDto = JSON.parse(JSON.stringify($scope.searchData));

        if ($scope.tmpSearchDto.poDate != null) {
            $scope.tmpSearchDto.poDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.poDate.startDate);
            $scope.tmpSearchDto.poDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.poDate.endDate);
        }

        console.log("$scope.searchData -- ", $scope.tmpSearchDto);
        $scope.purchaseOrderArr = [];
        purchaseOrderFactory.search.query($scope.tmpSearchDto).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            $scope.purchaseOrderArr = data.responseObject.searchResult;

        });
    }



    /* Attachment Tab */

    $scope.attachConfig = {
        "isEdit": false,
        "isDelete": false,
        "page": "noDoc",
        "columnDefs": [{
                "name": "Reference #",
                "model": "refNo",
                "type": "text"
            },
            {
                "name": "Unprotected File Name",
                "model": "unprotectedFileName",
                "type": "file"
            },
            {
                "name": "Protected File Name",
                "model": "protectedFileName",
                "type": "file"
            }
        ]
    };

    $scope.downloadAttach = function(param) {
        console.log("download ", param);

        if (param.data.id != null && (param.data.protectedFile == null || param.data.unprotectedFile == null)) {
            console.log("API CALL")
            $http({
                url: $rootScope.baseURL + '/api/v1/purchaseorder/files/' + param.data.id + '/' + param.type + '/false',
                method: "GET",
                responseType: 'arraybuffer'
            }).success(function(data, status, headers, config) {
                var blob = new Blob([data], {});
                console.log("blob ", blob);
                if (param.type == 'unprotectedFileName')
                    saveAs(blob, param.data.unprotectedFileName);
                if (param.type == 'protectedFileName')
                    saveAs(blob, param.data.protectedFileName);
            }).error(function(data, status, headers, config) {
                console.log("Error Downloading");
            });
        }
    }

    $scope.view = function(id) {

        console.log("View method is called ", id);

        purchaseOrderFactory.common.findById({
            id: id
        }).$promise.then(function(data) {

            if (data.responseCode == 'ERR0') {
                $scope.purchaseOrder = data.responseObject;

                if ($scope.purchaseOrder != undefined && $scope.purchaseOrder != null && $scope.purchaseOrder.buyer != undefined && $scope.purchaseOrder.buyer != null &&
                    $scope.purchaseOrder.buyer.bcName != undefined && $scope.purchaseOrder.buyer.bcName != null) {
                    $rootScope.subTitle = $scope.purchaseOrder.buyer.bcName;
                } else {
                    $rootScope.subTitle = "Unknown Buyer"
                }
                $rootScope.unfinishedFormTitle = "PurchaseOrder View # " + $scope.purchaseOrder.poNo;
                var rHistoryObj = {
                    'title': "PurchaseOrder View #" + $scope.purchaseOrder.poNo,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'PurchaseOrder',
                    'serviceType': 'AIR',
                    'showService': false
                }

                RecentHistorySaveService.form(rHistoryObj);
            } else {
                console.log("Exception while getting Purchase Order by Id ", data.responseDescription);
            }

        }, function(error) {
            console.log("Error ", error);
        });

    };


    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            $scope.searchData.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
            $scope.searchData.recordPerPage = 1;
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchData.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchData.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.status != undefined && $stateParams.status != null) {
                stateParameters.status = $scope.searchData.status = $stateParams.status;
            }
            if ($stateParams.buyerName != undefined && $stateParams.buyerName != null) {
                stateParameters.buyerName = $scope.searchData.buyerName = $stateParams.buyerName;
            }
            if ($stateParams.supplierName != undefined && $stateParams.supplierName != null) {
                stateParameters.supplierName = $scope.searchData.supplierName = $stateParams.supplierName;
            }
            if ($stateParams.poNo != undefined && $stateParams.poNo != null) {
                stateParameters.poNo = $scope.searchData.poNo = $stateParams.poNo;
            }
            if ($stateParams.origin != undefined && $stateParams.origin != null) {
                stateParameters.origin = $scope.searchData.origin = $stateParams.origin;
            }
            if ($stateParams.destination != undefined && $stateParams.destination != null) {
                stateParameters.destination = $scope.searchData.destination = $stateParams.destination;
            }
            if ($stateParams.transportMode != undefined && $stateParams.transportMode != null) {
                stateParameters.transportMode = $scope.searchData.transportMode = $stateParams.transportMode;
            }
            if ($stateParams.tos != undefined && $stateParams.tos != null) {
                stateParameters.tos = $scope.searchData.tos = $stateParams.tos;
            }
            $scope.searchData.poDate = {};
            if ($stateParams.poDateStartDate != undefined && $stateParams.poDateStartDate != null && $stateParams.poDateEndDate != undefined && $stateParams.poDateEndDate != null) {
                stateParameters.poDateStartDate = $scope.searchData.poDate.startDate = $stateParams.poDateStartDate;
                stateParameters.poDateEndDate = $scope.searchData.poDate.endDate = $stateParams.poDateEndDate;
            }

        } else {
            return;
        }
        purchaseOrderFactory.search.query($scope.searchData).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.selectedPageNumber = $scope.searchData.selectedPageNumber;
            stateParameters.id = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.viewPurchaseOrder", stateParameters);

        });
    }
    if ($stateParams.action == "SEARCH") {
        console.log("Search Page....");
        $scope.search();

        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.PurchaseOrder"
            },
            {
                label: "Purchase Order",
                state: null
            }
        ];

    } else if ($stateParams.action == "VIEW") {

        console.log("View Page....", $stateParams.id);

        $scope.view($stateParams.id);

        $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
        $scope.totalRecord = parseInt($stateParams.totalRecord);
        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.PurchaseOrder"
            },
            {
                label: "Purchase Order",
                state: "layout.PurchaseOrder"
            },
            {
                label: "View Purchase Order",
                state: null
            }
        ];
    }

}]);