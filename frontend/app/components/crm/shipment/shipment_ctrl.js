/**
 * Created by hmspl on 12/4/16.
 */
app.controller('shipmentCtrl', ['$rootScope', '$timeout', '$scope', '$http', '$location', '$modal',
    'ShipmentGet', 'ShipmentSearch', 'PartiesList', 'PortByTransportMode',
    'ServiceByTransportMode', 'CurrencySearchExclude', 'CarrierByTransportMode',
    'ShipmentAsyncSearch', 'ShipmentGetFromUID', 'PortGroupList', 'ngDialog', 'ShipmentUpdate',
    'NavigationService', 'AesSearch', 'EmployeeList', 'commentMasterFactory', 'SequenceFactoryForCAN',
    'ShipmentStatusChange', 'discardService', 'cloneService', 'ShipmentGetByUid', 'ShipmentDocumentIdList',
    'ServiceGetByUid', 'NgMap', '$uibModal', '$sce', '$window', 'ValidateUtil', 'invoiceService', 'ShipmentServiceInvoiceList',
    'RecentHistorySaveService', '$state', '$stateParams', 'QuotationViewByQuotationNo', 'ConsolGetByUid',
    'downloadFactory', 'downloadMultipleFactory', 'AutoImportProcess', 'ProvisionalViewByShipmentServiceUid',
    'uiShipmentDataService', 'appMetaFactory', 'Notification', 'AesAddAll', 'roleConstant', 'ShipmentTrack',
    'ServiceSignOffStatus', 'uiShipmentAccountsService', 'uiShipmentFactory',
    function($rootScope, $timeout, $scope, $http, $location, $modal,
        ShipmentGet, ShipmentSearch, PartiesList, PortByTransportMode,
        ServiceByTransportMode, CurrencySearchExclude, CarrierByTransportMode,
        ShipmentAsyncSearch, ShipmentGetFromUID, PortGroupList, ngDialog, ShipmentUpdate,
        NavigationService, AesSearch, EmployeeList, commentMasterFactory, SequenceFactoryForCAN,
        ShipmentStatusChange, discardService, cloneService, ShipmentGetByUid, ShipmentDocumentIdList,
        ServiceGetByUid, NgMap, $uibModal, $sce, $window, ValidateUtil, invoiceService, ShipmentServiceInvoiceList,
        RecentHistorySaveService, $state, $stateParams, QuotationViewByQuotationNo, ConsolGetByUid,
        downloadFactory, downloadMultipleFactory, AutoImportProcess, ProvisionalViewByShipmentServiceUid,
        uiShipmentDataService, appMetaFactory, Notification, AesAddAll, roleConstant, ShipmentTrack,
        ServiceSignOffStatus, uiShipmentAccountsService, uiShipmentFactory) {
        $scope.$uiShipmentAccountsService = uiShipmentAccountsService;
        $scope.$uiShipmentDataService = uiShipmentDataService;
        $scope.$roleConstant = roleConstant;
        //attachment button
        var attachmentModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_attachment_popup_view.html',
            show: false
        });
        $scope.showAttachmentView = function(shipment, index) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_ATTACHMENT_VIEW)) {
                $scope.shipment = shipment;
                attachmentModal.$promise.then(attachmentModal.show);

            }

        };

        //track-shipment button
        var addtrackModal;
        $scope.trackmodel = function(shipmentId) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_TRACK_SHIPMENT_VIEW)) {
                $scope.trackShipment(shipmentId);
                addtrackModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/crm/new-shipment/dialog/track-shipment-popup.html',
                    show: false
                });
                $scope.navigateToNextField("flightSchedule.service");
            }
        };


        //boe view button
        var boeViewModal;
        $scope.viewboemodel = function() {

            boeViewModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_boe_view_popup.html',
                show: false
            });
            boeViewModal.$promise.then(boeViewModal.show)
        };
        //boe note view button
        var noteBoeModal;
        $scope.addnoteboemodel = function() {

            noteBoeModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_boe_notes_view_popup.html',
                show: false
            });
            noteBoeModal.$promise.then(noteBoeModal.show)
        };

        $scope.isPoView = $stateParams.action;

        $scope.getSerivceSignOffStatus = function(fromState) {
                var serviceId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;
                ServiceSignOffStatus.get({
                    serviceId: serviceId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (data.responseObject != null && data.responseObject.isSignOff == true) {

                            if (fromState == 'INV') {
                                if ($rootScope.roleAccess(roleConstant.FINANCE_INVOICE_CREATE)) {
                                    uiShipmentAccountsService.createInvoice($scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                                }
                            } else if (fromState == 'CREDIT_NOTE_COST') {
                                if ($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_COST_CREATE)) {
                                    uiShipmentAccountsService.createCreditNoteCost($scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                                }
                            } else if (fromState == 'CREDIT_NOTE_REVENUE') {
                                if ($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_CREATE)) {
                                    uiShipmentAccountsService.createCreditNoteRevenue($scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                                }
                            }

                        } else {
                            console.log($rootScope.nls["ERR40027"]);
                            $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                        }
                    } else {
                        console.log($rootScope.nls["ERR40027"]);
                        $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                    }
                }, function(error) {
                    console.log("error in getSerivceSignOffStatus method : " + error);
                });
            }
            // trigger button
        var triggerModel = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_trigger_popup_view.html',
            show: false
        });

        $scope.showTriggerView = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_TRIGGER_UPDATE_VIEW)) {
                triggerModel.$promise.then(triggerModel.show)
            }
        };


        /*open Event Modal*/
        var eventModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_event_popup_view.html',
            show: false
        });;
        $scope.showEventView = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_EVENTS_VIEW)) {
                eventModal.$promise.then(eventModal.show)
            }
        };

        /*Refer Modal*/
        var referenceModel = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_reference_popup_view.html',
            show: false
        });


        $scope.showReferenceView = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_REFERENCE_VIEW)) {
                referenceModel.$promise.then(referenceModel.show)
            }
        };




        var myTotalOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/shipment/views/notespopup.html',
            show: false
        });

        $scope.showNotesView = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_NOTES_VIEW)) {
                $scope.object = {};
                $scope.object.notes = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].notes;
                $scope.object.internalNote = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].internalNote;
                myTotalOtherModal.$promise.then(myTotalOtherModal.show);

            }
        };

        $scope.limitArr = [10, 15, 20];
        $scope.searchOption = ['Customer Order No', 'Shipper Reference No', 'Hawb No', 'Created By', 'Booking Person'];

        $scope.tabArray = $rootScope.enum['ServiceStatus'];
        var tempServiceStatusArr = [];
        $scope.tabArray.forEach(function (item) {
            tempServiceStatusArr.push({'displayName':item, 'totalCount':0});
        })
        tempServiceStatusArr.push({'displayName':"All", 'totalCount':0});
        $scope.tabArray = tempServiceStatusArr;
        tempServiceStatusArr = [];

        $scope.tabChange = function(tabName) {

            $scope.searchDto.importExport = "Both";
            $scope.detailTab = tabName;
            if(typeof(tabName)=="object"){
            $scope.selectedTabName = tabName.displayName;
            }
            else{
                $scope.selectedTabName = tabName;
            }

            $scope.page = 0;
            $scope.limit = 10;
            $scope.totalRecord = 10;
            $scope.search();
        }

        $scope.showShipmentTab = function(index) {
            $scope.selectedTabIndex = index;
            $scope.moreAddressTab = 'moreInfo';
        };

        $scope.checkOpenTab = function() {
            if ($scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport) {
                $scope.moreAddressTab = 'documentInfo';
            } else {
                $scope.moreAddressTab = 'moreInfo';
            }
        }



        NgMap.getMap().then(function(map) {
            $scope.map = map;
        });

        $scope.googleMapModal = function(sourcePlace, destinationPlace) {
            var src = "";
            if (sourcePlace != undefined && sourcePlace != null) {
                if (sourcePlace.addressLine1 != undefined && sourcePlace.addressLine1 != null && sourcePlace.addressLine1.length > 0) {
                    src = sourcePlace.addressLine1 + ", ";
                }
                if (sourcePlace.addressLine2 != undefined && sourcePlace.addressLine2 != null && sourcePlace.addressLine2.length > 0) {
                    src = src + sourcePlace.addressLine2 + ", ";
                }
                if (sourcePlace.addressLine3 != undefined && sourcePlace.addressLine3 != null && sourcePlace.addressLine3.length > 0) {
                    src = src + sourcePlace.addressLine1 + ", ";
                }
                if (sourcePlace.city != undefined && sourcePlace.city != null && sourcePlace.city.cityName != undefined &&
                    sourcePlace.city.cityName != null && sourcePlace.city.cityName.length > 0) {
                    src = src + sourcePlace.city.cityName + ", ";
                }
                if (sourcePlace.zipCode != undefined && sourcePlace.zipCode != null && sourcePlace.zipCode.length > 0) {
                    src = src + sourcePlace.zipCode;
                }
            }

            var dest = "";
            if (destinationPlace != undefined && destinationPlace != null) {
                if (destinationPlace.addressLine1 != undefined && destinationPlace.addressLine1 != null && destinationPlace.addressLine1.length > 0) {
                    dest = destinationPlace.addressLine1 + ", ";
                }
                if (destinationPlace.addressLine2 != undefined && destinationPlace.addressLine2 != null && destinationPlace.addressLine2.length > 0) {
                    dest = dest + destinationPlace.addressLine2 + ", ";
                }
                if (destinationPlace.addressLine3 != undefined && destinationPlace.addressLine3 != null && destinationPlace.addressLine3.length > 0) {
                    dest = dest + destinationPlace.addressLine1 + ", ";
                }
                if (destinationPlace.city != undefined && destinationPlace.city != null && destinationPlace.city.cityName != undefined &&
                    destinationPlace.city.cityName != null && destinationPlace.city.cityName.length > 0) {
                    dest = dest + destinationPlace.city.cityName + ", ";
                }
                if (destinationPlace.zipCode != undefined && destinationPlace.zipCode != null && destinationPlace.zipCode.length > 0) {
                    dest = dest + destinationPlace.zipCode;
                }
            }
            $scope.sourceMapPlace = src;
            $scope.destinationMapPlace = dest;
        }


        $scope.notePopup = $modal({
            scope: $scope,
            templateUrl: 'holdStatusNote.html',
            backdrop: 'static',
            show: false,
            keyboard: true
        });
        $scope.notespopup = function(note) {
            $scope.notePopup.$promise.then($scope.notePopup.show);
            $scope.popUpNote = note;
        };

        $scope.provisionalObj = {};



        $scope.showActionButton = true;



        $scope.errorMap = new Map();

        $scope.rateMerged = false;
        var rateMergedValue = $rootScope.appMasterData['booking.rates.merged'];
        if (rateMergedValue.toLowerCase() == 'true' || rateMergedValue == true) {
            $scope.rateMerged = true;
        }
        $scope.showPagination = true;

        $scope.reportData = {};
        $scope.searchDto = {};


        $scope.searchBy = function(clearFlag) {
            $scope.page = 0;
            if ($scope.searchHeadArr.length == 8) {
                $scope.searchHeadArr.splice($scope.searchHeadArr.length - 1, 1);
            }
            $scope.navigateToNextField('searchValueAll');
            if ($scope.searchDto.searchName === undefined || $scope.searchDto.searchName === null) {
                $scope.searchDto.searchValue = "";
                $scope.searchValueAllFlag = false;
                var obj = {
                    "name": "Terms of Shipment",
                    "model": "tosName",
                    "search": true,
                    "wrap_cell": true,
                    "type": "text",
                    "sort": true,
                    "width": "w150px",
                    "prefWidth": "150",
                    "key": "tosName"
                };
                $scope.searchHeadArr.push(obj);
            } else {
                $scope.searchValueAllFlag = true;
                $scope.detailTab = 'all';
                $scope.searchDto.serviceStatus = null;
                if (clearFlag) {
                    $scope.searchDto.searchValue = "";
                }
                if ($scope.searchDto.searchName === "Customer Order No") {
                    var obj = {
                        "name": "Customer Order No",
                        "model": "referenceNumber",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "width": "w150px",
                        "prefWidth": "150",
                        "key": "customerOrderNumber"
                    }
                    $scope.searchHeadArr.push(obj);
                } else if ($scope.searchDto.searchName === "Shipper Reference No") {

                    var obj = {
                        "name": "Shipper Reference No",
                        "model": "referenceNumber",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "width": "w150px",
                        "prefWidth": "150",
                        "key": "shipperReferenceNumber"
                    }

                    $scope.searchHeadArr.push(obj);

                } else if ($scope.searchDto.searchName === "Hawb No") {

                    var obj = {
                        "name": "Hawb No",
                        "model": "hawbNo",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "width": "w150px",
                        "prefWidth": "150",
                        "key": "hawbNo"
                    }

                    $scope.searchHeadArr.push(obj);

                } else if ($scope.searchDto.searchName === "Created By") {

                    var obj = {
                        "name": "Created By",
                        "model": "employeeName",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "width": "w150px",
                        "prefWidth": "150",
                        "key": "createdBy"
                    }

                    $scope.searchHeadArr.push(obj);

                } else {

                    var obj = {
                        "name": "Booking Person",
                        "model": "customerService",
                        "search": false,
                        "wrap_cell": true,
                        "type": "text",
                        "sort": false,
                        "width": "w150px",
                        "prefWidth": "150",
                        "key": "customerService"
                    }

                    $scope.searchHeadArr.push(obj);

                }

            }

            $scope.search();
        }


        $scope.searchHeadArr = [{
                "name": "#",
                "width": "w50px",
                "prefWidth": "50",
                "model": "no",
                "search": false,

            },
            {
                "name": "Shipper",
                "search": true,
                "model": "shipperName",
                "wrap_cell": true,
                "type": "color-text",
                "sort": true,
                "width": "w175px",
                "prefWidth": "175",
                "key": "shipperName"
            },
            {
                "name": "Consignee",
                "search": true,
                "model": "consigneeName",
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "width": "w175px",
                "prefWidth": "175",
                "key": "consigneeName"
            },
            {
                "name": "Shipment ID",
                "model": "shipmentUid",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "width": "w125px",
                "prefWidth": "125",
                "key": "shipmentUid"
            },
            {
                "name": "Shipment Date",
                "model": "shipmentReqDate",
                "search": true,
                "wrap_cell": true,
                "type": "date-range",
                "sort": true,
                "width": "w150px",
                "prefWidth": "150",
                "key": "shipmentDate"

            },
            {
                "name": "Origin",
                "model": "originName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "width": "w125px",
                "prefWidth": "125",
                "key": "originName"

            },
            {
                "name": "Destination",
                "model": "destinationName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "width": "w125px",
                "prefWidth": "125",
                "key": "destinationName"
            }, {
                "name": "Terms of Shipment",
                "model": "tosName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "width": "w150px",
                "prefWidth": "150",
                "key": "tosName"
            }
        ];

        $scope.init = function() {
            $scope.invoiceList = [];
            $scope.customerAgent = undefined;
            $scope.documentTypeCode = undefined;

            if (NavigationService.getSubData1() != null && NavigationService.getSubData1() == "QUOTATION_TO_SHIPMENT") {
                $scope.isNavigation = true;
                $scope.rowSelectForShipmentUid(NavigationService.get().bookingNo);
            } else {
                $scope.isNavigation = false;
                $scope.shipment = {};
                $scope.reportData = {};
                $scope.reportData.dateObj = new Date();
                $scope.tabChange('Booked');
                $scope.cloneConnectionList = [];
                $scope.accountList = [];
            }
            getIndivisualCount();

        }


        /*Aes generate all check box code started here*/

        $scope.allAesModel = {};
        $scope.allAesModel.allAesSelected = false;

        $scope.selectAllAes = function() {

            for (var i = 0; i < $scope.finalArr.length; i++) {
                $scope.finalArr[i].isSelected = $scope.allAesModel.allAesSelected;
            }
        }
        $scope.chkAesStatus = function(chkFlag, detObj) {
            console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
        }

        $scope.generateAllAes = function() {
            $scope.spinner = true;
            var count = 0;
            var aesArr = [];
            if ($scope.finalArr != null && $scope.finalArr != undefined && $scope.finalArr.length > 0) {
                $scope.aesGenerateAdd($scope.finalArr[0]);
            }

        }

        $scope.aesGenerateAll = function(aesArr) {
                console.log("Aes files need to genarate : ", aesArr);
                AesAddAll.save(aesArr).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Aes Saved Successfully");
                        $scope.spinner = false;
                        Notification.success("Successfully generated ");
                    } else {
                        $scope.spinner = false;
                        console.log("Aes Saving Failed ", data.responseDescription);
                    }
                }, function(error) {
                    $scope.spinner = false;
                    console.log("Aes Saving Failed : ", error)
                });
            }
            /*Aes generate all check box code ended here*/


        /*Accounts Modal*/
        var cfsReceiveModel = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_cfs.html',
            show: false
        });

        $scope.showCfsPopup = function() {
            if ($scope.validateForCfs($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex], $scope.selectedShipmentServiceIndex)) {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_CFS_VIEW)) {
                    $scope.cfsReceiveEntryList = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].cfsReceiveEntryList;
                    cfsReceiveModel.$promise.then(cfsReceiveModel.show);
                }
            }
        };

        $scope.validateForCfs = function(service, index) {

            if (!$rootScope.isGoodsReceived(service)) {

                if (service.carrier == undefined || service.carrier == "" || service.carrier == null || service.carrier.id == undefined) {
                    cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                    Notification.error($rootScope.nls["ERR90063"])
                    uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.carrier." + index, $rootScope.nls["ERR90063"], index, "serviceRouting", undefined);
                    return false;
                }

                if (service.routeNo == undefined || service.routeNo == "" || service.routeNo == null) {
                    cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                    Notification.error($rootScope.nls["ERR90067"])
                    uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.flightNo." + index, $rootScope.nls["ERR90067"], index, "serviceRouting", undefined);
                    return false;
                }

                if (service.eta == undefined || service.eta == null || service.eta == '') {
                    cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                    Notification.error($rootScope.nls["ERR90095"])
                    uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.eta." + index, $rootScope.nls["ERR90095"], index, "serviceRouting", undefined);
                    return false;
                }

                if (service.etd == undefined || service.etd == null || service.etd == '') {
                    cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
                    Notification.error($rootScope.nls["ERR90094"])
                    uiShipmentValidationService.validationResponse(true, "shipment.serviceDetail.etd." + index, $rootScope.nls["ERR90094"], index, "serviceRouting", undefined);
                    return false;
                }

            }
            return true;
        }


        $scope.createCFS = function(serviceUid) {

            var param = {
                shipmentUid: $scope.shipment.shipmentUid,
                serviceUid: $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].serviceUid,
                fromState: $state.current.name,
                fromStateParams: $scope.shipment.id
            };
            cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
            $state.go("layout.createAirCFS", param);

        }

        $scope.viewCFS = function(id) {

            console.log("CFS ID " + id);

            var params = {
                cfsId: id,
                fromState: $state.current.name,
                fromStateParams: $scope.shipment.id
            }
            cfsReceiveModel.$promise.then(cfsReceiveModel.hide);
            $state.go('layout.viewAirCFS', params);
        }

        /*Accounts Modal*/
        var accountsModel = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_account.html',
            show: false
        });

        $scope.showAccountsPopup = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_ACCOUNTS_VIEW)) {
                var serviceId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;
                var serviceUid = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].serviceUid;
                $scope.invoiceList = [];
                $scope.provisionalObj = {};
                $scope.getAccountData(serviceId);
                $scope.getProvisionalData(serviceUid);
                accountsModel.$promise.then(accountsModel.show);


            }
        };

        $scope.getProvisionalData = function(serviceUid) {
            $scope.provisionSpinner = true;
            ProvisionalViewByShipmentServiceUid.get({
                serviceuid: serviceUid
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.provisionalObj = data.responseObject;
                    $scope.provisionSpinner = false;
                }
            }, function(error) {
                console.log("ProvisionalViewByShipmentServiceUid Failed : " + error);
                $scope.provisionSpinner = false;
            });
        }

        $scope.getAccountData = function(serviceId) {
            $scope.invoiceSpinner = true;
            var searchDto = {};
            searchDto.param1 = serviceId;
            searchDto.param2 = null;
            return ShipmentServiceInvoiceList.fetch(searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.invoiceList = data.responseObject;
                        $scope.invoiceSpinner = false;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching invoicelist');
                    $scope.invoiceSpinner = false;
                }
            );
        }



        $scope.filterCustomerAgent = function(type) {

            if (type == 'Customer')
                $scope.customerAgent = 'Customer';
            if (type == 'Agent')
                $scope.customerAgent = 'Agent';
            if (type == 'All')
                $scope.customerAgent = undefined;

        }


        $scope.filterInvoiceCreditNote = function(type) {

            if (type == 'Invoice')
                $scope.documentTypeCode = 'INV';
            if (type == 'Credit Note')
                $scope.documentTypeCode = 'CRN';
            if (type == 'All')
                $scope.documentTypeCode = undefined;

        }


        $scope.provisionalFromShipment = function(object) {
            if (object != null && object.id != null) {
                if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_MODIFY)) {
                    uiShipmentAccountsService.addOrEditProvisional(object, $scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                }
            } else {
                if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_CREATE)) {
                    uiShipmentAccountsService.addOrEditProvisional(null, $scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
                }
            }
        }


        $scope.getInvoiceCrnView = function(invoiceCreditNote) {
            uiShipmentAccountsService.getInvoiceCrnView(invoiceCreditNote, $scope.shipment, accountsModel, $scope.selectedShipmentServiceIndex);
        }

       $scope.onBtnGroupChange = function(){
         getIndivisualCount(); 
         $scope.search();
       }

        $scope.search = function() {
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            if ($scope.selectedTabName == 'All') {
                $scope.searchDto.serviceStatus = null;
            } else {
                $scope.searchDto.serviceStatus = $scope.selectedTabName;
            }
            if ($scope.searchDto.shipmentDate != null) {
                $scope.searchDto.shipmentDate.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.shipmentDate.startDate);
                $scope.searchDto.shipmentDate.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.shipmentDate.endDate);
            }


            ShipmentSearch.query($scope.searchDto).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = data.responseObject.searchResult;
                $scope.shipmentArr = [];
                angular.forEach(tempArr, function(item, index) {
                    var tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    if (tempObj.shipperColorCode != null && tempObj.shipperColorCode != "") {
                        tempObj.changeColor = true;
                        tempObj.color = tempObj.shipperColorCode;
                    }
                    $scope.shipmentArr.push(tempObj);
                });
            }, function(error) {
                console.log(error);
            });

        }

        var getIndivisualCount = function() {
            var copySearchDTO = angular.copy($scope.searchDto)

            copySearchDTO.selectedPageNumber = -1;
            copySearchDTO.recordPerPage = -1;

            copySearchDTO.serviceStatus = null;
          
            if (copySearchDTO.shipmentDate != null) {
                copySearchDTO.shipmentDate.startDate = $rootScope.sendApiStartDateTime(copySearchDTO.shipmentDate.startDate);
                copySearchDTO.shipmentDate.endDate = $rootScope.sendApiEndDateTime(copySearchDTO.shipmentDate.endDate);
            }

    
            ShipmentSearch.query(copySearchDTO).$promise.then(function(data, status) {
                var tempArr = data.responseObject.searchResult;
                resetTotalCount();
                angular.forEach(tempArr, function(item, index) {
                    switch (item.serviceStatus) {
                        case "Booked":
                            updateTabTotalCount("Booked");
                            updateTabTotalCount("All");
                            break;

                        case "Received":
                            updateTabTotalCount("Received");
                            updateTabTotalCount("All");
                            break;

                        case "Generated":
                            updateTabTotalCount("Generated");
                            updateTabTotalCount("All");
                            break;

                        case "Closed":
                            updateTabTotalCount("Closed");
                            updateTabTotalCount("All");
                            break;

                        case "Cancelled":
                            updateTabTotalCount("Cancelled");
                            updateTabTotalCount("All");
                            break;

                    }
                    
                });
            }, function(error) {
                console.log(error);
            });

        }

        var updateTabTotalCount = function (tabName) {
            $scope.tabArray.forEach(function (item) {
                if (item.displayName === tabName) {
                    item.totalCount = item.totalCount + 1
                };
            })
        }
        var resetTotalCount = function () {
            $scope.tabArray.forEach(function (item) {
                item.totalCount = 0;
            })
        }


        $scope.searchSortSelection = {
            sortKey: "shipmentUid",
            sortOrder: "desc"
        }




        $scope.changePage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.cancel();
            $scope.search();
        }

        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }

        $scope.cancel = function() {
            console.log("Back button is pressed.....");
            $scope.showDetail = false;
            $state.go("layout.crmShipment");
        }

        $scope.rowSelectForShipmentUid = function(shipmentUid) {

            ShipmentGetFromUID.get({
                    shipmentuid: shipmentUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.shipment = data.responseObject;
                        $scope.showDetail = true;
                        $scope.dataChangeEvent($scope.shipment);
                        $scope.hidePage = false;
                        //rate booking configuration
                        $scope.showRateEdit = true;
                        $scope.showDocumentEdit = true;
                        $scope.enableBookingRates = $rootScope.appMasterData['booking.rates.merged'];

                        // To fill the attachment table..
                        $scope.populateAttachment();

                    } else {
                        $scope.shipment = null;
                    }
                },
                function(error) {
                    console.log("Shipment get Failed : " + error)
                    $scope.shipment = null;
                });

        }

        $scope.rowSelect = function(data, index) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_VIEW)) {
                $scope.indexFlag = index;
                $scope.searchDto.selectedPageNumber = $scope.page;
                $scope.searchDto.recordPerPage = $scope.limit;
                $scope.selectedRowIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
                var localRefStateParam = {
                    shipmentId: data.id,
                    selectedPageNumber: $scope.selectedRowIndex,
                    totalRecord: $scope.totalRecord,
                    status: null
                };
                console.log("State Parameters :: ", localRefStateParam);
                $state.go("layout.viewCrmShipment", $scope.searchDtoToStateParams(localRefStateParam));
            }
        }



        $scope.searchDtoToStateParams = function(param) {
            if (param == undefined || param == null) {
                param = {};
            }
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                if ($scope.searchDto.shipmentDate != undefined && $scope.searchDto.shipmentDate != null) {
                    param.shipmentDateStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.shipmentDate.startDate);
                    param.shipmentDateEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.shipmentDate.endDate);
                }
                if ($scope.searchDto.importExport != undefined && $scope.searchDto.importExport != null) {
                    param.importExport = $scope.searchDto.importExport;
                }

                if ($scope.searchDto.serviceStatus != undefined && $scope.searchDto.serviceStatus != null) {
                    param.shipmentStatus = $scope.searchDto.serviceStatus;
                }
                if ($scope.searchDto.routedBy != undefined && $scope.searchDto.routedBy != null) {
                    param.routedBy = $scope.searchDto.routedBy;
                }
                if ($scope.searchDto.routed != undefined && $scope.searchDto.routed != null) {
                    param.routed = $scope.searchDto.routed;
                }
                if ($scope.searchDto.createdBy != undefined && $scope.searchDto.createdBy != null) {
                    param.createdBy = $scope.searchDto.createdBy;
                }
                if ($scope.searchDto.tos != undefined && $scope.searchDto.tos != null) {
                    param.tos = $scope.searchDto.tos;
                }
                if ($scope.searchDto.destination != undefined && $scope.searchDto.destination != null) {
                    param.destination = $scope.searchDto.destination;
                }
                if ($scope.searchDto.origin != undefined && $scope.searchDto.origin != null) {
                    param.origin = $scope.searchDto.origin;
                }
                if ($scope.searchDto.shipmentUid != undefined && $scope.searchDto.shipmentUid != null) {
                    param.shipmentUid = $scope.searchDto.shipmentUid;
                }
                if ($scope.searchDto.partyName != undefined && $scope.searchDto.partyName != null) {
                    param.partyName = $scope.searchDto.partyName;
                }
            }
            return param;
        }
        $scope.dataChangeEvent = function(dataObj) {
            dataObj.statusChange = dataObj.lastUpdatedStatus.shipmentStatus == "Cancelled" ? true : false;
            for (var i = 0; i < dataObj.shipmentServiceList.length; i++) {
                if (dataObj.shipmentServiceList[i].lastUpdatedStatus != null && dataObj.shipmentServiceList[i].lastUpdatedStatus != undefined)
                    dataObj.shipmentServiceList[i].statusChange = dataObj.shipmentServiceList[i].lastUpdatedStatus.serviceStatus == "Cancelled" ? true : false;

            }

        }

        $scope.changeSearch = function(param) {
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }




        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.cancel();
            $scope.search();
        }




        $scope.shipmentStatusCancelled = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_MARK_AS_CANCELLED_MODIFY)) {
                if ($scope.shipment.statusChange == true) {

                    ngDialog.openConfirm({
                        template: '<p>Do you want to cancel the shipment?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        if (value == 1) {

                            $scope.saveShipmentStatus("Cancelled", false, null);
                        } else {
                            $scope.shipment.statusChange = false;
                        }
                    })

                }


            }
        }

        $scope.shipmentServiceStatusCancelled = function(service) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MARK_AS_CANCELLED_VIEW)) {

                ngDialog.openConfirm({
                    template: '<p>Do you want to cancel the shipment Service?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        console.log("inside the YES");
                        $scope.saveShipmentStatus("Cancelled", true, service.id);
                    }
                });


            }
        }


        $scope.saveShipmentStatus = function(status, isPartial, serviceId) {
            console.log("Save statusMethod is called.");

            $scope.shipmentDto = {};
            $scope.shipmentDto.shipmentId = $scope.shipment.id;
            $scope.shipmentDto.shipmentStatus = isPartial ? null : status,
                $scope.shipmentDto.serviceId = isPartial ? serviceId : null,
                $scope.shipmentDto.serviceStatus = isPartial ? status : null
            ShipmentStatusChange.status($scope.shipmentDto).$promise
                .then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.shipment = data.responseObject;
                        $scope.dataChangeEvent($scope.shipment);
                    } else {
                        console.log("Service added Failed ", data.responseDescription)
                    }
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                }, function(error) {
                    console.log("Service added Failed : ", error)
                });

        }


        $scope.back = function() {
            if ($stateParams.fromState != undefined && $stateParams.fromState != null &&
                ($stateParams.fromState != "" && $stateParams.fromState.length > 0) &&
                $stateParams.fromStateParams != undefined && $stateParams.fromStateParams != null) {
                console.log("I am in " + $stateParams.fromState);
                console.log("I am in $stateParams.fromStateParams", JSON.parse($stateParams.fromStateParams));
                $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
            } else {
                $scope.cancel();
                if ($scope.detailTab == 'active') {
                    $scope.activeSearch();
                } else {
                    $scope.search();
                }
            }
        }

        $scope.tmpsearch = function(val) {

            $scope.tmpSearchDto = $scope.detailTab == 'active' ? $scope.activeSearchDto : $scope.searchDto;

            var x = $scope.tmpSearchDto.selectedPageNumber + val;
            var y = $scope.totalRecord;

            if ((x >= y) || (x < 0))
                return;

            $scope.tmpSearchDto.selectedPageNumber = $scope.tmpSearchDto.selectedPageNumber + val;
            $scope.tmpSearchDto.recordPerPage = 1;
            $scope.hidePage = true;
            ShipmentSearch.query($scope.tmpSearchDto).$promise.then(function(
                data, status) {

                $scope.totalRecordView = data.responseObject.totalRecord;
                if (data.responseObject.searchResult != null && data.responseObject.searchResult.length != 0) {
                    ShipmentGet.get({
                            id: data.responseObject.searchResult[0].id
                        }, function(data) {

                            if (data.responseCode == 'ERR0') {
                                $scope.shipment = data.responseObject;
                                $scope.dataChangeEvent($scope.shipment);
                                $scope.hidePage = false;

                                // 	To fill the attachment table..
                                $scope.populateAttachment();

                            } else {
                                $scope.shipment = null;
                            }
                        },
                        function(error) {
                            console.log("Shipment get Failed : " + error)
                            $scope.shipment = null;
                        });



                }


            });
        }


        $scope.before = function() {
            $scope.invoiceList = [];
            $scope.afterbefore = "BEFORE";
            $scope.tmpsearch(-1);

        }
        $scope.after = function(index) {
            $scope.invoiceList = [];
            $scope.afterbefore = "AFTER";
            $scope.tmpsearch(1)
        }



        $scope.goToCOnsolShipmentView = function(consolUid) {
            $rootScope.nav_src_bkref_key = new Date().toISOString();
            ConsolGetByUid.get({
                consolUid: consolUid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    var param = {
                        consolId: data.responseObject.id,
                        nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                        fromShipmentId: $scope.shipment.id,
                        fromState: $state.current.name,
                        fromStateParams: JSON.stringify($stateParams)
                    }
                    $state.go("layout.viewNewAirConsol", param);
                }
            }, function(error) {
                console.log("Consol get Failed : " + error)
            });
        }

        $scope.deleteMaster = function() {
            console.log("Delete Button is pressed....");

            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected shipment ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                console.log("delete shipment")
            }, function(value) {
                console.log("deleted cancelled");

            });


        }




        $scope.serialNo = function(index) {
            var index = index + 1;
            return index;
        }



        /* attachment */


        $scope.attachConfig = {
            "isEdit": false, //false for view only mode
            "isDelete": true, //
            "page": "shipment",
            "columnDefs": [{
                    "name": "Document Name",
                    "model": "documentMaster.documentName",
                    "type": "text"
                },
                {
                    "name": "Reference #",
                    "model": "refNo",
                    "type": "text"
                },
                {
                    "name": "Customs",
                    "model": "isCustoms",
                    "type": "text"
                },
                {
                    "name": "Protected",
                    "model": "isProtected",
                    "type": "text"
                },
                {
                    "name": "File Name",
                    "model": "fileName",
                    "type": "file"
                }
            ]

        }


        /*			$scope.populateAttachment = function() {
        				
        				console.log("Populating Attachment....");
        				
        				$scope.attachConfig.data = [];
        				
        				if($scope.shipment.shipmentServiceList != null && $scope.shipment.shipmentServiceList.length != 0){
        					
        					console.log("There are " + $scope.shipment.shipmentServiceList.length +" services...");
        					
        					for(var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {
        						
        						if($scope.shipment.shipmentServiceList[i].shipmentAttachmentList != null && $scope.shipment.shipmentServiceList[i].shipmentAttachmentList.length != 0) {
        							
        							console.log("There are " + $scope.shipment.shipmentServiceList[i].shipmentAttachmentList.length + " attachments.....");
        							
        							for(var j = 0; j < $scope.shipment.shipmentServiceList[i].shipmentAttachmentList.length; j++) {
        								var displayObject = new Object();
        								
        								displayObject.id =  $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].id;
        								displayObject.refNo =  $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].refNo;
        								displayObject.documentMaster = $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].documentMaster;
        								displayObject.customs = $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].customs;
        								
        								
        								displayObject.unprotectedFileName =   $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].unprotectedFileName;
        								displayObject.unprotectedFileContentType =  $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].unprotectedFileContentType;
        								displayObject.unprotectedFile =  $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].unprotectedFile;
        								
        								displayObject.protectedFileName =   $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].protectedFileName;
        								displayObject.protectedFileContentType =  $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].protectedFileContentType;
        								displayObject.protectedFile = $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].protectedFile;
        								
        								
        								displayObject.systemTrack = $scope.shipment.shipmentServiceList[i].shipmentAttachmentList[j].systemTrack;
        								
        								
        								$scope.attachConfig.data.push(displayObject);
        							}
        						} else {
        							console.log("There is no attachements....");
        						}
        					}
        				}		
        				else {
        					console.log("There is no service in this shipment...");
        				}
        				
        				console.log("Populating Attachment Over.....");
        			}

        */
        $scope.downloadAttach = function(param) {
            console.log("download ", param);
            if (param.data.id != null && param.data.file == null) {
                console.log("API CALL")
                $http({
                    url: $rootScope.baseURL + '/api/v1/shipment/files/' + param.data.id + '/' + param.type + '/false',
                    method: "GET",
                    responseType: 'arraybuffer'
                }).success(function(data, status, headers, config) {
                    console.log("hiddenElement ", data)
                    var blob = new Blob([data], {});
                    console.log("blob ", blob);
                    saveAs(blob, param.data.fileName);
                }).error(function(data, status, headers, config) {
                    //upload failed
                });
            }
        }



        $scope.documentIdList = [];
        $scope.reportList = [];
        $scope.reportCountList = [];
        $scope.reportDetailData = {};
        var allRepoVar = {};
        $scope.allRepoVarModel = {};
        $scope.allRepoVarModel.allReportSelected = false;
        $scope.reportLoadingImage = {};
        $scope.reportLoadingImage.status = false;


        $scope.reportPopUp = function(sd, resourceRefId) {
            $scope.serviceObject = angular.copy(sd);
            //if($scope.hasAccess('CRM_SHIPMENT_SERVICE_REPORTS_VIEW')) {
            $rootScope.mainpreloder = true;
            $scope.reportList = [];
            //console.log("reportListMaking  Tab :: ",resourceRefId);
            $scope.moreAddressTab = 'reports';
            ShipmentDocumentIdList.get({
                    serviceId: resourceRefId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.documentIdList = data.responseObject;
                        console.log("$scope.documentIdList -- ", $scope.documentIdList);
                        if ($scope.documentIdList.length > 0) {
                            for (var i = 0; i < $scope.documentIdList.length; i++) {
                                if (sd.serviceMaster.importExport == 'Import') {
                                    $scope.reportDetailData = {
                                        resourceRefId: $scope.documentIdList[i].id,
                                        isDocument: true,
                                        isService: false,
                                        documentNo: $scope.documentIdList[i].documentNo,
                                        reportName: "CARGO_ARRICAL_NOTICE",
                                        reportDisplayName: 'Cargo Arrival Notice | Document # ' + $scope.documentIdList[i].documentNo,
                                        key: 'cargoArrivalNotice'
                                    };
                                    $scope.reportList.push($scope.reportDetailData);
                                } else {
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "HAWB",
                                            reportDisplayName: 'HAWB| Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'hawb'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }

                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "HAWB_DRAFT",
                                            reportDisplayName: 'HAWB DRAFT| Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'hawbDraft'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "HAWB_LABEL",
                                            reportDisplayName: 'HAWB Label| Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'hawbDraft'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }

                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "BOOKING_CONFIRMATION",
                                            reportDisplayName: 'Shipment Confirmation | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'shipmentConfirmation'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "TRUCKING_INSTRUCTION",
                                            reportDisplayName: 'Trucking Instruction | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'truckingInstruction'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "SHIPPING_INSTRUCTION",
                                            reportDisplayName: 'Shipping Instruction | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'shippingInstruction'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "CERTIFICATE_OF_ORIGIN",
                                            reportDisplayName: 'Certificate Of Origin | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'certificateOfOrigin'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "STATEMENT_OF_CHARGE",
                                            reportDisplayName: 'Statement Of Charge | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'statementOfCharge'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "SUBJOB_COST_SHEET",
                                            reportDisplayName: ' Consol Profitability | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'consolProfitability'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }

                                    if ((sd.directShipment == 'No' || sd.directShipment == false)) {
                                        for (var i = 0; i < $scope.documentIdList.length; i++) {
                                            $scope.reportDetailData = {};
                                            $scope.reportDetailData = {
                                                resourceRefId: $scope.documentIdList[i].id,
                                                isDocument: true,
                                                isService: false,
                                                documentNo: $scope.documentIdList[i].documentNo,
                                                reportName: "DOCK_RECEIPT",
                                                reportDisplayName: ' Dock Receipt | Document # ' + $scope.documentIdList[i].documentNo,
                                                key: 'dockReceipt'
                                            };
                                            $scope.reportList.push($scope.reportDetailData);
                                        }
                                    }

                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "EXPORT_JOB_CARD_HAWB",
                                            reportDisplayName: ' Export Job Card(HAWB) | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'exportJobCard'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = {
                                            resourceRefId: $scope.documentIdList[i].id,
                                            isDocument: true,
                                            isService: false,
                                            documentNo: $scope.documentIdList[i].documentNo,
                                            reportName: "FORWARDERS_CARGO_RECEIPT",
                                            reportDisplayName: ' Forwarders Cargo Receipt | Document # ' + $scope.documentIdList[i].documentNo,
                                            key: 'forwardersCargoReceipt'
                                        };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }

                                }


                            }
                            $scope.assignDefaultReports(sd, resourceRefId);
                        } else {
                            $scope.assignDefaultReports(sd, resourceRefId);
                        }

                    } else {
                        $scope.assignDefaultReports(sd, resourceRefId);

                    }
                },
                function(error) {
                    $scope.assignDefaultReports(sd, resourceRefId);
                    console.log("Shipment get Failed : " + error)
                    $rootScope.mainpreloder = false;
                });
        }

        /*$scope.checkCargoRecieved=function(sd){
        	for(var i=0;i<sd.eventList.length;i++){
        		if(sd.eventList[i].eventMaster.eventMasterType!=null && sd.eventList[i].eventMaster.eventMasterType=='CARGO_RECEIVED'){
        			return true;
        			break;
        		}
        	}
        	return false;
        }*/
        $scope.assignDefaultReports = function(sd, resourceRefId) {
            if (sd.serviceMaster.importExport == 'Export') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "COMMERCIAL_INVOICE",
                    reportDisplayName: 'Commercial Invoice ',
                    key: 'commercialInvoice'
                };
                $scope.reportData = {};
                $scope.reportList.push($scope.reportDetailData);
            }

            $rootScope.mainpreloder = false;
            var myOtherModalLists = $modal({
                scope: $scope,
                templateUrl: '/app/components/crm/new-shipment/dialog/report.html',
                show: false
            });
            $rootScope.mainpreloder = false;
            myOtherModalLists.$promise.then(myOtherModalLists.show);
        }

        $scope.selectAllReports = function() {
            for (var i = 0; i < $scope.reportList.length; i++) {
                $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
            }
            if (!$scope.allRepoVarModel.allReportSelected) {
                $scope.isGenerateAll = false;
            }
        }


        $scope.chkReportStatus = function(chkFlag, detObj) {
            console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
        }

        $scope.isSingle = false;
        $scope.isMultiple = false;

        $scope.isGenerateAll = false;

        $scope.generateAll = function() {
            $scope.dataReportName = null;
            var flag = false;
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    flag = true
                    break;
                }
            }
            if (!flag) {
                $scope.isGenerateAll = false;
                Notification.warning("Please select atleast one.......)")
            } else {
                $scope.isGenerateAll = true;
                $scope.reportData = {};
                $scope.myReportModal = {};
                $scope.myReportModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/report/report_opt.html',
                    show: false
                });
                $rootScope.mainpreloder = false;
                $scope.myReportModal.$promise.then($scope.myReportModal.show);
            }
        }


        $scope.generateAllReports = function() {
            if ($scope.reportList == undefined || $scope.reportList == null || $scope.reportList.length == 0) {} else {
                //Using for mail sending for download reports 
                $scope.emailReportList = [];
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        $scope.emailReport = {};
                        $scope.emailReport.resourceId = $scope.reportList[i].resourceRefId;
                        $scope.emailReport.reportName = $scope.reportList[i].reportName;
                        $scope.emailReport.downloadOption = $scope.downloadOption;
                        $scope.emailReport.downloadFileType = $scope.downloadFileType;
                        $scope.emailReportList.push($scope.emailReport);
                    }
                }

            }
            if ($scope.emailReportList != undefined && $scope.emailReportList.length > 0) {
                $scope.isMultiple = true;
                $scope.isSingle = false;
            } else {
                $scope.isSingle = true;
                $scope.isMultiple = false;
            }
            var flag = false;
            if ($scope.isSingle) {
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        flag = true
                        $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
                        $timeout(function() {
                            $scope.reportLoadingImage.status = false;
                        }, 1000);
                    }
                    if (flag) {
                        break;
                    }
                }
                if (!flag) {
                    Notification.warning("Please select atleast one.......)");
                    return;
                }
            } else if ($scope.isMultiple) {
                $scope.downloadGenericCallMultiple($scope.emailReportList, $scope.downloadOption, $scope.isMultiple, "PDF", $scope.reportList[0].resourceRefId)
                console.log("Nothig selected");
            }
        }


        /*Report Related Codes Starts Here*/
        $scope.downloadFileType = "PDF";
        $scope.generate = {};
        $scope.generate.typeArr = ["Download", "Preview", "Print", "eMail"];

        $scope.reportModal = function(resourceRefId, reportRefName, key) {
            if ($scope.reportList != undefined && $scope.reportList != null) {
                $scope.allRepoVarModel.allReportSelected = false;
                $scope.isGenerateAll = false;
                for (var i = 0; i < $scope.reportList.length; i++) {
                    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
                }
            }

            if (key == 'cargoArrivalNotice') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_CARGO_ARRICAL_NOTICE_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'hawb') {
                if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_HAWB_DOWNLOAD)) {
                    //invoice checking
                    $scope.serviceId = [];

                    if ($scope.shipment != undefined && $scope.shipment.shipmentServiceList != undefined && $scope.shipment.shipmentServiceList.length > 0) {
                        $scope.servId = $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].id;

                        // STEP-1---Invoice Validation
                        if ($scope.servId != undefined && $scope.servId != null) {
                            $scope.serviceId.push($scope.servId);
                        }
                        if ($scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].lastUpdatedStatus.serviceStatus != "Received" && $scope.shipment.shipmentServiceList[$scope.selectedShipmentServiceIndex].lastUpdatedStatus.serviceStatus != "Generated") {
                            Notification.error($rootScope.nls["ERR90568"]);
                            $rootScope.mainpreloder = false;
                            return false;
                        }
                        appMetaFactory.reportInvoiceValidation.findByServiceId({
                            serviceId: $scope.serviceId,
                        }).$promise.then(function(
                            data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.spinner = false;
                                $scope.reportPopUpOpen(resourceRefId, reportRefName);
                            }
                        }, function(error) {
                            console.log("error while fetching service id");
                            $scope.reportPopUpOpen(resourceRefId, reportRefName);
                            $scope.spinner = false;
                            return false;
                        });

                    } else {
                        $scope.reportPopUpOpen(resourceRefId, reportRefName);
                    }
                } else {
                    return;
                }
            }
            if (key == 'hawbDraft') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_HAWB_DRAFT_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'shipmentConfirmation') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_BOOKING_CONFIRMATION_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'truckingInstruction') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_TRUCKING_INSTRUCTION_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'shippingInstruction') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_SHIPPING_INSTRUCTION_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'certificateOfOrigin') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_CERTIFICATE_OF_ORIGIN_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'statementOfCharge') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_STATEMENT_OF_CHARGE_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }

            if (key == 'consolProfitability') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_SUBJOB_COST_SHEET_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }

            if (key == 'exportJobCard') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_EXPORT_JOB_CARD_HAWB_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }

            if (key == 'forwardersCargoReceipt') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_FORWARDERS_CARGO_RECEIPT_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'dockReceipt') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_DOCK_RECEIPT_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }
            if (key == 'commercialInvoice') {
                if (!$rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_REPORTS_COMMERCIAL_INVOICE_DOWNLOAD)) {
                    return;
                }
                $scope.reportPopUpOpen(resourceRefId, reportRefName);
            }

        };
        $scope.reportPopUpOpen = function(resourceRefId, reportRefName) {

            console.log("Service ID - ", resourceRefId, " -- Name --", reportRefName);
            $scope.changeReportDownloadFormat("PDF");
            $scope.dataResourceId = resourceRefId;
            $scope.dataReportName = reportRefName;
            $scope.reportData = {};
            $scope.myReportModal = {}
            $scope.myReportModal = $modal({
                scope: $scope,
                templateUrl: '/app/components/report/report_opt.html',
                show: false
            });
            $scope.myReportModal.$promise.then($scope.myReportModal.show);

        }


        $scope.changeReportDownloadFormat = function(dType) {
            console.log("Download type Changed :: ", dType);
            $scope.downloadFileType = dType;
            if (dType === 'PDF') {
                $scope.generate.TypeArr = ["Download", "Preview", "Print"];
            } else if (dType === 'eMail') {
                $scope.generate.TypeArr = ["eMail"]
            } else {
                $scope.generate.TypeArr = ["Download"];
            }
        }

        $scope.downloadGenericCall = function(id, downOption, repoName) {
            $scope.downloadOption = downOption;
            if ($scope.isGenerateAll) {
                $scope.generateAllReports();
            } else {
                var reportDownloadRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: repoName, //HAWB,BOOKIG_CONFIRMATION,
                    single: true,
                    parameter1: $scope.reportData.parameter1,
                    parameter2: $scope.reportData.parameter2
                };
                if (reportDownloadRequest.reportName == 'CARGO_ARRICAL_NOTICE') {
                    SequenceFactoryForCAN.fetch({
                        'documentId': id
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            if (downOption === 'eMail') {
                                if (!flag) {
                                    Notification.error($rootScope.nls["ERR275"]);
                                    return false;
                                } else {
                                    $rootScope.mainpreloder = true;
                                    downloadFactory.download(reportDownloadRequest);
                                    $timeout(function() {
                                        $rootScope.mainpreloder = false;
                                        $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                                    }, 3000);
                                }
                            }
                            $rootScope.mainpreloder = true;
                            downloadFactory.download(reportDownloadRequest);
                        }
                    });
                } else {
                    //$rootScope.mainpreloder = true;
                    if (downOption === 'eMail') {
                        var flag = checkMail();
                        if (!flag) {
                            Notification.error($rootScope.nls["ERR275"]);
                            return false;
                        } else {
                            $rootScope.mainpreloder = true;
                            downloadFactory.download(reportDownloadRequest);
                            $timeout(function() {
                                $rootScope.mainpreloder = false;
                                $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                            }, 3000);
                        }
                    } else {
                        $rootScope.mainpreloder = true;
                        downloadFactory.download(reportDownloadRequest);
                    }
                }
            }

        }


        function checkMail() {
            var flag = false;
            if ($scope.serviceObject != null && $scope.serviceObject != undefined) {
                if ($scope.serviceObject.party != null && $scope.serviceObject.party != undefined) {
                    if ($scope.serviceObject.party.partyAddressList != null && $scope.serviceObject.party.partyAddressList != undefined && $scope.serviceObject.party.partyAddressList.length > 0) {

                        for (var i = 0; i < $scope.serviceObject.party.partyAddressList.length; i++) {
                            if ($scope.serviceObject.party.partyAddressList[0].addressType == "Primary") {
                                var emailList = $scope.serviceObject.party.partyAddressList[0].email;
                                if (emailList == null || emailList == undefined) {
                                    flag = false;
                                } else {
                                    flag = true;
                                    break;
                                }
                            } else {
                                flag = false;
                            }
                        }
                    }
                }
            }
            return flag;
        }

        $scope.downloadGenericCallMultiple = function(emailReportList, downOption, multiple, type, id) {
                var reportDownloadRequest = {
                    emailReportList: emailReportList,
                    downloadOption: downOption, // "Download","Preview","Print"
                    //downloadOption : $scope.downOption,
                    downloadFileType: type, //PDF
                    isMultiple: multiple,
                    resourceId: id,
                    screenType: 'Shipment'
                };
                if (downOption === 'eMail') {
                    var flag = checkMail();
                    if (!flag) {
                        Notification.error($rootScope.nls["ERR275"]);
                        return false;
                    } else {
                        $rootScope.mainpreloder = true;
                        downloadMultipleFactory.download(reportDownloadRequest);
                        $timeout(function() {
                            $rootScope.mainpreloder = false;
                            $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                        }, 3000);
                    }
                } else {
                    $rootScope.mainpreloder = false;
                    for (var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
                        var reportFileName = '';
                        reportFileName = reportDownloadRequest.emailReportList[i].reportName;
                        var reportDownloaddRequest = {
                            resourceId: id,
                            downloadOption: downOption, // "Download","Preview","Print"
                            downloadFileType: $scope.downloadFileType, //PDF
                            reportName: reportFileName,
                            single: true
                        };
                        $rootScope.mainpreloder = true;
                        downloadFactory.download(reportDownloaddRequest);
                    }
                }
            }
            //Report Related code End Here




        $scope.viewRate = function(tmpObj) {
            var newScope = $scope.$new();
            newScope.rateMerged = $scope.rateMerged;
            newScope.localCurrency = tmpObj.localCurrency.currencyCode;
            newScope.clientGrossRate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.clientGrossRate);
            newScope.clientNetRate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.clientNetRate);
            newScope.declaredCost = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.declaredCost);
            newScope.rate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.rate);
            newScope.cost = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.cost);
            var rateModal = $modal({
                scope: newScope,
                templateUrl: 'viewlocalRate.html',
                show: false
            });
            rateModal.$promise.then(rateModal.show);
        };
        $scope.showcostserivce = function(id, type) {
            var myOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/crm/shipment/views/get_cost_service_popup.html',
                show: false
            });
            $scope.docType = type;
            myOtherModal.$promise.then(myOtherModal.show);
            $timeout(function() {
                $scope.isError = true;
            }, 500);
        };
        //angular.forEach($scope.awb.data,function(item,index){
        //    $scope.awb.totalNoOfPiece+= item.noPieces;
        //    $scope.awb.totalGross+= item.grossWeight;
        //    item.total = item.chargeableWeight * item.rate_charge;
        //    $scope.awb.totalCharge+= item.total;
        //});


        /*BUTTON GROUP SCOPE VALUE*/
        $scope.btnGrp = {};
        $scope.btnGrpFilter = {};
        $scope.btnGrp.TypeServiceArr = ["Export", "Import", "Both"];
        $scope.btnGrp.TypeArr = ["Invoice", "Credit Note", "All"];
        $scope.btnGrpFilter.TypeArr = ["Customer", "Agent", "All"];
        $scope.modelOption = {};

        $scope.popup = {};


        $scope.close = function() {

            // $scope.hawbModal = $modal({scope: $scope, templateUrl: 'app/components/dynamicreport/view/awb.html',backdrop:'static',show: false});
            $scope.hawbModal.$promise.then($scope.hawbModal.hide);


        }


        $scope.agreedChange = function() {
            if ($scope.shipmentServiceDetailClone.isAgreed || $scope.shipmentServiceDetailClone.isAgreed == 'Yes') {
                $scope.documentDetailClone.ratePerChargeClone = null;

                $scope.documentDetailClone.totalAmountClone = "AS AGREED";

                $scope.shipmentServiceDetailClone.shipmentChargeListClone = [];
                $scope.shipmentServiceDetailClone.shipmentChargeListClone = new Array(6);

                if ($scope.documentDetailClone.chgs == 'P') {
                    $scope.documentDetailClone.preWeightChargeClone = "AS AGREED";
                    $scope.documentDetailClone.totalPrepaidClone = "AS AGREED";
                }
                if ($scope.documentDetailClone.chgs == 'C') {
                    $scope.documentDetailClone.collWeightChargeClone = "AS AGREED";
                    $scope.documentDetailClone.totalCollectClone = "AS AGREED";
                }
            } else {
                $scope.documentDetailClone.ratePerChargeClone = $scope.documentDetailClone.ratePerCharge;
                $scope.calculateTotalAmount();
                $scope.documentDetailClone.preWeightChargeClone = null;
                $scope.documentDetailClone.collWeightChargeClone = null;
                $scope.caluclatePrepaid();
                $scope.caluclateCollect();
                $scope.shipmentServiceDetailClone.shipmentChargeListClone = $scope.shipmentServiceDetailClone.shipmentChargeList;
                if ($scope.shipmentServiceDetailClone.shipmentChargeListClone != null && $scope.shipmentServiceDetailClone.shipmentChargeListClone.length > 0) {
                    for (var i = 0; i < $scope.shipmentServiceDetailClone.shipmentChargeListClone.length; i++) {
                        $scope.hawbCharge = {};
                        $scope.hawbCharge.company = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].company;
                        $scope.hawbCharge.country = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].country;
                        $scope.hawbCharge.chargeMaster = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].chargeMaster;
                        $scope.hawbCharge.chargeName = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].chargeName;
                        $scope.hawbCharge.ppcc = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].ppcc;
                        if ($scope.shipmentServiceDetailClone.shipmentChargeListClone[i].unitMaster.unitType == 'Unit') {
                            $scope.hawbCharge.unit = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].actualChargeable == 'CHARGEABLE' ? $scope.shipmentServiceDetailClone.bookedChargeableUnit : service.bookedGrossWeightUnitKg;
                        } else {
                            $scope.hawbCharge.unit = 1;
                        }
                        if ($rootScope.appMasterData['booking.rates.merged'] == true || $rootScope.appMasterData['booking.rates.merged'] == 'true') {
                            $rootScope.userProfile.selectedUserLocation.currencyMaster
                            $scope.hawbCharge.currency = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].rateCurrency;
                            if ($scope.hawbCharge.currency != undefined && $scope.hawbCharge.currency != null) {
                                if ($scope.hawbCharge.currency.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                                    $scope.hawbCharge.currencyRoe = 1.0;
                                } else {
                                    var roe = $rootScope.baseCurrenyRate[$scope.hawbCharge.currency.id].csell;
                                    if (roe != undefined && roe != null) {
                                        $scope.hawbCharge.currencyRoe = roe;
                                    }
                                }
                            }
                            $scope.hawbCharge.amountPerUnit = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].rateAmount;
                            //consolCharge.amount = service.shipmentChargeList[i].rateMinimum;

                        } else {
                            $scope.hawbCharge.currency = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].grossSaleCurrency;

                            if ($scope.hawbCharge.currency != undefined && $scope.hawbCharge.currency != null) {
                                if ($scope.hawbCharge.currency.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                                    $scope.hawbCharge.currencyRoe = 1.0;
                                } else {
                                    var roe = $rootScope.baseCurrenyRate[$scope.hawbCharge.currency.id].csell;

                                    if (roe != undefined && roe != null) {
                                        $scope.hawbCharge.currencyRoe = roe;
                                    }
                                }
                            }

                            $scope.hawbCharge.amountPerUnit = $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].grossSaleAmount;
                            //consolCharge.amount = service.shipmentChargeList[i].grossSaleMinimum;
                        }
                        if ($scope.hawbCharge.amountPerUnit != undefined && $scope.hawbCharge.amountPerUnit != null &&
                            $scope.hawbCharge.unit != undefined && $scope.hawbCharge.unit != null &&
                            $scope.hawbCharge.currencyRoe != undefined && $scope.hawbCharge.currencyRoe != null) {
                            $scope.hawbCharge.amount = $scope.hawbCharge.amountPerUnit * $scope.hawbCharge.unit;
                            $scope.hawbCharge.localAmount = $scope.hawbCharge.currencyRoe * $scope.hawbCharge.amount;
                        }
                        if ($scope.hawbCharge.localAmount != undefined && $scope.hawbCharge.localAmount != null)
                            $scope.shipmentServiceDetailClone.shipmentChargeListClone[i].localAmount = $rootScope.currencyFormat($scope.shipmentServiceDetailClone.localCurrency, parseFloat($scope.hawbCharge.localAmount))

                        ;
                    }

                }
                //$scope.documentDetailClone.totalAmountClone=$scope.documentDetailClone.totalAmount;
            }

            if ($scope.shipmentServiceDetailClone.isMarksNo == false || $scope.shipmentServiceDetailClone.isMarksNo == 'No') {
                $scope.documentDetailClone.marksAndNoClone = null;
            } else {
                $scope.documentDetailClone.marksAndNoClone = $scope.documentDetailClone.marksAndNo;
            }
        };

        $scope.caluclatePrepaid = function() {

            if ($scope.shipmentServiceDetailClone.isAgreed == 'No' || $scope.shipmentServiceDetailClone.isAgreed == false) {
                $scope.documentDetailClone.totalPrepaidClone = parseFloat($scope.documentDetailClone.preWeightChargeClone == null ? 0 : $scope.documentDetailClone.preWeightChargeClone) +
                    parseFloat($scope.documentDetailClone.preValuationCharge == null ? 0 : $scope.documentDetailClone.preValuationCharge) +
                    parseFloat($scope.documentDetailClone.prepaidTax == null ? 0 : $scope.documentDetailClone.prepaidTax) +
                    parseFloat($scope.documentDetailClone.otherChargesDuePreAgent == null ? 0 : $scope.documentDetailClone.otherChargesDuePreAgent) +
                    parseFloat($scope.documentDetailClone.otherChargesDuePreCarrier == null ? 0 : $scope.documentDetailClone.otherChargesDuePreCarrier)
            } else {

                $scope.documentDetailClone.totalPrepaidClone = null;
            }
            if ($scope.documentDetailClone.totalPrepaidClone == 0) {

                $scope.documentDetailClone.totalPrepaidClone = null;
            }

        }

        $scope.caluclateCollect = function() {
            if ($scope.shipmentServiceDetailClone.isAgreed == 'No' || $scope.shipmentServiceDetailClone.isAgreed == false) {
                $scope.documentDetailClone.totalCollectClone = parseFloat($scope.documentDetailClone.collWeightChargeClone == null ? 0 : $scope.documentDetailClone.collWeightChargeClone) +
                    parseFloat($scope.documentDetailClone.collValuationCharge == null ? 0 : $scope.documentDetailClone.collValuationCharge) +
                    parseFloat($scope.documentDetailClone.collectTax == null ? 0 : $scope.documentDetailClone.collectTax) +
                    parseFloat($scope.documentDetailClone.otherChargesDueCollAgent == null ? 0 : $scope.documentDetailClone.otherChargesDueCollAgent) +
                    parseFloat($scope.documentDetailClone.otherChargesDueCollCarrier == null ? 0 : $scope.documentDetailClone.otherChargesDueCollCarrier)
            } else {

                $scope.documentDetailClone.totalCollectClone = null;
            }
            if ($scope.documentDetailClone.totalCollectClone == 0) {

                $scope.documentDetailClone.totalCollectClone = null;
            }
        }



        $scope.getAccounts = function(code) {

            if (code == 0 || code == 1) {

                $scope.documentDetailClone.shipperAccount = null;
                if ($scope.documentDetailClone.shipper != undefined && $scope.documentDetailClone.shipper != null && $scope.documentDetailClone.shipper.partyAccountList != null && $scope.documentDetailClone.shipper.partyAccountList.length > 0) {

                    $scope.documentDetailClone.shipperAccount = cloneService.clone($scope.documentDetailClone.shipper.partyAccountList[0].accountMaster.accountCode);
                } else {
                    $scope.documentDetailClone.shipperAccount = null;
                }

            }

            if (code == 0 || code == 2) {

                $scope.documentDetailClone.consigneeAccount = null;
                if ($scope.documentDetailClone.consignee != undefined && $scope.documentDetailClone.consignee != null && $scope.documentDetailClone.consignee.partyAccountList != null && $scope.documentDetailClone.consignee.partyAccountList.length > 0) {

                    $scope.documentDetailClone.consigneeAccount = $scope.documentDetailClone.consignee.partyAccountList[0].accountMaster.accountCode;

                } else {

                    $scope.documentDetailClone.consigneeAccount = null;
                }
            }

            if (code == 0 || code == 3) {

                $scope.documentDetailClone.issuingAgentAccount = null;
                if ($scope.documentDetailClone.issuingAgent != undefined && $scope.documentDetailClone.issuingAgent != null && $scope.documentDetailClone.issuingAgent.partyAccountList != null && $scope.documentDetailClone.issuingAgent.partyAccountList.length > 0) {

                    $scope.documentDetailClone.issuingAgentAccount = $scope.documentDetailClone.issuingAgent.partyAccountList[0].accountMaster.accountCode;

                } else {

                    $scope.documentDetailClone.issuingAgentAccount = null;
                }

            }


        }


        $scope.calculateTotalAmount = function() {

                if ($scope.shipmentServiceDetailClone.isAgreed == "No" || $scope.shipmentServiceDetailClone.isAgreed == false && $scope.documentDetailClone.ratePerChargeClone != undefined && $scope.documentDetailClone.ratePerChargeClone != null && $scope.documentDetailClone.chargebleWeight != undefined && $scope.documentDetailClone.chargebleWeight != null)
                    $scope.documentDetailClone.totalAmountClone = parseFloat($scope.documentDetailClone.ratePerChargeClone) * parseFloat($scope.documentDetailClone.chargebleWeight);
            }
            /*$scope.calculateTotal=function(){
            	var total=0.0;
            	if($scope.documentDetailClone.dimensionList[j].rateCharge!=undefined && $scope.documentDetailClone.dimensionList[j].chargebleWeight!=undefined)
            	total=parseFloat(total) + (parseFloat($scope.documentDetailClone.dimensionList[j].rateCharge)*parseFloat($scope.documentDetailClone.dimensionList[j].chargebleWeight))
            	$scope.documentDetailClone.totalAmount=total;
            }*/

        var specialElementHandlers = {
            '#content': function(element, renderer) {
                return true;
            }
        };


        $scope.serviceToConsol = function(shipment, service) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_ATTACH_TO_MASTER_VIEW)) {
                uiShipmentFactory.isConsolCreatedFromShipment.query({
                    serviceUid: service.serviceUid
                }).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (uiShipmentDataService.serviceToConsol(shipment, service)) {
                            uiShipmentDataService.goToConsol(shipment, service);
                        }
                    } else {
                        Notification.warning($rootScope.nls["ERR96560"]);
                    }
                }, function(error) {
                    console.log("serviceToConsol view Failed : " + error)
                });
            }
        }

        $scope.refmodel = function() {
            var myTotalOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/crm/shipment/views/refpopup.html',
                show: false
            });
            myTotalOtherModal.$promise.then(myTotalOtherModal.show);
        };

        // invoice list under shipment service


        //Provisional cost start

        $scope.refmodel = function(provisionalItem) {
            var newScope = $scope.$new();
            newScope.provisionalItem = provisionalItem;
            var myTotalOtherModal = $modal({
                scope: newScope,
                templateUrl: 'app/components/finance/provisional_cost/view/refpopup.html',
                show: false
            });
            myTotalOtherModal.$promise.then(myTotalOtherModal.show);
        };


        var signOffModel;
        //Sign Off Modal
        $scope.serviceSignOffModal = function(serviceObject, index) {


            $scope.serviceObject = angular.copy(serviceObject);
            $scope.shipmentServiceSignOffIndex = index;
            if ($scope.serviceObject.shipmentServiceSignOff == undefined || $scope.serviceObject.shipmentServiceSignOff == null) {
                $scope.serviceObject.shipmentServiceSignOff = {};
                $scope.serviceObject.shipmentServiceSignOff.signOffBy = $rootScope.userProfile.employee;
                $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
                $scope.serviceObject.shipmentServiceSignOff.shipmentUid = serviceObject.shipmentUid;
                $scope.serviceObject.shipmentServiceSignOff.serviceUid = serviceObject.serviceUid;
                $scope.serviceObject.shipmentServiceSignOff.isSignOff = true;
                $scope.serviceObject.shipmentServiceSignOff.consolUid = serviceObject.consolUid;
                $scope.serviceObject.shipmentServiceSignOff.shipmentServiceDetail = serviceObject.id;
            } else {
                $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString($scope.serviceObject.shipmentServiceSignOff.signOffDate);
            }
            signOffModel = $modal({
                scope: $scope,
                templateUrl: 'app/components/crm/shipment/views/service_signoff_popup.html',
                show: false
            });
            signOffModel.$promise.then(signOffModel.show);


        };

        /*Salesman Select Picker*/
        $scope.ajaxEmployeeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return EmployeeList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.customerServiceList = data.responseObject.searchResult;
                        console.log("$scope.customerServiceList", $scope.customerServiceList);
                        return $scope.customerServiceList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Customer Services');
                }
            );

        }
        $scope.ajaxCommentEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return commentMasterFactory.keyword.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.commentList = data.responseObject.searchResult;
                        console.log("$scope.commentList", $scope.commentList);
                        return $scope.commentList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Customer Services');
                }
            );

        }

        $scope.selectedCommentMaster = function() {
            $scope.serviceObject.shipmentServiceSignOff.description = $scope.serviceObject.shipmentServiceSignOff.commentMaster.description;
        }


        $scope.saveServiceSignOff = function(signOffObject) {
            console.log("Sign Off Object", signOffObject);
            // close the model

            signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
            if (signOffObjectCopy.signOffDate != null)
                signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);

            appMetaFactory.shipmentService.signOff(signOffObjectCopy).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    signOffModel.$promise.then(signOffModel.hide);
                    var stateparamsRef = JSON.parse(JSON.stringify($stateParams));
                    stateparamsRef.readCount = parseInt(stateparamsRef.readCount) + 1;
                    $state.go('layout.viewCrmShipment', stateparamsRef);
                } else {
                    Notification.error($rootScope.nls[data.responseCode]);
                    console.log("Sign off Failed", data.responseDescription);
                }
            }, function(error) {
                console.log("Sign off Failed", error);
            });

        }


        var unSignOffModel;
        //Sign Off Modal
        $scope.serviceUnSignOffModal = function(serviceObject, index) {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_MORE_UNSIGN_OFF_MODIFY)) {

                $scope.serviceObject = serviceObject;

                $scope.serviceObject.shipmentServiceSignOff.signOffBy = $rootScope.userProfile.employee;
                $scope.serviceObject.shipmentServiceSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());

                $scope.shipmentServiceSignOffIndex = index;
                unSignOffModel = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/crm/shipment/views/service_unsignoff_popup.html',
                    show: false
                });
                unSignOffModel.$promise.then(unSignOffModel.show);

            }
        };

        $scope.saveServiceUnSignOff = function(signOffObject) {
            // close the model


            signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
            if (signOffObjectCopy.signOffDate != null)
                signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);

            signOffObjectCopy.isSignOff = false;

            appMetaFactory.shipmentServiceUnSignOff.unSignOff(signOffObjectCopy).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    unSignOffModel.$promise.then(unSignOffModel.hide);
                    var stateparamsRef = JSON.parse(JSON.stringify($stateParams));
                    stateparamsRef.readCount = parseInt(stateparamsRef.readCount) + 1;
                    $state.go('layout.viewCrmShipment', stateparamsRef);
                } else {

                    Notification.error($rootScope.nls[data.responseCode]);

                    console.log("Sign off Failed", data.responseDescription);
                }
            }, function(error) {
                console.log("Sign off Failed", error);
            });
        }

        // Provisional cost end


        //AES code

        $scope.aesSearchforService = function(service) {
            if (service.serviceUid != undefined && service.serviceUid != null) {
                $scope.searchDto = {};
                $scope.searchDto.serviceUid = service.serviceUid;
                AesSearch.query($scope.searchDto).$promise.then(function(data, status) {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    var tempArr = [];
                    $scope.finalArr = [];
                    var tempArr = data.responseObject.searchResult;
                    var tempObj = {};
                    angular.forEach(tempArr, function(item, index) {
                        tempObj = item;
                        tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                        if (tempObj.id != undefined && tempObj.id != null) {
                            tempObj.status = 'Generated'
                            tempObj.shipperName = tempObj.shipperName;
                            tempObj.consigneeName = tempObj.consigneeName;
                        }
                        $scope.finalArr.push(tempObj);
                    });

                    if ($scope.finalArr != null && $scope.finalArr.length > 0) {} else {
                        var tempObj = {};
                        tempObj.serviceUid = service.serviceUid;
                        tempObj.consolUid = service.consolUid;
                        tempObj.status = 'Active';
                        if (service.documentList != null && service.documentList[0] != undefined && service.documentList[0].length != 0) {
                            tempObj.hawbNo = service.documentList[0].hawbNo;
                            tempObj.shipperName = service.documentList[0].shipper.partyName;
                            tempObj.consigneeName = service.documentList[0].consignee.partyName;
                        }
                        tempObj.mawbNo = service.mawbNo;
                        tempObj.shipmentUid = service.shipmentUid;
                        $scope.finalArr.push(tempObj);
                    }



                    return $scope.finalArr;
                });
            } else {
                return;
            }
        }

        $scope.aesGenerateView = function(aesFile) {
            var fromScreen = {};
            fromScreen = $state.current.name;
            if (aesFile != undefined && aesFile != null) {
                ServiceGetByUid.get({
                    serviceUid: aesFile.serviceUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.serviceObj = data.responseObject;
                        if ($scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || $scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                            $state.go('layout.aesView', {
                                aesId: aesFile.id,
                                fromScreen: fromScreen,
                                shipmentUid: $scope.serviceObj.shipmentUid
                            });
                        } else {
                            $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                            return false;
                        }
                    }
                }, function(error) {
                    console.log("Consol get Failed : " + error)
                });
            }
        }




        $scope.aesGenerateAdd = function(aesFile) {
            console.log("aesGenerateAdd method called");
            $rootScope.clientMessage = null;
            var fromScreen = {};
            fromScreen = $state.current.name;
            if (aesFile != undefined && aesFile != null) {
                ServiceGetByUid.get({
                    serviceUid: aesFile.serviceUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.serviceObj = data.responseObject;
                        if ($scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || $scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                            var navAesObj = {
                                selectedPageNumber: $scope.detailTab == 'active' ? $scope.activeSearchDto.selectedPageNumber : $scope.searchDto.selectedPageNumber,
                                status: $scope.detailTab,
                                totalRecord: $scope.detailTab == 'active' ? $scope.totalRecordView : $scope.totalRecord,
                                shipmentId: $scope.shipment.id,
                                serviceUid: $scope.serviceObj.serviceUid,
                                shipmentUid: $scope.serviceObj.shipmentUid,
                                consolUid: $scope.serviceObj.consolUid,
                                etd: $rootScope.dateAndTimeToString($scope.serviceObj.etd),
                                carrier: $scope.serviceObj.carrier,
                                shipper: $scope.serviceObj.documentList[0].shipper,
                                consignee: $scope.serviceObj.documentList[0].consignee,
                                fromState: $state.current.name,
                                fromScreen: fromScreen,
                                mawbNo: $scope.serviceObj.mawbNo,
                                hawbNo: $scope.serviceObj.documentList[0].hawbNo
                            };

                            if ($scope.serviceObj.serviceMaster.transportMode == 'Air') {
                                navAesObj.iataOrScacCode = $scope.serviceObj.carrier.iataCode;
                            } else if ($scope.serviceObj.serviceMaster.transportMode == 'Ocean') {
                                navAesObj.iataOrScacCode = $scope.serviceObj.carrier.scacCode;
                            }
                            $state.go("layout.addAes", {
                                navAesObj: navAesObj
                            });
                        } else {
                            $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                            $scope.spinner = false;
                            return false;
                        }
                    }
                    $scope.spinner = false;
                }, function(error) {
                    console.log("Consol get Failed : " + error);
                    $scope.spinner = false;
                });
            } else {
                $scope.spinner = false;
            }
        };

        // Go to shipment link

        $scope.goToShipmentViewByServiceUid = function(serviceuid) {
            console.log("State  : ", $state)
            console.log("State Param  : ", $stateParams)
            console.log("serviceuid ", serviceuid)
            ShipmentIdByServiceUid.get({
                    serviceuid: serviceuid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var param = {
                            shipmentId: data.responseObject,
                            fromState: $state.current.name,
                            fromStateParams: JSON.stringify($stateParams)

                        }
                        $state.go("layout.viewCrmShipment", param);
                    }
                },
                function(error) {
                    console.log("Shipment get Failed : " + error)
                });

        }

        //AES CodE


        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                if ($scope.shipmentServiceDetailClone.shipmentChargeListClone != undefined && $scope.shipmentServiceDetailClone.shipmentChargeListClone[i] != undefined) {
                    input.push($scope.shipmentServiceDetailClone.shipmentChargeListClone[i]);
                }
            }
            return input;
        };

        $scope.getPhoneandFax = function(partyObj) {
            $scope.partyPhandFax = "";
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].phone != undefined && partyObj.partyAddressList[i].phone != null) {
                    if ($scope.partyPhandFax.includes("PH")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].phone;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "PH:" + partyObj.partyAddressList[i].phone;
                    }
                }
            }
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].fax != undefined && partyObj.partyAddressList[i].fax != null) {
                    if ($scope.partyPhandFax.includes("FAX")) {
                        $scope.partyPhandFax = +" " + $scope.partyPhandFax + "," + partyObj.partyAddressList[i].fax;
                    } else {
                        $scope.partyPhandFax = +" " + $scope.partyPhandFax + "FAX:" + partyObj.partyAddressList[i].fax;
                    }
                }

            }
            return $scope.partyPhandFax;
        }

        $scope.goToQuotationView = function(quotationNo) {
            $rootScope.nav_src_bkref_key = new Date().toISOString();
            QuotationViewByQuotationNo.get({
                quotationNumber: quotationNo
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    var param = {
                        quotationId: data.responseObject.id,
                        nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                        fromShipmentId: $scope.shipment.id,
                        fromState: $state.current.name,
                        fromStateParams: JSON
                            .stringify($stateParams)
                    }
                    $state.go("layout.salesQuotationView", param);
                }
            }, function(error) {
                console.log("Consol get Failed : " + error)
            });
        }
        $scope.singlePageNavigation = function(val) {
            if ($stateParams != undefined && $stateParams != null) {

                if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                    return;

                var stateParameters = {};

                $scope.searchDto = {};
                $scope.searchDto.recordPerPage = 1;
                $scope.searchDto.serviceStatus = $scope.detailTab;
                stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

                if ($stateParams.importExport != undefined && $stateParams.importExport != null) {
                    stateParameters.importExport = $scope.searchDto.importExport = $stateParams.importExport;
                }
                $scope.searchDto.shipmentDate = {};
                if ($stateParams.shipmentDateStartDate != undefined && $stateParams.shipmentDateStartDate != null) {
                    stateParameters.shipmentDateStartDate = $scope.searchDto.shipmentDate.startDate = $stateParams.shipmentDateStartDate;
                }
                if ($stateParams.shipmentDateEndDate != undefined && $stateParams.shipmentDateEndDate != null) {
                    stateParameters.shipmentDateEndDate = $scope.searchDto.shipmentDate.endDate = $stateParams.shipmentDateEndDate;
                }

                if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                    stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
                }
                if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                    stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
                }
                if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                    stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
                }
                if ($stateParams.shipmentStatus != undefined && $stateParams.shipmentStatus != null) {
                    stateParameters.shipmentStatus = $scope.searchDto.serviceStatus = $stateParams.shipmentStatus;
                }
                if ($stateParams.routedBy != undefined && $stateParams.routedBy != null) {
                    stateParameters.routedBy = $scope.searchDto.routedBy = $stateParams.routedBy;
                }
                if ($stateParams.routed != undefined && $stateParams.routed != null) {
                    stateParameters.routed = $scope.searchDto.routed = $stateParams.routed;
                }
                if ($stateParams.status != undefined && $stateParams.status != null) {
                    stateParameters.status = $scope.searchDto.status = $stateParams.status;
                }
                if ($stateParams.createdBy != undefined && $stateParams.createdBy != null) {
                    stateParameters.createdBy = $scope.searchDto.createdBy = $stateParams.createdBy;
                }
                if ($stateParams.tos != undefined && $stateParams.tos != null) {
                    stateParameters.tos = $scope.searchDto.tos = $stateParams.tos;
                }
                if ($stateParams.destination != undefined && $stateParams.destination != null) {
                    stateParameters.destination = $scope.searchDto.destination = $stateParams.destination;
                }
                if ($stateParams.origin != undefined && $stateParams.origin != null) {
                    stateParameters.origin = $scope.searchDto.origin = $stateParams.origin;
                }
                if ($stateParams.shipmentUid != undefined && $stateParams.shipmentUid != null) {
                    stateParameters.shipmentUid = $scope.searchDto.shipmentUid = $stateParams.shipmentUid;
                }
                if ($stateParams.partyName != undefined && $stateParams.partyName != null) {
                    stateParameters.partyName = $scope.searchDto.partyName = $stateParams.partyName;
                }
                ShipmentSearch.query($scope.searchDto).$promise.then(function(
                    data, status) {

                    stateParameters.totalRecord = data.responseObject.totalRecord;
                    stateParameters.shipmentId = data.responseObject.searchResult[0].id;

                    $state.go("layout.viewCrmShipment", stateParameters);
                });
            } else {
                return;
            }
        }



        $scope.viewById = function(id) {
            ShipmentGet.get({
                id: id
            }, function(data) {
                $rootScope.setNavigate1("CRM");
                $rootScope.setNavigate2("Shipment");
                $scope.populateShipmentData(data);
            }, function(error) {
                console.log("Shipment view Failed : " + error)
            });
        }

        $scope.viewByUid = function(uid) {
            ShipmentGetFromUID.get({
                shipmentuid: uid
            }, function(data) {
                $rootScope.setNavigate1("CRM");
                $rootScope.setNavigate2("Shipment");
                $scope.populateShipmentData(data);
            }, function(error) {
                console.log("Shipment view Failed : " + error)
            });
        }

        $scope.populateShipmentData = function(data) {
            if (data.responseCode == 'ERR0') {

                $rootScope.setNavigate1("CRM");
                $rootScope.setNavigate2("Shipment");
                $scope.shipment = data.responseObject;

                var flag = false;

                for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {

                    if (!flag) {
                        if ($scope.shipment.shipmentServiceList[i].location.id == $rootScope.userProfile.selectedUserLocation.id) {

                            $scope.changeShipmentServiceView(i, 'serviceRouting');

                            flag = true;
                        }
                    }
                }
                $scope.showDetail = true;
                $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber);
                $scope.searchDto.totalRecord = parseInt($stateParams.totalRecord);
                $scope.dataChangeEvent($scope.shipment);

                $scope.totalRecordView = $scope.totalRecord;
                $scope.hidePage = false;

                //rate booking configuration
                $scope.showRateEdit = true;
                $scope.showDocumentEdit = true;
                $scope.enableBookingRates = $rootScope.appMasterData['booking.rates.merged'];

                if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.party != undefined &&
                    $scope.shipment.party != null && $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
                    $rootScope.subTitle = $scope.shipment.party.partyName;
                } else {
                    $rootScope.subTitle = "Unkonwn Party";
                }
                $rootScope.category = "Shipment";
                $rootScope.serviceCodeForUnHistory = "";
                $rootScope.orginAndDestinationUnHistory = "";
                if ($scope.shipment != undefined && $scope.shipment != null) {
                    if ($scope.shipment.party != null && $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
                        $rootScope.subTitle = $scope.shipment.party.partyName;
                    } else {
                        $rootScope.subTitle = "Unkonwn Party";
                    }
                    if ($scope.shipment.origin != undefined && $scope.shipment.origin != null) {
                        $rootScope.serviceCodeForUnHistory = $scope.shipment.origin.portGroupCode;
                    }

                    if ($scope.shipment.destination != undefined && $scope.shipment.destination != null) {
                        $rootScope.orginAndDestinationUnHistory = $scope.shipment.destination.portGroupCode;
                    }

                } else {
                    $rootScope.subTitle = "Unkonwn Party";
                    $rootScope.serviceCodeForUnHistory = "";
                    $rootScope.orginAndDestinationUnHistory = "";
                }
                var rHistoryObj = {
                    'title': 'Shipment View #' + $scope.shipment.shipmentUid,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'Shipment',
                    'serviceType': 'AIR',
                    'serviceCode': $rootScope.serviceCodeForUnHistory,
                    'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                    'showService': true
                }

                RecentHistorySaveService.form(rHistoryObj);
            } else {
                $scope.shipment = null;
                console.log("Shipment view Failed " +
                    data.responseDescription)
            }

            if ($stateParams.fromQuoId != null && $stateParams.fromQuoId != undefined && $stateParams.fromQuoId != "") {
                $scope.showActionButton = false;
            }

            if ($stateParams.selectedPageNumber == null || $stateParams.selectedPageNumber == undefined) {
                $scope.showActionButton = false;
            }
        }




        $scope.moveToConsol = function(consolUid) {

            $rootScope.nav_src_bkref_key = new Date().toISOString();
            ConsolView.get({
                consolUid: consolUid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    var param = {
                        consolUid: data.responseObject.consolUid,
                        nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                        fromShipmentId: $scope.shipment.id,
                        fromState: $state.current.name,
                        fromStateParams: JSON
                            .stringify($stateParams)
                    }
                    $state.go("layout.viewNewAirConsol", param);
                }
            }, function(error) {
                console.log("Consol get Failed : " + error)
            });

        }


        $scope.add = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_CREATE)) {
                $state.go("layout.addNewShipment");
                /*$state.go("layout.addCrmShipment");*/
            }

        };

        $scope.edit = function() {
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_MODIFY) && $scope.shipment.id != undefined) {
                var localRefStateParam = {
                    shipmentId: $scope.shipment.id
                };
                console.log("State Parameters :: ", localRefStateParam);
                $state.go("layout.editNewShipment", localRefStateParam);
                /*$state.go("layout.editCrmShipment", localRefStateParam);*/
            }
        };

        $scope.documentListTab = 0;

        $scope.clickDocumentIndex = function(index) {
            $scope.documentListTab = null;
            $scope.documentListTab = index;
        };

        if ($stateParams.action != undefined && $stateParams.action != null && $stateParams.action == "VIEW") {
            console.log("I am In Shipment View Page");
            $rootScope.unfinishedFormTitle = "Shipment View # " + $stateParams.shipmentId;
            $rootScope.unfinishedData = undefined;
            $scope.selectedShipmentServiceIndex = 0;

            //$scope.view($stateParams.shipmentId, $stateParams.selectedPageNumber);

            if ($stateParams.shipmentId != undefined && $stateParams.shipmentId != null) {
                $scope.viewById($stateParams.shipmentId);
            } else if ($stateParams.shipmentUid != undefined && $stateParams.shipmentUid != null) {
                $scope.viewByUid($stateParams.shipmentUid);
            }

            if ($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
                $scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
            }
            if ($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
                $scope.totalRecord = parseInt($stateParams.totalRecord);
            }

            if ($stateParams.fromState != undefined && $stateParams.fromState != null && ($stateParams.fromState == "layout.viewNewAirConsol" || $stateParams.fromState == "layout.editAirConsol" || $stateParams.fromState == "layout.crmImportToExport")) {
                console.log("I am in " + $stateParams.fromState);
                $scope.showActionButton = false;
            } else if ($stateParams.fromState != undefined && $stateParams.fromState != null && ($stateParams.fromState == "layout.myIndividualAirExportTask" || $stateParams.fromState == "layout.myIndividualAirImportTask")) {
                console.log("I am in " + $stateParams.fromState);
                $scope.showActionButton = true;
            }
            if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_SERVICE_ROUTING_VIEW)) {
                $scope.shipmentTab = 'serviceRouting';
            }
            $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.crmShipment"
            }, {
                label: "Shipment",
                state: "layout.crmShipment"
            }, {
                label: "View Shipment",
                state: null
            }];
        } else {
            console.log("I am In Shipment List Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.crmShipment"
            }, {
                label: "Shipment",
                state: null
            }];
        }
        $scope.copy_Shipment = function() {
            var myTotalOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/crm/shipment/views/copy_shipment_popup.html',
                show: false
            });
            myTotalOtherModal.$promise.then(myTotalOtherModal.show);
        };



        /*AUTHENTICATED DOC POPUP*/
        $scope.authInfo = function(authenticatedDoc) {
            var newScope = $scope.$new();
            newScope.authenticatedDoc = authenticatedDoc;
            var myOtherModal = $modal({
                scope: newScope,
                backDrop: true,
                templateUrl: 'app/components/crm/shipment/views/auth_doc_moreinfo_popup.html',
                show: false
            });
            myOtherModal.$promise.then(myOtherModal.show);
        };
        $scope.goToShipmentService = function(shipmentUid) {

            var param = {
                shipmentUid: shipmentUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.viewCrmShipment', param);
        }
        $scope.goToConsol = function(consolUid) {
            var param = {
                consolUid: consolUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.viewAirConsol', param);
        }


        //	initMap("Chennai", "Thirunelvali, TamilNadu");


        //Added by K.Sathish Kumar for new shipment screen

        $scope.changeShipmentServiceView = function(index, tabName) {
            $scope.selectedShipmentServiceIndex = index;
            if (tabName != undefined && tabName != null) {
                if (tabName == 'serviceRouting' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_SERVICE_ROUTING_VIEW)) {
                    $scope.shipmentTab = tabName;
                } else {
                    $scope.shipmentTab = null;
                }
                if (tabName == 'cargoDetails' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_CARGO_DETAILS_VIEW)) {
                    $scope.shipmentTab = tabName;
                }
                if (tabName == 'hawb' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_HAWB_VIEW)) {
                    $scope.shipmentTab = tabName;
                }
                if (tabName == 'pickupDelivery' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_PICKUP_DELIVERY_VIEW)) {
                    $scope.shipmentTab = tabName;
                }
                if (tabName == 'aes&Rates' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_AES_RATES_VIEW)) {
                    $scope.shipmentTab = tabName;
                }
                if (tabName == 'others' && $rootScope.roleAccess(roleConstant.CRM_SHIPMENT_SERVICE_OTHERS_VIEW)) {
                    $scope.shipmentTab = tabName;
                }
            }
        }



        //shipment milestone code started here

        $scope.trackShipment = function(shipmentId) {
                ShipmentTrack.get({
                        shipmentId: shipmentId
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            addtrackModal.$promise.then(addtrackModal.show);
                            $scope.shipmentTrackList = uiShipmentDataService.prepareToolTipData(data.responseObject);
                        } else {
                            Notification.error($rootScope.nls[data.responseCode]);
                        }
                    },
                    function(error) {
                        console.log("track Shipment Failed : " + error)
                    });
            }
            //shipment milestone code ends here



        //Purchase order


        //default check and get po items
        $scope.hidePO = function(serv) {
            poModel.$promise.then(poModel.hide);
        }

        $scope.savePurchaseOrder = function() {
            poModel.$promise.then(poModel.hide);
        }

        $scope.moreinfoPopup = function(parentindex, index) {
            $scope.poIndex = index;
            $scope.poParentIndex = parentindex;

            var myPoMoreModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/crm/new-shipment/dialog/shipment_po_moreinfo_popup.html',
                show: false
            });
            myPoMoreModal.$promise.then(myPoMoreModal.show);
        };

        var poModel = $modal({
            scope: $scope,
            // backdrop: 'static',
            templateUrl: 'app/components/crm/new-shipment/dialog/shipment_purchase_order_popup.html',
            show: false
        });

        $scope.showPurchaseOrderPopup = function(service, index) {

            if (service != undefined && service.purchaseOrder != undefined && service.purchaseOrder != null && service.purchaseOrder.id != undefined) {
                //$scope.checkAndGetPurchaseOrder(service.poNo,index); 
            } else if (service) {
                service.purchaseOrder = null;
            }
            poModel.$promise.then(poModel.show)
        }

    }
]);