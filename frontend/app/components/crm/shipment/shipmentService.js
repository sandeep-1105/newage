
app.service('attachToMasterService',['$rootScope','$state', function($rootScope,$state){
	
	
	this.isDimensionFound = function (service){
		
		var flag=false;
		
		if(service.documentList!=undefined 
				&& service.documentList!=null
				&& service.documentList.length!=0){
			for(var i=0;i<service.documentList.length;i++){
				if(service.documentList[i].dimensionList!=undefined
						&& service.documentList[i].dimensionList!=null
						&& service.documentList[i].dimensionList.length!=0){
					if(service.documentList[i].dimensionList.length==1){
						var keyNames = Object.keys(service.documentList[i].dimensionList[0]);
						if(keyNames.length==0){
							continue;
						}
					}
					flag=true;
					break;
				}
			}
		}
		
		return flag;
	}
	
	this.chargableWeightCalulcate = function (service, whichValue){
		
		if(service.bookedVolumeWeightUnitPound!=undefined && service.bookedVolumeWeightUnitPound!=null && whichValue=='volumePound'){
			try{
				var weight = $rootScope.roundOffWeight(((parseFloat(service.bookedVolumeWeightUnitPound) * 0.453592) * 100)/100);
				service.bookedVolumeWeightUnitKg = weight;
				
			}catch(err){
				service.bookedVolumeWeightUnitPound = 0.0;
				service.bookedVolumeWeightUnitKg = 0.0;
			}
		}
		
		if(service.bookedGrossWeightUnitPound!=undefined && service.bookedGrossWeightUnitPound!=null && whichValue=='grossPound'){
			try{
				var weight = $rootScope.roundOffWeight(((parseFloat(service.bookedGrossWeightUnitPound) * 0.453592) * 100)/100);
				service.bookedGrossWeightUnitKg = weight;
			}catch(err){
				service.bookedGrossWeightUnitPound = 0.0;
				service.bookedGrossWeightUnitKg = 0.0;
			}
		}
		
		
		if(service.bookedVolumeWeightUnitKg!=undefined && service.bookedVolumeWeightUnitKg!=null && whichValue=='volumekg'){
			try{
				var weight = Math.round((parseFloat(service.bookedVolumeWeightUnitKg) * 2.20462) * 100) / 100;
				service.bookedVolumeWeightUnitPound = weight;
			}catch(err){
				service.bookedVolumeWeightUnitKg = 0.0;
				service.bookedVolumeWeightUnitPound = 0.0;
			}
		}
		
		if(service.bookedGrossWeightUnitKg!=undefined && service.bookedGrossWeightUnitKg!=null && whichValue=='grossKg'){
			try{
				
				var weight = Math.round((parseFloat(service.bookedGrossWeightUnitKg) * 2.20462) * 100) / 100;
				service.bookedGrossWeightUnitPound = weight;
			}catch(err){
				service.bookedGrossWeightUnitKg = 0.0;
				service.bookedGrossWeightUnitPound = 0.0;
			}
		}
		
	
		
		this.calculateChargeWeight(service);
	}

	this.calculateChargeWeight = function(service){
		var volumeKg = parseFloat(service.bookedVolumeWeightUnitKg);
		var grossKg = parseFloat(service.bookedGrossWeightUnitKg);
		
		if(volumeKg>0 && grossKg>0){
			
			if(volumeKg>grossKg){
				service.bookedChargeableUnit = volumeKg;
			}else{
				service.bookedChargeableUnit = grossKg;
			}
			
		}else if(volumeKg<=0 && grossKg>0){
			service.bookedChargeableUnit = grossKg;
		}else if(volumeKg>0 && grossKg<=0){
			service.bookedChargeableUnit = volumeKg;
		}else{
			service.bookedChargeableUnit = 0.0;
		}
		
		
		if(service.bookedChargeableUnit>0){
			var conversionUnit = null;
			
			if(service.serviceMaster.transportMode==='Air'){
				conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
			}else if(service.serviceMaster.transportMode==='Road'){
				conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
			}else if(service.serviceMaster.transportMode==='Ocean'){
				conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
			}
			
			if(conversionUnit!=null){
				service.bookedVolumeUnitCbm = parseFloat(service.bookedChargeableUnit)/parseFloat(conversionUnit);
				service.bookedVolumeUnitCbm = $rootScope.weightFormat(service.bookedVolumeUnitCbm,service.location.countryMaster.locale);
				
			}
		}else{
			service.bookedVolumeUnitCbm = 0.0;
		}
		
	}
	
	this.isDocumentDimensionFound = function (document){
		
		var flag=false;
		
		if(document.dimensionList!=undefined
				&& document.dimensionList!=null
				&& document.dimensionList.length!=0){
			
			if(document.dimensionList.length==1){
				var keyNames = Object.keys(document.dimensionList[0]);
				if(keyNames.length==0){
					flag=false
				}else{
					flag=true;
				}
			}else{
				flag=true;
			}
		}
		
		return flag;
	}

	this.calculateDocumentWeight = function(documentDetail,service) {
    	
        if (documentDetail.dimensionUnit == false) {

        	documentDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
        	
        	if(documentDetail.volumeWeight!=undefined 
        			&& documentDetail.volumeWeight!=null
        			&& documentDetail.volumeWeight!=''){
        		documentDetail.volumeWeightInPound = Math.round((documentDetail.volumeWeight * 2.20462) * 100) / 100;
        	}
        	
        	if(documentDetail.grossWeight!=undefined 
        			&& documentDetail.grossWeight!=null
        			&& documentDetail.grossWeight!=''){
        		documentDetail.grossWeightInPound = Math.round((documentDetail.grossWeight * 2.20462) * 100) / 100;
        	}
        	
        } else {
        	documentDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
        	
        	if(documentDetail.volumeWeightInPound!=undefined 
        			&& documentDetail.volumeWeightInPound!=null
        			&& documentDetail.volumeWeightInPound!=''){
        		documentDetail.volumeWeight = $rootScope.roundOffWeight(Math.round((documentDetail.volumeWeightInPound * 0.45359237) * 100) / 100);
        			
        	}
        	
        	if(documentDetail.grossWeightInPound!=undefined 
        			&& documentDetail.grossWeightInPound!=null
        			&& documentDetail.grossWeightInPound!=''){
        		documentDetail.grossWeight = $rootScope.roundOffWeight(Math.round((documentDetail.grossWeightInPound * 0.45359237) * 100) / 100);
        	}
        	
        }
   
        this.calculateDocChargeWeight(documentDetail,service);
  }
	
	this.calculateWeight = function(param, documentDetail, service) {
		
        if (param.grossWeight > 0.0 && param.volumeWeight > 0.0) {
            if (documentDetail.dimensionUnit) {
                documentDetail.grossWeight = $rootScope.roundOffWeight(param.dimensionGrossWeightInKg);
                documentDetail.grossWeightInPound = param.grossWeight; 
            } else {
                documentDetail.grossWeight = $rootScope.roundOffWeight(param.grossWeight);
                documentDetail.grossWeightInPound = Math.round((documentDetail.grossWeight * 2.20462) * 100)/100;  
            }

            documentDetail.dimensionVolumeWeight=param.volumeWeight;
    		documentDetail.dimensionGrossWeight=param.grossWeight;
    		documentDetail.dimensionGrossWeightInKg=param.dimensionGrossWeightInKg;
    		
    		
            documentDetail.volumeWeight = $rootScope.roundOffWeight(param.volumeWeight);
            documentDetail.volumeWeightInPound = Math.round((documentDetail.volumeWeight * 2.20462) * 100)/100; 
            documentDetail.noOfPieces = param.totalPieces;
            this.calculateDocChargeWeight(documentDetail,service);
        }
    }
	
	this.calculateDocChargeWeight = function(document,service){
		
		var volumeKg = parseFloat(document.volumeWeight);
		var grossKg = parseFloat(document.grossWeight);
		
		if(volumeKg>0 && grossKg>0){
			
			if(volumeKg>grossKg){
				document.chargebleWeight = volumeKg;
			}else{
				document.chargebleWeight = grossKg;
			}
			
		}else if(volumeKg<=0 && grossKg>0){
			document.chargebleWeight = grossKg;
		}else if(volumeKg>0 && grossKg<=0){
			document.chargebleWeight = volumeKg;
		}else{
			document.chargebleWeight = 0.0;
		}
		
		if(document.chargebleWeight>0){
			var conversionUnit = null;
			
			if(service.serviceMaster.transportMode==='Air'){
				conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
			}else if(service.serviceMaster.transportMode==='Road'){
				conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
			}else if(service.serviceMaster.transportMode==='Ocean'){
				conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
			}
			
			if(conversionUnit!=null){
				document.bookedVolumeUnitCbm = parseFloat(document.chargebleWeight)/parseFloat(conversionUnit);
				document.bookedVolumeUnitCbm = $rootScope.weightFormat(document.bookedVolumeUnitCbm,service.location.countryMaster.locale);
			}
		}else{
			document.bookedVolumeUnitCbm = 0.0;
		}
		
		this.finalWeight(service);
		
	}

	this.finalWeight = function(service){
		
		var noOfPieces = 0;
		var grossWeight = 0.0;
		var volumeWeight = 0.0;
		var grossPound = 0.0;
		var volumePound = 0.0;
		var volumeInCbm = 0.0;
		
		
		for(var i=0;i<service.documentList.length;i++){
			var doc = service.documentList[i];
			
			try{
				noOfPieces = parseInt(noOfPieces) + parseInt(doc.noOfPieces);
			}catch(err){
				
			}
			
			try{
				grossWeight = parseFloat(grossWeight) + parseFloat(doc.grossWeight);
			}catch(err){
				
			}
			
			try{
				volumeWeight = parseFloat(volumeWeight) + parseFloat(doc.volumeWeight);
			}catch(err){
				
			}
			try{
				grossPound = parseFloat(grossPound) + parseFloat(doc.grossWeightInPound);
			}catch(err){
				
			}
			try{
				volumePound = parseFloat(volumePound) + parseFloat(doc.volumeWeightInPound);
			}catch(err){
				
			}
			try{
				volumeInCbm = parseFloat(volumeInCbm) + parseFloat(doc.bookedVolumeUnitCbm);
			}catch(err){
				
			}
			
		}
		
		service.bookedPieces = noOfPieces;
		service.bookedGrossWeightUnitKg = grossWeight;
		service.bookedVolumeWeightUnitKg = volumeWeight;
		service.bookedGrossWeightUnitPound = grossPound;
		service.bookedVolumeWeightUnitPound = volumePound;
		service.bookedVolumeUnitCbm = volumeInCbm;
		
		this.calculateChargeWeight(service);
	}
	
	
	this.goToConsol = function (shipment,service){
		
		console.log("goto Consol");
		var consol = {};
    	consol.createdBy = $rootScope.userProfile.employee;
    	consol.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
    	consol.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
    	consol.location = $rootScope.userProfile.selectedUserLocation;
    	consol.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
    	consol.masterDate = $rootScope.dateToString(new Date());
    	consol.directShipment = shipment.directShipment=='Yes'?true:false;
    	consol.consolDocument = {};
    	consol.consolDocument.mawbNo = service.mawbNo;
    	consol.consolDocument.mawbGeneratedDate=$rootScope.dateAndTimeToString(service.mawbDate);
    	consol.serviceMaster = service.serviceMaster;

    	if(service.serviceMaster.importExport=='Import'){
    		consol.whoRouted=service.whoRouted=='Self'?false:true;
    		if(consol.whoRouted){
    			consol.routedAgent=service.agent;
    			consol.routedSalesman=null;
    		}else{
    			consol.routedSalesman=service.salesman;	
    			consol.routedAgent=null;
    		}
    	}else{
    		consol.whoRouted = null;
    		consol.routedAgent=null;
			consol.routedSalesman=null;
			consol.routedSalesman=null;	
			consol.routedAgent=null;
    	}
    	consol.ppcc = service.ppcc=='Prepaid'?false:true;
    	consol.jobStatus = 'Active';
    	consol.isJobCompleted = false;
    	consol.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
    	consol.currency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
		consol.currencyRate = $rootScope.baseCurrenyRate[consol.currency.id]==undefined || $rootScope.baseCurrenyRate[consol.currency.id]==null?1:$rootScope.baseCurrenyRate[consol.currency.id].csell;
    	consol.origin = service.origin;
    	consol.pol = service.pol;
    	consol.pod = service.pod;
    	consol.destination = service.destination;
    	//consol.currency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
    	//consol.currencyRate = $rootScope.baseCurrenyRate[consol.currency.id].csell;
    	consol.consolDocument.noOfPieces= service.bookedPieces;
    	//consol.volume = 
    	consol.consolDocument.grossWeight = service.bookedGrossWeightUnitKg;
    	consol.consolDocument.volumeWeight = service.bookedVolumeWeightUnitKg;
    	consol.consolDocument.chargebleWeight = service.bookedChargeableUnit;
    	consol.consolDocument.grossWeightInPound = service.bookedGrossWeightUnitPound;
    	consol.consolDocument.volumeWeightInPound = service.bookedVolumeWeightUnitPound;
    	if(service.bookedVolumeUnitCbm!=null){
    		consol.consolDocument.volume=service.bookedVolumeUnitCbm;	
    	}
    	
    	
    	consol.carrier = service.carrier;
    	consol.etd = $rootScope.dateAndTimeToString(service.etd);
    	consol.eta = $rootScope.dateAndTimeToString(service.eta);
    	consol.consolDocument.routeNo = service.routeNo;
    	
    	consol.consolDocument.isAgreed = service.isAgreed=='Yes'||service.isAgreed?true:false;
    	consol.consolDocument.isMarksNo = service.isMarksNo=='Yes'||service.isMarksNo?true:false;
    	
    	consol.shipmentLinkList = [];
    	
    	var shipmentLink = {};
    	shipmentLink.company = service.company;
    	shipmentLink.country = service.country;
    	shipmentLink.location = service.location;
    	shipmentLink.shipmentUid = service.shipmentUid;
    	shipmentLink.service = service;
    	shipmentLink.service.isAgreed = shipmentLink.service.isAgreed || shipmentLink.service.isAgreed=='Yes'?'Yes':'No';
    	shipmentLink.service.isMarksNo = shipmentLink.service.isMarksNo || shipmentLink.service.isMarksNo=='Yes'?'Yes':'No';
    	shipmentLink.serviceUid = service.serviceUid;
    	
    	consol.shipmentLinkList.push(shipmentLink);
    	
    	consol.connectionList = [];
    	
    	for(var i=0;i<service.connectionList.length;i++){
    		var consolConnection = {};
    		consolConnection.move = service.connectionList[i].move;
    		consolConnection.etd = $rootScope.dateToString(service.connectionList[i].etd);
    		consolConnection.eta = $rootScope.dateToString(service.connectionList[i].eta);
    		consolConnection.pol = service.connectionList[i].pol;
    		consolConnection.pod = service.connectionList[i].pod;
    		consolConnection.carrierMaster = service.connectionList[i].carrierMaster;
    		consolConnection.flightVoyageNo = service.connectionList[i].flightVoyageNo;
    		consolConnection.connectionStatus = service.connectionList[i].connectionStatus;
    		
    		consol.connectionList.push(consolConnection);
    			
    	}
    	
    	if(consol.connectionList==undefined || consol.connectionList==null || consol.connectionList.length==0){
    		consol.connectionList.push({connectionStatus : 'Planned'});
    	}
    	
    	
        	
    	consol.eventList = [];
    	for(var i=0;i<service.eventList.length;i++){
    		var event = {};
    		event.eventMaster = service.eventList[i].eventMaster;
    		event.eventDate = $rootScope.dateToString(service.eventList[i].eventDate);
    		event.followUpRequired = (service.eventList[i].followUpRequired == 'Yes' || service.eventList[i].followUpRequired == true)? 'Yes' : 'No';
    		event.assignedTo = service.eventList[i].assignedTo;
    		event.followUpDate = $rootScope.dateToString(service.eventList[i].followUpDate);
    		event.isCompleted = service.eventList[i].isCompleted=='Yes'?true:false;
    		consol.eventList.push(event);
    	}
    	if(consol.eventList==undefined || consol.eventList==null || consol.eventList.length==0){
    		consol.eventList.push({isCompleted : 'Yes',followUpRequired : 'No'});
    	}
    	
    	consol.pickUpDeliveryPoint={};
    	consol.pickUpDeliveryPoint.transporter = service.pickUpDeliveryPoint.transporter;
    	consol.pickUpDeliveryPoint.transporterAddress = service.pickUpDeliveryPoint.transporterAddress;
    	consol.pickUpDeliveryPoint.isOurPickUp = service.pickUpDeliveryPoint.isOurPickUp=='No'|| service.pickUpDeliveryPoint.isOurPickUp==false?false:true; 
    	consol.pickUpDeliveryPoint.pickUpClientRefNo = service.pickUpDeliveryPoint.pickUpClientRefNo;
    	consol.pickUpDeliveryPoint.pickupPoint = service.pickUpDeliveryPoint.pickupPoint;
    	consol.pickUpDeliveryPoint.pickupFrom = service.pickUpDeliveryPoint.pickupFrom;
    	consol.pickUpDeliveryPoint.pickUpPlace = service.pickUpDeliveryPoint.pickUpPlace;
    	consol.pickUpDeliveryPoint.pickUpContactPerson = service.pickUpDeliveryPoint.pickUpContactPerson;
    	consol.pickUpDeliveryPoint.pickUpMobileNo = service.pickUpDeliveryPoint.pickUpMobileNo;
    	consol.pickUpDeliveryPoint.pickUpPhoneNo = service.pickUpDeliveryPoint.pickUpPhoneNo;
    	consol.pickUpDeliveryPoint.pickUpEmail = service.pickUpDeliveryPoint.pickUpEmail;
    	consol.pickUpDeliveryPoint.pickUpFax = service.pickUpDeliveryPoint.pickUpFax;
    	consol.pickUpDeliveryPoint.pickUpFollowUp = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.pickUpFollowUp);
    	consol.pickUpDeliveryPoint.pickUpPlanned = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.pickUpPlanned);
    	consol.pickUpDeliveryPoint.pickUpActual = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.pickUpActual);
    	
    	consol.pickUpDeliveryPoint.deliveryPoint = service.pickUpDeliveryPoint.deliveryPoint;
    	consol.pickUpDeliveryPoint.deliveryFrom = service.pickUpDeliveryPoint.deliveryFrom;
    	consol.pickUpDeliveryPoint.deliveryPlace = service.pickUpDeliveryPoint.deliveryPlace;
    	consol.pickUpDeliveryPoint.deliveryContactPerson = service.pickUpDeliveryPoint.deliveryContactPerson;
    	consol.pickUpDeliveryPoint.deliveryMobileNo = service.pickUpDeliveryPoint.deliveryMobileNo;
    	consol.pickUpDeliveryPoint.deliveryPhoneNo = service.pickUpDeliveryPoint.deliveryPhoneNo;
    	consol.pickUpDeliveryPoint.deliveryEmail = service.pickUpDeliveryPoint.deliveryEmail;
    	consol.pickUpDeliveryPoint.deliveryExpected = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.deliveryExpected);
    	consol.pickUpDeliveryPoint.deliveryActual = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.deliveryActual);
    	
    	
    	consol.pickUpDeliveryPoint.doorDeliveryPoint = service.pickUpDeliveryPoint.doorDeliveryPoint;
    	consol.pickUpDeliveryPoint.doorDeliveryFrom = service.pickUpDeliveryPoint.doorDeliveryFrom;
    	consol.pickUpDeliveryPoint.doorDeliveryPlace = service.pickUpDeliveryPoint.doorDeliveryPlace;
    	consol.pickUpDeliveryPoint.doorDeliveryContactPerson = service.pickUpDeliveryPoint.doorDeliveryContactPerson;
    	consol.pickUpDeliveryPoint.doorDeliveryMobileNo = service.pickUpDeliveryPoint.doorDeliveryMobileNo;
    	consol.pickUpDeliveryPoint.doorDeliveryPhoneNo = service.pickUpDeliveryPoint.doorDeliveryPhoneNo;
    	consol.pickUpDeliveryPoint.doorDeliveryEmail = service.pickUpDeliveryPoint.doorDeliveryEmail;
    	consol.pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.doorDeliveryExpected);
    	consol.pickUpDeliveryPoint.doorDeliveryActual = $rootScope.dateAndTimeToString(service.pickUpDeliveryPoint.doorDeliveryActual);
    	

    	// Attachments no need to copy...
    	consol.consolAttachmentList = [{}];
    	
    	consol.consolDocument.documentReqDate = $rootScope.dateToString(new Date());
		consol.consolDocument.documentNo=service.documentList[0].documentNo;
					
		if(consol.directShipment=='Yes' || consol.directShipment==true){
			
			consol.agent = null;
			consol.agentAddress = null;
			
			consol.consolDocument.shipper = service.documentList[0].shipper;
			consol.consolDocument.shipperAddress = service.documentList[0].shipperAddress;
			if(consol.consolDocument.shipperAddress!=null){
				consol.consolDocument.shipperAddress.id = null;
	    	}
			
			consol.consolDocument.consignee = service.documentList[0].consignee;
			consol.consolDocument.consigneeAddress = service.documentList[0].consigneeAddress;
			if(consol.consolDocument.consigneeAddress!=null){
				consol.consolDocument.consigneeAddress.id = null;
	    	}
			
		}else{
			
			consol.agent = service.documentList[0].agent;;
			consol.agentAddress = service.documentList[0].agentAddress;;
			if(consol.agentAddress!=null)
				consol.agentAddress.id = null;
			
			consol.consolDocument.shipper = service.documentList[0].issuingAgent;
			consol.consolDocument.shipperAddress = service.documentList[0].issuingAgentAddress;
			if(consol.consolDocument.shipperAddress!=null)
				consol.consolDocument.shipperAddress.id = null;
			
			consol.consolDocument.consignee = service.documentList[0].agent;
			consol.consolDocument.consigneeAddress = service.documentList[0].agentAddress;
			if(consol.consolDocument.consigneeAddress!=null)
				consol.consolDocument.consigneeAddress.id = null;
			
		}
		
		consol.consolDocument.forwarder = service.documentList[0].forwarder;
		consol.consolDocument.forwarderAddress = service.documentList[0].forwarderAddress;
		if(consol.consolDocument.forwarderAddress!=null){
			consol.consolDocument.forwarderAddress.id = null;
    	}
		
		consol.consolDocument.firstNotify = service.documentList[0].firstNotify;
		consol.consolDocument.firstNotifyAddress = service.documentList[0].firstNotifyAddress;
		if(consol.consolDocument.firstNotifyAddress!=null){
			consol.consolDocument.firstNotifyAddress.id = null;
    	}
		
		consol.consolDocument.secondNotify = service.documentList[0].secondNotify;
		consol.consolDocument.secondNotifyAddress = service.documentList[0].secondNotifyAddress;
		if(consol.consolDocument.secondNotifyAddress!=null){
			consol.consolDocument.secondNotifyAddress.id = null;
    	}
		
		consol.consolDocument.agent = service.documentList[0].agent;
		consol.consolDocument.agentAddress = service.documentList[0].agentAddress;
		if(consol.consolDocument.agentAddress!=null){
			consol.consolDocument.agentAddress.id = null;
    	}
		
		consol.consolDocument.issuingAgent = service.documentList[0].issuingAgent;
		consol.consolDocument.issuingAgentAddress = service.documentList[0].issuingAgentAddress;
		if(consol.consolDocument.issuingAgentAddress!=null){
			consol.consolDocument.issuingAgentAddress.id = null;
    	}
		
		
		consol.consolDocument.packMaster = service.documentList[0].packMaster;
		consol.consolDocument.packDescription = service.documentList[0].packDescription;
		consol.consolDocument.dimensionUnit = service.documentList[0].dimensionUnit;
		if(service.bookedVolumeUnitCbm!=null){
    		consol.consolDocument.volume=service.bookedVolumeUnitCbm;	
    	}
		consol.consolDocument.dimensionUnitValue = service.documentList[0].dimensionUnitValue;
		
		consol.consolDocument.handlingInformation = service.documentList[0].handlingInformation;
		consol.consolDocument.accountingInformation = service.documentList[0].accoutingInformation;
		consol.consolDocument.commodityDescription = service.documentList[0].commodityDescription;
		consol.consolDocument.marksAndNo = service.documentList[0].marksAndNo;
		consol.consolDocument.rateClass = service.documentList[0].rateClass;
		
		consol.consolDocument.company = service.documentList[0].company;
		consol.consolDocument.country = service.documentList[0].country;
		consol.consolDocument.location = service.documentList[0].location;
		
		
		consol.consolDocument.dimensionList = [];
		
    	for(var i=0;i<service.documentList.length;i++){
    		for(var j=0;j<service.documentList[i].dimensionList.length;j++){
    			var consDim = {};
    			consDim.noOfPiece = service.documentList[i].dimensionList[j].noOfPiece;
    			consDim.length = service.documentList[i].dimensionList[j].length;
    			consDim.width = service.documentList[i].dimensionList[j].width;
    			consDim.height = service.documentList[i].dimensionList[j].height;
    			consDim.volWeight = service.documentList[i].dimensionList[j].volWeight;
    			consDim.grossWeight = service.documentList[i].dimensionList[j].grossWeight;
    			consDim.grossWeightKg = service.documentList[i].dimensionList[j].grossWeightKg;
    			consDim.serviceId=service.id;
    			consDim.serviceDocumentId=service.documentList[i].id;
    			
    			consol.consolDocument.dimensionList.push(consDim);
    		}
    	}
    	
    	
    	
    	consol.consolDocument.dimensionList.push({});
    	if(service.documentList[0].ratePerCharge!= undefined&& service.documentList[0].ratePerCharge!=null){
			consol.consolDocument.ratePerCharge = service.documentList[0].ratePerCharge;
		}
    	
    	consol.chargeList = [{}];
    	consol.referenceList = [{}];
    	$rootScope.createMasterShipment = consol;
    	var params={};
    	params.fromState="fromShipment"
        $state.go('layout.createAirConsol',params);
	}
	
	 this.serviceToConsol = function (shipment,service){
		 
		 	if(shipment.holdShipment || shipment.holdShipment=='Yes'){
		 		$rootScope.clientMessage = $rootScope.nls["ERR90614"];
	    		return false;
			}
		 
		 
	    	if (shipment.party.isDefaulter) {
	    		$rootScope.clientMessage = $rootScope.nls["ERR90584"];
	    		return false;
	    	}
	    	
	    	if (service.party.isDefaulter) {
	    		$rootScope.clientMessage = $rootScope.nls["ERR90584"];
	    		return false;
	        }
	    	
	    	if (service.brokerageParty!=undefined && service.brokerageParty!=null 
	    			&& service.brokerageParty.id!=null && service.brokerageParty.isDefaulter) {
	            $rootScope.clientMessage = $rootScope.nls["ERR90585"];
	    		return false;
	        }
	    	
	    	if (service.pickUpDeliveryPoint!=undefined 
	    			&& service.pickUpDeliveryPoint!=null 
	    			&& service.pickUpDeliveryPoint.id!=null 
	    			&& service.pickUpDeliveryPoint.transporter!=undefined 
	    			&& service.pickUpDeliveryPoint.transporter !=null 
	    			&& service.pickUpDeliveryPoint.transporter.id!=null
	    			&& service.pickUpDeliveryPoint.transporter.isDefaulter) {
	            $rootScope.clientMessage = $rootScope.nls["ERR90593"];
	    		return false;
	        }
	    	
	    	if (service.pickUpDeliveryPoint!=undefined 
	    			&& service.pickUpDeliveryPoint!=null 
	    			&& service.pickUpDeliveryPoint.id!=null 
	    			&& service.pickUpDeliveryPoint.pickupPoint!=undefined 
	    			&& service.pickUpDeliveryPoint.pickupPoint !=null 
	    			&& service.pickUpDeliveryPoint.pickupPoint.id!=null
	    			&& service.pickUpDeliveryPoint.pickupPoint.isDefaulter) {
	            $rootScope.clientMessage = $rootScope.nls["ERR90594"];
	    		return false;
	        }
	    	
	    	for(var i=0;i<service.documentList.length;i++){
	    		if(service.documentList[i].shipper!=undefined && service.documentList[i].shipper!=null && service.documentList[i].shipper.id!=null && service.documentList[i].shipper.isDefaulter){
	    			$rootScope.clientMessage = $rootScope.nls["ERR90586"];
	        		return false;
	    		}
				if(service.documentList[i].forwarder!=undefined && service.documentList[i].forwarder!=null && service.documentList[i].forwarder.id!=null && service.documentList[i].forwarder.isDefaulter){
					$rootScope.clientMessage = $rootScope.nls["ERR90591"];
		    		return false;		
				}
				if(service.documentList[i].consignee!=undefined && service.documentList[i].consignee!=null && service.documentList[i].consignee.id!=null && service.documentList[i].consignee.isDefaulter){
					$rootScope.clientMessage = $rootScope.nls["ERR90587"];
		    		return false;
				}
				if(service.documentList[i].firstNotify!=undefined && service.documentList[i].firstNotify!=null && service.documentList[i].firstNotify.id!=null&& service.documentList[i].firstNotify.isDefaulter){
					$rootScope.clientMessage = $rootScope.nls["ERR90588"];
		    		return false;
				}
				if(service.documentList[i].secondNotify!=undefined && service.documentList[i].secondNotify!=null && service.documentList[i].secondNotify.id!=null&& service.documentList[i].secondNotify.isDefaulter){
					$rootScope.clientMessage = $rootScope.nls["ERR90589"];
		    		return false;
				}
				if(service.documentList[i].agent!=undefined && service.documentList[i].agent!=null && service.documentList[i].agent.id!=null&& service.documentList[i].agent.isDefaulter){
					$rootScope.clientMessage = $rootScope.nls["ERR90590"];
		    		return false;
				}
				if(service.documentList[i].issuingAgent!=undefined && service.documentList[i].issuingAgent!=null && service.documentList[i].issuingAgent.id!=null && service.documentList[i].issuingAgent.isDefaulter){
					$rootScope.clientMessage = $rootScope.nls["ERR90592"];
		    		return false;
				}

	    	}
	    	
	    	if(service.serviceMaster.importExport=='Export'){
	    		if(!$rootScope.isGoodsReceived(service)){
		    		$rootScope.clientMessage = $rootScope.nls["ERR90568"];
		    		return false;
		    	}
	    	}
	    	
	    	
	    	if(service.mawbNo==undefined || service.mawbNo==null || service.mawbNo==''){
	    		$rootScope.clientMessage = $rootScope.nls["ERR90569"];
	    		return false;
	    		
	    	}else{
	    		
	    		if(service.carrier==undefined || service.carrier==null || service.carrier.id==null){
	    			$rootScope.clientMessage = $rootScope.nls["ERR90063"];
	        		return false;
	    		}
	    		var check = $rootScope.validateMawb(service.carrier,service.mawbNo);
	    		if(check!="S"){
	    			if(check!="F"){
	    				 $rootScope.clientMessage = $rootScope.nls["ERR90216"] + "(" + check + ")";
	    			}else{
	    				$rootScope.clientMessage = $rootScope.nls["ERR90068"];
	    			}
	    			
	        		return false;
	    		}
	    	}
	    	
	    	if(service.etd==undefined || service.etd==null || service.etd==''){
	    		$rootScope.clientMessage = $rootScope.nls["ERR90094"];
	    		return false;
	    	}
	    	
	    	if(service.eta==undefined || service.eta==null || service.eta==''){
	    		$rootScope.clientMessage = $rootScope.nls["ERR90095"];
	    		return false;
	    	} 
	    	
	    return true;
	    }
	 
	 this.getPartyAddress = function(obj, mapToAddrss) {
         if (obj != null && obj.id != null) {
             for (var i = 0; i < obj.partyAddressList.length; i++) {
                 if (obj.partyAddressList[i].addressType == 'Primary') {
                     this.addressMapping(obj.partyAddressList[i], obj, obj.countryMaster, mapToAddrss);
                 }
             }
         }
     }

     this.addressMapping = function(partyAddress, partyObj, countryMaster, mapToAddrss) {

    	 if(mapToAddrss ==undefined || mapToAddrss ==null)
    		 {
    		 mapToAddrss={};
    		 }
         mapToAddrss.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox + ", " + partyAddress.addressLine1 : partyAddress.addressLine1;
         mapToAddrss.addressLine2 = partyAddress.addressLine2;
         mapToAddrss.addressLine3 = partyAddress.addressLine3;

         var addressLine4 = "";
         if (partyAddress.cityMaster !== null) {
             addressLine4 = addressLine4 + partyAddress.cityMaster.cityName;
         }

         if (partyAddress.zipCode != null) {
             if (addressLine4 != "")

                 addressLine4 = addressLine4 + " - ";
             addressLine4 = addressLine4 + partyAddress.zipCode;
         }

         if (partyAddress.stateMaster != null) {
             if (addressLine4 != "")
                 addressLine4 = addressLine4 + ", ";
             addressLine4 = addressLine4 + partyAddress.stateMaster.stateName;

         }

         if (countryMaster != null) {
             if (addressLine4 != "")
                 addressLine4 = addressLine4 + ", ";
             addressLine4 = addressLine4 + countryMaster.countryName;

         }
         mapToAddrss.addressLine4 = addressLine4 + "\n" + this.getPhoneandFax(partyObj);
     }
     
     this.getPhoneandFax = function(partyObj) {
    	 this.partyPhandFax = "";
         for (var i = 0; i < partyObj.partyAddressList.length; i++) {
             if (partyObj.partyAddressList[i].phone != undefined && partyObj.partyAddressList[i].phone != null) {
                 if (this.partyPhandFax.includes("PH")) {
                	 this.partyPhandFax = this.partyPhandFax + "," + partyObj.partyAddressList[i].phone;
                 } else {
                	 this.partyPhandFax = this.partyPhandFax + "PH:" + partyObj.partyAddressList[i].phone;
                 }
             }
         }
         for (var i = 0; i < partyObj.partyAddressList.length; i++) {
             if (partyObj.partyAddressList[i].fax != undefined && partyObj.partyAddressList[i].fax != null) {
                 if (this.partyPhandFax.includes("FAX")) {
                	 this.partyPhandFax = +" " + this.partyPhandFax + "," + partyObj.partyAddressList[i].fax;
                 } else {
                	 this.partyPhandFax = +" " + this.partyPhandFax + "FAX:" + partyObj.partyAddressList[i].fax;

                 }
             }

         }
         return this.partyPhandFax;
     }
	
     this.chkAndClear = function(shipment, forObject, isService, serviceIndex, docIndex) {
 		switch (forObject) {
 			case "shipment.party":
 				if(shipment.party != null && shipment.party.id != null) {
 					shipment.party = null;
 					shipment.partyAddress = this.clearAddress();
 				} else {
 					shipment.partyAddress = this.clearAddress();
 				}
 			break;
 			
 			case "documentDetail.shipper":
 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipper != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipper.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipper = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipperAddress = this.clearAddress();	
 					} else {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].shipperAddress = this.clearAddress();
 					}
 				}
 			break;

 			case "documentDetail.consignee":
 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consignee != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consignee.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consignee = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consigneeAddress = this.clearAddress();	
 					} else {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].consigneeAddress = this.clearAddress();
 					}
 				}
 			break;

 			case "documentDetail.firstNotify":
 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotify != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotify.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotify = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotifyAddress = this.clearAddress();	
 					} else {
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].firstNotifyAddress = this.clearAddress();
 					}
 				}
 			break;
 			
 			case "documentDetail.secondNotify":
 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotify != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotify.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotify = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotifyAddress = this.clearAddress();	
 					} else {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].secondNotifyAddress = this.clearAddress();
 					}
 				}
 			break;
 			
 			case "documentDetail.forwarder":
 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarder != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarder.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarder = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarderAddress = this.clearAddress();	
 					} else {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].forwarderAddress = this.clearAddress();
 					}
 				}
 			break;
 			
 			case "documentDetail.agent":

 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agent != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agent.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agent = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agentAddress = this.clearAddress();	
 					} else {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].agentAddress = this.clearAddress();
 					}
 				}
 				break;
 			case "documentDetail.issuingAgent":

 				if(serviceIndex > -1 && docIndex > -1) {
 					if(shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgent != null && shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgent.id != null) {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgent = null;
 	 	                shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgentAddress = this.clearAddress();	
 					} else {
 						shipment.shipmentServiceList[serviceIndex].documentList[docIndex].issuingAgentAddress = this.clearAddress();
 					}
 				}
 				break;
 			default:

 		}
 		return shipment;
 	}
     
     
     this.configureData=function(shipment){
    	 
    	 shipment.whoRouted = shipment.whoRouted == 'Agent' ? true : false;
         shipment.directShipment = shipment.directShipment == 'Yes' ? true : false;
         for (var i = 0; i < shipment.shipmentServiceList.length; i++) {
             shipment.shipmentServiceList[i].whoRouted = shipment.shipmentServiceList[i].whoRouted == 'Agent' ? true : false;
             shipment.shipmentServiceList[i].ppcc = shipment.shipmentServiceList[i].ppcc == 'Prepaid' ? false : true;
         	
             shipment.shipmentServiceList[i].serviceReqDate = $rootScope.dateToString(shipment.shipmentServiceList[i].serviceReqDate);
             shipment.shipmentServiceList[i].eta = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].eta);
             shipment.shipmentServiceList[i].etd = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].etd);

             if (shipment.shipmentServiceList[i].documentList != undefined && shipment.shipmentServiceList[i].documentList != null &&
            		 shipment.shipmentServiceList[i].documentList.length>0) {
                 for (var j = 0; j < shipment.shipmentServiceList[i].documentList.length; j++) {
                     if(shipment.shipmentServiceList[i].documentList[j].dimensionList==undefined  ||
                     		shipment.shipmentServiceList[i].documentList[j].dimensionList == null ||
                     		shipment.shipmentServiceList[i].documentList[j].dimensionList.length == 0){
                     	shipment.shipmentServiceList[i].documentList[j].dimensionList=[];
                     	shipment.shipmentServiceList[i].documentList[j].dimensionList.push({});
                     }
                     
                     shipment.shipmentServiceList[i].documentList[j].documentReqDate = $rootScope.dateToString(shipment.shipmentServiceList[i].documentList[j].documentReqDate);
                     shipment.shipmentServiceList[i].documentList[j].etd = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].etd);
                     shipment.shipmentServiceList[i].documentList[j].eta = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].eta);
                     
                     shipment.shipmentServiceList[i].documentList[j].canIssuedDate = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].canIssuedDate);
                     shipment.shipmentServiceList[i].documentList[j].doIssuedDate = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].doIssuedDate);
                     shipment.shipmentServiceList[i].documentList[j].hawbReceivedOn = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].documentList[j].hawbReceivedOn);
                     
                     
                 }
             }
             
             if(shipment.shipmentServiceList[i].connectionList == undefined ||  shipment.shipmentServiceList[i].connectionList == null || shipment.shipmentServiceList[i].connectionList.length == 0) {
         		shipment.shipmentServiceList[i].connectionList = [];
         		shipment.shipmentServiceList[i].connectionList.push({connectionStatus : 'Planned'});
         	}else{
             	for (var j = 0; j < shipment.shipmentServiceList[i].connectionList.length; j++) {
             		shipment.shipmentServiceList[i].connectionList[j].etd = $rootScope.dateToString(shipment.shipmentServiceList[i].connectionList[j].etd);
             		shipment.shipmentServiceList[i].connectionList[j].eta = $rootScope.dateToString(shipment.shipmentServiceList[i].connectionList[j].eta);
                 }
             
         	}
             
             if(shipment.shipmentServiceList[i].shipmentAttachmentList == undefined ||  shipment.shipmentServiceList[i].shipmentAttachmentList == null || shipment.shipmentServiceList[i].shipmentAttachmentList.length == 0) {
            	 shipment.shipmentServiceList[i].shipmentAttachmentList=[];
            	 shipment.shipmentServiceList[i].shipmentAttachmentList.push({});
         	}
             
             if(shipment.shipmentServiceList[i].pickUpDeliveryPoint!=undefined && shipment.shipmentServiceList[i].pickUpDeliveryPoint!=null){
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = shipment.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp == 'No' ? false : true;	
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp);	
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned);
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual);
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected);
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual);
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected);
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual = $rootScope.dateAndTimeToString(shipment.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual);
             	
             }else{
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint ={};
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.transporter ={};
             	shipment.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = false;
             }
             
             if (shipment.shipmentServiceList[i].shipmentServiceTriggerList == undefined || shipment.shipmentServiceList[i].shipmentServiceTriggerList == null || shipment.shipmentServiceList[i].shipmentServiceTriggerList.length == 0) {
             	shipment.shipmentServiceList[i].shipmentServiceTriggerList = [];
         		shipment.shipmentServiceList[i].shipmentServiceTriggerList.push({});
             }else{
             	for (var k = 0; k < shipment.shipmentServiceList[i].shipmentServiceTriggerList.length; k++) {
             		shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].date = $rootScope.dateToString(shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].date);
             		shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].followUpDate = $rootScope.dateToString(shipment.shipmentServiceList[i].shipmentServiceTriggerList[k].followUpDate);
                 }
             }
             
             if (shipment.shipmentServiceList[i].eventList == undefined || shipment.shipmentServiceList[i].eventList == null || shipment.shipmentServiceList[i].eventList.length == 0) {
             	shipment.shipmentServiceList[i].eventList = [];
         		shipment.shipmentServiceList[i].eventList.push({isCompleted : 'Yes',followUpRequired : 'No'});
             }else{
             	for (var j = 0; j < shipment.shipmentServiceList[i].eventList.length; j++) {
             		shipment.shipmentServiceList[i].eventList[j].eventDate = $rootScope.dateToString(shipment.shipmentServiceList[i].eventList[j].eventDate);
             		shipment.shipmentServiceList[i].eventList[j].followUpDate = $rootScope.dateToString(shipment.shipmentServiceList[i].eventList[j].followUpDate);
                 }
             }
             
             if(shipment.shipmentServiceList[i].referenceList == undefined ||  shipment.shipmentServiceList[i].referenceList == null || shipment.shipmentServiceList[i].referenceList.length == 0) {
         		shipment.shipmentServiceList[i].referenceList = [];
         		shipment.shipmentServiceList[i].referenceList.push({});
         	} 
             
          	
         	if (shipment.shipmentServiceList[i].authenticatedDocList == undefined || shipment.shipmentServiceList[i].authenticatedDocList == null || shipment.shipmentServiceList[i].authenticatedDocList.length == 0) {
             	shipment.shipmentServiceList[i].authenticatedDocList = [];
         		shipment.shipmentServiceList[i].authenticatedDocList.push({});
             }else{
             	 for (var k = 0; k < shipment.shipmentServiceList[i].authenticatedDocList.length; k++) {
             		 shipment.shipmentServiceList[i].authenticatedDocList[k].date = $rootScope.dateToString(shipment.shipmentServiceList[i].authenticatedDocList[k].date);
                  }
             }
             
         	shipment.shipmentServiceList[i].attachConfig = {
 			            "isEdit": true,
 			            "isDelete": true,
 			            "page": "shipment",
 			            "columnDefs": [

 			                {
 			                    "name": "Document Name",
 			                    "model": "documentMaster.documentName",
 			                    "type": "text"
 			                }, {
 			                    "name": "Reference #",
 			                    "model": "refNo",
 			                    "type": "text"
 			                }, {
 			                    "name": "Customs",
 			                    "model": "customs",
 			                    "type": "text"
 			                }, {
 			                    "name": "Unprotected File Name",
 			                    "model": "unprotectedFileName",
 			                    "type": "file"
 			                }, {
 			                    "name": "Protected File Name",
 			                    "model": "protectedFileName",
 			                    "type": "file"
 			                }, {
 			                    "name": "Actions",
 			                    "model": "action",
 			                    "type": "action"
 			                }
 			            ]
 				}
         	
             
         }
         return shipment;
     }
     
 	this.clearAddress = function() {
 		address = {};
 		return address;
 	}
}]);

