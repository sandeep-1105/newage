/**
 * 
 */
app.factory("ShipmentSearch",['$resource', function($resource) {
		return $resource("/api/v1/shipment/search", {}, {
			query : {
				method : 'POST'
			}
		});
}]);




app.factory("ShipmentAutoAttach",['$resource', function($resource) {
	return $resource("/api/v1/shipment/auto/attach", {}, {
		attach : {
			method : 'POST'
		}
	});
}]);

app.factory("CopyShipmentSearch",['$resource', function($resource) {
	return $resource("/api/v1/shipment/copy_shipment_search", {}, {
		query : {
			method : 'POST'
		}
	});
}]);

app.factory("ImportExportSearch",['$resource', function($resource) {
	return $resource("/api/v1/shipment/importExportSearch", {}, {
		query : {
			method : 'POST'
		}
	});
}]);

app.factory("ServiceGet",['$resource', function($resource) {
	return $resource("/api/v1/shipment/service/get/id/:id", {}, {
		get : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}]);


app.factory("PortFromCFS",['$resource', function($resource) {
	return $resource("/api/v1/cfsmaster/get/search/keyword/:portid", {}, {
		fetch : {
			method : 'POST',
			params : {
				portid : ''
			},
			isArray : false
		}
	});
}]);

app.factory("ShipmentAsyncSearch",['$resource', function($resource) {
	return $resource("/api/v1/shipment/search", {}, {
		query : {
			method : 'POST'
		}
	});
}]);


app.factory("ShipmentGet",['$resource', function($resource) {
	return $resource("/api/v1/shipment/get/id/:id", {}, {
		get : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}]);

app.factory("ShipmentGetFromUID",['$resource', function($resource) {
	return $resource("/api/v1/shipment/get/shipmentuid/:shipmentuid", {}, {
		get : {
			method : 'GET',
			params : {
				shipmentuid : ''
			},
			isArray : false
		}
	});
}]);

app.factory("ShipmentGetByUid",['$resource', function($resource) {
	return $resource("/api/v1/shipment/get/shipmentuid/:shipmentUid", {}, {
		get : {
			method : 'GET',
			params : {
				shipmentUid : ''
			},
			isArray : false
		}
	});
}]);


app.factory("ServiceGetByUid",['$resource', function($resource) {
	return $resource("/api/v1/shipment/get/serviceUid/:serviceUid", {}, {
		get : {
			method : 'GET',
			params : {
				serviceUid : ''
			},
			isArray : false
		}
	});
}]);





app.factory("ShipmentUpdate",['$resource', function($resource) {
	return $resource("/api/v1/shipment/update", {}, {
		update : {
			method : 'POST'
		}
	});
}]);


app.factory("ShipmentStatusChange",['$resource', function($resource) {
	return $resource("/api/v1/shipment/statuschange", {}, {
		status : {
			method : 'POST',
			isArray : false
		}
	});
}]);



app.factory("ShipmentAdd",['$resource', function($resource) {
	return $resource("/api/v1/shipment/create", {}, {
		save : {
			method : 'POST'
		}
	});
}]);

app.factory("ShipmentServiceExportAdd",['$resource', function($resource) {
	return $resource("/api/v1/shipment/createexport", {}, {
		save : {
			method : 'POST'
		}
	});
}]);

app.factory("ShipmentUpdateForStock",['$resource', function($resource) {
	return $resource("/api/v1/shipment/updatebyShipmentuid", {}, {
		save : {
			method : 'POST'
		}
	});
}]);



app.factory("AutoImportProcess",['$resource', function($resource) {
	return $resource("/api/v1/shipment/autoimport/serviceId/:serviceId", {}, {
		process : {
			method : 'GET',
			params : {
				serviceId : ''
			},
			isArray : false
		}
	});
}]);

app.factory("ShipmentUid",['$resource', function($resource) {
	return $resource("/api/v1/shipment/getid/:serviceId", {}, {
		process : {
			method : 'GET',
			params : {
				serviceId : ''
			},
			isArray : false
		}
	});
}]);


app.factory("ShipmentIdByServiceUid",['$resource', function($resource) {
	return $resource("/api/v1/shipment/getshipmentid/byserviceuid/:serviceuid", {}, {
		process : {
			method : 'GET',
			params : {
				serviceuid : ''
			},
			isArray : false
		}
	});
}]);


app.factory("ServiceIdByServiceUid",['$resource', function($resource) {
	return $resource("/api/v1/shipment/getserviceid/byserviceuid/:serviceuid", {}, {
		process : {
			method : 'GET',
			params : {
				serviceuid : ''
			},
			isArray : false
		}
	});
}]);


app.factory("ShipmentDocumentIdList",['$resource', function($resource) {
	return $resource("/api/v1/shipment/report/shipment/:serviceId", {}, {
		get : {
			method : 'GET',
			params : {
				serviceId : ''
			},
			isArray : false
		}
	});
}]);


app.factory("ShipmentSearchKeyword",['$resource', function($resource) {
	return $resource("/api/v1/shipment/get/search/keyword", {}, {
		fetch : {
			method : 'POST'
		}
	});
}]);

app.factory("updateAttachEvent",['$resource', function($resource) {
	return $resource("/api/v1/shipment/service/updateAttachEvent", {}, {
		update : {
			method : 'POST'
		}
	});
}]);


app.factory("ShipmentServiceSearchKeyword",['$resource', function($resource) {
	return $resource("/api/v1/shipment/service/get/search/keyword", {}, {
		fetch : {
			method : 'POST'
		}
	});
}]);

app.factory("ShipmentIdBasedOnUid",['$resource', function($resource) {
	return $resource("/api/v1/shipment/shipmentIdBasedOnUid/:shipmentUid", {}, {
		get : {
			method : 'GET',
			params : {
				shipmentUid : ''
			},
			isArray : false
		}
	});
}]);

app.factory("ShipmentTrack",['$resource', function($resource) {
	return $resource("/api/v1/shipment/trackShipment/:shipmentId", {}, {
		process : {
			method : 'GET',
			params : {
				shipmentId : ''
			},
			isArray : false
		}
	});
}]);
