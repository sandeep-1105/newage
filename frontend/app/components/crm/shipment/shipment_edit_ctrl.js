/**
 * Created by hmspl on 12/4/16.
 */
app.controller('shipmentEditCtrl', function($rootScope, $scope, $location, ngDialog, $http, $modal, $timeout, Notification,
    PartiesList, ngProgressFactory, ProjectList,
    ShipmentAdd, TosSearchKeyword, CommoditySearchKeyword, SalesmanList, ShipmentGetByUid,
    ServiceByTransportMode, discardService, discardService1, ValidateUtil, EmployeeList, DivisionListByCompany,
    PackList, CarrierByTransportMode, cloneService, ShipmentUpdate, ChargeSearchKeyword, ConsolGetByUid,
    UnitSearchKeyword, NavigationService, PartyMasterService, CurrencyMasterSearchKeyword, ServiceGetByUid,
    TriggerSearchKeyword, TriggerTypeSearchKeyword, DocumentMasterList, EventSearchKeyword, PortGroupList,
    PortFromCFS, PartiesListByType, invoiceService, PortByPortGroup, GetNextMawb, ShipmentServiceInvoiceList,
    RecentHistorySaveService, ShipmentGet, $state, $stateParams, $document, PackGetByCode, AesSearch,
    ProvisionalViewByShipmentServiceUid, ProvisionalService, QuotationViewByQuotationNo, appConstant, CommonValidationService,
    CopyShipmentSearch, ShipmentGetFromUID, PortByTransportMode, downloadFactory, ShipmentDocumentIdList, AgentByServicePort,
    attachToMasterService, FlightPlanSearchForShipment, $log, addressJoiner, AesAddAll) {


    $scope.directiveDateTimePickerOpts = {
        format: "DD-MM-YYYY HH:mm",
        useCurrent: false,
        sideBySide: true,
        viewDate: moment()
    };
    $scope.directiveDateTimePickerOpts.widgetParent = "body";
    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";
    $scope.dateTimePickerOptionsLeft = {
        format: "DD-MM-YYYY HH:mm",
        useCurrent: false,
        sideBySide: true,
        viewDate: moment()
    };
    $scope.dateTimePickerOptionsLeft.widgetParent = "body";
    $scope.dateTimePickerOptionsLeft.widgetPositioning = {
        horizontal: 'right'
    };


    $scope.reportData = {};
    $scope.activeViewTabAndService = function(serviceIndex, activeTabName) {
        $scope.clickTabIndex(serviceIndex);
        $scope.shipmentTab = activeTabName;
    }


    $scope.disableSubmitBtn = false;
    $scope.isFormValueDirty = false;
    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            console.log("Enable btn ,", new Date());
            $scope.disableSubmitBtn = false;
            $rootScope.mainpreloder = false;
        }, 500);
    }
    $scope.rateMerged = false;
    var rateMergedValue = $rootScope.appMasterData['booking.rates.merged'];
    if (rateMergedValue.toLowerCase() == 'true' || rateMergedValue == true) {
        $scope.rateMerged = true;
    }

    $scope.notePopup = $modal({ scope: $scope, templateUrl: 'holdStatusNote.html', backdrop: 'static', show: false, keyboard: true });
    $scope.notespopup = function(id) {
        $scope.notePopup.$promise.then($scope.notePopup.show);
        $scope.popUpNote = $scope.shipment.reasonForHold;
        $rootScope.navigateToNextField(id);
    };
    $scope.saveForHold = function(note) {
        $scope.shipment.reasonForHold = note;
        $scope.notePopup.$promise.then($scope.notePopup.hide);
    }

    $scope.setOldDataVal = function() {
        $timeout(function() {
            $scope.oldData = JSON.stringify($scope.shipment);
        }, 3000)
    }

    $scope.provisionalObj = {};


    $scope.tmpClick = function(object) {
        if (object != undefined && object != null && object.id != null && object.id != undefined) {
            $rootScope.clientMessage = null;
            var str = $rootScope.nls['ERR90612'];
            str = str.replace("%s", object.serviceMaster.serviceName);
            str = str.replace("%s", object.lastUpdatedStatus.serviceStatus);
            $rootScope.clientMessage = str;
        } else {
            // if coming from Quotation;
            $scope.clickTabIndex(0);
        }
    }
    $scope.tabNavEvent = function(e) {
        console.log("Event ", e);
        var key = e.keyCode ? e.keyCode : e.which;
        if (key == 9 && !e.shiftKey) {
            $rootScope.navigateToNextField(nxtElem);
        }
    };
    $scope.partyBlurChkData = function(party, forObject, isService, serviceIndex, docIndex) {
        if (party == undefined || party == null || party == "" || party.id == undefined || party.id == null || party.id == "") {
            $scope.shipment = attachToMasterService.chkAndClear($scope.shipment, forObject, isService, serviceIndex, docIndex);
        }
    }
    $scope.routePartyEvent = function(fromObjModel, isAdd, partyMaster, isService, serviceIndex, docIndex) {
        $rootScope.nav_src_bkref_key = new Date().toISOString();
        var param = {
            nav_src_bkref_key: $rootScope.nav_src_bkref_key,
            forObj: fromObjModel,
            category: "Shipment"
        };
        if (isService) {
            param.serIndx = serviceIndex;
            param.docIndx = docIndex;
        }
        console.log("Shipment Obj :: ", $scope.shipment);
        if (isAdd) {
            $state.go("layout.addParty", param);
        } else {
            if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                param.partyId = partyMaster.id;
                $state.go("layout.editParty", param);
            } else {
                console.log("Party not selected...");
            }
        }
    }

    $scope.shipmentServiceCheck = {};
    $scope.docNavigatePage = function(addressType) {
        $scope.addShipment = NavigationService.getSubData5().get('addShipment');
        $scope.editServiceIndex = NavigationService.getSubData5().get('editServiceIndex');
        $scope.addDocument = NavigationService.getSubData5().get('addDocument');
        $scope.editDocumentsIndex = NavigationService.getSubData5().get('editDocumentsIndex');
        $scope.shipmentTab = NavigationService.getSubData5().get('shipmentTab');

        $scope.choosePartyAddress(addressType);
    }

    $scope.downloadAttach = function(param) {
        console.log("download ", param);
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            $http({
                url: $rootScope.baseURL + '/api/v1/shipment/files/' + param.data.id + '/' + param.type + '/false',
                method: "GET",
                responseType: 'arraybuffer'
            }).success(function(data, status, headers, config) {
                console.log("hiddenElement ", data)
                var blob = new Blob([data], {});
                console.log("blob ", blob);
                saveAs(blob, param.data.fileName);
            }).error(function(data, status, headers, config) {
                //upload failed
            });
        }
    }



    $scope.openTab = function(tabName, index, shipmentServiceDetail) {

            if (tabName == 'general') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_MOREINFO_VIEW')) {
                    $scope.shipmentTab = 'general';
                    $rootScope.navigateToNextField('partyService' + index);
                }
            } else if (tabName == 'documents') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_DOCUMENT_VIEW')) {
                    $scope.selectedTabIndex = index;
                    $scope.goToDocFocus(shipmentServiceDetail, 'documents', 'exprtdocShipper', 'importdocConsignee');
                }
            } else if (tabName == 'connections') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_CONNECTION_VIEW')) {
                    $scope.shipmentTab = 'connections';
                    $rootScope.navigateToNextField(index + 'connectionMove0');
                }
            } else if (tabName == 'attachMent') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_CONNECTION_VIEW')) {
                    $rootScope.navigateToNextField(index + 'attachDocument0');
                    $scope.shipmentTab = 'attachMent';

                }
            } else if (tabName == 'pickup') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_PICKUP_VIEW')) {
                    $scope.shipmentTab = 'pickup';
                    $rootScope.navigateToNextField('pickTransporter' + index)
                }
            } else if (tabName == 'rates') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_RATES_VIEW')) {
                    $scope.shipmentTab = 'rates';

                    if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > Rate > Brokerage Party') == true ||
                        $rootScope.dynamicFormFieldMap.get('Shipment > Service > Rate > Brokerage Party') == 'true') {
                        $rootScope.navigateToNextField('brokerageParty' + index);
                    } else {
                        $rootScope.navigateToNextField(index + 'chargeCode0');
                    }


                }
            } else if (tabName == 'statusUpdate') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_STATUS_VIEW')) {
                    $scope.shipmentTab = 'statusUpdate';
                    $rootScope.navigateToNextField(index + 'statusDate0');
                }
            } else if (tabName == 'events') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_EVENTS_VIEW')) {
                    $scope.shipmentTab = 'events';
                    $rootScope.navigateToNextField(index + 'eventMaster0');
                }
            } else if (tabName == 'reports') {
                $scope.reportListMaking(shipmentServiceDetail, shipmentServiceDetail.id)
                $scope.shipmentTab = 'reports';
            } else if (tabName == 'accounts') {
                if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_ACCOUNTS_VIEW')) {

                    if (shipmentServiceDetail.id != undefined && shipmentServiceDetail.id != null) {
                        $scope.getAccountData(shipmentServiceDetail.id);
                        $scope.getProvisionalData(shipmentServiceDetail.serviceUid);
                        $scope.shipmentTab = 'accounts'
                    } else {
                        $scope.shipmentTab = 'accounts'
                    }
                }
            } else if (tabName == 'references') {
                $scope.shipmentTab = 'references';
                $rootScope.navigateToNextField(index + 'referenceTypeMaster0');
            } else if (tabName == 'aes') {
                if (shipmentServiceDetail.id != undefined && shipmentServiceDetail.id != null) {
                    $scope.aesSearchforService(shipmentServiceDetail);
                    $scope.shipmentTab = 'aes';
                } else {
                    $scope.shipmentTab = 'aes';
                }
            } else if (tabName == 'AuthenticatedDocs') {
                $scope.shipmentTab = 'AuthenticatedDocs';
                $rootScope.navigateToNextField(index + 'documentNo0');
            } else if (tabName == 'cfs') {
                $scope.shipmentTab = 'cfs';
                //$rootScope.navigateToNextField(index+'documentNo0');
            } else {
                console.log("No Tabs Clicked");
            }
        }
        /*Aes generate all check box code started here*/

    $scope.allAesModel = {};
    $scope.allAesModel.allAesSelected = false;

    $scope.selectAllAes = function() {

        for (var i = 0; i < $scope.finalArr.length; i++) {
            $scope.finalArr[i].isSelected = $scope.allAesModel.allAesSelected;
        }
    }
    $scope.chkAesStatus = function(chkFlag, detObj) {
        console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
    }

    $scope.generateAllAes = function() {
        var count = 0;
        var aesArr = [];
        for (var i = 0; i < $scope.finalArr.length; i++) {
            if ($scope.finalArr[i].isSelected) {
                count = count + 1;
                aesArr.push($scope.finalArr[i]);
            }
        }
        if (count > 1) {
            console.log("generate more than one,no need to go aes add page");
            $scope.aesGenerateAll(aesArr);
        } else if (count == 1) {
            console.log("generate one ,go to aes add page ");
            $scope.aesGenerateAdd(aesArr[0]);
        } else {
            console.log("Please select at least one aes to genarate");
            $rootScope.clientMessage = "Please select at least one aes to genarate";
        }
    }

    $scope.showGenerateBtn = function() {
        var flag = false;
        if ($scope.finalArr == null || $scope.finalArr == undefined) {
            return flag;
        }
        for (var i = 0; i < $scope.finalArr.length; i++) {
            if ($scope.finalArr[i].status == 'Generate') {
                flag = true;
                return flag;
            }
        }
        return flag;
    }

    $scope.aesGenerateAll = function(aesArr) {
            console.log("Aes files need to genarate : ", aesArr);
            AesAddAll.save(aesArr).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Aes Saved Successfully");
                    $scope.spinner = false;
                    Notification.success("Successfully generated ");
                } else {
                    $scope.spinner = false;
                    console.log("Aes Saving Failed ", data.responseDescription);
                }
            }, function(error) {
                $scope.spinner = false;
                console.log("Aes Saving Failed : ", error)
            });
        }
        /*Aes generate all check box code ended here*/
    $scope.newFirstDocument = function(flag) {

        var serviceMasterId = null;
        var portMasterId = null;

        if ($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].serviceMaster.transportMode === 'Air' &&
            $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].serviceMaster.importExport === 'Import' &&
            ($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].serviceMaster.serviceType === undefined ||
                $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].serviceMaster.serviceType === null)) {
            serviceMasterId = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].serviceMaster.id;
            portMasterId = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].origin.id;
        } else {
            serviceMasterId = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].serviceMaster.id;
            portMasterId = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].destination.id;
        }


        AgentByServicePort.fetch({
            "serviceId": serviceMasterId,
            "portId": portMasterId
        }).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    var documentDetail = {};
                    documentDetail.dimensionList = [{}];
                    documentDetail.shipperAddress = {};
                    documentDetail.forwarderAddress = {};
                    documentDetail.consigneeAddress = {};
                    documentDetail.firstNotifyAddress = {};
                    documentDetail.secondNotifyAddress = {};
                    documentDetail.agentAddress = {};
                    documentDetail.issuingAgentAddress = {};
                    documentDetail.packMaster = {};
                    documentDetail.carrier = {};

                    var serviceDetail = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex];

                    if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                        documentDetail.dimensionUnit = true;
                        documentDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                    } else {
                        documentDetail.dimensionUnit = false;
                        documentDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                    }

                    documentDetail.volumeWeight = serviceDetail.bookedVolumeWeightUnitKg;
                    documentDetail.grossWeight = serviceDetail.bookedGrossWeightUnitKg;
                    documentDetail.bookedVolumeUnitCbm = serviceDetail.bookedVolumeUnitCbm;
                    documentDetail.volumeWeightInPound = serviceDetail.bookedVolumeWeightUnitPound
                    documentDetail.grossWeightInPound = serviceDetail.bookedGrossWeightUnitPound;
                    documentDetail.chargebleWeight = serviceDetail.bookedChargeableUnit;
                    documentDetail.noOfPieces = serviceDetail.bookedPieces;
                    documentDetail.packMaster = serviceDetail.packMaster;

                    var agentExport = data.responseObject;
                    if (agentExport != undefined &&
                        agentExport != null &&
                        agentExport.id != undefined &&
                        $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != undefined &&
                        $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != null &&
                        $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != undefined) {

                        documentDetail.agent = agentExport.agent;
                        $scope.getPartyAddress(documentDetail.agent, documentDetail.agentAddress);
                    }
                    if ($scope.shipment.isFromConsolForImport != undefined && ($scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport)) {
                        documentDetail.issuingAgent = null;
                        documentDetail.issuingAgentAddress = null;
                    } else {
                        if ($rootScope.userProfile.selectedUserLocation.tmpPartyMaster != undefined &&
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != null &&
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != undefined &&
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != null) {

                            documentDetail.issuingAgent = $rootScope.userProfile.selectedUserLocation.tmpPartyMaster;
                            $scope.getPartyAddress(documentDetail.issuingAgent, documentDetail.issuingAgentAddress);
                        }
                    }


                    if (serviceDetail != undefined && serviceDetail != null) {
                        if (serviceDetail.party != undefined && serviceDetail.party.id != null) {

                            if (serviceDetail.serviceMaster != undefined && serviceDetail.serviceMaster != null &&
                                serviceDetail.serviceMaster.id != null && serviceDetail.serviceMaster.importExport == 'Export') {
                                documentDetail.shipper = serviceDetail.party;
                                $scope.getPartyAddress(documentDetail.shipper, documentDetail.shipperAddress);
                            } else if (serviceDetail.serviceMaster != undefined && serviceDetail.serviceMaster != null &&
                                serviceDetail.serviceMaster.id != null && serviceDetail.serviceMaster.importExport == 'Import') {
                                documentDetail.consignee = serviceDetail.party;
                                $scope.getPartyAddress(documentDetail.consignee, documentDetail.consigneeAddress);
                            }

                        }
                        if (serviceDetail.origin != undefined)
                            documentDetail.origin = serviceDetail.origin;
                        if (serviceDetail.destination != undefined)
                            documentDetail.destination = serviceDetail.destination;
                        if (serviceDetail.pol != undefined)
                            documentDetail.pol = serviceDetail.pol;
                        if (serviceDetail.pod != undefined)
                            documentDetail.pod = serviceDetail.pod;
                        if (serviceDetail.mawbNo != undefined && serviceDetail.mawbNo != null)
                            documentDetail.mawbNo = serviceDetail.mawbNo;
                        if (serviceDetail.carrier != undefined && serviceDetail.carrier.id != undefined) {
                            documentDetail.carrier = serviceDetail.carrier;
                        }

                        if (serviceDetail.routeNo != undefined && serviceDetail.routeNo != null)
                            documentDetail.routeNo = serviceDetail.routeNo;

                        if (serviceDetail.eta != undefined && serviceDetail.eta != null) {
                            documentDetail.eta = serviceDetail.eta;
                        }

                        if (serviceDetail.etd != undefined && serviceDetail.etd != null) {
                            documentDetail.etd = serviceDetail.etd;
                        }


                    }
                    if (serviceDetail.documentList == undefined || serviceDetail.documentList == null) {
                        serviceDetail.documentList = [];
                    }
                    serviceDetail.documentList.push(documentDetail);
                    $scope.documentListTab = serviceDetail.documentList.length - 1;

                    if (flag) {
                        $scope.goToDocFocus(serviceDetail, 'documents', 'exprtdocShipper', 'importdocConsignee');
                    }
                }
            },
            function(errResponse) {
                console.error('Error while fetching agent  based on service port');
            }

        );

    }







    /* Party Select Picker */
    $scope.ajaxShipmentPartyEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.shipmentPartyList = data.responseObject.searchResult;
                    return $scope.shipmentPartyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedShipmentParty = function(obj) {
        console.log("selectedShipmentParty is running", obj);
        $scope.errorMap = new Map();
        if ($scope.validateShipment(1)) {
            $scope.getShipmentPartyAddress($scope.shipment.party);
            for (var int = 0; int < $scope.shipment.shipmentServiceList.length; int++) {
                if ($scope.shipment.shipmentServiceList[int].party == undefined ||
                    $scope.shipment.shipmentServiceList[int].party == null ||
                    $scope.shipment.shipmentServiceList[int].party.partyName == undefined ||
                    $scope.shipment.shipmentServiceList[int].party.partyName == null ||
                    $scope.shipment.shipmentServiceList[int].party.partyName == "") {
                    $scope.shipment.shipmentServiceList[int].party = $scope.shipment.party;
                }
            }

            $rootScope.navigateToNextField(obj);
        }
    };


    $scope.getShipmentPartyAddress = function(obj) {
        if (obj != null && obj.id != null) {
            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {
                    $scope.shipment.partyAddress = addressJoiner.joinAddressLineByLine($scope.shipment.partyAddress, $scope.shipment.party, obj.partyAddressList[i]);
                    break;
                }
            }
        } else {
            $scope.shipment.partyAddress = {};
        }

    }

    $scope.showShipmentPartyAddressList = false;
    $scope.shipmentPartyAddressListConfig = {
        search: false,
        address: true
    }
    $scope.partyAddressListShipment = [];
    $scope.showShipmentPartyAddress = function() {
        console.log("Showing Party address list...");
        $scope.panelTitle = "Party Address";
        $scope.showShipmentPartyAddressList = true;
        $scope.selectedItem = -1;
    };

    $scope.selectedShipmentPartyAddress = function(obj) {

        $scope.shipment.partyAddress = addressJoiner.joinAddressLineByLine($scope.shipment.partyAddress, $scope.shipment.party, obj);

        $scope.cancelShipmentPartyAddress();
    };

    $scope.cancelShipmentPartyAddress = function() {
        $scope.showShipmentPartyAddressList = false;
    }

    /* Origin List Picker */
    $scope.ajaxOriginEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortGroupList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.originList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {

                        $scope.originList = resultList;
                    }
                    return $scope.originList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Origin List');
            }
        );

    }

    $scope.selectedOrigin = function(obj, index, shipment) {
        if ($scope.validateShipment(6)) {
            $rootScope.navigateToNextField(obj);
            if (shipment.id != null && shipment.id != undefined) {
                $scope.setOriginDestData($scope.shipment.shipmentServiceList[index], index, shipment);
            }
        }

    };


    /* Destination Port - List Picker */
    $scope.ajaxDestination = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortGroupList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.destinationList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        $scope.destinationList = resultList;
                    }
                    return $scope.destinationList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Origin List');
            }
        );

    }

    $scope.selectedDestination = function(obj, index, shipment) {
        if ($scope.validateShipment(7)) {
            $rootScope.navigateToNextField(obj);
            if (shipment.id != null && shipment.id != undefined) {
                $scope.setOriginDestData($scope.shipment.shipmentServiceList[index], index, shipment);
            }
        }
    };


    /* Project Select Picker */

    $scope.ajaxCompanyEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ProjectList.query($scope.searchDto).$promise.then(function(data, status) {

                if (data.responseCode == "ERR0") {
                    $scope.projectList = data.responseObject.searchResult;
                    return $scope.projectList;

                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );
    }

    $scope.selectedProject = function(object) {
        if ($scope.validateShipment(8)) {
            $rootScope.navigateToNextField(object);
        }
    };


    /* show tos list*/

    $scope.ajaxTosEvent = function(object) {
        console.log("ajaxTosEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.tosList = data.responseObject.searchResult;
                    return $scope.tosList
                }
            },
            function(errResponse) {
                console.error('Error while fetching Tos');
            }
        );;

    }

    $scope.selectedTos = function(obj) {

        if ($scope.validateShipment(9)) {
            $scope.selectPrepaidCollect($scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
            $rootScope.navigateToNextField(obj);
        }
    };


    /* Commodity List Picker */

    $scope.ajaxCommodityEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CommoditySearchKeyword.fetch($scope.searchDto).$promise.then(function(
                data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.commodityMasterList = data.responseObject.searchResult;
                    return $scope.commodityMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching CommodityMaster');
            }
        );

    }

    $scope.selectedCommodityMaster = function(nex1, nex2) {
        if ($scope.validateShipment(10)) {
            if ($rootScope.dynamicFormFieldMap.get('Shipment > AES No') == 'true' || $rootScope.dynamicFormFieldMap.get('Shipment > AES No') == true) {
                $rootScope.navigateToNextField(nex2);
            } else {
                $rootScope.navigateToNextField(nex1);
            }
        }
    };


    /*Salesman Select Picker*/
    $scope.ajaxSalesmanEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return SalesmanList.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.salesmanList = data.responseObject.searchResult;
                    return $scope.salesmanList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Salesman');
            }
        );

    }

    $scope.selectedSalesman = function(obj) {

        if ($scope.validateShipment(11)) {
            $scope.changeRoutedBy($scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
            $rootScope.navigateToNextField(obj);

        }
    };


    /* Agent Select Picker */

    $scope.ajaxAgentEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.agentList = data.responseObject.searchResult;
                    return $scope.agentList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedAgent = function(partyobj) {

        if ($scope.validateShipment(11)) {
            $scope.changeRoutedBy($scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
            $rootScope.navigateToNextField(partyobj);
        }

    };

    /*Service tab started*/

    $scope.initClickTabIndex = function(obj) {
        var flag = false;
        $scope.selectedTabIndex = 0;
        if (obj.shipmentServiceList != undefined && obj.shipmentServiceList != null && obj.shipmentServiceList.length != 0) {

            for (var i = 0; i < obj.shipmentServiceList.length; i++) {
                if (obj.shipmentServiceList[i].location.id == $rootScope.userProfile.selectedUserLocation.id) {
                    $scope.selectedTabIndex = i;
                    flag = true;
                    break;
                }
            }
        }
        if (flag) {
            $scope.clickTabIndex($scope.selectedTabIndex);
        }
    };

    $scope.clickTabIndex = function(index) {
        $scope.selectedTabIndex = null;
        $scope.selectedTabIndex = index;
        if ($scope.shipment.isFromConsolForImport != undefined && ($scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport)) {
            $scope.shipmentTab = 'documents';
        } else {
            $scope.shipmentTab = 'general';
        }

    };

    $scope.deleteShipmentService = function(index) {
        console.log("Delete shipment service is called.....", index);
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR214"];
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            $scope.shipment.shipmentServiceList.splice(index, 1);
            $scope.initClickTabIndex($scope.shipment);
        }, function(value) {
            console.log("Shipment service Deletion cancelled");
        });
    }


    $scope.addShipmentServiceRow = function() {
        var serviceError = false;
        if ($scope.shipment.shipmentServiceList != undefined && $scope.shipment.shipmentServiceList != null && $scope.shipment.shipmentServiceList.length != undefined && $scope.shipment.shipmentServiceList.length > 0) {
            for (var serIdx = 0; serIdx < $scope.shipment.shipmentServiceList.length; serIdx++) {
                if ($scope.validateShipmentServiceDetail(0, $scope.shipment.shipmentServiceList[serIdx], serIdx) == false) {
                    serviceError = true;
                    $scope.clickTabIndex(serIdx);
                    break;
                }
            }
        }

        if (!serviceError) {
            $scope.errorMap = new Map();

            var shipmentServiceDetail = {};
            if ($scope.shipment.party != undefined && $scope.shipment.party != null && $scope.shipment.party != "") {
                shipmentServiceDetail.party = $scope.shipment.party;
            }
            shipmentServiceDetail.serviceReqDate = new Date();
            shipmentServiceDetail.pickUpDeliveryPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.transporter = {};
            shipmentServiceDetail.pickUpDeliveryPoint.pickupPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.pickupFrom = {};
            shipmentServiceDetail.pickUpDeliveryPoint.deliveryPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.deliveryFrom = {};
            shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryPoint = {};
            shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryFrom = {};
            shipmentServiceDetail.whoRouted = $scope.shipment.whoRouted;
            shipmentServiceDetail.customerService = $rootScope.userProfile.employee;
            shipmentServiceDetail.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;

            if ($scope.shipment.party != undefined && $scope.shipment.party != null) {
                shipmentServiceDetail.party = $scope.shipment.party;
            }

            if ($scope.shipment.whoRouted == false && $scope.shipment.salesman != undefined) {
                shipmentServiceDetail.salesman = $scope.shipment.salesman;

            } else if ($scope.shipment.whoRouted == true && $scope.shipment.agent != undefined) {
                shipmentServiceDetail.agent = $scope.shipment.agent;
            }

            if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                shipmentServiceDetail.dimensionUnit = true;
                shipmentServiceDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            } else {
                shipmentServiceDetail.dimensionUnit = false;
                shipmentServiceDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            }

            shipmentServiceDetail.ppcc = false;

            shipmentServiceDetail.location = $rootScope.userProfile.selectedUserLocation;
            shipmentServiceDetail.company = $rootScope.userProfile.selectedUserLocation.companyMaster;

            shipmentServiceDetail.documentList = [];
            shipmentServiceDetail.shipmentChargeList = [];
            shipmentServiceDetail.connectionList = [{ connectionStatus: 'Planned' }];
            shipmentServiceDetail.shipmentAttachmentList = [{}];
            shipmentServiceDetail.eventList = [{ isCompleted: 'Yes', followUpRequired: 'No' }];
            // Added for Reference Tab
            shipmentServiceDetail.referenceList = [{}];

            shipmentServiceDetail.shipmentServiceTriggerList = [];

            shipmentServiceDetail.authenticatedDocList = [{}];



            // Attachment Configuration for each shipment service
            shipmentServiceDetail.attachConfig = {
                "isEdit": true,
                "isDelete": true,
                "page": "shipment",
                "columnDefs": [

                    {
                        "name": "Document Name",
                        "model": "documentMaster.documentName",
                        "type": "text"
                    }, {
                        "name": "Reference #",
                        "model": "refNo",
                        "type": "text"
                    }, {
                        "name": "Customs",
                        "model": "customs",
                        "type": "text"
                    }, {
                        "name": "Unprotected File Name",
                        "model": "unprotectedFileName",
                        "type": "file"
                    }, {
                        "name": "Protected File Name",
                        "model": "protectedFileName",
                        "type": "file"
                    }, {
                        "name": "Actions",
                        "model": "action",
                        "type": "action"
                    }
                ]
            }



            // Assigning Default Package in Shipment Service Detail configured in appMasterData..
            shipmentServiceDetail.packMaster = {}
            var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
            if (defaultPackageCode != undefined && defaultPackageCode != null) {
                console.log("Default Package is assigned....");
                PackGetByCode.get({
                    code: defaultPackageCode
                }, function(data) {
                    if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                        console.log("Successful while getting pack.", data)
                        shipmentServiceDetail.packMaster = data.responseObject;
                    }
                }, function(error) {
                    console.log("Error while getting pack.", error)
                });
            } else {
                console.log("Default Package Code is not defined in Default Master Data....");
            }


            $scope.shipment.shipmentServiceList.push(shipmentServiceDetail);

            $scope.clickTabIndex($scope.shipment.shipmentServiceList.length - 1);
            $scope.setOldDataVal();

        }
    };

    /* Service Detail service List Picker */

    $scope.ajaxServiceEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ServiceByTransportMode.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceMasterList = data.responseObject.searchResult;
                    return $scope.serviceMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ServiceMaster');
            }
        );

    }

    $scope.selectedServiceMaster = function(serviceDetail, index, nextIdValue, shipment) {
        serviceDetail.tradeCode = serviceDetail.serviceMaster.importExport;
        if ($scope.validateShipmentServiceDetail(1, serviceDetail, index)) {
            $scope.setOriginDestData(serviceDetail, index, shipment);
            $rootScope.navigateToNextField(nextIdValue + index);
        }
    };


    $scope.setOriginDestData = function(serviceDetail, iindex, sp) {
        if (sp.origin != undefined && sp.origin.portMasterList != undefined && sp.origin.portMasterList != null && sp.origin.portMasterList.length > 0) {
            angular.forEach(sp.origin.portMasterList, function(port, index) {
                if (port.transportMode === "Air") {
                    $scope.shipment.shipmentServiceList[iindex].origin = {};
                    $scope.shipment.shipmentServiceList[iindex].origin = port;
                    $scope.shipment.shipmentServiceList[iindex].pol = {};
                    $scope.shipment.shipmentServiceList[iindex].pol = port;
                }
            });
        }

        if (sp.destination != undefined && sp.destination.portMasterList != undefined && sp.destination.portMasterList != null && sp.destination.portMasterList.length > 0) {
            angular.forEach(sp.destination.portMasterList, function(port, index) {
                if (port.transportMode === "Air") {
                    $scope.shipment.shipmentServiceList[iindex].destination = {};
                    $scope.shipment.shipmentServiceList[iindex].destination = port;
                    $scope.shipment.shipmentServiceList[iindex].pod = {};
                    $scope.shipment.shipmentServiceList[iindex].pod = port;
                }
            });
        }
    }


    /* Service Detail Origin List Picker */

    $scope.ajaxServiceOriginEvent = function(serviceDetail, index, object) {
        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '' || serviceDetail.serviceMaster.id == null) {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90031"]);
            return false;
        }

        /*  if ($scope.shipment.origin == undefined || $scope.shipment.origin == null || $scope.shipment.origin == '') {
              console.log($rootScope.nls["ERR90013"]);
              $scope.errorMap.put("origin", $rootScope.nls["ERR90013"]);
              return false;
          }*/


        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceOriginList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (serviceDetail.destination == undefined || serviceDetail.destination == null) {
                            $scope.serviceOriginList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != serviceDetail.destination.id) {
                                    $scope.serviceOriginList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.serviceOriginList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Port');
            }
        );


    }

    $scope.selectedServiceOrigin = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(2, serviceDetail, index)) {


            // Assign the default POL to Origin
            if (serviceDetail.pol == undefined || serviceDetail.pol == null) {
                serviceDetail.pol = serviceDetail.origin;
            }

            $rootScope.navigateToNextField(obj + index);
        }
    };


    /* Service Detail Destination Port - List Picker */

    $scope.ajaxServiceDestination = function(serviceDetail, index, object) {
        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '' || serviceDetail.serviceMaster.id == null) {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90031"]);
            return false;
        }
        /* if ($scope.shipment.destination == undefined || $scope.shipment.destination == null || $scope.shipment.destination == '') {
             console.log($rootScope.nls["ERR90014"]);
             $scope.errorMap.put("destination", $rootScope.nls["ERR90014"]);
             return false;
         }*/

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceDestinationList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (serviceDetail.origin == undefined || serviceDetail.origin == null) {
                            $scope.serviceDestinationList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != serviceDetail.origin.id) {
                                    $scope.serviceDestinationList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.serviceDestinationList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Port');
            }
        );

    }

    $scope.selectedServiceDestination = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(5, serviceDetail, index)) {
            $rootScope.navigateToNextField(obj + index);
        }
    };

    /* Service Detail Port Of Loading List Picker */

    $scope.ajaxPortOfLoadingEvent = function(serviceDetail, index, object) {
        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '' || serviceDetail.serviceMaster == null) {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90031"]);
            return false;
        }

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.portOfLoadingList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (serviceDetail.pod == undefined || serviceDetail.pod == null) {
                            $scope.portOfLoadingList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != serviceDetail.pod.id) {
                                    $scope.portOfLoadingList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.portOfLoadingList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Port');
            }
        );

    }

    $scope.selectedPortOfLoading = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(3, serviceDetail, index)) {
            $rootScope.navigateToNextField(obj + index);
        }
    };


    /* Service Detail Port Of Discharge - List Picker */

    $scope.ajaxPortOfDischarge = function(serviceDetail, index, object) {
        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '' || serviceDetail.serviceMaster.id == null) {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90031"]);
            return false;
        }

        /* if ($scope.shipment.destination == undefined || $scope.shipment.destination == null || $scope.shipment.destination == '') {
             console.log($rootScope.nls["ERR90014"]);
             $scope.errorMap.put("destination", $rootScope.nls["ERR90014"]);
             return false;
         }*/

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.portOfDischargeList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (serviceDetail.pol == undefined || serviceDetail.pol == null) {
                            $scope.portOfDischargeList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != serviceDetail.pol.id) {
                                    $scope.portOfDischargeList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.portOfDischargeList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Port');
            }
        );

    }

    $scope.selectedPortOfDischarge = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(4, serviceDetail, index)) {

            // Assign default destination to pod
            if (serviceDetail.destination == undefined || serviceDetail.destination == null) {
                serviceDetail.destination = serviceDetail.pod;
            }

            $rootScope.navigateToNextField(obj + index);
        }
    };


    /* Service Detail Salesman Select Picker*/

    $scope.ajaxServiceDetailSalesmanEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return SalesmanList.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceDetailSalesmanList = data.responseObject.searchResult;
                    return $scope.serviceDetailSalesmanList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Salesman');
            }
        );


    }

    $scope.selectedServiceDetailSalesman = function(serviceDetail, index, obj) {

        if ($scope.validateShipmentServiceDetail(6, serviceDetail, index)) {
            $rootScope.navigateToNextField(obj + index);
        }
    };



    /* Service Detail Agent Select Picker */

    $scope.ajaxServiceDetailAgentEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceDetailAgentList = data.responseObject.searchResult;
                    return $scope.serviceDetailAgentList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedServiceDetailAgent = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(6, serviceDetail, index)) {
            $rootScope.navigateToNextField(obj + index);
        }
    };



    /* shipment service detail Party Select Picker */


    $scope.ajaxShipmentServiceDetailPartyEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.shipmentServiceDetailPartyList = data.responseObject.searchResult;
                    return $scope.shipmentServiceDetailPartyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedShipmentServiceDetailParty = function(serviceDetail, index, partyobj) {
        if ($scope.validateShipmentServiceDetail(7, serviceDetail, index)) {
            $rootScope.navigateToNextField(partyobj + index);
        }
    };

    $scope.cancelShipmentServiceDetailParty = function() {
        $scope.showShipmentServiceDetailPartyList = false;
    }

    $scope.shipmentServiceDetailPartylistConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
            "title": "partyName",
            seperator: false
        }, {
            "title": "partyCode",
            seperator: true
        }]
    }


    /*customer service list */
    $scope.ajaxCustomerServiceEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return EmployeeList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.customerServiceList = data.responseObject.searchResult;
                    return $scope.customerServiceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Customer Services');
            }
        );

    }
    $scope.selectedCustomerService = function(serviceDetail, index, customerServiceObj) {
        if ($scope.validateShipmentServiceDetail(8, serviceDetail, index)) {
            $rootScope.navigateToNextField(customerServiceObj + index);
        }
    };

    /* shipment service detail CoLoader Select Picker */

    $scope.ajaxShipmentServiceDetailCoLoaderEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.shipmentServiceDetailCoLoaderList = data.responseObject.searchResult;
                    return $scope.shipmentServiceDetailCoLoaderList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching CoLoader');
            }
        );

    }

    $scope.selectedShipmentServiceDetailCoLoader = function(serviceDetail, index, coLoaderobj) {
        if ($scope.validateShipmentServiceDetail(9, serviceDetail, index)) {
            $rootScope.navigateToNextField(coLoaderobj + index);
        }
    };

    /*Shipment service details division list*/

    $scope.ajaxDivisionEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return DivisionListByCompany.fetch({
            "companyId": $rootScope.userProfile.selectedUserLocation.companyMaster.id
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.divisionList = data.responseObject.searchResult;
                    return $scope.divisionList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching division List Based on the Company');
            }
        );

    }

    $scope.selectedDivision = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(10, serviceDetail, index)) {
            $rootScope.navigateToNextField(obj + index);
        }
    };


    /*shipment service details pack list */

    $scope.ajaxPackEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PackList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.packList = data.responseObject.searchResult;
                    return $scope.packList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Pack List');
            }
        );

    }

    $scope.selectedPack = function(serviceDetail, index, packObj) {
        if ($scope.validateShipmentServiceDetail(11, serviceDetail, index)) {
            $scope.updateMoreInfoToDocument('Pack', packObj + index)
        }
    };

    $scope.selectedDocumentPackMaster = function(documentDetail, pIndex, index, nextFocus) {
        if ($scope.validateServiceDocument(31, documentDetail, pIndex, index)) {
            if ($scope.isDocumentDimensionFound(documentDetail)) {
                $rootScope.navigateToNextField("ratePerCharge" + pIndex + "_" + index);
            } else {
                $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
            }

        }
    };


    /*shipment service details carrier list*/

    $scope.ajaxCarrierEvent = function(serviceDetail, object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CarrierByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.carrierMasterList = data.responseObject.searchResult;
                    return $scope.carrierMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air');
            }
        );

    }

    $scope.selectedDocumentCarrierMaster = function(documentDetail, pIndex, index, nextFocus) {
        if ($scope.validateServiceDocument(24, documentDetail, pIndex, index)) {
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }
    };

    $scope.selectedCarrierMaster = function(serviceDetail, index, obj) {
        if ($scope.validateShipmentServiceDetail(17, serviceDetail, index)) {
            serviceDetail.routeNo = null;
            serviceDetail.eta = null;
            serviceDetail.etd = null;
            serviceDetail.scheduleUid = null;
            serviceDetail.vesselId = null;
            $rootScope.navigateToNextField(obj + index);
        }

    };



    /*save shipment service details*/

    /* $scope.saveShipmentServiceValidate = function () {
         	                 		console.log("Save ShipmentServiceDetail is Called.....");
         	                 		
         	                 		$scope.setConnectionState();
         	                 		
         	                 		if(!$scope.tableConnectionState){
                 	  	 				console.log("table Connection  invalid")
                 	  	 				$scope.shipmentTab=='connections';
                 	  	 			$rootScope.clientMessage = $rootScope.nls["ERR90560"];
                 	                  return false;
         	                 	    }	
         	                 		
         	                 		if(!$scope.tableRateState){
         	                 			console.log("table Rates invalid")
         	                 			$scope.showRateErrorState=null;
         	                 		 $timeout(function(){
         	                 			$scope.showRateErrorState=$scope.tableRateState;
                      	                },1)
                      	             $rootScope.clientMessage = $rootScope.nls["ERR90561"];
                	                  return false;
         	                 		}
         	                 		
         	                 		if($scope.addDocument && !$scope.saveDocument()){
         	                 			return false;
         	                 		}
         	                 		
         	                 		
         	                 		if ($scope.validateShipmentServiceDetail(0) && $scope.validatePickUpDelivery(0)){
         	                 			return true;
         	                 		} 
         	                 		
         	                 		return false;


         	                 	}

              	   	           $scope.saveShipmentServiceDetail = function () {
              	                 		console.log("Save ShipmentServiceDetail is Called.....");
              	                 		
              	                 		if($scope.saveShipmentServiceValidate()){
              	                 			if ($scope.editServiceIndex == null) {
              	                 				if ($scope.shipment.shipmentServiceList == null)
              	                 					$scope.shipment.shipmentServiceList = [];
              	                 				$scope.saveShipmentAttachment();
              	                 				
              	                 				$scope.shipment.shipmentServiceList.push($scope.shipmentServiceDetail);
              	                 				$scope.closeShipmenService();
              	                 			}
              	                 			else {
              	                 				$scope.saveShipmentAttachment();
              	                               	$scope.shipment.shipmentServiceList.splice($scope.editServiceIndex, 1,$scope.shipmentServiceDetail);
              	                 				$scope.closeShipmenService();
              	                 			}
              	                 			$scope.showEditShipmentServiceDetail=false;
              	                 			$scope.addShipment=true;
              	                 			$scope.editServiceIndex=null;
              	                 		}


              	                 	}*/




    /*
        $scope.discardShipmenService = function(){
    		if(!discardService1.compare($scope.shipmentServiceDetail)){

    			ngDialog.openConfirm({
    				template: '<p>Do you want to update your changes?</p>' +
    				'<div class="ngdialog-footer">' +
    					'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
    					'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
    				'</div>',
    				plain: true,
    				className: 'ngdialog-theme-default'
    			}).then(function (value) {
    				if(value == 1 ) {
    					$scope.saveShipmentServiceDetail();
    				}else{
    					$scope.closeShipmenService();
    				}
    				});

    		}else{
    			$scope.closeShipmenService();
    		}
    	}

        $scope.closeShipmenService= function(){

    		$scope.errorMap = new Map();
    		$scope.addShipment = true;
    		$scope.moreAddressTab='moreInfo';
    		 $scope.showRateEdit = true;
    		 $scope.showDocumentEdit = true;
    	}*/




    $scope.cancelToList = function() {


        if ($scope.oldData != JSON.stringify($scope.shipment)) {
            $scope.isFormValueDirty = true;
        }
        if ($scope.isFormValueDirty == true) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        if (value == 1 && $scope.shipment != undefined &&
                            ($scope.shipment.id == null || $scope.shipment.id == undefined || $scope.shipment.id == -1)) {
                            $scope.saveShipment();
                        } else if (value == 1 &&
                            $scope.shipment != undefined && $scope.shipment.id != null) {
                            $scope.isCreateConsol = null;
                            $scope.updateShipment();
                        } else if (value == 2) {
                            if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {
                                var params = {};
                                params.forPurpose = "createdShipmentFromConsol";
                                params.shipmentUid = $scope.shipment.shipmentUid;
                                $state.go($stateParams.fromScreen, params);
                                console.log("navigate to consol screen");
                            } else if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
                                if (window.history != undefined)
                                    window.history.back();
                            } else {
                                var params = { submitAction: 'Cancelled' }
                                $state.go('layout.crmShipment', params);
                            }
                        } else {
                            console.log("cancelled");
                        }

                    });

        } else {
            var params = {};
            if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {
                params.forPurpose = "createdShipmentFromConsol";
                params.shipmentUid = $scope.shipment.shipmentUid;
                $state.go($stateParams.fromScreen, params);
                console.log("navigate to consol screen");
            } else if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
                if (window.history != undefined)
                    window.history.back();
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go('layout.crmShipment');
            }
        }
    }




    /*	 $scope.deleteMaster = function() {
            console.log("Delete Button is pressed....");

            ngDialog.openConfirm({
                template:
                '<p>Are you sure you want to delete selected shipment ?</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No</button>' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
                '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
                then(function (value) {
                    console.log("delete shipment")
                }, function (value) {
                    console.log("deleted cancelled");

                });

                }*/


    /*	$scope.assignDocValues=function(){

    		if($scope.documentDetail!=undefined && $scope.documentDetail!=null)

    		if($scope.documentDetail.origin!=undefined)
         	   $scope.documentDetail.origin=$scope.shipmentServiceDetail.origin;
         	if($scope.documentDetail.destination!=undefined)
         		 $scope.documentDetail.destination=$scope.shipmentServiceDetail.destination;
         	if($scope.documentDetail.pol!=undefined)
         		 $scope.documentDetail.pol=$scope.shipmentServiceDetail.pol;
         	if($scope.documentDetail.pod!=undefined)
         		 $scope.documentDetail.pod=$scope.shipmentServiceDetail.pod;

         	if($scope.documentDetail.bookedPieces!=undefined)
         		 $scope.documentDetail.noOfPieces=$scope.shipmentServiceDetail.bookedPieces

         	if($scope.documentDetail.bookedGrossWeightUnitKg!=undefined)
         		 $scope.documentDetail.grossWeight=$scope.shipmentServiceDetail.bookedGrossWeightUnitKg;

         	if($scope.documentDetail.bookedVolumeWeightUnitKg!=undefined)
         		 $scope.documentDetail.volumeWeight=$scope.shipmentServiceDetail.bookedVolumeWeightUnitKg;

         	if($scope.documentDetail.bookedGrossWeightUnitPound!=undefined)
         		 $scope.documentDetail.bookedGrossWeightUnitPound=$scope.shipmentServiceDetail.bookedGrossWeightUnitPound;

         	if($scope.documentDetail.bookedVolumeWeightUnitPound!=undefined)
         		 $scope.documentDetail.volumeWeightInPound=$scope.shipmentServiceDetail.bookedVolumeWeightUnitPound;
         	if($scope.documentDetail.bookedChargeableUnit!=undefined)
         		 $scope.documentDetail.chargebleWeight=$scope.shipmentServiceDetail.bookedChargeableUnit;


    	}*/
    $scope.$on('addCrmShipmentEventMenuNavi', function(events, moveToScreen) {
        $scope.move(moveToScreen);

    })
    $scope.$on('editCrmShipmentEventMenuNavi', function(events, moveToScreen) {
        $scope.move(moveToScreen);

    })

    $scope.move = function(moveToScreen) {
        if ($scope.shipmentForm.$dirty) {
            ngDialog.openConfirm({
                template: '<p>Do you want to Save your changes?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    ' <button type="button" class="btn btn-default btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-primary pull-left btn-property grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    if ($scope.shipment.id != null) {
                        $scope.isCreateConsol = null;
                        $scope.updateShipment();
                    } else {
                        $scope.saveShipment();
                    }
                } else if (value == 2) {
                    $state.go(moveToScreen, {
                        submitAction: 'Cancelled'
                    });
                } else if (value == 3) {
                    //$state.go(args);
                    console.log("cancelled");
                } else {
                    console.log("nothing selected")
                }
            }, function(error) {
                console.log('error occurred');
            });
        } else {
            console.log("No Dirty" + moveToScreen);
            $state.go(moveToScreen);
        }

    }


    $scope.saveShipment = function() {
        if (!$scope.disableSubmitBtn) {
            $scope.disableSubmitBtn = true;
            console.log("Save Method is called.");

            $rootScope.clientMessage = null;
            if ($scope.validateShipment(0)) {

                var minimumSipment = false;

                for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {


                    if ($scope.validateShipmentServiceDetail(0, $scope.shipment.shipmentServiceList[i], i)) {

                        if ($scope.shipment.shipmentServiceList[i].documentList == undefined ||
                            $scope.shipment.shipmentServiceList[i].documentList == null ||
                            $scope.shipment.shipmentServiceList[i].documentList.length <= 0) {
                            Notification.error($rootScope.nls["ERR90558"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            $log.error($rootScope.nls["ERR90558"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'documents')
                            return false;
                        }
                        for (var j = 0; j < $scope.shipment.shipmentServiceList[i].documentList.length; j++) {
                            if ($scope.validateServiceDocument(0, $scope.shipment.shipmentServiceList[i].documentList[j], i, j) == false) {
                                console.log("Document Validation failed :" + i + ":" + j);
                                Notification.error($rootScope.nls["ERR90574"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                $log.error($rootScope.nls["ERR90574"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                $scope.activeViewTabAndService(i, 'documents');
                                $scope.enableSubmitBtnFn();
                                return false;
                            } else {
                                if ($scope.shipment.shipmentServiceList[i].documentList[j].isDimensionStatus() == false) {
                                    Notification.error($rootScope.nls["ERR8340"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                    console.log("isDimensionStatus failed :" + i + ":" + j);
                                    $log.error($rootScope.nls["ERR8340"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                    $scope.enableSubmitBtnFn();
                                    $scope.activeViewTabAndService(i, 'documents');
                                    return false;
                                }
                            }

                        }
                        if ($scope.shipment.shipmentServiceList[i].isConnectionCheck() == false) {
                            console.log("isConnectionCheck failed :" + i);
                            Notification.error($rootScope.nls["ERR90560"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'connections');
                            return false;
                        }

                        var connectionRequired = $rootScope.appMasterData['connection.required'];
                        $scope.connectionRequiredFlag = false;
                        if (connectionRequired.toLowerCase() == 'true' || connectionRequired == true) {


                            if ($scope.shipment.shipmentServiceList[i].origin != null && $scope.shipment.shipmentServiceList[i].origin.id != null) {
                                if ($scope.shipment.shipmentServiceList[i].pol != null && $scope.shipment.shipmentServiceList[i].pol.id != null) {
                                    if ($scope.shipment.shipmentServiceList[i].origin.id != $scope.shipment.shipmentServiceList[i].pol.id) {
                                        $scope.connectionRequiredFlag = true;
                                    }
                                }
                            }

                            if ($scope.shipment.shipmentServiceList[i].destination != null && $scope.shipment.shipmentServiceList[i].destination.id != null) {
                                if ($scope.shipment.shipmentServiceList[i].pod != null && $scope.shipment.shipmentServiceList[i].pod.id != null) {
                                    if ($scope.shipment.shipmentServiceList[i].destination.id != $scope.shipment.shipmentServiceList[i].pod.id) {
                                        $scope.connectionRequiredFlag = true;
                                    }
                                }
                            }


                            if ($scope.connectionRequiredFlag) {
                                if ($scope.shipment.shipmentServiceList[i].connectionList == undefined || $scope.shipment.shipmentServiceList[i].connectionList == null || $scope.shipment.shipmentServiceList[i].connectionList.length == 0) {
                                    Notification.error($rootScope.nls["ERR90364"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                    $scope.shipment.shipmentServiceList[i].connectionList = [{}];
                                    $scope.activeViewTabAndService(i, 'connections');
                                    $scope.enableSubmitBtnFn();
                                    return false;
                                }

                            }

                        }

                        if ($scope.shipment.shipmentServiceList[i].isRatesCheck() == false) {
                            Notification.error($rootScope.nls["ERR90561"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            console.log("isRatesCheck failed :" + i);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'rates');
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isStatusCheck() == false) {
                            Notification.error($rootScope.nls["ERR90562"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            console.log("isStatusCheck failed :" + i);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'statusUpdate');
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isEventCheck() == false) {
                            Notification.error($rootScope.nls["ERR90563"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            console.log("isEventCheck failed :" + i);
                            $scope.activeViewTabAndService(i, 'events');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isReferenceCheck() == false) {
                            Notification.error($rootScope.nls["ERR90573"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            console.log("isReferenceCheck failed :" + i);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'references');
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isAttachmentCheck() == false) {
                            Notification.error($rootScope.nls["ERR250"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            console.log("Attachment is invalid");
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'attachMent');
                            return false;
                        }


                        if ($scope.shipment.shipmentServiceList[i].authenticatedDocList != undefined && $scope.shipment.shipmentServiceList[i].authenticatedDocList != null &&
                            $scope.shipment.shipmentServiceList[i].authenticatedDocList.length > 0) {

                            for (var j = 0; j < $scope.shipment.shipmentServiceList[i].authenticatedDocList.length; j++) {

                                if (isEmptyRow($scope.shipment.shipmentServiceList[i].authenticatedDocList[j])) {

                                    $scope.shipment.shipmentServiceList[i].authenticatedDocList.splice(j, 1);
                                    delete $scope.shipment.shipmentServiceList[i].authenticatedDocList[j];

                                }

                            }

                        }

                        if ($scope.callValidateAuthenticatedDocs($scope.shipment.shipmentServiceList[i]) == false) {
                            Notification.error($rootScope.nls["ERR05571"]);
                            $scope.activeViewTabAndService(i, 'AuthenticatedDocs');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }


                        //pickup delivery validation 
                        if ($scope.validatePickUpDelivery(0, $scope.shipment.shipmentServiceList[i], i) == false) {
                            Notification.error();
                            $scope.activeViewTabAndService(i, 'pickup');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }


                    } else {
                        $scope.clickTabIndex(i);
                        $scope.enableSubmitBtnFn();
                        return false;
                    }

                    if ($scope.shipment.shipmentServiceList[i].serviceMaster.importExport === 'Export') {
                        var volweight = 0;
                        var grossWeight = 0;
                        var volpound = 0;
                        var grosspound = 0;
                        var chargebleweight = 0;
                        var volumeInCbm = 0;
                        var noofpieces = 0;

                        for (var j = 0; j < $scope.shipment.shipmentServiceList[i].documentList.length; j++) {

                            volweight += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].volumeWeight);

                            grossWeight += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].grossWeight);

                            volpound += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].volumeWeightInPound);

                            grosspound += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].grossWeightInPound);

                            chargebleweight += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].chargebleWeight);

                            noofpieces += parseInt($scope.shipment.shipmentServiceList[i].documentList[j].noOfPieces);

                            if ($scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm != undefined &&
                                $scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm != null &&
                                $scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm != '') {
                                volumeInCbm += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm);
                            }
                        }

                        if ($scope.shipment.shipmentServiceList[i].bookedVolumeWeightUnitKg != volweight) {
                            Notification.error($rootScope.nls["ERR90578"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'general');
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedGrossWeightUnitKg != grossWeight) {
                            Notification.error($rootScope.nls["ERR90577"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'general');
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedVolumeWeightUnitPound != volpound) {
                            Notification.error($rootScope.nls["ERR90581"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'general');
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedGrossWeightUnitPound != grosspound) {
                            Notification.error($rootScope.nls["ERR90580"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'general');
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedChargeableUnit != chargebleweight) {
                            Notification.error($rootScope.nls["ERR90579"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'general');
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedPieces != noofpieces) {
                            Notification.error($rootScope.nls["ERR90583"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.enableSubmitBtnFn();
                            $scope.activeViewTabAndService(i, 'general');
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != undefined && volumeInCbm != undefined &&
                            $scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != null && volumeInCbm != null &&
                            $scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != '' && volumeInCbm != '') {

                            if ($scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != volumeInCbm) {
                                Notification.error($rootScope.nls["ERR90582"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                                $scope.enableSubmitBtnFn();
                                $scope.activeViewTabAndService(i, 'general');
                                return false;
                            }
                        }

                        if ($scope.shipment.shipmentServiceList[i].bookedChargeableUnit < $scope.appMasterData['shipment.minimum.chargeable.weight']) {
                            minimumSipment = true;
                        }

                        if (minimumSipment) {
                            $scope.minimumShipmentAlert();
                        } else {
                            $scope.addToDatabase();
                        }
                    } else {
                        $scope.addToDatabase();
                    }

                }



            } else {
                console.log("Invalid Form Submission.");
                $scope.enableSubmitBtnFn();
            }
        }
    }

    $scope.addToDatabase = function() {
        $rootScope.mainpreloder = true;
        $scope.tmpObj = cloneService.clone($scope.shipment);
        $scope.tmpObj.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
        $scope.tmpObj.location = $rootScope.userProfile.selectedUserLocation;

        $scope.tmpObj.directShipment = $scope.tmpObj.directShipment == false ? 'No' : 'Yes';
        $scope.tmpObj.whoRouted = $scope.tmpObj.whoRouted == false ? 'Self' : 'Agent';
        for (var i = 0; i < $scope.tmpObj.shipmentServiceList.length; i++) {
            $scope.tmpObj.shipmentServiceList[i].company = $rootScope.userProfile.selectedUserLocation.companyMaster;
            $scope.tmpObj.shipmentServiceList[i].location = $rootScope.userProfile.selectedUserLocation;
            $scope.tmpObj.shipmentServiceList[i].whoRouted = $scope.tmpObj.shipmentServiceList[i].whoRouted == false ? 'Self' : 'Agent';
            $scope.tmpObj.shipmentServiceList[i].eta = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].eta);
            $scope.tmpObj.shipmentServiceList[i].etd = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].etd);

            $scope.tmpObj.shipmentServiceList[i].ppcc = $scope.tmpObj.shipmentServiceList[i].ppcc == false ? 'Prepaid' : 'Collect';


            $scope.tmpObj.shipmentServiceList[i].company = $rootScope.userProfile.selectedUserLocation.companyMaster;
            $scope.tmpObj.shipmentServiceList[i].location = $rootScope.userProfile.selectedUserLocation;


            $scope.tmpObj.shipmentServiceList[i].isAgreed = 'No';
            $scope.tmpObj.shipmentServiceList[i].isMarksNo = 'No';
            $scope.tmpObj.shipmentServiceList[i].serviceReqDate = $rootScope.sendApiDateAndTime(new Date());

            if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint == undefined) {
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint = {};
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = 'No';
            } else {
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp ? 'Yes' : 'No';
            }


            if ($scope.tmpObj.shipmentServiceList[i].connectionList != undefined && $scope.tmpObj.shipmentServiceList[i].connectionList != null && $scope.tmpObj.shipmentServiceList[i].connectionList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].connectionList.length; j++) {
                    $scope.tmpObj.shipmentServiceList[i].connectionList[j].eta = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].connectionList[j].eta);
                    $scope.tmpObj.shipmentServiceList[i].connectionList[j].etd = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].connectionList[j].etd);

                }
            }

            if ($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList != undefined && $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList != null && $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList.length; j++) {
                    $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date);
                    if ($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired == 'Yes' || $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired == true) {
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired = 'Yes';
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate);
                    } else {
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired = 'No';
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate = null;
                    }

                    $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].protect = $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].protect ? 'Yes' : 'No';
                    $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].completed = $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].completed ? 'Yes' : 'No';

                }
            }

            if ($scope.tmpObj.shipmentServiceList[i].eventList != undefined && $scope.tmpObj.shipmentServiceList[i].eventList != null && $scope.tmpObj.shipmentServiceList[i].eventList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].eventList.length; j++) {

                    $scope.tmpObj.shipmentServiceList[i].eventList[j].eventDate = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].eventList[j].eventDate);


                    if ($scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired == 'Yes' || $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired == true) {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired = 'Yes';
                    } else {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired = 'No';
                    }

                    if ($scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate != undefined &&
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate != null) {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate);
                    } else {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate = null;
                    }

                    $scope.tmpObj.shipmentServiceList[i].eventList[j].isCompleted = $scope.tmpObj.shipmentServiceList[i].eventList[j].isCompleted ? 'Yes' : 'No';

                }
            }


            if ($scope.tmpObj.shipmentServiceList[i].documentList != undefined && $scope.tmpObj.shipmentServiceList[i].documentList != null && $scope.tmpObj.shipmentServiceList[i].documentList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].documentList.length; j++) {

                    $scope.tmpObj.shipmentServiceList[i].documentList[j].documentReqDate = $rootScope.sendApiDateAndTime(new Date());
                    $scope.tmpObj.shipmentServiceList[i].documentList[j].eta = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].documentList[j].eta);
                    $scope.tmpObj.shipmentServiceList[i].documentList[j].etd = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].documentList[j].etd);
                    if ($scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn != undefined && $scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn != null)
                        $scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn);
                }
            }

            if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint != undefined && $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint != null &&
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint != "") {

                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual);

            }
            if ($scope.tmpObj.shipmentServiceList[i].authenticatedDocList != undefined && $scope.tmpObj.shipmentServiceList[i].authenticatedDocList != null && $scope.tmpObj.shipmentServiceList[i].authenticatedDocList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].authenticatedDocList.length; j++) {
                    $scope.tmpObj.shipmentServiceList[i].authenticatedDocList[j].date = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].authenticatedDocList[j].date);


                }
            }
        }



        ShipmentAdd.save($scope.tmpObj).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                $rootScope.mainpreloder = false;
                $scope.showResponse(data.responseObject, true);
            } else {
                console.log("Shipment added Failed " + data.responseDescription)
            }
            $scope.enableSubmitBtnFn();

        }, function(error) {
            console.log("Shipment added Failed : " + error)
            $scope.enableSubmitBtnFn();
        });

    }


    $scope.removeEmptyRow = function() {
        for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {
            for (var j = 0; j < $scope.shipment.shipmentServiceList[i].documentList.length; j++) {
                $scope.shipment.shipmentServiceList[i].documentList[j]
                    .isDimensionStatus();
            }

            $scope.shipment.shipmentServiceList[i]
                .isConnectionCheck();
            $scope.shipment.shipmentServiceList[i].isRatesCheck();
            $scope.shipment.shipmentServiceList[i].isStatusCheck();
            $scope.shipment.shipmentServiceList[i].isEventCheck();
            $scope.shipment.shipmentServiceList[i]
                .isReferenceCheck();
            $scope.shipment.shipmentServiceList[i]
                .isAttachmentCheck();

            if ($scope.shipment.shipmentServiceList[i].authenticatedDocList != undefined &&
                $scope.shipment.shipmentServiceList[i].authenticatedDocList != null &&
                $scope.shipment.shipmentServiceList[i].authenticatedDocList.length > 0) {
                for (var j = 0; j < $scope.shipment.shipmentServiceList[i].authenticatedDocList.length; j++) {
                    if (isEmptyRow($scope.shipment.shipmentServiceList[i].authenticatedDocList[j])) {
                        $scope.shipment.shipmentServiceList[i].authenticatedDocList
                            .splice(j, 1);
                        delete $scope.shipment.shipmentServiceList[i].authenticatedDocList[j];
                    }

                }

            }
        }
    }

    $scope.updateShipment = function(forWhichPurpose) {

        if (!$scope.disableSubmitBtn) {
            $scope.disableSubmitBtn = true;
            $rootScope.clientMessage = null;

            if ($scope.validateShipment(0)) {
                var minimumSipment = false;
                for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {

                    if ($scope.shipment.shipmentServiceList[i].location.id != $rootScope.userProfile.selectedUserLocation.id) {
                        $scope.removeEmptyRow();
                        continue;
                    }

                    var id = $scope.shipment.shipmentServiceList[i].serviceUid;

                    if ($scope.validateShipmentServiceDetail(0, $scope.shipment.shipmentServiceList[i], i)) {

                        if ($scope.shipment.shipmentServiceList[i].documentList == undefined ||
                            $scope.shipment.shipmentServiceList[i].documentList == null ||
                            $scope.shipment.shipmentServiceList[i].documentList.length <= 0) {
                            if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                Notification.error($rootScope.nls["ERR90558"] + "  " + $rootScope.nls["ERR90572"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            }
                            $scope.activeViewTabAndService(i, 'documents');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        for (var j = 0; j < $scope.shipment.shipmentServiceList[i].documentList.length; j++) {

                            if ($scope.validateServiceDocument(0, $scope.shipment.shipmentServiceList[i].documentList[j], i, j) == false) {
                                console.log("Document Validation failed :" + i + ":" + j);
                                $scope.activeViewTabAndService(i, 'documents');
                                if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                    Notification.error($rootScope.nls["ERR90574"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                                } else {
                                    Notification.error($rootScope.nls["ERR90574"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                }
                                $scope.enableSubmitBtnFn();
                                return false;
                            } else {
                                if ($scope.shipment.shipmentServiceList[i].documentList[j].isDimensionStatus() == false) {
                                    console.log("isDimensionStatus failed :" + i + ":" + j);
                                    $scope.activeViewTabAndService(i, 'documents');
                                    if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                        Notification.error($rootScope.nls["ERR8340"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                                    } else {
                                        Notification.error($rootScope.nls["ERR8340"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                                    }
                                    $scope.enableSubmitBtnFn();
                                    return false;
                                }
                            }

                        }


                        if ($scope.shipment.shipmentServiceList[i].isConnectionCheck() == false) {
                            console.log("isConnectionCheck failed :" + i);
                            if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                Notification.error($rootScope.nls["ERR90560"] + "  " + $rootScope.nls["ERR90572"] + "-" + id)
                            } else {
                                Notification.error($rootScope.nls["ERR90560"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1))
                            }
                            $scope.activeViewTabAndService(i, 'connections');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }




                        var connectionRequired = $rootScope.appMasterData['connection.required'];
                        $scope.connectionRequiredFlag = false;
                        if (connectionRequired.toLowerCase() == 'true' || connectionRequired == true) {


                            if ($scope.shipment.shipmentServiceList[i].origin != null && $scope.shipment.shipmentServiceList[i].origin.id != null) {
                                if ($scope.shipment.shipmentServiceList[i].pol != null && $scope.shipment.shipmentServiceList[i].pol.id != null) {
                                    if ($scope.shipment.shipmentServiceList[i].origin.id != $scope.shipment.shipmentServiceList[i].pol.id) {
                                        $scope.connectionRequiredFlag = true;
                                    }
                                }
                            }

                            if ($scope.shipment.shipmentServiceList[i].destination != null && $scope.shipment.shipmentServiceList[i].destination.id != null) {
                                if ($scope.shipment.shipmentServiceList[i].pod != null && $scope.shipment.shipmentServiceList[i].pod.id != null) {
                                    if ($scope.shipment.shipmentServiceList[i].destination.id != $scope.shipment.shipmentServiceList[i].pod.id) {
                                        $scope.connectionRequiredFlag = true;
                                    }
                                }
                            }


                            if ($scope.connectionRequiredFlag) {
                                if ($scope.shipment.shipmentServiceList[i].connectionList == undefined || $scope.shipment.shipmentServiceList[i].connectionList == null || $scope.shipment.shipmentServiceList[i].connectionList.length == 0) {
                                    Notification.error($rootScope.nls["ERR90364"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                                    $scope.shipment.shipmentServiceList[i].connectionList = [{}];
                                    $scope.activeViewTabAndService(i, 'connections');
                                    $scope.enableSubmitBtnFn();
                                    return false;
                                }

                            }

                        }




                        if ($scope.shipment.shipmentServiceList[i].isRatesCheck() == false) {

                            if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                Notification.error($rootScope.nls["ERR90561"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                            } else {
                                Notification.error($rootScope.nls["ERR90561"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            }
                            $scope.activeViewTabAndService(i, 'rates');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isStatusCheck() == false) {
                            if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                Notification.error($rootScope.nls["ERR90562"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                            } else {
                                Notification.error($rootScope.nls["ERR90562"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            }
                            $scope.activeViewTabAndService(i, 'statusUpdate');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isEventCheck() == false) {
                            if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {

                                Notification.error($rootScope.nls["ERR90563"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                            } else {
                                Notification.error($rootScope.nls["ERR90563"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            }
                            $scope.activeViewTabAndService(i, 'events');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].isReferenceCheck() == false) {

                            if ($scope.shipment.shipmentServiceList[i].id != undefined && $scope.shipment.shipmentServiceList[i].id != null) {
                                Notification.error($rootScope.nls["ERR90573"] + "  " + $rootScope.nls["ERR90572"] + "-" + id);
                            } else {
                                Notification.error($rootScope.nls["ERR90573"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            }
                            console.log("isReferenceCheck failed :" + i);
                            $scope.activeViewTabAndService(i, 'references');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }


                        if ($scope.shipment.shipmentServiceList[i].isAttachmentCheck() == false) {
                            Notification.error($rootScope.nls["ERR250"] + "  " + $rootScope.nls["ERR90572"] + "-" + (i + 1));
                            console.log("Attachment is invalid");
                            $scope.activeViewTabAndService(i, 'attachMent');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                        if ($scope.shipment.shipmentServiceList[i].authenticatedDocList != undefined && $scope.shipment.shipmentServiceList[i].authenticatedDocList != null &&
                            $scope.shipment.shipmentServiceList[i].authenticatedDocList.length > 0) {

                            for (var j = 0; j < $scope.shipment.shipmentServiceList[i].authenticatedDocList.length; j++) {

                                if (isEmptyRow($scope.shipment.shipmentServiceList[i].authenticatedDocList[j])) {

                                    $scope.shipment.shipmentServiceList[i].authenticatedDocList.splice(j, 1);
                                    delete $scope.shipment.shipmentServiceList[i].authenticatedDocList[j];

                                }

                            }

                        }

                        if ($scope.callValidateAuthenticatedDocs($scope.shipment.shipmentServiceList[i]) == false) {
                            Notification.error($rootScope.nls["ERR05571"]);
                            $scope.activeViewTabAndService(i, 'AuthenticatedDocs');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        //pickup delivery validation 
                        if ($scope.validatePickUpDelivery(0, $scope.shipment.shipmentServiceList[i], i) == false) {
                            Notification.error();
                            $scope.activeViewTabAndService(i, 'pickup');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }

                    } else {
                        $scope.clickTabIndex(i);
                        $scope.enableSubmitBtnFn();
                        return false;
                    }

                    if ($scope.shipment.shipmentServiceList[i].serviceMaster.importExport === 'Export') {
                        var volweight = 0;
                        var grossWeight = 0;
                        var volpound = 0;
                        var grosspound = 0;
                        var chargebleweight = 0;
                        var volumeInCbm = 0;
                        var noofpieces = 0;

                        for (var j = 0; j < $scope.shipment.shipmentServiceList[i].documentList.length; j++) {

                            volweight += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].volumeWeight);

                            grossWeight += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].grossWeight);

                            volpound += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].volumeWeightInPound);

                            grosspound += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].grossWeightInPound);

                            chargebleweight += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].chargebleWeight);

                            noofpieces += parseInt($scope.shipment.shipmentServiceList[i].documentList[j].noOfPieces);

                            if ($scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm != null) {
                                volumeInCbm += parseFloat($scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm);
                            }
                        }

                        if ($scope.shipment.shipmentServiceList[i].bookedVolumeWeightUnitKg != volweight) {
                            Notification.error($rootScope.nls["ERR90578"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.activeViewTabAndService(i, 'general');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedGrossWeightUnitKg != grossWeight) {
                            Notification.error($rootScope.nls["ERR90577"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.activeViewTabAndService(i, 'general');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedVolumeWeightUnitPound != volpound) {
                            Notification.error($rootScope.nls["ERR90581"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.activeViewTabAndService(i, 'general');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedGrossWeightUnitPound != grosspound) {
                            Notification.error($rootScope.nls["ERR90580"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.activeViewTabAndService(i, 'general');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedChargeableUnit != chargebleweight) {
                            Notification.error($rootScope.nls["ERR90579"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.activeViewTabAndService(i, 'general');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedPieces != noofpieces) {
                            Notification.error($rootScope.nls["ERR90583"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                            $scope.activeViewTabAndService(i, 'general');
                            $scope.enableSubmitBtnFn();
                            return false;
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != undefined && volumeInCbm != undefined &&
                            $scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != null && volumeInCbm != null &&
                            $scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != '' && volumeInCbm != '') {

                            if ($scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm != volumeInCbm) {
                                $scope.activeViewTabAndService(i, 'general');
                                Notification.error($rootScope.nls["ERR90582"] + "-" + $scope.shipment.shipmentServiceList[i].serviceMaster.serviceName);
                                $scope.enableSubmitBtnFn();
                                return false;
                            }
                        }
                        if ($scope.shipment.shipmentServiceList[i].bookedChargeableUnit < $scope.appMasterData['shipment.minimum.chargeable.weight']) {
                            minimumSipment = true;
                        }
                    }
                }

                if (forWhichPurpose != undefined && forWhichPurpose == 'HAWB') {
                    $scope.updateToDatabase(forWhichPurpose);
                    return;
                }
                if (minimumSipment) {
                    $scope.minimumShipmentAlert();
                } else {
                    $scope.updateToDatabase();
                }

            } else {
                console.log("Invalid Form Submission.");
                $scope.enableSubmitBtnFn();
            }
        }
    }


    $scope.updateToDatabase = function(forWhichPurpose) {
        $rootScope.mainpreloder = true;
        $scope.tmpObj = cloneService.clone($scope.shipment);

        $scope.tmpObj.whoRouted = $scope.tmpObj.whoRouted == false ? 'Self' : 'Agent';
        $scope.tmpObj.directShipment = $scope.tmpObj.directShipment == false ? 'No' : 'Yes';

        for (var i = 0; i < $scope.tmpObj.shipmentServiceList.length; i++) {
            $scope.tmpObj.shipmentServiceList[i].whoRouted = $scope.tmpObj.shipmentServiceList[i].whoRouted == false ? 'Self' : 'Agent';
            $scope.tmpObj.shipmentServiceList[i].serviceReqDate = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].serviceReqDate);
            $scope.tmpObj.shipmentServiceList[i].eta = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].eta);
            $scope.tmpObj.shipmentServiceList[i].etd = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].etd);
            $scope.tmpObj.shipmentServiceList[i].ppcc = $scope.tmpObj.shipmentServiceList[i].ppcc == false ? 'Prepaid' : 'Collect';

            $scope.tmpObj.shipmentServiceList[i].isAgreed = $scope.tmpObj.shipmentServiceList[i].isAgreed ? 'Yes' : 'No'; //dynamic report preview
            $scope.tmpObj.shipmentServiceList[i].isMarksNo = $scope.tmpObj.shipmentServiceList[i].isMarksNo ? 'Yes' : 'No'; //dynamic report preview
            if ($scope.tmpObj.shipmentServiceList[i].connectionList != undefined && $scope.tmpObj.shipmentServiceList[i].connectionList != null && $scope.tmpObj.shipmentServiceList[i].connectionList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].connectionList.length; j++) {
                    $scope.tmpObj.shipmentServiceList[i].connectionList[j].eta = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].connectionList[j].eta);
                    $scope.tmpObj.shipmentServiceList[i].connectionList[j].etd = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].connectionList[j].etd);
                }
            }

            if ($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList != undefined && $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList != null && $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList.length; j++) {
                    $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].date);


                    if ($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired == 'Yes' || $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired == true) {
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired = 'Yes';
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate);
                    } else {
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpRequired = 'No';
                        $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].followUpDate = null;
                    }

                    $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].protect = $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].protect ? 'Yes' : 'No';
                    $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].completed = $scope.tmpObj.shipmentServiceList[i].shipmentServiceTriggerList[j].completed ? 'Yes' : 'No';
                }
            }

            if ($scope.tmpObj.shipmentServiceList[i].eventList != undefined && $scope.tmpObj.shipmentServiceList[i].eventList != null && $scope.tmpObj.shipmentServiceList[i].eventList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].eventList.length; j++) {

                    $scope.tmpObj.shipmentServiceList[i].eventList[j].eventDate = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].eventList[j].eventDate);


                    if ($scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired == 'Yes' || $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired == true) {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired = 'Yes';

                    } else {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpRequired = 'No';
                    }

                    if ($scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate != undefined &&
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate != null) {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate);
                    } else {
                        $scope.tmpObj.shipmentServiceList[i].eventList[j].followUpDate = null;
                    }

                    $scope.tmpObj.shipmentServiceList[i].eventList[j].isCompleted = $scope.tmpObj.shipmentServiceList[i].eventList[j].isCompleted ? 'Yes' : 'No';

                }
            }
            if ($scope.tmpObj.shipmentServiceList[i].documentList != undefined && $scope.tmpObj.shipmentServiceList[i].documentList != null && $scope.tmpObj.shipmentServiceList[i].documentList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].documentList.length; j++) {

                    $scope.tmpObj.shipmentServiceList[i].documentList[j].documentReqDate = $rootScope.sendApiDateAndTime(new Date());
                    $scope.tmpObj.shipmentServiceList[i].documentList[j].eta = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].documentList[j].eta);
                    $scope.tmpObj.shipmentServiceList[i].documentList[j].etd = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].documentList[j].etd);
                    if ($scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn != undefined && $scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn != null)
                        $scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].documentList[j].hawbReceivedOn);
                }
            }

            if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint == undefined) {
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint = {};
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = 'No';
            } else {
                $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp = $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.isOurPickUp ? 'Yes' : 'No';
            }


            if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint != undefined && $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint != null && $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint != "") {
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpFollowUp);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpPlanned);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.pickUpActual);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryExpected);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.deliveryActual);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryExpected);
                if ($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual != null)
                    $scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual = $rootScope.sendApiDateAndTime($scope.tmpObj.shipmentServiceList[i].pickUpDeliveryPoint.doorDeliveryActual);




            }
            if ($scope.tmpObj.shipmentServiceList[i].authenticatedDocList != undefined && $scope.tmpObj.shipmentServiceList[i].authenticatedDocList != null && $scope.tmpObj.shipmentServiceList[i].authenticatedDocList.length != 0) {
                for (var j = 0; j < $scope.tmpObj.shipmentServiceList[i].authenticatedDocList.length; j++) {
                    $scope.tmpObj.shipmentServiceList[i].authenticatedDocList[j].date = $rootScope.sendApiStartDateTime($scope.tmpObj.shipmentServiceList[i].authenticatedDocList[j].date);

                }
            }
        }
        ShipmentUpdate.save($scope.tmpObj).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                $rootScope.mainpreloder = false;
                if ($scope.isCreateConsol != null && $scope.isCreateConsol === 'CONSOL') {
                    for (var i = 0; i < data.responseObject.shipmentServiceList.length; i++) {
                        if ($scope.createConsolServiceId === data.responseObject.shipmentServiceList[i].id) {
                            attachToMasterService.goToConsol(data.responseObject, data.responseObject.shipmentServiceList[i]);
                            break;
                        }
                    }
                    $scope.enableSubmitBtnFn();

                } else if ($scope.isCreateConsol != null && $scope.isCreateConsol === 'CFS') {
                    var param = {
                        shipmentUid: $scope.shipment.shipmentUid,
                        serviceUid: $scope.createConsolServiceId,
                        fromState: $state.current.name,
                        fromStateParams: $scope.shipment.id
                    };
                    $state.go("layout.createAirCFS", param);

                    $scope.enableSubmitBtnFn();
                } else if (forWhichPurpose != undefined && forWhichPurpose == 'HAWB') {
                    $scope.enableSubmitBtnFn();
                    $rootScope.$broadcast('shipment-update-for-hawb');
                    return;
                } else {
                    $scope.showResponse(data.responseObject, false);
                }
            } else {
                $scope.enableSubmitBtnFn();
                console.log("Shipment updated Failed " + data.responseDescription)
            }
            $scope.enableSubmitBtnFn();
        }, function(error) {
            console.log("Shipment updated Failed : " + error)
            $scope.enableSubmitBtnFn();
        });
    }

    $scope.validateShipment = function(validateCode) {

        $scope.firstFocus = false;
        $scope.errorMap = new Map();


        if (validateCode == 0 || validateCode == 1) {

            if ($scope.shipment.party == undefined ||
                $scope.shipment.party == null ||
                $scope.shipment.party == "" ||
                $scope.shipment.party.id == null) {
                console.log($rootScope.nls["ERR90002"]);
                $scope.errorMap.put("shipmentParty", $rootScope.nls["ERR90002"]);
                return false;

            } else {
                if ($scope.shipment.party.isDefaulter) {
                    $scope.errorMap.put("shipmentParty", $rootScope.nls["ERR90584"]);
                    $scope.shipment.party = null;
                    $scope.shipment.partyAddress = {};
                    return false

                }

                if (ValidateUtil.isStatusBlocked($scope.shipment.party.status)) {
                    console.log($rootScope.nls["ERR90003"]);
                    $scope.errorMap.put("shipmentParty", $rootScope.nls["ERR90003"]);
                    $scope.shipment.party = null;
                    $scope.shipment.partyAddress = {};
                    return false

                }

                if (ValidateUtil.isStatusHidden($scope.shipment.party.status)) {
                    console.log($rootScope.nls["ERR90004"]);
                    $scope.errorMap.put("shipmentParty", $rootScope.nls["ERR90004"]);
                    $scope.shipment.party = null;
                    $scope.shipment.partyAddress = {};
                    return false

                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.shipment.partyAddress.addressLine1 == null ||
                $scope.shipment.partyAddress.addressLine1 == "" ||
                $scope.shipment.partyAddress.addressLine1 == undefined) {
                console.log($rootScope.nls["ERR90005"]);
                $scope.errorMap.put("shipment.partyAddress.addressLine1", $rootScope.nls["ERR90005"]);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 3) {
            /*if ($scope.shipment.partyAddress.addressLine2 == null ||
                $scope.shipment.partyAddress.addressLine2 == "" ||
                $scope.shipment.partyAddress.addressLine2 == undefined) {
                console.log($rootScope.nls["ERR90007"]);
                $scope.errorMap.put("shipment.partyAddress.addressLine2", $rootScope.nls["ERR90007"]);
                return false;
            } */

        }

        if (validateCode == 0 || validateCode == 4) {
            if ($scope.shipment.partyAddress.addressLine3 != null) {}
        }

        if (validateCode == 0 || validateCode == 5) {
            /*if ($scope.shipment.partyAddress.addressLine4 == null ||
                $scope.shipment.partyAddress.addressLine4 == "" ||
                $scope.shipment.partyAddress.addressLine4 == undefined) {
                console.log($rootScope.nls["ERR90010"]);
                $scope.errorMap.put("shipment.partyAddress.addressLine4", $rootScope.nls["ERR90010"]);
            }*/
        }


        if (validateCode == 0 || validateCode == 6) {
            /*
                        if ($scope.shipment.origin == null ||
                            $scope.shipment.origin.id == null ||
                            $scope.shipment.origin.id.trim == "") {
                            console.log($rootScope.nls["ERR90013"]);
                            $scope.errorMap.put("origin", $rootScope.nls["ERR90013"]);
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.shipment.origin.status)) {
                            $scope.shipment.origin = null;
                            console.log($rootScope.nls["ERR90037"]);
                            $scope.errorMap.put("origin", $rootScope.nls["ERR90037"]);
                            return false;
                        }

                        if (ValidateUtil.isStatusHidden($scope.shipment.origin.status)) {
                            $scope.shipment.origin = null;
                            console.log($rootScope.nls["ERR90038"]);
                            $scope.errorMap.put("origin", $rootScope.nls["ERR90038"]);
                            return false;
                        }
                    */
        }

        if (validateCode == 0 || validateCode == 7) {
            /*
                        if ($scope.shipment.destination == null ||
                            $scope.shipment.destination.id == null ||
                            $scope.shipment.destination.id.trim == "") {
                            console.log($rootScope.nls["ERR90014"]);
                            $scope.errorMap.put("destination", $rootScope.nls["ERR90014"]);
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.shipment.destination.status)) {
                            $scope.shipment.destination = null;
                            console.log($rootScope.nls["ERR90039"]);
                            $scope.errorMap.put("destination", $rootScope.nls["ERR90039"]);
                            return false;
                        }

                        if (ValidateUtil.isStatusHidden($scope.shipment.destination.status)) {
                            $scope.shipment.destination = null;
                            console.log($rootScope.nls["ERR90040"]);
                            $scope.errorMap.put("destination", $rootScope.nls["ERR90040"]);
                            return false;
                        }
                    */
        }

        if (validateCode == 0 || validateCode == 8) {
            if ($scope.shipment.project != null && $scope.shipment.project != undefined) {
                if ($scope.shipment.project.id != null || $scope.shipment.project.id != undefined ||
                    $scope.shipment.project.id.trim != "") {
                    if (ValidateUtil.isStatusBlocked($scope.shipment.project.status)) {
                        $scope.shipment.project = null;
                        console.log($rootScope.nls["ERR90018"]);
                        $scope.errorMap.put("project", $rootScope.nls["ERR90018"]);
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden($scope.shipment.project.status)) {
                        $scope.shipment.project = null;
                        console.log($rootScope.nls["ERR90019"]);
                        $scope.errorMap.put("project", $rootScope.nls["ERR90019"]);
                        return false;
                    }
                }
            }
        }
        if (validateCode == 0 || validateCode == 9) {
            if ($scope.shipment.tosMaster == undefined || $scope.shipment.tosMaster == null ||
                $scope.shipment.tosMaster.id == null ||
                $scope.shipment.tosMaster.id.trim == "") {
                console.log($rootScope.nls["ERR90015"]);
                $scope.errorMap.put("tosMaster1", $rootScope.nls["ERR90015"]);
                return false;
            }

            if (ValidateUtil.isStatusBlocked($scope.shipment.tosMaster.status)) {
                $scope.shipment.tosMaster = null;
                console.log($rootScope.nls["ERR90016"]);
                $scope.errorMap.put("tosMaster1", $rootScope.nls["ERR90016"]);
                console.log("Message : " + $scope.errorMap.get("tosMaster1"));
                return false;
            }
            if (ValidateUtil.isStatusHidden($scope.shipment.tosMaster.status)) {
                $scope.shipment.tosMaster = null;
                console.log($rootScope.nls["ERR90017"]);
                $scope.errorMap.put("tosMaster1", $rootScope.nls["ERR90017"]);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 10) {
            if ($scope.shipment.commodity == null ||
                $scope.shipment.commodity.id == null ||
                $scope.shipment.commodity.id.trim == "") {
                // commented for fixing a bug-NES-1030
                //console.log($rootScope.nls["ERR90021"]);
                //$scope.errorMap.put("commodity",$rootScope.nls["ERR90021"]);
                //return false;
            } else {

                if (ValidateUtil.isStatusBlocked($scope.shipment.commodity.status)) {
                    $scope.shipment.commodity = null;
                    console.log($rootScope.nls["ERR90022"]);
                    $scope.errorMap.put("commodity", $rootScope.nls["ERR90022"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.shipment.commodity.status)) {
                    $scope.shipment.commodity = null;
                    console.log($rootScope.nls["ERR90023"]);
                    $scope.errorMap.put("commodity", $rootScope.nls["ERR90023"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 11) {

            if ($scope.shipment.whoRouted == true && $scope.shipment.directShipment == false) {
                if ($scope.shipment.agent == null ||
                    $scope.shipment.agent.id == null ||
                    $scope.shipment.agent.id.trim == "") {
                    console.log($rootScope.nls["ERR90028"]);
                    $scope.errorMap.put("routedBy", $rootScope.nls["ERR90028"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.shipment.agent.status)) {
                    $scope.shipment.agent = null;
                    console.log($rootScope.nls["ERR90029"]);
                    $scope.errorMap.put("routedBy", $rootScope.nls["ERR90029"]);
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.shipment.agent.status)) {
                    $scope.shipment.agent = null;
                    console.log($rootScope.nls["ERR90030"]);
                    $scope.errorMap.put("routedBy", $rootScope.nls["ERR90030"]);
                    return false;
                }
            }

            if ($scope.shipment.whoRouted == true && $scope.shipment.directShipment == true) {
                if ($scope.shipment.agent != null &&
                    $scope.shipment.agent.id != null &&
                    $scope.shipment.agent.id.trim != "") {
                    if (ValidateUtil.isStatusBlocked($scope.shipment.agent.status)) {
                        $scope.shipment.agent = null;
                        console.log($rootScope.nls["ERR90029"]);
                        $scope.errorMap.put("routedBy", $rootScope.nls["ERR90029"]);
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.shipment.agent.status)) {
                        $scope.shipment.agent = null;
                        console.log($rootScope.nls["ERR90030"]);
                        $scope.errorMap.put("routedBy", $rootScope.nls["ERR90030"]);
                        return false;
                    }
                }
            }

            if ($scope.shipment.whoRouted == false) {

                if ($scope.shipment.salesman == undefined || $scope.shipment.salesman == null ||
                    $scope.shipment.salesman.id == null ||
                    $scope.shipment.salesman.id.trim == "") {
                    console.log($rootScope.nls["ERR90024"]);
                    $scope.errorMap.put("routedBy", $rootScope.nls["ERR90024"]);
                    return false;
                }

                if (ValidateUtil.isEmployeeResigned($scope.shipment.salesman.employementStatus)) {
                    $scope.shipment.salesman = null;
                    console.log($rootScope.nls["ERR90025"]);
                    $scope.errorMap.put("routedBy", $rootScope.nls["ERR90025"]);
                    return false;
                }
                if (ValidateUtil.isEmployeeTerminated($scope.shipment.salesman.employementStatus)) {
                    $scope.shipment.salesman = null;
                    console.log($rootScope.nls["ERR90026"]);
                    $scope.errorMap.put("routedBy", $rootScope.nls["ERR90026"]);
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 12) {
            if ($scope.shipment.aesNo != undefined && $scope.shipment.aesNo != null && $scope.shipment.aesNo != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NO, $scope.shipment.aesNo)) {
                    $scope.errorMap.put("shipmentaes", $rootScope.nls["ERR90098"]);
                    return false;
                }
            } else {
                $scope.shipment.aesNo = null;
            }
        }

        return true;

    }

    $scope.validateShipmentServiceDetail = function(validateCode, serviceDetail, index) {

        $scope.errorMap = new Map();
        $rootScope.clientMessage = null;


        if (validateCode == 0 || validateCode == 1) {

            if (serviceDetail.serviceMaster == null || serviceDetail.serviceMaster.id == undefined ||
                serviceDetail.serviceMaster.id == null ||
                serviceDetail.serviceMaster.id == "") {

                console.log("serviceMaster" + index + " " + $rootScope.nls["ERR90031"]);
                $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90031"]);
                $scope.shipmentTab = 'general';
                return false;
            } else {

                if (ValidateUtil.isStatusBlocked(serviceDetail.serviceMaster.status)) {
                    serviceDetail.serviceMaster = null;

                    $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90032"]);
                    $scope.shipmentTab = "general";
                    return false

                }
                if (ValidateUtil.isStatusHidden(serviceDetail.serviceMaster.status)) {
                    serviceDetail.serviceMaster = null;
                    console.log($rootScope.nls["ERR90033"]);
                    $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90033"]);
                    $scope.shipmentTab = "general";
                    return false

                }

                for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {

                    if ($scope.shipment.shipmentServiceList[i].lastUpdatedStatus != undefined &&
                        ($scope.shipment.shipmentServiceList[i].lastUpdatedStatus.serviceStatus == 'Cancelled' ||
                            $scope.shipment.shipmentServiceList[i].lastUpdatedStatus.serviceStatus == 'Closed')) {
                        continue;

                    }

                    if (index == i) {
                        continue;
                    } else if ($scope.shipment.shipmentServiceList[i].serviceMaster != undefined && $scope.shipment.shipmentServiceList[i].serviceMaster.id == serviceDetail.serviceMaster.id) {
                        console.log($rootScope.nls["ERR90570"]);
                        $scope.errorMap.put("serviceMaster" + index, $rootScope.nls["ERR90570"]);
                        $scope.shipmentTab = "general";
                        return false;
                    }


                }

            }


            console.log("Service Master validation done...");
        }

        if (validateCode == 0 || validateCode == 2) {
            if (serviceDetail.origin == null ||
                serviceDetail.origin.id == null ||
                serviceDetail.origin.id.trim == "") {
                //port group is mandatory because port list is coming based on group
                /*  if ($scope.shipment.origin == undefined || $scope.shipment.origin == null || $scope.shipment.origin == '') {
                      console.log($rootScope.nls["ERR90013"]);
                      $scope.errorMap.put("origin" + index, $rootScope.nls["ERR90013"]);
                      return false;
                  }*/
                console.log($rootScope.nls["ERR90013"]);
                $scope.errorMap.put("serviceOrigin" + index, $rootScope.nls["ERR90013"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusBlocked(serviceDetail.origin.status)) {
                serviceDetail.origin = null;
                console.log($rootScope.nls["ERR90037"]);
                $scope.errorMap.put("serviceOrigin" + index, $rootScope.nls["ERR90037"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusHidden(serviceDetail.origin.status)) {
                serviceDetail.origin = null;
                console.log($rootScope.nls["ERR90038"]);
                $scope.errorMap.put("serviceOrigin" + index, $rootScope.nls["ERR90038"]);
                $scope.shipmentTab = "general";
                return false;
            }
        }




        if (validateCode == 0 || validateCode == 3) {
            if (serviceDetail.pol == null ||
                serviceDetail.pol.id == null ||
                serviceDetail.pol.id.trim == "") {
                //port group is mandatory because port list is coming based on group
                /* if ($scope.shipment.origin == undefined || $scope.shipment.origin == null || $scope.shipment.origin == '') {
                     console.log($rootScope.nls["ERR90013"]);
                     $scope.errorMap.put("origin" + index, $rootScope.nls["ERR90013"]);
                     return false;
                 }*/
                console.log($rootScope.nls["ERR90041"]);
                $scope.errorMap.put("pol" + index, $rootScope.nls["ERR90041"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusBlocked(serviceDetail.pol.status)) {
                serviceDetail.pol = null;
                console.log($rootScope.nls["ERR90042"]);
                $scope.errorMap.put("pol" + index, $rootScope.nls["ERR90042"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusHidden(serviceDetail.pol.status)) {
                serviceDetail.pol = null;
                console.log($rootScope.nls["ERR90043"]);
                $scope.errorMap.put("pol" + index, $rootScope.nls["ERR90043"]);
                $scope.shipmentTab = "general";
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            if (serviceDetail.pod == null ||
                serviceDetail.pod.id == null ||
                serviceDetail.pod.id.trim == "") {

                //port group is mandatory because port list is coming based on group
                /* if ($scope.shipment.destination == undefined || $scope.shipment.destination == null || $scope.shipment.destination == '') {
                     console.log($rootScope.nls["ERR90014"]);
                     $scope.errorMap.put("destination" + index, $rootScope.nls["ERR90014"]);
                     return false;
                 }*/
                console.log($rootScope.nls["ERR90044"]);
                $scope.errorMap.put("pod" + index, $rootScope.nls["ERR90044"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusBlocked(serviceDetail.pod.status)) {
                serviceDetail.pod = null;
                console.log($rootScope.nls["ERR90045"]);
                $scope.errorMap.put("pod" + index, $rootScope.nls["ERR90045"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusHidden(serviceDetail.pod.status)) {
                serviceDetail.pod = null;
                console.log($rootScope.nls["ERR90046"]);
                $scope.errorMap.put("pod" + index, $rootScope.nls["ERR90046"]);
                $scope.shipmentTab = "general";
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 5) {
            if (serviceDetail.destination == null ||
                serviceDetail.destination.id == null ||
                serviceDetail.destination.id.trim == "") {

                //port group is mandatory because port list is coming based on group
                /*if ($scope.shipment.destination == undefined || $scope.shipment.destination == null || $scope.shipment.destination == '') {
                    console.log($rootScope.nls["ERR90014"]);
                    $scope.errorMap.put("destination" + index, $rootScope.nls["ERR90014"]);
                    return false;
                }*/
                console.log($rootScope.nls["ERR90014"]);
                $scope.errorMap.put("serviceDestination" + index, $rootScope.nls["ERR90014"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusBlocked(serviceDetail.destination.status)) {
                serviceDetail.destination = null;
                console.log($rootScope.nls["ERR90039"]);
                $scope.errorMap.put("serviceDestination" + index, $rootScope.nls["ERR90039"]);
                $scope.shipmentTab = "general";
                return false;
            }

            if (ValidateUtil.isStatusHidden(serviceDetail.destination.status)) {
                serviceDetail.destination = null;
                console.log($rootScope.nls["ERR90040"]);
                $scope.errorMap.put("serviceDestination" + index, $rootScope.nls["ERR90040"]);
                $scope.shipmentTab = "general";
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 6) {

            if (serviceDetail.whoRouted == true && $scope.shipment.directShipment == false) {
                if (serviceDetail.agent == null ||
                    serviceDetail.agent.id == null ||
                    serviceDetail.agent.id.trim == "") {
                    console.log($rootScope.nls["ERR90028"]);
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90028"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

                if (serviceDetail.agent.isDefaulter) {
                    serviceDetail.agent = null;
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90610"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

                if (ValidateUtil.isStatusBlocked(serviceDetail.agent.status)) {
                    serviceDetail.agent = null;
                    console.log($rootScope.nls["ERR90029"]);
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90029"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                if (ValidateUtil.isStatusHidden(serviceDetail.agent.status)) {
                    serviceDetail.agent = null;
                    console.log($rootScope.nls["ERR90030"]);
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90030"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
            }

            if (serviceDetail.whoRouted == true && $scope.shipment.directShipment == true) {
                if (serviceDetail.agent != null &&
                    serviceDetail.agent.id != null &&
                    serviceDetail.agent.id.trim != "") {
                    if (ValidateUtil.isStatusBlocked(serviceDetail.agent.status)) {
                        serviceDetail.agent = null;
                        console.log($rootScope.nls["ERR90029"]);
                        $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90029"]);
                        $scope.shipmentTab = "general";
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden(serviceDetail.agent.status)) {
                        serviceDetail.agent = null;
                        console.log($rootScope.nls["ERR90030"]);
                        $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90030"]);
                        $scope.shipmentTab = "general";
                        return false;
                    }
                }
            }

            if (serviceDetail.whoRouted == false) {

                if (serviceDetail.salesman == null ||
                    serviceDetail.salesman.id == null ||
                    serviceDetail.salesman.id.trim == "") {
                    console.log($rootScope.nls["ERR90024"]);
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90024"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

                if (ValidateUtil.isEmployeeResigned(serviceDetail.salesman.employementStatus)) {
                    serviceDetail.salesman = null;
                    console.log($rootScope.nls["ERR90025"]);
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90025"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                if (ValidateUtil.isEmployeeTerminated(serviceDetail.salesman.employementStatus)) {
                    serviceDetail.salesman = null;
                    console.log($rootScope.nls["ERR90026"]);
                    $scope.errorMap.put("serviceRoutedBy" + index, $rootScope.nls["ERR90026"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

            }




        }

        if (validateCode == 0 || validateCode == 7) {

            if (serviceDetail.party == undefined ||
                serviceDetail.party == null ||
                serviceDetail.party == "" ||
                serviceDetail.party.id == null) {
                console.log($rootScope.nls["ERR90002"]);
                $scope.errorMap.put("shipmentServiceParty" + index, $rootScope.nls["ERR90002"]);
                $scope.shipmentTab = "general";
                return false;

            } else {

                if (serviceDetail.party.isDefaulter) {
                    $scope.errorMap.put("shipmentServiceParty" + index, $rootScope.nls["ERR90584"]);
                    serviceDetail.party = null;
                    $scope.shipmentTab = "general";
                    return false

                }

                if (ValidateUtil.isStatusBlocked(serviceDetail.party.status)) {
                    console.log($rootScope.nls["ERR90003"]);
                    $scope.errorMap.put("shipmentServiceParty" + index, $rootScope.nls["ERR90003"]);
                    serviceDetail.party = null;
                    $scope.shipmentTab = "general";
                    return false

                }

                if (ValidateUtil.isStatusHidden(serviceDetail.party.status)) {
                    console.log($rootScope.nls["ERR90004"]);
                    $scope.errorMap.put("shipmentServiceParty" + index, $rootScope.nls["ERR90004"]);
                    serviceDetail.party = null;
                    $scope.shipmentTab = "general";
                    return false

                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {

            if (serviceDetail.customerService == undefined ||
                serviceDetail.customerService == null ||
                serviceDetail.customerService == "" ||
                serviceDetail.customerService.id == null) {
                console.log($rootScope.nls["ERR90034"]);
                $scope.errorMap.put("shipmentCustomerService" + index, $rootScope.nls["ERR90034"]);
                $scope.shipmentTab = "general";
                return false;

            } else if (serviceDetail.customerService.employementStatus != null) {
                if (ValidateUtil.isEmployeeResigned(serviceDetail.customerService.employementStatus)) {
                    serviceDetail.customerService = null;
                    console.log($rootScope.nls["ERR90035"]);
                    $scope.errorMap.put("shipmentCustomerService" + index, $rootScope.nls["ERR90035"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                if (ValidateUtil.isEmployeeTerminated(serviceDetail.customerService.employementStatus)) {
                    serviceDetail.customerService = null;
                    console.log($rootScope.nls["ERR90036"]);
                    $scope.errorMap.put("shipmentCustomerService" + index, $rootScope.nls["ERR90036"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {

            if (serviceDetail.coLoader != undefined &&
                serviceDetail.coLoader != null &&
                serviceDetail.coLoader != "" &&
                serviceDetail.coLoader.id != null) {


                if (serviceDetail.coLoader.status != null) {

                    if (serviceDetail.coLoader.isDefaulter) {
                        $scope.errorMap.put("shipmentServiceColoader" + index, $rootScope.nls["ERR90611"]);
                        serviceDetail.coLoader = null;
                        $scope.shipmentTab = "general";
                        return false

                    }


                    if (ValidateUtil.isStatusBlocked(serviceDetail.coLoader.status)) {
                        console.log($rootScope.nls["ERR90003"]);
                        $scope.errorMap.put("shipmentServiceColoader" + index, $rootScope.nls["ERR90048"]);
                        serviceDetail.coLoader = null;
                        $scope.shipmentTab = "general";
                        return false

                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.coLoader.status)) {
                        console.log($rootScope.nls["ERR90004"]);
                        $scope.errorMap.put("shipmentServiceColoader" + index, $rootScope.nls["ERR90049"]);
                        serviceDetail.coLoader = null;
                        $scope.shipmentTab = "general";
                        return false

                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 10) {

            if (serviceDetail.division != undefined &&
                serviceDetail.division != null &&
                serviceDetail.division != "" &&
                serviceDetail.division.id != null) {


                if (serviceDetail.division.status != null) {
                    if (ValidateUtil.isStatusBlocked(serviceDetail.division.status)) {
                        console.log($rootScope.nls["ERR90054"]);
                        $scope.errorMap.put("shipmentDivision" + index, $rootScope.nls["ERR90054"]);
                        serviceDetail.division = null;
                        $scope.shipmentTab = "general";
                        return false

                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.division.status)) {
                        console.log($rootScope.nls["ERR90055"]);
                        $scope.errorMap.put("shipmentDivision" + index, $rootScope.nls["ERR90055"]);
                        serviceDetail.division = null;
                        $scope.shipmentTab = "general";
                        return false

                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 11) {

            if (serviceDetail.packMaster != undefined &&
                serviceDetail.packMaster != null &&
                serviceDetail.packMaster != "" &&
                serviceDetail.packMaster.id != null) {


                if (serviceDetail.packMaster.status != null) {
                    if (ValidateUtil.isStatusBlocked(serviceDetail.packMaster.status)) {
                        console.log($rootScope.nls["ERR90057"]);
                        $scope.errorMap.put("shipmentPack" + index, $rootScope.nls["ERR90057"]);
                        serviceDetail.packMaster = null;
                        $scope.shipmentTab = "general";
                        return false

                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.packMaster.status)) {
                        console.log($rootScope.nls["ERR90058"]);
                        $scope.errorMap.put("shipmentPack" + index, $rootScope.nls["ERR90058"]);
                        serviceDetail.packMaster = null;
                        $scope.shipmentTab = "general";
                        return false

                    }
                }
            }
        }



        if (validateCode == 0 || validateCode == 17) {

            if (serviceDetail.carrier != undefined &&
                serviceDetail.carrier != null &&
                serviceDetail.carrier != "" &&
                serviceDetail.carrier.id != null) {


                if (serviceDetail.carrier.status != null) {
                    if (ValidateUtil.isStatusBlocked(serviceDetail.carrier.status)) {
                        console.log($rootScope.nls["ERR90064"]);
                        $scope.errorMap.put("shipmentCarrier" + index, $rootScope.nls["ERR90064"]);
                        serviceDetail.carrier = null;
                        $scope.shipmentTab = "general";
                        return false

                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.carrier.status)) {
                        console.log($rootScope.nls["ERR90065"]);
                        $scope.errorMap.put("shipmentCarrier" + index, $rootScope.nls["ERR90065"]);
                        serviceDetail.carrier = null;
                        $scope.shipmentTab = "general";
                        return false

                    }
                }

            }
        }

        if (validateCode == 0 || validateCode == 18) {
            if (serviceDetail.routeNo != undefined && serviceDetail.routeNo != "" && serviceDetail.routeNo != null) {

                if (serviceDetail.routeNo == 0) {
                    console.log($rootScope.nls["ERR90066"]);
                    $scope.errorMap.put("flight" + index, $rootScope.nls["ERR90066"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_FLIGHT_NUMBER, serviceDetail.routeNo)) {
                    console.log($rootScope.nls["ERR90066"]);
                    $scope.errorMap.put("flight" + index, $rootScope.nls["ERR90066"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

            } else {
                serviceDetail.routeNo = null;
            }

        }

        if (validateCode == 0 || validateCode == 19) {
            $rootScope.clientMessage = null;
            if (serviceDetail.mawbNo != undefined && serviceDetail.mawbNo != "" && serviceDetail.mawbNo != null) {
                if (serviceDetail.carrier == undefined ||
                    serviceDetail.carrier == null ||
                    serviceDetail.carrier == "" ||
                    serviceDetail.carrier.id == undefined) {
                    $scope.errorMap.put("shipmentCarrier" + index, $rootScope.nls["ERR90063"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                if (isNaN(serviceDetail.mawbNo) == true) {
                    console.log($rootScope.nls["ERR90068"]);
                    $scope.errorMap.put("mawbNo" + index, $rootScope.nls["ERR90068"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

                if (serviceDetail.mawbNo == 0) {
                    console.log($rootScope.nls["ERR90068"]);
                    $scope.errorMap.put("mawbNo" + index, $rootScope.nls["ERR90068"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                if (serviceDetail.mawbNo.length != 11) {
                    console.log($rootScope.nls["ERR96523"]);
                    $scope.errorMap.put("mawbNo" + index, $rootScope.nls["ERR96523"]);
                    $scope.shipmentTab = "general";
                    return false;
                }
                var check = $rootScope.validateMawb(serviceDetail.carrier, serviceDetail.mawbNo);
                if (check != "S") {
                    if (check != "F") {
                        $rootScope.clientMessage = $rootScope.nls["ERR90216"] + "(" + check + ")";
                    } else {
                        $rootScope.clientMessage = $rootScope.nls["ERR90068"];
                    }
                    return false;
                }
            }

        }

        if (validateCode == 0 || validateCode == 20) {

            if (serviceDetail.eta != null && serviceDetail.etd != null) {
                var etd = $rootScope.convertToDate(serviceDetail.etd);

                var eta = $rootScope.convertToDate(serviceDetail.eta);
                if (etd > eta) {
                    console.log($rootScope.nls["ERR90096"]);
                    $scope.errorMap.put("etd" + index, $rootScope.nls["ERR90096"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 21) {

            if (serviceDetail.etd != null && serviceDetail.eta != null) {
                var etd = $rootScope.convertToDate(serviceDetail.etd);

                var eta = $rootScope.convertToDate(serviceDetail.eta);

                if (etd > eta) {
                    console.log($rootScope.nls["ERR90096"]);
                    $scope.errorMap.put("eta" + index, $rootScope.nls["ERR90096"]);
                    $scope.shipmentTab = "general";
                    return false;
                }

            }
        }


        if ((validateCode == 0 || validateCode == 22) && serviceDetail.brokerageParty != undefined &&
            serviceDetail.brokerageParty != null && serviceDetail.brokerageParty != "" &&
            serviceDetail.brokerageParty.id != null && serviceDetail.brokerageParty.status != null) {

            if (serviceDetail.brokerageParty.isDefaulter) {
                $scope.errorMap.put("shipmentServiceDetail_brokerageParty" + index, $rootScope.nls["ERR90585"]);
                serviceDetail.brokerageParty = null;
                $scope.shipmentTab = "rates";
                return false
            }

            if (ValidateUtil.isStatusBlocked(serviceDetail.brokerageParty.status)) {
                console.log($rootScope.nls["ERR90564"]);
                $scope.errorMap.put("shipmentServiceDetail_brokerageParty" + index, $rootScope.nls["ERR90564"]);
                serviceDetail.brokerageParty = null;
                $scope.shipmentTab = "rates";
                return false
            }

            if (ValidateUtil.isStatusHidden(serviceDetail.brokerageParty.status)) {
                console.log($rootScope.nls["ERR90565"]);
                $scope.errorMap.put("shipmentServiceDetail_brokerageParty" + index, $rootScope.nls["ERR90565"]);
                serviceDetail.brokerageParty = null;
                $scope.shipmentTab = "rates";
                return false
            }

        }

        if ((validateCode == 0 || validateCode == 23) && serviceDetail.brokeragePercentage != undefined &&
            serviceDetail.brokeragePercentage != null && serviceDetail.brokeragePercentage != "") {
            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_SHIPMENT_BROKERAGE_PERCENTAGE, serviceDetail.brokeragePercentage)) {
                $scope.errorMap.put("shipmentServiceDetail_brokeragePercentage" + index, $rootScope.nls["ERR90566"]);
                return false;
            }
        }


        if (validateCode == 0 || validateCode == 24) {

            if (serviceDetail.bookedVolumeWeightUnitKg != null && serviceDetail.bookedVolumeWeightUnitKg != undefined && serviceDetail.bookedVolumeWeightUnitKg != "") {
                $scope.errorMap = new Map();
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG, serviceDetail.bookedVolumeWeightUnitKg)) {
                    $scope.errorMap.put("bookedVolumeWeightUnitKg" + index, $rootScope.nls["ERR90213"]);
                    return false;
                }
            }
            if (serviceDetail.bookedVolumeWeightUnitKg != null && serviceDetail.bookedVolumeWeightUnitKg != undefined && serviceDetail.bookedVolumeWeightUnitKg != "") {
                if (serviceDetail.bookedVolumeWeightUnitKg < 0 || serviceDetail.bookedVolumeWeightUnitKg >= 99999999999.99) {
                    $scope.errorMap.put("bookedVolumeWeightUnitKg" + index, $rootScope.nls["ERR90213"]);
                    return false;
                }
            }
            $scope.chargableWeightCalulcate(serviceDetail, 'volumekg');
        }

        if (validateCode == 0 || validateCode == 25) {
            if (serviceDetail.bookedGrossWeightUnitKg != null && serviceDetail.bookedGrossWeightUnitKg != undefined && serviceDetail.bookedGrossWeightUnitKg != "") {
                $scope.errorMap = new Map();
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG, serviceDetail.bookedGrossWeightUnitKg)) {
                    $scope.errorMap.put("bookedGrossWeightUnitKg" + index, $rootScope.nls["ERR90098"]);
                    return false;
                }
            }
            if (serviceDetail.bookedGrossWeightUnitKg != null && serviceDetail.bookedGrossWeightUnitKg != undefined && serviceDetail.bookedGrossWeightUnitKg != "") {
                if (serviceDetail.bookedGrossWeightUnitKg < 0 || serviceDetail.bookedGrossWeightUnitKg >= 99999999999.99) {
                    $scope.errorMap.put("bookedGrossWeightUnitKg" + index, $rootScope.nls["ERR90098"]);
                    return false;
                }

            }

            $scope.chargableWeightCalulcate(serviceDetail, 'grossKg');
        }
        if (validateCode == 0 || validateCode == 26) {

            if (!isNaN(parseFloat(serviceDetail.bookedVolumeUnitCbm))) {
                if (parseFloat(serviceDetail.bookedVolumeUnitCbm) != null && serviceDetail.bookedVolumeUnitCbm != null && serviceDetail.bookedVolumeUnitCbm != undefined && serviceDetail.bookedVolumeUnitCbm != "") {
                    $scope.errorMap = new Map();
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM, serviceDetail.bookedVolumeUnitCbm)) {
                        $scope.errorMap.put("bookedVolumeUnitCbm" + index, $rootScope.nls["ERR90559"]);
                        return false;
                    }
                }
                if (parseFloat(serviceDetail.bookedVolumeUnitCbm) != null && serviceDetail.bookedVolumeUnitCbm != null && serviceDetail.bookedVolumeUnitCbm != undefined && serviceDetail.bookedVolumeUnitCbm != "") {
                    if (serviceDetail.bookedVolumeUnitCbm < 0 || serviceDetail.bookedVolumeUnitCbm >= 99999999999.999) {
                        $scope.errorMap.put("bookedVolumeUnitCbm" + index, $rootScope.nls["ERR90559"]);
                        $scope.shipmentTab = "general";
                        return false;
                    }
                }
            } else {
                serviceDetail.bookedVolumeUnitCbm = null;
            }
        }

        if (validateCode == 0 || validateCode == 27) {
            if (serviceDetail.bookedVolumeWeightUnitPound != null && serviceDetail.bookedVolumeWeightUnitPound != undefined && serviceDetail.bookedVolumeWeightUnitPound != "") {
                $scope.errorMap = new Map();
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_POUND, serviceDetail.bookedVolumeWeightUnitPound)) {
                    $scope.errorMap.put("bookedVolumeWeightUnitPound" + index, $rootScope.nls["ERR90576"]);
                    return false;
                }
            }
            if (serviceDetail.bookedVolumeWeightUnitPound != null && serviceDetail.bookedVolumeWeightUnitPound != undefined && serviceDetail.bookedVolumeWeightUnitPound != "") {
                if (serviceDetail.bookedVolumeWeightUnitPound < 0 || serviceDetail.bookedVolumeWeightUnitPound >= 99999999999.999) {
                    $scope.errorMap.put("bookedVolumeWeightUnitPound" + index, $rootScope.nls["ERR90576"]);
                    return false;
                }
            }
            $scope.chargableWeightCalulcate(serviceDetail, 'volumePound');
        }

        if (validateCode == 0 || validateCode == 28) {
            if (serviceDetail.bookedGrossWeightUnitPound != null && serviceDetail.bookedGrossWeightUnitPound != undefined && serviceDetail.bookedGrossWeightUnitPound != "") {
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_POUND, serviceDetail.bookedGrossWeightUnitPound)) {
                    $scope.errorMap.put("bookedGrossWeightUnitPound" + index, $rootScope.nls["ERR90575"]);
                    return false;
                }
            }
            if (serviceDetail.bookedGrossWeightUnitPound != null && serviceDetail.bookedGrossWeightUnitPound != undefined && serviceDetail.bookedGrossWeightUnitPound != "") {
                if (serviceDetail.bookedGrossWeightUnitPound < 0 || serviceDetail.bookedGrossWeightUnitPound >= 99999999999.999) {
                    $scope.errorMap.put("bookedGrossWeightUnitPound" + index, $rootScope.nls["ERR90575"]);
                    return false;
                }
            }
            $scope.chargableWeightCalulcate(serviceDetail, 'grossPound');
        }

        if (validateCode == 0 || validateCode == 30) {
            if (serviceDetail.bookedPieces != null && serviceDetail.bookedPieces != undefined && serviceDetail.bookedPieces != "") {
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES, serviceDetail.bookedPieces)) {
                    $scope.errorMap.put("bookedPieces" + index, $rootScope.nls["ERR90097"]);
                    return false;
                }
            }
            if (serviceDetail.bookedPieces != null && serviceDetail.bookedPieces != undefined && serviceDetail.bookedPieces != "") {
                if (serviceDetail.bookedPieces < 0 || serviceDetail.bookedPieces >= 99999999999) {
                    $scope.errorMap.put("bookedPieces" + index, $rootScope.nls["ERR90097"]);
                    return false;
                }
            }
        }
        return true;

    }




    $scope.selectPrepaidCollect = function(serviceDetail) {

        if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment != "" && $scope.shipment.tosMaster != undefined) {

            serviceDetail.ppcc = $scope.shipment.tosMaster.freightPPCC == 'Prepaid' ? false : true;

        } else {
            $scope.errorMap = new Map();
            $scope.errorMap.put("tosRequired", $rootScope.nls["ERR90215"]);
            serviceDetail.ppcc = false;
        }


    }





    $scope.statusCompleted = false;
    $scope.changeCompleted = function() {
        if ($scope.statusCompleted == false) {
            ngDialog.openConfirm({
                template: '<p>Are you sure to complete this event ? An Auto-Email will be trigerred.</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">Cancel</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Proceed</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                if (value == 1) {
                    $scope.statusCompleted = true;
                } else if (value == 2) {
                    $scope.statusCompleted = false;
                }
            });
        } else {
            $scope.statusCompleted = false;
        }
    };

    /*Document Tab Starts*/

    $scope.selectRateClass = function(validateCode, documentDetail, pIndex, cIndex, nextFocus) {

        if ($scope.validateServiceDocument(validateCode, documentDetail, pIndex, cIndex)) {
            $rootScope.navigateToNextField(nextFocus);
        }
    }

    $scope.addDocumentRow = function() {

        if ($scope.validateShipmentServiceDetail(0, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex], $scope.selectedTabIndex)) {
            var isDocError = false;
            if ($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList != null && $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList.length > 0) {
                for (var docCIndex = 0; docCIndex < $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList.length; docCIndex++) {
                    var summaCHk = $scope.validateServiceDocument(0, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[docCIndex], $scope.selectedTabIndex, docCIndex);
                    if (!summaCHk) {
                        isDocError = true;
                        break;
                    }
                }
            }
            if (!isDocError) {
                $scope.errorMap = new Map();
                $scope.newFirstDocument(false);
            }

        }

    };




    $scope.deleteServiceDocument = function(index, serviceDetail) {
        console.log("Delete shipment DOcument is called.....", index);
        ngDialog.openConfirm({
            template: '<p>Are you sure you want to delete selected Document  ?</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +

                '</button>' +
                '</div>',
            plain: true,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList.splice(index, 1);
            $scope.documentListTab = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList.length - 1;
            console.log("Shipment service Document Deleted");
        }, function(value) {
            console.log("Shipment Document Deletion cancelled");
        });
    }


    $scope.changeDocumentDimension = function(documentDetail, service) {
        attachToMasterService.calculateDocumentWeight(documentDetail, service);
    }

    $scope.ajaxPartyEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partyList = data.responseObject.searchResult;
                    return $scope.partyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedBrokerageParty = function(serviceDetail, index, nextFocus) {
        if ($scope.validateShipmentServiceDetail(22, serviceDetail, index)) {
            $rootScope.navigateToNextField(nextFocus + index);
        }

    }

    $scope.selectedAjaxShipperEvent = function(documentDetail, pIndex, index, nextFocus) {
        console.log("selectedAjaxPartyEvent")
        if ($scope.validateServiceDocument(1, documentDetail, pIndex, index)) {
            $scope.choosePartyAddress(documentDetail, 'shipper', pIndex, index);
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAjaxCongineeEvent = function(documentDetail, pIndex, index, nextFocus) {
        console.log("selectedAjaxCongineeEvent")
        if ($scope.validateServiceDocument(5, documentDetail, pIndex, index)) {

            $scope.choosePartyAddress(documentDetail, 'consignee');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAjaxFirstNotifyEvent = function(documentDetail, pIndex, index, nextFocus) {
        console.log("selectedAjaxFirstNotifyEvent")
        if ($scope.validateServiceDocument(6, documentDetail, pIndex, index)) {
            $scope.choosePartyAddress(documentDetail, 'firstNotify');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }
    }

    $scope.selectedAjaxSecondNotifyEvent = function(documentDetail, pIndex, index, nextFocus) {

        if ($scope.validateServiceDocument(7, documentDetail, pIndex, index)) {

            $scope.choosePartyAddress(documentDetail, 'secondNotify');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAjaxForwarderEvent = function(documentDetail, pIndex, index, nextFocus) {

        if ($scope.validateServiceDocument(3, documentDetail, pIndex, index)) {

            $scope.choosePartyAddress(documentDetail, 'forwarder');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAjaxAgentEvent = function(documentDetail, pIndex, index, nextFocus) {

        if ($scope.validateServiceDocument(8, documentDetail, pIndex, index)) {

            $scope.choosePartyAddress(documentDetail, 'agent');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAjaxIssueAgentEvent = function(documentDetail, pIndex, index, nextFocus) {

        if ($scope.validateServiceDocument(9, documentDetail, pIndex, index)) {

            $scope.choosePartyAddress(documentDetail, 'issuingAgent');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    /* $scope.selectedParty = function(partyobj) {
         console.log("Selected Party ", partyobj);

         if ($scope.bindPartyObj == 'documentDetail.shipper') {
             $scope.documentDetail.shipper = partyobj;
             $scope.bindParty(partyobj, $scope.documentDetail.shipper, $scope.focusKey, 1, $scope.documentDetail.shipperAddress);
         }

         if ($scope.bindPartyObj == 'documentDetail.forwarder') {
             $scope.documentDetail.forwarder = partyobj;
             $scope.bindParty(partyobj, $scope.documentDetail.forwarder, $scope.focusKey, 3, $scope.documentDetail.forwarderAddress);
         }

         if ($scope.bindPartyObj == 'documentDetail.consignee') {
             $scope.documentDetail.consignee = partyobj;
             $scope.bindParty(partyobj, $scope.documentDetail.consignee, $scope.focusKey, 5, $scope.documentDetail.consigneeAddress);
         }

         if ($scope.bindPartyObj == 'documentDetail.firstNotify') {
             $scope.documentDetail.firstNotify = partyobj;

             $scope.bindParty(partyobj, $scope.documentDetail.firstNotify, $scope.focusKey, 6, $scope.documentDetail.firstNotifyAddress);
         }

         if ($scope.bindPartyObj == 'documentDetail.secondNotify') {
             $scope.documentDetail.secondNotify = partyobj;
             $scope.bindParty(partyobj, $scope.documentDetail.secondNotify, $scope.focusKey, 7, $scope.documentDetail.secondNotifyAddress);
         }

         if ($scope.bindPartyObj == 'documentDetail.agent') {
             $scope.documentDetail.agent = partyobj;
             $scope.bindParty(partyobj, $scope.documentDetail.agent, $scope.focusKey, 8, $scope.documentDetail.agentAddress);
         }

         if ($scope.bindPartyObj == 'documentDetail.issuingAgent') {
             $scope.documentDetail.issuingAgent = partyobj;
             $scope.bindParty(partyobj, $scope.documentDetail.issuingAgent, $scope.focusKey, 9, $scope.documentDetail.issuingAgentAddress);
         }

     };*/

    $scope.cancelParty = function() {
            $scope.showPartyList = false;
        }
        /*  $scope.bindParty = function(partyobj, toParty, focusField, validationIndex, mapToAddrss) {
              console.log("Selected Party ", partyobj);
              $scope.cancelParty();
              $scope.getPartyAddress('PARTY', toParty, mapToAddrss);
              $scope.validateServiceDocument(validationIndex);
              document.getElementById(focusField).focus();
          };*/

    $scope.getPartyAddress = function(obj, mapToAddrss) {
        if (obj != null && obj.id != null) {
            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {
                    mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss, obj, obj.partyAddressList[i]);
                }
            }
        }

    }

    $scope.showPartyAddress = function(addressKey, documentDetail, pIndex, index) {
        $scope.panelTitle = "Party Address";
        $scope.selectedItem = -1;
        $scope.errorMap = new Map();

        $scope.showPartyAddressList = true;
        $scope.docPartyAddressList = [];
        $scope.partyAddressVal = addressKey;

        if ($scope.partyAddressVal == 'shipperAddress') {

            if (documentDetail.shipper == undefined || documentDetail.shipper == null || documentDetail.shipper.id == null) {
                $scope.errorMap.put("documentShipper" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.shipper.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.shipper.partyAddressList[i]);
            }
        } else if ($scope.partyAddressVal == 'forwarderAddress') {

            if (documentDetail.forwarder == undefined || documentDetail.forwarder == null || documentDetail.forwarder.id == null) {
                $scope.errorMap.put("documentForwarder" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.forwarder.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.forwarder.partyAddressList[i]);
            }
        } else if ($scope.partyAddressVal == 'consigneeAddress') {

            if (documentDetail.consignee == undefined || documentDetail.consignee == null || documentDetail.consignee.id == null) {
                $scope.errorMap.put("documentConsignee" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.consignee.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.consignee.partyAddressList[i]);
            }
        } else if ($scope.partyAddressVal == 'firstNotifyAddress') {

            if (documentDetail.firstNotify == undefined || documentDetail.firstNotify == null || documentDetail.firstNotify.id == null) {
                $scope.errorMap.put("documentFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.firstNotify.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.firstNotify.partyAddressList[i]);
            }
        } else if ($scope.partyAddressVal == 'secondNotifyAddress') {

            if (documentDetail.secondNotify == undefined || documentDetail.secondNotify == null || documentDetail.secondNotify.id == null) {
                $scope.errorMap.put("documentSecondNotify" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.secondNotify.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.secondNotify.partyAddressList[i]);
            }
        } else if ($scope.partyAddressVal == 'agentAddress') {

            if (documentDetail.agent == undefined || documentDetail.agent == null || documentDetail.agent.id == null) {
                $scope.errorMap.put("partyAgent" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.agent.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.agent.partyAddressList[i]);
            }
        } else if ($scope.partyAddressVal == 'issuingAgentAddress') {

            if (documentDetail.issuingAgent == undefined || documentDetail.issuingAgent == null || documentDetail.issuingAgent.id == null) {
                $scope.errorMap.put("partyIssuingAgent" + pIndex + "_" + index, $rootScope.nls["ERR95909"]);
                $scope.showPartyAddressList = false;
                return false;
            }

            for (var i = 0; i < documentDetail.issuingAgent.partyAddressList.length; i++) {
                $scope.docPartyAddressList.push(documentDetail.issuingAgent.partyAddressList[i]);
            }
        }


    };



    $scope.choosePartyAddress = function(documentDetail, addressKey, pIndex, index) {

        if (addressKey == 'shipper') {
            if (documentDetail.shipperAddress == undefined || documentDetail.shipperAddress == null) {
                documentDetail.shipperAddress = {};
            }
            if (documentDetail.shipper == undefined || documentDetail.shipper == null) {
                documentDetail.shipperAddress = {};
            }
            $scope.getPartyAddress(documentDetail.shipper, documentDetail.shipperAddress);
            $scope.checkforTsa(documentDetail.shipper, index);

        } else if (addressKey == 'forwarder') {
            if (documentDetail.forwarderAddress == undefined || documentDetail.forwarderAddress == null) {
                documentDetail.forwarderAddress = {};
            }

            if (documentDetail.forwarder == undefined || documentDetail.forwarder == null) {
                documentDetail.forwarderAddress = {};
            }
            $scope.getPartyAddress(documentDetail.forwarder, documentDetail.forwarderAddress);
        } else if (addressKey == 'consignee') {
            if (documentDetail.consigneeAddress == undefined || documentDetail.consigneeAddress == null) {
                documentDetail.consigneeAddress = {};
            }

            if (documentDetail.consignee == undefined || documentDetail.consignee == null) {
                documentDetail.consigneeAddress = {};
            }
            $scope.getPartyAddress(documentDetail.consignee, documentDetail.consigneeAddress);
        } else if (addressKey == 'firstNotify') {
            if (documentDetail.firstNotifyAddress == undefined || documentDetail.firstNotifyAddress == null) {
                documentDetail.firstNotifyAddress = {};
            }

            if (documentDetail.firstNotify == undefined || documentDetail.firstNotify == null) {
                documentDetail.firstNotifyAddress = {};
            }
            $scope.getPartyAddress(documentDetail.firstNotify, documentDetail.firstNotifyAddress);
        } else if (addressKey == 'secondNotify') {
            if (documentDetail.secondNotifyAddress == undefined || documentDetail.secondNotifyAddress == null) {
                documentDetail.secondNotifyAddress = {};
            }

            if (documentDetail.secondNotify == undefined || documentDetail.secondNotify == null) {
                documentDetail.secondNotifyAddress = {};
            }
            $scope.getPartyAddress(documentDetail.secondNotify, documentDetail.secondNotifyAddress);
        } else if (addressKey == 'agent') {
            if (documentDetail.agentAddress == undefined || documentDetail.agentAddress == null) {
                documentDetail.agentAddress = {};
            }

            if (documentDetail.agent == undefined || documentDetail.agent == null) {
                documentDetail.agentAddress = {};
            }
            $scope.getPartyAddress(documentDetail.agent, documentDetail.agentAddress);
        } else if (addressKey == 'issuingAgent') {
            if (documentDetail.issuingAgentAddress == undefined || documentDetail.issuingAgentAddress == null) {
                documentDetail.issuingAgentAddress = {};
            }
            if (documentDetail.issuingAgent == undefined || documentDetail.issuingAgent == null) {
                documentDetail.issuingAgentAddress = {};
            }
            $scope.getPartyAddress(documentDetail.issuingAgent, documentDetail.issuingAgentAddress);
        }


    }






    $scope.selectedPartyAddress = function(obj) {

        var documentDetail = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[$scope.documentListTab];
        if ($scope.partyAddressVal == 'shipperAddress') {
            documentDetail.shipperAddress = addressJoiner.joinAddressLineByLine(documentDetail.shipperAddress, documentDetail.shipper, obj);
            $scope.validateServiceDocument(2, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }
        if ($scope.partyAddressVal == 'forwarderAddress') {
            documentDetail.forwarderAddress = addressJoiner.joinAddressLineByLine(documentDetail.forwarderAddress, documentDetail.forwarder, obj);
            $scope.validateServiceDocument(4, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }
        if ($scope.partyAddressVal == 'consigneeAddress') {
            documentDetail.consigneeAddress = addressJoiner.joinAddressLineByLine(documentDetail.consigneeAddress, documentDetail.consignee, obj);
            $scope.validateServiceDocument(5, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }
        if ($scope.partyAddressVal == 'firstNotifyAddress') {
            documentDetail.firstNotifyAddress = addressJoiner.joinAddressLineByLine(documentDetail.firstNotifyAddress, documentDetail.firstNotify, obj);
            $scope.validateServiceDocument(6, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }
        if ($scope.partyAddressVal == 'secondNotifyAddress') {
            documentDetail.secondNotifyAddress = addressJoiner.joinAddressLineByLine(documentDetail.secondNotifyAddress, documentDetail.secondNotify, obj);
            $scope.validateServiceDocument(7, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }
        if ($scope.partyAddressVal == 'agentAddress') {
            documentDetail.agentAddress = addressJoiner.joinAddressLineByLine(documentDetail.agentAddress, documentDetail.agent, obj);
            $scope.validateServiceDocument(8, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }
        if ($scope.partyAddressVal == 'issuingAgentAddress') {
            documentDetail.issuingAgentAddress = addressJoiner.joinAddressLineByLine(documentDetail.issuingAgentAddress, documentDetail.issuingAgent, obj);
            $scope.validateServiceDocument(9, documentDetail, $scope.selectedTabIndex, $scope.documentListTab);
        }

        $scope.cancelPartyAddress();
    };




    $scope.showPartyAddressList = false;
    $scope.partyAddressListConfig = {
        search: false,
        address: true
    }



    $scope.cancelPartyAddress = function() {
        $scope.showPartyAddressList = false;
    }


    $scope.listDocConfigPack = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "packName",
            seperator: false
        }, {
            "title": "packCode",
            seperator: true
        }]
    }
    $scope.showDocPack = function(pack) {
        $scope.panelTitle = "Pack";
        $scope.selectedItem80 = -1;
        $scope.ajaxDocPackEvent(null);

    };

    $scope.ajaxDocPackEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PackList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.docPackList = data.responseObject.searchResult;
                    return $scope.docPackList;
                    //$scope.showDocumentPackList=true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Pack List');
            }
        );

    }
    $scope.selectedDocPack = function(packObj) {
        console.log("Selected pack", packObj);
        $scope.documentDetail.packMaster = packObj;
        $scope.cancelDocPack();
        //$scope.validateShipmentDocumentDetail(11);
        //document.getElementById('shipmentPack').focus();
    };
    $scope.cancelDocPack = function() {
        console.log("Cancel Docpack List Button Pressed....");
        $scope.searchText = "";
        $scope.showDocumentPackList = false;
    };

    //document Port Starts

    $scope.ajaxDOcOriginEvent = function(documentDetail, object, serviceDetail, pIndex, index) {

        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '') {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + pIndex + '_' + index, $rootScope.nls["ERR90031"]);
            return false;
        }

        console.log("ajaxDocOriginEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {


                if (data.responseCode == "ERR0") {
                    $scope.docOriginList = [];
                    var resultList = data.responseObject.searchResult;
                    if (resultList != null && resultList.length != 0) {
                        if (documentDetail.destination == undefined || documentDetail.destination == null) {
                            $scope.docOriginList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {
                                if (resultList[i].id != documentDetail.destination.id) {
                                    $scope.docOriginList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.docOriginList;
                }



            },
            function(errResponse) {
                console.error('Error while fetching Origin List');
            }
        );

    }

    $scope.selectedDocOrigin = function(documentDetail, pIndex, index, nextFocus) {
        if ($scope.validateServiceDocument(10, documentDetail, pIndex, index)) {

            // Assigning default pol to Origin
            if (documentDetail.pol == undefined || documentDetail.pol == null) {
                documentDetail.pol = documentDetail.origin;
            }

            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }
    };



    $scope.ajaxDocDestination = function(documentDetail, object, serviceDetail, pIndex, index) {
        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '') {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + pIndex + "_" + index, $rootScope.nls["ERR90031"]);
            return false;
        }

        console.log("ajaxDestination ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.docDestinationList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (documentDetail.origin == undefined || documentDetail.origin == null) {
                            $scope.docDestinationList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {
                                if (resultList[i].id != documentDetail.origin.id) {
                                    $scope.docDestinationList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.docDestinationList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Destination List');
            }
        );

    }

    $scope.selectedDocDestination = function(documentDetail, pIndex, index, nextFocus) {
        if ($scope.validateServiceDocument(13, documentDetail, pIndex, index)) {
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }
    };




    $scope.ajaxPortOfDocLoading = function(documentDetail, object, serviceDetail, pIndex, index) {

        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '') {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + pIndex + "_" + index, $rootScope.nls["ERR90031"]);
            return false;
        }
        console.log("ajaxPortOfLoadingEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {


                if (data.responseCode == "ERR0") {
                    $scope.portOfDocLoadingList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (documentDetail.pod == undefined || documentDetail.pod == null) {
                            $scope.portOfDocLoadingList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != documentDetail.pod.id) {
                                    $scope.portOfDocLoadingList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.portOfDocLoadingList;
                }



            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );

    }

    $scope.selectedPortOfDocLoading = function(documentDetail, pIndex, index, nextFocus) {
        if ($scope.validateServiceDocument(11, documentDetail, pIndex, index)) {
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }
    };



    $scope.ajaxPortOfDocDischarge = function(documentDetail, object, serviceDetail, pIndex, index) {

        if (serviceDetail.serviceMaster == undefined || serviceDetail.serviceMaster == null || serviceDetail.serviceMaster == '') {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("serviceMaster" + pIndex + "_" + index, $rootScope.nls["ERR90031"]);
            return false;
        }

        console.log("ajaxPortOfDischarge ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": serviceDetail.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.portOfDocDischargeList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if (documentDetail.pol == undefined || documentDetail.pol == null) {
                            $scope.portOfDocDischargeList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != documentDetail.pol.id) {
                                    $scope.portOfDocDischargeList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.portOfDocDischargeList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching PortOfDischarge List');
            }
        );

    }

    $scope.selectedPortOfDocDischarge = function(documentDetail, pIndex, index, nextFocus) {
        if ($scope.validateServiceDocument(12, documentDetail, pIndex, index)) {

            // Assign default destination to pod
            if (documentDetail.destination == undefined || documentDetail.destination == null) {
                documentDetail.destination = documentDetail.pod;
            }

            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }
    };



    /*$scope.bindDataToDocument = function(serviceDetail) {
     	    	         	    	  
     	    	   console.log("bindDataToDocument is called");
     	    	 
     	    	   
      	    	  if(serviceDetail!=undefined && serviceDetail.documentList!=undefined && serviceDetail.documentList.length>0 && $scope.editDocumentsIndex==null){
      	    	   for(var i=0;i<serviceDetail.documentList.length;i++){
      	    		  if(serviceDetail.carrier!=undefined && serviceDetail.carrier!=null && serviceDetail.carrier!="")   
                 	    	  serviceDetail.documentList[i].carrier=serviceDetail.carrier;
      	    		 if(serviceDetail.eta!=undefined&&serviceDetail.eta!=null&&serviceDetail.eta!="")
      	                   	 serviceDetail.documentList[i].eta=serviceDetail.eta;
      	    		 if(serviceDetail.etd!=undefined&&serviceDetail.etd!=null&&serviceDetail.etd!="")
      	                   	 serviceDetail.documentList[i].etd=serviceDetail.etd;
      	    		 if(serviceDetail.routeNo!=undefined&&serviceDetail.routeNo!=null&&serviceDetail.routeNo!="")
      	                   	 serviceDetail.documentList[i].routeNo=serviceDetail.routeNo;
      	    		 if(serviceDetail.mawbNo!=undefined&&serviceDetail.mawbNo!=null&&serviceDetail.mawbNo!="")     	 
      	                   	 serviceDetail.documentList[i].mawbNo=serviceDetail.mawbNo;
      	    	   }

      	    	  }
                     if($scope.editDocumentsIndex!=null){

                     	for(var i=0;i<serviceDetail.documentList.length;i++){

                     		  if(serviceDetail.carrier!=undefined&&serviceDetail.carrier!=null&&serviceDetail.carrier!="")   
                     	    	  serviceDetail.documentList[i].carrier=serviceDetail.carrier;
          	    		 if(serviceDetail.eta!=undefined&&serviceDetail.eta!=null&&serviceDetail.eta!="")
          	                   	 serviceDetail.documentList[i].eta=serviceDetail.eta;
          	    		 if(serviceDetail.etd!=undefined&&serviceDetail.etd!=null&&serviceDetail.etd!="")
          	                   	 serviceDetail.documentList[i].etd=serviceDetail.etd;
          	    		 if(serviceDetail.routeNo!=undefined&&serviceDetail.routeNo!=null&&serviceDetail.routeNo!="")
          	                   	 serviceDetail.documentList[i].routeNo=serviceDetail.routeNo;
          	    		 if(serviceDetail.mawbNo!=undefined&&serviceDetail.mawbNo!=null&&serviceDetail.mawbNo!="")     	 
          	                   	 serviceDetail.documentList[i].mawbNo=serviceDetail.mawbNo;

     	    	            }
                     	
                     	  if(serviceDetail.carrier!=undefined&&serviceDetail.carrier!=null&&serviceDetail.carrier!="")   
                     	  $scope.documentDetail.carrier=serviceDetail.carrier;
                     	  
                     	  if(serviceDetail.eta!=undefined&&serviceDetail.eta!=null&&serviceDetail.eta!="")  
   	                   	 $scope.documentDetail.eta=serviceDetail.eta;
                     	  
                     	  
                     	  if(serviceDetail.etd!=undefined&&serviceDetail.etd!=null&&serviceDetail.etd!="")	  
   	    	             $scope.documentDetail.etd=serviceDetail.etd;
                     	 
                     	  
                     	  if(serviceDetail.routeNo!=undefined&&serviceDetail.routeNo!=null&&serviceDetail.routeNo!="")	  
   	    	             $scope.documentDetail.routeNo=serviceDetail.routeNo;
                     	  if(serviceDetail.mawbNo!=undefined&&serviceDetail.mawbNo!=null&&serviceDetail.mawbNo!="")     	 	  
   	    	           $scope.documentDetail.mawbNo=serviceDetail.mawbNo;

      	    	  }

                     if($scope.editDocumentsIndex==null){

                     	if(serviceDetail.documentList.length>0){
                     		for(var i=0;i<serviceDetail.documentList.length;i++){
                     		  if(serviceDetail.carrier!=undefined&&serviceDetail.carrier!=null&&serviceDetail.carrier!="")   
                     	    	  serviceDetail.documentList[i].carrier=serviceDetail.carrier;
          	    		 if(serviceDetail.eta!=undefined&&serviceDetail.eta!=null&&serviceDetail.eta!="")
          	                   	 serviceDetail.documentList[i].eta=serviceDetail.eta;
          	    		 if(serviceDetail.etd!=undefined&&serviceDetail.etd!=null&&serviceDetail.etd!="")
          	                   	 serviceDetail.documentList[i].etd=serviceDetail.etd;
          	    		 if(serviceDetail.routeNo!=undefined&&serviceDetail.routeNo!=null&&serviceDetail.routeNo!="")
          	                   	 serviceDetail.documentList[i].routeNo=serviceDetail.routeNo;
          	    		 if(serviceDetail.mawbNo!=undefined&&serviceDetail.mawbNo!=null&&serviceDetail.mawbNo!="")     	 
          	                   	 serviceDetail.documentList[i].mawbNo=serviceDetail.mawbNo;
  	    	            
                     		}
                     		}
                     	
                     	if($scope.documentDetail == null || $scope.documentDetail == undefined){
                     		$scope.documentDetail={};
                     	}
                     	  if(serviceDetail.carrier!=undefined&&serviceDetail.carrier!=null&&serviceDetail.carrier!="")   
                         	  $scope.documentDetail.carrier=serviceDetail.carrier;
                         	  
                         	  if(serviceDetail.eta!=undefined&&serviceDetail.eta!=null&&serviceDetail.eta!="")  
       	                   	 $scope.documentDetail.eta=serviceDetail.eta;
                         	  $scope.documentDetail.eta=$rootScope.convertToDate($scope.documentDetail.eta);
                         	  if(serviceDetail.etd!=undefined&&serviceDetail.etd!=null&&serviceDetail.etd!="")	  
       	    	             $scope.documentDetail.etd=serviceDetail.etd;
                         	  $scope.documentDetail.etd=$rootScope.convertToDate($scope.documentDetail.etd);
                         	  
                         	  if(serviceDetail.routeNo!=undefined&&serviceDetail.routeNo!=null&&serviceDetail.routeNo!="")	  
       	    	             $scope.documentDetail.routeNo=serviceDetail.routeNo;
                         	  if(serviceDetail.mawbNo!=undefined&&serviceDetail.mawbNo!=null&&serviceDetail.mawbNo!="")     	 	  
       	    	           $scope.documentDetail.mawbNo=serviceDetail.mawbNo;
                      }

                
     	       }*/


    $scope.checkforTsa = function(shipper, index) {
        if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.shipmentServiceList[index] != undefined &&
            $scope.shipment.shipmentServiceList[index].serviceMaster.importExport == 'Export') {
            if (ValidateUtil.isTSAExpired(shipper, $rootScope.partyAddressType)) {
                Notification.warning($rootScope.nls["ERR5131"]);
            }
        }
    }

    $scope.isDisabled = function() {


        if ($scope.shipmentServiceDetail != undefined) {
            if ($scope.shipmentServiceDetail.documentList != undefined && $scope.shipmentServiceDetail.documentList.length > 0) {


                if ($scope.editDocumentsIndex != undefined && $scope.editDocumentsIndex != null && $scope.editDocumentsIndex == 0) {

                    return false;
                } else {

                    return true;
                }

            } else {
                return false;
            }
        }
    }

    //Document Port Ends


    //Validate Document
    $scope.validateServiceDocument = function(validateCode, documentDetail, pIndex, index) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if (documentDetail.shipper == undefined || documentDetail.shipper.id == undefined || documentDetail.shipper == null || documentDetail.shipper == "") {

                $log.debug($rootScope.nls["ERR90106"]);
                $scope.errorMap.put("documentShipper" + pIndex + "_" + index, $rootScope.nls["ERR90106"]);
                $scope.shipmentTab = 'documents';
                return false;

            } else {
                if (documentDetail.shipper.isDefaulter) {
                    $log.debug($rootScope.nls["ERR90586"]);
                    $scope.errorMap.put("documentShipper" + pIndex + "_" + index, $rootScope.nls["ERR90586"]);
                    documentDetail.shipper = {};
                    documentDetail.shipperAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }

                if (ValidateUtil.isStatusBlocked(documentDetail.shipper.status)) {
                    $scope.errorMap.put("documentShipper" + pIndex + "_" + index, $rootScope.nls["ERR90107"]);
                    $log.debug($rootScope.nls["ERR90107"]);
                    documentDetail.shipper = {};
                    documentDetail.shipperAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }

                if (ValidateUtil.isStatusHidden(documentDetail.shipper.status)) {
                    $scope.errorMap.put("documentShipper" + pIndex + "_" + index, $rootScope.nls["ERR90108"]);
                    $log.debug($rootScope.nls["ERR90108"]);
                    documentDetail.shipper = {};
                    documentDetail.shipperAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }

            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if (documentDetail.shipperAddress.addressLine1 == undefined || documentDetail.shipperAddress.addressLine1 == null || documentDetail.shipperAddress.addressLine1 == "") {
                $scope.errorMap.put("document.shipperAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90110"]);
                $log.debug($rootScope.nls["ERR90110"]);
                $scope.shipmentTab = 'documents';
                return false;
            }
            /*if (documentDetail.shipperAddress.addressLine2 == null || documentDetail.shipperAddress.addressLine2 == "" || documentDetail.shipperAddress.addressLine2 == undefined) {
                $scope.errorMap.put("document.shipperAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90112"]);
                $log.debug($rootScope.nls["ERR90112"]);
                $scope.shipmentTab = 'documents';
                return false;
            } */
            /*if($scope.shipment!=undefined && $scope.shipment!=null && $scope.shipment.shipmentServiceList[index]!=undefined && 
            		$scope.shipment.shipmentServiceList[index].serviceMaster.importExport=='Export'){
                 	$scope.checkforTsa(documentDetail.shipper,index);
            }*/

            if (documentDetail.shipperAddress.email != null && documentDetail.shipperAddress.email != undefined && documentDetail.shipperAddress.email != "") {
                if (ValidateUtil.isValidMail(documentDetail.shipperAddress.email)) {
                    $scope.errorMap.put("document.shipperAdrress.email" + pIndex + "_" + index, $rootScope.nls["ERR90118"]);
                    $log.debug($rootScope.nls["ERR90118"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }
            }

        }

        //Forwarder

        if (validateCode == 0 || validateCode == 3) {


            if (documentDetail.forwarder != undefined && documentDetail.forwarder != null && documentDetail.forwarder != "") {

                if (documentDetail.forwarder.isDefaulter) {
                    $scope.errorMap.put("documentForwarder" + pIndex + "_" + index, $rootScope.nls["ERR90591"]);
                    $log.debug($rootScope.nls["ERR90591"]);
                    documentDetail.forwarder = {};
                    documentDetail.forwarderAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.forwarder.status)) {
                    $scope.errorMap.put("documentForwarder" + pIndex + "_" + index, $rootScope.nls["ERR90120"]);
                    $log.debug($rootScope.nls["ERR90120"]);
                    documentDetail.forwarder = {};
                    documentDetail.forwarderAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusHidden(documentDetail.forwarder.status)) {
                    $scope.errorMap.put("documentForwarder" + pIndex + "_" + index, $rootScope.nls["ERR90121"]);
                    $log.debug($rootScope.nls["ERR90121"]);
                    documentDetail.forwarder = null;
                    documentDetail.forwarderAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }
            }

        }
        if (validateCode == 0 || validateCode == 4) {

            if (documentDetail.forwarder != null && documentDetail.forwarder != undefined && documentDetail.forwarder.id != null) {

                if (documentDetail.forwarderAddress.addressLine1 == undefined || documentDetail.forwarderAddress.addressLine1 == null ||
                    documentDetail.forwarderAddress.addressLine1 == "") {
                    $log.debug($rootScope.nls["ERR90123"]);
                    $scope.errorMap.put("document.forwarderAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90123"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                /*if (documentDetail.forwarderAddress.addressLine2 == undefined || documentDetail.forwarderAddress.addressLine2 == null || documentDetail.forwarderAddress.addressLine2 == "") {
                	$log.debug($rootScope.nls["ERR90125"]);
                    $scope.errorMap.put("document.forwarderAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90125"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }*/

                if (documentDetail.forwarderAddress.email != null && documentDetail.forwarderAddress.email != undefined && documentDetail.forwarderAddress.email != "") {
                    if (!CommonValidationService.checkMultipleMail(documentDetail.forwarderAddress.email)) {
                        $log.debug($rootScope.nls["ERR90131"]);
                        $scope.errorMap.put("document.forwarderAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90131"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 5) {

            if (documentDetail.consignee == undefined || documentDetail.consignee.id == undefined || documentDetail.consignee == null || documentDetail.consignee == "") {
                $log.debug($rootScope.nls["ERR90132"]);
                $scope.errorMap.put("documentConsignee" + pIndex + "_" + index, $rootScope.nls["ERR90132"]);
                $scope.shipmentTab = 'documents';
                return false;
            } else {

                if (documentDetail.consignee.isDefaulter) {
                    $log.debug($rootScope.nls["ERR90587"]);
                    $scope.errorMap.put("documentConsignee" + pIndex + "_" + index, $rootScope.nls["ERR90587"]);
                    documentDetail.consignee = {};
                    documentDetail.consigneeAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }
                if (ValidateUtil.isStatusBlocked(documentDetail.consignee.status)) {
                    $scope.errorMap.put("documentConsignee" + pIndex + "_" + index, $rootScope.nls["ERR90133"]);
                    $log.debug($rootScope.nls["ERR90133"]);
                    documentDetail.consignee = {};
                    documentDetail.consigneeAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }
                if (ValidateUtil.isStatusHidden(documentDetail.consignee.status)) {
                    $log.debug($rootScope.nls["ERR90134"]);
                    $scope.errorMap.put("documentConsignee" + pIndex + "_" + index, $rootScope.nls["ERR90134"]);
                    documentDetail.consignee = {};
                    documentDetail.consigneeAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }
            }
        }


        if (validateCode == 0 || validateCode == 6) {
            if (documentDetail.firstNotify == undefined || documentDetail.firstNotify.id == undefined || documentDetail.firstNotify == null || documentDetail.firstNotify == "") {

            } else {

                if (documentDetail.firstNotify.isDefaulter) {
                    $scope.errorMap.put("documentFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR90588"]);
                    $log.debug($rootScope.nls["ERR90588"]);
                    documentDetail.firstNotify = {};
                    documentDetail.firstNotifyAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.firstNotify.status)) {
                    $scope.errorMap.put("documentFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR90146"]);
                    $log.debug($rootScope.nls["ERR90146"]);
                    documentDetail.firstNotify = {};
                    documentDetail.firstNotifyAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }

                if (ValidateUtil.isStatusHidden(documentDetail.firstNotify.status)) {
                    $scope.errorMap.put("documentFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR90147"]);
                    $log.debug($rootScope.nls["ERR90147"]);
                    documentDetail.firstNotify = {};
                    documentDetail.firstNotifyAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }
            }
        }

        if (validateCode == 0 || validateCode == 7) {
            if (documentDetail.secondNotify == undefined || documentDetail.secondNotify.id == undefined ||
                documentDetail.secondNotify == null ||
                documentDetail.secondNotify == "") {

            } else {

                if (documentDetail.secondNotify.isDefaulter) {
                    $log.debug($rootScope.nls["ERR90589"]);
                    $scope.errorMap.put("documentSecondNotify" + pIndex + "_" + index, $rootScope.nls["ERR90589"]);
                    documentDetail.secondNotify = {};
                    documentDetail.secondNotifyAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }

                if (ValidateUtil.isStatusBlocked(documentDetail.secondNotify.status)) {
                    $scope.errorMap.put("documentSecondNotify" + pIndex + "_" + index, $rootScope.nls["ERR90159"]);
                    $log.debug($rootScope.nls["ERR90159"]);
                    documentDetail.secondNotify = {};
                    documentDetail.secondNotifyAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusHidden(documentDetail.secondNotify.status)) {
                    $scope.errorMap.put("documentSecondNotify" + pIndex + "_" + index, $rootScope.nls["ERR90160"]);
                    $log.debug($rootScope.nls["ERR90160"]);
                    documentDetail.secondNotify = {};
                    documentDetail.secondNotifyAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {
            if (documentDetail.agent == undefined || documentDetail.agent.id == undefined || documentDetail.agent == null || documentDetail.agent == "") {} else {
                if (documentDetail.agent.isDefaulter) {
                    $scope.errorMap.put("partyAgent" + pIndex + "_" + index, $rootScope.nls["ERR90590"]);
                    $log.debug($rootScope.nls["ERR90590"]);
                    documentDetail.agent = {};
                    documentDetail.agentAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.agent.status)) {
                    $scope.errorMap.put("partyAgent" + pIndex + "_" + index, $rootScope.nls["ERR90172"]);
                    $log.debug($rootScope.nls["ERR90172"]);
                    documentDetail.agent = {};
                    documentDetail.agentAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusHidden(documentDetail.agent.status)) {
                    $scope.errorMap.put("partyAgent" + pIndex + "_" + index, $rootScope.nls["ERR90173"]);
                    $log.debug($rootScope.nls["ERR90173"]);
                    documentDetail.agent = {};
                    documentDetail.agentAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {


            if (documentDetail.issuingAgent == undefined || documentDetail.issuingAgent.id == undefined || documentDetail.issuingAgent == null || documentDetail.issuingAgent == "") {} else {
                if (documentDetail.issuingAgent.isDefaulter) {
                    $scope.errorMap.put("documentPartyIssuigAgent" + pIndex + "_" + index, $rootScope.nls["ERR90592"]);
                    $log.debug($rootScope.nls["ERR90592"]);
                    documentDetail.issuingAgent = {};
                    documentDetail.issuingAgentAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.issuingAgent.status)) {
                    $scope.errorMap.put("documentPartyIssuigAgent" + pIndex + "_" + index, $rootScope.nls["ERR90185"]);
                    $log.debug($rootScope.nls["ERR90185"]);
                    documentDetail.issuingAgent = {};
                    documentDetail.issuingAgentAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false
                }

                if (ValidateUtil.isStatusHidden(documentDetail.issuingAgent.status)) {
                    $scope.errorMap.put("documentPartyIssuigAgent" + pIndex + "_" + index, $rootScope.nls["ERR90186"]);
                    $log.debug($rootScope.nls["ERR90186"]);
                    documentDetail.issuingAgent = {};
                    documentDetail.issuingAgentAddress = {};
                    $scope.shipmentTab = 'documents';
                    return false

                }
            }

        }


        if (validateCode == 0 || validateCode == 10) {

            if ($scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport == true) {
                if (documentDetail.origin == undefined || documentDetail.origin == null || documentDetail.origin.id == null || documentDetail.origin.id.trim == "") {
                    $scope.errorMap.put("docOrigin" + pIndex + "_" + index, $rootScope.nls["ERR90013"]);
                    $log.debug($rootScope.nls["ERR90013"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.origin.status)) {
                    documentDetail.origin = null;
                    $scope.errorMap.put("docOrigin" + pIndex + "_" + index, $rootScope.nls["ERR90037"]);
                    $log.debug($rootScope.nls["ERR90037"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusHidden(documentDetail.origin.status)) {
                    documentDetail.origin = null;
                    console.log($rootScope.nls["ERR90038"]);
                    $scope.errorMap.put("docOrigin" + pIndex + "_" + index, $rootScope.nls["ERR90038"]);
                    $log.debug($rootScope.nls["ERR90038"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

            }
        }


        if (validateCode == 0 || validateCode == 11) {

            if ($scope.shipment.isFromConsolForImport == undefined || $scope.shipment.isFromConsolForImport != 'true' || $scope.shipment.isFromConsolForImport != true) {
                if (documentDetail.pol == null ||
                    documentDetail.pol.id == null ||
                    documentDetail.pol.id.trim == "" || documentDetail.pol == undefined) {
                    console.log($rootScope.nls["ERR90041"]);
                    $scope.errorMap.put("docPOL" + pIndex + "_" + index, $rootScope.nls["ERR90041"]);
                    $log.debug($rootScope.nls["ERR90041"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.pol.status)) {
                    documentDetail.pol = null;
                    $scope.errorMap.put("docPOL" + pIndex + "_" + index, $rootScope.nls["ERR90042"]);
                    $log.debug($rootScope.nls["ERR90042"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusHidden(documentDetail.pol.status)) {
                    documentDetail.pol = null;
                    $scope.errorMap.put("docPOL" + pIndex + "_" + index, $rootScope.nls["ERR90043"]);
                    $log.debug($rootScope.nls["ERR90043"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

            }

        }

        if (validateCode == 0 || validateCode == 12) {
            if ($scope.shipment.isFromConsolForImport == undefined || $scope.shipment.isFromConsolForImport != 'true' || $scope.shipment.isFromConsolForImport != true) {
                if (documentDetail.pod == null || documentDetail.pod.id == null || documentDetail.pod.id.trim == "" || documentDetail.pod == undefined) {
                    console.log($rootScope.nls["ERR90044"]);
                    $scope.errorMap.put("docPOD" + pIndex + "_" + index, $rootScope.nls["ERR90044"]);
                    $log.debug($rootScope.nls["ERR90044"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.pod.status)) {
                    documentDetail.pod = null;
                    $log.debug($rootScope.nls["ERR90045"]);
                    $scope.errorMap.put("docPOD" + pIndex + "_" + index, $rootScope.nls["ERR90045"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusHidden(documentDetail.pod.status)) {
                    documentDetail.pod = null;
                    $log.debug($rootScope.nls["ERR90046"]);
                    $scope.errorMap.put("docPOD" + pIndex + "_" + index, $rootScope.nls["ERR90046"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 13) {

            if ($scope.shipment.isFromConsolForImport == undefined || $scope.shipment.isFromConsolForImport != 'true' || $scope.shipment.isFromConsolForImport != true) {
                if (documentDetail.destination == null ||
                    documentDetail.destination.id == null ||
                    documentDetail.destination.id.trim == "" || documentDetail.destination == undefined) {
                    $scope.errorMap.put("docDestination" + pIndex + "_" + index, $rootScope.nls["ERR90014"]);
                    $log.debug($rootScope.nls["ERR90014"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked(documentDetail.destination.status)) {
                    documentDetail.destination = null;
                    $scope.errorMap.put("docDestination" + pIndex + "_" + index, $rootScope.nls["ERR90039"]);
                    $log.debug($rootScope.nls["ERR90039"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                if (ValidateUtil.isStatusHidden(documentDetail.destination.status)) {
                    documentDetail.destination = null;
                    $scope.errorMap.put("docDestination" + pIndex + "_" + index, $rootScope.nls["ERR90040"]);
                    $log.debug($rootScope.nls["ERR90040"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

            }

        }
        //Calculate Weights
        if (validateCode == 0 || validateCode == 14) {

            if ($scope.shipment.shipmentServiceList[pIndex].serviceMaster.importExport == 'Export') {
                if (documentDetail.noOfPieces == undefined || documentDetail.noOfPieces == "" || documentDetail.noOfPieces == null) {
                    $scope.errorMap.put("docPieces" + pIndex + "_" + index, $rootScope.nls["ERR90473"]);
                    $log.debug($rootScope.nls["ERR90473"]);
                    return false;
                } else {
                    if (documentDetail.noOfPieces == 0) {
                        $log.debug($rootScope.nls["ERR90474"]);
                        $scope.errorMap.put("docPieces" + pIndex + "_" + index, $rootScope.nls["ERR90474"]);
                        return false;
                    }
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES, documentDetail.noOfPieces)) {
                        $log.debug($rootScope.nls["ERR90474"]);
                        $scope.errorMap.put("docPieces" + pIndex + "_" + index, $rootScope.nls["ERR90474"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }
            }



        }

        if (validateCode == 0 || validateCode == 15) {
            if ($scope.shipment.shipmentServiceList[pIndex].serviceMaster.importExport == 'Export') {
                if (documentDetail.dimensionUnit) {
                    if (documentDetail.grossWeightInPound == undefined ||
                        documentDetail.grossWeightInPound == null ||
                        documentDetail.grossWeightInPound == "") {
                        $scope.errorMap.put("grossWeightPound" + pIndex + "_" + index, $rootScope.nls["ERR90477"]);
                        $log.debug($rootScope.nls["ERR90477"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    } else {
                        if (documentDetail.grossWeightInPound <= 0 || documentDetail.grossWeightInPound > 99999999999.99) {
                            $scope.errorMap.put("grossWeightPound" + pIndex + "_" + index, $rootScope.nls["ERR90478"]);
                            $log.debug($rootScope.nls["ERR90478"]);
                            $scope.shipmentTab = 'documents';
                            return false;
                        }
                        if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_POUND, documentDetail.grossWeightInPound)) {
                            $scope.errorMap.put("grossWeightPound" + pIndex + "_" + index, $rootScope.nls["ERR90478"]);
                            $log.debug($rootScope.nls["ERR90478"]);
                            $scope.shipmentTab = 'documents';
                            return false;
                        }
                        $scope.changeDocumentDimension(documentDetail, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
                    }
                    console.log("Gross Weight in Pound validation done...");
                }
            }

        }

        if (validateCode == 0 || validateCode == 16) {
            if ($scope.shipment.shipmentServiceList[pIndex].serviceMaster.importExport == 'Export') {
                if (documentDetail.grossWeight == undefined ||
                    documentDetail.grossWeight == null ||
                    documentDetail.grossWeight == "") {
                    $log.debug($rootScope.nls["ERR90475"]);
                    $scope.errorMap.put("grossWeightKg" + pIndex + "_" + index, $rootScope.nls["ERR90475"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                } else {

                    if (documentDetail.grossWeight <= 0 || documentDetail.grossWeight > 99999999999.99) {
                        $scope.errorMap.put("grossWeightKg" + pIndex + "_" + index, $rootScope.nls["ERR90476"]);
                        $log.debug($rootScope.nls["ERR90476"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG, documentDetail.grossWeight)) {
                        $scope.errorMap.put("grossWeightKg" + pIndex + "_" + index, $rootScope.nls["ERR90476"]);
                        $log.debug($rootScope.nls["ERR90476"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }

                    $scope.changeDocumentDimension(documentDetail, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
                }
            }

            console.log("Gross Weight in Kg validation done...");
        }

        if (validateCode == 0 || validateCode == 17) {
            if ($scope.shipment.shipmentServiceList[pIndex].serviceMaster.importExport == 'Export') {
                if (documentDetail.dimensionUnit) {
                    if (documentDetail.volumeWeightInPound == undefined ||
                        documentDetail.volumeWeightInPound == null ||
                        documentDetail.volumeWeightInPound == "") {
                        $scope.errorMap.put("volumeWeightPound" + pIndex + "_" + index, $rootScope.nls["ERR90481"]);
                        $log.debug($rootScope.nls["ERR90481"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    } else {
                        if (documentDetail.volumeWeightInPound <= 0 || documentDetail.volumeWeightInPound > 99999999999.99) {
                            $scope.errorMap.put("volumeWeightPound" + pIndex + "_" + index, $rootScope.nls["ERR90482"]);
                            $log.debug($rootScope.nls["ERR90482"]);
                            $scope.shipmentTab = 'documents';
                            return false;
                        }
                        if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_POUND, documentDetail.volumeWeightInPound)) {
                            $scope.errorMap.put("volumeWeightPound" + pIndex + "_" + index, $rootScope.nls["ERR90482"]);
                            $log.debug($rootScope.nls["ERR90482"]);
                            $scope.shipmentTab = 'documents';
                            return false;
                        }
                        $scope.changeDocumentDimension(documentDetail, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
                    }
                    console.log("Volume Weight pound validation done...");
                }
            }

        }

        if (validateCode == 0 || validateCode == 18) {
            if ($scope.shipment.shipmentServiceList[pIndex].serviceMaster.importExport == 'Export') {
                if (documentDetail.volumeWeight == undefined || documentDetail.volumeWeight == null || documentDetail.volumeWeight == "") {
                    $scope.errorMap.put("volumeWeightKg" + pIndex + "_" + index, $rootScope.nls["ERR90479"]);
                    $log.debug($rootScope.nls["ERR90479"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                } else {
                    if (documentDetail.volumeWeight <= 0 || documentDetail.volumeWeight > 99999999999.99) {
                        $scope.errorMap.put("volumeWeightKg" + pIndex + "_" + index, $rootScope.nls["ERR90480"]);
                        $log.debug($rootScope.nls["ERR90480"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG, documentDetail.volumeWeight)) {
                        $scope.errorMap.put("volumeWeightKg" + pIndex + "_" + index, $rootScope.nls["ERR90480"]);
                        $log.debug($rootScope.nls["ERR90480"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                    $scope.changeDocumentDimension(documentDetail, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
                }
            }

        }

        //Calculate Weights Ends
        if (validateCode == 0 || validateCode == 19) {
            if (documentDetail.handlingInformation != null && documentDetail.handlingInformation != undefined && documentDetail.handlingInformation != "") {}
        }

        if (validateCode == 0 || validateCode == 20) {
            if (documentDetail.accoutingInformation != null && documentDetail.accoutingInformation != undefined && documentDetail.accoutingInformation != "") {}
        }

        if (validateCode == 0 || validateCode == 21) {
            if (documentDetail.commodityDescription != null && documentDetail.commodityDescription != undefined && documentDetail.commodityDescription != "") {}
        }

        if (validateCode == 0 || validateCode == 22) {
            if (documentDetail.marksAndNo != null && documentDetail.marksAndNo != undefined && documentDetail.marksAndNo != "") {}
        }

        if (validateCode == 0 || validateCode == 23) {
            if (documentDetail.rateClass != null && documentDetail.rateClass != undefined && documentDetail.rateClass != "") {
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_RATE_CLASS, documentDetail.rateClass)) {
                    $scope.errorMap.put("rateclass" + pIndex + "_" + index, $rootScope.nls["ERR90204"]);
                    $log.debug($rootScope.nls["ERR90204"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 24) {

            if ($scope.shipment.isFromConsolForImport == undefined || $scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport == true) {



                if (documentDetail.carrier != null && documentDetail.carrier != undefined && documentDetail.carrier != "") {
                    if (ValidateUtil.isStatusBlocked(documentDetail.carrier.status)) {
                        documentDetail.carrier = null;
                        $scope.errorMap.put("shipmentCarrier" + pIndex + "_" + index, $rootScope.nls["ERR90064"]);
                        $log.debug($rootScope.nls["ERR90064"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden(documentDetail.carrier.status)) {
                        documentDetail.carrier = null;
                        $scope.errorMap.put("shipmentCarrier" + pIndex + "_" + index, $rootScope.nls["ERR90065"]);
                        $log.debug($rootScope.nls["ERR90065"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }

            }
        }

        if (validateCode == 0 || validateCode == 25) {

            if (isNaN(documentDetail.ratePerCharge)) {
                documentDetail.ratePerCharge = null;
            }
            if (documentDetail.ratePerCharge == undefined || documentDetail.ratePerCharge == null || documentDetail.ratePerCharge == "") {


            } else {
                if (documentDetail.ratePerCharge <= 0 || documentDetail.ratePerCharge > 9999999999.999999) {
                    $scope.errorMap.put("ratePerCharge" + pIndex + "_" + index, $rootScope.nls["ERR90556"]);
                    $log.debug($rootScope.nls["ERR90556"]);
                    $scope.shipmentTab = "documents";
                    return false;
                }

                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_RATE_CLASS, documentDetail.ratePerCharge)) {
                    $scope.errorMap.put("ratePerCharge" + pIndex + "_" + index, $rootScope.nls["ERR90556"]);
                    $log.debug($rootScope.nls["ERR90556"]);
                    $scope.shipmentTab = "documents";
                    return false;
                }
            }
            console.log("ratePerCharge validation done...");
        }

        if (validateCode == 0 || validateCode == 26) {
            if (documentDetail.bookedVolumeUnitCbm == undefined || documentDetail.bookedVolumeUnitCbm == null || documentDetail.bookedVolumeUnitCbm == "") {} else {

                if (documentDetail.bookedVolumeUnitCbm != null && documentDetail.bookedVolumeUnitCbm != undefined && documentDetail.bookedVolumeUnitCbm != "")
                    if (documentDetail.bookedVolumeUnitCbm <= 0 || documentDetail.bookedVolumeUnitCbm > 9999999999.999) {
                        $scope.errorMap.put("bookedVolumeUnitCbm" + pIndex + "_" + index, $rootScope.nls["ERR90559"]);
                        $log.debug($rootScope.nls["ERR90559"]);
                        $scope.shipmentTab = "documents";
                        return false;
                    }
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM, documentDetail.bookedVolumeUnitCbm)) {
                    $scope.errorMap.put("bookedVolumeUnitCbm" + pIndex + "_" + index, $rootScope.nls["ERR90559"]);
                    $log.debug($rootScope.nls["ERR90559"]);
                    $scope.shipmentTab = "documents";
                    return false;
                }

                $scope.changeDocumentDimension(documentDetail, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex]);
            }
        }

        if (validateCode == 0) {
            if ($scope.shipment.isFromConsolForImport == undefined || $scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport == true) {
                if (documentDetail.eta != null && documentDetail.etd != null) {
                    var etd = $rootScope.convertToDate(documentDetail.etd);
                    var eta = $rootScope.convertToDate(documentDetail.eta);
                    if (etd > eta) {
                        $log.debug($rootScope.nls["ERR90096"]);
                        $scope.errorMap.put("etd" + pIndex + "_" + index, $rootScope.nls["ERR90096"]);
                        $scope.shipmentTab = "documents";
                        return false;
                    }

                }
            }
        }

        /* if (validateCode == 0 || validateCode == 28) {
             if (documentDetail.etd != null && documentDetail.eta != null) {
                 var etd = $rootScope.convertToDate(documentDetail.etd);
                 var eta = $rootScope.convertToDate(documentDetail.eta);
                 if (etd > eta) {
                     console.log($rootScope.nls["ERR90096"]);
                     $scope.errorMap.put("eta" + pIndex + "_" + index, $rootScope.nls["ERR90096"]);
                     $scope.shipmentTab = "documents";
                     return false;
                 }

             }
         }
         */

        if (validateCode == 0 || validateCode == 29) {
            if ($scope.shipment.isFromConsolForImport == undefined || $scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport == true) {
                if (documentDetail.routeNo != undefined && documentDetail.routeNo != "" && documentDetail.routeNo != null) {
                    if (documentDetail.routeNo == 0) {
                        console.log($rootScope.nls["ERR90066"]);
                        $scope.errorMap.put("flight" + pIndex + "_" + index, $rootScope.nls["ERR90066"]);
                        $scope.shipmentTab = "documents";
                        return false;
                    }
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_FLIGHT_NUMBER, documentDetail.routeNo)) {
                        console.log($rootScope.nls["ERR90066"]);
                        $scope.errorMap.put("flight" + pIndex + "_" + index, $rootScope.nls["ERR90066"]);
                        $scope.shipmentTab = "documents";
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 30) {
            if (documentDetail.mawbNo != undefined && documentDetail.mawbNo != "" && documentDetail.mawbNo != null) {
                if (documentDetail.mawbNo == 0) {
                    console.log($rootScope.nls["ERR90068"]);
                    $scope.errorMap.put("mawbNo" + pIndex + "_" + index, $rootScope.nls["ERR90068"]);
                    $scope.shipmentTab = "documents";
                    return false;
                }
                if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MAWB_NUMBER, documentDetail.mawbNo)) {
                    console.log($rootScope.nls["ERR90068"]);
                    $scope.errorMap.put("mawbNo" + pIndex + "_" + index, $rootScope.nls["ERR90068"]);
                    $scope.shipmentTab = "documents";
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 31) {

            if (documentDetail.packMaster != undefined &&
                documentDetail.packMaster != null &&
                documentDetail.packMaster != "" &&
                documentDetail.packMaster.id != null) {


                if (documentDetail.packMaster.status != null) {
                    if (ValidateUtil.isStatusBlocked(documentDetail.packMaster.status)) {
                        console.log($rootScope.nls["ERR90057"]);
                        $scope.errorMap.put("shipmentPack" + pIndex + "_" + index, $rootScope.nls["ERR90057"]);
                        documentDetail.packMaster = null;
                        $scope.shipmentTab = "documents";
                        return false

                    }
                    if (ValidateUtil.isStatusHidden(documentDetail.packMaster.status)) {
                        console.log($rootScope.nls["ERR90058"]);
                        $scope.errorMap.put("shipmentPack" + pIndex + "_" + index, $rootScope.nls["ERR90058"]);
                        documentDetail.packMaster = null;
                        $scope.shipmentTab = "documents";
                        return false
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 32) {

            if (documentDetail.consignee != null && documentDetail.consignee != undefined && documentDetail.consignee.id != null) {

                if (documentDetail.consigneeAddress.addressLine1 == undefined || documentDetail.consigneeAddress.addressLine1 == null ||
                    documentDetail.consigneeAddress.addressLine1 == "") {
                    $scope.errorMap.put("document.consigneeAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90136"]);
                    $scope.shipmentTab = "documents";
                    return false
                }

                /*if (documentDetail.consigneeAddress.addressLine2 == undefined || documentDetail.consigneeAddress.addressLine2 == null ||
                    documentDetail.consigneeAddress.addressLine2 == "") {
                    $scope.errorMap.put("document.consigneeAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90138"]);
                    $scope.shipmentTab = "documents";
                    return false
                } */

                if (documentDetail.consigneeAddress.email != null && documentDetail.consigneeAddress.email != undefined && documentDetail.consigneeAddress.email != "") {
                    if (!CommonValidationService.checkMultipleMail(documentDetail.consigneeAddress.email)) {
                        $scope.errorMap.put("document.consigneeAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90144"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }

            }
        }
        if (validateCode == 0 || validateCode == 33) {

            if (documentDetail.firstNotify != null && documentDetail.firstNotify != undefined && documentDetail.firstNotify.id != null) {
                if (documentDetail.firstNotifyAddress.addressLine1 == undefined || documentDetail.firstNotifyAddress.addressLine1 == null || documentDetail.firstNotifyAddress.addressLine1 == "") {
                    $scope.errorMap.put("document.firstNotifyAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90149"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                /*if (documentDetail.firstNotifyAddress.addressLine2 == undefined || documentDetail.firstNotifyAddress.addressLine2 == null ||
                    documentDetail.firstNotifyAddress.addressLine2 == "") {
                    $scope.errorMap.put("document.firstNotifyAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90151"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                } */

                if (documentDetail.firstNotifyAddress.email != null && documentDetail.firstNotifyAddress.email != undefined && documentDetail.firstNotifyAddress.email != "") {
                    if (!CommonValidationService.checkMultipleMail(documentDetail.firstNotifyAddress.email)) {
                        $scope.errorMap.put("document.firstNotifyAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90157"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 34) {

            if (documentDetail.secondNotify != null && documentDetail.secondNotify != undefined && documentDetail.secondNotify.id != null) {

                if (documentDetail.secondNotifyAddress.addressLine1 == undefined || documentDetail.secondNotifyAddress.addressLine1 == null || documentDetail.secondNotifyAddress.addressLine1 == "") {
                    $scope.errorMap.put("document.secondNotifyAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90162"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                /*if (documentDetail.secondNotifyAddress.addressLine2 == undefined || documentDetail.secondNotifyAddress.addressLine2 == null || documentDetail.secondNotifyAddress.addressLine2 == "") {
                    $scope.errorMap.put("document.secondNotifyAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90164"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                } */

                if (documentDetail.secondNotifyAddress.email != null && documentDetail.secondNotifyAddress.email != undefined && documentDetail.secondNotifyAddress.email != "") {
                    if (!CommonValidationService.checkMultipleMail(documentDetail.secondNotifyAddress.email)) {
                        $scope.errorMap.put("document.secondNotifyAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90170"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }

            }
        }

        if (validateCode == 0 || validateCode == 35) {

            if ($scope.shipment.isFromConsolForImport != undefined && ($scope.shipment.isFromConsolForImport == 'true' || $scope.shipment.isFromConsolForImport == true)) {
                documentDetail.issuingAgent = null;
                documentDetail.issuingAgentAddress = null;
            } else {
                if (documentDetail.issuingAgent != null && documentDetail.issuingAgent != undefined && documentDetail.issuingAgent.id != null) {

                    if (documentDetail.issuingAgentAddress.addressLine1 == undefined || documentDetail.issuingAgentAddress.addressLine1 == null ||
                        documentDetail.issuingAgentAddress.addressLine1 == "") {
                        $scope.errorMap.put("document.issuingAgentAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90188"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }

                    /*if (documentDetail.issuingAgentAddress.addressLine2 == undefined || documentDetail.issuingAgentAddress.addressLine2 == null || documentDetail.issuingAgentAddress.addressLine2 == "") {
                        $scope.errorMap.put("document.issuingAgentAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90190"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    } */

                    if (documentDetail.issuingAgentAddress.email != null && documentDetail.issuingAgentAddress.email != undefined && documentDetail.issuingAgentAddress.email != "") {
                        if (!CommonValidationService.checkMultipleMail(documentDetail.issuingAgentAddress.email)) {
                            $scope.errorMap.put("document.issuingAgentAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90196"]);
                            $scope.shipmentTab = 'documents';
                            return false;
                        }
                    }

                }
            }
        }


        if (validateCode == 0 || validateCode == 36) {

            if (documentDetail.agent != null && documentDetail.agent != undefined && documentDetail.agent.id != null) {

                if (documentDetail.agentAddress.addressLine1 == undefined || documentDetail.agentAddress.addressLine1 == null || documentDetail.agentAddress.addressLine1 == "") {
                    $scope.errorMap.put("document.agentAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90175"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }

                /*if (documentDetail.agentAddress.addressLine2 == undefined || documentDetail.agentAddress.addressLine2 == null || documentDetail.agentAddress.addressLine2 == "") {
                    $scope.errorMap.put("document.agentAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR90177"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                } */

                if (documentDetail.agentAddress.email != null && documentDetail.agentAddress.email != undefined && documentDetail.agentAddress.email != "") {
                    if (!CommonValidationService.checkMultipleMail(documentDetail.agentAddress.email)) {
                        $scope.errorMap.put("document.agentAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90183"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 37) {


            if (documentDetail.chaAgent != null && documentDetail.chaAgent != undefined && documentDetail.chaAgent.id != null) {

                if (documentDetail.chaAgentAddress.addressLine1 == undefined || documentDetail.chaAgentAddress.addressLine1 == null ||
                    documentDetail.chaAgentAddress.addressLine1 == "") {
                    $scope.errorMap.put("document.chaAgentAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR90705"]);
                    $scope.shipmentTab = 'documents';
                    return false;
                }


                if (documentDetail.chaAgentAddress.email != null && documentDetail.chaAgentAddress.email != undefined && documentDetail.chaAgentAddress.email != "") {
                    if (!CommonValidationService.checkMultipleMail(documentDetail.chaAgentAddress.email)) {
                        $scope.errorMap.put("document.chaAgentAddress.email" + pIndex + "_" + index, $rootScope.nls["ERR90706"]);
                        $scope.shipmentTab = 'documents';
                        return false;
                    }
                }

            }

        }




        return true;

    }

    /*Document Tab Ends*/


    $scope.changeCompleted = function() {
        if ($scope.statusCompleted == false) {
            ngDialog.openConfirm({
                template: '<p>Are you sure to complete this event ? An Auto-Email will be trigerred.</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">Cancel</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Proceed</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                if (value == 1) {
                    $scope.statusCompleted = true;
                } else if (value == 2) {
                    $scope.statusCompleted = false;
                }
            });
        } else {
            $scope.statusCompleted = false;
        }
    };


    /*Rate Tab Starts*/

    $scope.discardRate = function() {
        if (!discardService1.compare($scope.charge)) {

            ngDialog.openConfirm({
                template: '<p>Do you want to update your changes?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveRate();
                } else {
                    $scope.closeRate();
                }
            });

        } else {
            $scope.closeRate();
        }
    }

    $scope.getDefaultRate = function() {
        console.log("Get Default Rate is called.");
    }

    $scope.addRate = function() {
        console.log("Add Rate is called.");
        $scope.charge = {};
        $scope.editRateIndex = null;
        $scope.toggleRate = false;
        $scope.charge.actualChargeable = false;
        $scope.charge.ppcc = false;
        $scope.errorMap = new Map();
        $scope.firstFocusInCharge = true;
        discardService1.set($scope.charge);
    };

    $scope.closeRate = function() {
        console.log("Close Rate is called.");
        $scope.toggleRate = true;
    };

    $scope.editRate = function(index) {
        console.log("Edit Rate is called.");
        $scope.toggleRate = false;
        $scope.editRateIndex = index;
        $scope.charge = cloneService.clone($scope.shipmentServiceDetail.shipmentChargeList[index]);
        $scope.firstFocusInCharge = true;
        $scope.charge.actualChargeable = $scope.charge.actualChargeable == 'ACTUAL' ? true : false;
        $scope.charge.ppcc = $scope.charge.ppcc == 'Prepaid' ? false : true;
        if ($scope.charge.note != null && $scope.charge.note != '' && $scope.charge.note != undefined) {
            $scope.notecheck = true;
        }
        discardService1.set($scope.charge);
    };



    /*$scope.setNoteFocus = function(num){

            document.getElementById('notecheck').focus();
        }*/


    $scope.deleteRate = function(index) {
        console.log("Delete rate service is called.....", index);
        ngDialog.openConfirm({
            template: '<p>Are you sure you want to delete selected rate  ?</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
                '</div>',
            plain: true,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            $scope.shipmentServiceDetail.shipmentChargeList.splice(index, 1);
            console.log("Shipment rate Deleted");
            $scope.rateCalculation();

        }, function(value) {
            console.log("Shipment rate Deletion cancelled");
        });
    }

    $scope.rateCalculation = function() {

        $scope.shipmentServiceDetail.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
        $scope.shipmentServiceDetail.localCurrencyCode = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode;

        $scope.shipmentServiceDetail.clientGrossRate = 0;
        $scope.shipmentServiceDetail.clientNetRate = 0;
        $scope.shipmentServiceDetail.declaredCost = 0;
        $scope.shipmentServiceDetail.cost = 0;
        $scope.shipmentServiceDetail.rate = 0;

        if ($scope.shipmentServiceDetail.shipmentChargeList != undefined && $scope.shipmentServiceDetail.shipmentChargeList != null) {
            for (var i = 0; i < $scope.shipmentServiceDetail.shipmentChargeList.length; i++) {
                $scope.charge = $scope.shipmentServiceDetail.shipmentChargeList[i];

                $scope.charge.localCurrency = $scope.shipmentServiceDetail.localCurrency;
                $scope.charge.localCurrencyCode = $scope.shipmentServiceDetail.localCurrency.currencyCode;

                $scope.charge.estimatedUnit = $rootScope.unitCalc($scope.charge.unitMaster, $scope.shipmentServiceDetail.bookedGrossWeightUnitKg, $scope.shipmentServiceDetail.bookedVolumeWeightUnitKg, $scope.shipmentServiceDetail.bookedPieces, $scope.shipmentServiceDetail.bookedVolumeUnitCbm, $scope.shipmentServiceDetail.bookedChargeableUnit);

                if ($scope.charge.grossSaleCurrency != undefined && $scope.charge.grossSaleCurrency != null && $scope.charge.grossSaleCurrency.id != null) {
                    var tmpCGR = $scope.charge.estimatedUnit * $scope.charge.grossSaleAmount;
                    if ($scope.charge.grossSaleMinimum != null && $scope.charge.grossSaleMinimum != "") {
                        $scope.charge.totalGrossSale = tmpCGR > $scope.charge.grossSaleMinimum ? tmpCGR : $scope.charge.grossSaleMinimum;
                    } else {
                        $scope.charge.totalGrossSale = tmpCGR;
                    }

                    $scope.charge.totalGrossSale = $rootScope.currency($scope.charge.grossSaleCurrency, $scope.charge.totalGrossSale);

                    var sellRate = null;

                    if ($rootScope.baseCurrenyRate[$scope.charge.grossSaleCurrency.id] != null && $scope.charge.localCurrency.id != $scope.charge.grossSaleCurrency.id) {
                        sellRate = $rootScope.baseCurrenyRate[$scope.charge.grossSaleCurrency.id].csell;
                    } else if ($scope.charge.localCurrency.id == $scope.charge.grossSaleCurrency.id) {
                        sellRate = 1;
                    }

                    if (sellRate != null) {
                        $scope.charge.localGrossSaleRoe = sellRate;
                        $scope.charge.localTotalGrossSale = $scope.charge.totalGrossSale * $scope.charge.localGrossSaleRoe;
                        $scope.charge.localTotalGrossSale = $rootScope.currency($scope.charge.grossSaleCurrency, $scope.charge.localTotalGrossSale);

                        $scope.shipmentServiceDetail.clientGrossRate = $scope.shipmentServiceDetail.clientGrossRate + $scope.charge.localTotalGrossSale;
                    }

                }

                if ($scope.charge.netSaleCurrency != undefined && $scope.charge.netSaleCurrency != null && $scope.charge.netSaleCurrency.id != null) {
                    var tmpCNR = $scope.charge.estimatedUnit * $scope.charge.netSaleAmount;
                    if ($scope.charge.netSaleMinimum != null && $scope.charge.netSaleMinimum != "") {
                        $scope.charge.totalNetSale = tmpCNR > $scope.charge.netSaleMinimum ? tmpCNR : $scope.charge.netSaleMinimum;
                    } else {
                        $scope.charge.totalNetSale = tmpCNR;
                    }
                    $scope.charge.totalNetSale = $rootScope.currency($scope.charge.netSaleCurrency, $scope.charge.totalNetSale);

                    var sellRate = null;

                    if ($rootScope.baseCurrenyRate[$scope.charge.netSaleCurrency.id] != null && $scope.charge.localCurrency.id != $scope.charge.netSaleCurrency.id) {
                        sellRate = $rootScope.baseCurrenyRate[$scope.charge.netSaleCurrency.id].csell;
                    } else if ($scope.charge.localCurrency.id == $scope.charge.netSaleCurrency.id) {
                        sellRate = 1;
                    }

                    if (sellRate != null) {
                        $scope.charge.localNetSaleRoe = sellRate;
                        $scope.charge.localTotalNetSale = $scope.charge.totalNetSale * $scope.charge.localNetSaleRoe;
                        $scope.charge.localTotalNetSale = $rootScope.currency($scope.charge.netSaleCurrency, $scope.charge.localTotalNetSale);

                        $scope.shipmentServiceDetail.clientNetRate = $scope.shipmentServiceDetail.clientNetRate + $scope.charge.localTotalNetSale;
                    }


                }

                if ($scope.charge.declaredSaleCurrency != undefined && $scope.charge.declaredSaleCurrency != null && $scope.charge.declaredSaleCurrency.id != null) {
                    var tmpDC = $scope.charge.estimatedUnit * $scope.charge.declaredSaleAmount;

                    if ($scope.charge.declaredSaleMinimum != null && $scope.charge.declaredSaleMinimum != "") {
                        $scope.charge.totalDeclaredSale = tmpDC > $scope.charge.declaredSaleMinimum ? tmpDC : $scope.charge.declaredSaleMinimum;
                    } else {
                        $scope.charge.totalDeclaredSale = tmpDC;
                    }
                    $scope.charge.totalDeclaredSale = $rootScope.currency($scope.charge.declaredSaleCurrency, $scope.charge.totalDeclaredSale);

                    var sellRate = null;

                    if ($rootScope.baseCurrenyRate[$scope.charge.declaredSaleCurrency.id] != null && $scope.charge.localCurrency.id != $scope.charge.declaredSaleCurrency.id) {
                        sellRate = $rootScope.baseCurrenyRate[$scope.charge.declaredSaleCurrency.id].csell;
                    } else if ($scope.charge.localCurrency.id == $scope.charge.declaredSaleCurrency.id) {
                        sellRate = 1;
                    }

                    if (sellRate != null) {
                        $scope.charge.localDeclaredSaleRoe = sellRate;
                        $scope.charge.localTotalDeclaredSale = $scope.charge.totalDeclaredSale * $scope.charge.localDeclaredSaleRoe;
                        $scope.charge.localTotalDeclaredSale = $rootScope.currency($scope.charge.declaredSaleCurrency, $scope.charge.localTotalDeclaredSale);

                        $scope.shipmentServiceDetail.declaredCost = $scope.shipmentServiceDetail.declaredCost + $scope.charge.localTotalDeclaredSale;
                    }



                }

                if ($scope.charge.costCurrency != undefined && $scope.charge.costCurrency != null && $scope.charge.costCurrency.id != null) {
                    var tmpCost = $scope.charge.estimatedUnit * $scope.charge.costAmount;
                    if ($scope.charge.costMinimum != null && $scope.charge.costMinimum != "") {
                        $scope.charge.totalCostAmount = tmpCost > $scope.charge.costMinimum ? tmpCost : $scope.charge.costMinimum;
                    } else {
                        $scope.charge.totalCostAmount = tmpCost;
                    }
                    $scope.charge.totalCostAmount = $rootScope.currency($scope.charge.costCurrency, $scope.charge.totalCostAmount);

                    var sellRate = null;

                    if ($rootScope.baseCurrenyRate[$scope.charge.costCurrency.id] != null && $scope.charge.localCurrency.id != $scope.charge.costCurrency.id) {
                        sellRate = $rootScope.baseCurrenyRate[$scope.charge.costCurrency.id].csell;
                    } else if ($scope.charge.localCurrency.id == $scope.charge.costCurrency.id) {
                        sellRate = 1;
                    }

                    if (sellRate != null) {
                        $scope.charge.localCostAmountRoe = sellRate;
                        $scope.charge.localTotalCostAmount = $scope.charge.totalCostAmount * $scope.charge.localCostAmountRoe;
                        $scope.charge.localTotalCostAmount = $rootScope.currency($scope.charge.costCurrency, $scope.charge.localTotalCostAmount);

                        $scope.shipmentServiceDetail.cost = $scope.shipmentServiceDetail.cost + $scope.charge.localTotalCostAmount;
                    }


                }

                if ($scope.charge.rateCurrency != undefined && $scope.charge.rateCurrency != null && $scope.charge.rateCurrency.id != null) {
                    var tmpRate = $scope.charge.estimatedUnit * $scope.charge.rateAmount;

                    if ($scope.charge.rateMinimum != null && $scope.charge.rateMinimum != "") {
                        $scope.charge.totalRateAmount = tmpRate > $scope.charge.rateMinimum ? tmpRate : $scope.charge.rateMinimum;
                    } else {
                        $scope.charge.totalRateAmount = tmpRate;
                    }
                    $scope.charge.totalRateAmount = $rootScope.currency($scope.charge.rateCurrency, $scope.charge.totalRateAmount);

                    var sellRate = null;

                    if ($rootScope.baseCurrenyRate[$scope.charge.rateCurrency.id] != null && $scope.charge.localCurrency.id != $scope.charge.rateCurrency.id) {
                        sellRate = $rootScope.baseCurrenyRate[$scope.charge.rateCurrency.id].csell;
                    } else if ($scope.charge.localCurrency.id == $scope.charge.rateCurrency.id) {
                        sellRate = 1;
                    }

                    if (sellRate != null) {
                        $scope.charge.localRateAmountRoe = sellRate;
                        $scope.charge.localTotalRateAmount = $scope.charge.totalRateAmount * $scope.charge.localRateAmountRoe;
                        $scope.charge.localTotalRateAmount = $rootScope.currency($scope.charge.rateCurrency, $scope.charge.localTotalRateAmount);

                        $scope.shipmentServiceDetail.rate = $scope.shipmentServiceDetail.rate + $scope.charge.localTotalRateAmount;
                    }


                }

            }
        }


        $scope.shipmentServiceDetail.clientGrossRate = $rootScope.currency($scope.shipmentServiceDetail.localCurrency, $scope.shipmentServiceDetail.clientGrossRate);
        $scope.shipmentServiceDetail.clientNetRate = $rootScope.currency($scope.shipmentServiceDetail.localCurrency, $scope.shipmentServiceDetail.clientNetRate);
        $scope.shipmentServiceDetail.declaredCost = $rootScope.currency($scope.shipmentServiceDetail.localCurrency, $scope.shipmentServiceDetail.declaredCost);
        $scope.shipmentServiceDetail.cost = $rootScope.currency($scope.shipmentServiceDetail.localCurrency, $scope.shipmentServiceDetail.cost);
        $scope.shipmentServiceDetail.rate = $rootScope.currency($scope.shipmentServiceDetail.localCurrency, $scope.shipmentServiceDetail.rate);

    }
    $scope.saveRate = function() {
        console.log("Save Rate is Called.....");

        if ($scope.validateChargeTab(0)) {

            $scope.charge.actualChargeable = $scope.charge.actualChargeable ? 'ACTUAL' : 'CHARGEABLE';
            $scope.charge.ppcc = $scope.charge.ppcc ? 'Prepaid' : 'Collect';

            if ($scope.editRateIndex == null) {
                if ($scope.shipmentServiceDetail.shipmentChargeList == null) {
                    $scope.shipmentServiceDetail.shipmentChargeList = [];
                    console.log("Shipment Charge List is created.......");
                }
                $scope.shipmentServiceDetail.shipmentChargeList.push($scope.charge);
            } else {
                $scope.shipmentServiceDetail.shipmentChargeList.splice($scope.editRateIndex, 1, $scope.charge);
            }

            $scope.editRateIndex = null;
            $scope.toggleRate = true;
            $scope.rateCalculation();
            console.log("Charge List Updated.....");
        } else {
            console.log("Charge Validation Failed");
        }
    };

    $scope.showChargeMasterList = false;

    $scope.chargeMasterListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "chargeName",
            seperator: false
        }, {
            "title": "chargeCode",
            seperator: true
        }]
    }


    $scope.showChargeMaster = function() {
        $scope.selectedItem = -1;
        $scope.panelTitle = "Charge";
        /*$scope.ajaxChargeEvent(null);*/
    };


    $scope.ajaxChargeEvent = function(object) {

        console.log("ajaxChargeEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.chargeMasterList = data.responseObject.searchResult;
                    /*$scope.showChargeMasterList=true;*/
                }
            },
            function(errResponse) {
                console.error('Error while fetching Charge');
            }
        );
    }


    $scope.selectedChargeMaster = function(obj) {
        $scope.charge.chargeName = "";
        console.log("Selected Charge ", obj);
        //$scope.charge.chargeMaster= obj;
        if ($scope.validateChargeTab(1))
            $scope.charge.chargeName = obj.chargeName;
        $scope.cancelChargeMaster();
        document.getElementById('rateCharge').focus();

    };

    $scope.cancelChargeMaster = function() {
        $scope.showChargeMasterList = false;
    }



    /*Unit Select Picker*/

    $scope.showUnitMasterList = false;

    $scope.unitMasterListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "unitName",
            seperator: false
        }, {
            "title": "unitCode",
            seperator: true
        }]
    }


    $scope.showUnitMaster = function() {
        $scope.selectedItem = -1;
        $scope.panelTitle = "Unit";
        $scope.ajaxUnitEvent(null);
    };


    $scope.ajaxUnitEvent = function(object) {

        console.log("ajaxUnitEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.unitMasterList = data.responseObject.searchResult;
                    $scope.showUnitMasterList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Unit List');
            }
        );
    }


    $scope.selectedUnitMaster = function(obj) {
        console.log("Selected Unit ", obj);
        $scope.charge.unitMaster = obj;
        $scope.validateChargeTab(4);
        $scope.cancelUnitMaster();
        document.getElementById('rateUnit').focus();
    };

    $scope.cancelUnitMaster = function() {
        $scope.showUnitMasterList = false;
    }


    /*Gross Currency Select Picker*/

    $scope.showGrossCurrencyList = false;

    $scope.grossCurrencyListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "currencyName",
            seperator: false
        }, {
            "title": "currencyCode",
            seperator: true
        }]
    }


    $scope.showGrossCurrency = function() {
        console.log("Showing List of Currency");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.ajaxGrossCurrency(null);
    };


    $scope.ajaxGrossCurrency = function(object) {

        console.log("ajaxGrossCurrency ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.grossCurrencyList = data.responseObject.searchResult;
                    $scope.showGrossCurrencyList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }


    $scope.selectedGrossCurrency = function(obj) {
        console.log("Selected Gross Currency ", obj);
        $scope.charge.grossSaleCurrency = obj;
        $scope.validateChargeTab(7);
        $scope.cancelGrossCurrency();
        document.getElementById('grossCurrency').focus();
    };



    $scope.cancelGrossCurrency = function() {
        $scope.showGrossCurrencyList = false;
    }




    /*Net Currency Select Picker*/

    $scope.showNetCurrencyList = false;

    $scope.netCurrencyListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "currencyName",
            seperator: false
        }, {
            "title": "currencyCode",
            seperator: true
        }]
    }


    $scope.showNetCurrency = function() {
        console.log("Showing List of Currency");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.ajaxNetCurrency(null);
    };


    $scope.ajaxNetCurrency = function(object) {

        console.log("ajaxNetCurrency ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.netCurrencyList = data.responseObject.searchResult;
                    $scope.showNetCurrencyList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }


    $scope.selectedNetCurrency = function(obj) {
        console.log("Selected Net Currency ", obj);
        $scope.charge.netSaleCurrency = obj;
        $scope.validateChargeTab(10);
        $scope.cancelNetCurrency();
        document.getElementById('netCurrency').focus();
    };



    $scope.cancelNetCurrency = function() {
        $scope.showNetCurrencyList = false;
    }


    /*Declared Currency Select Picker*/

    $scope.showDeclaredCurrencyList = false;

    $scope.declaredCurrencyListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "currencyName",
            seperator: false
        }, {
            "title": "currencyCode",
            seperator: true
        }]
    }


    $scope.showDeclaredCurrency = function() {
        console.log("Showing List of Currency");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.ajaxDeclaredCurrency(null);
    };


    $scope.ajaxDeclaredCurrency = function(object) {

        console.log("ajaxDeclaredCurrency ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.declaredCurrencyList = data.responseObject.searchResult;
                    $scope.showDeclaredCurrencyList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }


    $scope.selectedDeclaredCurrency = function(obj) {
        console.log("Selected Declared Currency ", obj);
        $scope.charge.declaredSaleCurrency = obj;
        $scope.validateChargeTab(13);
        $scope.cancelDeclaredCurrency();
        document.getElementById('declaredCurrency').focus();
    };



    $scope.cancelDeclaredCurrency = function() {
        $scope.showDeclaredCurrencyList = false;
    }


    /*Rate Currency Select Picker*/

    $scope.showRateCurrencyList = false;

    $scope.rateCurrencyListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "currencyName",
            seperator: false
        }, {
            "title": "currencyCode",
            seperator: true
        }]
    }


    $scope.showRateCurrency = function() {
        console.log("Showing List of Currency");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.ajaxRateCurrency(null);
    };


    $scope.ajaxRateCurrency = function(object) {

        console.log("ajaxRateCurrency ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.rateCurrencyList = data.responseObject.searchResult;
                    $scope.showRateCurrencyList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }


    $scope.selectedRateCurrency = function(obj) {
        console.log("Selected Rate Currency ", obj);
        $scope.charge.rateCurrency = obj;
        $scope.validateChargeTab(16);
        $scope.cancelRateCurrency();
        document.getElementById('rateCurrency').focus();
    };



    $scope.cancelRateCurrency = function() {
        $scope.showRateCurrencyList = false;
    }


    /*Cost Currency Select Picker*/

    $scope.showCostCurrencyList = false;

    $scope.costCurrencyListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "currencyName",
            seperator: false
        }, {
            "title": "currencyCode",
            seperator: true
        }]
    }


    $scope.showCostCurrency = function() {
        console.log("Showing List of Currency");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.ajaxCostCurrency(null);
    };


    $scope.ajaxCostCurrency = function(object) {

        console.log("ajaxCostCurrency ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.costCurrencyList = data.responseObject.searchResult;
                    $scope.showCostCurrencyList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }


    $scope.selectedCostCurrency = function(obj) {
        console.log("Selected Cost Currency ", obj);
        $scope.charge.costCurrency = obj;
        $scope.validateChargeTab(19);
        $scope.cancelCostCurrency();
        document.getElementById('costCurrency').focus();
    };



    $scope.cancelCostCurrency = function() {
        $scope.showCostCurrencyList = false;
    }


    /*Vendor Select Picker*/

    $scope.showVendorList = false;

    $scope.vendorListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "partyName",
            seperator: false
        }, {
            "title": "partyCode",
            seperator: true
        }]
    }


    $scope.showVendor = function() {
        console.log("Showing List of Party");
        $scope.selectedItem = -1;
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.ajaxVendorEvent(null);
    };


    $scope.ajaxVendorEvent = function(object) {

        console.log("ajaxVendorEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.vendorList = data.responseObject.searchResult;
                    $scope.showVendorList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
            }
        );
    }


    $scope.selectedVendor = function(obj) {
        console.log("Selected Vendor ", obj);
        $scope.charge.vendor = obj;
        $scope.validateChargeTab(22);
        $scope.cancelVendor();
        document.getElementById('rateParty').focus();
    };



    $scope.cancelVendor = function() {
        $scope.showVendorList = false;
    }



    /* Rate Tab validation Begins */

    $scope.validateChargeTab = function(validateCode) {

        console.log("Validate Charge Tab executed..");

        $scope.errorMap = new Map();


        if (validateCode == 0 || validateCode == 1) {

            if ($scope.charge.chargeMaster == undefined ||
                $scope.charge.chargeMaster.id == undefined ||
                $scope.charge.chargeMaster == null ||
                $scope.charge.chargeMaster.id == null) {

                console.log("chargeMaster", $rootScope.nls["ERR90300"]);
                $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR90300"]);
                return false;

            } else {

                if (ValidateUtil.isStatusBlocked($scope.charge.chargeMaster.status)) {
                    console.log("chargeMaster", $rootScope.nls["ERR90301"]);
                    $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR90301"]);
                    $scope.charge.chargeMaster = null;
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.charge.chargeMaster.status)) {
                    console.log("chargeMaster", $rootScope.nls["ERR90302"]);
                    $scope.errorMap.put("chargeMaster", $rootScope.nls["ERR90302"]);
                    $scope.charge.chargeMaster = null;
                    return false;
                }

            }
        }


        if (validateCode == 0 || validateCode == 2) {

            if ($scope.charge.chargeName == undefined ||
                $scope.charge.chargeName == null ||
                $scope.charge.chargeName == "") {
                console.log("chargeName", $rootScope.nls["ERR90303"]);
                $scope.errorMap.put("chargeName", $rootScope.nls["ERR90303"]);
                return false;
            } else {
                var regexp = new RegExp("^[ 0-9a-zA-Z-'.,&@:;?!()$#*_]{0,100}$");
                if (!regexp.test($scope.charge.chargeName)) {
                    console.log("chargeName invalid ", $rootScope.nls["ERR90304"])
                    $scope.errorMap.put("chargeName", $rootScope.nls["ERR90304"]);
                    return false;
                }
            }

        }


        if (validateCode == 0 || validateCode == 3) {

        }


        if (validateCode == 0 || validateCode == 4) {
            if ($scope.charge.unitMaster == undefined ||
                $scope.charge.unitMaster == null ||
                $scope.charge.unitMaster.id == undefined ||
                $scope.charge.unitMaster.id == null) {

                console.log("Please Select Unit....");

                $scope.errorMap.put("unitMaster", $rootScope.nls["ERR90305"]);

                return false;

            } else if ($scope.charge.unitMaster.status != null) {

                if (ValidateUtil.isStatusBlocked($scope.charge.unitMaster.status)) {

                    $scope.charge.unitMaster = null;

                    console.log("Selected unitMaster is Blocked");

                    $scope.errorMap.put("unitMaster", $rootScope.nls["ERR90306"]);

                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.charge.unitMaster.status)) {

                    $scope.charge.unitMaster = null;

                    console.log("Selected unitMaster is Hidden");

                    $scope.errorMap.put("unitMaster", $rootScope.nls["ERR90307"]);

                    return false
                }

            }
        }


        if (validateCode == 0 || validateCode == 5) {

            if ($scope.charge.noOfMinUnit != undefined &&
                $scope.charge.noOfMinUnit != null &&
                $scope.charge.noOfMinUnit != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.noOfMinUnit))) {
                        $scope.errorMap.put("Min", $rootScope.nls["ERR90310"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("Min", $rootScope.nls["ERR90310"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if (!regexp.test($scope.charge.noOfMinUnit)) {
                    $scope.errorMap.put("Min", $rootScope.nls["ERR90310"]);
                    return false;
                }

                if ($scope.charge.noOfMinUnit == 0) {
                    $scope.errorMap.put("Min", $rootScope.nls["ERR90308"]);
                    return false;
                }

                if ($scope.charge.noOfMaxUnit != undefined && $scope.charge.noOfMaxUnit != null && +($scope.charge.noOfMaxUnit) < +($scope.charge.noOfMinUnit)) {
                    $scope.errorMap.put("Min", $rootScope.nls["ERR90312"]);
                    return false;
                } else {
                    if ($scope.charge.noOfMinUnit < 0 || $scope.charge.noOfMinUnit >= 99999999999.999) {
                        $scope.errorMap.put("Min", $rootScope.nls["ERR90310"]);
                        return false;
                    }
                }

            }
        }

        if (validateCode == 0 || validateCode == 6) {

            if ($scope.charge.noOfMaxUnit != undefined &&
                $scope.charge.noOfMaxUnit != null &&
                $scope.charge.noOfMaxUnit != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.noOfMaxUnit))) {
                        $scope.errorMap.put("Max", $rootScope.nls["ERR90311"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("Max", $rootScope.nls["ERR90311"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if (!regexp.test($scope.charge.noOfMaxUnit)) {
                    $scope.errorMap.put("Max", $rootScope.nls["ERR90311"]);
                    return false;
                }

                if ($scope.charge.noOfMaxUnit == 0) {
                    $scope.errorMap.put("Max", $rootScope.nls["ERR90309"]);
                    return false;
                }

                if ($scope.charge.noOfMinUnit != undefined && $scope.charge.noOfMinUnit != null && +($scope.charge.noOfMaxUnit) < +($scope.charge.noOfMinUnit)) {
                    $scope.errorMap.put("Max", $rootScope.nls["ERR90312"]);
                    return false;
                } else {
                    if ($scope.charge.noOfMaxUnit < 0 || $scope.charge.noOfMaxUnit >= 99999999999.999) {
                        $scope.errorMap.put("Max", $rootScope.nls["ERR90311"]);
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 7) {
            if ($scope.charge.grossSaleCurrency != undefined &&
                $scope.charge.grossSaleCurrency != null &&
                $scope.charge.grossSaleCurrency.id != undefined &&
                $scope.charge.grossSaleCurrency.id != null) {

                if (ValidateUtil.isStatusBlocked($scope.charge.grossSaleCurrency.status)) {

                    $scope.charge.grossSaleCurrency = null;

                    console.log("Selected grossSaleCurrency is Blocked");

                    $scope.errorMap.put("grossSaleCurrency", $rootScope.nls["ERR90313"]);

                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.charge.grossSaleCurrency.status)) {

                    $scope.charge.grossSaleCurrency = null;

                    console.log("Selected grossSaleCurrency is Hidden");

                    $scope.errorMap.put("grossSaleCurrency", $rootScope.nls["ERR90314"]);

                    return false
                }

                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != $scope.charge.grossSaleCurrency.id) {
                    if ($rootScope.baseCurrenyRate[$scope.charge.grossSaleCurrency.id] == null) {
                        var st = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + " -> " + $scope.charge.grossSaleCurrency.currencyCode;
                        console.log("Selected Currency does not have Rate of Exchange : " + st + $rootScope.nls["ERR90345"]);
                        $scope.charge.grossSaleCurrency = null;
                        $scope.errorMap.put("grossSaleCurrency", $rootScope.nls["ERR90345"]);
                        return false
                    }
                }

            } else {}

        }


        if (validateCode == 0 || validateCode == 8) {
            if ($scope.charge.grossSaleAmount != undefined &&
                $scope.charge.grossSaleAmount != null &&
                $scope.charge.grossSaleAmount != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.grossSaleAmount))) {
                        $scope.errorMap.put("grossSaleAmount", $rootScope.nls["ERR90316"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("grossSaleAmount", $rootScope.nls["ERR90316"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.grossSaleCurrency != null && $scope.charge.grossSaleCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.grossSaleAmount)) {
                    $scope.errorMap.put("grossSaleAmount", $rootScope.nls["ERR90316"]);
                    return false;
                }

                if ($scope.charge.grossSaleAmount == 0) {
                    $scope.errorMap.put("grossSaleAmount", $rootScope.nls["ERR90315"]);
                    return false;
                } else {
                    if ($scope.charge.grossSaleAmount < 0 || $scope.charge.grossSaleAmount >= 99999999999.999) {
                        $scope.errorMap.put("grossSaleAmount", $rootScope.nls["ERR90316"]);
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {
            if ($scope.charge.grossSaleMinimum != undefined &&
                $scope.charge.grossSaleMinimum != null &&
                $scope.charge.grossSaleMinimum != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.grossSaleMinimum))) {
                        $scope.errorMap.put("grossSaleMinimum", $rootScope.nls["ERR90318"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("grossSaleMinimum", $rootScope.nls["ERR90318"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.grossSaleCurrency != null && $scope.charge.grossSaleCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.grossSaleMinimum)) {
                    $scope.errorMap.put("grossSaleMinimum", $rootScope.nls["ERR90318"]);
                    return false;
                }

                if ($scope.charge.grossSaleMinimum == 0) {
                    $scope.errorMap.put("grossSaleMinimum", $rootScope.nls["ERR90317"]);
                    return false;
                } else {
                    if ($scope.charge.grossSaleMinimum < 0 || $scope.charge.grossSaleMinimum >= 99999999999.999) {
                        $scope.errorMap.put("grossSaleMinimum", $rootScope.nls["ERR90318"]);
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 10) {
            if ($scope.charge.netSaleCurrency != undefined &&
                $scope.charge.netSaleCurrency != null &&
                $scope.charge.netSaleCurrency.id != undefined &&
                $scope.charge.netSaleCurrency.id != null) {

                if (ValidateUtil.isStatusBlocked($scope.charge.netSaleCurrency.status)) {

                    $scope.charge.netSaleCurrency = null;

                    console.log("Selected netSaleCurrency is Blocked");

                    $scope.errorMap.put("netSaleCurrency", $rootScope.nls["ERR90319"]);

                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.charge.netSaleCurrency.status)) {

                    $scope.charge.netSaleCurrency = null;

                    console.log("Selected netSaleCurrency is Hidden");

                    $scope.errorMap.put("netSaleCurrency", $rootScope.nls["ERR90320"]);

                    return false
                }

                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != $scope.charge.netSaleCurrency.id) {
                    if ($rootScope.baseCurrenyRate[$scope.charge.netSaleCurrency.id] == null) {
                        var st = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + " -> " + $scope.charge.netSaleCurrency.currencyCode;
                        console.log("Selected Currency does not have Rate of Exchange : " + st);
                        $scope.charge.netSaleCurrency = null;
                        $scope.errorMap.put("netSaleCurrency", $rootScope.nls["ERR90345"]);
                        return false
                    }
                }

            } else {}

        }


        if (validateCode == 0 || validateCode == 11) {
            if ($scope.charge.netSaleAmount != undefined &&
                $scope.charge.netSaleAmount != null &&
                $scope.charge.netSaleAmount != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.netSaleAmount))) {
                        $scope.errorMap.put("netSaleAmount", $rootScope.nls["ERR90322"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("netSaleAmount", $rootScope.nls["ERR90322"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.netSaleCurrency != null && $scope.charge.netSaleCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.netSaleAmount)) {
                    $scope.errorMap.put("netSaleAmount", $rootScope.nls["ERR90322"]);
                    return false;
                }

                if ($scope.charge.netSaleAmount == 0) {
                    $scope.errorMap.put("netSaleAmount", $rootScope.nls["ERR90321"]);
                    return false;
                } else {
                    if ($scope.charge.netSaleAmount < 0 || $scope.charge.netSaleAmount >= 99999999999.999) {
                        $scope.errorMap.put("netSaleAmount", $rootScope.nls["ERR90322"]);
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 12) {
            if ($scope.charge.netSaleMinimum != undefined &&
                $scope.charge.netSaleMinimum != null &&
                $scope.charge.netSaleMinimum != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.netSaleMinimum))) {
                        $scope.errorMap.put("netSaleMinimum", $rootScope.nls["ERR90324"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("netSaleMinimum", $rootScope.nls["ERR90324"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.netSaleCurrency != null && $scope.charge.netSaleCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.netSaleMinimum)) {
                    $scope.errorMap.put("netSaleMinimum", $rootScope.nls["ERR90324"]);
                    return false;
                }

                if ($scope.charge.netSaleMinimum == 0) {
                    $scope.errorMap.put("netSaleMinimum", $rootScope.nls["ERR90323"]);
                    return false;
                } else {
                    if ($scope.charge.netSaleMinimum < 0 || $scope.charge.netSaleMinimum >= 99999999999.999) {
                        $scope.errorMap.put("netSaleMinimum", $rootScope.nls["ERR90324"]);
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 13) {
            if ($scope.charge.declaredSaleCurrency != undefined &&
                $scope.charge.declaredSaleCurrency != null &&
                $scope.charge.declaredSaleCurrency.id != undefined &&
                $scope.charge.declaredSaleCurrency.id != null) {

                if (ValidateUtil.isStatusBlocked($scope.charge.declaredSaleCurrency.status)) {

                    $scope.charge.declaredSaleCurrency = null;

                    console.log("Selected declaredSaleCurrency is Blocked");

                    $scope.errorMap.put("declaredSaleCurrency", $rootScope.nls["ERR90325"]);

                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.charge.declaredSaleCurrency.status)) {

                    $scope.charge.declaredSaleCurrency = null;

                    console.log("Selected declaredSaleCurrency is Hidden");

                    $scope.errorMap.put("declaredSaleCurrency", $rootScope.nls["ERR90326"]);

                    return false
                }

                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != $scope.charge.declaredSaleCurrency.id) {
                    if ($rootScope.baseCurrenyRate[$scope.charge.declaredSaleCurrency.id] == null) {
                        var st = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + " -> " + $scope.charge.declaredSaleCurrency.currencyCode;
                        console.log("Selected Currency does not have Rate of Exchange : " + st);
                        $scope.charge.declaredSaleCurrency = null;
                        $scope.errorMap.put("declaredSaleCurrency", $rootScope.nls["ERR90345"]);
                        return false
                    }
                }

            } else {}

        }


        if (validateCode == 0 || validateCode == 14) {
            if ($scope.charge.declaredSaleAmount != undefined &&
                $scope.charge.declaredSaleAmount != null &&
                $scope.charge.declaredSaleAmount != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.declaredSaleAmount))) {
                        $scope.errorMap.put("declaredSaleAmount", $rootScope.nls["ERR90328"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("declaredSaleAmount", $rootScope.nls["ERR90328"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.declaredSaleCurrency != null && $scope.charge.declaredSaleCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.declaredSaleAmount)) {
                    $scope.errorMap.put("declaredSaleAmount", $rootScope.nls["ERR90328"]);
                    return false;
                }

                if ($scope.charge.declaredSaleAmount == 0) {
                    $scope.errorMap.put("declaredSaleAmount", $rootScope.nls["ERR90327"]);
                    return false;
                } else {
                    if ($scope.charge.declaredSaleAmount < 0 || $scope.charge.declaredSaleAmount >= 99999999999.999) {
                        $scope.errorMap.put("declaredSaleAmount", $rootScope.nls["ERR90328"]);
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 15) {
            if ($scope.charge.declaredSaleMinimum != undefined &&
                $scope.charge.declaredSaleMinimum != null &&
                $scope.charge.declaredSaleMinimum != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.declaredSaleMinimum))) {
                        $scope.errorMap.put("declaredSaleMinimum", $rootScope.nls["ERR90330"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("declaredSaleMinimum", $rootScope.nls["ERR90330"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.declaredSaleCurrency != null && $scope.charge.declaredSaleCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.declaredSaleMinimum)) {
                    $scope.errorMap.put("declaredSaleMinimum", $rootScope.nls["ERR90330"]);
                    return false;
                }

                if ($scope.charge.declaredSaleMinimum == 0) {
                    $scope.errorMap.put("declaredSaleMinimum", $rootScope.nls["ERR90329"]);
                    return false;
                } else {
                    if ($scope.charge.declaredSaleMinimum < 0 || $scope.charge.declaredSaleMinimum >= 99999999999.999) {
                        $scope.errorMap.put("declaredSaleMinimum", $rootScope.nls["ERR90330"]);
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 16) {
            if ($scope.charge.rateCurrency != undefined &&
                $scope.charge.rateCurrency != null &&
                $scope.charge.rateCurrency.id != undefined &&
                $scope.charge.rateCurrency.id != null) {

                if (ValidateUtil.isStatusBlocked($scope.charge.rateCurrency.status)) {

                    $scope.charge.rateCurrency = null;

                    console.log("Selected rateCurrency is Blocked");

                    $scope.errorMap.put("rateCurrency", $rootScope.nls["ERR90331"]);

                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.charge.rateCurrency.status)) {

                    $scope.charge.rateCurrency = null;

                    console.log("Selected rateCurrency is Hidden");

                    $scope.errorMap.put("rateCurrency", $rootScope.nls["ERR90332"]);

                    return false
                }

                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != $scope.charge.rateCurrency.id) {
                    if ($rootScope.baseCurrenyRate[$scope.charge.rateCurrency.id] == null) {
                        var st = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + " -> " + $scope.charge.rateCurrency.currencyCode;
                        console.log("Selected Currency does not have Rate of Exchange : " + st);
                        $scope.charge.rateCurrency = null;
                        $scope.errorMap.put("rateCurrency", $rootScope.nls["ERR90345"]);
                        return false
                    }
                }

            } else {}

        }


        if (validateCode == 0 || validateCode == 17) {
            if ($scope.charge.rateAmount != undefined &&
                $scope.charge.rateAmount != null &&
                $scope.charge.rateAmount != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.rateAmount))) {
                        $scope.errorMap.put("rateAmount", $rootScope.nls["ERR90334"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("rateAmount", $rootScope.nls["ERR90334"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.rateCurrency != null && $scope.charge.rateCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.rateAmount)) {
                    $scope.errorMap.put("rateAmount", $rootScope.nls["ERR90334"]);
                    return false;
                }

                if ($scope.charge.rateAmount == 0) {
                    $scope.errorMap.put("rateAmount", $rootScope.nls["ERR90333"]);
                    return false;
                } else {
                    if ($scope.charge.rateAmount < 0 || $scope.charge.rateAmount >= 99999999999.999) {
                        $scope.errorMap.put("rateAmount", $rootScope.nls["ERR90334"]);
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 18) {
            if ($scope.charge.rateMinimum != undefined &&
                $scope.charge.rateMinimum != null &&
                $scope.charge.rateMinimum != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.rateMinimum))) {
                        $scope.errorMap.put("rateMinimum", $rootScope.nls["ERR90336"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("rateMinimum", $rootScope.nls["ERR90336"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.rateCurrency != null && $scope.charge.rateCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.rateMinimum)) {
                    $scope.errorMap.put("rateMinimum", $rootScope.nls["ERR90336"]);
                    return false;
                }

                if ($scope.charge.rateMinimum == 0) {
                    $scope.errorMap.put("rateMinimum", $rootScope.nls["ERR90335"]);
                    return false;
                } else {
                    if ($scope.charge.rateMinimum < 0 || $scope.charge.rateMinimum >= 99999999999.999) {
                        $scope.errorMap.put("rateMinimum", $rootScope.nls["ERR90336"]);
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 19) {
            if ($scope.charge.costCurrency != undefined &&
                $scope.charge.costCurrency != null &&
                $scope.charge.costCurrency.id != undefined &&
                $scope.charge.costCurrency.id != null) {

                if (ValidateUtil.isStatusBlocked($scope.charge.costCurrency.status)) {

                    $scope.charge.costCurrency = null;

                    console.log("Selected costCurrency is Blocked");

                    $scope.errorMap.put("costCurrency", $rootScope.nls["ERR90337"]);

                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.charge.costCurrency.status)) {

                    $scope.charge.costCurrency = null;

                    console.log("Selected costCurrency is Hidden");

                    $scope.errorMap.put("costCurrency", $rootScope.nls["ERR90338"]);

                    return false
                }

                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != $scope.charge.costCurrency.id) {
                    if ($rootScope.baseCurrenyRate[$scope.charge.costCurrency.id] == null) {
                        var st = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + " -> " + $scope.charge.costCurrency.currencyCode;
                        console.log("Selected Currency does not have Rate of Exchange : " + st);
                        $scope.charge.costCurrency = null;
                        $scope.errorMap.put("costCurrency", $rootScope.nls["ERR90345"]);
                        return false
                    }
                }

            } else {}

        }


        if (validateCode == 0 || validateCode == 20) {
            if ($scope.charge.costAmount != undefined &&
                $scope.charge.costAmount != null &&
                $scope.charge.costAmount != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.costAmount))) {
                        $scope.errorMap.put("costAmount", $rootScope.nls["ERR90340"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("costAmount", $rootScope.nls["ERR90340"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.costCurrency != null && $scope.charge.costCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.costAmount)) {
                    $scope.errorMap.put("costAmount", $rootScope.nls["ERR90340"]);
                    return false;
                }

                if ($scope.charge.costAmount == 0) {
                    $scope.errorMap.put("costAmount", $rootScope.nls["ERR90339"]);
                    return false;
                } else {
                    if ($scope.charge.costAmount < 0 || $scope.charge.costAmount >= 99999999999.999) {
                        $scope.errorMap.put("costAmount", $rootScope.nls["ERR90340"]);
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 21) {
            if ($scope.charge.costMinimum != undefined &&
                $scope.charge.costMinimum != null &&
                $scope.charge.costMinimum != "") {

                try {
                    if (isNaN(parseFloat($scope.charge.costMinimum))) {
                        $scope.errorMap.put("costMinimum", $rootScope.nls["ERR90342"]);
                        return false;
                    }
                } catch (err) {
                    $scope.errorMap.put("costMinimum", $rootScope.nls["ERR90342"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,3})$");

                if ($scope.charge.costCurrency != null && $scope.charge.costCurrency.decimalPoint == "2") {
                    regexp = new RegExp("^[0-9]{0,11}(.[0-9]{0,2})$");
                }

                if (!regexp.test($scope.charge.costMinimum)) {
                    $scope.errorMap.put("costMinimum", $rootScope.nls["ERR90342"]);
                    return false;
                }

                if ($scope.charge.costMinimum == 0) {
                    $scope.errorMap.put("costMinimum", $rootScope.nls["ERR90341"]);
                    return false;
                } else {
                    if ($scope.charge.costMinimum < 0 || $scope.charge.costMinimum >= 99999999999.999) {
                        $scope.errorMap.put("costMinimum", $rootScope.nls["ERR90342"]);
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 22) {

            if ($scope.charge.vendor == undefined ||
                $scope.charge.vendor == null ||
                $scope.charge.vendor == "" ||
                $scope.charge.vendor.id == null) {


            } else if ($scope.charge.vendor.status != null) {
                if (ValidateUtil.isStatusBlocked($scope.charge.vendor.status)) {
                    console.log($rootScope.nls["ERR90343"]);
                    $scope.errorMap.put("chargeVendor", $rootScope.nls["ERR90343"]);
                    $scope.charge.vendor = null;
                    return false

                }

                if (ValidateUtil.isStatusHidden($scope.charge.vendor.status)) {
                    console.log($rootScope.nls["ERR90344"]);
                    $scope.errorMap.put("chargeVendor", $rootScope.nls["ERR90344"]);
                    $scope.charge.vendor = null;
                    return false

                }
            }
        }

        return true;

    }

    /* Rate Tab validation Ends */



    /*Rate Tab ends*/


    /*connection tab starts*/

    $scope.connPolConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "portName",
            seperator: false
        }, {
            "title": "portCode",
            seperator: true
        }]
    }

    $scope.showConPol = function(name) {
        if ($scope.serviceConnection.move == undefined || $scope.serviceConnection.move == null || $scope.serviceConnection.move == '') {
            console.log($rootScope.nls["ERR90031"]);
            $scope.errorMap.put("connection.move", $rootScope.nls["ERR90031"]);
            return false;
        }
        console.log("Showing List of Port Of Loading.....");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Port Of Loading";
        $scope.ajaxConnPolEvent(null);
    };

    $scope.ajaxConnPolEvent = function(object) {
        console.log("ajaxPortOfLoadingEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        PortByTransportMode.fetch({
            "transportMode": $scope.serviceConnection.move
        }, $scope.searchDto).$promise.then(
            function(data) {


                if (data.responseCode == "ERR0") {
                    $scope.connPolList = [];
                    var resultList = data.responseObject.searchResult;
                    if (resultList != null && resultList.length != 0) {
                        if ($scope.serviceConnection.pod == undefined || $scope.serviceConnection.pod == null) {
                            $scope.connPolList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != $scope.serviceConnection.pod.id) {
                                    $scope.connPolList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    $scope.showConnPol = true;
                }



            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );
    }

    $scope.selectedConnPol = function(obj) {
        console.log("Selected PortOfLoading  : ", obj);
        $scope.serviceConnection.pol = obj;
        $scope.validateServiceConnection(3);
        $scope.cancelConnPol();
        document.getElementById('connPol').focus();
    };

    $scope.cancelConnPol = function() {
        console.log("PortOfLoading List Cancelled....");
        $scope.showConnPol = false;
    }



    /* Connection Port Of Discharge - List Picker */

    $scope.connPodConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "portName",
            seperator: false
        }, {
            "title": "portCode",
            seperator: true
        }]
    }


    $scope.serviceToConsol = function(shipment, service) {
        if (attachToMasterService.serviceToConsol(shipment, service)) {
            $scope.isCreateConsol = 'CONSOL';
            $scope.createConsolServiceId = service.id;
            $scope.updateShipment();

        }
    }



    $scope.showConPod = function(name) {
        if ($scope.serviceConnection.move == undefined || $scope.serviceConnection.move == null || $scope.serviceConnection.move == '') {
            $scope.errorMap.put("connection.move", $rootScope.nls["ERR90031"]);
            return false;
        }
        console.log("Showing List of Port Of Discharge.....");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Port Of Discharge";
        $scope.ajaxConnPodEvent(null);
    };

    $scope.ajaxConnPodEvent = function(object) {
        console.log("ajaxPortOfLoadingEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        PortByTransportMode.fetch({
            "transportMode": $scope.serviceConnection.move
        }, $scope.searchDto).$promise.then(
            function(data) {


                if (data.responseCode == "ERR0") {
                    $scope.connPodList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if ($scope.serviceConnection.pol == undefined || $scope.serviceConnection.pol == null) {
                            $scope.connPodList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != $scope.serviceConnection.pol.id) {
                                    $scope.connPodList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    $scope.showConnPod = true;
                }



            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );
    }

    $scope.selectedConnPod = function(obj) {
        $scope.serviceConnection.pod = obj;
        $scope.validateServiceConnection(4);
        $scope.cancelConnPod();
        document.getElementById('connPod').focus();
    };

    $scope.cancelConnPod = function() {
        console.log("PortOfDischarging List Cancelled....");
        $scope.showConnPod = false;
    }


    $scope.showConnCarrierList = false;

    $scope.connCarrierlistConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "carrierName",
            seperator: false
        }, {
            "title": "carrierCode",
            seperator: true
        }]
    }

    $scope.showConCarrier = function(name) {

        console.log("showing list of carriers......");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Carrier";
        $scope.ajaxConnCarrierEvent(null);
    };

    $scope.ajaxConnCarrierEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        CarrierByTransportMode.fetch({
            "transportMode": $scope.serviceConnection.move
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.connCarrierList = data.responseObject.searchResult;
                    $scope.showConnCarrierList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air');
            }
        );
    }

    $scope.selectedConnCarrier = function(obj) {
        $scope.serviceConnection.carrierMaster = obj;
        $scope.validateServiceConnection(7);
        $scope.cancelConnCarrier();
        document.getElementById('connCarrier').focus();
    };

    $scope.cancelConnCarrier = function() {
        $scope.showConnCarrierList = false;
    }


    //ADD/EDIT/DELETE CONNECTION
    /*    $scope.addserviceConnection=function(){
     	   $scope.addConnection=false;
     	   $scope.addConnectionShow = false;
        		$scope.serviceConnection={};
        		$scope.errorMap = new Map();
        		$scope.serviceConnection.connectionStatus="Planned";
        		$scope.editServiceConnectionIndex = null;
        		discardService1.set($scope.serviceConnection);
        }*/

    /* $scope.saveServiceConnection=function(){
	                if($scope.validateServiceConnection(0))	{
	                	if ($scope.editServiceConnectionIndex == null) {
              				if ($scope.shipmentServiceDetail.connectionList == null)
              					$scope.shipmentServiceDetail.connectionList = [];
              				$scope.shipmentServiceDetail.connectionList.push($scope.serviceConnection);
              			 $scope.addConnectionShow = true;
               			$scope.addConnection=true;
              				}
              			else {
              				$scope.shipmentServiceDetail.connectionList
              						.splice($scope.editServiceConnectionIndex, 1,$scope.serviceConnection);
              			 $scope.addConnectionShow = true;
            			$scope.addConnection=true;
              			}
              		} else {

              			console.log("Connection Validation failed");
              		}

	                }*/

    /*$scope.discardServiceConnection=function(){

    	   if(!discardService1.compare($scope.serviceConnection)){
    			ngDialog.openConfirm({
    				template: '<p>Do you want to update your changes?</p>' +
    				'<div class="ngdialog-footer">' +
    					'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
    					'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
    				'</div>',
    				plain: true,
    				className: 'ngdialog-theme-default'
    			}).then(function (value) {
    				if(value == 1 ) {
    					$scope.saveServiceConnection();
    				}else{
    					 $scope.addConnectionShow = true;
             			$scope.addConnection=true;
    					$scope.editServiceConnectionIndex=null;
    				}
    				});

    		}else{

    			 $scope.addConnectionShow = true;
      			$scope.addConnection=true;
    			$scope.editServiceConnectionIndex=null;
    		}
    }*/
    /* $scope.editServiceConnection = function(index) {
	 $scope.editServiceConnectionIndex=index;
		$scope.errorMap = new Map();
		$scope.showShipmentDimension=true;
		 $scope.addConnectionShow=false;
		$scope.serviceConnection=cloneService.clone($scope.shipmentServiceDetail.connectionList[index]);
		discardService1.set($scope.serviceConnection);
	}*/


    /*$scope.deleteServiceConnection = function(index) {
    	 ngDialog.openConfirm({
        template:
            '<p>Are you sure you want to delete selected Connection?</p>' +
            '<div class="ngdialog-footer">' +
              '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
              '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
            '</button>' +
    			   '</div>',
        plain: true,
        className: 'ngdialog-theme-default'
    }).then(function (value) {
    	 $scope.shipmentServiceDetail.connectionList.splice(index, 1);
      }, function (value) {
          console.log("Dimension Deletion cancelled");
      });

    }*/


    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }


    //Validation for COnnection//


    $scope.validateServiceConnection = function(validateCode) {


        $scope.errorMap = new Map();
        if (validateCode == 0 || validateCode == 1) {

            if ($scope.serviceConnection.move == undefined || $scope.serviceConnection.move == null ||
                $scope.serviceConnection.move == null ||
                $scope.serviceConnection.move == "") {
                $scope.errorMap.put("conn.move", $rootScope.nls["ERR90500"]);
                return false;
            }


        }
        if (validateCode == 0 || validateCode == 2) {

            if ($scope.serviceConnection.connectionStatus == undefined || $scope.serviceConnection.connectionStatus == null ||
                $scope.serviceConnection.connectionStatus == null ||
                $scope.serviceConnection.connectionStatus == "") {
                $scope.errorMap.put("connection.move", $rootScope.nls["ERR90501"]);
                return false;
            }


        }
        if (validateCode == 0 || validateCode == 3) {

            if ($scope.serviceConnection.pol == undefined || $scope.serviceConnection.pol == null ||
                $scope.serviceConnection.pol.id == null ||
                $scope.serviceConnection.pol.id.trim == "") {
                $scope.errorMap.put("connection.pol", $rootScope.nls["ERR90502"]);
                return false;
            }

            if (ValidateUtil.isStatusBlocked($scope.serviceConnection.pol.status)) {
                $scope.serviceConnection.pol = null;
                $scope.errorMap.put("connection.pol", $rootScope.nls["ERR90503"]);
                return false;
            }

            if (ValidateUtil.isStatusHidden($scope.serviceConnection.pol.status)) {
                $scope.serviceConnection.pol = null;
                $scope.errorMap.put("connection.pol", $rootScope.nls["ERR90504"]);
                return false;
            }

        }
        if (validateCode == 0 || validateCode == 4) {

            if ($scope.serviceConnection.pod == undefined || $scope.serviceConnection.pod == null ||
                $scope.serviceConnection.pod.id == null ||
                $scope.serviceConnection.pod.id.trim == "") {
                $scope.errorMap.put("connection.pod", $rootScope.nls["ERR90505"]);
                return false;
            }

            if (ValidateUtil.isStatusBlocked($scope.serviceConnection.pod.status)) {
                $scope.serviceConnection.pod = null;
                $scope.errorMap.put("connection.pod", $rootScope.nls["ERR90506"]);
                return false;
            }

            if (ValidateUtil.isStatusHidden($scope.serviceConnection.pod.status)) {
                $scope.serviceConnection.pod = null;
                $scope.errorMap.put("connection.pod", $rootScope.nls["ERR90507"]);
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 5) {
            if ($scope.serviceConnection.etd == undefined ||
                $scope.serviceConnection.etd == null ||
                $scope.serviceConnection.etd == "") {
                $scope.errorMap.put("connection.etd", $rootScope.nls["ERR90509"]);
                return false;
            }

            if ($scope.serviceConnection.eta != undefined && $scope.serviceConnection.eta != null) {

                if ($scope.serviceConnection.etd > $scope.serviceConnection.eta) {
                    $scope.errorMap.put("connection.etd", $rootScope.nls["ERR90510"]);
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 6) {

            if ($scope.serviceConnection.eta == undefined ||
                $scope.serviceConnection.eta == null ||
                $scope.serviceConnection.eta == "") {
                $scope.errorMap.put("connection.eta", $rootScope.nls["ERR90508"]);
                return false;
            }

            if ($scope.serviceConnection.etd != undefined && $scope.serviceConnection.etd != null) {


                if ($scope.serviceConnection.etd > $scope.serviceConnection.eta) {
                    $scope.errorMap.put("connection.eta", $rootScope.nls["ERR90510"]);
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 7) {

            if ($scope.serviceConnection.carrierMaster == undefined || $scope.serviceConnection.carrierMaster == null ||
                $scope.serviceConnection.carrierMaster.id == null ||
                $scope.serviceConnection.carrierMaster.id.trim == "") {
                $scope.errorMap.put("connection.carrierMaster", $rootScope.nls["ERR90511"]);
                return false;
            }

            if (ValidateUtil.isStatusBlocked($scope.serviceConnection.carrierMaster.status)) {
                $scope.serviceConnection.carrierMaster = null;
                $scope.errorMap.put("connection.carrierMaster", $rootScope.nls["ERR90512"]);
                return false;
            }

            if (ValidateUtil.isStatusHidden($scope.serviceConnection.carrierMaster.status)) {
                $scope.serviceConnection.carrierMaster = null;
                $scope.errorMap.put("connection.carrierMaster", $rootScope.nls["ERR90513"]);
                return false;
            }


        }
        if (validateCode == 0 || validateCode == 8) {

            if ($scope.serviceConnection.flightVoyageNo == undefined ||
                $scope.serviceConnection.flightVoyageNo == null ||
                $scope.serviceConnection.flightVoyageNo == "") {
                $scope.errorMap.put("connection.carrierOrVoyageN", $rootScope.nls["ERR90514"]);
                return false;
            }

            var regexp = new RegExp("[A-Za-z0-9 ]{0,10}");
            if (!regexp.test($scope.serviceConnection.flightVoyageNo)) {
                $scope.errorMap.put("connection.carrierOrVoyageN", $rootScope.nls["ERR90515"]);
                return false;
            }

        }


        return true;
    }

    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }


    $scope.changeConnMove = function() {
        $scope.errorMap = new Map();
        if ($scope.serviceConnection.move == "Air") {

            $scope.serviceConnection.pol = {};
            $scope.serviceConnection.pod = {};
            $scope.serviceConnection.carrierMaster = {};

        }

        if ($scope.serviceConnection.move == "Ocean") {


            $scope.serviceConnection.pol = {};
            $scope.serviceConnection.pod = {};
            $scope.serviceConnection.carrierMaster = {};

        }

        if ($scope.serviceConnection.move == "Road") {

            $scope.serviceConnection.pol = {};
            $scope.serviceConnection.pod = {};

            $scope.serviceConnection.carrierMaster = {};

        }
        if ($scope.serviceConnection.move == "Rail") {

            $scope.serviceConnection.pol = {};
            $scope.serviceConnection.pod = {};
            $scope.serviceConnection.carrierMaster = {};


        }

    }

    /*connection tab end*/


    /* Status update start*/

    /* Status update start*/

    /*ssignedTo list */
    $scope.listConfigAssignedTo = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "employeeName",
            seperator: false
        }, {
            "title": "employeeCode",
            seperator: true
        }]
    }
    $scope.showAssignedTo = function(customer) {
        $scope.panelTitle = "Assigned To";
        $scope.selectedItem = -1;
        $scope.ajaxAssignedToEvent(null);

    };

    $scope.ajaxAssignedToEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        EmployeeList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.assignedToList = data.responseObject.searchResult;
                    $scope.showAssignedToList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Customer Services');
            }
        );
    }
    $scope.selectedAssignedTo = function(assignedToObj) {
        console.log("Selected assignedTo", assignedToObj);
        $scope.shipmentServiceTrigger.employeeMaster = assignedToObj;
        $scope.cancelAssignedTo();
        $scope.validateShipmentServiceTrigger(3);
        document.getElementById('shipmentBookingPersion').focus();
    };
    $scope.cancelAssignedTo = function() {
        console.log("Cancel customer service List Button Pressed....");
        $scope.searchText = "";
        $scope.showAssignedToList = false;
    };


    /*Trigger Type */

    /*Trigger Type Master list */
    $scope.listConfigTriggerTypeMaster = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "triggerTypeName",
            seperator: false
        }, {
            "title": "triggerTypeCode",
            seperator: true
        }]
    }
    $scope.showTriggerTypeMasters = function(customer) {
        $scope.panelTitle = "Trigger Type";
        $scope.selectedItem = -1;
        $scope.ajaxTriggerTypeMasterEvent(null);

    };

    $scope.ajaxTriggerTypeMasterEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        TriggerTypeSearchKeyword.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.triggerTypeMasterList = data.responseObject.searchResult;
                    $scope.showTriggerTypeMasterList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Customer Services');
            }
        );
    }
    $scope.selectedTriggerTypeMaster = function(triggerTypeMasterObj) {
        console.log("Selected triggerTypeMaster", triggerTypeMasterObj);
        $scope.shipmentServiceTrigger.triggerTypeMaster = triggerTypeMasterObj;
        $scope.cancelTriggerTypeMaster();
        $scope.validateShipmentServiceTrigger(1);
        document.getElementById('triggerType').focus();
    };
    $scope.cancelTriggerTypeMaster = function() {
        console.log("Cancel Trigger Type Master List Button Pressed....");
        $scope.searchText = "";
        $scope.showTriggerTypeMasterList = false;
    };

    /*Trigger Type ends*/

    /*Trigger Master list */


    //Based on the type the trigger master values will fetch--$scope.shipmentServiceTrigger.triggerTypeMaster//

    $scope.listConfigTriggerMaster = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "triggerName",
            seperator: false
        }, {
            "title": "triggerCode",
            seperator: true
        }]
    }
    $scope.showTriggerMasters = function(customer) {
        $scope.panelTitle = "Trigger Name";
        $scope.selectedItem = -1;
        $scope.ajaxTriggerMasterEvent(null);

    };


    $scope.ajaxTriggerMasterEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        if ($scope.shipmentServiceTrigger.triggerTypeMaster.triggerTypeName != undefined && $scope.shipmentServiceTrigger.triggerTypeMaster.triggerTypeName != null && $scope.shipmentServiceTrigger.triggerTypeMaster.triggerTypeName != "") {
            TriggerSearchKeyword.fetch({
                "triggerType": $scope.shipmentServiceTrigger.triggerTypeMaster.triggerTypeName
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.triggerMasterList = data.responseObject.searchResult;
                        $scope.showTriggerMasterList = true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Customer Services');
                }
            );
        } else {
            $scope.validateShipmentServiceTrigger(1);
        }
    }




    $scope.selectedTriggerMaster = function(triggerMasterObj) {
        console.log("Selected triggerMaster", triggerMasterObj);
        $scope.shipmentServiceTrigger.triggerMaster = triggerMasterObj;
        $scope.cancelTriggerMaster();
        $scope.validateShipmentServiceTrigger(2);
        $scope.shipmentServiceTrigger.note = $scope.shipmentServiceTrigger.triggerMaster.note;
        document.getElementById('trigger').focus();
    };
    $scope.cancelTriggerMaster = function() {
        console.log("Cancel Trigger Master List Button Pressed....");
        $scope.searchText = "";
        $scope.showTriggerMasterList = false;
    };

    /* Trigger Master ends*/


    $scope.addStatusUpdateRow = function() {
        $scope.errorMap = new Map();
        //$scope.addStatusUpdate = false;
        $scope.addStatusShow = false;
        $scope.statusButtonShow = false;
        $scope.editStatusUpdatesIndex = null;
        $scope.firstFocusToStatusUpdates = true;
        $scope.statusUpdateDetail = {};
        $scope.shipmentServiceTrigger = {};
        $scope.shipmentServiceTrigger.triggerMaster = {};
        $scope.shipmentServiceTrigger.triggerTypeMaster = {};
        $scope.shipmentServiceTrigger.employeeMaster = {};
        $scope.shipmentServiceTrigger.followUpRequired = false;
        $scope.shipmentServiceTrigger.protect = false;
        $scope.shipmentServiceTrigger.completed = false;
        $scope.shipmentServiceTrigger.date = new Date();
        //$scope.addStatusUpdateShow=false;

        discardService1.set($scope.shipmentServiceTrigger);
    };



    $scope.saveStatus = function() {


        if ($scope.validateShipmentServiceTrigger(0)) {
            if ($scope.editStatusUpdatesIndex == null) {
                if ($scope.shipmentServiceDetail.shipmentServiceTriggerList == null)
                    $scope.shipmentServiceDetail.shipmentServiceTriggerList = [];
                $scope.shipmentServiceDetail.shipmentServiceTriggerList.push($scope.shipmentServiceTrigger);
                $scope.shipmentServiceTrigger.followUpRequired = $scope.shipmentServiceTrigger.followUpRequired == false ? 'No' : 'Yes';
                $scope.shipmentServiceTrigger.protect = $scope.shipmentServiceTrigger.protect == false ? 'No' : 'Yes';
                $scope.shipmentServiceTrigger.completed = $scope.shipmentServiceTrigger.completed == false ? 'No' : 'Yes';

            } else {
                $scope.shipmentServiceDetail.shipmentServiceTriggerList
                    .splice($scope.editStatusUpdatesIndex, 1, $scope.shipmentServiceTrigger);
                $scope.shipmentServiceTrigger.followUpRequired = $scope.shipmentServiceTrigger.followUpRequired == false ? 'No' : 'Yes';
                $scope.shipmentServiceTrigger.protect = $scope.shipmentServiceTrigger.protect == false ? 'No' : 'Yes';
                $scope.shipmentServiceTrigger.completed = $scope.shipmentServiceTrigger.completed == false ? 'No' : 'Yes';
            }
            $scope.addStatusShow = true;
            $scope.statusButtonShow = true;
        } else {

            console.log("StatusUpdate Validation Failed");
        }


    }



    $scope.deleteStatus = function(index) {
        ngDialog.openConfirm({
            template: '<p>Are you sure you want to delete selected status update  ?</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +

                '</button>' +
                '</div>',
            plain: true,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            $scope.shipmentServiceDetail.shipmentServiceTriggerList.splice(index, 1);
        }, function(value) {
            console.log("Shipment StatusUpdate Deletion cancelled");
        });
    }



    $scope.discardStatus = function() {

        if (!discardService1.compare($scope.shipmentServiceTrigger)) {
            ngDialog.openConfirm({
                template: '<p>Do you want to update your changes?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveStatus();
                } else {
                    $scope.addStatusShow = true;
                    $scope.statusButtonShow = true;
                }
            });


        } else {
            $scope.addStatusShow = true;
            $scope.statusButtonShow = true;
        }

    }

    $scope.dummyFunction = function() {
        //Its used for weight calculation
    }
    $scope.changeShipmentServiceDimension = function(serviceDetail) {
        if (serviceDetail.dimensionUnit == false) {

            serviceDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];

            if (serviceDetail.bookedVolumeWeightUnitKg != undefined &&
                serviceDetail.bookedVolumeWeightUnitKg != null &&
                serviceDetail.bookedVolumeWeightUnitKg != '') {
                serviceDetail.bookedChargeableUnit = serviceDetail.bookedVolumeWeightUnitKg;
                serviceDetail.bookedVolumeWeightUnitPound = $rootScope.roundOffWeight(Math.round((serviceDetail.bookedVolumeWeightUnitKg * 2.20462) * 100) / 100);
            }

            if (serviceDetail.bookedGrossWeightUnitKg != undefined &&
                serviceDetail.bookedGrossWeightUnitKg != null &&
                serviceDetail.bookedGrossWeightUnitKg != '') {
                serviceDetail.bookedChargeableUnit = serviceDetail.bookedGrossWeightUnitKg;
                serviceDetail.bookedGrossWeightUnitPound = $rootScope.roundOffWeight(Math.round((serviceDetail.bookedGrossWeightUnitKg * 2.20462) * 100) / 100);
            }

        } else {
            serviceDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];

            if (serviceDetail.bookedVolumeWeightUnitPound != undefined &&
                serviceDetail.bookedVolumeWeightUnitPound != null &&
                serviceDetail.bookedVolumeWeightUnitPound != '') {

                serviceDetail.bookedVolumeWeightUnitKg = $rootScope.roundOffWeight(Math.round((serviceDetail.bookedVolumeWeightUnitPound * 0.45359237) * 100) / 100);
                serviceDetail.bookedChargeableUnit = serviceDetail.bookedVolumeWeightUnitKg;

            }

            if (serviceDetail.bookedGrossWeightUnitPound != undefined &&
                serviceDetail.bookedGrossWeightUnitPound != null &&
                serviceDetail.bookedGrossWeightUnitPound != '') {

                serviceDetail.bookedGrossWeightUnitKg = $rootScope.roundOffWeight(Math.round((serviceDetail.bookedGrossWeightUnitPound * 0.45359237) * 100) / 100);
                serviceDetail.bookedChargeableUnit = serviceDetail.bookedGrossWeightUnitKg;
            }

        }

        if (+(serviceDetail.bookedVolumeWeightUnitKg) > +(serviceDetail.bookedGrossWeightUnitKg)) {
            serviceDetail.bookedChargeableUnit = serviceDetail.bookedVolumeWeightUnitKg;
        } else {
            serviceDetail.bookedChargeableUnit = serviceDetail.bookedGrossWeightUnitKg;
        }

    }




    //Validate ShipmentServiceTrigger//
    $scope.validateShipmentServiceTrigger = function(validate) {


        $scope.firstFocusToStatusUpdate = false;
        $scope.errorMap = new Map();


        if (validate == 0 || validate == 1) {

            if ($scope.shipmentServiceTrigger.triggerTypeMaster == undefined ||
                $scope.shipmentServiceTrigger.triggerTypeMaster == null ||
                $scope.shipmentServiceTrigger.triggerTypeMaster == "" ||
                $scope.shipmentServiceTrigger.triggerTypeMaster.id == null) {
                console.log($rootScope.nls["ERR90400"]);
                $scope.errorMap.put("triggerType", $rootScope.nls["ERR90400"]);
                return false;

            } else if ($scope.shipmentServiceTrigger.triggerTypeMaster.status != null) {
                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceTrigger.triggerTypeMaster.status)) {
                    console.log($rootScope.nls["ERR90401"]);
                    $scope.errorMap.put("triggerType", $rootScope.nls["ERR90401"]);
                    $scope.shipmentServiceTrigger.triggerTypeMaster = null;
                    $scope.shipmentServiceTrigger.triggerMaster = null;
                    return false

                }

                if (ValidateUtil.isStatusHidden($scope.shipmentServiceTrigger.triggerTypeMaster.status)) {
                    console.log($rootScope.nls["ERR90402"]);
                    $scope.errorMap.put("triggerType", $rootScope.nls["ERR90402"]);
                    $scope.hipmentServiceTrigger.triggerTypeMaster = null;
                    $scope.shipmentServiceTrigger.triggerMaster = null;
                    return false

                }
            }
        }

        if (validate == 0 || validate == 2) {

            if ($scope.shipmentServiceTrigger.triggerMaster == undefined ||
                $scope.shipmentServiceTrigger.triggerMaster == null ||
                $scope.shipmentServiceTrigger.triggerMaster == "" ||
                $scope.shipmentServiceTrigger.triggerMaster.id == null) {
                console.log($rootScope.nls["ERR90410"]);
                $scope.errorMap.put("trigger", $rootScope.nls["ERR90410"]);
                return false;

            } else if ($scope.shipmentServiceTrigger.triggerMaster.status != null) {
                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceTrigger.triggerMaster.status)) {
                    console.log($rootScope.nls["ERR90411"]);
                    $scope.errorMap.put("trigger", $rootScope.nls["ERR90411"]);
                    $scope.shipmentServiceTrigger.triggerMaster = null;
                    $scope.shipmentServiceTrigger.note = null;
                    return false

                }

                if (ValidateUtil.isStatusHidden($scope.shipmentServiceTrigger.triggerMaster.status)) {
                    console.log($rootScope.nls["ERR90412"]);
                    $scope.errorMap.put("trigger", $rootScope.nls["ERR90412"]);
                    $scope.shipmentServiceTrigger.triggerMaster = null;
                    $scope.shipmentServiceTrigger.note = null;
                    return false

                }
            }
        }


        if (validate == 0 || validate == 3) {
            if ($scope.shipmentServiceTrigger.followUpRequired == 'Yes' || $scope.shipmentServiceTrigger.followUpRequired == true) {

                if ($scope.shipmentServiceTrigger.employeeMaster == undefined ||
                    $scope.shipmentServiceTrigger.employeeMaster == null ||
                    $scope.shipmentServiceTrigger.employeeMaster == "" ||
                    $scope.shipmentServiceTrigger.employeeMaster.id == null) {
                    console.log($rootScope.nls["ERR90410"]);
                    $scope.errorMap.put("triggeremployeeName", $rootScope.nls["ERR90420"]);
                    return false;

                } else if ($scope.shipmentServiceTrigger.employeeMaster.status != null) {
                    if (ValidateUtil.isEmployeeResigned($scope.shipmentServiceTrigger.employeeMaster.employementStatus)) {
                        console.log($rootScope.nls["ERR90411"]);
                        $scope.errorMap.put("triggeremployeeName", $rootScope.nls["ERR90421"]);
                        $scope.shipmentServiceTrigger.triggerMaster = null;
                        return false
                    }

                    if (ValidateUtil.isEmployeeTerminated($scope.shipmentServiceTrigger.employeeMaster.employementStatus)) {
                        console.log($rootScope.nls["ERR90412"]);
                        $scope.errorMap.put("triggeremployeeName", $rootScope.nls["ERR90422"]);
                        $scope.shipmentServiceTrigger.triggerMaster = null;
                        return false

                    }
                }
            }
        }

        if (validate == 0 || validate == 4) {

            if ($scope.shipmentServiceTrigger.followUpRequired == 'Yes' || $scope.shipmentServiceTrigger.followUpRequired == true) {

                if ($scope.shipmentServiceTrigger.followUpDate == undefined ||
                    $scope.shipmentServiceTrigger.followUpDate == null ||
                    $scope.shipmentServiceTrigger.followUpDate == ""
                ) {
                    console.log($rootScope.nls["ERR90410"]);
                    $scope.errorMap.put("followUpDate", $rootScope.nls["ERR90430"]);
                    return false;

                }
            }
        }

        //Notes
        if (validate == 0 || validate == 5) {
            if ($scope.shipmentServiceTrigger.note == undefined || $scope.shipmentServiceTrigger.note == "" || $scope.shipmentServiceTrigger.note == null) {
                $scope.errorMap.put("statusUpdate.notes", $rootScope.nls["ERR90440"]);
                return false;
            }

            if ($scope.shipmentServiceTrigger.note != undefined && $scope.shipmentServiceTrigger.note != "" && $scope.shipmentServiceTrigger.note != null) {
                var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,500}");
                if (!regexp.test($scope.shipmentServiceTrigger.note)) {
                    $scope.errorMap.put("statusUpdate.notes", $rootScope.nls["ERR90441"]);
                    return false;
                }
            }

        }

        return true;
    }




    $scope.shipmentTab = "general";

    $scope.downloadAttach = function(param) {
        console.log("download ", param);

        if (param.data.id != null && (param.data.protectedFile == null || param.data.unprotectedFile == null)) {
            console.log("API CALL")
                //var requestData = {attachmentId:param.data.id, attachmentType : param.type,}
            $http({
                url: $rootScope.baseURL + '/api/v1/shipment/files/' + param.data.id + '/' + param.type + '/' + param.data.swapped,
                method: "GET",
                responseType: 'arraybuffer'
            }).success(function(data, status, headers, config) {
                console.log("hiddenElement ", data)
                var blob = new Blob([data], {});
                console.log("blob ", blob);
                if (param.type == 'unprotectedFileName')
                    saveAs(blob, param.data.unprotectedFileName);

                if (param.type == 'protectedFileName')
                    saveAs(blob, param.data.protectedFileName);

            }).error(function(data, status, headers, config) {
                //upload failed
            });


        } else {
            console.log("NO API CALL", param);
            if (param.type == "unprotectedFileName")
                saveAs(param.data.tmpFile2, param.data.unprotectedFileName);
            else if (param.type == "protectedFileName")
                saveAs(param.data.tmpFile1, param.data.protectedFileName);
        }
    }


    /* Shipment Attachment Ends... */

    $scope.reportModal = function() {
        //$scope.reportPopUPSpinner=false;
        //$scope.$emit('$destroy',{"state":true});
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/shipment/views/report_popup.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
    };
    $scope.reportModalWithEstimation = function() {
        //$scope.reportPopUPSpinner=false;
        //$scope.$emit('$destroy',{"state":true});
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/shipment/views/report_popup_with_estimation.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
    };



    $scope.validEnquiryDimension = function(param, documentDetail) {

        console.log("table state data", param);

        documentDetail.shipmentDimensionValid = param;
    }

    $scope.calculateWeight = function(param, documentDetail, service) {
        attachToMasterService.calculateWeight(param, documentDetail, service);
    }

    //Event tab started.

    /* show event list*/

    $scope.showEventMasterList = false;
    $scope.eventMasterlistConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "eventName",
            seperator: false
        }, {
            "title": "eventCode",
            seperator: true
        }]
    }

    $scope.showEventMaster = function(name) {

        console.log("Showing List of Event..");
        $scope.selectedItem = -1;
        $scope.panelTitle = "Event";
        $scope.ajaxEventMasterEvent(null);
    };

    $scope.ajaxEventMasterEvent = function(object) {
        console.log("ajaxEventMasterEvent ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        EventSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.eventMasterList = data.responseObject.searchResult;
                    $scope.showEventMasterList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Event');
            }
        );
    }

    $scope.selectedEventMaster = function(obj) {

        $scope.shipmentServiceEvent.eventMaster = obj;
        $scope.validateShipmentEvent(1);
        $scope.cancelEventMaster();
        document.getElementById('shipmentEventMaster').focus();
    };

    $scope.cancelEventMaster = function() {
        $scope.showEventMasterList = false;
    }


    /*ssignedTo list */
    $scope.listConfigEventAssignedTo = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
            "title": "employeeName",
            seperator: false
        }, {
            "title": "employeeCode",
            seperator: true
        }]
    }
    $scope.showEventAssignedTo = function(customer) {
        $scope.panelTitle = "Assigned To";
        $scope.selectedItem = -1;
        $scope.ajaxEventAssignedToEvent(null);

    };

    $scope.ajaxEventAssignedToEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        EmployeeList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.eventAssignedToList = data.responseObject.searchResult;
                    $scope.showEventAssignedToList = true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Customer Services');
            }
        );
    }
    $scope.selectedEventAssignedTo = function(assignedToObj) {
        console.log("Selected assignedTo", assignedToObj);
        $scope.shipmentServiceEvent.assignedTo = assignedToObj;
        $scope.cancelEventAssignedTo();
        $scope.validateShipmentEvent(3);
        document.getElementById('shipmentEventAssignedTo').focus();
    };
    $scope.cancelEventAssignedTo = function() {
        console.log("Cancel Event AssignedTo List Button Pressed....");
        $scope.searchText = "";
        $scope.showEventAssignedToList = false;
    };




    $scope.saveEvent = function() {


        if ($scope.validateShipmentEvent(0)) {
            if ($scope.editEventIndex == null) {
                if ($scope.shipmentServiceDetail.eventList == null)
                    $scope.shipmentServiceDetail.eventList = [];
                $scope.shipmentServiceEvent.isCompleted = 'No';
                $scope.shipmentServiceEvent.followUpRequired = $scope.shipmentServiceEvent.followUpRequired == false ? 'No' : 'Yes';
                $scope.shipmentServiceDetail.eventList.push($scope.shipmentServiceEvent);



            } else {
                $scope.shipmentServiceEvent.isCompleted = $scope.shipmentServiceEvent.isCompleted == false ? 'No' : 'Yes';
                $scope.shipmentServiceEvent.followUpRequired = $scope.shipmentServiceEvent.followUpRequired == false ? 'No' : 'Yes';
                $scope.shipmentServiceDetail.eventList
                    .splice($scope.editEventIndex, 1, $scope.shipmentServiceEvent);



            }
            $scope.cancelEvent();
        } else {

            console.log("Shipment event Validation Failed");
        }


    }




    // Add Event row .
    $scope.addEventRow = function() {
        $scope.addEvent = false;
        $scope.addEventShow = false;
        $scope.shipmentServiceEvent = {};
        $scope.shipmentServiceEvent.eventMaster = {};
        $scope.shipmentServiceEvent.assignedTo = {};
        $scope.shipmentServiceEvent.followUpRequired = false;
        $scope.errorMap = new Map();
        $scope.editEventIndex = null;
        discardService1.set($scope.shipmentServiceEvent);
    }

    $scope.editEventRow = function(index) {
        $scope.addEvent = false;
        $scope.addEventShow = false;
        $scope.editEventIndex = index;
        $scope.errorMap = new Map();
        $scope.shipmentServiceEvent = cloneService.clone($scope.shipmentServiceDetail.eventList[index]);
        $scope.shipmentServiceEvent.isCompleted = $scope.shipmentServiceEvent.isCompleted == 'No' ? false : true;
        $scope.shipmentServiceEvent.followUpRequired = $scope.shipmentServiceDetail.followUpRequired == 'No' ? false : true;
        discardService1.set($scope.shipmentServiceEvent);
    }


    $scope.discardEvent = function() {
        if (!discardService1.compare($scope.shipmentServiceEvent)) {

            ngDialog.openConfirm({
                template: '<p>Do you want to update your changes?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveEvent();
                } else {
                    $scope.cancelEvent();
                }
            });

        } else {
            $scope.cancelEvent();
        }
    }
    $scope.cancelEvent = function() {
        $scope.addEvent = true;
        $scope.addEventShow = true;
        $scope.editEventIndex = null;
        $scope.errorMap = new Map();
        $scope.shipmentServiceEvent = {};
    };


    $scope.deleteEvent = function(index) {
        ngDialog.openConfirm({
            template: '<p>Are you sure you want to delete selected Event?</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
                '</div>',
            plain: true,
            className: 'ngdialog-theme-default'
        }).then(function(value) {
            $scope.shipmentServiceDetail.eventList.splice(index, 1);
        }, function(value) {
            console.log("Event Deletion cancelled");
        });
    }


    $scope.changeEventCompleted = function() {
        if ($scope.eventStatusCompleted == false) {
            ngDialog.openConfirm({
                template: '<p>Are you sure to complete this event ? An Auto-Email will be trigerred.</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">Cancel</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Proceed</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                if (value == 1) {
                    $scope.eventStatusCompleted = true;
                } else if (value == 2) {
                    $scope.eventStatusCompleted = false;
                }
            });
        } else {
            $scope.eventStatusCompleted = false;
        }
    };

    //Validate ShipmentEvent//
    $scope.validateShipmentEvent = function(validate) {


        $scope.firstFocusToEvent = false;
        $scope.errorMap = new Map();


        if (validate == 0 || validate == 1) {

            if ($scope.shipmentServiceEvent.eventMaster == undefined ||
                $scope.shipmentServiceEvent.eventMaster == null ||
                $scope.shipmentServiceEvent.eventMaster == "" ||
                $scope.shipmentServiceEvent.eventMaster.id == null) {
                console.log($rootScope.nls["ERR90465"]);
                $scope.errorMap.put("shipment.event.eventMaster", $rootScope.nls["ERR90465"]);
                return false;

            } else if ($scope.shipmentServiceEvent.eventMaster.status != null) {
                if (ValidateUtil.isStatusBlocked($scope.shipmentServiceEvent.eventMaster.status)) {
                    console.log($rootScope.nls["ERR90466"]);
                    $scope.errorMap.put("shipment.event.eventMaster", $rootScope.nls["ERR90466"]);
                    $scope.shipmentServiceEvent.eventMaster = null;

                    return false

                }

                if (ValidateUtil.isStatusHidden($scope.shipmentServiceEvent.eventMaster.status)) {
                    console.log($rootScope.nls["ERR90467"]);
                    $scope.errorMap.put("shipment.event.eventMaster", $rootScope.nls["ERR90467"]);
                    $scope.shipmentServiceEvent.eventMaster = null;
                    return false

                }
            }
        }


        if (validate == 0 || validate == 2) {


            if ($scope.shipmentServiceEvent.eventDate == undefined ||
                $scope.shipmentServiceEvent.eventDate == null ||
                $scope.shipmentServiceEvent.eventDate == "") {
                console.log($rootScope.nls["ERR90468"]);
                $scope.errorMap.put("shipment.event.eventDate", $rootScope.nls["ERR90468"]);
                return false;


            }

        }


        if (validate == 0 || validate == 3) {
            if ($scope.shipmentServiceEvent.followUpRequired == 'Yes' || $scope.shipmentServiceEvent.followUpRequired == true) {

                if ($scope.shipmentServiceEvent.assignedTo == undefined ||
                    $scope.shipmentServiceEvent.assignedTo == null ||
                    $scope.shipmentServiceEvent.assignedTo == "" ||
                    $scope.shipmentServiceEvent.assignedTo.id == null) {
                    console.log($rootScope.nls["ERR90469"]);
                    $scope.errorMap.put("shipment.event.assignedTo", $rootScope.nls["ERR90469"]);
                    return false;

                } else if ($scope.shipmentServiceEvent.assignedTo.status != null) {
                    if (ValidateUtil.isEmployeeResigned($scope.shipmentServiceEvent.assignedTo.employementStatus)) {
                        console.log($rootScope.nls["ERR90470"]);
                        $scope.errorMap.put("shipment.event.assignedTo", $rootScope.nls["ERR90470"]);
                        $scope.shipmentServiceEvent.assignedTo = null;
                        return false
                    }

                    if (ValidateUtil.isEmployeeTerminated($scope.shipmentServiceEvent.assignedTo.employementStatus)) {
                        console.log($rootScope.nls["ERR90471"]);
                        $scope.errorMap.put("shipment.event.assignedTo", $rootScope.nls["ERR90471"]);
                        $scope.shipmentServiceEvent.assignedTo = null;
                        return false

                    }
                }
            }
        }

        if (validate == 0 || validate == 4) {

            if ($scope.shipmentServiceEvent.followUpRequired == 'Yes' || $scope.shipmentServiceEvent.followUpRequired == true) {

                if ($scope.shipmentServiceEvent.followUpDate == undefined ||
                    $scope.shipmentServiceEvent.followUpDate == null ||
                    $scope.shipmentServiceEvent.followUpDate == ""
                ) {
                    console.log($rootScope.nls["ERR90472"]);
                    $scope.errorMap.put("shipment.event.followUpDate", $rootScope.nls["ERR90472"]);
                    return false;

                }
            }
        }



        return true;
    }

    //pick up delivery tab starts
    $scope.ajaxShipmentTransporterEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesListByType.fetch({
            "partyType": 'Transporter'
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.shipmentTransporterList = data.responseObject.searchResult;
                    return $scope.shipmentTransporterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedShipmentTransporter = function(serviceDetail, index, obj) {

        if ($scope.validatePickUpDelivery(1, serviceDetail, index)) {
            if (serviceDetail.pickUpDeliveryPoint.transporter != null && serviceDetail.pickUpDeliveryPoint.transporter.id != null) {
                for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList.length; i++) {
                    if (serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList[i].addressType == 'Primary') {
                        if (serviceDetail.pickUpDeliveryPoint.transporterAddress == undefined) {
                            serviceDetail.pickUpDeliveryPoint.transporterAddress = {};
                        }
                        serviceDetail.pickUpDeliveryPoint.transporterAddress = addressJoiner.joinAddressLineByLine({}, serviceDetail.pickUpDeliveryPoint.transporter, serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList[i]).fullAddress;
                        break;
                    }
                }
            }
            $rootScope.navigateToNextField(obj + index);
        }


    };

    $scope.showTransportAddressList = false;
    $scope.transportAddressListConfig = {
        search: false,
        address: true
    }

    $scope.showTransportPartyAddress = function(serviceDetail) {
        $scope.panelTitle = "Transporter Address";
        $scope.showTransportAddressList = true;
        $scope.selectedItem = -1;
        $scope.transportAddressListShipment = [];
        if (serviceDetail.pickUpDeliveryPoint.transporter != null && serviceDetail.pickUpDeliveryPoint.transporter.id != null) {
            for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList.length; i++) {
                $scope.transportAddressListShipment.push(serviceDetail.pickUpDeliveryPoint.transporter.partyAddressList[i]);
            }
        }


    };

    $scope.selectedTransportPartyAddress = function(obj) {

        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].pickUpDeliveryPoint.transporterAddress = addressJoiner.joinAddressLineByLine({}, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].pickUpDeliveryPoint.transporter, obj).fullAddress;


        $scope.cancelTransportPartyAddress();

        $rootScope.navigateToNextField('ourPickUp' + $scope.selectedTabIndex);

    };

    $scope.cancelTransportPartyAddress = function() {
        $scope.showTransportAddressList = false;
        $rootScope.navigateToNextField('transporterAddress' + $scope.selectedTabIndex);
    }


    //pick up point

    $scope.ajaxShipmentPickUpEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.shipmentPickUpPointList = data.responseObject.searchResult;
                    return $scope.shipmentPickUpPointList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.selectedShipmentPickUpPoint = function(serviceDetail, index, nextFocus) {

        if ($scope.validatePickUpDelivery(3, serviceDetail, index)) {

            var tmpPartyAddress = null;

            for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList.length; i++) {

                if (serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i].addressType == 'Warehouse') {
                    tmpPartyAddress = serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i];
                    break;
                } else if (serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i].addressType == 'Primary') {
                    tmpPartyAddress = serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i];
                }
            }
            $scope.addressPickUpMapping(serviceDetail, tmpPartyAddress, serviceDetail.pickUpDeliveryPoint.pickupPoint);
            $rootScope.navigateToNextField(nextFocus + index);
        }

    };


    $scope.addressPickUpMapping = function(serviceDetail, partyAddress, partyMaster) {

        var address = {};

        address = addressJoiner.joinAddressLineByLine(address, partyMaster, partyAddress);

        serviceDetail.pickUpDeliveryPoint.pickUpPlace = address.fullAddress;
        serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = address.phone;
        serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = address.mobileNo;
        serviceDetail.pickUpDeliveryPoint.pickUpEmail = address.email;
        serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = address.contactPerson;

    }


    //pick up from

    $scope.ajaxPickupFromEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Road'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.pickupFromList = [];
                    $scope.pickupFromList = data.responseObject.searchResult;
                    return $scope.pickupFromList;
                }
            },
            function(errResponse) {
                $scope.showPickUpFromList = false;
            }
        );
    }

    $scope.selectedPickUpFrom = function(serviceDetail, index, nextFocus) {
        if ($scope.validatePickUpDelivery(4, serviceDetail, index)) {
            $rootScope.navigateToNextField(nextFocus + index);
        }
    };


    $scope.showPickUpPlaceAddressList = false;
    $scope.pickUpPlaceAddressListConfig = {
        search: false,
        address: true
    }

    $scope.showPickUpPlacePartyAddress = function(serviceDetail) {
        $scope.panelTitle = "Pickup Place Address";
        $scope.showPickUpPlaceAddressList = true;
        $scope.selectedItem = -1;
        $scope.pickUpPlaceAddressListShipment = [];
        if (serviceDetail.pickUpDeliveryPoint.pickupPoint != null && serviceDetail.pickUpDeliveryPoint.pickupPoint.id != null) {
            for (var i = 0; i < serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList.length; i++) {
                $scope.pickUpPlaceAddressListShipment.push(serviceDetail.pickUpDeliveryPoint.pickupPoint.partyAddressList[i]);
            }
        }


    };

    $scope.selectedPickUpPlacePartyAddress = function(obj) {

        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].pickUpDeliveryPoint.pickUpPlace = addressJoiner.joinAddressLineByLine({}, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].pickUpDeliveryPoint.pickupPoint, obj).fullAddress;


        $scope.cancelPickUpPlacePartyAddress();

        $rootScope.navigateToNextField('pickUpContactPerson' + $scope.selectedTabIndex);

    };

    $scope.cancelPickUpPlacePartyAddress = function() {
        $scope.showPickUpPlaceAddressList = false;
        $rootScope.navigateToNextField('PickupPlace' + $scope.selectedTabIndex);
    }




    //pick up delivery tab ENds

    //Delivery Point

    $scope.ajaxDeliveryPointEvent = function(object, serviceDetail, index) {
        if ($scope.validateShipmentServiceDetail(2, serviceDetail, index)) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortFromCFS.fetch({
                "portId": serviceDetail.origin.id
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.DeliveryPointList = [];
                        $scope.deliveryPointList = data.responseObject.searchResult;
                        return $scope.deliveryPointList;
                    }

                },
                function(errResponse) {
                    $scope.showDeliveryPointList = false;
                }
            );
        }


    }
    $scope.selectedDeliveryPoint = function(serviceDetail, index, nextFocus) {
        var obj = serviceDetail.pickUpDeliveryPoint.deliveryPoint;
        if ($scope.validatePickUpDelivery(12, serviceDetail, index)) {

            if (obj.address != null && obj.address != undefined && obj.address != '')
                serviceDetail.pickUpDeliveryPoint.deliveryPlace = obj.address;

            if (obj.contact != null && obj.contact != undefined && obj.contact != '')
                serviceDetail.pickUpDeliveryPoint.deliveryContactPerson = obj.contact;

            if (obj.phone != null && obj.phone != undefined && obj.phone != '')
                serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo = obj.phone;

            if (obj.mobileNo != null && obj.mobileNo != undefined && obj.mobileNo != '')
                serviceDetail.pickUpDeliveryPoint.deliveryMobileNo = obj.mobileNo;

            if (obj.email != null && obj.email != undefined && obj.email != '')
                serviceDetail.pickUpDeliveryPoint.deliveryEmail = obj.email;

        }
        $rootScope.navigateToNextField(nextFocus + $scope.selectedTabIndex);
    };

    //Delivery Point ends

    //Delivery From
    $scope.ajaxDeliveryFromEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Road'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.deliveryFromList = [];
                    $scope.deliveryFromList = data.responseObject.searchResult;
                    return $scope.deliveryFromList;
                }

            },
            function(errResponse) {
                $scope.showDeliveryFromList = false;
            }
        );

    }
    $scope.selectedDeliveryFrom = function(serviceDetail, index, nextFocus) {
        if ($scope.validatePickUpDelivery(13, serviceDetail, index)) {
            $rootScope.navigateToNextField(nextFocus + index);
        }

    };

    //Door Delivery Point

    $scope.ajaxDoorDeliveryPointEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Road'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.doorDeliveryPointList = [];
                    $scope.doorDeliveryPointList = data.responseObject.searchResult;
                    return $scope.doorDeliveryPointList;
                }

            },
            function(errResponse) {
                $scope.showDoorDeliveryPointList = false;
            }
        );

    }

    $scope.selectedDoorDeliveryPoint = function(serviceDetail, index, nextFocus) {
        if ($scope.validatePickUpDelivery(21, serviceDetail, index)) {
            $rootScope.navigateToNextField(nextFocus + index);
        }
    };




    //Door Delivery From
    $scope.ajaxDoorDeliveryFromEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PortByTransportMode.fetch({
            "transportMode": 'Road'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.doorDeliveryFromList = [];
                    $scope.doorDeliveryFromList = data.responseObject.searchResult;
                    return $scope.doorDeliveryFromList;
                }

            },
            function(errResponse) {
                $scope.showDoorDeliveryPointList = false;
            }
        );

    }

    $scope.selectedDoorDeliveryFrom = function(sericeDetail, index, nextFocus) {
        if ($scope.validatePickUpDelivery(22, sericeDetail, index)) {
            $rootScope.navigateToNextField(nextFocus + index);
        }

    };



    $scope.validatePickUpDelivery = function(validateCode, serviceDetail, index) {




            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                if (serviceDetail.pickUpDeliveryPoint.transporter != undefined &&
                    serviceDetail.pickUpDeliveryPoint.transporter != null &&
                    serviceDetail.pickUpDeliveryPoint.transporter.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.transporter.id != null) {

                    if (serviceDetail.pickUpDeliveryPoint.transporter.isDefaulter) {
                        $scope.errorMap.put("pickTransporter" + index, $rootScope.nls["ERR90593"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        serviceDetail.pickUpDeliveryPoint.transporter = {};
                        serviceDetail.pickUpDeliveryPoint.transporterAddress = null;
                        return false;
                    }

                    if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.transporter.status)) {
                        $scope.errorMap.put("pickTransporter" + index, $rootScope.nls["ERR90455"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        serviceDetail.pickUpDeliveryPoint.transporter = {};
                        serviceDetail.pickUpDeliveryPoint.transporterAddress = null;
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.transporter.status)) {
                        $scope.errorMap.put("pickTransporter" + index, $rootScope.nls["ERR90456"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        serviceDetail.pickUpDeliveryPoint.transporter = {};
                        serviceDetail.pickUpDeliveryPoint.transporterAddress = null;
                        return false;
                    }


                }

            }

            if (validateCode == 0 || validateCode == 3) {

                if (serviceDetail.pickUpDeliveryPoint.pickupPoint != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickupPoint != null &&
                    serviceDetail.pickUpDeliveryPoint.pickupPoint.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickupPoint.id != null) {

                    if (serviceDetail.pickUpDeliveryPoint.pickupPoint.isDefaulter) {
                        $scope.errorMap.put("pickuppoint" + index, $rootScope.nls["ERR90594"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        serviceDetail.pickUpDeliveryPoint.pickupPoint = {};
                        serviceDetail.pickUpDeliveryPoint.pickUpPlace = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpEmail = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = null;
                        return false;
                    }


                    if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.pickupPoint.status)) {
                        $scope.errorMap.put("pickuppoint" + index, $rootScope.nls["ERR90484"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        serviceDetail.pickUpDeliveryPoint.pickupPoint = {};
                        serviceDetail.pickUpDeliveryPoint.pickUpPlace = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpEmail = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = null;
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.pickupPoint.status)) {
                        $scope.errorMap.put("pickuppoint" + index, $rootScope.nls["ERR90485"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        serviceDetail.pickUpDeliveryPoint.pickupPoint = {};
                        serviceDetail.pickUpDeliveryPoint.pickUpPlace = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpEmail = null;
                        serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = null;
                        return false;
                    }




                } else {

                    if (serviceDetail.pickUpDeliveryPoint.isOurPickUp == 'Yes' || serviceDetail.pickUpDeliveryPoint.isOurPickUp == true) {
                        $scope.errorMap.put("pickuppoint" + index, $rootScope.nls["ERR90483"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }

                }

            }

            if (validateCode == 0 || validateCode == 4) {

                if (serviceDetail.pickUpDeliveryPoint.pickupFrom != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickupFrom != null &&
                    serviceDetail.pickUpDeliveryPoint.pickupFrom.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickupFrom.id != null) {

                    if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.pickupFrom.status)) {
                        serviceDetail.pickUpDeliveryPoint.pickupFrom = {};
                        $scope.errorMap.put("pickupFrom" + index, $rootScope.nls["ERR90487"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.pickupFrom.status)) {
                        serviceDetail.pickUpDeliveryPoint.pickupFrom = {};
                        $scope.errorMap.put("pickupFrom" + index, $rootScope.nls["ERR90488"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }
                } else {
                    if (serviceDetail.pickUpDeliveryPoint.isOurPickUp == 'Yes' || serviceDetail.pickUpDeliveryPoint.isOurPickUp == true) {
                        $scope.errorMap.put("pickupFrom" + index, $rootScope.nls["ERR90486"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }
                }


            }


            if (validateCode == 0 || validateCode == 6) {



                if (serviceDetail.pickUpDeliveryPoint.pickUpContactPerson != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickUpContactPerson != null &&
                    serviceDetail.pickUpDeliveryPoint.pickUpContactPerson != ""
                ) {
                    if (!ValidateUtil.isContactPerson(serviceDetail.pickUpDeliveryPoint.pickUpContactPerson)) {
                        $scope.errorMap.put("pickUpContactPerson" + index, $rootScope.nls["ERR90490"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }
                }



            }

            if (validateCode == 0 || validateCode == 7) {



                if (serviceDetail.pickUpDeliveryPoint.pickUpMobileNo != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickUpMobileNo != null &&
                    serviceDetail.pickUpDeliveryPoint.pickUpMobileNo != ""
                ) {
                    serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = serviceDetail.pickUpDeliveryPoint.pickUpMobileNo.trim();
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER, serviceDetail.pickUpDeliveryPoint.pickUpMobileNo)) {
                        $scope.errorMap.put("pickUpMobile" + index, $rootScope.nls["ERR90491"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }
                }



            }

            if (validateCode == 0 || validateCode == 8) {



                if (serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo != null &&
                    serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo != "") {

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo)) {
                        $scope.errorMap.put("pickUpPhoneNo" + index, $rootScope.nls["ERR90492"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }
                }



            }


            if (validateCode == 0 || validateCode == 9) {


                if (serviceDetail.pickUpDeliveryPoint.pickUpEmail != undefined &&
                    serviceDetail.pickUpDeliveryPoint.pickUpEmail != null &&
                    serviceDetail.pickUpDeliveryPoint.pickUpEmail != ""
                ) {
                    if (ValidateUtil.isValidMail(serviceDetail.pickUpDeliveryPoint.pickUpEmail)) {
                        $scope.errorMap.put("pickUpemail" + index, $rootScope.nls["ERR90494"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.pickup = true;
                        return false;
                    }

                }
            }



            if (validateCode == 0 || validateCode == 10) {




            }
            if (validateCode == 0 || validateCode == 11) {



            }

            //Delivery Validation

            if (validateCode == 0 || validateCode == 12) {
                if (serviceDetail.pickUpDeliveryPoint.deliveryPoint != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPoint != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPoint.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPoint.id != null) {

                    if (serviceDetail.pickUpDeliveryPoint.deliveryPoint.status != null) {

                        if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.deliveryPoint.status)) {
                            $scope.errorMap.put("deliveryPoint" + index, $rootScope.nls["ERR90496"]);
                            $scope.shipmentTab = 'pickup';
                            $scope.delivery = true;
                            serviceDetail.pickUpDeliveryPoint.deliveryPoint = {};
                            serviceDetail.pickUpDeliveryPoint.deliveryPlace = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryMobileNo = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryEmail = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryContactPerson = null;
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.deliveryPoint.status)) {
                            $scope.errorMap.put("deliveryPoint" + index, $rootScope.nls["ERR90497"]);
                            $scope.shipmentTab = 'pickup';
                            $scope.delivery = true;
                            serviceDetail.pickUpDeliveryPoint.deliveryPoint = {};
                            serviceDetail.pickUpDeliveryPoint.deliveryPlace = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryMobileNo = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryEmail = null;
                            serviceDetail.pickUpDeliveryPoint.deliveryContactPerson = null;
                            return false;
                        }

                    }


                }
                /*else
			    			  {

				    		  if(serviceDetail.serviceMaster!=undefined && serviceDetail.serviceMaster != null 
				    				  && serviceDetail.serviceMaster!=''
				    				  && serviceDetail.serviceMaster.importExport=='Export')
				    			  {
				    		  $scope.errorMap.put("deliveryPoint"+index,$rootScope.nls["ERR90495"]);
					    	  $scope.Tabs=='pickup';
					    	  $scope.shipmentTab='pickup';
					    	  $scope.delivery=true;
					    	  return false;
				    			  }
				    		  
			    			  }*/

            }

            if (validateCode == 0 || validateCode == 13) {
                if (serviceDetail.pickUpDeliveryPoint.deliveryFrom != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryFrom != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryFrom.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryFrom.id != null) {

                    if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.deliveryFrom.status)) {
                        serviceDetail.pickUpDeliveryPoint.deliveryFrom = {};
                        $scope.errorMap.put("deliveryFrom" + index, $rootScope.nls["ERR90499"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.deliveryFrom.status)) {
                        serviceDetail.pickUpDeliveryPoint.deliveryFrom = {};
                        $scope.errorMap.put("deliveryFrom" + index, $rootScope.nls["ERR90500"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }
                } else {
                    if (serviceDetail.pickUpDeliveryPoint.isOurPickUp == 'Yes' || serviceDetail.pickUpDeliveryPoint.isOurPickUp == true) {
                        $scope.errorMap.put("deliveryFrom" + index, $rootScope.nls["ERR90498"]);
                        $scope.Tabs = 'pickup';
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 14) {



                if (serviceDetail.pickUpDeliveryPoint.deliveryPlace != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPlace != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPlace != ""
                ) {
                    /*var regexp = new RegExp("^[ A-Za-z0-9\n_@.#&+-]{0,100}$");
                        						if (!regexp.test(serviceDetail.pickUpDeliveryPoint.deliveryPlace)) {
                        							$scope.errorMap.put("deliveryPlace"+index,$rootScope.nls["ERR90501"]);
                        							$scope.Tabs=='pickup';
                        							$scope.shipmentTab='pickup';
                        							$scope.delivery=true;
                        		 		    			return false;
                        		    			}*/
                }



            }

            if (validateCode == 0 || validateCode == 15) {



                if (serviceDetail.pickUpDeliveryPoint.deliveryContactPerson != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryContactPerson != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryContactPerson != ""
                ) {
                    if (!ValidateUtil.isContactPerson(serviceDetail.pickUpDeliveryPoint.deliveryContactPerson)) {
                        $scope.errorMap.put("deliveryContactPerson" + index, $rootScope.nls["ERR90502"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }
                }



            }

            if (validateCode == 0 || validateCode == 16) {



                if (serviceDetail.pickUpDeliveryPoint.deliveryMobileNo != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryMobileNo != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryMobileNo != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER, serviceDetail.pickUpDeliveryPoint.deliveryMobileNo)) {
                        $scope.errorMap.put("deliveryMobile" + index, $rootScope.nls["ERR90503"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }
                }



            }

            if (validateCode == 0 || validateCode == 17) {



                if (serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo != ""
                ) {

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, serviceDetail.pickUpDeliveryPoint.deliveryPhoneNo)) {
                        $scope.errorMap.put("deliveryPhoneNo" + index, $rootScope.nls["ERR90504"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 18) {


                if (serviceDetail.pickUpDeliveryPoint.deliveryEmail != undefined &&
                    serviceDetail.pickUpDeliveryPoint.deliveryEmail != null &&
                    serviceDetail.pickUpDeliveryPoint.deliveryEmail != "") {

                    if (!CommonValidationService.checkMultipleMail(serviceDetail.pickUpDeliveryPoint.deliveryEmail)) {
                        $scope.errorMap.put("deliveryEmail" + index, $rootScope.nls["ERR90506"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.delivery = true;
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 19) {



            }
            if (validateCode == 0 || validateCode == 20) {



            }


            //Door Delivery Validation

            if (validateCode == 0 || validateCode == 21) {

                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint.id != null) {

                    if (serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint.status != null) {

                        if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint.status)) {
                            $scope.errorMap.put("doorDeliveryPoint" + index, $rootScope.nls["ERR90508"]);
                            $scope.shipmentTab = 'pickup';
                            $scope.doordelivery = true;
                            serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint.status)) {
                            $scope.errorMap.put("doorDeliveryPoint" + index, $rootScope.nls["ERR90509"]);
                            $scope.shipmentTab = 'pickup';
                            $scope.doordelivery = true;
                            serviceDetail.pickUpDeliveryPoint.doorDeliveryPoint = {};
                            return false;
                        }

                    }


                }

                /* else
       			    		  {
       			    		 
       			    		 
       			    		  if(serviceDetail.serviceMaster!=undefined && serviceDetail.serviceMaster != null 
       			    				  && serviceDetail.serviceMaster!=''
       			    				  && serviceDetail.serviceMaster.importExport=='Export')
       			    			  {
       			    		  $scope.errorMap.put("doorDeliveryPoint"+index,$rootScope.nls["ERR90507"]);
       			    		$scope.doordelivery=true;
       				    	  $scope.Tabs=='pickup';
       				    	$scope.shipmentTab='pickup';
       				    	  return false;
       			    			  }
       			    		  }*/


            }

            if (validateCode == 0 || validateCode == 22) {
                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom.id != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom.id != null) {

                    if (ValidateUtil.isStatusBlocked(serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom.status)) {
                        serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom = {};
                        $scope.errorMap.put("doorDeliveryFrom" + index, $rootScope.nls["ERR90511"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.doordelivery = true;
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden(serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom.status)) {
                        serviceDetail.pickUpDeliveryPoint.doorDeliveryFrom = {};
                        $scope.errorMap.put("doorDeliveryFrom" + index, $rootScope.nls["ERR90512"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.doordelivery = true;
                        return false;
                    }
                }

                /*else
                               			{
                               			$scope.errorMap.put("doorDeliveryFrom"+index,$rootScope.nls["ERR90510"]);
                               			$scope.Tabs=='pickup';
                               			$scope.shipmentTab='pickup';
                               			$scope.doordelivery=true;
                               			return false;
                               			}*/

            }

            if (validateCode == 0 || validateCode == 23) {


                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace != ""
                ) {
                    /*var regexp = new RegExp("^[ A-Za-z0-9\n_@.#&+-]{0,100}$");
                             						if (!regexp.test(serviceDetail.pickUpDeliveryPoint.doorDeliveryPlace)) {
                             							$scope.errorMap.put("deliveryPlace"+index,$rootScope.nls["ERR90513"]);
                             							$scope.Tabs=='pickup';
                             							$scope.shipmentTab='pickup';
                             							$scope.doordelivery=true;
                             		 		    			return false;
                             		    			}*/
                }


            }

            if (validateCode == 0 || validateCode == 24) {


                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryContactPerson != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryContactPerson != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryContactPerson != ""
                ) {
                    if (!ValidateUtil.isContactPerson(serviceDetail.pickUpDeliveryPoint.doorDeliveryContactPerson)) {
                        $scope.errorMap.put("doorDeliveryContactPerson" + index, $rootScope.nls["ERR90514"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.doordelivery = true;
                        return false;
                    }
                }


            }

            if (validateCode == 0 || validateCode == 25) {


                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryMobileNo != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryMobileNo != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryMobileNo != ""
                ) {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER, serviceDetail.pickUpDeliveryPoint.doorDeliveryMobileNo)) {
                        $scope.errorMap.put("doorDeliveryMobileNo" + index, $rootScope.nls["ERR90515"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.doordelivery = true;
                        return false;
                    }
                }


            }

            if (validateCode == 0 || validateCode == 26) {


                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryPhoneNo != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPhoneNo != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryPhoneNo != ""
                ) {

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER, serviceDetail.pickUpDeliveryPoint.doorDeliveryPhoneNo)) {
                        $scope.errorMap.put("doorDeliveryPhoneNo" + index, $rootScope.nls["ERR90516"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.doordelivery = true;
                        return false;
                    }
                }


            }


            if (validateCode == 0 || validateCode == 27) {

                if (serviceDetail.pickUpDeliveryPoint.doorDeliveryEmail != undefined &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryEmail != null &&
                    serviceDetail.pickUpDeliveryPoint.doorDeliveryEmail != ""
                ) {

                    if (!CommonValidationService.checkMultipleMail(serviceDetail.pickUpDeliveryPoint.doorDeliveryEmail)) {
                        $scope.errorMap.put("doorDeliveryEmail" + index, $rootScope.nls["ERR90518"]);
                        $scope.shipmentTab = 'pickup';
                        $scope.doordelivery = true;
                        return false;
                    }


                }
                /*else{
                                 					if(serviceDetail.pickUpDeliveryPoint.pickUpEmail!=null && serviceDetail.pickUpDeliveryPoint.pickUpEmail!= undefined && serviceDetail.pickUpDeliveryPoint.pickUpEmail!=""){
                                 					 $scope.errorMap.put("pickUpEmail"+index,$rootScope.nls["ERR95909"]);
                             				    	                               				    	  $scope.Tabs=='documents'
                             	 						return false;
                                 					}
                                 				}*/

            }
            if (validateCode == 0 || validateCode == 28) {



            }
            if (validateCode == 0 || validateCode == 29) {



            }

            return true;




        } //validate ends

    $scope.unitChange = function() {
        $scope.errorMap = new Map();

    }

    $scope.changePickUpToggle = function(serviceDetail) {


        $scope.errorMap = new Map();
        if (serviceDetail.pickUpDeliveryPoint.isOurPickUp == false || serviceDetail.pickUpDeliveryPoint.isOurPickUp == 'No') {
            serviceDetail.pickUpDeliveryPoint = {};
        } else {
            //Need to fetch from document shipper and forwarder if our pick up yes

            if (serviceDetail.documentList.length == 0) {
                console.log("pick up to document");
                $scope.shipmentTab = 'documents';
                $rootScope.clientMessage = $rootScope.nls["ERR90558"];
                serviceDetail.pickUpDeliveryPoint.isOurPickUp = false;
                return false;
            } else {


                if (serviceDetail != undefined && serviceDetail.documentList != undefined && serviceDetail.documentList != null) {

                    if (serviceDetail.documentList[0].forwarder != undefined) {

                        serviceDetail.pickUpDeliveryPoint.pickupPoint = serviceDetail.documentList[0].forwarder;


                        if (serviceDetail.documentList[0].forwarderAddress != undefined && serviceDetail.documentList[0].forwarderAddress != null) {
                            var partyAddress = serviceDetail.documentList[0].forwarderAddress;
                            serviceDetail.pickUpDeliveryPoint.pickUpPlace = partyAddress.fullAddress;
                            serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = partyAddress.phone;
                            serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = partyAddress.mobileNo;
                            serviceDetail.pickUpDeliveryPoint.pickUpEmail = partyAddress.email;
                            serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = partyAddress.contactPerson;
                        }

                    }
                    if (serviceDetail != undefined && serviceDetail.documentList[0].forwarder == undefined && serviceDetail.documentList[0].shipper != undefined) {

                        serviceDetail.pickUpDeliveryPoint.pickupPoint = serviceDetail.documentList[0].shipper;

                        if (serviceDetail.documentList[0].shipperAddress != undefined && serviceDetail.documentList[0].shipperAddress != null) {

                            var partyAddress = serviceDetail.documentList[0].shipperAddress;


                            serviceDetail.pickUpDeliveryPoint.pickUpPlace = partyAddress.fullAddress;
                            serviceDetail.pickUpDeliveryPoint.pickUpPhoneNo = partyAddress.phone;
                            serviceDetail.pickUpDeliveryPoint.pickUpMobileNo = partyAddress.mobileNo;
                            serviceDetail.pickUpDeliveryPoint.pickUpEmail = partyAddress.email;
                            serviceDetail.pickUpDeliveryPoint.pickUpContactPerson = partyAddress.contactPerson;

                        }


                    }

                }

            }


        }




    }


    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }
    $scope.orginRender = function(item) {
        return {
            label: item.portGroupName,
            item: item
        }
    }
    $scope.projectNameRender = function(item) {
        return {
            label: item.projectName,
            item: item
        }

    }
    $scope.tosNameRender = function(item) {
        return {
            label: item.tosName,
            item: item
        }

    }
    $scope.hsNameRender = function(item) {
        return {
            label: item.hsName,
            item: item
        }

    }
    $scope.empNameRender = function(item) {
        return {
            label: item.employeeName,
            item: item
        }

    }
    $scope.serviceNameRender = function(item) {
        return {
            label: item.serviceName,
            item: item
        }

    }
    $scope.PortNameRender = function(item) {
        return {
            label: item.portName,
            item: item
        }

    }
    $scope.divisionRender = function(item) {
        return {
            label: item.divisionName,
            item: item
        }

    }
    $scope.packRender = function(item) {
        return {
            label: item.packName,
            item: item
        }

    }

    $scope.carrierNameRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }

    }
    $scope.cfsNameRender = function(item) {
        return {
            label: item.cfsName,
            item: item
        }

    }

    $scope.viewRate = function(tmpObj) {

        if (tmpObj.isRatesCheck()) {
            var newScope = $scope.$new();

            if (tmpObj.localCurrency != null) {
                newScope.rateMerged = $scope.rateMerged;
                newScope.localCurrency = tmpObj.localCurrency.currencyCode;
                newScope.clientGrossRate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.clientGrossRate);
                newScope.clientNetRate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.clientNetRate);
                newScope.declaredCost = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.declaredCost);
                newScope.rate = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.rate);
                newScope.cost = $rootScope.currencyFormat(tmpObj.localCurrency, tmpObj.cost);
            }

            var rateModal = $modal({
                scope: newScope,
                templateUrl: 'viewlocalRate.html',
                show: false
            });
            rateModal.$promise.then(rateModal.show);
        }

    };


    $scope.minimumShipmentAlert = function() {
        var newScope = $scope.$new();
        newScope.message = $rootScope.nls['ERR90598'];
        newScope.message = newScope.message.replace('%S', $rootScope.appMasterData['shipment.minimum.chargeable.weight'])
        ngDialog.openConfirm({
            template: '<p>{{message}}</p> ' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(0)">No' +
                '</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            if (value == 1) {
                if ($scope.shipment.id == undefined || $scope.shipment.id == null || $scope.shipment.id == -1) {
                    $scope.addToDatabase();
                } else {
                    $scope.updateToDatabase();
                }
            } else {
                $scope.enableSubmitBtnFn();
            }


        });
    }

    $scope.showResponse = function(shipment, isAdded) {
        $scope.enableSubmitBtnFn
        var newScope = $scope.$new();
        newScope.shipmentUid = shipment.shipmentUid;
        newScope.opt = isAdded == true ? 'saved' : 'updated';
        ngDialog.openConfirm({
            template: '<p>Shipment  {{shipmentUid}} successfully {{opt}}.</p> ' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                '</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default top_9999'
        }).
        then(function(value) {
            if ($scope.hawbModal != undefined && $scope.hawbModal != null) {
                $scope.hawbModal.$promise.then($scope.hawbModal.hide);
            }
            var params = {};
            if ($scope.checkIsItFromConsol(shipment, params)) {
                console.log("navigate to consol ")
            } else {

                var stateCount = 0;
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }
                $state.go("layout.editNewShipment", {
                    shipmentId: shipment.id,
                    submitAction: 'Saved',
                    count: stateCount
                });


            }
        }, function(value) {
            $state.go("layout.crmShipment", {
                submitAction: 'Cancelled'
            });
        });
    }


    $scope.checkIsItFromConsol = function(shipment, params) {
        if ($stateParams.action == 'CREATE' && $stateParams.fromScreen != undefined && $stateParams.forPurpose == 'createShipmentFromConsol') {
            console.log("shipment creation from consol import");
            if (shipment != undefined && shipment != null && shipment.shipmentServiceList != undefined && shipment.shipmentServiceList != null &&
                shipment.shipmentServiceList.length > 0) {
                params.forPurpose = "createdShipmentFromConsol";
                params.shipmentUid = shipment.shipmentUid;
                $state.go($stateParams.fromScreen, params);
            }
        }
        return false;
    }

    /*
        $scope.$watch('shipment', function(newVal, oldVal) {
            $scope.isFormValueDirtyCount++;
            if ($scope.isFormValueDirtyCount > 1) {
                $scope.isFormValueDirty = true;
            }
        }, true);*/

    $scope.tableDimensionStatee = function(param, shipmentServiceDetail) {
        shipmentServiceDetail.tableConnectionState = param;
    }

    $scope.tableRateStatee = function(param) {
        $scope.tableRateState = param;
    }


    /*BUTTON GROUP SCOPE VALUE*/
    $scope.btnGrp = {};
    $scope.btnGrpFilter = {};
    $scope.btnGrp.TypeArr = ["Invoice", "Credit Note", "All"];
    $scope.btnGrpFilter.TypeArr = ["Customer", "Agent", "All"];


    //angular.forEach($scope.awb.data,function(item,index){
    //    $scope.awb.totalNoOfPiece+= item.noPieces;
    //    $scope.awb.totalGross+= item.grossWeight;
    //    item.total = item.chargeableWeight * item.rate_charge;
    //    $scope.awb.totalCharge+= item.total;
    //});
    $scope.modelOption = {};

    $scope.showcostserivce = function(id, type) {
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/shipment/views/get_cost_service_popup.html',
            show: false
        });
        $scope.docType = type;
        myOtherModal.$promise.then(myOtherModal.show);
        $timeout(function() {
            $scope.isError = true;
        }, 500);
    };



    $scope.createInvoice = function(shipmentServiceDetail) {

        if (shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Received' ||
            shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Generated') {


            var param = {
                shipmentId: $scope.shipment.id,
                shipmentServiceId: shipmentServiceDetail.serviceUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.invoiceCreate', param);
        } else {
            console.log("Can't create invoice, if shipment service is not received or generated")
            $rootScope.clientMessage = $rootScope.nls["ERR40015"];
            return false;

        }

    }
    $scope.createCreditNote = function(shipmentServiceDetail) {
        if (shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Received' ||
            shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Generated') {

            var param = {
                shipmentId: $scope.shipment.id,
                shipmentServiceId: shipmentServiceDetail.serviceUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.creditNoteCostCreate', param);
        } else {
            console.log("Can't create credit note cost, if shipment service is not received or generated")
            $rootScope.clientMessage = $rootScope.nls["ERR40016"];
            return false;

        }
    }

    $scope.createCreditNoteRevenue = function(shipmentServiceDetail) {


        var param = {
            shipmentUid: $scope.shipment.shipmentUid,
            shipmentServiceId: shipmentServiceDetail.id,
            fromState: $state.current.name,
            fromStateParams: JSON.stringify($stateParams)
        }

        $state.go('layout.addCreditNoteRevenue', param);

    }




    $scope.getInvoiceCrnView = function(invoiceCreditNote, shipmentServiceDetail) {

        var params = {};
        if (invoiceCreditNote.documentTypeMaster.documentTypeCode == 'INV') {

            var param = {
                shipmentUid: $scope.shipment.shipmentUid,
                invoiceId: invoiceCreditNote.id,
                shipmentServiceId: shipmentServiceDetail.id,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.viewInvoice', param);
        } else {
            if (invoiceCreditNote.documentTypeMaster.documentTypeCode == 'CRN' && invoiceCreditNote.adjustmentInvoiceNo != null) {
                var param = {
                    shipmentUid: $scope.shipment.shipmentUid,
                    creditNoteRevenueId: invoiceCreditNote.id,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify($stateParams)
                }
                $state.go('layout.viewCreditNoteRevenue', param);
            } else {
                var param = {
                    shipmentUid: $scope.shipment.shipmentUid,
                    creditNoteCostId: invoiceCreditNote.id,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify($stateParams)
                }
                $state.go('layout.viewCreditNoteCost', param);
            }
        }

    }


    $scope.ajaxPortOfDocLoadingDynamic = function(object) {
        console.log("ajaxPortOfLoadingEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": $scope.shipmentServiceDetailClone.serviceMaster.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {


                if (data.responseCode == "ERR0") {
                    $scope.portOfDocLoadingList = [];
                    var resultList = data.responseObject.searchResult;

                    if (resultList != null && resultList.length != 0) {
                        if ($scope.documentDetailClone.pod == undefined || $scope.documentDetailClone.pod == null) {
                            $scope.portOfDocLoadingList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != $scope.documentDetailClone.pod.id) {
                                    $scope.portOfDocLoadingList.push(resultList[i]);
                                }

                            }
                        }
                    }
                    return $scope.portOfDocLoadingList;
                    //$scope.showPortOfDocLoadingList=true;
                }



            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );

    }

    $scope.ajaxCarrierDynamicEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CarrierByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(
            function(data) {
                $scope.carrierMasterList = data.responseObject.searchResult;
                if (data.responseCode == "ERR0") {
                    //$scope.showCarrierMasterList=true;
                    return $scope.carrierMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air');
            }
        );

    }




    $scope.changeRoutedBy = function(serviceDetail) {

        if ($scope.shipment.whoRouted && (serviceDetail.agent == undefined || serviceDetail.agent == null)) {
            serviceDetail.whoRouted = $scope.shipment.whoRouted ? true : false;
            serviceDetail.salesman = null;
            serviceDetail.agent = null;
            if ($scope.shipment.agent != null) {
                serviceDetail.agent = JSON.parse(JSON.stringify($scope.shipment.agent));
            }


        } else if (!$scope.shipment.whoRouted && (serviceDetail.salesman == undefined || serviceDetail.salesman == null)) {
            serviceDetail.whoRouted = $scope.shipment.whoRouted ? true : false;
            serviceDetail.salesman = null;
            serviceDetail.agent = null;
            if ($scope.shipment.salesman != null) {
                serviceDetail.salesman = JSON.parse(JSON.stringify($scope.shipment.salesman));
            }

        }

    }




    //validation for DYnamic Document


    $scope.ajaxCurrencyEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CurrencySearchExclude.fetch({
            "excludeCurrencyCode": $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.currencyList = data.responseObject.searchResult;
                    /*$scope.showCurrencyList = true;*/
                    return $scope.currencyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency');
            }
        );


    }

    $scope.validateCarrier = function(serviceDetail) {

        $scope.errorMap = new Map();

        if (serviceDetail.carrier == undefined ||
            serviceDetail.carrier == null ||
            serviceDetail.carrier == "" ||
            serviceDetail.carrier.id == null) {
            $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90063"]);
            $scope.shipmentTab = "general";
            return false
        } else if (serviceDetail.carrier.status != null) {
            if (ValidateUtil.isStatusBlocked(serviceDetail.carrier.status)) {
                console.log($rootScope.nls["ERR90064"]);
                $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90064"]);
                serviceDetail.carrier = null;
                $scope.shipmentTab = "general";
                return false

            }

            if (ValidateUtil.isStatusHidden(serviceDetail.carrier.status)) {
                console.log($rootScope.nls["ERR90065"]);
                $scope.errorMap.put("shipmentCarrier", $rootScope.nls["ERR90065"]);
                serviceDetail.carrier = null;
                $scope.shipmentTab = "general";
                return false

            }
        }

        return true;
    }
    $scope.fetchMawb = function(serviceDetail, index) {
        console.log("FetchMAWB is called");
        $rootScope.clientMessage = null;
        if ($scope.validateCarrier(serviceDetail) && $scope.validateShipmentServiceDetail(2, serviceDetail, index)) {
            if (serviceDetail.mawbNo == undefined || serviceDetail.mawbNo == null || serviceDetail.mawbNo == "") {
                GetNextMawb.fetch({
                    "carrierId": serviceDetail.carrier.id,
                    "portId": serviceDetail.origin.id
                }).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            if (data.responseObject != null) {
                                serviceDetail.mawbNo = data.responseObject.mawbNo;
                                serviceDetail.mawbDate = $rootScope.sendApiDateAndTime(new Date());
                                $scope.updateMoreInfoToDocument('MAWB', 'etdMoreInfoShipment' + $scope.selectedTabIndex)
                                if (data.responseObject.remindNo != null) {
                                    $rootScope.clientMessage = $rootScope.nls["ERR100023"];
                                }
                            } else {
                                $rootScope.clientMessage = $rootScope.nls["ERR100022"];
                                $rootScope.navigateToNextField('mawbNo' + $scope.selectedTabIndex)
                            }
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching mawb no');
                    }
                );
            } else {
                $rootScope.clientMessage = $rootScope.nls["ERR100026"];
                $rootScope.navigateToNextField('etdMoreInfoShipment' + $scope.selectedTabIndex);
            }
        }

    }


    $scope.notesmodel = function() {
        var myTotalOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/shipment/views/notespopup_edit.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show)
    };
    $scope.refmodel = function() {
        var myTotalOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/crm/shipment/views/refpopup_edit.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);
    };

    // invoice list under shipment service


    // Provisional Cost

    $scope.refmodel = function(provisionalItem) {
        var newScope = $scope.$new();
        newScope.provisionalItem = provisionalItem;
        var myTotalOtherModal = $modal({
            scope: newScope,
            templateUrl: 'app/components/finance/provisional_cost/view/refpopup.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);
    };

    $scope.addNewProvisionalFromShipment = function(shipmentServiceDetail) {

        if (shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Received' || shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Generated') {
            console.log("Provisional addNewProvisionalFromShipment Button is Pressed.....");
            var param = {
                shipmentId: $scope.shipment.id,
                shipmentServiceId: shipmentServiceDetail.serviceUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
            $state.go("layout.addProvisional", param);
        } else {
            console.log("Can't create or edit provisional cost, if shipment is not received or generated")
            $rootScope.clientMessage = $rootScope.nls["ERR96676"];
            return false;
        }
    }
    $scope.editNewProvisionalFromShipment = function(object, shipmentServiceDetail) {

        if (shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Received' || shipmentServiceDetail.lastUpdatedStatus.serviceStatus == 'Generated') {
            console.log("Provisional editNewProvisionalFromConsol Button is Pressed.....");
            var param = {
                shipmentId: $scope.shipment.id,
                shipmentServiceId: shipmentServiceDetail.serviceUid,
                provisionalId: object.id,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
            $state.go("layout.editProvisional", param);
        } else {
            console.log("Can't create or edit provisional cost, if shipment is not received or generated")
            $rootScope.clientMessage = $rootScope.nls["ERR96676"];
            return false;
        }
    }

    $scope.getProvisionalData = function(serviceuid) {
        $scope.provisionSpinner = true;
        ProvisionalViewByShipmentServiceUid.get({
            serviceuid: serviceuid
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("ProvisionalViewByShipmentServiceUid Successfull")
                $scope.provisionalObj = data.responseObject;
                console.log("Provisional Object : ", $scope.provisional);
                $scope.provisionSpinner = false;
            } else {
                console.log("ProvisionalViewByShipmentServiceUid Failed " + data.responseDescription);
                $scope.provisionSpinner = false;
            }
        }, function(error) {
            console.log("ProvisionalViewByShipmentServiceUid Failed : " + error)
        });
    }

    //Provisional end


    $scope.getAccountData = function(shipmentServiceId) {
        $scope.invoiceSpinner = true;
        $scope.searchDto = {};
        $scope.searchDto.param1 = shipmentServiceId;
        $scope.searchDto.param2 = null;
        return ShipmentServiceInvoiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.invoiceList = data.responseObject;
                    $scope.invoiceSpinner = false;
                }
            },
            function(errResponse) {
                console.error('Error while fetching invoicelist');
                $scope.invoiceSpinner = false;
            }
        );
    }

    $scope.filterCustomerAgent = function(type) {

        if (type == 'Customer')
            $scope.customerAgent = 'Customer';
        if (type == 'Agent')
            $scope.customerAgent = 'Agent';
        if (type == 'All')
            $scope.customerAgent = undefined;

    }


    $scope.filterInvoiceCreditNote = function(type) {

        if (type == 'Invoice')
            $scope.documentTypeCode = 'INV';
        if (type == 'Credit Note')
            $scope.documentTypeCode = 'CRN';
        if (type == 'All')
            $scope.documentTypeCode = undefined;

    }

    //  CRM Shipment Documents tab Navigation

    $scope.selectPartyDocument = function(name, nextIdValue) {


        if (name == 'shipper') {
            $scope.choosePartyAddress('shipper');
            if ($scope.validateServiceDocument(1)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }
        if (name == 'consignee') {
            $scope.choosePartyAddress('consignee');
            if ($scope.validateServiceDocument(5)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }
        if (name == 'firstNotify') {
            $scope.choosePartyAddress('firstNotify');
            if ($scope.validateServiceDocument(6)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }
        if (name == 'secondNotify') {
            $scope.choosePartyAddress('secondNotify');
            if ($scope.validateServiceDocument(7)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }
        if (name == 'forwarder') {
            $scope.choosePartyAddress('forwarder');
            if ($scope.validateServiceDocument(3)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }
        if (name == 'agent') {
            $scope.choosePartyAddress('agent');
            if ($scope.validateServiceDocument(8)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }
        if (name == 'issuingAgent') {
            $scope.choosePartyAddress('issuingAgent');
            if ($scope.validateServiceDocument(9)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        }

    }
    $scope.nextFocus = function(id) {
        console.log("called." + id);
        $rootScope.navigateToNextField(id);
    }

    $scope.addNewDocument = function() {
        $scope.shipmentServiceDetail.documentList.push({});
        $scope.documentListTab = null;
        $scope.documentListTab = $scope.shipmentServiceDetail.documentList.length - 1;
    };

    $scope.clickDocumentIndex = function(index) {
        $scope.documentListTab = null;
        $scope.documentListTab = index;
    };

    $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push($scope.shipmentServiceDetailClone.shipmentChargeListClone[i]);
        }
        return input;
    };

    $scope.changeShipment = function() {

        $scope.errorMap = new Map();
    }




    $scope.updateMoreInfoToDocument = function(whichField, nextFocus) {


        if ($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList != undefined && $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList != null && $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList.length != 0) {

            for (var i = 0; i < $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList.length; i++) {

                if (whichField == 'Pack') {
                    console.log("UpdateMoreInfoToDocument ", whichField);
                    $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].packMaster = JSON.parse(JSON.stringify($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].packMaster));
                    $rootScope.navigateToNextField(nextFocus);
                } else if (whichField == 'Carrier') {
                    $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].carrier = JSON.parse(JSON.stringify($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].carrier));
                    $rootScope.navigateToNextField(nextFocus);
                } else if (whichField == 'ETD') {
                    if ($scope.validateShipmentServiceDetail(20, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex], $scope.selectedTabIndex)) {
                        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].etd = null;
                        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].etd = $rootScope.convertToDateTime($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].etd);
                        $rootScope.navigateToNextField(nextFocus);
                    }
                } else if (whichField == 'ETA') {
                    if ($scope.validateShipmentServiceDetail(21, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex], $scope.selectedTabIndex)) {
                        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].eta = null;
                        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].eta = $rootScope.convertToDateTime($scope.shipment.shipmentServiceList[$scope.selectedTabIndex].eta);
                        $rootScope.navigateToNextField(nextFocus);
                    }
                } else if (whichField == 'RouteNo') {
                    if ($scope.validateShipmentServiceDetail(18, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex], $scope.selectedTabIndex)) {
                        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].routeNo = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].routeNo;
                        $rootScope.navigateToNextField(nextFocus);
                    }
                } else if (whichField == 'MAWB') {
                    if ($scope.validateShipmentServiceDetail(19, $scope.shipment.shipmentServiceList[$scope.selectedTabIndex], $scope.selectedTabIndex)) {
                        $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].documentList[i].mawbNo = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex].mawbNo;
                        // $rootScope.navigateToNextField(nextFocus);
                    }
                }

            }
        } else {
            $rootScope.navigateToNextField(nextFocus);
        }



    }

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('service-panel'));
    $scope.contained_progressbar.setAbsolute();


    // which is for creating a shipment from other screens (written for shipment from consol)
    $scope.createInit = function() {
        $scope.shipmentCommonData();
    }


    $scope.shipmentCommonData = function() {

        $scope.cloneConnectionList = []; // for dynamic hawb report
        $scope.errorMap = new Map(); // for dynamic hawb report
        $scope.firstFocus = true;
        $scope.enableBookingRates = $rootScope.appMasterData['booking.rates.merged'];
        $scope.shipmentDimensionValid = true;
        $scope.invoiceList = [];
        $scope.addConnection = true;
        $scope.addEvent = true;
        $scope.addEventShow = true;
        $scope.addStatusShow = true;
        $scope.statusButtonShow = true;
        $scope.addDiemensionShow = true;
        $scope.showShipmentDimension = true;
        $scope.addConnectionShow = true;
        $scope.tableConnectionState = true;
        $scope.showErrorState = true;
        $scope.tableRateState = true;
        $scope.showRateErrorState = true;
        $scope.customerAgent = undefined;
        $scope.invoiceCreditNote = undefined;
        $scope.addShipment = true;
        $scope.moreAddressTab = 'moreInfo';
        $scope.servicesTab = 'services';
        $scope.showRateEdit = true;
        $scope.showDocumentEdit = false;
        $scope.docPartyAddressList = [];
        $scope.isFormValueDirty = false;
        $scope.isFormValueDirtyCount = 0;

    }


    $scope.addInit = function() {
        console.log("Init " + "method is invoked...");
        $scope.shipment = {};
        $scope.shipment.party = null;
        $scope.shipment.shipmentServiceList = [];
        $scope.shipment.partyAddress = {};
        $scope.shipmentServiceDetail = {};
        $scope.shipmentServiceDetail.authenticatedDocList = [];
        $scope.shipment.createdBy = $rootScope.userProfile.employee;
        $scope.shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
        $scope.shipment.whoRouted = false;
        $scope.shipment.directShipment = false;
        $scope.tableConnectionState = true;
        $scope.showErrorState = true;
        $scope.tableRateState = true;
        $scope.showRateErrorState = true;
        $scope.addShipment = false;
        $scope.focusFieldAfter2Sec('shipmentParty');
        $scope.addShipmentServiceRow();
    }


    $scope.editInit = function() {
        console.log("EditInit is called");
        $scope.cloneConnectionList = []; //for dynamic hawb report
        $scope.errorMap = new Map(); //for dynamic hawb report
        $scope.enableBookingRates = $rootScope.appMasterData['booking.rates.merged'];
        $scope.invoiceList = [];
        $scope.customerAgent = undefined;
        $scope.invoiceCreditNote = undefined;
        $scope.docPartyAddressList = [];
        $scope.shipment = attachToMasterService.configureData($scope.shipment);
        $scope.setOldDataVal();
    }


    $scope.goToShipmentService = function(shipmentUid) {

        var param = {
            shipmentUid: shipmentUid,
            fromState: $state.current.name,
            fromStateParams: JSON.stringify($stateParams)
        }

        $state.go('layout.viewCrmShipment', param);
    }
    $scope.goToConsol = function(consolUid) {
        var param = {
            consolUid: consolUid,
            fromState: $state.current.name,
            fromStateParams: JSON.stringify($stateParams)
        }

        $state.go('layout.viewAirConsol', param);
    }

    //aes coding



    //AES code

    $scope.aesSearchforService = function(service) {
        if (service.serviceUid != undefined && service.serviceUid != null) {
            $scope.searchDto = {};
            $scope.searchDto.serviceUid = service.serviceUid;
            AesSearch.query($scope.searchDto).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                $scope.finalArr = [];
                var tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    if (tempObj.id != undefined && tempObj.id != null) {
                        tempObj.status = 'Update'
                    }
                    $scope.finalArr.push(tempObj);
                });

                if ($scope.finalArr != null && $scope.finalArr.length > 0) {} else {
                    var tempObj = {};
                    tempObj.serviceUid = service.serviceUid;
                    tempObj.consolUid = service.consolUid;
                    tempObj.status = 'Generate';
                    if (service.documentList != null && service.documentList[0] != undefined && service.documentList[0].length != 0) {
                        tempObj.hawbNo = service.documentList[0].hawbNo;
                    }
                    tempObj.mawbNo = service.mawbNo;
                    tempObj.shipmentUid = service.shipmentUid;
                    $scope.finalArr.push(tempObj);
                }



                return $scope.finalArr;
            });
        } else {
            return;
        }
    }

    $scope.aesGenerateView = function(aesFile) {
        console.log($scope.shipment);
        var fromScreen = {};
        fromScreen = $state.current.name;
        if (aesFile != undefined && aesFile != null) {
            ServiceGetByUid.get({
                serviceUid: aesFile.serviceUid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.serviceObj = data.responseObject;
                    if ($scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || $scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                        $state.go('layout.aesView', {
                            aesId: aesFile.id,
                            fromScreen: $state.current.name,
                            shipmentUid: $scope.serviceObj.shipmentUid,
                            shipmentId: $scope.shipment.id
                        });
                    } else {
                        $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                        return false;
                    }
                }

            }, function(error) {
                console.log("Consol get Failed : " + error)
            });
        }
    }

    $scope.aesGenerateAdd = function(aesFile) {
        console.log("aesGenerateAdd method called");
        $rootScope.clientMessage = null;
        var fromScreen = {};
        fromScreen = $state.current.name;
        if (aesFile != undefined && aesFile != null) {
            ServiceGetByUid.get({
                serviceUid: aesFile.serviceUid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.serviceObj = data.responseObject;
                    if ($scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Received' || $scope.serviceObj.lastUpdatedStatus.serviceStatus == 'Generated') {
                        var navAesObj = {
                            selectedPageNumber: $scope.detailTab == 'active' ? $scope.activeSearchDto.selectedPageNumber : $scope.searchDto.selectedPageNumber,
                            status: $scope.detailTab,
                            totalRecord: $scope.detailTab == 'active' ? $scope.totalRecordView : $scope.totalRecord,
                            shipmentId: $scope.shipment.id,
                            serviceUid: $scope.serviceObj.serviceUid,
                            shipmentUid: $scope.serviceObj.shipmentUid,
                            consolUid: $scope.serviceObj.consolUid,
                            fromState: $state.current.name,
                            fromScreen: fromScreen,
                            mawbNo: $scope.serviceObj.mawbNo,
                            hawbNo: $scope.serviceObj.documentList[0].hawbNo
                        };
                        $state.go("layout.addAes", { navAesObj: navAesObj });
                    } else {
                        $rootScope.clientMessage = $rootScope.nls["ERR2012013"];
                        return false;
                    }
                }
            }, function(error) {
                console.log("Consol get Failed : " + error)
            });
        }
    };

    //Go to shipment link

    $scope.goToShipmentViewByServiceUid = function(serviceuid) {
        console.log("State  : ", $state)
        console.log("State Param  : ", $stateParams)
        console.log("serviceuid ", serviceuid)
        ShipmentIdByServiceUid.get({
                serviceuid: serviceuid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    var param = {
                        shipmentId: data.responseObject,
                        fromState: $state.current.name,
                        fromStateParams: JSON.stringify($stateParams)

                    }
                    $state.go("layout.viewCrmShipment", param);
                }
            },
            function(error) {
                console.log("Shipment get Failed : " + error)
            });

    }

    //aes coding



    //shipment flight schedule code starts
    var flightScheduleModal;
    $scope.flightmodel = function() {
        var shipmentServiceDetail = $scope.getShipmentService();
        $scope.flightSchedule = {};
        $scope.searchDto = {};
        $scope.searchDto.orderByType = 'asc';
        $scope.searchDto.sortByColumn = 'etd';
        if (shipmentServiceDetail.carrier != null && shipmentServiceDetail.carrier != undefined &&
            shipmentServiceDetail.carrier.id != null) {
            $scope.searchDto.carrier = shipmentServiceDetail.carrier.carrierName;
        }
        $scope.flightScheduleSearchResult = [];
        $scope.selectedFlightScheduleData = null;
        $scope.flightScheduleLimitArray = [10, 15, 20];
        $scope.flightSchedulePage = 0;
        $scope.flightScheduleLimit = 10;
        $scope.flightScheduleTotalRecord = 0;
        $scope.flightScheduleSearch();
        flightScheduleModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/shipment/shipment_flight_schedule_popup.html',
            show: false
        });
        $scope.navigateToNextField("flightSchedule.service");
        flightScheduleModal.$promise.then(flightScheduleModal.show)
    };

    $scope.datepickeropts = {
        clearLabel: 'Clear',
        locale: {
            applyClass: 'btn-green',
            applyLabel: "Apply",
            fromLabel: "From",
            //format: $rootScope.userProfile.selectedUserLocation.dateFormat,
            format: $rootScope.userProfile.selectedUserLocation.dateFormat ? $rootScope.userProfile.selectedUserLocation.dateFormat : "DD-MM-YYYY",
            toLabel: "To",
            //cancelLabel: 'Clear',
            //customRangeLabel: 'Custom range'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()]
        },
        eventHandlers: {
            'apply.daterangepicker': function(ev, picker) {
                $timeout(function() {

                    $scope.flightScheduleSearch();
                }, 2);
            },
            'cancel.daterangepicker': function(ev, picker) {
                $timeout(function() {
                    $scope.searchDto.eta = null;
                    $scope.searchDto.etd = null;
                    $scope.flightScheduleSearch();
                }, 2);

            }
        }
    };



    $scope.flightScheduleSearch = function() {
        console.log("flightScheduleSearch is called.");
        var shipmentServiceDetail = $scope.getShipmentService();
        if ($scope.searchDto == undefined || $scope.searchDto == null) {
            $scope.searchDto = {};
        }

        $scope.searchDto.selectedPageNumber = $scope.flightSchedulePage;
        $scope.searchDto.recordPerPage = $scope.flightScheduleLimit;

        if ($scope.searchDto.eta != null) {
            $scope.searchDto.eta.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.eta.startDate);
            $scope.searchDto.eta.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.eta.endDate);
        } else {
            $scope.searchDto.eta = null;
        }

        if ($scope.searchDto.etd != null) {
            $scope.searchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.startDate);
            $scope.searchDto.etd.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.etd.endDate);
        } else {
            $scope.searchDto.etd = null;
        }

        console.log("$scope.searchDto ", $scope.searchDto);


        FlightPlanSearchForShipment.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.flightScheduleTotalRecord = data.responseObject.totalRecord;
            console.log("$scope.flightScheduleTotalRecord ", $scope.flightScheduleTotalRecord);
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.flightSchedulePage * $scope.flightScheduleLimit);
                resultArr.push(tempObj);
                tempObj = {};
            });

            $scope.flightScheduleSearchResult = resultArr;

        });


    }

    $scope.toggleFlightPlan = function(flightSchedule) {
        if ($scope.flightDetail == 'child' + flightSchedule.id) {
            $scope.flightDetail = null;
            $scope.flightSchedule = null;
        } else {
            $scope.flightDetail = 'child' + flightSchedule.id;
            $scope.flightSchedule = flightSchedule;
        }
    }

    $scope.flightScheduleChangePage = function(param) {
        console.log("flightScheduleChangePage ", param);
        $scope.flightSchedulePage = param.page;
        $scope.flightScheduleLimit = param.size;
        $scope.flightScheduleSearch();

    }

    $scope.flightScheduleLimitChange = function(item) {
        console.log("flightScheduleLimitChange ", item);
        $scope.flightSchedulePage = 0;
        $scope.flightScheduleLimit = item;
        $scope.flightScheduleSearch();
    }

    $scope.flightScheduleSortChange = function(sortKey) {

        $scope.searchDto.sortByColumn = sortKey;
        if ($scope.searchDto.orderByType == 'asc') {
            $scope.searchDto.orderByType = 'desc';
        } else {
            $scope.searchDto.orderByType = 'asc';
        }
        $scope.flightScheduleSearch();

    }


    $scope.selectedFlightSchedule = function() {
        flightScheduleModal.hide();
        var shipmentServiceDetail = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex];
        if (shipmentServiceDetail != null && shipmentServiceDetail != undefined) {
            var index = $scope.shipment.shipmentServiceList.indexOf(shipmentServiceDetail);
            $scope.shipmentServiceDetail = $scope.shipment.shipmentServiceList[index];
            if ($scope.flightSchedule != null && $scope.flightSchedule != undefined) {
                $scope.shipmentServiceDetail.carrier = $scope.flightSchedule.carrierMaster;
                $scope.shipmentServiceDetail.routeNo = $scope.flightSchedule.flightNo;
                $scope.shipmentServiceDetail.eta = $rootScope.dateToString($scope.flightSchedule.eta);
                $scope.shipmentServiceDetail.etd = $rootScope.dateToString($scope.flightSchedule.etd);
                $scope.shipmentServiceDetail.scheduleUid = $scope.flightSchedule.scheduleId;
                $scope.shipmentServiceDetail.vesselId = $scope.flightSchedule.id;
            }
        }

        $rootScope.navigateToNextField('mawb' + $scope.selectedTabIndex);
    }

    $scope.getShipmentService = function() {
        return $scope.shipmentService = $scope.shipment.shipmentServiceList[$scope.selectedTabIndex];
    }

    $scope.cancelFlightSchedule = function() {
        $scope.flightSchedule = {};
        $rootScope.navigateToNextField('mawb' + $scope.selectedTabIndex);
    };

    $scope.newFlightSchedule = function(flightSchedule) {

        $scope.flightSchedule = flightSchedule;

    };



    //shipment flight schedule code ends





    $scope.goToQuotationView = function(quotationNo) {
        $rootScope.nav_src_bkref_key = new Date().toISOString();
        QuotationViewByQuotationNo.get({
            quotationNumber: quotationNo
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                var param = {
                    quotationId: data.responseObject.id,
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    fromShipmentId: $scope.shipment.id,
                    fromState: $state.current.name,
                    fromStateParams: JSON
                        .stringify($stateParams)
                }
                $state.go("layout.salesQuotationView", param);
            }
        }, function(error) {
            console.log("Consol get Failed : " + error)
        });
    }

    $scope.getShipment = function(shipmentId) {
        ShipmentGet.get({
                id: shipmentId
            }, function(data) {

                if (data.responseCode == 'ERR0') {
                    $scope.shipment = data.responseObject;
                    $rootScope.category = "Shipment";
                    $rootScope.serviceCodeForUnHistory = "";
                    $rootScope.orginAndDestinationUnHistory = "";
                    if ($scope.shipment != undefined && $scope.shipment != null) {
                        if ($scope.shipment.party != null && $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
                            $rootScope.subTitle = $scope.shipment.party.partyName;
                        } else {
                            $rootScope.subTitle = "Unkonwn Party";
                        }
                        if ($scope.shipment.origin != undefined && $scope.shipment.origin != null) {
                            $rootScope.serviceCodeForUnHistory = $scope.shipment.origin.portGroupCode;
                        }

                        if ($scope.shipment.destination != undefined && $scope.shipment.destination != null) {
                            $rootScope.orginAndDestinationUnHistory = $scope.shipment.destination.portGroupCode;
                        }

                    } else {
                        $rootScope.subTitle = "Unknown Party";
                        $rootScope.serviceCodeForUnHistory = "";
                        $rootScope.orginAndDestinationUnHistory = "";
                    }

                    var rHistoryObj = {
                        'title': 'Shipment Edit # ' + $scope.shipment.shipmentUid,
                        'subTitle': $rootScope.subTitle,
                        'stateName': $state.current.name,
                        'stateParam': JSON.stringify($stateParams),
                        'stateCategory': 'Shipment',
                        'serviceType': 'AIR',
                        'serviceCode': $rootScope.serviceCodeForUnHistory,
                        'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                        'showService': true
                    }

                    RecentHistorySaveService.form(rHistoryObj);
                    $scope.editInit();
                    $scope.initClickTabIndex($scope.shipment)
                } else {
                    $scope.shipment = null;
                }
            },
            function(error) {
                console.log("Shipment get Failed : " + error)
                $scope.shipment = null;
            });
    }




    var myTotalOtherModal = null;
    $scope.get_Shipment = function() {
        $scope.copyShipment = {};
        $scope.copyShipmentSearchResult = [];
        $scope.selectedCopyShipmentData = null;
        $scope.copyShipmentLimitArray = [10, 15, 20];
        $scope.copyShipmentPage = 0;
        $scope.copyShipmentLimit = 10;
        $scope.copyShipmentTotalRecord = 0;
        myTotalOtherModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/crm/shipment/views/get_shipment_popup.html',
            show: false
        });
        $scope.navigateToNextField("copy_shipment_service");
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);

    };

    $scope.validateCopyShipmentSearch = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 1 || validateCode == 0) {
            if ($scope.copyShipment.service == undefined || $scope.copyShipment.service == null || $scope.copyShipment.service.id == null) {

                $scope.errorMap.put("copyShipment.service", $rootScope.nls["ERR90599-1"]);
                return false;

            } else {

                if (ValidateUtil.isStatusBlocked($scope.copyShipment.service.status)) {
                    $scope.errorMap.put("copyShipment.service", $rootScope.nls["ERR90599-2"]);
                    return false;
                }


                if (ValidateUtil.isStatusHidden($scope.copyShipment.service.status)) {
                    $scope.errorMap.put("copyShipment.service", $rootScope.nls["ERR90599-3"]);
                    return false;
                }
            }

        }


        if (validateCode == 2 || validateCode == 0) {
            if ($scope.copyShipment.party == undefined || $scope.copyShipment.party == null || $scope.copyShipment.party.id == null) {

                $scope.errorMap.put("copyShipment.party", $rootScope.nls["ERR90599-4"]);
                return false;

            } else {

                if (ValidateUtil.isStatusBlocked($scope.copyShipment.party.status)) {
                    $scope.errorMap.put("copyShipment.party", $rootScope.nls["ERR90599-5"]);
                    return false;
                }


                if (ValidateUtil.isStatusHidden($scope.copyShipment.service.status)) {
                    $scope.errorMap.put("copyShipment.party", $rootScope.nls["ERR90599-6"]);
                    return false;
                }
            }
        }

        return true;
    }


    $scope.copyShipmentSearch_selectedService = function(nextFieldId) {
        if ($scope.validateCopyShipmentSearch(1)) {
            if (nextFieldId != undefined && nextFieldId != null) {
                $scope.navigateToNextField(nextFieldId);
            }
        }
    }

    $scope.copyShipmentSearch_selectedParty = function(nextFieldId) {
        if ($scope.validateCopyShipmentSearch(2)) {
            if (nextFieldId != undefined && nextFieldId != null) {
                $scope.navigateToNextField(nextFieldId);
            }
        }
    }

    $scope.copyShipment_search = function() {
        console.log("Search is called.");

        if ($scope.validateCopyShipmentSearch(0)) {

            $scope.copyShipment.selectedPageNumber = $scope.copyShipmentPage;
            $scope.copyShipment.recordPerPage = $scope.copyShipmentLimit;

            $scope.copyShipment.shipmentDate = {};

            if ($scope.copyShipment.shipmentDateTemp != null && $scope.copyShipment.shipmentDateTemp != "") {
                $scope.copyShipment.shipmentDate.startDate = $rootScope.sendApiStartDateTime($scope.copyShipment.shipmentDateTemp);
                $scope.copyShipment.shipmentDate.endDate = $rootScope.sendApiEndDateTime($scope.copyShipment.shipmentDateTemp);
            }

            $scope.copyShipment.serviceId = $scope.copyShipment.service.id;
            $scope.copyShipment.partyId = $scope.copyShipment.party.id;


            console.log("$scope.copyShipment ", $scope.copyShipment);
            CopyShipmentSearch.query($scope.copyShipment).$promise.then(function(data, status) {

                $scope.copyShipmentTotalRecord = data.responseObject.totalRecord;
                console.log("$scope.copyShipmentTotalRecord ", $scope.copyShipmentTotalRecord);
                $scope.copyShipmentSearchResult = data.responseObject.searchResult;

            });

        }
    }




    $scope.copyShipmentChangePage = function(param) {
        console.log("copyShipmentChangePage ", param);
        $scope.copyShipmentPage = param.page;
        $scope.copyShipmentLimit = param.size;
        $scope.copyShipment_search();
    }

    $scope.copyShipmentLimitChange = function(item) {
        console.log("copyShipmentLimitChange ", item);
        $scope.copyShipmentPage = 0;
        $scope.copyShipmentLimit = item;
        $scope.copyShipment_search();
    }

    //go to consol view


    $scope.goToCOnsolShipmentView = function(consolUid) {
        $rootScope.nav_src_bkref_key = new Date().toISOString();
        ConsolGetByUid.get({
            consolUid: consolUid
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                var param = {
                    consolId: data.responseObject.id,
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    fromShipmentId: $scope.shipment.id,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify($stateParams)
                }
                $state.go("layout.viewAirConsol", param);
            }
        }, function(error) {
            console.log("Consol get Failed : " + error)
        });
    }


    $scope.shipmentCopy = function(documentId, resultObject) {

        if (documentId == undefined || documentId == null || resultObject == undefined || resultObject == null || resultObject.legnth == 0) {
            console.log("Not Selected any shipment");
            return;
        } else {
            myTotalOtherModal.$promise.then(myTotalOtherModal.hide);
            $scope.spinner = true;
        }
        var data = null;
        for (var i = 0; i < resultObject.length; i++) {
            if (documentId == resultObject[i].id) {
                data = resultObject[i];
                break;
            }
        }
        if (!data) return false;
        $scope.copyShipmentObject = {
            documentId: data.id,
            serviceId: data.shipmentServiceDetailId,
            shipmentUid: data.shipmentUid
        };

        console.log("Copy Shipment Object ", $scope.copyShipmentObject);

        ShipmentGetFromUID.get({
                shipmentuid: $scope.copyShipmentObject.shipmentUid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.shipment = data.responseObject;
                    $scope.shipment.id = null;
                    $scope.shipment.versionLock = 0;
                    $scope.shipment.shipmentUid = null;
                    $scope.shipment.shipmentReqDate = null;
                    $scope.shipment.lastUpdatedStatus = null;
                    $scope.shipment.shipmentStatusList = null;
                    $scope.shipment.directShipment = false;
                    $scope.shipment.whoRouted = $scope.shipment.whoRouted == 'Self' ? false : true;
                    $scope.shipment.partyAddress.id = null;
                    $scope.shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
                    if ($scope.shipment.project != undefined && $scope.shipment.project != null) {
                        $scope.shipment.project = null;
                        $scope.shipment.projectCode = null;
                    }
                    if ($scope.shipment.commodity != undefined && $scope.shipment.commodity != null) {
                        $scope.shipment.commodity = null;
                        $scope.shipment.commodityCode = null;
                    }
                    $scope.shipment.partyAddress.id = null;
                    if ($scope.shipment.shipmentServiceList != undefined &&
                        $scope.shipment.shipmentServiceList != null &&
                        $scope.shipment.shipmentServiceList.length > 0) {

                        var tempService = null;
                        for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {

                            if ($scope.shipment.shipmentServiceList[i].id === $scope.copyShipmentObject.serviceId) {

                                $scope.shipment.shipmentServiceList[i].whoRouted = $scope.shipment.shipmentServiceList[i].whoRouted == 'Self' ? false : true;
                                $scope.shipment.shipmentServiceList[i].ppcc = $scope.shipment.shipmentServiceList[i].ppcc == 'Prepaid' ? false : true;
                                $scope.shipment.shipmentServiceList[i].dimensionUnit = false;
                                $scope.shipment.shipmentServiceList[i].serviceReqDate = $rootScope.sendApiDateAndTime(new Date());
                                $scope.shipment.shipmentServiceList[i].isMinimumShipment = false;
                                $scope.shipment.shipmentServiceList[i].id = null;
                                $scope.shipment.shipmentServiceList[i].taskId = null;
                                $scope.shipment.shipmentServiceList[i].processInstanceId = null;
                                $scope.shipment.shipmentServiceList[i].versionLock = 0;
                                $scope.shipment.shipmentServiceList[i].systemTrack = null;
                                $scope.shipment.shipmentServiceList[i].serviceUid = null;
                                $scope.shipment.shipmentServiceList[i].shipmentUid = null;
                                $scope.shipment.shipmentServiceList[i].consolUid = null;
                                $scope.shipment.shipmentServiceList[i].quotationUid = null;
                                $scope.shipment.shipmentServiceList[i].mawbNo = null;
                                $scope.shipment.shipmentServiceList[i].mawbDate = null;
                                $scope.shipment.shipmentServiceList[i].pickUpDeliveryPoint = {};
                                $scope.shipment.shipmentServiceList[i].serviceStatusList = null;
                                $scope.shipment.shipmentServiceList[i].lastUpdatedStatus = null;
                                $scope.shipment.shipmentServiceList[i].shipmentServiceTriggerList = null;
                                $scope.shipment.shipmentServiceList[i].shipmentAttachmentList = null;
                                $scope.shipment.shipmentServiceList[i].connectionList = null;
                                $scope.shipment.shipmentServiceList[i].eventList = null;
                                $scope.shipment.shipmentServiceList[i].referenceList = null;
                                $scope.shipment.shipmentServiceList[i].hawbNo = null;
                                $scope.shipment.shipmentServiceList[i].bookedPieces = null;
                                $scope.shipment.shipmentServiceList[i].bookedGrossWeightUnitKg = null;
                                $scope.shipment.shipmentServiceList[i].bookedVolumeWeightUnitKg = null;
                                $scope.shipment.shipmentServiceList[i].bookedVolumeUnitCbm = null;
                                $scope.shipment.shipmentServiceList[i].bookedChargeableUnit = null;
                                $scope.shipment.shipmentServiceList[i].bookedGrossWeightUnitPound = null;
                                $scope.shipment.shipmentServiceList[i].bookedVolumeWeightUnitPound = null;
                                $scope.shipment.shipmentServiceList[i].exportRef = null;
                                $scope.shipment.shipmentServiceList[i].importRef = null;
                                $scope.shipment.shipmentServiceList[i].carrier = null;

                                $scope.shipment.shipmentServiceList[i].carrier = null;
                                $scope.shipment.shipmentServiceList[i].carrierCode = null;
                                $scope.shipment.shipmentServiceList[i].etd = null;
                                $scope.shipment.shipmentServiceList[i].eta = null;

                                $scope.shipment.shipmentServiceList[i].routeNo = null;
                                $scope.shipment.shipmentServiceList[i].mawbNo = null;
                                $scope.shipment.shipmentServiceList[i].mawbDate = null;
                                $scope.shipment.shipmentServiceList[i].coLoader = null;
                                $scope.shipment.shipmentServiceList[i].coLoaderCode = null;

                                $scope.shipment.shipmentServiceList[i].division = null;
                                $scope.shipment.shipmentServiceList[i].divisionCode = null;


                                $scope.shipment.shipmentServiceList[i].connectionList = [{ connectionStatus: 'Planned' }];
                                $scope.shipment.shipmentServiceList[i].shipmentAttachmentList = [{}];
                                $scope.shipment.shipmentServiceList[i].eventList = [{ isCompleted: 'Yes', followUpRequired: 'No' }];
                                $scope.shipment.shipmentServiceList[i].referenceList = [{}];
                                //$scope.shipment.shipmentServiceList[i].shipmentServiceTriggerList = [{}];
                                $scope.shipment.shipmentServiceList[i].authenticatedDocList = [{}];






                                $scope.shipment.shipmentServiceList[i].attachConfig = {
                                    "isEdit": true,
                                    "isDelete": true,
                                    "page": "shipment",
                                    "columnDefs": [

                                        {
                                            "name": "Document Name",
                                            "model": "documentMaster.documentName",
                                            "type": "text"
                                        }, {
                                            "name": "Reference #",
                                            "model": "refNo",
                                            "type": "text"
                                        }, {
                                            "name": "Customs",
                                            "model": "customs",
                                            "type": "text"
                                        }, {
                                            "name": "Unprotected File Name",
                                            "model": "unprotectedFileName",
                                            "type": "file"
                                        }, {
                                            "name": "Protected File Name",
                                            "model": "protectedFileName",
                                            "type": "file"
                                        }, {
                                            "name": "Actions",
                                            "model": "action",
                                            "type": "action"
                                        }
                                    ]
                                }



                                if ($scope.shipment.shipmentServiceList[i].shipmentChargeList != undefined &&
                                    $scope.shipment.shipmentServiceList[i].shipmentChargeList != null &&
                                    $scope.shipment.shipmentServiceList[i].shipmentChargeList.length > 0) {
                                    for (var j = 0; j < $scope.shipment.shipmentServiceList[i].shipmentChargeList.length; j++) {
                                        $scope.shipment.shipmentServiceList[i].shipmentChargeList[j].id = null;
                                    }
                                }







                                if ($scope.shipment.shipmentServiceList[i].documentList != undefined &&
                                    $scope.shipment.shipmentServiceList[i].documentList != null &&
                                    $scope.shipment.shipmentServiceList[i].documentList.length > 0) {

                                    for (var j = 0; j < $scope.shipment.shipmentServiceList[i].documentList.length; j++) {
                                        $scope.shipment.shipmentServiceList[i].documentList[j].id = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].versionLock = 0;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].systemTrack = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].documentReqDate = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].shipmentUid = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].serviceUid = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].documentNo = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].mawbNo = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].hawbNo = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].dimensionUnit = false;


                                        $scope.shipment.shipmentServiceList[i].documentList[j].grossWeight = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].chargebleWeight = null;

                                        $scope.shipment.shipmentServiceList[i].documentList[j].volumeWeightInPound = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].grossWeightInPound = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].bookedVolumeUnitCbm = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].commodityDescription = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].rateClass = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].marksAndNo = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].blReceivedPerson = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].blReceivedPersonCode = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].blReceivedDate = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].eta = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].etd = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].chargebleWeight = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].carrier = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].noOfPieces = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].commodityDescription = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].hawbReceivedBy = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].hawbReceivedOn = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].volumeWeight = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].routeNo = null;


                                        if ($scope.shipment.shipmentServiceList[i].documentList[j].shipper != undefined && $scope.shipment.shipmentServiceList[i].documentList[j].shipper != null && $scope.shipment.shipmentServiceList[i].documentList[j].shipper.id != null && $scope.shipment.shipmentServiceList[i].documentList[j].shipperAddress.id != null) {
                                            $scope.shipment.shipmentServiceList[i].documentList[j].shipperAddress.id = null
                                        }

                                        if ($scope.shipment.shipmentServiceList[i].documentList[j].forwarder != undefined && $scope.shipment.shipmentServiceList[i].documentList[j].forwarder != null && $scope.shipment.shipmentServiceList[i].documentList[j].forwarder.id != null && $scope.shipment.shipmentServiceList[i].documentList[j].forwarderAddress.id != null) {
                                            $scope.shipment.shipmentServiceList[i].documentList[j].forwarderAddress.id = null
                                        }

                                        if ($scope.shipment.shipmentServiceList[i].documentList[j].consignee != undefined && $scope.shipment.shipmentServiceList[i].documentList[j].consignee != null && $scope.shipment.shipmentServiceList[i].documentList[j].consignee.id != null && $scope.shipment.shipmentServiceList[i].documentList[j].consigneeAddress.id != null) {
                                            $scope.shipment.shipmentServiceList[i].documentList[j].consigneeAddress.id = null;
                                        }


                                        $scope.shipment.shipmentServiceList[i].documentList[j].firstNotify = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].firstNotifyAddress = null

                                        $scope.shipment.shipmentServiceList[i].documentList[j].secondNotify = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].secondNotifyAddress = null

                                        $scope.shipment.shipmentServiceList[i].documentList[j].agent = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].agentAddress = null


                                        $scope.shipment.shipmentServiceList[i].documentList[j].issuingAgent = null;
                                        $scope.shipment.shipmentServiceList[i].documentList[j].issuingAgentAddress = null;

                                        $scope.shipment.shipmentServiceList[i].documentList[j].dimensionList = [{}];
                                    }

                                }


                                tempService = $scope.shipment.shipmentServiceList[i];
                            } else {
                                console.log("Removing the service....");
                                $scope.shipment.shipmentServiceList.splice(i, 1);
                            }
                        }

                        $scope.shipment.shipmentServiceList = [];
                        $scope.shipment.shipmentServiceList.push(tempService);
                    }
                    var stateCount = 0;

                    if ($stateParams.count != undefined &&
                        $stateParams.count != null) {
                        stateCount = $stateParams.count + 1;
                    }
                    $rootScope.copyShipmentData = $scope.shipment;
                    $scope.setActiveObj();
                    $scope.spinner = false;
                    $state.go('layout.addCrmShipment', { count: stateCount, forCopyShipment: true });
                    $rootScope.navigateToNextField('shipmentParty');

                } else {
                    $scope.shipment = null;
                }
            },
            function(error) {
                console.log("Shipment get Failed : " + error)
                $scope.shipment = null;
            });


    }













    $scope.showAddressAuth = function() {
        $scope.showPartyAddressList = false;
    }

    /*AUTHENTICATED DOC POPUP*/
    $scope.authInfoEdit = function(authenticatedDoc, parent, child) {
        var newScope = $scope.$new();
        newScope.authenticatedDoc = authenticatedDoc;
        newScope.sideview = false;
        newScope.parent = parent;
        newScope.child = child;
        var myOtherModal = $modal({ scope: newScope, backDrop: 'static', templateUrl: 'app/components/crm/shipment/views/auth_doc_edit_moreinfo_popup.html', show: false });
        myOtherModal.$promise.then(myOtherModal.show);
        $rootScope.navigateToNextField('authDocShipper' + parent + '_' + child);
    };
    $scope.showsidebar = function() {
        $scope.sideview = true;
    }
    $scope.cancelRoleList = function() {
        $scope.rolesList = false;
    }
    $scope.cancelinfo = function() {
        $scope.sideview = false;
    }
    $scope.addRoles = false;
    $scope.panelTitle = "Select Address"

    $scope.searchText = "";

    $scope.addConfig = {
        search: false,
        showCode: false,
        columns: [
            { "title": "addressType", seperator: false },
            { "title": "contactPerson", seperator: true }
        ]

    }
    $scope.PortRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    };


    //authenticated docs tab code started

    //show errors
    var myOtherModalAuthDoc = $modal({ scope: $scope, templateUrl: 'errorPopUpAuthDoc.html', show: false, keyboard: true });
    var errorOnRowIndex = null;
    $scope.errorShowAuthDocs = function(errorObj, index) {
        console.log("errorshow", errorObj, index);
        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModalAuthDoc.$promise.then(myOtherModalAuthDoc.show);
    };

    $scope.ajaxAuthDocsOriginEvent = function(object, authenticatedDoc) {
        console.log(" Ajax origin Event method : ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {


                    var resultList = data.responseObject.searchResult;
                    $scope.authDocsOriginList = [];
                    if (resultList != null && resultList.length != 0) {
                        if (authenticatedDoc.destination == undefined || authenticatedDoc.destination == null) {
                            $scope.authDocsOriginList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != authenticatedDoc.destination.id) {
                                    $scope.authDocsOriginList.push(resultList[i]);
                                }

                            }
                        }
                    }

                    console.log("$scope.authDocsOriginList ", $scope.authDocsOriginList);
                    return $scope.authDocsOriginList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching originList');
            });
    };

    $scope.ajaxAuthDocsDestinationEvent = function(object, authenticatedDoc) {
        console.log(" Ajax Destination Event method : ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {

                    var resultList = data.responseObject.searchResult;
                    $scope.authDocsDestinationList = [];
                    if (resultList != null && resultList.length != 0) {
                        if (authenticatedDoc.origin == undefined || authenticatedDoc.origin == null) {
                            $scope.authDocsDestinationList = resultList;
                        } else {
                            for (var i = 0; i < resultList.length; i++) {

                                if (resultList[i].id != authenticatedDoc.origin.id) {
                                    $scope.authDocsDestinationList.push(resultList[i]);
                                }

                            }
                        }
                    }

                    console.log("$scope.authDocsOriginList ", $scope.authDocsDestinationList);
                    return $scope.authDocsDestinationList;


                }
            },
            function(errResponse) {
                console.error('Error while fetching authDocsDestinationList');
            });
    };

    $scope.callValidateAuthenticatedDocs = function(shipmentService) {
        var isValid = true;
        if (shipmentService.authenticatedDocList == undefined || shipmentService.authenticatedDocList == null || shipmentService.authenticatedDocList.length == 0) { return isValid; }
        var index = shipmentService.authenticatedDocList.length;

        var fineArr = [];
        $scope.secondArr = [];
        angular.forEach(shipmentService.authenticatedDocList, function(dataObj, index) {
            $scope.secondArr[index] = {};
            var validResult = validateAuthenticatedDocs(dataObj, index, "row");
            if (validResult) {
                fineArr.push(1);
                $scope.secondArr[index].errRow = false;

            } else {
                $scope.secondArr[index].errRow = true;
                fineArr.push(0);
                isValid = false;
            }

        });

        return isValid;
    }


    function isEmptyRow(obj) {

        var isempty = true;

        console.log("empty obj*******", obj);

        $scope.emptyObjRef = {};
        console.log("$scope.emptyObjRef*****",
            $scope.emptyObjRef);
        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {

            //check the new key is empty or not
            if (obj[k[i]]) {
                // check the default object contains the key
                if (!$scope.emptyObjRef.hasOwnProperty(k[i])) {
                    isempty = false;
                    break;
                }
                //check the type is object or not
                if (angular.isObject(obj[k[i]])) {
                    if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]].id != null && obj[k[i]].id != $scope.emptyObjRef[k[i]].id) {
                        isempty = false;
                        break;
                    }
                } else {
                    if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                        isempty = false;
                        break;

                    }
                }
            }
        }
        console.log("empty check *****", isempty)
        return isempty;
    }


    //add NewAuthDocs 
    $scope.addNewAuthDocs = function(shipmentService) {
        if (shipmentService.authenticatedDocList == undefined || shipmentService.authenticatedDocList == null || shipmentService.authenticatedDocList.length == 0) {
            shipmentService.authenticatedDocList = [];
            shipmentService.authenticatedDocList.push({});

        }
        console.log($scope.authenticatedDoc);
        var index = shipmentService.authenticatedDocList.length;
        var fineArr = [];
        $scope.secondArr = [];
        angular.forEach(shipmentService.authenticatedDocList, function(dataObj, index) {
            $scope.secondArr[index] = {};
            var validResult = validateAuthenticatedDocs(dataObj, index, "row");
            if (validResult) {
                fineArr.push(1);
                $scope.secondArr[index].errRow = false;

            } else {
                $scope.secondArr[index].errRow = true;
                fineArr.push(0);
            }
            //console.log();
        });
        if (fineArr.indexOf(0) < 0) {
            shipmentService.authenticatedDocList.push({});
            $scope.secondArr.push({ "errRow": false });
            $scope.indexFocus = null;
            if ($scope.tableState) {
                $scope.tableState({ param: true });
            }
        } else {
            if ($scope.tableState) {
                $scope.tableState({ param: false });
            }
        }

        $rootScope.navigateToNextField("documentNo" + index);

    };

    var validateAuthenticatedDocs = function(dataObj, index, type) {

        var errorArr = [];
        if (type == "row") {
            $scope.secondArr[index].errTextArr = [];
        }
        //documnetNo
        if (dataObj.documentNo == null || dataObj.documentNo == undefined ||
            dataObj.documentNo == "") {
            console.log("documentNo is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].documentNo = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05500"]);
            }

            errorArr.push(0);
        }

        //date
        if (dataObj.date == null || dataObj.date == undefined ||
            dataObj.date == "") {
            console.log("Date is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].date = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05503"]);
            }

            errorArr.push(0);
        }



        //origin
        if (dataObj.origin == null || dataObj.origin == undefined || dataObj.origin == "" || dataObj.origin.id == undefined ||
            dataObj.origin.id == null || dataObj.origin.id == "") {
            console.log("Origin is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].origin = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05504"]);
            }

            errorArr.push(0);
        } else {

            if (dataObj.origin.status == 'Block') {
                console.log("Selected Origin is Blocked.");
                if (type == "row") {
                    $scope.secondArr[index].origin = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05505"]);
                }

                errorArr.push(0);

            } else if (dataObj.origin.status == 'Hide') {
                console.log("Selected Origin is Hidden.");
                if (type == "row") {
                    $scope.secondArr[index].origin = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05506"]);
                }

                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].origin = false;

                }

                errorArr.push(1);
            }


        }

        //destination
        if (dataObj.destination == null || dataObj.destination == undefined || dataObj.destination == "" || dataObj.destination.id == undefined ||
            dataObj.destination.id == null || dataObj.destination.id == "") {
            console.log("Destination is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].destination = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05509"]);
            }

            errorArr.push(0);
        } else {

            if (dataObj.destination.status == 'Block') {
                console.log("Selected Destination is Blocked.");
                if (type == "row") {
                    $scope.secondArr[index].destination = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05510"]);
                }

                errorArr.push(0);

            } else if (dataObj.destination.status == 'Hide') {
                console.log("Selected Origin is Hidden.");
                if (type == "row") {
                    $scope.secondArr[index].destination = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05511"]);
                }

                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].destination = false;

                }

                errorArr.push(1);
            }


        }

        //noOfPiece
        if (dataObj.noOfPiece == undefined &&
            dataObj.noOfPiece == null &&
            dataObj.noOfPiece == "") {
            console.log("No Of Piece is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].noOfPiece = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05570"]);
            }
            errorArr.push(0);
        } else {
            if (isNaN(parseFloat(dataObj.noOfPiece))) {
                if (type == "row") {
                    $scope.secondArr[index].noOfPiece = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05513"]);
                }
                errorArr.push(0);
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES, dataObj.noOfPiece)) {
                if (type == "row") {
                    $scope.secondArr[index].noOfPiece = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05513"]);
                }
                errorArr.push(0);
            } else if (dataObj.noOfPiece == 0) {
                if (type == "row") {
                    $scope.secondArr[index].noOfPiece = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05512"]);
                }
                errorArr.push(0);
            } else if (dataObj.noOfPiece < 0 || dataObj.noOfPiece >= 99999999999) {
                if (type == "row") {
                    $scope.secondArr[index].noOfPiece = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05513"]);
                }
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].noOfPiece = false;
                }
                errorArr.push(1);
            }
        }

        //gross weight

        if (dataObj.grossWeight == undefined && dataObj.grossWeight == null && dataObj.grossWeight == "") {
            console.log("Gross Weight is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].grossWeight = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05568"]);
            }
            errorArr.push(0);
        } else {
            if (isNaN(parseFloat(dataObj.grossWeight))) {
                if (type == "row") {
                    $scope.secondArr[index].grossWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05501"]);
                }
                errorArr.push(0);
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG, dataObj.grossWeight)) {
                if (type == "row") {
                    $scope.secondArr[index].grossWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05501"]);
                }
                errorArr.push(0);
            } else if (dataObj.grossWeight == 0) {
                if (type == "row") {
                    $scope.secondArr[index].grossWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05508"]);
                }
                errorArr.push(0);
            } else if (dataObj.grossWeight < 0 || dataObj.grossWeight >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].grossWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05501"]);
                }
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].grossWeight = false;
                }
                errorArr.push(1);
            }
        }


        //volumne weight
        if (dataObj.volWeight == undefined && dataObj.volWeight == null && dataObj.volWeight == "") {
            console.log("Volume Weight is Mandatory")
            if (type == "row") {
                $scope.secondArr[index].volWeight = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05569"]);
            }
            errorArr.push(0);
        } else {
            if (isNaN(parseFloat(dataObj.volWeight))) {
                if (type == "row") {
                    $scope.secondArr[index].volWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05502"]);
                }
                errorArr.push(0);
            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG, dataObj.volWeight)) {
                if (type == "row") {
                    $scope.secondArr[index].volWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05502"]);
                }
                errorArr.push(0);
            } else if (dataObj.volWeight == 0) {
                if (type == "row") {
                    $scope.secondArr[index].volWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05507"]);
                }
                errorArr.push(0);
            } else if (dataObj.volWeight < 0 || dataObj.volWeight >= 99999999999.99) {
                if (type == "row") {
                    $scope.secondArr[index].volWeight = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR05502"]);
                }
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].volWeight = false;
                }
                errorArr.push(1);
            }
        }

        if (errorArr.indexOf(0) < 0) {
            return true
        } else {
            return false;
        }
    }

    $scope.deleteAuthenticatedDoc = function(shipmentServiceDetail, index) {

        shipmentServiceDetail.authenticatedDocList
            .splice(index, 1);

    }

    $scope.selectedAuthDocsShipperEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
        console.log("selectedAuthDocsShipperEvent")
        if ($scope.validateAuthDocsMore(1, authenticatedDoc, pIndex, index)) {
            $scope.choosePartyAddress(authenticatedDoc, 'shipper');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAuthConsigneEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
        console.log("selectedAuthConsigneEvent")
        if ($scope.validateAuthDocsMore(3, authenticatedDoc, pIndex, index)) {
            $scope.choosePartyAddress(authenticatedDoc, 'consignee');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    $scope.selectedAuthFirstNotifyEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
        console.log("selectedAuthFirstNotifyEvent")
        if ($scope.validateAuthDocsMore(5, authenticatedDoc, pIndex, index)) {
            $scope.choosePartyAddress(authenticatedDoc, 'firstNotify');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }
    $scope.selectedAuthForwarderEvent = function(authenticatedDoc, pIndex, index, nextFocus) {
        console.log("selectedAuthForwarderEvent")
        if ($scope.validateAuthDocsMore(7, authenticatedDoc, pIndex, index)) {
            $scope.choosePartyAddress(authenticatedDoc, 'forwarder');
            $rootScope.navigateToNextField(nextFocus + pIndex + "_" + index);
        }

    }

    //validateAuthDocsMore
    $scope.validateAuthDocsMore = function(validateCode, authenticatedDoc, pIndex, index) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if (authenticatedDoc.shipper == undefined ||
                authenticatedDoc.shipper == null ||
                authenticatedDoc.shipper == "" ||
                authenticatedDoc.shipper.id == undefined) {
                // $scope.errorMap.put("authDocShipper" + pIndex + "_" + index, $rootScope.nls["ERR05516"]);
                // $scope.shipmentTab = 'AuthenticatedDocs';
                //return false;

            } else {

                if (ValidateUtil.isStatusBlocked(authenticatedDoc.shipper.status)) {
                    $scope.errorMap.put("authDocShipper" + pIndex + "_" + index, $rootScope.nls["ERR05517"]);
                    authenticatedDoc.shipper = {};
                    authenticatedDoc.shipperAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false

                }
                if (ValidateUtil.isStatusHidden(authenticatedDoc.shipper.status)) {
                    $scope.errorMap.put("authDocShipper" + pIndex + "_" + index, $rootScope.nls["ERR05518"]);
                    authenticatedDoc.shipper = {};
                    authenticatedDoc.shipperAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false

                }
            }

        }

        if (validateCode == 0 || validateCode == 2) {
            if (authenticatedDoc.shipper != null && authenticatedDoc.shipper != undefined && authenticatedDoc.shipper.id != null) {
                if (authenticatedDoc.shipperAddress.addressLine1 == undefined || authenticatedDoc.shipperAddress.addressLine1 == null || authenticatedDoc.shipperAddress.addressLine1 == "") {
                    $scope.errorMap.put("authenticatedDoc.shipperAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05520"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                }
                /*if (authenticatedDoc.shipperAddress.addressLine2 == null || authenticatedDoc.shipperAddress.addressLine2 == "" || authenticatedDoc.shipperAddress.addressLine2 == undefined) {
                    $scope.errorMap.put("authenticatedDoc.shipperAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05522"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                } */
            }
        }
        if (validateCode == 0 || validateCode == 3) {

            if (authenticatedDoc.consignee == undefined || authenticatedDoc.consignee == null || authenticatedDoc.consignee == "" || authenticatedDoc.consignee.id == undefined) {
                // $scope.errorMap.put("authConsignee" + pIndex + "_" + index, $rootScope.nls["ERR05542"]);
                // $scope.shipmentTab = 'AuthenticatedDocs';
                // return false;
            } else {
                if (ValidateUtil.isStatusBlocked(authenticatedDoc.consignee.status)) {
                    $scope.errorMap.put("authConsignee" + pIndex + "_" + index, $rootScope.nls["ERR05543"]);
                    authenticatedDoc.consignee = {};
                    authenticatedDoc.consigneeAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false
                }
                if (ValidateUtil.isStatusHidden(authenticatedDoc.consignee.status)) {
                    $scope.errorMap.put("authConsignee" + pIndex + "_" + index, $rootScope.nls["ERR05544"]);
                    authenticatedDoc.consignee = {};
                    authenticatedDoc.consigneeAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false
                }
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            if (authenticatedDoc.consignee != null && authenticatedDoc.consignee != undefined && authenticatedDoc.consignee.id != null) {
                if (authenticatedDoc.consigneeAddress.addressLine1 == undefined || authenticatedDoc.consigneeAddress.addressLine1 == null || authenticatedDoc.consigneeAddress.addressLine1 == "") {
                    $scope.errorMap.put("authenticatedDoc.consigneeAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05546"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                }

                /*if (authenticatedDoc.consigneeAddress.addressLine2 == null || authenticatedDoc.consigneeAddress.addressLine2 == "" || authenticatedDoc.consigneeAddress.addressLine2 == undefined) {
                    $scope.errorMap.put("authenticatedDoc.consigneeAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05548"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                }*/
            }
        }
        if (validateCode == 0 || validateCode == 5) {
            if (authenticatedDoc.firstNotify == undefined ||
                authenticatedDoc.firstNotify == null ||
                authenticatedDoc.firstNotify == "" ||
                authenticatedDoc.firstNotify.id == undefined) {
                //$scope.errorMap.put("authFirstNotify"+pIndex+"_"+index,$rootScope.nls["ERR05555"]);
                //$scope.shipmentTab='documents';
                //return false;

            } else {
                if (ValidateUtil.isStatusBlocked(authenticatedDoc.firstNotify.status)) {
                    $scope.errorMap.put("authFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR05556"]);
                    authenticatedDoc.firstNotify = {};
                    authenticatedDoc.firstNotifyAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false
                }
                if (ValidateUtil.isStatusHidden(authenticatedDoc.firstNotify.status)) {
                    $scope.errorMap.put("authFirstNotify" + pIndex + "_" + index, $rootScope.nls["ERR05557"]);
                    authenticatedDoc.firstNotify = {};
                    authenticatedDoc.firstNotifyAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false
                }
            }

        }

        if (validateCode == 0 || validateCode == 6) {
            if (authenticatedDoc.firstNotify != null && authenticatedDoc.firstNotify != undefined && authenticatedDoc.firstNotify.id != null) {
                if (authenticatedDoc.firstNotifyAddress.addressLine1 == undefined || authenticatedDoc.firstNotifyAddress.addressLine1 == null || authenticatedDoc.firstNotifyAddress.addressLine1 == "") {
                    $scope.errorMap.put("authenticatedDoc.firstNotifyAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05559"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                }

                /* if (authenticatedDoc.firstNotifyAddress.addressLine2 == null || authenticatedDoc.firstNotifyAddress.addressLine2 == "" || authenticatedDoc.firstNotifyAddress.addressLine2 == undefined) {
                     $scope.errorMap.put("authenticatedDoc.firstNotifyAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05561"]);
                     $scope.shipmentTab = 'AuthenticatedDocs';
                     return false;
                 } */
            }
        }

        //Forwarder
        if (validateCode == 0 || validateCode == 7) {


            if (authenticatedDoc.forwarder == undefined || authenticatedDoc.forwarder == null || authenticatedDoc.forwarder == "" || authenticatedDoc.forwarder.id == undefined) {
                // $scope.errorMap.put("authForwarder" + pIndex + "_" + index, $rootScope.nls["ERR05555"]);
                // $scope.shipmentTab = 'AuthenticatedDocs';
                //return false;
            } else {
                if (ValidateUtil.isStatusBlocked(authenticatedDoc.forwarder.status)) {
                    $scope.errorMap.put("authForwarder" + pIndex + "_" + index, $rootScope.nls["ERR05530"]);
                    authenticatedDoc.forwarder = {};
                    authenticatedDoc.forwarderAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false

                }

                if (ValidateUtil.isStatusHidden(authenticatedDoc.forwarder.status)) {
                    $scope.errorMap.put("authForwarder" + pIndex + "_" + index, $rootScope.nls["ERR05531"]);
                    authenticatedDoc.forwarder = null;
                    authenticatedDoc.forwarderAddress = {};
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false

                }
            }
        }


        if (validateCode == 0 || validateCode == 8) {

            if (authenticatedDoc.forwarder != null && authenticatedDoc.forwarder != undefined && authenticatedDoc.forwarder.id != null) {

                if (authenticatedDoc.forwarderAddress.addressLine1 == undefined || authenticatedDoc.forwarderAddress.addressLine1 == null ||
                    authenticatedDoc.forwarderAddress.addressLine1 == "") {
                    $scope.errorMap.put("authenticatedDoc.forwarderAddress.addressLine1" + pIndex + "_" + index, $rootScope.nls["ERR05533"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                }

                /*if (authenticatedDoc.forwarderAddress.addressLine2 == undefined || authenticatedDoc.forwarderAddress.addressLine2 == null || authenticatedDoc.forwarderAddress.addressLine2 == "") {
                    $scope.errorMap.put("authenticatedDoc.forwarderAddress.addressLine2" + pIndex + "_" + index, $rootScope.nls["ERR05535"]);
                    $scope.shipmentTab = 'AuthenticatedDocs';
                    return false;
                }*/
            }
        }
        return true;
    }

    $scope.selectedAuthDocsPartyAddress = function(obj, authenticatedDoc, parent, child) {

        var authenticatedDoc = authenticatedDoc;
        if ($scope.partyAddressVal == 'shipperAddress') {
            authenticatedDoc.shipperAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.shipperAddress, authenticatedDoc.shipper, obj);
            $scope.validateAuthDocsMore(2, authenticatedDoc, parent, child);
        }
        if ($scope.partyAddressVal == 'consigneeAddress') {
            authenticatedDoc.consigneeAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.consigneeAddress, authenticatedDoc.consignee, obj);
            $scope.validateAuthDocsMore(4, authenticatedDoc, parent, child);
        }
        if ($scope.partyAddressVal == 'firstNotifyAddress') {
            authenticatedDoc.firstNotifyAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.firstNotifyAddress, authenticatedDoc.firstNotify, obj);
            $scope.validateAuthDocsMore(6, authenticatedDoc, parent, child);
        }
        if ($scope.partyAddressVal == 'forwarderAddress') {
            authenticatedDoc.forwarderAddress = addressJoiner.joinAddressLineByLine(authenticatedDoc.forwarderAddress, authenticatedDoc.forwarder, obj);
            $scope.validateAuthDocsMore(8, authenticatedDoc, parent, child);
        }
    };


    $scope.documentIdList = [];
    $scope.reportList = [];
    $scope.reportDetailData = {};
    var allRepoVar = {};
    $scope.allRepoVarModel = {};
    $scope.allRepoVarModel.allReportSelected = false;
    $scope.reportLoadingImage = {};
    $scope.reportLoadingImage.status = false;



    $scope.reportListMaking = function(sd, resourceRefId) {
        if ($scope.hasAccess('CRM_SHIPMENT_SERVICE_REPORTS_VIEW')) {
            $scope.reportSpinner = true;
            $scope.reportList = [];
            console.log("reportListMaking  Tab :: ", resourceRefId);
            $scope.moreAddressTab = 'reports';
            ShipmentDocumentIdList.get({
                    serviceId: resourceRefId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.documentIdList = data.responseObject;
                        console.log("$scope.documentIdList -- ", $scope.documentIdList);

                        if ($scope.documentIdList.length > 0) {

                            for (var i = 0; i < $scope.documentIdList.length; i++) {
                                if (sd.serviceMaster.importExport == 'Import') {
                                    $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "CARGO_ARRICAL_NOTICE", reportDisplayName: 'Cargo Arrival Notice | Document # ' + $scope.documentIdList[i].documentNo };
                                    $scope.reportList.push($scope.reportDetailData);
                                } else {

                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "HAWB", reportDisplayName: 'HAWB | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "HAWB_DRAFT", reportDisplayName: 'HAWB DRFT | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "HAWB_LABEL", reportDisplayName: 'HAWB Label | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "BOOKING_CONFIRMATION", reportDisplayName: 'Shipment Confirmation | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "TRUCKING_INSTRUCTION", reportDisplayName: 'Trucking Instruction | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "SHIPPING_INSTRUCTION", reportDisplayName: 'Shipping Instruction | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "CERTIFICATE_OF_ORIGIN", reportDisplayName: 'Certificate Of Origin | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "STATEMENT_OF_CHARGE", reportDisplayName: 'Statement Of Charge | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "SUBJOB_COST_SHEET", reportDisplayName: ' Consol Profitability | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "DOCK_RECEIPT", reportDisplayName: ' Dock Receipt | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "EXPORT_JOB_CARD_HAWB", reportDisplayName: ' Export Job Card(HAWB) | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                    for (var i = 0; i < $scope.documentIdList.length; i++) {
                                        $scope.reportDetailData = {};
                                        $scope.reportDetailData = { resourceRefId: $scope.documentIdList[i].id, isDocument: true, isService: false, documentNo: $scope.documentIdList[i].documentNo, reportName: "FORWARDERS_CARGO_RECEIPT", reportDisplayName: ' Forwarders Cargo Receipt | Document # ' + $scope.documentIdList[i].documentNo };
                                        $scope.reportList.push($scope.reportDetailData);
                                    }
                                }

                            }
                            $scope.assignDefaultReports(sd, resourceRefId);
                        } else {
                            $scope.assignDefaultReports(sd, resourceRefId);
                        }

                        //console.log("Reports Data :: ", JSON.stringify($scope.reportList));
                    } else {
                        $scope.assignDefaultReports(sd, resourceRefId);

                    }
                },
                function(error) {
                    $scope.assignDefaultReports(sd, resourceRefId);
                    console.log("Shipment get Failed : " + error)
                    $scope.reportSpinner = false;
                });


        } else {
            console.log("Rights to access the report is not present.")
        }
    }


    $scope.assignDefaultReports = function(sd, resourceRefId) {
        if (sd.serviceMaster.importExport == 'Export') {
            $scope.reportDetailData = { resourceRefId: resourceRefId, isDocument: true, isService: false, documentNo: '', reportName: "COMMERCIAL_INVOICE", reportDisplayName: 'Commercial Invoice' };
            $scope.reportList.push($scope.reportDetailData);
        }
        $scope.reportSpinner = false;
    }



    $scope.selectAllReports = function() {
        for (var i = 0; i < $scope.reportList.length; i++) {
            $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
        }
    }

    $scope.chkReportStatus = function(chkFlag, detObj) {
        console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
    }

    $scope.generateAllReports = function() {

        var flag = false;
        for (var i = 0; i < $scope.reportList.length; i++) {
            if ($scope.reportList[i].isSelected) {
                flag = true
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        $scope.downloadGenericCall($scope.reportList[i].resourceRefId, "Download", $scope.reportList[i].reportName);
                    }
                }
                $timeout(function() {
                    $scope.reportLoadingImage.status = false;
                }, 1000);
            }
            if (flag) {
                break;
            }
        }
        if (!flag) {
            Notification.warning("Please select atleast one.......)")
        }

    }



    /*Report Related Codes Starts Here*/
    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print"];

    $scope.reportModal = function(resourceRefId, reportRefName) {

        console.log("Service ID - ", resourceRefId, " -- Name --", reportRefName);
        $scope.changeReportDownloadFormat("PDF");
        $scope.dataResourceId = resourceRefId;
        $scope.dataReportName = reportRefName;
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/report/report_opt.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);

    };

    $scope.changeReportDownloadFormat = function(dType) {
        console.log("Download type Changed :: ", dType);
        $scope.downloadFileType = dType;
        if (dType === 'PDF') {
            $scope.generate.TypeArr = ["Download", "Preview", "Print"];
        } else {
            $scope.generate.TypeArr = ["Download"];
        }
    }

    $scope.downloadGenericCall = function(id, downOption, repoName) {
            //$scope.reportPopUPSpinner=true;
            if (repoName === 'COMMERCIAL_INVOICE') {
                var reportDownloadRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: repoName,
                    parameter1: $scope.reportData.parameter1,
                    parameter2: $scope.reportData.parameter2
                };
            } else {
                var reportDownloadRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: repoName
                };
            }
            downloadFactory.download(reportDownloadRequest);
        }
        /*Report Related Codes Ends Here*/



    $scope.focusFieldAfter2Sec = function(fieldName) {

        $timeout(function() {
            $rootScope.navigateToNextField(fieldName);
        }, 3000);
    }

    $scope.isDimensionFound = function(service) {
        return attachToMasterService.isDimensionFound(service);

    }

    $scope.isDocumentDimensionFound = function(document) {
        return attachToMasterService.isDocumentDimensionFound(document);

    }
    $scope.chargableWeightCalulcate = function(service, whichValue) {
        return attachToMasterService.chargableWeightCalulcate(service, whichValue);

    }

    $scope.viewCFS = function(id) {

        console.log("CFS ID " + id);

        var params = {
            cfsId: id,
            fromState: $state.current.name,
            fromStateParams: $scope.shipment.id
        }

        $state.go('layout.viewAirCFS', params);
    }


    $scope.partyMasterActionResponseFn = function() {
        if ($scope.shipment != null && $stateParams.forObj != undefined && $stateParams.forObj != null) {
            var isNaviPartyMasterEmpty = true;
            var stateServiceIndex = 0;
            var stateDocumentIndex = 0;

            if ($stateParams.serIndx != undefined && $stateParams.serIndx != null) {
                stateServiceIndex = parseInt($stateParams.serIndx);
            }
            if ($stateParams.docIndx != undefined && $stateParams.docIndx != null) {
                stateDocumentIndex = parseInt($stateParams.docIndx);
            }
            console.log("Service Index", stateServiceIndex, " : DOC Index -- ", stateDocumentIndex);
            if ($rootScope.naviPartyMaster != undefined && $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != null) {
                isNaviPartyMasterEmpty = false;
            }
            if ($stateParams.forObj == "shipmentServiceDetail.party" && stateServiceIndex > -1) {
                if ($scope.shipment.shipmentServiceList != undefined && $scope.shipment.shipmentServiceList != null &&
                    $scope.shipment.shipmentServiceList.length > 0) {
                    if (isNaviPartyMasterEmpty) {
                        //$scope.shipment.shipmentServiceList[stateServiceIndex].party = {};
                    } else {
                        $scope.shipment.shipmentServiceList[stateServiceIndex].party = $rootScope.naviPartyMaster;
                    }
                }
                $scope.activeViewTabAndService(stateServiceIndex, "general");
                $scope.focusFieldAfter2Sec('partyService' + stateServiceIndex);
            } else if ($stateParams.forObj == "shipment.party") {
                if (isNaviPartyMasterEmpty) {
                    // $scope.shipment.party = {};
                } else {
                    $scope.shipment.party = $rootScope.naviPartyMaster;
                    $scope.getShipmentPartyAddress($scope.shipment.party);
                }
                //				$scope.focusFieldAfter2Sec('shipmentParty');
            } else if ($stateParams.forObj == "documentDetail.shipper") {

                console.log("$scope.shipment.shipmentServiceList ::: ", $scope.shipment.shipmentServiceList);
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].shipper = {};
                } else {
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].shipper = $rootScope.naviPartyMaster;
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "shipper");
                }

                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $scope.focusFieldAfter2Sec('docShipper' + stateServiceIndex + "_" + stateDocumentIndex);
            } else if ($stateParams.forObj == "documentDetail.forwarder") {
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].forwarder = {};
                } else {
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].forwarder = $rootScope.naviPartyMaster;
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "forwarder");
                }

                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $rootScope.navigateToNextField('docForwarder' + stateServiceIndex + "_" + stateDocumentIndex);
            } else if ($stateParams.forObj == "documentDetail.consignee") {
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].consignee = {};
                } else {
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].consignee = $rootScope.naviPartyMaster;
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "consignee");
                }

                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $scope.focusFieldAfter2Sec('docConsignee' + stateServiceIndex + "_" + stateDocumentIndex);
            } else if ($stateParams.forObj == "documentDetail.firstNotify") {
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].firstNotify = {};
                } else {
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].firstNotify = $rootScope.naviPartyMaster;
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "firstNotify");
                }

                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $scope.focusFieldAfter2Sec('docFirstNotify' + stateServiceIndex + "_" + stateDocumentIndex);
            } else if ($stateParams.forObj == "documentDetail.secondNotify") {
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].secondNotify = {};
                } else {
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "secondNotify");
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].secondNotify = $rootScope.naviPartyMaster;
                }

                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $scope.focusFieldAfter2Sec('docSecondNotify' + stateServiceIndex + "_" + stateDocumentIndex);
            } else if ($stateParams.forObj == "documentDetail.agent") {
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].agent = {};
                } else {
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].agent = $rootScope.naviPartyMaster;
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "agent");
                }

                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $scope.focusFieldAfter2Sec('docAgent' + stateServiceIndex + "_" + stateDocumentIndex);
            } else if ($stateParams.forObj == "documentDetail.issuingAgent") {
                if (isNaviPartyMasterEmpty) {
                    //$scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].issuingAgent = {};
                } else {
                    $scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex].issuingAgent = $rootScope.naviPartyMaster;
                    $scope.choosePartyAddress($scope.shipment.shipmentServiceList[stateServiceIndex].documentList[stateDocumentIndex], "issuingAgent");
                }
                $scope.activeViewTabAndService(stateServiceIndex, "documents");
                $scope.focusFieldAfter2Sec('docIssueAgent' + stateServiceIndex + "_" + stateDocumentIndex);
            }


        }
    }

    $scope.goToDocFocus = function(service, tabName, focus1, focus2) {
        $scope.shipmentTab = tabName;
        if (service.documentList != undefined && service.documentList != null && service.documentList.length != 0) {
            $scope.documentListTab = 0;
            if (service.serviceMaster.importExport == 'Export') {
                $rootScope.navigateToNextField((focus1 + $scope.selectedTabIndex + '_' + $scope.documentListTab));
            } else if (service.serviceMaster.importExport == 'Import') {
                $rootScope.navigateToNextField((focus2 + $scope.selectedTabIndex + '_' + $scope.documentListTab));
            }
        } else {
            $scope.newFirstDocument(true);
        }

    }


    $scope.createCFS = function(sUid) {
        console.log("Create CFS button is pressed...");

        $scope.isCreateConsol = 'CFS';
        $scope.createConsolServiceId = sUid;
        $scope.updateShipment();

    }

    //Page Navigation and Events Like Reload Functionalites starts here
    //Also It must be a Last functionailty of the controller
    $scope.setActiveObj = function() {
        var activeObj = {};
        activeObj.selectedTabIndex = $scope.selectedTabIndex;
        activeObj.shipmentTab = $scope.shipmentTab;
        activeObj.documentListTab = $scope.documentListTab;
        $scope.shipment.activeObj = activeObj;
    }
    $scope.getActiveObj = function() {
        var activeObj = $scope.shipment.activeObj;
        $scope.selectedTabIndex = activeObj.selectedTabIndex;
        $scope.shipmentTab = activeObj.shipmentTab;
        $scope.documentListTab = activeObj.documentListTab;
        $scope.shipment.activeObj = undefined;
    }
    $scope.$on('addCrmShipmentEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        $scope.setActiveObj();
        localStorage.reloadFormData = JSON.stringify($scope.shipment);
        localStorage.isShipmentReloaded = "YES";
        return "Do you want to Reload";
    });

    $scope.$on('editCrmShipmentEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        $scope.setActiveObj();
        localStorage.reloadFormData = JSON.stringify($scope.shipment);
        localStorage.isShipmentReloaded = "YES";
        return "Do you want to Reload";
    });

    $scope.$on('createCrmShipmentEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        $scope.setActiveObj();
        localStorage.reloadFormData = JSON.stringify($scope.shipment);
        localStorage.isShipmentReloaded = "YES";
        return "Do you want to Reload";
    });

    $scope.$on('addCrmShipmentEvent', function(events, args) {

        //console.log("Scope enquiryLog data 2222 :: ", $scope.shipment);

        $scope.setActiveObj();
        $rootScope.unfinishedData = $scope.shipment;
        $rootScope.unfinishedFormTitle = "Shipment (New)";
        $rootScope.category = "Shipment";
        if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.party != undefined && $scope.shipment.party != null &&
            $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
            $rootScope.subTitle = $scope.shipment.party.partyName;
        } else {
            $rootScope.subTitle = "Unknown Party"
        }
        console.log("addCrmShipmentEvent Broadcast Listener :: ", args);
    })

    $scope.$on('editCrmShipmentEvent', function(events, args) {
        //console.log("Scope Shipment data :: ", $scope.shipment);
        $scope.setActiveObj();
        $rootScope.unfinishedData = $scope.shipment;
        $rootScope.category = "Shipment";
        $rootScope.unfinishedFormTitle = "Shipment Edit # " + $scope.shipment.shipmentUid;
        if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.party != undefined && $scope.shipment.party != null &&
            $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
            $rootScope.subTitle = $scope.shipment.party.partyName;
        } else {
            $rootScope.subTitle = "Unknown Party"
        }
        console.log("editCrmShipmentEvent Broadcast Listener :: ", args);
    })

    $scope.$on('createCrmShipmentEvent', function(events, args) {
        //console.log("Scope Shipment data :: ", $scope.shipment);
        $scope.setActiveObj();
        $rootScope.unfinishedData = $scope.shipment;
        $rootScope.category = "Shipment";
        $rootScope.unfinishedFormTitle = "Shipment (New) ";
        if ($scope.shipment != undefined && $scope.shipment != null && $scope.shipment.party != undefined && $scope.shipment.party != null &&
            $scope.shipment.party.partyName != undefined && $scope.shipment.party.partyName != null) {
            $rootScope.subTitle = $scope.shipment.party.partyName;
        } else {
            $rootScope.subTitle = "Unknown Party"
        }
        console.log("editCrmShipmentEvent Broadcast Listener :: ", args);
    })

    $scope.isReloaded = localStorage.isShipmentReloaded;
    if ($stateParams.action == "ADD") {
        $scope.showCopyShipment = true;
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isShipmentReloaded = "NO";
                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                $scope.focusFieldAfter2Sec('shipmentParty');
            } else {
                $scope.shipment = $rootScope.selectedUnfilledFormData;
                $scope.partyMasterActionResponseFn();
            }
            $scope.getActiveObj();
        } else if ($stateParams.forCopyShipment) {
            $scope.shipment = $rootScope.copyShipmentData;
            $scope.getActiveObj();
            $rootScope.navigateToNextField('shipmentParty');
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isShipmentReloaded = "NO";
                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                $scope.getActiveObj();
                $rootScope.navigateToNextField('shipmentParty');
            } else {
                $scope.addInit();
            }
            $scope.focusFieldAfter2Sec('shipmentParty');
        }
        $scope.partyDisabledFlag = false;
        $rootScope.breadcrumbArr = [{ label: "CRM", state: "layout.crmShipment" }, { label: "Shipment", state: "layout.crmShipment" }, { label: "Add Shipment", state: null }];
    } else if ($stateParams.action == "CREATE") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isShipmentReloaded = "NO";
                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                $rootScope.navigateToNextField('shipmentParty');
            } else {
                $scope.shipment = $rootScope.selectedUnfilledFormData;
                $scope.partyMasterActionResponseFn();
            }
            $scope.getActiveObj();
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isShipmentReloaded = "NO";
                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                $scope.getActiveObj();
                $rootScope.navigateToNextField('shipmentParty');
            } else {
                $scope.shipment = $rootScope.createShipment;
                if ($scope.shipment.id == undefined) {
                    $scope.shipment.id = -1;
                }
                $scope.showCopyShipment = false;
                $scope.initClickTabIndex($scope.shipment)
                $rootScope.navigateToNextField('shipmentParty');
            }
        }


        if ($scope.shipment.id == -1 && $stateParams.fromState != undefined && $stateParams.fromState == "fromQuot") {

            if (angular.uppercase($rootScope.appMasterData['quotation.to.shipment.change.party']) == "TRUE") {
                $scope.partyDisabledFlag = false;
            } else {
                $scope.partyDisabledFlag = true;
                $rootScope.navigateToNextField('projectSp');
            }

        } else {
            $scope.partyDisabledFlag = false;
        }
        $scope.setOldDataVal();
        $rootScope.breadcrumbArr = [{ label: "CRM", state: "layout.crmShipment" }, { label: "Shipment", state: "layout.crmShipment" }, { label: "Create Shipment", state: null }];
    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isShipmentReloaded = "NO";
                $scope.shipment = JSON.parse(localStorage.reloadFormData)

            } else {
                $scope.shipment = $rootScope.selectedUnfilledFormData;
            }

            $scope.getActiveObj();
            $rootScope.navigateToNextField('shipmentParty');
            $scope.partyMasterActionResponseFn();
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isShipmentReloaded = "NO";
                $scope.shipment = JSON.parse(localStorage.reloadFormData);
                $scope.getActiveObj();
                $rootScope.navigateToNextField('shipmentParty');
            } else {
                $scope.getShipment($stateParams.shipmentId);
            }
        }

        $scope.partyDisabledFlag = false;

        $rootScope.breadcrumbArr = [{ label: "CRM", state: "layout.crmShipment" }, { label: "Shipment", state: "layout.crmShipment" }, { label: "Edit Shipment", state: null }];
    }



});