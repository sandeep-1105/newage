/**
 * Created by hmspl on 12/08/16.
 */
app.controller("importToExportCtrl",['$rootScope', '$timeout', '$stateParams', '$state', '$scope', '$http', '$location', '$modal', 
        'CommonValidationService', 'discardService', 'ngDialog', 'ShipmentGetFromUID', 'ImportExportSearch', 
        'ServiceGet', 'PartiesList', 'ServiceByTransportMode', 'ShipmentServiceExportAdd', 'ValidateUtil', 'cloneService',
        'AgentByServicePort', 'addressJoiner', 'roleConstant',
    function($rootScope, $timeout, $stateParams, $state, $scope, $http, $location, $modal, 
        CommonValidationService, discardService, ngDialog, ShipmentGetFromUID, ImportExportSearch, 
        ServiceGet, PartiesList, ServiceByTransportMode, ShipmentServiceExportAdd, ValidateUtil, cloneService,
        AgentByServicePort, addressJoiner, roleConstant) {

    $scope.limitArr = [10, 15, 20];

    $scope.page = 0;
    $scope.limit = 10;
    // $scope.totalRecord = 10;

    $scope.limitArrActive = [10, 15, 20];

    $scope.importToExportDto = {};
    $scope.importToExportDto.obj = {};
    $scope.importToExportDto.importService = {};
    $scope.importToExportDto.exportService = {};

    $scope.searchDto = {};



    $scope.init = function() {
        $scope.pageActive = 0;
        $scope.page = 0;
        $scope.limit = 10;
        $scope.shipment = {};
        $scope.importToExportDto = {};
        $scope.importToExportDto.importService = {};
        $scope.importToExportDto.exportService = {};
        $scope.agent = {};
        $scope.partyAddress = {};
        $scope.serviceMaster = {};
        $scope.agent.partyAddressList = [];
        $scope.showPartyAddressCustomerList = false;
        $scope.shipmentServiceDetail = {};
        $scope.reportData = {};
        $scope.reportData.dateObj = new Date();
        $scope.detailTab = 'active';
        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.crmImportToExport"
            },
            {
                label: "Import to Export",
                state: "layout.crmImportToExport"
            }
        ];
        $scope.searchDto = {};
        $scope.activeSearchDto = {};
        $scope.activeSearch();
    }

    $scope.tabChange = function(tabName) {
        $scope.shipment = {};

        $scope.pageActive = 0;
        $scope.page = 0;
        $scope.limit = 10;

        if (tabName == 'active') {
            $scope.searchDto = {};
            $scope.searchDto.searchType = 'Active';
            $scope.activeSearch();
        } else if (tabName == 'all') {
            $scope.searchDto = {};
            $scope.searchDto.searchType = 'All';
            $scope.activeSearch();
        }
        $scope.detailTab = tabName;

    }

    $scope.sortSelection = {
        sortKey: "serviceReqDate",
        sortOrder: "asc"
    }

    $scope.activeSortSelection = {
        sortKey: "serviceReqDate",
        sortOrder: "asc"
    }

    $scope.importToExportHeadAllArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "Location",
            "search": true,
            "model": "locationName",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "location"
        },
        {
            "name": "Shipment ID",
            "model": "shipmentUid",
            "search": true,
            "wrap_cell": true,
            "type": "text", // changed to text from link by mani demo
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "shipmentUid"
        },
        {
            "name": "Nomination Ref.No",
            "model": "serviceUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "serviceUid"
        }, {
            "name": "Export Ref.No",
            "model": "exportRef",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "exportRef"
        },

        {
            "name": "Service Date",
            "model": "serviceReqDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "nominationDate",
            "color": true
        }, {
            "name": "Shipper",
            "model": "shipperName",
            "search": true,
            "wrap_cell": true,
            "type": "color-text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipperName"
        }, {
            "name": "Consignee",
            "model": "consigneeName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "consigneeName"
        }, {
            "name": "Origin",
            "model": "originName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "origin"
        }, {
            "name": "Destination",
            "model": "destinationName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "destination"
        }

    ]

    $scope.importToExportHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "Location",
            "search": true,
            "model": "locationName",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "location"
        },
        {
            "name": "Shipment ID",
            "model": "shipmentUid",
            "search": true,
            "wrap_cell": true,
            "type": "text", // changed to text from link by mani demo
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "shipmentUid"
        },
        {
            "name": "Nomination Ref.No",
            "model": "serviceUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "serviceUid"
        },
        {
            "name": "Service Date",
            "model": "serviceReqDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "nominationDate",
            "color": true
        }, {
            "name": "Shipper",
            "model": "shipperName",
            "search": true,
            "wrap_cell": true,
            "type": "color-text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipperName"
        }, {
            "name": "Consignee",
            "model": "consigneeName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "consigneeName"
        }, {
            "name": "Origin",
            "model": "originName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "origin"
        }, {
            "name": "Destination",
            "model": "destinationName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "destination"
        }

    ]

    /*$scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.activeSearch();
    }*/

    /*$scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.activeSearch();
    }*/


    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        if ($scope.detailTab == 'active') {
            $scope.searchDto.searchType = 'Active';
        } else if ($scope.detailTab == 'all') {
            $scope.searchDto.searchType = 'All';
        }

        $scope.page = 0;
        $scope.activeSearch();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        if ($scope.detailTab == 'active') {
            $scope.searchDto.searchType = 'Active';
        } else if ($scope.detailTab == 'all') {
            $scope.searchDto.searchType = 'All';
        }

        $scope.activeSearch();
    }

    $scope.limitChange = function(item) {

        $scope.searchDto = {};
        $scope.page = 0;
        $scope.limit = item;

        if ($scope.detailTab == 'active') {
            $scope.searchDto.searchType = 'Active';
        } else if ($scope.detailTab == 'all') {
            $scope.searchDto.searchType = 'All';
        }
        $scope.activeSearch();
    }

    $scope.changePage = function(param) {
        $scope.searchDto = {};
        if ($scope.detailTab == 'active') {
            $scope.searchDto.searchType = 'Active';
        } else if ($scope.detailTab == 'all') {
            $scope.searchDto.searchType = 'All';
        }
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.activeSearch();
    }

    /* $scope.testArr=[

         {
             "no":1,
             "location":"dubai",
             "shipmentid": "787797977",
             "servicename": "Ai Import",
             "nomination":"3321313",
             "nomdate":"01/03/16",
             "party":"SFSL Logistics Pvt Ltd",
             "origin":"madurai",
             "destination":"Chennai",
             "etd":"11/04/16",
             "eta":"11/06/16"

         }

     ]*/

    $scope.edit = function() {

        $location.path("/crm/import_export_operation");

    };

    $scope.back = function() {
        $scope.showDetail = false;
        $state.go('layout.crmImportToExport');
    }

    // Start page navigation 

    $scope.singlePageNavigation = function(val) {

        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            if ($scope.detailTab == 'active') {
                $scope.searchDto.searchType = 'Active';
            } else if ($scope.detailTab == 'all') {
                $scope.searchDto.searchType = 'All';
            }

            // location
            if ($stateParams.location != undefined && $stateParams.location != null) {
                stateParameters.location = $scope.searchDto.location = $stateParams.location;
            }

            // shipmentUid
            if ($stateParams.shipmentUid != undefined && $stateParams.shipmentUid != null) {
                stateParameters.shipmentUid = $scope.searchDto.shipmentUid = $stateParams.shipmentUid;
            }
            // service
            if ($stateParams.service != undefined && $stateParams.service != null) {
                stateParameters.service = $scope.searchDto.service = $stateParams.service;
            }
            // nominationNo
            if ($stateParams.nominationNo != undefined && $stateParams.nominationNo != null) {
                stateParameters.nominationNo = $scope.searchDto.nominationNo = $stateParams.nominationNo;
            }
            // nominationDate
            $scope.searchDto.nominationDate = {};
            if ($stateParams.nominationDateStartDate != undefined && $stateParams.nominationDateStartDate != null) {
                stateParameters.nominationDateStartDate = $scope.searchDto.nominationDate.startDate = $stateParams.nominationDateStartDate;
            }

            if ($stateParams.nominationDateEndDate != undefined && $stateParams.nominationDateEndDate != null) {
                stateParameters.nominationDateEndDate = $scope.searchDto.nominationDate.endDate = $stateParams.nominationDateEndDate;
            }

            // partyName
            if ($stateParams.partyName != undefined && $stateParams.partyName != null) {
                stateParameters.partyName = $scope.searchDto.partyName = $stateParams.partyName;
            }
            // origin
            if ($stateParams.origin != undefined && $stateParams.origin != null) {
                stateParameters.origin = $scope.searchDto.origin = $stateParams.origin;
            }
            // destination;
            if ($stateParams.destination != undefined && $stateParams.destination != null) {
                stateParameters.destination = $scope.searchDto.destination = $stateParams.destination;
            }

            // eta
            $scope.searchDto.eta = {};
            if ($stateParams.etaStartDate != undefined && $stateParams.etaStartDate != null) {
                stateParameters.etaStartDate = $scope.searchDto.eta.startDate = $stateParams.etaStartDate;
            }

            if ($stateParams.etaEndDate != undefined && $stateParams.etaEndDate != null) {
                stateParameters.etaEndDate = $scope.searchDto.eta.endDate = $stateParams.etaEndDate;
            }

            // etd
            $scope.searchDto.etd = {};
            if ($stateParams.etdStartDate != undefined && $stateParams.etdStartDate != null) {
                stateParameters.etdStartDate = $scope.searchDto.etd.startDate = $stateParams.etdStartDate;
            }

            if ($stateParams.etdEndDate != undefined && $stateParams.etdEndDate != null) {
                stateParameters.etdEndDate = $scope.searchDto.etd.endDate = $stateParams.etdEndDate;
            }

            // searchType
            if ($stateParams.searchType != undefined && $stateParams.searchType != null) {
                stateParameters.searchType = $scope.searchDto.searchType = $stateParams.searchType;
            }

            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchFlag != undefined && $stateParams.searchFlag != null) {
                stateParameters.searchFlag = $scope.searchFlag = $stateParams.searchFlag;
            } else {
                stateParameters.searchFlag = $scope.searchFlag = "active";
            }
        } else {
            return;
        }

        //$scope.searchDto.searchType='All';

        ImportExportSearch.query({
            "searchFlag": $scope.searchFlag
        }, $scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.enqIdx = stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.serviceId = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.crmImportToExportView", stateParameters);
        });
    }


    // End Page navigation


    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.enqIdx;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }

                // location
                if ($scope.searchDto.location != undefined && $scope.searchDto.location != null) {
                    param.location = $scope.searchDto.location;
                }
                // shipmentUid
                if ($scope.searchDto.shipmentUid != undefined && $scope.searchDto.shipmentUid != null) {
                    param.shipmentUid = $scope.searchDto.shipmentUid;
                }
                // service
                if ($scope.searchDto.service != undefined && $scope.searchDto.service != null) {
                    param.service = $scope.searchDto.service;
                }
                // nominationNo
                if ($scope.searchDto.nominationNo != undefined && $scope.searchDto.nominationNo != null) {
                    param.nominationNo = $scope.searchDto.nominationNo;
                }
                // nominationDate
                if ($scope.searchDto.nominationDate != undefined && $scope.searchDto.nominationDate != null) {
                    param.nominationDateStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.nominationDate.startDate);
                    param.nominationDateEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.nominationDate.endDate);
                }

                // partyName
                if ($scope.searchDto.partyName != undefined && $scope.searchDto.partyName != null) {
                    param.partyName = $scope.searchDto.partyName;
                }
                // origin
                if ($scope.searchDto.origin != undefined && $scope.searchDto.origin != null) {
                    param.origin = $scope.searchDto.origin;
                }
                // destination;
                if ($scope.searchDto.destination != undefined && $scope.searchDto.destination != null) {
                    param.destination = $scope.searchDto.destination;
                }
                // etd
                if ($scope.searchDto.etd != undefined && $scope.searchDto.etd != null) {
                    param.etdStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.startDate);
                    param.etdEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.endDate);
                }

                // eta
                if ($scope.searchDto.eta != undefined && $scope.searchDto.eta != null) {
                    param.etaStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.eta.startDate);
                    param.etaEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.eta.endDate);
                }

                // searchType
                if ($scope.detailTab == 'active') {
                    $scope.searchDto.searchType = 'Active';
                } else if ($scope.detailTab == 'all') {
                    $scope.searchDto.searchType = 'All';
                }

                if ($scope.searchDto.searchType != undefined && $scope.searchDto.searchType != null) {
                    param.searchType = $scope.searchDto.searchType;
                }

            }

        }
        return param;
    }

    $scope.goToShipment = function(shipmentUid) {

        var param = {
            shipmentUid: shipmentUid,
            fromState: $state.current.name,
            fromStateParams: JSON.stringify($stateParams)
        }

        $state.go('layout.viewCrmShipment', param);
    }

    $scope.getServiceById = function(id) {
        ServiceGet.get({
            id: id
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("ShipmentServiceDetailView Successfully")

                $scope.importToExportDto.obj = data.responseObject.importService;
                $scope.importToExportDto.exportService = data.responseObject.exportService;
                $scope.importToExportDto.shipmentUid = data.responseObject.importService.shipmentUid;
                $scope.importToExportDto.importService.id = data.responseObject.importService.id;
                var labelName;
                if ($scope.importToExportDto.obj.exportRef != null) {
                    labelName = "View";
                } else {
                    labelName = "Generate";
                }

                $rootScope.breadcrumbArr = [{
                        label: "CRM",
                        state: "layout.crmImportToExport"
                    },
                    {
                        label: "Import to Export",
                        state: "layout.crmImportToExport"
                    },
                    {
                        label: labelName + " Import to Export",
                        state: null
                    }

                ];
                $scope.showDetail = true;
                $scope.populatePartyMaster();
            } else {
                console.log("ShipmentServiceDetailView Failed " + data.responseDescription)
            }
        }, function(error) {
            console.log("ShipmentServiceDetailView Failed : " + error)
        });
    }

    //Row Select

    $scope.rowSelect = function(data, index) {

        if ($rootScope.roleAccess(roleConstant.CRM_IMPORT_TO_EXPORT_VIEW)) {

            if (data.exportRef == undefined || data.exportRef == null) {
                $scope.rowSelectActive(data, index);
            } else {
                $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
                var localRefStateParam = {
                    serviceId: data.id,
                    enqIdx: $scope.selectedRecordIndex,
                    searchFlag: $scope.searchFlag,
                    totalRecord: $scope.totalRecord
                };
                console.log("ImportToExport  $scope.searchFlag -------------- ", $scope.searchFlag);
                $state.go("layout.crmImportToExportView", $scope.searchDtoToStateParams(localRefStateParam));

            }
        }
    }



    $scope.rowSelectActive = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.CRM_IMPORT_TO_EXPORT_VIEW)) {
            ngDialog.openConfirm({
                template: '<p>Are you sure to generate a Export Shipment for this nomination ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default',
            }).
            then(function(value) {
                // $scope.apporveApiCall("Generated");
                $rootScope.mainpreloder = true;
                $scope.importToExportDto = {}
                $scope.importToExportDto.serviceUid = data.serviceUid;
                ShipmentServiceExportAdd.save($scope.importToExportDto).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        var params = {};
                        params.action = 'EDIT';
                        params.shipmentId = data.responseObject.id;
                        params.fromScreen = $state.current.name;
                        $rootScope.mainpreloder = false;
                        $state.go("layout.editNewShipment", params);

                    } else {
                        console.log("Shipment added Failed " + data.responseDescription);
                        $rootScope.mainpreloder = false;
                    }
                }, function(error) {
                    console.log("Shipment added Failed : " + error)
                    $rootScope.mainpreloder = false;
                });

                //$scope.generateImport=true;
                //$scope.spinner = true;

            }, function(value) {
                console.log("Cancelled ............");
                $scope.generateImport = false;
            });
        }
    };

    $scope.showResponse = function(serviceObj, isAdded) {
        var newScope = $scope.$new();
        var enquiryNo = serviceObj.enquiryNo;
        $scope.spinner = false;
        if (isAdded) {
            newScope.errorMessage = $rootScope.nls["ERR0215"];
        }

        if (newScope.errorMessage != undefined) {
            newScope.errorMessage = newScope.errorMessage.replace("%s", serviceObj);
        }
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p> ' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                '</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            var params = {};
            params.submitAction = 'Saved';
            $state.go('layout.crmImportToExport');
        }, function(value) {
            console.log("ok cancelled");
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go('layout.crmImportToExport');
        });

    }

    /* Agent Select Picker */

    $scope.ajaxAgentEvent = function(object) {
        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.agentList = data.responseObject.searchResult;
                    return $scope.agentList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }

    //Start 

    /* List of Customers select Picker */
    $scope.showCustomerList = false;

    $scope.customerlistConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
                "title": "partyName",
                seperator: false
            },
            {
                "title": "partyCode",
                seperator: true
            }
        ]
    }

    $scope.routePartyEvent = function(fromObjModel, isAdd, partyMaster) {
        $rootScope.nav_src_bkref_key = new Date().toISOString();
        if (isAdd) {
            $state.go("layout.addParty", {
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                forObj: fromObjModel
            });
        } else {
            if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                $rootScope.nav_src_bkref_key = new Date().toISOString();
                var stateRouteParam = {
                    partyId: partyMaster.id,
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    forObj: fromObjModel
                };
                $state.go("layout.editParty", stateRouteParam);
            } else {
                console.log("Party not selected...");
            }
        }
    }

    $scope.showCustomer = function(partyName) {

        console.log("Showing List of Customer.....");
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.selectedItem = -1;


    };

    $scope.ajaxCustomerEvent = function(object) {

        console.log("ajaxCustomerEvent is called", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        //$scope.showCustomerList=true;


        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.customerList = data.responseObject.searchResult;
                    //$scope.showCustomerList=true;
                    return $scope.customerList;

                }
            },
            function(errResponse) {
                console.error('Error while fetching Countries');
            }
        );


    }

    /*$scope.serviceToConsol=function(importExpoObj){
         
        ShipmentGetFromUID.get({
            shipmentuid : importExpoObj.shipmentUid
        },function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.shipmentObj={};
                $scope.shipmentObj=data.responseObject;
                uiShipmentDataService.goToConsol($scope.shipmentObj,$scope.importToExportDto.obj);
            }
        }, 
        function(error) {
            console.log("Shipment get Failed : " + error)
        });
         
     }*/


    $scope.selectedCustomer = function(agent) {
        if ($scope.validateImportExport(1)) {
            $scope.agent = agent;
            $scope.getPartyAddress('CUSTOMER', agent);
            var navigateTo = 'addressLine1';
            $rootScope.navigateToNextField(navigateTo);
        }
    };

    $scope.getPartyAddress = function(type, obj) {
        if (($scope.agent != null && $scope.agent.id != null)) {

            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {
                    $scope.addressMapping('CUSTOMER', obj.partyAddressList[i], obj.countryMaster);
                }
            }
        } else if (type == 'SHIPPER') {
            if (($scope.ImportExport.shipper != null && $scope.ImportExport.shipper.id != null)) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressMapping('SHIPPER', obj.partyAddressList[i], obj.countryMaster);
                    }
                }
            }
        }

    }

    $scope.addressMapping = function(type, partyAddress, countryMaster) {
        $scope.partyAddress = {};



        $scope.partyAddress.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox + ", " + partyAddress.addressLine1 : partyAddress.addressLine1;

        $scope.partyAddress.addressLine2 = partyAddress.addressLine2;

        $scope.partyAddress.addressLine3 = partyAddress.addressLine3;

        var addressLine4 = "";

        if (partyAddress.cityMaster !== null) {
            addressLine4 = addressLine4 + partyAddress.cityMaster.cityName;
        }


        if (partyAddress.zipCode != null) {
            if (addressLine4 != "")
                addressLine4 = addressLine4 + " - ";
            addressLine4 = addressLine4 + partyAddress.zipCode;
        }

        if (partyAddress.stateMaster != null) {
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 + partyAddress.stateMaster.stateName;

        }

        if (countryMaster != null) {
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 + countryMaster.countryName;

        }



        $scope.partyAddress.addressLine4 = addressLine4;

        $scope.partyAddress.phone = partyAddress.phone;
        $scope.partyAddress.mobile = partyAddress.mobileNo;
        $scope.partyAddress.email = partyAddress.email;




        console.log("Address List 4 ", $scope.agent.partyAddressList);
    }
    $scope.cancelCustomer = function() {
        $scope.showCustomerList = false;


    }


    $scope.showPartyAddressCustomerList = false;

    $scope.partyAddressCustomerlistConfig = {
        search: true,
        showCode: true,
        columns: [{
                "title": "addressType",
                seperator: false
            },
            {
                "title": "contactPerson",
                seperator: true
            }
        ]

    }


    $scope.partyAddresslistCustomerConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddressCustomer = function() {

        console.log("Showing Customers address list...");

        if (true) {
            $scope.panelTitle = "Party Address";
            console.log("Address List ", $scope.agent.partyAddressList)
            $scope.partyAddressCustomer = [];
            $scope.showPartyAddressCustomerList = true;
            $scope.selectedItem = -1;
            $scope.isPickUp = false;
        }
    };

    $scope.goToPickup = function() {


        if ($scope.validateImportExport(1)) {
            $scope.panelTitle = "Pick up Address";
            $scope.partyAddressCustomer = [];
            $scope.showPartyAddressCustomerList = true;
            $scope.selectedItem = -1;

            $scope.isPickUp = true;
        }
    };



    $scope.selectedPartyAddressCustomer = function(obj) {

        var navigateTo = 'addressLine1';
        $rootScope.navigateToNextField(navigateTo);

        if ($scope.isPickUp) {
            var addressMaster = {};
            addressMaster = addressJoiner.joinAddressLineByLine(addressMaster, $scope.agent, obj);
            $scope.ImportExportDetail.pickUpAddress = addressMaster.fullAddress;
        } else {
            $scope.addressMapping('CUSTOMER', obj, $scope.agent.countryMaster);

        }
        $scope.showPartyAddressCustomerList = false;
        $rootScope.navigateToNextField('delivery')
    };

    $scope.cancelPartyAddressCustomer = function() {
        $scope.showPartyAddressCustomerList = false;
        $rootScope.navigateToNextField('pickup')
    }



    //End


    //Validations start

    $scope.validateImportExport = function(validateCode) {

        $scope.firstFocus = false;
        $scope.errorMap = new Map();
        console.log("ImportExport Validation Begins " + validateCode);

        //Service validation start

        if (validateCode == 0 || validateCode == 6) {

            if ($scope.importToExportDto.exportService.serviceMaster == null || $scope.importToExportDto.exportService.serviceMaster.id == undefined ||
                $scope.importToExportDto.exportService.serviceMaster.id == null ||
                $scope.importToExportDto.exportService.serviceMaster.id == "") {

                console.log($rootScope.nls["ERR0211"]);
                $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR0211"]);
                var navigateTo = 'serviceMaster';
                $rootScope.navigateToNextField(navigateTo);
                return false;
            } else {

                if (ValidateUtil.isStatusBlocked($scope.importToExportDto.exportService.serviceMaster.status)) {
                    $scope.importToExportDto.exportService.serviceMaster = null;

                    $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR0212"]);
                    var navigateTo = 'serviceMaster';
                    $rootScope.navigateToNextField(navigateTo);
                    return false

                }
                if (ValidateUtil.isStatusHidden($scope.importToExportDto.exportService.serviceMaster.status)) {
                    $scope.importToExportDto.exportService.serviceMaster = null;
                    console.log($rootScope.nls["ERR90033"]);
                    $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR0213"]);
                    var navigateTo = 'serviceMaster';
                    $rootScope.navigateToNextField(navigateTo);
                    return false

                }

            }

        }

        //Service validation end


        if (validateCode == 0 || validateCode == 1) {
            if ($scope.importToExportDto.exportService.agent == null ||
                $scope.importToExportDto.exportService.agent.id == undefined ||
                $scope.importToExportDto.exportService.agent.id == null ||
                $scope.importToExportDto.exportService.agent.id == "") {

                console.log("Agent is Mandatory");

                $scope.errorMap.put("customer", $rootScope.nls["ERR0200"]);

                var navigateTo = 'partyMaster';
                $rootScope.navigateToNextField(navigateTo);
                return false;

            } else if ($scope.importToExportDto.exportService.agent.status != null) {

                if ($scope.importToExportDto.exportService.agent.isDefaulter) {
                    $scope.importToExportDto.exportService.agent.party = null;

                    $scope.partyAddress.addressLine1 = "";
                    $scope.partyAddress.addressLine2 = "";
                    $scope.partyAddress.addressLine3 = "";
                    $scope.partyAddress.addressLine4 = "";
                    $scope.partyAddress.phone = "";
                    $scope.partyAddress.mobile = "";
                    $scope.partyAddress.email = "";

                    console.log("Selected agent is defaulter");
                    $scope.errorMap.put("customer", $rootScope.nls["ERR0214"]);
                    return false

                }

                if (ValidateUtil.isStatusBlocked($scope.importToExportDto.exportService.agent.status)) {

                    $scope.importToExportDto.exportService.agent.party = null;

                    $scope.partyAddress.addressLine1 = "";
                    $scope.partyAddress.addressLine2 = "";
                    $scope.partyAddress.addressLine3 = "";
                    $scope.partyAddress.addressLine4 = "";
                    $scope.partyAddress.phone = "";
                    $scope.partyAddress.mobile = "";
                    $scope.partyAddress.email = "";

                    console.log("Selected Customer is Blocked");

                    $scope.errorMap.put("customer", $rootScope.nls["ERR0201"]);
                    var navigateTo = 'partyMaster';
                    $rootScope.navigateToNextField(navigateTo);
                    return false
                }

                if (ValidateUtil.isStatusHidden($scope.importToExportDto.exportService.agent.status)) {

                    $scope.importToExportDto.exportService.agent.party = null;

                    $scope.partyAddress.addressLine1 = "";
                    $scope.partyAddress.addressLine2 = "";
                    $scope.partyAddress.addressLine3 = "";
                    $scope.partyAddress.addressLine4 = "";
                    $scope.partyAddress.phone = "";
                    $scope.partyAddress.mobile = "";
                    $scope.partyAddress.email = "";

                    console.log("Selected Customer is Hidden");

                    $scope.errorMap.put("customer", $rootScope.nls["ERR0202"]);
                    var navigateTo = 'partyMaster';
                    $rootScope.navigateToNextField(navigateTo);
                    return false
                }

            }
        }


        if (validateCode == 0 || validateCode == 2) {

            if ($scope.partyAddress.addressLine1 == null ||
                $scope.partyAddress.addressLine1 == "" ||
                $scope.partyAddress.addressLine1 == undefined) {
                console.log($rootScope.nls["ERR0203"]);
                $scope.errorMap.put("agent.agentAddress.addressLine1", $rootScope.nls["ERR0203"]);
                var navigateTo = 'addressLine1';
                $rootScope.navigateToNextField(navigateTo);
                return false;
            } else {
                /*                      var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
                                        if (!regexp.test($scope.partyAddress.addressLine1)) {
                                            console.log($rootScope.nls["ERR0204"]);
                                            $scope.errorMap.put("agent.agentAddress.addressLine1",$rootScope.nls["ERR0204"]);
                                            var navigateTo = 'addressLine1';
                                            $rootScope.navigateToNextField(navigateTo);
                                            return false;
                                        }
                */
                if ($scope.partyAddress.addressLine1.length > 100) {
                    console.log($rootScope.nls["ERR0204"]);
                    $scope.errorMap.put("agent.agentAddress.addressLine1", $rootScope.nls["ERR0204"]);
                    var navigateTo = 'addressLine1';
                    $rootScope.navigateToNextField(navigateTo);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {
            if ($scope.partyAddress.addressLine2 == null ||
                $scope.partyAddress.addressLine2 == "" ||
                $scope.partyAddress.addressLine2 == undefined) {
                console.log($rootScope.nls["ERR0205"]);
                $scope.errorMap.put("agent.agentAddress.addressLine2", $rootScope.nls["ERR0205"]);
                var navigateTo = 'addressLine2';
                $rootScope.navigateToNextField(navigateTo);
                return false;
            } else {
                /*                      var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
                                        if (!regexp.test($scope.partyAddress.addressLine2)) {
                                            console.log($rootScope.nls["ERR0206"]);
                                            $scope.errorMap.put("agent.agentAddress.addressLine2",$rootScope.nls["ERR0206"]);
                                            var navigateTo = 'addressLine2';
                                            $rootScope.navigateToNextField(navigateTo);
                                            return false;
                                        }
                */
                if ($scope.partyAddress.addressLine2.length > 100) {
                    console.log($rootScope.nls["ERR0206"]);
                    $scope.errorMap.put("agent.agentAddress.addressLine2", $rootScope.nls["ERR0206"]);
                    var navigateTo = 'addressLine2';
                    $rootScope.navigateToNextField(navigateTo);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            if ($scope.partyAddress.addressLine3 != null) {
                /*                      var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
                                        if (!regexp.test($scope.partyAddress.addressLine3)) {
                                            console.log($rootScope.nls["ERR0208"]);
                                            $scope.errorMap.put("agent.agentAddress.addressLine3",$rootScope.nls["ERR0208"]);
                                            var navigateTo = 'addressLine3';
                                            $rootScope.navigateToNextField(navigateTo);
                                            return false;
                                        }
                */
                if ($scope.partyAddress.addressLine3.length > 100) {
                    console.log($rootScope.nls["ERR0208"]);
                    $scope.errorMap.put("agent.agentAddress.addressLine3", $rootScope.nls["ERR0208"]);
                    var navigateTo = 'addressLine3';
                    $rootScope.navigateToNextField(navigateTo);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 5) {
            if ($scope.partyAddress.addressLine4 == null ||
                $scope.partyAddress.addressLine4 == "" ||
                $scope.partyAddress.addressLine4 == undefined) {
                console.log($rootScope.nls["ERR0209"]);
                $scope.errorMap.put("agent.agentAddress.addressLine4", $rootScope.nls["ERR0209"]);
                var navigateTo = 'addressLine4';
                $rootScope.navigateToNextField(navigateTo);
            } else {
                /*                      var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
                                        if (!regexp.test($scope.partyAddress.addressLine4)) {
                                            console.log($rootScope.nls["ERR0210"]);
                                            $scope.errorMap.put("agent.agentAddress.addressLine4",$rootScope.nls["ERR0210"]);
                                            var navigateTo = 'addressLine4';
                                            $rootScope.navigateToNextField(navigateTo);
                                            return false;
                                        }
                */
                if ($scope.partyAddress.addressLine4.length > 100) {
                    console.log($rootScope.nls["ERR0210"]);
                    $scope.errorMap.put("agent.agentAddress.addressLine4", $rootScope.nls["ERR0210"]);
                    var navigateTo = 'addressLine4';
                    $rootScope.navigateToNextField(navigateTo);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 7) {

            if ($scope.partyAddress.email != null && $scope.partyAddress.email != undefined && $scope.partyAddress.email != "") {
                if (!CommonValidationService.checkMultipleMail($scope.partyAddress.email)) {
                    $scope.errorMap.put("agent.agentAddress.email", $rootScope.nls["ERR90118"]);
                    return false;
                }
            }
        }




        return true;
    }

    // Validations end
    $scope.populatePartyMaster = function() {
        console.log("$stateParams.forObj -- ", $stateParams.forObj, "  || $rootScope.naviPartyMaster -- ", $rootScope.naviPartyMaster);
        if ($stateParams.forObj != undefined && $stateParams.forObj != null && $rootScope.naviPartyMaster != undefined &&
            $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != undefined && $rootScope.naviPartyMaster.id != null) {
            if ($stateParams.forObj == "importToExportDto.exportService.agent") {
                $scope.importToExportDto.exportService.agent = $rootScope.naviPartyMaster;
                $scope.getPartyAddress('CUSTOMER', $scope.importToExportDto.exportService.agent);
            }
        }
    }
    $scope.getPartyAddress = function(type, obj) {
        if (type == 'CUSTOMER') {
            if (($scope.importToExportDto.exportService.agent != null && $scope.importToExportDto.exportService.agent.id != null)) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressMapping('CUSTOMER', obj.partyAddressList[i], obj.countryMaster);
                    }
                }
            }

        }

    }

    $scope.addressMapping = function(type, partyAddress, countryMaster) {
        $scope.partyAddress = {};
        if (type == 'CUSTOMER') {
            $scope.partyAddress.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox + ", " + partyAddress.addressLine1 : partyAddress.addressLine1;
            $scope.partyAddress.addressLine2 = partyAddress.addressLine2;
            $scope.partyAddress.addressLine3 = partyAddress.addressLine3;
            var addressLine4 = "";
            if (partyAddress.cityMaster !== null) {
                addressLine4 = addressLine4 + partyAddress.cityMaster.cityName;
            }
            if (partyAddress.zipCode != null) {
                if (addressLine4 != "")
                    addressLine4 = addressLine4 + " - ";
                addressLine4 = addressLine4 + partyAddress.zipCode;
            }
            if (partyAddress.stateMaster != null) {
                if (addressLine4 != "")
                    addressLine4 = addressLine4 + ", ";
                addressLine4 = addressLine4 + partyAddress.stateMaster.stateName;
            }
            if (countryMaster != null) {
                if (addressLine4 != "")
                    addressLine4 = addressLine4 + ", ";
                addressLine4 = addressLine4 + countryMaster.countryName;
            }
            $scope.partyAddress.addressLine4 = addressLine4;
            $scope.partyAddress.phone = partyAddress.phone;
            $scope.partyAddress.mobile = partyAddress.mobileNo;
            $scope.partyAddress.email = partyAddress.email;
        }

    }


    $scope.activeSearch = function() {


        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.searchDto.status = null;


        $scope.tmpSearchDto = cloneService.clone($scope.searchDto);

        if ($scope.tmpSearchDto.nominationDate != null) {
            $scope.tmpSearchDto.nominationDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.nominationDate.startDate);
            $scope.tmpSearchDto.nominationDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.nominationDate.endDate);
        }

        if ($scope.tmpSearchDto.eta != null) {
            $scope.tmpSearchDto.eta.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.eta.startDate);
            $scope.tmpSearchDto.eta.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.eta.endDate);
        }

        if ($scope.tmpSearchDto.etd != null) {
            $scope.tmpSearchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.etd.startDate);
            $scope.tmpSearchDto.etd.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.etd.endDate);
        }

        $scope.importToExportArr = [];
        ImportExportSearch.query($scope.tmpSearchDto).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                if (tempObj.whoRouted == 'Self') {
                    tempObj.routedBy = tempObj.salesman.employeeName
                } else {
                    if (tempObj.agent != null)
                        tempObj.routedBy = tempObj.agent.partyName;
                }
                var today = $rootScope.sendApiStartDateTime($rootScope.dateToString(new Date()));

                if (tempObj.serviceReqDate < today) {
                    tempObj.changeDateColor = true;
                }

                if (tempObj.colorCode != null) {
                    tempObj.color = tempObj.colorCode;
                    tempObj.changeColor = true;
                }

                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.importToExportArr = resultArr;
        });

    }

    //Service list start

    /* Service Detail service List Picker */

    $scope.ajaxServiceEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ServiceByTransportMode.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceMasterListTemp = data.responseObject.searchResult;

                    console.log("before ", $scope.serviceMasterListTemp.length)
                    for (var int = 0; int < $scope.serviceMasterListTemp.length; int++) {

                        if ($scope.serviceMasterListTemp[int].importExport == 'Import') {
                            $scope.serviceMasterListTemp.splice(int, 1);
                        }

                    }
                    console.log("after ", $scope.serviceMasterListTemp.length)
                    $scope.serviceMasterList = $scope.serviceMasterListTemp;

                    return $scope.serviceMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ServiceMaster');
            }
        );

    }

    $scope.serviceRender = function(item) {
        return {
            label: item.serviceName,
            item: item
        }
    };

    $scope.selectedService = function(serviceDetail, index, nextIdValue) {

        if ($scope.validateImportExport(6)) {
            AgentByServicePort.fetch({
                "serviceId": $scope.importToExportDto.exportService.serviceMaster.id,
                "portId": $scope.importToExportDto.obj.destination.id
            }).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        var agentExport = data.responseObject;
                        if (agentExport != undefined &&
                            agentExport != null &&
                            agentExport.id != undefined) {
                            $scope.importToExportDto.exportService.agent = agentExport.agent;
                            $scope.selectedCustomer($scope.importToExportDto.exportService.agent);
                            $rootScope.navigateToNextField('partyMaster');
                        }
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching agent  based on service port');
                }

            );

            serviceDetail.tradeCode = $scope.importToExportDto.exportService.serviceMaster.importExport;
            $rootScope.navigateToNextField('partyMaster');
        }

    };

    $scope.$on('crmImportToExportViewEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.importToExportDto);
        localStorage.isExportImportReloaded = "YES";
        e.preventDefault();
    });

    $scope.$on('crmImportToExportViewEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.importToExportDto;
        $rootScope.category = "Import To Export";
        $rootScope.unfinishedFormTitle = "Import To Export (View)";
        if ($scope.importToExportDto != undefined && $scope.importToExportDto != null && $scope.importToExportDto.exportService != undefined && $scope.importToExportDto.exportService.serviceMaster != undefined) {
            $rootScope.subTitle = $scope.importToExportDto.exportService.serviceMaster.serviceName;
        } else {
            $rootScope.subTitle = "Unknown Service"
        }
    })




    // Service list end


    // Routing start


    $scope.isReloaded = localStorage.isExportImportReloaded;
    if ($stateParams.action != undefined && $stateParams.action != null && $stateParams.action == "SEARCH") {
        $scope.init();
    } else if ($stateParams.action != undefined && $stateParams.action != null && $stateParams.action == "VIEW") {
        $rootScope.breadcrumbArr = [{
                label: "CRM",
                state: "layout.crmImportToExport"
            },
            {
                label: "Import to Export",
                state: "layout.crmImportToExport"
            }
        ];
        if ($stateParams != null) {
            $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
            $scope.totalRecord = parseInt($stateParams.totalRecord);
        }
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isExportImportReloaded = "NO";
                if (localStorage.reloadFormData != undefined) {
                    $scope.importToExportDto = (JSON.parse(localStorage.reloadFormData));
                    if ($stateParams.serviceId != undefined && $stateParams.serviceId != null) {
                        $scope.showDetail = true;
                        $scope.getServiceById($stateParams.serviceId);
                    }
                } else {

                    if ($stateParams.serviceId != undefined && $stateParams.serviceId != null) {
                        $scope.showDetail = true;
                        $scope.getServiceById($stateParams.serviceId);
                    }

                }
            } else {
                $scope.importToExportDto = $rootScope.selectedUnfilledFormData;
                if ($stateParams.serviceId != undefined && $stateParams.serviceId != null) {
                    $scope.showDetail = true;
                    $scope.getServiceById($stateParams.serviceId);
                }
                $scope.populatePartyMaster();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isExportImportReloaded = "NO";
                if (localStorage.reloadFormData != undefined) {
                    $scope.importToExportDto = (JSON.parse(localStorage.reloadFormData));
                } else {

                    if ($stateParams.serviceId != undefined && $stateParams.serviceId != null) {
                        $scope.showDetail = true;
                        $scope.getServiceById($stateParams.serviceId);
                    }

                }
                $scope.populatePartyMaster();
            } else {
                $rootScope.unfinishedFormTitle = "Import To Export View # " + $stateParams.serviceId;
                $rootScope.unfinishedData = undefined;
                if ($stateParams.serviceId != undefined && $stateParams.serviceId != null) {
                    $scope.showDetail = true;
                    $scope.getServiceById($stateParams.serviceId);
                }
            }
        }
    } else {
        console.log("no view")
    }


    // Routing end



}]);