/**
 * Created by hmspl on 12/4/16.
 */
app.controller('CFSController',['$rootScope', '$scope', '$state', '$stateParams',
    'CFSReceiveSearch', 'CFSReceiveGet', 'RecentHistorySaveService', 'cloneService',
    'GoToUnfilledFormNavigationService', 'ShipmentIdByServiceUid', 'downloadFactory', 
    'roleConstant', 'ShipmentIdBasedOnUid',
    function($rootScope, $scope, $state, $stateParams,
    CFSReceiveSearch, CFSReceiveGet, RecentHistorySaveService, cloneService,
    GoToUnfilledFormNavigationService, ShipmentIdByServiceUid, downloadFactory, 
    roleConstant, ShipmentIdBasedOnUid) {

    $scope.$roleConstant = roleConstant;
    $scope.cfsHeadArr = [{
        "name": "#",
        "width": "w50px",
        "model": "no",
        "search": false,
        "prefWidth": "50"
    }, {
        "name": "Receipt No",
        "width": "w100px",
        "model": "receiptNumber",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchReceiptNumber",
        "prefWidth": "100"

    }, {
        "name": "Receipt Date",
        "width": "w100px",
        "model": "receiptDate",
        "search": true,
        "type": "date-range",
        "sort": true,
        "key": "searchReceiptDate",
        "prefWidth": "100"

    }, {
        "name": "CFS",
        "width": "w150px",
        "model": "cfsMasterName",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchCSF",
        "prefWidth": "150"

    }, {
        "name": "PRO No",
        "width": "w100px",
        "model": "proNumber",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchProNumber",
        "prefWidth": "100"
    }, {
        "name": "PRO Date",
        "width": "w100px",
        "model": "proDate",
        "search": true,
        "type": "date-range",
        "sort": true,
        "key": "searchProDate",
        "prefWidth": "100"
    }, {
        "name": "Shipper",
        "width": "w200px",
        "model": "shipperName",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchShipper",
        "prefWidth": "200"
    }, {
        "name": "Consignee",
        "width": "w200px",
        "model": "consigneeName",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchConsignee",
        "prefWidth": "200"
    }, {
        "name": "Shipment UID",
        "width": "w120px",
        "model": "shipmentUid",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchShipmentUid",
        "prefWidth": "120"
    }]


    $scope.searchOption = ['Shipper Ref No', 'On Hand', 'Trucker'];

    $scope.searchBy = function(clearFlag) {
        $scope.page = 0;
        if ($scope.cfsHeadArr.length == 9) {
            $scope.cfsHeadArr.splice($scope.cfsHeadArr.length - 1, 1);
        }
        $scope.navigateToNextField('searchValueAll');
        if ($scope.searchDto.searchName === undefined || $scope.searchDto.searchName === null) {
            $scope.searchDto.searchValue = "";
            $scope.searchValueAllFlag = false;
            var obj = {
                "name": "Shipment UID",
                "width": "w120px",
                "model": "shipmentUid",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchShipmentUid",
                "prefWidth": "120"
            };
            $scope.cfsHeadArr.push(obj);
        } else {
            $scope.searchValueAllFlag = true;
            $scope.currentTab = 'All';
            $scope.searchDto.status = null;

            if (clearFlag) {
                $scope.searchDto.searchValue = "";
            }

            if ($scope.searchDto.searchName === "Shipper Ref No") {
                var obj = {
                    "name": "Shipper Ref No",
                    "width": "w150px",
                    "model": "shipperRefNumber",
                    "search": false,
                    "type": "text",
                    "sort": true,
                    "key": "searchShipperRefNumber",
                    "prefWidth": "150"
                }
                $scope.cfsHeadArr.push(obj);
            } else if ($scope.searchDto.searchName === "On Hand") {

                var obj = {
                    "name": "On Hand",
                    "width": "w150px",
                    "model": "onHand",
                    "search": false,
                    "type": "text",
                    "sort": true,
                    "key": "searchOnHand",
                    "prefWidth": "150"
                }

                $scope.cfsHeadArr.push(obj);

            } else if ($scope.searchDto.searchName === "Trucker") {

                var obj = {
                    "name": "Trucker",
                    "width": "w200px",
                    "model": "truckerName",
                    "search": false,
                    "type": "text",
                    "sort": true,
                    "key": "searchTrucker",
                    "prefWidth": "200"
                }

                $scope.cfsHeadArr.push(obj);

            }

        }
        $scope.search();
    }




    $scope.selectedRecordIndex = 0;

    $scope.sortSelection = {
        sortKey: "receiptNumber",
        sortOrder: "desc"
    };

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_VIEW)) {
            $scope.selectedRowIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var param = {
                cfsId: data.id,
                cfsIndex: index,
                selectedPageNumber: $scope.selectedRowIndex,
                totalRecord: $scope.totalRecord
            };
            $state.go("layout.viewAirCFS", $scope.searchDtoToStateParams(param));
        }
    };

    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                param.recordPerPage = 1;
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }

                if ($scope.searchDto.searchReceiptNumber != undefined && $scope.searchDto.searchReceiptNumber != null) {
                    param.searchReceiptNumber = $scope.searchDto.searchReceiptNumber;
                }
                if ($scope.searchDto.searchReceiptDate != undefined && $scope.searchDto.searchReceiptDate != null) {
                    param.searchReceiptDate = $scope.searchDto.searchReceiptDate;
                }
                if ($scope.searchDto.searchCSF != undefined && $scope.searchDto.searchCSF != null) {
                    param.searchCSF = $scope.searchDto.searchCSF;
                }
                if ($scope.searchDto.searchShipmentUid != undefined && $scope.searchDto.searchShipmentUid != null) {
                    param.searchShipmentUid = $scope.searchDto.searchShipmentUid;
                }
                if ($scope.searchDto.searchProNumber != undefined && $scope.searchDto.searchProNumber != null) {
                    param.searchProNumber = $scope.searchDto.searchProNumber;
                }
                if ($scope.searchDto.searchProDate != undefined && $scope.searchDto.searchProDate != null) {
                    param.searchProDate = $scope.searchDto.searchProDate;
                }
                if ($scope.searchDto.searchShipper != undefined && $scope.searchDto.searchShipper != null) {
                    param.searchShipper = $scope.searchDto.searchShipper;
                }
                if ($scope.searchDto.searchConsignee != undefined && $scope.searchDto.searchConsignee != null) {
                    param.searchConsignee = $scope.searchDto.searchConsignee;
                }
                if ($scope.searchDto.searchShipperRefNumber != undefined && $scope.searchDto.searchShipperRefNumber != null) {
                    param.searchShipperRefNumber = $scope.searchDto.searchShipperRefNumber;
                }
                if ($scope.searchDto.searchOnHand != undefined && $scope.searchDto.searchOnHand != null) {
                    param.searchOnHand = $scope.searchDto.searchOnHand;
                }
                if ($scope.searchDto.searchTrucker != undefined && $scope.searchDto.searchTrucker != null) {
                    param.searchTrucker = $scope.searchDto.searchTrucker;
                }

            }

        }
        return param;
    }

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;

            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchReceiptNumber != undefined && $stateParams.searchReceiptNumber != null) {
                stateParameters.searchReceiptNumber = $scope.searchDto.searchReceiptNumber = $stateParams.searchReceiptNumber;
            }
            if ($stateParams.searchReceiptDate != undefined && $stateParams.searchReceiptDate != null) {
                stateParameters.searchReceiptDate = $scope.searchDto.searchReceiptDate = $stateParams.searchReceiptDate;
            }
            if ($stateParams.searchCSF != undefined && $stateParams.searchCSF != null) {
                stateParameters.searchCSF = $scope.searchDto.searchCSF = $stateParams.searchCSF;
            }
            if ($stateParams.searchShipmentUid != undefined && $stateParams.searchShipmentUid != null) {
                stateParameters.searchShipmentUid = $scope.searchDto.searchShipmentUid = $stateParams.searchShipmentUid;
            }
            if ($stateParams.searchProNumber != undefined && $stateParams.searchProNumber != null) {
                stateParameters.searchProNumber = $scope.searchDto.searchProNumber = $stateParams.searchProNumber;
            }
            if ($stateParams.searchProDate != undefined && $stateParams.searchProDate != null) {
                stateParameters.searchProDate = $scope.searchDto.searchProDate = $stateParams.searchProDate;
            }
            if ($stateParams.searchShipper != undefined && $stateParams.searchShipper != null) {
                stateParameters.searchShipper = $scope.searchDto.searchShipper = $stateParams.searchShipper;
            }
            if ($stateParams.searchConsignee != undefined && $stateParams.searchConsignee != null) {
                stateParameters.searchConsignee = $scope.searchDto.searchConsignee = $stateParams.searchConsignee;
            }
            if ($stateParams.searchShipperRefNumber != undefined && $stateParams.searchShipperRefNumber != null) {
                stateParameters.searchShipperRefNumber = $scope.searchDto.searchShipperRefNumber = $stateParams.searchShipperRefNumber;
            }
            if ($stateParams.searchOnHand != undefined && $stateParams.searchOnHand != null) {
                stateParameters.searchOnHand = $scope.searchDto.searchOnHand = $stateParams.searchOnHand;
            }
            if ($stateParams.searchTrucker != undefined && $stateParams.searchTrucker != null) {
                stateParameters.searchTrucker = $scope.searchDto.searchTrucker = $stateParams.searchTrucker;
            }

        } else {
            return;
        }
        CFSReceiveSearch.query($scope.searchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;

            stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.cfsId = data.responseObject.searchResult[0].id;
            stateParameters.totalRecord = $scope.totalRecord;
            $state.go("layout.viewAirCFS", stateParameters);
        });
    }


    $scope.limitChange = function(item) {

        console.log("Limit Change is called ", item);

        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    };

    $scope.changePage = function(param) {

        console.log("Change Page is called ", param);

        $scope.page = param.page;
        $scope.limit = param.size;

        $scope.search();
    };

    $scope.changeSearch = function(param) {
        console.log("change Search is called ", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("sort Change is called ", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.search = function() {

        console.log("Search is called.");

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        $scope.tmpSearchDto = cloneService.clone($scope.searchDto);

        if ($scope.tmpSearchDto.searchReceiptDate != null) {
            $scope.tmpSearchDto.searchReceiptDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchReceiptDate.startDate);
            $scope.tmpSearchDto.searchReceiptDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchReceiptDate.endDate);
        }

        if ($scope.tmpSearchDto.searchProDate != null) {
            $scope.tmpSearchDto.searchProDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchProDate.startDate);
            $scope.tmpSearchDto.searchProDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchProDate.endDate);
        }

        $scope.cfsArrList = [];
        CFSReceiveSearch.query($scope.tmpSearchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            console.log("$scope.totalRecord  ", $scope.totalRecord);
            $scope.cfsArrList = data.responseObject.searchResult;
            console.log("$scope.cfsArrList ", $scope.cfsArrList);
        });

    }


    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.init = function() {
        console.log("Init method called.");
        $scope.cfsReceiveEntry = {};
        $scope.showHistory = false;

        $scope.totalRecord = 10;
        $scope.detailTab = 'active';
        $scope.searchDto = {};
        $scope.search();
    }


    $scope.setSearch = function(tab) {
        console.log("Tab Selected " + tab);
        $scope.search();
    }

    /* Detail Page */

    $scope.back = function() {
        console.log("Back is pressed...");


        if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
            console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
            $state.go($stateParams.fromState, {
                shipmentId: $stateParams.fromStateParams
            });
        } else {
            $state.go("layout.airCFS");
        }
    };

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_CREATE)) {
            console.log("Add CFS Receive Entry button is pressed......");
            $state.go("layout.addAirCFS");
        }
    }

    $scope.editCFS = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_MODIFY)) {
            console.log("Edit Button is Pressed....")
            var localRefStateParam = {
                cfsId: $scope.cfsReceiveEntry.id,
                serviceUid: $scope.cfsReceiveEntry.serviceUid,
            };

            var ishistoryFounded = false;
            if ($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0) {
                for (var iIndex = 0; iIndex < $rootScope.unfinishedFormHistoryList.length; iIndex++) {
                    var tit = "CFS Receive Entry Edit # " + $scope.cfsReceiveEntry.id;
                    if ($rootScope.unfinishedFormHistoryList[iIndex].title == tit) {
                        var unSavedData = $rootScope.unfinishedFormHistoryList[iIndex];
                        GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[iIndex], undefined); // $rootScope.unfinishedFormHistoryList.push(stateDataObject);
                        ishistoryFounded = true;
                    }
                    if (ishistoryFounded) {
                        break;
                    }
                }
            }
            if (!ishistoryFounded) {
                $state.go("layout.editAirCFS", localRefStateParam);
            }
        }
    }

    $scope.clickOnTab = function(tab) {
        if (tab == 'dimension' && $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_DIMENSION_VIEW)) {
            $scope.moreTab = tab;
        }
        if (tab == 'attachment' && $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_ATTACHMENT_VIEW)) {
            $scope.moreTab = tab;
        }
        if (tab == 'notes' && $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_NOTES_VIEW)) {
            $scope.moreTab = tab;
        }
    }

    $scope.remove = function() {
        console.log("Remove button is pressed.....");
    }



    $scope.view = function(id) {

        console.log("View is called ", id);

        CFSReceiveGet.get({
            id: id
        }, function(data) {
            if (data.responseCode == "ERR0") {
                $scope.cfsReceiveEntry = data.responseObject;
                console.log("CFSReceiveGet Successful ", $scope.cfsReceiveEntry);

                if ($scope.cfsReceiveEntry != undefined && $scope.cfsReceiveEntry != null && $scope.cfsReceiveEntry.cfsMaster != undefined && $scope.cfsReceiveEntry.cfsMaster != null && $scope.cfsReceiveEntry.cfsMaster.id != undefined) {
                    $rootScope.unfinishedFormTitle = "CFS Receive Entry # " + $scope.cfsReceiveEntry.cfsReceiptNumber;
                    $rootScope.unfinishedData = undefined;
                    $rootScope.subTitle = $scope.cfsReceiveEntry.cfsMaster.cfsName;
                    // $scope.populateAttachment();
                }

                var rHistoryObj = {
                    'title': 'CFS Receive Entry View #' + $scope.cfsReceiveEntry.id,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'CFS Receive Entry',
                    'serviceType': 'AIR'
                }

                RecentHistorySaveService.form(rHistoryObj);

                if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
                    $scope.showButtonFlag = true;
                } else {
                    $scope.showButtonFlag = false;
                }


            } else {
                console.log("CFSReceiveGet Error ", data.responseDescription);
            }
        }, function(error) {
            console.log("CFSReceiveGet failed ", error)
        });


    }



    $scope.attachConfig = {
        "isEdit": false, //false for view only mode
        "isDelete": false, //
        "columnDefs": [{
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                width: "inl_l"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file"
            }
        ]
    }

    $scope.downloadAttach = function(param) {
        console.log(param);
        console.log(param.data.id);
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL", param.data.file);
            downloadFactory.downloadAttachment('/api/v1/cfsreceive/files/', param.data.id, param.data.fileName);
        }
    }

    $scope.goToShipmentViewByShipmentUid = function(shipmentUid) {
        console.log("State  : ", $state)
        console.log("State Param  : ", $stateParams)
        console.log("shipmentUid ", shipmentUid)
        ShipmentIdBasedOnUid.get({
                shipmentUid: shipmentUid
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    var param = {
                        shipmentId: data.responseObject,
                        fromState: $state.current.name,
                        fromStateParams: JSON.stringify($stateParams)

                    }
                    $state.go("layout.viewCrmShipment", param);
                }
            },
            function(error) {
                console.log("Shipment get Failed : " + error)
            });

    }

    switch ($stateParams.action) {
        case "VIEW":

            if ($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
                $scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
            }
            if ($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
                $scope.totalRecord = parseInt($stateParams.totalRecord);
            }


            $scope.view($stateParams.cfsId);

            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airCFS"
                },
                {
                    label: "CFS Receive Entry",
                    state: "layout.airCFS"
                },
                {
                    label: "View CFS Receive Entry",
                    state: null
                }
            ];
            break;
        case "SEARCH":
            console.log("CFS Receive Entry Search Page");
            $scope.init();

            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airCFS"
                },
                {
                    label: "CFS Receive Entry",
                    state: null
                }
            ];
            break;
    }
}]);