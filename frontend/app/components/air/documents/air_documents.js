app.controller('air_doc_ctrl',['$rootScope', '$http', '$scope', '$state', '$location', '$modal', 'ngDialog', '$stateParams', 
        'ngProgressFactory', 'ConsolDocumentSearch', 'ConsolDocumentGet', 'roleConstant',
    function($rootScope, $http, $scope, $state, $location, $modal, ngDialog, $stateParams, 
        ngProgressFactory, ConsolDocumentSearch, ConsolDocumentGet, roleConstant) {

    $scope.hawbHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,
        }, {
            "name": "HAWB",
            "model": "hawb",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "hawb"

        },
        {
            "name": "Shipment ID",
            "model": "shipmentUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipmentUid"
        },
        {
            "name": "Shipper",
            "model": "shipperName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipperName"
        },
        {
            "name": "Consignee",
            "model": "consigneeName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "consigneeName"
        },
        {
            "name": "Origin",
            "model": "originName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "originName"

        },
        {
            "name": "Destination",
            "model": "destinationName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "destinationName"

        }
    ];

    $scope.mawbHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,
        }, {
            "name": "MAWB #",
            "model": "mawb",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "mawb"

        },
        {
            "name": "Master ID",
            "search": true,
            "model": "masterUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "masterUid",
        },
        {
            "name": "Shipper",
            "model": "shipperName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipperName"
        },
        {
            "name": "Consignee",
            "model": "consigneeName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "consigneeName"
        },
        {
            "name": "Origin",
            "model": "originName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "originName"

        },
        {
            "name": "Destination",
            "model": "destinationName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "destinationName"

        }
    ];

    $scope.doHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,
        },
        {
            "name": "Delivery Order #",
            "model": "doNumber",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "doNumber"

        },
        {
            "name": "Shipment ID",
            "model": "shipmentUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipmentUid"
        },

        {
            "name": "Shipper",
            "model": "shipperName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipperName"
        },
        {
            "name": "Consignee",
            "model": "consigneeName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "consigneeName"
        },
        {
            "name": "Origin",
            "model": "originName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "originName"

        },
        {
            "name": "Destination",
            "model": "destinationName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "destinationName"

        }
    ];


    $scope.init = function() {
        $scope.searchDto = {};
        $scope.searchSortSelection = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search();
    }

    $scope.tabChange = function(tabName) {
        $scope.detailTab = tabName;
        $scope.init();
    }


    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.AIR_DOCUMENT_VIEW)) {
            if ($scope.detailTab == "HAWB") {
                $state.go("layout.airDocumentsView", {
                    id: data.id,
                    docType: 'HAWB',
                    docNo: data.hawb
                });
            } else if ($scope.detailTab == "MAWB") {
                $state.go("layout.airDocumentsView", {
                    id: data.id,
                    docType: 'MAWB',
                    docNo: data.mawb
                });
            } else if ($scope.detailTab == "DO") {
                $state.go("layout.airDocumentsView", {
                    id: data.id,
                    docType: 'DO',
                    docNo: data.doNumber
                });
            }
        }
    };

    $scope.search = function() {
        $scope.documentArr = [];
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.searchDto.whichReport = $scope.detailTab;
        ConsolDocumentSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.documentArr = resultArr;
        });

    }


    $scope.changePage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();

    };

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();

    };

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();

    }

    $scope.back = function() {
        $state.go('layout.airDocuments');
    }

    $scope.getDocumentById = function(param) {
        $scope.detailTab = param.docType;
        $scope.detailTabNo = param.docNo;
        ConsolDocumentGet.get({
            "id": param.id,
            "docType": param.docType
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("DocumentDetailView Successfully")
                $scope.airDocument = data.responseObject;
            } else {
                console.log("DocumentDetailView Failed " + data.responseDescription)
            }
        }, function(error) {
            console.log("DocumentDetailView Failed : " + error)
        });

    };
    switch ($stateParams.action) {
        case "SEARCH":
            $scope.detailTab = "HAWB";
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airDocuments"
            }, {
                label: "Documents",
                state: "layout.airDocuments"
            }];
            break;
        case "VIEW":
            $scope.getDocumentById($stateParams);
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airDocuments"
            }, {
                label: "Documents",
                state: "layout.airDocuments"
            }, {
                label: "View",
                state: null
            }];
            break;
    }


}]);