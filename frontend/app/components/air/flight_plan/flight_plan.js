/**
 * Created by hmspl on 12/4/16.
 */
app.controller('flight_plan_Ctrl',['$rootScope', '$http', '$scope', '$location', '$modal', 'ngDialog',
    'FlightPlanSearch', 'FlightPlanView', 'FlightPlanCopy', 'FlightPlanRemove', 
    'FlightPlanService', '$state', '$stateParams', 'roleConstant',
    function($rootScope, $http, $scope, $location, $modal, ngDialog,
    FlightPlanSearch, FlightPlanView, FlightPlanCopy, FlightPlanRemove, 
    FlightPlanService, $state, $stateParams, roleConstant) {

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 10;

    $scope.searchDto = {};

    $scope.init = function() {

        $scope.detailView = false;
        $scope.flight = {};
        $scope.detailTab = 'all';
        $rootScope.setNavigate1("Air");
        $rootScope.setNavigate2("Flight Schedule");
        $scope.searchDto = {};
        $scope.searchAllTab();

    }


    $scope.tabChange = function(tabName) {
        console.log(tabName + " tab changed....");
        $scope.pageActive = 0;
        $scope.page = 0;
        if (tabName == 'active') {} else {
            $scope.searchDto = {};
            $scope.searchAllTab();
        }
        $scope.detailTab = tabName;
    }

    $scope.rowSelectAll = function(param, index) {
        if ($rootScope.roleAccess(roleConstant.AIR_FLIGHT_SCHEDULE_VIEW)) {
            console.log("rowSelectAll called", param, index);
            $scope.view(param.id, false);
        }
    };

    $scope.view = function(flightPlanid, isEditPage) {
        FlightPlanView.get({
            id: flightPlanid
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Flight Plan view Successfully")
                if (isEditPage) {
                    $scope.flightPlan = data.responseObject;
                    $scope.edit();
                } else {
                    $scope.flightPlan = data.responseObject;
                    $scope.detailView = true;
                    console.log("Going to Detail view", $scope.flightPlan);
                }

            } else {
                console.log("Flight Plan view Failed " + data.responseDescription)
            }
        }, function(error) {
            console.log("Flight Plan view Failed : " + error)
        });
    }

    $scope.searchHeadArrAll = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false
        },
        {
            "": "",
            "width": "w15px",
            "prefWidth": "15",
            "model": "icon",
            "search": false
        },
        {
            "name": "Schedule ID",
            "search": true,
            "model": "scheduleId",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "scheduleId"
        },
        {
            "name": "Carrier Name",
            "model": "carrierMaster.carrierName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "carrier"
        },
        {
            "name": "Flight No",
            "search": true,
            "model": "flightNo",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "flightNo"
        },
        {
            "name": "Airport of Loading",
            "model": "pol.portName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "pol"
        },
        {
            "name": "Airport of Discharge",
            "model": "pod.portName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "pod"
        },
        {
            "name": "ETD",
            "model": "etd",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "etd"

        },
        {
            "name": "ETA",
            "model": "eta",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "eta"

        }
        /*{
            "name":"Capacity Reserved (Kg)",
            "model":"capacityReserved",
            "search":true,
            "wrap_cell":true,
            "type":"number",
            "sort":true,
            "width":"w150px",
            "prefWidth":"150",
            "key":"capacityReserved"
        },*/
        //{
        //    "name":"Status",
        //    "width":"w150px",
        //    "model":"flightPlanStatus",
        //    "search":true,
        //    "type":"drop",
        //    "data":$rootScope.enum['FlightPlanStatus'],
        //    "key":"status",
        //    "sort":true
        //}
    ];

    $scope.searchAllTab = function() {

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        if ($scope.searchDto.etd != null) {
            $scope.searchDto.etd.startDate = $rootScope.sendApiDateAndTime($scope.searchDto.etd.startDate);
            $scope.searchDto.etd.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.etd.endDate);
        }
        if ($scope.searchDto.eta != null) {
            $scope.searchDto.eta.startDate = $rootScope.sendApiDateAndTime($scope.searchDto.eta.startDate);
            $scope.searchDto.eta.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.eta.endDate);
        }
        FlightPlanSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = data.responseObject.searchResult;
            var resultArr = [];
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                if (tempObj.connectionList != undefined && tempObj.connectionList.length != 0) {
                    tempObj.key = tempObj.id;
                    if (tempObj.connectionList.length == 1) {
                        tempObj.role = "parent";
                    } else {
                        tempObj.role = "child";
                        for (var i = 0; i < tempObj.connectionList.length; i++) {
                            tempObj.connectionList[i].role = "child";
                            tempObj.connectionList[i].id = tempObj.id;
                        }
                        /*if(tempObj.connectionList.length > 1) {
                            console.log("Removing first element.")
                            tempObj.connectionList.shift();
                        }*/
                    }
                }
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.flightPlanArr = resultArr;
            console.log("Flight Plan Array", $scope.flightPlanArr);
        });

    }

    $scope.changePage = function(param) {
        console.log("Change Page is called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.cancel();
        $scope.searchAllTab();
    };

    $scope.limitChange = function(item) {
        console.log("limitChange is called.", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.searchAllTab();
    };


    $scope.searchSortSelection = {
        sortKey: "scheduleId",
        sortOrder: "desc"
    };


    $scope.sortChangeAll = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.searchAllTab();
    }

    $scope.changeSearchAll = function(param) {
        console.log("changeSearchAll ", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.searchAllTab();
        console.log("change search", $scope.searchDto);
    }



    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_FLIGHT_SCHEDULE_CREATE)) {
            $rootScope.clientMessage = null;
            FlightPlanService.set({});
            FlightPlanService.setLoc("FLIGHT");
            // $location.path('/air/flight_plan_operation');
            $state.go("layout.addFlightPlan");
        }
    };

    $scope.edit = function() {
        console.log("Flight Plan Edit Button is Pressed.....");
        FlightPlanService.set($scope.flightPlan);
        FlightPlanService.setLoc("FLIGHT_PLAN");
        //$location.path('/air/flight_plan_operation');
        $state.go("layout.editFlightPlan");

    };


    $scope.cancel = function() {
        $scope.detailView = false;
    };

    $scope.deleteFlightPlan = function() {

        console.log("Delete is called");


        ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected flight plan?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                plain: true,
                className: 'ngdialog-theme-default'
            })
            .then(
                function(value) {
                    FlightPlanRemove.remove({
                            id: $scope.flightPlan.id
                        },
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                console.log("FlightPlan deleted Successfully")
                                $scope.cancel();
                                $scope.init();
                            } else {
                                console.log("FlightPlan deleted Failed " + data.responseDescription)
                            }
                        },
                        function(error) {
                            console.log("FlightPlan deleted Failed : " + error)
                        });
                },
                function(value) {
                    console.log("FlightPlan deleted cancelled");
                });
    }



    var myOtherModal = null;
    $scope.showModal = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_FLIGHT_SCHEDULE_COPY_SCHEDULE_CREATE)) {
            myOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/air/flight_plan/view/copy_schedule_popup.html',
                backdrop: 'static',
                show: false
            });
            $scope.flightPlanCopyDto = {};
            myOtherModal.$promise.then(myOtherModal.show);
        }
    };


    $scope.copyFlightPlan = function() {

        if ($scope.validateCopy()) {

            $scope.flightPlanCopyDto.flightPlan = $scope.flightPlan;
            console.log("CopyFlightPlan", $scope.flightPlanCopyDto.flightPlan, $scope.flightPlanCopyDto.effectiveUpto);

            $scope.flightPlanCopyDto.effectiveUpto = $rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto);
            $scope.flightPlanCopyDto.flightPlan.flightFrequency = $scope.flightPlanCopyDto.flightFrequency;
            $scope.flightPlanCopyDto.copyFrom = 'VIEW';
            FlightPlanCopy.save($scope.flightPlanCopyDto).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("FlightPlanCopy successful.");
                        $scope.searchAllTab();
                        $scope.cancel();
                        myOtherModal.$promise.then(myOtherModal.hide);
                    } else {
                        console.log("FlightPlanCopy Failed " + data.responseDescription)
                    }
                },
                function(error) {
                    myOtherModal.$promise.then(myOtherModal.hide);
                    console.log("FlightPlanCopy Failed : ", error);
                });
        } else {
            console.log("validation failed in copy");
        }
    }

    $scope.validateCopy = function() {

        $scope.errorMap = new Map();

        if ($scope.flightPlanCopyDto.flightFrequency == undefined || $scope.flightPlanCopyDto.flightFrequency == null || $scope.flightPlanCopyDto.flightFrequency == "") {

            $scope.errorMap.put("flightFrequency", $rootScope.nls["ERR99519"]);
            console.log($rootScope.nls["ERR99519"]);
            return false;

        } else {
            var regexp = new RegExp("^[0-9]{0,3}$");
            if (!regexp.test($scope.flightPlanCopyDto.flightFrequency)) {
                $scope.errorMap.put("flightFrequency", $rootScope.nls["ERR99518"]);
                console.log($rootScope.nls["ERR99518"]);
                return false;
            }

            if ($scope.flightPlanCopyDto.flightFrequency <= 0) {
                $scope.errorMap.put("flightFrequency", $rootScope.nls["ERR99518"]);
                console.log($rootScope.nls["ERR99518"]);
                return false;
            }

        }
        if ($scope.flightPlanCopyDto.effectiveUpto == undefined || $scope.flightPlanCopyDto.effectiveUpto == null || $scope.flightPlanCopyDto.effectiveUpto == "") {
            $scope.errorMap.put("effectiveUpto", $rootScope.nls["ERR99527"]);
            console.log($rootScope.nls["ERR99527"]);
            return false;
        }
        if ($scope.flightPlanCopyDto.effectiveUpto != undefined && $scope.flightPlanCopyDto.effectiveUpto != null && $scope.flightPlanCopyDto.effectiveUpto != "") {
            console.log($rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto));

            if ($rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto) < $scope.flightPlan.etd) {
                $scope.errorMap.put("effectiveUpto", $rootScope.nls["ERR99529"]);
                return false;
            }

            if ($rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto) <= $scope.flightPlan.eta) {
                $scope.errorMap.put("effectiveUpto", $rootScope.nls["ERR99530"]);
                return false;
            }
            /* if($rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto)<=$scope.flightPlan.eta){
                  $scope.errorMap.put("effectiveUpto",$rootScope.nls["ERR99531"]);
                  $scope.errorMap.put("effectiveUpto","ETA date And ETD upto Date is same");
                  return false;
              }*/
        }



        return true
    }


    /*$scope.copyFlightPlan = function() {

  
        $scope.errorMap = new Map();
        if($scope.flightPlanCopyDto.effectiveUpto!=null&&$scope.flightPlanCopyDto.effectiveUpto!=undefined&&$scope.flightPlanCopyDto.effectiveUpto!=""){
        $scope.flightPlanCopyDto.flightPlan = $scope.flightPlan;
        console.log("CopyFlightPlan", $scope.flightPlanCopyDto.flightPlan, $scope.flightPlanCopyDto.effectiveUpto);
        $scope.flightPlanCopyDto.effectiveUpto = $rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto);
        $scope.flightPlan.effectiveUpto = $scope.flightPlanCopyDto.effectiveUpto;
        console.log("Flight Plan Copy Button is Pressed.....");
        myOtherModal.$promise.then(myOtherModal.hide);
        FlightPlanService.set($scope.flightPlan);
        FlightPlanService.setLoc("COPY_FLIGHT_PLAN");
        $location.path('/air/flight_plan_operation');
        }else{
            console.log("effectiveUpto invalid");
            $scope.errorMap.put("effectiveUpDate",$rootScope.nls["ERR99527"]);
            return false;
        }
        
    }*/

    $scope.dateConversions = function() {

        $scope.flightPlanCopyDto.flightPlan.effectiveUpto = $rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.flightPlan.effectiveUpto);

        $scope.flightPlanCopyDto.effectiveUpto = $rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.effectiveUpto);

        if ($scope.flightPlanCopyDto.flightPlan.connectionList != null && $scope.flightPlanCopyDto.flightPlan.connectionList.length != 0) {
            for (var i = 0; i < $scope.flightPlan.connectionList.length; i++) {
                $scope.flightPlanCopyDto.flightPlan.connectionList[i].eta = $rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.flightPlan.connectionList[i].eta);
                $scope.flightPlanCopyDto.flightPlan.connectionList[i].etd = $rootScope.sendApiDateAndTime($scope.flightPlanCopyDto.flightPlan.connectionList[i].etd);
            }
        }
    }

    // Routing

    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airFlightPlan"
                },
                {
                    label: "Flight Schedule",
                    state: "layout.airFlightPlan"
                }
            ];
            break;
        case "VIEW":
            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airFlightPlan"
                },
                {
                    label: "Flight Schedule",
                    state: "layout.airFlightPlan"
                },
                {
                    label: "View Flight Schedule",
                    state: null
                }
            ];
            break;
    }



}]);