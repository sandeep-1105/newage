/**
 * Created by hmspl on 14/7/16.
 */
app.controller('flightPlan_edit_Ctrl',['$rootScope', '$stateParams', '$state', '$http', '$scope', '$location', '$modal', 'ngDialog',
    'PortSearchKeyword', 'FlightPlanSave', 'FlightPlanEdit', 'FlightPlanService',
    'cloneService', 'ngProgressFactory', 'FlightPlanCopy', 'appConstant', 'CommonValidationService', 'roleConstant',
    function($rootScope, $stateParams, $state, $http, $scope, $location, $modal, ngDialog,
    PortSearchKeyword, FlightPlanSave, FlightPlanEdit, FlightPlanService,
    cloneService, ngProgressFactory, FlightPlanCopy, appConstant, CommonValidationService, roleConstant){

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('service-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.initDateFormat = function() {
        $scope.directiveDateTimePickerOpts = {
            format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
            useCurrent: false,
            sideBySide: true,
            viewDate: moment()
        };
        $scope.directiveDateTimePickerOpts.widgetParent = "body";

        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";

        $scope.dateTimePickerOptionsLeft = {
            format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
            useCurrent: false,
            sideBySide: true,
            viewDate: moment()
        };
        $scope.dateTimePickerOptionsLeft.widgetParent = "body";
        $scope.dateTimePickerOptionsLeft.widgetPositioning = {
            horizontal: 'right'
        };
    }
    $scope.init = function() {

        if ($scope.flightPlan == undefined || $scope.flightPlan == null) {
            $scope.flightPlan = {};
        }

        if ($scope.flightPlan.id == null) {

            console.log("Add New Flight Plan");

            $scope.flightPlan.flightPlanStatus = "Planned";

            // Creating the Connection List and adding one empty element..
            $scope.flightPlan.connectionList = [];

            $scope.flightPlan.connectionList.push({});

            $scope.connectionTableState = true;

            $scope.connectionTableError = false;


        } else {
            console.log("Edit Flight Plan", $scope.flightPlan);
            $scope.getDateConversions();
            console.log("Location", FlightPlanService.getLoc())
            $scope.connectionTableState = true;
            if (FlightPlanService.getLoc() === "COPY_FLIGHT_PLAN") {
                $scope.copyFlag = true;
            }
        }
    }

    $scope.getDateConversions = function() {
        if ($scope.flightPlan.effectiveUpto != null)
            $scope.flightPlan.effectiveUpto = $rootScope.dateToString($scope.flightPlan.effectiveUpto);

        if ($scope.flightPlan.connectionList != null && $scope.flightPlan.connectionList.length != 0) {
            for (var i = 0; i < $scope.flightPlan.connectionList.length; i++) {
                if ($scope.flightPlan.connectionList[i].eta != null)
                    $scope.flightPlan.connectionList[i].eta = $rootScope.dateToString($scope.flightPlan.connectionList[i].eta);
                if ($scope.flightPlan.connectionList[i].etd != null)
                    $scope.flightPlan.connectionList[i].etd = $rootScope.dateToString($scope.flightPlan.connectionList[i].etd);
            }
        }

        if ($scope.flightPlan.etd != null)
            $scope.flightPlan.etd = $rootScope.dateToString($scope.flightPlan.etd);

        if ($scope.flightPlan.eta != null)
            $scope.flightPlan.eta = $rootScope.dateToString($scope.flightPlan.eta);
    }


    $scope.dateConversions = function() {
        $scope.tmpObject.effectiveUpto = $rootScope.sendApiDateAndTime($scope.tmpObject.effectiveUpto);
        if ($scope.tmpObject.connectionList != null && $scope.tmpObject.connectionList.length != 0) {
            for (var i = 0; i < $scope.tmpObject.connectionList.length; i++) {
                $scope.tmpObject.connectionList[i].eta = $rootScope.sendApiDateAndTime($scope.tmpObject.connectionList[i].eta);
                $scope.tmpObject.connectionList[i].etd = $rootScope.sendApiDateAndTime($scope.tmpObject.connectionList[i].etd);
            }
        }

        if ($scope.tmpObject.eta != null)
            $scope.tmpObject.eta = $rootScope.sendApiDateAndTime($scope.tmpObject.eta);
        if ($scope.tmpObject.etd != null)
            $scope.tmpObject.etd = $rootScope.sendApiDateAndTime($scope.tmpObject.etd);
    }


    $scope.save = function() {
        console.log("Save method is called....", $scope.flightPlan);
        console.log("valid table state", $scope.tableValidCheck());
        if ($scope.validateFlightPlan(0)) {

            //if($scope.connectionTableState) {
            if ($scope.tableValidCheck()) {

                $scope.tmpObject = cloneService.clone($scope.flightPlan);

                $scope.dateConversions();

                $scope.contained_progressbar.start();

                FlightPlanSave.save($scope.tmpObject).$promise.then(function(data) {

                    if (data.responseCode == 'ERR0') {

                        var params = {}
                        params.submitAction = 'Saved';
                        $state.go("layout.airFlightPlan", params);

                    } else {

                        console.log("Flight Plan adding Failed " + data.responseDescription)

                    }

                }, function(error) {

                    console.log("Flight Plan adding Failed : " + error)

                });

                $scope.contained_progressbar.complete();

                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");

            } else {

                $rootScope.errorMap = new Map();
                //$rootScope.errorMap.put("connectionError",$rootScope.nls["ERR99526"]);
                $rootScope.clientMessage = $rootScope.nls["ERR99526"];
                $scope.connectionTableError = true;
                console.log("Connections are not valid");
                /*$rootScope.respCode = "ERR1";
                $rootScope.respDesc =$rootScope.nls["ERR99526"];*/

                return false;
            }

        } else {

            console.log("Flight Plan is not Validated.....");

        }
    }

    $scope.update = function() {

        console.log("Update method is called....", $scope.flightPlan);

        if ($scope.validateFlightPlan(0)) {
            /*if($scope.connectionTableState) {*/
            $scope.tmpObject = cloneService.clone($scope.flightPlan);
            $scope.dateConversions();
            $scope.contained_progressbar.start();
            FlightPlanEdit.update($scope.tmpObject).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.listview();
                } else {
                    console.log("Flight Plan Update Failed.", data.responseDescription);
                }
            }, function(error) {
                console.log("Flight Plan Update Failed.", error);
            });
            $scope.contained_progressbar.complete();
            angular.element(".panel-body").animate({
                scrollTop: 0
            }, "slow");
            /*} else {
            	$scope.errorMap = new Map();
            	$scope.errorMap.put("connectionError",$rootScope.nls["ERR99526"]);
            	console.log("Connections are not valid");
            	return false;
            }*/
        } else {
            console.log("Flight Plan is not Validated.....");
        }


    }




    $scope.cancel = function() {
        var tempObj = $scope.flightPlan;
        $scope.isDirty = $scope.flightPlanForm.$dirty;
        if ($scope.isDirty) {
            if (Object.keys(tempObj).length) {
                ngDialog.openConfirm({
                        template: '<p>Do you want to save your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        closeByDocument: false,
                        className: 'ngdialog-theme-default'
                    })
                    .then(function(value) {
                        if (value == 1 && $scope.flightPlan.id == null) {
                            $scope.save();
                        } else if (value == 1 && $scope.flightPlan.id != null) {
                            $scope.update();
                        } else if (value == 2) {
                            var params = {}
                            params.submitAction = 'Cancelled';
                            $state.go("layout.airFlightPlan", params);
                        } else {
                            var params = {}
                            params.submitAction = 'Cancelled';
                            $state.go("layout.airFlightPlan", params);
                        }
                    });
            } else {
                var params = {}
                params.submitAction = 'Cancelled';
                $state.go("layout.airFlightPlan", params);
            }
        } else {
            var params = {}
            params.submitAction = 'Cancelled';
            $state.go("layout.airFlightPlan", params);
        }
    }

    $scope.listview = function() {
        var params = {}
        params.submitAction = 'list';
        $state.go("layout.airFlightPlan", params);;
    }

    $scope.openEvent = function(param) {
        if (param == true) {
            $scope.dropStyle = {
                "overflow-x": "hidden"
            };
        } else {
            $scope.dropStyle = {
                "overflow-x": "auto",
                "overflow-y": "hidden"
            };
        }
    }



    $scope.validateFlightPlan = function(validateCode) {

        console.log("Validating Flight Plan ", validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 1 || validateCode == 0) {

            if ($scope.flightPlan.capacityReserved != undefined &&
                $scope.flightPlan.capacityReserved != null &&
                $scope.flightPlan.capacityReserved != "") {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FLIGHT_PLAN_CAPACITYRESERVED, $scope.flightPlan.capacityReserved)) {
                    $scope.errorMap.put("capacityReserved", $rootScope.nls["ERR99516"]);
                    return false;
                }

                if ($scope.flightPlan.capacityReserved <= 0) {
                    $scope.errorMap.put("capacityReserved", $rootScope.nls["ERR99516"]);
                    console.log($rootScope.nls["ERR99516"]);
                    return false;
                }

            }

        }

        if (validateCode == 2 || validateCode == 0) {

        }

        /*if(validateCode == 3 || validateCode == 0) {
    		

    		
        	
	    	if($scope.flightPlan.flightFrequency == undefined 
	    				|| $scope.flightPlan.flightFrequency == null 
	    				||  $scope.flightPlan.flightFrequency == "") {
	    			
	    			$scope.errorMap.put("flightFrequency",$rootScope.nls["ERR99519"]);
					console.log($rootScope.nls["ERR99519"]);
					return false;
	    			
	    		}
	    	else {
    			var regexp = new RegExp("^[0-9]{0,3}$");
    			if (!regexp.test($scope.flightPlan.flightFrequency)) {
					$scope.errorMap.put("flightFrequency",$rootScope.nls["ERR99518"]);
					console.log($rootScope.nls["ERR99518"]);
					return false;
    			}
    			
    			if($scope.flightPlan.flightFrequency <= 0) {
    				$scope.errorMap.put("flightFrequency",$rootScope.nls["ERR99518"]);
        			console.log($rootScope.nls["ERR99518"]);
        			return false;	
    			}
    			
    		}
    		
    	}*/

        /*if(validateCode == 4 || validateCode == 0) {
    		if($scope.flightPlan.effectiveUpto == undefined 
    				|| $scope.flightPlan.effectiveUpto == null 
    				||  $scope.flightPlan.effectiveUpto == "") {
    			
    			$scope.errorMap.put("effectiveUpto",$rootScope.nls["ERR99521"]);
				console.log($rootScope.nls["ERR99521"]);
				return false;
    			
    		} else {
    			
    		}
    	}*/



        // Validate the number of Connections
        if (validateCode == 5 || validateCode == 0) {

            if ($scope.flightPlan.connectionList == null ||
                $scope.flightPlan.connectionList.length == 0) {

                /*$scope.errorMap.put("connectionList",$rootScope.nls["ERR99500"]);
                console.log($rootScope.nls["ERR99500"]);*/
                $scope.connectionTableState = true;
                return false;


            } else {


            }

            if ($scope.flightPlan.connectionList != null &&
                $scope.flightPlan.connectionList.length > 0) {

                if ($scope.flightPlan.connectionList[0].carrierMaster == undefined) {
                    /*$scope.errorMap.put("connectionList",$rootScope.nls["ERR99500"]);
        			console.log($rootScope.nls["ERR99500"]);*/
                    $scope.connectionTableState = true;
                    return false;
                }
            }
        }


        return true;

    };


    $scope.fnConnectionTableState = function(param) {
        $scope.connectionTableState = param;
        console.log("fnConnectionTableState ", param);
        console.log("table state data", param);
        if (!param) {
            $scope.errorMap = new Map();
        }
    }

    //Start copy code

    $scope.copyFlightPlan = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_FLIGHT_SCHEDULE_COPY_SCHEDULE_CREATE)) {
            if ($scope.validateFlightPlanForCopy(0)) {
                $scope.flightPlanCopyDto = {};

                $scope.tmpObject = cloneService.clone($scope.flightPlan);
                $scope.dateConversions();

                $scope.flightPlanCopyDto.flightPlan = $scope.tmpObject;

                console.log("CopyFlightPlan", $scope.flightPlanCopyDto.flightPlan, $scope.flightPlan.effectiveUpto);

                $scope.flightPlanCopyDto.effectiveUpto = $rootScope.sendApiDateAndTime($scope.flightPlan.effectiveUpto);

                FlightPlanCopy.save($scope.flightPlanCopyDto).$promise.then(function(data) {
                        if (data.responseCode == "ERR0") {
                            console.log("FlightPlanCopy successful.");
                            var params = {}
                            params.submitAction = 'Copied';
                            $state.go("layout.airFlightPlan", params);
                        } else {
                            console.log("FlightPlanCopy Failed " + data.responseDescription)
                        }
                    },
                    function(error) {
                        console.log("FlightPlanCopy Failed : ", error);
                    });

            } else {

                console.log("Flight Plan is not Validated.....");

            }
        }
    }


    $scope.validateFlightPlanForCopy = function(validateCode) {

        console.log("Validating Flight Plan ", validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 1 || validateCode == 0) {

            if ($scope.flightPlan.capacityReserved != undefined &&
                $scope.flightPlan.capacityReserved != null &&
                $scope.flightPlan.capacityReserved != "") {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FLIGHT_PLAN_CAPACITYRESERVED, $scope.flightPlan.capacityReserved)) {
                    $scope.errorMap.put("capacityReserved", $rootScope.nls["ERR99516"]);
                    return false;
                }

                if ($scope.flightPlan.capacityReserved <= 0) {
                    $scope.errorMap.put("capacityReserved", $rootScope.nls["ERR99516"]);
                    console.log($rootScope.nls["ERR99516"]);
                    return false;
                }

            }

        }

        if (validateCode == 2 || validateCode == 0) {

        }

        if (validateCode == 3 || validateCode == 0) {




            if ($scope.flightPlan.flightFrequency == undefined ||
                $scope.flightPlan.flightFrequency == null ||
                $scope.flightPlan.flightFrequency == "") {

                $scope.errorMap.put("flightFrequency", $rootScope.nls["ERR99519"]);
                console.log($rootScope.nls["ERR99519"]);
                return false;

            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FLIGHT_PLAN_FLIGHTFREQUENCY, $scope.flightPlan.flightFrequency)) {
                    $scope.errorMap.put("flightFrequency", $rootScope.nls["ERR99518"]);
                    console.log($rootScope.nls["ERR99518"]);
                    return false;
                }

                if ($scope.flightPlan.flightFrequency <= 0) {
                    $scope.errorMap.put("flightFrequency", $rootScope.nls["ERR99518"]);
                    console.log($rootScope.nls["ERR99518"]);
                    return false;
                }

            }




        }

        if (validateCode == 4 || validateCode == 0) {
            if ($scope.flightPlan.effectiveUpto == undefined ||
                $scope.flightPlan.effectiveUpto == null ||
                $scope.flightPlan.effectiveUpto == "") {

                $scope.errorMap.put("effectiveUpto", $rootScope.nls["ERR99521"]);
                console.log($rootScope.nls["ERR99521"]);
                return false;

            } else {

            }
        }



        // Validate the number of Connections
        if (validateCode == 5 || validateCode == 0) {

            if ($scope.flightPlan.connectionList == null ||
                $scope.flightPlan.connectionList.length == 0) {

                $scope.errorMap.put("connectionList", $rootScope.nls["ERR99500"]);
                console.log($rootScope.nls["ERR99500"]);
                $scope.connectionTableState = true;
                return false;


            } else {


            }

            if ($scope.flightPlan.connectionList != null &&
                $scope.flightPlan.connectionList.length > 0) {

                if ($scope.flightPlan.connectionList[0].carrierMaster == undefined) {
                    $scope.errorMap.put("connectionList", $rootScope.nls["ERR99500"]);
                    console.log($rootScope.nls["ERR99500"]);
                    $scope.connectionTableState = true;
                    return false;
                }
            }
        }


        return true;

    };

    //End Copy code

    // History Start

    $scope.getFlightPlanById = function(chargeId) {
        console.log("getFlightPlanById ", chargeId);

        FlightPlanView.get({
            id: flightplanId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting flight plan.", data)
                $scope.flightPlan = data.responseObject;
            }

        }, function(error) {
            console.log("Error while getting flight plan.", error)
        });

    }


    //On leave the Unfilled flight plan Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addFlightPlanEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.flightPlan;
        $rootScope.category = "Air";
        $rootScope.unfinishedFormTitle = "FlightPlan (New)";
        if ($scope.flightPlan != undefined && $scope.flightPlan != null) {
            //$rootScope.subTitle = ;
        } else {
            $rootScope.subTitle = "Unknown FlightPlan"
        }
    })

    $scope.$on('editFlightPlanEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.flightPlan;
        $rootScope.category = "Air";
        $rootScope.unfinishedFormTitle = "FlightPlan Edit";
        if ($scope.flightPlan != undefined && $scope.flightPlan != null && $scope.flightPlan.scheduleId != undefined &&
            $scope.flightPlan.scheduleId != null && $scope.flightPlan.scheduleId != "") {
            $rootScope.subTitle = $scope.flightPlan.scheduleId;
        } else {
            $rootScope.subTitle = "Unknown FlightPlan"
        }
    })


    $scope.$on('addFlightPlanEventReload', function(e, confirmation) {
        console.log("addFlightPlanEventReload is called ", $scope.flightPlan);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.flightPlan);
        localStorage.isFlightPlanReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editFlightPlanEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.flightPlan);
        localStorage.isFlightPlanReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isFlightPlanReloaded;

    if ($stateParams.action == "ADD") {
        $scope.initDateFormat();
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isFlightPlanReloaded = "NO";
                $scope.flightPlan = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.flightPlan = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isFlightPlanReloaded = "NO";
                $scope.flightPlan = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airFlightPlan"
            },
            {
                label: "Flight Schedule",
                state: "layout.airFlightPlan"
            },
            {
                label: "Add Flight Schedule",
                state: null
            }
        ];

    } else {
        $scope.initDateFormat();
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isFlightPlanReloaded = "NO";
                $scope.flightPlan = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.flightPlan = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isFlightPlanReloaded = "NO";
                $scope.flightPlan = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.getFlightPlanById($stateParams.chargeId);
                $scope.init();
            }
        }

    }

    // History End
}]);