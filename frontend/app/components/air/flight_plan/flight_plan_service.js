app.factory('FlightPlanService',[ function() {
	var savedData = {}
	var loc=null;
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
	}
	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}

	return {
		set : set,
		get : get,
		setLoc : setLoc,
		getLoc : getLoc,
	}
	
	function setLoc(data) {
		loc = data;
	}
	function getLoc() {
		
	return loc;
	}
	
}]);