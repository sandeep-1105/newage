(function() {

	app.factory("FlightPlanSearch",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("FlightPlanSearchForShipment",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/shipment/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("FlightPlanSave",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("FlightPlanEdit",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("FlightPlanView",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("FlightPlanRemove",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);


	app.factory("FlightPlanCopy",['$resource', function($resource) {
		return $resource("/api/v1/flightplan/copy", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
})();
