/**
 * Created by hmspl on 12/4/16.
 */
app.controller('stockMange_Ctrl',['$rootScope', '$http', '$scope', '$location',
    '$modal', 'ngDialog', 'StockGenerate', 'PortByTransportMode', 'consolService', 'CommonValidationService', 'appConstant',
    'CarrierByTransportMode', 'ValidateUtil', 'ConsolGet', 'NavigationService', 'stockService', 'cloneService', 
    'StockGenerationSearch', 'StockEnquirySearch', 'stockUpdate', 'ShipmentUpdateForStock', '$stateParams', '$state', 
    'roleConstant',
    function($rootScope, $http, $scope, $location,
    $modal, ngDialog, StockGenerate, PortByTransportMode, consolService, CommonValidationService, appConstant,
    CarrierByTransportMode, ValidateUtil, ConsolGet, NavigationService, stockService, cloneService, 
    StockGenerationSearch, StockEnquirySearch, stockUpdate, ShipmentUpdateForStock, $stateParams, $state, 
    roleConstant) {


    $scope.limitArr = [10, 15, 20];
    $scope.limitArrStock = [10, 15, 20];
    $scope.pageStock = 0;
    $scope.limitStock = 10;
    $scope.totalRecordStock = 10;

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 10;

    $scope.init = function() {
        $scope.stockGeneration = {};
        $scope.stockGenerationSearchDto = {};
        $scope.stockEnquirySearchDto = {};
        $scope.stockGenerationSearchDto.noOfDays = 60;
        $scope.getDetailsByDate();
        $scope.stockGeneration.por = {};
        $scope.stockGeneration.carrier = {};
        $scope.stockGeneration.service = {};
        //$scope.ajaxCarrierEvent(null);
        //$scope.ajaxConnPorEvent(null);
        $scope.detailTab = 'stockGeneration';
        $rootScope.setNavigate1("Air");
        $rootScope.setNavigate2("MAWB Stock Generation");
        $scope.searchDays = true;
        $scope.searchbyData = false;
        $scope.viewModalflag = true;
        $scope.generate = false; //indicates operation performs on stock generation
        $scope.generationflag = true;
        $scope.searchchange = false; //Flag used while search-filter operation with date 0f 60 days
        $scope.stockData = true; //Flag is used for the pop up will come only at click on view button, not on every serach

    };

    //for Sort Generation Tab Data

    $scope.stockGenerationArr = [

        {
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "Carrier",
            "search": true,
            "model": "carrier.carrierName",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "carrierName"
        },
        {
            "name": "Origin",
            "model": "por.portCode",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "portCode"
        },
        {
            "name": "Carrier No",
            "search": true,
            "model": "carrier.carrierNo",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "carrierNo"
        },
        {
            "name": "Serial No",
            "model": "awbNo",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "awbNo"
        },
        {
            "name": "Check Digit",
            "model": "checkDigit",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w80px",
            "prefWidth": "80",
            "key": "checkDigit"

        },
        {
            "name": "MAWB No",
            "model": "mawbNo",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w125px",
            "prefWidth": "125",
            "key": "mawbNo"

        },
        {
            "name": "Service Code",
            "model": "serviceCode",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w80px",
            "prefWidth": "80",
            "key": "serviceCode"
        },
        {
            "name": "Master Id",
            "model": "consol.consolUid",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w125px",
            "prefWidth": "125",
            "key": "consolUid"
        },
        {
            "name": "Status",
            "width": "w125px",
            "prefWidth": "125",
            "model": "stockStatus",
            "search": true,
            "type": "drop",
            "data": $rootScope.enum['StockStatus'],
            "key": "stockStatus",
            "sort": true
        },

        {
            "name": "Actions",
            "width": "w80px",
            "model": "",
            "search": false,
            "type": "action",
            "prefWidth": "80"

        }

    ]

    $scope.stockSortSelection = {
        sortKey: "carrier.carrierName",
        sortOrder: "asc"
    };

    $scope.changeSearchStock = function(param) {

        if ($scope.detailTab == 'stockEnquiry') {
            $scope.stockGenerationSearchDto = {};
            $scope.stockEnquirySearchDto = {};
            for (var attrname in param) {
                $scope.stockEnquirySearchDto[attrname] = param[attrname];
            }
            $scope.stockEnquirySearch();
        } else {

            for (var attrname in param) {
                $scope.stockGenerationSearchDto[attrname] = param[attrname];
            }
            $scope.pageStock = 0;
            if ($scope.generate) {
                $scope.generationflag = false;
                $scope.searchGeneratedStock();
            }
            if ($scope.searchDays) {
                $scope.getDetailsByDate();
                $scope.searchbyData = false;
                $scope.searchchange = true;
            }
            if ($scope.searchbyData) {
                $scope.stockData = false;
                $scope.stockByData();
                $scope.searchDays = false;
            }
        }
    }

    $scope.sortChangeStock = function(param) {
        $scope.stockGenerationSearchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.stockGenerationSearchDto.sortByColumn = param.sortKey;
        if ($scope.detailTab == 'stockEnquiry') {
            $scope.stockGenerationSearchDto = {};
            $scope.stockEnquirySearchDto = {};
            $scope.stockEnquirySearchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.stockEnquirySearchDto.sortByColumn = param.sortKey;
            if (param.sortKey == "carrierNo" || param.sortKey == "carrierName" || param.sortKey == "portName") {
                $scope.stockEnquirySearchDto.sortByDynamicColumn = null;
                if ($scope.stockEnquirySearchDto.sortByColumn == "carrierNo") {
                    $scope.stockEnquirySearchDto.sortByColumn = "carrier.carrierNo";
                } else if ($scope.stockEnquirySearchDto.sortByColumn == "carrierName") {
                    $scope.stockEnquirySearchDto.sortByColumn = "carrier.carrierName";
                } else if ($scope.stockEnquirySearchDto.sortByColumn == "portName") {
                    $scope.stockEnquirySearchDto.sortByColumn = "por.portName";
                }

            } else {
                $scope.stockEnquirySearchDto.sortByColumn = null;
                $scope.stockEnquirySearchDto.sortByDynamicColumn = param.sortKey;
            }
            $scope.stockEnquirySearch();
        } else {
            if ($scope.generate) {
                $scope.searchbyData = false;
                $scope.generationflag = false;
                $scope.searchGeneratedStock();
            }
            if ($scope.searchDays) {
                $scope.getDetailsByDate();
                $scope.searchbyData = false;
            }
            if ($scope.searchbyData) {
                $scope.stockData = false;
                $scope.stockByData();
                $scope.searchDays = false;
            }
        }

    }

    $scope.getDetailsByDate = function() {

        if ($scope.validateDays()) {

            if ($scope.stockGenerationSearchDto.noOfDays == undefined || $scope.stockGenerationSearchDto.noOfDays == null || $scope.stockGenerationSearchDto.noOfDays == "" || $scope.stockGenerationSearchDto.noOfDays == 0) {
                $scope.stockGenerationSearchDto.noOfDays = 60;
            }
            $scope.stockGenerationSearchDto.selectedPageNumber = $scope.pageStock;
            $scope.stockGenerationSearchDto.recordPerPage = $scope.limitStock;
            if (!$scope.searchchange) {
                $scope.stockGenerationSearchDto.carrierName = null;
                $scope.stockGenerationSearchDto.portCode = null;
                $scope.stockGenerationSearchDto.endingNo = null;
            }
            $scope.stockGeneration = {};
            if ($scope.stockGenerationSearchDto.noOfDays != undefined && $scope.stockGenerationSearchDto.noOfDays != null) {
                var today = new Date()
                var priorDate = new Date().setDate(today.getDate() - $scope.stockGenerationSearchDto.noOfDays);
                $scope.stockGenerationSearchDto.receivedOn = $rootScope.sendApiStartDateTime(moment().valueOf());
                $scope.stockGenerationSearchDto.priorDate = $rootScope.sendApiStartDateTime(priorDate);
            }
            StockGenerationSearch.query($scope.stockGenerationSearchDto).$promise.then(function(
                data, status) {

                $scope.totalRecordStock = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.pageStock * $scope.limitStock);
                    //tempObj.routedBy=tempObj.whoRouted=='Self'?tempObj.salesman.employeeName:tempObj.agent.partyName;
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.stockGenerationArrList = resultArr;
                $scope.searchDays = true;
                $scope.searchbyData = false;
                $scope.generate = false;

            });

        } else {
            console.log("Validation failed");

        }
    }

    $scope.stockByData = function() {
        $scope.stockGenerationSearchDto.carrier = {};
        $scope.stockGenerationSearchDto.por = {};
        $scope.stockGenerationSearchDto.endingNo = null;
        $scope.stockGenerationSearchDto.noOfDays = null;
        $scope.stockGenerationSearchDto.status = "Available";
        if ($scope.validateTotalStocks(1) && $scope.validateTotalStocks(3)) {
            $scope.stockGenerationSearchDto.carrierName = $scope.stockGeneration.carrier.carrierName;
            $scope.stockGenerationSearchDto.portCode = $scope.stockGeneration.por.portCode;
            $scope.stockGenerationSearchDto.selectedPageNumber = $scope.pageStock;
            $scope.stockGenerationSearchDto.recordPerPage = $scope.limitStock;
            StockGenerationSearch.query($scope.stockGenerationSearchDto).$promise.then(function(
                data, status) {
                $scope.totalRecordStock = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.pageStock * $scope.limitStock);
                    //tempObj.routedBy=tempObj.whoRouted=='Self'?tempObj.salesman.employeeName:tempObj.agent.partyName;
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.searchDays = false;
                $scope.searchbyData = true;
                $scope.generate = false;
                $scope.stockGenerationArrList = resultArr;
                if ($scope.pageStock == 0 && $scope.stockData) {
                    $scope.viewModal();
                }

            });
        } else {
            console.log("validation failed")
        }
    }

    $scope.setflag = function() {
        $scope.stockData = true;
        $scope.searchchange = false;
        //Flag is used for the pop up will come only at click on view button, not on every serach
    }

    $scope.cancel = function() {
        console.log("Cancel is not implementd");
    };

    $scope.rowSelectStock = function(data, index) {

        if (data.consol != undefined && data.consol != null && data.consol.id != null) {
            ConsolGet.get({
                    id: data.consol.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.consol = data.responseObject;
                        $scope.showdetail = true;
                        NavigationService.set($scope.consol);
                        NavigationService.setSubData1($scope.showdetail);
                        NavigationService.setLocation('/air/stock_management/stock_management.html');
                        $location.path('/air/consol_shipments_list');
                    } else {
                        $scope.consol = null;
                    }
                },
                function(error) {
                    console.log("Consol get Failed : " + error)
                    $scope.consol = null;
                });
        } else {
            console.log("Stock consol -row select failed");

        }
    }
    $scope.limitChangeStock = function(item) {
        $scope.pageStock = 0;
        $scope.limitStock = item;
        $scope.cancel();
        if ($scope.generate) {
            $scope.searchGeneratedStock();
        }
        if ($scope.searchDays) {
            $scope.getDetailsByDate();
            $scope.searchbyData = false;
        }
        if ($scope.searchbyData) {
            $scope.stockByData();
            $scope.searchDays = false;
        }
    };

    $scope.changePageStock = function(param) {
        $scope.errorMap = new Map();
        $scope.pageStock = param.page;
        if ($scope.pageStock == 0) {
            $scope.generationflag = true;
        } else {
            $scope.generationflag = false;
        }
        $scope.limitStock = param.size;
        $scope.cancel();
        if ($scope.generate) {
            $scope.searchGeneratedStock();
        }
        if ($scope.searchDays) {
            $scope.getDetailsByDate();
            $scope.searchbyData = false;
        }
        if ($scope.searchbyData) {
            $scope.stockByData();
            $scope.searchDays = false;


        }
    };
    $scope.generateStocks = function() {
        if ($scope.validateTotalStocks(0)) {
            $scope.tmpObj = cloneService.clone($scope.stockGeneration);
            $scope.tmpObj.receivedOn = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.stockGeneration.receivedOn));
            $scope.tmpObj.receivedOnCheck = $rootScope.sendApiStartDateTime(moment().valueOf());
            $scope.popup.spinner = true;
            StockGenerate.generate($scope.tmpObj).$promise
                .then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {
                            var tempArr = [];
                            var resultArr = [];
                            tempArr = data.responseObject;
                            $scope.totalRecordStock = data.responseObject.length;
                            $scope.pageStock = 0;
                            $scope.limitStock = 10;
                            $scope.stockGenerationSearchDto.carrier = {};
                            $scope.stockGenerationSearchDto.por = {};
                            $scope.stockGenerationSearchDto.receivedOn = $scope.tmpObj.receivedOn;
                            $scope.stockGenerationSearchDto.endingNo = $scope.tmpObj.endingNo;
                            $scope.stockGenerationSearchDto.carrier = $scope.tmpObj.carrier.carrierName;
                            $scope.stockGenerationSearchDto.por = $scope.tmpObj.por.portCode;
                            $scope.stockGenerationSearchDto.status = "Available";
                            $scope.stockGenerationSearchDto.carrierName = $scope.tmpObj.carrier.carrierName;
                            $scope.stockGenerationSearchDto.portCode = $scope.tmpObj.por.portCode;
                            $scope.errorMap = new Map();
                            $scope.generationflag = true;
                            $scope.stockGeneration.carrier = null;
                            $scope.stockGeneration.por = null;
                            $scope.searchGeneratedStock();
                        } else if (data.responseCode == 'ERR100019' || data.responseCode == 'ERR100018') {
                            console.log("Respnse Failed" + data);
                            $scope.popup.spinner = false;
                            $scope.errorMap = new Map();
                            $scope.errorMap.put("AlreadyGenerated", $rootScope.nls["ERR100019"]);
                            $scope.myOtherModal.$promise.then($scope.myOtherModal.hide);
                            $scope.viewModalGenerated();
                        } else {
                            $scope.popup.spinner = false;
                            $scope.myOtherModal.$promise.then($scope.myOtherModal.hide);
                            $scope.errorMap.put("AlreadyGenerated", $rootScope.nls["ERR100025"]);
                            $scope.stockGeneration.carrier = null;
                            $scope.stockGeneration.por = null;
                            console.log("Respnse Failed" + data);
                        }
                    },
                    function(error) {
                        $scope.errorMap = new Map();
                        console.log("Respnse Failed" + error);
                        if (error != null && error.status == -1) {
                            console.log("Respnse Failed" + error);
                            $scope.popup.spinner = false;
                            $scope.errorMap.put("AlreadyGenerated", $rootScope.nls["ERR100025"]);
                            $scope.myOtherModal.$promise.then($scope.myOtherModal.hide);
                            $scope.stockGeneration.carrier = null;
                            $scope.stockGeneration.por = null;
                        } else {
                            $scope.popup.spinner = false;
                            $scope.myOtherModal.$promise.then($scope.myOtherModal.hide);
                            $scope.stockGeneration.carrier = null;
                            $scope.stockGeneration.por = null;
                            $scope.errorMap.put("AlreadyGenerated", $rootScope.nls["ERR100025"]);
                        }
                    });

        } else {
            console.log("validation failed");
        }

    }


    $scope.searchGeneratedStock = function() {
        $scope.errorMap = new Map();
        $scope.stockGenerationSearchDto.selectedPageNumber = $scope.pageStock;
        $scope.stockGenerationSearchDto.recordPerPage = $scope.limitStock;
        StockGenerationSearch.query($scope.stockGenerationSearchDto).$promise.then(function(
            data, status) {
            $scope.totalRecordStock = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.pageStock * $scope.limitStock);
                //tempObj.routedBy=tempObj.whoRouted=='Self'?tempObj.salesman.employeeName:tempObj.agent.partyName;
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.stockGenerationArrList = resultArr;
            $scope.myOtherModal.$promise.then($scope.myOtherModal.hide);
            $scope.hidePage = false;
            if ($scope.pageStock == 0 && $scope.generationflag) {
                $scope.viewModalGenerated();
            } else {
                $scope.generationflag = false;
            }
            $scope.detailTab = 'active';
            $scope.generate = true;
            $scope.stockGeneration = {};
            $scope.popup.spinner = false;
        });

    }

    $scope.ajaxCarrierEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CarrierByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.carrierMasterList = data.responseObject.searchResult;
                    return $scope.carrierMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air');
            }
        );

    }



    $scope.ajaxConnPorEvent = function(object) {
        console.log("ajaxPortOfLoadingEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PortByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.connPodList = [];
                    $scope.porList = data.responseObject.searchResult;
                    return $scope.porList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );

    }


    $scope.selectedCarrier = function(value) {

        if ($scope.validateTotalStocks(1)) {
            $scope.navigateToNextField(value);
        }
    }

    $scope.selectedPor = function(value) {

        if ($scope.validateTotalStocks(3)) {
            $scope.navigateToNextField(value);
        }
    }

    $scope.popup = {};
    $scope.showModal = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_MAWB_STOCK_GENERATION_GENERATE_STOCKS_CREATE)) {
            $scope.stockGeneration = {};
            $scope.errorMap = new Map();
            $scope.myOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/air/stock_management/view/generate_stocks_popup.html',
                backdrop: 'static',
                show: false
            });
            $scope.myOtherModal.$promise.then($scope.myOtherModal.show);
        }
    };
    $scope.viewModal = function() {
        var myTotalOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/air/stock_management/view/total_stocks_popup.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);
    };

    $scope.viewModalGenerated = function() {
        var mygeneratedModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/air/stock_management/view/newlygenerated_stocks_popup.html',
            show: false
        });
        mygeneratedModal.$promise.then(mygeneratedModal.show);
    };

    //$scope.ajaxCarrierEvent = function(object) {
    //      $scope.searchDto={};
    //          $scope.searchDto.keyword=object==null?"":object;
    //          $scope.searchDto.selectedPageNumber = 0;
    //          $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    //          CarrierByTransportMode.fetch({"transportMode":'Air'}, $scope.searchDto).$promise.then(
    //                  function(data) {
    //                      if (data.responseCode =="ERR0"){
    //                          $scope.carrierMasterList  = data.responseObject.searchResult;
    //                      }
    //             },
    //              function(errResponse){
    //                  console.error('Error while fetching Carrier List Based on Air');
    //              }
    //          );
    //    return $scope.carrierMasterList;
    //   }



    //$scope.ajaxConnPorEvent = function(object) {
    //          console.log("ajaxPortOfLoadingEvent ", object);
    //          $scope.searchDto={};
    //  $scope.searchDto.keyword=object==null?"":object;
    //  $scope.searchDto.selectedPageNumber = 0;
    //  $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    //  PortByTransportMode.fetch({"transportMode":'Air'}, $scope.searchDto).$promise.then(
    //          function(data) {
    //              if (data.responseCode =="ERR0"){
    //                  $scope.connPodList = [];
    //                  $scope.porList = data.responseObject.searchResult;
    //              }
    //       },
    //        function(errResponse){
    //            console.error('Error while fetching PortOfLoading List');
    //        }
    //  );
    //    return $scope.porList;
    //}


    $scope.selectedGeneratedCarrier = function(obj, value) {

        if ($scope.validateTotalStocks(1)) {
            if (obj != undefined && obj != "") {
                obj.carrierRefNo = obj.carrier.carrierNo;
            }
            $scope.navigateToNextField(value);
        }
    }

    $scope.selectedGeneratePor = function(value) {

        if ($scope.validateTotalStocks(3)) {
            $scope.navigateToNextField(value);
        }

    }


    $scope.changeStatusAvailable = function(param) {
        $scope.errorMap = new Map();
        $rootScope.clientMessage = null
        if (param.stockStatus != 'Generated') {
            if (param.stockStatus == 'Cancelled') {
                $scope.stockGenerationn = {};
                $scope.stockGenerationn = param;
                $scope.stockGenerationn.statusChange = 'Available';
                $scope.update(param, 'Available')
            } else if (param.stockStatus == 'Available') {
                $rootScope.clientMessage = $rootScope.nls["ERR100016"];
            } else {
                if (param.stockStatus == 'Reserved') {
                    $scope.stockGenerationn = param;
                    $scope.stockGenerationn.statusChange = 'Available';
                    $scope.showResponse(param, 'Available');
                }
            }
        } else {
            $rootScope.clientMessage = $rootScope.nls["ERR100024"];
        }
    }

    $scope.changeStatusCancelled = function(param) {
        if (param.stockStatus != 'Generated') {
            $scope.errorMap = new Map();
            if (param.stockStatus == 'Available') {
                $scope.stockGenerationn = {};
                $scope.stockGenerationn = param;
                $scope.stockGenerationn.statusChange = 'Cancelled';
                $scope.update(param, 'Cancelled');
            } else if (param.stockStatus == 'Reserved') {
                $scope.stockGenerationn = {};
                $scope.stockGenerationn = param;
                $scope.stockGenerationn.statusChange = 'Cancelled';
                $scope.showResponse(param, 'Cancelled');
            } else {
                $rootScope.clientMessage = $rootScope.nls["ERR100017"];
                return;
            }
        } else {
            $rootScope.clientMessage = $rootScope.nls["ERR100024"];
            return;
        }
    }
    $scope.showResponse = function(param, status) {
        $rootScope.clientMessage = null
        var newScope = $scope.$new();
        newScope.shipmentUid = param.shipmentUid;
        ngDialog.openConfirm({
            template: '<p>This master airway bill(MAWB) number is already used in Pre-Booking {{shipmentUid}} \n Do you want to delete this MAWB in the Pre-Booking and Re-use it.</p> ' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            if (value == 1) {

                console.log("Need to delete Mawb in Shipment and their corresponding document");
                if ($scope.stockGenerationn != undefined && $scope.stockGenerationn != null && $scope.stockGenerationn.id != undefined) {
                    stockUpdate.update($scope.stockGenerationn).$promise
                        .then(
                            function(data) {
                                if (data.responseCode == 'ERR0') {
                                    console
                                        .log("Stock status updated Successfully")
                                    if (status == 'Cancelled') {
                                        param.stockStatus = 'Cancelled';
                                        if ($scope.searchDays) {
                                            $scope.getDetailsByDate();
                                            $scope.searchbyData = false;
                                        }
                                        if ($scope.searchbyData) {
                                            $scope.stockData = false;
                                            $scope.stockByData();
                                            $scope.searchDays = false;

                                        }
                                        //update Values in shipmenet and document
                                        ShipmentUpdateForStock.save(param).$promise.then(function(data) {
                                            if (data.responseCode == 'ERR0') {

                                                console.log("Stock updated  ")
                                            } else {
                                                console.log("Stock updated Failed " + data)
                                            }
                                        }, function(error) {
                                            console.log("Shipment updated Failed : " + data)
                                        });
                                        return true;
                                    }
                                    if (status == 'Available') {
                                        param.stockStatus = 'Available';
                                        if ($scope.searchDays) {
                                            $scope.getDetailsByDate();
                                            $scope.searchbyData = false;
                                        }
                                        if ($scope.searchbyData) {
                                            $scope.stockByData();
                                            $scope.searchDays = false;
                                        }
                                        //update Values in shipmenet and document
                                        ShipmentUpdateForStock.save(param).$promise.then(function(data) {
                                            if (data.responseCode == 'ERR0') {

                                                console.log("Stock updated  ")
                                            } else {
                                                console.log("Stock updated Failed " + data)
                                            }
                                        }, function(error) {
                                            console.log("Shipment updated Failed : " + error)
                                        });
                                        return true;
                                    }

                                } else {
                                    console
                                        .log("stock updated Failed " +
                                            data.responseDescription)
                                }

                            },
                            function(error) {
                                console
                                    .log("stock updated Failed : " +
                                        error)
                            });

                }
            } else {
                console.log("hide the pop up");
            }
        });

    }


    $scope.actionChange = function(param) {

        if (param.type == 'refresh') {
            $scope.changeStatusAvailable(param.data);
        }
        if (param.type == 'ban') {
            $scope.changeStatusCancelled(param.data);
        }

    }


    $scope.update = function(param, status) {

        if ($scope.stockGenerationn != undefined && $scope.stockGenerationn != null && $scope.stockGenerationn.id != undefined) {

            var newScope = $scope.$new();
            newScope.status = status == 'Cancelled' ? 'Are you sure you want to cancel MAWB' : 'Are you sure you want to change the status to Available ';

            ngDialog.openConfirm({

                template: '<p>{{status}}<p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">Cancel</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Proceed</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                if (value == 1) {
                    stockUpdate.update($scope.stockGenerationn).$promise
                        .then(
                            function(data) {
                                if (data.responseCode == 'ERR0') {
                                    console
                                        .log("Stock status updated Successfully")
                                    if (status == 'Cancelled') {
                                        param.stockStatus = 'Cancelled';
                                        if ($scope.searchDays) {
                                            $scope.getDetailsByDate();
                                            $scope.searchbyData = false;
                                        }
                                        if ($scope.searchbyData) {
                                            $scope.stockData = false;
                                            $scope.stockByData();
                                            $scope.searchDays = false;

                                        }
                                        return true;
                                    }
                                    if (status == 'Available') {
                                        param.stockStatus = 'Available';
                                        if ($scope.searchDays) {
                                            $scope.getDetailsByDate();
                                            $scope.searchbyData = false;
                                        }
                                        if ($scope.searchbyData) {
                                            $scope.stockByData();
                                            $scope.searchDays = false;
                                        }
                                        return true;
                                    }

                                } else {
                                    console
                                        .log("stock updated Failed " +
                                            data.responseDescription)
                                }

                            },
                            function(error) {
                                console
                                    .log("stock updated Failed : " +
                                        error)
                            });

                } else if (value == 2) {
                    console.log("Status Change Cancelled");
                }
            });



        } else {
            console.log("Stock is Undefined");
        }
    }




    $scope.actionChange = function(param) {
        if (param.type == 'refresh') {
            $scope.changeStatusAvailable(param.data);
        }
        if (param.type == 'ban') {
            $scope.changeStatusCancelled(param.data);
        }

    }



    $scope.validateTotalStocks = function(validateCode) {
        $scope.errorMap = new Map();
        if (validateCode == 0 || validateCode == 1) {
            if ($scope.stockGeneration.carrier == undefined || $scope.stockGeneration.carrier == null || $scope.stockGeneration.carrier.id == undefined) {
                $scope.errorMap.put("stockCarrier", $rootScope.nls["ERR100001"]);
                return false;
            } else {
                if (ValidateUtil.isStatusBlocked($scope.stockGeneration.carrier.status)) {
                    $scope.stockGeneration.carrier = {};
                    $scope.errorMap.put("stockCarrier", $rootScope.nls["ERR100002"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.stockGeneration.carrier.status)) {
                    $scope.stockGeneration.carrier = {};
                    $scope.errorMap.put("stockCarrier", $rootScope.nls["ERR100003"]);
                    return false;
                }
                if ($scope.stockGeneration.carrier.carrierNo == undefined || $scope.stockGeneration.carrier.carrierNo == null || $scope.stockGeneration.carrier.carrierNo == "") {
                    $scope.stockGeneration.carrier = {};
                    $scope.errorMap.put("stockCarrier", $rootScope.nls["ERR1219"]);
                    return false;
                }

            }

        }

        //POr

        if (validateCode == 0 || validateCode == 2) {


            if ($scope.stockGeneration.receivedOn == undefined || $scope.stockGeneration.receivedOn == null) {
                $scope.errorMap.put("receivedOn", $rootScope.nls["ERR100004"]);
                return false;
            }
        }


        if (validateCode == 0 || validateCode == 3) {

            if ($scope.stockGeneration.por == undefined || $scope.stockGeneration.por == null || $scope.stockGeneration.por.id == undefined) {
                $scope.errorMap.put("stockPor", $rootScope.nls["ERR100005"]);
                return false;

            } else {

                if (ValidateUtil.isStatusBlocked($scope.stockGeneration.por.status)) {
                    $scope.stockGeneration.por = {};
                    $scope.errorMap.put("stockPor", $rootScope.nls["ERR100006"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.stockGeneration.por.status)) {
                    $scope.stockGeneration.por = {};
                    $scope.errorMap.put("stockPor", $rootScope.nls["ERR100007"]);
                    return false;
                }

            }

        }


        if (validateCode == 0 || validateCode == 4) {


            if ($scope.stockGeneration.startingNo == undefined || $scope.stockGeneration.startingNo == null) {
                $scope.errorMap.put("startingNo", $rootScope.nls["ERR100010"]);
                return false;
            }
            if (parseInt($scope.stockGeneration.startingNo) == 0) {
                $scope.errorMap.put("startingNo", $rootScope.nls["ERR100010"]);
                return false;
            }
            var regexp = new RegExp("^[0-9]{0,7}$");
            if (!regexp
                .test(parseInt($scope.stockGeneration.startingNo))) {
                $scope.errorMap.put("startingNo", $rootScope.nls["ERR100014"]);
                return false;


            }
            if (parseInt($scope.stockGeneration.remindNo) != undefined && parseInt($scope.stockGeneration.remindNo) != null && parseInt($scope.stockGeneration.remindNo) != "") {
                if (parseInt($scope.stockGeneration.remindNo) < parseInt($scope.stockGeneration.startingNo) || parseInt($scope.stockGeneration.remindNo) > parseInt($scope.stockGeneration.endingNo)) {
                    $scope.errorMap.put("remindNo", $rootScope.nls["ERR100013"]);
                    return false;
                }
            }
            if (parseInt($scope.stockGeneration.endingNo) != null && parseInt($scope.stockGeneration.endingNo) != 0) {
                if (parseInt($scope.stockGeneration.endingNo) < parseInt($scope.stockGeneration.startingNo)) {
                    $scope.errorMap.put("endingNo", $rootScope.nls["ERR100009"]);
                    return false;
                }
            }

        }

        if (validateCode == 0 || validateCode == 5) {
            if ($scope.stockGeneration.endingNo == undefined || $scope.stockGeneration.endingNo == null) {
                $scope.errorMap.put("endingNo", $rootScope.nls["ERR100008"]);
                return false;
            }
            if (parseInt($scope.stockGeneration.endingNo) == 0) {
                $scope.errorMap.put("endingNo", $rootScope.nls["ERR100008"]);
                return false;
            }
            var regexp = new RegExp("^[0-9]{0,7}$");
            if (!regexp
                .test(parseInt($scope.stockGeneration.endingNo))) {
                $scope.errorMap.put("endingNo", $rootScope.nls["ERR100015"]);
                return false;

            }
            if ($scope.stockGeneration.remindNo != undefined && $scope.stockGeneration.remindNo != null && $scope.stockGeneration.remindNo != "") {
                if (parseInt($scope.stockGeneration.remindNo) < parseInt($scope.stockGeneration.startingNo) || parseInt($scope.stockGeneration.remindNo) > parseInt($scope.stockGeneration.endingNo)) {
                    $scope.errorMap.put("remindNo", $rootScope.nls["ERR100013"]);
                    return false;
                }
            }
            if (parseInt($scope.stockGeneration.startingNo) != null && parseInt($scope.stockGeneration.startingNo) != 0) {
                if (parseInt($scope.stockGeneration.startingNo) > parseInt($scope.stockGeneration.endingNo)) {
                    $scope.errorMap.put("endingNo", $rootScope.nls["ERR100009"]);
                    return false;
                }
            }

        }
        if (validateCode == 0 || validateCode == 6) {

            if ($scope.stockGeneration.remindNo != undefined && $scope.stockGeneration.remindNo != null && $scope.stockGeneration.remindNo != "") {

                if (parseInt($scope.stockGeneration.remindNo) == 0) {
                    $scope.errorMap.put("remindNo", $rootScope.nls["ERR100012"]);
                    return false;
                }

                var regexp = new RegExp("^[0-9]{0,7}$");
                if (!regexp
                    .test(parseInt($scope.stockGeneration.remindNo))) {
                    $scope.errorMap.put("remindNo", $rootScope.nls["ERR100012"]);
                    return false;

                }
                if (parseInt($scope.stockGeneration.startingNo) != null && $scope.stockGeneration.endingNo != null) {
                    if (parseInt($scope.stockGeneration.remindNo) != undefined && parseInt($scope.stockGeneration.remindNo) != null && parseInt($scope.stockGeneration.remindNo) != "") {
                        if (parseInt($scope.stockGeneration.remindNo) < parseInt($scope.stockGeneration.startingNo) || parseInt($scope.stockGeneration.remindNo) > parseInt($scope.stockGeneration.endingNo)) {
                            $scope.errorMap.put("remindNo", $rootScope.nls["ERR100013"]);
                            return false;
                        }
                    }
                }


            }

        }

        if (validateCode == 0 || validateCode == 7) {
            if ($scope.stockGeneration.carrierRefNo != undefined && $scope.stockGeneration.carrierRefNo != null && $scope.stockGeneration.carrierRefNo != "") {
                if ($scope.stockGeneration.carrierRefNo == 0) {
                    $scope.errorMap.put("carrierRefNo", $rootScope.nls["ERR100020"]);
                    return false;
                }
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_STOCK_CARRIER_REF_NO, $scope.stockGeneration.carrierRefNo)) {
                    $scope.errorMap.put("carrierRefNo", $rootScope.nls["ERR100020"]);
                    return false;
                }
            } else {
                $scope.stockGeneration.carrierRefNo = null;
            }
        }
        return true;
    }
    $scope.validateDays = function() {
        $scope.errorMap = new Map();
        if (parseInt($scope.stockGenerationSearchDto.noOfDays) == 0) {
            $scope.errorMap.put("noOfDays", $rootScope.nls["ERR100021"]);
            return false;
        }
        if (parseInt($scope.stockGenerationSearchDto.noOfDays) > 9999999) {
            $scope.errorMap.put("noOfDays", $rootScope.nls["ERR100021"]);
            return false;
        }
        if (parseInt($scope.stockGenerationSearchDto.noOfDays) != undefined && $scope.stockGenerationSearchDto.noOfDays != "" && $scope.stockGenerationSearchDto.noOfDays != null) {
            var regexp = new RegExp("^[0-9]{0,7}$");
            if (!regexp
                .test(parseInt($scope.stockGenerationSearchDto.noOfDays))) {
                $scope.errorMap.put("noOfDays", $rootScope.nls["ERR100021"]);
                return false;
            }
        }
        return true;
    }

    //Stock Generation data ends

    $scope.tabChange = function(tabName) {
        if (tabName == 'stockGeneration' && $rootScope.roleAccess(roleConstant.AIR_MAWB_STOCK_GENERATION_LIST)) {
            $scope.detailTab = 'stockGeneration';
            $scope.stockGeneration = {};
            $scope.stockGenerationSearchDto = {};
            $scope.stockGenerationSearchDto.noOfDays = 60;
            $scope.getDetailsByDate();
        }
        if (tabName == 'stockEnquiry' && $rootScope.roleAccess(roleConstant.AIR_MAWB_STOCK_GENERATION_MAWB_STOCK_ENQUIRY_LIST)) {
            $scope.detailTab = 'stockEnquiry';
            $scope.stockGeneration = {};
            $scope.stockGenerationSearchDto = {};
            $scope.stockEnquirySearchDto = {};
            $scope.stockEnquirySearch();

        }

    }

    //Stock Enquiry Starts

    $scope.stockEnquiryHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "Carrier No",
            "search": true,
            "model": "carrierNo",
            "wrap_cell": true,
            "type": "text",
            "width": "w40px",
            "prefWidth": "40",
            "key": "carrierNo"

        },
        {
            "name": "Carrier",
            "search": true,
            "model": "carrierName",
            "wrap_cell": true,
            "type": "text",
            "width": "w150px",
            "prefWidth": "150",
            "key": "carrierName"
        },
        {
            "name": "Origin",
            "model": "portName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "width": "w150px",
            "prefWidth": "150",
            "key": "portName"
        },
        {
            "name": "Stocks in Hand",
            "model": "stockInHand",
            "wrap_cell": true,
            "type": "number",
            "width": "w100px",
            "prefWidth": "100",
            "key": "stockInHand"
        },
        {
            "name": "Last used <br> MAWB No",
            "model": "lastUsedMawb",
            "wrap_cell": true,
            "type": "text",
            "width": "w100px",
            "prefWidth": "100",
            "key": "lastUsedMawb"

        },
        {
            "name": "Last used  <br> MAWB Date",
            "model": "lastUsedMawbDate",
            "wrap_cell": true,
            "type": "date-range",
            "width": "w100px",
            "prefWidth": "100",
            "key": "lastUsedMawbDate"

        },
        {
            "name": "Master Id",
            "model": "shipment",
            "wrap_cell": true,
            "type": "link",
            "width": "w100px",
            "prefWidth": "100",
            "key": "shipment"
        },
        {
            "name": "Next MAWB No",
            "model": "nextMawb",
            "wrap_cell": true,
            "type": "text",
            "width": "w100px",
            "prefWidth": "100",
            "key": "nextMawb"
        },
        {
            "name": "Warning Level",
            "model": "warning",
            "wrap_cell": true,
            "type": "text",
            "width": "w100px",
            "prefWidth": "100",
            "key": "warning"
        }
    ];

    $scope.stockEnquirySortSelection = {
        sortKey: "carrier.carrierName",
        sortOrder: "asc"
    };

    $scope.limitChangeStockEnquiry = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.stockEnquirySearch();
    };

    $scope.changePage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.stockEnquirySearch();
    };

    $scope.stockEnquirySearch = function() {
        $scope.stockGeneration = {};
        $scope.stockEnquirySearchDto.selectedPageNumber = $scope.page;
        $scope.stockEnquirySearchDto.recordPerPage = $scope.limit;
        $scope.stockEnquiryArrList = [];
        StockEnquirySearch.query($scope.stockEnquirySearchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.stockEnquiryArrList = resultArr;
        });
    }

    $scope.carrierRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }

    }
    $scope.portRender = function(item) {
        return {
            label: item.portName,
            item: item
        }

    }

    $scope.navigateToNextField = function(id) {
        console.log("navigateToNextField is called.", id);
        document.getElementById(id).focus();

    }

    switch ($stateParams.action) {
        case "SEARCH":
            console.log("Pack Master Search Page....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airStock"
                },
                {
                    label: "MAWB Stock Generation",
                    state: "layout.airStock"
                }
            ];
            break;
    }

}]);