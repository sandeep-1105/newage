/**
 * 
 */
app.factory("StockGenerationSearch", ['$resource', function($resource) {
    return $resource("/api/v1/stock/search", {}, {
        query: {
            method: 'POST'
        }
    });
}]);

app.factory("StockGenerate", ['$resource', function($resource) {
    return $resource("/api/v1/stock/generate", {}, {
        generate: {
            method: 'POST'
        }
    });
}]);

app.factory("stockUpdate", ['$resource', function($resource) {
    return $resource("/api/v1/stock/update", {}, {
        update: {
            method: 'POST'
        }
    });
}]);

app.factory("GetNextMawb", ['$resource', function($resource) {
    return $resource("/api/v1/stock/getMawb/:carrierId/:portId", {}, {
        fetch: {
            method: 'GET',
            params: {
                carrierId: '',
                portId: ''
            },
            isArray: false
        }
    });
}]);

app.factory("StockEnquirySearch", ['$resource', function($resource) {
    return $resource("/api/v1/stock/enquirysearch", {}, {
        query: {
            method: 'POST'
        }
    });
}]);