/**
 * 
 */

$scope.aesFileMoreInfoValidator = function(validateCode) {
						$scope.errorMap = new Map();

						if (validateCode == 0 || validateCode == 1) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.serviceUid == undefined
									|| $scope.aesFile.serviceUid == null
									|| $scope.aesFile.serviceUid == "") {
								$scope.errorMap.put('aes.serviceUid',
										$rootScope.nls["ERR201015"]);
								return false;
							}
						}
						if (validateCode == 0 || validateCode == 2) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.masterUid == undefined
									|| $scope.aesFile.masterUid == null
									|| $scope.aesFile.masterUid == "") {
								$scope.errorMap.put("aes.masterUid",
										$rootScope.nls["ERR201017"]);
								return false;
							}

						}
						if (validateCode == 0 || validateCode == 3) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.hawbNo == undefined
									|| $scope.aesFile.hawbNo == null
									|| $scope.aesFile.hawbNo == "") {
								$scope.errorMap.put("aes.hawbNo",
										$rootScope.nls["ERR201041"]);
								return false;
							}

						}

						if (validateCode == 0 || validateCode == 4) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.mawbNo == undefined
									|| $scope.aesFile.mawbNo == null
									|| $scope.aesFile.mawbNo == "") {
								$scope.errorMap.put("aes.mawbNo",
										$rootScope.nls["ERR201042"]);
								return false;
							}

						}
						if (validateCode == 0 || validateCode == 5) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.aesAction == undefined
									|| $scope.aesFile.aesAction == null
									|| $scope.aesFile.aesAction == "") {
								$scope.errorMap.put("aes.aesaction",
										$rootScope.nls["ERR201043"]);
								return false;
							}

						}

						/*
						 * if(validateCode==0 || validateCode==6){
						 * if($scope.aesFile!=undefined &&
						 * $scope.aesFile.aesFilingType==undefined ||
						 * $scope.aesFile.aesFilingType==null ||
						 * $scope.aesFile.aesFilingType=="" ){
						 * $scope.errorMap.put("aes.fillingType",
						 * $rootScope.nls["ERR201019"]); return false; }
						 *  }
						 */
						if (validateCode == 0 || validateCode == 7) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.aesTransportMode == undefined
									|| $scope.aesFile.aesTransportMode == null
									|| $scope.aesFile.aesTransportMode == "") {
								$scope.errorMap.put("aes.transportmode",
										$rootScope.nls["ERR201020"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.aesTransportMode != undefined
										&& $scope.aesFile.aesTransportMode) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.aesTransportMode.status)) {
										$scope.aesFile.aesTransportMode = null;
										$scope.errorMap.put(
												"aes.transportmode",
												$rootScope.nls["ERR201039"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.aesTransportMode.status)) {
										$scope.aesFile.aesTransportMode = null;
										$scope.errorMap.put(
												"aes.transportmode",
												$rootScope.nls["ERR201040"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 8) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.originState == undefined
									|| $scope.aesFile.originState == null
									|| $scope.aesFile.originState == "") {
								$scope.errorMap.put("aes.originstate",
										$rootScope.nls["ERR201024"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.originState != undefined
										&& $scope.aesFile.originState) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.originState.status)) {
										$scope.aesFile.originState = null;
										$scope.errorMap.put("aes.originstate",
												$rootScope.nls["ERR201025"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.originState.status)) {
										$scope.aesFile.originState = null;
										$scope.errorMap.put("aes.originstate",
												$rootScope.nls["ERR201026"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 9) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.pol == undefined
									|| $scope.aesFile.pol == null
									|| $scope.aesFile.pol == "") {
								$scope.errorMap.put("aes.pol",
										$rootScope.nls["ERR201036"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.pol != undefined
										&& $scope.aesFile.pol) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.pol.status)) {
										$scope.aesFile.pol = null;
										$scope.errorMap.put("aes.pol",
												$rootScope.nls["ERR201037"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.pol.status)) {
										$scope.aesFile.pol = null;
										$scope.errorMap.put("aes.pol",
												$rootScope.nls["ERR201038"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 10) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.pod == undefined
									|| $scope.aesFile.pod == null
									|| $scope.aesFile.pod == "") {
								$scope.errorMap.put("aes.pod",
										$rootScope.nls["ERR201033"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.pod != undefined
										&& $scope.aesFile.pod) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.pod.status)) {
										$scope.aesFile.pod = null;
										$scope.errorMap.put("aes.pod",
												$rootScope.nls["ERR201034"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.pod.status)) {
										$scope.aesFile.pod = null;
										$scope.errorMap.put("aes.pod",
												$rootScope.nls["ERR201035"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 11) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.destinationCountry == undefined
									|| $scope.aesFile.destinationCountry == null
									|| $scope.aesFile.destinationCountry == "") {
								$scope.errorMap.put("aes.destination",
										$rootScope.nls["ERR201027"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.destinationCountry != undefined
										&& $scope.aesFile.destinationCountry) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.destinationCountry.status)) {
										$scope.aesFile.destinationCountry = null;
										$scope.errorMap.put("aes.destination",
												$rootScope.nls["ERR201028"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.destinationCountry.status)) {
										$scope.aesFile.destinationCountry = null;
										$scope.errorMap.put("aes.destination",
												$rootScope.nls["ERR201029"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 13) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.iata == undefined
									|| $scope.aesFile.iata == null
									|| $scope.aesFile.iata == "") {
								$scope.errorMap.put("aes.iata",
										$rootScope.nls["ERR201046"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.iata != undefined
										&& $scope.aesFile.iata) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.iata.status)) {
										$scope.aesFile.iata = null;
										$scope.errorMap.put("aes.iata",
												$rootScope.nls["ERR201047"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.iata.status)) {
										$scope.aesFile.iata = null;
										$scope.errorMap.put("aes.iata",
												$rootScope.nls["ERR201048"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 12) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.etd == undefined
									|| $scope.aesFile.etd == null
									|| $scope.aesFile.etd == "") {
								$scope.errorMap.put("aes.etd",
										$rootScope.nls["ERR201045"]);
								$scope.Tabs = 'moreInfo';
								return false;
							}

						}

						if (validateCode == 0 || validateCode == 14) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.carrier == undefined
									|| $scope.aesFile.carrier == null
									|| $scope.aesFile.carrier == "") {
								$scope.errorMap.put("aes.carrier",
										$rootScope.nls["ERR201030"]);
								$scope.Tabs = 'moreInfo';
								return false;
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.carrier != undefined
										&& $scope.aesFile.carrier) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.carrier.status)) {
										$scope.aesFile.carrier = null;
										$scope.errorMap.put("aes.carrier",
												$rootScope.nls["ERR201031"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.carrier.status)) {
										$scope.aesFile.carrier = null;
										$scope.errorMap.put("aes.carrier",
												$rootScope.nls["ERR201032"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 15) {
							if ($scope.aesFile.routedTransaction == undefined
									|| $scope.aesFile.routedTransaction == null) {
								$scope.errorMap.put("aes.routedTransaction",
										$rootScope.nls["ERR201021"]);
								$scope.Tabs = 'moreInfo';
								return false;
							}

						}

						if (validateCode == 0 || validateCode == 16) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.usspiUltimateConsignee == undefined
									|| $scope.aesFile.usspiUltimateConsignee == null
									|| $scope.aesFile.usspiUltimateConsignee === "") {
								$scope.errorMap.put("aes.consigneerelated",
										$rootScope.nls["ERR201022"]);
								$scope.Tabs = 'moreInfo';
								return false;
							}

						}
						if (validateCode == 0 || validateCode == 17) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.hazardousCargo == undefined
									|| $scope.aesFile.hazardousCargo == null) {
								$scope.errorMap.put("aes.hazardousCargo",
										$rootScope.nls["ERR201049"]);
								$scope.Tabs = 'moreInfo';
								return false;
							}

						}

						if (validateCode == 0 || validateCode == 18) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.inbondType == undefined
									|| $scope.aesFile.inbondType == null
									|| $scope.aesFile.inbondType == "") {

							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.inbondType != undefined
										&& $scope.aesFile.inbondType) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.inbondType.status)) {
										$scope.aesFile.inbondType = null;
										$scope.errorMap.put("aes.inbondtype",
												$rootScope.nls["ERR201051"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.inbondType.status)) {
										$scope.aesFile.inbondType = null;
										$scope.errorMap.put("aes.inbondtype",
												$rootScope.nls["ERR201052"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}

						if (validateCode == 0 || validateCode == 19) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.importEntryNo == undefined
									|| $scope.aesFile.importEntryNo == null
									|| $scope.aesFile.importEntryNo == "") {
								/*
								 * $scope.errorMap.put("aes.importEntryNo",
								 * $rootScope.nls["ERR201049"]);
								 * $scope.Tabs='moreInfo';
								 */
								return false;
							} else {

								var regexp = new RegExp("^[0-9a-zA-Z ]{0,15}$");
								if (!regexp.test($scope.aesFile.importEntryNo)) {
									$scope.errorMap.put("aes.importEntryNo",
											$rootScope.nls[ERR201053]);
									$scope.Tabs = 'moreInfo';
									return false;
								}
							}
						}
						if (validateCode == 0 || validateCode == 20) {
							if ($scope.aesFile != undefined
									&& $scope.aesFile.foreignTradeZone == undefined
									|| $scope.aesFile.foreignTradeZone == null
									|| $scope.aesFile.foreignTradeZone == "") {

							} else {

								var regexp = new RegExp(
										"^[ 0-9a-zA-Z-'.,&@:;?!()$#*_ ]{0,7}$");
								if (!regexp
										.test($scope.aesFile.foreignTradeZone)) {
									$scope.errorMap.put("aes.foreignTradeZone",
											$rootScope.nls[ERR201053]);
									$scope.Tabs = 'moreInfo';
									return false;
								}
							}
						}
						if (validateCode == 0 || validateCode == 21) {

							if ($scope.aesFile != undefined
									&& $scope.aesFile.iata == undefined
									|| $scope.aesFile.iata == null
									|| $scope.aesFile.iata == "") {
								$scope.errorMap.put("aes.iata",
										$rootScope.nls["ERR201046"]);
								$scope.Tabs = 'moreInfo';
							} else {
								if ($scope.aesFile != undefined
										&& $scope.aesFile.carrier != undefined
										&& $scope.aesFile.carrier) {

									if (ValidateUtil
											.isStatusBlocked($scope.aesFile.iata.status)) {
										$scope.aesFile.carrier = null;
										$scope.errorMap.put("aes.iata",
												$rootScope.nls["ERR201047"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
									if (ValidateUtil
											.isStatusHidden($scope.aesFile.iata.status)) {
										$scope.aesFile.carrier = null;
										$scope.errorMap.put("aes.iata",
												$rootScope.nls["ERR201048"]);
										$scope.Tabs = 'moreInfo';
										return false;
									}
								}
							}
						}
						return true;

					}// validation ends
//validation

$scope.aesFileUsspiValidator=function(validateCode){
	
	 $scope.errorMap = new Map();

	  if (validateCode == 0 || validateCode == 1) {
          if ($scope.aesFile.aesUsppi!= undefined && $scope.aesFile.aesUsppi.shipper == undefined ||
              $scope.aesFile.aesUsppi.shipper == null ||
              $scope.aesFile.aesUsppi.shipper == "" || $scope.aesFile.aesUsppi.shipper.id == undefined ) {
              $scope.errorMap.put("aesShipper",$rootScope.nls["ERR201055"]);
              $scope.Tabs='usppi';
              return false;
          } else {
          	if ($scope.aesFile.aesUsppi.shipper.isDefaulter) {
                  $scope.errorMap.put("aesShipper",$rootScope.nls["ERR90108"]);
                  $scope.aesFile.aesUsppi.shipper = {};
                  $scope.Tabs='usppi';
                  return false
              }
              if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.status)) {
                  $scope.errorMap.put("aesShipper",$rootScope.nls["ERR201058"]);
                  $scope.aesFile.aesUsppi.shipper = {};
                  $scope.Tabs='usppi';
                  return false
              }
              if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.status)) {
          	    $scope.errorMap.put("aesShipper",$rootScope.nls["ERR201057"]);
                $scope.aesFile.aesUsppi.shipper = {};
                $scope.Tabs='usppi';
                $scope.shipmentTab = 'documents';
                return false

            }
          }
      }
	  
	  //address for origin
	  if (validateCode == 0 || validateCode == 2) {
         
		  if ($scope.aesFile.aesUsppi!= undefined && $scope.aesFile.aesUsppi.cargoOrigin == undefined ||
	              $scope.aesFile.aesUsppi.cargoOrigin == null ||
	              $scope.aesFile.aesUsppi.cargoOrigin == "" || $scope.aesFile.aesUsppi.cargoOrigin.id == undefined ) {
	              $scope.errorMap.put("aes.usspi.cargo",$rootScope.nls["ERR201055"]);
	              $scope.Tabs='usppi';
	              return false;
	          } else {
	          	if ($scope.aesFile.aesUsppi.cargoOrigin.isDefaulter) {
	                  $scope.errorMap.put("aes.usspi.cargo",$rootScope.nls["ERR90108"]);
	                  $scope.aesFile.aesUsppi.cargoOrigin = {};
	                  $scope.aesFile.aesUsppi.cargoOriginAddress = {};
	                  $scope.Tabs='usppi';
	                  return false
	              }
	              if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.cargoOrigin.status)) {
	                  $scope.errorMap.put("aes.usspi.cargo",$rootScope.nls["ERR201058"]);
	                  $scope.aesFile.aesUsppi.cargoOrigin = {};
	                  $scope.aesFile.aesUsppi.cargoOriginAddress = {};
	                  $scope.Tabs='usppi';
	                  return false
	              }
	              if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.cargoOrigin.status)) {
	          	    $scope.errorMap.put("aes.usspi.cargo",$rootScope.nls["ERR201057"]);
	                $scope.aesFile.aesUsppi.cargoOrigin = {};
	                $scope.aesFile.aesUsppi.cargoOriginAddress = {};
	                $scope.Tabs='usppi';
	                return false
	            }
	          }
      }
	  
	  
	  
	 if (validateCode == 0 || validateCode == 3) {
		  
  		if($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine1  == null
  				|| $scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine1  == ""
  				|| $scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine1 == undefined) {
  			$scope.errorMap.put("aesFile.aesUsppi.addressLine1",$rootScope.nls["ERR38005"]);
  			$scope.Tabs='usppi';
  			return false;
  		}
  		else {
  			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine1)) {
					$scope.errorMap.put("aesFile.aesUsppi.addressLine1",$rootScope.nls["ERR38006"]);
					$scope.Tabs='usppi';
					return false;
  			}
  	    }
  		if($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine2  == null
  				|| $scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine2  ==""
  				|| $scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine2 ==undefined) {
  			//$scope.errorMap.put("aesFile.aesUsppi.addressLine2",$rootScope.nls["ERR38007"]);
  			//$scope.Tabs='usppi';
  			//return false;
  		} else {
  			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
  			if (!regexp.test($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine2)) {
  				$scope.errorMap.put("aesFile.aesUsppi.addressLine2",$rootScope.nls["ERR38008"]);
  				$scope.Tabs='usppi';
  				return false;
  			}
  		}


  		if($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine3!=null) {
				var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine3)) {
					console.log($rootScope.nls["ERR38009"]);
					$scope.errorMap.put("aesFile.aesUsppi.addressLine3",$rootScope.nls["ERR38009"]);
					$scope.Tabs='usppi';
					return false;
				}
  		}

  		if($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine4  == null
  				|| $scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine4  ==""
  				|| $scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine4 ==undefined) {
  			console.log($rootScope.nls["ERR38010"]);
  			//$scope.Tabs='usppi';
  			//return false;
  		}
  		else {
				var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesUsppi.cargoOriginAddress.partyAddress.addressLine4)) {
					console.log($rootScope.nls["ERR38011"]);
					$scope.errorMap.put("aesFile.aesUsppi.addressLine4",$rootScope.nls["ERR38011"]);
					$scope.Tabs='usppi';
					return false;
				}
  		}
	  
	  
}	  
	  
	  if(validateCode==0 || validateCode==4){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.stateMaster==undefined || $scope.aesFile.aesUsppi.stateMaster==null ||$scope. aesFile.aesUsppi.stateMaster=="" ){
			
			   $scope.errorMap.put("aes.aesusppi.state", $rootScope.nls["ERR201062"]);
			   $scope.Tabs='usppi';
			   return false
		   }
			else{
				if($scope.aesFile!=undefined &&  $scope.aesFile.aesUsppi!=undefined &&$scope.aesFile.aesUsppi.stateMaster!=undefined &&$scope.aesFile.aesUsppi.stateMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.stateMaster.status)) {
		               $scope.aesFile.aesUsppi.stateMaster = null;
		               $scope.errorMap.put("aes.aesusppi.state", $rootScope.nls["ERR201135"]);
		               $scope.Tabs='usppi';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.stateMaster.status)) {
		               $scope.aesFile.aesUsppi.stateMaster = null;
		               $scope.errorMap.put("aes.aesusppi.state", $rootScope.nls["ERR201136"]);
		               $scope.Tabs='usppi';
		               return false;
		           }
				}
			}
		}
	  
	  if(validateCode==0 || validateCode==5){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.cityMaster==undefined || $scope.aesFile.aesUsppi.cityMaster==null ||$scope. aesFile.aesUsppi.cityMaster=="" ){
			   $scope.errorMap.put("aes.aesusppi.city", $rootScope.nls["ERR201061"]);
			   $scope.Tabs='usppi';
		   return false
		   }
			else{
				if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined &&$scope.aesFile.aesUsppi.cityMaster!=undefined &&$scope.aesFile.aesUsppi.cityMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.cityMaster.status)) {
		               $scope.aesFile.aesUsppi.cityMaster = null;
		               $scope.errorMap.put("aes.aesusppi.city", $rootScope.nls["ERR201137"]);
		               $scope.Tabs='usppi';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.cityMaster.status)) {
		               $scope.aesFile.aesUsppi.cityMaster = null;
		               $scope.errorMap.put("aes.aesusppi.city", $rootScope.nls["ERR201138"]);
		               $scope.Tabs='usppi';
		               return false;
		           }
				}
			}
		}
	  
	  if(validateCode==0 || validateCode==6){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.idType==undefined || $scope.aesFile.aesUsppi.idType==null ||$scope. aesFile.aesUsppi.idType=="" ){
			   $scope.errorMap.put("aes.aesusppi.idType", $rootScope.nls["ERR201064"]);
			   $scope.Tabs='usppi';
		   return false
		   }
	  }
	  
	  if(validateCode==0 || validateCode==7){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.idNo!=undefined && $scope.aesFile.aesUsppi.idNo!=null && $scope. aesFile.aesUsppi.idNo!="" ){
			  
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,11}");
				if (!regexp.test($scope.aesFile.aesUsppi.idNo)) {
					$scope.errorMap.put("aesFile.aesUsppi.idNo",$rootScope.nls["ERR201126"]);
					$scope.Tabs='usppi';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==8){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.firstName!=undefined && $scope.aesFile.aesUsppi.firstName!=null && $scope. aesFile.aesUsppi.firstName!="" ){
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
				if (!regexp.test($scope.aesFile.aesUsppi.firstName)) {
					$scope.errorMap.put("aesFile.aesUsppi.firstName",$rootScope.nls["ERR201127"]);
					$scope.Tabs='usppi';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==9){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.firstName!=undefined && $scope.aesFile.aesUsppi.firstName!=null && $scope.aesFile.aesUsppi.firstName!="" ){
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
				if (!regexp.test($scope.aesFile.aesUsppi.lastName)) {
					$scope.errorMap.put("aesFile.aesUsppi.lastName",$rootScope.nls["ERR201128"]);
					$scope.Tabs='usppi';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==10){
		
		  if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.contactNo==undefined && $scope.aesFile.aesUsppi.contactNo==null && $scope.aesFile.aesUsppi.contactNo=="" ){
				$scope.errorMap.put("aesFile.aesUsppi.contactNo",$rootScope.nls["ERR201060"]);
				$scope.Tabs='usppi';
				return false;
		  }else{
			  var regexp = new RegExp("[0-9+  ]{0,20}");
				if (!regexp.test($scope.aesFile.aesUsppi.contactNo)) {
					$scope.errorMap.put("aesFile.aesUsppi.contactNo",$rootScope.nls["ERR201142"]);
					$scope.Tabs='usppi';
					return false;
				}
		  }
	  }
	  
	  if(validateCode==0 || validateCode==11){
			
		  if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.zipCode==undefined && $scope.aesFile.aesUsppi.zipCode==null && $scope.aesFile.aesUsppi.zipCode=="" ){
				$scope.errorMap.put("aesFile.aesUsppi.zipCode",$rootScope.nls["ERR201063"]);
				$scope.Tabs='usppi';
				return false;
		  }else{
			  var regexp = new RegExp("[0-9A-Za-z  ]{0,20}");
				if (!regexp.test($scope.aesFile.aesUsppi.zipCode)) {
					$scope.errorMap.put("aesFile.aesUsppi.zipCode",$rootScope.nls["ERR201141"]);
					$scope.Tabs='usppi';
					return false;
				}
		  }
	  }
	  
	  if(validateCode==0 || validateCode==12){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined && $scope.aesFile.aesUsppi.countryMaster==undefined || $scope.aesFile.aesUsppi.countryMaster==null ||$scope. aesFile.aesUsppi.countryMaster=="" ){
			/*   $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR2011997"]);
			   $scope.Tabs='usppi';
		   return false*/
		   }
			else{
				if($scope.aesFile!=undefined && $scope.aesFile.aesUsppi!=undefined &&$scope.aesFile.aesUsppi.countryMaster!=undefined &&$scope.aesFile.aesUsppi.countryMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.countryMaster.status)) {
		               $scope.aesFile.aesUsppi.countryMaster = null;
		               $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR2011998"]);
		               $scope.Tabs='usppi';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.countryMaster.status)) {
		               $scope.aesFile.aesUsppi.countryMaster = null;
		               $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR2011999"]);
		               $scope.Tabs='usppi';
		               return false;
		           }
				}
			}
		}
	  
	  return true;
}//usppi validation ends


$scope.aesUltimateConsigneeValidator=function(validateCode){
	
		 $scope.errorMap = new Map();

		  if (validateCode == 0 || validateCode == 1) {
	          if ($scope.aesFile.aesUltimateConsignee!= undefined && $scope.aesFile.aesUltimateConsignee.ultimateConsignee == undefined ||
	              $scope.aesFile.aesUltimateConsignee.ultimateConsignee == null ||
	              $scope.aesFile.aesUltimateConsignee.ultimateConsignee == "" || $scope.aesFile.aesUltimateConsignee.ultimateConsignee.id == undefined ) {
	        	  $scope.Tabs='consignee';
	              $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee",$rootScope.nls["ERR201110"]);
	              return false;
	          } else {
	          	if ($scope.aesFile.aesUltimateConsignee.ultimateConsignee.isDefaulter) {
	                  $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee",$rootScope.nls["ERR201120"]);
	                  $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
	                  $scope.Tabs='consignee';
	                  return false
	              }
	              if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.status)) {
	                  $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee",$rootScope.nls["ERR201111"]);
	                  $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
	                  $scope.Tabs='consignee';
	                  return false
	              }
	              if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.status)) {
	          	    $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee",$rootScope.nls["ERR201118"]);
	                $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
	                $scope.Tabs='consignee';
	                return false

	            }
	          }
	      }
		  
		  //address for origin
		  if (validateCode == 0 || validateCode == 2) {
	         
			  if ($scope.aesFile.aesUltimateConsignee!= undefined && $scope.aesFile.aesUltimateConsignee.company == undefined ||
		              $scope.aesFile.aesUltimateConsignee.company == null ||
		              $scope.aesFile.aesUltimateConsignee.company == "" || $scope.aesFile.aesUltimateConsignee.company.id == undefined ) {
		              $scope.errorMap.put("aes.usspi.company",$rootScope.nls["ERR201147"]);
		              $scope.Tabs='consignee';
		              return false;
		          } else {
		          	if ($scope.aesFile.aesUltimateConsignee.company.isDefaulter) {
		                  $scope.errorMap.put("aes.usspi.company",$rootScope.nls["ERR201150"]);
		                  $scope.aesFile.aesUltimateConsignee.company = {};
		                  $scope.aesFile.aesUltimateConsignee.companyAddress = {};
		                  $scope.Tabs='consignee';
		                  return false
		              }
		              if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.company.status)) {
		                  $scope.errorMap.put("aes.usspi.company",$rootScope.nls["ERR201148"]);
		                  $scope.aesFile.aesUltimateConsignee.company = {};
		                  $scope.aesFile.aesUltimateConsignee.companyAddress = {};
		                  $scope.Tabs='consignee';
		                  return false
		              }
		              if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.company.status)) {
		          	    $scope.errorMap.put("aes.usspi.company",$rootScope.nls["ERR201149"]);
		                $scope.aesFile.aesUltimateConsignee.company = {};
		                $scope.aesFile.aesUltimateConsignee.companyAddress = {};
		                $scope.Tabs='consignee';
		                return false
		            }
		          }
	      }
		  
		  
		  
		 if (validateCode == 0 || validateCode == 3) {
			  
	  		if($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1  == null
	  				|| $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1  == ""
	  				|| $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1 == undefined) {
	  			$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine1",$rootScope.nls["ERR201151"]);
	  			 $scope.Tabs='consignee';
	  			return false;
	  		}
	  		else {
	  			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1)) {
						$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine1",$rootScope.nls["ERR201152"]);
						 $scope.Tabs='consignee';
						return false;
	  			}
	  	    }
	  		if($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2  == null
	  				|| $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2  ==""
	  				|| $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2 ==undefined) {
	  			//$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine2",$rootScope.nls["ERR201153"]);
	  			// $scope.Tabs='consignee';
	  			//return false;
	  		} else {
	  			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
	  			if (!regexp.test($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2)) {
	  				$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine2",$rootScope.nls["ERR201154"]);
	  				 $scope.Tabs='consignee';
	  				return false;
	  			}
	  		}


	  		if($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine3!=null) {
					var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine3)) {
						console.log($rootScope.nls["ERR38009"]);
						$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine3",$rootScope.nls["ERR201155"]);
						 $scope.Tabs='consignee';
						return false;
					}
	  		}

	  		if($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4  == null
	  				|| $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4  ==""
	  				|| $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4 ==undefined) {
	  			console.log($rootScope.nls["ERR38010"]);
	  			//$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine4",$rootScope.nls["ERR201156"]);
	  			// $scope.Tabs='consignee';
	  			//return false;
	  		}
	  		else {
					var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4)) {
						console.log($rootScope.nls["ERR38011"]);
						$scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine4",$rootScope.nls["ERR201157"]);
						 $scope.Tabs='consignee';
						return false;
					}
	  		}
		  
	}	  
		  
		  if(validateCode==0 || validateCode==4){

			   if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.stateMaster==undefined || $scope.aesFile.aesUltimateConsignee.stateMaster==null ||$scope. aesFile.aesUltimateConsignee.stateMaster=="" ){
				
				 
			   }
				else{
					if($scope.aesFile!=undefined &&  $scope.aesFile.aesUltimateConsignee!=undefined &&$scope.aesFile.aesUltimateConsignee.stateMaster!=undefined &&$scope.aesFile.aesUltimateConsignee.stateMaster!=null){
						
						 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.stateMaster.status)) {
			               $scope.aesFile.aesUltimateConsignee.stateMaster = null;
			               $scope.errorMap.put("aes.aesUltimateConsignee.state", $rootScope.nls["ERR201163"]);
			               $scope.Tabs='consignee';
			               return false;
			           }
			           if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.stateMaster.status)) {
			               $scope.aesFile.aesUltimateConsignee.stateMaster = null;
			               $scope.errorMap.put("aes.aesUltimateConsignee.state", $rootScope.nls["ERR201164"]);
			               $scope.Tabs='consignee';
			               return false;
			           }
					}
				}
			}
		  
		  if(validateCode==0 || validateCode==5){

			   if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.cityMaster==undefined || $scope.aesFile.aesUltimateConsignee.cityMaster==null ||$scope. aesFile.aesUltimateConsignee.cityMaster=="" ){
				   $scope.errorMap.put("aes.aesUltimateConsignee.city", $rootScope.nls["ERR201123"]);
				   $scope.Tabs='consignee';
			   return false
			   }
				else{
					if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined &&$scope.aesFile.aesUltimateConsignee.cityMaster!=undefined &&$scope.aesFile.aesUltimateConsignee.cityMaster!=null){
						
						 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.cityMaster.status)) {
			               $scope.aesFile.aesUltimateConsignee.cityMaster = null;
			               $scope.errorMap.put("aes.aesUltimateConsignee.city", $rootScope.nls["ERR201165"]);
			               $scope.Tabs='consignee';
			               return false;
			           }
			           if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.cityMaster.status)) {
			               $scope.aesFile.aesUltimateConsignee.cityMaster = null;
			               $scope.errorMap.put("aes.aesUltimateConsignee.city", $rootScope.nls["ERR201166"]);
			               $scope.Tabs='consignee';
			               return false;
			               
			           }
					}
				}
			}
		  
		  if(validateCode==0 || validateCode==6){
			  /*if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.idType==undefined || $scope.aesFile.aesUltimateConsignee.idType==null ||$scope. aesFile.aesUltimateConsignee.idType=="" ){
				   $scope.errorMap.put("aes.aesUltimateConsignee.idType", $rootScope.nls["ERR201064"]);
			   return false
			   }*/
		  }
		  
		  if(validateCode==0 || validateCode==7){
			  if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.idNo!=undefined && $scope.aesFile.aesUltimateConsignee.idNo!=null && $scope. aesFile.aesUltimateConsignee.idNo!="" ){
				  
				  var regexp = new RegExp("[ A-Za-z0-9  ]{0,11}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.idNo)) {
						$scope.errorMap.put("aesFile.aesUltimateConsignee.idNo",$rootScope.nls["ERR201126"]);
						 $scope.Tabs='consignee';
						return false;
					}
			   }
		  }
		  
		  if(validateCode==0 || validateCode==8){
			  if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.firstName!=undefined && $scope.aesFile.aesUltimateConsignee.firstName!=null && $scope. aesFile.aesUltimateConsignee.firstName!="" ){
				  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.firstName)) {
						$scope.errorMap.put("aesFile.aesUltimateConsignee.firstName",$rootScope.nls["ERR201127"]);
						 $scope.Tabs='consignee';
						return false;
					}
			   }
		  }
		  
		  if(validateCode==0 || validateCode==9){
			  if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.firstName!=undefined && $scope.aesFile.aesUltimateConsignee.firstName!=null && $scope.aesFile.aesUltimateConsignee.firstName!="" ){
				  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.lastName)) {
						$scope.errorMap.put("aesFile.aesUltimateConsignee.lastName",$rootScope.nls["ERR201128"]);
						 $scope.Tabs='consignee';
						return false;
					}
			   }
		  }
		  
		  if(validateCode==0 || validateCode==10){
			
			  if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.contactNo==undefined && $scope.aesFile.aesUltimateConsignee.contactNo==null && $scope.aesFile.aesUltimateConsignee.contactNo=="" ){
					$scope.errorMap.put("aesFile.aesUltimateConsignee.contactNo",$rootScope.nls["ERR201174"]);
					 $scope.Tabs='consignee';
					return false;
			  }else{
				  var regexp = new RegExp("[0-9+  ]{0,20}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.contactNo)) {
						$scope.errorMap.put("aesFile.aesUltimateConsignee.contactNo",$rootScope.nls["ERR201169"]);
						 $scope.Tabs='consignee';
						return false;
					}
			  }
		  }
		  if(validateCode==0 || validateCode==11){
				
			  if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.zipCode==undefined && $scope.aesFile.aesUltimateConsignee.zipCode==null && $scope.aesFile.aesUltimateConsignee.zipCode=="" ){
					
				  
			  }else{
				  var regexp = new RegExp("[0-9A-Za-z  ]{0,20}");
					if (!regexp.test($scope.aesFile.aesUltimateConsignee.zipCode)) {
						$scope.errorMap.put("aesFile.aesUltimateConsignee.zipCode",$rootScope.nls["ERR201170"]);
						 $scope.Tabs='consignee';
						return false;
					}
			  }
		  }
		 
		  if(validateCode==0 || validateCode==12){

			   if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.countryMaster==undefined || $scope.aesFile.aesUltimateConsignee.countryMaster==null ||$scope. aesFile.aesUltimateConsignee.countryMaster=="" ){
				   $scope.errorMap.put("aes.aesUltimateConsignee.country", $rootScope.nls["ERR201124"]);
				   $scope.Tabs='consignee';
			   return false
			   }
				else{
					if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined &&$scope.aesFile.aesUltimateConsignee.countryMaster!=undefined &&$scope.aesFile.aesUltimateConsignee.countryMaster!=null){
						
						 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.countryMaster.status)) {
			               $scope.aesFile.aesUltimateConsignee.countryMaster = null;
			               $scope.errorMap.put("aes.aesUltimateConsignee.country", $rootScope.nls["ERR201137"]);
			               $scope.Tabs='consignee';
			               return false;
			           }
			           if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.countryMaster.status)) {
			               $scope.aesFile.aesUltimateConsignee.countryMaster = null;
			               $scope.errorMap.put("aes.aesUltimateConsignee.country", $rootScope.nls["ERR201138"]);
			               $scope.Tabs='consignee';
			               return false;
			           }
					}
				}
			}
		  if(validateCode==0 || validateCode==13){
			  if($scope.aesFile!=undefined && $scope.aesFile.aesUltimateConsignee!=undefined && $scope.aesFile.aesUltimateConsignee.consigneeType==undefined || $scope.aesFile.aesUltimateConsignee.consigneeType==null ||$scope. aesFile.aesUltimateConsignee.consigneeType=="" ){
				   $scope.errorMap.put("aes.aesUltimateConsignee.consigneeType", $rootScope.nls["ERR201176"]);
				   $scope.Tabs='consignee';
			   return false
			   }
		  }
		  
		  return true;
	}//ultimate consignee validation ends




//intermediatae consignee

$scope.aesIntermediateConsigneeValidator=function(validateCode){
	
	 $scope.errorMap = new Map();

	  if (validateCode == 0 || validateCode == 1) {
         if ($scope.aesFile.aesIntermediateConsignee!= undefined && $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == undefined ||
             $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == null ||
             $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == "" || $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.id == undefined ) {
            /* $scope.errorMap.put("aes.aesIntermediateConsignee.consignee",$rootScope.nls["ERR201055"]);
             return false;*/
         } else {
         	if ($scope.aesFile.aesIntermediateConsignee.intermediateConsignee.isDefaulter) {
                 $scope.errorMap.put("aesFile.aesIntermediateConsignee.intermediateConsignee",$rootScope.nls["ERR90108"]);
                 $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = {};
                 $scope.Tabs='intermediateConsignee';
                 return false
             }
             if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.status)) {
                 $scope.errorMap.put("aesFile.aesIntermediateConsignee.intermediateConsignee",$rootScope.nls["ERR201118"]);
                 $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = {};
                 $scope.Tabs='intermediateConsignee';
                 return false
             }
             if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.status)) {
         	    $scope.errorMap.put("aesFile.aesIntermediateConsignee.intermediateConsignee",$rootScope.nls["ERR201119"]);
               $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = {};
               $scope.Tabs='intermediateConsignee';
               return false

           }
         }
     }
	  
	  //address for origin
	  if (validateCode == 0 || validateCode == 2) {
        
		  if ($scope.aesFile.aesIntermediateConsignee!= undefined && $scope.aesFile.aesIntermediateConsignee.company == undefined ||
	              $scope.aesFile.aesIntermediateConsignee.company == null ||
	              $scope.aesFile.aesIntermediateConsignee.company == "" || $scope.aesFile.aesIntermediateConsignee.company.id == undefined ) {
	             /* $scope.errorMap.put("aes.usspi.company",$rootScope.nls["ERR201055"]);
	              return false;*/
	          } else {
	          	if ($scope.aesFile.aesIntermediateConsignee.company.isDefaulter) {
	                  $scope.errorMap.put("aesFile.aesIntermediateConsignee.company",$rootScope.nls["ERR201186"]);
	                  $scope.aesFile.aesIntermediateConsignee.company = {};
	                  $scope.aesFile.aesIntermediateConsignee.companyAddress = {};
	                  $scope.Tabs='intermediateConsignee';
	                  return false
	              }
	              if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.company.status)) {
	                  $scope.errorMap.put("aesFile.aesIntermediateConsignee.cargo",$rootScope.nls["ERR201184"]);
	                  $scope.aesFile.aesIntermediateConsignee.company = {};
	                  $scope.aesFile.aesIntermediateConsignee.companyAddress = {};
	                  $scope.Tabs='intermediateConsignee';
	                  return false
	              }
	              if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.company.status)) {
	          	    $scope.errorMap.put("aesFile.aesIntermediateConsignee.cargo",$rootScope.nls["ERR201185"]);
	                $scope.aesFile.aesIntermediateConsignee.company = {};
	                $scope.aesFile.aesIntermediateConsignee.companyAddress = {};
	                $scope.Tabs='intermediateConsignee';
	                return false
	            }
	          }
     }
	  
	  
	  
	 if (validateCode == 0 || validateCode == 3) {
		  
 		if($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1  == null
 				|| $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1  == ""
 				|| $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1 == undefined) {
 			/*$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine1",$rootScope.nls["ERR38005"]);
 			return false;*/
 		}
 		else {
 			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1)) {
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine1",$rootScope.nls["ERR38006"]);
					return false;
 			}
 	    }
 		if($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2  == null
 				|| $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2  ==""
 				|| $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2 ==undefined) {
 			/*$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine2",$rootScope.nls["ERR38007"]);
 			return false;*/
 		} else {
 			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
 			if (!regexp.test($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2)) {
 				$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine2",$rootScope.nls["ERR38008"]);
 				return false;
 			}
 		}


 		if($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine3!=null) {
				var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine3)) {
					console.log($rootScope.nls["ERR38009"]);
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine3",$rootScope.nls["ERR38009"]);
					return false;
				}
 		}

 		if($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4  == null
 				|| $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4  ==""
 				|| $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4 ==undefined) {
 			/*console.log($rootScope.nls["ERR38010"]);
 			$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine4",$rootScope.nls["ERR38010"]);*/
 		}
 		else {
				var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4)) {
					console.log($rootScope.nls["ERR38011"]);
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine4",$rootScope.nls["ERR38011"]);
					return false;
				}
 		}
	  
}	  
	  
	  if(validateCode==0 || validateCode==4){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.stateMaster==undefined || $scope.aesFile.aesIntermediateConsignee.stateMaster==null ||$scope. aesFile.aesIntermediateConsignee.stateMaster=="" ){
			
			  /* $scope.errorMap.put("aes.aesusppi.state", $rootScope.nls["ERR201194"]);
			   return false*/
		   }
			else{
				if($scope.aesFile!=undefined &&  $scope.aesFile.aesIntermediateConsignee!=undefined &&$scope.aesFile.aesIntermediateConsignee.stateMaster!=undefined &&$scope.aesFile.aesIntermediateConsignee.stateMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.stateMaster.status)) {
		               $scope.aesFile.aesIntermediateConsignee.stateMaster = null;
		               $scope.errorMap.put("aes.aesIntermediateConsignee.state", $rootScope.nls["ERR201179"]);
		               $scope.Tabs='intermediateConsignee';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.stateMaster.status)) {
		               $scope.aesFile.aesIntermediateConsignee.stateMaster = null;
		               $scope.errorMap.put("aes.aesIntermediateConsignee.state", $rootScope.nls["ERR201180"]);
		               $scope.Tabs='intermediateConsignee';
		               return false;
		           }
				}
			}
		}
	  
	  if(validateCode==0 || validateCode==5){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.cityMaster==undefined || $scope.aesFile.aesIntermediateConsignee.cityMaster==null ||$scope. aesFile.aesIntermediateConsignee.cityMaster=="" ){
			  /* $scope.errorMap.put("aes.aesusppi.city", $rootScope.nls["ERR201061"]);
		   return false*/
		   }
			else{
				if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined &&$scope.aesFile.aesIntermediateConsignee.cityMaster!=undefined &&$scope.aesFile.aesIntermediateConsignee.cityMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.cityMaster.status)) {
		               $scope.aesFile.aesIntermediateConsignee.cityMaster = null;
		               $scope.errorMap.put("aesFile.aesIntermediateConsignee.city", $rootScope.nls["ERR201181"]);
		               $scope.Tabs='intermediateConsignee';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.cityMaster.status)) {
		               $scope.aesFile.aesIntermediateConsignee.cityMaster = null;
		               $scope.errorMap.put("aesFile.aesIntermediateConsignee.city", $rootScope.nls["ERR201182"]);
		               $scope.Tabs='intermediateConsignee';
		               return false;
		           }
				}
			}
		}
	  
	  if(validateCode==0 || validateCode==6){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.idType==undefined || $scope.aesFile.aesIntermediateConsignee.idType==null ||$scope. aesFile.aesIntermediateConsignee.idType=="" ){
			  /* $scope.errorMap.put("aes.aesusppi.idType", $rootScope.nls["ERR201064"]);
		   return false*/
		   }
	  }
	  
	  if(validateCode==0 || validateCode==7){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.idNo!=undefined && $scope.aesFile.aesIntermediateConsignee.idNo!=null && $scope. aesFile.aesIntermediateConsignee.idNo!="" ){
			  
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,11}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.idNo)) {
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.idNo",$rootScope.nls["ERR201191"]);
					$scope.Tabs='intermediateConsignee';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==8){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.firstName!=undefined && $scope.aesFile.aesIntermediateConsignee.firstName!=null && $scope. aesFile.aesIntermediateConsignee.firstName!="" ){
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.firstName)) {
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.firstName",$rootScope.nls["ERR201187"]);
					$scope.Tabs='intermediateConsignee';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==9){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.firstName!=undefined && $scope.aesFile.aesIntermediateConsignee.firstName!=null && $scope.aesFile.aesIntermediateConsignee.firstName!="" ){
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.lastName)) {
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.lastName",$rootScope.nls["ERR201188"]);
					$scope.Tabs='intermediateConsignee';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==10){
		
		  if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.contactNo==undefined && $scope.aesFile.aesIntermediateConsignee.contactNo==null && $scope.aesFile.aesIntermediateConsignee.contactNo=="" ){
				/*$scope.errorMap.put("aesFile.aesIntermediateConsignee.contactNo",$rootScope.nls["ERR201060"]);
				return false;*/
		  }else{
			  var regexp = new RegExp("[0-9+  ]{0,20}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.contactNo)) {
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.contactNo",$rootScope.nls["ERR201189"]);
					$scope.Tabs='intermediateConsignee';
					return false;
				}
		  }
	  }
	  
	  if(validateCode==0 || validateCode==11){
			
		  if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.zipCode==undefined && $scope.aesFile.aesIntermediateConsignee.zipCode==null && $scope.aesFile.aesIntermediateConsignee.zipCode=="" ){
				/*$scope.errorMap.put("aesFile.aesIntermediateConsignee.zipCode",$rootScope.nls["ERR201063"]);
				return false;*/
		  }else{
			  var regexp = new RegExp("[0-9A-Za-z  ]{0,20}");
				if (!regexp.test($scope.aesFile.aesIntermediateConsignee.zipCode)) {
					$scope.errorMap.put("aesFile.aesIntermediateConsignee.zipcode",$rootScope.nls["ERR201190"]);
					$scope.Tabs='intermediateConsignee';
					return false;
				}
		  }
	  }
	 
	  
	  if(validateCode==0 || validateCode==12){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined && $scope.aesFile.aesIntermediateConsignee.countryMaster==undefined || $scope.aesFile.aesIntermediateConsignee.countryMaster==null ||$scope. aesFile.aesIntermediateConsignee.countryMaster=="" ){
			 /*  $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR201061"]);
			   $scope.Tabs='consignee';
		   return false*/
		   }
			else{
				if($scope.aesFile!=undefined && $scope.aesFile.aesIntermediateConsignee!=undefined &&$scope.aesFile.aesIntermediateConsignee.countryMaster!=undefined &&$scope.aesFile.aesIntermediateConsignee.countryMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.countryMaster.status)) {
		               $scope.aesFile.aesIntermediateConsignee.countryMaster = null;
		               $scope.errorMap.put("aesFile.aesIntermediateConsignee.country", $rootScope.nls["ERR201181"]);
		               $scope.Tabs='intermediateConsignee';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.countryMaster.status)) {
		               $scope.aesFile.aesIntermediateConsignee.countryMaster = null;
		               $scope.errorMap.put("aesFile.aesIntermediateConsignee.country", $rootScope.nls["ERR201182"]);
		               $scope.Tabs='intermediateConsignee';
		               return false;
		           }
				}
			}
		}
	  
	 
	  return true;
}//intermediate consignee validation ends


//freight forwarder validation

$scope.aesForwarderValidation=function(validateCode){
	
	 $scope.errorMap = new Map();

	  if (validateCode == 0 || validateCode == 1) {
        if ($scope.aesFile.aesFreightForwarder!= undefined && $scope.aesFile.aesFreightForwarder.forwarder == undefined ||
            $scope.aesFile.aesFreightForwarder.forwarder == null ||
            $scope.aesFile.aesFreightForwarder.forwarder == "" || $scope.aesFile.aesFreightForwarder.forwarder.id == undefined ) {
            $scope.errorMap.put("aesFile.aesForwarder.forwarder",$rootScope.nls["ERR201106"]);
            $scope.Tabs='forwarder';
            return false;
        } else {
        	if ($scope.aesFile.aesFreightForwarder.forwarder.isDefaulter) {
                $scope.errorMap.put("aesFile.aesForwarder.forwarder",$rootScope.nls["ERR201112"]);
                $scope.aesFile.aesFreightForwarder.forwarder = {};
                $scope.Tabs='forwarder';
                return false
            }
            if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.status)) {
                $scope.errorMap.put("aesFile.aesForwarder.forwarder",$rootScope.nls["ERR201107"]);
                $scope.aesFile.aesFreightForwarder.forwarder = {};
                $scope.Tabs='forwarder';
                return false
            }
            if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.status)) {
        	    $scope.errorMap.put("aesFile.aesForwarder.forwarder",$rootScope.nls["ERR201108"]);
              $scope.aesFile.aesFreightForwarder.forwarder = {};
              $scope.Tabs='forwarder';
              return false

          }
        }
    }
	  
	  //address for origin
	  if (validateCode == 0 || validateCode == 2) {
       
		  if ($scope.aesFile.aesFreightForwarder!= undefined && $scope.aesFile.aesFreightForwarder.company == undefined ||
	              $scope.aesFile.aesFreightForwarder.company == null ||
	              $scope.aesFile.aesFreightForwarder.company == "" || $scope.aesFile.aesFreightForwarder.company.id == undefined ) {
	              $scope.errorMap.put("aesFile.aesForwarder.company",$rootScope.nls["ERR201055"]);
	              $scope.Tabs='forwarder';
	              return false;
	          } else {
	          	if ($scope.aesFile.aesFreightForwarder.company.isDefaulter) {
	                  $scope.errorMap.put("aesFile.aesForwarder.company",$rootScope.nls["ERR201150"]);
	                  $scope.aesFile.aesFreightForwarder.company = {};
	                  $scope.aesFile.aesFreightForwarder.companyAddress = {};
	                  $scope.Tabs='forwarder';
	                  return false
	              }
	              if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.company.status)) {
	                  $scope.errorMap.put("aesFile.aesForwarder.company",$rootScope.nls["ERR90051"]);
	                  $scope.aesFile.aesFreightForwarder.company = {};
	                  $scope.aesFile.aesFreightForwarder.companyAddress = {};
	                  $scope.Tabs='forwarder';
	                  return false
	              }
	              if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.company.status)) {
	          	    $scope.errorMap.put("aesFile.aesForwarder.company",$rootScope.nls["ERR90052"]);
	                $scope.aesFile.aesFreightForwarder.company = {};
	                $scope.aesFile.aesFreightForwarder.companyAddress = {};
	                $scope.Tabs='forwarder';
	                return false
	            }
	          }
    }
	  
	  
	  
	 if (validateCode == 0 || validateCode == 3) {
		  
		if($scope.aesFile.aesFreightForwarder.companyAddress.addressLine1  == null
				|| $scope.aesFile.aesFreightForwarder.companyAddress.addressLine1  == ""
				|| $scope.aesFile.aesFreightForwarder.companyAddress.addressLine1 == undefined) {
			$scope.errorMap.put("aesFile.aesForwarder.addressLine1",$rootScope.nls["ERR38005"]);
			$scope.Tabs='forwarder';
			return false;
		}
		else {
			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.companyAddress.addressLine1)) {
					$scope.errorMap.put("aesFile.aesForwarder.addressLine1",$rootScope.nls["ERR38006"]);
					$scope.Tabs='forwarder';
					return false;
			}
	    }
		if($scope.aesFile.aesFreightForwarder.companyAddress.addressLine2  == null
				|| $scope.aesFile.aesFreightForwarder.companyAddress.addressLine2  ==""
				|| $scope.aesFile.aesFreightForwarder.companyAddress.addressLine2 ==undefined) {
			//$scope.errorMap.put("aesFile.aesFreightForwarder.addressLine2",$rootScope.nls["ERR38007"]);
			//$scope.Tabs='forwarder';
			//return false;
		} else {
			var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
			if (!regexp.test($scope.aesFile.aesFreightForwarder.companyAddress.addressLine2)) {
				$scope.errorMap.put("aesFile.aesForwarder.addressLine2",$rootScope.nls["ERR38008"]);
				$scope.Tabs='forwarder';
				return false;
			}
		}


		if($scope.aesFile.aesFreightForwarder.companyAddress.addressLine3!=null) {
				var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.companyAddress.addressLine3)) {
					console.log($rootScope.nls["ERR38009"]);
					$scope.errorMap.put("aesFile.aesForwarder.addressLine3",$rootScope.nls["ERR38009"]);
					$scope.Tabs='forwarder';
					return false;
				}
		}

		if($scope.aesFile.aesFreightForwarder.companyAddress.addressLine4  == null
				|| $scope.aesFile.aesFreightForwarder.companyAddress.addressLine4  ==""
				|| $scope.aesFile.aesFreightForwarder.companyAddress.addressLine4 ==undefined) {
			console.log($rootScope.nls["ERR38010"]);
			//$scope.errorMap.put("aesFile.aesForwarder.addressLine4",$rootScope.nls["ERR38010"]);
			//$scope.Tabs='forwarder';
			//return false;
		}
		else {
				var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,100}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.companyAddress.addressLine4)) {
					console.log($rootScope.nls["ERR38011"]);
					$scope.errorMap.put("aesFile.aesForwarder.addressLine4",$rootScope.nls["ERR38011"]);
					$scope.Tabs='forwarder';
					return false;
				}
		}
	  
}	  
	  
	  if(validateCode==0 || validateCode==4){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.stateMaster==undefined || $scope.aesFile.aesFreightForwarder.stateMaster==null ||$scope. aesFile.aesFreightForwarder.stateMaster=="" ){
			   $scope.errorMap.put("aes.aesForwarder.state", $rootScope.nls["ERR201194"]);
			   $scope.Tabs='forwarder';
			   return false
		   }
			else{
				if($scope.aesFile!=undefined &&  $scope.aesFile.aesFreightForwarder!=undefined &&$scope.aesFile.aesFreightForwarder.stateMaster!=undefined &&$scope.aesFile.aesFreightForwarder.stateMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.stateMaster.status)) {
		               $scope.aesFile.aesFreightForwarder.stateMaster = null;
		               $scope.errorMap.put("aes.aesForwarder.state", $rootScope.nls["ERR201195"]);
		               $scope.Tabs='forwarder';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.stateMaster.status)) {
		               $scope.aesFile.aesFreightForwarder.stateMaster = null;
		               $scope.errorMap.put("aes.aesForwarder.state", $rootScope.nls["ERR201196"]);
		               $scope.Tabs='forwarder';
		               return false;
		           }
				}
			}
		}
	  
	  if(validateCode==0 || validateCode==5){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.cityMaster==undefined || $scope.aesFile.aesFreightForwarder.cityMaster==null ||$scope. aesFile.aesFreightForwarder.cityMaster=="" ){
			   $scope.errorMap.put("aes.aesForwarder.city", $rootScope.nls["ERR201114"]);
			   $scope.Tabs='forwarder';
		   return false
		   }
			else{
				if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined &&$scope.aesFile.aesFreightForwarder.cityMaster!=undefined &&$scope.aesFile.aesFreightForwarder.cityMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.cityMaster.status)) {
		               $scope.aesFile.aesFreightForwarder.cityMaster = null;
		               $scope.errorMap.put("aes.aesForwarder.city", $rootScope.nls["ERR201197"]);
		               $scope.Tabs='forwarder';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.cityMaster.status)) {
		               $scope.aesFile.aesFreightForwarder.cityMaster = null;
		               $scope.errorMap.put("aes.aesForwarder.city", $rootScope.nls["ERR201198"]);
		               $scope.Tabs='forwarder';
		               return false;
		           }
				}
			}
		}
	  
	  if(validateCode==0 || validateCode==6){
		  /*if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.idType==undefined || $scope.aesFile.aesFreightForwarder.idType==null ||$scope. aesFile.aesFreightForwarder.idType=="" ){
			   $scope.errorMap.put("aes.aesForwarder.idType", $rootScope.nls["ERR201064"]);
		   return false
		   }*/
	  }
	  
	  if(validateCode==0 || validateCode==7){
		  if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.idNo!=undefined && $scope.aesFile.aesFreightForwarder.idNo!=null && $scope. aesFile.aesFreightForwarder.idNo!="" ){
			  
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,11}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.idNo)) {
					$scope.errorMap.put("aesFile.aesForwarder.idNo",$rootScope.nls["ERR2011989"]);
					$scope.Tabs='forwarder';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==8){
		  
		  if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.firstName==undefined || $scope.aesFile.aesFreightForwarder.firstName==null || $scope. aesFile.aesFreightForwarder.firstName=="" ){
			  $scope.errorMap.put("aesFile.aesForwarder.firstName",$rootScope.nls["ERR2011990"]);
			  $scope.Tabs='forwarder';
				return false;
			  
		  }
		  
		  
		  if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.firstName!=undefined && $scope.aesFile.aesFreightForwarder.firstName!=null && $scope. aesFile.aesFreightForwarder.firstName!="" ){
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.firstName)) {
					$scope.errorMap.put("aesFile.aesForwarder.firstName",$rootScope.nls["ERR201192"]);
					$scope.Tabs='forwarder';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==9){
		  
 if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.lastName==undefined || $scope.aesFile.aesFreightForwarder.lastName==null || $scope. aesFile.aesFreightForwarder.lastName=="" ){
			  
	 $scope.errorMap.put("aesFile.aesForwarder.lastName",$rootScope.nls["ERR2011991"]);
	 $scope.Tabs='forwarder';
		return false;
	 }
		  
		  if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.firstName!=undefined && $scope.aesFile.aesFreightForwarder.firstName!=null && $scope.aesFile.aesFreightForwarder.firstName!="" ){
			  var regexp = new RegExp("[ A-Za-z0-9  ]{0,100}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.lastName)) {
					$scope.errorMap.put("aesFile.aesForwarder.lastName",$rootScope.nls["ERR201193"]);
					$scope.Tabs='forwarder';
					return false;
				}
		   }
	  }
	  
	  if(validateCode==0 || validateCode==10){
		
		  if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.contactNo==undefined && $scope.aesFile.aesFreightForwarder.contactNo==null && $scope.aesFile.aesFreightForwarder.contactNo=="" ){
				/*$scope.errorMap.put("aesFile.aesForwarder.contactNo",$rootScope.nls["ERR201060"]);
				return false;*/
		  }else{
			  var regexp = new RegExp("[0-9+  ]{0,20}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.contactNo)) {
					$scope.errorMap.put("aesFile.aesForwarder.contactNo",$rootScope.nls["ERR2011992"]);
					$scope.Tabs='forwarder';
					return false;
				}
		  }
	  }
	  
	  if(validateCode==0 || validateCode==11){
			
		  if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.zipCode==undefined && $scope.aesFile.aesFreightForwarder.zipCode==null && $scope.aesFile.aesFreightForwarder.zipCode=="" ){
				/*$scope.errorMap.put("aesFile.aesForwarder.zipCode",$rootScope.nls["ERR201063"]);
				return false;*/
		  }else{
			  var regexp = new RegExp("[0-9A-Za-z  ]{0,20}");
				if (!regexp.test($scope.aesFile.aesFreightForwarder.zipCode)) {
					$scope.errorMap.put("aesFile.aesForwarder.zipCode",$rootScope.nls["ERR2011993"]);
					$scope.Tabs='forwarder';
					return false;
				}
		  }
	  }
	  
	  
	  if(validateCode==0 || validateCode==12){

		   if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined && $scope.aesFile.aesFreightForwarder.countryMaster==undefined || $scope.aesFile.aesFreightForwarder.countryMaster==null ||$scope. aesFile.aesFreightForwarder.countryMaster=="" ){
			 /*  $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR201061"]);
			   $scope.Tabs='consignee';
		   return false*/
		   }
			else{
				if($scope.aesFile!=undefined && $scope.aesFile.aesFreightForwarder!=undefined &&$scope.aesFile.aesFreightForwarder.countryMaster!=undefined &&$scope.aesFile.aesFreightForwarder.countryMaster!=null){
					
					 if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.countryMaster.status)) {
		               $scope.aesFile.aesFreightForwarder.countryMaster = null;
		               $scope.errorMap.put("aesFile.aesForwarder.country", $rootScope.nls["ERR2011994"]);
		               $scope.Tabs='forwarder';
		               return false;
		           }
		           if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.countryMaster.status)) {
		               $scope.aesFile.aesFreightForwarder.countryMaster = null;
		               $scope.errorMap.put("aesFile.aesForwarder.country", $rootScope.nls["ERR2011995"]);
		               $scope.Tabs='forwarder';
		               return false;
		           }
				}
			}
		}
	 
	  return true;
}//freight forwarder validation ends


function isEmptyRow(obj){
    //return (Object.getOwnPropertyNames(obj).length === 0);
    var isempty = true; //  empty
    if (!obj) {
        return isempty;
    }

    var k = Object.getOwnPropertyNames(obj);
    for (var i = 0; i < k.length; i++){
        if (obj[k[i]]) {
            isempty = false; // not empty
            break;

        }
    }
    return isempty;
}




//commodity validation
$scope.validateCommodityList=function(){
	
	var fineArr = [];
    $scope.secondArr=[];
    var validResult;
    var finalData = [];
    angular.forEach($scope.aesFile.aesCommodityList,function(dataObj,index){
        $scope.secondArr[index]={};
        if (isEmptyRow(dataObj)){
            validResult = true;
            $scope.$scope.aesFile.aesCommodityList.splice(index,1); 
            delete  $scope.$scope.aesFile.aesCommodityList[index]
        }else{
            validResult = validateObj(dataObj,index);
            finalData.push(dataObj);
        }
        if(validResult){
            fineArr.push(1);
            $scope.secondArr[index].errRow = false;
        }else{
            $scope.secondArr[index].errRow = true;
            fineArr.push(0);
        }
    });
    if(fineArr.indexOf(0)<0){
        $scope.tableData = finalData;
        return true;
    }else{
        return false;
   }
}   
    
    

    var validateObj = function(dataObj,index){
    	
        var errorArr = [];
        $scope.secondArr[index].errTextArr = [];
        
        if(dataObj.aesAction==null || dataObj.aesAction=="" || dataObj.aesAction==undefined){
                $scope.secondArr[index].aesAction = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201068"]);
            errorArr.push(0);
        }
        
        if(dataObj.originOfGoods==null || dataObj.originOfGoods=="" || dataObj.originOfGoods==undefined){
            $scope.secondArr[index].originOfGoods = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201069"]);
            errorArr.push(0);
        }
        
        if(dataObj.originOfGoods==null || dataObj.originOfGoods=="" || dataObj.originOfGoods==undefined){
            $scope.secondArr[index].originOfGoods = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201069"]);
            errorArr.push(0);
        }
        
        if(dataObj.aesLicense==null || dataObj.aesLicense=="" || dataObj.aesLicense==undefined || dataObj.aesLicense.id==undefined){
            $scope.secondArr[index].aesLicense = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201070"]);
            errorArr.push(0);
        }else{
        	 if(ValidateUtil.isStatusBlocked(dataObj.aesLicense.status)) {
                     $scope.secondArr[index].aesLicense = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201071"]);
                 errorArr.push(0);

             }
        	 if(ValidateUtil.isStatusHidden(dataObj.aesLicense.status)) {
                     $scope.secondArr[index].aesLicense = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201072"]);
                     errorArr.push(0);
              }
        }
        
        if(dataObj.aesSchedule==null || dataObj.aesSchedule=="" || dataObj.aesSchedule==undefined || dataObj.aesSchedule.id==undefined){
            $scope.secondArr[index].aesSchedule = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201073"]);
            errorArr.push(0);
        }
        
        if(dataObj.firstNoOfPieces==null || dataObj.firstNoOfPieces=="" || dataObj.firstNoOfPieces==undefined){
            $scope.secondArr[index].firstNoOfPieces = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201079"]);
            errorArr.push(0);
        }else{
        	var regexp = new RegExp("[0-9]{0,10}");
            if (!regexp.test(dataObj.firstNoOfPieces)) {
            	$scope.secondArr[index].firstNoOfPieces = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201129"]);
                errorArr.push(0);
            }
        }
        
        if(dataObj.secondNoOfPieces==null || dataObj.secondNoOfPieces=="" || dataObj.secondNoOfPieces==undefined){
            $scope.secondArr[index].secondNoOfPieces = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201079"]);
            errorArr.push(0);
        }else{
        	var regexp = new RegExp("[0-9]{0,10}");
            if (!regexp.test(dataObj.secondNoOfPieces)) {
            	$scope.secondArr[index].secondNoOfPieces = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201129"]);
                errorArr.push(0);
            }
        }

        
        if(dataObj.firstUnit==null || dataObj.firstUnit=="" || dataObj.firstUnit==undefined || dataObj.firstUnit.id==undefined){
            $scope.secondArr[index].firstUnit = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201089"]);
            errorArr.push(0);
        }else{
        	 if(ValidateUtil.isStatusBlocked(dataObj.firstUnit.status)) {
                     $scope.secondArr[index].firstUnit = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201090"]);
                 errorArr.push(0);

             }
        	 if(ValidateUtil.isStatusHidden(dataObj.firstUnit.status)) {
                     $scope.secondArr[index].firstUnit = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201091"]);
                     errorArr.push(0);
              }
        }
        
        if(dataObj.secondUnit==null || dataObj.secondUnit=="" || dataObj.secondUnit==undefined || dataObj.secondUnit.id==undefined){
            $scope.secondArr[index].secondUnit = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201089"]);
            errorArr.push(0);
        }else{
        	 if(ValidateUtil.isStatusBlocked(dataObj.secondUnit.status)) {
                     $scope.secondArr[index].secondUnit = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201090"]);
                 errorArr.push(0);

             }
        	 if(ValidateUtil.isStatusHidden(dataObj.secondUnit.status)) {
                     $scope.secondArr[index].secondUnit = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201091"]);
                     errorArr.push(0);
              }
        }

        
        if(dataObj.valueInUsd==null || dataObj.valueInUsd=="" || dataObj.valueInUsd==undefined){
            $scope.secondArr[index].valueInUsd = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201079"]);
            errorArr.push(0);
        }else{
        	var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,2})?|\.[0-9]+)$");
            if (!regexp.test(dataObj.valueInUsd)) {
            	$scope.secondArr[index].valueInUsd = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201129"]);
                errorArr.push(0);
            }
        }
        
        if(dataObj.grossWeightInKg==null || dataObj.grossWeightInKg=="" || dataObj.grossWeightInKg==undefined){
            $scope.secondArr[index].grossWeightInKg = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201081"]);
            errorArr.push(0);
        }else{
        	var regexp = new RegExp("^[+]?([0-9]{0,11}(?:[\.][0-9]{0,2})?|\.[0-9]+)$");
            if (!regexp.test(dataObj.grossWeightInKg)) {
            	$scope.secondArr[index].grossWeightInKg = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22071"]);
                errorArr.push(0);
            }
        }
        
        
        
   if(dataObj.itarExcemption!=null && dataObj.itarExcemption!="" && dataObj.itarExcemption!=undefined && dataObj.itarExcemption.id!=undefined){  
        
        if(ValidateUtil.isStatusBlocked(dataObj.itarExcemption.status)) {
            $scope.secondArr[index].itarExcemption = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201082"]);
        errorArr.push(0);

    }
	 if(ValidateUtil.isStatusHidden(dataObj.itarExcemption.status)) {
            $scope.secondArr[index].itarExcemption = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201083"]);
            errorArr.push(0);
     }
  }
        
        
   if(dataObj.categoryMaster==null || dataObj.categoryMaster=="" || dataObj.categoryMaster==undefined || dataObj.categoryMaster.id==undefined){
       $scope.secondArr[index].categoryMaster = true;
       $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201086"]);
       errorArr.push(0);
   }else{
   	 if(ValidateUtil.isStatusBlocked(dataObj.categoryMaster.status)) {
                $scope.secondArr[index].categoryMaster = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201087"]);
            errorArr.push(0);

        }
   	 if(ValidateUtil.isStatusHidden(dataObj.categoryMaster.status)) {
                $scope.secondArr[index].categoryMaster = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201088"]);
                errorArr.push(0);
         }
   }   
   
   
   if(dataObj.dtdc==null || dataObj.dtdc=="" || dataObj.dtdc==undefined || dataObj.dtdc.id==undefined){
       $scope.secondArr[index].dtdc = true;
       $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201089"]);
       errorArr.push(0);
   }else{
   	 if(ValidateUtil.isStatusBlocked(dataObj.dtdc.status)) {
                $scope.secondArr[index].dtdc = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201090"]);
            errorArr.push(0);

        }
   	 if(ValidateUtil.isStatusHidden(dataObj.dtdc.status)) {
                $scope.secondArr[index].dtdc = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201091"]);
                errorArr.push(0);
         }
   }   
        
        
        
        
   if(dataObj.dtdcQuantity==null || dataObj.dtdcQuantity=="" || dataObj.dtdcQuantity==undefined){
       $scope.secondArr[index].dtdcQuantity = true;
       $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201092"]);
       errorArr.push(0);
   }else{
   	var regexp = new RegExp("[0-9]{0,7}");
       if (!regexp.test(dataObj.dtdcQuantity)) {
       	$scope.secondArr[index].dtdcQuantity = true;
           $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201093"]);
           errorArr.push(0);
       }
   }
        
   if(dataObj.aesExport==null || dataObj.aesExport=="" || dataObj.aesExport==undefined || dataObj.aesExport.id==undefined){
       $scope.secondArr[index].aesExport = true;
       $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201094"]);
       errorArr.push(0);
   }else{
   	 if(ValidateUtil.isStatusBlocked(dataObj.aesExport.status)) {
                $scope.secondArr[index].aesExport = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201095"]);
            errorArr.push(0);

        }
   	 if(ValidateUtil.isStatusHidden(dataObj.aesExport.status)) {
                $scope.secondArr[index].aesExport = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201096"]);
                errorArr.push(0);
         }
   }     
        
   if(dataObj.licenseValue==null || dataObj.licenseValue=="" || dataObj.licenseValue==undefined ){
       $scope.secondArr[index].licenseValue = true;
       $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201099"]);
       errorArr.push(0);
   }else{
	   	var regexp = new RegExp("[0-9]{0,10}");
	       if (!regexp.test(dataObj.licenseValue)) {
	       	$scope.secondArr[index].licenseValue = true;
	           $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201100"]);
	           errorArr.push(0);
	       }
	   }
   
   
        if(dataObj.vinProductId!=null && dataObj.vinProductId!="" && dataObj.vinProductId!=undefined ){
        	var regexp = new RegExp("[0-9A-Za-z ]{0,25}");
 	       if (!regexp.test(dataObj.vinProductId)) {
 	       	$scope.secondArr[index].vinProductId = true;
 	           $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201102"]);
 	           errorArr.push(0);
 	       }
 	   }
        
        if(dataObj.vehicleTitleNo!=null && dataObj.vehicleTitleNo!="" && dataObj.vehicleTitleNo!=undefined ){
        	var regexp = new RegExp("[0-9A-Za-z ]{0,15}");
 	       if (!regexp.test(dataObj.vehicleTitleNo)) {
 	       	$scope.secondArr[index].vehicleTitleNo = true;
 	           $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201103"]);
 	           errorArr.push(0);
 	       }
 	   }
        
        if(dataObj.vehicleTitleState!=null && dataObj.vehicleTitleState!="" && dataObj.vehicleTitleState!=undefined){
          
        	 if(ValidateUtil.isStatusBlocked(dataObj.vehicleTitleState.status)) {
                     $scope.secondArr[index].vehicleTitleState = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201104"]);
                 errorArr.push(0);
             }
        	 if(ValidateUtil.isStatusHidden(dataObj.vehicleTitleState.status)) {
                     $scope.secondArr[index].vehicleTitleState = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201105"]);
                     errorArr.push(0);
              }
        }    
        
        if(errorArr.indexOf(0)<0){
            return true
        }else{
            return false;
        }
    }
        
    $scope.errorShow = function(errorObj,index){
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    
    $scope.addCommodityRow = function(){
        var fineArr = [];
        $scope.secondArr=[];
        angular.forEach($scope.aesFile.aesCommodityList,function(dataObj,index){
            //var curObj = $scope.tableData.data[index];
            $scope.secondArr[index]={};
            var validResult  = validateObj(dataObj,index);
            if(validResult){
                fineArr.push(1);
                $scope.secondArr[index].errRow = false;
            }else{
                $scope.secondArr[index].errRow = true;
                fineArr.push(0);
            }
            //console.log();
        });
        if(fineArr.indexOf(0)<0){
            if($scope.$scope.aesFile.aesCommodityList==undefined || $scope.$scope.aesFile.aesCommodityList==null)
                $scope.aesFile.aesCommodityList=[];
            $scope.aesCommodity={};
            $scope.aesCommodity.aesAction='Add';
            $scope.aesFile.aesCommodityList.push( $scope.aesCommodity);
            $scope.secondArr.push({"errRow":false});
            $scope.indexFocus = null;
          
        }
    };


  
      
