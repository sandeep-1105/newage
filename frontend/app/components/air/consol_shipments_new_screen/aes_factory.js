(function() {

	app.factory("AesSearch", function($resource) {
		return $resource("/api/v1/aesfile/search", {}, {
			query : {
				method : 'POST'
			}
		});
	});
	
	
	app.factory("AesPendingSearch", function($resource) {
		return $resource("/api/v1/aesfile/aespendingsearch", {}, {
			search : {
				method : 'POST'
			}
		});
	});
	
	
	app.factory("AesList", function($resource) {
		return $resource("/api/v1/aesfile/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	app.factory("AesTransportModeList", function($resource) {
		return $resource("/api/v1/aestransport/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	app.factory("IataMasterList", function($resource) {
		return $resource("/api/v1/iatamaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	app.factory("ScacMasterList", function($resource) {
		return $resource("/api/v1/scacamaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	app.factory("LicenseList", function($resource) {
		return $resource("/api/v1/aeslicense/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});

	
	app.factory("ScheduleList", function($resource) {
		return $resource("/api/v1/schedulebhtsmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});

	app.factory("ExportList", function($resource) {
		return $resource("/api/v1/exportcodemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	app.factory("ItarList", function($resource) {
		return $resource("/api/v1/itarexmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	
	app.factory("InBondTypeMasterList", function($resource) {
		return $resource("/api/v1/inbondtypemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	

	app.factory("AesAdd", function($resource) {
		return $resource("/api/v1/aesfile/create", {}, {
			save : {
				method : 'POST'
			}
		});
	});
	
	app.factory("AesAddAll", function($resource) {
		return $resource("/api/v1/aesfile/createall", {}, {
			save : {
				method : 'POST'
			}
		});
	});

	app.factory("AesEdit", function($resource) {
		return $resource("/api/v1/aesfile/update", {}, {
			update : {
				method : 'POST'
			}
		});
	});

	app.factory("AesView", function($resource) {
		return $resource("/api/v1/aesfile/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	});
	
	app.factory("AesStatusHistory", function($resource) {
		return $resource("/api/v1/aesfile/get/status/history/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	});
	
	app.factory("AesGetAll", function($resource) {
		return $resource("/api/v1/aesfile/getallaeslist/masteruid/:masteruid", {}, {
			get : {
				method : 'GET',
				params : {
					masteruid : ''
				},
				isArray : false
			}
		});
	});
	
	

	app.factory("AesRemove", function($resource) {
		return $resource("/api/v1/aesfile/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	});
	
	
	app.factory("AesEdiSave", function($resource) {
		return $resource("/api/v1/aesfile/edi/save/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	});
	
	app.factory("AesPortSearch", function($resource) {
		return $resource("/api/v1/aesfile/portmaster/search", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	
	app.factory("AesPortForeignSearch", function($resource) {
		return $resource("/api/v1/aesfile/foreign/portmaster/search", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});

	app.factory("AesStateSearch", function($resource) {
		return $resource("/api/v1/aesfile/statecodemaster/search", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});
	
	
})();
