app.service('consolShipmentDataService',['$rootScope', '$timeout','$state','roleConstant','Notification','$stateParams',
                                     function($rootScope, $timeout,$state,roleConstant,Notification,$stateParams) {
	
	
	this.isPickUpDeliveyModified=function(obj,type){
		if(obj==null || obj==undefined){
			return false;
		}
		 var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]] && k[i].startsWith(type, 0)) {
           	 if(angular.isObject(obj[k[i]])){
           		 if(obj[k[i]]!=null && obj[k[i]] != undefined && obj[k[i]].id!=null){
           			 return true
           		 }
           	 }
           	 else{
           		 if(obj[k[i]]!=null && obj[k[i]] !=undefined && obj[k[i]] !=''){
           			 return true
           		 }
           	}
            }
        }
        return false;
	 }
	
	this.connectionStatus = function(consol) {
     	if(consol!=undefined && consol.connectionList != null && consol.connectionList != undefined && consol.connectionList.length > 0) {
     		if(consol.connectionList.length > 1) {
     			return true;
     		}
     		for(var lIdx = 0; lIdx < consol.connectionList.length; lIdx++) {
     			if(consol.connectionList[lIdx] != null && consol.connectionList[lIdx] != undefined) {
     				
     				if((consol.connectionList[lIdx].move != null && consol.connectionList[lIdx].move != undefined && consol.connectionList[lIdx].move != "") 
     	        			|| (consol.connectionList[lIdx].pol != null && consol.connectionList[lIdx].pol != undefined && consol.connectionList[lIdx].pol != "") 
     	        			|| (consol.connectionList[lIdx].pod != null && consol.connectionList[lIdx].pod != undefined && consol.connectionList[lIdx].pod != "")
     	        			|| (consol.connectionList[lIdx].etd != null && consol.connectionList[lIdx].etd != undefined && consol.connectionList[lIdx].etd != "")
     	        			|| (consol.connectionList[lIdx].eta != null && consol.connectionList[lIdx].eta != undefined && consol.connectionList[lIdx].eta != "")
     	        			|| (consol.connectionList[lIdx].carrierMaster != null && consol.connectionList[lIdx].carrierMaster != undefined && consol.connectionList[lIdx].carrierMaster != "")) {
     	        		return true;
     	        	}
     			}
     		}
     	}
     	return false;
     }
	
	
	this.dimensionStatus = function(consol) {
		if(consol==undefined || consol.consolDocument==null || consol.consolDocument == undefined){
			return false;
		}
		var dimensionList =  consol.consolDocument.dimensionList;
	     	if(dimensionList != null && dimensionList != undefined && dimensionList.length > 0) {
	     		if(dimensionList.length > 1) {
	     			return true;
	     		}
	     		for(var lIdx = 0; lIdx < dimensionList.length; lIdx++) {
	     			if(dimensionList[lIdx] != null && dimensionList[lIdx] != undefined) {
	     				if((dimensionList[lIdx].noOfPiece != null && dimensionList[lIdx].noOfPiece != undefined && dimensionList[lIdx].noOfPiece != "") 
	     	        			|| (dimensionList[lIdx].length != null && dimensionList[lIdx].length != undefined && dimensionList[lIdx].length != "") 
	     	        			|| (dimensionList[lIdx].width != null && dimensionList[lIdx].width != undefined && dimensionList[lIdx].width != "")
	     	        			|| (dimensionList[lIdx].height != null && dimensionList[lIdx].height != undefined && dimensionList[lIdx].height != "")
	     	        			|| (dimensionList[lIdx].volWeight != null && dimensionList[lIdx].volWeight != undefined && dimensionList[lIdx].volWeight != "")
	     	        			|| (dimensionList[lIdx].grossWeight != null && dimensionList[lIdx].grossWeight != undefined && dimensionList[lIdx].grossWeight != "")) {
	     	        		return true;
	     	        	}
	     			}
	     		}
	     	}
	     	return false;
	     }
	
	this.packAndDimensionStatus = function(consol) {
		if(consol==undefined || consol==null || consol.consolDocument==null || consol.consolDocument == undefined){
			return false;
		}
		var consolDocument=consol.consolDocument;
		if((consolDocument.noOfPieces != null && consolDocument.noOfPieces != undefined && consolDocument.noOfPieces != "" ) 
  			|| (consolDocument.grossWeightInPound != null && consolDocument.grossWeightInPound != undefined && consolDocument.grossWeightInPound != "")
  			|| (consolDocument.volumeWeightInPound != null && consolDocument.volumeWeightInPound != undefined && consolDocument.volumeWeightInPound != "")
		) {
  		return true;
  	      }
		else{
			this.dimensionStatus(consol);
		}
     return false;
 }
	
	this.otherInfoStatus = function(consol) {
		if(consol==undefined || consol.consolDocument==null || consol.consolDocument == undefined || consol.serviceMaster==undefined ||
				consol.serviceMaster==null){
			return false;
		}
		var consolDocument=consol.consolDocument;
		if(consol.serviceMaster.importExport=='Export'){
		if((consolDocument.mawbIssueDate != null && consolDocument.mawbIssueDate != undefined && consolDocument.mawbIssueDate != "" ) 
  			|| (consolDocument.employeeName != null && consolDocument.employeeName != undefined && consolDocument.employeeName != "")
  			|| (consolDocument.handlingInformation != null && consolDocument.handlingInformation != undefined && consolDocument.handlingInformation != "")
  			|| (consolDocument.accountingInformation != null && consolDocument.accountingInformation != undefined && consolDocument.accountingInformation != "")
  			|| (consolDocument.commodityDescription != null && consolDocument.commodityDescription != undefined && consolDocument.commodityDescription != "")
  			|| (consolDocument.marksAndNo != null && consolDocument.marksAndNo != undefined && consolDocument.marksAndNo != "")
  			|| (consolDocument.rateClass != null && consolDocument.rateClass != undefined && consolDocument.rateClass != "")
		) {
  		return true;
  	      }
		}
		else{
			if((consolDocument.isHold == true) 
		  			|| (consolDocument.holdReleasenote != null && consolDocument.holdReleasenote != undefined && consolDocument.holdReleasenote != "")
		  			|| (consolDocument.itemNo != null && consolDocument.itemNo != undefined && consolDocument.itemNo != "")
		  			|| (consolDocument.rateClass != null && consolDocument.rateClass != undefined && consolDocument.rateClass != "")
		  			|| (consolDocument.commodityDescription != null && consolDocument.commodityDescription != undefined && consolDocument.commodityDescription != "")
		  			|| (consolDocument.marksAndNo != null && consolDocument.marksAndNo != undefined && consolDocument.marksAndNo != "")
				) {
		  		return true;
		  	      }
		}
     return false;
 }
	this.routingAndCarrierStatus = function(consol) {	
		if(consol==undefined || consol.consolDocument==null || consol.consolDocument == undefined){
			return false;
		}
		var consolDocument=consol.consolDocument;
			if((consol.carrier != null && consol.carrier != undefined && consol.carrier != "" && consol.carrier.id != null) 
     			|| (consolDocument.routeNo != null && consolDocument.routeNo != undefined && consolDocument.routeNo != "")
     			|| (consolDocument.mawbNo != null && consolDocument.mawbNo != undefined && consolDocument.mawbNo != "")
     			|| (consolDocument.etd != null && consolDocument.etd != undefined && consolDocument.etd != "")
     			|| (consolDocument.eta != null && consolDocument.eta != undefined && consolDocument.eta != "")
     			|| (consolDocument.etd != null && consolDocument.atd != undefined && consolDocument.atd != "")
     			|| (consolDocument.ata != null && consolDocument.ata != undefined && consolDocument.ata != "")
     			|| (consolDocument.mawbGeneratedDate != null && consolDocument.mawbGeneratedDate != undefined && consolDocument.mawbGeneratedDate != "") ) {
     		return true;
     	}
return false;
}
	this.hawbCustomerInfoStatus = function(documentDetail) {
		if(documentDetail==null || documentDetail == undefined){
			return false;
		}
		if((documentDetail.shipper != null && documentDetail.shipper != undefined && documentDetail.shipper != "" && documentDetail.shipper.id!=null) 
				|| (documentDetail.consignee != null && documentDetail.consignee != undefined && documentDetail.consignee != "" && documentDetail.consignee.id!=null)
				|| (documentDetail.firstNotify != null && documentDetail.firstNotify != undefined && documentDetail.firstNotify != "" && documentDetail.firstNotify.id!=null)
				|| (documentDetail.secondNotify != null && documentDetail.secondNotify != undefined && documentDetail.secondNotify != "" && documentDetail.secondNotify.id!=null)
				|| (documentDetail.forwarder != null && documentDetail.forwarder != undefined && documentDetail.forwarder != "" && documentDetail.forwarder.id!=null)
				|| (documentDetail.agent != null && documentDetail.agent != undefined && documentDetail.agent != "" && documentDetail.agent.id!=null)
				|| (documentDetail.issuingAgent != null && documentDetail.issuingAgent != undefined && documentDetail.issuingAgent != "" && documentDetail.issuingAgent.id!=null)
		) {
			return true;
		}
	return false;
	}
	
}]);