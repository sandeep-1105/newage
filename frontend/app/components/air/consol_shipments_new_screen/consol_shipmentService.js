/**
 * 
 */
app.factory('consolService', ['NavigationService', '$rootScope', '$location',
    function (NavigationService, $rootScope, $location) {

        var savedData = {}

        var loc = null;

        function set(data) {
            savedData = data;
            localStorage.savedData = JSON.stringify(savedData);
        }

        function get() {
            savedData = localStorage.savedData == null ? null : JSON.parse(localStorage.savedData);
            return savedData;
        }

        function setLoc(data) {
            loc = data;
        }

        function getLoc() {
            return loc;
        }



        function goToEdit(consol) {

            consol.consolDocument.documentReqDate = $rootScope.getDate(consol.consolDocument.documentReqDate);
            consol.consolDocument.dimensionUnit = consol.consolDocument.dimensionUnit == 'CENTIMETERORKILOS' ? false : true;
            consol.consolDocument.dimensionUnitValue = consol.consolDocument.dimensionUnit ? $rootScope.appMasterData['inch.to.pounds'] : $rootScope.appMasterData['centimeter.to.kilos'];
            consol.ppcc = consol.ppcc == 'Prepaid' ? false : true;
            consol.consolDocument.isAgreed == 'No' ? false : true;
            consol.consolDocument.isMarksNo == 'No' ? false : true;
            if (consol.pickUpDeliveryPoint == null && consol.pickUpDeliveryPoint == undefined) {
                consol.pickUpDeliveryPoint = {};
            }

            consol.masterDate = consol.masterDate;
            consol.consolDocument.mawbIssueDate = $rootScope.getDate(consol.consolDocument.mawbIssueDate);
            consol.eta = $rootScope.getDate(consol.eta);
            consol.etd = $rootScope.getDate(consol.etd);
            consol.atd = $rootScope.getDate(consol.atd);
            consol.ata = $rootScope.getDate(consol.ata);

            if (consol.connectionList != undefined && consol.connectionList != null && consol.connectionList.length != 0) {
                for (var i = 0; i < consol.connectionList.length; i++) {
                    consol.connectionList[i].eta = $rootScope.getDate(consol.connectionList[i].eta);
                    consol.connectionList[i].etd = $rootScope.getDate(consol.connectionList[i].etd);
                }
            }

            if (consol.eventList != undefined && consol.eventList != null && consol.eventList.length != 0) {
                for (var i = 0; i < consol.eventList.length; i++) {
                    consol.eventList[i].eventDate = $rootScope.getDate(consol.eventList[i].eventDate);
                    consol.eventList[i].followUpDate = $rootScope.getDate(consol.eventList[i].followUpDate);
                }
            }

            return consol;

        }

        return {
            set: set,
            get: get,
            setLoc: setLoc,
            getLoc: getLoc,
            goToEdit: goToEdit,
        }

    }]);


app.factory('modifyGenerateEdiServiceRequest', function () {


    var modifyGenerateEDIRequest = function (requestObjcet) {
        if (requestObjcet.locationMaster) {
            requestObjcet.locationMaster = overrideObjectWithIdAndVersion(requestObjcet.locationMaster);
        }
        if (requestObjcet.companyMaster) {
            requestObjcet.companyMaster = overrideObjectWithIdAndVersion(requestObjcet.companyMaster);
        }
        if (requestObjcet.countryMaster) {
            requestObjcet.countryMaster = overrideObjectWithIdAndVersion(requestObjcet.countryMaster);
        }
        if (requestObjcet.shipper) {
            requestObjcet["shipper"] = overrideObjectWithIdAndVersion(requestObjcet["shipper"]);
        }
        if (requestObjcet.shipperAddress) {
            requestObjcet["shipperAddress"] = updateAddress(requestObjcet["shipperAddress"]);
        }
        if (requestObjcet.consignee) {
            requestObjcet["consignee"] = overrideObjectWithIdAndVersion(requestObjcet["consignee"]);
        }
        if (requestObjcet.consigneeAddress) {
            requestObjcet["consigneeAddress"] = updateAddress(requestObjcet["consigneeAddress"])
        }
        if (requestObjcet.agent) {
            requestObjcet["agent"] = overrideObjectWithIdAndVersion(requestObjcet["agent"]);
        }
        if (requestObjcet.agentAddress) {
            requestObjcet["agentAddress"] = updateAddress(requestObjcet["agentAddress"])
        }
        if (requestObjcet.currencyMaster) {
            requestObjcet.currencyMaster = overrideObjectWithIdAndVersion(requestObjcet.currencyMaster);
        }
        if (requestObjcet.origin) {
            requestObjcet.origin = overrideObjectWithIdAndVersion(requestObjcet.origin);
        }
        if (requestObjcet.pol) {
            requestObjcet.pol = overrideObjectWithIdAndVersion(requestObjcet.pol);
        }
        if (requestObjcet.pod) {
            requestObjcet.pod = overrideObjectWithIdAndVersion(requestObjcet.pod);
        }
        if (requestObjcet.destination) {
            requestObjcet.destination = overrideObjectWithIdAndVersion(requestObjcet.destination);
        }
        if (requestObjcet.carrierMaster) {
            requestObjcet.carrierMaster = overrideObjectWithIdAndVersion(requestObjcet.carrierMaster);
        }

        if (requestObjcet.connectionList && requestObjcet.connectionList.length > 0) {
            for (var i = 0; i < requestObjcet.connectionList.length; i++) {
                if (requestObjcet.connectionList[i]) {
                    if (requestObjcet.connectionList[i].locationMaster) {
                        requestObjcet.connectionList[i].locationMaster = overrideObjectWithIdAndVersion(requestObjcet.connectionList[i].locationMaster);
                    }
                    if (requestObjcet.connectionList[i].companyMaster) {
                        requestObjcet.connectionList[i].companyMaster = overrideObjectWithIdAndVersion(requestObjcet.connectionList[i].companyMaster);
                    }
                    if (requestObjcet.connectionList[i].countryMaster) {
                        requestObjcet.connectionList[i].countryMaster = overrideObjectWithIdAndVersion(requestObjcet.connectionList[i].countryMaster);
                    }
                    if (requestObjcet.connectionList[i].pol) {
                        requestObjcet.connectionList[i].pol = overrideObjectWithIdAndVersion(requestObjcet.connectionList[i].pol);
                    }
                    if (requestObjcet.connectionList[i].pod) {
                        requestObjcet.connectionList[i].pod = overrideObjectWithIdAndVersion(requestObjcet.connectionList[i].pod);
                    }
                    if (requestObjcet.connectionList[i].carrierMaster) {
                        requestObjcet.connectionList[i].carrierMaster = overrideObjectWithIdAndVersion(requestObjcet.connectionList[i].carrierMaster);
                    }
                }
            }
        }
        if (requestObjcet.houseList && requestObjcet.houseList.length > 0) {
            for (var i = 0; i < requestObjcet.houseList.length; i++) {
                if (requestObjcet.houseList[i]) {
                    if (requestObjcet.houseList[i].locationMaster) {
                        requestObjcet.houseList[i].locationMaster = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].locationMaster);
                    }
                    if (requestObjcet.houseList[i].companyMaster) {
                        requestObjcet.houseList[i].companyMaster = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].companyMaster);
                    }
                    if (requestObjcet.houseList[i].countryMaster) {
                        requestObjcet.houseList[i].countryMaster = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].countryMaster);
                    }
                    if (requestObjcet.houseList[i].origin) {
                        requestObjcet.houseList[i].origin = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].origin);
                    }
                    if (requestObjcet.houseList[i].destination) {
                        requestObjcet.houseList[i].destination = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].destination);
                    }
                    if (requestObjcet.houseList[i].shipper) {
                        requestObjcet.houseList[i]["shipper"] = overrideObjectWithIdAndVersion(requestObjcet.houseList[i]["shipper"]);
                    }
                    if (requestObjcet.houseList[i].shipperAddress) {
                        requestObjcet.houseList[i]["shipperAddress"] = updateAddress(requestObjcet.houseList[i]["shipperAddress"]);
                    }
                    if (requestObjcet.houseList[i].consignee) {
                        requestObjcet.houseList[i]["consignee"] = overrideObjectWithIdAndVersion(requestObjcet.houseList[i]["consignee"]);
                    }
                    if (requestObjcet.houseList[i].consigneeAddress) {
                        requestObjcet.houseList[i]["consigneeAddress"] = updateAddress(requestObjcet.houseList[i]["consigneeAddress"])
                    }
                    if (requestObjcet.houseList[i].agent) {
                        requestObjcet.houseList[i]["agent"] = overrideObjectWithIdAndVersion(requestObjcet.houseList[i]["agent"]);
                    }
                    if (requestObjcet.houseList[i].agentAddress) {
                        requestObjcet.houseList[i]["agentAddress"] = updateAddress(requestObjcet.houseList[i]["agentAddress"]);
                    }
                    if (requestObjcet.houseList[i].tosMaster) {
                        requestObjcet.houseList[i].tosMaster = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].tosMaster);
                    }
                    if (requestObjcet.houseList[i].commodityMaster) {
                        requestObjcet.houseList[i].commodityMaster = overrideObjectWithIdAndVersion(requestObjcet.houseList[i].commodityMaster);
                    }
                }
            }
        }
        return requestObjcet;
    }

    var updateAddress = function (parentObject, reqObjKey) {
        if (parentObject.state) {
            parentObject.state = overrideObjectWithIdAndVersion(parentObject.state);
        }
        if (parentObject.city) {
            parentObject.city = overrideObjectWithIdAndVersion(parentObject.city);
        }
        if (parentObject.country) {
            parentObject.country = overrideObjectWithIdAndVersion(parentObject.country);
        }
        return parentObject;
    }

    var updatePersonDetail = function (parentObject, reqObjKey) {

        if (parentObject.countryMaster) {
            parentObject.countryMaster = overrideObjectWithIdAndVersion(parentObject.countryMaster);
        }
        if (parentObject.partyDetail) {
            parentObject.partyDetail = overrideObjectWithIdAndVersion(parentObject.partyDetail);
        }
        if (parentObject.partyTypeList && parentObject.partyTypeList.length > 0) {
            for (var i = 0; i < parentObject.partyTypeList.length; i++) {
                if (parentObject.partyTypeList[i]) {
                    if (parentObject.partyTypeList[i].partyTypeMaster) {
                        parentObject.partyTypeList[i].partyTypeMaster = overrideObjectWithIdAndVersion(parentObject.partyTypeList[i].partyTypeMaster);
                    }
                }
            }
        }
        if (parentObject.partyAddressList && parentObject.partyAddressList.length > 0) {
            for (var i = 0; i < parentObject.partyAddressList.length; i++) {
                if (parentObject.partyAddressList[i]) {
                    if (parentObject.partyAddressList[i].stateMaster) {
                        parentObject.partyAddressList[i].stateMaster = overrideObjectWithIdAndVersion(parentObject.partyAddressList[i].stateMaster);
                    }
                    if (parentObject.partyAddressList[i].cityMaster) {
                        parentObject.partyAddressList[i].cityMaster = overrideObjectWithIdAndVersion(parentObject.partyAddressList[i].cityMaster);
                    }
                    if (parentObject.partyAddressList[i].partyCountryField) {
                        parentObject.partyAddressList[i].partyCountryField = overrideObjectWithIdAndVersion(parentObject.partyAddressList[i].partyCountryField);
                    }
                }
            }
        }
        if (parentObject.partyServiceList && parentObject.partyServiceList.length > 0) {
            for (var i = 0; i < parentObject.partyServiceList.length; i++) {
                if (parentObject.partyServiceList[i]) {
                    if (parentObject.partyServiceList[i].serviceMaster) {
                        parentObject.partyServiceList[i].serviceMaster = overrideObjectWithIdAndVersion(parentObject.partyServiceList[i].serviceMaster);
                    }
                    if (parentObject.partyServiceList[i].tosMaster) {
                        parentObject.partyServiceList[i].tosMaster = overrideObjectWithIdAndVersion(parentObject.partyServiceList[i].tosMaster);
                    }
                    if (parentObject.partyServiceList[i].locationMaster) {
                        parentObject.partyServiceList[i].locationMaster = overrideObjectWithIdAndVersion(parentObject.partyServiceList[i].locationMaster);
                    }
                    if (parentObject.partyServiceList[i].salesman) {
                        parentObject.partyServiceList[i].salesman = overrideObjectWithIdAndVersion(parentObject.partyServiceList[i].salesman);
                    }
                    if (parentObject.partyServiceList[i].customerServiceCode) {
                        parentObject.partyServiceList[i].customerServiceCode = overrideObjectWithIdAndVersion(parentObject.partyServiceList[i].customerServiceCode);
                    }
                }
            }
        }
        if (parentObject.partyAccountList && parentObject.partyAccountList.length > 0) {
            for (var i = 0; i < parentObject.partyAccountList.length; i++) {
                if (parentObject.partyAccountList[i]) {
                    if (parentObject.partyAccountList[i].accountMaster) {
                        parentObject.partyAccountList[i].accountMaster = overrideObjectWithIdAndVersion(parentObject.partyAccountList[i].accountMaster);
                    }
                    if (parentObject.partyAccountList[i].locationMaster) {
                        parentObject.partyAccountList[i].locationMaster = overrideObjectWithIdAndVersion(parentObject.partyAccountList[i].locationMaster);
                    }
                    if (parentObject.partyAccountList[i].currencyMaster) {
                        parentObject.partyAccountList[i].currencyMaster = overrideObjectWithIdAndVersion(parentObject.partyAccountList[i].currencyMaster);
                    }
                }
            }
        }
        if (parentObject.partyCreditLimitList && parentObject.partyCreditLimitList.length > 0) {
            for (var i = 0; i < parentObject.partyCreditLimitList.length; i++) {
                if (parentObject.partyCreditLimitList[i]) {
                    if (parentObject.partyCreditLimitList[i].serviceMaster) {
                        parentObject.partyCreditLimitList[i].serviceMaster = overrideObjectWithIdAndVersion(parentObject.partyCreditLimitList[i].serviceMaster);
                    }
                    if (parentObject.partyCreditLimitList[i].locationMaster) {
                        parentObject.partyCreditLimitList[i].locationMaster = overrideObjectWithIdAndVersion(parentObject.partyCreditLimitList[i].locationMaster);
                    }
                }
            }
        }
        if (parentObject.partyEmailMappingList && parentObject.partyEmailMappingList.length > 0) {
            for (var i = 0; i < parentObject.partyEmailMappingList.length; i++) {
                if (parentObject.partyEmailMappingList[i]) {
                    if (parentObject.partyEmailMappingList[i].companyMaster) {
                        parentObject.partyEmailMappingList[i].companyMaster = overrideObjectWithIdAndVersion(parentObject.partyEmailMappingList[i].companyMaster);
                    }
                    if (parentObject.partyEmailMappingList[i].countryMaster) {
                        parentObject.partyEmailMappingList[i].countryMaster = overrideObjectWithIdAndVersion(parentObject.partyEmailMappingList[i].countryMaster);
                    }
                    if (parentObject.partyEmailMappingList[i].locationMaster) {
                        parentObject.partyEmailMappingList[i].locationMaster = overrideObjectWithIdAndVersion(parentObject.partyEmailMappingList[i].locationMaster);
                    }
                    if (parentObject.partyEmailMappingList[i].destinationCountry) {
                        parentObject.partyEmailMappingList[i].destinationCountry = overrideObjectWithIdAndVersion(parentObject.partyEmailMappingList[i].destinationCountry);
                    }
                    if (parentObject.partyEmailMappingList[i].originCountry) {
                        parentObject.partyEmailMappingList[i].originCountry = overrideObjectWithIdAndVersion(parentObject.partyEmailMappingList[i].originCountry);
                    }
                }
            }
        }
        if (parentObject.partyBusinessDetailList && parentObject.partyBusinessDetailList.length > 0) {
            for (var i = 0; i < parentObject.partyBusinessDetailList.length; i++) {
                if (parentObject.partyBusinessDetailList[i]) {
                    if (parentObject.partyBusinessDetailList[i].createdLocation) {
                        parentObject.partyBusinessDetailList[i].createdLocation = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].createdLocation);
                    }
                    if (parentObject.partyBusinessDetailList[i].serviceMaster) {
                        parentObject.partyBusinessDetailList[i].serviceMaster = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].serviceMaster);
                    }
                    if (parentObject.partyBusinessDetailList[i].period) {
                        parentObject.partyBusinessDetailList[i].period = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].period);
                    }
                    if (parentObject.partyBusinessDetailList[i].origin) {
                        parentObject.partyBusinessDetailList[i].origin = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].origin);
                    }
                    if (parentObject.partyBusinessDetailList[i].destination) {
                        parentObject.partyBusinessDetailList[i].destination = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].destination);
                    }
                    if (parentObject.partyBusinessDetailList[i].commodity) {
                        parentObject.partyBusinessDetailList[i].commodity = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].commodity);
                    }
                    if (parentObject.partyBusinessDetailList[i].tosMaster) {
                        parentObject.partyBusinessDetailList[i].tosMaster = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].tosMaster);
                    }
                    if (parentObject.partyBusinessDetailList[i].unitMaster) {
                        parentObject.partyBusinessDetailList[i].unitMaster = overrideObjectWithIdAndVersion(parentObject.partyBusinessDetailList[i].unitMaster);
                    }

                }
            }
        }
        if (parentObject.partyCompanyAssociateList && parentObject.partyCompanyAssociateList.length > 0) {
            for (var i = 0; i < parentObject.partyCompanyAssociateList.length; i++) {
                if (parentObject.partyCompanyAssociateList[i]) {
                    if (parentObject.partyCompanyAssociateList[i].companyMaster) {
                        parentObject.partyCompanyAssociateList[i].companyMaster = overrideObjectWithIdAndVersion(parentObject.partyCompanyAssociateList[i].companyMaster);
                    }
                }
            }
        }
        return parentObject;
    }

    var overrideObjectWithIdAndVersion = function (requestedObject) {
        requestedObject = {
            "id": requestedObject.id,
            "versionLock": requestedObject.versionLock
        };
        return requestedObject;
    }

    return {
        modifyGenerateEDIRequest: modifyGenerateEDIRequest
    }

});