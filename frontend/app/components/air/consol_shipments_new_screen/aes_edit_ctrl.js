/**
 * Created by hmspl on 12/4/16.
 */
app.controller('aes_EditCtrl',['$rootScope', '$scope', '$location', '$modal', 'ngDialog', '$http', '$timeout', '$stateParams', 'ValidateUtil', 
        '$state', '$window', 'Notification', 'aesService', 'AesView', 'CarrierByTransportMode', 'PortByTransportMode', 
        'StateList', 'CountryList', 'PartiesList', 'AesAdd', 'cloneService', 'ShipmentGetByUid',
        'InBondTypeMasterList', 'ScacMasterList', 'IataMasterList', 'AesTransportModeList',
        'AesEdit', 'CityList', 'LicenseList', 'CommodityList', 'ScheduleList', 'ExportList', 'ItarList', 
        'CategoryList', 'UnitSearchKeyword', 'appConstant', 'addressJoiner', 'CommonValidationService', 
        'AesEdiSave', 'StateListCode', 'AesStateSearch', 'AesPortSearch', 'AesStatusHistory', 'AesPortForeignSearch',
    function($rootScope, $scope, $location, $modal, ngDialog, $http, $timeout, $stateParams, ValidateUtil, 
        $state, $window, Notification, aesService, AesView, CarrierByTransportMode, PortByTransportMode, 
        StateList, CountryList, PartiesList, AesAdd, cloneService, ShipmentGetByUid,
        InBondTypeMasterList, ScacMasterList, IataMasterList, AesTransportModeList,
        AesEdit, CityList, LicenseList, CommodityList, ScheduleList, ExportList, ItarList, 
        CategoryList, UnitSearchKeyword, appConstant, addressJoiner, CommonValidationService, 
        AesEdiSave, StateListCode, AesStateSearch, AesPortSearch, AesStatusHistory, AesPortForeignSearch) {

    $scope.initDateFormat = function() {
        $scope.directiveDateTimePickerOpts = {
            format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
            useCurrent: false,
            sideBySide: true,
            viewDate: moment()
        };
        $scope.directiveDateTimePickerOpts.widgetParent = "body";

        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";
    }


    $scope.reloadAesFlag = false
    // Select arr's
    $scope.aesInBondTypeArr = $rootScope.enum['AesInBondType'];

    $scope.consigneeTypeArr = $rootScope.enum['ConsigneeType'];
    $scope.idTypeArr = $rootScope.enum['IdType'];
    $scope.aesTransportModeArr = $rootScope.enum['AesTransportMode'];
    $scope.aesActionArr = ['Add', 'Change', 'Cancel', 'Replace'];
    $scope.aesActionCommodity = ['Add', 'Change', 'Delete'];

    $scope.init = function() {
        console.log("aes init called  ... ");
        //$rootScope.breadcrumbArr = [{label:"Air", state:"layout.airConsol"},{label:"AES Generate", state:null}];


        if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewNewAirConsol') {
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.viewNewAirConsol"
            }, {
                label: "Master Shipment",
                state: null
            }, {
                label: "Automated Export System",
                state: null
            }];
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.editAirConsol') {
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.editAirConsol"
            }, {
                label: "Master Shipment",
                state: null
            }, {
                label: "Automated Export System",
                state: null
            }];
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewCrmShipment') {
            $rootScope.breadcrumbArr = [{
                label: "Crm",
                state: "layout.viewCrmShipment"
            }, {
                label: "Shipment",
                state: null
            }, {
                label: "Automated Export System",
                state: null
            }];
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.editNewShipment') {
            $rootScope.breadcrumbArr = [{
                label: "Crm",
                state: "layout.editNewShipment"
            }, {
                label: "Shipment",
                state: null
            }, {
                label: "Automated Export System",
                state: null
            }];
        } else {
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.editAirConsol"
            }, {
                label: "Master Shipment",
                state: null
            }, {
                label: "Automated Export System",
                state: null
            }];
        }

        if ($scope.aesFile.aesUsppi == undefined || $scope.aesFile.aesUsppi == null) {
            $scope.aesFile.aesUsppi = {};
        }

        if ($scope.aesFile.aesUsppi.idType == undefined || $scope.aesFile.aesUsppi.idType == null) {
            $scope.aesFile.aesUsppi.idType = 'EIN';
        }

        if ($scope.aesFile.aesIntermediateConsignee == undefined || $scope.aesFile.aesIntermediateConsignee == null) {
            $scope.aesFile.aesIntermediateConsignee = {};
        }
        if ($scope.aesFile.aesUltimateConsignee == undefined || $scope.aesFile.aesUltimateConsignee == null) {
            $scope.aesFile.aesUltimateConsignee = {};
        }
        if ($scope.aesFile.aesFreightForwarder == undefined || $scope.aesFile.aesFreightForwarder == null) {
            $scope.aesFile.aesFreightForwarder = {};
        }
        if ($scope.aesFile.aesCommodityList == undefined || $scope.aesFile.aesCommodityList == null || $scope.aesFile.aesCommodityList.length == 0) {
            $scope.aesFile.aesCommodityList = [];
            $scope.aesFile.aesCommodityList.push({});
            $scope.aesFile.aesCommodityList[0].dtdcVehicleList = [];
            $scope.aesFile.aesCommodityList[0].dtdcVehicleList.push({});
        } else {
            if (!$scope.reloadAesFlag) {
                var length = $scope.aesFile.aesCommodityList.length;
                if ($scope.aesFile.aesCommodityList[length - 1].dtdcVehicleList || $scope.aesFile.aesCommodityList[length - 1].dtdcVehicleList == null) {
                    $scope.aesFile.aesCommodityList[length - 1].dtdcVehicleList = [];
                    $scope.aesFile.aesCommodityList[length - 1].dtdcVehicleList.push({});
                }
                $scope.aesFile.aesCommodityList.push({});
            }
        }

        if ($scope.aesFile != undefined && $scope.aesFile != null && $scope.aesFile.id == undefined && $stateParams.action == 'ADD') {

            if ($scope.aesFile.aesFilingType == undefined || $scope.aesFile.aesFilingType == null) {
                $scope.aesFile.aesFilingType = false;
            }
            if ($scope.aesFile.routedTransaction == undefined || $scope.aesFile.routedTransaction == null) {
                $scope.aesFile.routedTransaction = false;
            }
            if ($scope.aesFile.usspiUltimateConsignee == undefined || $scope.aesFile.usspiUltimateConsignee == null) {
                $scope.aesFile.usspiUltimateConsignee = false;
            }
            if ($scope.aesFile.hazardousCargo == undefined || $scope.aesFile.hazardousCargo == null) {
                $scope.aesFile.hazardousCargo = false;
            }
            if ($scope.aesFile.aesAction == undefined || $scope.aesFile.aesAction == null) {
                $scope.aesFile.aesAction = 'Add';
            }

            if ($scope.aesFile.aesUltimateConsignee.soldEnRoute == undefined || $scope.aesFile.aesUltimateConsignee.soldEnRoute == null) {
                $scope.aesFile.aesUltimateConsignee.soldEnRoute = false;
            }
        } else {
            if ($scope.aesFile.aesFilingType == undefined || $scope.aesFile.aesFilingType == null) {
                $scope.aesFile.aesFilingType = false;
            }
            if ($scope.aesFile.routedTransaction == undefined || $scope.aesFile.routedTransaction == null) {
                $scope.aesFile.routedTransaction = false;
            }
            if ($scope.aesFile.usspiUltimateConsignee == undefined || $scope.aesFile.usspiUltimateConsignee == null) {
                $scope.aesFile.usspiUltimateConsignee = false;
            }
            if ($scope.aesFile.hazardousCargo == undefined || $scope.aesFile.hazardousCargo == null) {
                $scope.aesFile.hazardousCargo = false;
            }
            if ($scope.aesFile.aesAction == undefined || $scope.aesFile.aesAction == null) {
                $scope.aesFile.aesAction = 'Add';
            }

        }

        for (var i = 0; i < $scope.aesFile.aesCommodityList.length; i++) {

            if ($scope.aesFile.aesCommodityList[i] != undefined) {
                if ($scope.aesFile.aesCommodityList[i].smeIndicator == undefined || $scope.aesFile.aesCommodityList[i].smeIndicator == null) {
                    $scope.aesFile.aesCommodityList[i].smeIndicator = false;
                }
                if ($scope.aesFile.aesCommodityList[i].eligiblePartyCertification == undefined || $scope.aesFile.aesCommodityList[i].eligiblePartyCertification == null) {
                    $scope.aesFile.aesCommodityList[i].eligiblePartyCertification = false;
                }
                if ($scope.aesFile.aesCommodityList[i].isUsedVehicle == undefined || $scope.aesFile.aesCommodityList[i].isUsedVehicle == null) {
                    $scope.aesFile.aesCommodityList[i].isUsedVehicle = false;
                }
            }

        }

        $scope.aesFile.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
        $scope.aesFile.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
        $scope.aesFile.location = $rootScope.userProfile.selectedUserLocation;

        $scope.openDefaultTab();
    }


    $scope.openDefaultTab = function() {
        console.log("openDefaultTab in edit page is running...");
        $scope.Tabs = 'moreInfo';
        $timeout(function() {
            $rootScope.navigateToNextField('transPortModeMoreInfo');
        }, 500);
    }

    // Start save

    $scope.saveAes = function() {
        console.log("saveAes Method is called.");

        $scope.spinner = true;
        if ($scope.aesFileMoreInfoValidator(0) && $scope.aesFileUsspiValidator(0) && $scope.aesUltimateConsigneeValidator(0) && $scope.aesIntermediateConsigneeValidator(0) &&

            $scope.aesForwarderValidation(0) && $scope.validateCommodityList()) {
            if ($scope.aesFile.etd != undefined && $scope.aesFile.etd != null && $scope.aesFile.etd != "") {
                $scope.aesFile.etd = $rootScope.sendApiDateAndTime($scope.aesFile.etd);
            }

            AesAdd.save($scope.aesFile).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Aes Saved Successfully")
                    $scope.showResponse(data.responseObject.serviceUid, false);
                    $scope.spinner = false;
                } else {
                    $scope.spinner = false;
                    console.log("Aes Saving Failed ", data.responseDescription);
                }

                $scope.Tabs = 'moreInfo';
            }, function(error) {
                $scope.spinner = false;
                $scope.Tabs = 'moreInfo';
                console.log("Aes Saving Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
            $scope.spinner = false;
        }
    }
    // End save

    // Update start

    $scope.updateAes = function() {

        console.log("Update Method is called.");

        $scope.spinner = true;
        if ($scope.aesFileMoreInfoValidator(0) && $scope.aesFileUsspiValidator(0) && $scope.aesUltimateConsigneeValidator(0) && $scope.aesIntermediateConsigneeValidator(0) &&
            $scope.aesForwarderValidation(0) && $scope.validateCommodityList()) {

            if ($scope.aesFile.etd != undefined && $scope.aesFile.etd != null && $scope.aesFile.etd != "") {
                $scope.aesFile.etd = $rootScope.sendApiDateAndTime($scope.aesFile.etd);
            }

            AesEdit.update($scope.aesFile).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("AesFil	e updated Successfully")
                    aesService.set({});
                    $scope.showResponse(data.responseObject.serviceUid, true);
                    // $scope.spinner = false;
                    /*var params = {};
						params.submitAction = 'Saved';
	                    params.consolId = $stateParams.consolId;
						$scope.showDetail=false;
						$state.go('layout.viewNewAirConsol', params);*/

                } else {
                    console.log("AesFile updated Failed ", data.responseDescription)
                    $scope.spinner = false;
                }

                $scope.Tabs = 'moreInfo';
            }, function(error) {
                $scope.Tabs = 'moreInfo';
                console.log("AesFile updation Failed : ", error);
                $scope.spinner = false;
            });
        } else {
            console.log("Invalid Form Submission.");
            $scope.spinner = false;
        }
    }
    // Update end
    $scope.navigateTabTo = function(navigateTo, tabVal) {
        $scope.Tabs = tabVal;

        if (tabVal == 'statusHistory') {
            console.log("statusHistory");
            AesStatusHistory.get({
                id: $scope.aesFile.id
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Successful while getting aes.", data)
                    $scope.aesFileStatusHistoryList = data.responseObject;
                    console.log($scope.aesFileStatusHistoryList);
                }
            }, function(error) {
                console.log("Error while getting aes.", error)
            });
        }
        $rootScope.navigateToNextField(navigateTo);
    }

    $scope.cancel = function() {
        if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewNewAirConsol') {
            var navAesObj = angular.toJson($stateParams.navAesObj);
            $state.go('layout.viewNewAirConsol', {
                navAesObj: navAesObj,
                fromState: $state.current.name,
                isfromAes: true
            });
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.editAirConsol') {
            var params = {};
            params.consolId = $stateParams.navAesObj.consolId;
            $state.go('layout.editAirConsol', params);
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewCrmShipment') {
            var params = {};
            params.shipmentId = $stateParams.navAesObj.shipmentId;
            $state.go('layout.viewCrmShipment', params);
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.editNewShipment') {
            var params = {};
            params.shipmentId = $stateParams.navAesObj.shipmentId;
            $state.go('layout.editNewShipment', params);
        } else if ($stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.aesList') {
            $state.go('layout.aesList');
        } else if (stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.aesView') {
            $state.go('layout.aesView');
        } else {
            console.log("Dont navigate any where ")
        }
    }
    $scope.backToConsol = function() {

        if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewNewAirConsol') {
            var navAesObj = angular.toJson($stateParams.navAesObj);
            $state.go('layout.viewNewAirConsol', {
                navAesObj: navAesObj,
                fromState: $state.current.name,
                isfromAes: true
            });
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.editAirConsol') {
            var params = {};
            params.consolId = $stateParams.navAesObj.consolId;
            $state.go('layout.editAirConsol', params);
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewCrmShipment') {
            var params = {};
            params.shipmentUid = $stateParams.navAesObj.shipmentUid;
            $state.go('layout.viewCrmShipment', params);
        } else if ($stateParams.navAesObj.fromState != undefined && $stateParams.navAesObj.fromScreen == 'layout.editNewShipment') {
            var params = {};
            params.shipmentId = $stateParams.navAesObj.shipmentId;
            $state.go('layout.editNewShipment', params);
        } else if ($stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.aesList') {
            $state.go('layout.aesList');
        } else if (stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.aesView') {
            $state.go('layout.aesView');
        } else {
            console.log("Dont Navigate anywhere");
        }
    }

    $scope.editmorepopup = function(obj, key) {
        var newScope = $scope.$new();
        if (obj.dtdcVehiclList == undefined || obj.dtdcVehiclList == null) {
            obj.dtdcVehiclList = []
            obj.dtdcVehiclList.push({});
        } else {
            obj.dtdcVehiclList.push({});
        }
        newScope.aesCommodity = obj;
        var myOtherModal = $modal({
            scope: newScope,
            templateUrl: 'app/components/air/consol_shipments_new_screen/views/Commodity_moreinfo_edit_popup.html',
            backdrop: 'static',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
        $rootScope.navigateToNextField(key);
    };

    //Total validationsobj

    $scope.aesFileMoreInfoValidator = function(validateCode) {
        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.aesFile != undefined && $scope.aesFile.serviceUid == undefined || $scope.aesFile.serviceUid == null || $scope.aesFile.serviceUid == "") {
                $scope.errorMap.put('aes.serviceUid', $rootScope.nls["ERR201015"]);
                return false;
            }
        }
        if (validateCode == 0 || validateCode == 2) {

            /*if($stateParams.navAesObj.fromScreen=='layout.viewCrmShipment' ||$stateParams.navAesObj.fromScreen=='layout.editNewShipment'){
            	
            	
            	
            }else{
            	if($scope.aesFile!=undefined &&$scope.aesFile.masterUid==undefined || $scope.aesFile.masterUid==null || $scope.aesFile.masterUid=="" ){
            		$scope.errorMap.put("aes.masterUid", $rootScope.nls["ERR201017"]);
            		return false;
            	}
            }*/
        }
        if (validateCode == 0 || validateCode == 3) {

            if ($scope.aesFile != undefined && $scope.aesFile.hawbNo == undefined || $scope.aesFile.hawbNo == null || $scope.aesFile.hawbNo == "") {
                $scope.errorMap.put("aes.hawbNo", $rootScope.nls["ERR201041"]);
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 4) {

            /*if($scope.aesFile!=undefined &&$scope.aesFile.mawbNo==undefined || $scope.aesFile.mawbNo==null || $scope.aesFile.mawbNo=="" ){
            	$scope.errorMap.put("aes.mawbNo", $rootScope.nls["ERR201042"]);
            	return false;
            }*/

        }
        if (validateCode == 0 || validateCode == 5) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesAction == undefined || $scope.aesFile.aesAction == null || $scope.aesFile.aesAction == "") {
                $scope.errorMap.put("aes.aesaction", $rootScope.nls["ERR201043"]);
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 6) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesFilingType == undefined || $scope.aesFile.aesFilingType == null) {
                $scope.errorMap.put("aes.fillingType", $rootScope.nls["ERR201019"]);
                return false;
            }

        }
        if (validateCode == 0 || validateCode == 7) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesTransportMode == undefined || $scope.aesFile.aesTransportMode == null || $scope.aesFile.aesTransportMode == "") {
                $scope.errorMap.put("aes.transportmode", $rootScope.nls["ERR201020"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesTransportMode != undefined && $scope.aesFile.aesTransportMode) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesTransportMode.status)) {
                        $scope.aesFile.aesTransportMode = null;
                        $scope.errorMap.put("aes.transportmode", $rootScope.nls["ERR201039"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesTransportMode.status)) {
                        $scope.aesFile.aesTransportMode = null;
                        $scope.errorMap.put("aes.transportmode", $rootScope.nls["ERR201040"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {

            if ($scope.aesFile != undefined && $scope.aesFile.originState == undefined || $scope.aesFile.originState == null || $scope.aesFile.originState == "") {
                $scope.errorMap.put("aes.originstate", $rootScope.nls["ERR201024"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.originState != undefined && $scope.aesFile.originState) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.originState.status)) {
                        $scope.aesFile.originState = null;
                        $scope.errorMap.put("aes.originstate", $rootScope.nls["ERR201025"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.originState.status)) {
                        $scope.aesFile.originState = null;
                        $scope.errorMap.put("aes.originstate", $rootScope.nls["ERR201026"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {

            if ($scope.aesFile != undefined && $scope.aesFile.pol == undefined || $scope.aesFile.pol == null || $scope.aesFile.pol == "") {
                $scope.errorMap.put("aes.pol", $rootScope.nls["ERR201036"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.pol != undefined && $scope.aesFile.pol) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.pol.status)) {
                        $scope.aesFile.pol = null;
                        $scope.errorMap.put("aes.pol", $rootScope.nls["ERR201037"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.pol.status)) {
                        $scope.aesFile.pol = null;
                        $scope.errorMap.put("aes.pol", $rootScope.nls["ERR201038"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    /*   if($scope.aesFile.pol.portGroupMaster.countryMaster.countryCode!="US"){
                       	$scope.aesFile.pol = null;
                           $scope.errorMap.put("aes.pol", $rootScope.nls["ERR201125"]);
                           $scope.Tabs='moreInfo';
                           return false;
                       }*/

                }
            }
        }


        if (validateCode == 0 || validateCode == 10) {

            if ($scope.aesFile != undefined && $scope.aesFile.pod == undefined || $scope.aesFile.pod == null || $scope.aesFile.pod == "") {
                $scope.errorMap.put("aes.pod", $rootScope.nls["ERR201033"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.pod != undefined && $scope.aesFile.pod) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.pod.status)) {
                        $scope.aesFile.pod = null;
                        $scope.errorMap.put("aes.pod", $rootScope.nls["ERR201034"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.pod.status)) {
                        $scope.aesFile.pod = null;
                        $scope.errorMap.put("aes.pod", $rootScope.nls["ERR201035"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 11) {

            if ($scope.aesFile != undefined && $scope.aesFile.destinationCountry == undefined || $scope.aesFile.destinationCountry == null || $scope.aesFile.destinationCountry == "") {
                $scope.errorMap.put("aes.destination", $rootScope.nls["ERR201027"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.destinationCountry != undefined && $scope.aesFile.destinationCountry) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.destinationCountry.status)) {
                        $scope.aesFile.destinationCountry = null;
                        $scope.errorMap.put("aes.destination", $rootScope.nls["ERR201028"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.destinationCountry.status)) {
                        $scope.aesFile.destinationCountry = null;
                        $scope.errorMap.put("aes.destination", $rootScope.nls["ERR201029"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 13) {

            if ($scope.aesFile != undefined && $scope.aesFile.iataOrScacCode == undefined || $scope.aesFile.iataOrScacCode == null || $scope.aesFile.iataOrScacCode == "") {
                $scope.errorMap.put("aes.iata", $rootScope.nls["ERR201046"]);
                $scope.Tabs = 'moreInfo';
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 12) {
            if ($scope.aesFile != undefined && $scope.aesFile.etd == undefined || $scope.aesFile.etd == null || $scope.aesFile.etd == "") {
                $scope.errorMap.put("aes.etd", $rootScope.nls["ERR201045"]);
                $scope.Tabs = 'moreInfo';
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 14) {

            if ($scope.aesFile != undefined && $scope.aesFile.carrier == undefined || $scope.aesFile.carrier == null || $scope.aesFile.carrier == "") {
                $scope.errorMap.put("aes.carrier", $rootScope.nls["ERR201030"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.carrier != undefined && $scope.aesFile.carrier) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.carrier.status)) {
                        $scope.aesFile.carrier = null;
                        $scope.errorMap.put("aes.carrier", $rootScope.nls["ERR201031"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.carrier.status)) {
                        $scope.aesFile.carrier = null;
                        $scope.errorMap.put("aes.carrier", $rootScope.nls["ERR201032"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 15) {
            if ($scope.aesFile.routedTransaction == undefined || $scope.aesFile.routedTransaction == null) {
                $scope.errorMap.put("aes.routedTransaction", $rootScope.nls["ERR201021"]);
                $scope.Tabs = 'moreInfo';
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 16) {
            if ($scope.aesFile != undefined && $scope.aesFile.usspiUltimateConsignee == undefined || $scope.aesFile.usspiUltimateConsignee == null || $scope.aesFile.usspiUltimateConsignee === "") {
                $scope.errorMap.put("aes.consigneerelated", $rootScope.nls["ERR201022"]);
                $scope.Tabs = 'moreInfo';
                return false;
            }

        }
        if (validateCode == 0 || validateCode == 17) {
            if ($scope.aesFile != undefined && $scope.aesFile.hazardousCargo == undefined || $scope.aesFile.hazardousCargo == null) {
                $scope.errorMap.put("aes.hazardousCargo", $rootScope.nls["ERR201049"]);
                $scope.Tabs = 'moreInfo';
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 18) {

            if ($scope.aesFile == undefined || $scope.aesFile.inbondType == undefined || $scope.aesFile.inbondType == null || $scope.aesFile.inbondType == "") {
                $scope.errorMap.put("aes.inbondType", $rootScope.nls["ERR201050"]);
                $scope.Tabs = 'moreInfo';
                return false;
            } else {
                if (ValidateUtil.isStatusBlocked($scope.aesFile.inbondType.status)) {
                    $scope.aesFile.inbondType = null;
                    $scope.errorMap.put("aes.inbondType", $rootScope.nls["ERR201051"]);
                    $scope.Tabs = 'moreInfo';
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.inbondType.status)) {
                    $scope.aesFile.inbondType = null;
                    $scope.errorMap.put("aes.inbondType", $rootScope.nls["ERR201052"]);
                    $scope.Tabs = 'moreInfo';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 19) {
            if ($scope.aesFile != undefined && $scope.aesFile.importEntryNo == undefined || $scope.aesFile.importEntryNo == null || $scope.aesFile.importEntryNo == "") {
                /*$scope.errorMap.put("aes.importEntryNo", $rootScope.nls["ERR201049"]);
                $scope.Tabs='moreInfo';*/
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ENTRY_NO, $scope.aesFile.importEntryNo)) {
                    $scope.errorMap.put("aes.importEntryNo", $rootScope.nls["ERR201053"]);
                    $scope.Tabs = 'moreInfo';
                    return false;
                }
            }
        }
        if (validateCode == 0 || validateCode == 20) {
            if ($scope.aesFile != undefined && $scope.aesFile.foreignTradeZone == undefined || $scope.aesFile.foreignTradeZone == null || $scope.aesFile.foreignTradeZone == "") {

            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_FOREIGN_TRADE_ZONE, $scope.aesFile.foreignTradeZone)) {
                    $scope.errorMap.put("aes.foreignTradeZone", $rootScope.nls["ERR201053"]);
                    $scope.Tabs = 'moreInfo';
                    return false;
                }
            }
        }
        if (validateCode == 0 || validateCode == 21) {

            if ($scope.aesFile != undefined && $scope.aesFile.iata == undefined || $scope.aesFile.iata == null || $scope.aesFile.iata == "") {
                $scope.errorMap.put("aes.iata", $rootScope.nls["ERR201046"]);
                $scope.Tabs = 'moreInfo';
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.carrier != undefined && $scope.aesFile.carrier) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.iata.status)) {
                        $scope.aesFile.carrier = null;
                        $scope.errorMap.put("aes.iata", $rootScope.nls["ERR201047"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.iata.status)) {
                        $scope.aesFile.carrier = null;
                        $scope.errorMap.put("aes.iata", $rootScope.nls["ERR201048"]);
                        $scope.Tabs = 'moreInfo';
                        return false;
                    }
                }
            }
        }
        return true;

    } //validation ends


    //TOtal validations end	




    // More Info tab start

    //AesTransportModeList
    $scope.ajaxAesTransportModeEvent = function(object) {
        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return AesTransportModeList.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.aesTransportModeListList = data.responseObject.searchResult;
                    return $scope.aesTransportModeListList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching IataMaster');
            }
        );

    }

    //IataMasterList
    $scope.ajaxIataMasterEvent = function(object) {
        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return IataMasterList.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.iataMasterList = data.responseObject.searchResult;
                    return $scope.iataMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching IataMaster');
            }
        );

    }



    // ScacMasterList
    $scope.ajaxScacMasterEvent = function(object) {
        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ScacMasterList.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.scacMasterList = data.responseObject.searchResult;
                    return $scope.scacMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ScacMaster');
            }
        );

    }


    // InBondTypeMasterList
    $scope.ajaxInBondTypeMasterEvent = function(object) {
        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return InBondTypeMasterList.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.inBondTypeMasterList = data.responseObject.searchResult;
                    return $scope.inBondTypeMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching InBondType');
            }
        );

    }

    $scope.transportModeRender = function(item) {
        return {
            label: item.aesTransportName,
            item: item
        }
    }

    $scope.scacRender = function(item) {
        return {
            label: item.scacName,
            item: item
        }
    }

    $scope.inBondTypeRender = function(item) {
        return {
            label: item.inbondType,
            item: item
        }
    }

    $scope.iataRender = function(item) {
        return {
            label: item.iataCode,
            item: item
        }
    }

    $scope.selectedTransportMode = function(obj) {
        var navigateTo = 'aesAction';
        if ($scope.aesFileMoreInfoValidator(7)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.selectedAction = function(obj) {
        var navigateTo = 'genEDI';
        $rootScope.navigateToNextField(navigateTo);
    }

    $scope.selectInBondType = function(navigateTo) {
        if ($scope.aesFileMoreInfoValidator(18)) {
            $rootScope.navigateToNextField(navigateTo);
        }

    }

    $scope.selectedCustomer = function(agent) {

        //var navigateTo = '';
        //$rootScope.navigateToNextField(navigateTo);
    }

    $scope.selectedIata = function(obj) {

        var navigateTo = 'etdMoreInfo';
        $rootScope.navigateToNextField(navigateTo);
    }

    $scope.selectedState = function(obj, navigateTo) {
        if ($scope.aesFileMoreInfoValidator(8)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }


    $scope.selectedPol = function(obj, navigateTo) {
        if ($scope.aesFileMoreInfoValidator(9)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.selectedPod = function(obj, navigateTo) {
        if ($scope.aesFileMoreInfoValidator(10)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }


    $scope.selectedCountry = function(navigateTo) {
        if ($scope.aesFileMoreInfoValidator(11)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.selectCarrier = function(navigateTo) {
        $scope.aesFile.iataOrScacCode = null;
        if ($scope.aesFileMoreInfoValidator(14)) {
            $rootScope.navigateToNextField(navigateTo);
            if ($scope.aesFile.carrier != null) {
                if ($scope.aesFile.carrier.transportMode == 'Air') {
                    $scope.aesFile.iataOrScacCode = $scope.aesFile.carrier.iataCode
                } else if ($scope.aesFile.carrier.transportMode == 'Ocean') {
                    $scope.aesFile.iataOrScacCode = $scope.aesFile.carrier.scacCode
                }
            }
        }
    }

    $scope.selectedEtd = function(obj) {
        var navigateTo = 'rtMoreInfo';
        $rootScope.navigateToNextField(navigateTo);
    }

    // State master

    $scope.ajaxStateEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return StateListCode
            .fetch({
                "countryCode": "US"
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.stateList = data.responseObject.searchResult;
                        return $scope.stateList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });



    }

    //AES STATE MASTER

    $scope.ajaxAESStateEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return AesStateSearch
            .fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0" && data.responseObject != undefined && data.responseObject != null) {
                        $scope.stateList = data.responseObject.content;
                        return $scope.stateList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });
    }

    //State maste end 


    // Country master
    $scope.ajaxCountryEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.countryList = data.responseObject.searchResult;
                        return $scope.countryList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Country');
                });

    }




    $scope.countryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }

    //Country master end



    // To display the pol
    $scope.ajaxPolEvent = function(object) {
        console.log("Displaying List of Ports....");
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    var resultList = data.responseObject.searchResult;
                    if (resultList != null && resultList.length != 0) {
                        $scope.polList = resultList;
                    }
                    return $scope.polList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );

    }


    $scope.ajaxAESPolEvent = function(object) {
        console.log("Displaying List of Ports....");
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return AesPortSearch.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0" && data.responseObject != undefined && data.responseObject != null) {
                    var resultList = data.responseObject.content;
                    $scope.polList = [];
                    if (resultList != null && resultList.length != 0) {
                        $scope.polList = resultList;
                    }
                    return $scope.polList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching PortOfLoading List');
            }
        );

    }




    // pol end 

    // To display the pod
    /*$scope.ajaxPodEvent = function(object) {
    	console.log("Displaying List of Ports....");
    	console.log("$scope.selectedPolName : "+$scope.selectedPolName);
        $scope.searchDto={};
        $scope.searchDto.keyword=object==null?"":object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({"transportMode":'Air'}, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode =="ERR0"){
                    var resultList = data.responseObject.searchResult;
                    console.log("List of Ports before removing : ", resultList);
                    if(resultList != null && resultList.length != 0) {
                    	
                    	for (var int = 0; int < resultList.length; int++) {
                    		if ($scope.selectedPolName != undefined &&  $scope.selectedPolName != null && $scope.selectedPolName != "") {
                    			if(resultList[int].portName == $scope.selectedPolName){
                    				
                    				var index = int;
                          		  resultList.splice(index, 1); 
                    				
                    			}
                        		  
                        	}
                    			
							}
                    	console.log("List of Ports after removing : ", resultList);
	                            $scope.podList=resultList;
                    }
                    return $scope.podList;
                }
            },
            function(errResponse){
                console.error('Error while fetching PortOfDischarge List');
            }
        );

    }*/

    $scope.ajaxAESPodEvent = function(object) {
        console.log("Displaying List of Ports....");
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return AesPortForeignSearch.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0" && data.responseObject != undefined && data.responseObject != null) {
                    var resultList = data.responseObject.content;
                    $scope.podList = [];
                    if (resultList != null && resultList.length != 0) {
                        $scope.podList = resultList;
                    }
                    return $scope.podList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching PortOfDischarge List');
            }
        );

    }
    //pod end
    // Carrier Start 

    $scope.ajaxCarrierEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CarrierByTransportMode.fetch({
            "transportMode": $scope.transportMode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.connCarrierList = data.responseObject.searchResult;
                    return $scope.connCarrierList;
                    //$scope.showConnCarrierList=true;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air');
            }
        );

    }

    // Carrier end

    $scope.portRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    }
    $scope.carrierRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }
    }

    $scope.stateRender = function(item) {
        return {
            label: item.stateName,
            item: item
        }
    }



    // 




    // More Info tab end


    // Tab USSPI start

    $scope.ajaxUsppiShipperEvent = function(object) {

        if ($scope.aesFile.aesUsppi == undefined || $scope.aesFile.aesUsppi == null || $scope.aesFile.aesUsppi == "") {
            $scope.aesFile.aesUsppi = {};
        }

        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.shipperList = data.responseObject.searchResult;
                        return $scope.shipperList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Party');
                });

    }

    $scope.selectedUsppiShipper = function(obj) {

        var navigateTo = 'idTypeUsppi';
        $rootScope.navigateToNextField(navigateTo);
        $scope.getPartyAddressCoa('SHIPPER', obj);
    }

    $scope.partyUsppiShipperRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }

    $scope.selectedUsppiIdType = function(obj) {
        var navigateTo = 'idNoUsppi';
        $rootScope.navigateToNextField(navigateTo);
    }



    // Rout Party

    $scope.routePartyEvent = function(fromObjModel, isAdd,
        partyMaster) {
        $rootScope.nav_src_bkref_key = new Date().toISOString();
        if (isAdd) {
            $state
                .go(
                    "layout.addParty", {
                        nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                        forObj: fromObjModel
                    });
        } else {
            if (partyMaster != undefined && partyMaster != null &&
                partyMaster.id != undefined &&
                partyMaster.id != null) {
                $rootScope.nav_src_bkref_key = new Date()
                    .toISOString();
                var stateRouteParam = {
                    partyId: partyMaster.id,
                    nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                    forObj: fromObjModel
                };
                $state.go("layout.editParty", stateRouteParam);
            } else {
                console.log("Party not selected...");
            }
        }
    }




    // COA Start

    /* List of Coas select Picker */
    $scope.showCoaList = false;

    $scope.coaListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
            "title": "partyName",
            seperator: false
        }, {
            "title": "partyCode",
            seperator: true
        }]
    }



    $scope.ajaxCoaEvent = function(object) {

        // console.log("ajaxCoaEvent is called", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        // $scope.showCoaList=true;
        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.coaList = data.responseObject.searchResult;
                        // $scope.coaList=true;
                        return $scope.coaList;

                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Countries');
                });

    }

    $scope.partyCoaRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }


    $scope.showCoa = function(partyName) {

        console.log("Showing List of Coa.....");
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.selectedItem = -1;

    };

    $scope.selectedCoa = function(obj) {

        var navigateTo = 'cityMasterUsppi';
        $rootScope.navigateToNextField(navigateTo);

        $scope.getPartyAddressCoa('CUSTOMER', obj);
    };

    $scope.getPartyAddressCoa = function(type, obj) {
        if (($scope.aesFile.aesUsppi.cargoOrigin != null && $scope.aesFile.aesUsppi.cargoOrigin.id != null)) {

            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {

                    $scope.addressCoaMapping('CUSTOMER',
                        obj.partyAddressList[i],
                        obj.countryMaster);
                }
            }
        } else if (type == 'SHIPPER') {
            if (($scope.aesFile.aesUsppi.shipper != null && $scope.aesFile.aesUsppi.shipper.id != null)) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressCoaMapping('SHIPPER',
                            obj.partyAddressList[i],
                            obj.countryMaster);
                    }
                }
            }
        }

    }

    $scope.addressCoaMapping = function(type, partyAddress,
        countryMaster) {

        $scope.aesFile.aesUsppi.cargoOriginAddress = {};

        $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox +
            ", " + partyAddress.addressLine1 :
            partyAddress.addressLine1;

        $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine2 = partyAddress.addressLine2;

        $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine3 = partyAddress.addressLine3;

        var addressLine4 = "";

        if (partyAddress.cityMaster !== null) {
            $scope.aesFile.aesUsppi.cityMaster = partyAddress.cityMaster;
            addressLine4 = addressLine4 +
                partyAddress.cityMaster.cityName;
        }

        if (partyAddress.zipCode != null) {
            $scope.aesFile.aesUsppi.zipCode = partyAddress.zipCode;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + " - ";
            addressLine4 = addressLine4 + partyAddress.zipCode;
        }

        if (partyAddress.stateMaster != null) {
            $scope.aesFile.aesUsppi.stateMaster = partyAddress.stateMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                partyAddress.stateMaster.stateName;

        }

        if (countryMaster != null) {
            $scope.aesFile.aesUsppi.countryMaster = countryMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                countryMaster.countryName;

        }

        $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine4 = addressLine4;

        $scope.aesFile.aesUsppi.cargoOriginAddress.phone = partyAddress.phone;
        $scope.aesFile.aesUsppi.cargoOriginAddress.mobile = partyAddress.mobileNo;
        $scope.aesFile.aesUsppi.cargoOriginAddress.email = partyAddress.email;

        console
            .log(
                "Address List 4 ",
                $scope.aesFile.aesUsppi.shipper.partyAddressList);
    }
    $scope.cancelCoa = function() {
        $scope.showCoaList = false;

    }

    $scope.showPartyAddressShipperList = false;

    $scope.partyAddressShipperlistConfig = {
        search: true,
        showCode: true,
        columns: [{
            "title": "addressType",
            seperator: false
        }, {
            "title": "contactPerson",
            seperator: true
        }]

    }

    $scope.partyAddresslistShipperConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddressShipper = function() {

        console.log("Showing Shipper address list...");

        if ($scope.aesFile.aesUsppi.shipper != null && $scope.aesFile.aesUsppi.shipper != undefined) {
            $scope.panelTitle = "Party Address";
            console
                .log(
                    "Address List ",
                    $scope.aesFile.aesUsppi.shipper.partyAddressList)
            $scope.showPartyAddressShipperList = true;
            $scope.selectedItem = -1;
            $scope.isPickUp = false;
        }
    };

    /*$scope.goToPickup = function() {

    	if ($scope.validateImportExport(1)) {
    		$scope.panelTitle = "Pick up Address";
    		// $scope.partyAddressCustomer = [];
    		$scope.showPartyAddressShipperList = true;
    		$scope.selectedItem = -1;

    		$scope.isPickUp = true;
    	}
    };*/

    $scope.selectedPartyAddressShipper = function(obj) {

        var navigateTo = 'addressLine1';
        $rootScope.navigateToNextField(navigateTo);

        if ($scope.isPickUp) {
            var addressMaster = {};
            addressMaster = addressJoiner.joinAddressLineByLine(
                addressMaster,
                $scope.aesFile.aesUsppi.shipper, obj);
            $scope.ImportExportDetail.pickUpAddress = addressMaster.fullAddress;
        } else {
            $scope
                .addressCoaMapping(
                    'CUSTOMER',
                    obj,
                    $scope.aesFile.aesUsppi.shipper.countryMaster);

        }
        $scope.showPartyAddressShipperList = false;
        $rootScope.navigateToNextField('delivery')
    };

    $scope.cancelPartyAddressShipper = function() {
        $scope.showPartyAddressShipperList = false;
        $rootScope.navigateToNextField('pickup')
    }

    // End

    // COA End

    // Usppi City start

    $scope.ajaxUsppiCityEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesUsppi.shipper == undefined || $scope.aesFile.aesUsppi.shipper == null) {
            return;
        }

        return CityList
            .fetch({
                "countryId": $scope.aesFile.aesUsppi.shipper.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.cityUsppiList = data.responseObject.searchResult;
                        return $scope.cityUsppiList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching City List Based on the Country');
                });

    }

    $scope.usppiCityRender = function(item) {
        return {
            label: item.cityName,
            item: item
        }
    }

    $scope.selectedUsppiCity = function(obj) {

        var navigateTo = 'stateUsppi';
        $rootScope.navigateToNextField(navigateTo);

    }

    // Usppi City end

    // Usppi State start

    $scope.ajaxUsppiStateEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesUsppi.shipper == undefined || $scope.aesFile.aesUsppi.shipper == null) {
            return;
        }

        return StateList
            .fetch({
                "countryId": $scope.aesFile.aesUsppi.shipper.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.usppiStateList = data.responseObject.searchResult;
                        return $scope.usppiStateList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });

    }

    $scope.usppiStateRender = function(item) {
        return {
            label: item.stateName,
            item: item
        }
    }

    $scope.selectedUsppiState = function(obj) {

        var navigateTo = 'countryUsppi';
        $rootScope.navigateToNextField(navigateTo);

    }

    // Usppi State start

    // Usppi Country start

    $scope.ajaxUsppiCountryEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.usppiCountryList = data.responseObject.searchResult;
                        return $scope.usppiCountryList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Country');
                });

    }

    $scope.usppiCountryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }

    $scope.selectedUsppicountry = function(item) {

        var navigateTo = 'zipCodeUsppi';
        $rootScope.navigateToNextField(navigateTo);

    }

    // Usppi Country end

    // Tab USSPI end

    // Tab Ultimate Consignee Start

    $scope.ajaxUCPartyEvent = function(object) {

        if ($scope.aesFile.aesUltimateConsignee == undefined || $scope.aesFile.aesUltimateConsignee == null || $scope.aesFile.aesUltimateConsignee == "") {
            $scope.aesFile.aesUltimateConsignee = {};
        }

        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.uCPartyList = data.responseObject.searchResult;
                        return $scope.uCPartyList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Party');
                });

    }

    $scope.selectedUCParty = function(navigateTo, obj) {
        var navigateTo = 'consigneeTypeUc';
        if ($scope.aesUltimateConsigneeValidator(1)) {
            $scope.getPartyAddressUcCA('CUSTOMER', obj);
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.partyUCPartyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }

    $scope.selectedConsigneeType = function(navigateTo) {
        if ($scope.aesUltimateConsigneeValidator(13)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.selectedConsigneeIdType = function(obj) {
        var navigateTo = 'idNoUc';
        $rootScope.navigateToNextField(navigateTo);
    }

    // Uc City start
    $scope.ajaxUcCityEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesUltimateConsignee.ultimateConsignee == undefined || $scope.aesFile.aesUltimateConsignee.ultimateConsignee == null) {
            return;
        }

        return CityList
            .fetch({
                "countryId": $scope.aesFile.aesUltimateConsignee.ultimateConsignee.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.cityUcList = data.responseObject.searchResult;
                        return $scope.cityUcList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching City List Based on the Country');
                });

    }

    $scope.ucCityRender = function(item) {
        return {
            label: item.cityName,
            item: item
        }
    }

    $scope.selectedUcCity = function(obj, navigateTo) {
        if ($scope.aesUltimateConsigneeValidator(5)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    // Uc City end

    // Uc State start

    $scope.ajaxUcStateEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesUltimateConsignee.ultimateConsignee == undefined || $scope.aesFile.aesUltimateConsignee.ultimateConsignee == null) {
            return;
        }

        return StateList
            .fetch({
                "countryId": $scope.aesFile.aesUltimateConsignee.ultimateConsignee.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.ucStateList = data.responseObject.searchResult;
                        return $scope.ucStateList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });

    }

    $scope.ucStateRender = function(item) {
        return {
            label: item.stateName,
            item: item
        }
    }

    $scope.selectedUcState = function(obj, navigateTo) {

        if ($scope.aesUltimateConsigneeValidator(4)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    // Uc State start

    // Uc Country start

    $scope.ajaxUcCountryEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.ucCountryList = data.responseObject.searchResult;
                        return $scope.ucCountryList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Country');
                });

    }

    $scope.ucCountryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }

    $scope.selectedUccountry = function(item, navigateTo) {
        if ($scope.aesUltimateConsigneeValidator(12)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.selectedICParty = function(navigateTo, obj) {
        if ($scope.aesIntermediateConsigneeValidator(1)) {
            $rootScope.navigateToNextField(navigateTo);
            $scope.getPartyAddressIcCA('CUSTOMER', obj);
        }
    }




    // Uc Country end


    // UCCA Start

    /* List of UcCAs select Picker */
    $scope.showUcCAList = false;

    $scope.ucCAListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
            "title": "partyName",
            seperator: false
        }, {
            "title": "partyCode",
            seperator: true
        }]
    }



    $scope.ajaxUcCAEvent = function(object) {

        // console.log("ajaxUcCAEvent is called", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        // $scope.showUcCAList=true;
        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.ucCAList = data.responseObject.searchResult;
                        // $scope.ucCAList=true;
                        return $scope.ucCAList;

                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Countries');
                });

    }

    $scope.partyUcCARender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }


    $scope.showUcCA = function(partyName) {

        console.log("Showing List of UcCA.....");
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.selectedItem = -1;

    };

    $scope.selectedUcCA = function(obj, navigateTo) {

        if ($scope.aesUltimateConsigneeValidator(2)) {
            $scope.getPartyAddressUcCA('CUSTOMER', obj);
            $rootScope.navigateToNextField(navigateTo);
        }

    };

    $scope.getPartyAddressUcCA = function(type, obj) {
        if (($scope.aesFile.aesUltimateConsignee.ultimateConsignee != null && $scope.aesFile.aesUltimateConsignee.ultimateConsignee.id != null)) {

            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {

                    $scope.addressUcCAMapping('CUSTOMER',
                        obj.partyAddressList[i],
                        obj.countryMaster);
                }
            }
        } else if (type == 'SHIPPER') {
            if (($scope.aesFile.aesUltimateConsignee.ultimateConsignee != null && $scope.aesFile.aesUltimateConsignee.ultimateConsignee.id != null)) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressUcCAMapping('SHIPPER',
                            obj.partyAddressList[i],
                            obj.countryMaster);
                    }
                }
            }
        }

    }

    $scope.addressUcCAMapping = function(type, partyAddress,
        countryMaster) {

        $scope.aesFile.aesUltimateConsignee.companyAddress = {};

        $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox +
            ", " + partyAddress.addressLine1 :
            partyAddress.addressLine1;

        $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2 = partyAddress.addressLine2;

        $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine3 = partyAddress.addressLine3;

        var addressLine4 = "";

        if (partyAddress.cityMaster !== null) {
            $scope.aesFile.aesUltimateConsignee.cityMaster = partyAddress.cityMaster;
            addressLine4 = addressLine4 +
                partyAddress.cityMaster.cityName;
        }

        if (partyAddress.zipCode != null) {
            $scope.aesFile.aesUltimateConsignee.zipCode = partyAddress.zipCode;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + " - ";
            addressLine4 = addressLine4 + partyAddress.zipCode;
        }

        if (partyAddress.stateMaster != null) {
            $scope.aesFile.aesUltimateConsignee.stateMaster = partyAddress.stateMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                partyAddress.stateMaster.stateName;

        }

        if (countryMaster != null) {
            $scope.aesFile.aesUltimateConsignee.countryMaster = countryMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                countryMaster.countryName;

        }

        $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4 = addressLine4;

        $scope.aesFile.aesUltimateConsignee.companyAddress.phone = partyAddress.phone;
        $scope.aesFile.aesUltimateConsignee.companyAddress.mobile = partyAddress.mobileNo;
        $scope.aesFile.aesUltimateConsignee.companyAddress.email = partyAddress.email;

        console
            .log(
                "Address List 4 ",
                $scope.aesFile.aesUltimateConsignee.ultimateConsignee.partyAddressList);
    }
    $scope.cancelUcCA = function() {
        $scope.showUcCAList = false;

    }

    $scope.showPartyAddressUcCAList = false;

    $scope.partyAddressUcCAlistConfig = {
        search: true,
        showCode: true,
        columns: [{
            "title": "addressType",
            seperator: false
        }, {
            "title": "contactPerson",
            seperator: true
        }]

    }

    $scope.partyAddresslistUcCAConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddressUcCA = function() {

        console.log("Showing UcCAs address list...");

        if ($scope.aesFile.aesUltimateConsignee.ultimateConsignee != null && $scope.aesFile.aesUltimateConsignee.ultimateConsignee != undefined) {
            $scope.panelTitle = "Party Address";
            console
                .log(
                    "Address List ",
                    $scope.aesFile.aesUltimateConsignee.ultimateConsignee.partyAddressList)
            // $scope.partyAddressCustomer = [];
            $scope.showPartyAddressUcCAList = true;
            $scope.selectedItem = -1;
            $scope.isPickUp = false;
        }
    };

    /*$scope.goToPickup = function() {

    	if ($scope.validateImportExport(1)) {
    		$scope.panelTitle = "Pick up Address";
    		// $scope.partyAddressCustomer = [];
    		$scope.showPartyAddressUcCAList = true;
    		$scope.selectedItem = -1;

    		$scope.isPickUp = true;
    	}
    };*/

    $scope.selectedPartyAddressUcCA = function(obj) {

        var navigateTo = 'addressLine1';
        $rootScope.navigateToNextField(navigateTo);

        if ($scope.isPickUp) {
            var addressMaster = {};
            addressMaster = addressJoiner.joinAddressLineByLine(
                addressMaster,
                $scope.aesFile.aesUltimateConsignee.ultimateConsignee, obj);
            $scope.ImportExportDetail.pickUpAddress = addressMaster.fullAddress;
        } else {
            $scope
                .addressUcCAMapping(
                    'CUSTOMER',
                    obj,
                    $scope.aesFile.aesUltimateConsignee.ultimateConsignee.countryMaster);

        }
        $scope.showPartyAddressUcCAList = false;
        $rootScope.navigateToNextField('delivery')
    };

    $scope.cancelPartyAddressUcCA = function() {
        $scope.showPartyAddressUcCAList = false;
        $rootScope.navigateToNextField('pickup')
    }

    // End

    // UCCA End


    // Tab Ultimate Consignee end


    // Tab IC start

    $scope.ajaxICPartyEvent = function(object) {

        if ($scope.aesFile.aesIntermediateConsignee == undefined || $scope.aesFile.aesIntermediateConsignee == null || $scope.aesFile.aesIntermediateConsignee == "") {
            $scope.aesFile.aesIntermediateConsignee = {};
        }


        $scope.searchDto = {};

        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.iCPartyList = data.responseObject.searchResult;
                        return $scope.iCPartyList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Party');
                });

    }


    $scope.partyICPartyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }


    // ICCA Start

    /* List of IcCAs select Picker */
    $scope.showIcCAList = false;

    $scope.icCAListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
            "title": "partyName",
            seperator: false
        }, {
            "title": "partyCode",
            seperator: true
        }]
    }



    $scope.ajaxIcCAEvent = function(object) {

        // console.log("ajaxIcCAEvent is called", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        // $scope.showIcCAList=true;
        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.icCAList = data.responseObject.searchResult;
                        // $scope.icCAList=true;
                        return $scope.icCAList;

                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Countries');
                });

    }

    $scope.partyIcCARender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }


    $scope.showIcCA = function(partyName) {

        console.log("Showing List of IcCA.....");
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.selectedItem = -1;

    };

    $scope.selectedIcCA = function(obj, navigateTo) {
        if ($scope.aesIntermediateConsigneeValidator(2)) {
            $rootScope.navigateToNextField(navigateTo);
            $scope.getPartyAddressIcCA('CUSTOMER', obj);
        }
    };

    $scope.getPartyAddressIcCA = function(type, obj) {
        if (($scope.aesFile.aesIntermediateConsignee.intermediateConsignee != null && $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.id != null)) {

            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {

                    $scope.addressIcCAMapping('CUSTOMER',
                        obj.partyAddressList[i],
                        obj.countryMaster);
                }
            }
        } else if (type == 'SHIPPER') {
            if (($scope.aesFile.aesIntermediateConsignee.intermediateConsignee != null && $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.id != null)) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressIcCAMapping('SHIPPER',
                            obj.partyAddressList[i],
                            obj.countryMaster);
                    }
                }
            }
        }

    }

    $scope.addressIcCAMapping = function(type, partyAddress,
        countryMaster) {
        $scope.aesFile.aesIntermediateConsignee.companyAddress = {};

        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox +
            ", " + partyAddress.addressLine1 :
            partyAddress.addressLine1;

        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2 = partyAddress.addressLine2;

        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine3 = partyAddress.addressLine3;

        var addressLine4 = "";

        if (partyAddress.cityMaster !== null) {
            $scope.aesFile.aesIntermediateConsignee.cityMaster = partyAddress.cityMaster;
            addressLine4 = addressLine4 +
                partyAddress.cityMaster.cityName;
        }

        if (partyAddress.zipCode != null) {
            $scope.aesFile.aesIntermediateConsignee.zipCode = partyAddress.zipCode;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + " - ";
            addressLine4 = addressLine4 + partyAddress.zipCode;
        }

        if (partyAddress.stateMaster != null) {
            $scope.aesFile.aesIntermediateConsignee.stateMaster = partyAddress.stateMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                partyAddress.stateMaster.stateName;

        }

        if (countryMaster != null) {
            $scope.aesFile.aesIntermediateConsignee.countryMaster = countryMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                countryMaster.countryName;

        }

        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4 = addressLine4;

        $scope.aesFile.aesIntermediateConsignee.companyAddress.phone = partyAddress.phone;
        $scope.aesFile.aesIntermediateConsignee.companyAddress.mobile = partyAddress.mobileNo;
        $scope.aesFile.aesIntermediateConsignee.companyAddress.email = partyAddress.email;

        console
            .log(
                "Address List 4 ",
                $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.partyAddressList);
    }
    $scope.cancelIcCA = function() {
        $scope.showIcCAList = false;

    }

    $scope.showPartyAddressIcCAList = false;

    $scope.partyAddressIcCAlistConfig = {
        search: true,
        showCode: true,
        columns: [{
            "title": "addressType",
            seperator: false
        }, {
            "title": "contactPerson",
            seperator: true
        }]

    }

    $scope.partyAddresslistIcCAConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddressIcCA = function() {

        console.log("Showing IcCAs address list...");

        if ($scope.aesFile.aesIntermediateConsignee.intermediateConsignee != null && $scope.aesFile.aesIntermediateConsignee.intermediateConsignee != undefined) {
            $scope.panelTitle = "Party Address";
            console
                .log(
                    "Address List ",
                    $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.partyAddressList)
            // $scope.partyAddressCustomer = [];
            $scope.showPartyAddressIcCAList = true;
            $scope.selectedItem = -1;
            $scope.isPickUp = false;
        }
    };

    /*$scope.goToPickup = function() {

    	if ($scope.validateImportExport(1)) {
    		$scope.panelTitle = "Pick up Address";
    		// $scope.partyAddressCustomer = [];
    		$scope.showPartyAddressIcCAList = true;
    		$scope.selectedItem = -1;

    		$scope.isPickUp = true;
    	}
    };*/

    $scope.selectedPartyAddressIcCA = function(obj) {

        var navigateTo = 'addressLine1';
        $rootScope.navigateToNextField(navigateTo);

        if ($scope.isPickUp) {
            var addressMaster = {};
            addressMaster = addressJoiner.joinAddressLineByLine(
                addressMaster,
                $scope.aesFile.aesIntermediateConsignee.intermediateConsignee, obj);
            $scope.ImportExportDetail.pickUpAddress = addressMaster.fullAddress;
        } else {
            $scope
                .addressIcCAMapping(
                    'CUSTOMER',
                    obj,
                    $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.countryMaster);

        }
        $scope.showPartyAddressIcCAList = false;
        $rootScope.navigateToNextField('delivery')
    };

    $scope.cancelPartyAddressIcCA = function() {
        $scope.showPartyAddressIcCAList = false;
        $rootScope.navigateToNextField('pickup')
    }

    // End

    // ICCA End


    // Ic City start

    $scope.ajaxIcCityEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesIntermediateConsignee.intermediateConsignee == undefined || $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == null) {
            return;
        }

        return CityList
            .fetch({
                "countryId": $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.cityIcList = data.responseObject.searchResult;
                        return $scope.cityIcList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching City List Based on the Country');
                });

    }

    $scope.icCityRender = function(item) {
        return {
            label: item.cityName,
            item: item
        }
    }

    $scope.selectedIcCity = function(obj) {
        var navigateTo = 'stateIc';
        if ($scope.aesIntermediateConsigneeValidator(5)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    // Ic City end

    // Ic State start

    $scope.ajaxIcStateEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesIntermediateConsignee.intermediateConsignee == undefined || $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == null) {
            return;
        }

        return StateList
            .fetch({
                "countryId": $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.icStateList = data.responseObject.searchResult;
                        return $scope.icStateList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });

    }

    $scope.icStateRender = function(item) {
        return {
            label: item.stateName,
            item: item
        }
    }

    $scope.selectedIcState = function(obj) {

        var navigateTo = 'countryIc';
        if ($scope.aesIntermediateConsigneeValidator(4)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    // Ic State start

    // Ic Country start

    $scope.ajaxIcCountryEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.icCountryList = data.responseObject.searchResult;
                        return $scope.icCountryList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Country');
                });

    }

    $scope.icCountryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }

    $scope.selectedIcCountry = function(item) {
        var navigateTo = 'zipIc';
        if ($scope.aesIntermediateConsigneeValidator(12)) {
            $rootScope.navigateToNextField(navigateTo);
        }
    }


    // Tab IC End


    // Tab FF Start



    // FFCA Start

    $scope.ajaxFFPartyEvent = function(object) {
        if ($scope.aesFile.aesFreightForwarder == undefined || $scope.aesFile.aesFreightForwarder == null || $scope.aesFile.aesFreightForwarder == "") {
            $scope.aesFile.aesFreightForwarder = {};
        }
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;

        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.fFPartyList = data.responseObject.searchResult;
                        return $scope.fFPartyList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Party');
                });

    }

    $scope.selectedFFParty = function(obj) {
        var navigateTo = 'idTypeFf';
        if ($scope.aesForwarderValidation(1)) {
            $scope.getPartyAddressFfCA('CUSTOMER', obj);
            $rootScope.navigateToNextField(navigateTo);
        }
    }

    $scope.partyFFPartyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }

    $scope.selectedFfIdType = function(obj) {
        var navigateTo = 'idNoFf';
        $rootScope.navigateToNextField(navigateTo);
    }

    /* List of FfCAs select Picker */
    $scope.showFfCAList = false;

    $scope.ffCAListConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
            "title": "partyName",
            seperator: false
        }, {
            "title": "partyCode",
            seperator: true
        }]
    }



    $scope.ajaxFfCAEvent = function(object) {

        // console.log("ajaxFfCAEvent is called", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        // $scope.showFfCAList=true;
        return PartiesList.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.ffCAList = data.responseObject.searchResult;
                        // $scope.ffCAList=true;
                        return $scope.ffCAList;

                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Countries');
                });

    }

    $scope.partyFfCARender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }


    $scope.showFfCA = function(partyName) {

        console.log("Showing List of FfCA.....");
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.selectedItem = -1;

    };

    $scope.selectedFfCA = function(obj) {

        var navigateTo = 'cityFreightForwarder';
        if ($scope.aesForwarderValidation(2)) {
            $scope.getPartyAddressFfCA('CUSTOMER', obj);
            $rootScope.navigateToNextField(navigateTo);
        }
    };

    $scope.getPartyAddressFfCA = function(type, obj) {
        if (($scope.aesFile.aesFreightForwarder.forwarder != null && $scope.aesFile.aesFreightForwarder.forwarder.id != null)) {

            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {

                    $scope.addressFfCAMapping('CUSTOMER',
                        obj.partyAddressList[i],
                        obj.countryMaster);
                }
            }
        } else if (type == 'SHIPPER') {
            if (($scope.aesFile.aesFreightForwarder.forwarder != null && $scope.aesFile.aesFreightForwarder.forwarder.id != null)) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.addressFfCAMapping('SHIPPER',
                            obj.partyAddressList[i],
                            obj.countryMaster);
                    }
                }
            }
        }

    }

    $scope.addressFfCAMapping = function(type, partyAddress,
        countryMaster) {

        $scope.aesFile.aesFreightForwarder.companyAddress = {};

        $scope.aesFile.aesFreightForwarder.companyAddress.addressLine1 = partyAddress.poBox != null ? partyAddress.poBox +
            ", " + partyAddress.addressLine1 :
            partyAddress.addressLine1;

        $scope.aesFile.aesFreightForwarder.companyAddress.addressLine2 = partyAddress.addressLine2;

        $scope.aesFile.aesFreightForwarder.companyAddress.addressLine3 = partyAddress.addressLine3;

        var addressLine4 = "";

        if (partyAddress.cityMaster !== null) {
            $scope.aesFile.aesFreightForwarder.cityMaster = partyAddress.cityMaster;
            addressLine4 = addressLine4 +
                partyAddress.cityMaster.cityName;
        }

        if (partyAddress.zipCode != null) {
            $scope.aesFile.aesFreightForwarder.zipCode = partyAddress.zipCode;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + " - ";
            addressLine4 = addressLine4 + partyAddress.zipCode;
        }

        if (partyAddress.stateMaster != null) {
            $scope.aesFile.aesFreightForwarder.stateMaster = partyAddress.stateMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                partyAddress.stateMaster.stateName;

        }

        if (countryMaster != null) {
            $scope.aesFile.aesFreightForwarder.countryMaster = countryMaster;
            if (addressLine4 != "")
                addressLine4 = addressLine4 + ", ";
            addressLine4 = addressLine4 +
                countryMaster.countryName;

        }

        $scope.aesFile.aesFreightForwarder.companyAddress.addressLine4 = addressLine4;

        $scope.aesFile.aesFreightForwarder.companyAddress.phone = partyAddress.phone;
        $scope.aesFile.aesFreightForwarder.companyAddress.mobile = partyAddress.mobileNo;
        $scope.aesFile.aesFreightForwarder.companyAddress.email = partyAddress.email;

        /*console
        		.log(
        				"Address List 4 ",
        				$scope.aesFile.aesFreightForwarder.forwarder.partyAddressList);*/
    }
    $scope.cancelFfCA = function() {
        $scope.showFfCAList = false;

    }

    $scope.showPartyAddressFfCAList = false;

    $scope.partyAddressFfCAlistConfig = {
        search: true,
        showCode: true,
        columns: [{
            "title": "addressType",
            seperator: false
        }, {
            "title": "contactPerson",
            seperator: true
        }]

    }

    $scope.partyAddresslistFfCAConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddressFfCA = function() {

        console.log("Showing FfCAs address list...");

        if ($scope.aesFile.aesFreightForwarder.forwarder != null && $scope.aesFile.aesFreightForwarder.forwarder != undefined) {
            $scope.panelTitle = "Party Address";
            console
                .log(
                    "Address List ",
                    $scope.aesFile.aesFreightForwarder.forwarder.partyAddressList)
            // $scope.partyAddressCustomer = [];
            $scope.showPartyAddressFfCAList = true;
            $scope.selectedItem = -1;
            $scope.isPickUp = false;
        }
    };

    /*$scope.goToPickup = function() {

    	if ($scope.validateImportExport(1)) {
    		$scope.panelTitle = "Pick up Address";
    		// $scope.partyAddressCustomer = [];
    		$scope.showPartyAddressFfCAList = true;
    		$scope.selectedItem = -1;

    		$scope.isPickUp = true;
    	}
    };*/




    $scope.selectedPartyAddressFfCA = function(obj) {

        var navigateTo = 'addressLine1';
        $rootScope.navigateToNextField(navigateTo);

        if ($scope.isPickUp) {
            var addressMaster = {};
            addressMaster = addressJoiner.joinAddressLineByLine(
                addressMaster,
                $scope.aesFile.aesFreightForwarder.forwarder, obj);
            $scope.ImportExportDetail.pickUpAddress = addressMaster.fullAddress;
        } else {
            $scope
                .addressFfCAMapping(
                    'CUSTOMER',
                    obj,
                    $scope.aesFile.aesFreightForwarder.forwarder.countryMaster);

        }
        $scope.showPartyAddressFfCAList = false;
        $rootScope.navigateToNextField('delivery')
    };

    $scope.cancelPartyAddressFfCA = function() {
        $scope.showPartyAddressFfCAList = false;
        $rootScope.navigateToNextField('pickup')
    }

    // End

    // FFCA End


    // Ff City start

    $scope.ajaxFfCityEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesFreightForwarder.forwarder == undefined || $scope.aesFile.aesFreightForwarder.forwarder == null) {
            return;
        }

        return CityList
            .fetch({
                "countryId": $scope.aesFile.aesFreightForwarder.forwarder.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.cityFfList = data.responseObject.searchResult;
                        return $scope.cityFfList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching City List Based on the Country');
                });

    }

    $scope.ffCityRender = function(item) {
        return {
            label: item.cityName,
            item: item
        }
    }

    $scope.selectedFfCity = function(obj) {

        var navigateTo = 'stateFf';
        $rootScope.navigateToNextField(navigateTo);

    }

    // Ff City end

    // Ff State start

    $scope.ajaxFfStateEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        if ($scope.aesFile.aesFreightForwarder.forwarder == undefined || $scope.aesFile.aesFreightForwarder.forwarder == null) {
            return;
        }

        return StateList
            .fetch({
                "countryId": $scope.aesFile.aesFreightForwarder.forwarder.countryMaster.id
            }, $scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.ffStateList = data.responseObject.searchResult;
                        return $scope.ffStateList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching State List Based on the Country');
                });

    }

    $scope.ffStateRender = function(item) {
        return {
            label: item.stateName,
            item: item
        }
    }

    $scope.selectedFfState = function(obj) {

        if ($scope.aesForwarderValidation(4)) {
            var navigateTo = 'countryFf';
            $rootScope.navigateToNextField(navigateTo);
        }

    }

    // Ff State start

    // Ff Country start

    $scope.ajaxFfCountryEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.ffCountryList = data.responseObject.searchResult;
                        return $scope.ffCountryList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Country');
                });

    }

    $scope.ffCountryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }

    $scope.selectedFfcountry = function(item) {

        if ($scope.aesForwarderValidation(12)) {
            var navigateTo = 'zipFf';
            $rootScope.navigateToNextField(navigateTo);
        }
    }



    // Tab FF End


    $scope.navigateCommodity = function(key) {
        if (key != undefined) {
            $rootScope.navigateToNextField(key);
        }
    }



    //Commodity starts


    $scope.ajaxCommodityLicenseEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return LicenseList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.aesCommodityLicenseList = data.responseObject.searchResult;
                    return $scope.aesCommodityLicenseList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching commodity Services');
            }
        );
    }

    $scope.licenseRender = function(item) {
        return {
            label: item.licenseType,
            item: item
        }
    }


    $scope.ajaxCommodityEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CommodityList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.aesCommodityList = data.responseObject.searchResult;
                    return $scope.aesCommodityList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching commodity Services');
            }
        );
    }

    $scope.commodityRender = function(item) {
        return {
            label: item.hsCode,
            item: item
        }
    }


    $scope.ajaxScheduleEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ScheduleList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.aesScheduleList = data.responseObject.searchResult;
                    return $scope.aesScheduleList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching schedules');
            }
        );
    }

    $scope.scheduleRender = function(item) {
        return {
            label: item.scheduleCode,
            item: item
        }
    }

    $scope.ajaxExportEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ExportList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.exportList = data.responseObject.searchResult;
                    return $scope.exportList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching export codes');
            }
        );
    }

    $scope.exportRender = function(item) {
        return {
            label: item.exportCode,
            item: item
        }
    }

    $scope.ajaxItarEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ItarList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.itarList = data.responseObject.searchResult;
                    return $scope.itarList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching itar codes');
            }
        );
    }

    $scope.itarRender = function(item) {
        return {
            label: item.excemptionNo,
            item: item
        }
    }

    $scope.ajaxCategoryEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CategoryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.categoryList = data.responseObject.searchResult;
                        return $scope.categoryList;
                        // $scope.showCategoryList=true;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Category List');
                });
    }

    $scope.categoryRender = function(item) {
        return {
            label: item.categoryCode,
            item: item
        }
    }

    $scope.ajaxCategoryEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CategoryList.fetch($scope.searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.categoryList = data.responseObject.searchResult;
                        return $scope.categoryList;
                        // $scope.showCategoryList=true;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Category List');
                });
    }

    $scope.categoryRender = function(item) {
        return {
            label: item.categoryCode,
            item: item
        }
    }


    $scope.ajaxUnitEvent = function(object) {
        console.log("Ajax CarrierUnit Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.unitList = data.responseObject.searchResult;
                    console.log("$scope.unitList ", $scope.unitList);
                    return $scope.unitList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching unit');
            });

    };

    $scope.unitRender = function(item) {
        return {
            label: item.unitCode,
            item: item
        }
    };


    $scope.addVehicle = function(obj, forValidation) {
        var fineArr = [];
        $scope.secondArrr = [];
        angular.forEach(obj.dtdcVehicleList, function(dataObj, index) {
            $scope.secondArrr[index] = {};
            var finalData = obj.dtdcVehicleList;
            if (isEmptyRow(dataObj)) {
                validResult = true;
                fineArr.push(0);
                obj.dtdcVehicleList.splice(index, 1);
                delete obj.dtdcVehicleList[index]
            } else {
                var validResult = $scope.validateVehicleObj(dataObj, index, "row");
                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArrr[index].errRow = false;
                } else {
                    $scope.secondArrr[index].errRow = true;
                    fineArr.push(0);
                    $scope.Tabs = 'commodity';
                }
            }
        });
        if (fineArr.indexOf(0) < 0) {
            if (obj.dtdcVehicleList == undefined || obj.dtdcVehicleList == null)
                obj.dtdcVehicleList = [];
            if (!forValidation) {
                obj.dtdcVehicleList.push({});
            }
            $scope.secondArrr.push({
                "errRow": false
            });
        }
    }

    $scope.deleteDtdcVehicle = function(obj, index) {
        obj.dtdcVehicleList
            .splice(index, 1);
    }


    $scope.deleteCommodity = function(obj, index) {
        obj.aesCommodityList
            .splice(index, 1);
    }




    $scope.validateVehicleObj = function(dataObj, index) {

        var errorArr = [];
        $scope.secondArrr[index].errTextArr = [];

        if (dataObj.vinProductId != null && dataObj.vinProductId != "" && dataObj.vinProductId != undefined) {
            var regexp = new RegExp("[0-9A-Za-z ]{0,25}");
            if (!regexp.test(dataObj.vinProductId)) {
                $scope.secondArrr[index].vinProductId = true;
                $scope.secondArrr[index].errTextArr.push($rootScope.nls["ERR201102"]);
                errorArr.push(0);
            } else {
                $scope.secondArrr[index].vinProductId = false;
                errorArr.push(1);
            }
        }

        if (dataObj.vehicleTitleNo != null && dataObj.vehicleTitleNo != "" && dataObj.vehicleTitleNo != undefined) {
            var regexp = new RegExp("[0-9A-Za-z ]{0,15}");
            if (!regexp.test(dataObj.vehicleTitleNo)) {
                $scope.secondArrr[index].vehicleTitleNo = true;
                $scope.secondArrr[index].errTextArr.push($rootScope.nls["ERR201103"]);
                errorArr.push(0);
            } else {
                $scope.secondArrr[index].vehicleTitleNo = false;
                errorArr.push(1);
            }
        }

        if (dataObj.vehicleTitleState != null && dataObj.vehicleTitleState != "" && dataObj.vehicleTitleState != undefined) {

            if (ValidateUtil.isStatusBlocked(dataObj.vehicleTitleState.status)) {
                $scope.secondArrr[index].vehicleTitleState = true;
                $scope.secondArrr[index].errTextArr.push($rootScope.nls["ERR201104"]);
                errorArr.push(0);
            } else {
                $scope.secondArrr[index].vehicleTitleState = false;
                errorArr.push(1);
            }
            if (ValidateUtil.isStatusHidden(dataObj.vehicleTitleState.status)) {
                $scope.secondArrr[index].vehicleTitleState = true;
                $scope.secondArrr[index].errTextArr.push($rootScope.nls["ERR201105"]);
                errorArr.push(0);
            } else {
                $scope.secondArrr[index].vehicleTitleState = false;
                errorArr.push(1);
            }
        }

        if (errorArr.indexOf(0) < 0) {
            return true
        } else {
            return false;
        }

    }


    $scope.setDefaultUnitValues = function(obj) {
        if (obj.aesSchedule != undefined && obj.aesSchedule != null) {
            obj.firstUnit = obj.aesSchedule.firstUnit != undefined ? obj.aesSchedule.firstUnit : '';
            obj.secondUnit = obj.aesSchedule.secondUnit != undefined ? obj.aesSchedule.secondUnit : '';
            obj.commodityMaster = obj.aesSchedule.commodityMaster != undefined ? obj.aesSchedule.commodityMaster : '';
        }
    }
    //Commodity ends

    $scope.showResponse = function(serviceObj, isAdded) {
        var newScope = $scope.$new();
        $scope.spinner = false;
        newScope.errorMessage = isAdded == true ? $rootScope.nls["ERR2012010"] : $rootScope.nls["ERR2012011"];
        if (newScope.errorMessage != undefined) {
            newScope.errorMessage = newScope.errorMessage.replace("%s", serviceObj);
        } else {
            newScope.errorMessage = $rootScope.nls["ERR2012012"];
        }
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p> ' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                '</button>' +
                '</div>',

            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).
        then(function(value) {
            if ($stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewNewAirConsol') {
                var navAesObj = angular.toJson($stateParams.navAesObj);
                $state.go('layout.viewNewAirConsol', {
                    navAesObj: navAesObj,
                    fromState: $state.current.name,
                    isfromAes: true
                });
            } else if ($stateParams.navAesObj != undefined && $stateParams.navAesObj.fromScreen == 'layout.editAirConsol') {
                var params = {};
                params.consolId = $stateParams.navAesObj.consolId;
                $state.go('layout.editAirConsol', params);
            } else if ($stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.viewCrmShipment') {
                var params = {};
                params.shipmentUid = $stateParams.navAesObj.shipmentUid;
                $state.go('layout.viewCrmShipment', params);
            } else if ($stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.editNewShipment') {
                var params = {};
                params.shipmentId = $stateParams.navAesObj.shipmentId;
                $state.go('layout.editNewShipment', params);
            } else if ($stateParams.navAesObj.fromScreen != undefined && $stateParams.navAesObj.fromScreen == 'layout.aesList') {
                $state.go('layout.aesList');
            } else {
                console.log("Dont navigate any where ")
                $state.go('layout.aesList');
            }
        }, function(value) {
            console.log("nothing selected");
        });

    }

    // History Start

    $scope.getAesById = function(aesId) {
        console.log("getAesById ", aesId);
        AesView.get({
            id: aesId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting aes.", data)
                $scope.aesFile = data.responseObject;
                $scope.init();
            }
        }, function(error) {
            console.log("Error while getting aes.", error)
        });

    }


    //validations start

    /**
     * 
     */
    //validation

    $scope.aesFileUsspiValidator = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.shipper == undefined ||
                $scope.aesFile.aesUsppi.shipper == null ||
                $scope.aesFile.aesUsppi.shipper == "" || $scope.aesFile.aesUsppi.shipper.id == undefined) {
                $scope.errorMap.put("aesShipper", $rootScope.nls["ERR37212"]);
                $scope.Tabs = 'usppi';
                return false;
            } else {
                if ($scope.aesFile.aesUsppi.shipper.isDefaulter) {
                    $scope.errorMap.put("aesShipper", $rootScope.nls["ERR201058"]);
                    $scope.aesFile.aesUsppi.shipper = {};
                    $scope.Tabs = 'usppi';
                    return false
                }
                if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.status)) {
                    $scope.errorMap.put("aesShipper", $rootScope.nls["ERR201056"]);
                    $scope.aesFile.aesUsppi.shipper = {};
                    $scope.Tabs = 'usppi';
                    return false
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.status)) {
                    $scope.errorMap.put("aesShipper", $rootScope.nls["ERR201057"]);
                    $scope.aesFile.aesUsppi.shipper = {};
                    $scope.Tabs = 'usppi';
                    $scope.shipmentTab = 'documents';
                    return false

                }
            }
        }




        if (validateCode == 0 || validateCode == 3) {

            if ($scope.aesFile.aesUsppi.cargoOriginAddress == undefined ||
                $scope.aesFile.aesUsppi.cargoOriginAddress == null ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine1 == null ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine1 == "" ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine1 == undefined) {
                $scope.errorMap.put("aesFile.aesUsppi.addressLine1", $rootScope.nls["ERR2012023"]);
                $scope.Tabs = 'usppi';
                return false;
            }
            if ($scope.aesFile.aesUsppi.cargoOriginAddress.addressLine2 == null ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine2 == "" ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine2 == undefined) {
            }

            if ($scope.aesFile.aesUsppi.cargoOriginAddress.addressLine4 == null ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine4 == "" ||
                $scope.aesFile.aesUsppi.cargoOriginAddress.addressLine4 == undefined) {
            }
        }

        if (validateCode == 0 || validateCode == 4) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.stateMaster == undefined || $scope.aesFile.aesUsppi.stateMaster == null || $scope.aesFile.aesUsppi.stateMaster == "") {

                $scope.errorMap.put("aesFile.aesUsppi.state", $rootScope.nls["ERR201062"]);
                $scope.Tabs = 'usppi';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.stateMaster != undefined && $scope.aesFile.aesUsppi.stateMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.stateMaster.status)) {
                        $scope.aesFile.aesUsppi.stateMaster = null;
                        $scope.errorMap.put("aesFile.aesUsppi.state", $rootScope.nls["ERR201135"]);
                        $scope.Tabs = 'usppi';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.stateMaster.status)) {
                        $scope.aesFile.aesUsppi.stateMaster = null;
                        $scope.errorMap.put("aesFile.aesUsppi.state", $rootScope.nls["ERR201136"]);
                        $scope.Tabs = 'usppi';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 5) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.cityMaster == undefined || $scope.aesFile.aesUsppi.cityMaster == null || $scope.aesFile.aesUsppi.cityMaster == "") {
                $scope.errorMap.put("aesFile.aesUsppi.city", $rootScope.nls["ERR201061"]);
                $scope.Tabs = 'usppi';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.cityMaster != undefined && $scope.aesFile.aesUsppi.cityMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.cityMaster.status)) {
                        $scope.aesFile.aesUsppi.cityMaster = null;
                        $scope.errorMap.put("aesFile.aesUsppi.city", $rootScope.nls["ERR201137"]);
                        $scope.Tabs = 'usppi';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.cityMaster.status)) {
                        $scope.aesFile.aesUsppi.cityMaster = null;
                        $scope.errorMap.put("aesFile.aesUsppi.city", $rootScope.nls["ERR201138"]);
                        $scope.Tabs = 'usppi';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 6) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.idType == undefined || $scope.aesFile.aesUsppi.idType == null || $scope.aesFile.aesUsppi.idType == "") {
                $scope.errorMap.put("aesFile.aesUsppi.idType", $rootScope.nls["ERR201064"]);
                $scope.Tabs = 'usppi';
                return false
            }
        }

        if (validateCode == 0 || validateCode == 7) {

            if ($scope.aesFile.aesUsppi.idNo == undefined || $scope.aesFile.aesUsppi.idNo == null || $scope.aesFile.aesUsppi.idNo == "") {
                $scope.errorMap.put("aesFile.aesUsppi.idNo", $rootScope.nls["ERR2012027"]);
                $scope.Tabs = 'usppi';
                return false;
            }


            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.idNo != undefined && $scope.aesFile.aesUsppi.idNo != null && $scope.aesFile.aesUsppi.idNo != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ID_NO, $scope.aesFile.aesUsppi.idNo)) {
                    $scope.errorMap.put("aesFile.aesUsppi.idNo", $rootScope.nls["ERR201126"]);
                    $scope.Tabs = 'usppi';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.firstName != undefined && $scope.aesFile.aesUsppi.firstName != null && $scope.aesFile.aesUsppi.firstName != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesUsppi.firstName)) {
                    $scope.errorMap.put("aesFile.aesUsppi.firstName", $rootScope.nls["ERR201127"]);
                    $scope.Tabs = 'usppi';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi.lastName == undefined || $scope.aesFile.aesUsppi.lastName == null || $scope.aesFile.aesUsppi.lastName == "") {
                $scope.errorMap.put("aesFile.aesUsppi.lastName", $rootScope.nls["ERR2012003"]);
                $scope.Tabs = 'usppi';
                return false;
            }
            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.lastName != undefined && $scope.aesFile.aesUsppi.lastName != null && $scope.aesFile.aesUsppi.lastName != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesUsppi.lastName)) {
                    $scope.errorMap.put("aesFile.aesUsppi.lastName", $rootScope.nls["ERR201128"]);
                    $scope.Tabs = 'usppi';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 10) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.contactNo == undefined || $scope.aesFile.aesUsppi.contactNo == null || $scope.aesFile.aesUsppi.contactNo == "") {
                $scope.errorMap.put("aesFile.aesUsppi.contactNo", $rootScope.nls["ERR201060"]);
                $scope.Tabs = 'usppi';
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_CONTACT_NO, $scope.aesFile.aesUsppi.contactNo)) {
                    $scope.errorMap.put("aesFile.aesUsppi.contactNo", $rootScope.nls["ERR201141"]);
                    $scope.Tabs = 'usppi';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 11) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.zipCode == undefined || $scope.aesFile.aesUsppi.zipCode == null || $scope.aesFile.aesUsppi.zipCode == "") {
                $scope.errorMap.put("aesFile.aesUsppi.zipCode", $rootScope.nls["ERR201063"]);
                $scope.Tabs = 'usppi';
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ZIP_CODE, $scope.aesFile.aesUsppi.zipCode)) {
                    $scope.errorMap.put("aesFile.aesUsppi.zipCode", $rootScope.nls["ERR201142"]);
                    $scope.Tabs = 'usppi';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 12) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.countryMaster == undefined || $scope.aesFile.aesUsppi.countryMaster == null || $scope.aesFile.aesUsppi.countryMaster == "") {
                /*   $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR2011997"]);
				   $scope.Tabs='usppi';
			   return false*/
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesUsppi != undefined && $scope.aesFile.aesUsppi.countryMaster != undefined && $scope.aesFile.aesUsppi.countryMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUsppi.countryMaster.status)) {
                        $scope.aesFile.aesUsppi.countryMaster = null;
                        $scope.errorMap.put("aes.aesUsppi.country", $rootScope.nls["ERR2011998"]);
                        $scope.Tabs = 'usppi';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesUsppi.countryMaster.status)) {
                        $scope.aesFile.aesUsppi.countryMaster = null;
                        $scope.errorMap.put("aes.aesUsppi.country", $rootScope.nls["ERR2011999"]);
                        $scope.Tabs = 'usppi';
                        return false;
                    }
                }
            }
        }

        return true;
    } //usppi validation ends


    $scope.aesUltimateConsigneeValidator = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.ultimateConsignee == undefined ||
                $scope.aesFile.aesUltimateConsignee.ultimateConsignee == null ||
                $scope.aesFile.aesUltimateConsignee.ultimateConsignee == "" || $scope.aesFile.aesUltimateConsignee.ultimateConsignee.id == undefined) {
                $scope.Tabs = 'consignee';
                $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee", $rootScope.nls["ERR201110"]);
                return false;
            } else {
                if ($scope.aesFile.aesUltimateConsignee.ultimateConsignee.isDefaulter) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee", $rootScope.nls["ERR201120"]);
                    $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
                    $scope.Tabs = 'consignee';
                    return false
                }
                if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.status)) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee", $rootScope.nls["ERR201111"]);
                    $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
                    $scope.Tabs = 'consignee';
                    return false
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.status)) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.consignee", $rootScope.nls["ERR201118"]);
                    $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
                    $scope.Tabs = 'consignee';
                    return false

                }
            }
        }



        if (validateCode == 0 || validateCode == 3) {

            if ($scope.aesFile.aesUltimateConsignee.companyAddress == undefined ||
                $scope.aesFile.aesUltimateConsignee.companyAddress == null ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1 == null ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1 == "" ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine1 == undefined) {
                $scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine1", $rootScope.nls["ERR2012023"]);
                $scope.Tabs = 'consignee';
                return false;
            } 
            if ($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2 == null ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2 == "" ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine2 == undefined) {
                
            }


            if ($scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4 == null ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4 == "" ||
                $scope.aesFile.aesUltimateConsignee.companyAddress.addressLine4 == undefined) {
                console.log($rootScope.nls["ERR38010"]);
                $scope.errorMap.put("aesFile.aesUltimateConsignee.addressLine4", $rootScope.nls["ERR2012025"]);
                $scope.Tabs = 'consignee';
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 4) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.stateMaster == undefined || $scope.aesFile.aesUltimateConsignee.stateMaster == null || $scope.aesFile.aesUltimateConsignee.stateMaster == "") {


            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.stateMaster != undefined && $scope.aesFile.aesUltimateConsignee.stateMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.stateMaster.status)) {
                        $scope.aesFile.aesUltimateConsignee.stateMaster = null;
                        $scope.errorMap.put("aesFile.aesUltimateConsignee.state", $rootScope.nls["ERR201163"]);
                        $scope.Tabs = 'consignee';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.stateMaster.status)) {
                        $scope.aesFile.aesUltimateConsignee.stateMaster = null;
                        $scope.errorMap.put("aesFile.aesUltimateConsignee.state", $rootScope.nls["ERR201164"]);
                        $scope.Tabs = 'consignee';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 5) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.cityMaster == undefined || $scope.aesFile.aesUltimateConsignee.cityMaster == null || $scope.aesFile.aesUltimateConsignee.cityMaster == "") {
                $scope.errorMap.put("aesFile.aesUltimateConsignee.city", $rootScope.nls["ERR201123"]);
                $scope.Tabs = 'consignee';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.cityMaster != undefined && $scope.aesFile.aesUltimateConsignee.cityMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.cityMaster.status)) {
                        $scope.aesFile.aesUltimateConsignee.cityMaster = null;
                        $scope.errorMap.put("aesFile.aesUltimateConsignee.city", $rootScope.nls["ERR201165"]);
                        $scope.Tabs = 'consignee';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.cityMaster.status)) {
                        $scope.aesFile.aesUltimateConsignee.cityMaster = null;
                        $scope.errorMap.put("aesFile.aesUltimateConsignee.city", $rootScope.nls["ERR201166"]);
                        $scope.Tabs = 'consignee';
                        return false;

                    }
                }
            }
        }


        if (validateCode == 0 || validateCode == 7) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.idNo != undefined && $scope.aesFile.aesUltimateConsignee.idNo != null && $scope.aesFile.aesUltimateConsignee.idNo != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ID_NO, $scope.aesFile.aesUltimateConsignee.idNo)) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.idNo", $rootScope.nls["ERR201126"]);
                    $scope.Tabs = 'consignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.firstName != undefined && $scope.aesFile.aesUltimateConsignee.firstName != null && $scope.aesFile.aesUltimateConsignee.firstName != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesUltimateConsignee.firstName)) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.firstName", $rootScope.nls["ERR201127"]);
                    $scope.Tabs = 'consignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.firstName != undefined && $scope.aesFile.aesUltimateConsignee.firstName != null && $scope.aesFile.aesUltimateConsignee.firstName != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesUltimateConsignee.lastName)) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.lastName", $rootScope.nls["ERR201128"]);
                    $scope.Tabs = 'consignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 10) {

            
            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_CONTACT_NO, $scope.aesFile.aesUltimateConsignee.contactNo)) {
                $scope.errorMap.put("aesFile.aesUltimateConsignee.contactNo", $rootScope.nls["ERR201169"]);
                $scope.Tabs = 'consignee';
                return false;
            }
        }
        if (validateCode == 0 || validateCode == 11) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && ($scope.aesFile.aesUltimateConsignee.zipCode == undefined || $scope.aesFile.aesUltimateConsignee.zipCode == null || $scope.aesFile.aesUltimateConsignee.zipCode == "")) {


            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ZIP_CODE, $scope.aesFile.aesUltimateConsignee.zipCode)) {
                    $scope.errorMap.put("aesFile.aesUltimateConsignee.zipCode", $rootScope.nls["ERR201170"]);
                    $scope.Tabs = 'consignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 12) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.countryMaster == undefined || $scope.aesFile.aesUltimateConsignee.countryMaster == null || $scope.aesFile.aesUltimateConsignee.countryMaster == "") {
                $scope.errorMap.put("aesFile.aesUltimateConsignee.country", $rootScope.nls["ERR201124"]);
                $scope.Tabs = 'consignee';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.countryMaster != undefined && $scope.aesFile.aesUltimateConsignee.countryMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesUltimateConsignee.countryMaster.status)) {
                        $scope.aesFile.aesUltimateConsignee.countryMaster = null;
                        $scope.errorMap.put("aesFile.aesUltimateConsignee.country", $rootScope.nls["ERR201137"]);
                        $scope.Tabs = 'consignee';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesUltimateConsignee.countryMaster.status)) {
                        $scope.aesFile.aesUltimateConsignee.countryMaster = null;
                        $scope.errorMap.put("aesFile.aesUltimateConsignee.country", $rootScope.nls["ERR201138"]);
                        $scope.Tabs = 'consignee';
                        return false;
                    }
                }
            }
        }
        if (validateCode == 0 || validateCode == 13) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesUltimateConsignee != undefined && $scope.aesFile.aesUltimateConsignee.consigneeType == undefined || $scope.aesFile.aesUltimateConsignee.consigneeType == null || $scope.aesFile.aesUltimateConsignee.consigneeType == "") {
                $scope.errorMap.put("aesFile.aesUltimateConsignee.consigneeType", $rootScope.nls["ERR201176"]);
                $scope.Tabs = 'consignee';
                return false
            }
        }

        return true;
    } //ultimate consignee validation ends




    //intermediatae consignee

    $scope.aesIntermediateConsigneeValidator = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == undefined ||
                $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == null ||
                $scope.aesFile.aesIntermediateConsignee.intermediateConsignee == "" || $scope.aesFile.aesIntermediateConsignee.intermediateConsignee.id == undefined) {
                /* $scope.errorMap.put("aes.aesIntermediateConsignee.consignee",$rootScope.nls["ERR201055"]);
                 return false;*/
                $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = null;
            } else {
                if ($scope.aesFile.aesIntermediateConsignee.intermediateConsignee.isDefaulter) {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.intermediateConsignee", $rootScope.nls["ERR90108"]);
                    $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = {};
                    $scope.Tabs = 'intermediateConsignee';
                    return false
                }
                if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.status)) {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.intermediateConsignee", $rootScope.nls["ERR201118"]);
                    $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = {};
                    $scope.Tabs = 'intermediateConsignee';
                    return false
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.status)) {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.intermediateConsignee", $rootScope.nls["ERR201119"]);
                    $scope.aesFile.aesIntermediateConsignee.intermediateConsignee = {};
                    $scope.Tabs = 'intermediateConsignee';
                    return false

                }
            }
        }




        if (validateCode == 0 || validateCode == 3) {


            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee != null &&
                $scope.aesFile.aesIntermediateConsignee != "" && $scope.aesFile.aesIntermediateConsignee.intermediateConsignee != undefined &&
                $scope.aesFile.aesIntermediateConsignee.intermediateConsignee != null) {

                if ($scope.aesFile.aesIntermediateConsignee.companyAddress == undefined ||
                    $scope.aesFile.aesIntermediateConsignee.companyAddress == null ||
                    $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1 == undefined ||
                    $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1 == null ||
                    $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine1 == "") {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine1", $rootScope.nls["ERR2012023"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                } 
                if ($scope.aesFile.aesIntermediateConsignee.companyAddress != undefined && ($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2 == null ||
                        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2 == "" ||
                        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine2 == undefined)) {
                }


                if ($scope.aesFile.aesIntermediateConsignee.companyAddress != undefined && ($scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4 == null ||
                        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4 == "" ||
                        $scope.aesFile.aesIntermediateConsignee.companyAddress.addressLine4 == undefined)) {

                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.addressLine4", $rootScope.nls["ERR2012025"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                } 
            }
        }

        if (validateCode == 0 || validateCode == 4) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.stateMaster != undefined && $scope.aesFile.aesIntermediateConsignee.stateMaster != null) {

                if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.stateMaster.status)) {
                    $scope.aesFile.aesIntermediateConsignee.stateMaster = null;
                    $scope.errorMap.put("aes.aesIntermediateConsignee.state", $rootScope.nls["ERR201179"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.stateMaster.status)) {
                    $scope.aesFile.aesIntermediateConsignee.stateMaster = null;
                    $scope.errorMap.put("aes.aesIntermediateConsignee.state", $rootScope.nls["ERR201180"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 5) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.cityMaster != undefined && $scope.aesFile.aesIntermediateConsignee.cityMaster != null) {

                if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.cityMaster.status)) {
                    $scope.aesFile.aesIntermediateConsignee.cityMaster = null;
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.city", $rootScope.nls["ERR201181"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.cityMaster.status)) {
                    $scope.aesFile.aesIntermediateConsignee.cityMaster = null;
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.city", $rootScope.nls["ERR201182"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
            }

        }

        if (validateCode == 0 || validateCode == 7) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.idNo != undefined && $scope.aesFile.aesIntermediateConsignee.idNo != null && $scope.aesFile.aesIntermediateConsignee.idNo != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ID_NO, $scope.aesFile.aesIntermediateConsignee.idNo)) {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.idNo", $rootScope.nls["ERR201191"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.firstName != undefined && $scope.aesFile.aesIntermediateConsignee.firstName != null && $scope.aesFile.aesIntermediateConsignee.firstName != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesIntermediateConsignee.firstName)) {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.firstName", $rootScope.nls["ERR201187"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.lastName != undefined && $scope.aesFile.aesIntermediateConsignee.lastName != null && $scope.aesFile.aesIntermediateConsignee.lastName != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesIntermediateConsignee.lastName)) {
                    $scope.errorMap.put("aesFile.aesIntermediateConsignee.lastName", $rootScope.nls["ERR201188"]);
                    $scope.Tabs = 'intermediateConsignee';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 10) {

            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_CONTACT_NO, $scope.aesFile.aesIntermediateConsignee.contactNo)) {
                $scope.errorMap.put("aesFile.aesIntermediateConsignee.contactNo", $rootScope.nls["ERR201189"]);
                $scope.Tabs = 'intermediateConsignee';
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 11) {

            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ZIP_CODE, $scope.aesFile.aesIntermediateConsignee.zipCode)) {
                $scope.errorMap.put("aesFile.aesIntermediateConsignee.zipCode", $rootScope.nls["ERR201190"]);
                $scope.Tabs = 'intermediateConsignee';
                return false;
            }
        }


        if (validateCode == 0 || validateCode == 12) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.countryMaster == undefined || $scope.aesFile.aesIntermediateConsignee.countryMaster == null || $scope.aesFile.aesIntermediateConsignee.countryMaster == "") {
                /*  $scope.errorMap.put("aes.aesusppi.country", $rootScope.nls["ERR201061"]);
				   $scope.Tabs='consignee';
			   return false*/
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesIntermediateConsignee != undefined && $scope.aesFile.aesIntermediateConsignee.countryMaster != undefined && $scope.aesFile.aesIntermediateConsignee.countryMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesIntermediateConsignee.countryMaster.status)) {
                        $scope.aesFile.aesIntermediateConsignee.countryMaster = null;
                        $scope.errorMap.put("aesFile.aesIntermediateConsignee.country", $rootScope.nls["ERR201181"]);
                        $scope.Tabs = 'intermediateConsignee';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesIntermediateConsignee.countryMaster.status)) {
                        $scope.aesFile.aesIntermediateConsignee.countryMaster = null;
                        $scope.errorMap.put("aesFile.aesIntermediateConsignee.country", $rootScope.nls["ERR201182"]);
                        $scope.Tabs = 'intermediateConsignee';
                        return false;
                    }
                }
            }
        }


        return true;
    } //intermediate consignee validation ends


    //freight forwarder validation

    $scope.aesForwarderValidation = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.forwarder == undefined ||
                $scope.aesFile.aesFreightForwarder.forwarder == null ||
                $scope.aesFile.aesFreightForwarder.forwarder == "" || $scope.aesFile.aesFreightForwarder.forwarder.id == undefined) {
                $scope.errorMap.put("aesFile.aesForwarder.forwarder", $rootScope.nls["ERR201106"]);
                $scope.Tabs = 'forwarder';
                return false;
            } else {
                if ($scope.aesFile.aesFreightForwarder.forwarder.isDefaulter) {
                    $scope.errorMap.put("aesFile.aesForwarder.forwarder", $rootScope.nls["ERR201112"]);
                    $scope.aesFile.aesFreightForwarder.forwarder = {};
                    $scope.Tabs = 'forwarder';
                    return false
                }
                if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.status)) {
                    $scope.errorMap.put("aesFile.aesForwarder.forwarder", $rootScope.nls["ERR201107"]);
                    $scope.aesFile.aesFreightForwarder.forwarder = {};
                    $scope.Tabs = 'forwarder';
                    return false
                }
                if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.status)) {
                    $scope.errorMap.put("aesFile.aesForwarder.forwarder", $rootScope.nls["ERR201108"]);
                    $scope.aesFile.aesFreightForwarder.forwarder = {};
                    $scope.Tabs = 'forwarder';
                    return false

                }
            }
        }




        if (validateCode == 0 || validateCode == 3) {

            if ($scope.aesFile.aesFreightForwarder.companyAddress == undefined ||
                $scope.aesFile.aesFreightForwarder.companyAddress == null ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine1 == null ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine1 == "" ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine1 == undefined) {
                $scope.errorMap.put("aes.aesForwarder.addressLine1", $rootScope.nls["ERR2012023"]);
                $scope.Tabs = 'forwarder';
                return false;
            }
            if ($scope.aesFile.aesFreightForwarder.companyAddress.addressLine2 == null ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine2 == "" ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine2 == undefined) {
                
            }


            if ($scope.aesFile.aesFreightForwarder.companyAddress.addressLine4 == null ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine4 == "" ||
                $scope.aesFile.aesFreightForwarder.companyAddress.addressLine4 == undefined) {

                $scope.errorMap.put("aes.aesFreightForwarder.addressLine4", $rootScope.nls["ERR2012025"]);
                $scope.Tabs = 'forwarder';
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 4) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.stateMaster == undefined || $scope.aesFile.aesFreightForwarder.stateMaster == null || $scope.aesFile.aesFreightForwarder.stateMaster == "") {
                $scope.errorMap.put("aesFile.aesForwarder.state", $rootScope.nls["ERR201194"]);
                $scope.Tabs = 'forwarder';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.stateMaster != undefined && $scope.aesFile.aesFreightForwarder.stateMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.stateMaster.status)) {
                        $scope.aesFile.aesFreightForwarder.stateMaster = null;
                        $scope.errorMap.put("aesFile.aesForwarder.state", $rootScope.nls["ERR201195"]);
                        $scope.Tabs = 'forwarder';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.stateMaster.status)) {
                        $scope.aesFile.aesFreightForwarder.stateMaster = null;
                        $scope.errorMap.put("aesFile.aesForwarder.state", $rootScope.nls["ERR201196"]);
                        $scope.Tabs = 'forwarder';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 5) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.cityMaster == undefined || $scope.aesFile.aesFreightForwarder.cityMaster == null || $scope.aesFile.aesFreightForwarder.cityMaster == "") {
                $scope.errorMap.put("aesFile.aesForwarder.city", $rootScope.nls["ERR201114"]);
                $scope.Tabs = 'forwarder';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.cityMaster != undefined && $scope.aesFile.aesFreightForwarder.cityMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.cityMaster.status)) {
                        $scope.aesFile.aesFreightForwarder.cityMaster = null;
                        $scope.errorMap.put("aesFile.aesForwarder.city", $rootScope.nls["ERR201197"]);
                        $scope.Tabs = 'forwarder';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.cityMaster.status)) {
                        $scope.aesFile.aesFreightForwarder.cityMaster = null;
                        $scope.errorMap.put("aesFile.aesForwarder.city", $rootScope.nls["ERR201198"]);
                        $scope.Tabs = 'forwarder';
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 6) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.idType != undefined && $scope.aesFile.aesFreightForwarder.idType != null && $scope.aesFile.aesFreightForwarder.idType != "") {
                //$scope.errorMap.put("aesFile.aesForwarder.idType", $rootScope.nls["ERR201064"]);
                //$scope.Tabs='forwarder';
                // return false
            } else {
                $scope.errorMap.put("aesFile.aesForwarder.idType", $rootScope.nls["ERR201211"]);
                $scope.Tabs = 'forwarder';
                return false
            }
        }

        if (validateCode == 0 || validateCode == 7) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.idNo != undefined && $scope.aesFile.aesFreightForwarder.idNo != null && $scope.aesFile.aesFreightForwarder.idNo != "") {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ID_NO, $scope.aesFile.aesFreightForwarder.idNo)) {
                    $scope.errorMap.put("aesFile.aesForwarder.idNo", $rootScope.nls["ERR2011989"]);
                    $scope.Tabs = 'forwarder';
                    return false;
                }
            } else {
                $scope.errorMap.put("aesFile.aesForwarder.idNo", $rootScope.nls["ERR201212"]);
                $scope.Tabs = 'forwarder';
                return false
            }
        }

        if (validateCode == 0 || validateCode == 8) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.firstName == undefined || $scope.aesFile.aesFreightForwarder.firstName == null || $scope.aesFile.aesFreightForwarder.firstName == "") {
                $scope.errorMap.put("aesFile.aesForwarder.firstName", $rootScope.nls["ERR2011990"]);
                $scope.Tabs = 'forwarder';
                return false;

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesFreightForwarder.firstName)) {
                $scope.errorMap.put("aesFile.aesForwarder.firstName", $rootScope.nls["ERR2012021"]);
                $scope.Tabs = 'forwarder';
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 9) {
            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.lastName == undefined || $scope.aesFile.aesFreightForwarder.lastName == null || $scope.aesFile.aesFreightForwarder.lastName == "") {
                $scope.errorMap.put("aesFile.aesForwarder.lastName", $rootScope.nls["ERR2011991"]);
                $scope.Tabs = 'forwarder';
                return false;
            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_NAME, $scope.aesFile.aesFreightForwarder.lastName)) {
                $scope.errorMap.put("aesFile.aesForwarder.lastName", $rootScope.nls["ERR2012022"]);
                $scope.Tabs = 'forwarder';
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 10) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && ($scope.aesFile.aesFreightForwarder.contactNo == undefined || $scope.aesFile.aesFreightForwarder.contactNo == null || $scope.aesFile.aesFreightForwarder.contactNo == "")) {
                /*$scope.errorMap.put("aesFile.aesForwarder.contactNo",$rootScope.nls["ERR201060"]);
                $scope.Tabs='forwarder';
                return false;*/
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_CONTACT_NO, $scope.aesFile.aesFreightForwarder.contactNo)) {
                    $scope.errorMap.put("aesFile.aesForwarder.contactNo", $rootScope.nls["ERR2011992"]);
                    $scope.Tabs = 'forwarder';
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 11) {
            if ($scope.aesFile == undefined || $scope.aesFile.aesFreightForwarder == undefined || $scope.aesFile.aesFreightForwarder.zipCode == undefined || $scope.aesFile.aesFreightForwarder.zipCode == null || $scope.aesFile.aesFreightForwarder.zipCode == "") {
                $scope.errorMap.put("aesFile.aesForwarder.zipcode", $rootScope.nls["ERR201063"]);
                $scope.Tabs = 'forwarder';
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_ZIP_CODE, $scope.aesFile.aesFreightForwarder.zipCode)) {
                    $scope.errorMap.put("aesFile.aesForwarder.zipcode", $rootScope.nls["ERR2011993"]);
                    $scope.Tabs = 'forwarder';
                    return false;
                }
            }
        }


        if (validateCode == 0 || validateCode == 12) {

            if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.countryMaster == undefined || $scope.aesFile.aesFreightForwarder.countryMaster == null || $scope.aesFile.aesFreightForwarder.countryMaster == "") {
                $scope.errorMap.put("aesFile.aesForwarder.country", $rootScope.nls["ERR201061"]);
                $scope.Tabs = 'forwarder';
                return false
            } else {
                if ($scope.aesFile != undefined && $scope.aesFile.aesFreightForwarder != undefined && $scope.aesFile.aesFreightForwarder.countryMaster != undefined && $scope.aesFile.aesFreightForwarder.countryMaster != null) {

                    if (ValidateUtil.isStatusBlocked($scope.aesFile.aesFreightForwarder.countryMaster.status)) {
                        $scope.aesFile.aesFreightForwarder.countryMaster = null;
                        $scope.errorMap.put("aesFile.aesForwarder.country", $rootScope.nls["ERR2011994"]);
                        $scope.Tabs = 'forwarder';
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.aesFile.aesFreightForwarder.countryMaster.status)) {
                        $scope.aesFile.aesFreightForwarder.countryMaster = null;
                        $scope.errorMap.put("aesFile.aesForwarder.country", $rootScope.nls["ERR2011995"]);
                        $scope.Tabs = 'forwarder';
                        return false;
                    }
                }
            }
        }

        return true;
    } //freight forwarder validation ends


    function isEmptyRow(obj) {
        //return (Object.getOwnPropertyNames(obj).length === 0);
        var isempty = true; //  empty
        if (!obj) {
            return isempty;
        }

        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;

            }
        }
        return isempty;
    }




    //commodity validation
    $scope.validateCommodityList = function() {
        var fineArr = [];
        $scope.secondArr = [];
        var validResult;
        var finalData = [];
        angular.forEach($scope.aesFile.aesCommodityList, function(dataObj, index) {
            $scope.secondArr[index] = {};
            if (isEmptyRow(dataObj)) {
                validResult = true;
                $scope.aesFile.aesCommodityList.splice(index, 1);
                delete $scope.aesFile.aesCommodityList[index]
            } else {
                validResult = $scope.validateCommObj(dataObj, index, "row");
                finalData.push(dataObj);
            }
            if (validResult) {
                fineArr.push(1);
                $scope.secondArr[index].errRow = false;
            } else {
                $scope.secondArr[index].errRow = true;
                fineArr.push(0);
            }
        });
        if (fineArr.indexOf(0) < 0) {
            $scope.tableData = finalData;
            return true;
        } else {
            $scope.Tabs = 'commodity';
            return false;
        }
    }


    $scope.validateCommObj = function(dataObj, index) {
        var errorArr = [];
        $scope.secondArr[index].errTextArr = [];
        if (dataObj.aesAction == null || dataObj.aesAction == "" || dataObj.aesAction == undefined) {
            $scope.secondArr[index].aesAction = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201068"]);
            errorArr.push(0);
        }

        if (dataObj.originOfGoods == null || dataObj.originOfGoods == "" || dataObj.originOfGoods == undefined) {
            $scope.secondArr[index].originOfGoods = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201069"]);
            errorArr.push(0);
        }


        if (dataObj.aesLicense == null || dataObj.aesLicense == "" || dataObj.aesLicense == undefined || dataObj.aesLicense.id == undefined) {
            $scope.secondArr[index].aesLicense = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201070"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.aesLicense.status)) {
                $scope.secondArr[index].aesLicense = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201071"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.aesLicense.status)) {
                $scope.secondArr[index].aesLicense = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201072"]);
                errorArr.push(0);
            }
        }



        if (dataObj.commodityMaster == null || dataObj.commodityMaster == "" || dataObj.commodityMaster == undefined || dataObj.commodityMaster.id == undefined) {
            $scope.secondArr[index].commodityMaster = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201076"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.commodityMaster.status)) {
                $scope.secondArr[index].commodityMaster = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201077"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.commodityMaster.status)) {
                $scope.secondArr[index].commodityMaster = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201078"]);
                errorArr.push(0);
            }
        }



        if (dataObj.aesSchedule == null || dataObj.aesSchedule == "" || dataObj.aesSchedule == undefined || dataObj.aesSchedule.id == undefined) {
            $scope.secondArr[index].aesSchedule = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201073"]);
            errorArr.push(0);
        }


        if (dataObj.firstUnit == null || dataObj.firstUnit == "" || dataObj.firstUnit == undefined || dataObj.firstUnit.id == undefined) {
            $scope.secondArr[index].firstUnit = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201203"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.firstUnit.status)) {
                $scope.secondArr[index].firstUnit = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201204"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.firstUnit.status)) {
                $scope.secondArr[index].firstUnit = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201205"]);
                errorArr.push(0);
            }
        }

        if (dataObj.firstNoOfPieces == null || dataObj.firstNoOfPieces == "" || dataObj.firstNoOfPieces == undefined) {
            $scope.secondArr[index].firstNoOfPieces = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201079"]);
            errorArr.push(0);
        } else {
            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_PIECES, dataObj.firstNoOfPieces)) {
                $scope.secondArr[index].firstNoOfPieces = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201209"]);
                errorArr.push(0);
            }
        }

        if (dataObj.secondUnit == null || dataObj.secondUnit == "" || dataObj.secondUnit == undefined || dataObj.secondUnit.id == undefined) {
            $scope.secondArr[index].secondUnit = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201206"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.secondUnit.status)) {
                $scope.secondArr[index].secondUnit = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201207"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.secondUnit.status)) {
                $scope.secondArr[index].secondUnit = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201208"]);
                errorArr.push(0);
            }
        }

        if (dataObj.secondNoOfPieces == null || dataObj.secondNoOfPieces == "" || dataObj.secondNoOfPieces == undefined) {
            $scope.secondArr[index].secondNoOfPieces = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201080"]);
            errorArr.push(0);
        } else {
            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_PIECES, dataObj.secondNoOfPieces)) {
                $scope.secondArr[index].secondNoOfPieces = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201210"]);
                errorArr.push(0);
            }
        }


        if (dataObj.valueInUsd == null || dataObj.valueInUsd == "" || dataObj.valueInUsd == undefined) {
            $scope.secondArr[index].valueInUsd = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201201"]);
            errorArr.push(0);
        } else {
            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_VALUE_USD, dataObj.valueInUsd)) {
                $scope.secondArr[index].valueInUsd = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201202"]);
                errorArr.push(0);
            }

        }

        if (dataObj.grossWeightInKg == null || dataObj.grossWeightInKg == "" || dataObj.grossWeightInKg == undefined) {
            $scope.secondArr[index].grossWeightInKg = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201081"]);
            errorArr.push(0);
        } else {

            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_AES_GROSS_WEIGHT, dataObj.grossWeightInKg)) {
                $scope.secondArr[index].grossWeightInKg = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22071"]);
                errorArr.push(0);
            }
        }

        if (dataObj.aesExport == null || dataObj.aesExport == "" || dataObj.aesExport == undefined || dataObj.aesExport.id == undefined) {
            $scope.secondArr[index].aesExport = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201094"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.aesExport.status)) {
                $scope.secondArr[index].aesExport = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201095"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.aesExport.status)) {
                $scope.secondArr[index].aesExport = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201096"]);
                errorArr.push(0);
            }
        }

        if (dataObj.licenseValue == null || dataObj.licenseValue == "" || dataObj.licenseValue == undefined) {
            $scope.secondArr[index].licenseValue = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201099"]);
            errorArr.push(0);
        } else {
            var regexp = new RegExp("[0-9]{0,10}");
            if (!regexp.test(dataObj.licenseValue)) {
                $scope.secondArr[index].licenseValue = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201100"]);
                errorArr.push(0);
            }
        }

        if (dataObj.itarExcemption != null && dataObj.itarExcemption != "" && dataObj.itarExcemption != undefined && dataObj.itarExcemption.id != undefined) {

            if (ValidateUtil.isStatusBlocked(dataObj.itarExcemption.status)) {
                $scope.secondArr[index].itarExcemption = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201082"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.itarExcemption.status)) {
                $scope.secondArr[index].itarExcemption = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201083"]);
                errorArr.push(0);
            }
        }


        if (dataObj.usml == null || dataObj.usml == "" || dataObj.usml == undefined || dataObj.usml.id == undefined) {
            $scope.secondArr[index].categoryMaster = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201086"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.usml.status)) {
                $scope.secondArr[index].usml = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201087"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.usml.status)) {
                $scope.secondArr[index].usml = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201088"]);
                errorArr.push(0);
            }
        }


        if (dataObj.dtdc == null || dataObj.dtdc == "" || dataObj.dtdc == undefined || dataObj.dtdc.id == undefined) {
            $scope.secondArr[index].dtdc = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201089"]);
            errorArr.push(0);
        } else {
            if (ValidateUtil.isStatusBlocked(dataObj.dtdc.status)) {
                $scope.secondArr[index].dtdc = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201090"]);
                errorArr.push(0);

            }
            if (ValidateUtil.isStatusHidden(dataObj.dtdc.status)) {
                $scope.secondArr[index].dtdc = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201091"]);
                errorArr.push(0);
            }
        }




        if (dataObj.dtdcQuantity == null || dataObj.dtdcQuantity == "" || dataObj.dtdcQuantity == undefined) {
            $scope.secondArr[index].dtdcQuantity = true;
            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201092"]);
            errorArr.push(0);
        } else {
            var regexp = new RegExp("[0-9]{0,7}");
            if (!regexp.test(dataObj.dtdcQuantity)) {
                $scope.secondArr[index].dtdcQuantity = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR201093"]);
                errorArr.push(0);
            }
        }



        $scope.addVehicle(dataObj, true);
        if (errorArr.indexOf(0) < 0) {
            return true
        } else {
            return false;
        }
    }

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.errorShowforDtdc = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        backdrop: 'static',
        show: false,
        keyboard: true
    });



    $scope.addCommodityRow = function() {
        var fineArr = [];
        $scope.secondArr = [];
        angular.forEach($scope.aesFile.aesCommodityList, function(dataObj, index) {
            //var curObj = $scope.tableData.data[index];
            $scope.secondArr[index] = {};
            if (isEmptyRow(dataObj)) {
                fineArr.push(0);
            } else {
                var validResult = $scope.validateCommObj(dataObj, index, "row");
                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArr[index].errRow = false;
                } else {
                    $scope.secondArr[index].errRow = true;
                    fineArr.push(0);
                }
            }
            //console.log();
        });
        if (fineArr.indexOf(0) < 0) {
            if ($scope.aesFile.aesCommodityList == undefined || $scope.aesFile.aesCommodityList == null)
                $scope.aesFile.aesCommodityList = [];
            $scope.aesCommodity = {};
            $scope.aesCommodity.aesAction = 'Add';
            $scope.aesCommodity.smeIndicator = false;
            $scope.aesCommodity.eligiblePartyCertification = false;
            $scope.aesCommodity.isUsedVehicle = false;
            $scope.aesFile.aesCommodityList.push($scope.aesCommodity);
            $scope.secondArr.push({
                "errRow": false
            });
        }
    };

    //validations end


    // EDI code starts


    $scope.validateAndGenerateEdi = function() {

        console.log("validateAndGenerateEdi is called...", $scope.aesFile.id);

        if ($scope.aesFile == null || $scope.aesFile.id == null) {
            Notification.error($rootScope.nls["ERR201214"]);
            return;
        }

        AesEdiSave.get({
            id: $scope.aesFile.id
        }, function(data) {
            if (data.responseCode == "ERR0") {

            } else if (data.responseCode == "ERR2012036") {
                var responseObject = data.responseObject;
                for (var errIdx = 0; errIdx < responseObject.fieldErrorList.length; errIdx++) {
                    Notification.error(responseObject.fieldErrorList[errIdx].errorMsg);
                }
            }
        }, function(error) {

        });

    }


    $scope.assignIDSuffix = function(value, which, id) {

        if (value != null && value != undefined && value != "" && value.length < 11) {
            $scope.message = $rootScope.nls["ERR201213"];
            ngDialog.openConfirm({
                template: '<p>{{message}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(1)">Ok</button>' +
                    '</div>',
                plain: true,
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function(data) {
                for (var j = value.length + 1; j <= 11; j++) {
                    value = value + 0;
                }
                if ($scope.aesFile != undefined && $scope.aesFile != null) {
                    if (which == 'Usppi') {
                        $scope.aesFile.aesUsppi != undefined ? $scope.aesFile.aesUsppi.idNo = value : "";
                    }

                    if (which == 'UltimateConsignee') {
                        $scope.aesFile.aesUltimateConsignee != undefined ? $scope.aesFile.aesUltimateConsignee.idNo = value : "";
                    }
                    if (which == 'Forwarder') {
                        $scope.aesFile.aesFreightForwarder != undefined ? $scope.aesFile.aesFreightForwarder.idNo = value : "";
                    }
                }
                $rootScope.navigateToNextField(id);
            });
        } else {
            console.log("No id assign");
        }

    }




    //On leave the Unfilled aes Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addAesEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.aesFile;
        $rootScope.aes = "Master";
        $rootScope.unfinishedFormTitle = "Aes (New)";
        if ($scope.aesFile != undefined && $scope.aesFile != null && $scope.aesFile.aesName != undefined &&
            $scope.aesFile.aesName != null && $scope.aesFile.aesName != "") {
            $rootScope.subTitle = $scope.aesFile.aesName;
        } else {
            $rootScope.subTitle = "Unknown Aes"
        }
    })

    $scope.$on('editAesEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.aesFile;
        $rootScope.aes = "Master";
        $rootScope.unfinishedFormTitle = "Aes Edit";
        if ($scope.aesFile != undefined && $scope.aesFile != null && $scope.aesFile.aesName != undefined &&
            $scope.aesFile.aesName != null && $scope.aesFile.aesName != "") {
            $rootScope.subTitle = $scope.aesFile.aesName;
        } else {
            $rootScope.subTitle = "Unknown Aes"
        }
    })

    $scope.$on('addAesEventReload', function(e, confirmation) {
        console.log("addAesEventReload is called ", $scope.aesFile);
        confirmation.message = "";
        localStorage.reloadAesParameters = JSON.stringify($stateParams.navAesObj);
        localStorage.reloadFormData = JSON.stringify($scope.aesFile);
        localStorage.isAesReloaded = "YES";
        e.preventDefault();
    });

    $scope.$on('editAesEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadAesParameters = JSON.stringify($stateParams.navAesObj);
        localStorage.reloadFormData = JSON.stringify($scope.aesFile);
        localStorage.isAesReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isAesReloaded;
    if ($stateParams.action == "ADD") {
        $scope.initDateFormat();
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                $scope.reloadAesFlag = true;
                localStorage.isAesReloaded = "NO";
                $stateParams.navAesObj = angular.fromJson(localStorage.reloadAesParameters);
                $scope.aesFile = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.aesFile = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isAesReloaded = "NO";
                $scope.reloadAesFlag = true;
                console.log($scope.reloadParameters);
                $scope.aesFile = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $stateParams.navAesObj = angular.fromJson(localStorage.reloadAesParameters);
                $scope.init();
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.aesFile = {};
                $scope.aesFile.aesUsppi = {};
                $scope.aesFile.aesUsppi.shipper = {};
                $scope.aesFile.aesUltimateConsignee = {};
                $scope.aesFile.aesUltimateConsignee.soldEnRoute = false;
                $scope.aesFile.aesUltimateConsignee.ultimateConsignee = {};
                $scope.aesFile.aesTransportMode = $rootScope.appMasterData['aes.transportmode.default'];
                if ($stateParams.navAesObj != undefined && $stateParams.navAesObj != null) {
                    $scope.aesFile.serviceUid = $stateParams.navAesObj.serviceUid;
                    $scope.aesFile.masterUid = $stateParams.navAesObj.consolUid;
                    $scope.aesFile.shipmentUid = $stateParams.navAesObj.shipmentUid;
                    $scope.aesFile.shipmentId = $stateParams.navAesObj.shipmentId;
                    $scope.aesFile.hawbNo = $stateParams.navAesObj.hawbNo;
                    $scope.aesFile.mawbNo = $stateParams.navAesObj.mawbNo;
                    $scope.aesFile.etd = $stateParams.navAesObj.etd;
                    $scope.aesFile.carrier = $stateParams.navAesObj.carrier;
                    $scope.aesFile.iataOrScacCode = $stateParams.navAesObj.iataOrScacCode;
                    if ($stateParams.navAesObj != undefined && $stateParams.navAesObj.shipper != undefined && $stateParams.navAesObj.shipper.id != undefined) {
                        $scope.aesFile.aesUsppi.shipper = $stateParams.navAesObj.shipper;
                        $scope.getPartyAddressCoa('SHIPPER', $scope.aesFile.aesUsppi.shipper);
                    }
                    if ($stateParams.navAesObj != undefined && $stateParams.navAesObj.consignee != undefined && $stateParams.navAesObj.consignee.id != undefined) {
                        $scope.aesFile.aesUltimateConsignee.ultimateConsignee = $stateParams.navAesObj.consignee;
                        $scope.getPartyAddressUcCA('CUSTOMER', $scope.aesFile.aesUltimateConsignee.ultimateConsignee);
                    }
                }
                $scope.init();
            }
        }
    } else {
        $scope.initDateFormat();
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                $scope.reloadAesFlag = true;
                localStorage.isAesReloaded = "NO";
                $stateParams.navAesObj = angular.fromJson(localStorage.reloadAesParameters);
                $scope.aesFile = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.aesFile = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isAesReloaded = "NO";
                $scope.reloadAesFlag = true;
                console.log($scope.reloadParameters);
                $stateParams.navAesObj = angular.fromJson(localStorage.reloadAesParameters);
                $scope.aesFile = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.getAesById($stateParams.navAesObj.aesId);
            }
        }
    }
    // History End

}]);