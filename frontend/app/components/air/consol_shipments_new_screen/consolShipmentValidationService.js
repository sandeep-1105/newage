app.service('consolShipmentValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', 'Notification',
                                            function($rootScope , ValidateUtil, CommonValidationService, appConstant, Notification) {
	
	this.validatePickUpDelivery = function(pickUpDeliveryPoint, code) {
		if(pickUpDeliveryPoint){
			if(code == 0 || code == 1) {
				if(pickUpDeliveryPoint!=undefined && pickUpDeliveryPoint.transporter != null && pickUpDeliveryPoint.transporter != undefined && pickUpDeliveryPoint.transporter!= "" ){
				
					if(pickUpDeliveryPoint.transporter.isDefaulter){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.transporter", $rootScope.nls["ERR90593"], "pickup", undefined);
					}
					if(pickUpDeliveryPoint.transporter.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.transporter", $rootScope.nls["ERR90455"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.transporter.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.transporter", $rootScope.nls["ERR90456"], "pickup", undefined);
					}
				}
			}
			
			
			if(code == 0 || code == 2) {
				if(pickUpDeliveryPoint.pickupPoint != null && pickUpDeliveryPoint.pickupPoint != undefined && pickUpDeliveryPoint.pickupPoint!= "" ){
					if(pickUpDeliveryPoint.pickupPoint.isDefaulter){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPoint", $rootScope.nls["ERR90594"], "pickup", undefined);
					}
					if(pickUpDeliveryPoint.pickupPoint.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPoint", $rootScope.nls["ERR90484"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.pickupPoint.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPoint", $rootScope.nls["ERR90485"], "pickup", undefined);
					}
				}
				if(pickUpDeliveryPoint.isOurPickUp==true || pickUpDeliveryPoint.isOurPickUp=='Yes'){
					if(pickUpDeliveryPoint.pickupPoint == null || pickUpDeliveryPoint.pickupPoint == undefined || pickUpDeliveryPoint.pickupPoint== "" ){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPoint",$rootScope.nls["ERR90641"], "pickup", undefined);
					}
				}
			}
			
			
			
			if(code == 0 || code == 3) {
				/*if(pickUpDeliveryPoint.pickupFrom != null && pickUpDeliveryPoint.pickupFrom != undefined && pickUpDeliveryPoint.pickupFrom!= "" ){
					if(pickUpDeliveryPoint.pickupFrom.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupFrom", $rootScope.nls["ERR90487"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.pickupFrom.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupFrom", $rootScope.nls["ERR90488"], "pickup", undefined);
					}
				}*/
				
				/*if(pickUpDeliveryPoint.isOurPickUp==true || pickUpDeliveryPoint.isOurPickUp=='Yes'){
					if(pickUpDeliveryPoint.pickupFrom == null || pickUpDeliveryPoint.pickupFrom == undefined || pickUpDeliveryPoint.pickupFrom== "" ){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupFrom",$rootScope.nls["ERR90640"], "pickup", undefined);
					}
				}*/
			}
			
			if(code == 0 || code == 4) {
				if(pickUpDeliveryPoint.pickUpPlace != null && pickUpDeliveryPoint.pickUpPlace != undefined && pickUpDeliveryPoint.pickUpPlace!= "" ){
					if(pickUpDeliveryPoint.pickUpPlace.state != null && pickUpDeliveryPoint.pickUpPlace.state != undefined && pickUpDeliveryPoint.pickUpPlace.state!= "" ){
						if(pickUpDeliveryPoint.pickUpPlace.state.status=="Block"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPlace.state", $rootScope.nls["ERR1817"], "pickup", undefined);
						}
						else if(pickUpDeliveryPoint.pickUpPlace.state.status=="Hide"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPlace.state", $rootScope.nls["ERR1818"], "pickup", undefined);
						}
					}
					if(pickUpDeliveryPoint.pickUpPlace.city != null && pickUpDeliveryPoint.pickUpPlace.city != undefined && pickUpDeliveryPoint.pickUpPlace.city!= "" ){
						if(pickUpDeliveryPoint.pickUpPlace.city.status=="Block"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPlace.city", $rootScope.nls["ERR90624"], "pickup", undefined);
						}
						else if(pickUpDeliveryPoint.pickUpPlace.city.status=="Hide"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickupPlace.city", $rootScope.nls["ERR90625"], "pickup", undefined);
						}
					}
				}
			}
			
			if(code == 0 || code == 5) {
				if(pickUpDeliveryPoint.pickUpContactPerson != null && pickUpDeliveryPoint.pickUpContactPerson != undefined && pickUpDeliveryPoint.pickUpContactPerson!= "" ){
					if (!ValidateUtil.isContactPerson(pickUpDeliveryPoint.pickUpContactPerson)) {
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickUpContactPerson", $rootScope.nls["ERR90490"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 6) {
				if(pickUpDeliveryPoint.pickUpMobileNo != null && pickUpDeliveryPoint.pickUpMobileNo != undefined && pickUpDeliveryPoint.pickUpMobileNo!= "" ){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,pickUpDeliveryPoint.pickUpMobileNo)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickUpMobileNo", $rootScope.nls["ERR90491"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 7) {
				if(pickUpDeliveryPoint.pickUpPhoneNo != null && pickUpDeliveryPoint.pickUpPhoneNo != undefined && pickUpDeliveryPoint.pickUpPhoneNo!= "" ){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,pickUpDeliveryPoint.pickUpPhoneNo)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickUpPhoneNo", $rootScope.nls["ERR90492"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 8) {
				if(pickUpDeliveryPoint.pickUpEmail != null && pickUpDeliveryPoint.pickUpEmail != undefined && pickUpDeliveryPoint.pickUpEmail!= "" ){
					if(ValidateUtil.isValidMail(pickUpDeliveryPoint.pickUpEmail)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.pickUpEmail", $rootScope.nls["ERR90492"], "pickup", undefined);
					}
				}
			}
			
			
			if(code == 0 || code == 9) {
				if(pickUpDeliveryPoint.deliveryPoint != null && pickUpDeliveryPoint.deliveryPoint != undefined && pickUpDeliveryPoint.deliveryPoint!= "" ){
					if(pickUpDeliveryPoint.deliveryPoint.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPoint", $rootScope.nls["ERR90496"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.deliveryPoint.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPoint", $rootScope.nls["ERR90497"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 10) {
				/*if(pickUpDeliveryPoint.deliveryFrom != null && pickUpDeliveryPoint.deliveryFrom != undefined && pickUpDeliveryPoint.deliveryFrom!= "" ){
					if(pickUpDeliveryPoint.deliveryFrom.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryFrom", $rootScope.nls["ERR90499"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.deliveryFrom.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryFrom", $rootScope.nls["ERR90500"], "pickup", undefined);
					}
				}*/
				
			}
			
			if(code == 0 || code == 11) {
				if(pickUpDeliveryPoint.deliveryPlace != null && pickUpDeliveryPoint.deliveryPlace != undefined && pickUpDeliveryPoint.deliveryPlace!= "" ){
					if(pickUpDeliveryPoint.deliveryPlace.state != null && pickUpDeliveryPoint.deliveryPlace.state != undefined && pickUpDeliveryPoint.deliveryPlace.state!= "" ){
						if(pickUpDeliveryPoint.deliveryPlace.state.status=="Block"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPlace.state", $rootScope.nls["ERR1817"], "pickup", undefined);
						}
						else if(pickUpDeliveryPoint.deliveryPlace.state.status=="Hide"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPlace.state", $rootScope.nls["ERR1818"], "pickup", undefined);
						}
					}
					if(pickUpDeliveryPoint.deliveryPlace.city != null && pickUpDeliveryPoint.deliveryPlace.city != undefined && pickUpDeliveryPoint.deliveryPlace.city!= "" ){
						if(pickUpDeliveryPoint.deliveryPlace.city.status=="Block"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPlace.city", $rootScope.nls["ERR90624"], "pickup", undefined);
						}
						else if(pickUpDeliveryPoint.deliveryPlace.city.status=="Hide"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPlace.city", $rootScope.nls["ERR90625"], "pickup", undefined);
						}
					}
				}
			}
			
			if(code == 0 || code == 12) {
				if(pickUpDeliveryPoint.deliveryContactPerson != null && pickUpDeliveryPoint.deliveryContactPerson != undefined && pickUpDeliveryPoint.deliveryContactPerson!= "" ){
					if (!ValidateUtil.isContactPerson(pickUpDeliveryPoint.deliveryContactPerson)) {
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryContactPerson", $rootScope.nls["ERR90502"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 13) {
				if(pickUpDeliveryPoint.deliveryMobileNo != null && pickUpDeliveryPoint.deliveryMobileNo != undefined && pickUpDeliveryPoint.deliveryMobileNo!= "" ){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,pickUpDeliveryPoint.deliveryMobileNo)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryMobileNo", $rootScope.nls["ERR90503"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 14) {
				if(pickUpDeliveryPoint.deliveryPhoneNo != null && pickUpDeliveryPoint.deliveryPhoneNo != undefined && pickUpDeliveryPoint.deliveryPhoneNo!= "" ){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,pickUpDeliveryPoint.deliveryPhoneNo)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryPhoneNo", $rootScope.nls["ERR90504"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 15) {
				if(pickUpDeliveryPoint.deliveryEmail != null && pickUpDeliveryPoint.deliveryEmail != undefined && pickUpDeliveryPoint.deliveryEmail!= "" ){
					if(ValidateUtil.isValidMail(pickUpDeliveryPoint.deliveryEmail)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.deliveryEmail", $rootScope.nls["ERR90506"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 16) {
				if(pickUpDeliveryPoint.doorDeliveryPoint != null && pickUpDeliveryPoint.doorDeliveryPoint != undefined && pickUpDeliveryPoint.doorDeliveryPoint!= "" ){
					if(pickUpDeliveryPoint.doorDeliveryPoint.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPoint", $rootScope.nls["ERR90508"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.doorDeliveryPoint.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPoint", $rootScope.nls["ERR90509"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 17) {
				/*if(pickUpDeliveryPoint.doorDeliveryFrom != null && pickUpDeliveryPoint.doorDeliveryFrom != undefined && pickUpDeliveryPoint.doorDeliveryFrom!= "" ){
					if(pickUpDeliveryPoint.doorDeliveryFrom.status=="Block"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryFrom", $rootScope.nls["ERR90511"], "pickup", undefined);
					}
					else if(pickUpDeliveryPoint.doorDeliveryFrom.status=="Hide"){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryFrom", $rootScope.nls["ERR90512"], "pickup", undefined);
					}
				}*/
			}
			
			if(code == 0 || code == 18) {
				if(pickUpDeliveryPoint.doorDeliveryPlace != null && pickUpDeliveryPoint.doorDeliveryPlace != undefined && pickUpDeliveryPoint.doorDeliveryPlace!= "" ){
					if(pickUpDeliveryPoint.doorDeliveryPlace.state != null && pickUpDeliveryPoint.doorDeliveryPlace.state != undefined && pickUpDeliveryPoint.doorDeliveryPlace.state!= "" ){
						if(pickUpDeliveryPoint.doorDeliveryPlace.state.status=="Block"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPlace.state", $rootScope.nls["ERR1817"], "pickup", undefined);
						}
						else if(pickUpDeliveryPoint.doorDeliveryPlace.state.status=="Hide"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPlace.state", $rootScope.nls["ERR1818"], "pickup", undefined);
						}
					}
					if(pickUpDeliveryPoint.doorDeliveryPlace.city != null && pickUpDeliveryPoint.doorDeliveryPlace.city != undefined && pickUpDeliveryPoint.doorDeliveryPlace.city!= "" ){
						if(pickUpDeliveryPoint.doorDeliveryPlace.city.status=="Block"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPlace.city", $rootScope.nls["ERR90624"], "pickup", undefined);
						}
						else if(pickUpDeliveryPoint.doorDeliveryPlace.city.status=="Hide"){
							return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPlace.city", $rootScope.nls["ERR90625"], "pickup", undefined);
						}
					}
				}
			}
			
			if(code == 0 || code == 19) {
				if(pickUpDeliveryPoint.doorDeliveryContactPerson != null && pickUpDeliveryPoint.doorDeliveryContactPerson != undefined && pickUpDeliveryPoint.doorDeliveryContactPerson!= "" ){
					if (!ValidateUtil.isContactPerson(pickUpDeliveryPoint.doorDeliveryContactPerson)) {
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryContactPerson", $rootScope.nls["ERR90514"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 20) {
				if(pickUpDeliveryPoint.doorDeliveryMobileNo != null && pickUpDeliveryPoint.doorDeliveryMobileNo != undefined && pickUpDeliveryPoint.doorDeliveryMobileNo!= "" ){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,pickUpDeliveryPoint.doorDeliveryMobileNo)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryMobileNo", $rootScope.nls["ERR90515"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 21) {
				if(pickUpDeliveryPoint.doorDeliveryPhoneNo != null && pickUpDeliveryPoint.doorDeliveryPhoneNo != undefined && pickUpDeliveryPoint.doorDeliveryPhoneNo!= "" ){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,pickUpDeliveryPoint.doorDeliveryPhoneNo)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryPhoneNo", $rootScope.nls["ERR90516"], "pickup", undefined);
					}
				}
			}
			
			if(code == 0 || code == 22) {
				if(pickUpDeliveryPoint.doorDeliveryEmail != null && pickUpDeliveryPoint.doorDeliveryEmail != undefined && pickUpDeliveryPoint.doorDeliveryEmail!= "" ){
					if(ValidateUtil.isValidMail(pickUpDeliveryPoint.doorDeliveryEmail)){
						return this.validationResponse(true, "consol.pickUpDeliveryPoint.doorDeliveryEmail", $rootScope.nls["ERR90518"], "pickup", undefined);
					}
				}
			}
		
		}
	
	return this.validationSuccesResponse();
}
	
	this.validationSuccesResponse = function() {
	    return {error : false};
	}
	
	this.validationResponse = function(err, elem, message, tabName, accordian) {
		return {error : err, errElement : elem, errMessage : message, tab : tabName, accordian: accordian};
	}
	
}]);