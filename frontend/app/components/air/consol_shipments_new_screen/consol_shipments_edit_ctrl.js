/**
 * Created by hmspl on 12/4/16.
 */
app.controller('consol_shipmentEditCtrl', ['$rootScope', '$scope', '$location', '$modal', 'ngDialog', '$http', 'ConsolAdd',
    'consolService', '$timeout', 'PackGetByCode', 'SalesmanList', 'ShipmentGetFromUID', 'PortView', 'Notification',
    'ConsolEdit', 'discardService', 'PartiesList', 'ngProgressFactory', 'PortByTransportMode', 'ValidateUtil',
    'CurrencySearchExclude', 'ServiceList', 'PackList', 'PartiesListByType', 'PortFromCFS',
    'CarrierByTransportMode', 'NavigationService', 'cloneService', 'PartyMasterService', 'DocumentMasterList',
    'TriggerTypeSearchKeyword', 'TriggerSearchKeyword', 'EventSearchKeyword', 'ConsolShipmentLink',
    'ConsolInvoiceList', 'invoiceService', 'ChargeSearchKeyword', 'ProvisionalService',
    'RecentHistorySaveService', 'AesGetAll', 'ConsolGetByUid', 'ConsolPartyClearService',
    '$stateParams', '$state', '$window', 'ConsolGet', 'appMetaFactory', 'CommentFactory', 'EmployeeList',
    'ProvisionalViewByConsolUid', 'GoToUnfilledFormNavigationService', 'GetNextMawb', 'ShipmentUid',
    'AesSearch', 'consolShipmentDocumentIdList', 'downloadFactory', 'downloadMultipleFactory', 'ShipmentIdBasedOnUid',
    'appConstant', 'AutoImportProcess', 'FlightPlanSearchForShipment',
    'addressJoiner', 'AesAddAll', 'ProvisionalViewByShipmentServiceUid', 'doPrintALL', 'AirlineEDISave',
    'AirlineEDIGet', 'ShipmentServiceInvoiceList', 'AirExportService', 'EDIResponse', 'roleConstant',
    'AutoCompleteService', 'ServiceSignOffStatus', 'ConsolSignOffStatus', 'consolShipmentAccountsService',
    'consolShipmentDataService', 'consolShipmentValidationService', 'previewReportService', 'AesEdiSave', 'CommonValidationService',
    'uiShipmentFactory', 'uiShipmentDataService', 'reportValidation', 'modifyGenerateEdiServiceRequest',

    function($rootScope, $scope, $location, $modal, ngDialog, $http, ConsolAdd,
        consolService, $timeout, PackGetByCode, SalesmanList, ShipmentGetFromUID, PortView, Notification,
        ConsolEdit, discardService, PartiesList, ngProgressFactory, PortByTransportMode, ValidateUtil,
        CurrencySearchExclude, ServiceList, PackList, PartiesListByType, PortFromCFS,
        CarrierByTransportMode, NavigationService, cloneService, PartyMasterService, DocumentMasterList,
        TriggerTypeSearchKeyword, TriggerSearchKeyword, EventSearchKeyword, ConsolShipmentLink,
        ConsolInvoiceList, invoiceService, ChargeSearchKeyword, ProvisionalService,
        RecentHistorySaveService, AesGetAll, ConsolGetByUid, ConsolPartyClearService,
        $stateParams, $state, $window, ConsolGet, appMetaFactory, CommentFactory, EmployeeList,
        ProvisionalViewByConsolUid, GoToUnfilledFormNavigationService, GetNextMawb, ShipmentUid,
        AesSearch, consolShipmentDocumentIdList, downloadFactory, downloadMultipleFactory, ShipmentIdBasedOnUid,
        appConstant, AutoImportProcess, FlightPlanSearchForShipment,
        addressJoiner, AesAddAll, ProvisionalViewByShipmentServiceUid, doPrintALL, AirlineEDISave,
        AirlineEDIGet, ShipmentServiceInvoiceList, AirExportService, EDIResponse, roleConstant,
        AutoCompleteService, ServiceSignOffStatus, ConsolSignOffStatus, consolShipmentAccountsService,
        consolShipmentDataService, consolShipmentValidationService, previewReportService, AesEdiSave, CommonValidationService,
        uiShipmentFactory, uiShipmentDataService, reportValidation, modifyGenerateEdiServiceRequest) {
        $scope.firstFocus = true;
        $scope.$roleConstant = roleConstant;
        $scope.popupValues = {};

        $scope.$consolShipmentAccountsService = consolShipmentAccountsService;
        $scope.$consolShipmentValidationService = consolShipmentValidationService;
        $scope.$consolShipmentDataService = consolShipmentDataService;
        $scope.$uiShipmentDataService = uiShipmentDataService;
        $scope.myClass = "accent-btn";

        $scope.tableRowExpanded = false;

        $scope.tableRowIndexCurrExpanded = "";
        $scope.tableRowIndexPrevExpanded = "";
        $scope.storeIdExpanded = "";
        $scope.dayDataCollapse = [true, true, true, true, true, true];
        $scope.$AutoCompleteService = AutoCompleteService;

        $scope.initDateFormat = function() {
            $scope.directiveDateTimePickerOpts = {
                format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                useCurrent: false,
                sideBySide: true,
                viewDate: moment()
            };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";

            $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";

            $scope.dateTimePickerOptionsLeft = {
                format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                useCurrent: false,
                sideBySide: true,
                viewDate: moment()
            };
            $scope.dateTimePickerOptionsLeft.widgetParent = "body";
            $scope.dateTimePickerOptionsLeft.widgetPositioning = {
                horizontal: 'right'
            };
        }

        $scope.selectTableRow = function(index, storeId) {
            //alert("clicked");
            if ($scope.myClass === "accent-btn")
                $scope.myClass = "btn-green";
            else
                $scope.myClass = "accent-btn";

            if ($scope.tableRowExpanded === false && $scope.tableRowIndexCurrExpanded === "" && $scope.storeIdExpanded === "") {
                $scope.tableRowIndexPrevExpanded = "";
                $scope.tableRowExpanded = true;
                $scope.tableRowIndexCurrExpanded = index;
                $scope.storeIdExpanded = storeId.id;
                $scope.dayDataCollapse[index] = false;
            } else if ($scope.tableRowExpanded === true) {
                if ($scope.tableRowIndexCurrExpanded === index && $scope.storeIdExpanded === storeId.id) {
                    $scope.tableRowExpanded = false;
                    $scope.tableRowIndexCurrExpanded = "";
                    $scope.storeIdExpanded = "";
                    $scope.dayDataCollapse[index] = true;
                } else {
                    $scope.tableRowIndexPrevExpanded = $scope.tableRowIndexCurrExpanded;
                    $scope.tableRowIndexCurrExpanded = index;
                    $scope.storeIdExpanded = storeId.id;
                    $scope.dayDataCollapse[$scope.tableRowIndexPrevExpanded] = true;
                    $scope.dayDataCollapse[$scope.tableRowIndexCurrExpanded] = false;
                }
            }
        }

        /*Aes generate all check box code started here*/

        $scope.allAesModel = {};
        $scope.allAesModel.allAesSelected = false;

        $scope.selectAllAes = function() {

            for (var i = 0; i < $scope.finalArr.length; i++) {
                $scope.finalArr[i].isSelected = $scope.allAesModel.allAesSelected;
            }
        }
        $scope.chkAesStatus = function(chkFlag, detObj) {
            console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
        }

        $scope.generateAllAes = function() {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_AES_GENERATE_CREATE)) {
                var isError = false;
                var errorObj = [];
                var count = 0;
                var aesArr = [];
                for (var i = 0; i < $scope.finalArr.length; i++) {
                    if ($scope.finalArr[i].isSelected) {
                        count = count + 1;
                        aesArr.push($scope.finalArr[i]);
                    }
                }
                if (count > 1) {
                    console.log("generate more than one,no need to go aes add page");
                    for (var ai = 0; ai < aesArr.length; ai++) {
                        if (aesArr[ai].id == undefined || aesArr[ai].id == null) {
                            isError = true;
                            errorObj.push(aesArr[ai].serviceUid);
                        }
                    }
                    if (isError) {
                        for (var aErr = 0; aErr < errorObj.length; aErr++) {
                            var errMsg = $rootScope.nls["ERR2012028"];
                            errMsg = errMsg.replace("%s", errorObj[aErr]);
                            Notification.error(errMsg);
                        }
                    } else {
                        for (var ai = 0; ai < aesArr.length; ai++) {
                            $scope.validateAndGenerateEdi(aesArr[ai], ai + 1);
                        }
                    }
                } else if (count == 1) {
                    if (aesArr[0].id == undefined || aesArr[0].id == null) {
                        var errMsg = $rootScope.nls["ERR2012028"];
                        errMsg = errMsg.replace("%s", aesArr[0].serviceUid);
                        Notification.error(errMsg);
                        return;
                    } else {
                        $scope.validateAndGenerateEdi(aesArr[0], 0);
                    }
                } else {
                    Notification.error($rootScope.nls["ERR2012029"]);
                }
            }
        }



        $scope.validateAndGenerateEdi = function(aesFile, index) {
            console.log("validateAndGenerateEdi is called...", $scope.aesFile.id);
            var errMsg = $rootScope.nls["ERR2012030"];

            AesEdiSave.get({
                id: aesFile.id
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    Notification.success("Success Generation for AES-" + index);
                } else if (data.responseCode == "ERR2012036") {
                    var responseObject = data.responseObject;
                    for (var errIdx = 0; errIdx < responseObject.fieldErrorList.length; errIdx++) {
                        Notification.error(responseObject.fieldErrorList[errIdx].errorMsg + "in service -" + aesFile.serviceUid);
                    }
                }
            }, function(error) {
                errMsg = errMsg.replace("%s", aesFile.serviceUid);
                Notification.error(errMsg);
            });
        }




        $scope.showGenerateBtn = function() {

            if ($scope.finalArr == null || $scope.finalArr == undefined) {
                return false;
            }
            return true;
        }

        $scope.aesGenerateAll = function(aesArr) {
                console.log("Aes files need to genarate : ", aesArr);
                AesAddAll.save(aesArr).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Aes Saved Successfully");
                        $scope.spinner = false;
                    } else {
                        $scope.spinner = false;
                        console.log("Aes Saving Failed ", data.responseDescription);
                    }
                }, function(error) {
                    $scope.spinner = false;
                    console.log("Aes Saving Failed : ", error)
                });
            }
            /*Aes generate all check box code ended here*/

        //consol flight schedule code starts
        var flightScheduleModal;
        $scope.flightmodel = function() {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DETAILS_FLIGHT_SCHEDULE_LIST)) {
                $scope.flightSchedule = {};
                $scope.searchDto = {};
                $scope.searchDto.orderByType = 'asc';
                $scope.searchDto.sortByColumn = 'etd';
                if ($scope.consol.carrier != null && $scope.consol.carrier != undefined &&
                    $scope.consol.carrier.id != null) {
                    $scope.searchDto.carrier = $scope.consol.carrier.carrierName;
                }
                $scope.flightScheduleSearchResult = [];
                $scope.selectedFlightScheduleData = null;
                $scope.flightScheduleLimitArray = [10, 15, 20];
                $scope.flightSchedulePage = 0;
                $scope.flightScheduleLimit = 10;
                $scope.flightScheduleTotalRecord = 0;
                $scope.flightScheduleSearch();
                flightScheduleModal = $modal({
                    scope: $scope,
                    backdrop: 'static',
                    templateUrl: 'app/components/air/consol_shipments/consol_flight_schedule_popup.html',
                    show: false
                });
                $scope.navigateToNextField("flightSchedule.service");
                flightScheduleModal.$promise.then(flightScheduleModal.show)

            }
        };



        $scope.downloadAttach = function(param) {
            console.log("download ", param);

            if (param.data.id != null && (param.data.file == null)) {
                console.log("API CALL")
                console.log("download ", param);
                if (param.data.id != null && param.data.file == null) {
                    console.log("API CALL")
                    $http({
                        url: $rootScope.baseURL + '/api/v1/consol/files/' + param.data.id + '/' + param.type + '/false',
                        method: "GET",
                        responseType: 'arraybuffer'
                    }).success(function(data, status, headers, config) {
                        console.log("hiddenElement ", data)
                        var blob = new Blob([data], {});
                        console.log("blob ", blob);
                        saveAs(blob, param.data.fileName);
                    }).error(function(data, status, headers, config) {
                        //upload failed
                    });
                }
            } else {
                console.log("NO API CALL", param);
                saveAs(param.data.tmpFile, param.data.fileName);
            }
        }



        $scope.datepickeropts = {
            clearLabel: 'Clear',
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                //format: $rootScope.userProfile.selectedUserLocation.dateFormat,
                format: $rootScope.userProfile.selectedUserLocation.dateFormat ? $rootScope.userProfile.selectedUserLocation.dateFormat : "DD-MM-YYYY",
                toLabel: "To",
                //cancelLabel: 'Clear',
                //customRangeLabel: 'Custom range'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()]
            },
            eventHandlers: {
                'apply.daterangepicker': function(ev, picker) {
                    $timeout(function() {

                        $scope.flightScheduleSearch();
                    }, 2);
                },
                'cancel.daterangepicker': function(ev, picker) {
                    $timeout(function() {
                        $scope.searchDto.eta = null;
                        $scope.searchDto.etd = null;
                        $scope.flightScheduleSearch();
                    }, 2);

                }
            }
        };



        $scope.flightScheduleSearch = function() {
            console.log("flightScheduleSearch is called.");

            if ($scope.searchDto == undefined || $scope.searchDto == null) {
                $scope.searchDto = {};
            }

            $scope.searchDto.selectedPageNumber = $scope.flightSchedulePage;
            $scope.searchDto.recordPerPage = $scope.flightScheduleLimit;

            if ($scope.searchDto.eta != null) {
                $scope.searchDto.eta.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.eta.startDate);
                $scope.searchDto.eta.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.eta.endDate);
            } else {
                $scope.searchDto.eta = null;
            }

            if ($scope.searchDto.etd != null) {
                $scope.searchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.startDate);
                $scope.searchDto.etd.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.etd.endDate);
            } else {
                $scope.searchDto.etd = null;
            }

            console.log("$scope.searchDto ", $scope.searchDto);


            FlightPlanSearchForShipment.query($scope.searchDto).$promise.then(function(data, status) {
                $scope.flightScheduleTotalRecord = data.responseObject.totalRecord;
                console.log("$scope.flightScheduleTotalRecord ", $scope.flightScheduleTotalRecord);
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.flightSchedulePage * $scope.flightScheduleLimit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });

                $scope.flightScheduleSearchResult = resultArr;

            });


        }

        $scope.toggleFlightPlan = function(flightSchedule) {
            if ($scope.flightDetail == 'child' + flightSchedule.id) {
                $scope.flightDetail = null;
                $scope.flightSchedule = null;
            } else {
                $scope.flightDetail = 'child' + flightSchedule.id;
                $scope.flightSchedule = flightSchedule;
            }
        }

        $scope.flightScheduleChangePage = function(param) {
            console.log("flightScheduleChangePage ", param);
            $scope.flightSchedulePage = param.page;
            $scope.flightScheduleLimit = param.size;
            $scope.flightScheduleSearch();

        }

        $scope.flightScheduleLimitChange = function(item) {
            console.log("flightScheduleLimitChange ", item);
            $scope.flightSchedulePage = 0;
            $scope.flightScheduleLimit = item;
            $scope.flightScheduleSearch();
        }

        $scope.flightScheduleSortChange = function(sortKey) {

            $scope.searchDto.sortByColumn = sortKey;
            if ($scope.searchDto.orderByType == 'asc') {
                $scope.searchDto.orderByType = 'desc';
            } else {
                $scope.searchDto.orderByType = 'asc';
            }
            $scope.flightScheduleSearch();

        }


        $scope.selectedFlightSchedule = function() {
            flightScheduleModal.hide();
            if ($scope.flightSchedule != null && $scope.flightSchedule != undefined) {
                $scope.consol.carrier = $scope.flightSchedule.carrierMaster;
                $scope.consol.consolDocument.routeNo = $scope.flightSchedule.flightNo;
                $scope.consol.eta = $rootScope.dateAndTimeToString($scope.flightSchedule.eta);
                $scope.consol.etd = $rootScope.dateAndTimeToString($scope.flightSchedule.etd);
                $scope.consol.scheduleUid = $scope.flightSchedule.scheduleId;
                $scope.consol.vesselId = $scope.flightSchedule.id;
            }
            $rootScope.navigateToNextField('mawb' + $scope.selectedTabIndex);
        }

        $scope.cancelFlightSchedule = function() {
            $scope.flightSchedule = {};
            $rootScope.navigateToNextField('mawb' + $scope.selectedTabIndex);
        };

        $scope.newFlightSchedule = function(flightSchedule) {
            $scope.flightSchedule = flightSchedule;

        };

        //consol flight schedule code ends

        $scope.processProvisional = function(object, shipmentServiceDetail) {
            if (object != null && object.id != null) {
                if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_MODIFY)) {
                    consolShipmentAccountsService.addOrEditProvisional(object, $scope.consol, shipmentServiceDetail);
                }
            } else {
                if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_CREATE)) {
                    consolShipmentAccountsService.addOrEditProvisional(null, $scope.consol, shipmentServiceDetail);
                }
            }
        }

        $scope.getInvoiceCrnView = function(invoiceCreditNote, shipmentServiceDetail) {
            consolShipmentAccountsService.getInvoiceCrnView(invoiceCreditNote, shipmentServiceDetail, $scope.consol);
        }

        $scope.getSignOffStatus = function(fromState, consol, shipmentServiceDetail) {
            var serviceId = null;
            if (consol.shipmentLinkList != null && consol.shipmentLinkList != undefined && consol.shipmentLinkList.length == 1 ||
                (shipmentServiceDetail != null && shipmentServiceDetail != undefined)) {
                if (shipmentServiceDetail != null && shipmentServiceDetail != undefined) {
                    serviceId = shipmentServiceDetail.id;
                } else {
                    serviceId = consol.shipmentLinkList[0].service.id;
                }
                ServiceSignOffStatus.get({
                    serviceId: serviceId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (data.responseObject != null && data.responseObject.isSignOff == true) {

                            if (fromState == 'INV') {
                                consolShipmentAccountsService.createInvoice(consol, shipmentServiceDetail);
                            } else if (fromState == 'CRN_COST') {
                                consolShipmentAccountsService.createCreditNoteCost(consol, shipmentServiceDetail);
                            } else if (fromState == 'CRN_REV') {
                                consolShipmentAccountsService.createCreditNoteRevenue(consol, shipmentServiceDetail);
                            }

                        } else {
                            console.log($rootScope.nls["ERR40028"]);
                            $rootScope.clientMessage = $rootScope.nls["ERR40028"];
                        }
                    } else {
                        console.log($rootScope.nls["ERR40028"]);
                        $rootScope.clientMessage = $rootScope.nls["ERR40028"];
                    }
                }, function(error) {
                    console.log("error in getSignOffStatus method : " + error);
                });
            } else {
                ConsolSignOffStatus.get({
                    consolId: consol.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (data.responseObject != null && data.responseObject.isSignOff == true) {

                            if (fromState == 'INV') {
                                consolShipmentAccountsService.createInvoice(consol, shipmentServiceDetail);
                            } else if (fromState == 'CRN_COST') {
                                consolShipmentAccountsService.createCreditNoteCost(consol, shipmentServiceDetail);
                            } else if (fromState == 'CRN_REV') {
                                consolShipmentAccountsService.createCreditNoteRevenue(consol, shipmentServiceDetail);
                            }

                        } else {
                            console.log($rootScope.nls["ERR40027"]);
                            $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                        }
                    } else {
                        console.log($rootScope.nls["ERR40027"]);
                        $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                    }
                }, function(error) {
                    console.log("error in getSignOffStatus method : " + error);
                });
            }
        }

        $scope.airlineEdiConfirmation = function(carrier) {

            if (carrier == undefined || carrier == null || carrier == "" || carrier.id == undefined) {
                Notification.error($rootScope.nls["ERR96519"]);
                return;
            }
            if (carrier.messagingType != undefined && carrier.messagingType != null && carrier.messagingType == 'WIN') {
                $scope.connectWinWeb();
            } else {
                if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_GENERATE_AIRLINE_EDI_CREATE)) {

                    if ($scope.oldData != JSON.stringify($scope.consol)) {

                        var newScope = $scope.$new();
                        newScope.errorMessage = $rootScope.nls["ERR9033"];

                        ngDialog.openConfirm({
                                template: '<p>{{errorMessage}}</p>' +
                                    '<div class="ngdialog-footer">' +
                                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)" style="float:both">Ok</button>' +
                                    '</div>',
                                plain: true,
                                scope: newScope,
                                className: 'ngdialog-theme-default'
                            })
                            .then(
                                function(value) {

                                });

                    } else {

                        if ($scope.consol.airlineEdi == undefined || $scope.consol.airlineEdi == null || $scope.consol.airlineEdi.id == null) {
                            $scope.airlineEdiDirectShipmentConfirmation();
                        } else {

                            var newScope = $scope.$new();
                            newScope.errorMessage = $rootScope.nls["ERR9021"]
                            ngDialog.openConfirm({
                                template: '<p>{{errorMessage}}</p>' +
                                    '<div class="ngdialog-footer">' +
                                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(false)">No</button>' +
                                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(true)">Yes</button>' +
                                    '</div>',
                                plain: true,
                                scope: newScope,
                                className: 'ngdialog-theme-default'
                            }).then(function(value) {
                                if (value == true) {
                                    $scope.airlineEdiDirectShipmentConfirmation();
                                } else {
                                    console.log("You pressed No");
                                }
                            });
                        }
                    }


                }

            }
        }

        $scope.airlineEdiDirectShipmentConfirmation = function() {
            if ($scope.consol.directShipment) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR9018"]
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(false)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(true)">Yes</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    $rootScope.mainpreloder = true;
                    console.log("You selected : ", value);
                    $scope.createEDI(value);
                });
            } else {
                $rootScope.mainpreloder = true;
                $scope.createEDI(false);
            }
        }

        var myOtherModalEdi;


        $scope.createEDI = function(flag) {
            AirlineEDIGet.get({
                id: $scope.consol.id,
                isAgentChange: flag
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $rootScope.mainpreloder = false;
                    $scope.airlineEdi = data.responseObject;
                    myOtherModalEdi = $modal({
                        scope: $scope,
                        templateUrl: '/app/components/air/consol_shipments_new_screen/views/airline_edi_popup.html',
                        backdrop: 'static',
                        show: false
                    });
                    myOtherModalEdi.$promise.then(myOtherModalEdi.show);
                } else {
                    $rootScope.mainpreloder = false;
                    console.log("Error while creating the Airline Edi Model ", data.responseDescription);
                }
            }, function(error) {
                $rootScope.mainpreloder = false;
                console.log("Error while creating the Airline Edi Model ", error);
            });
        }


        $scope.getAllEdiStatus = function() {
            if ($scope.consol.airlineEdi != undefined && $scope.consol.airlineEdi != null && $scope.consol.airlineEdi.id != null) {
                EDIResponse.get({
                    id: $scope.consol.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.ediResponseList = data.responseObject;
                        console.log("Air Line Edi Object : ", $scope.airlineEdi, $scope.ediResponseList);
                    } else {
                        console.log(data.responseDescription);
                    }
                }, function(error) {
                    console.log(error);
                });
            }
        }

        $scope.generateEdi = function(airlineEdiObj) {
            console.log("Generate Edi is pressed....");
            $scope.spinner = true;
            airlineEdiObj = modifyGenerateEdiServiceRequest.modifyGenerateEDIRequest(airlineEdiObj);
            AirlineEDISave.save(airlineEdiObj).$promise.then(function(data) {

                if (data.responseCode == 'ERR0') {
                    console.log("Success...", data.responseObject);

                    $rootScope.newConsolAfterEDI = data.responseObject;
                    if ($rootScope.newConsolAfterEDI != null) {
                        Notification.success($rootScope.nls["ERR9991"]);
                    }
                    $scope.ediReload();
                } else {
                    console.log("Failure...", data.responseDescription);
                    $scope.ediReload();
                }

            }, function(error) {
                console.log("Failure...", error);
                $scope.ediReload();
            });

            myOtherModalEdi.$promise.then(myOtherModalEdi.hide);
        }

        $timeout(function() {
            $scope.ediReload = function() {
                var stateCount = 0;
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }
                $scope.spinner = false;
                $state.go("layout.editAirConsol", {
                    consolId: $scope.consol.id,
                    count: stateCount,
                    forPurpose: 'EDI'
                });
            }
        }, 2);




        // Start auto import

        $scope.confirmEdiAndCompleteImportProcess = function() {

            console.log("confirmEdiAndCompleteImportProcess ");

            if ($scope.consol.airlineEdi == undefined || $scope.consol.airlineEdi == null || $scope.consol.airlineEdi.id == null) {
                Notification.warning($rootScope.nls["ERR264"])
                $scope.consol.isJobCompleted = false;
            } else {
                $scope.autoImportProcessCompleted();
            }
        }

        $scope.autoImportProcessCompleted = function() {
            if ($scope.consol.isJobCompleted == true) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR223"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.processAutoImport("Completed");
                    } else {
                        $scope.consol.isJobCompleted = false;
                    }
                })

            }

        }

        $scope.processAutoImport = function(status) {
                console.log("ProcessAutoImport method is called.");
                $rootScope.mainpreloder = true;
                $scope.consolDto = {};
                $scope.consolDto.consolId = $scope.consol.id;
                $scope.consolDto.consolStatus = status;
                AutoImportProcess.process($scope.consolDto).$promise
                    .then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            //$scope.consol = data.responseObject;
                            Notification.success($rootScope.nls["ERR96546"]);
                            $scope.consol.isJobCompleted = true;

                            $rootScope.mainpreloder = false;

                            var stateCount = 0;
                            if ($stateParams.count != undefined && $stateParams.count != null) {
                                stateCount = $stateParams.count + 1;
                            }

                            $state.go("layout.editAirConsol", {
                                consolId: $scope.consol.id,
                                submitAction: 'Saved',
                                count: stateCount
                            });

                        } else {
                            console.log("Service added Failed ", data.responseDescription)
                            $scope.consol.isJobCompleted = false;
                            $rootScope.mainpreloder = false;
                        }
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    }, function(error) {
                        console.log("Service added Failed : ", error);
                        $scope.consol.isJobCompleted = false;
                        $rootScope.mainpreloder = false;
                    });

            }
            // End auto import




        $scope.cancelfag = false;

        $scope.isFormValueDirty = false;

        $scope.setOldDataVal = function() {

            $timeout(function() {
                $scope.oldData = JSON.stringify($scope.consol);
            }, 300)

        }


        $scope.partyBlurChkData = function(party, forObject) {
            if (party == undefined || party == null || party == "" || party.id == undefined || party.id == null || party.id == "") {
                $scope.consol = ConsolPartyClearService.chkAndClear($scope.consol, forObject);
            }
        }
        $scope.routePartyEvent = function(fromObjModel, isAdd, partyMaster) {

            $rootScope.nav_src_bkref_key = new Date().toISOString();
            var param = {
                nav_src_bkref_key: $rootScope.nav_src_bkref_key,
                forObj: fromObjModel,
                category: 'Master Shipment'
            };

            if (isAdd) {
                $state.go("layout.addParty", param);
            } else {
                if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                    param.partyId = partyMaster.id;
                    $state.go("layout.editParty", param);
                } else {
                    console.log("Party not selected...");
                }
            }
        }

        $scope.focusFieldAfter2Sec = function(fieldName, tabName) {

            $timeout(function() {
                if (tabName != undefined && tabName != null) {
                    $scope.menuTabClick(fieldName, tabName)
                } else {
                    $rootScope.navigateToNextField(fieldName);
                }
            }, 3000);
        }


        $scope.partyMasterActionResponseFn = function() {
            if ($scope.consol != null && $stateParams.forObj != undefined && $stateParams.forObj != null) {
                var isNaviPartyMasterEmpty = true;
                if ($rootScope.naviPartyMaster != undefined && $rootScope.naviPartyMaster != null && $rootScope.naviPartyMaster.id != null) {
                    isNaviPartyMasterEmpty = false;
                }
                if ($stateParams.forObj == "consol.agent") {
                    if ($scope.consol.agent != undefined && $scope.consol.agent != null) {
                        if (isNaviPartyMasterEmpty) {
                            $scope.consol.agent = {};
                        } else {
                            $scope.consol.agent = $rootScope.naviPartyMaster;
                        }
                    }
                    $scope.focusFieldAfter2Sec('consolPartyAgent', null);
                } else if ($stateParams.forObj == "consol.consolDocument.shipper") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.shipper = {};
                    } else {
                        $scope.consol.consolDocument.shipper = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('shipper', 'documents');
                } else if ($stateParams.forObj == "consol.consolDocument.consignee") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.consignee = {};
                    } else {
                        $scope.consol.consolDocument.consignee = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('consigneeConsolDoc', 'documents');
                } else if ($stateParams.forObj == "consol.consolDocument.firstNotify") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.firstNotify = {};
                    } else {
                        $scope.consol.consolDocument.firstNotify = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('notifyCust1ConsolDoc', 'documents');
                } else if ($stateParams.forObj == "consol.consolDocument.secondNotify") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.secondNotify = {};
                    } else {
                        $scope.consol.consolDocument.secondNotify = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('notifyCust2ConsolDocument', 'documents');
                } else if ($stateParams.forObj == "consol.consolDocument.forwarder") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.forwarder = {};
                    } else {
                        $scope.consol.consolDocument.forwarder = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('forwarderConsolDocument', 'documents');
                } else if ($stateParams.forObj == "consol.consolDocument.agent") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.agent = {};
                    } else {
                        $scope.consol.consolDocument.agent = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('agentConsolDocument', 'documents');
                } else if ($stateParams.forObj == "consol.consolDocument.issuingAgent") {
                    if (isNaviPartyMasterEmpty) {
                        $scope.consol.consolDocument.issuingAgent = {};
                    } else {
                        $scope.consol.consolDocument.issuingAgent = $rootScope.naviPartyMaster;
                    }
                    $scope.focusFieldAfter2Sec('issuingAgentConsolDocument', 'documents');
                }


            }
        }


        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('consol-panel'));
        $scope.contained_progressbar.setAbsolute();
        $scope.init = function() {



            $scope.errorMap = new Map();
            $scope.customerAgent = undefined;
            $scope.documentTypeCode = undefined;
            $scope.cancelfag = false;

            $scope.btnGrp = {};
            $scope.btnGrpFilter = {};
            $scope.btnGrp.TypeArr = ["Invoice", "Credit Note", "All"];
            $scope.btnGrpFilter.TypeArr = ["Customer", "Agent", "All"];


            $scope.openDefaultTab();



        }

        $scope.ajaxConsolAgentEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolAgentList = data.responseObject.searchResult;
                        return $scope.consolAgentList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolAgent = function(partyobj) {
            if ($scope.validateConsol(3)) {
                $scope.getConsolAgentAddress($scope.consol.agent);
                $rootScope.navigateToNextField(partyobj);
            }

        };

        $scope.getConsolAgentAddress = function(obj) {

            if (obj.partyAddressList != null && obj.partyAddressList != undefined) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        $scope.consol.agentAddress = addressJoiner.joinAddressLineByLine($scope.consol.agentAddress, obj, obj.partyAddressList[i]);
                    }
                }
            }

        }


        $scope.showConsolAgentAddressList = false;
        $scope.consolAgentAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showConsolAgent = function() {
            $scope.panelTitle = "Agent Address";
            $scope.showConsolAgentAddressList = true;
            $scope.selectedItem = -1;

            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.agent.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.agent.partyAddressList[i]);
            }
        };

        $scope.selectedConsolAgentAddress = function(obj) {

            $scope.consol.agentAddress = addressJoiner.joinAddressLineByLine($scope.consol.agentAddress, $scope.consol.agent, obj);

            $scope.cancelConsolAgentAddress();
        };

        $scope.cancelConsolAgentAddress = function() {
            $scope.showConsolAgentAddressList = false;
        }


        /* Consol Origin List Picker */

        $scope.ajaxOriginEvent = function(object) {
            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR95015"]);
                $scope.errorMap.put("consolService", $rootScope.nls["ERR95015"]);
                return false;
            }
            console.log("ajaxOriginEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {


                    if (data.responseCode == "ERR0") {
                        $scope.originList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.destination == undefined || $scope.consol.destination == null) {
                                $scope.originList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consol.destination.id) {
                                        $scope.originList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.originList;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching Origin List');
                }
            );

        }

        $scope.selectedOrigin = function(nextIdValue) {
            if ($scope.validateConsol(8)) {

                // Assign default pol to origin
                if ($scope.consol.pol == undefined || $scope.consol.pol == null) {
                    $scope.consol.pol = $scope.consol.origin;
                }

                $rootScope.navigateToNextField(nextIdValue);
            }

        };


        $scope.ajaxDestination = function(object) {

            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR95015"]);
                $scope.errorMap.put("consolService", $rootScope.nls["ERR95015"]);
                return false;
            }

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.destinationList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.origin == undefined || $scope.consol.origin == null) {
                                $scope.destinationList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consol.origin.id) {
                                        $scope.destinationList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.destinationList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Destination List');
                }
            );

        }

        $scope.selectedDestination = function(nextIdValue) {
            if ($scope.validateConsol(11)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        };


        $scope.ajaxPortOfLoadingEvent = function(object) {

            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR95015"]);
                $scope.errorMap.put("consolService", $rootScope.nls["ERR95015"]);
                return false;
            }

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {


                    if (data.responseCode == "ERR0") {
                        $scope.portOfLoadingList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.pod == undefined || $scope.consol.pod == null) {
                                $scope.portOfLoadingList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consol.pod.id) {
                                        $scope.portOfLoadingList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.portOfLoadingList;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );

        }

        $scope.selectedPortOfLoading = function(nextIdValue) {
            if ($scope.validateConsol(9)) {
                $rootScope.navigateToNextField(nextIdValue);
            }
        };


        $scope.ajaxPortOfDischarge = function(object) {
            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR95015"]);
                $scope.errorMap.put("consolService", $rootScope.nls["ERR95015"]);
                return false;
            }
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDischargeList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.pol == undefined || $scope.consol.pol == null) {
                                $scope.portOfDischargeList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consol.pol.id) {
                                        $scope.portOfDischargeList.push(resultList[i]);
                                    }

                                }
                            }

                        }
                        return $scope.portOfDischargeList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );

        }

        $scope.selectedPortOfDischarge = function(nextIdValue) {
            if ($scope.validateConsol(10)) {

                // Assign default destination to pod
                if ($scope.consol.destination == undefined || $scope.consol.destination == null) {
                    $scope.consol.destination = $scope.consol.pod;
                }

                $rootScope.navigateToNextField(nextIdValue);
            }
        };


        $scope.ajaxCurrencyEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencySearchExclude.fetch({
                "excludeCurrencyCode": $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.currencyList = data.responseObject.searchResult;
                        return $scope.currencyList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Currency');
                }
            );


        }

        $scope.selectedCurrency = function(nextIdValue) {
            if ($scope.validateConsol(12)) {
                if ($rootScope.baseCurrenyRate[$scope.consol.currency.id] != undefined && $rootScope.baseCurrenyRate[$scope.consol.currency.id] != null) {
                    $scope.consol.currencyRate = $rootScope.baseCurrenyRate[$scope.consol.currency.id].csell;
                }

                $rootScope.navigateToNextField(nextIdValue);
            }

        }

        /* Consol Service Select Picker */

        $scope.changeService = function(dataObj) {
            $scope.ajaxOriginEvent(null);
            $scope.ajaxDestination(null);
            $scope.ajaxPortOfLoadingEvent(null);
            $scope.ajaxPortOfDischarge(null);
            $scope.ajaxConnPolEvent(null);
            $scope.ajaxConnPodEvent(null);
            $scope.ajaxConnCarrierEvent(null);
        }

        $scope.ajaxServiceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ServiceList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.serviceList = data.responseObject.searchResult;
                        return $scope.serviceList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching  Services');
                }
            );

        }


        $scope.selectedService = function(serviceObj, nextIdValue) {

            if ($scope.consol.serviceMaster != null && ($scope.consol.serviceMaster.status == 'Block' || $scope.consol.serviceMaster == 'Hide')) {
                $scope.consol.serviceMaster = null;
            }
            if ($scope.validateConsol(1)) {
                if ($scope.consol.serviceMaster.importExport == "Import") {
                    $scope.consol.whoRouted = false;
                }
                $rootScope.navigateToNextField(nextIdValue);
            }
        };

        $scope.cancelService = function() {
            console.log("Cancel service List Button Pressed....");
            $scope.searchText = "";
            $scope.showServiceList = false;
        };


        $scope.ajaxCarrierEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;



            return CarrierByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.carrierMasterList = data.responseObject.searchResult;
                        return $scope.carrierMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier List Based on Air');
                }
            );

        }

        $scope.selectedCarrierMaster = function(nextIdValue) {
            if ($scope.validateConsol(14)) {
                $scope.consol.consolDocument.routeNo = null;
                $scope.consol.eta = null;
                $scope.consol.etd = null;
                $scope.consol.scheduleUid = null;
                $scope.consol.vesselId = null;
                $rootScope.navigateToNextField(nextIdValue);
            }
        };



        $scope.ajaxDocCarrierEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;



            return CarrierByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.carrierMasterList = data.responseObject.searchResult;
                        return $scope.carrierMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier List Based on Air');
                }
            );

        }


        $scope.cancel = function() {
            if ($scope.oldData != JSON.stringify($scope.consol)) {
                $scope.isFormValueDirty = true;
            }

            var cancelParams = {};
            cancelParams.submitAction = 'Cancelled';
            if ($scope.isFormValueDirty == true) {

                $scope.cancelfag = true;
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-primary btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            ' <button type="button" class="btn btn-default btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-primary pull-left btn-property grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 &&
                                $scope.consol.id == null) {
                                $scope.saveConsol();
                            } else if (value == 1 &&
                                $scope.consol.id != null) {
                                $scope.updateConsol();
                            } else if (value == 2) {
                                if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
                                    if (window.history != undefined)
                                        window.history.back();
                                }
                                if (($stateParams.action == "CREATE" && ($stateParams.fromHistory == undefined || $stateParams.fromHistory == null || $stateParams.fromHistory == ""))) {
                                    if ($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0) {
                                        var unSavedData = $rootScope.unfinishedFormHistoryList[$rootScope.unfinishedFormHistoryList.length - 1];
                                        GoToUnfilledFormNavigationService.goToState(unSavedData, undefined); // $rootScope.unfinishedFormHistoryList.push(stateDataObject);
                                    } else {
                                        $state.go('layout.airNewConsol', cancelParams);
                                    }
                                } else {
                                    $state.go('layout.airNewConsol', cancelParams);
                                }
                            } else {
                                console.log("cancelled consol", cancelParams);
                            }

                        });

            } else {

                if ($stateParams.fromState != undefined && $stateParams.fromState != null) {
                    if (window.history != undefined)
                        window.history.back();
                } else {
                    $state.go('layout.airNewConsol', cancelParams);
                }
            }
        }

        $scope.consolDimension = {};
        $scope.consolDimension.dimensionCheck = {};

        var modifyRequest = function(modifiedObject) {

            if (modifiedObject.createdBy) {
                modifiedObject.createdBy = {
                    "versionLock": modifiedObject.createdBy.versionLock,
                    "id": modifiedObject.createdBy.id
                }
            }
            if (modifiedObject.company) {
                modifiedObject.company = {
                    "versionLock": modifiedObject.company.versionLock,
                    "id": modifiedObject.company.id,
                }
            }
            if (modifiedObject.location) {
                modifiedObject.location = {
                    "id": modifiedObject.location.id,
                    "versionLock": modifiedObject.location.versionLock,
                    "countryMaster": {
                        "versionLock": modifiedObject.location.countryMaster.versionLock,
                        "id": modifiedObject.location.countryMaster.id
                    }
                }
            }
            if (modifiedObject.country) {
                modifiedObject.country = {
                    "versionLock": modifiedObject.country.versionLock,
                    "id": modifiedObject.country.id,
                    "currencyMaster": {
                        "versionLock": modifiedObject.country.currencyMaster.versionLock,
                        "id": modifiedObject.country.currencyMaster.id
                    }
                }
            }

            if (modifiedObject.serviceMaster) {
                modifiedObject.serviceMaster = {
                    "versionLock": modifiedObject.serviceMaster.versionLock,
                    "id": modifiedObject.serviceMaster.id
                }
            }

            if (modifiedObject.localCurrency) {
                modifiedObject.localCurrency = {
                    "versionLock": modifiedObject.localCurrency.versionLock,
                    "id": modifiedObject.localCurrency.id
                }
            }

            if (modifiedObject.currency) {
                modifiedObject.currency = {
                    "versionLock": modifiedObject.currency.versionLock,
                    "id": modifiedObject.currency.id
                }
            }
            if (modifiedObject.origin) {
                modifiedObject.origin = {
                    "versionLock": modifiedObject.origin.versionLock,
                    "id": modifiedObject.origin.id
                }
            }
            if (modifiedObject.pol) {
                modifiedObject.pol = {
                    "versionLock": modifiedObject.pol.versionLock,
                    "id": modifiedObject.pol.id
                }
            }
            if (modifiedObject.pod) {
                modifiedObject.pod = {
                    "versionLock": modifiedObject.pod.versionLock,
                    "id": modifiedObject.pod.id
                }
            }
            if (modifiedObject.destination) {
                modifiedObject.destination = {
                    "versionLock": modifiedObject.destination.versionLock,
                    "id": modifiedObject.destination.id
                }
            }
            if (modifiedObject.carrier) {
                modifiedObject.carrier = {
                    "versionLock": modifiedObject.carrier.versionLock,
                    "id": modifiedObject.carrier.id
                }
            }

            if (modifiedObject.firstCarrier) {
                modifiedObject.firstCarrier = {
                    "versionLock": modifiedObject.firstCarrier.versionLock,
                    "id": modifiedObject.firstCarrier.id
                }
            }


            if (modifiedObject.handlingAgent) {
                modifiedObject.handlingAgent = {
                    "versionLock": modifiedObject.handlingAgent.versionLock,
                    "id": modifiedObject.handlingAgent.id
                }
            }

            if (modifiedObject.carrierAgent) {
                modifiedObject.carrierAgent = {
                    "versionLock": modifiedObject.carrierAgent.versionLock,
                    "id": modifiedObject.carrierAgent.id
                }
            }

            if (modifiedObject.coLoader) {
                modifiedObject.coLoader = {
                    "versionLock": modifiedObject.coLoader.versionLock,
                    "id": modifiedObject.coLoader.id
                }
            }

            if (modifiedObject.documentIssuingAgent) {
                modifiedObject.documentIssuingAgent = {
                    "versionLock": modifiedObject.documentIssuingAgent.versionLock,
                    "id": modifiedObject.documentIssuingAgent.id
                }
            }

            if (modifiedObject.firstNotify) {
                modifiedObject.firstNotify = {
                    "versionLock": modifiedObject.firstNotify.versionLock,
                    "id": modifiedObject.firstNotify.id
                }
            }

            if (modifiedObject.agent) {
                modifiedObject.agent = {
                    "versionLock": modifiedObject.agent.versionLock,
                    "id": modifiedObject.agent.id
                }
            }


            if (modifiedObject.agentAddress) {
                if (modifiedObject.agentAddress.city) {
                    modifiedObject.agentAddress.city = {
                        "versionLock": modifiedObject.agentAddress.city.versionLock,
                        "id": modifiedObject.agentAddress.city.id

                    }
                }
                if (modifiedObject.agentAddress.state) {
                    modifiedObject.agentAddress.state = {
                        "versionLock": modifiedObject.agentAddress.state.versionLock,
                        "id": modifiedObject.agentAddress.state.id

                    }
                }
                if (modifiedObject.agentAddress.country) {
                    modifiedObject.agentAddress.country = {
                        "versionLock": modifiedObject.agentAddress.country.versionLock,
                        "id": modifiedObject.agentAddress.country.id

                    }
                }

            }


            if (modifiedObject.consolDocument) {

                if (modifiedObject.consolDocument.shipper) {
                    modifiedObject.consolDocument.shipper = {
                        "versionLock": modifiedObject.consolDocument.shipper.versionLock,
                        "id": modifiedObject.consolDocument.shipper.id
                    }
                }


                if (modifiedObject.consolDocument.shipperAddress) {
                    if (modifiedObject.consolDocument.shipperAddress.city) {
                        modifiedObject.consolDocument.shipperAddress.city = {
                            "versionLock": modifiedObject.consolDocument.shipperAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.shipperAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.shipperAddress.state) {
                        modifiedObject.consolDocument.shipperAddress.state = {
                            "versionLock": modifiedObject.consolDocument.shipperAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.shipperAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.shipperAddress.country) {
                        modifiedObject.consolDocument.shipperAddress.country = {
                            "versionLock": modifiedObject.consolDocument.shipperAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.shipperAddress.country.id

                        }
                    }
                }

                if (modifiedObject.consolDocument.consignee) {
                    modifiedObject.consolDocument.consignee = {
                        "versionLock": modifiedObject.consolDocument.consignee.versionLock,
                        "id": modifiedObject.consolDocument.consignee.id
                    }
                }

                if (modifiedObject.consolDocument.consigneeAddress) {
                    if (modifiedObject.consolDocument.consigneeAddress.city) {
                        modifiedObject.consolDocument.consigneeAddress.city = {
                            "versionLock": modifiedObject.consolDocument.consigneeAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.consigneeAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.consigneeAddress.state) {
                        modifiedObject.consolDocument.consigneeAddress.state = {
                            "versionLock": modifiedObject.consolDocument.consigneeAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.consigneeAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.consigneeAddress.country) {
                        modifiedObject.consolDocument.consigneeAddress.country = {
                            "versionLock": modifiedObject.consolDocument.consigneeAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.consigneeAddress.country.id

                        }
                    }
                }


                if (modifiedObject.consolDocument.forwarder) {
                    modifiedObject.consolDocument.forwarder = {
                        "versionLock": modifiedObject.consolDocument.forwarder.versionLock,
                        "id": modifiedObject.consolDocument.forwarder.id
                    }
                }

                if (modifiedObject.consolDocument.forwarderAddress) {
                    if (modifiedObject.consolDocument.forwarderAddress.city) {
                        modifiedObject.consolDocument.forwarderAddress.city = {
                            "versionLock": modifiedObject.consolDocument.forwarderAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.forwarderAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.forwarderAddress.state) {
                        modifiedObject.consolDocument.forwarderAddress.state = {
                            "versionLock": modifiedObject.consolDocument.forwarderAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.forwarderAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.forwarderAddress.country) {
                        modifiedObject.consolDocument.forwarderAddress.country = {
                            "versionLock": modifiedObject.consolDocument.forwarderAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.forwarderAddress.country.id

                        }
                    }
                }

                if (modifiedObject.consolDocument.firstNotify) {
                    modifiedObject.consolDocument.firstNotify = {
                        "versionLock": modifiedObject.consolDocument.firstNotify.versionLock,
                        "id": modifiedObject.consolDocument.firstNotify.id
                    }
                }

                if (modifiedObject.consolDocument.firstNotifyAddress) {
                    if (modifiedObject.consolDocument.firstNotifyAddress.city) {
                        modifiedObject.consolDocument.firstNotifyAddress.city = {
                            "versionLock": modifiedObject.consolDocument.firstNotifyAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.firstNotifyAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.firstNotifyAddress.state) {
                        modifiedObject.consolDocument.firstNotifyAddress.state = {
                            "versionLock": modifiedObject.consolDocument.firstNotifyAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.firstNotifyAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.firstNotifyAddress.country) {
                        modifiedObject.consolDocument.firstNotifyAddress.country = {
                            "versionLock": modifiedObject.consolDocument.firstNotifyAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.firstNotifyAddress.country.id

                        }
                    }
                }

                if (modifiedObject.consolDocument.secondNotify) {
                    modifiedObject.consolDocument.secondNotify = {
                        "versionLock": modifiedObject.consolDocument.secondNotify.versionLock,
                        "id": modifiedObject.consolDocument.secondNotify.id
                    }
                }

                if (modifiedObject.consolDocument.secondNotifyAddress) {
                    if (modifiedObject.consolDocument.secondNotifyAddress.city) {
                        modifiedObject.consolDocument.secondNotifyAddress.city = {
                            "versionLock": modifiedObject.consolDocument.secondNotifyAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.secondNotifyAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.secondNotifyAddress.state) {
                        modifiedObject.consolDocument.secondNotifyAddress.state = {
                            "versionLock": modifiedObject.consolDocument.secondNotifyAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.secondNotifyAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.secondNotifyAddress.country) {
                        modifiedObject.consolDocument.secondNotifyAddress.country = {
                            "versionLock": modifiedObject.consolDocument.secondNotifyAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.secondNotifyAddress.country.id

                        }
                    }
                }

                if (modifiedObject.consolDocument.agent) {
                    modifiedObject.consolDocument.agent = {
                        "versionLock": modifiedObject.consolDocument.agent.versionLock,
                        "id": modifiedObject.consolDocument.agent.id
                    }
                }

                if (modifiedObject.consolDocument.agentAddress) {
                    if (modifiedObject.consolDocument.agentAddress.city) {
                        modifiedObject.consolDocument.agentAddress.city = {
                            "versionLock": modifiedObject.consolDocument.agentAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.agentAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.agentAddress.state) {
                        modifiedObject.consolDocument.agentAddress.state = {
                            "versionLock": modifiedObject.consolDocument.agentAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.agentAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.agentAddress.country) {
                        modifiedObject.consolDocument.agentAddress.country = {
                            "versionLock": modifiedObject.consolDocument.agentAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.agentAddress.country.id

                        }
                    }
                }

                if (modifiedObject.consolDocument.issuingAgent) {
                    modifiedObject.consolDocument.issuingAgent = {
                        "versionLock": modifiedObject.consolDocument.issuingAgent.versionLock,
                        "id": modifiedObject.consolDocument.issuingAgent.id
                    }
                }

                if (modifiedObject.consolDocument.issuingAgentAddress) {
                    if (modifiedObject.consolDocument.issuingAgentAddress.city) {
                        modifiedObject.consolDocument.issuingAgentAddress.city = {
                            "versionLock": modifiedObject.consolDocument.issuingAgentAddress.city.versionLock,
                            "id": modifiedObject.consolDocument.issuingAgentAddress.city.id

                        }
                    }
                    if (modifiedObject.consolDocument.issuingAgentAddress.state) {
                        modifiedObject.consolDocument.issuingAgentAddress.state = {
                            "versionLock": modifiedObject.consolDocument.issuingAgentAddress.state.versionLock,
                            "id": modifiedObject.consolDocument.issuingAgentAddress.state.id

                        }
                    }
                    if (modifiedObject.consolDocument.issuingAgentAddress.country) {
                        modifiedObject.consolDocument.issuingAgentAddress.country = {
                            "versionLock": modifiedObject.consolDocument.issuingAgentAddress.country.versionLock,
                            "id": modifiedObject.consolDocument.issuingAgentAddress.country.id

                        }
                    }
                }

                if (modifiedObject.consolDocument.packMaster) {
                    modifiedObject.consolDocument.packMaster = {
                        "versionLock": modifiedObject.consolDocument.packMaster.versionLock,
                        "id": modifiedObject.consolDocument.packMaster.id
                    }
                }
                if (modifiedObject.consolDocument.company) {
                    modifiedObject.consolDocument.company = {
                        "versionLock": modifiedObject.consolDocument.company.versionLock,
                        "id": modifiedObject.consolDocument.company.id
                    }
                }
                if (modifiedObject.consolDocument.country) {
                    modifiedObject.consolDocument.country = {
                        "versionLock": modifiedObject.consolDocument.country.versionLock,
                        "id": modifiedObject.consolDocument.country.id
                    }
                }
                if (modifiedObject.consolDocument.location) {
                    modifiedObject.consolDocument.location = {
                        "versionLock": modifiedObject.consolDocument.location.versionLock,
                        "id": modifiedObject.consolDocument.location.id
                    }
                }
                if (modifiedObject.consolDocument.commodity) {
                    modifiedObject.consolDocument.commodity = {
                        "versionLock": modifiedObject.consolDocument.commodity.versionLock,
                        "id": modifiedObject.consolDocument.commodity.id
                    }
                }

                if (modifiedObject.consolDocument.pickUpDeliveryPoint != undefined && modifiedObject.consolDocument.pickUpDeliveryPoint != null &&
                    modifiedObject.consolDocument.pickUpDeliveryPoint != "") {

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpFollowUp != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpFollowUp = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpFollowUp);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlanned != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlanned = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlanned);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpActual != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpActual = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpActual);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryExpected != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryExpected = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryExpected);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryActual != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryActual = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryActual);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.doorDeliveryExpected != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.doorDeliveryExpected);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.doorDeliveryActual != null)
                        modifiedObject.consolDocument.pickUpDeliveryPoint.doorDeliveryActual = $rootScope.sendApiDateAndTime(modifiedObject.consolDocument.pickUpDeliveryPoint.doorDeliveryActual);

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint) {
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.countryMaster) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.countryMaster = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.countryMaster.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.countryMaster.id
                            };
                        }
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.partyDetail) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.partyDetail = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.partyDetail.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.partyDetail.id
                            };
                        }


                        var partyAddressList = modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.partyAddressList;
                        if (partyAddressList) {
                            for (var j = 0; j < partyAddressList.length; j++) {
                                if (partyAddressList[j].stateMaster) {
                                    partyAddressList[j].stateMaster = {
                                        "versionLock": partyAddressList[j].stateMaster.versionLock,
                                        "id": partyAddressList[j].stateMaster.id
                                    }
                                }
                                if (partyAddressList[j].cityMaster) {
                                    partyAddressList[j].cityMaster = {
                                        "versionLock": partyAddressList[j].cityMaster.versionLock,
                                        "id": partyAddressList[j].cityMaster.id
                                    }
                                }
                                if (partyAddressList[j].partyCountryField) {
                                    partyAddressList[j].partyCountryField = {
                                        "versionLock": partyAddressList[j].partyCountryField.versionLock,
                                        "id": partyAddressList[j].partyCountryField.id
                                    }
                                }

                            }
                        }

                        var partyServiceList = modifiedObject.consolDocument.pickUpDeliveryPoint.pickupPoint.partyServiceList;

                        if (partyServiceList) {
                            for (var j = 0; j < partyServiceList.length; j++) {
                                if (partyServiceList[j].serviceMaster) {
                                    partyServiceList[j].serviceMaster = {
                                        "versionLock": partyServiceList[j].serviceMaster.versionLock,
                                        "id": partyServiceList[j].serviceMaster.id
                                    }
                                }
                                if (partyServiceList[j].tosMaster) {
                                    partyServiceList[j].tosMaster = {
                                        "versionLock": partyServiceList[j].tosMaster.versionLock,
                                        "id": partyServiceList[j].tosMaster.id
                                    }
                                }
                                if (partyServiceList[j].locationMaster) {
                                    partyServiceList[j].locationMaster = {
                                        "versionLock": partyServiceList[j].locationMaster.versionLock,
                                        "id": partyServiceList[j].locationMaster.id
                                    }
                                }
                                if (partyServiceList[j].salesman) {
                                    partyServiceList[j].salesman = {
                                        "versionLock": partyServiceList[j].salesman.versionLock,
                                        "id": partyServiceList[j].salesman.id
                                    }
                                }
                                if (partyServiceList[j].customerService) {
                                    partyServiceList[j].customerService = {
                                        "versionLock": partyServiceList[j].customerService.versionLock,
                                        "id": partyServiceList[j].customerService.id
                                    }
                                }


                            }

                        }


                    }

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace) {
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.state) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.state = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.state.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.state.id
                            };
                        }
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.city) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.city = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.city.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.city.id
                            };
                        }
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.country) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.country = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.country.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.pickUpPlace.country.id
                            };
                        }

                    }

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace) {
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.state) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.state = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.state.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.state.id
                            };
                        }
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.city) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.city = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.city.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPlace.city.id
                            };
                        }

                    }

                    if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPoint) {
                        if (modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPoint.portMaster) {
                            modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPoint.portMaster = {
                                "versionLock": modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPoint.portMaster.versionLock,
                                "id": modifiedObject.consolDocument.pickUpDeliveryPoint.deliveryPoint.portMaster.id
                            };
                        }
                    }

                }
            }
            return modifiedObject;
        }

        $scope.getTempObject = function(baseObject) {

            var tmpObj = cloneService.clone(baseObject);

            tmpObj = modifyRequest(tmpObj);

            tmpObj.ppcc = tmpObj.ppcc == false ? 'Prepaid' : 'Collect';

            tmpObj.eta = $rootScope.sendApiDateAndTime(tmpObj.eta);
            tmpObj.etd = $rootScope.sendApiDateAndTime(tmpObj.etd);

            tmpObj.atd = $rootScope.sendApiDateAndTime(tmpObj.atd);
            tmpObj.ata = $rootScope.sendApiDateAndTime(tmpObj.ata);

            if (tmpObj.serviceMaster != undefined && tmpObj.serviceMaster.importExport == "Import") {
                tmpObj.whoRouted = tmpObj.whoRouted == false ? 'Self' : 'Agent';
            }
            tmpObj.consolDocument.mawbIssueDate = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.mawbIssueDate);
            tmpObj.consolDocument.mawbGeneratedDate = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.mawbGeneratedDate);
            tmpObj.consolDocument.dimensionUnit = tmpObj.consolDocument.dimensionUnit == false ? 'CENTIMETERORKILOS' : 'INCHORPOUNDS';
            tmpObj.consolDocument.dimensionUnitValue = tmpObj.consolDocument.dimensionUnit === 'CENTIMETERORKILOS' ? $rootScope.appMasterData['centimeter.to.kilos'] : $rootScope.appMasterData['inch.to.pounds'];
            tmpObj.directShipment = tmpObj.directShipment == false ? 'No' : 'Yes';
            if (tmpObj.consolDocument.eta != null)
                tmpObj.consolDocument.eta = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.eta);
            if (tmpObj.consolDocument.etd != null)
                tmpObj.consolDocument.etd = $rootScope.sendApiDateAndTime(tmpObj.consolDocument.etd);


            if (tmpObj.pickUpDeliveryPoint == undefined) {
                tmpObj.pickUpDeliveryPoint = {};
                tmpObj.pickUpDeliveryPoint.isOurPickUp = false;
            }



            if (tmpObj.pickUpDeliveryPoint != undefined && tmpObj.pickUpDeliveryPoint != null &&
                tmpObj.pickUpDeliveryPoint != "") {

                if (tmpObj.pickUpDeliveryPoint.pickUpFollowUp != null)
                    tmpObj.pickUpDeliveryPoint.pickUpFollowUp = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.pickUpFollowUp);

                if (tmpObj.pickUpDeliveryPoint.pickUpPlanned != null)
                    tmpObj.pickUpDeliveryPoint.pickUpPlanned = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.pickUpPlanned);

                if (tmpObj.pickUpDeliveryPoint.pickUpActual != null)
                    tmpObj.pickUpDeliveryPoint.pickUpActual = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.pickUpActual);

                if (tmpObj.pickUpDeliveryPoint.deliveryExpected != null)
                    tmpObj.pickUpDeliveryPoint.deliveryExpected = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.deliveryExpected);

                if (tmpObj.pickUpDeliveryPoint.deliveryActual != null)
                    tmpObj.pickUpDeliveryPoint.deliveryActual = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.deliveryActual);

                if (tmpObj.pickUpDeliveryPoint.doorDeliveryExpected != null)
                    tmpObj.pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.doorDeliveryExpected);

                if (tmpObj.pickUpDeliveryPoint.doorDeliveryActual != null)
                    tmpObj.pickUpDeliveryPoint.doorDeliveryActual = $rootScope.sendApiDateAndTime(tmpObj.pickUpDeliveryPoint.doorDeliveryActual);

                if (tmpObj.pickUpDeliveryPoint.pickupPoint) {
                    if (tmpObj.pickUpDeliveryPoint.pickupPoint.countryMaster) {
                        tmpObj.pickUpDeliveryPoint.pickupPoint.countryMaster = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.pickupPoint.countryMaster.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.pickupPoint.countryMaster.id
                        };
                    }
                    if (tmpObj.pickUpDeliveryPoint.pickupPoint.partyDetail) {
                        tmpObj.pickUpDeliveryPoint.pickupPoint.partyDetail = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.pickupPoint.partyDetail.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.pickupPoint.partyDetail.id
                        };
                    }


                    var partyAddressList = tmpObj.pickUpDeliveryPoint.pickupPoint.partyAddressList;
                    if (partyAddressList) {
                        for (var j = 0; j < partyAddressList.length; j++) {
                            if (partyAddressList[j].stateMaster) {
                                partyAddressList[j].stateMaster = {
                                    "versionLock": partyAddressList[j].stateMaster.versionLock,
                                    "id": partyAddressList[j].stateMaster.id
                                }
                            }
                            if (partyAddressList[j].cityMaster) {
                                partyAddressList[j].cityMaster = {
                                    "versionLock": partyAddressList[j].cityMaster.versionLock,
                                    "id": partyAddressList[j].cityMaster.id
                                }
                            }
                            if (partyAddressList[j].partyCountryField) {
                                partyAddressList[j].partyCountryField = {
                                    "versionLock": partyAddressList[j].partyCountryField.versionLock,
                                    "id": partyAddressList[j].partyCountryField.id
                                }
                            }

                        }
                    }

                    var partyServiceList = tmpObj.pickUpDeliveryPoint.pickupPoint.partyServiceList;

                    if (partyServiceList) {
                        for (var j = 0; j < partyServiceList.length; j++) {
                            if (partyServiceList[j].serviceMaster) {
                                partyServiceList[j].serviceMaster = {
                                    "versionLock": partyServiceList[j].serviceMaster.versionLock,
                                    "id": partyServiceList[j].serviceMaster.id
                                }
                            }
                            if (partyServiceList[j].tosMaster) {
                                partyServiceList[j].tosMaster = {
                                    "versionLock": partyServiceList[j].tosMaster.versionLock,
                                    "id": partyServiceList[j].tosMaster.id
                                }
                            }
                            if (partyServiceList[j].locationMaster) {
                                partyServiceList[j].locationMaster = {
                                    "versionLock": partyServiceList[j].locationMaster.versionLock,
                                    "id": partyServiceList[j].locationMaster.id
                                }
                            }
                            if (partyServiceList[j].salesman) {
                                partyServiceList[j].salesman = {
                                    "versionLock": partyServiceList[j].salesman.versionLock,
                                    "id": partyServiceList[j].salesman.id
                                }
                            }
                            if (partyServiceList[j].customerService) {
                                partyServiceList[j].customerService = {
                                    "versionLock": partyServiceList[j].customerService.versionLock,
                                    "id": partyServiceList[j].customerService.id
                                }
                            }


                        }

                    }


                }

                if (tmpObj.pickUpDeliveryPoint.pickUpPlace) {
                    if (tmpObj.pickUpDeliveryPoint.pickUpPlace.state) {
                        tmpObj.pickUpDeliveryPoint.pickUpPlace.state = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.pickUpPlace.state.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.pickUpPlace.state.id
                        };
                    }
                    if (tmpObj.pickUpDeliveryPoint.pickUpPlace.city) {
                        tmpObj.pickUpDeliveryPoint.pickUpPlace.city = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.pickUpPlace.city.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.pickUpPlace.city.id
                        };
                    }
                    if (tmpObj.pickUpDeliveryPoint.pickUpPlace.country) {
                        tmpObj.pickUpDeliveryPoint.pickUpPlace.country = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.pickUpPlace.country.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.pickUpPlace.country.id
                        };
                    }

                }

                if (tmpObj.pickUpDeliveryPoint.deliveryPlace) {
                    if (tmpObj.pickUpDeliveryPoint.deliveryPlace.state) {
                        tmpObj.pickUpDeliveryPoint.deliveryPlace.state = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.deliveryPlace.state.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.deliveryPlace.state.id
                        };
                    }
                    if (tmpObj.pickUpDeliveryPoint.deliveryPlace.city) {
                        tmpObj.pickUpDeliveryPoint.deliveryPlace.city = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.deliveryPlace.city.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.deliveryPlace.city.id
                        };
                    }

                }

                if (tmpObj.pickUpDeliveryPoint.deliveryPoint) {
                    if (tmpObj.pickUpDeliveryPoint.deliveryPoint.portMaster) {
                        tmpObj.pickUpDeliveryPoint.deliveryPoint.portMaster = {
                            "versionLock": tmpObj.pickUpDeliveryPoint.deliveryPoint.portMaster.versionLock,
                            "id": tmpObj.pickUpDeliveryPoint.deliveryPoint.portMaster.id
                        };
                    }

                }



            }

            /*// for dynamic column
            if(tmpObj.consolDocument!=undefined && tmpObj.consolDocument!=null
                    && tmpObj.consolDocument!=""){
                tmpObj.consolDocument.isAgreed='No';
                tmpObj.consolDocument.isMarksNo='Yes';
            }*/

            if (tmpObj.connectionList != null && tmpObj.connectionList.length != undefined) {

                for (var i = 0; i < tmpObj.connectionList.length; i++) {
                    tmpObj.connectionList[i].eta = $rootScope.sendApiDateAndTime(tmpObj.connectionList[i].eta);
                    tmpObj.connectionList[i].etd = $rootScope.sendApiDateAndTime(tmpObj.connectionList[i].etd);
                    tmpObj.connectionList[i].move = tmpObj.connectionList[i].move;
                    tmpObj.connectionList[i].flightVoyageNo = tmpObj.connectionList[i].flightVoyageNo;
                    tmpObj.connectionList[i].connectionStatus = tmpObj.connectionList[i].connectionStatus;

                    if (tmpObj.connectionList[i].pol) {
                        tmpObj.connectionList[i].pol = {
                            "versionLock": tmpObj.connectionList[i].pol.versionLock,
                            "id": tmpObj.connectionList[i].pol.id
                        }
                    }
                    if (tmpObj.connectionList[i].pod) {
                        tmpObj.connectionList[i].pod = {
                            "versionLock": tmpObj.connectionList[i].pod.versionLock,
                            "id": tmpObj.connectionList[i].pod.id
                        }
                    }
                    if (tmpObj.connectionList[i].carrierMaster) {
                        tmpObj.connectionList[i].carrierMaster = {
                            "versionLock": tmpObj.connectionList[i].carrierMaster.versionLock,
                            "id": tmpObj.connectionList[i].carrierMaster.id
                        }
                    }

                }

            }

            if (tmpObj.chargeList != null && tmpObj.chargeList.length != undefined) {

                for (var i = 0; i < tmpObj.chargeList.length; i++) {

                    if (tmpObj.chargeList[i].chargeMaster) {
                        tmpObj.chargeList[i].chargeMaster = {
                            "versionLock": tmpObj.chargeList[i].chargeMaster.versionLock,
                            "id": tmpObj.chargeList[i].chargeMaster.id
                        }
                    }
                    if (tmpObj.chargeList[i].currency) {
                        tmpObj.chargeList[i].currency = {
                            "versionLock": tmpObj.chargeList[i].currency.versionLock,
                            "id": tmpObj.chargeList[i].currency.id
                        }
                    }
                }

            }


            if (tmpObj.shipmentLinkList != null && tmpObj.shipmentLinkList.length != undefined) {

                for (var i = 0; i < tmpObj.shipmentLinkList.length; i++) {
                    tmpObj.shipmentLinkList[i].serviceUid = tmpObj.shipmentLinkList[i].serviceUid;
                    tmpObj.shipmentLinkList[i].shipmentUid = tmpObj.shipmentLinkList[i].shipmentUid;

                    if (tmpObj.shipmentLinkList[i].company) {
                        tmpObj.shipmentLinkList[i].company = {
                            "versionLock": tmpObj.shipmentLinkList[i].company.versionLock,
                            "id": tmpObj.shipmentLinkList[i].company.id
                        }
                    }
                    if (tmpObj.shipmentLinkList[i].country) {
                        tmpObj.shipmentLinkList[i].country = {
                            "versionLock": tmpObj.shipmentLinkList[i].country.versionLock,
                            "id": tmpObj.shipmentLinkList[i].country.id
                        }
                    }
                    if (tmpObj.shipmentLinkList[i].location) {
                        tmpObj.shipmentLinkList[i].location = {
                            "versionLock": tmpObj.shipmentLinkList[i].location.versionLock,
                            "id": tmpObj.shipmentLinkList[i].location.id
                        }
                    }

                    if (tmpObj.shipmentLinkList[i].service) {
                        tmpObj.shipmentLinkList[i].service = {
                            "versionLock": tmpObj.shipmentLinkList[i].service.versionLock,
                            "id": tmpObj.shipmentLinkList[i].service.id
                        }
                    }

                }

            }


            if (tmpObj.eventList != undefined && tmpObj.eventList != null && tmpObj.eventList.length != 0) {
                for (var j = 0; j < tmpObj.eventList.length; j++) {

                    tmpObj.eventList[j].eventDate = $rootScope.sendApiStartDateTime(tmpObj.eventList[j].eventDate);
                    if (tmpObj.eventList[j].followUpRequired == true || tmpObj.eventList[j].followUpRequired == 'Yes') {
                        tmpObj.eventList[j].followUpRequired = 'Yes';
                        tmpObj.eventList[j].followUpDate = $rootScope.sendApiDateAndTime(tmpObj.eventList[j].followUpDate);
                    } else {
                        tmpObj.eventList[j].followUpRequired = 'No';
                        tmpObj.eventList[j].followUpDate = null;
                    }

                    tmpObj.eventList[j].isCompleted = tmpObj.eventList[j].isCompleted ? 'Yes' : 'No';


                    if (tmpObj.eventList[j].eventMaster) {
                        tmpObj.eventList[j].eventMaster = {
                            "versionLock": tmpObj.eventList[j].eventMaster.versionLock,
                            "id": tmpObj.eventList[j].eventMaster.id
                        }
                    }

                }
            }

            if (tmpObj.serviceMaster.importExport == 'Export' && tmpObj.etd != null) {
                tmpObj.consolDocument.documentReqDate = $rootScope.sendApiDateAndTime($rootScope.dateAndTimeToString(tmpObj.etd));
            } else if (tmpObj.serviceMaster.importExport == 'Import' && tmpObj.eta != null) {
                tmpObj.consolDocument.documentReqDate = $rootScope.sendApiDateAndTime($rootScope.dateAndTimeToString(tmpObj.eta));
            } else {
                tmpObj.consolDocument.documentReqDate = $rootScope.sendApiDateAndTime($rootScope.dateAndTimeToString(new Date()));
            }

            return tmpObj;
        }

        /* Save method for consol */
        $scope.saveConsol = function() {
            try {

                console.log("Save Method is called.");
                $rootScope.clientMessage = null;
                $scope.spinner = true;
                if ($scope.validateConsol(0)) {
                    if (!$scope.consolDimension.dimensionCheck()) {
                        $scope.Tabs = 'documents';
                        $scope.spinner = false;
                        return false;
                    }
                    // For Direct Import Dimension is not mandatory
                    /*if($scope.consol.serviceMaster.importExport != 'Import') {
                        if($scope.consol.consolDocument!=undefined && $scope.consol.consolDocument.dimensionList==undefined  || $scope.consol.consolDocument.dimensionList==null ||
                                $scope.consol.consolDocument.dimensionList.length==0){
                                 $rootScope.clientMessage = $rootScope.nls["ERR7999"];		
                                 return false;
                            }	
                    }*/

                    if (!$scope.isConnectionCheck()) {
                        $scope.Tabs = 'general';
                        $scope.spinner = false;
                        return false;
                    }

                    if ($stateParams.action != "ADDWIZ") {
                        if (!$scope.isChargeCheck()) {
                            $scope.Tabs = 'rates';
                            $scope.spinner = false;
                            return false;
                        }
                        /*if(!$scope.isEventCheck()){
                        	
                            $scope.spinner=false;
                            return false;
                        }
                        if(!$scope.isReferenceCheck()){
                        	
                            $scope.spinner=false;
                            return false;
                        }
                        if(!$scope.isAttachmentCheck()){
                            $scope.Tabs='attachMent';
                            $scope.spinner=false;
                            return false;
                        }*/

                        if ($scope.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 0)) {
                            $scope.Tabs = 'pickup';
                            $scope.spinner = false;
                            return false;
                        }
                    } else {
                        $scope.consol.connectionList = null;
                        $scope.consol.eventList = null;
                        $scope.consol.referenceList = null;
                        $scope.consol.chargeList = null;
                        $scope.consol.consolAttachmentList = null;
                    }

                    if ($scope.consol.shipmentLinkList.length <= 0) {
                        console.log("Please add atleast one service and document");
                        Notification.error($rootScope.nls["ERR96524"])
                        $scope.Tabs = 'consol';
                        $scope.spinner = false;
                        return false;
                    }
                    if ($scope.consol.connectionList != null && $scope.consol.connectionList.length > 0 && $scope.consol.eta != null) {
                        var connEta = $rootScope.convertToDate($scope.consol.connectionList[$scope.consol.connectionList.length - 1].eta);
                        var consolEta = $rootScope.convertToDate($scope.consol.eta);
                        if (consolEta < connEta) {
                            Notification.error($rootScope.nls["ERR96541"]);
                            $scope.Tabs = 'general';
                            $scope.spinner = false;
                            $rootScope.navigateToNextField('etaMoreInfoConsol');
                            return false;
                        }
                    }


                    var connectionRequired = $rootScope.appMasterData['connection.required'];
                    $scope.connectionRequiredFlag = false;
                    if (connectionRequired.toLowerCase() == 'true' || connectionRequired == true) {


                        if ($scope.consol.origin != null && $scope.consol.origin.id != null) {
                            if ($scope.consol.pol != null && $scope.consol.pol.id != null) {
                                if ($scope.consol.origin.id != $scope.consol.pol.id) {
                                    $scope.connectionRequiredFlag = true;
                                }
                            }
                        }

                        if ($scope.consol.destination != null && $scope.consol.destination.id != null) {
                            if ($scope.consol.pod != null && $scope.consol.pod.id != null) {
                                if ($scope.consol.destination.id != $scope.consol.pod.id) {
                                    $scope.connectionRequiredFlag = true;
                                }
                            }
                        }


                        //which is used for making event in disable mode while coming from shipment to consol
                        if ($scope.consol.eventList != undefined && $scope.consol.eventList != null && $scope.consol.eventList.length != undefined && $scope.consol.eventList.length > 0) {
                            angular.forEach($scope.consol.eventList, function(dataObj, index) {
                                if (dataObj.id != undefined && dataObj.id != null && dataObj.id == -1) {
                                    dataObj.id = null;
                                }
                            });
                        }

                        if ($scope.connectionRequiredFlag) {
                            if ($scope.consol.connectionList == undefined && $scope.consol.connectionList == null || $scope.consol.connectionList.length == 0) {
                                $scope.consol.connectionList = [{}];
                                Notification.error($rootScope.nls["ERR90364"]);
                                $scope.Tabs = 'general';
                                $scope.spinner = false;
                                return false;
                            }
                        }
                    }


                    $scope.tmpObj = $scope.getTempObject($scope.consol);
                    ConsolAdd.save($scope.tmpObj).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.showResponse(data.responseObject, true);
                        } else {
                            console.log("Consol added Failed " + data.responseDescription)
                            $scope.spinner = false;
                        }
                    }, function(error) {
                        console.log("Consol added Failed : " + error)
                        $scope.spinner = false;
                    });



                } else {
                    console.log("Invalid Form Submission.");
                    $scope.spinner = false;
                }

            } catch (err) {
                $scope.spinner = false;
                console.log("Invalid Form Submission while saving consol", err);
            }
        }

        $scope.validateConsolOthers = function() {

            if (!$scope.consolDimension.dimensionCheck()) {
                $scope.Tabs = 'documents';
                $scope.spinner = false;
                return false;
            }
            // For Direct Import Dimension is not mandatory
            //As discussed with shanmugam, we removed dimension manatory for export
            /*if($scope.consol.serviceMaster.importExport != 'Import') {
            if($scope.consol.consolDocument!=undefined && $scope.consol.consolDocument.dimensionList==undefined  || $scope.consol.consolDocument.dimensionList==null ||
                    $scope.consol.consolDocument.dimensionList.length==0){
                     $rootScope.clientMessage = $rootScope.nls["ERR7999"];		
                     return false;
                }
        }*/

            if (!$scope.isConnectionCheck()) {
                $scope.Tabs = 'general';
                $scope.spinner = false;
                return false;
            }

            if (!$scope.isChargeCheck()) {
                $scope.Tabs = 'rates';
                $scope.spinner = false;
                return false;
            }

            /*if($scope.consol.isEventCheck()!=undefined && !$scope.consol.isEventCheck()){
                Notification.error($rootScope.nls["ERR90563"]);
                $scope.spinner=false;
                return false;
            }
        	
            if($scope.consol.isReferenceCheck()!=undefined && !$scope.consol.isReferenceCheck()){
                Notification.error($rootScope.nls["ERR90573"]);
                $scope.spinner=false;
                return false;
            }
        	
            if($scope.consol.isAttachmentCheck()!=undefined && !$scope.consol.isAttachmentCheck()){
                $scope.Tabs='attachMent';
                Notification.error($rootScope.nls["ERR250"]);
                $scope.spinner=false;
                return false;
            }
            */

            if ($scope.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 0)) {
                $scope.Tabs = 'pickup';
                $scope.spinner = false;
                return false;
            }
            if ($scope.consol.connectionList != null && $scope.consol.connectionList != undefined && $scope.consol.connectionList.length > 0 && $scope.consol.eta != null) {
                var connEta = $rootScope.convertToDate($scope.consol.connectionList[$scope.consol.connectionList.length - 1].eta);
                var consolEta = $rootScope.convertToDate($scope.consol.eta);
                if (consolEta < connEta) {
                    Notification.error($rootScope.nls["ERR96541"]);
                    $scope.Tabs = 'general';
                    $scope.spinner = false;
                    $rootScope.navigateToNextField('etaMoreInfoConsol');
                    return false;
                }
            }



            var connectionRequired = $rootScope.appMasterData['connection.required'];
            $scope.connectionRequiredFlag = false;
            if (connectionRequired.toLowerCase() == 'true' || connectionRequired == true) {


                if ($scope.consol.origin != null && $scope.consol.origin.id != null) {
                    if ($scope.consol.pol != null && $scope.consol.pol.id != null) {
                        if ($scope.consol.origin.id != $scope.consol.pol.id) {
                            $scope.connectionRequiredFlag = true;
                        }
                    }
                }

                if ($scope.consol.destination != null && $scope.consol.destination.id != null) {
                    if ($scope.consol.pod != null && $scope.consol.pod.id != null) {
                        if ($scope.consol.destination.id != $scope.consol.pod.id) {
                            $scope.connectionRequiredFlag = true;
                        }
                    }
                }


                if ($scope.connectionRequiredFlag) {
                    if ($scope.consol.connectionList == undefined && $scope.consol.connectionList == null || $scope.consol.connectionList.length == 0) {
                        $scope.consol.connectionList = [{}];
                        Notification.error($rootScope.nls["ERR90364"]);
                        $scope.Tabs = 'general';
                        $scope.spinner = false;
                        return false;
                    }
                }
            }
            if ($scope.consol.shipmentLinkList.length <= 0) {
                console.log("Please add atleast one service and document");
                $scope.Tabs = 'consol';
                $scope.spinner = false;
                Notification.error($rootScope.nls["ERR96524"]);
                return false;
            }
            return true;
        }


        $scope.updateConsol = function(forWhichPurpose, obj) {
            try {
                console.log("Update Method is called.");
                $rootScope.clientMessage = null;
                $scope.spinner = true;
                if (forWhichPurpose != undefined && forWhichPurpose == 'MAWB' && obj != undefined) {
                    $scope.consol = obj;
                }
                if ($scope.validateConsol(0) && $scope.validateConsolOthers()) {
                    $scope.tmpObj = $scope.getTempObject($scope.consol);
                    ConsolEdit.update($scope.tmpObj).$promise.then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                console.log("Consol updated Successfully")
                                if (forWhichPurpose != undefined && forWhichPurpose == 'MAWB') {
                                    $scope.spinner = false;
                                    $rootScope.$broadcast('consol-update-for-mawb');
                                    return;
                                } else if (forWhichPurpose != undefined && forWhichPurpose == 'WIN') {
                                    $scope.spinner = false;
                                    $scope.openWinWeb();
                                    return;
                                } else if (forWhichPurpose != undefined && forWhichPurpose == 'DO') {
                                    $scope.spinner = false;
                                    $rootScope.$broadcast("consol-update-for-do", {
                                        state: data.responseObject
                                    });
                                    return
                                } else {
                                    $scope.showResponse(data.responseObject, false);
                                }
                            } else {
                                console.log("Consol updated Failed " +
                                    data.responseDescription)
                                $scope.spinner = false;
                            }
                            $scope.contained_progressbar.complete();
                            angular.element(".panel-body").animate({
                                scrollTop: 0
                            }, "slow");
                        },
                        function(error) {
                            $scope.spinner = false;
                        });
                } else {
                    console.log("Invalid Form Submission.");
                    $scope.spinner = false;
                }

            } catch (err) {
                $scope.spinner = false;
                console.log("Invalid Form Submission while updating consol", err);
            }
        }


        $scope.convertGrossWeightPoundToKg = function() {

            $scope.consol.consolDocument.grossWeight = 0.0;
            if ($scope.consol.consolDocument.grossWeightInPound != null) {
                $scope.consol.consolDocument.grossWeight = Math.round(($scope.consol.consolDocument.grossWeightInPound * 0.45359237) * 100) / 100;

            }
            $scope.calculateChargebleWeight();
        }


        $scope.convertGrossWeightKgToPound = function() {
            $scope.consol.consolDocument.grossWeightInPound = 0.0;
            if ($scope.consol.consolDocument.grossWeight != null) {
                $scope.consol.consolDocument.grossWeightInPound = Math.round(($scope.consol.consolDocument.grossWeight * 2.20462) * 100) / 100;

            }
            $scope.calculateChargebleWeight();
        }

        $scope.convertVolumeWeightPoundToKg = function() {
            $scope.consol.consolDocument.volumeWeight = 0.0;
            if ($scope.consol.consolDocument.volumeWeightInPound != null) {
                $scope.consol.consolDocument.volumeWeight = Math.round(($scope.consol.consolDocument.volumeWeightInPound * 0.45359237) * 100) / 100;

            }
            $scope.calculateChargebleWeight();
        }

        $scope.convertVolumeWeightKgToPound = function() {
            $scope.consol.consolDocument.volumeWeightInPound = 0.0;
            if ($scope.consol.consolDocument.volumeWeight != null) {
                $scope.consol.consolDocument.volumeWeightInPound = Math.round(($scope.consol.consolDocument.volumeWeight * 2.20462) * 100) / 100;

            }
            $scope.calculateChargebleWeight();
        }


        $scope.calculateChargebleWeight = function() {

            if (+($scope.consol.consolDocument.volumeWeight) > +($scope.consol.consolDocument.grossWeight)) {
                $scope.consol.consolDocument.chargeableWeight = $scope.consol.consolDocument.volumeWeight;
            } else {
                $scope.consol.consolDocument.chargeableWeight = $scope.consol.consolDocument.grossWeight;
            }
        }



        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Closed') {
                return 'blockedtype';
            } else if (data == 'Cancelled') {
                return 'hiddentype';
            }
        }

        /* Consol connection tab started */



        /*
         * $scope.changeConnMove=function(){ $scope.errorMap = new Map();
         * if($scope.consolConnection.move=="Air"){
         * 
         * $scope.consolConnection.pol={}; $scope.consolConnection.pod={};
         * $scope.consolConnection.carrierMaster={};
         *  }
         * 
         * if($scope.consolConnection.move=="Ocean"){
         * 
         * 
         * $scope.consolConnection.pol={}; $scope.consolConnection.pod={};
         * $scope.consolConnection.carrierMaster={};
         *  }
         * 
         * if($scope.consolConnection.move=="Road"){
         * 
         * $scope.consolConnection.pol={}; $scope.consolConnection.pod={};
         * 
         * $scope.consolConnection.carrierMaster={};
         *  } if($scope.consolConnection.move=="Rail"){
         * 
         * $scope.consolConnection.pol={}; $scope.consolConnection.pod={};
         * $scope.consolConnection.carrierMaster={};
         * 
         *  }
         *  }
         */
        // Connection POL list
        $scope.connPolConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showConPol = function(name) {
            if ($scope.consolConnection.move == undefined || $scope.consolConnection.move == null || $scope.consolConnection.move == '') {
                console.log($rootScope.nls["ERR90031"]);
                $scope.errorMap.put("connection.move", $rootScope.nls["ERR90031"]);
                return false;
            }
            console.log("Showing List of Port Of Loading.....");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Port Of Loading";
            $scope.ajaxConnPolEvent(null);
        };

        $scope.ajaxConnPolEvent = function(object) {
            console.log("ajaxPortOfLoadingEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consolConnection.move
            }, $scope.searchDto).$promise.then(
                function(data) {


                    if (data.responseCode == "ERR0") {
                        $scope.connPolList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consolConnection.pod == undefined || $scope.consolConnection.pod == null) {
                                $scope.connPolList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consolConnection.pod.id) {
                                        $scope.connPolList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.connPolList;
                        // $scope.showConnPol=true;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );
        }

        $scope.selectedConnPol = function(obj) {
            console.log("Selected PortOfLoading  : ", obj);
            $scope.consolConnection.pol = obj;
            $scope.validateConsolConnection(3);
            $scope.cancelConnPol();
            document.getElementById('consolConnectionPol').focus();
        };

        $scope.cancelConnPol = function() {
            console.log("PortOfLoading List Cancelled....");
            $scope.showConnPol = false;
        }



        /* Connection Port Of Discharge - List Picker */

        $scope.connPodConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showConPod = function(name) {
            if ($scope.consolConnection.move == undefined || $scope.consolConnection.move == null || $scope.consolConnection.move == '') {
                $scope.errorMap.put("connection.move", $rootScope.nls["ERR90031"]);
                return false;
            }
            console.log("Showing List of Port Of Discharge.....");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Port Of Discharge";
            $scope.ajaxConnPodEvent(null);
        };

        $scope.ajaxConnPodEvent = function(object) {
            console.log("ajaxPortOfLoadingEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            PortByTransportMode.fetch({
                "transportMode": $scope.consolConnection.move
            }, $scope.searchDto).$promise.then(
                function(data) {


                    if (data.responseCode == "ERR0") {
                        $scope.connPodList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consolConnection.pol == undefined || $scope.consolConnection.pol == null) {
                                $scope.connPodList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consolConnection.pol.id) {
                                        $scope.connPodList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        $scope.showConnPod = true;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );
        }

        $scope.selectedConnPod = function(obj) {
            $scope.consolConnection.pod = obj;
            $scope.validateConsolConnection(4);
            $scope.cancelConnPod();
            document.getElementById('consolConnectionPod').focus();
        };

        $scope.cancelConnPod = function() {
            console.log("PortOfDischarging List Cancelled....");
            $scope.showConnPod = false;
        }

        // Connection carrier list
        $scope.showConnCarrierList = false;

        $scope.connCarrierlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "carrierName",
                    seperator: false
                },
                {
                    "title": "carrierCode",
                    seperator: true
                }
            ]
        }

        $scope.showConCarrier = function(name) {

            console.log("showing list of carriers......");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Carrier";
            $scope.ajaxConnCarrierEvent(null);
        };

        $scope.ajaxConnCarrierEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            CarrierByTransportMode.fetch({
                "transportMode": $scope.consolConnection.move
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.connCarrierList = data.responseObject.searchResult;
                        $scope.showConnCarrierList = true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier List Based on Air');
                }
            );
        }

        $scope.selectedConnCarrier = function(obj) {
            $scope.consolConnection.carrierMaster = obj;
            $scope.validateConsolConnection(7);
            $scope.cancelConnCarrier();
            document.getElementById('consolConnectionCarrier').focus();
        };

        $scope.cancelConnCarrier = function() {
            $scope.showConnCarrierList = false;
        }

        // Validation for Connection//


        $scope.validateConsolConnection = function(validateCode) {


            $scope.errorMap = new Map();
            if (validateCode == 0 || validateCode == 1) {

                if ($scope.consolConnection.move == undefined || $scope.consolConnection.move == null ||
                    $scope.consolConnection.move == null ||
                    $scope.consolConnection.move == "") {
                    console.log($rootScope.nls["ERR95600"]);
                    $scope.errorMap.put("conn.move", $rootScope.nls["ERR95600"]);
                    return false;
                }


            }
            if (validateCode == 0 || validateCode == 2) {

                if ($scope.consolConnection.connectionStatus == undefined || $scope.consolConnection.connectionStatus == null ||
                    $scope.consolConnection.connectionStatus == null ||
                    $scope.consolConnection.connectionStatus == "") {
                    $scope.errorMap.put("connection.move", $rootScope.nls["ERR95601"]);
                    return false;
                }


            }
            if (validateCode == 0 || validateCode == 3) {

                if ($scope.consolConnection.pol == undefined || $scope.consolConnection.pol == null ||
                    $scope.consolConnection.pol.id == null ||
                    $scope.consolConnection.pol.id.trim == "") {
                    $scope.errorMap.put("connection.pol", $rootScope.nls["ERR95602"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consolConnection.pol.status)) {
                    $scope.consolConnection.pol = null;
                    $scope.errorMap.put("connection.pol", $rootScope.nls["ERR95603"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consolConnection.pol.status)) {
                    $scope.consolConnection.pol = null;
                    $scope.errorMap.put("connection.pol", $rootScope.nls["ERR95604"]);
                    return false;
                }

            }
            if (validateCode == 0 || validateCode == 4) {

                if ($scope.consolConnection.pod == undefined || $scope.consolConnection.pod == null ||
                    $scope.consolConnection.pod.id == null ||
                    $scope.consolConnection.pod.id.trim == "") {
                    $scope.errorMap.put("connection.pod", $rootScope.nls["ERR95605"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consolConnection.pod.status)) {
                    $scope.consolConnection.pod = null;
                    $scope.errorMap.put("connection.pod", $rootScope.nls["ERR95606"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consolConnection.pod.status)) {
                    $scope.consolConnection.pod = null;
                    $scope.errorMap.put("connection.pod", $rootScope.nls["ERR95607"]);
                    return false;
                }

            }

            if (validateCode == 0 || validateCode == 5) {

                if ($scope.consolConnection.etd == undefined ||
                    $scope.consolConnection.etd == null ||
                    $scope.consolConnection.etd == "") {
                    $scope.errorMap.put("connection.etd", $rootScope.nls["ERR95609"]);
                    return false;
                }

                if ($scope.consolConnection.eta != undefined && $scope.consolConnection.eta != null) {

                    if ($scope.consolConnection.etd > $scope.consolConnection.eta) {
                        $scope.errorMap.put("connection.etd", $rootScope.nls["ERR95610"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 6) {


                if ($scope.consolConnection.eta == undefined ||
                    $scope.consolConnection.eta == null ||
                    $scope.consolConnection.eta == "") {
                    $scope.errorMap.put("connection.eta", $rootScope.nls["ERR95608"]);
                    return false;
                }

                if ($scope.consolConnection.etd != undefined && $scope.consolConnection.etd != null) {


                    if ($scope.consolConnection.etd > $scope.consolConnection.eta) {
                        $scope.errorMap.put("connection.eta", $rootScope.nls["ERR95610"]);
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 7) {


                if ($scope.consolConnection.carrierMaster == undefined || $scope.consolConnection.carrierMaster == null ||
                    $scope.consolConnection.carrierMaster.id == null ||
                    $scope.consolConnection.carrierMaster.id.trim == "") {
                    console.log($rootScope.nls["ERR95611"]);
                    $scope.errorMap.put("connection.carrierMaster", $rootScope.nls["ERR95611"]);
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consolConnection.carrierMaster.status)) {
                    $scope.consolConnection.carrierMaster = null;
                    console.log($rootScope.nls["ERR95612"]);
                    $scope.errorMap.put("connection.carrierMaster", $rootScope.nls["ERR95612"]);
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consolConnection.carrierMaster.status)) {
                    $scope.consolConnection.carrierMaster = null;
                    console.log($rootScope.nls["ERR95613"]);
                    $scope.errorMap.put("connection.carrierMaster", $rootScope.nls["ERR95613"]);
                    return false;
                }


            }
            if (validateCode == 0 || validateCode == 8) {


                if ($scope.consolConnection.flightVoyageNo == undefined ||
                    $scope.consolConnection.flightVoyageNo == null ||
                    $scope.consolConnection.flightVoyageNo == "") {
                    console.log($rootScope.nls["ERR95614"]);
                    $scope.errorMap.put("connection.carrierOrVoyageN", $rootScope.nls["ERR95614"]);
                    return false;
                }

                var regexp = new RegExp("[A-Za-z0-9 ]{0,10}");
                if (!regexp.test($scope.consolConnection.flightVoyageNo)) {
                    console.log($rootScope.nls["ERR95615"]);
                    $scope.errorMap.put("connection.carrierOrVoyageN", $rootScope.nls["ERR95615"]);
                    return false;
                }

            }


            return true;
        }


        $scope.serialNo = function(index) {
                var index = index + 1;
                return index;
            }
            /* Consol connection tab ended */



        // Attachment config
        $scope.Tabs = "general";
        $scope.attachConfig = {
            "isEdit": true, // false for view only mode
            "isDelete": true, //
            "page": "shipment",
            "columnDefs": [{
                    "name": "Document Name",
                    "model": "documentMaster.documentName",
                    "type": "text"
                },
                {
                    "name": "Reference #",
                    "model": "refNo",
                    "type": "text"
                },
                {
                    "name": "Customs",
                    "model": "customs",
                    "type": "text"
                },
                {
                    "name": "Unprotected File Name",
                    "model": "unprotectedFileName",
                    "type": "file"
                },
                {
                    "name": "Protected File Name",
                    "model": "protectedFileName",
                    "type": "file"
                },
                {
                    "name": "Actions",
                    "model": "action",
                    "type": "action"
                }
            ]
        }



        $scope.tableChargeState = true;


        /*
         * $scope.viewAttachConfig = { "isEdit":false, //false for view only mode
         * "isDelete":true, // "page":"shipment", "columnDefs":[
         * 
         * {"name":"Document Name", "model":"docName","type":"text"}, {"name":"Reference
         * #", "model":"refNo","type":"text"}, {"name":"Customs",
         * "model":"custom","type":"text"}, {"name":"Unprotected File Name",
         * "model":"unProcFile","type":"text"}, {"name":"Protected File Name",
         * "model":"procFile","type":"text"}, {"name":"Actions","model":"action",
         * "type":"action"} ] }
         */
        /*
         * $scope.manipulateAttach = function(param){ console.log("param",param);
         * if(param.index == null) { $scope.attachConfig.data.push(param.data); } else {
         * $scope.attachConfig.data.splice(param.index, 1, param.data); } }
         * 
         * $scope.removeAttach = function(param){
         * $scope.attachConfig.data.splice(param.index,1); }
         */

        //	$scope.downloadAttach = function(param){
        //		console.log("download ",param);
        //
        //		if(param.data.id!=null && (param.data.protectedFile == null || param.data.unprotectedFile == null)){
        //			console.log("API CALL")
        //			if(param.data.swapped==undefined) {
        //				param.data.swapped = false;
        //			}
        //			$http({
        //				url: $rootScope.baseURL+'/api/v1/consol/files/'+param.data.id+'/'+param.type+'/'+param.data.swapped,
        //				method: "GET",
        //				responseType: 'arraybuffer'
        //			}).success(function (data, status, headers, config) {
        //				console.log("hiddenElement ",data)
        //				var blob = new Blob([data], {});
        //				console.log("blob ", blob);
        //				if(param.type == 'unprotectedFileName')
        //					saveAs(blob, param.data.unprotectedFileName);
        //
        //				if(param.type == 'protectedFileName')
        //					saveAs(blob, param.data.protectedFileName);
        //
        //			}).error(function (data, status, headers, config) {
        //				// upload failed
        //			});
        //
        //
        //		}else{
        //			console.log("NO API CALL", param);
        //			if(param.type == "unprotectedFileName")
        //				saveAs(param.data.tmpFile2, param.data.unprotectedFileName);
        //			else if(param.type == "protectedFileName")
        //				saveAs(param.data.tmpFile1, param.data.protectedFileName);
        //		}
        //	}



        $scope.discard = function() {
            $scope.addDiemension = true;
        }

        $scope.addratesRow = function() {
            $scope.addrates = false;
        };
        $scope.closeRates = function() {
            $scope.addrates = true;
        }
        $scope.addeventRow = function() {
            $scope.addEvent = false;
        };
        $scope.closeEvent = function() {
            $scope.addEvent = true;
        }
        $scope.customerlistConfig = {
            search: true
        }
        $scope.shipmentOrigin = function() {
            $scope.showPartyList = true;
        }

        $scope.cancelCustomer = function() {
            $scope.showPartyList = false;
        };

        // Dimension Table


        $scope.calculateDocWeight = function(param) {

            if ($scope.consol.consolDocument.dimensionUnit) {

                $scope.consol.consolDocument.dimGrossWeight = Math.round((param.grossWeight * 0.45359237) * 100) / 100;
                $scope.consol.consolDocument.dimGrossWeightInPound = Math.round(((param.grossWeight * 0.45359237) * 2.20462) * 100) / 100;
            } else {
                $scope.consol.consolDocument.dimGrossWeight = param.grossWeight;
                $scope.consol.consolDocument.dimGrossWeightInPound = Math.round((param.grossWeight * 2.20462) * 100) / 100;
            }

            $scope.consol.consolDocument.dimVolumeWeight = param.volumeWeight;
            $scope.consol.consolDocument.dimNoOfPieces = param.totalPieces;
            $scope.consol.consolDocument.dimVolumeWeightInPound = Math.round((param.volumeWeight * 2.20462) * 100) / 100;
            $scope.assignToConsolLevel($scope.consol.consolDocument);
        };

        //update dimension value top of consol header level
        $scope.assignToConsolLevel = function(consolDocument) {

            if (consolDocument.dimensionList != undefined && consolDocument.dimensionList != null && consolDocument.dimensionList.length != undefined &&

                consolDocument.dimensionList.length > 0 && !isEmptyRow(consolDocument.dimensionList[0])) {

                $scope.consol.consolDocument.noOfPieces = consolDocument.dimNoOfPieces;

                $scope.consol.consolDocument.volumeWeight = consolDocument.dimVolumeWeight;

                $scope.consol.consolDocument.volumeWeightInPound = consolDocument.dimVolumeWeightInPound;

                $scope.consol.consolDocument.grossWeight = consolDocument.dimGrossWeight;

                $scope.consol.consolDocument.grossWeightInPound = consolDocument.dimGrossWeightInPound;
                $scope.calculateDocChargebleWeight();
            }

        }



        $scope.validDocumentDimension = function(param) {

            console.log("table state data", param);

            $scope.documentDimensionValid = param;
        }

        $scope.tableDimensionState = function(param) {
            $scope.validDimension = param;
            console.log("table state", param);
        };

        $scope.fnDimensionUnit = function() {
            if ($scope.consol.dimensionUnit != undefined) {
                if ($scope.consol.dimensionUnit == false) {
                    $scope.consol.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                } else {
                    $scope.consol.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                }

            }

        };


        $scope.bindToConsolWeight = function(consol) {
            if (consol.dimensionUnit == false) {
                $scope.consol.noOfPack = consol.totalDimensionPieces;
                $scope.consol.volumeWeight = consol.totalDimensionvolumeWeight;
                $scope.consol.grossWeight = consol.totalDimensionGrossWeight;
                $scope.consol.volumeWeightInPound = Math.round(($scope.consol.volumeWeight * 2.20462) * 100) / 100;
                $scope.consol.grossWeightInPound = Math.round(($scope.consol.grossWeight * 2.20462) * 100) / 100;
            } else {
                $scope.consol.noOfPack = consol.totalDimensionPieces;
                $scope.consol.grossWeightInPound = consol.totalDimensionvolumeWeight;
                $scope.consol.volumeWeightInPound = consol.totalDimensionGrossWeight;
                $scope.consol.volumeWeight = Math.round(($scope.consol.volumeWeightInPound * 0.45359237) * 100) / 100;
                $scope.consol.grossWeight = Math.round(($scope.consol.grossWeightInPound * 0.45359237) * 100) / 100;


            }

        }

        // Dimension Table Ended

        /*
         * $scope.chargeTable = {}; $scope.chargeTable.dataArr = [ {
         * "charge":{"chargeCode": "DOC", "chargeName": "Document
         * Charge"},"crn":"PROFIT", "freight":"PREPAID","currency":{ "currencyCode":
         * "INR","currencyName": "Indian Rupee"},"roe":"234", "unit":{"unitCode":
         * "SHIP","unitName":
         * "Shipment"},"amtPerUnit":234.32,amt:23.2,locamt:500.23,due:"AGENT" } ]
         */

        $scope.callbackConsol = function(param) {
            console.log("table state", param);
        }


        $scope.tableChargeState = function(param) {
            console.log("table state", param);
        };

        $scope.chargeConsolCb = function(totalAmount) {
            $scope.consol.totalAmount = totalAmount.totalAmount;

        }

        /**
         * Attachment
         */

        $scope.documentListConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "documentName",
                    seperator: false
                },
                {
                    "title": "documentCode",
                    seperator: true
                }
            ]
        }

        $scope.showDocList = function(param) {
            console.log("Showing List of Documents..", param);
            $scope.panelTitle = "Document";
            $scope.selectedItem = -1;
            $scope.showDocumentList = true;
            $scope.ajaxDocumentMaster(null);
        }

        $scope.ajaxDocumentMaster = function(object) {

            console.log("ajaxDocumentMaster ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            DocumentMasterList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.documentMasterList = data.responseObject.searchResult;
                        console.log("$scope.documentMasterList", $scope.documentMasterList);
                        $scope.showDocumentList = true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Currency List');
                }
            );
        }

        $scope.selectedDocumentMaster = function(obj) {
            console.log("Selected Document Master ", obj);
            $scope.documentName = obj;
            $scope.cancelDocumentMasterList();
        };

        $scope.cancelDocumentMasterList = function() {
            console.log("Document List cancelled....");
            $scope.showDocumentList = false;
        }



        $scope.changeStatusUpdateDimension = function() {
            if ($scope.statusUpdateDetail.dimensionUnit == false) {

                $scope.statusUpdateDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            } else {
                $scope.statusUpdateDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            }

            $scope.changeConsolVolume();
        }


        $scope.statusCompleted = false;
        $scope.changeCompleted = function() {
            if ($scope.statusCompleted == false) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR221"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">Cancel</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Proceed</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).
                then(function(value) {
                    if (value == 1) {
                        $scope.statusCompleted = true;
                    } else if (value == 2) {
                        $scope.statusCompleted = false;
                    }
                });
            } else {
                $scope.statusCompleted = false;
            }
        };




        /*
         * Attachment Code Begins
         */




        /*
         * $scope.saveAttachment = function() { console.log("Save Attachment is
         * called........");
         * 
         * $scope.consol.consolAttachmentList = [];
         * 
         * if($scope.attachConfig.data!= null && $scope.attachConfig.data.length != 0){
         * 
         * console.log("There are " +$scope.attachConfig.data.length +"
         * attachments....");
         * 
         * for(var i = 0; i < $scope.attachConfig.data.length; i++) {
         * 
         * var objectToStore = new Object();
         * 
         * objectToStore.id = $scope.attachConfig.data[i].id; objectToStore.refNo =
         * $scope.attachConfig.data[i].refNo; objectToStore.documentMaster =
         * $scope.attachConfig.data[i].documentMaster; objectToStore.customs =
         * $scope.attachConfig.data[i].customs; objectToStore.unprotectedFileName =
         * $scope.attachConfig.data[i].unprotectedFileName;
         * objectToStore.unprotectedFileContentType =
         * $scope.attachConfig.data[i].unprotectedFileContentType;
         * objectToStore.unprotectedFile = $scope.attachConfig.data[i].unprotectedFile;
         * objectToStore.protectedFileName =
         * $scope.attachConfig.data[i].protectedFileName;
         * objectToStore.protectedFileContentType =
         * $scope.attachConfig.data[i].protectedFileContentType;
         * objectToStore.protectedFile = $scope.attachConfig.data[i].protectedFile;
         * objectToStore.systemTrack = $scope.attachConfig.data[i].systemTrack;
         * objectToStore.versionLock = $scope.attachConfig.data[i].versionLock;
         * objectToStore.swapped = $scope.attachConfig.data[i].swapped;
         * 
         * 
         * $scope.consol.consolAttachmentList.push(objectToStore); } } else {
         * console.log("There are no attachments to save...."); }
         * 
         * console.log("Save Attachment is executed successfully...."); }
         * 
         * 
         * $scope.populateAttachment = function(data) {
         * 
         * console.log("Populating Attachment....");
         * 
         * if($scope.consol!=undefined && $scope.consol!=null &&
         * $scope.consol.consolAttachmentList != null &&
         * $scope.consol.consolAttachmentList.length != 0){
         * 
         * console.log("There are " + $scope.consol.consolAttachmentList.length + "
         * attachments.....");
         * 
         * for(var i = 0; i < $scope.consol.consolAttachmentList.length; i++) { var
         * displayObject = new Object();
         * 
         * displayObject.id = $scope.consol.consolAttachmentList[i].id;
         * displayObject.refNo = $scope.consol.consolAttachmentList[i].refNo;
         * displayObject.documentMaster =
         * $scope.consol.consolAttachmentList[i].documentMaster; displayObject.customs =
         * $scope.consol.consolAttachmentList[i].customs;
         * 
         * displayObject.unprotectedFileName =
         * $scope.consol.consolAttachmentList[i].unprotectedFileName;
         * displayObject.unprotectedFileContentType =
         * $scope.consol.consolAttachmentList[i].unprotectedFileContentType;
         * displayObject.unprotectedFile =
         * $scope.consol.consolAttachmentList[i].unprotectedFile;
         * 
         * displayObject.protectedFileName =
         * $scope.consol.consolAttachmentList[i].protectedFileName;
         * displayObject.protectedFileContentType =
         * $scope.consol.consolAttachmentList[i].protectedFileContentType;
         * displayObject.protectedFile =
         * $scope.consol.consolAttachmentList[i].protectedFile;
         * 
         * displayObject.systemTrack =
         * $scope.consol.consolAttachmentList[i].systemTrack; displayObject.versionLock =
         * $scope.consol.consolAttachmentList[i].versionLock; displayObject.swapped =
         * $scope.consol.consolAttachmentList[i].swapped;
         * 
         * data.push(displayObject); } } else { console.log("There is no
         * attachements...."); data.push({}); }
         * 
         * console.log("Populating Attachment Over....."); }
         * 
         */

        // Document Tab STarts


        $scope.checkBoxConsolDocument = function(checkBoxCode) {
            if (checkBoxCode == 1) {
                if ($scope.consol.consolDocument.isConsignee == false) {
                    $scope.consol.consolDocument.consignee = {};
                    $scope.consol.consolDocument.consigneeAddress = {};
                }
            }

            if (checkBoxCode == 2) {
                if ($scope.consol.consolDocument.isFirstNotify == false) {
                    $scope.consol.consolDocument.firstNotify = {};
                    $scope.consol.consolDocument.firstNotifyAddress = {};
                }
            }
            if (checkBoxCode == 3) {
                if ($scope.consol.consolDocument.isSecondNotify == false) {
                    $scope.consol.consolDocument.secondNotify = {};
                    $scope.consol.consolDocument.secondNotifyAddress = {};
                }
            }

            if (checkBoxCode == 4) {
                if ($scope.consol.consolDocument.isAgent == false) {
                    $scope.consol.consolDocument.agent = {};
                    $scope.consol.consolDocument.agentAddress = {};
                }
            }
            if (checkBoxCode == 5) {
                if ($scope.consol.consolDocument.isIssuingAgent == false) {
                    $scope.consol.consolDocument.issuingAgent = {};
                    $scope.consol.consolDocument.issuingAgentAddress = {};
                }
            }


        }


        // Document Lists Flag//

        $scope.showConsoDocShippList = false;
        $scope.showConsolShipperAddressList = false;
        $scope.showConsolForwarderAddressList = false;
        $scope.showConsoDocForwList = false;
        $scope.showConsoDocConsList = false;
        $scope.showConsolConsigneeAddressList = false;
        $scope.showConsoDocFNList = false;
        $scope.showConsolFirstNotifyAddressList = false;
        $scope.showConsoDocSNList = false;
        $scope.showConsolSecondNotifyAddressList = false;
        $scope.showConsoDocAgentList = false;
        $scope.showConsolDocAgentAddressList = false;
        $scope.showConsoDocIssuingAgentList = false;
        $scope.showConsolIssuingAgentAddressList = false;
        $scope.showDocOriginList = false;
        $scope.showDocDestinationList = false;
        $scope.showPortOfDocLoadingList = false;
        $scope.showPortOfDocDischargeList = false;
        $scope.showPackList = false;

        // List Flag ends//


        // COnsol Doc-Shipper COnfiguration.//

        $scope.ajaxConsolDocShippEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocShippList = data.responseObject.searchResult;
                        /* $scope.showConsoDocShippList=true; */
                        return $scope.consolDocShippList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocShipper = function(partyobj) {
            if ($scope.validateConsol(19)) {
                $scope.getConsolDocPartyAddress('Shipper', $scope.consol.consolDocument.shipper);
                $rootScope.navigateToNextField(partyobj);
            }
        };

        $scope.consolDocShipperAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showDocShipperAddress = function() {
            $scope.panelTitle = "Shipper Address";
            $scope.validateConsol(1);
            $scope.showConsolShipperAddressList = true;
            $scope.selectedItemDocSAdd = -1;

            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.shipper.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.shipper.partyAddressList[i]);
            }

        };

        $scope.selectedConsolShipperAddress = function(obj) {
            $scope.consol.consolDocument.shipperAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.shipperAddress, $scope.consol.consolDocument.shipper, obj);
            $scope.cancelConsolShipperAddress();
        };

        $scope.cancelConsolShipperAddress = function() {
            $scope.showConsolShipperAddressList = false;
        }



        // COnsol Doc-Forwarder COnfiguration.//
        $scope.consolDocForwarderlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showConsolDocForwarder = function() {
            $scope.selectedItemFDoc = -1;
            $scope.panelTitle = "Forwarder";
            // $scope.ajaxConsolDocForwarderEvent(null);
        }

        $scope.ajaxConsolDocForwarderEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocForwList = data.responseObject.searchResult;
                        return $scope.consolDocForwList;
                        // $scope.showConsoDocForwList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocForwarder = function(partyobj) {
            if ($scope.validateConsol(25)) {
                $scope.getConsolDocPartyAddress('Forwarder', $scope.consol.consolDocument.forwarder);
                $rootScope.navigateToNextField(partyobj);
            }
        };

        $scope.cancelConsolDocForwarder = function() {
            $scope.showConsoDocForwList = false;
        }

        $scope.consolDocForwarderAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showDocForwarderAddress = function() {
            $scope.panelTitle = "Forwarder Address";
            $scope.validateConsol(7);
            $scope.showConsolForwarderAddressList = true;
            $scope.selectedItemDocFAdd = -1;
            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.forwarder.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.forwarder.partyAddressList[i]);
            }
        };

        $scope.selectedConsolForwarderAddress = function(obj) {
            $scope.consol.consolDocument.forwarderAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.forwarderAddress, $scope.consol.consolDocument.forwarder, obj);
            $scope.cancelConsolForwarderAddress();
        };

        $scope.cancelConsolForwarderAddress = function() {
            $scope.showConsolForwarderAddressList = false;
        }

        // Forwarder ends


        // COnsol Doc-Consignee COnfiguration.//
        $scope.consolDocConsigneelistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showConsolDocConsignee = function() {

            $scope.selectedItemCDoc = -1;
            $scope.panelTitle = "Consignee";
            // $scope.ajaxConsolDocConsigneeEvent(null);
        }

        $scope.ajaxConsolDocConsigneeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocConsList = data.responseObject.searchResult;
                        return $scope.consolDocConsList;
                        // $scope.showConsoDocConsList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocConsignee = function(partyobj) {
            if ($scope.validateConsol(31)) {
                $scope.getConsolDocPartyAddress('Consignee', $scope.consol.consolDocument.consignee);
                $rootScope.navigateToNextField(partyobj);
            }
        };

        $scope.cancelConsolDocConsignee = function() {
            $scope.showConsoDocConsList = false;
        }

        $scope.consolDocConsigneeAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showDocConsigneeAddress = function() {
            $scope.panelTitle = "Consignee Address";
            $scope.validateConsol(13);
            $scope.showConsolConsigneeAddressList = true;
            $scope.selectedItemDocCAdd = -1;
            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.consignee.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.consignee.partyAddressList[i]);
            }
        };

        $scope.selectedConsolConsigneeAddress = function(obj) {
            $scope.consol.consolDocument.consigneeAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.consigneeAddress, $scope.consol.consolDocument.consignee, obj);
            $scope.cancelConsolConsigneeAddress();
        };

        $scope.cancelConsolConsigneeAddress = function() {
            $scope.showConsolConsigneeAddressList = false;
        }

        // consignee ends


        // COnsol Doc-Firstnotify COnfiguration.//
        $scope.consolDocFirstNotifylistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showConsolDocFirstNotify = function() {
            $scope.selectedItemFNDoc = -1;
            $scope.panelTitle = "FirstNotify";
            // $scope.ajaxConsolDocFirstNotifyEvent(null);
        }

        $scope.ajaxConsolDocFirstNotifyEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.consolDocFNList = [];
            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocFNList = data.responseObject.searchResult;
                        // $scope.showConsoDocFNList=true;
                        return $scope.consolDocFNList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocFirstNotify = function(nextIdValue) {
            if ($scope.validateConsol(37)) {
                $scope.getConsolDocPartyAddress('FirstNotify', $scope.consol.consolDocument.firstNotify);
                $rootScope.navigateToNextField(nextIdValue);
            }
        };

        $scope.cancelConsolDocFirstNotify = function() {
            $scope.showConsoDocFNList = false;
        }

        $scope.consolDocFirstNotifyAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showDocFirstNotifyAddress = function() {
            $scope.panelTitle = "FirstNotify Address";
            $scope.validateConsol(19);
            $scope.showConsolFirstNotifyAddressList = true;
            $scope.selectedItemDocFNAdd = -1;
            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.firstNotify.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.firstNotify.partyAddressList[i]);
            }
        };

        $scope.selectedConsolFirstNotifyAddress = function(obj) {
            $scope.consol.consolDocument.firstNotifyAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.firstNotifyAddress, $scope.consol.consolDocument.firstNotify, obj);
            $scope.cancelConsolFirstNotifyAddress();
        };

        $scope.cancelConsolFirstNotifyAddress = function() {
            $scope.showConsolFirstNotifyAddressList = false;
        }

        // first notify ends


        // second notify


        // COnsol Doc-Firstnotify COnfiguration.//
        $scope.consolDocSecondNotifylistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showConsolDocSecondNotify = function() {
            $scope.selectedItemFNDoc = -1;
            $scope.panelTitle = "SecondNotify";
            // $scope.ajaxConsolDocSecondNotifyEvent(null);
        }

        $scope.ajaxConsolDocSecondNotifyEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocSNList = data.responseObject.searchResult;
                        return $scope.consolDocSNList;
                        // $scope.showConsoDocSNList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocSecondNotify = function(partyobj) {
            if ($scope.validateConsol(43)) {
                $scope.getConsolDocPartyAddress('SecondNotify', $scope.consol.consolDocument.secondNotify);
                $rootScope.navigateToNextField(partyobj);
            }
        };

        $scope.cancelConsolDocSecondNotify = function() {
            $scope.showConsoDocSNList = false;
        }

        $scope.consolDocSecondNotifyAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showDocSecondNotifyAddressAddress = function() {
            $scope.panelTitle = "SecondNotify Address";
            $scope.validateConsol(25);
            $scope.showConsolSecondNotifyAddressList = true;
            $scope.selectedItemDocSNAdd = -1;
            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.secondNotify.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.secondNotify.partyAddressList[i]);
            }
        };

        $scope.selectedConsolSecondNotifyAddress = function(obj) {
            $scope.consol.consolDocument.secondNotifyAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.secondNotifyAddress, $scope.consol.consolDocument.secondNotify, obj);
            $scope.cancelConsolSecondNotifyAddress();
        };

        $scope.cancelConsolSecondNotifyAddress = function() {
            $scope.showConsolSecondNotifyAddressList = false;
        }

        // second notify ends

        // agent starts

        $scope.consolDocAgentlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showConsolDocAgent = function() {
            $scope.selectedItemAgentDoc = -1;
            $scope.panelTitle = "Agent";
            // $scope.ajaxConsolDocAgentEvent(null);
        }

        $scope.ajaxConsolDocAgentEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocAgentList = data.responseObject.searchResult;
                        return $scope.consolDocAgentList;
                        // $scope.showConsoDocAgentList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocAgent = function(partyobj) {
            if ($scope.validateConsol(49)) {
                $scope.getConsolDocPartyAddress('Agent', $scope.consol.consolDocument.agent);
                $rootScope.navigateToNextField(partyobj);
            }
        };

        $scope.cancelConsolDocAgent = function() {
            $scope.showConsoDocAgentList = false;
        }

        $scope.consolDocAgentAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showConsolAgentAddress = function() {
            $scope.panelTitle = "Agent Address";
            if ($scope.validateConsol(31));
            $scope.showConsolDocAgentAddressList = true;
            $scope.selectedItemDocAgentAdd = -1;
            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.agent.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.agent.partyAddressList[i]);
            }
        };

        $scope.selectedConsolDocAgentAddress = function(obj) {
            $scope.consol.consolDocument.agentAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.agentAddress, $scope.consol.consolDocument.agent, obj);
            $scope.cancelConsolDocAgentAddress();
        };

        $scope.cancelConsolDocAgentAddress = function() {
            $scope.showConsolDocAgentAddressList = false;
        }

        // agent ends



        // issuing agent starts

        $scope.consolDocIssuingAgentlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }

        $scope.showConsolDocIssuingAgent = function() {
            $scope.selectedItemIssuingAgentDoc = -1;
            $scope.panelTitle = "IssuingAgent";
            // $scope.ajaxConsolDocIssuingAgentEvent(null);
        }

        $scope.ajaxConsolDocIssuingAgentEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.consolDocIssuingAgentList = data.responseObject.searchResult;
                        return $scope.consolDocIssuingAgentList;
                        // $scope.showConsoDocIssuingAgentList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.selectedConsolDocIssuingAgent = function(nextIdValue) {
            if ($scope.validateConsol(55)) {
                $scope.getConsolDocPartyAddress('IssuingAgent', $scope.consol.consolDocument.issuingAgent);
                $rootScope.navigateToNextField(nextIdValue);
            }
        };

        $scope.cancelConsolDocIssuingAgent = function() {
            $scope.showConsoDocIssuingAgentList = false;
        }

        $scope.consolDocIssuingAgentAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showDocIssuingAgentddress = function() {
            $scope.panelTitle = "IssuingAgent Address";
            if ($scope.validateConsol(37));
            $scope.showConsolIssuingAgentAddressList = true;
            $scope.selectedItemDocIssuingAgentAdd = -1;
            $scope.consolAgentList = [];
            for (var i = 0; i < $scope.consol.consolDocument.issuingAgent.partyAddressList.length; i++) {
                $scope.consolAgentList.push($scope.consol.consolDocument.issuingAgent.partyAddressList[i]);
            }
        };

        $scope.selectedConsolIssuingAgentAddress = function(obj) {
            $scope.consol.consolDocument.issuingAgentAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.issuingAgentAddress, $scope.consol.consolDocument.issuingAgent, obj);
            $scope.cancelConsolIssuingAgentAddress();
        };

        $scope.cancelConsolIssuingAgentAddress = function() {
            $scope.showConsolIssuingAgentAddressList = false;
        }

        // issuing agent ends

        $scope.getConsolDocPartyAddress = function(type, obj) {
            if (type == 'Shipper') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.shipperAddress == undefined ||
                                $scope.consol.consolDocument.shipperAddress == null) {
                                $scope.consol.consolDocument.shipperAddress = {};
                            }
                            $scope.consol.consolDocument.shipperAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.shipperAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }
            if (type == 'Forwarder') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.forwarderAddress == undefined ||
                                $scope.consol.consolDocument.forwarderAddress == null) {
                                $scope.consol.consolDocument.forwarderAddress = {};
                            }
                            $scope.consol.consolDocument.forwarderAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.forwarderAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }
            if (type == 'Consignee') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.consigneeAddress == undefined ||
                                $scope.consol.consolDocument.consigneeAddress == null) {
                                $scope.consol.consolDocument.consigneeAddress = {};
                            }
                            $scope.consol.consolDocument.consigneeAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.consigneeAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }
            if (type == 'FirstNotify') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.firstNotifyAddress == undefined ||
                                $scope.consol.consolDocument.firstNotifyAddress == null) {
                                $scope.consol.consolDocument.firstNotifyAddress = {};
                            }
                            $scope.consol.consolDocument.firstNotifyAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.firstNotifyAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }
            if (type == 'SecondNotify') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.secondNotifyAddress == undefined ||
                                $scope.consol.consolDocument.secondNotifyAddress == null) {
                                $scope.consol.consolDocument.secondNotifyAddress = {};
                            }
                            $scope.consol.consolDocument.secondNotifyAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.secondNotifyAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }

            if (type == 'Agent') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.agentAddress == undefined ||
                                $scope.consol.consolDocument.agentAddress == null) {
                                $scope.consol.consolDocument.agentAddress = {};
                            }
                            $scope.consol.consolDocument.agentAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.agentAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }

            if (type == 'IssuingAgent') {
                if (obj != null && obj.id != null) {
                    for (var i = 0; i < obj.partyAddressList.length; i++) {
                        if (obj.partyAddressList[i].addressType == 'Primary') {
                            if ($scope.consol.consolDocument.issuingAgentAddress == undefined ||
                                $scope.consol.consolDocument.issuingAgentAddress == null) {
                                $scope.consol.consolDocument.issuingAgentAddress = {};
                            }
                            $scope.consol.consolDocument.issuingAgentAddress = addressJoiner.joinAddressLineByLine($scope.consol.consolDocument.issuingAgentAddress, obj, obj.partyAddressList[i]);
                        }
                    }
                }
            }

        }


        $scope.listDocConfigPack = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "packName",
                    seperator: false
                },
                {
                    "title": "packCode",
                    seperator: true
                }
            ]
        }
        $scope.showDocPack = function(pack) {
            $scope.panelTitle = "Pack";
            $scope.selectedItempack = -1;
            // $scope.ajaxDocPackEvent(null);

        };

        $scope.ajaxDocPackEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PackList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.docPackList = data.responseObject.searchResult;
                        return $scope.docPackList;
                        // $scope.showDocumentPackList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Pack List');
                }
            );

        }
        $scope.selectedDocPack = function(packObj) {
            if ($scope.validateConsol(64)) {
                $rootScope.navigateToNextField(packObj);
            }
        };
        $scope.cancelDocPack = function() {
            console.log("Cancel Docpack List Button Pressed....");
            $scope.searchText = "";
            $scope.showDocumentPackList = false;
        };

        // document Port Starts


        $scope.docOriginlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDocOrigin = function(name) {
            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR90031"]);
                $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR90031"]);
                return false;
            }
            console.log("Showing List of Origins.....");
            $scope.selectedItemOr = -1;
            $scope.panelTitle = "Origin";
            // $scope.ajaxDOcOriginEvent(null);
        };

        $scope.ajaxDOcOriginEvent = function(object) {
            console.log("ajaxDocOriginEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {

                    if (data.responseCode == "ERR0") {
                        $scope.docOriginList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.consolDocument.destination == undefined || $scope.consol.consolDocument.destination.id == null) {
                                $scope.docOriginList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {
                                    if (resultList[i].id != $scope.consol.consolDocument.destination.id) {
                                        $scope.docOriginList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.docOriginList;
                        // $scope.showDocOriginList=true;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching Origin List');
                }
            );

        }

        $scope.selectedDocOrigin = function(nextIdValue) {
            // $scope.consol.consolDocument.origin = obj;
            if ($scope.validateConsol(43)) {
                $scope.bindtoConsole();
                $rootScope.navigateToNextField(nextIdValue);
            }
            // $scope.cancelDocOrigin();
            // document.getElementById('shipmentDocOrigin').focus();
        };

        $scope.cancelDocOrigin = function() {
            console.log("Origin List Cancelled....");
            $scope.showDocOriginList = false;
        }




        $scope.showDocDestinationList = false;

        $scope.docDestinationlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDocDestination = function(name) {
            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR90031"]);
                $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR90031"]);
                return false;
            }
            console.log("Showing List of Destination");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Destination";
            // $scope.ajaxDocDestination(null);
        };

        $scope.ajaxDocDestination = function(object) {

            console.log("ajaxDestination ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.docDestinationList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.consolDocument.origin == undefined || $scope.consol.consolDocument.origin == null) {
                                $scope.docDestinationList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {
                                    if (resultList[i].id != $scope.consol.consolDocument.origin.id) {
                                        $scope.docDestinationList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.docDestinationList;
                        /* $scope.showDocDestinationList=true; */
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Destination List');
                }
            );

        }

        $scope.selectedDocDestination = function(nextIdValue) {
            // $scope.consol.consolDocument.destination= obj;
            if ($scope.validateConsol(46)) {
                $scope.bindtoConsole();
                $rootScope.navigateToNextField(nextIdValue);
            }
            // $scope.cancelDocDestination();
            // document.getElementById('shipmentDocDestination').focus();
        };
        $scope.cancelDocDestination = function() {
            console.log("Destination List cancelled.")
            $scope.showDocDestinationList = false;
        }


        $scope.portOfLoadingDoclistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDocPoL = function(name) {
            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR90031"]);
                $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR90031"]);
                return false;
            }
            console.log("Showing List of Port Of Loading.....");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Port Of Loading";
            // $scope.ajaxPortOfDocLoading(null);
        };

        $scope.ajaxPortOfDocLoading = function(object) {
            console.log("ajaxPortOfLoadingEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {


                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocLoadingList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.consolDocument.pod == undefined || $scope.consol.consolDocument.pod == null) {
                                $scope.portOfDocLoadingList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consol.consolDocument.pod.id) {
                                        $scope.portOfDocLoadingList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.portOfDocLoadingList;
                        // $scope.showPortOfDocLoadingList=true;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );

        }

        $scope.selectedPortOfDocLoading = function(nextIdValue) {
            // console.log("Selected PortOfLoading : ", obj);
            // $scope.consol.consolDocument.pol = obj;
            if ($scope.validateConsol(44)) {
                $scope.bindtoConsole();
                $rootScope.navigateToNextField(nextIdValue);
            }
            // $scope.cancelPortOfDocLoading();
            // document.getElementById('shipmentDocPol').focus();
        };

        $scope.cancelPortOfDocLoading = function() {
            console.log("PortOfLoading List Cancelled....");
            $scope.showPortOfDocLoadingList = false;
        }



        $scope.showPortOfDocDischargeList = false;

        $scope.portOfDischargeDoclistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDocPOD = function(name) {
            if ($scope.consol.serviceMaster == undefined || $scope.consol.serviceMaster == null || $scope.consol.serviceMaster == '') {
                console.log($rootScope.nls["ERR90031"]);
                $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR90031"]);
                return false;
            }
            console.log("Showing List of PortOfDischarge");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Destination";
            // $scope.ajaxPortOfDocDischarge(null);
        };

        $scope.ajaxPortOfDocDischarge = function(object) {

            console.log("ajaxPortOfDischarge ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consol.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocDischargeList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consol.consolDocument.pol == undefined || $scope.consol.consolDocument.pol == null) {
                                $scope.portOfDocDischargeList = resultList;

                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consol.consolDocument.pol.id) {
                                        $scope.portOfDocDischargeList.push(resultList[i]);
                                    }

                                }

                            }
                        }

                        return $scope.portOfDocDischargeList;
                        // $scope.showPortOfDocDischargeList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );

        }

        $scope.selectedPortOfDocDischarge = function(nextIdValue) {
            // console.log("Selected PortOfDischarge ", obj);
            // $scope.consol.consolDocument.pod= obj;
            if ($scope.validateConsol(45)) {
                $scope.bindtoConsole();
                $rootScope.navigateToNextField(nextIdValue);
            }
            // $scope.cancelPortOfDocDischarge();
            // document.getElementById('shipmentDocPod').focus();
        };

        $scope.cancelPortOfDocDischarge = function() {
            console.log("PortOfDischarge List cancelled.")
            $scope.showPortOfDocDischargeList = false;
        }


        $scope.convertDocGrossWeightPoundToKg = function() {

            if ($scope.consol.consolDocument.grossWeightInPound != null) {
                $scope.consol.consolDocument.grossWeight = Math.round(($scope.consol.consolDocument.grossWeightInPound * 0.45359237) * 100) / 100;

            }
            $scope.calculateDocChargebleWeight();
        }


        $scope.convertDocGrossWeightKgToPound = function() {
            if ($scope.consol.consolDocument.grossWeight != null) {
                $scope.consol.consolDocument.grossWeightInPound = Math.round(($scope.consol.consolDocument.grossWeight * 2.20462) * 100) / 100;

            }
            $scope.calculateDocChargebleWeight();
        }



        $scope.convertDocVolumeWeightPoundToKg = function() {
            if ($scope.consol.consolDocument.volumeWeightInPound != null) {
                $scope.consol.consolDocument.volumeWeight = Math.round(($scope.consol.consolDocument.volumeWeightInPound * 0.45359237) * 100) / 100;

            }
            $scope.calculateDocChargebleWeight();
        }



        $scope.convertDocVolumeWeightKgToPound = function() {
            if ($scope.consol.consolDocument.volumeWeight != null) {
                $scope.consol.consolDocument.volumeWeightInPound = Math.round(($scope.consol.consolDocument.volumeWeight * 2.20462) * 100) / 100;

            }
            $scope.calculateDocChargebleWeight();
        }


        $scope.calculateDocChargebleWeight = function() {
            if (+($scope.consol.consolDocument.volumeWeight) > +($scope.consol.consolDocument.grossWeight)) {
                $scope.consol.consolDocument.chargebleWeight = $scope.consol.consolDocument.volumeWeight;
            } else {
                $scope.consol.consolDocument.chargebleWeight = $scope.consol.consolDocument.grossWeight;
            }
        }

        $scope.changeDocumentDimension = function() {
            if ($scope.consol.consolDocument.dimensionUnit == false) {

                $scope.consol.consolDocument.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            } else {
                $scope.consol.consolDocument.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            }

        }




        $scope.consoleditDocument = function(index) {

            $scope.consolDocEditIndex = index;
            $scope.errorMap = new Map();
            $scope.addDocument = false;
            var date = $scope.consol.documentList[index].documentReqDate;
            $scope.consolDocument = cloneService.clone($scope.consol.documentList[index]);
            $scope.consol.consolDocument.isConsignee = $scope.consol.consolDocument.isConsignee == 'Yes' ? true : false;
            $scope.consol.consolDocument.isFirstNotify = $scope.consol.consolDocument.isFirstNotify == 'Yes' ? true : false;
            $scope.consol.consolDocument.isSecondNotify = $scope.consol.consolDocument.isSecondNotify == 'Yes' ? true : false;
            $scope.consol.consolDocument.isAgent = $scope.consol.consolDocument.isAgent == 'Yes' ? true : false;
            $scope.consol.consolDocument.isIssuingAgent = $scope.consol.consolDocument.isIssuingAgent == 'Yes' ? true : false;
            $scope.consol.consolDocument.dimensionUnit = $scope.consol.consolDocument.dimensionUnit == 'CENTIMETERORKILOS' ? false : true;
            $scope.consol.consolDocument.documentReqDate = date;
            if ($scope.consol.consolDocument.shipperAddress == undefined ||
                $scope.consol.consolDocument.shipperAddress == null) {
                $scope.consol.consolDocument.shipperAddress = {};
            }
            if ($scope.consol.consolDocument.forwarderAddress == undefined ||
                $scope.consol.consolDocument.forwarderAddress == null) {
                $scope.consol.consolDocument.forwarderAddress = {};
            }
            if ($scope.consol.consolDocument.consigneeAddress == undefined ||
                $scope.consol.consolDocument.consigneeAddress == null) {
                $scope.consol.consolDocument.consigneeAddress = {};
            }
            if ($scope.consol.consolDocument.firstNotifyAddress == undefined ||
                $scope.consol.consolDocument.firstNotifyAddress == null) {
                $scope.consol.consolDocument.firstNotifyAddress = {};
            }
            if ($scope.consol.consolDocument.secondNotifyAddress == undefined ||
                $scope.consol.consolDocument.secondNotifyAddress == null) {
                $scope.consol.consolDocument.secondNotifyAddress = {};
            }
            if ($scope.consol.consolDocument.agentAddress == undefined ||
                $scope.consol.consolDocument.agentAddress == null) {
                $scope.consol.consolDocument.agentAddress = {};
            }
            if ($scope.consol.consolDocument.issuingAgentAddress == undefined ||
                $scope.consol.consolDocument.issuingAgentAddress == null) {
                $scope.consol.consolDocument.issuingAgentAddress = {};
            }

            discardService.set($scope.consolDocument);


        }

        $scope.consolDuplicateDocument = function(index) {
            $scope.consolDocument = cloneService.clone($scope.consol.documentList[index]);
            $scope.consolDocEditIndex = null;
            $scope.consol.consolDocument.id = null;
            $scope.consol.consolDocument.documentNo = null;
            $scope.consol.consolDocument.documentReqDate = null
            if ($scope.consol.consolDocument.shipperAddress == undefined ||
                $scope.consol.consolDocument.shipperAddress == null) {
                $scope.consol.consolDocument.shipperAddress = {};
            } else {
                $scope.consol.consolDocument.shipperAddress.id = null;
            }

            if ($scope.consol.consolDocument.forwarderAddress == undefined ||
                $scope.consol.consolDocument.forwarderAddress == null) {
                $scope.consol.consolDocument.forwarderAddress = {};
            } else {
                $scope.consol.consolDocument.forwarderAddress.id = null;
            }

            if ($scope.consol.consolDocument.consigneeAddress == undefined ||
                $scope.consol.consolDocument.consigneeAddress == null) {
                $scope.consol.consolDocument.consigneeAddress = {};
            } else {
                $scope.consol.consolDocument.consigneeAddress.id = null;
            }
            if ($scope.consol.consolDocument.firstNotifyAddress == undefined ||
                $scope.consol.consolDocument.firstNotifyAddress == null) {
                $scope.consol.consolDocument.firstNotifyAddress = {};
            } else {
                $scope.consol.consolDocument.firstNotifyAddress.id = null;
            }
            if ($scope.consol.consolDocument.secondNotifyAddress == undefined ||
                $scope.consol.consolDocument.secondNotifyAddress == null) {
                $scope.consol.consolDocument.secondNotifyAddress = {};
            } else {
                $scope.consol.consolDocument.secondNotifyAddress.id = null;
            }
            if ($scope.consol.consolDocument.agentAddress == undefined ||
                $scope.consol.consolDocument.agentAddress == null) {
                $scope.consol.consolDocument.agentAddress = {};
            } else {
                $scope.consol.consolDocument.agentAddress.id = null;
            }
            if ($scope.consol.consolDocument.issuingAgentAddress == undefined ||
                $scope.consol.consolDocument.issuingAgentAddress == null) {
                $scope.consol.consolDocument.issuingAgentAddress = {};
            } else {
                $scope.consol.consolDocument.issuingAgentAddress.id = null;
            }
            $scope.consol.consolDocument.isConsignee = $scope.consol.consolDocument.isConsignee == 'Yes' ? true : false;
            $scope.consol.consolDocument.isFirstNotify = $scope.consol.consolDocument.isFirstNotify == 'Yes' ? true : false;
            $scope.consol.consolDocument.isSecondNotify = $scope.consol.consolDocument.isSecondNotify == 'Yes' ? true : false;
            $scope.consol.consolDocument.isAgent = $scope.consol.consolDocument.isAgent == 'Yes' ? true : false;
            $scope.consol.consolDocument.isIssuingAgent = $scope.consol.consolDocument.isIssuingAgent == 'Yes' ? true : false;
            $scope.consol.consolDocument.dimensionUnit = $scope.consol.consolDocument.dimensionUnit == 'CENTIMETERORKILOS' ? false : true;
            $scope.addDocument = false;
            discardService.set($scope.consolDocument);

        }

        $scope.consoldeleteDocument = function(index) {
            console.log("Delete shipment Document is called.....", index);
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected Document  ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +

                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                $scope.consol.documentList.splice(index, 1);
                $scope.addDocument = true;
            }, function(value) {

            });
        }



        $scope.closeConsolDocument = function() {

            if (!discardService.compare($scope.consolDocument)) {
                ngDialog.openConfirm({
                    template: '<p>Do you want to update your changes?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.saveDocument();
                    } else {
                        $scope.addDocument = true;
                    }
                });

            } else {

                $scope.addDocument = true;
            }

        }


        $scope.bindtoDocument = function() {

            if ($scope.consol.documentList != undefined && $scope.consol.documentList != null && $scope.consol.documentList.length != undefined && $scope.consol.documentList.length > 0) {
                if ($scope.consol.documentList.length == 1) {

                    for (var i = 0; i < $scope.consol.documentList.length; i++) {

                        if ($scope.consol.origin != undefined && $scope.consol.origin != null)
                            $scope.consol.documentList[i].origin = $scope.consol.origin;
                        if ($scope.consol.destination != undefined && $scope.consol.destination != null)
                            $scope.consol.documentList[i].destination = $scope.consol.destination;

                        if ($scope.consol.pol != undefined && $scope.consol.pol != null)
                            $scope.consol.documentList[i].pol = $scope.consol.pol;
                        if ($scope.consol.pod != undefined && $scope.consol.pod != null)
                            $scope.consol.documentList[i].pod = $scope.consol.pod;

                        if ($scope.consol.carrier != undefined && $scope.consol.carrier != null && $scope.consol.carrier.id != undefined)
                            $scope.consol.documentList[i].carrier = $scope.consol.carrier;

                        if ($scope.consol.eta != undefined && $scope.consol.eta != null)
                            $scope.consol.documentList[i].eta = $scope.consol.eta;

                        if ($scope.consol.etd != undefined && $scope.consol.etd != null)
                            $scope.consol.documentList[i].etd = $scope.consol.etd;
                        if ($scope.consol.masterNo != undefined && $scope.consol.masterNo != null)
                            $scope.consol.documentList[i].mawbNo = $scope.consol.masterNo;
                        if ($scope.consol.routeNo != undefined && $scope.consol.routeNo != null)
                            $scope.consol.documentList[i].routeNo = $scope.consol.routeNo;
                        if ($scope.consol.bookedPieces != undefined && $scope.consol.bookedPieces != null && $scope.consol.bookedPieces != "")
                            $scope.consol.documentList[i].noOfPieces = $scope.consol.bookedPieces;
                        if ($scope.consol.bookedGrossWeightUnitKg != undefined && $scope.consol.bookedGrossWeightUnitKg != null && $scope.consol.bookedGrossWeightUnitKg != "")
                            $scope.consol.documentList[i].grossWeight = $scope.consol.bookedGrossWeightUnitKg;
                        if ($scope.consol.bookedVolumeWeightUnitKg != undefined && $scope.consol.bookedVolumeWeightUnitKg != null && $scope.consol.bookedVolumeWeightUnitKg != "")
                            $scope.consol.documentList[i].volumeWeight = $scope.consol.bookedVolumeWeightUnitKg;
                        if ($scope.consol.bookedGrossWeightUnitPound != undefined && $scope.consol.bookedGrossWeightUnitPound != null && $scope.consol.bookedGrossWeightUnitPound != "")
                            $scope.consol.documentList[i].bookedGrossWeightUnitPound = $scope.consol.bookedGrossWeightUnitPound;
                        if ($scope.consol.volumeWeightInPound != undefined && $scope.consol.volumeWeightInPound != null && $scope.consol.volumeWeightInPound != "")
                            $scope.consol.documentList[i].volumeWeightInPound = $scope.consol.volumeWeightInPound;
                        if ($scope.consol.bookedChargeableUnit != undefined && $scope.consol.bookedChargeableUnit != null && $scope.consol.bookedChargeableUnit != "")
                            $scope.consol.documentList[i].chargebleWeight = $scope.consol.volumeWeightInPound;
                    }

                } else if ($scope.consol.documentList.length > 1) {
                    for (var i = 0; i < $scope.consol.documentList.length; i++) {
                        if ($scope.consol.origin != undefined && $scope.consol.origin != null)
                            $scope.consol.documentList[i].origin = $scope.consol.origin;
                        if ($scope.consol.destination != undefined && $scope.consol.destination != null)
                            $scope.consol.documentList[i].destination = $scope.consol.destination;
                        if ($scope.consol.pol != undefined && $scope.consol.pol != null)
                            $scope.consol.documentList[i].pol = $scope.consol.pol;
                        if ($scope.consol.pod != undefined && $scope.consol.pod != null)
                            $scope.consol.documentList[i].pod = $scope.consol.pod;
                        if ($scope.consol.pod != undefined && $scope.consol.pod != null)
                            $scope.consol.documentList[i].pod = $scope.consol.pod;
                        if ($scope.consol.carrier != undefined && $scope.consol.carrier != null && $scope.consol.carrier.id != undefined)
                            $scope.consol.documentList[i].carrier = $scope.consol.carrier;
                        if ($scope.consol.eta != undefined && $scope.consol.eta != null)
                            $scope.consol.documentList[i].eta = $scope.consol.eta;
                        if ($scope.consol.etd != undefined && $scope.consol.etd != null)
                            $scope.consol.documentList[i].etd = $scope.consol.etd;
                        if ($scope.consol.masterNo != undefined && $scope.consol.masterNo != null)
                            $scope.consol.documentList[i].mawbNo = $scope.consol.masterNo;
                        if ($scope.consol.routeNo != undefined && $scope.consol.routeNo != null)
                            $scope.consol.documentList[i].routeNo = $scope.consol.routeNo;


                    }


                }
            }

        }


        $scope.bindtoConsole = function() {

            if ($scope.consol.documentList.length == 1) {
                if ($scope.consol != undefined && $scope.consol.documentList[0].pod != undefined && $scope.consol.documentList[0].pod.id != undefined) {
                    $scope.consol.pod = {};
                    $scope.consol.pod = $scope.consol.documentList[0].pod;
                }

                if ($scope.consol != undefined && $scope.consol.documentList[0].pol != undefined && $scope.consol.documentList[0].pol.id != undefined) {
                    $scope.consol.pol = {};
                    $scope.consol.pol = $scope.consol.documentList[0].pol;
                }
                if ($scope.consol != undefined && $scope.consol.documentList[0].origin != undefined && $scope.consol.documentList[0].origin.id != undefined) {
                    $scope.consol.origin = {};
                    $scope.consol.origin = $scope.consol.documentList[0].origin;
                }
                if ($scope.consol != undefined && $scope.consol.documentList[0].origin != undefined && $scope.consol.documentList[0].origin.id != undefined) {
                    $scope.consol.destination = {};
                    $scope.consol.destination = $scope.consol.documentList[0].destination;
                }

            }


        }

        /*
         * if($scope.consolDocument!=undefined && $scope.consolDocument!=null){
         * 
         * if($scope.consol.consolDocument.origin!=undefined)
         * $scope.consol.consolDocument.origin=$scope.consol.origin;
         * if($scope.consol.consolDocument.destination!=undefined)
         * $scope.consol.consolDocument.destination=$scope.consol.destination;
         * if($scope.consol.consolDocument.pol!=undefined)
         * $scope.consol.consolDocument.pol=$scope.consol.pol;
         * if($scope.consol.consolDocument.pod!=undefined)
         * $scope.consol.consolDocument.pod=$scope.consol.pod;
         * 
         * if($scope.consol.consolDocument.noOfPieces!=undefined)
         * $scope.consol.consolDocument.noOfPieces=$scope.consol.bookedPieces
         * 
         * if($scope.consol.consolDocument.grossWeight!=undefined)
         * $scope.consol.consolDocument.grossWeight=$scope.consol.bookedGrossWeightUnitKg;
         * 
         * if($scope.consol.consolDocument.volumeWeight!=undefined)
         * $scope.consol.consolDocument.volumeWeight=$scope.consol.bookedVolumeWeightUnitKg;
         * 
         * if($scope.consol.consolDocument.bookedGrossWeightUnitPound!=undefined)
         * $scope.consol.consolDocument.bookedGrossWeightUnitPound=$scope.consol.bookedGrossWeightUnitPound;
         * 
         * if($scope.consol.consolDocument.volumeWeightInPound!=undefined)
         * $scope.consol.consolDocument.volumeWeightInPound=$scope.consol.bookedVolumeWeightUnitPound;
         * if($scope.consol.consolDocument.chargebleWeight!=undefined)
         * $scope.consol.consolDocument.chargebleWeight=$scope.consol.bookedChargeableUnit;
         *  }
         */

        $scope.directShipmentChange = function() {
            // This message is asked in the time of generating the airline Edi - Discussed on 08.08.2017
            /*if($scope.consol.directShipment=='Yes' || $scope.consol.directShipment==true){
                    var newScope = $scope.$new();
                    newScope.errorMessage =$rootScope.nls["ERR9018"]
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                        plain: true,
                        scope:newScope,
                        className: 'ngdialog-theme-default'
                    }).then(function (value) {
                        if(value == 1){
                            $scope.consol.agent = null;
                            $scope.consol.agentAddress = null;
                        }else{
                            //$scope.consol.directShipment=false;
                        }
                    });	
            }*/
        }

        $scope.validateConsol = function(validateCode) {
            $scope.errorMap = new Map();
            $scope.firstFocus = false;
            if (validateCode == 0 || validateCode == 1) {
                if ($scope.consol.serviceMaster == null || $scope.consol.serviceMaster.id == undefined || $scope.consol.serviceMaster.id == null || $scope.consol.serviceMaster.id == "") {
                    console.log($rootScope.nls["ERR95015"]);
                    $scope.errorMap.put("consolService", $rootScope.nls["ERR95015"]);
                    $scope.Tabs = 'general';
                    return false;
                } else {
                    if ($scope.consol.serviceMaster.status == 'Block') {
                        console.log($rootScope.nls["ERR95016"]);
                        $scope.consol.serviceMaster = null;
                        $scope.errorMap.put("consolService", $rootScope.nls["ERR95016"]);
                        $scope.Tabs = 'general';
                        return false;
                    }
                    if ($scope.consol.serviceMaster.status == 'Hide') {
                        console.log($rootScope.nls["ERR95017"]);
                        $scope.consol.serviceMaster = null;
                        $scope.errorMap.put("consolService", $rootScope.nls["ERR95017"]);
                        $scope.Tabs = 'general';
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 3) {

                if ($scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.importExport == "Import") {



                } else {

                    if ($scope.consol.directShipment == 'Yes' || $scope.consol.directShipment == true) {
                        $scope.consol.agent = null;
                        $scope.consol.agentAddress = null;

                    } else {
                        if ($scope.consol.agent == undefined ||
                            $scope.consol.agent == null ||
                            $scope.consol.agent == "" ||
                            $scope.consol.agent.id == null) {
                            $scope.consol.agentAddress = {};
                            console.log($rootScope.nls["ERR95018"]);
                            $scope.errorMap.put("consolAgent", $rootScope.nls["ERR95018"]);
                            $scope.Tabs = 'general';
                            return false;

                        } else {

                            if ($scope.consol.agent.isDefaulter) {
                                $scope.errorMap.put("consolAgent", $rootScope.nls["ERR96530"]);
                                $scope.consol.agent = null;
                                $scope.consol.agentAddress = {};
                                $scope.Tabs = 'general';
                                return false

                            }

                            if (ValidateUtil.isStatusBlocked($scope.consol.agent.status)) {
                                console.log($rootScope.nls["ERR95019"]);
                                $scope.errorMap.put("consolAgent", $rootScope.nls["ERR95019"]);
                                $scope.consol.agent = null;
                                $scope.consol.agentAddress = {};
                                $scope.Tabs = 'general';
                                return false

                            }

                            if (ValidateUtil.isStatusHidden($scope.consol.agent.status)) {
                                console.log($rootScope.nls["ERR95020"]);
                                $scope.errorMap.put("consolAgent", $rootScope.nls["ERR95020"]);
                                $scope.consol.agent = null;
                                $scope.consol.agentAddress = {};
                                $scope.Tabs = 'general';
                                return false
                            }
                        }
                    }
                }
            }


            if (validateCode == 0 || validateCode == 8) {
                if ($scope.consol.origin == null ||
                    $scope.consol.origin.id == null ||
                    $scope.consol.origin.id.trim == "") {
                    console.log($rootScope.nls["ERR95029"]);
                    $scope.errorMap.put("consolOrigin", $rootScope.nls["ERR95029"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consol.origin.status)) {
                    $scope.consol.origin = null;
                    console.log($rootScope.nls["ERR95030"]);
                    $scope.errorMap.put("consolOrigin", $rootScope.nls["ERR95030"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consol.origin.status)) {
                    $scope.consol.origin = null;
                    console.log($rootScope.nls["ERR95031"]);
                    $scope.errorMap.put("consolOrigin", $rootScope.nls["ERR95031"]);
                    $scope.Tabs = 'general';
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 9) {
                if ($scope.consol.pol == null ||
                    $scope.consol.pol.id == null ||
                    $scope.consol.pol.id.trim == "") {
                    console.log($rootScope.nls["ERR95038"]);
                    $scope.errorMap.put("consolPol", $rootScope.nls["ERR95038"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consol.pol.status)) {
                    $scope.consol.pol = null;
                    console.log($rootScope.nls["ERR95039"]);
                    $scope.errorMap.put("consolPol", $rootScope.nls["ERR95039"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consol.pol.status)) {
                    $scope.consol.pol = null;
                    console.log($rootScope.nls["ERR95040"]);
                    $scope.errorMap.put("consolPol", $rootScope.nls["ERR95040"]);
                    $scope.Tabs = 'general';
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 10) {
                if ($scope.consol.pod == null ||
                    $scope.consol.pod.id == null ||
                    $scope.consol.pod.id.trim == "") {
                    console.log($rootScope.nls["ERR95042"]);
                    $scope.errorMap.put("consolPod", $rootScope.nls["ERR95042"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusBlocked($scope.consol.pod.status)) {
                    $scope.consol.pod = null;
                    console.log($rootScope.nls["ERR95043"]);
                    $scope.errorMap.put("consolPod", $rootScope.nls["ERR95043"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consol.pod.status)) {
                    $scope.consol.pod = null;
                    console.log($rootScope.nls["ERR95044"]);
                    $scope.errorMap.put("consolPod", $rootScope.nls["ERR95044"]);
                    $scope.Tabs = 'general';
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 11) {
                if ($scope.consol.destination == null ||
                    $scope.consol.destination.id == null ||
                    $scope.consol.destination.id.trim == "") {
                    console.log($rootScope.nls["ERR95033"]);
                    $scope.errorMap.put("consolDestination", $rootScope.nls["ERR95033"]);
                    $scope.Tabs = 'general';
                    return false;

                }

                if (ValidateUtil.isStatusBlocked($scope.consol.destination.status)) {
                    $scope.consol.destination = null;
                    console.log($rootScope.nls["ERR95034"]);
                    $scope.errorMap.put("consolDestination", $rootScope.nls["ERR95034"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.consol.destination.status)) {
                    $scope.consol.destination = null;
                    console.log($rootScope.nls["ERR95035"]);
                    $scope.errorMap.put("consolDestination", $rootScope.nls["ERR95036"]);
                    $scope.Tabs = 'general';
                    return false;

                }
            }


            if (validateCode == 0 || validateCode == 12) {


                if ($scope.consol.currency == undefined ||
                    $scope.consol.currency == null ||
                    $scope.consol.currency == "") {} else {

                    if ($scope.consol.currency.status == "Block") {
                        $scope.consol.currency = null;
                        $scope.Tabs = 'general';
                        $scope.errorMap.put("consolCurrency", $rootScope.nls["ERR95048"]);
                        console.log($rootScope.nls["ERR95048"]);
                        return false;

                    }

                    if ($scope.consol.currency.status == "Hide") {
                        $scope.consol.currency = null;
                        $scope.Tabs = 'general';
                        $scope.errorMap.put("consolCurrency", $rootScope.nls["ERR95049"]);
                        console.log($rootScope.nls["ERR95049"]);
                        return false;

                    }
                }

            }
            if (validateCode == 0 || validateCode == 13) {
                if ($scope.consol.currencyRate != undefined && $scope.consol.currencyRate != null && $scope.consol.currencyRate != "") {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_RATE, $scope.consol.currencyRate)) {
                        $scope.Tabs = 'general';
                        $scope.errorMap.put("currencyRate", $rootScope.nls["ERR95050"]);
                        console.log($rootScope.nls["ERR95050"]);
                        return false;
                    }
                }
            }


            if (validateCode == 0 || validateCode == 14) {

                if ($scope.consol.carrier == undefined || $scope.consol.carrier == "" || $scope.consol.carrier == null ||
                    $scope.consol.carrier.id == undefined || $scope.consol.carrier.id == "" || $scope.consol.carrier.id == null) {
                    $scope.Tabs = 'general';
                    $scope.errorMap.put("consolCarrier", $rootScope.nls["ERR96519"]);
                    return false;
                }

                if ($scope.consol.carrier != undefined &&
                    $scope.consol.carrier != null &&
                    $scope.consol.carrier != "" &&
                    $scope.consol.carrier.id != null) {


                    if ($scope.consol.carrier.status != null) {
                        if (ValidateUtil.isStatusBlocked($scope.consol.carrier.status)) {
                            $scope.Tabs = 'general';
                            $scope.errorMap.put("consolCarrier", $rootScope.nls["ERR90064"]);
                            $scope.consol.carrier = null;
                            $scope.Tabs = "general";
                            return false

                        }

                        if (ValidateUtil.isStatusHidden($scope.consol.carrier.status)) {
                            $scope.Tabs = 'general';
                            $scope.errorMap.put("consolCarrier", $rootScope.nls["ERR90065"]);
                            $scope.consol.carrier = null;
                            $scope.Tabs = "general";
                            return false

                        }
                    }

                } else {
                    $scope.Tabs = 'general';
                    $scope.errorMap.put("consolCarrier", $rootScope.nls["ERR96508"]);
                    return false
                }
            }


            if (validateCode == 0 || validateCode == 15) {

                if ($scope.consol.etd == undefined || $scope.consol.etd == "" || $scope.consol.etd == null) {
                    console.log($rootScope.nls["ERR96517"]);
                    $scope.Tabs = 'general';
                    $scope.errorMap.put("etd", $rootScope.nls["ERR96517"]);
                    return false;
                }
                if (($scope.consol.etd != undefined || $scope.consol.etd != null || $scope.consol.etd != "") && ($scope.consol.eta == undefined || $scope.consol.eta == null || $scope.consol.eta == "")) {
                    $scope.consol.eta = $scope.consol.etd;
                }


                if ($scope.consol.eta != null && $scope.consol.etd != null) {
                    var etd = $rootScope.convertToDate($scope.consol.etd);

                    var eta = $rootScope.convertToDate($scope.consol.eta);
                    if (etd > eta) {
                        $scope.Tabs = 'general';
                        console.log($rootScope.nls["ERR96113"]);
                        $scope.errorMap.put("etd", $rootScope.nls["ERR96113"]);
                        $scope.Tabs = "general";
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 16) {


                if ($scope.consol.eta == undefined || $scope.consol.eta == "" || $scope.consol.eta == null) {
                    console.log($rootScope.nls["ERR96518"]);
                    $scope.Tabs = 'general';
                    $scope.errorMap.put("eta", $rootScope.nls["ERR96518"]);
                    return false;
                }

                if ($scope.consol.etd != null && $scope.consol.eta != null) {


                    var etd = $rootScope.convertToDate($scope.consol.etd);

                    var eta = $rootScope.convertToDate($scope.consol.eta);

                    if (etd > eta) {
                        console.log($rootScope.nls["ERR96112"]);
                        $scope.Tabs = 'general';
                        $scope.errorMap.put("eta", $rootScope.nls["ERR96112"]);
                        $scope.Tabs = "general";
                        return false;
                    }

                }
            }


            if (validateCode == 0 || validateCode == 17) {


                if ($scope.consol.atd != null && $scope.consol.ata != null) {
                    var atd = $rootScope.convertToDate($scope.consol.atd);

                    var ata = $rootScope.convertToDate($scope.consol.ata);
                    if (atd > ata) {
                        $scope.Tabs = 'general';
                        console.log($rootScope.nls["ERR96110"]);
                        $scope.errorMap.put("ata", $rootScope.nls["ERR96110"]);
                        $scope.Tabs = "general";
                        return false;
                    }

                }
            }

            if (validateCode == 0 || validateCode == 18) {


                if ($scope.consol.atd != null && $scope.consol.ata != null) {
                    var atd = $rootScope.convertToDate($scope.consol.atd);

                    var ata = $rootScope.convertToDate($scope.consol.ata);
                    if (atd > ata) {
                        $scope.Tabs = 'general';
                        $scope.errorMap.put("atd", $rootScope.nls["ERR96111"]);
                        $scope.Tabs = "general";
                        return false;
                    }

                }
            }


            if (validateCode == 0 || validateCode == 19) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {



                    if ($scope.consol.serviceMaster.importExport == 'Export') {
                        if ($scope.consol.consolDocument.shipper == undefined ||
                            $scope.consol.consolDocument.shipper == null ||
                            $scope.consol.consolDocument.shipper.id == null) {
                            $scope.Tabs = 'documents';
                            $scope.errorMap.put("consolDocShipper", $rootScope.nls["ERR95806"]);
                            $scope.Tabs = 'documents'
                            return false;
                        }
                    }

                    if ($scope.consol.consolDocument.shipper != undefined &&
                        $scope.consol.consolDocument.shipper != null &&
                        $scope.consol.consolDocument.shipper.id != null &&
                        $scope.consol.consolDocument.shipper.id != '') {

                        if ($scope.consol.consolDocument.shipper.isDefaulter) {
                            $scope.errorMap.put("consolDocShipper", $rootScope.nls["ERR96533"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.shipper = {};
                            $scope.consol.consolDocument.shipperAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.shipper.status)) {
                            $scope.errorMap.put("consolDocShipper", $rootScope.nls["ERR95807"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.shipper = {};
                            $scope.consol.consolDocument.shipperAddress = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.shipper.status)) {
                            $scope.errorMap.put("consolDocShipper", $rootScope.nls["ERR95808"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.shipper = {};
                            $scope.consol.consolDocument.shipperAddress = {};
                            return false;
                        }

                    }

                }

            }
            if (validateCode == 0 || validateCode == 20) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.shipper != undefined &&
                        $scope.consol.consolDocument.shipper != null &&
                        $scope.consol.consolDocument.shipper.id != undefined &&
                        $scope.consol.consolDocument.shipper.id != '') {

                        if ($scope.consol.consolDocument.shipperAddress.addressLine1 == undefined || $scope.consol.consolDocument.shipperAddress.addressLine1 == null ||
                            $scope.consol.consolDocument.shipperAddress.addressLine1 == "") {
                            $scope.errorMap.put("consolDocShipperAddressLine1", $rootScope.nls["ERR95809"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }
                    }

                }
            }

            if (validateCode == 0 || validateCode == 21) {

                /*if($scope.consol.serviceMaster.importExport != 'Import'){	
                    if($scope.consol.consolDocument.shipper!=undefined 
                            && $scope.consol.consolDocument.shipper!=null
                            && $scope.consol.consolDocument.shipper.id!=undefined  
                            && $scope.consol.consolDocument.shipper.id!='' ){
                        if($scope.consol.consolDocument.shipperAddress.addressLine2== undefined || $scope.consol.consolDocument.shipperAddress.addressLine2  == null
                            || $scope.consol.consolDocument.shipperAddress.addressLine2  == "") {
                            $scope.errorMap.put("consolDocShipperAddressLine2",$rootScope.nls["ERR95812"]);
                            $scope.Tabs='documents';
                            return false;
                        }
                    }
    
                }// address 2 ended
    */
            }

            if (validateCode == 0 || validateCode == 22) {

            } // address 3 ended


            if (validateCode == 0 || validateCode == 23) {

            } // address 4 ended

            if (validateCode == 0 || validateCode == 24) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.shipper != undefined && $scope.consol.consolDocument.shipper != null && $scope.consol.consolDocument.shipper.id != undefined && $scope.consol.consolDocument.shipper.id != '' && $scope.consol.consolDocument.shipperAddress != undefined && $scope.consol.consolDocument.shipperAddress != null) {
                        if ($scope.consol.consolDocument.shipperAddress.email != undefined && $scope.consol.consolDocument.shipperAddress.email != null && $scope.consol.consolDocument.shipperAddress.email != "") {
                            if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.shipperAddress.email)) {
                                $scope.errorMap.put("consolDocShipperAddressMail", $rootScope.nls["ERR90118"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }
                        }

                    }
                }
            }

            if (validateCode == 0 || validateCode == 25) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.forwarder != undefined &&
                        $scope.consol.consolDocument.forwarder != null &&
                        $scope.consol.consolDocument.forwarder.id != undefined &&
                        $scope.consol.consolDocument.forwarder.id != '') {

                        if ($scope.consol.consolDocument.forwarder.isDefaulter) {
                            $scope.errorMap.put("consolDocForwarder", $rootScope.nls["ERR96535"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.forwarder = {};
                            $scope.consol.consolDocument.forwarderAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.forwarder.status)) {
                            $scope.errorMap.put("consolDocForwarder", $rootScope.nls["ERR95820"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.forwarder = {};
                            $scope.consol.consolDocument.forwarderAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.forwarder.status)) {
                            $scope.errorMap.put("consolDocForwarder", $rootScope.nls["ERR95821"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.forwarder = {};
                            $scope.consol.consolDocument.forwarderAddress = {};
                            return false;
                        }

                    }

                }
            }

            if (validateCode == 0 || validateCode == 26) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.forwarder != undefined &&
                        $scope.consol.consolDocument.forwarder != null &&
                        $scope.consol.consolDocument.forwarder.id != null &&
                        $scope.consol.consolDocument.forwarder.id != '') {

                        if ($scope.consol.consolDocument.forwarderAddress.addressLine1 == undefined || $scope.consol.consolDocument.forwarderAddress.addressLine1 == null ||
                            $scope.consol.consolDocument.forwarderAddress.addressLine1 == "") {
                            $scope.errorMap.put("consolDocForwarderAddressLine1", $rootScope.nls["ERR95823"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }

                        if ($scope.consol.consolDocument.forwarderAddress.addressLine1 != undefined && $scope.consol.consolDocument.forwarderAddress.addressLine1 != null &&
                            $scope.consol.consolDocument.forwarderAddress.addressLine1 != "") {}
                    }
                }
            }

            if (validateCode == 0 || validateCode == 27) {

                /*if($scope.consol.serviceMaster.importExport != 'Import'){
                    if($scope.consol.consolDocument.forwarder!=undefined
                            && $scope.consol.consolDocument.forwarder!=null
                            && $scope.consol.consolDocument.forwarder.id!=null  
                            && $scope.consol.consolDocument.forwarder.id!='' ){
    
                        if($scope.consol.consolDocument.forwarderAddress.addressLine2== undefined || $scope.consol.consolDocument.forwarderAddress.addressLine2  == null
                            || $scope.consol.consolDocument.forwarderAddress.addressLine2  == "") {
                            $scope.errorMap.put("consolDocShipperAddressLine2",$rootScope.nls["ERR95825"]);
                            $scope.Tabs='documents';
                            return false;
                        }
                    }
    
                }// address 2 ended
    */
            }

            if (validateCode == 0 || validateCode == 28) {

            } // address 3 ended


            if (validateCode == 0 || validateCode == 29) {

            } // address 4 ended


            if (validateCode == 0 || validateCode == 30) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.forwarder != undefined && $scope.consol.consolDocument.forwarder != null && $scope.consol.consolDocument.forwarder.id != null && $scope.consol.consolDocument.forwarder.id != '' && $scope.consol.consolDocument.forwarderAddress != undefined) {
                        if ($scope.consol.consolDocument.forwarderAddress.email != undefined && $scope.consol.consolDocument.forwarderAddress.email != null && $scope.consol.consolDocument.forwarderAddress.email != "") {
                            if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.forwarderAddress.email)) {
                                $scope.errorMap.put("consolDocForwarderAddressMail", $rootScope.nls["ERR95831"]);
                                $scope.Tabs = 'documents';
                                return false;

                            }
                        }
                    }
                }
            }
            if (validateCode == 0 || validateCode == 31) {

                if ($scope.consol.consolDocument.consignee == undefined ||
                    $scope.consol.consolDocument.consignee == null ||
                    $scope.consol.consolDocument.consignee.id == undefined) {
                    $scope.errorMap.put("consolDocConsignee", $rootScope.nls["ERR95832"]);
                    $scope.Tabs = 'documents'
                    return false;
                }

                if ($scope.consol.consolDocument.consignee != undefined &&
                    $scope.consol.consolDocument.consignee != null &&
                    $scope.consol.consolDocument.consignee.id != undefined &&
                    $scope.consol.consolDocument.consignee.id != '') {

                    if ($scope.consol.consolDocument.consignee.isDefaulter) {
                        $scope.errorMap.put("consolDocConsignee", $rootScope.nls["ERR96534"]);
                        $scope.Tabs = 'documents';
                        $scope.consol.consolDocument.consignee = {};
                        $scope.consol.consolDocument.consigneeAddress = {};
                        return false;
                    }

                    if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.consignee.status)) {
                        $scope.errorMap.put("consolDocConsignee", $rootScope.nls["ERR95833"]);
                        $scope.Tabs = 'documents';
                        $scope.consol.consolDocument.consignee = {};
                        $scope.consol.consolDocument.consigneeAddress = {};
                        return false;
                    }
                    if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.consignee.status)) {
                        $scope.errorMap.put("consolDocConsignee", $rootScope.nls["ERR95834"]);
                        $scope.Tabs = 'documents';
                        $scope.consol.consolDocument.consignee = {};
                        $scope.consol.consolDocument.consigneeAddress = {};
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 32) {

                if ($scope.consol.consolDocument.consignee != undefined &&
                    $scope.consol.consolDocument.consignee != null &&
                    $scope.consol.consolDocument.consignee.id != null &&
                    $scope.consol.consolDocument.consignee.id != '') {

                    if ($scope.consol.consolDocument.consigneeAddress.addressLine1 == undefined || $scope.consol.consolDocument.consigneeAddress.addressLine1 == null ||
                        $scope.consol.consolDocument.consigneeAddress.addressLine1 == "") {
                        $scope.errorMap.put("consolDocConsigneeAddressLine1", $rootScope.nls["ERR95836"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }
                }

            }
            if (validateCode == 0 || validateCode == 33) {

                /*if($scope.consol.serviceMaster.importExport != 'Import'){
                    if($scope.consol.consolDocument.consignee!=undefined
                            && $scope.consol.consolDocument.consignee!=null
                            && $scope.consol.consolDocument.consignee.id!=null  
                            && $scope.consol.consolDocument.consignee.id!='' ){
    
                            if($scope.consol.consolDocument.consigneeAddress.addressLine2== undefined || $scope.consol.consolDocument.consigneeAddress.addressLine2  == null
                                || $scope.consol.consolDocument.consigneeAddress.addressLine2  == "") {
                                $scope.errorMap.put("consolDocConsigneeAddressLine2",$rootScope.nls["ERR95838"]);
                                $scope.Tabs='documents';
                                return false;
                            }
                        }
                }// address 2 ended
    */
            }

            if (validateCode == 0 || validateCode == 34) {

            }

            if (validateCode == 0 || validateCode == 35) {

            } // address 4 ended


            if (validateCode == 0 || validateCode == 36) {

                if ($scope.consol.consolDocument.consignee != undefined &&
                    $scope.consol.consolDocument.consignee != null &&
                    $scope.consol.consolDocument.consignee.id != null &&
                    $scope.consol.consolDocument.consignee.id != '') {
                    if ($scope.consol.consolDocument.consigneeAddress.email != undefined && $scope.consol.consolDocument.consigneeAddress.email != null && $scope.consol.consolDocument.consigneeAddress.email != "") {
                        if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.consigneeAddress.email)) {
                            $scope.errorMap.put("consolDocConsigneeAddressMail", $rootScope.nls["ERR95844"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }
                    }
                }

            }
            if (validateCode == 0 || validateCode == 37) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.firstNotify != undefined &&
                        $scope.consol.consolDocument.firstNotify != null &&
                        $scope.consol.consolDocument.firstNotify.id != null &&
                        $scope.consol.consolDocument.firstNotify.id != '') {

                        if ($scope.consol.consolDocument.firstNotify.isDefaulter) {
                            $scope.errorMap.put("consolDocFirstNotify", $rootScope.nls["ERR96536"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.firstNotify = {};
                            $scope.consol.consolDocument.firstNotifyAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.firstNotify.status)) {
                            $scope.errorMap.put("consolDocFirstNotify", $rootScope.nls["ERR95846"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.firstNotify = {};
                            $scope.consol.consolDocument.firstNotifyAddress = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.firstNotify.status)) {
                            $scope.errorMap.put("consolDocFirstNotify", $rootScope.nls["ERR95847"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.firstNotify = {};
                            $scope.consol.consolDocument.firstNotifyAddress = {};
                            return false;
                        }

                    }

                }
            }

            if (validateCode == 0 || validateCode == 38) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.firstNotify != undefined &&
                        $scope.consol.consolDocument.firstNotify != null &&
                        $scope.consol.consolDocument.firstNotify.id != null &&
                        $scope.consol.consolDocument.firstNotify.id != '') {
                        if ($scope.consol.consolDocument.firstNotifyAddress.addressLine1 == undefined || $scope.consol.consolDocument.firstNotifyAddress.addressLine1 == null || $scope.consol.consolDocument.firstNotifyAddress.addressLine1 == "") {
                            $scope.errorMap.put("consolDocFirstNotifyAddressLine1", $rootScope.nls["ERR95849"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }
                    }
                }
            }
            if (validateCode == 0 || validateCode == 39) {
                /*if($scope.consol.serviceMaster.importExport != 'Import'){
                    if($scope.consol.consolDocument.firstNotify!=undefined
                            && $scope.consol.consolDocument.firstNotify!=null
                            && $scope.consol.consolDocument.firstNotify.id!=null 
                            && $scope.consol.consolDocument.firstNotify.id!='' ){
    
                            if($scope.consol.consolDocument.firstNotifyAddress.addressLine2== undefined || $scope.consol.consolDocument.firstNotifyAddress.addressLine2  == null
                                || $scope.consol.consolDocument.firstNotifyAddress.addressLine2  == "") {
                                $scope.errorMap.put("consolDocFirstNotifyAddressLine2",$rootScope.nls["ERR95851"]);
                                $scope.Tabs='documents';
                                return false;
                            }
                        }
                }*/
            }
            if (validateCode == 0 || validateCode == 40) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.firstNotify != undefined &&
                        $scope.consol.consolDocument.firstNotify != null &&
                        $scope.consol.consolDocument.firstNotify.id != null &&
                        $scope.consol.consolDocument.firstNotify.id != '') {

                    }

                }
            }

            if (validateCode == 0 || validateCode == 41) {

            }


            if (validateCode == 0 || validateCode == 42) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.firstNotify != undefined &&
                        $scope.consol.consolDocument.firstNotify != null &&
                        $scope.consol.consolDocument.firstNotify.id != null &&
                        $scope.consol.consolDocument.firstNotify.id != '' && $scope.consol.consolDocument.firstNotifyAddress != undefined) {

                        if ($scope.consol.consolDocument.firstNotifyAddress.email != undefined && $scope.consol.consolDocument.firstNotifyAddress.email != null &&
                            $scope.consol.consolDocument.firstNotifyAddress.email != "") {
                            if ($scope.consol.consolDocument.firstNotifyAddress.email != undefined && $scope.consol.consolDocument.firstNotifyAddress.email != null && $scope.consol.consolDocument.firstNotifyAddress.email != "") {
                                if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.firstNotifyAddress.email)) {
                                    $scope.errorMap.put("consolDocFirstNotifyAddressMail", $rootScope.nls["ERR95857"]);
                                    $scope.Tabs = 'documents'
                                    return false;
                                }
                            }

                        }

                    }


                }
            }



            if (validateCode == 0 || validateCode == 43) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.secondNotify != undefined &&
                        $scope.consol.consolDocument.secondNotify != null &&
                        $scope.consol.consolDocument.secondNotify.id != null &&
                        $scope.consol.consolDocument.secondNotify.id != '') {

                        if ($scope.consol.consolDocument.secondNotify.isDefaulter) {
                            $scope.errorMap.put("consolDocSecondNotify", $rootScope.nls["ERR96537"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.secondNotify = {};
                            $scope.consol.consolDocument.secondNotifyAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.secondNotify.status)) {
                            $scope.errorMap.put("consolDocSecondNotify", $rootScope.nls["ERR95859"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.secondNotify = {};
                            $scope.consol.consolDocument.secondNotifyAddress = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.secondNotify.status)) {
                            $scope.errorMap.put("consolDocSecondNotify", $rootScope.nls["ERR95860"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.secondNotify = {};
                            $scope.consol.consolDocument.secondNotifyAddress = {};
                            return false;
                        }


                    }
                }
            }

            if (validateCode == 0 || validateCode == 44) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.secondNotify != undefined &&
                        $scope.consol.consolDocument.secondNotify != null &&
                        $scope.consol.consolDocument.secondNotify.id != null &&
                        $scope.consol.consolDocument.secondNotify.id != '') {
                        if ($scope.consol.consolDocument.secondNotifyAddress.addressLine1 == undefined || $scope.consol.consolDocument.secondNotifyAddress.addressLine1 == null ||
                            $scope.consol.consolDocument.secondNotifyAddress.addressLine1 == "") {
                            $scope.errorMap.put("consolDocSecondNotifyAddressLine1", $rootScope.nls["ERR95862"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }
                    }
                }
            }

            if (validateCode == 0 || validateCode == 45) {
                /*if($scope.consol.serviceMaster.importExport != 'Import'){
                    if($scope.consol.consolDocument.secondNotify!=undefined
                            && $scope.consol.consolDocument.secondNotify!=null
                            && $scope.consol.consolDocument.secondNotify.id!=null  
                            && $scope.consol.consolDocument.secondNotify.id!='' ){
    
                            if($scope.consol.consolDocument.secondNotifyAddress.addressLine2== undefined || $scope.consol.consolDocument.secondNotifyAddress.addressLine2  == null
                                || $scope.consol.consolDocument.secondNotifyAddress.addressLine2  == "") {
                                $scope.errorMap.put("consolDocSecondNotifyAddressLine2",$rootScope.nls["ERR95864"]);
                                $scope.Tabs='documents';
                                return false;
                            }
                        }
                }*/
            }

            if (validateCode == 0 || validateCode == 46) {

            } // address 3 ended


            if (validateCode == 0 || validateCode == 47) {

            } // address 4 ended


            if (validateCode == 0 || validateCode == 48) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.secondNotify != undefined &&
                        $scope.consol.consolDocument.secondNotify != null &&
                        $scope.consol.consolDocument.secondNotify.id != null &&
                        $scope.consol.consolDocument.secondNotify.id != '' && $scope.consol.consolDocument.secondNotifyAddress != undefined && $scope.consol.consolDocument.secondNotifyAddress != null) {
                        if ($scope.consol.consolDocument.secondNotifyAddress.email != undefined && $scope.consol.consolDocument.secondNotifyAddress.email != null && $scope.consol.consolDocument.secondNotifyAddress.email != "") {

                            if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.secondNotifyAddress.email)) {
                                $scope.errorMap.put("consolDocSecondNotifyAddressMail", $rootScope.nls["ERR95870"]);
                                $scope.Tabs = 'documents'
                                return false;
                            }
                        }
                    }


                }

            }
            if (validateCode == 0 || validateCode == 49) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.agent != undefined &&
                        $scope.consol.consolDocument.agent != null &&
                        $scope.consol.consolDocument.agent.id != null &&
                        $scope.consol.consolDocument.agent.id != '') {

                        if ($scope.consol.consolDocument.agent.isDefaulter) {
                            $scope.errorMap.put("consolDocAgent", $rootScope.nls["ERR96538"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.agent = {};
                            $scope.consol.consolDocument.agentAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.agent.status)) {
                            $scope.errorMap.put("consolDocAgent", $rootScope.nls["ERR95872"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.agent = {};
                            $scope.consol.consolDocument.agentAddress = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.agent.status)) {
                            $scope.errorMap.put("consolDocAgent", $rootScope.nls["ERR95873"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.agent = {};
                            $scope.consol.consolDocument.agentAddress = {};
                            return false;
                        }
                    }
                }

            }
            if (validateCode == 0 || validateCode == 50) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.agent != undefined &&
                        $scope.consol.consolDocument.agent != null &&
                        $scope.consol.consolDocument.agent.id != null &&
                        $scope.consol.consolDocument.agent.id != '') {
                        if ($scope.consol.consolDocument.agentAddress == null || $scope.consol.consolDocument.agentAddress.addressLine1 == undefined || $scope.consol.consolDocument.agentAddress.addressLine1 == null ||
                            $scope.consol.consolDocument.agentAddress.addressLine1 == "") {
                            $scope.errorMap.put("consolDocAgentAddressLine1", $rootScope.nls["ERR95875"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }
                    }
                }
            }
            if (validateCode == 0 || validateCode == 51) {
                /*if($scope.consol.serviceMaster.importExport != 'Import'){
                    if($scope.consol.consolDocument.agent!=undefined
                            && $scope.consol.consolDocument.agent!=null
                            && $scope.consol.consolDocument.agent.id!=null 
                            && $scope.consol.consolDocument.agent.id!='' ){
    
                            if($scope.consol.consolDocument.agentAddress==null || $scope.consol.consolDocument.agentAddress.addressLine2== undefined || $scope.consol.consolDocument.agentAddress.addressLine2  == null
                                || $scope.consol.consolDocument.agentAddress.addressLine2  == "") {
                                $scope.errorMap.put("consolDocAgentAddressLine2",$rootScope.nls["ERR95877"]);
                                $scope.Tabs='documents';
                                return false;
                            }
                        }
                	
                }// address 2 ended
    */
            }
            if (validateCode == 0 || validateCode == 52) {

            }
            if (validateCode == 0 || validateCode == 53) {

            } // address 4 ended


            if (validateCode == 0 || validateCode == 54) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.agent != undefined &&
                        $scope.consol.consolDocument.agent != null &&
                        $scope.consol.consolDocument.agent.id != null &&
                        $scope.consol.consolDocument.agent.id != '' && $scope.consol.consolDocument.agentAddress != undefined && $scope.consol.consolDocument.agentAddress != null) {

                        if ($scope.consol.consolDocument.agentAddress.email != undefined && $scope.consol.consolDocument.agentAddress.email != null &&
                            $scope.consol.consolDocument.agentAddress.email != "") {

                            if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.agentAddress.email)) {
                                $scope.errorMap.put("consolDocAgentAddressMail", $rootScope.nls["ERR95883"]);
                                $scope.Tabs = 'documents'
                                return false;
                            }
                        }
                    }

                }
            }


            if (validateCode == 0 || validateCode == 55) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.issuingAgent != undefined &&
                        $scope.consol.consolDocument.issuingAgent != null &&
                        $scope.consol.consolDocument.issuingAgent.id != null &&
                        $scope.consol.consolDocument.issuingAgent.id != '') {

                        if ($scope.consol.consolDocument.issuingAgent.isDefaulter) {
                            $scope.errorMap.put("consolDocIssuingAgent", $rootScope.nls["ERR96539"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.issuingAgent = {};
                            $scope.consol.consolDocument.issuingAgentAddress = {};
                            return false;
                        }

                        if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.issuingAgent.status)) {
                            $scope.errorMap.put("consolDocIssuingAgent", $rootScope.nls["ERR95885"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.issuingAgent = {};
                            $scope.consol.consolDocument.issuingAgentAddress = {};
                            return false;
                        }
                        if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.issuingAgent.status)) {
                            $scope.errorMap.put("consolDocIssuingAgent", $rootScope.nls["ERR95886"]);
                            $scope.Tabs = 'documents';
                            $scope.consol.consolDocument.issuingAgent = {};
                            $scope.consol.consolDocument.issuingAgentAddress = {};
                            return false;
                        }
                        if ($scope.consol.consolDocument.issuingAgent.partyDetail == null || $scope.consol.consolDocument.issuingAgent.partyDetail.iataCode == null ||
                            $scope.consol.consolDocument.issuingAgent.partyDetail.iataCode == undefined ||
                            $scope.consol.consolDocument.issuingAgent.partyDetail.iataCode == '') {
                            $scope.errorMap.put("consolDocIssuingAgent", $rootScope.nls["ERR90613"]);
                            $scope.Tabs = 'documents';
                            return false

                        }


                    }

                }
            }

            if (validateCode == 0 || validateCode == 56) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {

                    if ($scope.consol.consolDocument.issuingAgent != undefined &&
                        $scope.consol.consolDocument.issuingAgent != null &&
                        $scope.consol.consolDocument.issuingAgent.id != null &&
                        $scope.consol.consolDocument.issuingAgent.id != '') {
                        if ($scope.consol.consolDocument.issuingAgentAddress.addressLine1 == undefined || $scope.consol.consolDocument.issuingAgentAddress.addressLine1 == null ||
                            $scope.consol.consolDocument.issuingAgentAddress.addressLine1 == "") {
                            $scope.errorMap.put("consolDocIssuingAgentAddressLine1", $rootScope.nls["ERR95888"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }
                    }

                }
            }

            if (validateCode == 0 || validateCode == 57) {
                /*if($scope.consol.serviceMaster.importExport != 'Import'){
                    if($scope.consol.consolDocument.issuingAgent!=undefined
                            && $scope.consol.consolDocument.issuingAgent!=null
                            && $scope.consol.consolDocument.issuingAgent.id!=null 
                            && $scope.consol.consolDocument.issuingAgent.id!='' ){
    
                            if($scope.consol.consolDocument.issuingAgentAddress.addressLine2== undefined || $scope.consol.consolDocument.issuingAgentAddress.addressLine2  == null
                                || $scope.consol.consolDocument.issuingAgentAddress.addressLine2  == "") {
                                $scope.errorMap.put("consolDocIssuingAgentAddressLine2",$rootScope.nls["ERR95890"]);
                                $scope.Tabs='documents';
                                return false;
                            }
                        }
                }// address 2 ended
    */
            }

            if (validateCode == 0 || validateCode == 58) {} // address 3 ended

            if (validateCode == 0 || validateCode == 59) {} // address 4 ended


            if (validateCode == 0 || validateCode == 60) {

                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.issuingAgent != undefined &&
                        $scope.consol.consolDocument.issuingAgent != null &&
                        $scope.consol.consolDocument.issuingAgent.id != null &&
                        $scope.consol.consolDocument.issuingAgent.id != '' && $scope.consol.consolDocument.issuingAgentAddress != undefined && $scope.consol.consolDocument.issuingAgentAddress != null) {
                        if ($scope.consol.consolDocument.issuingAgentAddress.email != undefined && $scope.consol.consolDocument.issuingAgentAddress.email != null && $scope.consol.consolDocument.issuingAgentAddress.email != "") {
                            if (!CommonValidationService.checkMultipleMail($scope.consol.consolDocument.issuingAgentAddress.email)) {
                                $scope.errorMap.put("consolDocIssuingAgentAddressMail", $rootScope.nls["ERR95896"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }
                        }
                    }

                }
            }
            // validation for issuing issuingAgent ends



            // pack

            if (validateCode == 0 || validateCode == 61) {
                if ($scope.consol.consolDocument.packMaster != undefined && $scope.consol.consolDocument.packMaster.id != undefined && $scope.consol.consolDocument.packMaster.packName != null) {

                    if (ValidateUtil.isStatusBlocked($scope.consol.consolDocument.packMaster.status)) {
                        $scope.consol.consolDocument.packMaster = null;
                        $scope.errorMap.put("docPack", $rootScope.nls["ERR95702"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }

                    if (ValidateUtil.isStatusHidden($scope.consol.consolDocument.packMaster.status)) {
                        $scope.consol.consolDocument.packMaster = null;
                        $scope.errorMap.put("docPack", $rootScope.nls["ERR95703"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }
                }
            }
            // destination ends

            if (validateCode == 0 || validateCode == 62) {
                if ($scope.consol.consolDocument.noOfPieces == undefined || $scope.consol.consolDocument.noOfPieces == "" || $scope.consol.consolDocument.noOfPieces == null) {
                    $scope.Tabs = 'documents';
                    $scope.errorMap.put("docPieces", $rootScope.nls["ERR96118"]);
                    return false;
                } else {
                    if ($scope.consol.consolDocument.noOfPieces == 0) {
                        console.log($rootScope.nls["ERR90097"]);
                        $scope.errorMap.put("docPieces", $rootScope.nls["ERR90097"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES, $scope.consol.consolDocument.noOfPieces)) {
                        $scope.errorMap.put("docPieces", $rootScope.nls["ERR90097"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 63) {

                if ($scope.consol.consolDocument.dimensionUnit == true) {
                    if ($scope.consol.consolDocument.grossWeightInPound == undefined ||
                        $scope.consol.consolDocument.grossWeightInPound == null ||
                        $scope.consol.consolDocument.grossWeightInPound == "") {
                        $scope.errorMap.put("grossWeightPoundd", $rootScope.nls["ERR95897"]);
                        $scope.Tabs = 'documents';
                        return false;
                    } else {
                        if ($scope.consol.consolDocument.grossWeightInPound != null && $scope.consol.consolDocument.grossWeightInPound != undefined && $scope.consol.consolDocument.grossWeightInPound != "") {
                            if ($scope.consol.consolDocument.grossWeightInPound <= 0 || $scope.consol.consolDocument.grossWeightInPound > 99999999999.999) {
                                $scope.errorMap.put("grossWeightPoundd", $rootScope.nls["ERR90098"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }

                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_POUND, $scope.consol.consolDocument.grossWeightInPound)) {
                                $scope.errorMap.put("grossWeightPoundd", $rootScope.nls["ERR90098"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }
                        }
                    }
                }
            }

            if (validateCode == 0 || validateCode == 64) {
                if ($scope.consol.consolDocument.dimensionUnit == false) {
                    if ($scope.consol.consolDocument.grossWeight == undefined ||
                        $scope.consol.consolDocument.grossWeight == null ||
                        $scope.consol.consolDocument.grossWeight == "") {
                        $scope.errorMap.put("grossWeightKgd", $rootScope.nls["ERR95705"]);
                        $scope.Tabs = 'documents';
                        return false;
                    } else {
                        if ($scope.consol.consolDocument.grossWeight != null && $scope.consol.consolDocument.grossWeight != undefined && $scope.consol.consolDocument.grossWeight != "") {
                            if ($scope.consol.consolDocument.grossWeight <= 0 || $scope.consol.consolDocument.grossWeight > 99999999999.99) {
                                $scope.errorMap.put("grossWeightKgd", $rootScope.nls["ERR96114"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }

                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_GROSS_WEIGHT_KG, $scope.consol.consolDocument.grossWeight)) {
                                $scope.errorMap.put("grossWeightKgd", $rootScope.nls["ERR96114"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }
                        }
                    }
                }
            }

            if (validateCode == 0 || validateCode == 65) {

                if ($scope.consol.consolDocument.dimensionUnit == true) {

                    if ($scope.consol.consolDocument.volumeWeightInPound == undefined ||
                        $scope.consol.consolDocument.volumeWeightInPound == null ||
                        $scope.consol.consolDocument.volumeWeightInPound == "") {
                        $scope.errorMap.put("volumeWeightPoundd", $rootScope.nls["ERR95057"]);
                        $scope.Tabs = 'documents';
                        return false;
                    } else {

                        if ($scope.consol.consolDocument.volumeWeightInPound != null && $scope.consol.consolDocument.volumeWeightInPound != undefined && $scope.consol.consolDocument.volumeWeightInPound != "") {
                            if ($scope.consol.consolDocument.volumeWeightInPound <= 0 || $scope.consol.consolDocument.volumeWeightInPound > 99999999999.999) {
                                $scope.errorMap.put("volumeWeightPoundd", $rootScope.nls["ERR95057"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }

                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_POUND, $scope.consol.consolDocument.volumeWeightInPound)) {
                                $scope.errorMap.put("volumeWeightPoundd", $rootScope.nls["ERR95057"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }
                        }
                    }
                }
            }

            if (validateCode == 0 || validateCode == 66) {

                if ($scope.consol.consolDocument.dimensionUnit == false) {

                    if ($scope.consol.consolDocument.volumeWeight == undefined ||
                        $scope.consol.consolDocument.volumeWeight == null ||
                        $scope.consol.consolDocument.volumeWeight == "") {
                        $scope.errorMap.put("volumeWeightKgd", $rootScope.nls["ERR96119"]);
                        $scope.Tabs = 'documents';
                        return false;
                    } else {
                        if ($scope.consol.consolDocument.volumeWeight != null && $scope.consol.consolDocument.volumeWeight != undefined && $scope.consol.consolDocument.volumeWeight != "") {
                            $scope.consol.consolDocument.volume = ($scope.consol.consolDocument.volumeWeight / 166.66).toFixed(3);

                            if ($scope.consol.consolDocument.volumeWeight <= 0 || $scope.consol.consolDocument.volumeWeight > 99999999999.99) {
                                $scope.errorMap.put("volumeWeightKgd", $rootScope.nls["ERR96115"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }

                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_KG, $scope.consol.consolDocument.volumeWeight)) {
                                $scope.errorMap.put("volumeWeightKgd", $rootScope.nls["ERR96115"]);
                                $scope.Tabs = 'documents';
                                return false;
                            }
                        }

                    }

                }

            }

            // Calculate Weights Ends


            if (validateCode == 0 || validateCode == 67) {
                if ($scope.consol.serviceMaster.importExport != 'Import') {
                    if ($scope.consol.consolDocument.routeNo != undefined && $scope.consol.consolDocument.routeNo != "" && $scope.consol.consolDocument.routeNo != null) {
                        if ($scope.consol.consolDocument.routeNo == 0) {
                            $scope.errorMap.put("flight", $rootScope.nls["ERR96512"]);
                            $scope.Tabs = 'general';
                            return false;
                        }
                        if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_FLIGHT_NUMBER, $scope.consol.consolDocument.routeNo)) {
                            $scope.errorMap.put("flight", $rootScope.nls["ERR96512"]);
                            $scope.Tabs = 'general';
                            return false;
                        }
                    } else {
                        $scope.errorMap.put("flight", $rootScope.nls["ERR96520"]);
                        $scope.Tabs = 'general';
                        return false;

                    }
                }
            }



            if (validateCode == 0 || validateCode == 68) {


                if ($scope.consol.consolDocument.volume == undefined || $scope.consol.consolDocument.volume == "" || $scope.consol.consolDocument.volume == null) {
                    $scope.consol.consolDocument.volume = null;
                } else {
                    $scope.consol.consolDocument.volumeWeight = ($scope.consol.consolDocument.volume * 166.66).toFixed(3);
                    if ($scope.consol.consolDocument.volume < 0) {
                        console.log($rootScope.nls["ERR95052"]);
                        $scope.errorMap.put("consolVolume", $rootScope.nls["ERR95052"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_VOLUME_WEIGHT_CBM, $scope.consol.consolDocument.volume)) {
                        console.log($rootScope.nls["ERR95052"]);
                        $scope.errorMap.put("consolVolume", $rootScope.nls["ERR95052"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }

                }
            }


            if (validateCode == 0 || validateCode == 69) {
                if ($scope.consol.consolDocument.ratePerCharge == undefined ||
                    $scope.consol.consolDocument.ratePerCharge == null ||
                    $scope.consol.consolDocument.ratePerCharge == "") {

                } else {
                    if ($scope.consol.consolDocument.ratePerCharge != null && $scope.consol.consolDocument.ratePerCharge != undefined && $scope.consol.consolDocument.ratePerCharge != "")

                        if ($scope.consol.consolDocument.ratePerCharge <= 0 || $scope.consol.consolDocument.ratePerCharge > 9999999999.999) {
                            $scope.errorMap.put("ratePerCharge", $rootScope.nls["ERR96515"]);
                            $scope.Tabs = 'documents';
                            return false;
                        }

                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_RATE_PER_CHARGE, $scope.consol.consolDocument.ratePerCharge)) {
                        $scope.errorMap.put("ratePerCharge", $rootScope.nls["ERR96515"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }




                }

            }


            if (validateCode == 0 || validateCode == 70) {

                if ($scope.consol.consolDocument != undefined && $scope.consol.consolDocument.mawbNo == undefined || $scope.consol.consolDocument.mawbNo == null || $scope.consol.consolDocument.mawbNo == "") {
                    $scope.errorMap.put("mawbNo", $rootScope.nls["ERR90569"]);
                    $scope.Tabs = 'general';
                    return false;
                }

                if ($scope.consol.consolDocument.mawbNo != undefined && $scope.consol.consolDocument.mawbNo != "" && $scope.consol.consolDocument.mawbNo != null) {
                    if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_MAWB_NUMBER, $scope.consol.consolDocument.mawbNo)) {
                        console.log($rootScope.nls["ERR96523"]);
                        $scope.errorMap.put("mawbNo", $rootScope.nls["ERR96523"]);
                        $scope.Tabs = 'general';
                        return false;
                    }

                    //un Commented due to Feedback point--suresh

                    /* Commented due to feedback point given from demo : In edit master, changing the carrier , the Mawb no validation is not required .*/

                    /* var check = $rootScope.validateMawb($scope.consol.carrier,$scope.consol.consolDocument.mawbNo);
                        if(check!="S"){
                    	
                            if(check!="F"){
                                 
                                $scope.errorMap.put("mawbNo",$rootScope.nls["ERR90216"]+"("+ check +")" );
                        	
                            }else{
                        	
                                $scope.errorMap.put("mawbNo",$rootScope.nls["ERR90068"]);
                        	
                            }
                        	
                            $scope.Tabs="general";
                        	
                            return false;
                    	
                        }*/

                } else {

                    console.log($rootScope.nls["ERR96525"]);

                    $scope.errorMap.put("mawbNo", $rootScope.nls["ERR96525"]);

                    $scope.Tabs = "general";

                    return false;

                }


            }


            if (validateCode == 0 || validateCode == 71) {

                if ($scope.consol.consolDocument != undefined && $scope.consol.consolDocument.isHold != null && ($scope.consol.consolDocument.isHold || $scope.consol.consolDocument.isHold == 'Yes')) {

                    if ($scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.importExport == 'Import' && $scope.consol.consolDocument.holdNote == undefined || $scope.consol.consolDocument.holdNote == null || $scope.consol.consolDocument.holdNote == "") {
                        $scope.errorMap.put("holdImport", $rootScope.nls["ERR90217"]);
                        $scope.Tabs = 'documents';
                        return false;
                    }
                }
            }


            return true;

        }


        // Event tab started.

        /* show event list */

        $scope.showEventMasterList = false;
        $scope.eventMasterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "eventName",
                    seperator: false
                },
                {
                    "title": "eventCode",
                    seperator: true
                }
            ]
        }

        $scope.showEventMaster = function(name) {

            console.log("Showing List of Event..");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Event";
            /* $scope.ajaxEventMasterEvent(null); */
        };

        $scope.ajaxEventMasterEvent = function(object) {
            console.log("ajaxEventMasterEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            EventSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.eventMasterList = data.responseObject.searchResult;
                        /* $scope.showEventMasterList=true; */
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Event');
                }
            );
            return $scope.eventMasterList;
        }

        $scope.selectedEventMaster = function(obj) {

            // $scope.consolEvent.eventMaster= obj;
            $scope.validateConsoleEvent(1);
            // $scope.cancelEventMaster();
            document.getElementById('ConsoleEventMaster').focus();
        };

        $scope.cancelEventMaster = function() {
            $scope.showEventMasterList = false;
        }


        /* AssignedTo list */
        $scope.listConfigEventAssignedTo = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "employeeName",
                    seperator: false
                },
                {
                    "title": "employeeCode",
                    seperator: true
                }
            ]
        }
        $scope.showEventAssignedTo = function(customer) {
            $scope.panelTitle = "Assigned To";
            $scope.selectedItem = -1;
            /* $scope.ajaxEventAssignedToEvent(null); */

        };

        $scope.ajaxEventAssignedToEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            EmployeeList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.eventAssignedToList = data.responseObject.searchResult;
                        // $scope.showEventAssignedToList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Customer Services');
                }
            );
            return $scope.eventAssignedToList;
        }
        $scope.selectedEventAssignedTo = function(assignedToObj) {
            console.log("Selected assignedTo", assignedToObj);
            // $scope.consolEvent.assignedTo = assignedToObj;
            // $scope.cancelEventAssignedTo();
            $scope.validateConsoleEvent(3);
            document.getElementById('ConsoleEventAssignedTo').focus();
        };
        $scope.cancelEventAssignedTo = function() {
            console.log("Cancel Event AssignedTo List Button Pressed....");
            $scope.searchText = "";
            $scope.showEventAssignedToList = false;
        };




        $scope.saveEvent = function() {


            if ($scope.validateConsoleEvent(0)) {
                if ($scope.editEventIndex == null) {
                    if ($scope.consol.eventList == null)
                        $scope.consol.eventList = [];
                    $scope.consolEvent.isCompleted = 'No';
                    $scope.consolEvent.followUpRequired = $scope.consolEvent.followUpRequired == false ? 'No' : 'Yes';
                    $scope.consol.eventList.push($scope.consolEvent);



                } else {
                    $scope.consolEvent.isCompleted = $scope.consolEvent.isCompleted == false ? 'No' : 'Yes';
                    $scope.consolEvent.followUpRequired = $scope.consolEvent.followUpRequired == false ? 'No' : 'Yes';
                    $scope.consol.eventList
                        .splice($scope.editEventIndex, 1, $scope.consolEvent);



                }
                $scope.cancelEvent();
            } else {

                console.log("Shipment event Validation Failed");
            }


        }


        $scope.ajaxConsolSalesmanEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return SalesmanList.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.serviceDetailSalesmanList = data.responseObject.searchResult;
                        return $scope.serviceDetailSalesmanList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Salesman');
                }
            );

        }

        $scope.salesManRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }
        }

        $scope.empNameRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }

        }

        $scope.consolRoutedChange = function(booleanValue, consol) {
            if (booleanValue != undefined && booleanValue != null) {
                if (booleanValue) {
                    if (consol != undefined && consol.routedSalesman != undefined) {
                        consol.routedSalesman = null;
                    }
                } else {
                    if (consol != undefined && consol.routedAgent != undefined) {
                        consol.routedAgent = null;
                    }
                }
            }
        }




        // Add Event row .
        $scope.addEventRow = function() {

            $scope.addEvent = false;
            $scope.addEventShow = false;
            $scope.consolEvent = {};
            $scope.consolEvent.eventMaster = {};
            $scope.consolEvent.assignedTo = {};
            $scope.consolEvent.followUpRequired = false;
            $scope.errorMap = new Map();
            $scope.editEventIndex = null;
            discardService.set($scope.consolEvent);
        }

        $scope.editEventRow = function(index) {
            $scope.addEvent = false;
            $scope.addEventShow = false;
            $scope.editEventIndex = index;
            $scope.errorMap = new Map();
            $scope.consolEvent = cloneService.clone($scope.consol.eventList[index]);
            $scope.consolEvent.isCompleted = $scope.consolEvent.isCompleted == 'No' ? false : true;
            $scope.consolEvent.followUpRequired = $scope.consolEvent.followUpRequired == 'No' ? false : true;
            discardService.set($scope.consolEvent);
        }


        $scope.discardEvent = function() {
            if (!discardService.compare($scope.consolEvent)) {

                ngDialog.openConfirm({
                    template: '<p>Do you want to update your changes?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.saveEvent();
                    } else {
                        $scope.cancelEvent();
                    }
                });

            } else {
                $scope.cancelEvent();
            }
        }
        $scope.cancelEvent = function() {
            $scope.addEvent = true;
            $scope.addEventShow = true;
            $scope.editEventIndex = null;
            $scope.errorMap = new Map();
            $scope.consolEvent = {};
        };


        $scope.deleteEvent = function(index) {
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected Event?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                $scope.consol.eventList.splice(index, 1);
            }, function(value) {
                console.log("Event Deletion cancelled");
            });
        }


        $scope.changeEventCompleted = function() {
            if ($scope.eventStatusCompleted == false) {
                ngDialog.openConfirm({
                    template: '<p>Are you sure to complete this event ? An Auto-Email will be trigerred.</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">Cancel</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Proceed</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).
                then(function(value) {
                    if (value == 1) {
                        $scope.eventStatusCompleted = true;
                    } else if (value == 2) {
                        $scope.eventStatusCompleted = false;
                    }
                });
            } else {
                $scope.eventStatusCompleted = false;
            }
        };

        // Validate ConsoleEvent//
        $scope.validateConsoleEvent = function(validate) {


                $scope.firstFocusToEvent = false;
                $scope.errorMap = new Map();


                if (validate == 0 || validate == 1) {

                    if ($scope.consolEvent.eventMaster == undefined ||
                        $scope.consolEvent.eventMaster == null ||
                        $scope.consolEvent.eventMaster == "" ||
                        $scope.consolEvent.eventMaster.id == null) {
                        console.log($rootScope.nls["ERR96500"]);
                        $scope.errorMap.put("consol.event.eventMaster", $rootScope.nls["ERR96500"]);
                        return false;

                    } else if ($scope.consolEvent.eventMaster.status != null) {
                        if (ValidateUtil.isStatusBlocked($scope.consolEvent.eventMaster.status)) {
                            console.log($rootScope.nls["ERR96501"]);
                            $scope.errorMap.put("consol.event.eventMaster", $rootScope.nls["ERR96501"]);
                            $scope.consolEvent.eventMaster = null;

                            return false

                        }

                        if (ValidateUtil.isStatusHidden($scope.consolEvent.eventMaster.status)) {
                            console.log($rootScope.nls["ERR96502"]);
                            $scope.errorMap.put("consol.event.eventMaster", $rootScope.nls["ERR96502"]);
                            $scope.consolEvent.eventMaster = null;
                            return false

                        }
                    }
                }


                if (validate == 0 || validate == 2) {


                    if ($scope.consolEvent.eventDate == undefined ||
                        $scope.consolEvent.eventDate == null ||
                        $scope.consolEvent.eventDate == "") {
                        console.log($rootScope.nls["ERR96503"]);
                        $scope.errorMap.put("consol.event.eventDate", $rootScope.nls["ERR96503"]);
                        return false;


                    }

                }


                if (validate == 0 || validate == 3) {
                    if ($scope.consolEvent.followUpRequired == 'Yes' || $scope.consolEvent.followUpRequired == true) {

                        if ($scope.consolEvent.assignedTo == undefined ||
                            $scope.consolEvent.assignedTo == null ||
                            $scope.consolEvent.assignedTo == "" ||
                            $scope.consolEvent.assignedTo.id == null) {
                            console.log($rootScope.nls["ERR96504"]);
                            $scope.errorMap.put("consol.event.assignedTo", $rootScope.nls["ERR96504"]);
                            return false;

                        } else if ($scope.consolEvent.assignedTo.status != null) {
                            if (ValidateUtil.isEmployeeResigned($scope.consolEvent.assignedTo.employementStatus)) {
                                console.log($rootScope.nls["ERR96505"]);
                                $scope.errorMap.put("consol.event.assignedTo", $rootScope.nls["ERR96505"]);
                                $scope.consolEvent.assignedTo = null;
                                return false
                            }

                            if (ValidateUtil.isEmployeeTerminated($scope.consolEvent.assignedTo.employementStatus)) {
                                console.log($rootScope.nls["ERR96506"]);
                                $scope.errorMap.put("consol.event.assignedTo", $rootScope.nls["ERR96506"]);
                                $scope.consolEvent.assignedTo = null;
                                return false

                            }
                        }
                    }
                }

                if (validate == 0 || validate == 4) {

                    if ($scope.consolEvent.followUpRequired == 'Yes' || $scope.consolEvent.followUpRequired == true) {

                        if ($scope.consolEvent.followUpDate == undefined ||
                            $scope.consolEvent.followUpDate == null ||
                            $scope.consolEvent.followUpDate == ""
                        ) {
                            console.log($rootScope.nls["ERR96507"]);
                            $scope.errorMap.put("consol.event.followUpDate", $rootScope.nls["ERR96507"]);
                            return false;

                        }
                    }
                }



                return true;
            }
            // pick up delivery tab starts

        $scope.clearPickUpPlace = function(pickUpDeliveryPoint) {
            // if ((
            //     pickUpDeliveryPoint != null && 
            //     pickUpDeliveryPoint != undefined && 
            //     pickUpDeliveryPoint.pickupPoint == undefined || 
            //     pickUpDeliveryPoint.pickupPoint == null || 
            //     pickUpDeliveryPoint.pickupPoint.id == null
            // )) 

            if (pickUpDeliveryPoint && !pickUpDeliveryPoint.pickupPoint) {
                pickUpDeliveryPoint.pickUpContactPerson = null;
                pickUpDeliveryPoint.pickUpEmail = null;
                pickUpDeliveryPoint.pickUpMobileNo = null;
                pickUpDeliveryPoint.pickUpPhoneNo = null;
                pickUpDeliveryPoint.pickUpPlace = null;
                pickUpDeliveryPoint.pickupPointCode = null;
            }
        }

        $scope.clearDeliveryPlace = function(pickUpDeliveryPoint) {
            // if ((
            //     pickUpDeliveryPoint != null && 
            //     pickUpDeliveryPoint != undefined && 
            //     pickUpDeliveryPoint.deliveryPoint == undefined || 
            //     pickUpDeliveryPoint.deliveryPoint == null || 
            //     pickUpDeliveryPoint.deliveryPoint.id == null
            // )) 

            if (pickUpDeliveryPoint && !pickUpDeliveryPoint.deliveryPoint) {

                pickUpDeliveryPoint.deliveryContactPerson = null;
                pickUpDeliveryPoint.deliveryEmail = null;
                pickUpDeliveryPoint.deliveryMobileNo = null;
                pickUpDeliveryPoint.deliveryPhoneNo = null;
                pickUpDeliveryPoint.deliveryPlace = null;
                pickUpDeliveryPoint.deliveryPointCode = null;
            }
        }

        $scope.clearDoorDeliveryPlace = function(pickUpDeliveryPoint) {
            // if ((
            //     pickUpDeliveryPoint != null && 
            //     pickUpDeliveryPoint != undefined && 
            //     pickUpDeliveryPoint.doorDeliveryPoint == null || 
            //     pickUpDeliveryPoint.doorDeliveryPoint == undefined || 
            //     pickUpDeliveryPoint.doorDeliveryPoint.id == null
            // )) 
            if (pickUpDeliveryPoint && !pickUpDeliveryPoint.doorDeliveryPoint) {
                pickUpDeliveryPoint.doorDeliveryContactPerson = null;
                pickUpDeliveryPoint.doorDeliveryEmail = null;
                pickUpDeliveryPoint.doorDeliveryMobileNo = null;
                pickUpDeliveryPoint.doorDeliveryPhoneNo = null;
                pickUpDeliveryPoint.doorDeliveryPlace = null;
                pickUpDeliveryPoint.doorDeliveryPointCode = null;
            }
        }

        $scope.showTransporterPartyList = false;
        $scope.shipmentTransporterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            createButton: true,
            columns: [{
                    "title": "partyName",
                    seperator: false
                },
                {
                    "title": "partyCode",
                    seperator: true
                }
            ]
        }


        $scope.showPartyTransporter = function() {
            console.log("Showing List of Party...");
            $scope.selectedItem = -1;
            $scope.panelTitle = "Transporter";
            /* $scope.ajaxShipmentTransporterEvent(null); */
        }


        $scope.selectedConsolTransporter = function(nextField) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 1), nextField)) {
                for (var i = 0; i < $scope.consol.pickUpDeliveryPoint.transporter.partyAddressList.length; i++) {
                    if ($scope.consol.pickUpDeliveryPoint.transporter.partyAddressList[i].addressType == 'Primary') {
                        if ($scope.consol.pickUpDeliveryPoint.transporterAddress == undefined) {
                            $scope.consol.pickUpDeliveryPoint.transporterAddress = {};
                        }
                        $scope.consol.pickUpDeliveryPoint.transporterAddress = addressJoiner.joinAddressLineByLine({}, $scope.consol.pickUpDeliveryPoint.transporter, $scope.consol.pickUpDeliveryPoint.transporter.partyAddressList[i]).fullAddress;
                        break;
                    }
                }
            }
        };

        $scope.focusAndMessageShower = function(resObj, nextField) {

            $scope.errorMap = new Map();
            console.log("resObj :: ", resObj);
            if (resObj.error == false) {
                if (nextField != undefined) {
                    $rootScope.navigateToNextField(nextField);
                }
                return false;
            } else {
                $scope.errorMap.put(resObj.errElement, resObj.errMessage);
                $scope.changeConsolView(resObj.tab, 'error', resObj.errElement);
                return true;
            }
        }

        $scope.changeConsolView = function(tabName, reason, id) {
            $scope.Tabs = tabName;
            if (reason != undefined && reason == 'error') {
                $rootScope.navigateToNextField(id);
            }

        }

        $scope.cancelShipmentTransporter = function() {
            $scope.showTransporterPartyList = false;
        }

        $scope.removeTransporter = function() {
            if ($scope.consol.pickUpDeliveryPoint != undefined && $scope.consol.pickUpDeliveryPoint.transporterAddress != undefined) {
                $scope.consol.pickUpDeliveryPoint.transporterAddress = null;
            }
        }

        // pick up point

        $scope.selectedConsolPickUpPoint = function(nextField) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 2), nextField)) {
                var tmpPartyAddress = null;

                for (var i = 0; i < $scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList.length; i++) {
                    if ($scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList[i].addressType == 'Warehouse') {
                        tmpPartyAddress = $scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList[i];
                        break;
                    } else if ($scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList[i].addressType == 'Primary') {
                        tmpPartyAddress = $scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList[i];
                    }
                }
                $scope.addressPickUpMapping(tmpPartyAddress, $scope.consol.pickUpDeliveryPoint.pickupPoint, $scope.consol.pickUpDeliveryPoint.pickupPoint);

            }

        };



        $scope.getShipmentPickUpAddress = function(obj, mainObj) {
            if (obj == null || obj == undefined || obj.partyAddressList == null || obj.partyAddressList == undefined) {
                return;
            }

            for (var i = 0; i < obj.partyAddressList.length; i++) {

                if (obj.partyAddressList[i].addressType == 'Warehouse') {
                    $scope.addressPickUpMapping(obj.partyAddressList[i], obj, mainObj);
                    break;
                }

                if (obj.partyAddressList[i].addressType == 'Primary') {
                    $scope.addressPickUpMapping(obj.partyAddressList[i], obj, mainObj);
                }

            }

        }

        $scope.addressPickUpMapping = function(partyAddress, partyMaster, mainObj) {
            $scope.consol.pickUpDeliveryPoint.pickUpPlace = {};
            if (partyAddress != undefined) {
                $scope.consol.pickUpDeliveryPoint.pickUpPlace.addressLine1 = partyAddress.addressLine1;
                $scope.consol.pickUpDeliveryPoint.pickUpPlace.addressLine2 = partyAddress.addressLine2;
                $scope.consol.pickUpDeliveryPoint.pickUpPlace.addressLine3 = partyAddress.addressLine3;
                if (partyAddress.zipCode == null || undefined) {
                    $scope.consol.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.poBox;
                } else {
                    $scope.consol.pickUpDeliveryPoint.pickUpPlace.zipCode = partyAddress.zipCode;
                }
                $scope.consol.pickUpDeliveryPoint.pickUpPhoneNo = partyAddress.phone;
                $scope.consol.pickUpDeliveryPoint.pickUpMobileNo = partyAddress.mobileNo;
                $scope.consol.pickUpDeliveryPoint.pickUpEmail = partyAddress.email;
                $scope.consol.pickUpDeliveryPoint.pickUpContactPerson = partyAddress.contactPerson;
                $scope.consol.pickUpDeliveryPoint.pickUpPlace.city = partyAddress.cityMaster;
                $scope.consol.pickUpDeliveryPoint.pickUpPlace.state = partyAddress.stateMaster;
            }
        }

        $scope.selectedPickUpFrom = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 3), nextFocus)) {

            }

        };

        $scope.showPickUpPlaceAddressList = false;
        $scope.pickUpPlaceAddressListConfig = {
            search: false,
            address: true
        }

        $scope.showPickUpPlacePartyAddress = function() {
            $scope.panelTitle = "Pickup Place Address";
            $scope.showPickUpPlaceAddressList = true;
            $scope.selectedItem = -1;
            $scope.pickUpPlaceAddressListShipment = [];
            if ($scope.consol.pickUpDeliveryPoint.pickupPoint != null && $scope.consol.pickUpDeliveryPoint.pickupPoint.id != null) {
                for (var i = 0; i < $scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList.length; i++) {
                    $scope.pickUpPlaceAddressListShipment.push($scope.consol.pickUpDeliveryPoint.pickupPoint.partyAddressList[i]);
                }
            }
        };

        $scope.selectedPickUpPlacePartyAddress = function(obj) {

            $scope.addressPickUpMapping(obj, null, $scope.consol.pickUpDeliveryPoint);
            $scope.cancelPickUpPlacePartyAddress();
            $rootScope.navigateToNextField('pickUpContactPerson');
        };

        $scope.cancelPickUpPlacePartyAddress = function() {
            $scope.showPickUpPlaceAddressList = false;
            $rootScope.navigateToNextField('pickupPlaceConsol');
        }


        $scope.selectedPickUpState = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 4), nextFocus)) {}
        }

        $scope.selectedPickUpCity = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 4), nextFocus)) {}
        }

        $scope.validatePickUpDelivery = function(pickUpDeliveryPoint, code, nextField) {
            return $scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery(pickUpDeliveryPoint, code), nextField);
        }

        $scope.selectedDeliveryPoint = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 9), nextFocus)) {
                var tmpPartyAddress = $scope.consol.pickUpDeliveryPoint.deliveryPoint.addressMaster;
                $scope.addressDeliveryMapping(tmpPartyAddress, $scope.consol.pickUpDeliveryPoint.deliveryPoint, $scope.consol.pickUpDeliveryPoint.deliveryPoint);
            }
        }

        $scope.selectedDeliveryState = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 11), nextFocus)) {}
        }

        $scope.selectedDeliveryCity = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 11), nextFocus)) {}
        }

        $scope.selectedDoorDeliveryState = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 18), nextFocus)) {}
        }

        $scope.selectedDoorDeliveryCity = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 18), nextFocus)) {}
        }



        /*$scope.selectedDeliveryPoint = function(partyobj){
            if($scope.validatePickUpDelivery(12)){
                $scope.getShipmentDeliveryAddress($scope.consol.pickUpDeliveryPoint.deliveryPoint,$scope.consol.pickUpDeliveryPoint);
                $rootScope.navigateToNextField(partyobj);
            }
        };*/


        $scope.getShipmentDeliveryAddress = function(obj, mainObj) {
            $scope.addressDeliveryMapping(obj.addressMaster, obj, mainObj);

        }

        $scope.addressDeliveryMapping = function(partyAddress, partyMaster, mainObj) {
            $scope.consol.pickUpDeliveryPoint.deliveryPlace = {};
            if (partyAddress != undefined) {
                $scope.consol.pickUpDeliveryPoint.deliveryPlace.addressLine1 = partyAddress.addressLine1;
                $scope.consol.pickUpDeliveryPoint.deliveryPlace.addressLine2 = partyAddress.addressLine2;
                $scope.consol.pickUpDeliveryPoint.deliveryPlace.addressLine3 = partyAddress.addressLine3;
                if (partyAddress.zipCode != undefined && partyAddress.zipCode != null) {
                    $scope.consol.pickUpDeliveryPoint.deliveryPlace.zipCode = partyAddress.zipCode;
                } else {
                    $scope.consol.pickUpDeliveryPoint.deliveryPlace.zipCode = partyAddress.poBox;
                }
                $scope.consol.pickUpDeliveryPoint.deliveryPhoneNo = partyAddress.phone;
                $scope.consol.pickUpDeliveryPoint.deliveryMobileNo = partyAddress.mobile;
                $scope.consol.pickUpDeliveryPoint.deliveryEmail = partyAddress.email;
                $scope.consol.pickUpDeliveryPoint.deliveryContactPerson = partyAddress.contact;
                $scope.consol.pickUpDeliveryPoint.deliveryPlace.city = partyAddress.city;
                $scope.consol.pickUpDeliveryPoint.deliveryPlace.state = partyAddress.state;
            }
        }

        $scope.deliveryFromlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }
        $scope.showDeliveryFrom = function(name) {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Delivery From";
        };

        $scope.selectedDeliveryFrom = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 10), nextFocus)) {}
        };

        $scope.cancelDeliveryFrom = function() {
            $scope.showDeliveryFromList = false;
        }

        $scope.doorDeliveryPointlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDoorDeliveryPoint = function(name) {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Door Delivery Point";
        };


        $scope.selectedDoorDeliveryPoint = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 16), nextFocus)) {}
        };

        $scope.cancelDoorDeliveryPoint = function() {
            $scope.showDoorDeliveryPointList = false;
        }


        $scope.doorDeliveryFromlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "portName",
                    seperator: false
                },
                {
                    "title": "portCode",
                    seperator: true
                }
            ]
        }

        $scope.showDoorDeliveryFrom = function(name) {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Door Delivery From";
        };


        $scope.selectedDoorDeliveryFrom = function(nextFocus) {
            if (!$scope.focusAndMessageShower(consolShipmentValidationService.validatePickUpDelivery($scope.consol.pickUpDeliveryPoint, 17), nextFocus)) {}
        };

        $scope.cancelDoorDeliveryFrom = function() {
            $scope.showDoorDeliveryFromList = false;
        }

        // pick up delivery code ends here

        $scope.checkAndNavigate = function() {

            if ($scope.consol != undefined && $scope.consol.origin != undefined && $scope.consol.origin.portGroupMaster != undefined && $scope.consol.origin.portGroupMaster.countryMaster.countryCode == 'US' || $scope.consol.pol.portGroupMaster.countryMaster.countryCode == 'US') {
                $scope.menuTabClick('aesfocus', 'aes');
            } else {
                $scope.menuTabClick('chargeMaster0', 'rates')
            }
        }

        $scope.goToShipmentView = function(serviceId) {
            console.log("State  : ", $state)
            console.log("State Param  : ", $stateParams)
            console.log("servce ", serviceId)
            ShipmentUid.get({
                    serviceId: serviceId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var param = {
                            shipmentId: data.responseObject,
                            fromState: $state.current.name,
                            consolId: $scope.consol.id,
                            fromStateParams: JSON.stringify($stateParams)
                        }
                        $state.go("layout.viewCrmShipment", param);
                    }
                },
                function(error) {
                    console.log("Consol get Failed : " + error)
                });

        }


        $scope.consolLinkModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/air/consol_shipments/views/consol_popup.html',
            show: false
        });


        $scope.tmpShipmentLink = null;


        $scope.linkPopUp = function(wizard) {
            $scope.spinner = true;
            $scope.consolDto = {};
            $scope.consolDto.consolUid = $scope.consol.consolUid == null || $scope.consol.consolUid == undefined ? "-1" : $scope.consol.consolUid,
                $scope.consolDto.serviceId = $scope.consol.serviceMaster.id;
            $scope.consolDto.locationId = $scope.consol.location.id;
            $scope.consolDto.polId = $scope.consol.pol.id;
            $scope.consolDto.podId = $scope.consol.pod.id;

            if ($scope.consol.etd != undefined && $scope.consol.etd != null) {
                $scope.consolDto.etd = $rootScope.sendApiStartDateTime($scope.consol.etd)
            }

            if ($scope.consol.eta != undefined && $scope.consol.eta != null) {
                $scope.consolDto.eta = $rootScope.sendApiStartDateTime($scope.consol.eta);
            }
            if ($scope.consol.carrier != undefined && $scope.consol.carrier != null && $scope.consol.carrier.id != undefined && $scope.consol.carrier.id != null) {
                $scope.consolDto.carrierId = $scope.consol.carrier.id;
            }




            $scope.consolDto.mawbNo = $scope.consol.consolDocument.mawbNo;
            ConsolShipmentLink.link($scope.consolDto).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.shipmentServiceData = [];
                        var tmpList = data.responseObject.searchResult;
                        var i = 0;
                        var j = 0;
                        var flag = true;
                        $scope.tmpShipmentLink = angular.copy($scope.consol.shipmentLinkList);
                        if ($scope.consol.shipmentLinkList == null || $scope.consol.shipmentLinkList.length == 0) {
                            $scope.shipmentServiceData = tmpList;
                        } else {
                            for (i = 0; i < tmpList.length; i++) {
                                flag = true;
                                for (j = 0; j < $scope.consol.shipmentLinkList.length; j++) {
                                    if (tmpList[i].id == $scope.consol.shipmentLinkList[j].service.id) {
                                        if (wizard) {
                                            tmpList[i].isSelected = true;
                                            break;
                                        } else {
                                            flag = false;
                                            break;
                                        }

                                    }
                                }
                                if (flag) {
                                    $scope.shipmentServiceData.push(tmpList[i])
                                }
                            }
                        }
                        for (i = 0; i < $scope.shipmentServiceData.length; i++) {
                            $scope.shipmentServiceData[i].serviceCodeWithShipmentId = $scope.shipmentServiceData[i].serviceMaster.serviceCode + '-' + $scope.shipmentServiceData[i].shipmentUid;
                        }

                    } else {
                        $scope.shipmentServiceData = [];
                    }
                    $scope.spinner = false;
                },
                function(error) {
                    console.log("Consol - Shipment - Service- Link get Failed : " + error)
                    $scope.shipmentServiceData = [];
                    $scope.spinner = false;
                });

        }

        $scope.consolpopup = function() {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_HAWB_LIST_LINK_SERVICES_LIST)) {
                $scope.errorMap = new Map();

                if ($scope.consol.serviceMaster.transportMode == 'Air' &&
                    $scope.consol.serviceMaster.importExport == 'Export') {
                    if ($scope.validateConsol(1) && $scope.validateConsol(9) && $scope.validateConsol(10) && $scope.validateConsol(14) && $scope.validateConsol(15) && $scope.validateConsol(16)) {
                        $scope.linkPopUp(false);
                        $scope.consolLinkModal.$promise.then($scope.consolLinkModal.show);
                    } else {
                        $scope.Tabs = 'general';
                        console.log("validation failed in link the list");
                    }
                } else {
                    if ($scope.validateConsol(1) && $scope.validateConsol(9) && $scope.validateConsol(10)) {
                        $scope.linkPopUp(false);
                        $scope.consolLinkModal.$promise.then($scope.consolLinkModal.show);
                    } else {
                        $scope.Tabs = 'general';
                        console.log("validation failed in link the list");
                    }
                }


            }
        };


        $scope.isKnownShipper = function(service) {
            if (service.documentList != null) {
                for (var i = 0; i < service.documentList.length; i++) {
                    if (service.documentList[i] != null && service.documentList[i].shipper != null) {
                        var isKnownShipper = true;
                        var partyAddressList = service.documentList[i].shipper.partyAddressList;
                        for (var j = 0; j < partyAddressList.length; j++) {
                            var partyAddress = partyAddressList[j];
                            if (partyAddress.partyCountryField != null && partyAddress.partyCountryField.knownShipperValidationNo != null &&
                                partyAddress.partyCountryField.knownShipperValidationDate != null) {
                                var dt1 = new Date(partyAddress.partyCountryField.knownShipperValidationDate);
                                console.log("ShipperValidatorDate:::" + dt1);
                                var dt2 = new Date();
                                console.log("ShipperValidatorDate:::" + dt2);
                                if (dt1 < dt2) {
                                    isKnownShipper = false;
                                    break;
                                } else {
                                    continue;
                                }
                            } else {
                                isKnownShipper = false;
                                break;
                            }
                        }
                    }
                }
                return isKnownShipper;
            }
        }

        $scope.getColorCode = function(service) {

            if (service.holdShipment) {
                return "Hold";
            } else if (service.hazardous) {
                return "Haz";
            } else if (service.overDimension) {
                return "Over";
            }

            return null;
        }


        $scope.saveLinkService = function() {
            console.log("Save Button is Pressed......");
            $scope.errorMap = new Map();
            $rootScope.clientMessage = null;
            if ($scope.consol.shipmentLinkList == null || $scope.consol.shipmentLinkList == undefined || $scope.consol.shipmentLinkList.length <= 0) {
                $rootScope.clientMessage = $rootScope.nls["ERR96527"];
                // $scope.errorMap.put("linkService",$rootScope.nls["ERR96527"]);
                return false;
            }
            $scope.consolLinkModal.$promise.then($scope.consolLinkModal.hide);
            $rootScope.navigateToNextField('consolPartyAgent');
            return true;
        }


        $scope.linkService = function(index, obj) {
            $scope.errorMap = new Map();
            if ($scope.consol.shipmentLinkList == null || $scope.consol.shipmentLinkList.length == 0) {
                $scope.consol.shipmentLinkList = [];
            }
            $scope.tmpShipmentLink = {};
            $scope.tmpShipmentLink.service = obj;
            $scope.consol.shipmentLinkList.push($scope.tmpShipmentLink);
            $scope.shipmentServiceData.splice(index, 1);
            $scope.serviceDimensionAdded();
        };

        $scope.cancelLinkService = function() {
            $rootScope.clientMessage = null;
            if ($scope.tmpShipmentLink != undefined && $scope.consol.shipmentLinkList != undefined && $scope.consol.shipmentLinkList.length > 0 && $scope.tmpShipmentLink != $scope.consol.shipmentLinkList) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).
                then(function(value) {
                    if (value == 2) {
                        $scope.consol.shipmentLinkList = angular.copy($scope.tmpShipmentLink);
                        $scope.consolLinkModal.$promise.then($scope.consolLinkModal.hide);
                        $rootScope.navigateToNextField('consolPartyAgent');
                    }
                }, function(value) {
                    console.log("Cancel service link");
                    $scope.consolLinkModal.$promise.then($scope.consolLinkModal.hide);
                    $rootScope.navigateToNextField('consolPartyAgent');
                });
            } else {
                $scope.consolLinkModal.$promise.then($scope.consolLinkModal.hide);
                $rootScope.navigateToNextField('consolPartyAgent');
            }
        };
        $scope.linkedServiceRemove = function(index, obj) {

            if ($scope.shipmentServiceData == null || $scope.shipmentServiceData.length == 0) {
                $scope.shipmentServiceData = [];
            }
            $scope.shipmentServiceData.push(obj.service);
            $scope.consol.shipmentLinkList.splice(index, 1);
            $scope.serviceDimensionAdded();


        };


        $scope.serviceDimensionAdded = function() {

            if ($scope.consol.consolDocument.dimensionList == undefined ||
                $scope.consol.consolDocument.dimensionList == null ||
                $scope.consol.consolDocument.dimensionList.length == 0) {
                $scope.consol.consolDocument.dimensionList = [];
            } else {
                var tmpObj = [];
                for (var i = 0; i < $scope.consol.consolDocument.dimensionList.length; i++) {
                    if ($scope.consol.consolDocument.dimensionList[i].serviceId == undefined ||
                        $scope.consol.consolDocument.dimensionList[i].serviceId == null) {
                        tmpObj.push($scope.consol.consolDocument.dimensionList[i]);
                    }
                }

                $scope.consol.consolDocument.dimensionList = [];
                $scope.consol.consolDocument.dimensionList = tmpObj;
            }

            $scope.consol.consolDocument.noOfPieces = 0.0;
            $scope.consol.consolDocument.chargebleWeight = 0.0;
            $scope.consol.consolDocument.grossWeight = 0.0;
            $scope.consol.consolDocument.volumeWeight = 0.0;
            $scope.consol.consolDocument.volumeWeightInPound = 0.0;
            $scope.consol.consolDocument.grossWeightInPound = 0.0;

            for (var k = 0; k < $scope.consol.shipmentLinkList.length; k++) {

                var obj = $scope.consol.shipmentLinkList[k].service;

                $scope.consol.consolDocument.noOfPieces = $scope.consol.consolDocument.noOfPieces + obj.bookedPieces;
                $scope.consol.consolDocument.chargebleWeight = $scope.consol.consolDocument.chargebleWeight + obj.bookedChargeableUnit;
                $scope.consol.consolDocument.grossWeight = $scope.consol.consolDocument.grossWeight + obj.bookedGrossWeightUnitKg;
                $scope.consol.consolDocument.volumeWeight = $scope.consol.consolDocument.volumeWeight + obj.bookedVolumeWeightUnitKg;
                $scope.consol.consolDocument.volumeWeightInPound = $scope.consol.consolDocument.volumeWeightInPound + obj.bookedVolumeWeightUnitPound;
                $scope.consol.consolDocument.volumeWeightInPound = parseFloat($scope.consol.consolDocument.volumeWeightInPound.toFixed(2));
                $scope.consol.consolDocument.grossWeightInPound = $scope.consol.consolDocument.grossWeightInPound + obj.bookedGrossWeightUnitPound;

                for (var i = 0; i < obj.documentList.length; i++) {

                    if (obj.documentList[i].dimensionList != undefined &&
                        obj.documentList[i].dimensionList != null &&
                        obj.documentList[i].dimensionList.length > 0) {

                        for (var j = 0; j < obj.documentList[i].dimensionList.length; j++) {
                            var consolDim = {};

                            consolDim.noOfPiece = obj.documentList[i].dimensionList[j].noOfPiece;
                            consolDim.length = obj.documentList[i].dimensionList[j].length;
                            consolDim.width = obj.documentList[i].dimensionList[j].width;
                            consolDim.height = obj.documentList[i].dimensionList[j].height;
                            consolDim.volWeight = obj.documentList[i].dimensionList[j].volWeight;
                            consolDim.grossWeight = obj.documentList[i].dimensionList[j].grossWeight;
                            consolDim.grossWeightKg = obj.documentList[i].dimensionList[j].grossWeightKg;
                            consolDim.serviceId = obj.id;
                            consolDim.serviceDocumentId = obj.documentList[i].id;


                            $scope.consol.consolDocument.dimensionList.push(consolDim);

                            /*var param = {};
                            param.grossWeight = consolDim.grossWeight;
                            param.volumeWeight = consolDim.volWeight;
                            param.totalPieces = consolDim.noOfPiece;
                        	
                            $scope.calculateDocWeight(param)*/
                        }


                    }


                }
            }

            /*if($scope.consolDimension!=undefined && $scope.consolDimension.totalDimensionCheck!=undefined){
                 $scope.consolDimension.totalDimensionCheck();
            }*/
        }


        $scope.consolLinkedServiceRemove = function(index) {

            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR96528"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                var obj = $scope.consol.shipmentLinkList[index];
                $scope.consol.shipmentLinkList.splice(index, 1);
                $scope.serviceDimensionAdded();
            }, function(value) {
                console.log("Service in shipment Deletion cancelled");
            });
        };


        $scope.serviceRender = function(item) {
            return {
                label: item.serviceName,
                item: item
            }
        };
        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }
        }
        $scope.portRender = function(item) {
            return {
                label: item.portName,
                item: item
            }
        }
        $scope.currencyRender = function(item) {
            return {
                label: item.currencyCode,
                item: item
            }
        }

        $scope.carrierRender = function(item) {
            return {
                label: item.carrierName,
                item: item
            }
        }
        $scope.packRender = function(item) {
            return {
                label: item.packName,
                item: item
            }
        }

        $scope.cfsRender = function(item) {
            return {
                label: item.cfsName,
                item: item
            }
        }
        $scope.changePickUpToggle = function() {
            $scope.errorMap = new Map();
            if ($scope.consol.pickUpDeliveryPoint.isOurPickUp == false || !$scope.consol.pickUpDeliveryPoint.isOurPickUp) {
                if ($scope.consol.pickUpDeliveryPoint != undefined) {
                    $scope.consol.pickUpDeliveryPoint.pickupPoint = null;
                    $scope.consol.pickUpDeliveryPoint.pickupFrom = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpPlace = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpContactPerson = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpEmail = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpMobileNo = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpPhoneNo = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpFollowUp = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpPlanned = null;
                    $scope.consol.pickUpDeliveryPoint.pickUpActual = null;
                }
            } else {
                if ($scope.consol != undefined &&
                    $scope.consol.consolDocument != undefined &&
                    $scope.consol.consolDocument != null) {
                    $scope.consol.pickUpDeliveryPoint.pickupPoint = $scope.consol.consolDocument.shipper;
                    $scope.getShipmentPickUpAddress($scope.consol.pickUpDeliveryPoint.pickupPoint, $scope.consol.pickUpDeliveryPoint);
                } else {
                    $scope.Tabs = 'documents';
                    $rootScope.clientMessage = $rootScope.nls["ERR90558"];
                }
            }

        }



        $scope.showResponse = function(consol, isAdded) {
            $scope.spinner = false;
            var newScope = $scope.$new();
            var consolUid = consol.consolUid;
            newScope.errorMessage = isAdded == true ? $rootScope.nls["ERR219"] : $rootScope.nls["ERR220"];
            newScope.errorMessage = newScope.errorMessage.replace("%s", consolUid);
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                var params = {};
                var stateCount = 0;
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }
                if (!$scope.cancelfag) {
                    $state.go("layout.editAirConsol", {
                        consolId: consol.id,
                        submitAction: 'Saved',
                        count: stateCount
                    });
                } else {
                    $state.go('layout.airNewConsol');
                }

            });
        }




        // Ajax Callss

        $scope.ajaxPortOfDocDischargeConn = function(object) {
            console.log("ajaxPortOfDischarge ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocDischargeList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            $scope.portOfDocDischargeList = resultList;
                        }
                        return $scope.portOfDocDischargeList;
                        // $scope.showPortOfDocDischargeList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );
        }


        $scope.showResponseDynamic = function(consolUid, isAdded) {
            var newScope = $scope.$new();
            newScope.consolUid = consolUid;
            newScope.opt = isAdded == true ? 'saved' : 'updated';
            ngDialog.openConfirm({
                template: '<p>ConolShipment  {{consolUid}} successfully {{opt}}.</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                $scope.mawbModal.$promise.then($scope.mawbModal.hide);
                $scope.showDetail = false;
                $location.path('/air/consol_shipments_list');

            }, function(value) {
                $scope.mawbModal.$promise.then($scope.mawbModal.hide);
                $scope.showDetail = false;
                $location.path('/air/consol_shipments_list');
            });

        }
        $scope.ajaxPartyEvent = function(object) {
            console.log("Ajax Party Event method : " + object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.partyList = data.responseObject.searchResult;
                        // $scope.showPartyList=true;
                        return $scope.partyList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Party');
                }
            );

        }

        $scope.ajaxPortOfDocDischarge = function(object) {
            console.log("ajaxPortOfDischarge ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocDischargeList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consolDocumentClone.pol == undefined || $scope.consolDocumentClone.pol == null) {
                                $scope.portOfDocDischargeList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consolDocumentClone.pol.id) {
                                        $scope.portOfDocDischargeList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.portOfDocDischargeList;
                        // $scope.showPortOfDocDischargeList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );

        }


        $scope.ajaxPortOfDocDischargeConn = function(object) {
            console.log("ajaxPortOfDischarge ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocDischargeList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            $scope.portOfDocDischargeList = resultList;
                        }
                        return $scope.portOfDocDischargeList;
                        // $scope.showPortOfDocDischargeList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfDischarge List');
                }
            );
        }




        $scope.ajaxPortOfDocLoading = function(object) {
            console.log("ajaxPortOfLoadingEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {

                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocLoadingList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            if ($scope.consolDocumentClone.pod == undefined || $scope.consolDocumentClone.pod == null) {
                                $scope.portOfDocLoadingList = resultList;
                            } else {
                                for (var i = 0; i < resultList.length; i++) {

                                    if (resultList[i].id != $scope.consolDocumentClone.pod.id) {
                                        $scope.portOfDocLoadingList.push(resultList[i]);
                                    }

                                }
                            }
                        }
                        return $scope.portOfDocLoadingList;
                        // $scope.showPortOfDocLoadingList=true;
                    }



                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );

        }

        $scope.ajaxServiceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ServiceList.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.serviceMasterList = data.responseObject.searchResult;
                        console.log("serviceMasterList ", $scope.serviceMasterList);
                        // $scope.showServiceMasterList=true;
                        return $scope.serviceMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching ServiceMaster');
                }
            );

        }

        $scope.ajaxCurrencyEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencySearchExclude.fetch({
                "excludeCurrencyCode": $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.currencyList = data.responseObject.searchResult;
                        /* $scope.showCurrencyList = true; */
                        return $scope.currencyList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Currency');
                }
            );


        }

        $scope.ajaxOriginEventDynamic = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortGroupList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.originList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            /*
                             * if($scope.shipment.destination==undefined ||
                             * $scope.shipment.destination ==null) {
                             * $scope.originList=resultList; } else {
                             * for(var i = 0; i < resultList.length;
                             * i++) {
                             * 
                             * if(resultList[i].id !=
                             * $scope.shipment.destination.id) {
                             * $scope.originList.push(resultList[i]); }
                             *  } }
                             */

                            $scope.originList = resultList;
                        }
                        return $scope.originList;
                        // $scope.showOriginList=true;
                    }



                },
                function(errResponse) {

                    console.error('Error while fetching Origin List');
                }
            );
        }




        $scope.ajaxDestinationDynamic = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortGroupList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.destinationList = [];
                        var resultList = data.responseObject.searchResult;

                        if (resultList != null && resultList.length != 0) {
                            /*
                             * if($scope.shipment.origin==undefined ||
                             * $scope.shipment.origin ==null) {
                             * $scope.destinationList=resultList; }
                             * else { for(var i = 0; i <
                             * resultList.length; i++) {
                             * 
                             * if(resultList[i].id !=
                             * $scope.shipment.origin.id) {
                             * $scope.destinationList.push(resultList[i]); }
                             *  } }
                             */

                            $scope.destinationList = resultList;
                        }
                        return $scope.destinationList;
                        // $scope.showDestinationList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Origin List');
                }
            );

        }


        $scope.ajaxCarrierEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CarrierByTransportMode.fetch({
                "transportMode": 'Air'
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.carrierMasterList = data.responseObject.searchResult;
                        // $scope.showCarrierMasterList=true;
                        return $scope.carrierMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Carrier List Based on Air');
                }
            );

        }


        $scope.ajaxTosEvent = function(object) {
            console.log("ajaxTosEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.tosList = data.responseObject.searchResult;
                        // $scope.showTosList=true;
                        return $scope.tosList
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Tos');
                }
            );;

        }


        $scope.ajaxDocDestination = function(object) {

            console.log("ajaxDestination ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.docDestinationList = [];
                        var resultList = data.responseObject.searchResult;
                        if (resultList != null && resultList.length != 0) {
                            $scope.docDestinationList = resultList;
                        }
                        return $scope.docDestinationList;
                        // $scope.showDocDestinationList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Destination List');
                }
            );

        }


        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }
        $scope.serviceNameRender = function(item) {
            return {
                label: item.serviceName,
                item: item
            }

        }
        $scope.PortNameRender = function(item) {
            return {
                label: item.portName,
                item: item
            }

        }
        $scope.carrierNameRender = function(item) {
            return {
                label: item.carrierName,
                item: item
            }

        }

        $scope.currencyRender = function(item) {
            return {
                label: item.currencyCode,
                item: item
            }
        }


        $scope.choosePartyAddress = function(addressKey) {
            if (addressKey == 'shipper') {
                if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper.status != 'Block' && ($scope.consolDocumentClone.shipperAddress == undefined || $scope.consolDocumentClone.shipperAddress == null)) {
                    $scope.consolDocumentClone.shipperAddress = {};
                    $scope.getPartyAddress($scope.consolDocumentClone.shipper, $scope.consolDocumentClone.shipperAddress);
                } else if ($scope.consolDocumentClone.shipper != undefined && $scope.consolDocumentClone.shipper != null && $scope.consolDocumentClone.shipper.status != 'Block') {
                    $scope.getPartyAddress($scope.consolDocumentClone.shipper, $scope.consolDocumentClone.shipperAddress);
                }
            }
            if (addressKey == 'consignee') {
                if ($scope.consolDocumentClone.consignee != undefined && $scope.consolDocumentClone.consignee != null && $scope.consolDocumentClone.consignee.status != 'Block' && ($scope.consolDocumentClone.consigneeAddress == undefined || $scope.consolDocumentClone.consigneeAddress == null)) {
                    $scope.consolDocumentClone.consigneeAddress = {};
                    $scope.getPartyAddress($scope.consolDocumentClone.consignee, $scope.consolDocumentClone.consigneeAddress);
                } else if ($scope.consolDocumentClone.consignee != undefined && $scope.consolDocumentClone.consignee != null && $scope.consolDocumentClone.consignee.status != 'Block') {
                    $scope.getPartyAddress($scope.consolDocumentClone.consignee, $scope.consolDocumentClone.consigneeAddress);
                }
            }

            if (addressKey == 'issuingAgent') {
                if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.status != 'Block' && ($scope.consolDocumentClone.issuingAgentAddress == undefined || $scope.consolDocumentClone.issuingAgentAddress == null)) {
                    $scope.consolDocumentClone.issuingAgentAddress = {};
                    $scope.getPartyAddress($scope.consolDocumentClone.issuingAgent, $scope.consolDocumentClone.issuingAgentAddress);
                } else if ($scope.consolDocumentClone.issuingAgent != undefined && $scope.consolDocumentClone.issuingAgent != null && $scope.consolDocumentClone.issuingAgent.status != 'Block') {
                    $scope.getPartyAddress($scope.consolDocumentClone.issuingAgent, $scope.consolDocumentClone.issuingAgentAddress);
                }
            }

            if (addressKey == 'firstNotify') {
                if ($scope.consolDocumentClone.firstNotify != undefined && $scope.consolDocumentClone.firstNotify != null && $scope.consolDocumentClone.firstNotify.status != 'Block' && ($scope.consolDocumentClone.firstNotifyAddress == undefined || $scope.consolDocumentClone.firstNotifyAddress == null)) {
                    $scope.consolDocumentClone.firstNotifyAddress = {};
                    $scope.getPartyAddress($scope.consolDocumentClone.firstNotify, $scope.documentDetail.firstNotifyAddress);
                } else if ($scope.consolDocumentClone.firstNotify != undefined && $scope.consolDocumentClone.firstNotify != null && $scope.consolDocumentClone.firstNotify.status != 'Block') {
                    $scope.getPartyAddress($scope.consolDocumentClone.firstNotify, $scope.documentDetail.firstNotifyAddress);
                }
            }

            if (addressKey == 'secondNotify') {
                if ($scope.consolDocumentClone.secondNotify != undefined && $scope.consolDocumentClone.secondNotify != null && $scope.consolDocumentClone.secondNotify.status != 'Block' && ($scope.consolDocumentClone.secondNotifyAddress == undefined || $scope.consolDocumentClone.secondNotifyAddress == null)) {
                    $scope.documentDetail.secondNotifyAddress = {};
                    $scope.getPartyAddress($scope.consolDocumentClone.secondNotify, $scope.consolDocumentClone.secondNotifyAddress);
                } else if ($scope.consolDocumentClone.secondNotify != undefined && $scope.consolDocumentClone.secondNotify != null && $scope.consolDocumentClone.secondNotify.status != 'Block') {
                    $scope.getPartyAddress($scope.consolDocumentClone.secondNotify, $scope.consolDocumentClone.secondNotifyAddress);
                }
            }
        }

        $scope.getPartyAddress = function(obj, mapToAddrss) {
            if (obj != null && obj.id != null) {
                for (var i = 0; i < obj.partyAddressList.length; i++) {
                    if (obj.partyAddressList[i].addressType == 'Primary') {
                        mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss, obj, obj.partyAddressList[i]);
                    }
                }
            }
        }


        $scope.openDefaultTab = function() {
            console.log("openDefaultTab in edit page is running...");

            var tab = ProvisionalService.getTabVal();

            if (tab != undefined && tab != null && tab != "") {
                if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_ACCOUNTS_MODIFY)) {
                    $scope.Tabs = 'accounts';
                }
            }
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DETAILS_MODIFY)) {
                $scope.Tabs = 'general';
            } else {
                $scope.Tabs = null;
                console.log("No Permission to perform this action.");
            }
        }


        $scope.downloadMAWB = function() {

            if ($scope.validateDynamicMAWBDocument(0)) {

                var node = document.getElementById('awb_image');
                var img = new Image();
                domtoimage.toPng(node)
                    .then(function(dataUrl) {
                        img.src = dataUrl;
                        // $("#result_image").attr("src",img.src);
                        console.log("final image got it");
                        var doc = new jsPDF();
                        var imgData = "";
                        doc.addImage(img.src, "PNG", 0, 0, -124, 0);
                        console.log("got some image ", img);
                        doc.save('mawb.pdf');
                    })
                    .catch(function(error) {
                        // $scope.printflag = false;
                        console.error('oops, something went wrong!', error);
                    });
            } else {
                console.log("validation failed in dynamic document");
            }

        }

        $scope.fetchShipmentDetailsByMawbNo = function() {

            if ($scope.consol != undefined && $scope.consol.id == undefined && $scope.validateConsol(1) && $scope.validateConsol(70)) {
                $scope.spinner = true;
                uiShipmentFactory.GetShipmentByMawbNo.fetch({
                    'mawbNo': $scope.consol.consolDocument.mawbNo,
                    'serviceId': $scope.consol.serviceMaster.id
                }, function(data) {
                    if (data.responseCode == 'ERR0' && data.responseObject != null) {
                        $scope.consol = uiShipmentDataService.consolContinueAfterReference(undefined, data.responseObject, 'forConsol');
                    }
                    $scope.spinner = false;
                });
            } else {
                $scope.spinner = false;
                console.log("error in mawbNo and Consol");
            }
        }

        /*$scope.linkAllServiceToConsol=function(){
            if($scope.consol!=undefined && $scope.validateConsol(1) && $scope.validateConsol(9) && $scope.validateConsol(10) && $scope.validateConsol(70)){
                        $scope.spinner=true;
                        $scope.consolDto={};
                        $scope.consolDto.consolUid =$scope.consol.consolUid==null || $scope.consol.consolUid==undefined?"-1":$scope.consol.consolUid,
                        $scope.consolDto.serviceId = $scope.consol.serviceMaster.id;
                        $scope.consolDto.locationId = $scope.consol.location.id;
                        $scope.consolDto.polId = $scope.consol.pol.id;	
                        $scope.consolDto.podId = $scope.consol.pod.id;
                    	
                        if($scope.consol.etd!=undefined && $scope.consol.etd!=null){
                            $scope.consolDto.etd=$rootScope.sendApiStartDateTime($scope.consol.etd)
                            }
                        	
                            if($scope.consol.eta!=undefined && $scope.consol.eta!=null){
                                $scope.consolDto.eta=$rootScope.sendApiStartDateTime($scope.consol.eta);
                            }
                            if($scope.consol.carrier!=undefined && $scope.consol.carrier!=null && $scope.consol.carrier.id!=undefined && $scope.consol.carrier.id!=null){
                                $scope.consolDto.carrierId=$scope.consol.carrier.id;
                            }
                    	
                $scope.consolDto.mawbNo=$scope.consol.consolDocument.mawbNo;
                ConsolShipmentLink.link($scope.consolDto).$promise.then(
                    function(data) {
                 if (data.responseCode == 'ERR0') {
                        $scope.shipmentServiceData = [];
                        var tmpList = data.responseObject.searchResult;
                        var i=0;
                        var j=0;
                        var flag=true;
                        $scope.tmpShipmentLink = angular.copy($scope.consol.shipmentLinkList);
                        if($scope.consol.shipmentLinkList==null || $scope.consol.shipmentLinkList.length==0){
                            $scope.shipmentServiceData = tmpList;
                        }else{
                            for(i=0; i<tmpList.length;i++){
                                flag=true;
                                for(j=0; j<$scope.consol.shipmentLinkList.length;j++){
                                    if(tmpList[i].id==$scope.consol.shipmentLinkList[j].service.id){
                                            flag=false;
                                            break;
                                    }
                                }
                                if(flag){
                                    $scope.shipmentServiceData.push(tmpList[i])
                                }
                            }
                        }
                        for(i=0; i<$scope.shipmentServiceData.length;i++){
                            $scope.shipmentServiceData[i].serviceCodeWithShipmentId=$scope.shipmentServiceData[i].serviceMaster.serviceCode+'-'+$scope.shipmentServiceData[i].shipmentUid;
                        }
                        $scope.tmpLinkAll(); 
                    	
                    //$scope.tmpShipmentLink = angular.copy($scope.consol.shipmentLinkList);	
                	
                    var service = data.responseObject.searchResult;
                	
                    $scope.consol=uiShipmentDataService.goToConsol(sipmentObj,service,'forConsol');
                        	
                    } else {
                        $scope.shipmentServiceData = [];
                    }
                 $scope.spinner=false;
                },
                function(error) {
                    console.log("Consol - Shipment - Service- Link get Failed : " + error)
                    $scope.shipmentServiceData = [];
                    $scope.spinner=false;
                });
            	
            }else{
                $scope.Tabs='general';
                console.log("validation failed in link the list");
            }
        }*/

        $scope.tmpLinkAll = function() {

            if ($scope.shipmentServiceData != undefined && $scope.shipmentServiceData != null) {
                if ($scope.consol.shipmentLinkList == null || $scope.consol.shipmentLinkList.length == 0) {
                    $scope.consol.shipmentLinkList = [];
                }
                for (var i = 0; i < $scope.shipmentServiceData.length; i++) {
                    $scope.tmpShipmentLink = {};
                    $scope.tmpShipmentLink.service = $scope.shipmentServiceData[i];
                    $scope.consol.shipmentLinkList.push($scope.tmpShipmentLink);
                    $scope.serviceDimensionAdded();
                }
                $scope.Tabs = 'consol';
            }

        }


        $scope.fetchMawb = function(consol, fieldName) {
            $rootScope.clientMessage = null;
            if ($scope.validateConsol(14) && $scope.validateConsol(8)) {
                if (consol.consolDocument != undefined && consol.consolDocument.mawbNo == undefined || consol.consolDocument.mawbNo == null || consol.consolDocument.mawbNo == "") {
                    GetNextMawb.fetch({
                        "carrierId": consol.carrier.id,
                        "portId": consol.origin.id
                    }).$promise.then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                if (data.responseObject != null) {
                                    // $scope.consol.consolDetail.mawbNo=data.responseObject.mawbNo;
                                    $scope.consol.consolDocument.mawbNo = data.responseObject.mawbNo;
                                    $rootScope.navigateToNextField(fieldName);
                                    if ($scope.consol.consolDocument.mawbGeneratedDate == undefined || $scope.consol.consolDocument.mawbGeneratedDate == null || $scope.consol.consolDocument.mawbGeneratedDate == "") {
                                        $scope.consol.consolDocument.mawbGeneratedDate = $rootScope.dateAndTimeToString(new Date());
                                        $rootScope.navigateToNextField('atdDateMoreInfoConsol');
                                    }
                                    if (data.responseObject.remindNo != null) {
                                        $rootScope.clientMessage = $rootScope.nls["ERR100023"];
                                    }
                                } else {
                                    $rootScope.clientMessage = $rootScope.nls["ERR100022"];
                                    $rootScope.navigateToNextField('mawbMoreInfoConsol');
                                }
                            }
                        },
                        function(errResponse) {
                            console.error('Error while fetching mawb no');
                        }
                    );
                } else {
                    $rootScope.clientMessage = $rootScope.nls["ERR100026"];
                }
            }
        }

        // ajax for dynamic


        $scope.ajaxPortOfDocLoadingDynamic = function(object) {
            console.log("ajaxPortOfLoadingEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": $scope.consolClone.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.portOfDocLoadingList = [];
                        var resultList = data.responseObject.searchResult;
                        return resultList;
                        // $scope.showPortOfDocLoadingList=true;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching PortOfLoading List');
                }
            );

        }

        // create invoice from consol
        $scope.getAccountsFromShipment = function(service) {
            $scope.getAccountDataByLinkService(service);
            $scope.provisionalViewByConsolServiceUid(service);

        }
        $scope.getAccountsFromMaster = function(consol) {
            if (consol.shipmentLinkList.length == 1) {
                $scope.getAccountsFromShipment(consol.shipmentLinkList[0].service);
            } else {
                $scope.getAccountData(consol.consolUid);
                $scope.getProvisionalData(consol.consolUid);
            }

        }




        $scope.getAccountDataByLinkService = function(shipmentService) {
            $scope.searchDto = {};
            $scope.searchDto.param1 = shipmentService.id;
            $scope.invoiceSpinner = true;
            //Bellow line commented ,as discussed with sathish robert 
            //$scope.searchDto.param2=$scope.consol.consolUid;
            return ShipmentServiceInvoiceList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.invoiceListByService = data.responseObject;
                        $scope.invoiceSpinner = false;

                    }
                },
                function(errResponse) {
                    console.error('Error while fetching invoicelist');
                    $scope.invoiceSpinner = false;
                }
            );



        }


        // invoice list under consol



        $scope.getAccountData = function(consolUid) {
            $scope.invoiceSpinner = true;
            ConsolInvoiceList.get({
                    "consolUid": consolUid
                },
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log(" Successfull", data.responseObject);
                        $scope.invoiceList = data.responseObject;
                        $scope.invoiceSpinner = false;

                    } else {
                        console.log(" Failed " + data.responseDescription);
                        $scope.invoiceSpinner = false;
                    }
                },
                function(error) {
                    console.log(" Failed : " + error);
                    $scope.invoiceSpinner = false;
                });
        }

        $scope.filterCustomerAgent = function(type) {

            if (type == 'Customer')
                $scope.customerAgent = 'Customer';
            if (type == 'Agent')
                $scope.customerAgent = 'Agent';
            if (type == 'All')
                $scope.customerAgent = undefined;

        }


        $scope.filterInvoiceCreditNote = function(type) {

            if (type == 'Invoice')
                $scope.documentTypeCode = 'INV';
            if (type == 'Credit Note')
                $scope.documentTypeCode = 'CRN';
            if (type == 'All')
                $scope.documentTypeCode = undefined;

        }

        $scope.getPhoneandFax = function(partyObj) {
            $scope.partyPhandFax = "";
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].phone != undefined && partyObj.partyAddressList[i].phone != null) {
                    if ($scope.partyPhandFax.includes("PH")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].phone;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "  " + "PH:" + partyObj.partyAddressList[i].phone;
                    }
                }
            }
            for (var i = 0; i < partyObj.partyAddressList.length; i++) {
                if (partyObj.partyAddressList[i].fax != undefined && partyObj.partyAddressList[i].fax != null) {
                    if ($scope.partyPhandFax.includes("FAX")) {
                        $scope.partyPhandFax = $scope.partyPhandFax + "," + partyObj.partyAddressList[i].fax;
                    } else {
                        $scope.partyPhandFax = $scope.partyPhandFax + "  " + "FAX:" + partyObj.partyAddressList[i].fax;

                    }
                }

            }
            return $scope.partyPhandFax;
        }

        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                if ($scope.consolClone.chargeListClone != undefined && $scope.consolClone.chargeListClone[i] != undefined) {
                    input.push($scope.consolClone.chargeListClone[i]);
                }
            }
            return input;
        }

        $scope.ajaxChargeEvent = function(object) {
            console.log("ajaxChargeEvent ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.chargeMasterList = data.responseObject.searchResult;
                        return $scope.chargeMasterList;
                        /* $scope.showChargeMasterList=true; */
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Charge');
                }
            );
        }



        $scope.chargeRender = function(item) {
            return {
                label: item.chargeCode,
                item: item
            }
        }

        $scope.nextToFocus = function(nextIdValue) {
            console.log("called." + nextIdValue);
            $scope.navigateToNextFieldWithOutTime(nextIdValue);
        }

        $scope.navigateToNextFieldWithOutTime = function(id) {
            if (id != undefined && id != null) {
                var docElement = document.getElementById(id);
                if (docElement != undefined && docElement != null) {
                    docElement.focus();
                }
            }
        }


        $scope.selectEtaDateNavigation = function(nextFocus1, nextFocus2) {
            if ($scope.validateConsol(23)) {
                if ($scope.consol.id != undefined && $scope.consol.id != null) {
                    $rootScope.navigateToNextField(nextFocus2);
                }
                $rootScope.navigateToNextField(nextFocus1);
            }
        }
        $scope.selectEtdDateNavigation = function(nextIdValue) {
            if ($scope.validateConsol(15)) {
                if (nextIdValue != undefined) {

                    $rootScope.navigateToNextField(nextIdValue);

                }
            }
        }

        $scope.navigateATDdate = function(nextIdValue) {
            // if($scope.validateConsol(16)) {
            if (nextIdValue != undefined) {
                $rootScope.navigateToNextField(nextIdValue);
            }
            /* } */
        }


        $scope.navigateATAdate = function(nextIdValue, consol) {
            if ($rootScope.checkIsTranshipment(consol)) {
                $rootScope.navigateToNextField('MasterShipment' + 'connectionMove0');
            } else {
                $scope.Tabs = 'consol';
            }
        }

        // Provisional Cost

        $scope.refmodel = function(provisionalItem) {
            var newScope = $scope.$new();
            newScope.provisionalItem = provisionalItem;
            var myTotalOtherModal = $modal({
                scope: newScope,
                templateUrl: 'app/components/finance/provisional_cost/view/refpopup.html',
                show: false
            });
            myTotalOtherModal.$promise.then(myTotalOtherModal.show);
        };

        $scope.provisionalViewByConsolServiceUid = function(shipmentServiceDetail) {
            $scope.provisionSpinner = true;
            ProvisionalViewByShipmentServiceUid.get({
                serviceuid: shipmentServiceDetail.serviceUid
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("ProvisionalViewByShipmentServiceUid Successfull")
                    $scope.provisionalForLink = data.responseObject;
                    console.log("Provisional Object : ", $scope.provisional);
                    $scope.provisionSpinner = false;
                } else {
                    console.log("ProvisionalViewByShipmentServiceUid Failed " + data.responseDescription);
                    $scope.provisionSpinner = false;
                }
            }, function(error) {
                console.log("ProvisionalViewByShipmentServiceUid Failed : " + error);
                $scope.provisionSpinner = false;
            });
        }

        $scope.getProvisionalData = function(masterUid) {
            $scope.provisionSpinner = true;
            ProvisionalViewByConsolUid.get({
                masteruid: masterUid
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("ProvisionalViewByConsolUid Successfull")
                    $scope.provisionalObj = data.responseObject;
                    console.log("Provisional Object : ", $scope.provisional);
                    $scope.provisionSpinner = false;
                } else {
                    console.log("ProvisionalViewByConsolUid Failed " + data.responseDescription);
                    $scope.provisionSpinner = false;
                }
            }, function(error) {
                console.log("ProvisionalViewByConsolUid Failed : " + error);
                $scope.provisionSpinner = false;
            });
        }

        $scope.menuTabClick = function(focusGoTo, tabKey) {
            if (focusGoTo != undefined && focusGoTo != null && focusGoTo != '')
                $rootScope.navigateToNextField(focusGoTo);
            if ($scope.consol.id != undefined) {
                if (tabKey == 'general') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DETAILS_MODIFY)) {
                        $scope.Tabs = tabKey;
                    };
                }
                if (tabKey == 'consol') {
                    if ($scope.consol.pod.portName != $scope.consol.destination.portName) {
                        if (!$scope.consol.connectionList.length) {
                            Notification.error("Please add a connection , as the Airport Of Discharge is differ from Destination");
                            return false;
                        } else if (!$scope.consol.connectionList[0].hasOwnProperty('carrierMaster')) {
                            Notification.error("Please add a connection , as the Airport Of Discharge is differ from Destination");
                            return false;
                        }
                    }
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'documents') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DOCUMENT_MODIFY)) {
                        $scope.Tabs = tabKey;
                        $scope.setDefaultValuesForDocument();
                        if ($scope.consol != undefined && $scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.importExport == 'Import') {
                            $scope.navigateToNextField("importconsigneeConsolDoc");
                        }
                    }
                }
                if (tabKey == 'connections') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'attachMent') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'pickup') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_PICK_UP_OR_DELIVERY_MODIFY)) {
                        $scope.Tabs = tabKey;
                    }
                }
                if (tabKey == 'rates') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'events') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'accounts') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_ACCOUNTS_MODIFY)) {
                        $scope.Tabs = tabKey;
                        $scope.getProvisionalData($scope.consol.consolUid);
                        $scope.getAccountData($scope.consol.consolUid);
                    }
                }
                if (tabKey == 'references') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'notes') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_NOTES_MODIFY)) {
                        $scope.Tabs = tabKey;
                    }
                }
                if (tabKey == 'aes') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_AES_MODIFY)) {
                        $scope.aesSearchFunction($scope.consol);
                        $scope.Tabs = tabKey;
                    }
                }


            } else {
                if (tabKey == 'general') {
                    if ($scope.consol.pod.portName != $scope.consol.destination.portName) {
                        if (!$scope.consol.connectionList.length) {
                            Notification.error("Please add a connection , as the Airport Of Discharge is differ from Destination");
                            $scope.togglePreviousButton = false;
                        } else if (!$scope.consol.connectionList[0].hasOwnProperty('carrierMaster')) {
                            Notification.error("Please add a connection , as the Airport Of Discharge is differ from Destination");
                            $scope.togglePreviousButton = false;
                        }
                    }
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'consol') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'documents') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DOCUMENT_CREATE)) {
                        $scope.setDefaultValuesForDocument();
                        $scope.Tabs = tabKey;
                    }
                }
                if (tabKey == 'connections') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'attachMent') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'pickup') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_PICK_UP_OR_DELIVERY_CREATE)) {
                        $scope.Tabs = tabKey;
                    }
                }
                if (tabKey == 'rates') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'events') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'accounts') {
                    if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_ACCOUNTS_CREATE)) {
                        $scope.Tabs = tabKey;
                        $scope.getProvisionalData($scope.consol.consolUid);
                        $scope.getAccountData($scope.consol.consolUid);
                    }
                }
                if (tabKey == 'references') {
                    $scope.Tabs = tabKey;
                }
                if (tabKey == 'notes') {
                    $scope.Tabs = tabKey;
                }
            }


        }

        // Provisional end

        $scope.afterSet = function() {
            try {
                if ($scope.consol != null && $scope.consol.id != null && $scope.consol.id != -1) {
                    $rootScope.navigateToNextField('freightTerm');
                    $scope.consol.title = 'Edit Master ID ' + $scope.consol.consolUid;
                } else {

                    if ($stateParams.fromState === "fromShipment") {
                        $rootScope.navigateToNextField('freightTerm');
                    } else {
                        $rootScope.navigateToNextField('serviceMaster');
                    }
                    $scope.consol.title = 'Add Master Shipment';

                }
                $scope.setOldDataVal();
            } catch (e) {
                console.log("some undefined");
            }

        }

        function createNew(wizard) {
            var consol = {};
            if (wizard) {

                AirExportService.get(function(data) {
                        if (data.responseCode == "ERR0") {
                            consol.serviceMaster = data.responseObject;
                            $scope.consol.allServiceSelected = false;
                        }
                    },
                    function(error) {
                        console.log("Consol get Failed : " + error)
                    });

            }

            consol.agent = {};
            consol.agentAddress = {};
            consol.shipmentLinkList = [];
            consol.chargeList = [{}];
            //consol.referenceList = [{}];
            consol.currency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            consol.currencyRate = 1;
            //consol.currencyRate = $rootScope.baseCurrenyRate[consol.currency.id]==null?1:$rootScope.baseCurrenyRate[consol.currency.id].csell;
            consol.ppcc = false;
            consol.jobStatus = 'Active';
            consol.createdBy = $rootScope.userProfile.employee;
            consol.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            consol.totalAmount = 0.000;
            consol.documentList = [];
            consol.location = $rootScope.userProfile.selectedUserLocation;
            consol.isJobCompleted = false;
            consol.directShipment = false;
            consol.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
            consol.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
            // consol.consolDetail={};
            consol.consolDocument = {};
            consol.consolDocument.isAgreed = false;
            consol.consolDocument.isMarksNo = false;
            consol.consolDocument.shipper = {};
            consol.consolDocument.shipperAddress = {};
            /*
             * consol.consolDo$scope.attachConfig.data = []
             * $scope.populateAttachment($scope.attachConfig.data);
             */
            // $scope.consol.id==null?$rootScope.setNavigate3("Add
            // Master Shipment"):$rootScope.setNavigate3("Edit
            // Master Shipment");
            consol.consolDocument.forwarder = {};
            consol.consolDocument.forwarderAddress = {};
            consol.consolDocument.consignee = {};
            consol.consolDocument.consigneeAddress = {};
            consol.consolDocument.firstNotify = {};
            consol.consolDocument.firstNotifyAddress = {};
            consol.consolDocument.secondNotify = {};
            consol.consolDocument.secondNotifyAddress = {};
            consol.consolDocument.agent = {};
            consol.consolDocument.agentAddress = {};
            consol.consolDocument.issuingAgent = {};
            consol.consolDocument.issuingAgentAddress = {};
            consol.consolDocument.origin = {};
            consol.consolDocument.destination = {};
            consol.consolDocument.packMaster = {};
            consol.consolDocument.pol = {};
            consol.consolDocument.pod = {};
            consol.consolDocument.isHold = false;

            if ($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS') == 'true') {
                consol.consolDocument.dimensionUnit = true;
                consol.consolDocument.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
            } else {
                consol.consolDocument.dimensionUnit = false;
                consol.consolDocument.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
            }

            consol.consolDocument.documentReqDate = $rootScope.dateToString(new Date());
            consol.consolDocument.dimensionList = [];
            consol.consolDocument.dimensionList.push({});
            consol.pickUpDeliveryPoint = {};
            consol.pickUpDeliveryPoint.isOurPickUp = false;
            consol.pickUpDeliveryPoint.transporter = {};
            consol.pickUpDeliveryPoint.pickupPoint = {};
            consol.pickUpDeliveryPoint.pickupFrom = {};
            consol.pickUpDeliveryPoint.deliveryPoint = {};
            consol.pickUpDeliveryPoint.deliveryFrom = {};
            consol.pickUpDeliveryPoint.doorDeliveryPoint = {};
            consol.pickUpDeliveryPoint.doorDeliveryFrom = {};
            //consol.consolAttachmentList = [{}];
            //consol.eventList = [{isCompleted : 'Yes',followUpRequired : 'No'}];
            consol.connectionList = [{
                connectionStatus: 'Planned'
            }];
            // Code to assign default package in consol
            // documents starts here...
            var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];
            if (defaultPackageCode != undefined && defaultPackageCode != null) {
                console.log("Default Package is assigned....");
                PackGetByCode.get({
                    code: defaultPackageCode
                }, function(data) {
                    if (data.responseCode == "ERR0" || data.responseCode == "ERR1914") {
                        console.log("Successful while getting pack.", data)
                        consol.consolDocument.packMaster = data.responseObject;
                    }
                }, function(error) {
                    console.log("Error while getting pack.", error)
                });
            } else {
                console.log("Default Package Code is not defined in Default Master Data....");
            }

            // Code to assign default package in consol
            // documents ends here...

            return consol;

        }

        $scope.reloadData = function(object) {
            $scope.consol = object;
            $scope.consol.etd = $rootScope.convertToDate($scope.consol.etd);
            $scope.consol.eta = $rootScope.convertToDate($scope.consol.eta);
            $scope.consol.atd = $rootScope.convertToDate($scope.consol.atd);
            $scope.consol.ata = $rootScope.convertToDate($scope.consol.ata);
            if ($scope.consol.consolDocument != undefined && $scope.consol.consolDocument.mawbIssueDate != undefined && $scope.consol.consolDocument.mawbIssueDate != null) {
                $scope.consol.consolDocument.mawbIssueDate = $rootScope.convertToDate($scope.consol.consolDocument.mawbIssueDate);
            } else {
                if ($scope.consol.consolDocument != undefined && $scope.consol.consolDocument != null) {
                    $scope.consol.consolDocument.mawbIssueDate = null;
                } else {
                    $scope.consol.consolDocument = {};
                }
            }
            /*
             * if($scope.consol.consolDetail==undefined){
             * $scope.consol.consolDetail={}; }
             */
            if ($scope.consol.pickUpDeliveryPoint != undefined && $scope.consol.pickUpDeliveryPoint != null &&
                $scope.consol.pickUpDeliveryPoint != "") {

                if ($scope.consol.pickUpDeliveryPoint.pickUpFollowUp != null)
                    $scope.consol.pickUpDeliveryPoint.pickUpFollowUp = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.pickUpFollowUp);

                if ($scope.consol.pickUpDeliveryPoint.pickUpPlanned != null)
                    $scope.consol.pickUpDeliveryPoint.pickUpPlanned = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.pickUpPlanned);

                if ($scope.consol.pickUpDeliveryPoint.pickUpActual != null)
                    $scope.consol.pickUpDeliveryPoint.pickUpActual = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.pickUpActual);

                if ($scope.consol.pickUpDeliveryPoint.deliveryExpected != null)
                    $scope.consol.pickUpDeliveryPoint.deliveryExpected = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.deliveryExpected);

                if ($scope.consol.pickUpDeliveryPoint.deliveryActual != null)
                    $scope.consol.pickUpDeliveryPoint.deliveryActual = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.deliveryActual);

                if ($scope.consol.pickUpDeliveryPoint.doorDeliveryExpected != null)
                    $scope.consol.pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.doorDeliveryExpected);

                if ($scope.consol.pickUpDeliveryPoint.doorDeliveryActual != null)
                    $scope.consol.pickUpDeliveryPoint.doorDeliveryActual = $rootScope.convertToDate($scope.consol.pickUpDeliveryPoint.doorDeliveryActual);

            }



            if ($scope.consol.connectionList != null && $scope.consol.connectionList.length != undefined) {

                for (var i = 0; i < $scope.consol.connectionList.length; i++) {
                    $scope.consol.connectionList[i].eta = $rootScope.convertToDate($scope.consol.connectionList[i].eta);
                    $scope.consol.connectionList[i].etd = $rootScope.convertToDate($scope.consol.connectionList[i].etd);
                }

            }



            if ($scope.consol.eventList != undefined && $scope.consol.eventList != null && $scope.consol.eventList.length != 0) {
                for (var j = 0; j < $scope.consol.eventList.length; j++) {
                    $scope.consol.eventList[j].eventDate = $rootScope.convertToDate($scope.consol.eventList[j].eventDate);
                    if ($scope.consol.eventList[j].followUpDate != null) {
                        $scope.consol.eventList[j].followUpDate = $rootScope.convertToDate($scope.consol.eventList[j].followUpDate);
                    } else {
                        $scope.consol.eventList[j].followUpDate = null;
                    }
                }
            }

        }

        $scope.editData = function(object) {
            $scope.consol = object;
            $scope.consol.etd = $rootScope.dateAndTimeToString($scope.consol.etd);
            $scope.consol.eta = $rootScope.dateAndTimeToString($scope.consol.eta);
            $scope.consol.atd = $rootScope.dateAndTimeToString($scope.consol.atd);
            $scope.consol.ata = $rootScope.dateAndTimeToString($scope.consol.ata);
            $scope.consol.consolDocument.mawbIssueDate = $rootScope.dateToString($scope.consol.consolDocument.mawbIssueDate);
            $scope.consol.consolDocument.mawbGeneratedDate = $rootScope.dateToString($scope.consol.consolDocument.mawbGeneratedDate);

            $scope.consol.consolDocument.canIssuedDate = $rootScope.dateToString($scope.consol.consolDocument.canIssuedDate);

            $scope.consol.ppcc = $scope.consol.ppcc == 'Prepaid' ? false : true;

            if ($scope.consol.serviceMaster.importExport == "Import") {
                $scope.consol.whoRouted = $scope.consol.whoRouted == 'Self' ? false : true;
            }
            $scope.consol.directShipment = $scope.consol.directShipment === 'No' || !$scope.consol.directShipment ? false : true;
            $scope.consol.consolDocument.dimensionUnit = $scope.consol.consolDocument.dimensionUnit == 'INCHORPOUNDS' ? true : false;
            $scope.consol.consolDocument.dimensionUnitValue = $scope.consol.consolDocument.dimensionUnit == 'INCHORPOUNDS' ? 366 : 6000;

            if ($scope.consol.consolDocument.dimensionList == undefined || $scope.consol.consolDocument.dimensionList == null) {
                $scope.consol.consolDocument.dimensionList = [{}];
                console.log("--------------------------------------Adding Empty Row in Dimension Table..............................")
            }

            if ($scope.consol.pickUpDeliveryPoint != undefined && $scope.consol.pickUpDeliveryPoint != null &&
                $scope.consol.pickUpDeliveryPoint != "") {

                if ($scope.consol.pickUpDeliveryPoint.pickUpFollowUp != null)
                    $scope.consol.pickUpDeliveryPoint.pickUpFollowUp = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.pickUpFollowUp);

                if ($scope.consol.pickUpDeliveryPoint.pickUpPlanned != null)
                    $scope.consol.pickUpDeliveryPoint.pickUpPlanned = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.pickUpPlanned);

                if ($scope.consol.pickUpDeliveryPoint.pickUpActual != null)
                    $scope.consol.pickUpDeliveryPoint.pickUpActual = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.pickUpActual);

                if ($scope.consol.pickUpDeliveryPoint.deliveryExpected != null)
                    $scope.consol.pickUpDeliveryPoint.deliveryExpected = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.deliveryExpected);

                if ($scope.consol.pickUpDeliveryPoint.deliveryActual != null)
                    $scope.consol.pickUpDeliveryPoint.deliveryActual = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.deliveryActual);

                if ($scope.consol.pickUpDeliveryPoint.doorDeliveryExpected != null)
                    $scope.consol.pickUpDeliveryPoint.doorDeliveryExpected = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.doorDeliveryExpected);

                if ($scope.consol.pickUpDeliveryPoint.doorDeliveryActual != null)
                    $scope.consol.pickUpDeliveryPoint.doorDeliveryActual = $rootScope.dateAndTimeToString($scope.consol.pickUpDeliveryPoint.doorDeliveryActual);

            }


            if ($scope.consol.connectionList != null &&
                $scope.consol.connectionList != undefined &&
                $scope.consol.connectionList.length > 0) {

                for (var i = 0; i < $scope.consol.connectionList.length; i++) {
                    $scope.consol.connectionList[i].eta = $rootScope.dateToString($scope.consol.connectionList[i].eta);
                    $scope.consol.connectionList[i].etd = $rootScope.dateToString($scope.consol.connectionList[i].etd);
                }

            } else {

                console.log("--------------------------------------Adding Empty Row in Connection Table..............................")
                $scope.consol.connectionList = [{
                    connectionStatus: 'Planned'
                }];
            }

            if ($scope.consol.eventList != undefined &&
                $scope.consol.eventList != null &&
                $scope.consol.eventList.length != 0) {

                for (var j = 0; j < $scope.consol.eventList.length; j++) {
                    $scope.consol.eventList[j].eventDate = $rootScope.dateToString($scope.consol.eventList[j].eventDate);

                    if ($scope.consol.eventList[j].followUpDate != null) {
                        $scope.consol.eventList[j].followUpDate = $rootScope.dateToString($scope.consol.eventList[j].followUpDate);
                    } else {
                        $scope.consol.eventList[j].followUpDate = null;
                    }
                }
            } else {
                console.log("--------------------------------------Adding Empty Row in Events Table..............................")
                    //$scope.consol.eventList = [{isCompleted : 'Yes',followUpRequired : 'No'}];
            }

            if ($scope.consol.chargeList == undefined || $scope.consol.chargeList == null || $scope.consol.chargeList.length == 0) {
                console.log("--------------------------------------Adding Empty Row in charge Table..............................")
                $scope.consol.chargeList.push({});
            }

            /*if($scope.consol.referenceList == undefined || $scope.consol.referenceList == null || $scope.consol.referenceList.length == 0) {
                console.log("--------------------------------------Adding Empty Row in Reference Table..............................")
                $scope.consol.referenceList.push({});
            }
            	
            if($scope.consol.consolAttachmentList == undefined || $scope.consol.consolAttachmentList == null || $scope.consol.consolAttachmentList.length == 0) {
                console.log("--------------------------------------Adding Empty Row in Attachment Table..............................")
                $scope.consol.consolAttachmentList.push({});
            }*/
        }

        $scope.getPortGroup = function(portId) {



        }


        //default values added as per demo fb..Jun 2nd

        $scope.setDefaultValuesForDocument = function() {

            if ($scope.consol != undefined && $scope.consol.consolDocument != undefined && $scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.id != undefined &&
                $scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.importExport == 'Export' && ($scope.consol.consolDocument.accountingInformation == undefined || $scope.consol.consolDocument.accountingInformation == null ||
                    $scope.consol.consolDocument.accountingInformation == "")) {
                var accDefaultValue = $rootScope.appMasterData['default.accountinginformation'];
                if (accDefaultValue != undefined && accDefaultValue != null) {
                    $scope.consol.consolDocument.accountingInformation = accDefaultValue;
                }
            }

            if ($scope.consol != undefined && $scope.consol.consolDocument != undefined && $scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.id != undefined &&
                $scope.consol.serviceMaster != undefined && $scope.consol.serviceMaster.importExport == 'Export' && ($scope.consol.consolDocument.handlingInformation == undefined || $scope.consol.consolDocument.handlingInformation == null ||
                    $scope.consol.consolDocument.handlingInformation == "")) {
                var defHandlingValue = $rootScope.appMasterData['default.handlinginformation'];
                if (defHandlingValue != undefined && defHandlingValue != null) {
                    $scope.consol.consolDocument.accountingInformation = defHandlingValue;
                }
            }

        }


        $scope.addShipmentFromConsol = function(consol) {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_HAWB_LIST_SHIPMENT_CREATE)) {
                console.log("addshipment from consol starts");
                if (consol != undefined && consol != null) {
                    if ($scope.validateConsol(0)) {
                        var shipment = {};
                        shipment.id = -1;
                        shipment.shipmentServiceList = [];
                        shipment.location = $rootScope.userProfile.selectedUserLocation;
                        //shipment.companyMaster = $rootScope.userProfile.selectedUserLocation.companyMaster;
                        //shipment.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
                        shipment.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
                        shipment.createdBy = $rootScope.userProfile.employee;
                        shipment.shipmentReqDate = $rootScope.sendApiDateAndTime(new Date());
                        var shipmentServiceDetail = {};
                        shipmentServiceDetail.whoRouted = consol.whoRouted;
                        if (consol.whoRouted) {
                            shipment.agent = null;
                        } else {
                            shipment.salesman = null;
                        }
                        shipmentServiceDetail.party = {};
                        shipmentServiceDetail.partyAddres = {};
                        if (consol.consolDocument != null && consol.consolDocument.consignee != null) {
                            shipmentServiceDetail.party = consol.consolDocument.consignee;
                            shipmentServiceDetail.partyAddres = consol.consolDocument.consigneeAddress;
                        }

                        shipmentServiceDetail.serviceMaster = consol.serviceMaster;
                        shipmentServiceDetail.directShipment = consol.directShipment;
                        shipmentServiceDetail.origin = consol.origin;
                        shipmentServiceDetail.pol = consol.pol
                        shipmentServiceDetail.destination = consol.destination;
                        shipmentServiceDetail.pod = consol.pod;
                        shipmentServiceDetail.carrier = consol.carrier;
                        shipmentServiceDetail.mawbNo = consol.consolDocument.mawbNo;
                        shipmentServiceDetail.eta = consol.eta;
                        shipmentServiceDetail.etd = consol.etd;
                        shipmentServiceDetail.routeNo = consol.consolDocument.routeNo;
                        shipmentServiceDetail.serviceReqDate = $rootScope.dateToString(new Date());
                        shipmentServiceDetail.tosMaster = {};
                        shipmentServiceDetail.commodity = {};




                        shipmentServiceDetail.pickUpDeliveryPoint = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.transporter = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.pickupPoint = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.pickupFrom = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.deliveryPoint = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.deliveryFrom = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryPoint = {};
                        shipmentServiceDetail.pickUpDeliveryPoint.doorDeliveryFrom = {};
                        //statusObj.completed = false;
                        //$scope.statusObj.followUpRequired = false;
                        // $scope.statusObj.followUpRequired = true;
                        //$scope.statusObj.protect = false;
                        shipmentServiceDetail.customerService = $rootScope.userProfile.employee;
                        shipmentServiceDetail.customerServicePersonEmail = $rootScope.userProfile.employee.email;
                        if (shipment.whoRouted == false) {
                            shipmentServiceDetail.salesman = {};
                            shipmentServiceDetail.salesman = consol.salesman;
                        } else if (consol.whoRouted == true) {
                            shipmentServiceDetail.agent = {};
                            shipmentServiceDetail.agent = consol.agent;
                        }
                        if (consol.consolDocument.dimensionUnit == false) {
                            shipmentServiceDetail.dimensionUnit = false;
                            shipmentServiceDetail.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
                        } else {
                            shipmentServiceDetail.dimensionUnit = true;
                            shipmentServiceDetail.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
                        }
                        shipmentServiceDetail.ppcc = false;
                        shipmentServiceDetail.holdShipment = false;
                        shipmentServiceDetail.hazardous = false;
                        shipmentServiceDetail.overDimension = false;
                        shipmentServiceDetail.isMinimumShipment = false;
                        shipmentServiceDetail.isAgreed = false;
                        shipmentServiceDetail.isMarksNo = true;
                        shipmentServiceDetail.location = $rootScope.userProfile.selectedUserLocation;
                        shipmentServiceDetail.company = $rootScope.userProfile.selectedUserLocation.companyMaster;

                        shipmentServiceDetail.shipmentChargeList = [{
                            "actualChargeable": "CHARGEABLE"
                        }];
                        shipmentServiceDetail.connectionList = [{
                            connectionStatus: 'Planned'
                        }];
                        shipmentServiceDetail.authenticatedDocList = [{}];

                        shipmentServiceDetail.documentList = [];

                        var document = {};
                        document.dimensionList = [{}];
                        document.shipper = consol.consolDocument.shipper;
                        document.shipperAddress = {};
                        if (consol.consolDocument.shipperAddress != undefined &&
                            consol.consolDocument.shipperAddress != null) {
                            document.shipperAddress = consol.consolDocument.shipperAddress;
                            //document.shipperAddress.id = null;
                            document.shipperAddress.versionLock = 0;
                        }

                        document.forwarder = consol.consolDocument.forwarder;
                        document.forwarderAddress = {};
                        if (consol.consolDocument.forwarderAddress != undefined &&
                            consol.consolDocument.forwarderAddress != null) {
                            document.forwarderAddress = consol.consolDocument.forwarderAddress;
                            document.forwarderAddress.id = null;
                            document.forwarderAddress.versionLock = 0;
                        }

                        document.consignee = consol.consolDocument.consignee;
                        document.consigneeAddress = {};
                        if (consol.consolDocument.consigneeAddress != undefined &&
                            consol.consolDocument.consigneeAddress != null) {
                            document.consigneeAddress = consol.consolDocument.consigneeAddress;
                            document.consigneeAddress.id = null;
                            document.consigneeAddress.versionLock = 0;
                        }

                        document.firstNotify = consol.consolDocument.firstNotify;
                        document.firstNotifyAddress = {};
                        if (consol.consolDocument.firstNotifyAddress != undefined &&
                            consol.consolDocument.firstNotifyAddress != null) {
                            document.firstNotifyAddress = consol.consolDocument.firstNotifyAddress;
                            document.firstNotifyAddress.id = null;
                            document.firstNotifyAddress.versionLock = 0;
                        }

                        document.secondNotify = consol.consolDocument.secondNotify;
                        document.secondNotifyAddress = {};
                        if (consol.consolDocument.secondNotifyAddress != undefined &&
                            consol.consolDocument.secondNotifyAddress != null) {
                            document.secondNotifyAddress = consol.consolDocument.secondNotifyAddress;
                            document.secondNotifyAddress.id = null;
                            document.secondNotifyAddress.versionLock = 0;
                        }

                        document.agent = consol.consolDocument.agent;
                        document.agentAddress = {};
                        if (consol.consolDocument.agentAddress != undefined &&
                            consol.consolDocument.agentAddress != null) {
                            document.agentAddress = consol.consolDocument.agentAddress;
                            document.agentAddress.id = null;
                            document.agentAddress.versionLock = 0;
                        }

                        document.issuingAgent = consol.consolDocument.issuingAgent;
                        document.issuingAgentAddress = {};
                        if (consol.consolDocument.issuingAgentAddress != undefined &&
                            consol.consolDocument.issuingAgentAddress != null) {
                            document.issuingAgentAddress = consol.consolDocument.issuingAgentAddress;
                            document.issuingAgentAddress.id = null;
                            document.issuingAgentAddress.versionLock = 0;
                        }

                        document.documentReqDate = $rootScope.sendApiDateAndTime(new Date());
                        document.dimensionUnit = shipmentServiceDetail.dimensionUnit;
                        document.dimensionUnitValue = shipmentServiceDetail.dimensionUnitValue;


                        if (consol.shipmentLinkList != undefined &&
                            consol.shipmentLinkList != null &&
                            consol.shipmentLinkList.length == 0) {

                            document.packMaster = consol.consolDocument.packMaster;
                            document.noOfPieces = consol.consolDocument.noOfPieces;
                            document.grossWeight = consol.consolDocument.grossWeight;
                            document.volumeWeight = consol.consolDocument.volumeWeight;
                            document.chargebleWeight = consol.consolDocument.chargebleWeight;
                            document.grossWeightInPound = consol.consolDocument.grossWeightInPound;
                            document.volumeWeightInPound = consol.consolDocument.volumeWeightInPound;

                            if (consol.consolDocument.dimensionList != undefined &&
                                consol.consolDocument.dimensionList != null &&
                                consol.consolDocument.dimensionList.length != 0) {
                                document.dimensionList = [];
                                for (var i = 0; i < consol.consolDocument.dimensionList.length; i++) {
                                    var obj = {};
                                    obj.noOfPiece = consol.consolDocument.dimensionList[i].noOfPiece;
                                    obj.length = consol.consolDocument.dimensionList[i].length;
                                    obj.width = consol.consolDocument.dimensionList[i].width;
                                    obj.height = consol.consolDocument.dimensionList[i].height;
                                    obj.volWeight = consol.consolDocument.dimensionList[i].volWeight;
                                    obj.grossWeight = consol.consolDocument.dimensionList[i].grossWeight;
                                    obj.grossWeightKg = consol.consolDocument.dimensionList[i].grossWeightKg;
                                    document.dimensionList.push(obj);
                                }

                            }
                        }

                        var goToShipmentFlag = true;

                        if (document.packMaster == undefined || document.packMaster == null ||
                            document.packMaster.id == undefined || document.packMaster.id == null) {

                            var defaultPackageCode = $rootScope.appMasterData['shipment.service.default.package.code'];

                            if (defaultPackageCode != undefined && defaultPackageCode != null) {

                                goToShipmentFlag = false;

                                PackGetByCode.get({
                                    code: defaultPackageCode
                                }, function(data) {
                                    if (data.responseCode == "ERR0") {
                                        document.packMaster = data.responseObject;
                                    }

                                    shipmentServiceDetail.documentList.push(document);
                                    shipment.shipmentServiceList.push(shipmentServiceDetail);
                                    var params = {};
                                    params.action = 'CREATE';
                                    params.forPurpose = "createShipmentFromConsol"
                                    params.fromScreen = $state.current.name;
                                    $rootScope.shipmentFromConsol = consol;
                                    shipment.isFromConsolForImport = true;
                                    $rootScope.createShipment = shipment;
                                    $state.go("layout.addNewShipment", params);

                                }, function(error) {
                                    console.log("Error while getting pack.", error)

                                    shipmentServiceDetail.documentList.push(document);
                                    shipment.shipmentServiceList.push(shipmentServiceDetail);
                                    var params = {};
                                    params.action = 'CREATE';
                                    params.forPurpose = "createShipmentFromConsol"
                                    params.fromScreen = $state.current.name;
                                    $rootScope.shipmentFromConsol = consol;
                                    shipment.isFromConsolForImport = true;
                                    $rootScope.createShipment = shipment;
                                    $state.go("layout.addNewShipment", params);

                                });

                            }
                        }


                        if (goToShipmentFlag) {

                            shipmentServiceDetail.documentList.push(document);
                            shipment.shipmentServiceList.push(shipmentServiceDetail);

                            var params = {};
                            params.action = 'CREATE';
                            params.forPurpose = "createShipmentFromConsol"
                            params.fromScreen = $state.current.name;
                            shipment.isFromConsolForImport = true;
                            $rootScope.createShipment = shipment;
                            $rootScope.shipmentFromConsol = consol;
                            $state.go("layout.addNewShipment", params);

                        }

                    } else {
                        console.log("Invalid Form Submission.");
                    }
                }

            }
        }


        // for AES


        $scope.aesSearchFunction = function(consol) {
            console.log("Search method is called. aes...........................!");
            $scope.finalArr = [];
            $scope.searchDto = {};
            $scope.page = 0;
            $scope.limit = 10;
            $scope.aesFile = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.searchDto.masterUid = consol.consolUid;
            $scope.aesFileArr = [];
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            AesSearch.query($scope.searchDto).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempAesArr = [];
                $scope.aesFileList = [];
                var tempAesArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempAesArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    if (tempObj.id != undefined && tempObj.id != null) {
                        tempObj.status = 'Generated';
                        tempObj.shipperName = tempObj.shipperName;
                        tempObj.consigneeName = tempObj.consigneeName;
                    }
                    $scope.aesFileList.push(tempObj);
                    tempObj = {};
                    console.log("count...in for each")
                });
                $scope.finalArr = $scope.checkisAnyNewLink(consol.shipmentLinkList, $scope.aesFileList);
                return $scope.finalArr;
            });
        }

        $scope.checkisAnyNewLink = function(shipmentLinkList, aesFileList) {
            var flag;
            if (aesFileList != null && aesFileList.length > 0) {

                $scope.finalArr = $scope.finalArr.concat(aesFileList);

                for (var i = 0; i < shipmentLinkList.length; i++) {

                    for (var j = 0; j < aesFileList.length; j++) {

                        if (shipmentLinkList[i].serviceUid != aesFileList[j].serviceUid) {
                            flag = true;
                        } else {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        if (shipmentLinkList[i].consolUid != null) {
                            var tempObj = {};
                            tempObj.serviceUid = $scope.consol.shipmentLinkList[i].serviceUid;
                            tempObj.status = 'Active';
                            if ($scope.consol.shipmentLinkList[i].service != undefined && $scope.consol.shipmentLinkList[i].service != null && $scope.consol.shipmentLinkList[i].service.documentList[0].length != 0) {
                                tempObj.hawbNo = $scope.consol.shipmentLinkList[i].service.documentList[0].hawbNo;
                                tempObj.shipperName = $scope.consol.shipmentLinkList[i].service.documentList[0].shipper.partyName;
                                tempObj.consigneeName = $scope.consol.shipmentLinkList[i].service.documentList[0].consignee.partyName;
                            }
                            tempObj.mawbNo = $scope.consol.shipmentLinkList[i].service.mawbNo;
                            tempObj.masterUid = $scope.consol.consolUid;
                            tempObj.shipmentUid = $scope.consol.shipmentLinkList[i].service.shipmentUid;
                            $scope.finalArr.push(tempObj);
                        }
                    }
                }
                return $scope.finalArr;

            } else {

                for (var i = 0; i < shipmentLinkList.length; i++) {
                    var tempObj = {};
                    tempObj.serviceUid = $scope.consol.shipmentLinkList[i].serviceUid;
                    tempObj.status = 'Active';
                    if ($scope.consol.shipmentLinkList[i].service != undefined && $scope.consol.shipmentLinkList[i].service != null && $scope.consol.shipmentLinkList[i].service.documentList[0].length != 0) {
                        tempObj.hawbNo = $scope.consol.shipmentLinkList[i].service.documentList[0].hawbNo;
                        tempObj.shipperName = $scope.consol.shipmentLinkList[i].service.documentList[0].shipper.partyName;
                        tempObj.consigneeName = $scope.consol.shipmentLinkList[i].service.documentList[0].consignee.partyName;
                    }
                    tempObj.masterUid = $scope.consol.consolUid;
                    tempObj.mawbNo = $scope.consol.shipmentLinkList[i].service.mawbNo;
                    tempObj.shipmentUid = $scope.consol.shipmentLinkList[i].service.shipmentUid;
                    $scope.finalArr.push(tempObj);
                }
                return $scope.finalArr;
            }
        }

        $scope.aesGenerateView = function(aesFile) {
            var fromScreen = {};
            fromScreen = $state.current.name;
            if (aesFile != undefined && aesFile != null) {
                ConsolGetByUid.get({
                    consolUid: $scope.consol.consolUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (data.responseObject != null && data.responseObject.jobStatus != 'Cancelled') {
                            $state.go('layout.aesView', {
                                aesId: aesFile.id,
                                fromScreen: fromScreen,
                                consolId: $scope.consol.id
                            });
                        } else {
                            $rootScope.clientMessage = $rootScope.nls["ERR2012008"];
                            return false;
                        }
                    }

                }, function(error) {
                    console.log("Consol get Failed : " + error)
                });
            }
        }


        $scope.aesGenerateAdd = function(aesFile, index) {
            $scope.spinner = true;
            console.log("aesGenerateAdd method called");
            var fromScreen = {};
            fromScreen = $state.current.name;
            $rootScope.clientMessage = null;
            if (aesFile != undefined && aesFile != null) {
                ConsolGetByUid.get({
                    consolUid: $scope.consol.consolUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        if (data.responseObject != null && data.responseObject.jobStatus != 'Cancelled') {
                            var navAesObj = {
                                selectedPageNumber: $scope.detailTab == 'active' ? $scope.activeSearchDto.selectedPageNumber : $scope.searchDto.selectedPageNumber,
                                status: $scope.detailTab,
                                totalRecord: $scope.detailTab == 'active' ? $scope.totalRecordView : $scope.totalRecord,
                                consolId: $scope.consol.id,
                                serviceUid: aesFile.serviceUid,
                                shipmentUid: aesFile.shipmentUid,
                                consolUid: $scope.consol.consolUid,
                                etd: $scope.consol.etd,
                                carrier: $scope.consol.carrier,
                                shipper: $scope.consol.consolDocument.shipper,
                                consignee: $scope.consol.consolDocument.consignee,
                                fromState: $state.current.name,
                                fromScreen: fromScreen,
                                mawbNo: aesFile.mawbNo,
                                hawbNo: aesFile.hawbNo
                            };
                            if ($scope.consol.serviceMaster.transportMode == 'Air') {
                                navAesObj.iataOrScacCode = $scope.consol.carrier.iataCode;
                            } else if ($scope.consol.serviceMaster.transportMode == 'Ocean') {
                                navAesObj.iataOrScacCode = $scope.consol.carrier.scacCode;
                            }
                            $state.go("layout.addAes", {
                                navAesObj: navAesObj
                            });
                        } else {
                            $rootScope.clientMessage = $rootScope.nls["ERR2012008"];
                            $scope.spinner = false;
                            return false;
                        }
                    } else {
                        $scope.spinner = false;
                        return false;
                    }
                }, function(error) {
                    $scope.spinner = false;
                    console.log("Consol get Failed : " + error)
                });
            }

        };



        // Go to shipment link

        $scope.goToShipmentViewByServiceUid = function(serviceuid) {
            console.log("State  : ", $state)
            console.log("State Param  : ", $stateParams)
            console.log("serviceuid ", serviceuid)
            ShipmentIdByServiceUid.get({
                    serviceuid: serviceuid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var param = {
                            shipmentId: data.responseObject,
                            fromState: $state.current.name,
                            fromStateParams: JSON.stringify($stateParams)

                        }
                        $state.go("layout.viewCrmShipment", param);
                    }
                },
                function(error) {
                    console.log("Shipment get Failed : " + error)
                });
        }

        $scope.setLinkToConsol = function(shipmentUid) {
            if ($scope.consol != undefined && $scope.consol != null && shipmentUid != undefined) {
                ShipmentGetFromUID.get({
                        shipmentuid: shipmentUid
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.shipmentData = data.responseObject;
                            if ($scope.shipmentData != undefined && $scope.shipmentData != null) {
                                if ($scope.shipmentData.shipmentServiceList != undefined && $scope.shipmentData.shipmentServiceList != null && $scope.shipmentData.shipmentServiceList.length > 0) {
                                    if ($scope.consol.shipmentLinkList == undefined || $scope.consol.shipmentLinkList == null) {
                                        $scope.consol.shipmentLinkList = [];
                                    }
                                    $scope.consolDto = {};
                                    $scope.consolDto.consolUid = $scope.consol.consolUid == null || $scope.consol.consolUid == undefined ? "-1" : $scope.consol.consolUid,
                                        $scope.consolDto.serviceId = $scope.consol.serviceMaster.id;
                                    $scope.consolDto.locationId = $scope.consol.location.id;
                                    $scope.consolDto.polId = $scope.consol.pol.id;
                                    $scope.consolDto.podId = $scope.consol.pod.id;
                                    $scope.consolDto.etd = $rootScope.sendApiStartDateTime($scope.consol.etd)
                                    $scope.consolDto.eta = $rootScope.sendApiStartDateTime($scope.consol.eta);
                                    $scope.consolDto.carrierId = $scope.consol.carrier.id;
                                    $scope.consolDto.mawbNo = $scope.consol.consolDocument.mawbNo;
                                    ConsolShipmentLink.link($scope.consolDto).$promise.then(
                                        function(data) {
                                            if (data.responseCode == 'ERR0') {
                                                var serviceData;
                                                var tmpList = data.responseObject.searchResult;
                                                if (tmpList != undefined && tmpList != null && tmpList.length > 0) {
                                                    for (var k = 0; k < tmpList.length; k++) {
                                                        if (tmpList[k].shipmentUid == shipmentUid) {
                                                            //$scope.shipmentServiceData.push(tmpList[k]);
                                                            $scope.tmpShipmentLinkImp = {};
                                                            $scope.tmpShipmentLinkImp.service = tmpList[k];
                                                            $scope.consol.shipmentLinkList.push($scope.tmpShipmentLinkImp)
                                                        }
                                                        //$scope.shipmentServiceData.splice(k,1);
                                                    }
                                                    $scope.checkDuplicate($scope.consol.shipmentLinkList);
                                                    $scope.serviceDimensionAdded();
                                                } else {}
                                            }
                                        },
                                        function(error) {
                                            console.log("Consol - Shipment - Service- Link get Failed : " + error)
                                        });
                                }
                            }
                        }
                    },
                    function(error) {
                        console.log("Shipment get Failed : " + error)
                    });

            }

        }

        $scope.checkDuplicate = function(linkList) {
            $scope.errorMap = new Map();
            if (linkList.length > 1) {
                for (var i = 0; i < linkList.length; i++) {
                    for (var j = 0; j < linkList.length; j++) {
                        if (i == j)
                            continue
                        if (linkList[i].serviceUid == linkList[j].serviceUid) {
                            $scope.consol.shipmentLinkList.splice(index, 1);
                        }
                    }
                }
            }
        }




        $scope.goToShipmentService = function(shipmentUid) {

            var param = {
                shipmentUid: shipmentUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.viewCrmShipment', param);
        }
        $scope.goToConsol = function(consolUid) {
            var param = {
                consolUid: consolUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }

            $state.go('layout.viewNewAirConsol', param);
        }




        // For Aes


        $scope.nextButtonLabel = "Next";
        $scope.togglePreviousButton = false;

        $scope.gotoPreviousTab = function() {
            if ($scope.Tabs == 'consol') {
                $scope.setTab('general');
            } else if ($scope.Tabs == 'documents') {
                $scope.setTab('consol');
            }
        }


        $scope.gotoNextTab = function() {

            if ($scope.Tabs == 'general') {
                $scope.setTab('consol');
            } else if ($scope.Tabs == 'consol') {
                $scope.setTab('documents');
            } else if ($scope.Tabs == 'documents') {
                $scope.setTab('finish');
            }

        }


        $scope.chkAllService = function() {
            for (var i = 0; i < $scope.shipmentServiceData.length; i++) {
                $scope.shipmentServiceData[i].isSelected = $scope.consol.allServiceSelected;
            }
        }


        $scope.setTab = function(tabName) {
            if (tabName == 'general') {
                $scope.Tabs = 'general';
                $scope.nextButtonLabel = "Next";
                $scope.togglePreviousButton = false;

            } else if (tabName == 'consol') {
                if ($scope.consol.pod.portName != $scope.consol.destination.portName) {
                    if (!$scope.consol.connectionList.length) {
                        Notification.error("Please add a connection , as the Airport Of Discharge is differ from Destination");
                        $scope.togglePreviousButton = false;
                        return false;
                    } else if (!$scope.consol.connectionList[0].hasOwnProperty('carrierMaster')) {
                        Notification.error("Please add a connection , as the Airport Of Discharge is differ from Destination");
                        $scope.togglePreviousButton = false;
                        return false;
                    }

                }
                if ($scope.validateConsol(1) && $scope.validateConsol(3) && $scope.validateConsol(8) && $scope.validateConsol(9) && $scope.validateConsol(10) && $scope.validateConsol(14) && $scope.validateConsol(15) && $scope.validateConsol(16)) {
                    $scope.linkPopUp(true);
                    $scope.Tabs = 'consol';
                    $scope.nextButtonLabel = "Next";
                    $scope.togglePreviousButton = true;
                } else {
                    $scope.Tabs = 'general';
                    $scope.nextButtonLabel = "Next";
                    $scope.togglePreviousButton = false;
                }

            } else if (tabName == 'documents') {

                if ($scope.validateConsol(1) && $scope.validateConsol(3) && $scope.validateConsol(8) && $scope.validateConsol(9) && $scope.validateConsol(10) && $scope.validateConsol(14) && $scope.validateConsol(15) && $scope.validateConsol(16)) {
                    $scope.Tabs = 'documents';
                    $scope.nextButtonLabel = "Finish";
                    $scope.togglePreviousButton = true;

                    try {
                        if ($rootScope.userProfile.selectedUserLocation.tmpPartyMaster != undefined &&
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster != null &&
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != undefined &&
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster.id != null && ($scope.consol.consolDocument.issuingAgent == undefined || $scope.consol.consolDocument.issuingAgent.id == undefined)) {
                            $scope.consol.consolDocument.issuingAgent = {};
                            $scope.consol.consolDocument.issuingAgentAddress = {};
                            $scope.consol.consolDocument.issuingAgent = $rootScope.userProfile.selectedUserLocation.tmpPartyMaster;
                            $scope.getPartyAddress($scope.consol.consolDocument.issuingAgent, $scope.consol.consolDocument.issuingAgentAddress);
                        }
                        if ($scope.consol.consolDocument.agent == undefined || $scope.consol.consolDocument.agent.id == undefined) {
                            $scope.consol.consolDocument.agent = {};
                            $scope.consol.consolDocument.agentAddress = {};
                            $scope.consol.consolDocument.agent = $scope.consol.agent;
                            $scope.consol.consolDocument.agentAddress = $scope.consol.agentAddress;
                        }

                        if (($scope.consol.consolDocument.shipper == undefined || $scope.consol.consolDocument.shipper.id == undefined) && ($scope.consol.consolDocument.issuingAgent != undefined && $scope.consol.consolDocument.issuingAgent.id != undefined)) {
                            $scope.consol.consolDocument.shipper = {};
                            $scope.consol.consolDocument.shipperAddress = {};
                            $scope.consol.consolDocument.shipper = $scope.consol.consolDocument.issuingAgent;
                            $scope.consol.consolDocument.shipperAddress = $scope.consol.consolDocument.issuingAgentAddress;
                        }
                        if (($scope.consol.consolDocument.consignee == undefined || $scope.consol.consolDocument.consignee.id == undefined) && ($scope.consol.consolDocument.agent != undefined && $scope.consol.consolDocument.agent.id != undefined)) {
                            $scope.consol.consolDocument.consignee = {};
                            $scope.consol.consolDocument.consigneeAddress = {};
                            $scope.consol.consolDocument.consignee = $scope.consol.consolDocument.agent;
                            $scope.consol.consolDocument.consigneeAddress = $scope.consol.consolDocument.agentAddress;
                        }
                    } catch (e) {
                        console.log(e);
                    }

                    $scope.consol.shipmentLinkList = [];

                    for (var i = 0; i < $scope.shipmentServiceData.length; i++) {
                        if ($scope.shipmentServiceData[i].isSelected == false) {
                            continue;
                        }
                        $scope.tmpShipmentLink = {};
                        $scope.tmpShipmentLink.service = $scope.shipmentServiceData[i];
                        $scope.consol.shipmentLinkList.push($scope.tmpShipmentLink);
                    }
                    if ($scope.consol.shipmentLinkList != undefined && $scope.consol.shipmentLinkList != null &&
                        $scope.consol.shipmentLinkList.length != 0) {
                        $scope.serviceDimensionAdded();
                    }
                } else {
                    $scope.Tabs = 'general';
                    $scope.nextButtonLabel = "Next";
                    $scope.togglePreviousButton = false;
                }
            } else if (tabName == 'finish') {
                $scope.saveConsol();
            }
        }

        $scope.nextPrevious = function(nexPre) {
            $scope.consol = {};
            $scope.pageActive = 0;
            $scope.page = 0;
            if (nexPre == 'general') {
                $scope.hawb = tabName;

            } else {
                $scope.document = tabName;

            }
            $scope.details = tabName;
        }



        $scope.setServiceCodeWithShipmentId = function() {
            if ($scope.consol.shipmentLinkList != null && $scope.consol.shipmentLinkList.length > 0) {
                for (var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
                    $scope.consol.shipmentLinkList[i].serviceCodeWithShipmentId = $scope.consol.shipmentLinkList[i].service.serviceMaster.serviceCode + '-' + $scope.consol.shipmentLinkList[i].shipmentUid;
                }
            }
        }




        $scope.getConsolData = function(consolId) {

            ConsolGet.get({
                    id: consolId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.consol = data.responseObject;
                        $scope.setServiceCodeWithShipmentId();
                        if (data.responseObject != undefined && data.responseObject != null) {

                            if (data.responseObject != null && data.responseObject.id != null && data.responseObject.id != -1) {
                                $rootScope.navigateToNextField('serviceMaster');
                                data.responseObject.title = 'Edit Consol ID ' + data.responseObject.consolUid;
                            }


                            if (data.responseObject.agent != undefined && data.responseObject.agent != null &&
                                data.responseObject.agent.partyName != undefined && data.responseObject.agent.partyName != null) {
                                $rootScope.subTitle = data.responseObject.agent.partyName;
                            } else {
                                $rootScope.subTitle = "Unkonwn Agent";
                            }
                            if (data.responseObject.origin != undefined && data.responseObject.origin != null) {
                                $rootScope.serviceCodeForUnHistory = data.responseObject.origin.portGroupCode;
                            } else {
                                $rootScope.serviceCodeForUnHistory = "";
                            }

                            if (data.responseObject.destination != undefined && data.responseObject.destination != null) {
                                $rootScope.orginAndDestinationUnHistory = data.responseObject.destination.portGroupCode;
                            } else {
                                $rootScope.orginAndDestinationUnHistory = "";
                            }


                        } else {
                            $rootScope.subTitle = "Unkonwn Agent";
                            $rootScope.serviceCodeForUnHistory = "";
                            $rootScope.orginAndDestinationUnHistory = "";
                        }

                        $rootScope.unfinishedFormTitle = "Master Shipment Edit # " + data.responseObject.consolUid;

                        var rHistoryObj = {
                            'title': 'Master Shipment Edit # ' + data.responseObject.consolUid,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Master Shipment',
                            'serviceType': 'AIR',
                            'serviceCode': $rootScope.serviceCodeForUnHistory,
                            'orginAndDestination': "(" + $rootScope.orginAndDestinationUnHistory + ")",
                            'showService': true
                        }

                        RecentHistorySaveService.form(rHistoryObj);
                        $scope.editData(data.responseObject);
                        $scope.afterSet();
                    } else {
                        $scope.consol = null;
                    }
                },
                function(error) {
                    console.log("Consol get Failed : " + error)
                    $scope.consol = null;
                });
        }


        $scope.downloadFileType = "PDF";
        $scope.generate = {};
        $scope.generate.typeArr = ["Download", "Preview", "Print", "eMail"];

        $scope.reportModal = function(consol, resourceRefId, reportRefName, key) {
            if ($scope.reportList != undefined && $scope.reportList != null) {
                $scope.allRepoVarModel.allReportSelected = false;
                $scope.isGenerateAll = false;
                for (var i = 0; i < $scope.reportList.length; i++) {
                    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
                }
            }

            if (key == 'mawb_label') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_MAWB_LABEL_DOWNLOAD)) {
                    return;
                }
            }

            if (key == 'security_endorsement_letter') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_SECURITY_ENDORSEMENT_LETTER_DOWNLOAD)) {
                    return;
                }
            }

            if (key == 'export_job_card') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_EXPORT_JOB_CARD_DOWNLOAD)) {
                    return;
                }
            }

            if (key == 'cargo_manifest' && reportRefName == "CARGO_MANIFEST") {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_CARGO_MANIFEST_DOWNLOAD)) {
                    return;
                }
                $scope.genReport(resourceRefId, reportRefName);
            }

            if (key == 'security_declaration') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_SECURITY_DECLARATION_DOWNLOAD)) {
                    return;
                }
            }

            if (key == 'air_cargo_load_plan') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_AIR_CARGO_LOAD_PLAN_DOWNLOAD)) {
                    return;
                }
            }

            if (key == 'job_card') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_JOB_CARD_DOWNLOAD)) {
                    return;
                }
                $scope.generateJobCard(resourceRefId, reportRefName);
                return;
            }

            if (key == 'pickup_delivery_order') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_PICKUP_DELIVERY_ORDER_DOWNLOAD)) {
                    return;
                }
            }
            if (key == 'cargo_arrival_notice_consol') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_CARGO_ARRIVAL_NOTICE_CONSOL_DOWNLOAD)) {
                    return;
                }
            }
            if (key == 'jobcost_sheet') {
                if (!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_JOBCOST_SHEET_DOWNLOAD)) {
                    return;
                }
            }

            if (reportRefName == 'ALERT_TO_AGENT') {
                $rootScope.mainpreloder = false;
                $scope.alertToAgentPopUp();
            }
            $scope.doModal = false;
            if (reportRefName == 'CARGO_MANIFEST') {
                $scope.changeReportDownloadFormat("PDF");
                $scope.dataResourceId = resourceRefId;
                $scope.dataReportName = reportRefName;
                var reportOtherModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/report/report_opt.html',
                    show: false
                });
                $rootScope.mainpreloder = false;
                reportOtherModal.$promise.then(reportOtherModal.hide);

            } else {
                $scope.changeReportDownloadFormat("PDF");
                $scope.dataResourceId = resourceRefId;
                $scope.dataReportName = reportRefName;
                $scope.myReportOtherModal = {};
                $scope.myReportOtherModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/report/report_opt.html',
                    show: false
                });
                $rootScope.mainpreloder = false;

                $scope.myReportOtherModal.$promise.then($scope.myReportOtherModal.show);
            }
        }



        //used to pop the modal for alert 
        //used to pop the modal for alert 
        $scope.alertToAgentPopUp = function() {
            $rootScope.mainpreloder = true;
            $http({
                method: 'GET',
                url: $rootScope.userProfile.reportServerBasePath + '/api/v1/preAlert/reportlength',
                params: {
                    'consolUid': $scope.consol.consolUid
                }
            }).success(function(data) {
                if (data != null) {
                    $scope.reportEmailRequestDto = {};
                    $scope.reportEmailRequestDto = data;
                    reportModal.$promise.then(reportModal.hide);
                    var emailSep;
                    if ($rootScope.appMasterData['single.text.split.by.multiple.email'] != null) {
                        emailSep = $rootScope.appMasterData['single.text.split.by.multiple.email'];
                    } else {
                        emailSep = ';';
                    }
                    $rootScope.mainpreloder = false;
                    $scope.alertAgentPopUp = $modal({
                        scope: $scope,
                        backdrop: 'static',
                        templateUrl: '/app/components/crm/new-shipment/dialog/shipment_report_alerttoagent_popup.html',
                        show: false
                    });
                    Notification.info("Please use" + emailSep + "for multiple emails");
                    $scope.alertAgentPopUp.$promise.then($scope.alertAgentPopUp.show);
                } else {
                    $rootScope.mainpreloder = false;
                    Notification.error("No Reports For found");
                }
                // With the data succesfully returned, call our callback
            }).error(function() {
                $rootScope.mainpreloder = false;
            });
        }

        $scope.emailValidate = function(code) {
            if (code == 0) {
                if ($scope.reportEmailRequestDto != null && $scope.reportEmailRequestDto != undefined) {
                    if ($scope.reportEmailRequestDto.toEmailIdList == undefined || $scope.reportEmailRequestDto.toEmailIdList == null || $scope.reportEmailRequestDto.toEmailIdList == "") {
                        Notification.error($rootScope.nls["ERR269"]);
                        return false;
                    } else if (!CommonValidationService.checkMultipleMail($scope.reportEmailRequestDto.toEmailIdList)) {
                        Notification.error($rootScope.nls["ERR270"]);
                        return false;
                    }
                }
                if ($scope.reportEmailRequestDto.ccEmailIdList == undefined || $scope.reportEmailRequestDto.ccEmailIdList == null || $scope.reportEmailRequestDto.ccEmailIdList == "") {

                } else {
                    if (!CommonValidationService.checkMultipleMail($scope.reportEmailRequestDto.ccEmailIdList)) {
                        Notification.error($rootScope.nls["ERR271"]);
                        return false;
                    }
                }

            }
            return true;
        }

        $scope.sendEmailToAgent = function() {
            if ($scope.emailValidate(0)) {
                $rootScope.mainpreloder = true;
                $scope.reportEmailRequestDto.consolUid = $scope.consol.consolUid;
                $http({
                    method: 'POST',
                    url: $rootScope.userProfile.reportServerBasePath + '/api/v1/preAlert/mailtoagent',
                    data: $scope.reportEmailRequestDto
                }).then(
                    function(response) {
                        if (response.data.responseCode === 'ERR0') {
                            Notification.success($rootScope.nls["ERR272"])
                        } else {
                            Notification.error($rootScope.nls["ERR273"])
                        }
                    },
                    function(error) {
                        Notification.error($rootScope.nls["ERR273"])
                    });
                Notification.success($rootScope.nls["ERR274"]);
                $scope.alertAgentPopUp.$promise.then($scope.alertAgentPopUp.hide);
                $rootScope.mainpreloder = false;
            }
        }

        $scope.genReport = function(resourceRefId, reportRefName) {
            //$scope.popupValues={};
            $scope.popupValues.showVolume = 'No';
            $scope.popupValues.showChargeWeight = 'No';
            $scope.popupValues.showShipperName = 'Yes';
            $scope.popupValues.showShipperAddress = 'No';
            $scope.popupValues.showNote = 'No';
            $scope.popupValues.showCharge = 'No';
            $scope.popupValues.showLogo = 'No';

            console.log("Service ID - ", resourceRefId, " -- Name --", reportRefName);
            $scope.changeReportDownloadFormat("PDF");
            $scope.dataResourceId = resourceRefId;
            $scope.dataReportName = reportRefName;
            $scope.myOtherModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: '/app/components/air/consol_shipments/views/cargo_manifest_popup.html',
                show: false
            });
            $scope.myOtherModal.$promise.then($scope.myOtherModal.show);
        }

        $scope.generateJobCard = function(resourceRefId, reportRefName) {
            $scope.popupValues.showCharge = 'No';

            console.log("Service ID - ", resourceRefId, " -- Name --", reportRefName);
            $scope.changeReportDownloadFormat("PDF");
            $scope.dataResourceId = resourceRefId;
            $scope.dataReportName = reportRefName;
            $scope.myOtherModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: '/app/components/air/consol_shipments_new_screen/views/jobCard_report_option_popup.html',
                show: false
            });
            $scope.myOtherModal.$promise.then($scope.myOtherModal.show);
        }
        $scope.showReportParameter = function(consolId, popupValues, downloadOption) {
            $scope.downloadOption = downloadOption;
            var reportDownloadRequest = {};
            reportDownloadRequest = {
                resourceId: consolId,
                downloadOption: $scope.downloadOption, // "Download","Preview","Print"
                downloadFileType: $scope.downloadFileType, //PDF
                reportName: "CARGO_MANIFEST",
                showVolume: popupValues.showVolume,
                showCfs: popupValues.showCfs,
                showChargeWeight: popupValues.showChargeWeight,
                showShipperName: popupValues.showShipperName,
                showShipperAddress: popupValues.showShipperAddress,
                showNote: popupValues.showNote,
                showCharge: popupValues.showCharge,
                showLogo: popupValues.showLogo,
                single: true

            };
            $rootScope.mainpreloder = true;
            downloadFactory.download(reportDownloadRequest);
        }


        $scope.updateParameterStatus = function(consolId) {
            var reportDownloadRequest = {};
            reportDownloadRequest = {
                resourceId: consolId,
                downloadOption: $scope.downloadOption, // "Download","Preview","Print"
                downloadFileType: $scope.downloadFileType, //PDF
                reportName: "JOB_CARD",
                single: true

            };
            $scope.myReportModal = {};
            $scope.myReportModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/report/report_opt.html',
                show: false
            });
            $rootScope.mainpreloder = false;
            $scope.myReportModal.$promise.then($scope.myReportModal.show);
        }


        $scope.changeReportDownloadFormat = function(dType) {
            console.log("Download type Changed :: ", dType);
            $scope.downloadFileType = dType;
            if (dType === 'PDF') {
                $scope.generate.TypeArr = ["Download", "Preview", "Print"];
            } else if (dType === 'eMail') {
                $scope.generate.TypeArr = ["eMail"];
            } else {
                $scope.generate.TypeArr = ["Download"];
            }
        }

        $scope.documentIdList = [];
        $scope.reportList = [];
        $scope.reportDetailData = {};

        var allRepoVar = {};
        $scope.allRepoVarModel = {};
        $scope.allRepoVarModel.allReportSelected = false;
        $scope.reportLoadingImage = {};
        $scope.reportLoadingImage.status = false;
        $scope.reportListMaking = function(consol, resourceRefId) {
            $scope.reportObject = angular.copy(consol);
            if ($rootScope.hasAccess('AIR_MASTER_REPORTS_TAB_VIEW')) {
                $rootScope.mainpreloder = true;
                $scope.reportList = [];
                $scope.moreAddressTab = 'reports';
                $scope.assignDefaultReports(consol, resourceRefId);
                consolShipmentDocumentIdList.get({
                        serviceId: resourceRefId
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.documentIdList = data.responseObject;
                            console.log("$scope.documentIdList -- ", $scope.documentIdList);
                            $rootScope.mainpreloder = false;
                        }
                    },
                    function(error) {
                        console.log("Shipment get Failed : " + error)
                        $rootScope.mainpreloder = false;
                    });

            } else {
                console.log("Rights are not there to perform this action!.");
            }
        }


        $scope.assignDefaultReports = function(consol, resourceRefId) {
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "SHIPMENT_MAWB",
                    reportDisplayName: 'MAWB',
                    key: "mawb"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "MAWB_LABEL",
                    reportDisplayName: 'MAWB Label',
                    key: "mawb_label"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "SECURITY_ENDORSEMENT_LETTER",
                    reportDisplayName: 'Security Endorsement Letter',
                    key: "security_endorsement_letter"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "EXPORT_JOB_CARD",
                    reportDisplayName: 'Export Job Card(MAWB)',
                    key: "export_job_card"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' && (consol.directShipment == 'No' || consol.directShipment == false)) {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "CARGO_MANIFEST",
                    reportDisplayName: 'Cargo Manifest(AIR EXPORT)',
                    key: "cargo_manifest"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "SECURITY_DECLARATION",
                    reportDisplayName: 'Security Declaration',
                    key: "security_declaration"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "AIR_CARGO_LOAD_PLAN",
                    reportDisplayName: 'Air Cargo Load Plan',
                    key: "air_cargo_load_plan"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Import' || consol.serviceMaster.importExport == 'IMPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "MASTER_DO",
                    reportDisplayName: 'Delivery' + (consol.serviceMaster.transportMode == "Air" ? 'Advice' : 'Order'),
                    key: "delivery_order"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Import' || consol.serviceMaster.importExport == 'IMPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "JOB_CARD",
                    reportDisplayName: 'Job card',
                    key: "job_card"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consolShipmentDataService.isPickUpDeliveyModified(consol.pickUpDeliveryPoint, 'pick') ||
                consolShipmentDataService.isPickUpDeliveyModified(consol.pickUpDeliveryPoint, 'delivery')) {
                if (consol.serviceMaster.importExport == 'Import' || consol.serviceMaster.importExport == 'IMPORT') {
                    $scope.reportDetailData = {
                        resourceRefId: resourceRefId,
                        isDocument: false,
                        isService: true,
                        documentNo: '',
                        reportName: "PICKUP_DELIVERY_ORDER",
                        reportDisplayName: 'Pickup Delivery Order',
                        key: "pickup_delivery_order"
                    };
                    $scope.reportList.push($scope.reportDetailData);
                }
            }
            if (consol.serviceMaster.importExport == 'Import' || consol.serviceMaster.importExport == 'IMPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "CARGO_ARRIVAL_NOTICE_CONSOL",
                    reportDisplayName: 'Cargo Arrival Notice',
                    key: "cargo_arrival_notice_consol"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "JOBCOST_SHEET",
                    reportDisplayName: 'Consol Profitability',
                    key: "jobcost_sheet"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
            if (consol.serviceMaster.importExport == 'Export' || consol.serviceMaster.importExport == 'EXPORT') {
                $scope.reportDetailData = {
                    resourceRefId: resourceRefId,
                    isDocument: false,
                    isService: true,
                    documentNo: '',
                    reportName: "ALERT_TO_AGENT",
                    reportDisplayName: 'Prealert To Agent',
                    key: "Alert To Agent"
                };
                $scope.reportList.push($scope.reportDetailData);
            }
        }



        $scope.selectAllReports = function() {
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].reportName == 'ALERT_TO_AGENT') {
                    $scope.reportList[i].isSelected = false;
                } else {
                    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
                }
            }
            if (!$scope.allRepoVarModel.allReportSelected) {
                $scope.isGenerateAll = false;
            }
        }

        $scope.chkReportStatus = function(chkFlag, detObj) {
            console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
        }



        $scope.isSingle = false;
        $scope.isMultiple = false;

        $scope.isGenerateAll = false;

        $scope.generateAll = function() {
            $scope.dataReportName = null;
            var flag = false;
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    flag = true
                    break;
                }
            }
            if (!flag) {
                $scope.isGenerateAll = false;
                Notification.warning("Please select atleast one.......)")
            } else {
                $scope.isGenerateAll = true;
                $scope.reportData = {};
                $scope.myReportModal = {};
                $scope.myReportModal = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/report/report_opt.html',
                    show: false
                });
                $rootScope.mainpreloder = false;
                $scope.myReportModal.$promise.then($scope.myReportModal.show);
            }
        }


        $scope.generateAllReports = function() {
            if ($scope.reportList == undefined || $scope.reportList == null || $scope.reportList.length == 0) {} else {
                //Using for mail sending for download reports 
                $scope.emailReportList = [];
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        $scope.emailReport = {};
                        $scope.emailReport.resourceId = $scope.reportList[i].resourceRefId;
                        $scope.emailReport.reportName = $scope.reportList[i].reportName;
                        $scope.emailReport.downloadOption = $scope.downloadOption;
                        $scope.emailReport.downloadFileType = $scope.downloadFileType;
                        $scope.emailReportList.push($scope.emailReport);
                    }
                }

            }
            if ($scope.emailReportList != undefined && $scope.emailReportList.length > 0) {
                $scope.isMultiple = true;
                $scope.isSingle = false;
            } else {
                $scope.isSingle = true;
                $scope.isMultiple = false;
            }
            var flag = false;
            if ($scope.isSingle) {
                for (var i = 0; i < $scope.reportList.length; i++) {
                    if ($scope.reportList[i].isSelected) {
                        flag = true
                        $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
                        $timeout(function() {
                            $scope.reportLoadingImage.status = false;
                        }, 1000);
                    }
                    if (flag) {
                        break;
                    }
                }
                if (!flag) {
                    Notification.warning("Please select atleast one.......)");
                    return;
                }
            } else if ($scope.isMultiple) {
                $scope.downloadGenericCallMultiple($scope.emailReportList, $scope.downloadOption, $scope.isMultiple, "PDF", $scope.reportList[0].resourceRefId)
                console.log("Nothig selected");
            }
        }


        $scope.downloadGenericCall = function(id, downOption, repoName) {

            if (repoName == "MAWB_LABEL") {
                var labeldValue = $rootScope.appMasterData['hawb.mawb.label.config'];
                if (labeldValue > 2) {
                    Notification.error($rootScope.nls["ERR278"]);
                    return;
                }

            }
            if (repoName == "MASTER_DO") {
                if ($scope.consol.consolDocument.isHold || $scope.consol.consolDocument.isHold == 'Yes') {
                    Notification.error($rootScope.nls["ERR96561"]);
                    return;
                }

                if (reportValidation.checkIsCanReceived($scope.consol, undefined, 'MasterShipment') && reportValidation.doValidation($scope.consol)) {

                } else {
                    return;
                }
                $scope.generateNumDto = {};
                $scope.generateNumDto.reportName = repoName;
                $scope.generateNumDto.isMultiple = true;
                $scope.generateNumDto.consolId = $scope.consol.id;
                appMetaFactory.generateNum.cangenerate($scope.generateNumDto).$promise.then(function(data) {
                    $scope.continueReportGeneration(id, downOption, repoName);
                }, function(error) {
                    $rootScope.mainpreloder = false;
                    //$scope.continueReportGeneration(id, downOption, repoName);
                });

            } else if (repoName == "CARGO_ARRIVAL_NOTICE_CONSOL") {

                $scope.generateNumDto = {};
                $scope.generateNumDto.reportName = repoName;
                $scope.generateNumDto.isMultiple = true;
                $scope.generateNumDto.consolId = $scope.consol.id;
                appMetaFactory.generateNum.cangenerate($scope.generateNumDto).$promise.then(function(data) {
                    $scope.continueReportGeneration(id, downOption, repoName);
                }, function(error) {
                    $rootScope.mainpreloder = false;
                    //$scope.continueReportGeneration(id, downOption, repoName);
                });
            } else if (repoName == "PICKUP_DELIVERY_ORDER") {
                if (!consolShipmentDataService.isPickUpDeliveyModified($scope.consol.pickUpDeliveryPoint, 'pick')) {
                    Notification.error($rootScope.nls["ERR276"])
                    $rootScope.mainpreloder = false;
                    return;
                } else if (!consolShipmentDataService.isPickUpDeliveyModified($scope.consol.pickUpDeliveryPoint, 'delivery')) {
                    Notification.error($rootScope.nls["ERR277"])
                    $rootScope.mainpreloder = false;
                    return;
                }
                $scope.continueReportGeneration(id, downOption, repoName);
            } else {
                $scope.continueReportGeneration(id, downOption, repoName);
            }
        }

        $scope.continueReportGeneration = function(id, downOption, repoName) {

            $scope.downloadOption = downOption;
            if ($scope.isGenerateAll) {
                $scope.generateAllReports();
            } else {
                var reportDownloadRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: repoName,
                    showCharge: $scope.popupValues != undefined ? $scope.popupValues.showCharge : null,
                    single: true,
                };
                if (downOption === 'eMail') {
                    var flag = checkMail();
                    if (!flag) {
                        Notification.error($rootScope.nls["ERR275"]);
                        $rootScope.mainpreloder = false;
                    } else {
                        $rootScope.mainpreloder = true;
                        downloadFactory.download(reportDownloadRequest);
                        $timeout(function() {
                            $rootScope.mainpreloder = false;
                            $scope.myReportOtherModal.$promise.then($scope.myReportOtherModal.hide);
                        }, 3000);
                    }
                } else {
                    $rootScope.mainpreloder = true;
                    //downloadFactory.download(reportDownloadRequest);
                    downloadFactory.download(reportDownloadRequest);
                }
            }

        }

        function checkMail() {
            var flag = false;
            if ($scope.reportObject != null && $scope.reportObject != undefined) {
                if ($scope.reportObject.agent != null && $scope.reportObject.agent != undefined) {
                    if ($scope.reportObject.agent.partyAddressList != null && $scope.reportObject.agent.partyAddressList != undefined && $scope.reportObject.agent.partyAddressList.length > 0) {
                        for (var i = 0; i < $scope.reportObject.agent.partyAddressList.length; i++) {
                            if ($scope.reportObject.agent.partyAddressList[0].addressType == "Primary") {
                                var emailList = $scope.reportObject.agent.partyAddressList[0].email;
                                if (emailList == null || emailList == undefined) {
                                    flag = false;
                                } else {
                                    flag = true;
                                    break;
                                }
                            } else {
                                flag = false;
                            }
                        }
                    }
                }
            }
            return flag;
        }



        $scope.downloadGenericCallMultiple = function(emailReportList, downOption, multiple, type, id) {
                var reportDownloadRequest = {
                    emailReportList: emailReportList,
                    downloadOption: downOption, // "Download","Preview","Print"
                    //downloadOption : $scope.downOption,
                    downloadFileType: type, //PDF
                    isMultiple: multiple,
                    resourceId: id,
                    screenType: 'Consol'
                };
                if (downOption === 'eMail') {
                    var flag = checkMail();
                    if (!flag) {
                        Notification.error($rootScope.nls["ERR275"]);
                        return false;
                    } else {
                        $rootScope.mainpreloder = true;
                        downloadMultipleFactory.download(reportDownloadRequest);
                        $timeout(function() {
                            $rootScope.mainpreloder = false;
                            $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                        }, 3000);
                    }
                } else {
                    $rootScope.mainpreloder = false;
                    for (var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
                        var reportFileName = '';
                        reportFileName = reportDownloadRequest.emailReportList[i].reportName;
                        var reportDownloaddRequest = {
                            resourceId: id,
                            downloadOption: downOption, // "Download","Preview","Print"
                            downloadFileType: $scope.downloadFileType, //PDF
                            reportName: reportFileName,
                            single: true
                        }

                        if (reportFileName == "MAWB_LABEL") {
                            var labeldValue = $rootScope.appMasterData['hawb.mawb.label.config'];
                            if (labeldValue > 2) {
                                Notification.error($rootScope.nls["ERR278"]);
                                continue;
                            }
                        }
                        if (reportFileName == "MASTER_DO") {
                            if ($scope.consol.consolDocument.isHold || $scope.consol.consolDocument.isHold == 'Yes') {
                                Notification.error($rootScope.nls["ERR96561"]);
                                continue;
                            }
                            if (reportValidation.checkIsCanReceived($scope.consol, undefined, 'MasterShipment') && reportValidation.doValidation($scope.consol)) {

                                $scope.generateNumDto = {};
                                $scope.generateNumDto.reportName = reportFileName;
                                $scope.generateNumDto.isMultiple = true;
                                $scope.generateNumDto.consolId = $scope.consol.id;
                                var doRequest = angular.copy(reportDownloaddRequest);
                                appMetaFactory.generateNum.cangenerate($scope.generateNumDto).$promise.then(function(data) {
                                    downloadFactory.download(doRequest);
                                }, function(error) {
                                    $rootScope.mainpreloder = false;
                                });
                            }
                        } else if (reportFileName == "CARGO_ARRIVAL_NOTICE_CONSOL") {
                            $scope.generateNumDto = {};
                            $scope.generateNumDto.reportName = reportFileName;
                            $scope.generateNumDto.isMultiple = true;
                            $scope.generateNumDto.consolId = $scope.consol.id;
                            var canRequest = angular.copy(reportDownloaddRequest);
                            appMetaFactory.generateNum.cangenerate($scope.generateNumDto).$promise.then(function(data) {
                                downloadFactory.download(canRequest);
                            }, function(error) {
                                $rootScope.mainpreloder = false;
                            });
                        } else if (reportFileName == "PICKUP_DELIVERY_ORDER") {
                            if (!consolShipmentDataService.isPickUpDeliveyModified($scope.consol.pickUpDeliveryPoint, 'pick')) {
                                Notification.error($rootScope.nls["ERR276"])
                                $rootScope.mainpreloder = false;
                                continue;
                            } else if (!consolShipmentDataService.isPickUpDeliveyModified($scope.consol.pickUpDeliveryPoint, 'delivery')) {
                                Notification.error($rootScope.nls["ERR277"])
                                $rootScope.mainpreloder = false;
                                continue;
                            }
                            downloadFactory.download(reportDownloaddRequest);
                        } else {
                            downloadFactory.download(reportDownloaddRequest);
                        }

                    }
                }
            }
            //Report Related code End Here




        $scope.servicesId = [];

        $scope.selectAllService = function() {
            $scope.servicesId = [];
            if ($scope.consol.allServicesSelected) {
                if ($scope.consol.shipmentLinkList != null && $scope.consol.shipmentLinkList.length > 0) {
                    for (var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
                        $scope.changeServiceDo($scope.consol.shipmentLinkList[i], true, i);
                        $scope.consol.shipmentLinkList[i].isSelected = true;
                    }
                }
            } else {
                if ($scope.consol.shipmentLinkList != null && $scope.consol.shipmentLinkList.length > 0) {
                    for (var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
                        $scope.changeServiceDo($scope.consol.shipmentLinkList[i], false, i);
                        $scope.consol.shipmentLinkList[i].isSelected = false;
                    }
                }
            }
        }
        $scope.changeServiceDo = function(obj, isSelected, index) {
            if (isSelected) {
                $scope.deliveryOrderDto = {};
                $scope.deliveryOrderDto.sid = obj.service.id;
                $scope.deliveryOrderDto.serviceUid = obj.serviceUid;
                $scope.deliveryOrderDto.hawbNo = obj.service.documentList[0].hawbNo;
                $scope.servicesId.splice(index, 0, $scope.deliveryOrderDto);
            } else {
                $scope.servicesId.splice(index, 1);
            }
        }



        //used for download multiple delivery order..
        $scope.downloadDo = function() {

            if ($scope.consol.consolDocument.isHold || $scope.consol.consolDocument.isHold == 'Yes') {
                Notification.error($rootScope.nls["ERR96561"]);
                return;
            }

            if (reportValidation.checkIsCanReceived($scope.consol, undefined, 'MasterShipment') && reportValidation.doValidation($scope.consol)) {
                var routingObj = {}
                routingObj.id = $scope.consol.id
                previewReportService.printDownload('MASTER_DO', 'Download', $scope.consol.id, null, true, "MasterShipment", routingObj);
            }
        }


        $scope.validateForWin = function() {

            if ($scope.consol != undefined && $scope.consol != null && $scope.consol.id != undefined) {

                if ($scope.consol.iataRate == undefined || $scope.consol.iataRate == null || $scope.consol.iataRate == " ") {
                    Notification.error($rootScope.nls["ERR96563"]);
                    return false;
                }
                if ($scope.consol.consolDocument.agent == undefined || $scope.consol.consolDocument.agent == null || $scope.consol.consolDocument.agent == "" || $scope.consol.consolDocument.agent.id == undefined) {
                    Notification.error($rootScope.nls["ERR96564"]);
                    return false;
                }
            } else {
                //Notification.error("Consol is invalid");
                return false;
            }
            return true;
        }

        $scope.openWinWeb = function() {

            $scope.winwebmodal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/air/winwebconnect/win_view_html.html',
                show: false
            });
            $scope.winwebmodal.$promise.then($scope.winwebmodal.show)
        }


        //win web connect
        $scope.connectWinWeb = function() {
            if ($scope.validateForWin()) {
                if ($scope.updateConsol('WIN')) {
                    console.log("consol updated..");
                }
            }
        }

        $scope.showWinError = function(consol) {
            $scope.optionModal = $modal.open({
                template: '<div class="modal-header" ng-mouseleave="close()"><h1 class="modal-title">ErrorMessage</h1></div><div class="modal-body">{{consol.errorMessage}}</div><div class="modal-footer" ng-click="close()">Close</div>',
                resolve: {
                    consol: function() {
                        return $scope.consol;
                    }
                }
            });
        }

        //new screen modal added

        //attachment button
        var attachModal;
        $scope.addattachmodel = function() {
            attachModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/consol_attachment_popup.html',
                show: false
            });
            if ($scope.consol.consolAttachmentList == undefined || $scope.consol.consolAttachmentList == null) {
                $scope.consol.consolAttachmentList = [];
            }
            $scope.attachmentTmpList = cloneService.clone($scope.consol.consolAttachmentList);
            if ($scope.attachmentTmpList == undefined || $scope.attachmentTmpList == null || $scope.attachmentTmpList.length == 0) {
                $scope.attachmentTmpList = [];
                $scope.attachmentTmpList.push({});
            }
            attachModal.$promise.then(attachModal.show);
            $rootScope.navigateToNextField('MasterShipmentreferenceTypeMaster0');
        };
        $scope.cancelAttachment = function() {

            for (i = 0; i < $scope.consol.consolAttachmentList.length; i++) {
                if ($scope.consol.consolAttachmentList[i].documentMaster == undefined ||
                    $scope.consol.consolAttachmentList[i].documentMaster.id == undefined ||
                    $scope.consol.consolAttachmentList[i].refNo == undefined ||
                    $scope.consol.consolAttachmentList[i].refNo == null
                ) {
                    $scope.consol.consolAttachmentList.splice(i, 1);
                }
            }
            attachModal.$promise.then(attachModal.hide);
        }

        $scope.saveAttachment = function() {

            if ($scope.consol.isAttachmentCheck()) {
                if ($scope.attachmentTmpList == null || $scope.attachmentTmpList.length == 0) {
                    $scope.consol.consolAttachmentList = null;
                } else {
                    $scope.consol.consolAttachmentList = cloneService.clone($scope.attachmentTmpList);
                }
                /*if($scope.consol.consolAttachmentList.length>0){
                    for(i=0;i<$scope.consol.consolAttachmentList.length;i++){
                        if($scope.consol.consolAttachmentList[i].documentMaster==undefined || 
                                $scope.consol.consolAttachmentList[i].documentMaster.id==undefined ||
                                $scope.consol.consolAttachmentList[i].refNo==undefined ||
                                $scope.consol.consolAttachmentList[i].refNo==null
                            ){
                            $scope.consol.consolAttachmentList.splice(i,1);
                        }
                    }
                }*/
                attachModal.$promise.then(attachModal.hide);
            }
        }



        //event button
        var eventModal;
        eventModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/consol_event_popup.html',
            show: false
        });

        $scope.addeventmodel = function() {

            if ($scope.validateConsol(0) && $scope.validateConsolOthers()) {
                if ($scope.consol.eventList == undefined ||
                    $scope.consol.eventList == null ||
                    $scope.consol.eventList.length == 0) {
                    $scope.consol.eventList = [];
                    $scope.consol.eventList.push({
                        isCompleted: 'Yes',
                        followUpRequired: 'No'
                    });
                }
                $scope.eventTmpList = cloneService.clone($scope.consol.eventList);
                $rootScope.navigateToNextField('MasterShipmenteventMaster0');
                eventModal.$promise.then(eventModal.show)
            }
        };

        $scope.cancelEventModal = function() {
            for (i = 0; i < $scope.consol.eventList.length; i++) {
                if ($scope.consol.eventList[i].eventMaster == undefined ||
                    $scope.consol.eventList[i].eventMaster == null ||
                    $scope.consol.eventList[i].eventDate == undefined ||
                    $scope.consol.eventList[i].eventDate == null ||
                    $scope.consol.eventList[i].eventDate == ""
                ) {
                    $scope.consol.eventList.splice(i, 1);
                }
            }
            eventModal.$promise.then(eventModal.hide);
        }

        /*Save event modal */
        $scope.saveEventModal = function() {
            $scope.consol.eventList = cloneService.clone($scope.eventTmpList);
            if ($scope.consol.isEventCheck()) {
                if ($scope.consol.eventList.length > 0) {
                    for (i = 0; i < $scope.consol.eventList.length; i++) {
                        if ($scope.consol.eventList[i].eventMaster == undefined ||
                            $scope.consol.eventList[i].eventMaster == null ||
                            $scope.consol.eventList[i].eventDate == undefined ||
                            $scope.consol.eventList[i].eventDate == null ||
                            $scope.consol.eventList[i].eventDate == ""
                        ) {
                            $scope.consol.eventList.splice(i, 1);
                        }
                    }
                }
                eventModal.$promise.then(eventModal.hide);
            }
        }


        //references button
        var refModal;
        $scope.addrefmodel = function() {

            refModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/consol_references_popup.html',

                show: false
            });

            if ($scope.consol.referenceList == undefined || $scope.consol.referenceList == null) {
                $scope.consol.referenceList = [];
            }

            $scope.refTmpList = cloneService.clone($scope.consol.referenceList);

            if ($scope.refTmpList == undefined || $scope.refTmpList == null || $scope.refTmpList.length == 0 || isEmptyRow($scope.refTmpList[0])) {
                $scope.refTmpList = [];
                $scope.refTmpList.push({});
            }
            refModal.$promise.then(refModal.show)
            $rootScope.navigateToNextField('MasterShipmentreferenceTypeMaster0');
        };

        function isEmptyRow(obj) {
            //return (Object.getOwnPropertyNames(obj).length === 0);
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }

            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {

                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;
                }
            }
            return isempty;
        }



        $scope.cancelReferences = function() {

            for (i = 0; i < $scope.consol.referenceList.length; i++) {
                if ($scope.consol.referenceList[i].referenceTypeMaster == undefined ||
                    $scope.consol.referenceList[i].referenceTypeMaster.id == undefined ||
                    $scope.consol.referenceList[i].referenceNumber == undefined
                ) {
                    $scope.consol.referenceList.splice(i, 1);
                }
            }
            refModal.$promise.then(refModal.hide);
        }


        /*Save event modal */
        $scope.saveReference = function() {
            if ($scope.consol.isReferenceCheck()) {
                $scope.consol.referenceList = cloneService.clone($scope.refTmpList);
                refModal.$promise.then(refModal.hide)
            }

        }

        //accounts button
        var accountModal;
        $scope.addaccountmodel = function() {
            $scope.getAccountsFromMaster($scope.consol);
            accountModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/accounts_view.html',

                show: false
            });
            $scope.navigateToNextField("flightSchedule.service");
            accountModal.$promise.then(accountModal.show)
        };

        //notes button
        var noteModal;
        $scope.addnotemodel = function() {
            noteModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/consol_notes_popup.html',
                show: false
            });
            $scope.tmpNotes = $scope.consol.notes;
            $scope.tmpINternalNote = $scope.consol.internalNote;
            noteModal.$promise.then(noteModal.show)
        };

        $scope.cancelNotes = function() {
            $scope.consol.notes = $scope.tmpNotes;
            $scope.consol.internalNote = $scope.tmpINternalNote;
            noteModal.$promise.then(noteModal.hide);
        }

        $scope.saveNotes = function() {
            noteModal.$promise.then(noteModal.hide);
        }

        //report button
        $scope.consolreportModal = {};
        $scope.addreportmodel = function() {
            $scope.consolreportModal = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/report.html',
                show: false
            });

            if ($scope.oldData != JSON.stringify($scope.consol)) {

                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR0700016"];
                ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)" style="float:both">Ok</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            $scope.reportListMaking($scope.consol, $scope.consol.id);
                            $scope.consolreportModal.$promise.then($scope.consolreportModal.show)
                        });

            } else {
                $scope.reportListMaking($scope.consol, $scope.consol.id);
                $scope.consolreportModal.$promise.then($scope.consolreportModal.show)
            }

        };

        $scope.closeReports = function() {

            $scope.consolreportModal.$promise.then($scope.consolreportModal.hide);
            var stateCount = 0;
            if ($stateParams != undefined && $stateParams.count != undefined && $stateParams.count != null) {
                stateCount = $stateParams.count + 1;
            }
            $state.go('layout.editAirConsol', {
                consolId: $scope.consol.id,
                count: stateCount
            });
        }


        //new screen modal ended

        //Sign off start

        var signOffModel;

        //Sign Off Modal
        $scope.consolSignOffModel = function(consolObject) {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_SIGNOFF_CREATE)) {
                $scope.consolObject = angular.copy(consolObject);;

                if ($scope.consolObject.consolSignOff == undefined || $scope.consolObject.consolSignOff == null) {
                    $scope.consolObject.consolSignOff = {};
                    $scope.consolObject.consolSignOff.signOffBy = $rootScope.userProfile.employee;
                    $scope.consolObject.consolSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
                    $scope.consolObject.consolSignOff.consolUid = consolObject.consolUid;
                    $scope.consolObject.consolSignOff.isSignOff = true;
                } else {
                    $scope.consolObject.consolSignOff.signOffDate = $rootScope.dateAndTimeToString($scope.consolObject.consolSignOff.signOffDate);
                }
                signOffModel = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/air/consol_shipments_new_screen/views/consol_signoff_popup.html',
                    show: false
                });
                signOffModel.$promise.then(signOffModel.show);

            }
        };


        /*Salesman Select Picker*/
        $scope.ajaxEmployeeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return EmployeeList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.customerServiceList = data.responseObject.searchResult;
                        console.log("$scope.customerServiceList", $scope.customerServiceList);
                        return $scope.customerServiceList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Customer Services');
                }
            );

        }
        $scope.ajaxCommentEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CommentFactory.search.query($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.commentList = data.responseObject.searchResult;
                        console.log("$scope.commentList", $scope.commentList);
                        return $scope.commentList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Customer Services');
                }
            );

        }


        $scope.selectedCommentMaster = function(nextFieldId) {
            $scope.consolObject.consolSignOff.description = $scope.consolObject.consolSignOff.commentMaster.description;
            $rootScope.navigateToNextField(nextFieldId);
        }


        $scope.saveConsolSignOff = function(signOffObject) {

            signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));

            if (signOffObjectCopy.signOffDate != null)
                signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);


            appMetaFactory.consol.signOff(signOffObjectCopy).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    signOffModel.$promise.then(signOffModel.hide);

                    if (data.responseObject.unSuccessfull != null) {
                        $scope.signOffResponse(data.responseObject.unSuccessfull)
                    } else {
                        var localRefStateParam = {
                            consolId: $scope.consol.id,
                            submitAction: 'Saved'
                        };
                        if ($stateParams.count != undefined && $stateParams.count != null) {
                            localRefStateParam.count = $stateParams.count + 1;
                        } else {
                            localRefStateParam.count = 0;
                        }
                        $state.go("layout.editAirConsol", localRefStateParam);
                    }


                } else {
                    Notification.error($rootScope.nls[data.responseCode]);
                    console.log("Sign off Failed", data.responseDescription);
                }
            }, function(error) {
                console.log("Sign off Failed", error);
            });

        }


        $scope.signOffResponse = function(str) {
            var newScope = $scope.$new();
            var str = str;
            newScope.errorMessage = $rootScope.nls["ERR90635"];
            newScope.errorMessage = newScope.errorMessage.replace("%s", str);
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {

                var localRefStateParam = {
                    consolId: $scope.consol.id,
                    submitAction: 'Saved'
                };
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    localRefStateParam.count = $stateParams.count + 1;
                } else {
                    localRefStateParam.count = 0;
                }
                $state.go("layout.editAirConsol", localRefStateParam);
            });
        }

        var unSignOffModel;
        //Sign Off Modal
        $scope.consolUnSignOffModal = function(object) {
            if ($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_UNSIGNOFF_MODIFY)) {

                $scope.consolObject = object;

                $scope.consolObject.consolSignOff.signOffBy = $rootScope.userProfile.employee;
                $scope.consolObject.consolSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());

                unSignOffModel = $modal({
                    scope: $scope,
                    templateUrl: 'app/components/air/consol_shipments_new_screen/views/consol_unsignoff_popup.html',
                    show: false
                });
                unSignOffModel.$promise.then(unSignOffModel.show);

            }
        };

        $scope.saveConsolUnSignOff = function(signOffObject) {
            // close the model
            signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
            if (signOffObjectCopy.signOffDate != null)
                signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);

            signOffObjectCopy.isSignOff = false;

            appMetaFactory.consolUnSignOff.unSignOff(signOffObjectCopy).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    unSignOffModel.$promise.then(unSignOffModel.hide);

                    var localRefStateParam = {
                        consolId: $scope.consol.id,
                        submitAction: 'Saved'
                    };
                    if ($stateParams.count != undefined && $stateParams.count != null) {
                        localRefStateParam.count = $stateParams.count + 1;
                    } else {
                        localRefStateParam.count = 0;
                    }
                    $state.go("layout.editAirConsol", localRefStateParam);
                } else {
                    Notification.error($rootScope.nls[data.responseCode]);
                    console.log("Sign off Failed", data.responseDescription);
                }
            }, function(error) {
                console.log("Sign off Failed", error);
            });
        }

        //Sign off end


        // Page Navigation and Events Like Reload Functionalites
        // starts here
        // Also It must be a Last functionality of the controller
        $scope.$on('addNewAirConsolEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.consol);
            localStorage.isConsolReloaded = "YES";
            return "Do you want to Reload"
        });
        $scope.$on('editAirConsolEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.consol);
            localStorage.isConsolReloaded = "YES";
            e.preventDefault();
        });
        $scope.$on('addWizardAirConsolEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.consol);
            localStorage.isConsolReloaded = "YES";
            return "Do you want to Reload"
        });
        $scope.$on('createAirConsolEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.consol);
            localStorage.isConsolReloaded = "YES";
            e.preventDefault();
        });
        $scope.$on('addNewAirConsolEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.consol;
            $rootScope.unfinishedFormTitle = "Master Shipment  (New)";
            $rootScope.category = "Master Shipment ";
            if ($scope.consol != undefined && $scope.consol != null && $scope.consol.agent != undefined && $scope.consol.agent != null &&
                $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                $rootScope.subTitle = $scope.consol.agent.partyName;
            } else {
                $rootScope.subTitle = "Unknown Party"
            }
            console.log("addCrmConsolEvent Broadcast Listener :: ", args);
        })
        $scope.$on('editAirConsolEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.consol;
            $rootScope.category = "Master Shipment ";
            $rootScope.unfinishedFormTitle = "Master Shipment Edit # " + $scope.consol.consolUid;
            if ($scope.consol != undefined && $scope.consol != null && $scope.consol.agent != undefined && $scope.consol.agent != null &&
                $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                $rootScope.subTitle = $scope.consol.agent.partyName;
            } else {
                $rootScope.subTitle = "Unknown Party"
            }
            console.log("editCrmConsolEvent Broadcast Listener :: ", args);
        })

        $scope.$on('createAirConsolEvent', function(events, args) {
            // console.log("Scope Shipment data :: ",
            // $scope.shipment);
            $rootScope.unfinishedData = $scope.consol;
            $rootScope.category = "Master Shipment ";
            $rootScope.unfinishedFormTitle = "Master Shipment Edit # " + $scope.consol.consolUid;
            if ($scope.consol != undefined && $scope.consol != null && $scope.consol.agent != undefined && $scope.consol.agent != null &&
                $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                $rootScope.subTitle = $scope.consol.agent.partyName;
            } else {
                $rootScope.subTitle = "Unknown Party"
            }
        })
        $scope.isReloaded = localStorage.isConsolReloaded;

        if ($stateParams.action == "ADD") {
            $scope.initDateFormat();
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.consol = $rootScope.selectedUnfilledFormData;
                }
                $scope.partyMasterActionResponseFn();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                } else {
                    if ($stateParams != undefined && $stateParams.forPurpose == "createdShipmentFromConsol") {
                        try {
                            try {
                                $scope.consol = $rootScope.shipmentFromConsol;
                                $scope.serviceDimensionAdded();
                            } catch (e) {
                                console.log("exception occured in localstorage")
                            }
                        } catch (e) {
                            console.log("exception occured in localstorage")
                        }
                    } else {
                        $scope.consol = createNew(false);
                    }
                }
            }
            $scope.init();
            $scope.afterSet();

            $scope.setOldDataVal();
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airNewConsol"
            }, {
                label: "Master Shipment",
                state: "layout.airNewConsol"
            }, {
                label: "Add Master Shipment",
                state: null
            }];
        } else if ($stateParams.action == "ADDWIZ") {
            $scope.initDateFormat();
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.consol = $rootScope.selectedUnfilledFormData;
                }
                $scope.partyMasterActionResponseFn();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                } else {
                    if ($stateParams != undefined && $stateParams.forPurpose == "createdShipmentFromConsol") {
                        try {
                            $scope.consol = $rootScope.shipmentFromConsol;
                            if ($stateParams.shipmentUid != undefined && $stateParams.shipmentUid != null) {
                                $scope.setLinkToConsol($stateParams.shipmentUid);
                            }
                        } catch (e) {
                            console.log("exception occured in localstorage")
                        }
                    } else {
                        $scope.consol = createNew(true);

                    }
                }
            }
            $scope.init();
            $scope.afterSet();

            $scope.setOldDataVal();
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.addAirConsolWizard"
            }, {
                label: "Master Shipment",
                state: "layout.airNewConsol"
            }, {
                label: "Add Master Shipment",
                state: null
            }];
        } else if ($stateParams.action == "CREATE") {
            $scope.initDateFormat();
            $scope.showDetail = false;
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.consol = $rootScope.selectedUnfilledFormData;
                }
                $scope.partyMasterActionResponseFn();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                } else {
                    if ($stateParams != undefined && $stateParams.forPurpose == "createdShipmentFromConsol") {
                        try {
                            $scope.consol = $rootScope.shipmentFromConsol;
                            $scope.serviceDimensionAdded();
                        } catch (e) {
                            console.log("exception occured in localstorage")
                        }
                    } else {
                        $scope.consol = $rootScope.createMasterShipment;
                    }
                }
            }
            $scope.init();
            $scope.afterSet();
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airNewConsol"
            }, {
                label: "Master Shipment",
                state: "layout.airNewConsol"
            }, {
                label: "Create Master Shipment",
                state: null
            }];
        } else {
            $scope.initDateFormat();
            $scope.showDetail = false;
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                    $scope.afterSet();
                } else {
                    $scope.consol = $rootScope.selectedUnfilledFormData;
                    $scope.afterSet();
                }
                $scope.partyMasterActionResponseFn();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isConsolReloaded = "NO";
                    $scope.consol = JSON.parse(localStorage.reloadFormData);
                    $scope.afterSet();
                } else {
                    if ($stateParams != undefined && $stateParams.forPurpose == "createdShipmentFromConsol") {
                        try {
                            $scope.consol = $rootScope.shipmentFromConsol;
                            $scope.serviceDimensionAdded();
                        } catch (e) {
                            console.log("exception occured in localstorage")
                        }
                    } else if ($stateParams != undefined && $stateParams.forPurpose == "EDI") {
                        try {
                            $scope.consol = $rootScope.newConsolAfterEDI;
                        } catch (e) {
                            console.log("exception occured in localstorage")
                        }
                    } else {
                        $scope.consol = createNew();
                    }
                    if ($stateParams.consolId != undefined) {
                        $scope.getConsolData($stateParams.consolId);
                    }
                }
            }
            $scope.init();

            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airNewConsol"
            }, {
                label: "Master Shipment",
                state: "layout.airNewConsol"
            }, {
                label: "Edit Master Shipment",
                state: null
            }];
        }
    }
]);