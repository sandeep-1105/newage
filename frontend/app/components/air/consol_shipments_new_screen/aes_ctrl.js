/**
 * Created by hmspl on 12/4/16.
 */
app.controller('aes_Ctrl',['$rootScope', '$http', '$timeout', '$scope', '$location', '$modal', 'ngDialog', '$state', 
        '$stateParams', 'aesService', 'AesView', 'AesStatusHistory',
    function($rootScope, $http, $timeout, $scope, $location, $modal, ngDialog, $state, 
        $stateParams, aesService, AesView, AesStatusHistory) {

    $scope.aesStatus = {};
    $scope.aesStatus = {
        "status": "-"
    };

    $rootScope.breadcrumbArr = [{
        label: "Air",
        state: "layout.airNewConsol"
    }, {
        label: "AES View",
        state: null
    }];

    $scope.init = function() {

        $rootScope.breadcrumbArr = [{
            label: "Air",
            state: "layout.airNewConsol"
        }, {
            label: "AES View",
            state: null
        }];

        $state.go('init called');

    }
    $scope.getAesHistory = function() {
        AesStatusHistory.get({
            id: $scope.aesFile.id
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting aes.", data)
                $scope.aesFileStatusHistoryList = data.responseObject;
                console.log($scope.aesFileStatusHistoryList);
            }
        }, function(error) {
            console.log("Error while getting aes.", error)
        });
    }

    $scope.cancel = function() {

        $scope.showDetail = false;
        $state.go('layout.viewNewAirConsol', {
            consolId: $stateParams.consolId
        });

    };

    $scope.backConsol = function() {

        if ($stateParams.fromScreen != undefined && $stateParams.fromScreen == 'layout.viewNewAirConsol') {
            var params = {};
            params.consolId = $stateParams.consolId;
            $state.go('layout.viewNewAirConsol', params);
        } else if ($stateParams.fromScreen != undefined && $stateParams.fromScreen == 'layout.editAirConsol') {
            var params = {};
            params.consolId = $stateParams.consolId;
            $state.go('layout.editAirConsol', params);
        } else if ($stateParams.fromScreen != undefined && $stateParams.fromScreen == 'layout.viewCrmShipment') {
            var params = {};
            params.shipmentUid = $stateParams.shipmentUid;
            $state.go('layout.viewCrmShipment', params);
        } else if ($stateParams.fromScreen != undefined && $stateParams.fromScreen == 'layout.editNewShipment') {
            var params = {};
            params.shipmentId = $stateParams.shipmentId;
            $state.go('layout.editNewShipment', params);
        } else {
            console.log("Dont navigate any where ")
        }
    }

    $scope.backStock = function() {
        NavigationService.set(null);
        NavigationService.setLocation(null);
        NavigationService.setSubData1(null);
        $location.path('/air/stock_management_list');
    }

    $scope.openDefaultTab = function() {
        console.log("openDefaultTab in edit page is running...");

        if ($scope.userProfile.featureMap['AIR_MASTER_MORE_INFO_TAB_VIEW']) {
            $scope.Tabs = 'moreInfo';
        } else {
            console.log("No Permission to perform this action.");
        }
    }

    $scope.addConsol = function() {

        $state.go("layout.addAirConsol");

    }
    $scope.openDefaultTab = function() {
        console.log("openDefaultTab in edit page is running...");

        if ($scope.userProfile.featureMap['AIR_MASTER_MORE_INFO_TAB_VIEW']) {
            $scope.Tabs = 'moreInfo';
        } else {
            console.log("No Permission to perform this action.");
        }
    }

    $scope.aesEdit = function(aesFile) {
        console.log("aesEdit method called : ", aesFile);
        var navAesObj = {
            status: $scope.detailTab,
            aesId: aesFile.id,
            fromScreen: $stateParams.fromScreen,
            totalRecord: $scope.detailTab == 'active' ? $scope.totalRecordView : $scope.totalRecord,
            consolId: $stateParams.consolId,
            shipmentUid: $stateParams.shipmentUid,
            fromState: $state.current.name,
            shipmentId: $stateParams.shipmentId
        };
        $state.go("layout.editAes", {
            navAesObj: navAesObj
        });
    }



    // View Aes
    $scope.getAesById = function(aesId) {
        console.log("getAesById ", aesId);
        AesView.get({
            id: aesId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting aes.", data)
                $scope.aesFile = data.responseObject;
                $scope.Tabs = 'moreInfo';
            }

        }, function(error) {
            console.log("Error while getting aes.", error)
        });

    }

    /*
     * $scope.editConsol = function() {
     * 
     * $state.go("layout.editAes"); }
     */

    /* AES POPUP */
    $scope.morepopup = function(obj) {
        var newScope = $scope.$new();
        newScope.aesCommodity = obj;
        newScope.aesCommodity.smeIndicator = newScope.aesCommodity.smeIndicator || newScope.aesCommodity.smeIndicator == 'Yes' ? 'Yes' : 'No';
        newScope.aesCommodity.eligiblePartyCertification = newScope.aesCommodity.eligiblePartyCertification || newScope.aesCommodity.eligiblePartyCertification == 'Yes' ? 'Yes' : 'No';
        newScope.aesCommodity.isUsedVehicle = newScope.aesCommodity.isUsedVehicle || newScope.aesCommodity.isUsedVehicle == 'Yes' ? 'Yes' : 'No';
        var myOtherModal = $modal({
            scope: newScope,
            templateUrl: 'app/components/air/consol_shipments/views/commodity_moreinfo_popup.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
    };

    console.log("$stateParams.action ----- ", $stateParams.action);
    switch ($stateParams.action) {
        case "VIEW":
            console.log("View Consol Page..........................!", $stateParams.consolId, +" " + $stateParams.consolIndex);
            $rootScope.unfinishedData = undefined;
            $scope.getAesById($stateParams.aesId);
            if ($stateParams.fromScreen == 'Shipment') {
                $rootScope.breadcrumbArr = [{
                    label: "Crm",
                    state: "layout.crmShipment"
                }, {
                    label: "AES View",
                    state: null
                }];
            } else {
                $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airNewConsol"
                }, {
                    label: "AES View",
                    state: null
                }];
            }
            break;
        case "SEARCH":
            console.log("List Consol Page...........................!");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airNewConsol"
            }, {
                label: "AES",
                state: null
            }];
            break;
    }

}]);