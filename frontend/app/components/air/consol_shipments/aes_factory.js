(function() {

	app.factory("AesSearch",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("AesPendingSearch",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/aespendingsearch", {}, {
			search : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("AesList",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("AesTransportModeList",['$resource', function($resource) {
		return $resource("/api/v1/aestransport/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("IataMasterList",['$resource', function($resource) {
		return $resource("/api/v1/iatamaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ScacMasterList",['$resource', function($resource) {
		return $resource("/api/v1/scacamaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("LicenseList",['$resource', function($resource) {
		return $resource("/api/v1/aeslicense/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	
	app.factory("ScheduleList",['$resource', function($resource) {
		return $resource("/api/v1/schedulebhtsmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("ExportList",['$resource', function($resource) {
		return $resource("/api/v1/exportcodemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ItarList",['$resource', function($resource) {
		return $resource("/api/v1/itarexmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("InBondTypeMasterList",['$resource', function($resource) {
		return $resource("/api/v1/inbondtypemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	

	app.factory("AesAdd",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("AesAddAll",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/createall", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AesEdit",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AesView",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("AesGetAll",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/getallaeslist/masteruid/:masteruid", {}, {
			get : {
				method : 'GET',
				params : {
					masteruid : ''
				},
				isArray : false
			}
		});
	}]);
	
	

	app.factory("AesRemove",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("AesEdiSave",['$resource', function($resource) {
		return $resource("/api/v1/aesfile/edi/save/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
})();
