app.service('ConsolPartyClearService', function($rootScope) {
	
	this.chkAndClear = function(consol, forObject) {
		switch (forObject) {
			case "consol.agent":
				if(consol.agent != null && consol.agent.id != null) {
					consol.agent = null;
					consol.agentAddress = this.clearAddress();	
				} else {
					consol.agentAddress = this.clearAddress();
				}
			break;
			case "consol.consolDocument.shipper":
				if(consol.consolDocument.shipper != null && consol.consolDocument.shipper.id != null) {
					consol.consolDocument.shipper = null;
					consol.consolDocument.shipperAddress = this.clearAddress();	
				} else {
					consol.consolDocument.shipperAddress = this.clearAddress();
				}
				
			break;
			case "consol.consolDocument.consignee":
				if(consol.consolDocument.consignee != null && consol.consolDocument.consignee.id != null) {
					consol.consolDocument.consignee = null;
					consol.consolDocument.consigneeAddress = this.clearAddress();	
				} else {
					consol.consolDocument.consigneeAddress = this.clearAddress();	
				}
				
			break;
			case "consol.consolDocument.firstNotify":
				if(consol.consolDocument.firstNotify != null && consol.consolDocument.firstNotify.id != null) {
					consol.consolDocument.firstNotify = null;
					consol.consolDocument.firstNotifyAddress = this.clearAddress();	
				} else {
					consol.consolDocument.firstNotifyAddress = this.clearAddress();
				}
				
			break;
			case "consol.consolDocument.secondNotify":
				if(consol.consolDocument.secondNotify != null && consol.consolDocument.secondNotify.id != null) {
					consol.consolDocument.secondNotify = null;
					consol.consolDocument.secondNotifyAddress = this.clearAddress();	
				} else {
					consol.consolDocument.secondNotifyAddress = this.clearAddress();
				}
				
			break;

			case "consol.consolDocument.forwarder":
				if(consol.consolDocument.forwarder != null && consol.consolDocument.forwarder.id != null) {
					consol.consolDocument.forwarder = null;
					consol.consolDocument.forwarderAddress = this.clearAddress();
				} else {
					consol.consolDocument.forwarderAddress = this.clearAddress();
				}
			break;

			case "consol.consolDocument.agent":
				if(consol.consolDocument.agent != null && consol.consolDocument.agent.id != null) {
					consol.consolDocument.agent = null;
					consol.consolDocument.agentAddress = this.clearAddress();	
				} else {
					consol.consolDocument.agentAddress = this.clearAddress();
				}
				
			break;

			case "consol.consolDocument.issuingAgent":
				if(consol.consolDocument.issuingAgent != null && consol.consolDocument.issuingAgent.id != null) {
					consol.consolDocument.issuingAgent = null;
					consol.consolDocument.issuingAgentAddress = this.clearAddress();	
				} else {
					consol.consolDocument.issuingAgentAddress = this.clearAddress();
				}
				
			break;
			
			default:

		}
		return consol;
	}
	
	this.clearAddress = function() {
 		address = {};
 		return address;
 	}

});