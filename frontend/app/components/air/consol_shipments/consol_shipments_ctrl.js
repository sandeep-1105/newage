/**
 * Created by hmspl on 12/4/16.
 */
app.controller('console_shipmentCtrl', function($rootScope,$http,$sce,$timeout,$scope,$location,$modal,ngDialog,ConsolGet,consolService,NavigationService,
		PartiesList,PortByTransportMode,ServiceByTransportMode,CurrencySearchExclude,ValidateUtil,PortGroupList,CarrierByTransportMode,
		ConsolSearch,ConsolStatusChange, downloadFactory,downloadMultipleFactory, consolShipmentDocumentIdList,AutoImportProcess,cloneService,ChargeSearchKeyword,
		ConsolInvoiceList,ConsolEdit,LogoGetByLocation,ConsolGetByUid,GetInvoiceCountByMasterUIDorServiceUID,
		invoiceService,SequenceFactoryForCAN, appMetaFactory,
		RecentHistorySaveService, $state, $stateParams,ProvisionalViewByConsolUid,ShipmentUid,CommentFactory, EmployeeList,
		ShipmentServiceInvoiceList,AesSearch,ShipmentIdByServiceUid, AirlineEDIGet, AirlineEDISave,ShipmentIdBasedOnUid,ProvisionalViewByShipmentServiceUid,Notification,AesAddAll,previewReportService,roleConstant,ServiceSignOffStatus,ConsolSignOffStatus,consolShipmentAccountsService,consolShipmentDataService,AesEdiSave,CommonValidationService){
	
	$scope.tabArray = ["Active", "Closed", "Cancelled", "All"];
	$scope.searchOption = ['Service','Agent', 'Freight','Carrier', 'Created By'];
	$scope.directShipmentOption=["Yes", "No", "All"];
	

	$scope.tabChange = function (tabName){
		$scope.searchDto.importExport = "Both";
		$scope.searchDto.directShipment = "All";
	    $scope.currentTab = tabName;
		if(tabName == 'All') {
			$scope.searchDto.status = null;
		} else {
			$scope.searchDto.status = tabName;
		}
		$scope.page = 0;
		$scope.limit = 10;
		$scope.totalRecord = 10;
		$scope.search();
	}



	

	$scope.consolSearchHeadArray = [ {
						"name" : "#",
						"width" : "w50px",
						"prefWidth" : "50",
						"model" : "no",
						"search" : false,
					}, {
						"name" : "Master ID",
						"model" : "masterId",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"width" : "w100px",
						"prefWidth" : "150",
						"key" : "consolUid"
					}, {
						"name" : "MAWB No",
						"model" : "mawbNumber",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"width" : "w100px",
						"prefWidth" : "150",
						"key" : "masterNo"
					}, {
						"name" : "MAWB Date",
						"model" : "mawbDate",
						"search" : true,
						"wrap_cell" : true,
						"type" : "date-range",
						"sort" : true,
						"width" : "w103px",
						"prefWidth" : "175",
						"key" : "mawbDate"

					}, {
						"name" : "Master Date",
						"model" : "masterDate",
						"search" : true,
						"wrap_cell" : true,
						"type" : "date-range",
						"sort" : true,
						"width" : "w103px",
						"prefWidth" : "175",
						"key" : "shipmentDate"

					}, {
						"name" : "Origin",
						"model" : "originName",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"width" : "w150px",
						"prefWidth" : "150",
						"key" : "origin"

					}, {
						"name" : "Destination",
						"model" : "destinationName",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"width" : "w150px",
						"prefWidth" : "150",
						"key" : "destination"
					},

					{
						"name" : "ETD",
						"model" : "etd",
						"search" : true,
						"wrap_cell" : true,
						"type" : "date-range",
						"sort" : true,
						"width" : "w103px",
						"prefWidth" : "175",
						"key" : "etd"

					}, {
						"name" : "ETA",
						"model" : "eta",
						"search" : true,
						"wrap_cell" : true,
						"type" : "date-range",
						"sort" : true,
						"width" : "w103px",
						"prefWidth" : "175",
						"key" : "eta"

					}
					/*
					 * { "name":"Status", "width":"w125px", "prefWidth":"125",
					 * "model":"jobStatus", "search":true, "type":"drop",
					 * "data":$rootScope.enum['JobStatus'], "key":"status",
					 * "sort":true}
					 */
					];
	
	 $scope.searchBy = function() {
 		
	    	if($scope.consolSearchHeadArray.length == 9) {
	    		$scope.consolSearchHeadArray.splice($scope.consolSearchHeadArray.length - 1, 1);
	    	}
	    	$scope.navigateToNextField('searchValueAll');
	    	if($scope.searchDto.searchName===undefined || $scope.searchDto.searchName===null){
	    		$scope.searchDto.searchValue = "";
	    		$scope.searchValueAllFlag = false;
	    		var obj = {
						"name" : "ETA",
						"model" : "eta",
						"search" : true,
						"wrap_cell" : true,
						"type" : "date-range",
						"sort" : true,
						"width" : "w103px",
						"prefWidth" : "175",
						"key" : "eta"

					};
	    		$scope.consolSearchHeadArray.push(obj);
	    	} else {
	    		$scope.searchValueAllFlag = true;
	    		$scope.currentTab = 'All';
	    		$scope.searchDto.status = null;
	    		
	    		
	    		if($scope.searchDto.searchName === "Service") {
	    			var obj = {
                      	  "name":"Service",
                    	  "search":false,
                    	  "model":"serviceName",
                    	  "wrap_cell":true,
                    	  "type":"text",
                    	  "sort":false,
                    	  "width":"w200px",
                    	  "prefWidth":"200",
                    	  "key":"serviceName",
                    	 }
			    		$scope.consolSearchHeadArray.push(obj);
		    	} else if($scope.searchDto.searchName === "Freight") {
		    		
		    		var obj = {
                	 	"name":"Freight",
                	 	"model":"frieght",
                	 	"search":false,
                	 	"wrap_cell":true,
                	 	"type":"text",
                	 	"sort":false,
                	 	"width":"w150px",
                	 	"prefWidth":"150",
                	 	"key":"freightTerm"
                	 }
		    		
		    		$scope.consolSearchHeadArray.push(obj);
		    		
		    	} else if($scope.searchDto.searchName === "Carrier") {
		    		
		    		var obj = {
                    	 	"name":"Carrier",
                    	 	"model":"carrierName",
                    	 	"search":false,
                    	 	"wrap_cell":true,
                    	 	"type":"text",
                    	 	"sort":false,
                    	 	"width":"w150px",
                    	 	"prefWidth":"150",
                    	 	"key":"carrier"
                    	 }
			    		
		    		$scope.consolSearchHeadArray.push(obj);
		    		
		    	} else if($scope.searchDto.searchName === "Created By") {
		    		
		    		var obj = {
                  	  "name":"Created By",
                  	  "model":"createdBy",
                  	  "search":false,
                  	  "wrap_cell":true,
                  	  "type":"text",
                  	  "sort":false,
                  	  "width":"w125px",
                  	  "prefWidth":"125",
                  	  "key":"createdBy"
                  	 }
			    		
		    		$scope.consolSearchHeadArray.push(obj);
		    		
		    	} else if($scope.searchDto.searchName === "Agent") {
		    		
		    		var obj = {
							"name" : "Agent",
							"search" : false,
							"model" : "agentName",
							"wrap_cell" : true,
							"type":"color-text",
							"sort" : false,
							"width" : "w200px",
							"prefWidth" : "200",
							"key" : "partyName",
						}
			    		
			    		$scope.consolSearchHeadArray.push(obj);
		    		
		    	}
	    		
	    	}
	    	$scope.search();
	    }
	 

	 $scope.search = function() {
	    	
	    	$scope.searchDto.selectedPageNumber = $scope.page;
			$scope.searchDto.recordPerPage = $scope.limit;

			if($scope.searchDto.shipmentDate!=null){
				$scope.searchDto.shipmentDate.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.shipmentDate.startDate);
				$scope.searchDto.shipmentDate.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.shipmentDate.endDate);
			}
			
			if($scope.searchDto.mawbDate!=null){
				$scope.searchDto.mawbDate.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.mawbDate.startDate);
				$scope.searchDto.mawbDate.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.mawbDate.endDate);
			}
			
			if($scope.searchDto.etd!=null){
				$scope.searchDto.etd.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.etd.startDate);
				$scope.searchDto.etd.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.etd.endDate);
			}
			
			if($scope.searchDto.eta!=null){
				$scope.searchDto.eta.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.eta.startDate);
				$scope.searchDto.eta.endDate = $rootScope.sendApiEndDateTime($scope.searchDto.eta.endDate);
			}
			
			$scope.consolArr = [];
			
			ConsolSearch.query($scope.searchDto).$promise.then(function(data, status) {
				$scope.totalRecord = data.responseObject.totalRecord;
				var tempArr = [];
				var resultArr = [];
				tempArr = data.responseObject.searchResult;
				var tempObj = {};
				
				angular.forEach(tempArr,function(item,index){
					tempObj = item;
					if(tempObj.agentName!=null && tempObj.colorCode!=null){
						tempObj.color=tempObj.colorCode;
						tempObj.changeColor=true;
					}
					tempObj.no = (index+1)+($scope.page*$scope.limit);
					resultArr.push(tempObj);
					tempObj = {};
				});
				
				$scope.consolArr = resultArr;
			});
	 }	 
	 
	 	$scope.limitArr = [10,15,20];
	    $scope.page = 0;
	    $scope.limit = 10;
	    $scope.totalRecord = 10;
	    
	    $scope.changePage = function(param){
			$scope.page = param.page;
			$scope.limit = param.size;
			$scope.search();
		};
	    
	    $scope.limitChange = function(item){
			$scope.page = 0;
			$scope.limit = item;
			$scope.search();
		};
 

		$scope.searchSortSelection = {
		        sortKey:"consolUid",
		        sortOrder:"desc"
		};
	
		$scope.changeSearch = function(param){
			for (var attrname in param) {
				$scope.searchDto[attrname] = param[attrname];
			}
			$scope.page = 0;
			$scope.search();
			console.log("change search",$scope.searchDto);
		}
	    
	    $scope.sortChange = function(param){
			$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
			$scope.searchDto.sortByColumn = param.sortKey;
			$scope.search();
		}
	
	$scope.$consolShipmentAccountsService = consolShipmentAccountsService;
	$scope.$consolShipmentDataService=consolShipmentDataService;
	$scope.$roleConstant = roleConstant;
	$scope.aesStatus={};
	$scope.aesStatus = {
		"status" : "Active"
	};
	/*  Code Mirrroer Popup   */

	$scope.modes = ['Scheme', 'XML', 'Javascript'];
	$scope.mode=$scope.modes[1];
	$scope.newdata=[];
	
	
	$scope.provisional = {};

	$scope.dataup=function(){

		console.log("pop clicked");
		$scope.newdata.push($scope.getval);
		//alert("sdfd"+$scope.newdata);
		console.log($scope.newdata);
	};

	$scope.getval="<!-- XML code in here. -->";

$scope.getColorCode = function(service){
		
		if(service.holdShipment){
			return "Hold";
		}else if (service.hazardous){
			return "Haz";
		}else if (service.overDimension){
			return "Over";
		}
		
		return null;
	}


	$scope.codeMirror_Generate = function(moredit){
		var newScope = $scope.$new();
		newScope.moredit = moredit;
		var myTotalOtherModal = $modal(
			{scope:newScope,templateUrl: '/app/components/air/consol_shipments_new_screen/codemir_pop.html', show: false});
		myTotalOtherModal.$promise.then(myTotalOtherModal.show);
	};
	
	//report view		
    var reportvwModal;		
    $scope.reportviewmodel = function() {		
    $scope.reportListMaking($scope.consol,$scope.consol.id);
     reportvw= $modal({		
         scope: $scope,		
         backdrop: 'static',		
         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/report_view.html',		
         		
         show: false		
     });		
     $scope.navigateToNextField("flightSchedule.service");		
     reportvw.$promise.then(reportvw.show)		
    };		
  //notes view		
    var notesvwModal;		
    $scope.notesviewmodel = function() {		
     notesvwModal= $modal({		
         scope: $scope,		
         backdrop: 'static',		
         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/consol_notes_popup_view.html',		
         		
         show: false		
     });		
     $scope.navigateToNextField("flightSchedule.service");		
     notesvwModal.$promise.then(notesvwModal.show)		
    };		
  //accounts view		
    var accvwModal;		
    $scope.accviewmodel = function() {		
     $scope.getAccountsFromMaster($scope.consol);
     accvwModal= $modal({		
         scope: $scope,		
         backdrop: 'static',		
         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/accounts_view.html',		
         		
         show: false		
     });		
     $scope.navigateToNextField("flightSchedule.service");		
     accvwModal.$promise.then(accvwModal.show)		
    };		
  //reference view		
    var refvwModal;		
    $scope.refviewmodel = function() {		
     refvwModal= $modal({		
         scope: $scope,		
         backdrop: 'static',		
         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/reference_table_view.html',		
         		
         show: false		
     });		
     $scope.navigateToNextField("flightSchedule.service");		
     refvwModal.$promise.then(refvwModal.show)		
    };		
    //attachment view		
    var attvwModal;		
    $scope.attviewmodel = function() {		
     attvwModal= $modal({		
         scope: $scope,		
         backdrop: 'static',		
         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/consol_attachment_popup_view.html',		
         		
         show: false		
     });		
     $scope.navigateToNextField("flightSchedule.service");		
     attvwModal.$promise.then(attvwModal.show)		
    };		
  //event view		
    var eventvwModal;		
    $scope.eventviewmodel = function() {		
     eventvwModal= $modal({		
         scope: $scope,		
         backdrop: 'static',		
         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/events_view.html',		
         		
         show: false		
     });		
     $scope.navigateToNextField("flightSchedule.service");		
     eventvwModal.$promise.then(eventvwModal.show)		
    };
	

	/*  Code Mirrroer Popup   */
    $scope.errorMap = new Map();
    
    $scope.cloneConnectionList=[];
    
    
    $scope.searchDto = {};

    $scope.init = function (){
    	
    	$scope.customerAgent=undefined; 
   	 
    	$scope.documentTypeCode=undefined; 
      
    	if(NavigationService.get()!=null){
		
    		$scope.consol = NavigationService.get();
			
    		$scope.showDetail = NavigationService.getSubData1();
			
    		$scope.setVisibilities('fromStock');
        

    	} else {    	
    	
    		$scope.consol={};
    	
    		$scope.showDocumentActions=true;
    	
    		$scope.showDetail=false;

    		$scope.tabChange('Active');
    	}
    }

    
    /*Aes generate all check box code started here*/

    $scope.allAesModel = {};
    $scope.allAesModel.allAesSelected = false;

    	     $scope.selectAllAes = function() {
    	     	
    	     	for ( var i = 0; i < $scope.finalArr.length; i++) {
    	     	    $scope.finalArr[i].isSelected = $scope.allAesModel.allAesSelected;
    	     	}
    	     }
    	     $scope.chkAesStatus = function(chkFlag, detObj) {
    	     	console.log("Changed Status :: ",chkFlag, "  -----------  ",detObj);
    	     }

    	     $scope.generateAllAes = function(){
    	    	 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_AES_GENERATE_CREATE)){
        	    	 var isError = false;
        	    	 var errorObj = [];
    	    	     	var count=0;
    	    	     	var aesArr=[];
    	    	     	for (var i = 0; i < $scope.finalArr.length; i++) {
    	    	     		if($scope.finalArr[i].isSelected) {
    	    	     			count=count+1;
    	    	     			aesArr.push($scope.finalArr[i]);
    	    	     		}
    	    	     	}
    	    	     	if(count>1){
    	    	     		console.log("generate more than one,no need to go aes add page");
    	    	     		for(var ai = 0; ai < aesArr.length; ai++) {
	    	    	     		if(aesArr[ai].id == undefined || aesArr[ai].id == null) {
		    	     				isError = true;
		    	     				errorObj.push(aesArr[ai].serviceUid);
		    	     			}
    	    	     		}
    	    	     		if(isError) {
    	    	     			for(var aErr = 0; aErr < errorObj.length; aErr++) {
    	    	     				var errMsg = $rootScope.nls["ERR2012028"];
    	    	     				errMsg = errMsg.replace("%s", errorObj[aErr]);
    	    	     				Notification.error(errMsg);
    	    	     			}
    	    	     		} else {
    	    	     			for(var ai = 0; ai < aesArr.length; ai++) {
    	    	     				$scope.validateAndGenerateEdi(aesArr[ai],ai+1);
        	    	     		}
    	    	     		}
    	    	     	}
    	    	     	else if (count==1){
    	    	     		if(aesArr[0].id==undefined || aesArr[0].id==null){
    	    	     			var errMsg = $rootScope.nls["ERR2012028"];
    	    	     			errMsg = errMsg.replace("%s", aesArr[0].serviceUid);
    	    	     			Notification.error(errMsg);
	    	     				return;
    	    	     		}else{
    	    	     			$scope.validateAndGenerateEdi(aesArr[0],0);
    	    	     		}
    	    	     	}
    	    	     	else{
    	    	     		$rootScope.clientMessage = $rootScope.nls["ERR2012029"];
    	    	     	}
    	    	 }
    	     }
    	     
    	     
    	     $scope.validateAndGenerateEdi = function(aesFile,index) {
    			 console.log("validateAndGenerateEdi is called...", $scope.aesFile.id);
    			 var errMsg = $rootScope.nls["ERR2012030"];
    			 
    			 AesEdiSave.get({
    		   			id : aesFile.id
    		   		}, function(data) {
    		   			if (data.responseCode =="ERR0"){
    		   				Notification.success("Success Generation for AES-"+index);
    		   			} else if(data.responseCode == "ERR2012036") {
    		   				var responseObject = data.responseObject;
    		   				for(var errIdx = 0; errIdx < responseObject.fieldErrorList.length; errIdx++) {
    		   					Notification.error(responseObject.fieldErrorList[errIdx].errorMsg +"in service -"+ aesFile.serviceUid);
    		   				}
    		   			}
    		   		}, function(error) {
    		   			errMsg = errMsg.replace("%s", aesFile.serviceUid);
    		   			Notification.error(errMsg);
    		   		});
    		 }

    	     $scope.showGenerateBtn = function(){
    	    	 
    	      	if($scope.finalArr==null || $scope.finalArr == undefined){
     	      		return false;
     	      	}
     	      	
    	      /*	for (var i = 0; i < $scope.finalArr.length; i++) {
    	      		if($scope.finalArr[i].status=='Generate') {
    	      			flag=true;
    	      			return flag;
    	      		}
    	      	}*/
    	      	return true;
    	      }

    	     $scope.aesGenerateAll = function(aesArr){
    	     	console.log("Aes files need to genarate : ",aesArr);
    	     	AesAddAll.save(aesArr).$promise.then(function(data) {
    				if (data.responseCode == 'ERR0') {
    					console.log("Aes Saved Successfully");
                        $scope.spinner = false;
    				} else {
    					 $scope.spinner = false;
    					console.log("Aes Saving Failed " , data.responseDescription);
    				}
    			}, function(error) {
    				$scope.spinner = false;
    				console.log("Aes Saving Failed : ", error)
    			});
    	     }
    	     /*Aes generate all check box code ended here*/		
    
    $scope.cancel = function(){
		$scope.showDetail=false;
	};
    
    $scope.processProvisional=function(object,shipmentServiceDetail){
   	 if(object!=null && object.id !=null){
   		if($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_MODIFY)){
   			 consolShipmentAccountsService.editProvisionalFromConsol(object,$scope.consol,shipmentServiceDetail);
   		 }
   	 }
   	 else{
   		if($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_MODIFY)){
   			 consolShipmentAccountsService.addProvisionalFromConsol($scope.consol,shipmentServiceDetail);
   		 }
   	 }
    }
    
    $scope.getInvoiceCrnView=function(invoiceCreditNote,shipmentServiceDetail)
    {
    	consolShipmentAccountsService.getInvoiceCrnView(invoiceCreditNote,shipmentServiceDetail,$scope.consol);
    }
    
    $scope.getSignOffStatus=function(fromState,consol,shipmentServiceDetail){
    	var serviceId=null;
    	if(consol.shipmentLinkList !=null && consol.shipmentLinkList !=undefined && consol.shipmentLinkList.length==1 || 
    			(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined)){
		if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined){
				 serviceId=shipmentServiceDetail.id;
		}
		else{
				serviceId=consol.shipmentLinkList[0].service.id;
		}
    	ServiceSignOffStatus.get({
    		serviceId: serviceId
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                    if(data.responseObject!=null && data.responseObject.isSignOff==true){
                    	
                    	if(fromState=='INV'){
                    		consolShipmentAccountsService.createInvoice(consol,shipmentServiceDetail);
                    	}
                    	else if(fromState=='CRN_COST'){
                    		consolShipmentAccountsService.createCreditNoteCost(consol,shipmentServiceDetail);
                    	}
                    	else if(fromState=='CRN_REV'){
                    		consolShipmentAccountsService.createCreditNoteRevenue(consol,shipmentServiceDetail);
                    	}
                    	
                    }
                    else{
                    	console.log( $rootScope.nls["ERR40028"]);
                        $rootScope.clientMessage = $rootScope.nls["ERR40028"];
                    }
            }
            else{
            	console.log( $rootScope.nls["ERR40028"]);
                $rootScope.clientMessage = $rootScope.nls["ERR40028"];
            }
        }, function(error) {
            console.log("error in getSignOffStatus method : " + error);
        });
		}
		
    	else{
    		ConsolSignOffStatus.get({
        		consolId: consol.id
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                        if(data.responseObject!=null && data.responseObject.isSignOff==true){
                        	
                        	if(fromState=='INV'){
                        		consolShipmentAccountsService.createInvoice(consol,shipmentServiceDetail);
                        	}
                        	else if(fromState=='CRN_COST'){
                        		consolShipmentAccountsService.createCreditNoteCost(consol,shipmentServiceDetail);
                        	}
                        	else if(fromState=='CRN_REV'){
                        		consolShipmentAccountsService.createCreditNoteRevenue(consol,shipmentServiceDetail);
                        	}
                        	
                        }
                        else{
                        	console.log( $rootScope.nls["ERR40027"]);
                            $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                        }
                }
                else{
                	console.log( $rootScope.nls["ERR40027"]);
                    $rootScope.clientMessage = $rootScope.nls["ERR40027"];
                }
            }, function(error) {
                console.log("error in getSignOffStatus method : " + error);
            });
    	}
    }

	    /*BUTTON GROUP SCOPE VALUE*/
	    $scope.btnGrp = {};
    	$scope.btnGrpFilter = {};
    	$scope.btnGrp.TypeServiceArr=["Export","Import","Both"];
    	$scope.btnGrp.TypeArr=["Invoice","Credit Note","All"];
	    $scope.btnGrpFilter.TypeArr=["Customer","Agent","All"];
	    
        /*PAGE CHANGE FUNCTION*/
		$scope.add = function(){
			consolService.goToAdd();
		};
		$scope.edit = function(){
			consolService.goToEdit($scope.consol);
		};
	   
	
	
	
	

	
	$scope.combinedProvisionalCost = function(){
		$scope.consol.tmpProvisionCost = [];
		for(var i=0;i<$scope.consol.shipmentLinkList.length;i++){
			var shipmentLink = $scope.consol.shipmentLinkList[i];
			
			if(shipmentLink.provisionalCostList==undefined || shipmentLink.provisionalCostList==null){
				
			}else{
				for(var j=0;j<shipmentLink.provisionalCostList.length;j++){
					var cost = shipmentLink.provisionalCostList[j];
					cost.shipmentLink = shipmentLink;
					$scope.consol.tmpProvisionCost.push(cost);
				}
			}
			
			
		}
	}
$scope.rowSelect = function(data,index){
	if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_VIEW)){
		$scope.selectedRecordIndex = index;
	     var localRefStateParam = {
			 consolId: data.id
	     };
		 localRefStateParam.status = null;
		 localRefStateParam.totalRecord = $scope.totalRecord;
		 $scope.selectedRecordIndex =  ($scope.page*$scope.limit)+index;
		 $scope.searchDto.selectedPageNumber =  ($scope.page*$scope.limit)+index;
		 localRefStateParam.selectedPageNumber = $scope.searchDto.selectedPageNumber;
	     $state.go("layout.viewNewAirConsol", $scope.searchDataToStateParams(localRefStateParam));	
	}
}



$scope.searchDataToStateParams = function(param, searchData) {
	if(param == undefined || param == null) {
		param = {};
	}
	if(searchData != undefined && searchData != null) {
    		param.recordPerPage = 1;
    		//Sorting Details
			if(searchData.sortByColumn != undefined && searchData.sortByColumn != null) {
        		param.sortByColumn = searchData.sortByColumn; 
        	}
			if(searchData.orderByType != undefined && searchData.orderByType != null) {
        		param.orderByType = searchData.orderByType; 
        	}
			//Shipment Date Between
			if(searchData.shipmentDate != undefined && searchData.shipmentDate != null) {
        		param.shipmentDateStartDate = $rootScope.sendApiStartDateTime(searchData.shipmentDate.startDate); 
        		param.shipmentDateEndDate = $rootScope.sendApiStartDateTime(searchData.shipmentDate.endDate); 
        	}
			if(searchData.mawbDate != undefined && searchData.mawbDate != null) {
        		param.mawbStartDate = $rootScope.sendApiStartDateTime(searchData.mawbDate.startDate); 
        		param.mawbEndDate = $rootScope.sendApiStartDateTime(searchData.mawbDate.endDate); 
        	}
			
			//ETA Date Between
			if(searchData.eta != undefined && searchData.eta != null) {
        		param.etaStartDate = $rootScope.sendApiStartDateTime(searchData.eta.startDate); 
        		param.etaEndDate = $rootScope.sendApiStartDateTime(searchData.eta.endDate); 
        	}
			
			//ETD Date
			if(searchData.etd != undefined && searchData.etd != null) {
        		param.searchLoggedOnStartDate = $rootScope.sendApiStartDateTime(searchData.etd.startDate); 
        		param.searchLoggedOnEndDate = $rootScope.sendApiStartDateTime(searchData.etd.endDate); 
        	}

			if(searchData.importExport != undefined && searchData.importExport != null) {
				param.importExport = searchData.importExport; 
	    	}

			if(searchData.serviceName != undefined && searchData.serviceName != null) {
				param.serviceName = searchData.serviceName = searchData.serviceName; 
	    	}
			if(searchData.consolUid != undefined && searchData.consolUid != null) {
				param.consolUid = searchData.consolUid; 
	    	}
			if(searchData.partyName != undefined && searchData.partyName != null) {
				param.partyName = searchData.partyName; 
	    	}
			if(searchData.masterNo != undefined && searchData.masterNo != null) {
				param.masterNo = searchData.masterNo; 
	    	}
			if(searchData.origin != undefined && searchData.origin != null) {
				param.origin = searchData.origin; 
	    	}

			if(searchData.masterNo != undefined && searchData.masterNo != null) {
				param.masterNo = searchData.masterNo; 
	    	}

			if(searchData.destination != undefined && searchData.destination != null) {
				param.destination = searchData.destination; 
	    	}
			if(searchData.freightTerm != undefined && searchData.freightTerm != null) {
				param.freightTerm = searchData.freightTerm; 
	    	}
			if(searchData.carrier != undefined && searchData.carrier != null) {
				param.carrier = searchData.carrier; 
	    	}
			if(searchData.createdBy != undefined && searchData.createdBy != null) {
				param.createdBy = searchData.createdBy; 
	    	}
			if(searchData.status != undefined && searchData.status != null) {
				param.status = searchData.status; 
	    	}
			if(searchData.sortByColumn != undefined && searchData.sortByColumn != null) {
				param.sortByColumn = searchData.sortByColumn; 
	    	}
			if(searchData.orderByType != undefined && searchData.orderByType != null) {
				param.orderByType = searchData.orderByType; 
	    	}
		
	}

    
	return param;
}
$scope.singlePageNavigation = function(val) {
	
	if($stateParams != undefined && $stateParams != null) {
        if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
            return;
        var stateParameters = {};
        
        $scope.activeSearchDto = {};
        $scope.activeSearchDto.recordPerPage = 1;
        $scope.hidePage = true;

		$scope.activeSearchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
		if($stateParams.importExport != undefined && $stateParams.importExport != null) {
			stateParameters.importExport = $scope.activeSearchDto.importExport = $stateParams.importExport; 
    	}
		$scope.activeSearchDto.shipmentDate = {};
		if($stateParams.shipmentDateStartDate != undefined && $stateParams.shipmentDateStartDate != null) {
			stateParameters.shipmentDateStartDate = $scope.activeSearchDto.shipmentDate.startDate = $stateParams.shipmentDateStartDate; 
    	}
		
		
		
		if($stateParams.shipmentDateEndDate != undefined && $stateParams.shipmentDateEndDate != null) {
			stateParameters.shipmentDateEndDate = $scope.activeSearchDto.shipmentDate.endDate = $stateParams.shipmentDateEndDate; 
    	}
		
		$scope.activeSearchDto.mawbDate = {};
		if($stateParams.mawbStartDate != undefined && $stateParams.mawbStartDate != null) {
			stateParameters.mawbStartDate = $scope.activeSearchDto.mawbDate.startDate = $stateParams.mawbStartDate; 
    	}
		if($stateParams.mawbEndDate != undefined && $stateParams.mawbEndDate != null) {
			stateParameters.mawbEndDate = $scope.activeSearchDto.mawbDate.endDate = $stateParams.mawbEndDate; 
    	}
		

		$scope.activeSearchDto.eta = {};
		if($stateParams.etaStartDate != undefined && $stateParams.etaStartDate != null) {
			stateParameters.etaStartDate = $scope.activeSearchDto.eta.startDate = $stateParams.etaStartDate; 
    	}
		if($stateParams.etaEndDate != undefined && $stateParams.etaEndDate != null) {
			stateParameters.etaEndDate = $scope.activeSearchDto.eta.endDate = $stateParams.etaEndDate; 
    	}
		
		$scope.activeSearchDto.etd = {};
		if($stateParams.etdStartDate != undefined && $stateParams.etdStartDate != null) {
			stateParameters.etdStartDate = $scope.activeSearchDto.etd.startDate = $stateParams.etdStartDate; 
    	}
		if($stateParams.etdEndDate != undefined && $stateParams.etdEndDate != null) {
			stateParameters.etdEndDate = $scope.activeSearchDto.etd.endDate = $stateParams.etdEndDate; 
    	}
	
		if($stateParams.serviceName != undefined && $stateParams.serviceName != null) {
			stateParameters.serviceName = $scope.activeSearchDto.serviceName = $stateParams.serviceName; 
    	}
		if($stateParams.consolUid != undefined && $stateParams.consolUid != null) {
			stateParameters.consolUid = $scope.activeSearchDto.consolUid = $stateParams.consolUid; 
    	}
		if($stateParams.partyName != undefined && $stateParams.partyName != null) {
			stateParameters.partyName = $scope.activeSearchDto.partyName = $stateParams.partyName; 
    	}
		if($stateParams.masterNo != undefined && $stateParams.masterNo != null) {
			stateParameters.masterNo = $scope.activeSearchDto.masterNo = $stateParams.masterNo; 
    	}
		if($stateParams.origin != undefined && $stateParams.origin != null) {
			stateParameters.origin = $scope.activeSearchDto.origin = $stateParams.origin; 
    	}

		if($stateParams.masterNo != undefined && $stateParams.masterNo != null) {
			stateParameters.masterNo = $scope.activeSearchDto.masterNo = $stateParams.masterNo; 
    	}

		if($stateParams.destination != undefined && $stateParams.destination != null) {
			stateParameters.destination = $scope.activeSearchDto.destination = $stateParams.destination; 
    	}
		if($stateParams.freightTerm != undefined && $stateParams.freightTerm != null) {
			stateParameters.freightTerm = $scope.activeSearchDto.freightTerm = $stateParams.freightTerm; 
    	}
		if($stateParams.carrier != undefined && $stateParams.carrier != null) {
			stateParameters.carrier = $scope.activeSearchDto.carrier = $stateParams.carrier; 
    	}
		if($stateParams.createdBy != undefined && $stateParams.createdBy != null) {
			stateParameters.createdBy = $scope.activeSearchDto.createdBy = $stateParams.createdBy; 
    	}
		if($stateParams.status != undefined && $stateParams.status != null) {
			stateParameters.status = $scope.activeSearchDto.status = $stateParams.status; 
    	}
		if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
			stateParameters.sortByColumn = $scope.activeSearchDto.sortByColumn = $stateParams.sortByColumn; 
    	}
		if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
			stateParameters.orderByType = $scope.activeSearchDto.orderByType = $stateParams.orderByType; 
    	}
		if($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
			stateParameters.recordPerPage = $scope.activeSearchDto.recordPerPage = $stateParams.recordPerPage; 
    	}
		stateParameters.isfromAes="false";
		stateParameters.fromState=null;
	} else {
		return;
	}
	ConsolSearch.query($scope.activeSearchDto).$promise.then(function(data, status) {
        $scope.totalRecord = data.responseObject.totalRecord;
        stateParameters.selectedPageNumber = $scope.activeSearchDto.selectedPageNumber;
        stateParameters.consolId = data.responseObject.searchResult[0].id;
        $scope.hidePage = false;
        $state.go("layout.viewNewAirConsol", stateParameters);
    });
}

$scope.goToShipmentView = function (serviceId){
	console.log("State  : ",$state)
	console.log("State Param  : ",$stateParams)
	console.log("servce ",serviceId)
	ShipmentUid.get({
		serviceId :serviceId
	},function(data) {
			if (data.responseCode == 'ERR0') {
				var param = {
						shipmentId:data.responseObject,
						fromState: $state.current.name,
						fromStateParams:JSON.stringify($stateParams)
						
				}
				$state.go("layout.viewCrmShipment", param);
			}
		}, 
		function(error) {
			console.log("Consol get Failed : " + error)
		});
	
}
$scope.setServiceCodeWithShipmentId=function(){
	if($scope.consol.shipmentLinkList!=null && $scope.consol.shipmentLinkList.length>0){
		for(var i=0;i<$scope.consol.shipmentLinkList.length;i++){
			$scope.consol.shipmentLinkList[i].serviceCodeWithShipmentId=$scope.consol.shipmentLinkList[i].service.serviceMaster.serviceCode+'-'+$scope.consol.shipmentLinkList[i].shipmentUid;
		}
	}
}
$scope.viewConsolGetData = function(consolId){

	ConsolGet.get({
		id :consolId
	},function(data) {
			if (data.responseCode == 'ERR0') {
				$scope.consol=data.responseObject;
				$scope.setServiceCodeWithShipmentId();
				$scope.consol.jobStatusChecker=$scope.consol.jobStatus=='Cancelled'?true:false;
				$scope.showDetail = true;
				$scope.showPageNavEdit=true;
				if($stateParams.fromState=='layout.viewCrmShipment'){
					$scope.showPageNavEdit=false;
				}
		       /* $scope.searchDto.selectedPageNumber=0;
		        $scope.totalRecordView = 10;
		       */ 
				if($stateParams.isfromAes=="true"&& ($stateParams.fromState=='layout.addAes' ||$stateParams.fromState=='layout.editAes' )){
					if($stateParams.navAesObj != undefined && $stateParams.navAesObj != null) {
						$scope.activeSearchDto.selectedPageNumber = parseInt($stateParams.navAesObj.selectedPageNumber);
						$scope.totalRecordView = parseInt($stateParams.navAesObj.totalRecord);
						$stateParams.selectedPageNumber=$scope.activeSearchDto.selectedPageNumber ;
						$scope.detailTab = $stateParams.navAesObj.status;
					} else {
						$scope.searchDto.selectedPageNumber = parseInt($stateParams.navAesObj.selectedPageNumber);
						$scope.totalRecord =  parseInt($stateParams.navAesObj.totalRecord);
					}
					
         		}else{
				if($stateParams.status != undefined && $stateParams.status != null && $stateParams.status =="Active") {
					$scope.activeSearchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber);
					$scope.totalRecordView = parseInt($stateParams.totalRecord);
					$scope.detailTab = 'active';
				} else {
					$scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber);
					$scope.totalRecord = parseInt($stateParams.totalRecord);
			    	}
		     	}
		        $scope.hidePage=false;
		     $scope.combinedProvisionalCost($scope.consol);
		        
                if ($scope.consol != undefined && $scope.consol != null && $scope.consol.agent != undefined && $scope.consol.agent != null &&
                    $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                    $rootScope.subTitle = $scope.consol.agent.partyName;
                } else {
                	$rootScope.subTitle = "Unkonwn Agent";
                }
                if($scope.consol != undefined && $scope.consol != null ){
                	if ($scope.consol.agent != undefined && $scope.consol.agent != null &&
                        $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                        $rootScope.subTitle = $scope.consol.agent.partyName;
                    } else {
                    	$rootScope.subTitle = "Unknown Agent";
                    }
             		if($scope.consol.origin != undefined && $scope.consol.origin != null ) {
             			$rootScope.serviceCodeForUnHistory = $scope.consol.origin.portGroupCode;
             		} else {
             			$rootScope.serviceCodeForUnHistory = "";
             		}

              		if($scope.consol.destination != undefined && $scope.consol.destination != null ) {
              			$rootScope.orginAndDestinationUnHistory = $scope.consol.destination.portGroupCode;
              		} else {
             			$rootScope.orginAndDestinationUnHistory = "";
             		}
              
                 } else {
    				$rootScope.subTitle = "Unknown Agent";
    				$rootScope.serviceCodeForUnHistory = "";
    				$rootScope.orginAndDestinationUnHistory = "";
    			}
                $rootScope.unfinishedFormTitle = "Master Shipment View # "+$scope.consol.consolUid;
    			
		        var rHistoryObj = {
	                    'title': 'Master Shipment View # ' +  $scope.consol.consolUid,
	                    'subTitle': $rootScope.subTitle,
	                    'stateName': $state.current.name,
	                    'stateParam': JSON.stringify($stateParams),
	                    'stateCategory': 'Master Shipment',
	                    'serviceType': 'AIR',
	                    'serviceCode': $rootScope.serviceCodeForUnHistory,
                        'orginAndDestination':"("+$rootScope.orginAndDestinationUnHistory+")",
                	    'showService':true
	                }

	                RecentHistorySaveService.form(rHistoryObj);
			} else {
				$scope.consol=null;
				$rootScope.unfinishedFormTitle = "";
				
			}
		}, 
		function(error) {
			console.log("Consol get Failed : " + error)
			$scope.consol=null;
		});
	
	localStorage.mawb=true;
	$scope.showDetail=true;
	$scope.showDocumentActions=false;
}


$scope.viewConsolUidGetData = function(consolUid){

	ConsolGetByUid.get({
		consolUid :consolUid
	},function(data) {
			if (data.responseCode == 'ERR0') {
				$scope.consol=data.responseObject;
				$scope.consol.jobStatusChecker=$scope.consol.jobStatus=='Cancelled'?true:false;
				$scope.showDetail = true;
				$scope.showPageNavEdit=true;
				if($stateParams.fromState=='layout.viewCrmShipment'){
					$scope.showPageNavEdit=false;
				}
		       /* $scope.searchDto.selectedPageNumber=0;
		        $scope.totalRecordView = 10;
		       */ 
				if($stateParams.isfromAes=="true"&& ($stateParams.fromState=='layout.addAes' ||$stateParams.fromState=='layout.editAes' )){
					if($stateParams.navAesObj != undefined && $stateParams.navAesObj != null) {
						$scope.activeSearchDto.selectedPageNumber = parseInt($stateParams.navAesObj.selectedPageNumber);
						$scope.totalRecordView = parseInt($stateParams.navAesObj.totalRecord);
						$stateParams.selectedPageNumber=$scope.activeSearchDto.selectedPageNumber ;
						$scope.detailTab = $stateParams.navAesObj.status;
					} else {
						$scope.searchDto.selectedPageNumber = parseInt($stateParams.navAesObj.selectedPageNumber);
						$scope.totalRecord =  parseInt($stateParams.navAesObj.totalRecord);
					}
					
         		}else{
				if($stateParams.status != undefined && $stateParams.status != null && $stateParams.status =="Active") {
					$scope.activeSearchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber);
					$scope.totalRecordView = parseInt($stateParams.totalRecord);
					$scope.detailTab = 'active';
				} else {
					$scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber);
					$scope.totalRecord = parseInt($stateParams.totalRecord);
			    	}
		     	}
		        $scope.hidePage=false;
		     $scope.combinedProvisionalCost($scope.consol);
		        
                if ($scope.consol != undefined && $scope.consol != null && $scope.consol.agent != undefined && $scope.consol.agent != null &&
                    $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                    $rootScope.subTitle = $scope.consol.agent.partyName;
                } else {
                	$rootScope.subTitle = "Unknown Agent";
                }
                if($scope.consol != undefined && $scope.consol != null ){
                	if ($scope.consol.agent != undefined && $scope.consol.agent != null &&
                        $scope.consol.agent.partyName != undefined && $scope.consol.agent.partyName != null) {
                        $rootScope.subTitle = $scope.consol.agent.partyName;
                    } else {
                    	$rootScope.subTitle = "Unknown Agent";
                    }
             		if($scope.consol.origin != undefined && $scope.consol.origin != null ) {
             			$rootScope.serviceCodeForUnHistory = $scope.consol.origin.portGroupCode;
             		} else {
             			$rootScope.serviceCodeForUnHistory = "";
             		}

              		if($scope.consol.destination != undefined && $scope.consol.destination != null ) {
              			$rootScope.orginAndDestinationUnHistory = $scope.consol.destination.portGroupCode;
              		} else {
             			$rootScope.orginAndDestinationUnHistory = "";
             		}
              
                 } else {
    				$rootScope.subTitle = "Unknown Agent";
    				$rootScope.serviceCodeForUnHistory = "";
    				$rootScope.orginAndDestinationUnHistory = "";
    			}
                $rootScope.unfinishedFormTitle = "Master Shipment View # "+$scope.consol.consolUid;
    			
		        var rHistoryObj = {
	                    'title': 'Master Shipment View # ' +  $scope.consol.consolUid,
	                    'subTitle': $rootScope.subTitle,
	                    'stateName': $state.current.name,
	                    'stateParam': JSON.stringify($stateParams),
	                    'stateCategory': 'Master Shipment',
	                    'serviceType': 'AIR',
	                    'serviceCode': $rootScope.serviceCodeForUnHistory,
                        'orginAndDestination':"("+$rootScope.orginAndDestinationUnHistory+")",
                	    'showService':true
	                }

	                RecentHistorySaveService.form(rHistoryObj);
			} else {
				$scope.consol=null;
				$rootScope.unfinishedFormTitle = "";
				
			}
		}, 
		function(error) {
			console.log("Consol get Failed : " + error)
			$scope.consol=null;
		});
	
	localStorage.mawb=true;
	$scope.showDetail=true;
	$scope.showDocumentActions=false;
}
   
    
    
    
$scope.tmpsearch=function(val){
	
		$scope.tmpSearchDto = $scope.detailTab=='active'?$scope.activeSearchDto:$scope.searchDto;
		
		var x = $scope.tmpSearchDto.selectedPageNumber+val;
		var y = $scope.totalRecord;

		if( (x >= y) || (x<0) )
			return;
		
		$scope.tmpSearchDto.selectedPageNumber = $scope.tmpSearchDto.selectedPageNumber+val;
		$scope.tmpSearchDto.recordPerPage = 1;
		$scope.hidePage=true;
	    ConsolSearch.query($scope.tmpSearchDto).$promise.then(function(
				data, status) {

	    	$scope.totalRecordView = data.responseObject.totalRecord;
			if(data.responseObject.searchResult!=null && data.responseObject.searchResult.length!=0){
				ConsolGet.get({
					id :data.responseObject.searchResult[0].id
				},function(data) {
					
		   			if (data.responseCode == 'ERR0') {
		   				$scope.consol=data.responseObject;
		   				$scope.hidePage=false;
		   				
		   				/*$scope.attachConfig.data = [];
		   				$scope.populateAttachment($scope.attachConfig.data);*/
		   				
		   			} else {
		   				$scope.consol=null;
		   			}
		   		}, 
		   		function(error) {
		   			console.log("Consol get Failed : " + error)
		   			$scope.consol=null;
		   		});
				
				
				
			}
				
			
		});
	}
    
    $scope.before=function(){
    	$scope.afterbefore="BEFORE";
    	$scope.tmpsearch(-1);
    	
    }
    $scope.after=function(index){
    	$scope.afterbefore="AFTER";
    	$scope.tmpsearch(1)	
    }
    
    
    
	
	
    
    
    
    $scope.backConsol = function(){
    		if($stateParams.fromState != undefined && $stateParams.fromState != null 
    			&& $stateParams.fromStateParams != undefined && $stateParams.fromStateParams != null) {
        		$state.go( $stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        	} else{
    			$state.go('layout.airNewConsol');
    		}
	}
    
    
    
    $scope.markAsCancelled = function() {
    	
    	var searchDto = {};
    	searchDto.masterUid = $scope.consol.consolUid;
    	searchDto.serviceUidList = [];
    	if($scope.consol != null && $scope.consol.shipmentLinkList != undefined && $scope.consol.shipmentLinkList != null && $scope.consol.shipmentLinkList.length > 0) {
    		for(var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
    			searchDto.serviceUidList.push($scope.consol.shipmentLinkList[i].id);
    		}
     	}
    	


		if($scope.consol.jobStatusChecker == true) {
			var newScope = $scope.$new();
			 newScope.errorMessage =$rootScope.nls["ERR222"]
			
			 ngDialog.openConfirm({
				template: '<p>{{errorMessage}}</p>' +
				'<div class="ngdialog-footer">' +
					'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
					'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
				'</div>',
				plain: true,
				scope:newScope,
				className: 'ngdialog-theme-default'
			 }).then(function (value) {
				if(value == 1 ) { 
					$scope.saveConsolShipmentStatus("Cancelled");
				}else{
					$scope.consol.jobStatusChecker = false;
				}
			 })
		}
	
		
    	/*GetInvoiceCountByMasterUIDorServiceUID.count(searchDto).$promise.then(function(data) {
    		if (data.responseCode == 'ERR0') {
    			console.log("Number of Invoice : ", data.responseObject);
        		
        		var numberOfInvoice = data.responseObject;
        		
        		if(numberOfInvoice != undefined && numberOfInvoice != null && numberOfInvoice > 0) {
        			
        				
        			console.log($rootScope.nls["ERR96540"]);
		    	    
        			Notification.error($rootScope.nls["ERR96540"]);
		    	    
        			$scope.consol.jobStatusChecker = false;
		    	    
        			return false;
        			
        		} else {
        			
        		}
    		} else {
    			console.log(data.responseDescription);
    		}
    	}, function(error) {
    		console.log(error);
    	});*/
    }
    
	
    $scope.consolStatusCancelled=function(){
    	
    	ConsolInvoiceList.get(
    			{
    				"consolUid" : $scope.consol.consolUid
    			},
    			function(data) {
    				if (data.responseCode == 'ERR0') {
    					console.log(" Successfull",data.responseObject);
    					$scope.invoiceList=data.responseObject;
    					if($scope.invoiceList !=null && $scope.invoiceList !=undefined && $scope.invoiceList.length>0)
    		    		{
    		    		 console.log("Can't mark as cancelled because master shipment having invoice");
    		    	     $rootScope.clientMessage =  $rootScope.nls["ERR96540"];
    		    	     $scope.consol.jobStatusChecker=false;
    		    	     return false;
    		    		}
    					else
    						{
    						if($scope.consol.jobStatusChecker==true){
    							var newScope = $scope.$new();
    							 newScope.errorMessage =$rootScope.nls["ERR222"]
    							ngDialog.openConfirm({
    								template: '<p>{{errorMessage}}</p>' +
    								'<div class="ngdialog-footer">' +
    									'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
    									'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
    								'</div>',
    								plain: true,
    								scope:newScope,
    								className: 'ngdialog-theme-default'
    							}).then(function (value) {
    								if(value == 1 ) { 
    									$scope.saveConsolShipmentStatus("Cancelled");
    								}else{
    									$scope.consol.jobStatusChecker=false;
    								}
    								})
    						}
    						}
    					
    				} else {
    					console.log(" Failed " + data.responseDescription);
    					$scope.invoiceList=[];
    				}
    			},
    			function(error) {
    				console.log(" Failed : " + error);
    				$scope.invoiceList=[];
    			});
	}
    
    $scope.saveConsolShipmentStatus = function(status) {
		console.log("Save statusMethod is called.");
		$scope.consolDto = {};
		$scope.consolDto.consolId = $scope.consol.id;
		$scope.consolDto.consolStatus =status;
		ConsolStatusChange.status($scope.consolDto).$promise
			.then(function(data) {
			if (data.responseCode == 'ERR0') {
				$scope.consol.jobStatusChecker=true;
				var param={consolId:$scope.consol.id,fromStateParams : JSON.stringify($stateParams)};
				$state.go("layout.viewNewAirConsol",param);
			} else {
				console.log("Service added Failed " , data.responseDescription)
			}
			angular.element(".panel-body").animate({scrollTop: 0}, "slow");
		}, function(error) {
			console.log("Service added Failed : " , error)
		});
	
	}
    
    
    // Start auto import
    
    
    	$scope.confirmEdiAndCompleteImportProcess = function(){
    		
    		console.log("confirmEdiAndCompleteImportProcess ");
    		
    		if($scope.consol.airlineEdi == undefined || $scope.consol.airlineEdi == null || $scope.consol.airlineEdi.id == null) {
    			Notification.warning($rootScope.nls["ERR264"])
    			$scope.consol.isJobCompleted = false;
    		} else {
    			 $scope.autoImportProcessCompleted();
    		}
    	}
    
    $scope.autoImportProcessCompleted=function(){
    	if($scope.consol.isJobCompleted==true){
			var newScope = $scope.$new();
			 newScope.errorMessage =$rootScope.nls["ERR223"];
			ngDialog.openConfirm({
				template: '<p>{{errorMessage}}</p>' +
				'<div class="ngdialog-footer">' +
					'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
					'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
				'</div>',
				plain: true,
				scope:newScope,
				className: 'ngdialog-theme-default'
			}).then(function (value) {
				if(value == 1 ) { 
					$scope.processAutoImport("Completed");
				}else{
					$scope.consol.isJobCompleted=false;
				}
				})
			
		}
		
	}
    
    $scope.processAutoImport = function(status) {
		console.log("ProcessAutoImport method is called.");
		$scope.consolDto = {};
		$scope.consolDto.consolId = $scope.consol.id;
		$scope.consolDto.consolStatus =status;
		AutoImportProcess.process($scope.consolDto).$promise
			.then(function(data) {
			if (data.responseCode == 'ERR0') {
				//$scope.consol = data.responseObject;
				Notification.success($rootScope.nls["ERR96546"]);
				$scope.consol.isJobCompleted=true;
			} else {
				console.log("Service added Failed " , data.responseDescription)
				$scope.consol.isJobCompleted=false;
			}
			angular.element(".panel-body").animate({scrollTop: 0}, "slow");
		}, function(error) {
			console.log("Service added Failed : " , error);
			$scope.consol.isJobCompleted=false;
		});
	}
    // End auto import
    
     $scope.serialNo = function(index) {
   		var	index = index + 1;
   			return index;
   		}
     
     
     
     
     
     
     
     // Report related code start here**********
     
    // $scope.downloadOption = "Print";
     $scope.downloadFileType = "PDF";
     $scope.generate = {};
     $scope.generate.typeArr=["Download","Preview", "Print","eMail"];
     
     $scope.reportModal = function(consol,resourceRefId, reportRefName,key) {
    	 if($scope.reportList!=undefined && $scope.reportList!=null){
	    		$scope.allRepoVarModel.allReportSelected=false;
	    		$scope.isGenerateAll=false;
	    		for ( var i = 0; i < $scope.reportList.length; i++) {
				    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
				}	
	    	}
	    	 
	    	 if(key=='mawb_label'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_MAWB_LABEL_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
    	     
	    	 if(key=='security_endorsement_letter'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_SECURITY_ENDORSEMENT_LETTER_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 
	    	 if(key=='export_job_card'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_EXPORT_JOB_CARD_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 
	    	 if(key=='cargo_manifest' && reportRefName=="CARGO_MANIFEST"){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_CARGO_MANIFEST_DOWNLOAD)){
	    			 return;
	    		 }
	    		 $scope.genReport(resourceRefId,reportRefName);
	    	 }
    	 
	    	 if(key=='security_declaration'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_SECURITY_DECLARATION_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 
	    	 if(key=='air_cargo_load_plan'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_AIR_CARGO_LOAD_PLAN_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 
	    	 if(key=='job_card'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_JOB_CARD_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 
	    	 if(key=='pickup_delivery_order'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_PICKUP_DELIVERY_ORDER_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 if(key=='jobcost_sheet'){
	    		 if(!$rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_JOBCOST_SHEET_DOWNLOAD)){
	    			 return;
	    		 }
	    	 }
	    	 if(reportRefName=='ALERT_TO_AGENT'){
	    			$rootScope.mainpreloder = false;
	    			$scope.alertToAgentPopUp();
	    	 }

			
             if(reportRefName =='CARGO_MANIFEST'){
            	$scope.changeReportDownloadFormat("PDF");
  				$scope.dataResourceId = resourceRefId;
  				$scope.dataReportName = reportRefName;
  				var reportOtherModal = $modal({
  					scope : $scope,
  					templateUrl : 'app/components/report/report_opt.html',
  					show : false
  				});
  				$rootScope.mainpreloder = false;
  				reportOtherModal.$promise.then(reportOtherModal.hide);
  			
              }else{
            	  $scope.changeReportDownloadFormat("PDF");
            	  $scope.dataResourceId = resourceRefId;
            	  $scope.dataReportName = reportRefName;
            	  var reportOtherModal = $modal({
            		  scope : $scope,
            		  templateUrl : 'app/components/report/report_opt.html',
            		  show : false
             });
            	  $rootScope.mainpreloder = false;
            	  reportOtherModal.$promise.then(reportOtherModal.show);
              }
              }
		
     
   //used to pop the modal for alert 
     $scope.alertToAgentPopUp=function(){
    	 $rootScope.mainpreloder = true;
  	   $http({
             method: 'GET',
             url: $rootScope.userProfile.reportServerBasePath+'/api/v1/preAlert/reportlength',
             params:{'consolUid' : $scope.consol.consolUid}
         }).success(function(data){
        	 if(data!=null ){
        		 $scope.reportEmailRequestDto={};
        		 $scope.reportEmailRequestDto=data;
        		// $scope.reportEmailRequestDto.reportAttachList=[];
        		// $scope.reportEmailRequestDto.reportAttachList=data
        		 $scope.reportEmailRequestDto.subject='Please find Attachment';
        		// $scope.reportEmailRequestDto.toEmailIdList=[];
        		 //$scope.reportEmailRequestDto.ccEmailAddress=[];
        		  reportModal.$promise.then(reportModal.hide);
        		  var emailSep;
        		  if($rootScope.appMasterData['single.text.split.by.multiple.email']!=null){
        			  emailSep=$rootScope.appMasterData['single.text.split.by.multiple.email'];
        		  }else{
        			  emailSep=';';
        		  }
        		  $rootScope.mainpreloder = false;
        		  $scope.alertAgentPopUp = $modal({scope: $scope, backdrop:'static',templateUrl: '/app/components/crm/new-shipment/dialog/shipment_report_alerttoagent_popup.html', show: false});
        		  Notification.info("Please use" +emailSep+ "for multiple emails");
        		  $scope.alertAgentPopUp.$promise.then($scope.alertAgentPopUp.show); 		 
        	 }else{
        		 $rootScope.mainpreloder = false;
        		 Notification.error("No Reports For found");
        	 }
             // With the data succesfully returned, call our callback
         }).error(function(){
        	 $rootScope.mainpreloder = false;
         });
     }
     
     
     $scope.emailValidate =function(code){
    	 if(code ==0 ){
    		          if($scope.reportEmailRequestDto!=null && $scope.reportEmailRequestDto!=undefined){
    		        	  if($scope.reportEmailRequestDto.toEmailIdList == undefined || $scope.reportEmailRequestDto.toEmailIdList == null || $scope.reportEmailRequestDto.toEmailIdList == ""){
	    	     				Notification.error($rootScope.nls["ERR269"]);
    	    	 				return false;
    	    	 			}else if(!CommonValidationService.checkMultipleMail($scope.reportEmailRequestDto.toEmailIdList)){
	    	     				Notification.error($rootScope.nls["ERR270"]);
    	    	 					return false;
    	    	 			}
    		          }
    		          if($scope.reportEmailRequestDto.ccEmailIdList == undefined || $scope.reportEmailRequestDto.ccEmailIdList == null || $scope.reportEmailRequestDto.ccEmailIdList == ""){
    		        	 
    		          }else {
    		        	  if(!CommonValidationService.checkMultipleMail($scope.reportEmailRequestDto.ccEmailIdList)){
	    	     				Notification.error($rootScope.nls["ERR271"]);
    		 					return false;
    		 			}
    		          }
    	 }
    	 return true;
     }
    
     $scope.sendEmailToAgent=function(){
    	 if( $scope.emailValidate(0)){
    		 $rootScope.mainpreloder = true;
    	    	$scope.reportEmailRequestDto.consolUid=$scope.consol.consolUid;
    	    	 $http({
    				  method: 'POST',
    		             url: $rootScope.userProfile.reportServerBasePath+'/api/v1/preAlert/mailtoagent',
    	    			 data: $scope.reportEmailRequestDto
    				}).then(
    				  function (response) {
    					  if(response.data.responseCode==='ERR0'){
    						  Notification.success($rootScope.nls["ERR272"])
    						}else{
    							 Notification.error($rootScope.nls["ERR273"])
    						}
    				  }, function (error) {
    					  Notification.error($rootScope.nls["ERR273"])
    				  });
    	    	  Notification.success($rootScope.nls["ERR274"]);
    	    	  $scope.alertAgentPopUp.$promise.then($scope.alertAgentPopUp.hide); 
    	    	  $rootScope.mainpreloder = false;
    	 }
     }

		
		
       $scope.genReport=function(resourceRefId,reportRefName){
			$scope.popupValues={};
			$scope.popupValues.showVolume='No';
			$scope.popupValues.showChargeWeight='No';
			$scope.popupValues.showShipperName='Yes';
			$scope.popupValues.showShipperAddress='No';
			$scope.popupValues.showNote='No';
			$scope.popupValues.showCharge='No';
			$scope.popupValues.showLogo='No';
			
			console.log("Service ID - ",resourceRefId," -- Name --",reportRefName);
		    $scope.changeReportDownloadFormat("PDF");
			$scope.dataResourceId = resourceRefId;
			$scope.dataReportName = reportRefName;
			$scope.myOtherModal = $modal({scope: $scope, backdrop:'static',templateUrl: '/app/components/air/consol_shipments_new_screen/views/cargo_manifest_popup.html', show: false});
			$scope.myOtherModal.$promise.then($scope.myOtherModal.show);
		}
       
       $scope.showReportParameter=function(consolId,popupValues,downloadOption){
    	   $scope.downloadOption=downloadOption;
			var reportDownloadRequest = {};
				reportDownloadRequest = {
				resourceId : consolId,
				downloadOption : $scope.downloadOption,// "Download","Preview","Print"
				downloadFileType : $scope.downloadFileType,//PDF
				reportName : "CARGO_MANIFEST", 
				showVolume : popupValues.showVolume,
				showCfs : popupValues.showCfs,
				showChargeWeight : popupValues.showChargeWeight,
				showShipperName : popupValues.showShipperName,
				showShipperAddress : popupValues.showShipperAddress,
				showNote : popupValues.showNote,
				showCharge : popupValues.showCharge,
				showLogo : popupValues.showLogo,
				single : true
				
			};
				$rootScope.mainpreloder = true;
				downloadFactory.download(reportDownloadRequest);
		}
		
       
       
       	$scope.isSingle=false;
		$scope.isMultiple=false;
		
		$scope.changeReportDownloadFormat = function(dType) {
			console.log("Download type Changed :: ",dType);
			$scope.downloadFileType = dType;
			if(dType === 'PDF') {
				$scope.generate.TypeArr=["Download","Preview","Print"];
			} else if(dType === 'eMail'){
				$scope.generate.TypeArr=["eMail"];
			}
			else  {
				$scope.generate.TypeArr=["Download"];
			}
		}
		

			
	    $scope.documentIdList = [];
	    $scope.reportList = [];
	    $scope.reportDetailData = {};
	    
	    var allRepoVar = {};
		$scope.allRepoVarModel = {};
		$scope.allRepoVarModel.allReportSelected = false;
		$scope.reportLoadingImage = {};
		$scope.reportLoadingImage.status = false;
		
	    $scope.reportListMaking = function(consol,resourceRefId){
	    if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_VIEW)) {
	    	
	    	$rootScope.mainpreloder = true;
				$scope.reportList = [];
				$scope.moreAddressTab='reports';
				$scope.assignDefaultReports(consol,resourceRefId);
				consolShipmentDocumentIdList.get({
					serviceId : resourceRefId
				},function(data) {
		   			if (data.responseCode == 'ERR0') {
						$scope.documentIdList  = data.responseObject;
						console.log("$scope.documentIdList -- ",$scope.documentIdList);
						$rootScope.mainpreloder = false;	
		   			} 
		   		}, 
		   		function(error) {
		   			console.log("Shipment get Failed : " + error) 
		   			$rootScope.mainpreloder = false;
		   		});
	    	
	    } else {
	    		console.log("Rights are not there to perform this action!.");
	    	} 
	    }
		
		$scope.assignDefaultReports = function(consol,resourceRefId){
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "SHIPMENT_MAWB", reportDisplayName : 'MAWB',key : "mawb"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "MAWB_LABEL", reportDisplayName : 'MAWB Label',key : "mawb_label"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "SECURITY_ENDORSEMENT_LETTER", reportDisplayName : 'Security Endorsement Letter',key : "security_endorsement_letter"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "EXPORT_JOB_CARD", reportDisplayName : 'Export Job Card(MAWB)',key : "export_job_card"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Export' && (consol.directShipment == 'No'  || consol.directShipment == false)){
			   $scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "CARGO_MANIFEST", reportDisplayName : 'Cargo Manifest(AIR EXPORT)',key : "cargo_manifest"};
			   $scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
		           $scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "SECURITY_DECLARATION", reportDisplayName : 'Security Declaration',key : "security_declaration"};
		           $scope.reportList.push($scope.reportDetailData );
				}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
		           $scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "AIR_CARGO_LOAD_PLAN", reportDisplayName : 'Air Cargo Load Plan',key : "air_cargo_load_plan"};
		           $scope.reportList.push($scope.reportDetailData );
				}
			if(consol.serviceMaster.importExport=='Import' || consol.serviceMaster.importExport=='IMPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "MASTER_DO", reportDisplayName : 'Delivery Order',key : "delivery_order"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Import' || consol.serviceMaster.importExport=='IMPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "JOB_CARD", reportDisplayName : 'Job card',key : "job_card"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Import' || consol.serviceMaster.importExport=='IMPORT'){
				$scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "PICKUP_DELIVERY_ORDER", reportDisplayName : 'Pickup Delivery Order',key : "pickup_delivery_order"};
				$scope.reportList.push($scope.reportDetailData );
			}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
		           $scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "JOBCOST_SHEET", reportDisplayName : 'Consol Profitability',key : "jobcost_sheet"};
		           $scope.reportList.push($scope.reportDetailData );
				}
			if(consol.serviceMaster.importExport=='Export' || consol.serviceMaster.importExport=='EXPORT'){
		           $scope.reportDetailData = {resourceRefId:resourceRefId, isDocument:false,isService:true,documentNo:'', reportName : "ALERT_TO_AGENT", reportDisplayName : 'Alert To Agent',key : "Alert To Agent"};
		           $scope.reportList.push($scope.reportDetailData );
				}
			
			}
		
		
		 var reportModal;
		    $scope.reportviewmodel = function() {
		     reportModal= $modal({
		         scope: $scope,
		         backdrop: 'static',
		         templateUrl: 'app/components/air/consol_shipments_new_screen/dialog/report.html',
		         show: false
		     });
		     reportModal.$promise.then(reportModal.show)
		     $scope.reportListMaking($scope.consol,$scope.consol.id);
		    };
		
		
		
		$scope.selectAllReports = function() {
			for ( var i = 0; i < $scope.reportList.length; i++) {
			    $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
			}
			if(!$scope.allRepoVarModel.allReportSelected){
				$scope.isGenerateAll=false;
			}
		}
		
		
		$scope.chkReportStatus = function(chkFlag, detObj) {
			console.log("Changed Status :: ",chkFlag, "  -----------  ",detObj);
		}

		$scope.isSingle=false;
		$scope.isMultiple=false;
		
		$scope.isGenerateAll=false;
		
		$scope.generateAll=function(){
			$scope.dataReportName=null;
			var flag=false;
			for ( var i = 0; i < $scope.reportList.length; i++) {
				if($scope.reportList[i].isSelected) {
					flag=true
					break;
				}
			}if(!flag){
				$scope.isGenerateAll=false;
				Notification.warning("Please select atleast one.......)")
			}
			else{
				$scope.isGenerateAll=true;
				$scope.reportData={};
				var myOtherModal={};
				$scope.myOtherModal = $modal({
					scope : $scope,
					templateUrl : 'app/components/report/report_opt.html',
					show : false
				});
				 $rootScope.mainpreloder = false;
				//myOtherModal.$promise.then(myOtherModal.show);
				 $scope.myOtherModal.$promise.then($scope.myOtherModal.show);
			}
		}
		
		
		$scope.generateAllReports = function(){
			if($scope.reportList==undefined || $scope.reportList==null || $scope.reportList.length==0){
				
			}else{
				//Using for mail sending for download reports 
				$scope.emailReportList= [];
				for ( var i = 0; i < $scope.reportList.length; i++) {
					if($scope.reportList[i].isSelected) {
						$scope.emailReport={};
						$scope.emailReport.resourceId=$scope.reportList[i].resourceRefId;
						if($scope.reportList[i].reportName!=undefined){
							$scope.emailReport.reportName=$scope.reportList[i].reportName;
						}else{
							$scope.emailReport.reportName="";
						}
						$scope.emailReport.downloadOption=$scope.downloadOption;
						$scope.emailReport.downloadFileType=$scope.downloadFileType;
						$scope.emailReportList.push($scope.emailReport);
					}
			}
			
			}
		if($scope.emailReportList!=undefined && $scope.emailReportList.length>0){
			$scope.isMultiple=true;
			$scope.isSingle=false;
		}else{
			$scope.isSingle=true;
			$scope.isMultiple=false;
		}	
		var flag=false;
		if($scope.isSingle){	
			for ( var i = 0; i < $scope.reportList.length; i++) {
				if($scope.reportList[i].isSelected) {
					flag=true
				   $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
					$timeout(function(){
						$scope.reportLoadingImage.status = false;
					},1000);
				}
				if(flag){
					break;
				}
			}
			if(!flag){
				Notification.warning("Please select atleast one.......)");
				return ;
			}
		}else if($scope.isMultiple){
			$scope.downloadGenericCallMultiple($scope.emailReportList,$scope.downloadOption,$scope.isMultiple,"PDF",$scope.reportList[0].resourceRefId)
			console.log("Nothig selected");
			}
		}
		
		
		$scope.downloadGenericCall = function(id, downOption, repoName) {
			$scope.downloadOption=downOption;
			if($scope.isGenerateAll){
				$scope.generateAllReports();
			}else{
				var reportDownloadRequest = {
						resourceId : id,
						downloadOption : downOption,// "Download","Preview","Print"
						downloadFileType : $scope.downloadFileType,//PDF
						reportName : repoName, 
						single : true,
					};
					$rootScope.mainpreloder = true;
					downloadFactory.download(reportDownloadRequest);
			}
			

		}
		
		
		$scope.downloadGenericCallMultiple=function(emailReportList,downOption,multiple,type,id){
			var reportDownloadRequest = {
					emailReportList:emailReportList,
					downloadOption : downOption,// "Download","Preview","Print"
					//downloadOption : $scope.downOption,
					downloadFileType : type,//PDF
					isMultiple : multiple,
					resourceId:id,
					screenType :'Consol'
			};
			if(downOption == 'eMail'){
				$rootScope.mainpreloder = true;
				downloadMultipleFactory.download(reportDownloadRequest);
				if(downOption === 'eMail'){
				$timeout(function(){
					$rootScope.mainpreloder = false;
				},5000);
				}
			}else{
				$rootScope.mainpreloder = false;
				for ( var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
					var reportFileName ='';
					reportFileName =reportDownloadRequest.emailReportList[i].reportName;
					var reportDownloaddRequest = {
							resourceId :id,
							downloadOption : downOption,// "Download","Preview","Print"
							downloadFileType : $scope.downloadFileType,//PDF
							reportName : reportFileName, 
							single : true
						};
					$rootScope.mainpreloder = true;
					downloadFactory.download(reportDownloaddRequest);
				}
				}
			}
		//Report Related code End Here
		
		
		
    /*ATTACHMENT TAB CONFI*/
	$scope.attachConfig = {
		"isEdit":false,  //false for view only mode
		"isDelete":true, //
		"page":"shipment",
		"columnDefs":[
						{"name":"Document Name", "model":"documentMaster.documentName","type":"text"},
						{"name":"Reference #", "model":"refNo","type":"text"},
						{"name":"Customs", "model":"isCustoms","type":"text"},
						{"name":"Protected", "model":"isProtected","type":"text"},
						{"name":"File Name", "model":"fileName","type":"file"}
					]
	};
	/*
	$scope.populateAttachment = function(data) {
			
			console.log("Populating Attachment....");
			
			if($scope.consol.consolAttachmentList != null && $scope.consol.consolAttachmentList.length != 0){
				
				console.log("There are " + $scope.consol.consolAttachmentList.length + " attachments.....");
						
						for(var i = 0; i < $scope.consol.consolAttachmentList.length; i++) {
							var displayObject = new Object();
							
							displayObject.id =  $scope.consol.consolAttachmentList[i].id;
							displayObject.refNo = $scope.consol.consolAttachmentList[i].refNo;
							displayObject.documentMaster = $scope.consol.consolAttachmentList[i].documentMaster;
							displayObject.customs = $scope.consol.consolAttachmentList[i].customs;
							
							displayObject.unprotectedFileName =   $scope.consol.consolAttachmentList[i].unprotectedFileName;
							displayObject.unprotectedFileContentType =  $scope.consol.consolAttachmentList[i].unprotectedFileContentType;
							displayObject.unprotectedFile =  $scope.consol.consolAttachmentList[i].unprotectedFile;
							
							displayObject.protectedFileName =   $scope.consol.consolAttachmentList[i].protectedFileName;
							displayObject.protectedFileContentType =  $scope.consol.consolAttachmentList[i].protectedFileContentType;
							displayObject.protectedFile = $scope.consol.consolAttachmentList[i].protectedFile;
							
							
							displayObject.systemTrack = $scope.consol.consolAttachmentList[i].systemTrack;
					
							
							data.push(displayObject);
						}
					} else {
						console.log("There is no attachements....");
					}
			
			console.log("Populating Attachment Over.....");
		}

*/
	$scope.downloadAttach = function(param){
		console.log("download ",param);
			
		if(param.data.id!=null && (param.data.file == null)){
			console.log("API CALL")	
			$http({
			    url: $rootScope.baseURL+'/api/v1/consol/files/'+param.data.id+'/'+param.type+'/false',
			    method: "GET",
			    responseType: 'arraybuffer'
			}).success(function (data, status, headers, config) {
				console.log("hiddenElement ",data)
			    var blob = new Blob([data], {});
				console.log("blob ", blob);
				saveAs(blob, param.data.fileName);
			}).error(function (data, status, headers, config) {
			    //upload failed
			});
			

		}
	}
	  //setting for stock to consol navigations 
    $scope.setVisibilities=function(value){
    if(value='fromStock'){
    	$scope.setVisibility=true;
    }
    }
 $scope.backStock=function(){
	 NavigationService.set(null);
 	NavigationService.setLocation(null);
 	NavigationService.setSubData1(null);
         $location.path('/air/stock_management_list');	 
 }
 // preview Mawb document.........


 $scope.range = function(min, max, step) {
	    step = step || 1;
	    var input = [];
	    for (var i = min; i <= max; i += step) {
	    	if($scope.consolClone.chargeList!=undefined &&$scope.consolClone.chargeList[i]!=undefined){
	        input.push($scope.consolClone.chargeList[i]);
	   }
}
	    return input;
 
 }
 
 
 
 
 
 
 
 
 $scope.calculateTotalAmount=function(){
		if($scope.consolDocumentClone.ratePerChargeClone!=undefined && $scope.consolDocumentClone.ratePerChargeClone!=null && $scope.consolDocumentClone.chargebleWeight!=undefined && $scope.consolDocumentClone.chargebleWeight!=null)
		$scope.consolDocumentClone.totalAmountClone=parseFloat($scope.consolDocumentClone.ratePerChargeClone)*parseFloat($scope.consolDocumentClone.chargebleWeight);
		 $scope.consolDocumentClone.totalAmountClone = Math.round($scope.consolDocumentClone.totalAmountClone * 1000) / 1000;
	}
	$scope.caluclatePrepaid=function(){
		if($scope.consolClone.consolDocument.isAgreed=='No' || $scope.consolClone.consolDocument.isAgreed==false){
			$scope.consolDocumentClone.totalPrepaidClone= parseFloat($scope.consolDocumentClone.preWeightChargeClone==null?0:$scope.consolDocumentClone.preWeightChargeClone) + 
			   parseFloat($scope.consolDocumentClone.preValuationCharge==null?0:$scope.consolDocumentClone.preValuationCharge)  +
			   parseFloat($scope.consolDocumentClone.prepaidTax==null?0:$scope.consolDocumentClone.prepaidTax)  +
			   parseFloat($scope.consolDocumentClone.otherChargesDuePreAgent==null?0:$scope.consolDocumentClone.otherChargesDuePreAgent)  +
			   parseFloat($scope.consolDocumentClone.otherChargesDuePreCarrier==null?0:$scope.consolDocumentClone.otherChargesDuePreCarrier) 
		}else{
			$scope.consolDocumentClone.totalPrepaidClone=null;
		}
		 if($scope.consolDocumentClone.totalPrepaidClone==0){
			 
			 $scope.consolDocumentClone.totalPrepaidClone=null;
		 }
	}
	    $scope.caluclateCollect=function(){
	    	if($scope.consolClone.consolDocument.isAgreed=='No' || $scope.consolClone.consolDocument.isAgreed==false){	
	    	$scope.consolDocumentClone.totalCollectClone= parseFloat($scope.consolDocumentClone.collWeightChargeClone==null?0:$scope.consolDocumentClone.collWeightChargeClone) + 
	    	   parseFloat($scope.consolDocumentClone.collValuationCharge==null?0:$scope.consolDocumentClone.collValuationCharge)  +
	    	   parseFloat($scope.consolDocumentClone.collectTax==null?0:$scope.consolDocumentClone.collectTax)  +
	    	   parseFloat($scope.consolDocumentClone.otherChargesDueCollAgent==null?0:$scope.consolDocumentClone.otherChargesDueCollAgent)  +
	    	   parseFloat($scope.consolDocumentClone.otherChargesDueCollCarrier==null?0:$scope.consolDocumentClone.otherChargesDueCollCarrier) 
	    	}else{
	    		$scope.consolDocumentClone.totalCollectClone=null;
	    	}
	    	if($scope.consolDocumentClone.totalCollectClone==0){
		    	
		    	$scope.consolDocumentClone.totalCollectClone=null;
		    }
		}
	    
	    
	    
 $scope.updateList=function(listObject,listKey){
		
		if(listKey=='pod-0'){
			$scope.consolClone.connectionList[0].pod=listObject;
			$scope.validateDynamicMAWBDocument(10);
		}
		if(listKey=='carrier'){
			$scope.consolClone.connectionList[0].carrierMaster=listObject;
			$scope.validateDynamicMAWBDocument(11);
		}
		if(listKey=='pod-1'){
			$scope.consolClone.connectionList[1].pod=listObject;
			$scope.validateDynamicMAWBDocument(12);
		}
		if(listKey=='carrier1'){
			$scope.consolClone.connectionList[1].carrierMaster=listObject;
			$scope.validateDynamicMAWBDocument(13);
		}
		
  	if(listKey=='flightanddate'){
		    var res = listObject.split("/");
			$scope.consolClone.connectionList[0].flightVoyageNo=res[0];
			$scope.consolClone.connectionList[0].eta=res[1];
		}

}
 
 $scope.agreedChange = function(){
		if($scope.consolClone.consolDocument.isAgreed || $scope.consolClone.consolDocument.isAgreed=='Yes'){
			$scope.consolDocumentClone.preWeightChargeClone=null;
			 $scope.consolDocumentClone.totalPrepaidClone=null;
			 $scope.consolDocumentClone.collWeightChargeClone=null;
			 $scope.consolDocumentClone.totalCollectClone=null;  
				$scope.consolClone.chargeListClone=[];
				$scope.consolClone.chargeListClone= new Array(6);
				$scope.consolDocumentClone.totalAmountClone="AS AGREED";
				if($scope.consolDocumentClone.chgs=='P'){
					 $scope.consolDocumentClone.preWeightChargeClone="AS AGREED";
					 $scope.consolDocumentClone.totalPrepaidClone="AS AGREED";
				 }
				 if($scope.consolDocumentClone.chgs=='C'){
					 $scope.consolDocumentClone.collWeightChargeClone="AS AGREED";
					 $scope.consolDocumentClone.totalCollectClone="AS AGREED";    
				 }
			}
		else{
			 $scope.consolDocumentClone.preWeightChargeClone=null;
			 $scope.consolDocumentClone.totalPrepaidClone=null;
			 $scope.consolDocumentClone.collWeightChargeClone=null;
			 $scope.consolDocumentClone.totalCollectClone=null;    

			 $scope.consolDocumentClone.ratePerChargeClone=$scope.consolDocumentClone.ratePerCharge;
		  $scope.consolClone.preWeightCharge=0.000;
		   $scope.consolClone.collWeightCharge=0.000;
		if($scope.consolClone.chargeList!=null&&$scope.consolClone.chargeList.length>0){
		 for(var c=0;c<$scope.consolClone.chargeList.length;c++){
			   if($scope.consolClone.chargeList[c].ppcc=='Prepaid' || $scope.consolClone.chargeList[c].ppcc==false){
	                $scope.consolDocumentClone.preWeightChargeClone=parseFloat($scope.consolClone.preWeightCharge) + parseFloat($scope.consolClone.chargeList[c].localAmount);
			   }else{
				   $scope.consolDocumentClone.collWeightChargeClone=parseFloat($scope.consolClone.collWeightCharge) + parseFloat($scope.consolClone.chargeList[c].localAmount);  
			   }
		   }
		}
		 $scope.consolClone.chargeListClone= $scope.consolClone.chargeList
		 /*if($scope.consolDocumentClone.chgs=='P'){
			
		 }else{
			 
		 }*/
		 $scope.caluclateCollect();
		 $scope.caluclatePrepaid(); 
		$scope.calculateTotalAmount();
	}
			if($scope.consolClone.isMarksNo==false || $scope.consolClone.isMarksNo=='No'){
		              $scope.consolDocumentClone.marksAndNoClone=null;
			}else{
				$scope.consolDocumentClone.marksAndNoClone=$scope.consolDocumentClone.marksAndNo;
			}
	};
	//Business Methods for preview document 
	
	$scope.getAccounts=function(code){
		
	if(code==0 || code==1){	
		$scope.consolDocumentClone.shipperAccount=null;
	     if($scope.consolDocumentClone.shipper!=undefined &&$scope.consolDocumentClone.shipper!=null && $scope.consolDocumentClone.shipper.partyAccountList!=null && $scope.consolDocumentClone.shipper.partyAccountList.length>0 ){
			
	    	 $scope.consolDocumentClone.shipperAccount= cloneService.clone($scope.consolDocumentClone.shipper.partyAccountList[0].accountMaster.accountCode);
			}else{
				$scope.consolDocumentClone.shipperAccount=null;
			}
	     
	}   
	
	if(code==0 || code==2){	
		$scope.consolDocumentClone.consigneeAccount=null;
			if($scope.consolDocumentClone.consignee!=undefined && $scope.consolDocumentClone.consignee!=null &&$scope.consolDocumentClone.consignee.partyAccountList!=null && $scope.consolDocumentClone.consignee.partyAccountList.length>0 ){
				
				$scope.consolDocumentClone.consigneeAccount=$scope.consolDocumentClone.consignee.partyAccountList[0].accountMaster.accountCode;
				
			}else{
				
				$scope.consolDocumentClone.consigneeAccount=null;
			}
	}		
	
	if(code==0 || code==3){	
		
		$scope.consolDocumentClone.issuingAgentAccount=null;
			
	       if($scope.consolDocumentClone.issuingAgent!=undefined &&$scope.consolDocumentClone.issuingAgent!=null && $scope.consolDocumentClone.issuingAgent.partyAccountList!=null && $scope.consolDocumentClone.issuingAgent.partyAccountList.length>0 ){
				
				$scope.consolDocumentClone.issuingAgentAccount=$scope.consolDocumentClone.issuingAgent.partyAccountList[0].accountMaster.accountCode;
				
			}else{
				
				$scope.consolDocumentClone.issuingAgentAccount=null;
			}
			
		}
	
	
	}
	//Ajax calls for dynamic document
	
	
	// Preview Document Auto complete  Vlaues
	 $scope.ajaxPartyEvent = function(object) {
	    	console.log("Ajax Party Event method : " + object);
	    	$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
						if (data.responseCode =="ERR0"){
							$scope.partyList = data.responseObject.searchResult;
							//$scope.showPartyList=true;
							return $scope.partyList;
						}
	               },
	                function(errResponse){
	                    console.error('Error while fetching Party');
	                }
	       );

	    }
	
	  $scope.ajaxPortOfDocDischarge = function(object) {
   		console.log("ajaxPortOfDischarge ", object);
   	  	$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			return PortByTransportMode.fetch({"transportMode":$scope.consolClone.serviceMaster.transportMode}, $scope.searchDto).$promise.then(
					function(data) {
						if (data.responseCode =="ERR0"){
							$scope.portOfDocDischargeList = [];
							var resultList = data.responseObject.searchResult;
							if(resultList != null && resultList.length != 0) {
								if($scope.consolDocumentClone.pol==undefined || $scope.consolDocumentClone.pol ==null) {
									$scope.portOfDocDischargeList=resultList;
								}
								else
									{
								for(var i = 0; i < resultList.length; i++) {

										if(resultList[i].id != $scope.consolDocumentClone.pol.id) {
										$scope.portOfDocDischargeList.push(resultList[i]);
										}

								}
									}
							}
							return $scope.portOfDocDischargeList;
							//$scope.showPortOfDocDischargeList=true;
						}
		       },
		        function(errResponse){
		            console.error('Error while fetching PortOfDischarge List');
		        }
			);

   }
	  
	  
	  $scope.ajaxPortOfDocDischargeConn = function(object) {
	   		console.log("ajaxPortOfDischarge ", object);
	   	  	$scope.searchDto={};
				$scope.searchDto.keyword=object==null?"":object;
				$scope.searchDto.selectedPageNumber = 0;
				$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
				return PortByTransportMode.fetch({"transportMode":$scope.consolClone.serviceMaster.transportMode}, $scope.searchDto).$promise.then(
						function(data) {
							if (data.responseCode =="ERR0"){
								$scope.portOfDocDischargeList = [];
								var resultList = data.responseObject.searchResult;
								if(resultList != null && resultList.length != 0) {
										$scope.portOfDocDischargeList=resultList;
								}
								return $scope.portOfDocDischargeList;
								//$scope.showPortOfDocDischargeList=true;
							}
			       },
			        function(errResponse){
			            console.error('Error while fetching PortOfDischarge List');
			        }
				);
	   }

	      $scope.ajaxPortOfDocLoading = function(object) {
	  	  		console.log("ajaxPortOfLoadingEvent ", object);
	  	  		$scope.searchDto={};
				$scope.searchDto.keyword=object==null?"":object;
				$scope.searchDto.selectedPageNumber = 0;
				$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

				return PortByTransportMode.fetch({"transportMode":$scope.consolClone.serviceMaster.transportMode}, $scope.searchDto).$promise.then(
						function(data) {

							if (data.responseCode =="ERR0"){
								$scope.portOfDocLoadingList = [];
								var resultList = data.responseObject.searchResult;

								if(resultList != null && resultList.length != 0) {
									if($scope.consolDocumentClone.pod==undefined || $scope.consolDocumentClone.pod ==null) {
										$scope.portOfDocLoadingList=resultList;
									}
									else
										{
									for(var i = 0; i < resultList.length; i++) {

											if(resultList[i].id != $scope.consolDocumentClone.pod.id) {
											$scope.portOfDocLoadingList.push(resultList[i]);
											}

									}
										}
								}
							return $scope.portOfDocLoadingList;
								//$scope.showPortOfDocLoadingList=true;
							}



			       },
			        function(errResponse){
			            console.error('Error while fetching PortOfLoading List');
			        }
				);

	    }
	      
	      $scope.ajaxServiceEvent = function(object) {

        	  	$scope.searchDto={};
    			$scope.searchDto.keyword=object==null?"":object;
    			$scope.searchDto.selectedPageNumber = 0;
    			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

    			return ServiceList.fetch($scope.searchDto).$promise.then(function(data, status) {
        					if (data.responseCode =="ERR0"){
        						$scope.serviceMasterList = data.responseObject.searchResult;
        						console.log("serviceMasterList " , $scope.serviceMasterList);
        						//$scope.showServiceMasterList=true;
								return $scope.serviceMasterList;
        					}
                       },
                        function(errResponse){
                            console.error('Error while fetching ServiceMaster');
                        }
               );

          }

	  	$scope.ajaxCurrencyEvent = function(object){
			$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			
			return CurrencySearchExclude.fetch({"excludeCurrencyCode":$rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode},$scope.searchDto).$promise.then(
					function(data) {
						if (data.responseCode =="ERR0"){
							$scope.currencyList = data.responseObject.searchResult;
							/*$scope.showCurrencyList = true;*/
							return $scope.currencyList;
						}
	               },
	                function(errResponse){
	                    console.error('Error while fetching Currency');
	                }
	       );


		}
	  	
	  	$scope.ajaxOriginEvent = function(object) {

 	  		$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

			return PortGroupList.fetch($scope.searchDto).$promise.then(
					function(data) {
						if (data.responseCode =="ERR0"){
							$scope.originList = [];
							var resultList = data.responseObject.searchResult;

							if(resultList != null && resultList.length != 0) {
								/*if($scope.shipment.destination==undefined || $scope.shipment.destination ==null) {
									$scope.originList=resultList;
								}
								else
									{
								for(var i = 0; i < resultList.length; i++) {

										if(resultList[i].id != $scope.shipment.destination.id) {
										$scope.originList.push(resultList[i]);
										}

								}
									}*/

								$scope.originList=resultList;
							}
							return $scope.originList;
							//$scope.showOriginList=true;
						}



		       },
		        function(errResponse){
		    	   
		            console.error('Error while fetching Origin List');
		        }
			);
   }
	  	
	  	
	  	
	  	
	  	
	  	 $scope.ajaxDestination = function(object) {

	    	  	$scope.searchDto={};
				$scope.searchDto.keyword=object==null?"":object;
				$scope.searchDto.selectedPageNumber = 0;
				$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

				return PortGroupList.fetch($scope.searchDto).$promise.then(
						function(data) {
							if (data.responseCode =="ERR0"){
								$scope.destinationList = [];
								var resultList = data.responseObject.searchResult;

								if(resultList != null && resultList.length != 0) {
									/*if($scope.shipment.origin==undefined || $scope.shipment.origin ==null) {
										$scope.destinationList=resultList;
									}
									else
										{
									for(var i = 0; i < resultList.length; i++) {

											if(resultList[i].id != $scope.shipment.origin.id) {
											$scope.destinationList.push(resultList[i]);
											}

									}
										}*/

									$scope.destinationList=resultList;
								}
								return $scope.destinationList;
								//$scope.showDestinationList=true;
							}
			       },
			        function(errResponse){
			            console.error('Error while fetching Origin List');
			        }
				);

	    }
	  	
	  	  $scope.ajaxCarrierEvent = function(object) {
            	  	$scope.searchDto={};
        			$scope.searchDto.keyword=object==null?"":object;
        			$scope.searchDto.selectedPageNumber = 0;
        			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        			return CarrierByTransportMode.fetch({"transportMode":'Air'}, $scope.searchDto).$promise.then(
        					function(data) {
        						if (data.responseCode =="ERR0"){
        							$scope.carrierMasterList  = data.responseObject.searchResult;
        							//$scope.showCarrierMasterList=true;
								return $scope.carrierMasterList;
        						}
        		       },
        		        function(errResponse){
        		            console.error('Error while fetching Carrier List Based on Air');
        		        }
        			);

          }

	     
	  	  
	  	 $scope.ajaxTosEvent = function(object) {
	     	  console.log("ajaxTosEvent ", object);

	     	  	$scope.searchDto={};
	 			$scope.searchDto.keyword=object==null?"":object;
	 			$scope.searchDto.selectedPageNumber = 0;
	 			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

	     		return TosSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
	     					if (data.responseCode =="ERR0"){
	     						$scope.tosList = data.responseObject.searchResult;
	     						//$scope.showTosList=true;
								return $scope.tosList
	     					}
	     		},
	     			function(errResponse){
	                         console.error('Error while fetching Tos');
	     			}
	            );
			 ;

	       }
	  	  
	  	
	  	 $scope.ajaxDocDestination = function(object) {

	     		console.log("ajaxDestination ", object);

	     	  	$scope.searchDto={};
	 			$scope.searchDto.keyword=object==null?"":object;
	 			$scope.searchDto.selectedPageNumber = 0;
	 			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
	 			return PortByTransportMode.fetch({"transportMode":$scope.consolClone.serviceMaster.transportMode}, $scope.searchDto).$promise.then(
	 					function(data) {
	 						if (data.responseCode =="ERR0"){
	 							$scope.docDestinationList = [];
	 							var resultList = data.responseObject.searchResult;
	 							if(resultList != null && resultList.length != 0) {
	 									$scope.docDestinationList=resultList;
	 							}
								return $scope.docDestinationList;
	 							//$scope.showDocDestinationList=true;
	 						}
	 		       },
	 		        function(errResponse){
	 		            console.error('Error while fetching Destination List');
	 		        }
	 			);

	     }


		$scope.partyRender = function(item){
			return{
				label:item.partyName,
				item:item
			}

		}
		$scope.serviceNameRender = function(item){
			return{
				label:item.serviceName,
				item:item
			}

		}
		$scope.PortNameRender = function(item){
			return{
				label:item.portName,
				item:item
			}

		}
		
		$scope.PortCodeRender = function(item){
			return{
				label:item.portCode,
				item:item
			}

		}
		
		$scope.carrierNameRender = function(item){
			return{
				label:item.carrierName,
				item:item
			}

		}
		
		$scope.carrierCodeRender = function(item){
			return{
				label:item.carrierCode,
				item:item
			}

		}
	
		$scope.currencyRender = function(item){
           return {
               label:item.currencyCode,
               item:item
           }
       }
 
 
		$scope.choosePartyAddress=function(addressKey){
			if(addressKey=='shipper'){
		   		if($scope.consolDocumentClone.shipper!=undefined && $scope.consolDocumentClone.shipper!=null &&$scope.consolDocumentClone.shipper.status!='Block' && ($scope.consolDocumentClone.shipperAddress == undefined || $scope.consolDocumentClone.shipperAddress == null)){
		   			$scope.consolDocumentClone.shipperAddress={};
		   			$scope.getPartyAddress($scope.consolDocumentClone.shipper,$scope.consolDocumentClone.shipperAddress);
		   		}else if($scope.consolDocumentClone.shipper!=undefined && $scope.consolDocumentClone.shipper!=null &&$scope.consolDocumentClone.shipper.status!='Block'){
		   		$scope.getPartyAddress($scope.consolDocumentClone.shipper,$scope.consolDocumentClone.shipperAddress);
		   		}
			}
		   		if(addressKey=='consignee'){
			   		if($scope.consolDocumentClone.consignee!=undefined &&$scope.consolDocumentClone.consignee!=null &&$scope.consolDocumentClone.consignee.status!='Block' && ($scope.consolDocumentClone.consigneeAddress == undefined || $scope.consolDocumentClone.consigneeAddress == null)){
			   			$scope.consolDocumentClone.consigneeAddress={};
			   			$scope.getPartyAddress($scope.consolDocumentClone.consignee,$scope.consolDocumentClone.consigneeAddress);
			   		}else if($scope.consolDocumentClone.consignee!=undefined &&$scope.consolDocumentClone.consignee!=null && $scope.consolDocumentClone.consignee.status!='Block'){
			   			$scope.getPartyAddress($scope.consolDocumentClone.consignee,$scope.consolDocumentClone.consigneeAddress);
			   		}
			   }
		   		
		   	 if(addressKey=='issuingAgent'){
			   		if($scope.consolDocumentClone.issuingAgent!=undefined && $scope.consolDocumentClone.issuingAgent!=null&&$scope.consolDocumentClone.issuingAgent.status!='Block' && ($scope.consolDocumentClone.issuingAgentAddress == undefined || $scope.consolDocumentClone.issuingAgentAddress == null)){
			   			$scope.consolDocumentClone.issuingAgentAddress={};
			   			$scope.getPartyAddress($scope.consolDocumentClone.issuingAgent,$scope.consolDocumentClone.issuingAgentAddress);
			   		}else if($scope.consolDocumentClone.issuingAgent!=undefined && $scope.consolDocumentClone.issuingAgent!=null&&$scope.consolDocumentClone.issuingAgent.status!='Block'){
			   			$scope.getPartyAddress($scope.consolDocumentClone.issuingAgent,$scope.consolDocumentClone.issuingAgentAddress);
			   		}
			  }
		   	 
		   	 if(addressKey=='firstNotify'){
			   		if($scope.consolDocumentClone.firstNotify!=undefined &&$scope.consolDocumentClone.firstNotify!=null&&$scope.consolDocumentClone.firstNotify.status!='Block' && ($scope.consolDocumentClone.firstNotifyAddress == undefined || $scope.consolDocumentClone.firstNotifyAddress == null)){
			   			$scope.consolDocumentClone.firstNotifyAddress={};
			   			$scope.getPartyAddress($scope.consolDocumentClone.firstNotify,$scope.documentDetail.firstNotifyAddress);
			   		}else if($scope.consolDocumentClone.firstNotify!=undefined &&$scope.consolDocumentClone.firstNotify!=null&&$scope.consolDocumentClone.firstNotify.status!='Block'){
			   			$scope.getPartyAddress($scope.consolDocumentClone.firstNotify,$scope.documentDetail.firstNotifyAddress);
			   		}
			   	}
			   		
			 if(addressKey=='secondNotify'){
			   		if($scope.consolDocumentClone.secondNotify!=undefined &&$scope.consolDocumentClone.secondNotify!=null &&$scope.consolDocumentClone.secondNotify.status!='Block' && ($scope.consolDocumentClone.secondNotifyAddress == undefined || $scope.consolDocumentClone.secondNotifyAddress == null)){
			   			$scope.documentDetail.secondNotifyAddress={};
			   			$scope.getPartyAddress($scope.consolDocumentClone.secondNotify,$scope.consolDocumentClone.secondNotifyAddress);
			   		}else if($scope.consolDocumentClone.secondNotify!=undefined &&$scope.consolDocumentClone.secondNotify!=null &&$scope.consolDocumentClone.secondNotify.status!='Block'){
			   		$scope.getPartyAddress($scope.consolDocumentClone.secondNotify,$scope.consolDocumentClone.secondNotifyAddress);
			 }
			   }
		}
 
		$scope.getPartyAddress=function(obj,mapToAddrss) {
		   	if(obj!=null && obj.id!=null){
					for(var i=0; i < obj.partyAddressList.length; i++){
		   			if(obj.partyAddressList[i].addressType=='Primary'){
		   				mapToAddrss = addressJoiner.joinAddressLineByLine(mapToAddrss,obj,obj.partyAddressList[i]);
		   			}
		   		}
				}
		   }
 

 $scope.updateMAWBConsol=function(){
	 if($scope.validateDynamicMAWBDocument(0)){
				$scope.consolClone.consolDocument.isAgreed=$scope.consolClone.consolDocument.isAgreed==true?'Yes':'No';
			     if($scope.consolClone.isMarksNo!=null){
				$scope.consolClone.isMarksNo=$scope.consolClone.isMarksNo==true?'Yes':'No';
			      }
			     ConsolEdit.update($scope.consolClone).$promise.then(
							function(data) {
								if (data.responseCode == 'ERR0') {
									console.log("Consol updated Successfully")
									$scope.showResponse($scope.consolClone.consolUid,false);
									
								} else {
									console.log("Consol updated Failed "
										+ data.responseDescription)
								}
								$scope.contained_progressbar.complete();
							}, function(error) {
								console.log("Consol updated Failed : " + error)
							});
	 }else{
		 
		 console.log("validation failed");
	 }
	 
 }
	$scope.showResponse = function(consolUid,isAdded){
		var newScope = $scope.$new();
	      var consolUid = consol.consolUid;
	      newScope.errorMessage = isAdded==true?$rootScope.nls["ERR219"]:$rootScope.nls["ERR220"];
	      newScope.errorMessage=newScope.errorMessage.replace("%s",consolUid);
	    ngDialog.openConfirm({
	           template:
	        	   '<p>{{errorMessage}}</p> ' +
	               '<div class="ngdialog-footer">' +
	                 '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
	               '</button>' +
				   '</div>',
	           plain: true,
	           scope: newScope,
	           className: 'ngdialog-theme-default'
	       }).
			then(function (value) {
				 $scope.mawbModal.$promise.then($scope.mawbModal.hide);
				 $scope.showDetail = false;
				 $location.path('/air/consol_shipments_list');
				
	       }, function (value) {
	    	   $scope.mawbModal.$promise.then($scope.mawbModal.hide);
	    	   $scope.showDetail = false;
	    	   $location.path('/air/consol_shipments_list');
	       });
	    $scope.mawbModal.$promise.then($scope.mawbModal.hide);
	}
	
	

    $scope.ajaxPortOfDocLoadingDynamic = function(object) {
	  		console.log("ajaxPortOfLoadingEvent ", object);
	  		$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

			return PortByTransportMode.fetch({"transportMode":$scope.consolClone.serviceMaster.transportMode}, $scope.searchDto).$promise.then(
					function(data) {
						if (data.responseCode =="ERR0"){
							$scope.portOfDocLoadingList = [];
							var resultList = data.responseObject.searchResult;
						return resultList;
							//$scope.showPortOfDocLoadingList=true;
						}

		       },
		        function(errResponse){
		            console.error('Error while fetching PortOfLoading List');
		        }
			);

  }
	
 
 $scope.closeDynamicDocument=function(){
	 $scope.errorMap={};
 }
 
 $scope.openDefaultTab = function() {
	 console.log("openDefaultTab in edit page is running...");
	 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DETAILS_VIEW)){
		 $scope.Tabs='moreInfo'	 
	 }
	 
}
 
 $scope.menuTabClick = function(tab){
	 if(tab=='moreInfo'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DETAILS_VIEW)){
			 $scope.Tabs=tab;	 
		 }
	 }
	 if(tab=='consol'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_HAWB_LIST_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='documentInfo'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_DOCUMENT_VIEW)){
			 $scope.Tabs=tab;	 	 
		 }
	 }
	 if(tab=='connections'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_CONNECTION_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='attachMent'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_ATTACHMENT_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='pickup'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_PICK_UP_OR_DELIVERY_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='rates'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_CHARGES_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='events'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_EVENTS_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='reports'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REPORTS_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='accounts'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_ACCOUNTS_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='references'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_REFERENCES_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='notes'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_NOTES_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='aes'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_AES_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	 if(tab=='EdiStatus'){
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_EDIT_STATUS_VIEW)){
			 $scope.Tabs=tab; 	 
		 }
	 }
	
 }

	//used for download multiple delivery order..
 $scope.downloadDo=function(){
	 var routingObj={}
	 routingObj.id=$scope.consol.id
	 previewReportService.printDownload('MASTER_DO','Download',$scope.consol.id,null,false,"MasterShipment",routingObj);	
 }

 
//create invoice from consol
$scope.getAccountsFromShipment = function(service){
	$scope.getAccountDataByLinkService(service);
	$scope.provisionalViewByConsolServiceUid(service);
	
}
$scope.getAccountsFromMaster = function(consol){
	if(consol.shipmentLinkList.length==1){
		$scope.getAccountsFromShipment(consol.shipmentLinkList[0].service);
	}else{
		$scope.getAccountData(consol.consolUid);
		$scope.getProvisionalData(consol.consolUid);
	}
	
}





//Provisional cost start

$scope.refmodel = function(provisionalItem){
	var newScope = $scope.$new();
	newScope.provisionalItem = provisionalItem;
    var myTotalOtherModal = $modal(
    		{scope:newScope,templateUrl: 'app/components/finance/provisional_cost/view/refpopup.html', show: false});
    myTotalOtherModal.$promise.then(myTotalOtherModal.show);
};

$scope.getProvisionalData = function(masteruid){
	$scope.provisionSpinner=true;
	 ProvisionalViewByConsolUid.get({
		 masteruid : masteruid
   		}, function(data) {
   			if (data.responseCode =="ERR0"){
   				console.log("ProvisionalViewByConsolUid Successfull")
   				$scope.provisionalObj = data.responseObject;
   				console.log("Provisional Object : ", $scope.provisional);
   				$scope.provisionSpinner=false;
   			} else {
   				console.log("ProvisionalViewByConsolUid Failed " + data.responseDescription);
   				$scope.provisionSpinner=false;
   			}
   		}, function(error) {
   			console.log("ProvisionalViewByConsolUid Failed : " + error);
   			$scope.provisionSpinner=false;
   		});
 }

$scope.provisionalViewByConsolServiceUid = function(shipmentServiceDetail){
	$scope.provisionSpinner=true;
	ProvisionalViewByShipmentServiceUid.get({
		serviceuid : shipmentServiceDetail.serviceUid
   		}, function(data) {
   			if (data.responseCode =="ERR0"){
   				console.log("ProvisionalViewByShipmentServiceUid Successfull")
   				$scope.provisionalForLink= data.responseObject;
   				console.log("Provisional Object : ", $scope.provisional);
   				$scope.provisionSpinner=false;
   			} else {
   				console.log("ProvisionalViewByShipmentServiceUid Failed " + data.responseDescription)
   				$scope.provisionSpinner=false;
   			}
   		}, function(error) {
   			console.log("ProvisionalViewByShipmentServiceUid Failed : " + error)
   		});
 }





// Provisional cost end


$scope.getAccountData=function(consolUid){
	$scope.invoiceSpinner=true;
	ConsolInvoiceList.get(
			{
				"consolUid" : consolUid
			},
			function(data) {
				if (data.responseCode == 'ERR0') {
					console.log(" Successfull",data.responseObject);
					$scope.invoiceList=data.responseObject;
					$scope.invoiceSpinner=false;
					
				} else {
					console.log(" Failed " + data.responseDescription);
					$scope.invoiceSpinner=false;
				}
			},
			function(error) {
				console.log(" Failed : " + error);
				$scope.invoiceSpinner=false;
			});
}

$scope.getAccountDataByLinkService=function(shipmentService){
	$scope.invoiceSpinner=true;
	$scope.searchDto={};
	$scope.searchDto.param1=shipmentService.id;
	//Bellow line commented ,as discussed with sathish robert 
	//$scope.searchDto.param2=$scope.consol.consolUid;
	return ShipmentServiceInvoiceList.fetch($scope.searchDto).$promise.then(
				function(data) {
					if (data.responseCode =="ERR0"){
						$scope.invoiceListByService=data.responseObject;
						$scope.invoiceSpinner=false;
						
					}
				},
				function(errResponse){
					console.error('Error while fetching invoicelist');
					$scope.invoiceSpinner=false;
				}
			);
	
	
	
}

$scope.filterCustomerAgent=function(type)
{
	
	 if(type=='Customer')
	 $scope.customerAgent='Customer';
	 if(type=='Agent')
    $scope.customerAgent='Agent';
	 if(type=='All')
	 $scope.customerAgent=undefined; 
	 
}


$scope.filterInvoiceCreditNote=function(type)
{
	 
	 if(type=='Invoice')
	 $scope.documentTypeCode='INV';
	 if(type=='Credit Note')
    $scope.documentTypeCode='CRN';
	 if(type=='All')
	 $scope.documentTypeCode=undefined; 
	 
}

$scope.getissuingAgentAddress=function(){
	
	 if($scope.consolDocumentClone!=undefined && $scope.consolDocumentClone!=null&&$scope.consolDocumentClone.issuingAgent!=undefined &&$scope.consolDocumentClone.issuingAgent!=null &&$scope.consolDocumentClone.issuingAgentAddress!=null){
			$scope.consolDocumentClone.issuingAgentAddress.addressLine="";
			
				if($scope.consolDocumentClone.issuingAgent!=undefined && $scope.consolDocumentClone.issuingAgent!=null && $scope.consolDocumentClone.issuingAgent!=""){
					
					$scope.consolDocumentClone.issuingAgentAddress.addressLine=		
						$scope.consolDocumentClone.issuingAgentAddress.addressLine +'\n'+ $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine1 +'\n'+
					
					$scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine2 +'\n'+ ""
					
					if($scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3!=undefined && $scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3!=null)
						$scope.consolDocumentClone.issuingAgentAddress.addressLine =$scope.consolDocumentClone.issuingAgentAddress.addressLine+$scope.consolDocumentClone.issuingAgent.partyAddressList[0].addressLine3+'\n'+$scope.getPhoneandFax($scope.consolDocumentClone.issuingAgent);
						
				}
		}
}



$scope.getPhoneandFax=function(partyObj){
	 $scope.partyPhandFax="";
	 for(var i=0; i < partyObj.partyAddressList.length; i++){
		 if(partyObj.partyAddressList[i].phone!=undefined && partyObj.partyAddressList[i].phone!=null){
		if($scope.partyPhandFax.includes("PH")){
			$scope.partyPhandFax=$scope.partyPhandFax+","+partyObj.partyAddressList[i].phone;
		}else{
			$scope.partyPhandFax=$scope.partyPhandFax+"  "+"PH:"+partyObj.partyAddressList[i].phone;
		}
		 }
	 }
	 for(var i=0; i < partyObj.partyAddressList.length; i++){
		 if(partyObj.partyAddressList[i].fax!=undefined && partyObj.partyAddressList[i].fax!=null){
			 if($scope.partyPhandFax.includes("FAX")){
				 $scope.partyPhandFax=$scope.partyPhandFax+","+partyObj.partyAddressList[i].fax;
			 }else{
				 $scope.partyPhandFax=$scope.partyPhandFax+"  "+"FAX:"+partyObj.partyAddressList[i].fax;
				 
			 }
		 }
	 
	 }
	 return $scope.partyPhandFax;
}





$scope.ajaxChargeEvent = function(object) {
    console.log("ajaxChargeEvent ", object);
    $scope.searchDto={};
    $scope.searchDto.keyword=object==null?"":object;
    $scope.searchDto.selectedPageNumber = 0;
    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

    return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
            if (data.responseCode =="ERR0"){
                $scope.totalRecord = data.responseObject.totalRecord;
                $scope.chargeMasterList = data.responseObject.searchResult;
                return $scope.chargeMasterList;
                /* $scope.showChargeMasterList=true; */
            }
        },
        function(errResponse){
            console.error('Error while fetching Charge');
        }
    );
}		



$scope.chargeRender = function(item){
    return {
        label:item.chargeCode,
        item:item
    }
}


$scope.addConsol = function() {
	if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_CREATE)){
		 $state.go("layout.addNewAirConsol");		
	}
}
$scope.addnewScreen = function() {
	if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_MASTER_WIZARD_CREATE)){
		console.log("add new screen");
	    $state.go("layout.addAirConsolWizard");
	}
}

$scope.editConsol = function() {
	if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_MODIFY)){
	    var localRefStateParam = {
	        consolId: $scope.consol.id
	    };
	    $state.go("layout.editAirConsol", localRefStateParam);

	}
}



	$scope.myClass = "accent-btn";

	$scope.tableRowExpanded = false;

	$scope.tableRowIndexCurrExpanded = "";
	$scope.tableRowIndexPrevExpanded = "";
	$scope.storeIdExpanded = "";
	$scope.dayDataCollapse = [true, true, true, true, true, true];

	$scope.selectTableRow = function(index,storeId){
		//alert("clicked");
		if ($scope.myClass === "accent-btn")
			$scope.myClass = "btn-green";
		else
			$scope.myClass = "accent-btn";

		if ($scope.tableRowExpanded === false && $scope.tableRowIndexCurrExpanded === "" && $scope.storeIdExpanded === "") {
			$scope.tableRowIndexPrevExpanded = "";
			$scope.tableRowExpanded = true;
			$scope.tableRowIndexCurrExpanded = index;
			$scope.storeIdExpanded = storeId.id;
			$scope.dayDataCollapse[index] = false;
		} else if ($scope.tableRowExpanded === true) {
			if ($scope.tableRowIndexCurrExpanded === index && $scope.storeIdExpanded === storeId.id) {
				$scope.tableRowExpanded = false;
				$scope.tableRowIndexCurrExpanded = "";
				$scope.storeIdExpanded = "";
				$scope.dayDataCollapse[index] = true;
			} else {
				$scope.tableRowIndexPrevExpanded = $scope.tableRowIndexCurrExpanded;
				$scope.tableRowIndexCurrExpanded = index;
				$scope.storeIdExpanded = storeId.id;
				$scope.dayDataCollapse[$scope.tableRowIndexPrevExpanded] = true;
				$scope.dayDataCollapse[$scope.tableRowIndexCurrExpanded] = false;
			}
		}
	}

	
	
	//AES Start
	
$scope.aesGenerateView=function(aesFile){
	var fromScreen = {};
	fromScreen=$state.current.name;
		if(aesFile!=undefined && aesFile!=null){
			ConsolGetByUid.get({
		    	consolUid: aesFile.masterUid
		    }, function(data) {
		        if (data.responseCode == 'ERR0') {
		        	if(data.responseObject!=null && data.responseObject.jobStatus!='Cancelled'){
		        	$state.go('layout.aesView', {aesId:aesFile.id,fromScreen:fromScreen,consolId:$scope.consol.id});
		        	}else{
		        		$rootScope.clientMessage = $rootScope.nls["ERR2012008"];
						return false;
		        	}
		        }
		
	    }, function(error) {
	        console.log("Consol get Failed : " + error)
	    });	
  }
}
	
$scope.aesGenerateAdd=function(aesFile){
	console.log("aesGenerateAdd method called");
$scope.spinner=false;
	var fromScreen = {};
	fromScreen=$state.current.name;
	$rootScope.clientMessage=null;
if(aesFile!=undefined && aesFile!=null){
	ConsolGetByUid.get({
		consolUid: $scope.consol.consolUid
    }, function(data) {
        if (data.responseCode == 'ERR0') {
        	if(data.responseObject!=null && data.responseObject.jobStatus!='Cancelled'){        	
        		var navAesObj=
        			{ 
        			  selectedPageNumber:$scope.detailTab=='active'?$scope.activeSearchDto.selectedPageNumber:$scope.searchDto.selectedPageNumber,
        			  status:$scope.detailTab,
        			  totalRecord:$scope.detailTab=='active'?$scope.totalRecordView:$scope.totalRecord,
        			  consolId:$scope.consol.id,
        			  serviceUid:aesFile.serviceUid,
        			  shipmentUid:aesFile.shipmentUid,
        			  consolUid:$scope.consol.consolUid,
        			  etd:$scope.consol.etd,
        			  shipper:$scope.consol.consolDocument.shipper,
        			  consignee:$scope.consol.consolDocument.consignee,
        			  fromState:$state.current.name,
        			  fromScreen:fromScreen,
        			  mawbNo:aesFile.mawbNo,
        			  hawbNo:aesFile.hawbNo	  
        			};
        	$state.go("layout.addAes",{navAesObj: navAesObj});
        	}else{
        		$rootScope.clientMessage = $rootScope.nls["ERR2012008"];
        		$scope.spinner=false;
				return false;
        		
        	}
        }else{
        	$scope.spinner=false;
        }
    }, function(error) {
    	$scope.spinner=false;
        console.log("Consol get Failed : " + error)
    });
}else{
	$scope.spinner=false;
}
	
};
			

	$scope.aesSearchFunction = function(consol){
		console.log("Search method is called. aes...........................!");
		$scope.finalArr=[];
		$scope.searchDto={};
		$scope.page = 0;
		$scope.limit = 10;
		$scope.aesFile={};
		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;
		$scope.searchDto.masterUid =consol.consolUid;
		$scope.aesFileArr = [];
		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;
		AesSearch.query($scope.searchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			var tempAesArr = [];
			$scope.aesFileList = [];
			var tempAesArr = data.responseObject.searchResult;
			var tempObj = {};
			angular.forEach(tempAesArr,function(item,index){
				tempObj = item;
				tempObj.no = (index+1)+($scope.page*$scope.limit);
				if (tempObj.id!=undefined && tempObj.id!=null) {
					tempObj.status='Update'
				}
				$scope.aesFileList.push(tempObj);
				tempObj = {};
				console.log("count...in for each")
			});
			 $scope.finalArr=$scope.checkisAnyNewLink(consol.shipmentLinkList,$scope.aesFileList);
			return $scope.finalArr;
		});
	}
	
	$scope.checkisAnyNewLink=function(shipmentLinkList,aesFileList){
		var flag;
		
		if (aesFileList != null && aesFileList.length> 0) {
			
			$scope.finalArr=$scope.finalArr.concat(aesFileList);
			
			for (var i = 0; i < shipmentLinkList.length; i++) {
				
				for (var j = 0; j < aesFileList.length; j++) {
					
					if (shipmentLinkList[i].serviceUid != aesFileList[j].serviceUid) {
						flag = true;
					}
					else {
						flag = false;
						break;
					}
				}
				if(flag) {
					if(shipmentLinkList[i].consolUid!=null){
						var tempObj = {};
						tempObj.serviceUid = $scope.consol.shipmentLinkList[i].serviceUid;
						tempObj.status = 'Generate';
						if ($scope.consol.shipmentLinkList[i].service != undefined && $scope.consol.shipmentLinkList[i].service != null && $scope.consol.shipmentLinkList[i].service.documentList[0].length != 0) {
							tempObj.hawbNo = $scope.consol.shipmentLinkList[i].service.documentList[0].hawbNo;
						}
						tempObj.masterUid=$scope.consol.consolUid;
						tempObj.mawbNo = $scope.consol.shipmentLinkList[i].service.mawbNo;
						tempObj.shipmentUid = $scope.consol.shipmentLinkList[i].service.shipmentUid;
						$scope.finalArr.push(tempObj);
					}
				}
			}
			return $scope.finalArr;
			
		} else {
			
	       for (var i = 0; i < shipmentLinkList.length; i++) {
	    	   var tempObj = {};
				tempObj.serviceUid = $scope.consol.shipmentLinkList[i].serviceUid;
				tempObj.status = 'Generate';
				if ($scope.consol.shipmentLinkList[i].service != undefined && $scope.consol.shipmentLinkList[i].service != null && $scope.consol.shipmentLinkList[i].service.documentList[0].length != 0) {
					tempObj.hawbNo = $scope.consol.shipmentLinkList[i].service.documentList[0].hawbNo;
				}
				tempObj.masterUid=$scope.consol.consolUid;
				tempObj.mawbNo = $scope.consol.shipmentLinkList[i].service.mawbNo;
				tempObj.shipmentUid = $scope.consol.shipmentLinkList[i].service.shipmentUid;
				$scope.finalArr.push(tempObj);
			}
	       return $scope.finalArr;
		}
	}
			
			
			
			// Go to shipment link
			
			$scope.goToShipmentViewByServiceUid = function (serviceuid){
				console.log("State  : ",$state)
				console.log("State Param  : ",$stateParams)
				console.log("serviceuid ",serviceuid)
				ShipmentIdByServiceUid.get({
					serviceuid :serviceuid
				},function(data) {
						if (data.responseCode == 'ERR0') {
							var param = {
									shipmentId:data.responseObject,
									fromState: $state.current.name,
									fromStateParams:JSON.stringify($stateParams)
									
							}
							$state.go("layout.viewCrmShipment", param);
						}
					}, 
					function(error) {
						console.log("Shipment get Failed : " + error)
					});
				
			}
	 

	///*AES POPUP*/
	//$scope.morepopup = function() {
	//	var myOtherModal = $modal({scope: $scope, templateUrl: 'app/components/air/consol_shipments/views/commodity_moreinfo_popup.html', show: false});
	//	myOtherModal.$promise.then(myOtherModal.show);
	//};
	
	//AES End
			
			
			
			//download do for selected ones
			
			$scope.servicesId=[];
			
			$scope.selectAllService=function(){
					$scope.servicesId=[];
				if($scope.consol.allServicesSelected){
					if($scope.consol.shipmentLinkList!=null && $scope.consol.shipmentLinkList.length>0){
						for(var i=0;i<$scope.consol.shipmentLinkList.length;i++){
							$scope.changeServiceDo($scope.consol.shipmentLinkList[i],true,i);
							$scope.consol.shipmentLinkList[i].isSelected=true;
						}
					}
				}else{
					if($scope.consol.shipmentLinkList!=null &&$scope.consol.shipmentLinkList.length>0){
						for(var i=0;i<$scope.consol.shipmentLinkList.length;i++){
							$scope.changeServiceDo($scope.consol.shipmentLinkList[i],false,i);
							$scope.consol.shipmentLinkList[i].isSelected=false;
						}
					}
				}
			}
			$scope.changeServiceDo=function(obj,isSelected,index){
				if(isSelected){
					$scope.deliveryOrderDto={};
					$scope.deliveryOrderDto.sid=obj.service.id;
					$scope.deliveryOrderDto.serviceUid=obj.serviceUid;
					$scope.deliveryOrderDto.hawbNo=obj.service.documentList[0].hawbNo;
					$scope.servicesId.splice(index,0,$scope.deliveryOrderDto);
				}else{
					$scope.servicesId.splice(index,1);
				}
			}
			
			
		    
	//download do all end	    
    
    
    /* Air Line EDI code starts here */
$scope.validateConsolForEDI = function() {
		    	
		console.log("AirEdi Validation Begins.....");
    	
    	
    	if($scope.consol.consolDocument.mawbNo == undefined || $scope.consol.consolDocument.mawbNo == null || $scope.consol.consolDocument.mawbNo == "" ) {
    		console.log($rootScope.nls["ERR9000"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9000"];
    		return false;
    	}
    	
    	
    	if($scope.consol.consolDocument.mawbGeneratedDate == undefined || $scope.consol.consolDocument.mawbGeneratedDate == null || $scope.consol.consolDocument.mawbGeneratedDate == "" ) {
    		console.log($rootScope.nls["ERR9001"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9001"];
    		return false;
    	}
    	
    	
    	if($scope.consol.origin == undefined || $scope.consol.origin == null || $scope.consol.origin.id == null) {
    		console.log($rootScope.nls["ERR9002"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9002"];
    		return false;
    	}
    	
    	if($scope.consol.destination == undefined || $scope.consol.destination == null || $scope.consol.destination.id == null ) {
    		console.log($rootScope.nls["ERR9003"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9003"];
    		return false;
    	}
    	
    	if($scope.consol.pol == undefined || $scope.consol.pol == null || $scope.consol.pol.id == null ) {
    		console.log($rootScope.nls["ERR9004"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9004"];
    		return false;
    	}
    	
    	if($scope.consol.pod == undefined || $scope.consol.pod == null || $scope.consol.pod.id == null) {
    		console.log($rootScope.nls["ERR9005"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9005"];
    		return false;
    	}
    	
    	if($scope.consol.carrier == undefined || $scope.consol.carrier == null || $scope.consol.carrier.id == null ) {
    		console.log($rootScope.nls["ERR9006"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9006"];
    		return false;
    	}
    	
    	if($scope.consol.consolDocument.routeNo == undefined || $scope.consol.consolDocument.routeNo == null || $scope.consol.consolDocument.routeNo == "" ) {
    		console.log($rootScope.nls["ERR9007"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9007"];
    		return false;
    	}
    	
    	
    	if($scope.consol.consolDocument.noOfPieces == undefined || $scope.consol.consolDocument.noOfPieces == null || $scope.consol.consolDocument.noOfPieces == "" ) {
    		console.log($rootScope.nls["ERR9008"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9008"];
    		return false;
    	}
    	
    	if($scope.consol.consolDocument.volumeWeight == undefined || $scope.consol.consolDocument.volumeWeight == null || $scope.consol.consolDocument.volumeWeight == "" ) {
    		console.log($rootScope.nls["ERR9010"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9010"];
    		return false;
    	}

    	if($scope.consol.consolDocument.grossWeight == undefined || $scope.consol.consolDocument.grossWeight == null || $scope.consol.consolDocument.grossWeight == "" ) {
    		console.log($rootScope.nls["ERR9011"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9011"];
    		return false;
    	}
    	
    	if($scope.consol.consolDocument.chargebleWeight == undefined || $scope.consol.consolDocument.chargebleWeight == null || $scope.consol.consolDocument.chargebleWeight == "" ) {
    		console.log($rootScope.nls["ERR9012"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9012"];
    		return false;
    	}
    	
    	if($scope.consol.consolDocument.commodityDescription == undefined || $scope.consol.consolDocument.commodityDescription == null || $scope.consol.consolDocument.commodityDescription == "" ) {
    		console.log($rootScope.nls["ERR9017"]);
    		$rootScope.clientMessage = $rootScope.nls["ERR9017"];
    		return false;
    	}
    	return true;
    }
    
//win web connect
$scope.connectWinWeb=function(){
	
if($scope.validateForWin()){
		
		 $scope.winwebmodal= $modal({
			    scope: $scope,
			    backdrop: 'static',
			    templateUrl: 'app/components/air/winwebconnect/win_view_html.html',
			    show: false
			});
			$scope.winwebmodal.$promise.then($scope.winwebmodal.show)		 
	   }	
}


$scope.validateForWin=function(){
	
	if($scope.consol!=undefined && $scope.consol!=null && $scope.consol.id!=undefined){
		
        if($scope.consol.iataRate==undefined || $scope.consol.iataRate==null || $scope.consol.iataRate==" "){
             Notification.error("Iata Rate is mandatory for connecting win web connect..");
        	 return false;
        }
        if($scope.consol.consolDocument.agent==undefined || $scope.consol.consolDocument.agent==null || $scope.consol.consolDocument.agent=="" || $scope.consol.consolDocument.agent.id==undefined){
        	Notification.error("Agent is mandatory");
       	    return false;
        }
	}else{
		Notification.error("Consol is invalid");
		return false;
	}
return true;
}


    
    
    $scope.airlineEdiConfirmation = function() {
    	
    	if($scope.consol.carrier==undefined || $scope.consol.carrier==null || $scope.consol.carrier=="" || $scope.consol.carrier.id==undefined){
			 Notification.error($rootScope.nls["ERR96519"]); 
			 return;
		 }
    	
    	 if($scope.consol.carrier.messagingType!=undefined && $scope.consol.carrier.messagingType!=null && $scope.consol.carrier.messagingType=='WIN'){
			 $scope.connectWinWeb();
		 }else{
    	if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_GENERATE_AIRLINE_EDI_CREATE)){
    		if($scope.consol.airlineEdi == undefined || $scope.consol.airlineEdi == null || $scope.consol.airlineEdi.id == null) {
    			$scope.airlineEdiDirectShipmentConfirmation();
    		} else {
    			var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR9021"]
    			ngDialog.openConfirm({
    				template: '<p>{{errorMessage}}</p>' +
    				'<div class="ngdialog-footer">' +
    				'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(false)">No</button>' +
    				'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(true)">Yes</button>' +
    				'</div>',
    				plain: true,
    				scope:newScope,
    				className: 'ngdialog-theme-default'
    			}).then(function (value) {
    				if(value == true) {
    					$scope.airlineEdiDirectShipmentConfirmation();
    				} else {
    					console.log("You pressed No");
    				}
    			});	
    		} 
    	}
    	
		 }
    }
    	
    	
    	
    
    $scope.airlineEdiDirectShipmentConfirmation = function() {
    	if($scope.consol.directShipment == "Yes" || $scope.consol.directShipment == true) {
    		var newScope = $scope.$new();
    		newScope.errorMessage =$rootScope.nls["ERR9018"]
    		ngDialog.openConfirm({
    			template: '<p>{{errorMessage}}</p>' +
    			'<div class="ngdialog-footer">' +
    			'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(false)">No</button>' +
    			'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(true)">Yes</button>' +
    			'</div>',
    			plain: true,
    			scope:newScope,
    			className: 'ngdialog-theme-default'
    		}).then(function (value) {
    			$rootScope.mainpreloder = true;
    			console.log("You selected : ", value);
    			$scope.airlineEdiFn(value);
    		});	
    	} else {
    		$scope.airlineEdiFn(false);
    	}
    }

    
    var myOtherModalEdi;
    
    $scope.airlineEdiFn = function(flag) {
    	console.log("The current consol shipment id is : " + $scope.consol.id + " isChangeAgent " + flag);
    	if($scope.validateConsolForEDI(flag)) {
    		$scope.createEDI(flag);
    	} else {
    		$rootScope.mainpreloder = false;
    		console.log("Airline Validation Failed....");
    	}
    };
    
    $scope.createEDI = function(flag) {
		AirlineEDIGet.get({id : $scope.consol.id, isAgentChange : flag }, function(data) {
			if(data.responseCode == 'ERR0') {
    			$rootScope.mainpreloder = false;
    			$scope.airlineEdi = data.responseObject;
    			myOtherModalEdi = $modal({
    				scope: $scope, 
    				templateUrl: '/app/components/air/consol_shipments_new_screen/views/airline_edi_popup.html',
    				backdrop:'static',
    				show: false
    			});
    			myOtherModalEdi.$promise.then(myOtherModalEdi.show);
    		} else {
    			console.log("Error while creating the Airline Edi Model ", data.responseDescription);
    		}
    	}, function(error) {
    		console.log("Error while creating the Airline Edi Model ", error);
       	});
	}
    
    
	$scope.generateEdi = function(airlineEdiObj) {
		console.log("Generate Edi is pressed....");
		$scope.spinner = true;
		AirlineEDISave.save(airlineEdiObj).$promise.then(function(data) {
			if(data.responseCode == 'ERR0') {
				console.log("Success...", data.responseObject);
				$rootScope.newConsolAfterEDI = data.responseObject;

				if($rootScope.newConsolAfterEDI != null) {
					
					Notification.success($rootScope.nls["ERR9991"]);
					
					var stateCount = 0;
	   				if ($stateParams.count != undefined && $stateParams.count != null) {
	   					stateCount = $stateParams.count + 1;
	   				} 
	   				$scope.spinner = false;
	   				$state.go("layout.viewNewAirConsol", {count:stateCount,forPurpose:'EDI'});
				}
				
				 
			} else {
				console.log("Failure...", data.responseDescription);
				$scope.spinner = false;
			}
		}, function(error){
			console.log("Failure...", error);
			$scope.spinner = false;
		});
		myOtherModalEdi.$promise.then(myOtherModalEdi.hide);
	}
    
	
	
	 $scope.goToShipmentService=function(shipmentUid)
		{
		 
			var param = {shipmentUid:shipmentUid,
						fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
				
	        $state.go('layout.viewCrmShipment', param);
		}
	 $scope.goToConsol=function(consolUid)
		{
			var param = {consolUid:consolUid,
					fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}

	        $state.go('layout.viewNewAirConsol', param);
		}
	 
	 
	 var signOffModel;
	 
	//Sign Off Modal
	 $scope.consolSignOffModel = function(consolObject) {
		 if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_SIGNOFF_CREATE)){
			 $scope.consolObject = angular.copy(consolObject);;

			 if($scope.consolObject.consolSignOff == undefined || $scope.consolObject.consolSignOff == null) {
		    		$scope.consolObject.consolSignOff = {};
		    		$scope.consolObject.consolSignOff.signOffBy  = $rootScope.userProfile.employee;
		    		$scope.consolObject.consolSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
		    		$scope.consolObject.consolSignOff.consolUid = consolObject.consolUid;
		    		$scope.consolObject.consolSignOff.isSignOff = true;
		    	}else{
		    		$scope.consolObject.consolSignOff.signOffDate = $rootScope.dateAndTimeToString($scope.consolObject.consolSignOff.signOffDate);
		    	}
		    	signOffModel = $modal({scope: $scope, templateUrl: 'app/components/air/consol_shipments_new_screen/views/consol_signoff_popup.html', show: false});
		        signOffModel.$promise.then(signOffModel.show);
		       
		 }
	 };
	    
	    
	    /*Salesman Select Picker*/
	    $scope.ajaxEmployeeEvent = function(object) {
	        $scope.searchDto = {};
	        $scope.searchDto.keyword = object == null ? "" : object;
	        $scope.searchDto.selectedPageNumber = 0;
	        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
	        return EmployeeList.fetch($scope.searchDto).$promise.then(
	            function(data) {
	                if (data.responseCode == "ERR0") {
	                    $scope.customerServiceList = data.responseObject.searchResult;
	                    console.log("$scope.customerServiceList", $scope.customerServiceList);
	                    return $scope.customerServiceList;
	                }
	            },
	            function(errResponse) {
	                console.error('Error while fetching Customer Services');
	            }
	        );

	    }
	    $scope.ajaxCommentEvent = function(object) {
	        $scope.searchDto = {};
	        $scope.searchDto.keyword = object == null ? "" : object;
	        $scope.searchDto.selectedPageNumber = 0;
	        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
	        return CommentFactory.search.query($scope.searchDto).$promise.then(
	            function(data) {
	                if (data.responseCode == "ERR0") {
	                    $scope.commentList = data.responseObject.searchResult;
	                    console.log("$scope.commentList", $scope.commentList);
	                    return $scope.commentList;
	                }
	            },
	            function(errResponse) {
	                console.error('Error while fetching Customer Services');
	            }
	        );

	    }

	    
	    $scope.selectedCommentMaster = function(nextFieldId) {
	    	$scope.consolObject.consolSignOff.description = $scope.consolObject.consolSignOff.commentMaster.description;
	    	$rootScope.navigateToNextField(nextFieldId);
	    }

	    
	    $scope.saveConsolSignOff = function(signOffObject) {
			
	    	signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
	    	
	       	 if (signOffObjectCopy.signOffDate != null)
	                signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);
	       	 
	       	 
	       	appMetaFactory.consol.signOff(signOffObjectCopy).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					signOffModel.$promise.then(signOffModel.hide);
					
					if(data.responseObject.unSuccessfull!=null){
						$scope.signOffResponse(data.responseObject.unSuccessfull)
					}else{
						var param={consolId:$scope.consol.id,fromStateParams : JSON.stringify($stateParams)};
						$state.go("layout.viewNewAirConsol",param);
					}
					
					
				} else {
					Notification.error($rootScope.nls[data.responseCode]);
	   				console.log("Sign off Failed", data.responseDescription);
				}
			}, function(error) {
				console.lolg("Sign off Failed", error);
			});
   	
	    }
	  
	    
	    $scope.signOffResponse = function(str){
			var newScope = $scope.$new();
		      var str = str;
		      newScope.errorMessage = $rootScope.nls["ERR90635"];
		      newScope.errorMessage=newScope.errorMessage.replace("%s",str);
		    ngDialog.openConfirm({
		           template:
		        	   '<p>{{errorMessage}}</p> ' +
		               '<div class="ngdialog-footer">' +
		                 '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok' +
		               '</button>' +
					   '</div>',
		           plain: true,
		           scope: newScope,
		           className: 'ngdialog-theme-default'
		       }).
				then(function (value) {
					
					var param={consolId:$scope.consol.id,fromStateParams : JSON.stringify($stateParams)};
					$state.go("layout.viewNewAirConsol",param);
				});
		}
	    
	    var unSignOffModel;
	    //Sign Off Modal
	    $scope.consolUnSignOffModal = function(object) {
	    	if($rootScope.roleAccess(roleConstant.AIR_MASTER_SHIPMENT_UNSIGNOFF_MODIFY)){

		    	$scope.consolObject = object;
		    	
		    	$scope.consolObject.consolSignOff.signOffBy  = $rootScope.userProfile.employee;
	    		$scope.consolObject.consolSignOff.signOffDate = $rootScope.dateAndTimeToString(new Date());
	    		
		    	unSignOffModel = $modal({scope: $scope, templateUrl: 'app/components/air/consol_shipments_new_screen/views/consol_unsignoff_popup.html', show: false});
		    	unSignOffModel.$promise.then(unSignOffModel.show);
		    
	    	}
	    };
	    
	    $scope.saveConsolUnSignOff = function(signOffObject) {
	    	// close the model
	    	signOffObjectCopy = JSON.parse(JSON.stringify(signOffObject));
	    	 if (signOffObjectCopy.signOffDate != null)
	             signOffObjectCopy.signOffDate = $rootScope.sendApiDateAndTime(signOffObjectCopy.signOffDate);
	    	 
	    	 signOffObjectCopy.isSignOff = false;
	    	 
	    	 appMetaFactory.consolUnSignOff.unSignOff(signOffObjectCopy).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					unSignOffModel.$promise.then(unSignOffModel.hide);
					var param={consolId:$scope.consol.id,fromStateParams : JSON.stringify($stateParams)};
					$state.go("layout.viewNewAirConsol",param);
				} else {
					Notification.error($rootScope.nls[data.responseCode]);
	   				console.log("Sign off Failed", data.responseDescription);
				}
			}, function(error) {
				console.log("Sign off Failed", error);
			});
	    }
	    
	    
	    console.log("$stateParams.action ----- ", $stateParams.action);
		switch ($stateParams.action) { 
			case "VIEW":
				$rootScope.clientMessage=null;
				console.log("View Consol Page..........................!", $stateParams.consolId, + " " + $stateParams.consolIndex);
				$rootScope.unfinishedData = undefined;
				if($stateParams.fromState=='layout.addAes' || $stateParams.fromState== 'layout.editAes' ){
					$stateParams.navAesObj=angular.fromJson($stateParams.navAesObj);
					$scope.viewConsolGetData($stateParams.navAesObj.consolId);
				}else if($stateParams!=undefined && $stateParams.forPurpose=="EDI"){
					try{
						$scope.consol = $rootScope.newConsolAfterEDI;
					}catch(e){
						console.log("exception occured in localstorage")
					}
				}else if($stateParams.consolUid !=undefined && $stateParams.consolUid !=null){
					$scope.viewConsolUidGetData($stateParams.consolUid);
				}else{
					$scope.viewConsolGetData($stateParams.consolId);
				}
				
				
				
				 $rootScope.breadcrumbArr = [{label:"Air", state:"layout.airNewConsol"}, {label:"Master Shipment", state:"layout.airNewConsol"}, {label:"View Master Shipment", state:null}];
				break;
			case "SEARCH":
				console.log("List Consol Page...........................!");
				$scope.init();
				$rootScope.breadcrumbArr = [{label:"Air", state:"layout.airNewConsol"}, {label:"Master Shipment", state:null}];
				break;
		}
	    
});

