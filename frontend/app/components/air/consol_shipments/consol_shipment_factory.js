/**
 * 
 */
 app.factory("ConsolSearch",['$resource', function($resource) {
 	return $resource("/api/v1/consol/search", {}, {
 		query : {
 			method : 'POST'
 		}
 	});
 }]);


 app.factory("ConsolAsyncSearch",['$resource', function($resource) {
 	return $resource("/api/v1/consol/search", {}, {
 		query : {
 			method : 'POST'
 		}
 	});
 }]);


 app.factory("ConsolGet",['$resource', function($resource) {
 	return $resource("/api/v1/consol/get/id/:id", {}, {
 		get : {
 			method : 'GET',
 			params : {
 				id : ''
 			},
 			isArray : false
 		}
 	});
 }]);


 app.factory("AirlineEDIGet",['$resource', function($resource) {
 	return $resource("/api/v1/consol/edi/get/:id/:isAgentChange", {}, {
 		get : {
 			method : 'GET',
 			params : {id : '',isAgentChange : ''},
 			isArray : false
 		}
 	});
 }]);


 app.factory("EDIResponse",['$resource', function($resource) {
 	return $resource("/api/v1/consol/edi/get/response/:id", {}, {
 		get : {
 			method : 'GET',
 			params : {id : ''},
 			isArray : false
 		}
 	});
 }]);


 app.factory("AirlineEDISave",['$resource', function($resource) {
 	return $resource("/api/v1/consol/edi/save", {}, {
 		save : {
 			method : 'POST'
 		}
 	});
 }]);





 app.factory("ConsolGetByUid",['$resource', function($resource) {
 	return $resource("/api/v1/consol/get/consolUid/:consolUid", {}, {
 		get : {
 			method : 'GET',
 			params : {
 				consolUid : ''
 			},
 			isArray : false
 		}
 	});
 }]);


 app.factory("ConsolEdit",['$resource', function($resource) {
 	return $resource("/api/v1/consol/update", {}, {
 		update : {
 			method : 'POST'
 		}
 	});
 }]);


 app.factory("ConsolStatusChange",['$resource', function($resource) {
 	return $resource("/api/v1/consol/statuschange", {}, {
 		status : {
 			method : 'POST',
 			isArray : false
 		}
 	});
 }]);

 app.factory("AutoImportProcess",['$resource', function($resource) {
 	return $resource("/api/v1/consol/processautoimport", {}, {
 		process : {
 			method : 'POST',
 			isArray : false
 		}
 	});
 }]);


 app.factory("ConsolAdd",['$resource', function($resource) {
 	return $resource("/api/v1/consol/create", {}, {
 		save : {
 			method : 'POST'
 		}
 	});
 }]);



 app.factory("ConsolDocumentUpdate",['$resource', function($resource) {
 	return $resource("/api/v1/consol/document/update", {}, {
 		save : {
 			method : 'POST'
 		}
 	});
 }]);




 app.factory("ConsolShipmentLink",['$resource', function($resource) {
 	return $resource("/api/v1/consol/shipmentLink", {}, {
 		link : {
 			method : 'POST',
 		}
 	});
 }]);



 app.factory("ConsolDocumentSearch",['$resource', function($resource) {
 	return $resource("/api/v1/consol/documentSearch", {}, {
 		query : {
 			method : 'POST'
 		}
 	});
 }]);


//});

app.factory("consolShipmentDocumentIdList",['$resource', function($resource) {
	return $resource("/api/v1/consol/report/shipment/:serviceId", {}, {
		get : {
			method : 'GET',
			params : {
				serviceId : ''
			},
			isArray : false
		}
	});
}]);



app.factory("doPrintALL",['$resource', function($resource) {
	return $resource("/api/v1/consol/doall", {}, {
		print : {
			method : 'POST'
		}
	});
}]);



app.factory("ConsolIdBasedOnUid",['$resource', function($resource) {
	return $resource("/api/v1/consol/consolIdBasedOnUid/:consolUid", {}, {
		get : {
			method : 'GET',
			params : {
				consolUid : ''
			},
			isArray : false
		}
	});
}]);




app.factory("PreAlertAgentReportLength",['$resource', function($resource) {
	return $resource("/api/v1/preAlert/reportlength", {}, {
		query : {
			method : 'GET',
			params : {
				consolUid : ''
			},
			isArray : false
		}
	});
}]);

app.factory("PreAlertAgentSendMail",['$resource', function($resource) {
	return $resource("/api/v1/preAlert/mailtoagent", {}, {
		query : {
			method : 'POST'
		}
	});
}]);