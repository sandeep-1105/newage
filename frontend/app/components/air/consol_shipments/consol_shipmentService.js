/**
 * 
 */
app.factory('consolService', function(NavigationService,$rootScope,$location) {
	
	var savedData = {}
	
	var loc=null;
	
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
	}

	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}

	return {
		set : set,
		get : get,
		setLoc : setLoc,
		getLoc : getLoc,
		goToEdit:goToEdit,
	}

	function setLoc(data) {
		loc = data;
	}
	
	function getLoc() {
		return loc;
	}

	
	
	function goToEdit(consol) {

		consol.consolDocument.documentReqDate=$rootScope.getDate(consol.consolDocument.documentReqDate);
		consol.consolDocument.dimensionUnit=consol.consolDocument.dimensionUnit=='CENTIMETERORKILOS'?false:true;
		consol.consolDocument.dimensionUnitValue = consol.consolDocument.dimensionUnit?$rootScope.appMasterData['inch.to.pounds']:$rootScope.appMasterData['centimeter.to.kilos'];
		consol.ppcc=consol.ppcc=='Prepaid'?false:true;
		consol.consolDocument.isAgreed=='No'?false:true;
		consol.consolDocument.isMarksNo=='No'?false:true;
		if(consol.pickUpDeliveryPoint==null&&consol.pickUpDeliveryPoint==undefined){
			consol.pickUpDeliveryPoint={};
		}

		consol.masterDate=consol.masterDate;
		consol.consolDocument.mawbIssueDate=$rootScope.getDate(consol.consolDocument.mawbIssueDate);
		consol.eta=$rootScope.getDate(consol.eta);
		consol.etd=$rootScope.getDate(consol.etd);
		consol.atd=$rootScope.getDate(consol.atd);
		consol.ata=$rootScope.getDate(consol.ata);
		
	if(consol.connectionList != undefined && consol.connectionList != null && consol.connectionList.length != 0 ) {
		for(var i=0;i<consol.connectionList.length;i++){
			consol.connectionList[i].eta=$rootScope.getDate(consol.connectionList[i].eta);
			consol.connectionList[i].etd=$rootScope.getDate(consol.connectionList[i].etd);
		}
	}	
		
	if(consol.eventList != undefined && consol.eventList != null && consol.eventList.length != 0 ) {
		for(var i=0;i<consol.eventList.length;i++){
			consol.eventList[i].eventDate=$rootScope.getDate(consol.eventList[i].eventDate);
			consol.eventList[i].followUpDate=$rootScope.getDate(consol.eventList[i].followUpDate);
		}
	}
		
		

		return consol;
	
	}
	
	
	

 
});


