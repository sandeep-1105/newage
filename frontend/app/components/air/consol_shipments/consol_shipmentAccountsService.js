app.service('consolShipmentAccountsService',['$rootScope', '$timeout','$state','RecentHistorySaveService','ShipmentIdBasedOnUid','roleConstant','Notification','$stateParams',
                                     function($rootScope, $timeout,$state, RecentHistorySaveService,ShipmentIdBasedOnUid,roleConstant,Notification,$stateParams) {

	this.$roleConstant = roleConstant;
	this.createInvoice = function(consol,shipmentServiceDetail) {
		if($rootScope.roleAccess(roleConstant.FINANCE_INVOICE_CREATE)){
			if(consol.shipmentLinkList ==null || consol.shipmentLinkList ==undefined || consol.shipmentLinkList =='' || consol.shipmentLinkList.size==0)
			{
			 console.log("Atleast one shipment link service is required")
			 Notification.error($rootScope.nls["ERR40022"])
		     return false;
			
			}
			
			if(consol.shipmentLinkList.length==1){

				ShipmentIdBasedOnUid.get({
		  		  shipmentUid : consol.shipmentLinkList[0].service.shipmentUid
					},function(data) {
						if (data.responseCode == 'ERR0') {
							var shipmentId= data.responseObject;
							var param = {shipmentId : shipmentId,consolId : consol.id,shipmentServiceId:consol.shipmentLinkList[0].service.serviceUid,
									fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
								$state.go('layout.invoiceCreate', param);
						}
			   		}, function(error) {
			   			console.log("ShipmentId getting Failed : " + error)
			   			
			   		});
				
			}else if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined)
				{
				ShipmentIdBasedOnUid.get({
		  		  shipmentUid : shipmentServiceDetail.shipmentUid
					},function(data) {
						if (data.responseCode == 'ERR0') {
							var shipmentId= data.responseObject;
							var param = {shipmentId : shipmentId,consolId : consol.id,shipmentServiceId:shipmentServiceDetail.serviceUid,
									fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
								$state.go('layout.invoiceCreate', param);
						}
			   		}, function(error) {
			   			console.log("ShipmentId getting Failed : " + error)
			   			
			   		});
				}
			else
				{
				var param = {consolId : consol.id,fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
				$state.go('layout.invoiceCreate', param);
				}
		}
	}

	this.createCreditNoteCost = function(consol,shipmentServiceDetail) {
		if($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_COST_CREATE)){
			if(consol.shipmentLinkList ==null || consol.shipmentLinkList ==undefined || consol.shipmentLinkList =='' || consol.shipmentLinkList.size==0)
			{
				console.log("Atleast one shipment link service is required")
				Notification.error($rootScope.nls["ERR40022"])
		        return false;
			
			}
			
			if(consol.shipmentLinkList.length==1)
			{
			ShipmentIdBasedOnUid.get({
				  shipmentUid : consol.shipmentLinkList[0].service.shipmentUid
				},function(data) {
					if (data.responseCode == 'ERR0') {
						var shipmentId= data.responseObject;
						var param = {shipmentId : shipmentId,consolId : consol.id,shipmentServiceId:consol.shipmentLinkList[0].service.serviceUid,
								fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
							$state.go('layout.creditNoteCostCreate', param);
					}
		   		}, function(error) {
		   			console.log("ShipmentId getting Failed : " + error)
		   			
		   		});
			}else if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined)
			{
			ShipmentIdBasedOnUid.get({
				  shipmentUid : shipmentServiceDetail.shipmentUid
				},function(data) {
					if (data.responseCode == 'ERR0') {
						var shipmentId= data.responseObject;
						var param = {shipmentId : shipmentId,consolId : consol.id,shipmentServiceId:shipmentServiceDetail.serviceUid,
								fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
							$state.go('layout.creditNoteCostCreate', param);
					}
		   		}, function(error) {
		   			console.log("ShipmentId getting Failed : " + error)
		   			
		   		});
			}
		else
			{
			var param = {consolId : consol.id,fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
			$state.go('layout.creditNoteCostCreate', param);
			}

		}
	}

	this.createCreditNoteRevenue = function(consol,shipmentServiceDetail) {
		if($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_CREATE)){
			if(consol.shipmentLinkList.length==1){

				var param = {shipmentUid : consol.shipmentLinkList[0].service.shipmentUid,shipmentServiceId:consol.shipmentLinkList[0].service.id,
						fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
					$state.go('layout.addCreditNoteRevenue', param);

			}else if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined)
			{
						var param = {shipmentUid : shipmentServiceDetail.shipmentUid,shipmentServiceId:shipmentServiceDetail.id,
								fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
							$state.go('layout.addCreditNoteRevenue', param);
			}
		else
			{
			var param = {consolUid : consol.consolUid,fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
			$state.go('layout.addCreditNoteRevenue', param);
			}
		}
	}



	this.getInvoiceCrnView=function(invoiceCreditNote,shipmentServiceDetail,consol)
	{
		
		if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined && shipmentServiceDetail !='')
			{
			
			var invoiceParam = {shipmentUid : shipmentServiceDetail.shipmentUid,invoiceId:invoiceCreditNote.id,
	  				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)};
			var creditNoteCostParam = {shipmentUid : shipmentServiceDetail.shipmentUid,creditNoteCostId:invoiceCreditNote.id,
	  				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)};
			var creditNoteRevenueParam = {shipmentUid : shipmentServiceDetail.shipmentUid,creditNoteRevenueId:invoiceCreditNote.id,
	  				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)};
			}
		else
			{
			var invoiceParam = {consolUid : consol.consolUid,invoiceId:invoiceCreditNote.id,
	  				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)};
			var creditNoteCostParam = {consolUid : consol.consolUid,creditNoteCostId:invoiceCreditNote.id,
	  				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)};
			var creditNoteRevenueParam = {consolUid : consol.consolUid,creditNoteRevenueId:invoiceCreditNote.id,
	  				fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)};
			}

		if(invoiceCreditNote.documentTypeMaster.documentTypeCode=='INV')
			{
	    $state.go('layout.viewInvoice', invoiceParam);
			}
		else
			{
		if(invoiceCreditNote.documentTypeMaster.documentTypeCode=='CRN' && invoiceCreditNote.adjustmentInvoiceNo!=null)
		{
			
	        $state.go('layout.viewCreditNoteRevenue', creditNoteRevenueParam);
		}
		else
			{
	        $state.go('layout.viewCreditNoteCost', creditNoteCostParam);
			}
			}
		
	}
	this.addProvisionalFromConsol = function(consol,shipmentServiceDetail) {
		
		console.log("shipmentServiceDetail ", shipmentServiceDetail);
		
		if(consol.shipmentLinkList ==null || consol.shipmentLinkList ==undefined || consol.shipmentLinkList =='' || consol.shipmentLinkList.size==0)
		{
			console.log("Atleast one shipment link service is required")
			Notification.error($rootScope.nls["ERR40022"])
	     return false;
		
		}
		if(consol.shipmentLinkList.length==1){
			shipmentServiceDetail = consol.shipmentLinkList[0].service;
		}
		
		if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined)
		{
		ShipmentIdBasedOnUid.get({
  		  shipmentUid : shipmentServiceDetail.shipmentUid
			},function(data) {
				if (data.responseCode == 'ERR0') {
					var shipmentId= data.responseObject;
					var param = {shipmentId : shipmentId,consolId : consol.id,shipmentServiceId:shipmentServiceDetail.serviceUid,
							fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
						$state.go('layout.addProvisional', param);
				}
	   		}, function(error) {
	   			console.log("ShipmentId getting Failed : " + error)
	   			
	   		});
		}
	else
		{
		var param = {consolId : consol.id,fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
		$state.go('layout.addProvisional', param);
		}
    }
	
	this.editProvisionalFromConsol = function(object,consol,shipmentServiceDetail) {
		if(consol.shipmentLinkList ==null || consol.shipmentLinkList ==undefined || consol.shipmentLinkList =='' || consol.shipmentLinkList.size==0)
		{
		 console.log("Atleast one shipment link service is required")
		 Notification.error($rootScope.nls["ERR40022"])
	     return false;
		
		}
    	
    	if(consol.shipmentLinkList.length==1){
			shipmentServiceDetail = consol.shipmentLinkList[0].service;
		}
    	
    	if(shipmentServiceDetail!=null && shipmentServiceDetail !=undefined)
		{
		ShipmentIdBasedOnUid.get({
  		  shipmentUid : shipmentServiceDetail.shipmentUid
			},function(data) {
				if (data.responseCode == 'ERR0') {
					var shipmentId= data.responseObject;
					var param = {shipmentId : shipmentId,consolId :consol.id,
							shipmentServiceId:shipmentServiceDetail.serviceUid,provisionalId:object.id,
							fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
						$state.go('layout.editProvisional', param);
				}
	   		}, function(error) {
	   			console.log("ShipmentId getting Failed : " + error)
	   			
	   		});
		}
	else
		{
		var param = {consolId : consol.id,provisionalId:object.id,fromState:$state.current.name, fromStateParams : JSON.stringify($stateParams)}
		$state.go('layout.editProvisional', param);
		}
    	
    }
}]);