(function() {

	app.factory("CFSReceiveSearch", function($resource) {
		return $resource("/api/v1/cfsreceive/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	});
	
	
	app.factory("CFSReceiveGet", function($resource) {
		return $resource("/api/v1/cfsreceive/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	});
	
	
	app.factory("CFSReceiveEntrySave", function($resource) {
		return $resource("/api/v1/cfsreceive/create", {}, {
			save : {
				method : 'POST'
			}
		});
	});
	
	app.factory("CFSReceiveEntryUpdate", function($resource) {
		return $resource("/api/v1/cfsreceive/update", {}, {
			update : {
				method : 'POST'
			}
		});
	});

})();
