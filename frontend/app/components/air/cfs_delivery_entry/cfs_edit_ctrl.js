/**
 * Created by saravanan on 17/8/16.
 */
app.controller('CFSEditController',function($scope, $rootScope, ngProgressFactory, $state,$modal, $stateParams, ngDialog, 
		CFSSearchKeyword, CFSReceiveEntrySave, CFSReceiveEntryUpdate, cloneService, CFSReceiveGet, 
		RecentHistorySaveService, PartiesList, PartiesListByType, $timeout, ShipmentSearchKeyword, ServiceGetByUid, ShipmentGetByUid,
		ShipmentServiceSearchKeyword, discardService, appConstant,CommonValidationService, CfsService,roleConstant ){

	$scope.$roleConstant = roleConstant;
	$scope.disableSubmitBtn = false;
    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            $scope.disableSubmitBtn = false;
        }, 300);
    }
	
	
    $scope.contained_progressbar = ngProgressFactory.createInstance();
	$scope.contained_progressbar.setParent(document.getElementById('cfs-panel'));
	$scope.contained_progressbar.setAbsolute();
	$scope.firstFocus = true;
	
	$scope.init = function() {
		console.log("Init is called....");
		
		if($scope.cfsReceiveEntry == undefined || $scope.cfsReceiveEntry == null) {
			$scope.cfsReceiveEntry =  {};
			$scope.cfsReceiveEntry.cfsReceiptDate = $rootScope.dateToString(new Date());
			
			$scope.cfsReceiveEntry.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
			$scope.cfsReceiveEntry.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
			$scope.cfsReceiveEntry.location = $rootScope.userProfile.selectedUserLocation;
			
			 if($rootScope.dynamicFormFieldMap.get('Shipment > Service > LBS')=='true'){
				 $scope.cfsReceiveEntry.dimensionUnit = true;
				 $scope.cfsReceiveEntry.dimensionUnitValue = $rootScope.appMasterData['inch.to.pounds'];
			 } else {
				 $scope.cfsReceiveEntry.dimensionUnit = false;
				 $scope.cfsReceiveEntry.dimensionUnitValue = $rootScope.appMasterData['centimeter.to.kilos'];
			 }
			
		} 
		
		if($scope.cfsReceiveEntry.cfsAttachementList == undefined || $scope.cfsReceiveEntry.cfsAttachementList == null ||$scope.cfsReceiveEntry.cfsAttachementList.length == 0) {
			$scope.cfsReceiveEntry.cfsAttachementList = [{}];
		} 

		if($scope.cfsReceiveEntry.cfsDimensionList == undefined || $scope.cfsReceiveEntry.cfsDimensionList == null ||$scope.cfsReceiveEntry.cfsDimensionList.length == 0) {
			$scope.cfsReceiveEntry.cfsDimensionList = [{}];
		} 
		
		discardService.set($scope.cfsReceiveEntry);
	}

	
	 $scope.calculateWeight = function(param) {
		 CfsService.calculateWeight(param, $scope.cfsReceiveEntry);
	 }
	
	 
	 //Measurement button
	    var viewMeasurementModal;
	    $scope.msrmodel = function() {
	    /* $scope.shipmentServiceDetail = {};
	     $scope.shipmentServiceDetail.eventList = [{}];*/
	     viewMeasurementModal= $modal({
	         scope: $scope,
	         backdrop: 'static',
	         templateUrl: 'app/components/air/cfs_delivery_entry/dialog/cfs_delivery_measurement_popup.html',
	         show: false
	     });
//	     $scope.navigateToNextField("flightSchedule.service");
	     viewMeasurementModal.$promise.then(viewMeasurementModal.show)
	    };

	    
	  //history button
	    var viewJournalModal;
	    $scope.journalmodel = function() {
	    /* $scope.shipmentServiceDetail = {};
	     $scope.shipmentServiceDetail.eventList = [{}];*/
	    	viewJournalModal= $modal({
	         scope: $scope,
	         backdrop: 'static',
	         templateUrl: 'app/components/air/cfs_delivery_entry/dialog/cfs_delivery_journaldetails_popup.html',
	         show: false
	     });
//	     $scope.navigateToNextField("flightSchedule.service");
	    	viewJournalModal.$promise.then(viewJournalModal.show)
	    };
	    
	  //more button
	    var viewMoreModal;
	    $scope.moremodel = function() {
	  
	    	viewMoreModal= $modal({
	         scope: $scope,
	         backdrop: 'static',
	         templateUrl: 'app/components/air/cfs_delivery_entry/dialog/cfs_delivery_notes_popup.html',
	         show: false
	     });
	    	viewMoreModal.$promise.then(viewMoreModal.show)
	    };
	    
	$scope.save = function() {
		console.log("Save method is called.", $scope.cfsReceiveEntry);
		
		   if (!$scope.disableSubmitBtn) {
	            $scope.disableSubmitBtn = true;
	            
	            
	            if($scope.validate(0)) {
	    			$rootScope.clientMessage=null;
	    			if($scope.isDimensionCheck()){
	    				if($scope.attachConfig.isAttachmentCheck()) {
	    				$scope.tmpObject = cloneService.clone($scope.cfsReceiveEntry);
	    				 
	    				if ($scope.tmpObject.cfsReceiptDate != null)
	    					$scope.tmpObject.cfsReceiptDate = $rootScope.sendApiStartDateTime($scope.tmpObject.cfsReceiptDate);
	    				
	    				if ($scope.tmpObject.proDate != null)
	    					$scope.tmpObject.proDate = $rootScope.sendApiStartDateTime($scope.tmpObject.proDate);
	    				
	    				// Saving the shipment Service Id for backend purpose
	    				
	    				if($scope.tmpObject.shipmentServiceDetail == null) {
	    					$scope.tmpObject.shipmentServiceDetail = $scope.tmpObject.shipment.serviceId;
		    				$scope.tmpObject.shipmentUid = $scope.tmpObject.shipment.shipmentUId;
		    				$scope.tmpObject.serviceUid = $scope.tmpObject.shipment.serviceUid;	 	
	    				}
	    				   				
	    				
	    				
	    				$scope.contained_progressbar.start();
	    				CFSReceiveEntrySave.save($scope.tmpObject).$promise.then(function(data) {
	    					if (data.responseCode == 'ERR0') {
	    						console.log("CFSReceiveEntry Saved successfully..");
	    						if($stateParams.fromState != undefined && $stateParams.fromState != null) {
	    			    			 console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
	    			        		 $state.go($stateParams.fromState, {shipmentId : $stateParams.fromStateParams});	
	    			    		 } else {
	    			    			var params = {submitAction : 'Saved'};
	 	    						$state.go("layout.airCFSdelivery",params);
	    			    		 }
	    						
	    						$rootScope.successDesc=$rootScope.nls["ERR400"];
	    						$scope.enableSubmitBtnFn();
	    						
	    					} else {
	    						$scope.enableSubmitBtnFn();
	    						console.log("CFSReceiveEntry Saving Failed ", data.responseDescription)
	    					}
	    					$scope.enableSubmitBtnFn();
	    					$scope.contained_progressbar.complete();
	    					angular.element(".panel-body").animate({scrollTop: 0}, "slow");
	    				}, function(error) {
	    					$scope.enableSubmitBtnFn();
	    					console.log("CFSReceiveEntry Saving Failed : ",error)
	    				});
	    				
	    				} else {
	    					if($scope.cfsReceiveEntry.cfsAttachementList!=null && $scope.cfsReceiveEntry.cfsAttachementList.length != 0) {
		    					$rootScope.clientMessage=$rootScope.nls["ERR250"];
		    					$scope.moreTab='attachTab';
		    					$scope.enableSubmitBtnFn();
		    					return false;
		    				}
	    				}
	    				
	    			}else{
	    				 
	    				if($scope.cfsReceiveEntry.cfsDimensionList!=null && $scope.cfsReceiveEntry.cfsDimensionList.length != 0) {
	    					$rootScope.clientMessage=$rootScope.nls["ERR8340"];
	    					$scope.moreTab='dimensionTab';
	    					$scope.enableSubmitBtnFn();
	    					return false;
	    				}
	    			}
	    		} else {
	    			console.log("Validation Failed.....................................");
	    			$scope.enableSubmitBtnFn();
	    		}
		   }
	}
	
	
	
	
	$scope.update = function() {
		console.log("Update method is called.", $scope.cfsReceiveEntry);
		
		   if (!$scope.disableSubmitBtn) {
	            $scope.disableSubmitBtn = true;
	            
	            
	            if($scope.validate(0)) {
	    			$rootScope.clientMessage=null;
	    			if($scope.isDimensionCheck()){
	    				if($scope.attachConfig.isAttachmentCheck()) {
	    				$scope.tmpObject = cloneService.clone($scope.cfsReceiveEntry);
	    				 
	    				if ($scope.tmpObject.cfsReceiptDate != null)
	    					$scope.tmpObject.cfsReceiptDate = $rootScope.sendApiStartDateTime($scope.tmpObject.cfsReceiptDate);
	    				
	    				if ($scope.tmpObject.proDate != null)
	    					$scope.tmpObject.proDate = $rootScope.sendApiStartDateTime($scope.tmpObject.proDate);
	    				
	    				$scope.contained_progressbar.start();
	    				CFSReceiveEntryUpdate.update($scope.tmpObject).$promise.then(function(data) {
	    					if (data.responseCode == 'ERR0') {
	    						console.log("CFSReceiveEntry Saved successfully..");
	    						var params = {};
	    						params.submitAction = 'Saved';
	    						$rootScope.successDesc=$rootScope.nls["ERR400"];
	    						$scope.enableSubmitBtnFn();
	    						$state.go("layout.airCFSdelivery",params);
	    					} else {
	    						$scope.enableSubmitBtnFn();
	    						console.log("CFSReceiveEntry Saving Failed ", data.responseDescription)
	    					}
	    					$scope.enableSubmitBtnFn();
	    					$scope.contained_progressbar.complete();
	    					angular.element(".panel-body").animate({scrollTop: 0}, "slow");
	    				}, function(error) {
	    					$scope.enableSubmitBtnFn();
	    					console.log("CFSReceiveEntry Saving Failed : ",error)
	    				});
	    				
	    				}else {
	    					if($scope.cfsReceiveEntry.cfsAttachementList!=null && $scope.cfsReceiveEntry.cfsAttachementList.length != 0) {
		    					$rootScope.clientMessage=$rootScope.nls["ERR250"];
		    					$scope.moreTab='attachTab';
		    					$scope.enableSubmitBtnFn();
		    					return false;
		    				}
	    				}
	    				
	    			}else{
	    				 
	    				if($scope.cfsReceiveEntry.cfsDimensionList!=null && $scope.cfsReceiveEntry.cfsDimensionList.length != 0) {
	    					$rootScope.clientMessage=$rootScope.nls["ERR8340"];
	    					$scope.moreTab='dimensionTab';
	    					$scope.enableSubmitBtnFn();
	    					return false;
	    				}
	    			}
	    		} else {
	    			console.log("Validation Failed.....................................");
	    			$scope.enableSubmitBtnFn();
	    		}
		   }
	}
	
	
	 
	 
	 
	 
	 
	 
	 
	 $scope.shipmentServiceRender = function(item){
         return{
             label : item.serviceUid,
             item:item
         }
     }
	 
	 $scope.ajaxShipmentServiceEvent = function(object) {

         $scope.searchDto={};
         $scope.searchDto.keyword=object==null?"":object;
         $scope.searchDto.selectedPageNumber = 0;
         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

         return ShipmentServiceSearchKeyword.fetch($scope.searchDto).$promise.then(
             function(data) {
                 if (data.responseCode =="ERR0"){
                     $scope.serviceUidList = data.responseObject.searchResult;
                     console.log("Successful fetching serviceUidList ", $scope.serviceUidList);
                     return $scope.serviceUidList;
                 } else {
                	 console.log("Error while fetching serviceUidList ", data.responseDescription);
                 }
             },
             function(errResponse){
                 console.error('Error while fetching  serviceUidList', errResponse);
             }
         );
     }
	 
	 
	 $scope.selectedShipmentServiceUid = function(nextFieldId){
		 if($scope.validate(3)){
			 console.log("Shipment Selected " , $scope.cfsReceiveEntry.shipmentServiceDetail);
			 if ($scope.cfsReceiveEntry.shipmentServiceDetail != undefined && $scope.cfsReceiveEntry.shipmentServiceDetail != null) {
				 $scope.mySpinner = true;
				 ServiceGetByUid.get({
					 serviceUid: $scope.cfsReceiveEntry.shipmentServiceDetail.serviceUid
		            }, function(data) {
		                if (data.responseCode == 'ERR0') {
		                	$scope.cfsReceiveEntry.shipmentServiceDetail = data.responseObject;
		                    CfsService.copyFromService($scope.cfsReceiveEntry, $scope.cfsReceiveEntry.shipmentServiceDetail);
		                    $scope.mySpinner = false;
		                } else {
		                	console.log("Error while getting shipment " + data.responseDescription);
		                	$scope.mySpinner = false;
		                }
		            }, function(error) {
		            	console.log("Error while getting shipment : ", error);
		            	$scope.mySpinner = false;
		            });
				 if(nextFieldId!=undefined) {
					 $rootScope.navigateToNextField(nextFieldId); 
				 }
			 }
			
		 }
	 }
	 
	 
	/* CFS Lov */

	$scope.cfsRender = function(item){
		return {
			label: item.cfsName,
			item: item
		}
	};
	
	$scope.ajaxCfsEvent = function(object) {
		
		console.log("ajaxCfsEvent is called ", object)
		
		$scope.searchDto={};
		$scope.searchDto.keyword=object==null?"":object;
		$scope.searchDto.selectedPageNumber = 0;
		$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
		
		return CFSSearchKeyword.fetch($scope.searchDto).$promise.then(
				function(data) {
					if (data.responseCode =="ERR0"){
						$scope.cfsMasterList = data.responseObject.searchResult;
						console.log("$scope.cfsMasterList ", $scope.cfsMasterList);
						return $scope.cfsMasterList;
					}
               },
                function(errResponse){
                    console.error('Error while fetching  cfsMasterList');
                }
       );
	}

	$scope.selectedCfs = function(object, nextIdValue) {
		console.log("Selected CFS ", object);
		if($scope.validate(1)){
			if(nextIdValue!=undefined)
			$rootScope.navigateToNextField(nextIdValue);
		}
	}
	
	
	$scope.selectedCfsReceiveDate = function(nextId){
		if($scope.validate(2)){
			if(nextId!=undefined)
				$scope.navigateToNextField(nextId);	
		}
	}
	
	$scope.selectedProDate = function(nextId){
		if($scope.validate(5)){
			if(nextId!=undefined)
				$scope.navigateToNextField(nextId);	
		}
	}
	
	
	/*Shipper Lov*/
	$scope.shipperRender = function(item){
		return{
			label:item.partyName,
			item:item
		}
	}
	
	$scope.ajaxShipperEvent = function(object) {
   	 
   	 console.log("ajaxShipperEvent is called", object);
   	 
   	 $scope.searchDto={};
   	 $scope.searchDto.keyword=object==null?"":object;
   	 $scope.searchDto.selectedPageNumber = 0;
   	 $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
 		
   	 return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
   		 if (data.responseCode =="ERR0"){
   			 $scope.totalRecord = data.responseObject.totalRecord;
   			 $scope.shipperList = data.responseObject.searchResult;
   			 return $scope.shipperList;
   		 }
   	 },
   	 function(errResponse){
   		 console.error('Error while fetching Shipper');
     }
	)};
	
	$scope.selectedShipper = function(object, nextIdValue) {
		console.log("Selected Shipper ", object);
		if($scope.validate(6)){
			if(nextIdValue!=undefined)
			$rootScope.navigateToNextField(nextIdValue);
		}
	}
	
	
	/* Consignee Lov*/
	$scope.consigneeRender = function(item){
		return{
			label:item.partyName,
			item:item
		}
	}
	
	$scope.ajaxConsigneeEvent = function(object) {
   	 
   	 console.log("ajaxConsigneeEvent is called", object);
   	 
   	 $scope.searchDto={};
   	 $scope.searchDto.keyword=object==null?"":object;
   	 $scope.searchDto.selectedPageNumber = 0;
   	 $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
 		
   	 return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
   		 if (data.responseCode =="ERR0"){
   			 $scope.totalRecord = data.responseObject.totalRecord;
   			 $scope.consigneeList = data.responseObject.searchResult;
   			 return $scope.consigneeList;
   		 }
   	 },
   	 function(errResponse){
   		 console.error('Error while fetching Shipper');
     }
	)};
	
	
	
	
	$scope.selectedConsignee = function(object, nextIdValue) {
		console.log("Selected consignee ", object);
		if($scope.validate(7)){
			if(nextIdValue!=undefined)
			$rootScope.navigateToNextField(nextIdValue);
		}
	}
	
	
	/* Transporter Lov */
	
	$scope.transporterRender = function(item){
		return{
			label:item.partyName,
			item:item
		}
	}
	
	$scope.ajaxTrasporterEvent = function(object) {

		console.log("ajaxTrasporterEvent method : " , object);

		$scope.searchDto={};
		$scope.searchDto.keyword=object==null?"":object;
		$scope.searchDto.selectedPageNumber = 0;
		$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

		return PartiesListByType.fetch({"partyType":'Transporter'},$scope.searchDto).$promise.then(function(data, status) {
				if (data.responseCode =="ERR0"){
					$scope.transporterList = data.responseObject.searchResult;
					return $scope.transporterList;
				}
			},
			function(errResponse){
				console.error('Error while fetching Party');
			}
		);
	}
	
	$scope.selectedTransporter = function(object, nextIdValue) {
		console.log("Selected Transporter ", object);
		if($scope.validate(8)){
			if(nextIdValue!=undefined)
			$rootScope.navigateToNextField(nextIdValue);
		}
	}
	
	

    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";

    $scope.cancel = function(){
    	
    	 if (!discardService.compare($scope.cfsReceiveEntry)) {
             var newScope = $scope.$new();
             newScope.errorMessage = $rootScope.nls["ERR200"];
             ngDialog.openConfirm({
                 template: '<p>{{errorMessage}}</p>' +
                     '<div class="ngdialog-footer">' +
                     '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                     '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                     '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                     '</div>',
                 plain: true,
                 scope: newScope,
                 className: 'ngdialog-theme-default'
             }).then(function(value) {
            	 if (value == 1 && $scope.cfsReceiveEntry.id == null) {
            		 $scope.save();
            	 } else if (value == 1 && $scope.cfsReceiveEntry.id != null) {
            		 $scope.update();
            	 } else if (value == 2) {
            		 console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
            		 if($stateParams.fromState != undefined && $stateParams.fromState != null) {
            			 console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
                		 $state.go($stateParams.fromState, {shipmentId : $stateParams.fromStateParams});	
            		 } else {
            			 var params = {submitAction : 'Cancelled'}
            			 $state.go("layout.airCFSdelivery", params);
            		 }
            	 } else {
            		 console.log("Nothing selected..Cancel")
            	 }
             });
    	 } else { 
    		 console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
    		 if($stateParams.fromState != undefined && $stateParams.fromState != null) {
    			 console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
        		 $state.go($stateParams.fromState, {shipmentId : $stateParams.fromStateParams});	
    		 } else {
    		 var params = {submitAction : 'Cancelled'}
             $state.go("layout.airCFSdelivery", params);
    		 }
    	 }
    }
    
    
    
    $scope.validate = function(validateCode) {
    	$scope.firstFocus = false;
    	console.log("Validate Code " + validateCode);
    	
    	 $scope.errorMap = new Map();
    	 
    	 if (validateCode == 0 || validateCode == 1) {

             if ($scope.cfsReceiveEntry.cfsMaster == undefined ||
                 $scope.cfsReceiveEntry.cfsMaster == null ||
                 $scope.cfsReceiveEntry.cfsMaster == "" ||
                 $scope.cfsReceiveEntry.cfsMaster.id == null) {

                 $scope.errorMap.put("cfsMaster", $rootScope.nls["ERR37200"]);
                 console.log($rootScope.nls["ERR37200"]);
                 return false;

             } else if ($scope.cfsReceiveEntry.cfsMaster.status != null) {

                 if ($scope.cfsReceiveEntry.cfsMaster.status == "Block") {
                     $scope.errorMap.put("cfsMaster", $rootScope.nls["ERR37201"]);
                     console.log($rootScope.nls["ERR37201"]);
                     $scope.cfsReceiveEntry.cfsMaster = null;
                     return false
                 }
                 
                 if ($scope.cfsReceiveEntry.cfsMaster.status == "Hide") {
                     $scope.errorMap.put("cfsMaster", $rootScope.nls["ERR37202"]);
                     console.log($rootScope.nls["ERR37202"]);
                     $scope.cfsReceiveEntry.cfsMaster = null;
                     return false
                 }
             }
         }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 2) {
             if ($scope.cfsReceiveEntry.cfsReceiptDate == null || $scope.cfsReceiveEntry.cfsReceiptDate == undefined) {
                 $scope.errorMap.put("cfsReceiptDate", $rootScope.nls["ERR37203"]);
                 console.log($rootScope.nls["ERR37203"]);
                 return false;
             }
         }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 3) {/*

             if ($scope.cfsReceiveEntry.serviceMaster == undefined ||
                 $scope.cfsReceiveEntry.serviceMaster == null ||
                 $scope.cfsReceiveEntry.serviceMaster == "" ||
                 $scope.cfsReceiveEntry.serviceMaster.id == null) {

                 $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR37205"]);
                 console.log($rootScope.nls["ERR37205"]);
                 return false;

             } else  {

                 if ($scope.cfsReceiveEntry.serviceMaster.status == "Block") {
                     $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR37206"]);
                     console.log($rootScope.nls["ERR37206"]);
                     $scope.cfsReceiveEntry.serviceMaster = null;
                     return false
                 }
                 
                 if ($scope.cfsReceiveEntry.serviceMaster.status == "Hide") {
                     $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR37207"]);
                     console.log($rootScope.nls["ERR37207"]);
                     $scope.cfsReceiveEntry.serviceMaster = null;
                     return false
                 }
             }
         */}
    	 
    	 
    	 if (validateCode == 0 || validateCode == 4) {
    		 
            /* if ($scope.cfsReceiveEntry.proNumber == undefined || $scope.cfsReceiveEntry.proNumber == "" || $scope.cfsReceiveEntry.proNumber == null) {
                 $scope.errorMap.put("proNumber", $rootScope.nls["ERR37208"]);
                 console.log($rootScope.nls["ERR37208"]);
                 return false;
             }*/ 
    		 
    		 
    		 
    		 if($scope.cfsReceiveEntry.proNumber != undefined && $scope.cfsReceiveEntry.proNumber != "") {
    			 
    			 if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Cfs_Receive_Pro_Number ,$scope.cfsReceiveEntry.proNumber)){
    				 $scope.errorMap.put("proNumber", $rootScope.nls["ERR37209"]);
                     console.log($rootScope.nls["ERR37209"]);
                     return false;
    			}
             }
         }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 5) {
             /*if ($scope.cfsReceiveEntry.proDate == null || $scope.cfsReceiveEntry.proDate == undefined) {
                 $scope.errorMap.put("proDate", $rootScope.nls["ERR37210"]);
                 console.log($rootScope.nls["ERR37210"]);
                 return false;
             }*/
         }
    	
    	 
    	 
    	 if (validateCode == 0 || validateCode == 6) {

             if ($scope.cfsReceiveEntry.shipper == undefined ||
                 $scope.cfsReceiveEntry.shipper == null ||
                 $scope.cfsReceiveEntry.shipper == "" ||
                 $scope.cfsReceiveEntry.shipper.id == null) {

                 $scope.errorMap.put("shipper", $rootScope.nls["ERR37212"]);
                 console.log($rootScope.nls["ERR37212"]);
                 return false;

             } else {

                 if ($scope.cfsReceiveEntry.shipper.status == "Block") {
                     $scope.errorMap.put("shipper", $rootScope.nls["ERR37213"]);
                     console.log($rootScope.nls["ERR37213"]);
                     $scope.cfsReceiveEntry.cfsMaster = null;
                     return false
                 }
                 
                 if ($scope.cfsReceiveEntry.shipper.status == "Hide") {
                     $scope.errorMap.put("shipper", $rootScope.nls["ERR37214"]);
                     console.log($rootScope.nls["ERR37214"]);
                     $scope.cfsReceiveEntry.shipper = null;
                     return false
                 }
             }
         }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 7) {

             if ($scope.cfsReceiveEntry.consignee == undefined ||
                 $scope.cfsReceiveEntry.consignee == null ||
                 $scope.cfsReceiveEntry.consignee == "" ||
                 $scope.cfsReceiveEntry.consignee.id == null) {

                /* $scope.errorMap.put("consignee", $rootScope.nls["ERR37215"]);
                 console.log($rootScope.nls["ERR37215"]);
                 return false;*/

             } else {

                 if ($scope.cfsReceiveEntry.consignee.status == "Block") {
                     $scope.errorMap.put("consignee", $rootScope.nls["ERR37216"]);
                     console.log($rootScope.nls["ERR37216"]);
                     $scope.cfsReceiveEntry.consignee = null;
                     return false
                 }
                 
                 if ($scope.cfsReceiveEntry.consignee.status == "Hide") {
                     $scope.errorMap.put("consignee", $rootScope.nls["ERR37217"]);
                     console.log($rootScope.nls["ERR37217"]);
                     $scope.cfsReceiveEntry.consignee = null;
                     return false
                 }
             }
         }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 8) {

             if ($scope.cfsReceiveEntry.transporter == undefined ||
                 $scope.cfsReceiveEntry.transporter == null ||
                 $scope.cfsReceiveEntry.transporter == "" ||
                 $scope.cfsReceiveEntry.transporter.id == null) {

                /* $scope.errorMap.put("transporter", $rootScope.nls["ERR37218"]);
                 console.log($rootScope.nls["ERR37218"]);
                 return false;*/

             } else {

                 if ($scope.cfsReceiveEntry.transporter.status == "Block") {
                     $scope.errorMap.put("transporter", $rootScope.nls["ERR37219"]);
                     console.log($rootScope.nls["ERR37219"]);
                     $scope.cfsReceiveEntry.transporter = null;
                     return false
                 }
                 
                 if ($scope.cfsReceiveEntry.transporter.status == "Hide") {
                     $scope.errorMap.put("transporter", $rootScope.nls["ERR37220"]);
                     console.log($rootScope.nls["ERR37220"]);
                     $scope.cfsReceiveEntry.transporter = null;
                     return false
                 }
             }
         }
    	 
    	 
    	 
    	 if (validateCode == 0 || validateCode == 9) {
    		 
            /* if ($scope.cfsReceiveEntry.warehouseLocation == undefined || $scope.cfsReceiveEntry.warehouseLocation == "" || $scope.cfsReceiveEntry.warehouseLocation == null) {
                  $scope.errorMap.put("warehouseLocation", $rootScope.nls["ERR37221"]);
                  console.log($rootScope.nls["ERR37221"]);
                  return false;
              }*/
     		 
     		 if($scope.cfsReceiveEntry.warehouseLocation != undefined && $scope.cfsReceiveEntry.warehouseLocation != "") {
     			 
     			 if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Cfs_Receive_Warehouse_location ,$scope.cfsReceiveEntry.warehouseLocation)){
     				 $scope.errorMap.put("warehouseLocation", $rootScope.nls["ERR37222"]);
                     console.log($rootScope.nls["ERR37222"]);
                     return false;
    			}
              }
          }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 10) {
    		 
            /* if ($scope.cfsReceiveEntry.shipperRefNumber == undefined || $scope.cfsReceiveEntry.shipperRefNumber == "" || $scope.cfsReceiveEntry.shipperRefNumber == null) {
                   $scope.errorMap.put("shipperRefNumber", $rootScope.nls["ERR37223"]);
                   console.log($rootScope.nls["ERR37223"]);
                   return false;
               }*/
      		 
      		 if($scope.cfsReceiveEntry.shipperRefNumber != undefined && $scope.cfsReceiveEntry.shipperRefNumber != "") {
      			 
      			 if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Cfs_Receive_Shipper_Ref_No ,$scope.cfsReceiveEntry.shipperRefNumber)){
      				  $scope.errorMap.put("shipperRefNumber", $rootScope.nls["ERR37224"]);
                      console.log($rootScope.nls["ERR37224"]);
                      return false;
    			 }
               }
           }
    	 
    	 
    	 if (validateCode == 0 || validateCode == 11) {
    		 
             /* if ($scope.cfsReceiveEntry.onHand == undefined || $scope.cfsReceiveEntry.onHand == "" || $scope.cfsReceiveEntry.onHand == null) {
                   $scope.errorMap.put("onHand", $rootScope.nls["ERR37225"]);
                   console.log($rootScope.nls["ERR37225"]);
                   return false;
               }*/
      		 
      		 if($scope.cfsReceiveEntry.onHand != undefined && $scope.cfsReceiveEntry.onHand != "") {
      			 
      			 if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Cfs_Receive_On_Hand ,$scope.cfsReceiveEntry.onHand)){
      				$scope.errorMap.put("onHand", $rootScope.nls["ERR37226"]);
                    console.log($rootScope.nls["ERR37226"]);
                    return false;
   			     }
               }
           }
    	 
    	 if (validateCode == 0 || validateCode == 11) {
    		 
      		 if($scope.cfsReceiveEntry.notes != undefined && $scope.cfsReceiveEntry.notes != "") {
      			 
      			 if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Cfs_Receive_Entry_Notes ,$scope.cfsReceiveEntry.notes)){
      				$scope.errorMap.put("notes", $rootScope.nls["ERR37227"]);
                    console.log($rootScope.nls["ERR37227"]);
                    return false;
    			     }
               }
           }
    	 
    	 
    	 return true;
    	 
    	 
    	 
    }
    
    
    $scope.editCFSReceiveEntryGetData = function(cfsId) {
    	
    	CFSReceiveGet.get({
            id: cfsId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                $scope.cfsReceiveEntry = data.responseObject;
                console.log("$stateParams.serviceUid " + $stateParams.serviceUid);
                $scope.cfsReceiveEntry.cfsReceiptDate = $rootScope.dateToString($scope.cfsReceiveEntry.cfsReceiptDate);
	            $scope.cfsReceiveEntry.proDate = $rootScope.dateToString($scope.cfsReceiveEntry.proDate);
	 			
                
                $scope.mySpinner = true;
   			 
                ServiceGetByUid.get({
   				 serviceUid: $stateParams.serviceUid
   	            }, function(data) {

   	            	
   	                if (data.responseCode == 'ERR0') {
   	                	$scope.cfsReceiveEntry.shipmentServiceDetail = data.responseObject;
   	                	CfsService.copyFromService($scope.cfsReceiveEntry, $scope.cfsReceiveEntry.shipmentServiceDetail);
   	                    $scope.mySpinner = false;
   	                    
   	                 if ($scope.cfsReceiveEntry != undefined && $scope.cfsReceiveEntry != null && 
   	                		$scope.cfsReceiveEntry.cfsMaster != undefined && $scope.cfsReceiveEntry.cfsMaster != null &&
   	                		$scope.cfsReceiveEntry.cfsMaster.cfsName != undefined && $scope.cfsReceiveEntry.cfsMaster.cfsName != null) {
   	                    $rootScope.subTitle = $scope.cfsReceiveEntry.cfsMaster.cfsName;
   	                } else {
   	                    $rootScope.subTitle = "Unknown CFS"
   	                }

   	                $rootScope.serviceCodeForUnHistory = "";
   	                $rootScope.orginAndDestinationUnHistory = "";

   	                var rHistoryObj = {
   	                    'title': 'CFS Receive Entry Edit # ' + $stateParams.cfsId,
   	                    'subTitle': $rootScope.subTitle,
   	                    'stateName': $state.current.name,
   	                    'stateParam': JSON.stringify($stateParams),
   	                    'stateCategory': 'CFS',
   	                    'serviceType': 'AIR',
   	                    'serviceCode': '',
   	                    'orginAndDestination': '',
   	                    'showService': true
   	                }
   	                RecentHistorySaveService.form(rHistoryObj);
   	                $scope.init();
   	                    
   	                    
   	                } else {
   	                	console.log("Error while getting shipment " + data.responseDescription);
   	                	$scope.mySpinner = false;
   	                }
   	            }, function(error) {
   	            	console.log("Error while getting shipment : ", error);
   	            	$scope.mySpinner = false;
   	            });

               
                
                
            } else {
                console.log("Error While CFSReceiveGet ", data.responseDescription)
            }
        }, function(error) {
            console.log("CFSReceiveGet Failed", error);
        });
    }
    
    
    
    
    $scope.attachConfig = {
    		"isEdit":true,
    		"isDelete":true,
    		"tableSize":"col-xs-8",
    		"columnDefs":[

    			{"name":"Reference No", "model":"refNo","type":"text","width":"inl_lp"},
    			{"name":"File Name", "model":"fileName","type":"file"},
    			{"name":"Actions","model":"action", "type":"action","width":"inl_l"}
    		]
    }
    
    $scope.setTab = function(tabName, fieldId) {
    	
    	if (tabName == 'dimensionTab') {
    		$scope.moreTab = tabName;
    	} 
    	else if (tabName == 'attachTab') {
    		$scope.moreTab = tabName;
    	}
    	if($scope.cfsReceiveEntry!=null && $scope.cfsReceiveEntry!=undefined && $scope.cfsReceiveEntry.id!=undefined){
    		if(tabName=='noteTab'&& $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_NOTES_MODIFY)){
				$scope.moreTab=tabName;
			}
		}else{
			if(tabName=='noteTab'){
				$scope.moreTab=tabName;
			}
		}
    	$rootScope.navigateToNextField(fieldId);
		
    }
    
    
    $scope.loadShipmentServiceData = function(sUid) {
    	
    	console.log("Loading Shipment Servoce Data ", sUid);
    	
    	$scope.mySpinner = true;
    	
    	$scope.cfsReceiveEntry =  {};
		$scope.cfsReceiveEntry.cfsReceiptDate = $rootScope.dateToString(new Date());
		
		$scope.cfsReceiveEntry.company = $rootScope.userProfile.selectedUserLocation.companyMaster;
		$scope.cfsReceiveEntry.country = $rootScope.userProfile.selectedUserLocation.countryMaster;
		$scope.cfsReceiveEntry.location = $rootScope.userProfile.selectedUserLocation;
    	
		ServiceGetByUid.get({
			serviceUid : sUid
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.cfsReceiveEntry.shipmentServiceDetail = data.responseObject;
       
                CfsService.copyFromService($scope.cfsReceiveEntry, $scope.cfsReceiveEntry.shipmentServiceDetail);

                if($scope.cfsReceiveEntry.cfsAttachementList == undefined || $scope.cfsReceiveEntry.cfsAttachementList == null ||$scope.cfsReceiveEntry.cfsAttachementList.length == 0) {
        			$scope.cfsReceiveEntry.cfsAttachementList = [{}];
        		} 
                
                $scope.mySpinner = false;
                
                
            } else {
            	console.log("Error While getting shipment...." , data.responseDescription);
            	$scope.mySpinner = false;
            }
        }, function(error) {
        	console.log("Error While getting shipment...." , error);
        	$scope.mySpinner = false;
        });
    } 
    
    $scope.$on('addAirCFSEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.cfsReceiveEntry;
        $rootScope.category = "CFS Receive Entry";
        $rootScope.unfinishedFormTitle = "CFS Receive Entry (New)";
        
        if (	$scope.cfsReceiveEntry != undefined && 
        		$scope.cfsReceiveEntry != null && 
        		$scope.cfsReceiveEntry.cfsMaster != undefined && 
        		$scope.cfsReceiveEntry.cfsMaster != null &&
        		$scope.cfsReceiveEntry.cfsMaster.cfsName != undefined && 
        		$scope.cfsReceiveEntry.cfsMaster.cfsName != null && 
        		$scope.cfsReceiveEntry.cfsMaster.cfsName != "")  {
            $rootScope.subTitle = $scope.cfsReceiveEntry.cfsMaster.cfsName;
        } else {
            $rootScope.subTitle = "Unknown CFS"
        }
    });

    $scope.$on('editAirCFSEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.cfsReceiveEntry;
        $rootScope.category = "CFS Receive Entry";
        $rootScope.unfinishedFormTitle = "CFS Receive Entry Edit # " + $scope.cfsReceiveEntry.cfsReceiptNumber;
    
        if (	$scope.cfsReceiveEntry != undefined && 
        		$scope.cfsReceiveEntry != null && 
        		$scope.cfsReceiveEntry.cfsMaster != undefined && 
        		$scope.cfsReceiveEntry.cfsMaster != null &&
        		$scope.cfsReceiveEntry.cfsMaster.cfsName != undefined && 
        		$scope.cfsReceiveEntry.cfsMaster.cfsName != null && 
        		$scope.cfsReceiveEntry.cfsMaster.cfsName != "")  {
            $rootScope.subTitle = $scope.cfsReceiveEntry.cfsMaster.cfsName;
        } else {
            $rootScope.subTitle = "Unknown CFS"
        }
        
    });
    
    $scope.$on('addAirCFSEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.cfsReceiveEntry);
        localStorage.isCFSReloaded = "YES";
        e.preventDefault();
    });

    $scope.$on('editAirCFSEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.cfsReceiveEntry);
        localStorage.isCFSReloaded = "YES";
        e.preventDefault();
    });
    
    $scope.isReloaded = localStorage.isCFSReloaded;
    
    if ($stateParams.action == "ADD") {
    
    	if ($stateParams.fromHistory == 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCFSReloaded = "NO";
                $scope.cfsReceiveEntry = JSON.parse(localStorage.reloadFormData);
                
            } else {
                $scope.cfsReceiveEntry = $rootScope.selectedUnfilledFormData;
            }
            $scope.init();
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCFSReloaded = "NO";
                $scope.cfsReceiveEntry = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
            	$scope.init();
            }
        }
    
    	$rootScope.breadcrumbArr = [
    	                            	{label:"Air", state:"layout.airCFSdelivery"}, 
    	                            	{label:"CFS Receive Entry", state:"layout.airCFSdelivery"},
    	                            	{label:"Add CFS Receive Entry", state:null}
    	                           ];
    	$rootScope.navigateToNextField('cfsMaster');
    
    } else if($stateParams.action == "CREATE") {
    	
    	
    	 $scope.loadShipmentServiceData($stateParams.serviceUid);
    	
    	 $rootScope.breadcrumbArr = [ 
    	                              {label:"Air", state : null}, 
    	                              {label:"CFS Receive Entry", state : null}, 
    	                              {label:"Create CFS Receive Entry", state : null}];
    	
    } else {
    	
        if ($stateParams.fromHistory == 'Yes') {
        
        	if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCFSReloaded = "NO";
                $scope.cfsReceiveEntry = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.cfsReceiveEntry = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }

        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCFSReloaded = "NO";
                $scope.cfsReceiveEntry = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.editCFSReceiveEntryGetData($stateParams.cfsId);
            }
        }
        $rootScope.breadcrumbArr = [
                                    {label:"Air", state:"layout.airCFSdelivery"}, 
                                    {label:"CFS Receive Entry", state:"layout.airCFSdelivery"},
	                            	{label:"Edit CFS Receive Entry", state:null}
                                   ];
        $rootScope.navigateToNextField('cfsMaster');
    }
    
    
});