/**
 * Created by hmspl on 12/4/16.
 */
app.controller('CFSDeliveryController', function($rootScope, $scope, $modal, $state, $stateParams, 
		CFSReceiveSearch, CFSReceiveGet, RecentHistorySaveService, cloneService,
		GoToUnfilledFormNavigationService, ShipmentIdByServiceUid, downloadFactory,roleConstant){

	$scope.$roleConstant=roleConstant;
    $scope.cfsHeadArr = [ {
		"name" : "#",
		"width" : "w50px",
		"model" : "no",
		"search" : false,
		"prefWidth" : "50"
	}, {
		"name" : "Shipment ID",
		"width" : "w75px",
		"model" : "jobNumber",
		"search" : true,
		"wrap_cell" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchReceiptNumber",
		"prefWidth" : "100"

	}, {
		"name" : "Pieces",
		"width" : "w25px text-right",
		"model" : "pieces",
		"search" : true,
		"type" : "number",
		"sort" : true,
		"key" : "searchReceiptDate",
		"prefWidth" : "100"

	}, {
		"name" : "Gross Weight (KG)",
		"width" : "w50px text-right",
		"model" : "grsWeight",
		"search" : true,
		"type" : "number",
		"sort" : true,
		"key" : "searchCSF",
		"prefWidth" : "150"

	}, {
		"name" : "In Gate Time",
		"width" : "w100px",
		"model" : "inGateTime",
		"search" : true,
		"type" : "date-time-range",
		"sort" : true,
		"key" : "searchShipmentUid",
		"prefWidth" : "100"
	}, {
		"name" : "Out Gate Time",
		"width" : "w100px",
		"model" : "outGateTime",
		"search" : true,
		"type" : "date-time-range",
		"sort" : true,
		"key" : "searchProNumber",
		"prefWidth" : "100"
	}, {
		"name" : "Volume (KG)",
		"width" : "w50px text-right",
		"model" : "volume",
		"search" : true,
		"type" : "number",
		"sort" : true,
		"key" : "searchProDate",
		"prefWidth" : "50"
	}, {
		"name" : "Delay Code",
		"width" : "w25px",
		"model" : "delayCode",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchShipper",
		"prefWidth" : "50"
	},  {
		"name" : "Truck No",
		"width" : "w75px",
		"model" : "truckNo",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchOnHand",
		"prefWidth" : "150"
	}, {
		"name" : "Driver Name",
		"width" : "w100px",
		"model" : "driverName",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchTrucker",
		"prefWidth" : "200"
	}]
    
    $scope.cfsHeadValues = [ {
		"#" : "1",
		"jobNumber" : "SHP000301",
		"pieces" : "1",
		"grsWeight" : "209.00",
		"inGateTime" : "12-07-2017 10:48",
		"outGateTime" : "12-07-2017 21:48",
		"volume" : "230.00",
		"delayCode" : "01",
		"boe" : "269874602589",
		"decNo" : "526987460255",
		"truckNo" : "TN 22 BL 2017",
		"driverName" : "Kishore",
		"note" : "Received"
		
	},
	{
		"#" : "2",
		"jobNumber" : "SHP000269",
		"pieces" : "5",
		"grsWeight" : "220.00",
		"inGateTime" : "05-07-2017 10:48",
		"outGateTime" : "05-07-2017 11:48",
		"volume" : "360.00",
		"delayCode" : "05",
		"boe" : "269874602555",
		"decNo" : "526987460256",
		"truckNo" : "TN 22 BL 2015",
		"driverName" : "Saran",
		"note" : "Received"
		
	},
	{
		"#" : "3",
		"jobNumber" : "SHP000265",
		"pieces" : "10",
		"grsWeight" : "19.00",
		"inGateTime" : "11-06-2017 11:48",
		"outGateTime" : "11-06-2017 12:48",
		"volume" : "20.00",
		"delayCode" : "06",
		"boe" : "269874602689",
		"decNo" : "526987460205",
		"truckNo" : "TN 22 BL 2016",
		"driverName" : "Sampath",
		"note" : "Received"
		
	},
	{
		"#" : "4",
		"jobNumber" : "SHP000200",
		"pieces" : "4",
		"grsWeight" : "129.00",
		"inGateTime" : "07-06-2017 13:01",
		"outGateTime" : "07-06-2017 15:02",
		"volume" : "10.00",
		"delayCode" : "03",
		"boe" : "269874602581",
		"decNo" : "526987460250",
		"truckNo" : "TN 22 BL 2015",
		"driverName" : "Saran",
		"note" : "Received"
		
	},
	{
		"#" : "5",
		"jobNumber" : "SHP000199",
		"pieces" : "2",
		"grsWeight" : "29.00",
		"inGateTime" : "08-06-2017 15:01",
		"outGateTime" : "08-06-2017 18:02",
		"volume" : "12.00",
		"delayCode" : "04",
		"boe" : "269874602579",
		"decNo" : "526987460225",
		"truckNo" : "TN 22 BL 2005",
		"driverName" : "Cheran",
		"note" : "Received"
		
	},
	{
		"#" : "6",
		"jobNumber" : "SHP000198",
		"pieces" : "6",
		"grsWeight" : "150.00",
		"inGateTime" : "07-06-2017 13:01",
		"outGateTime" : "07-06-2017 15:02",
		"volume" : "8.00",
		"delayCode" : "02",
		"boe" : "269874602489",
		"decNo" : "526987460125",
		"truckNo" : "TN 22 BL 2015",
		"driverName" : "Saran",
		"note" : "Received"
		
	},
	{
		"#" : "7",
		"jobNumber" : "SHP000197",
		"pieces" : "2",
		"grsWeight" : "109.00",
		"inGateTime" : "04-06-2017 05:01",
		"outGateTime" : "04-06-2017 07:02",
		"volume" : "10.00",
		"delayCode" : "03",
		"boe" : "269874602289",
		"decNo" : "526987462255",
		"truckNo" : "TN 22 BL 2017",
		"driverName" : "Kishore",
		"note" : "Received"
		
	},
	{
		"#" : "8",
		"jobNumber" : "SHP000188",
		"pieces" : "4",
		"grsWeight" : "129.00",
		"inGateTime" : "07-06-2017 13:01",
		"outGateTime" : "07-06-2017 15:02",
		"volume" : "10.00",
		"delayCode" : "03",
		"boe" : "269874602222",
		"decNo" : "526987460201",
		"truckNo" : "TN 22 BL 2015",
		"driverName" : "Saran",
		"note" : "Received"
		
	},
	{
		"#" : "9",
		"jobNumber" : "SHP000187",
		"pieces" : "2",
		"grsWeight" : "29.00",
		"inGateTime" : "02-06-2017 15:01",
		"outGateTime" : "02-06-2017 18:02",
		"volume" : "12.00",
		"delayCode" : "04",
		"boe" : "269874602268",
		"decNo" : "526987460223",
		"truckNo" : "TN 22 BL 2005",
		"driverName" : "Cheran",
		"note" : "Received"
		
	},
	{
		"#" : "10",
		"jobNumber" : "SHP000186",
		"pieces" : "4",
		"grsWeight" : "129.00",
		"inGateTime" : "07-06-2017 13:01",
		"outGateTime" : "07-06-2017 15:02",
		"volume" : "10.00",
		"delayCode" : "03",
		"boe" : "269874602269",
		"decNo" : "526987460215",
		"truckNo" : "TN 22 BL 2015",
		"driverName" : "Saran",
		"note" : "Received"
		
	}]

    
    //moreview button
    var viewNoteModal;
    $scope.moreviewmodel = function() {
  
    	viewNoteModal= $modal({
         scope: $scope,
         backdrop: 'static',
         templateUrl: 'app/components/air/cfs_delivery_entry/dialog/cfs_delivery_notes_view_popup.html',
         show: false
     });
    	viewNoteModal.$promise.then(viewNoteModal.show)
    };
	$scope.selectedRecordIndex = 0;

    $scope.sortSelection = {
            sortKey:"receiptNumber",
            sortOrder:"desc"
    };

    $scope.rowSelect = function(data, index){
    	if($rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_VIEW)){
        $scope.selectedRowIndex =  ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
	    var param = {
	    		cfsId   : data.id,
	    		cfsIndex   : index,
	    		selectedPageNumber : $scope.selectedRowIndex,
	    		totalRecord : $scope.totalRecord
	    };
	    $state.go("layout.viewAirCFSDelivery", $scope.searchDtoToStateParams(param));
	 }
    };
	
	 $scope.searchDtoToStateParams = function(param) {
	    	if(param == undefined || param == null) {
	    		param = {};
	    	}
	    	if($scope.searchDto != undefined && $scope.searchDto != null) {
	    		if($scope.searchDto != undefined && $scope.searchDto != null) {
	        		param.recordPerPage = 1;
	    			if($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
	            		param.sortByColumn = $scope.searchDto.sortByColumn; 
	            	}
	    			if($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
	            		param.orderByType = $scope.searchDto.orderByType; 
	            	}
	    			
	    			if($scope.searchDto.searchReceiptNumber != undefined && $scope.searchDto.searchReceiptNumber != null) {
	            		param.searchReceiptNumber = $scope.searchDto.searchReceiptNumber; 
	            	}
	    			if($scope.searchDto.searchReceiptDate != undefined && $scope.searchDto.searchReceiptDate != null) {
	            		param.searchReceiptDate = $scope.searchDto.searchReceiptDate; 
	            	}
	    			if($scope.searchDto.searchCSF != undefined && $scope.searchDto.searchCSF != null) {
	            		param.searchCSF = $scope.searchDto.searchCSF; 
	            	}
	    			if($scope.searchDto.searchShipmentUid != undefined && $scope.searchDto.searchShipmentUid != null) {
	            		param.searchShipmentUid = $scope.searchDto.searchShipmentUid; 
	            	}
	    			if($scope.searchDto.searchProNumber != undefined && $scope.searchDto.searchProNumber != null) {
	            		param.searchProNumber = $scope.searchDto.searchProNumber; 
	            	}
	    			if($scope.searchDto.searchProDate != undefined && $scope.searchDto.searchProDate != null) {
	            		param.searchProDate = $scope.searchDto.searchProDate; 
	            	}
	    			if($scope.searchDto.searchShipper != undefined && $scope.searchDto.searchShipper != null) {
	            		param.searchShipper = $scope.searchDto.searchShipper; 
	            	}
	    			if($scope.searchDto.searchConsignee != undefined && $scope.searchDto.searchConsignee != null) {
	            		param.searchConsignee = $scope.searchDto.searchConsignee; 
	            	}
	    			if($scope.searchDto.searchShipperRefNumber != undefined && $scope.searchDto.searchShipperRefNumber != null) {
	            		param.searchShipperRefNumber = $scope.searchDto.searchShipperRefNumber; 
	            	}
	    			if($scope.searchDto.searchOnHand != undefined && $scope.searchDto.searchOnHand != null) {
	            		param.searchOnHand = $scope.searchDto.searchOnHand; 
	            	}
	    			if($scope.searchDto.searchTrucker != undefined && $scope.searchDto.searchTrucker != null) {
	            		param.searchTrucker = $scope.searchDto.searchTrucker; 
	            	}
	    			
	        	}
	    		
	    	}
	    	return param;
	    }

	  $scope.singlePageNavigation = function(val) {
	    	if($stateParams != undefined && $stateParams != null) {
	    		
	            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
	                return;
	            
	            var stateParameters = {};
	            $scope.searchDto = {};
	            $scope.searchDto.recordPerPage = 1; 
	    		$scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

				if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
					stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn; 
	        	}
				if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
					stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType; 
	        	}
				if($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
					stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage; 
	        	}
				if($stateParams.searchReceiptNumber != undefined && $stateParams.searchReceiptNumber != null) {
	        		stateParameters.searchReceiptNumber = $scope.searchDto.searchReceiptNumber = $stateParams.searchReceiptNumber; 
	        	}
				if($stateParams.searchReceiptDate != undefined && $stateParams.searchReceiptDate != null) {
	        		stateParameters.searchReceiptDate = $scope.searchDto.searchReceiptDate = $stateParams.searchReceiptDate; 
	        	}
				if($stateParams.searchCSF != undefined && $stateParams.searchCSF != null) {
	        		stateParameters.searchCSF = $scope.searchDto.searchCSF = $stateParams.searchCSF; 
	        	}
				if($stateParams.searchShipmentUid != undefined && $stateParams.searchShipmentUid != null) {
	        		stateParameters.searchShipmentUid = $scope.searchDto.searchShipmentUid = $stateParams.searchShipmentUid; 
	        	}
				if($stateParams.searchProNumber != undefined && $stateParams.searchProNumber != null) {
	        		stateParameters.searchProNumber = $scope.searchDto.searchProNumber = $stateParams.searchProNumber; 
	        	}
				if($stateParams.searchProDate != undefined && $stateParams.searchProDate != null) {
	        		stateParameters.searchProDate = $scope.searchDto.searchProDate = $stateParams.searchProDate; 
	        	}
				if($stateParams.searchShipper != undefined && $stateParams.searchShipper != null) {
	        		stateParameters.searchShipper = $scope.searchDto.searchShipper = $stateParams.searchShipper; 
	        	}
				if($stateParams.searchConsignee != undefined && $stateParams.searchConsignee != null) {
	        		stateParameters.searchConsignee = $scope.searchDto.searchConsignee = $stateParams.searchConsignee; 
	        	}
				if($stateParams.searchShipperRefNumber != undefined && $stateParams.searchShipperRefNumber != null) {
	        		stateParameters.searchShipperRefNumber = $scope.searchDto.searchShipperRefNumber = $stateParams.searchShipperRefNumber; 
	        	}
				if($stateParams.searchOnHand != undefined && $stateParams.searchOnHand != null) {
	        		stateParameters.searchOnHand = $scope.searchDto.searchOnHand = $stateParams.searchOnHand; 
	        	}
				if($stateParams.searchTrucker != undefined && $stateParams.searchTrucker != null) {
	        		stateParameters.searchTrucker = $scope.searchDto.searchTrucker = $stateParams.searchTrucker; 
	        	}
				
	    	} else {
	    		return;
	    	}
	    	CFSReceiveSearch.query($scope.searchDto).$promise.then(function(
					data, status) {
				$scope.totalRecord = data.responseObject.totalRecord;
				
				stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
	            stateParameters.cfsId = data.responseObject.searchResult[0].id;
	            stateParameters.totalRecord = $scope.totalRecord; 
	            $state.go("layout.viewAirCFS", stateParameters);
			});
	    }
	

    $scope.limitChange = function(item){
    	
    	console.log("Limit Change is called ", item);
    	
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    };
    
    $scope.changePage = function(param) {
    	
    	console.log("Change Page is called ", param);
    	
		$scope.page = param.page;
		$scope.limit = param.size;

		$scope.search();
	};
    
	$scope.changeSearch = function(param) {
		console.log("change Search is called ", param);
		for (var attrname in param){
			$scope.searchDto[attrname] = param[attrname];
		}
        $scope.page = 0;
        $scope.search();
	}
    
    $scope.sortChange = function(param){
    	console.log("sort Change is called ", param);
		$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.searchDto.sortByColumn = param.sortKey;
		$scope.search();
	}
    
    $scope.search=  function(){
		
		console.log("Search is called.");

		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;
		
		 $scope.tmpSearchDto = cloneService.clone($scope.searchDto);
		 
		 if($scope.tmpSearchDto.searchReceiptDate != null) {
			 $scope.tmpSearchDto.searchReceiptDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchReceiptDate.startDate);
	         $scope.tmpSearchDto.searchReceiptDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchReceiptDate.endDate);
		 }
		
		 if($scope.tmpSearchDto.searchProDate != null) {
			 $scope.tmpSearchDto.searchProDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.searchProDate.startDate);
	         $scope.tmpSearchDto.searchProDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.searchProDate.endDate);
		 }
		
		$scope.cfsArrList = [];
		CFSReceiveSearch.query($scope.tmpSearchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			console.log("$scope.totalRecord  ", $scope.totalRecord);
			$scope.cfsArrList = data.responseObject.searchResult;
			console.log("$scope.cfsArrList ", $scope.cfsArrList);
		});

	}
    
    
    $scope.limitArr = [10,15,20];
	$scope.page = 0;
	$scope.limit = 10;
    $scope.init = function () {
    	console.log("Init method called.");
		$scope.cfsReceiveEntry = {};
		$scope.showHistory = false;
		
		$scope.totalRecord=10;
		$scope.detailTab='active';
		$scope.searchDto = {};
		$scope.search();
    }
  
    
    $scope.setSearch = function(tab) {
    	console.log("Tab Selected " +tab);
    	$scope.search();
    }

    /* Detail Page */
    
    $scope.back = function() {
    	 console.log("Back is pressed...");
    	 
    	 
    	 if($stateParams.fromState != undefined && $stateParams.fromState != null) {
    		 console.log("$stateParams.fromStateParams", $stateParams.fromStateParams);
    		 $state.go($stateParams.fromState, {shipmentId : $stateParams.fromStateParams});
    	 } else {
    		 $state.go("layout.airCFS");
    	 }
	};
	
	$scope.add = function(){
		if($rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_CREATE)){
			console.log("Add CFS Receive Entry button is pressed......");
			$state.go("layout.addAirCFSdelivery");
		}
	}
	
	$scope.editCFS = function() {
		if($rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_MODIFY)){
		console.log("Edit Button is Pressed....")
        var localRefStateParam = {
			cfsId: $scope.cfsReceiveEntry.id,
			serviceUid : $scope.cfsReceiveEntry.serviceUid,
        };

		var ishistoryFounded = false;
        if($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0 ) {
    		for(var iIndex = 0; iIndex < $rootScope.unfinishedFormHistoryList.length; iIndex++) {
    			var tit = "CFS Receive Entry Edit # "+ $scope.cfsReceiveEntry.id;
    			if($rootScope.unfinishedFormHistoryList[iIndex].title == tit) {
	    			var unSavedData = $rootScope.unfinishedFormHistoryList[iIndex];
	                GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[iIndex], undefined);// $rootScope.unfinishedFormHistoryList.push(stateDataObject);
                    ishistoryFounded = true;
            	} 
            	if(ishistoryFounded) {
            		break;
            	}
    		}
        } 
        if(!ishistoryFounded){
        	$state.go("layout.addAirCFSdelivery", localRefStateParam);
        }
      }
	}
	
	$scope.clickOnTab = function(tab){
		if(tab=='dimension'&& $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_DIMENSION_VIEW)){
			$scope.moreTab=tab;
		}
		if(tab=='attachment'&& $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_ATTACHMENT_VIEW)){
			$scope.moreTab=tab;
		}
		if(tab=='notes'&& $rootScope.roleAccess(roleConstant.AIR_CFS_RECIEVE_ENTRY_NOTES_VIEW)){
			$scope.moreTab=tab;
		}
	}
	
	$scope.remove = function() {
		console.log("Remove button is pressed.....");
	}

    
    
    $scope.view = function(id) {
    	
    	console.log("View is called ", id);

    	CFSReceiveGet.get({id: id}, function(data) {
    		  if (data.responseCode == "ERR0") {
    			  $scope.cfsReceiveEntry = data.responseObject;
    			  console.log("CFSReceiveGet Successful ", $scope.cfsReceiveEntry);
    			  
    			  if ($scope.cfsReceiveEntry != undefined && $scope.cfsReceiveEntry != null && $scope.cfsReceiveEntry.cfsMaster != undefined && $scope.cfsReceiveEntry.cfsMaster != null && $scope.cfsReceiveEntry.cfsMaster.id != undefined ) {
    			        $rootScope.unfinishedFormTitle = "CFS Receive Entry # " + $scope.cfsReceiveEntry.cfsReceiptNumber;
    			        $rootScope.unfinishedData = undefined;
	                    $rootScope.subTitle = $scope.cfsReceiveEntry.cfsMaster.cfsName;
	                   // $scope.populateAttachment();
    			  }
	                
	                var rHistoryObj = {
	                    'title': 'CFS Receive Entry View #' + $scope.cfsReceiveEntry.id,
	                    'subTitle': $rootScope.subTitle,
	                    'stateName': $state.current.name,
	                    'stateParam': JSON.stringify($stateParams),
	                    'stateCategory': 'CFS Receive Entry',
	                    'serviceType': 'AIR'
	                }

	                RecentHistorySaveService.form(rHistoryObj);
	                
	            	if($stateParams.fromState != undefined && $stateParams.fromState != null) {
	    				$scope.showButtonFlag = true;  
	    			} else {
	    				$scope.showButtonFlag = false;
	    			}
	    			
    			  
    		  } else {
    			  console.log("CFSReceiveGet Error ", data.responseDescription);
    		  }
    	}, function(error) {
            console.log("CFSReceiveGet failed " ,error)
        });
    
    
    }
	
	
	
	$scope.attachConfig = {
			"isEdit":false,  //false for view only mode
			"isDelete":false, //
			"columnDefs":[
				{"name":"Reference No", "model":"refNo","type":"text",width:"inl_l"},
				{"name":"File Name", "model":"fileName","type":"file"}
			]
		}
	
	$scope.downloadAttach = function(param) {
		console.log(param);
		console.log(param.data.id);
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL",param.data.file);
            downloadFactory.downloadAttachment('/api/v1/cfsreceive/files/', param.data.id, param.data.fileName);
        } 
    }
	
	$scope.goToShipmentViewByServiceUid = function (serviceuid){
		console.log("State  : ",$state)
		console.log("State Param  : ",$stateParams)
		console.log("serviceuid ",serviceuid)
		ShipmentIdByServiceUid.get({
			serviceuid :serviceuid
		},function(data) {
				if (data.responseCode == 'ERR0') {
					var param = {
							shipmentId:data.responseObject,
							fromState: $state.current.name,
							fromStateParams:JSON.stringify($stateParams)
							
					}
					$state.go("layout.viewCrmShipment", param);
				}
			}, 
			function(error) {
				console.log("Shipment get Failed : " + error)
			});
		
	}
		
    switch ($stateParams.action) {
	    case "VIEW":
	      
	        if($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
				$scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
			}
			if($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
				$scope.totalRecord = parseInt($stateParams.totalRecord);
			}
			
	        
	        $scope.view($stateParams.cfsId);
	        
	        $rootScope.breadcrumbArr = [
	                                    {label:"Air", state:"layout.airCFSdelivery"}, 
	                                    {label:"CFS Receive Entry", state:"layout.airCFSdelivery"},
	                                    {label:"View CFS Receive Entry", state:null}
	                                   ];
	        break;
	    case "SEARCH":
	        console.log("CFS Receive Entry Search Page");
	        $scope.init();
	        
	        $rootScope.breadcrumbArr = [
	                                    {label:"Air", state:"layout.airCFSdelivery"}, 
	                                    {label:"CFS Receive Entry", state:null}
	                                   ];
	        break;
    }				
});
