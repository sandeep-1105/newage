/**
 * 
 */
app.service('CfsService',function($rootScope,$state){

	this.calculateWeight = function(param, cfsReceive) {
		
		console.log("Param : ", param);
		
        if (param.grossWeight > 0.0 && param.volumeWeight > 0.0) {
            if (cfsReceive.dimensionUnit) {
            	cfsReceive.grossWeight = $rootScope.roundOffWeight(param.dimensionGrossWeightInKg);
            	cfsReceive.grossWeightInPound = param.grossWeight; 
            } else {
            	cfsReceive.grossWeight = $rootScope.roundOffWeight(param.grossWeight);
            	cfsReceive.grossWeightInPound = Math.round((cfsReceive.grossWeight * 2.20462) * 100)/100;  
            }

            cfsReceive.dimensionVolumeWeight=param.volumeWeight;
            cfsReceive.dimensionGrossWeight=param.grossWeight;
            cfsReceive.dimensionGrossWeightInKg=param.dimensionGrossWeightInKg;
    		
    		
            cfsReceive.volumeWeight = $rootScope.roundOffWeight(param.volumeWeight);
            cfsReceive.volumeWeightInPound = Math.round((cfsReceive.volumeWeight * 2.20462) * 100)/100; 
            cfsReceive.noOfPieces = param.totalPieces;
            this.calculateChargeWeight(cfsReceive);
        }
    }
	
	this.calculateChargeWeight = function(cfsReceive){
		
		var volumeKg = parseFloat(cfsReceive.volumeWeight);
		var grossKg = parseFloat(cfsReceive.grossWeight);
		
		if(volumeKg>0 && grossKg>0){
			
			if(volumeKg>grossKg){
				cfsReceive.chargebleWeight = volumeKg;
			}else{
				cfsReceive.chargebleWeight = grossKg;
			}
			
		}else if(volumeKg<=0 && grossKg>0){
			cfsReceive.chargebleWeight = grossKg;
		}else if(volumeKg>0 && grossKg<=0){
			cfsReceive.chargebleWeight = volumeKg;
		}else{
			cfsReceive.chargebleWeight = 0.0;
		}
		
		
		if(cfsReceive.chargebleWeight>0){
			var conversionUnit = null;
			if(cfsReceive.shipmentServiceDetail!=undefined&&cfsReceive.shipmentServiceDetail!=null&&cfsReceive.shipmentServiceDetail.serviceMaster!=undefined&&cfsReceive.shipmentServiceDetail.serviceMaster!=null){
				if(cfsReceive.shipmentServiceDetail.serviceMaster.transportMode==='Air'){
					conversionUnit = $rootScope.appMasterData['air.cubic.meter'];
				}else if(cfsReceive.shipmentServiceDetail.serviceMaster.transportMode==='Road'){
					conversionUnit = $rootScope.appMasterData['road.cubic.meter'];
				}else if(cfsReceive.shipmentServiceDetail.serviceMaster.transportMode==='Ocean'){
					conversionUnit = $rootScope.appMasterData['ocean.cubic.meter'];
				}	
			}
			
			
			if(conversionUnit!=null){
				cfsReceive.bookedVolumeUnitCbm = parseFloat(cfsReceive.volumeWeight)/parseFloat(conversionUnit);
				cfsReceive.bookedVolumeUnitCbm = $rootScope.weightFormat(cfsReceive.bookedVolumeUnitCbm,cfsReceive.location.countryMaster.locale);
			}
		}else{
			cfsReceive.bookedVolumeUnitCbm = 0.0;
		}
		
	}
	
	this.copyFromService = function(cfsReceiveEntry, service) {
		
		if(service != undefined && service != null && service.documentList[0] != undefined && service.documentList[0] != null) {
	
			 cfsReceiveEntry.shipper = service.documentList[0].shipper;
	         cfsReceiveEntry.consignee = service.documentList[0].consignee;
	         cfsReceiveEntry.noOfPieces = service.documentList[0].noOfPieces;
	         cfsReceiveEntry.dimensionUnit = service.documentList[0].dimensionUnit;
	         cfsReceiveEntry.dimensionUnitValue = service.documentList[0].dimensionUnitValue;
	         cfsReceiveEntry.chargebleWeight = service.documentList[0].chargebleWeight;
	         cfsReceiveEntry.grossWeight = service.documentList[0].grossWeight;
	         cfsReceiveEntry.volumeWeight = service.documentList[0].volumeWeight;
	         cfsReceiveEntry.volumeWeightInPound = service.documentList[0].volumeWeightInPound;
	         cfsReceiveEntry.grossWeightInPound = service.documentList[0].grossWeightInPound;
	         cfsReceiveEntry.bookedVolumeUnitCbm = service.documentList[0].bookedVolumeUnitCbm;
	         cfsReceiveEntry.dimensionGrossWeight = service.documentList[0].dimensionGrossWeight;
	         cfsReceiveEntry.dimensionVolumeWeight = service.documentList[0].dimensionVolumeWeight;
	         cfsReceiveEntry.dimensionGrossWeightInKg = service.documentList[0].dimensionGrossWeightInKg;
	         cfsReceiveEntry.cfsDimensionList = [];
	         if(service.documentList[0].dimensionList != undefined && service.documentList[0].dimensionList != null && service.documentList[0].dimensionList.length > 0) {
	         	   for(var i = 0; i < service.documentList[0].dimensionList.length; i++) {
	                	var dimension = {};
	                	dimension.noOfPiece = service.documentList[0].dimensionList[i].noOfPiece;
	                	dimension.length = service.documentList[0].dimensionList[i].length;
	                	dimension.width = service.documentList[0].dimensionList[i].width;
	                	dimension.height = service.documentList[0].dimensionList[i].height;
	                	dimension.volWeight = service.documentList[0].dimensionList[i].volWeight;
	                	dimension.grossWeight = service.documentList[0].dimensionList[i].grossWeight;
	                	dimension.grossWeightKg = service.documentList[0].dimensionList[i].grossWeightKg;
	                	cfsReceiveEntry.cfsDimensionList.push(dimension);
	                }
	         } else {
	         	cfsReceiveEntry.cfsDimensionList.push({});
	         }
			
		}
	}
});

