(function() {

	app.factory("AirlinePrebookingSave",['$resource', function($resource) {
		return $resource("/api/v1/airline_prebooking/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AirlinePrebookingSearch",['$resource', function($resource) {
		return $resource("/api/v1/airline_prebooking/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("AirlinePrebookingView",['$resource', function($resource) {
		return $resource("/api/v1/airline_prebooking/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

})();
