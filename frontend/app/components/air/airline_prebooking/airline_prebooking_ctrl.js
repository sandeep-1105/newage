app.controller("AirlinePrebookingController",['$stateParams', '$state', '$scope', '$rootScope', 'AirlinePrebookingSearch', 'AirlinePrebookingView', 'roleConstant',

    function($stateParams, $state, $scope, $rootScope, AirlinePrebookingSearch, AirlinePrebookingView, roleConstant) {

    $scope.limitArr = [10, 15, 20];

    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 10;

    $scope.limitArrActive = [10, 15, 20];

    $scope.pageActive = 0;
    $scope.limitActive = 10;
    $scope.totalRecordActive = 10;

    $scope.searchDto = {};
    $scope.activeSearchDto = {};



    $scope.init = function() {

        console.log("init method is called");

        $scope.detailTab = 'active';
        $scope.activeSearchDto = {};
        $scope.activeSearch();

    }

    $scope.tabChange = function(tabName) {

        $scope.pageActive = 0;
        $scope.page = 0;

        if (tabName == 'active') {
            $scope.activeSearchDto = {};
            $scope.activeSearch();
        } else {
            $scope.searchDto = {};
            $scope.search();
        }

        $scope.detailTab = tabName;

    }


    $scope.searchHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,

        },
        {
            "name": "PreBooking #",
            "search": true,
            "model": "prebookingNumber",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "200",
            "key": "prebookingNumber",
        },
        {
            "name": "Date",
            "model": "prebookingDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w75px",
            "prefWidth": "150",
            "key": "prebookingDate"
        },
        {
            "name": "Carrier",
            "model": "carrierName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w100px",
            "prefWidth": "200",
            "key": "carrierName"
        },
        {
            "name": "Service",
            "model": "serviceName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "serviceName"

        },
        {
            "name": "Origin",
            "model": "porName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "porName"
        }, {
            "name": $rootScope.appMasterData['Party_Label'],
            "model": "partyName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "partyName"
        }, {
            "name": "No of pieces",
            "model": "noOfPieces",
            "search": true,
            "wrap_cell": true,
            "type": "number",
            "sort": true,
            "width": "w25px",
            "prefWidth": "150",
            "key": "noOfPieces"
        }, {
            "name": "Volume(CBM)",
            "model": "volume",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w25px",
            "prefWidth": "150",
            "key": "volume"
        }, {
            "name": "Gross Weight(KG)",
            "model": "grossWeight",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w25px",
            "prefWidth": "150",
            "key": "grossWeight"
        },
        {
            "name": "Volume Weight(KG)",
            "model": "volumeWeight",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w25px",
            "prefWidth": "150",
            "key": "volumeWeight"
        },
        {
            "name": "No of MAWBs",
            "model": "noOfMawb",
            "search": true,
            "wrap_cell": true,
            "type": "number",
            "sort": true,
            "width": "w25px",
            "prefWidth": "150",
            "key": "noOfMawb"
        }

    ]

    $scope.activeSortSelection = {
        sortKey: "prebookingNumber",
        sortOrder: "desc"
    }

    $scope.searchSortSelection = {
        sortKey: "prebookingNumber",
        sortOrder: "desc"
    }

    $scope.changePageActive = function(param) {
        $scope.pageActive = param.page;
        $scope.limitActive = param.size;
        $scope.cancel();
        $scope.activeSearch();
    }

    $scope.limitChangeActive = function(item) {
        $scope.pageActive = 0;
        $scope.limitActive = item;
        $scope.cancel();
        $scope.activeSearch();
    }

    $scope.changePage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.cancel();
        $scope.search();
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.cancel = function() {
        $scope.showDetail = false;
    }


    $scope.activeSearch = function() {

        $scope.activeSearchDto.selectedPageNumber = $scope.pageActive;
        $scope.activeSearchDto.recordPerPage = $scope.limitActive;

        if ($scope.activeSearchDto.prebookingDate != null) {
            $scope.activeSearchDto.prebookingDate.startDate = $rootScope.sendApiStartDateTime($scope.activeSearchDto.prebookingDate.startDate);
            $scope.activeSearchDto.prebookingDate.endDate = $rootScope.sendApiEndDateTime($scope.activeSearchDto.prebookingDate.endDate);
        }

        if ($scope.activeSearchDto.volume == undefined || $scope.activeSearchDto.volume == "" || $scope.activeSearchDto.volume.trim().length == 0) {
            $scope.activeSearchDto.volume = null;
        }
        if ($scope.activeSearchDto.grossWeight == undefined || $scope.activeSearchDto.grossWeight == "" || $scope.activeSearchDto.grossWeight.trim().length == 0) {
            $scope.activeSearchDto.grossWeight = null;
        }
        if ($scope.activeSearchDto.volumeWeight == undefined || $scope.activeSearchDto.volumeWeight == "" || $scope.activeSearchDto.volumeWeight.trim().length == 0) {
            $scope.activeSearchDto.volumeWeight = null;
        }
        if ($scope.activeSearchDto.noOfPieces == undefined || $scope.activeSearchDto.noOfPieces == "" || $scope.activeSearchDto.noOfPieces.trim().length == 0) {
            $scope.activeSearchDto.noOfPieces = null;
        }
        if ($scope.activeSearchDto.noOfMawb == undefined || $scope.activeSearchDto.noOfMawb == "" || $scope.activeSearchDto.noOfMawb.trim().length == 0) {
            $scope.activeSearchDto.noOfMawb = null;
        }

        $scope.airlinePrebookingArray = [];
        AirlinePrebookingSearch.query($scope.activeSearchDto).$promise.then(function(data, status) {
            $scope.totalRecordActive = data.responseObject.totalRecord;

            $scope.airlinePrebookingArray = [];
            angular.forEach(data.responseObject.searchResult, function(tempObj) {
                tempObj.volume = $rootScope.weightFormat(tempObj.volume, tempObj.locale);
                tempObj.grossWeight = $rootScope.weightFormat(tempObj.grossWeight, tempObj.locale);
                tempObj.volumeWeight = $rootScope.weightFormat(tempObj.volumeWeight, tempObj.locale);
                $scope.airlinePrebookingArray.push(tempObj);
            });


            console.log("data ", $scope.airlinePrebookingArray);
        });
    }




    $scope.back = function() {
        $state.go("layout.airlinePrebooking");
    };


    $scope.editAirlinePrebooking = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_MODIFY)) {
            console.log("EditAirlinePrebooking is called....");
            $state.go("layout.editAirlinePrebooking", {
                prebookingId: $scope.airlinePrebooking.id
            });
        }
    }



    $scope.addAirlinePrebooking = function() {
        if ($rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_CREATE)) {
            console.log('addAirlinePrebooking is called');
            $state.go('layout.addAirlinePrebooking');
        }
    };


    $scope.changeSearchActive = function(param) {
        console.log("param", param);
        for (var attrname in param) {
            $scope.activeSearchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.activeSearch();
    }

    $scope.changeSearch = function(param) {
        console.log("param", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.sortChangeActive = function(param) {
        $scope.activeSearchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.activeSearchDto.sortByColumn = param.sortKey;
        $scope.activeSearch();
    }

    $scope.clickOnTabView = function(tab) {
        if (tab == 'reservedMAWB' && $rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_RESERVED_MAWB_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'notes' && $rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_NOTES_VIEW)) {
            $scope.Tabs = tab;
        }
    }

    $scope.rowSelectActive = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_VIEW)) {
            console.log("rowSelect is called....");
            $scope.selectedRowIndex = ($scope.activeSearchDto.recordPerPage * $scope.activeSearchDto.selectedPageNumber) + index;
            var param = {
                id: data.id,
                selectedPageNumber: $scope.selectedRowIndex,
                totalRecord: $scope.totalRecord
            };
            console.log("State Parameters : ", param);
            $state.go("layout.viewAirlinePrebooking", $scope.searchDtoToStateParams(param));
        }
    }


    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //   param.selectedPageNumber = param.enqIdx; 
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                //Reference No
                if ($scope.searchDto.searchRefNo != undefined && $scope.searchDto.searchRefNo != null) {
                    param.searchRefNo = $scope.searchDto.searchRefNo;
                }
                //QUotation Valid From Date Between
                if ($scope.searchDto.searchValidFrom != undefined && $scope.searchDto.searchValidFrom != null) {
                    param.searchValidFromStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidFrom.startDate);
                    param.searchValidFromEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidFrom.endDate);
                }
                //QUotation Valid To Date Between
                if ($scope.searchDto.searchValidTo != undefined && $scope.searchDto.searchValidTo != null) {
                    param.searchValidToStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidTo.startDate);
                    param.searchValidToEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchValidTo.endDate);
                }
                //QUotation Logged On Date Between
                if ($scope.searchDto.searchLoggedOn != undefined && $scope.searchDto.searchLoggedOn != null) {
                    param.searchLoggedOnStartDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchLoggedOn.startDate);
                    param.searchLoggedOnEndDate = $rootScope.sendApiStartDateTime($scope.searchDto.searchLoggedOn.endDate);
                }
                if ($scope.searchDto.importExport != undefined && $scope.searchDto.importExport != null) {
                    param.importExport = $scope.searchDto.importExport;
                }
                if ($scope.searchDto.status != undefined && $scope.searchDto.status != null) {
                    param.status = $scope.searchDto.status;
                }
                if ($scope.searchDto.transportMode != undefined && $scope.searchDto.transportMode != null) {
                    param.transportMode = $scope.searchDto.transportMode;
                }
                if ($scope.searchDto.searchLoggedBy != undefined && $scope.searchDto.searchLoggedBy != null) {
                    param.searchLoggedBy = $scope.searchDto.searchLoggedBy;
                }
                if ($scope.searchDto.searchSalesman != undefined && $scope.searchDto.searchSalesman != null) {
                    param.searchSalesman = $scope.searchDto.searchSalesman;
                }
                if ($scope.searchDto.searchSalesCordinator != undefined && $scope.searchDto.searchSalesCordinator != null) {
                    param.searchSalesCordinator = $scope.searchDto.searchSalesCordinator;
                }
                if ($scope.searchDto.searchShipper != undefined && $scope.searchDto.searchShipper != null) {
                    param.searchShipper = $scope.searchDto.searchShipper;
                }
                if ($scope.searchDto.searchPortDestination != undefined && $scope.searchDto.searchPortDestination != null) {
                    param.searchPortDestination = $scope.searchDto.searchPortDestination;
                }
                if ($scope.searchDto.searchPortOrigin != undefined && $scope.searchDto.searchPortOrigin != null) {
                    param.searchPortOrigin = $scope.searchDto.searchPortOrigin;
                }
                if ($scope.searchDto.searchQuotationNo != undefined && $scope.searchDto.searchQuotationNo != null) {
                    param.searchQuotationNo = $scope.searchDto.searchQuotationNo;
                }
                if ($scope.searchDto.searchCustomerName != undefined && $scope.searchDto.searchCustomerName != null) {
                    param.searchCustomerName = $scope.searchDto.searchCustomerName;
                }
                if ($scope.searchDto.searchServiceName != undefined && $scope.searchDto.searchServiceName != null) {
                    param.searchServiceName = $scope.searchDto.searchServiceName;
                }
            }

        }
        return param;
    }

    $scope.view = function(prebookingId) {
        AirlinePrebookingView.get({
            id: prebookingId
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.airlinePrebooking = data.responseObject;
                console.log("Airline Prebooking : ", $scope.airlinePrebooking);
            } else {
                console.log("Description : ", data.responseDescription);
            }

        }, function(error) {
            console.log("Airline Prebooking View Failed : ", error)
        });
    }

    switch ($stateParams.action) {
        case "VIEW":

            console.log("Airline Prebooking View Page");
            $rootScope.unfinishedFormTitle = "Airline Prebooking Entry View # " + $stateParams.id;
            $rootScope.unfinishedData = undefined;

            if ($stateParams.id != undefined && $stateParams.id != null) {
                $scope.view($stateParams.id);
            }

            if ($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
                $scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
            }

            if ($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
                $scope.totalRecord = parseInt($stateParams.totalRecord);
            }

            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airlinePrebooking"
                },
                {
                    label: "Airline Prebooking Entry",
                    state: "layout.airlinePrebooking"
                },
                {
                    label: "View Airline Prebooking Entry",
                    state: null
                }
            ];

            break;
        case "SEARCH":
            console.log("Airline Prebooking Search Page....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Air",
                    state: "layout.airlinePrebooking"
                },
                {
                    label: "Airline Prebooking Entry",
                    state: "layout.airlinePrebooking"
                }
            ];
            break;
    }

}]);