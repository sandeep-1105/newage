app.controller("AirlinePrebookingEditController",['$rootScope', '$scope', '$stateParams', '$state', '$modal', 'ngDialog', 'CarrierByTransportMode', 'ServiceByTransportMode',
    'PortByTransportMode', 'CharterList', 'AirlinePrebookingSave', '$timeout', 'PartiesList', 'AirlinePrebookingView', 
    'appMetaFactory', 'roleConstant', 'ValidateUtil', 'CommonValidationService', 'appConstant', 'Notification',
    function($rootScope, $scope, $stateParams, $state, $modal, ngDialog, CarrierByTransportMode, ServiceByTransportMode,
    PortByTransportMode, CharterList, AirlinePrebookingSave, $timeout, PartiesList, AirlinePrebookingView, 
    appMetaFactory, roleConstant, ValidateUtil, CommonValidationService, appConstant, Notification) {


    $scope.disableSubmitBtn = false;

    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            $scope.disableSubmitBtn = false;
        }, 300);
    }
    $scope.selectedMawbs = [];
    $scope.selectedFlightPlans = [];

    $scope.fetchFightPlanScheduleData = {};
    $scope.fetchFightPlanScheduleData.ids = [];
    $scope.fetchStockMawbData = {};
    $scope.fetchStockMawbData.mawbList = [];

    $scope.fetchStockList = [];
    $scope.mawbList = [];
    $scope.statusList = ['booked'];

    $scope.init = function() {
        console.log("Init is called..........", $scope.airlinePrebooking);
    }

    $scope.directiveDateTimePickerOpts = angular.copy($rootScope.dateTimePickerOptions);
    $scope.directiveDateTimePickerOpts.widgetParent = "body";
    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";

    $scope.populateFetchRequiredDataChange = function(flagVar) {
        if (flagVar === 1 || flagVar === 0) {
            if ($scope.airlinePrebooking.carrierMaster != undefined && $scope.airlinePrebooking.carrierMaster != null &&
                $scope.airlinePrebooking.carrierMaster.id != undefined && $scope.airlinePrebooking.carrierMaster.id != null) {
                $scope.fetchFightPlanScheduleData.carrierId = $scope.airlinePrebooking.carrierMaster.id;
                $scope.fetchStockMawbData.carrierId = $scope.airlinePrebooking.carrierMaster.id;
            }

        }
        if (flagVar === 2 || flagVar === 0) {
            if ($scope.airlinePrebooking.por != undefined && $scope.airlinePrebooking.por != null &&
                $scope.airlinePrebooking.por.id != undefined && $scope.airlinePrebooking.por.id != null) {
                $scope.fetchFightPlanScheduleData.porId = $scope.airlinePrebooking.por.id;
                $scope.fetchStockMawbData.porId = $scope.airlinePrebooking.por.id;
            }
        }
        if (flagVar === 3 || flagVar === 0) {
            $scope.fetchStockMawbData.count = $scope.airlinePrebooking.noOfMawb;
            if ($scope.fetchStockMawbData.mawbList != undefined && $scope.fetchStockMawbData.mawbList != null && $scope.fetchStockMawbData.mawbList.length > 0) {
                $scope.fetchStockMawbData.count = $scope.airlinePrebooking.noOfMawb - $scope.fetchStockMawbData.mawbList.length;
                if ($scope.fetchStockMawbData.count < 1) {
                    $scope.fetchStockMawbData.count = 0;
                }
            }
        }
        $scope.fetchMawbAndFlightPlan();
    }

    $scope.afterSelectMawb = function() {

    }
    $scope.afterSelectFlightPlan = function() {

    }



    $scope.clickOnTabView = function(tab) {
        if (tab == 'reservedMAWB' && $rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_RESERVED_MAWB_LIST)) {
            $scope.Tabs = tab;
            $scope.reservedMAWBList = true;
        }
        if ($scope.airlinePrebooking.id != null && $scope.airlinePrebooking.id != undefined) {
            if (tab == 'notes' && $rootScope.roleAccess(roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_NOTES_MODIFY)) {
                $scope.Tabs = tab;
            }
        } else {
            $scope.Tabs = tab;
        }


    }


    $scope.saveReservedMawb = function(obj, mawb) {
        if (!$scope.validateLinkReservedMawb(0, obj)) {
            return false;
        }
        if (obj.flightPlan != undefined && obj.flightPlan != null &&
            obj.mawbNo != undefined && obj.mawbNo != null) {
            console.log(obj, mawb);

            var tempObj = angular.copy(obj);
            $scope.fetchStockMawbData.mawbList.push(tempObj.mawbNo);
            $scope.fetchFightPlanScheduleData.ids.push(tempObj.flightPlan.id);
            $scope.airlinePrebooking.scheduleList.push(tempObj);
            return true;
        } else {
            Notification.warning($rootScope.nls["ERR70027"]);
            return false;
        }

    }

    $scope.validateAirlinePrebooking = function(validateCode) {
        $scope.errorMap = new Map();
        if (validateCode == 0 || validateCode == 1) {
            if ($scope.airlinePrebooking == undefined || $scope.airlinePrebooking.carrierMaster == undefined || $scope.airlinePrebooking.carrierMaster == null || $scope.airlinePrebooking.carrierMaster.id == undefined) {
                $scope.errorMap.put("carrierMaster", $rootScope.nls["ERR70001"]);
                return false;
            } else {
                if (ValidateUtil.isStatusBlocked($scope.airlinePrebooking.carrierMaster.status)) {
                    $scope.errorMap.put("carrierMaster", $rootScope.nls["ERR70002"]);
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.airlinePrebooking.carrierMaster.status)) {
                    $scope.errorMap.put("carrierMaster", $rootScope.nls["ERR70003"]);
                    return false;
                }
            }
        }
        if (validateCode == 0 || validateCode == 2) {
            if ($scope.airlinePrebooking.prebookingDate == undefined || $scope.airlinePrebooking.prebookingDate == null || $scope.airlinePrebooking.prebookingDate == undefined) {
                $scope.errorMap.put("prebookingDate", $rootScope.nls["ERR70004"]);
                return false;
            } else {

                // Create date from input value
                var inputDate = new Date($scope.airlinePrebooking.prebookingDate);

                // Get today's date
                var todaysDate = new Date();

                // call setHours to take the time out of the comparison
                if (inputDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
                    $scope.errorMap.put("prebookingDate", $rootScope.nls["ERR70005"]);
                    return false;
                }

            }
        }
        if (validateCode == 0 || validateCode == 3) {
            if ($scope.airlinePrebooking.serviceMaster == undefined || $scope.airlinePrebooking.serviceMaster == null || $scope.airlinePrebooking.serviceMaster.id == undefined) {
                $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR70006"]);
                return false;
            } else {
                if (ValidateUtil.isStatusBlocked($scope.airlinePrebooking.serviceMaster.status)) {
                    $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR70007"]);
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.airlinePrebooking.serviceMaster.status)) {
                    $scope.errorMap.put("serviceMaster", $rootScope.nls["ERR70008"]);
                    return false;
                }
            }
        }
        if (validateCode == 0 || validateCode == 4) {
            if ($scope.airlinePrebooking.por == undefined || $scope.airlinePrebooking.por == null || $scope.airlinePrebooking.por.id == undefined) {
                $scope.errorMap.put("origin", $rootScope.nls["ERR70009"]);
                return false;
            } else {
                if (ValidateUtil.isStatusBlocked($scope.airlinePrebooking.por.status)) {
                    $scope.errorMap.put("origin", $rootScope.nls["ERR70010"]);
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.airlinePrebooking.por.status)) {
                    $scope.errorMap.put("origin", $rootScope.nls["ERR70011"]);
                    return false;
                }
            }
        }
        if (validateCode == 0 || validateCode == 5) {
            if ($scope.airlinePrebooking.partyMaster == undefined || $scope.airlinePrebooking.partyMaster == null || $scope.airlinePrebooking.partyMaster.id == undefined) {
                $scope.errorMap.put("partyMaster", $rootScope.nls["ERR70012"]);
                return false;
            } else {
                if (ValidateUtil.isStatusBlocked($scope.airlinePrebooking.partyMaster.status)) {
                    $scope.errorMap.put("partyMaster", $rootScope.nls["ERR70013"]);
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.airlinePrebooking.partyMaster.status)) {
                    $scope.errorMap.put("partyMaster", $rootScope.nls["ERR70014"]);
                    return false;
                }
            }
        }


        if (validateCode == 0 || validateCode == 6) {
            if ($scope.airlinePrebooking.noOfMawb != undefined && $scope.airlinePrebooking.noOfMawb != null && $scope.airlinePrebooking.noOfMawb != "") {
                if ($scope.airlinePrebooking.noOfMawb < 0 || $scope.airlinePrebooking.noOfMawb > 9999999999.99) {
                    $scope.errorMap.put("noOfMawb", $rootScope.nls["ERR70016"]);
                    return false;
                }
                if (isNaN($scope.airlinePrebooking.noOfMawb) || !CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_NO_OF_PIECES, $scope.airlinePrebooking.noOfMawb)) {
                    $scope.errorMap.put("noOfMawb", $rootScope.nls["ERR70015"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 7) {
            if ($scope.airlinePrebooking.noOfPieces != undefined && $scope.airlinePrebooking.noOfPieces != null && $scope.airlinePrebooking.noOfPieces != "") {
                if ($scope.airlinePrebooking.noOfPieces < 0 || $scope.airlinePrebooking.noOfPieces > 9999999999.99) {
                    $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR70018"]);
                    return false;
                }
                if (isNaN($scope.airlinePrebooking.noOfPieces) || !CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_NO_OF_PIECES, $scope.airlinePrebooking.noOfPieces)) {
                    $scope.errorMap.put("noOfPieces", $rootScope.nls["ERR70017"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 8) {
            if ($scope.airlinePrebooking.charterMaster != undefined && $scope.airlinePrebooking.charterMaster != null && $scope.airlinePrebooking.charterMaster.id != undefined) {

                if (ValidateUtil.isStatusBlocked($scope.airlinePrebooking.charterMaster.status)) {
                    $scope.errorMap.put("charterMaster", $rootScope.nls["ERR70019"]);
                    return false;
                }
                if (ValidateUtil.isStatusHidden($scope.airlinePrebooking.charterMaster.status)) {
                    $scope.errorMap.put("charterMaster", $rootScope.nls["ERR70020"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 9) {

            if ($scope.airlinePrebooking.volume != undefined && $scope.airlinePrebooking.volume != null &&
                $scope.airlinePrebooking.volume != '' && !isNaN($scope.airlinePrebooking.volume)) {

                if ($scope.airlinePrebooking.volume < 0 || $scope.airlinePrebooking.volume > 9999999999.99) {
                    $scope.errorMap.put("volume", $rootScope.nls["ERR70022"]);
                    return false;
                } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, $scope.airlinePrebooking.volume)) {
                    $scope.errorMap.put("volume", $rootScope.nls["ERR70021"]);
                    return false;
                }
            } else {
                $scope.airlinePrebooking.volume = null;
            }
        }

        if (validateCode == 0 || validateCode == 10) {

            if ($scope.airlinePrebooking.grossWeight != undefined && $scope.airlinePrebooking.grossWeight != null &&
                $scope.airlinePrebooking.grossWeight != '' && !isNaN($scope.airlinePrebooking.grossWeight)) {
                if ($scope.airlinePrebooking.grossWeight < 0 || $scope.airlinePrebooking.grossWeight > 9999999999.99) {
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR70024"]);
                    return false;
                } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, $scope.airlinePrebooking.grossWeight)) {
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR70023"]);
                    return false;
                }
            } else {
                $scope.airlinePrebooking.grossWeight = null;
            }
        }

        if (validateCode == 0 || validateCode == 11) {

            if ($scope.airlinePrebooking.volumeWeight != undefined && $scope.airlinePrebooking.volumeWeight != null &&
                $scope.airlinePrebooking.volumeWeight != '' && !isNaN($scope.airlinePrebooking.volumeWeight)) {

                if ($scope.airlinePrebooking.volumeWeight < 0 || $scope.airlinePrebooking.volumeWeight > 9999999999.99) {
                    $scope.errorMap.put("volumeWeight", $rootScope.nls["ERR70026"]);
                    return false;
                } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, $scope.airlinePrebooking.volumeWeight)) {
                    $scope.errorMap.put("volumeWeight", $rootScope.nls["ERR70025"]);
                    return false;
                }
            } else {
                $scope.airlinePrebooking.volumeWeight = null;
            }
        }



        return true;
    }

    $scope.validateLinkReservedMawb = function(validateCode, obj) {
        $scope.errorMap = new Map();
        if (validateCode == 0 || validateCode == 1) {
            if (obj == undefined || obj.mawbNo == undefined || obj.mawbNo == null || obj.mawbNo == '') {
                $scope.errorMap.put("mawbNo", $rootScope.nls["ERR70000"]);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if (obj.noOfPiece != undefined && obj.noOfPiece != null && obj.noOfPiece != "") {
                if (obj.noOfPiece < 0 || obj.noOfPiece > 9999999999.99) {
                    $scope.errorMap.put("noOfPiece", $rootScope.nls["ERR70018"]);
                    return false;
                }
                if (isNaN(obj.noOfPiece) || !CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_NO_OF_PIECES, obj.noOfPiece)) {
                    $scope.errorMap.put("noOfPiece", $rootScope.nls["ERR70017"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {

            if (obj.volume != undefined && obj.volume != null &&
                obj.volume != '' && !isNaN(obj.volume)) {

                if (obj.volume < 0 || obj.volume > 9999999999.99) {
                    $scope.errorMap.put("volume", $rootScope.nls["ERR70022"]);
                    return false;
                } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, obj.volume)) {
                    $scope.errorMap.put("volume", $rootScope.nls["ERR70021"]);
                    return false;
                }
            } else {
                obj.volume = null;
            }
        }

        if (validateCode == 0 || validateCode == 4) {

            if (obj.grossWeight != undefined && obj.grossWeight != null &&
                obj.grossWeight != '' && !isNaN(obj.grossWeight)) {
                if (obj.grossWeight < 0 || obj.grossWeight > 9999999999.99) {
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR70024"]);
                    return false;
                } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, obj.grossWeight)) {
                    $scope.errorMap.put("grossWeight", $rootScope.nls["ERR70023"]);
                    return false;
                }
            } else {
                obj.grossWeight = null;
            }
        }

        if (validateCode == 0 || validateCode == 5) {

            if (obj.volWeight != undefined && obj.volWeight != null &&
                obj.volWeight != '' && !isNaN(obj.volWeight)) {

                if (obj.volWeight < 0 || obj.volWeight > 9999999999.99) {
                    $scope.errorMap.put("volWeight", $rootScope.nls["ERR70026"]);
                    return false;
                } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, obj.volWeight)) {
                    $scope.errorMap.put("volWeight", $rootScope.nls["ERR70025"]);
                    return false;
                }
            } else {
                obj.volWeight = null;
            }
        }
        return true;
    }

    $scope.cancel = function() {

        if ($scope.airlineForm.$dirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.saveAirlinePrebooking();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go("layout.airlinePrebooking", params);
                    } else {
                        console.log("Nothing selected..Cancel")
                    }
                });

        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.airlinePrebooking", params);
        }

    };



    $scope.setStatus = function(airlinePrebookingSchedule) {
        airlinePrebookingSchedule.status = 'booked';
    }



    /*Carrier Lov*/
    $scope.ajaxCarrierEvent = function(object) {
        console.log("ajaxCarrierEvent is called ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CarrierByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.carrierMasterList = data.responseObject.searchResult;
                    return $scope.carrierMasterList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air');
            }
        );
    }


    $scope.selectedCarrierMaster = function(id) {
        if ($scope.validateAirlinePrebooking(1)) {
            $scope.navigateToNextField(id);
        }
    };


    $scope.carrierRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }
    }

    /*Service Lov*/
    $scope.ajaxServiceEvent = function(object) {
        console.log("ajaxServiceEvent ", object);


        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


        return ServiceByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(function(data, status) {
            if (data.responseCode == "ERR0") {
                $scope.serviceMasterList = data.responseObject.searchResult;
                console.log("serviceMasterList ", $scope.serviceMasterList);
                return $scope.serviceMasterList;
            }
        }, function(errResponse) {
            console.error('Error while fetching ServiceMaster');
        });
    }

    $scope.selectedServiceMaster = function(id) {
        if ($scope.validateAirlinePrebooking(3)) {
            $scope.navigateToNextField(id);
        }
    };

    $scope.serviceRender = function(item) {
        return {
            label: item.serviceName,
            item: item
        }
    }



    /*POR Lov*/

    $scope.ajaxPortOfReceipt = function(object) {

        console.log("ajaxPortOfReceipt is called", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PortByTransportMode.fetch({
            "transportMode": 'Air'
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Origin List');
            }
        );
    }

    $scope.selectedPor = function(id) {
        if ($scope.validateAirlinePrebooking(4)) {
            $scope.navigateToNextField(id);
        }
    };


    $scope.porRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    }




    /*Charter Lov*/

    $scope.ajaxCharterEvent = function(object) {

        console.log("ajaxCharterEvent is called", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CharterList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while searching Charter List');
            }
        );
    }

    $scope.selectedCharterMaster = function(id) {
        if ($scope.validateAirlinePrebooking(8)) {
            $scope.navigateToNextField(id);
        }
    };


    $scope.charterRender = function(item) {
        return {
            label: item.charterName,
            item: item
        }
    }




    /*Party Lov*/

    $scope.ajaxPartyEvent = function(object) {

        console.log("ajaxPartyEvent is called", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.customerList = data.responseObject.searchResult;
                    return $scope.customerList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );
    }


    $scope.selectedParty = function(id) {
        if ($scope.validateAirlinePrebooking(5)) {
            $scope.navigateToNextField(id);
        }
    };

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }
    }

    //flight schedule code stars

    $scope.linkServiceModal = function() {
        $scope.flightSchedule = {};
        $scope.searchDto = {};
        $scope.searchDto.orderByType = 'asc';
        $scope.searchDto.sortByColumn = 'etd';
        $scope.fetchStockList = [];
        $scope.selectedFlightScheduleData = null;
        $scope.flightScheduleLimitArray = [5, 10, 15];
        $scope.flightSchedulePage = 0;
        $scope.flightScheduleLimit = 5;
        $scope.flightScheduleTotalRecord = 0;
        $scope.fetchMawbAndFlightPlan();
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/air/airline_prebooking/views/link_reserved_mawbs_popup.html',
            show: false
        });
        myOtherModal.$promise.then(myOtherModal.show);
    };


    $scope.datepickeropts = {
        clearLabel: 'Clear',
        locale: {
            applyClass: 'btn-green',
            applyLabel: "Apply",
            fromLabel: "From",
            format: $rootScope.userProfile.selectedUserLocation.dateFormat ? $rootScope.userProfile.selectedUserLocation.dateFormat : "DD-MM-YYYY",
            toLabel: "To",
        },
        ranges: {
            'Today': [moment(), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()]
        },
        eventHandlers: {
            'apply.daterangepicker': function(ev, picker) {
                $timeout(function() {

                    $scope.fetchMawbAndFlightPlan();
                }, 2);
            },
            'cancel.daterangepicker': function(ev, picker) {
                $timeout(function() {
                    $scope.fetchFightPlanScheduleData.eta = null;
                    $scope.fetchFightPlanScheduleData.etd = null;
                    $scope.fetchMawbAndFlightPlan();
                }, 2);

            }
        }
    };

    $scope.fetchMawbAndFlightPlan = function() {
        if ($scope.fetchFightPlanScheduleData.porId != undefined && $scope.fetchFightPlanScheduleData.porId != null &&
            $scope.fetchFightPlanScheduleData.carrierId != undefined && $scope.fetchFightPlanScheduleData.carrierId != null) {

            $scope.fetchFightPlanScheduleData.selectedPageNumber = $scope.flightSchedulePage;
            $scope.fetchFightPlanScheduleData.recordPerPage = $scope.flightScheduleLimit;
            if ($scope.fetchFightPlanScheduleData.eta != null) {
                $scope.fetchFightPlanScheduleData.eta.startDate = $rootScope.sendApiStartDateTime($scope.fetchFightPlanScheduleData.eta.startDate);
                $scope.fetchFightPlanScheduleData.eta.endDate = $rootScope.sendApiEndDateTime($scope.fetchFightPlanScheduleData.eta.endDate);
            } else {
                $scope.fetchFightPlanScheduleData.eta = null;
            }
            if ($scope.fetchFightPlanScheduleData.etd != null) {
                $scope.fetchFightPlanScheduleData.etd.startDate = $rootScope.sendApiStartDateTime($scope.fetchFightPlanScheduleData.etd.startDate);
                $scope.fetchFightPlanScheduleData.etd.endDate = $rootScope.sendApiEndDateTime($scope.fetchFightPlanScheduleData.etd.endDate);
            } else {
                $scope.fetchFightPlanScheduleData.etd = null;
            }
            console.log("$scope.searchDto ", $scope.fetchFightPlanScheduleData);


            appMetaFactory.fetchFightPlanSchedule.get($scope.fetchFightPlanScheduleData).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {

                    $scope.flightScheduleTotalRecord = data.responseObject.totalRecord;
                    console.log("$scope.flightScheduleTotalRecord ", $scope.flightScheduleTotalRecord);
                    var tempArr = [];
                    var resultArr = [];
                    tempArr = data.responseObject.searchResult;
                    var tempObj = {};
                    angular.forEach(tempArr, function(item, index) {
                        tempObj = item;
                        tempObj.no = (index + 1) + ($scope.flightSchedulePage * $scope.flightScheduleLimit);
                        resultArr.push(tempObj);
                        tempObj = {};
                    });

                    $scope.fetchStockList = resultArr;

                    console.log("$scope.mawbList", $scope.fetchStockList);
                } else {

                }
            }, function(error) {

            });
        }

        if ($scope.fetchStockMawbData.count != undefined && $scope.fetchStockMawbData.count != null && $scope.fetchStockMawbData.count > 0 &&
            $scope.fetchStockMawbData.porId != undefined && $scope.fetchStockMawbData.porId != null &&
            $scope.fetchStockMawbData.carrierId != undefined && $scope.fetchStockMawbData.carrierId != null) {
            appMetaFactory.fetchStockMawb.get($scope.fetchStockMawbData).$promise.then(function(bata) {
                if (bata.responseCode == "ERR0") {
                    $scope.mawbList = bata.responseObject;
                    console.log("$scope.fetchStockList", $scope.mawbList);
                } else {

                }
            }, function(error) {

            });
        }
    }


    $scope.toggleFlightPlan = function(flightSchedule) {
        if ($scope.flightDetail == 'child' + flightSchedule.id) {
            $scope.flightDetail = null;
            $scope.flightSchedule = null;
        } else {
            $scope.flightDetail = 'child' + flightSchedule.id;
            $scope.flightSchedule = flightSchedule;
        }
    }

    $scope.flightScheduleChangePage = function(param) {
        console.log("flightScheduleChangePage ", param);
        $scope.flightSchedulePage = param.page;
        $scope.flightScheduleLimit = param.size;
        $scope.fetchMawbAndFlightPlan();

    }

    $scope.flightScheduleLimitChange = function(item) {
        console.log("flightScheduleLimitChange ", item);
        $scope.flightSchedulePage = 0;
        $scope.flightScheduleLimit = item;
        $scope.fetchMawbAndFlightPlan();
    }

    $scope.flightScheduleSortChange = function(sortKey) {

        $scope.fetchFightPlanScheduleData.sortByColumn = sortKey;
        if ($scope.fetchFightPlanScheduleData.orderByType == 'asc') {
            $scope.fetchFightPlanScheduleData.orderByType = 'desc';
        } else {
            $scope.fetchFightPlanScheduleData.orderByType = 'asc';
        }
        $scope.fetchMawbAndFlightPlan();

    }

    $scope.newFlightSchedule = function(flightSchedule) {
        $scope.flightSchedule = flightSchedule;

    };


    //flight schedule ends here

    /* Saving Airline Prebooking */

    $scope.saveAirlinePrebooking = function() {

        console.log("SaveAirlinePrebooking is called......", $scope.airlinePrebooking);
        if (!$scope.validateAirlinePrebooking(0)) {
            return;
        }

        $scope.tmpObject = JSON.parse(JSON.stringify($scope.airlinePrebooking));
        $scope.tmpObject.prebookingDate = $rootScope.sendApiStartDateTime($rootScope.convertToDate($scope.tmpObject.prebookingDate));

        AirlinePrebookingSave.save($scope.tmpObject).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Airline Prebooking saved Successfully");
                $scope.enableSubmitBtnFn();
                $rootScope.successDesc = $rootScope.nls["ERR401"];
                var params = {}
                params.submitAction = 'Saved';
                $state.go("layout.airlinePrebooking", params);
            } else {
                $scope.enableSubmitBtnFn();
                console.log("Failure ", data.responseDescription)
            }
        }, function(error) {
            $scope.enableSubmitBtnFn();
            console.log("Failure ", error);
        });

        $scope.enableSubmitBtnFn();
    }




    var reloadDataConversion = function(airlinePrebooking) {
        airlinePrebooking.prebookingDate = $rootScope.convertToDateTime(airlinePrebooking.prebookingDate);
        return airlinePrebooking;
    }



    $scope.getAirlinePrebooking = function(prebookingId) {
        console.log("Prebooking ID " + prebookingId);

        AirlinePrebookingView.get({
            id: prebookingId
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.airlinePrebooking = data.responseObject;
                console.log("Airline Prebooking : ", $scope.airlinePrebooking);
            } else {
                console.log("Description : ", data.responseDescription);
            }

        }, function(error) {
            console.log("Airline Prebooking View Failed : ", error)
        });
    }



    // Reload Event 
    $scope.$on('addAirlinePrebookingEventReload', function(e, confirmation) {

        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.airlinePrebooking);
        localStorage.isPrebookingReloaded = "YES";
        e.preventDefault();

    });

    $scope.$on('editAirlinePrebookingEventReload', function(e, confirmation) {

        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.airlinePrebooking);
        localStorage.isPrebookingReloaded = "YES";
        e.preventDefault();

    });

    $scope.$on('addAirlinePrebookingEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.airlinePrebooking;
        $rootScope.unfinishedFormTitle = "Airline Prebooking Entry (New)";
        $rootScope.category = "Airline Prebooking";

        if ($scope.airlinePrebooking != undefined && $scope.airlinePrebooking != null &&
            $scope.airlinePrebooking.carrierMaster != undefined && $scope.airlinePrebooking.carrierMaster != null) {
            $rootScope.subTitle = $scope.airlinePrebooking.carrierMaster.carrierName;
        } else {
            $rootScope.subTitle = "Unknown Carrier"
        }

    });



    $scope.$on('editSalesQuotationEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.airlinePrebooking;
        $rootScope.unfinishedFormTitle = "Edit Airline Prebooking Entry " + $scope.airlinePrebooking.id;
        $rootScope.category = "Airline Prebooking";

        if ($scope.airlinePrebooking != undefined && $scope.airlinePrebooking != null &&
            $scope.airlinePrebooking.carrierMaster != undefined && $scope.airlinePrebooking.carrierMaster != null) {
            $rootScope.subTitle = $scope.airlinePrebooking.carrierMaster.carrierName;
        } else {
            $rootScope.subTitle = "Unknown Carrier"
        }


    });



    $scope.isReloaded = localStorage.isPrebookingReloaded;

    if ($stateParams.action == "ADD") {
        console.log("Airline Prebooking ADD page");

        if ($stateParams.fromHistory == 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("1");
                $scope.isReloaded = "NO";
                localStorage.isPrebookingReloaded = "NO";
                $scope.airlinePrebooking = reloadDataConversion(JSON.parse(localStorage.reloadFormData));
            } else {
                console.log("2");
                $scope.airlinePrebooking = reloadDataConversion($rootScope.selectedUnfilledFormData);
            }
        } else {
            if ($scope.isReloaded == "YES") {
                console.log("3");
                $scope.isReloaded = "NO";
                localStorage.isPrebookingReloaded = "NO";
                $scope.airlinePrebooking = reloadDataConversion(JSON.parse(localStorage.reloadFormData));
            } else {
                console.log("4");
                $scope.airlinePrebooking = {};
                $scope.airlinePrebooking.scheduleList = [];
            }
        }

        $scope.init()

        $rootScope.navigateToNextField("carrier");
        $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airlinePrebooking"
            },
            {
                label: "Airline Prebooking Entry",
                state: "layout.airlinePrebooking"
            },
            {
                label: "Add Airline Prebooking Entry",
                state: null
            }
        ];
    } else {
        console.log("Airline Prebooking EDIT page");
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPrebookingReloaded = "NO";
                $scope.airlinePrebooking = reloadDataConversion(JSON.parse(localStorage.reloadFormData));
            } else {
                $scope.airlinePrebooking = reloadDataConversion($rootScope.selectedUnfilledFormData);
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPrebookingReloaded = "NO";
                $scope.airlinePrebooking = reloadDataConversion(JSON.parse(localStorage.reloadFormData));
            } else {
                $scope.getAirlinePrebooking($stateParams.prebookingId)
            }
        }
        $rootScope.navigateToNextField("carrier");
        $rootScope.breadcrumbArr = [{
                label: "Air",
                state: "layout.airlinePrebooking"
            },
            {
                label: "Airline Prebooking Entry",
                state: "layout.airlinePrebooking"
            },
            {
                label: "Edit Airline Prebooking Entry",
                state: null
            }
        ];

    }

}]);