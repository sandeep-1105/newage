/**
 * 
 */
app.controller('winWebController',['$scope', '$rootScope', '$http', 'Notification', '$stateParams', '$state', '$timeout', 
    function($scope, $rootScope, $http, Notification, $stateParams, $state, $timeout) {
    
    $scope.init = function() {

        $scope.winConfigurationRequestDto = {};
        $scope.searchStatus();

    }


    $scope.searchStatus = function() {

        $http({
            url: '/api/v1/winconfiguration/get/search/keyword',
            method: "POST",
            data: $scope.winConfigurationRequestDto
        }).success(function(data, status, headers, config) {}).error(function(data, status, headers, config) {
            console.log("Problem while sending.....");

        });

    }


    //Stock Enquiry Starts

    $scope.winStatusHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "AwbId",
            "search": true,
            "model": "awbId",
            "wrap_cell": true,
            "type": "text",
            "width": "w40px",
            "prefWidth": "40",
            "key": "awbId"

        },
        {
            "name": "MawbId",
            "search": true,
            "model": "mawbId",
            "wrap_cell": true,
            "type": "text",
            "width": "w150px",
            "prefWidth": "150",
            "key": "mawbId"
        },
        {
            "name": "Awb Number",
            "model": "awbNumber",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "width": "w150px",
            "prefWidth": "150",
            "key": "awbNumber"
        },
        {
            "name": "Win Status",
            "model": "winStatus",
            "wrap_cell": true,
            "type": "text",
            "width": "w100px",
            "prefWidth": "100",
            "key": "winStatus"
        },
        {
            "name": "Master Id",
            "model": "shipment",
            "wrap_cell": true,
            "type": "link",
            "width": "w100px",
            "prefWidth": "100",
            "key": "consolUid"
        },
        {
            "name": "Error Code",
            "model": "errorCode",
            "wrap_cell": true,
            "type": "text",
            "width": "w100px",
            "prefWidth": "100",
            "key": "errorCode"
        }, {
            "name": "Error Message",
            "model": "errorMessage",
            "wrap_cell": true,
            "type": "text",
            "width": "w100px",
            "prefWidth": "100",
            "key": "errorMessage"
        }
    ];


    //post the data
    $scope.connectWinWebController = function(consol) {

        $scope.winspinner = true;
        $scope.consolDto = {};
        $scope.consolDto.consolUid = consol.consolUid;
        $http({
            url: '/api/v1/winconfiguration/winwebconnect',
            method: "POST",
            responseType: 'arraybuffer',
            data: $scope.consolDto
        }).success(function(data, status, headers, config) {
            $timeout(function() {
                $scope.winspinner = false;
                $scope.winwebmodal.$promise.then($scope.winwebmodal.hide);
                var params = {};
                var stateCount = 0;
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }
                Notification.success($rootScope.nls["ERR8905"]);
                $state.go("layout.editAirConsol", {
                    consolId: consol.id,
                    submitAction: 'Saved',
                    count: stateCount
                });
            }, 2);
        }).error(function(data, status, headers, config) {
            console.log("Problem while sending.....");
            $timeout(function() {
                $scope.winspinner = false;
                $scope.winwebmodal.$promise.then($scope.winwebmodal.hide)
                var params = {};
                var stateCount = 0;
                if ($stateParams.count != undefined && $stateParams.count != null) {
                    stateCount = $stateParams.count + 1;
                }
                Notification.error($rootScope.nls["ERR8906"]);
                $state.go("layout.editAirConsol", {
                    consolId: consol.id,
                    submitAction: 'Saved',
                    count: stateCount
                });
            }, 2);
        });
    }




    $scope.hideWinWeb = function(consol, isdetail) {

        if (isdetail != undefined && isdetail) {
            return;
        }
        var params = {};
        var stateCount = 0;
        if ($stateParams.count != undefined && $stateParams.count != null) {
            stateCount = $stateParams.count + 1;
        }
        $state.go("layout.editAirConsol", {
            consolId: consol.id,
            submitAction: 'Saved',
            count: stateCount
        });
    }

    //update win web
    $scope.updateWinWeb = function() {

        //PUT /api/v1/Awb/{AwbID} 



    }


    //win login
    $scope.winLogin = function() {



    }



    $scope.viewAwbData = function() {

        //GET /api/v1/Agent/{AgentID}/Awb/{AwbID}

    }

    $scope.listallawbforagent = function() {

        //GET /api/v1/ListofAWBs/{AgentID}

    }

    $scope.listawllawbforcompany = function() {

        ///api/v1/ListofAWBs
    }

    //get latest staus for all 
    $scope.getLatestStatus = function() {

        $http({
            url: '/api/v1/ediconfiguration/get/uri/laststatus',
            method: "GET",
        }).success(function(data, status, headers, config) {


        }).error(function(data, status, headers, config) {
            console.log("Problem while sending.....");
        });

    }


    $scope.getStatusBasedOnAgent = function() {

        //GET /Api/v1/Pouch/getLatestStatus/Agent/{AgentID}
    }

    $scope.getstatusofparticualrmawb = function() {

        // /Api/v1/Pouch/getLatestStatus/Agent/{AgentID}/MAWB/{MAWBID}
    }

    $scope.getstatusofparticualrmawbonsometime = function() {

        ///Api/v1/Pouch/getLatestStatus/Agent/{AgentID}/MAWB/{MAWBID}/{Timestamp}
    }

    $scope.allagentlist = function() {

        ///api/v1/Air/Agents
    }

}]);