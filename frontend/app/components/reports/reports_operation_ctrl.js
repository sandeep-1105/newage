app.controller('reportsOperationCtrl',['$rootScope', '$scope', '$http', '$location',
    'CommodityAdd', 'CommodityEdit', 'commodityService', 'ngProgressFactory', 'ngDialog',
    function($rootScope, $scope, $http, $location,
    CommodityAdd, CommodityEdit, commodityService, ngProgressFactory, ngDialog) {

    $scope.commodityMaster = {};
    $scope.init = function() {
        $scope.commodityMaster = commodityService.get();
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('reference-panel'));
        $scope.contained_progressbar.setAbsolute();
        $rootScope.setNavigate3($scope.commodityMaster.id != null ? "Edit Reference Type" : "Add Reference Type");

        if ($scope.commodityMaster.id == null)
            $scope.commodityMaster.status = 'Active';
    };

    $scope.cancel = function() {
        var tempObj = $scope.commodityMaster;
        $scope.isDirty = $scope.commodityMasterForm.$dirty;
        if ($scope.isDirty) {
            if (Object.keys(tempObj).length) {
                ngDialog
                    .openConfirm({
                        template: '<p>Do you want to save your changes?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                            '</div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 &&
                                $scope.commodityMaster.id == null) {
                                $scope.save();
                            } else if (value == 1 &&
                                $scope.commodityMaster.id != null) {
                                $scope.update();
                            } else if (value == 2) {
                                $location
                                    .path('/reference_type/reference_type_list');
                            } else {
                                $location
                                    .path('/reference_type/reference_type_list');
                            }

                        });
            } else {
                $location.path('/reference_type/reference_type_list');
            }
        } else {
            $location.path('/reference_type/reference_type_list');
        }
    }



    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateCommodityMaster(0)) {
            $scope.contained_progressbar.start();
            CommodityEdit.update($scope.commodityMaster).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Reference updated Successfully")
                        commodityService.set({});
                        $location.path('/reference_type/reference_type_list');

                    } else {
                        console.log("Reference updated Failed " +
                            data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Commodity updated Failed : " + error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validateCommodityMaster(0)) {
            $scope.contained_progressbar.start();
            CommodityAdd.save($scope.commodityMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    commodityService.set({});
                    $location.path('/reference_type/reference_type_list');
                } else {
                    console.log("Reports added Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Reports added Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    //$scope.validateCommodityMaster = function(validateCode) {
    //	console.log("Validate Called");
    //	$scope.errorMap = new Map();
    //
    //	if (validateCode == 0 || validateCode == 1) {
    //		if ($scope.commodityMaster.hsName == undefined
    //				|| $scope.commodityMaster.hsName == null
    //				|| $scope.commodityMaster.hsName == "") {
    //			$scope.errorMap.put("hsName",$rootScope.nls["ERR2407"]);
    //			return false;
    //		} else {
    //
    //			var regexp = new RegExp("^[ 0-9a-zA-Z-'.,&@:;?!()$#*_]{0,100}$");
    //			if (!regexp
    //					.test($scope.commodityMaster.hsName)) {
    //				$scope.errorMap.put("hsName",$rootScope.nls["ERR2404"]);
    //				return false;
    //			}
    //
    //		}
    //	}
    //
    //	if (validateCode == 0 || validateCode == 2) {
    //		if ($scope.commodityMaster.hsCode == undefined
    //				|| $scope.commodityMaster.hsCode == null
    //				|| $scope.commodityMaster.hsCode == "") {
    //			$scope.errorMap.put("hsCode",$rootScope.nls["ERR2406"]);
    //			return false;
    //		} else {
    //
    //			var regexp = new RegExp("^[0-9A-Z]{0,10}$");
    //			if (!regexp
    //					.test($scope.commodityMaster.hsCode)) {
    //				$scope.errorMap.put("hsCode",$rootScope.nls["ERR2403"]);
    //				return false;
    //			}
    //
    //		}
    //	}
    //
    //
    //	if (validateCode == 0 || validateCode == 3) {
    //
    //	}
    //
    //
    //	return true;
    //
    //}

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }
}]);