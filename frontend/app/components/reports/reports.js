app.controller('reportsCtrl', ['$rootScope', '$scope', '$http', '$location', '$state', '$window', '$stateParams', 'ngTableParams',
    'ServiceList', 'CountryList', 'PortGroupList', 'PortSearchKeyword', 'CarrierByTransportMode',
    'ngDialog', 'ReportSearch', 'ReportsDynamicSearch', 'ValidateUtil', '$modal', 'downloadFactory',
    'PortByTransportModeCountry', 'roleConstant',
    function($rootScope, $scope, $http, $location, $state, $window, $stateParams, ngTableParams,
        ServiceList, CountryList, PortGroupList, PortSearchKeyword, CarrierByTransportMode,
        ngDialog, ReportSearch, ReportsDynamicSearch, ValidateUtil, $modal, downloadFactory,
        PortByTransportModeCountry, roleConstant) {

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
        $scope.testArr = {};
        $scope.commodityMaster = false;

        $scope.init = function() {
            console.log("Reports Type Init method called.");
            $rootScope.setNavigate1("Reports");
            $scope.reportSearchDto = {};
            $scope.search();
            $scope.errorMap = new Map();
            $scope.reportSearchDto.orderByType = "ASC";
            $scope.sortIconCommodityName = "fa fa-chevron-down";
            $scope.reportSearchDto.sortByColumn = "reportName";
            $scope.reportLabelShow = false;
            $scope.downloadFileType = "PDF";
            $scope.generate = {};
            $scope.generate.typeArr = ["Download", "Preview", "Print"];

            if ($scope.reportsFormData == undefined || $scope.reportsFormData == null) {
                $scope.reportsFormData = {};
            }

            $scope.directiveDateTimePickerOpts = {
                format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                useCurrent: false,
                sideBySide: true,
                viewDate: moment()
            };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";
            $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";

        };

        $scope.changeSearch = function(param) {

            console.log("Change Search", param);

            for (var attrname in param) {
                $scope.reportSearchDto[attrname] = param[attrname];
            }

            $scope.page = 0;
            console.log("change search", $scope.reportSearchDto);

            $scope.search();
        }

        $scope.sortChange = function(param) {
            console.log("Sort Change Called.", param);

            $scope.reportSearchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.reportSearchDto.sortByColumn = param.sortKey;

            $scope.search();
        }


        $scope.reportHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-7",
                "model": "reportName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "reportName"

            },
            {
                "name": "Short Name",
                "width": "col-xs-2half",
                "model": "reportShortName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "reportShortName"
            }
        ];

        $scope.sortSelection = {
            sortKey: "reportName",
            sortOrder: "asc"
        }

        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.changePage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.cancel();
            $scope.search();
        }
        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.search = function() {
            console.log("Commodity Search loaded..");
            $scope.reportSearchDto.selectedPageNumber = $scope.page;
            $scope.reportSearchDto.recordPerPage = $scope.limit;
            $scope.reportArr = [];

            //For Report Searching query ReportName,ReportShortName
            ReportSearch.query($scope.reportSearchDto).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;

                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;

                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });

                $scope.reportArr = resultArr;
            });

        };


        $scope.rowSelect = function(obj) {
            if ($rootScope.roleAccess(roleConstant.REPORTS_VIEW)) {
                //$scope.reportsFormData=obj;
                console.log("data", obj);
                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }

                console.log("data", obj);
                $scope.reportObject = {};
                $scope.reportObject = obj;
                console.log("Report object ::::::::", $scope.reportObject);
                $scope.reportName = obj.reportName;
                $scope.reportObject.id = obj.id;
                console.log("id", $scope.reportObject.id);

                $scope.cancelArr = function() {
                    console.log("Back Button is called");
                    $scope.reportLabelShow = false;
                }

                //getting  corresponding fields  from report wise based on id
                ReportsDynamicSearch.fetch({
                    "id": $scope.reportObject.id
                }).$promise.then(function(data) {
                    $scope.reportRowObject = [];
                    $scope.reportRowObject = data.responseObject
                    console.log("$scope.reportRowObject ", $scope.reportRowObject);
                    $scope.navigateToNextField($scope.reportRowObject[0].modelName);
                    $scope.reportLabelShow = true;
                });

            }
        }

        $scope.reportObj = {};
        $scope.entity = [{
                "row": [{
                    "labelName": "Service Code",
                    "modelName": "param1",
                    "dataType": "INPUT",
                    "htmlType": "text",
                    "minLength": 2,
                    "maxLength": 15,
                    "required": true,
                    "datePickerFormat": null,
                    "timePickerFormat": null,
                    "datetimePickerFormat": null,
                    "defaultDate": null,
                    "defaultTime": null,
                    "colWidth": "col-xs-3",
                    "userdefinedArray": []

                }]
            }, {
                "row": [{
                        "labelName": "From Date",
                        "modelName": "param2",
                        "dataType": "DATE",
                        "minLength": null,
                        "maxLength": null,
                        "required": true,
                        "datePickerFormat": "DD-MM-YYYY",
                        "timePickerFormat": null,
                        "datetimePickerFormat": null,
                        "defaultDate": "today",
                        "defaultTime": null,
                        "colWidth": "col-xs-6",
                        "userdefinedArray": []
                    },
                    {
                        "labelName": "Start Time",
                        "modelName": "param3",
                        "dataType": "TIME",
                        "minLength": null,
                        "maxLength": null,
                        "required": true,
                        "datePickerFormat": null,
                        "timePickerFormat": "HH:mm:ss",
                        "datetimePickerFormat": null,
                        "defaultDate": null,
                        "defaultTime": "now",
                        "colWidth": "col-xs-6",
                        "userdefinedArray": []
                    }
                ]
            }, {
                "row": [{
                        "labelName": "Estimated On",
                        "modelName": "param4",
                        "dataType": "DATETIME",
                        "minLength": null,
                        "maxLength": null,
                        "required": true,
                        "datePickerFormat": null,
                        "timePickerFormat": null,
                        "datetimePickerFormat": "DD-MM-YYYY HH:mm:ss",
                        "defaultDate": "today",
                        "defaultTime": "now",
                        "colWidth": "col-xs-6",
                        "userdefinedArray": []
                    },
                    {
                        "labelName": "Status",
                        "modelName": "param5",
                        "dataType": "USERDEFINED",
                        "minLength": null,
                        "maxLength": null,
                        "required": true,
                        "datePickerFormat": null,
                        "timePickerFormat": null,
                        "datetimePickerFormat": null,
                        "defaultDate": null,
                        "defaultTime": null,
                        "colWidth": "col-xs-6",
                        "userdefinedArray": ["ACTIVE", "BLOCKED", "DELETED"]
                    }
                ]
            }, {
                "row": [{
                    "labelName": "Party Type",
                    "modelName": "param7",
                    "dataType": "CHECKBOX",
                    "minLength": null,
                    "maxLength": null,
                    "required": false,
                    "datePickerFormat": null,
                    "timePickerFormat": null,
                    "datetimePickerFormat": null,
                    "defaultDate": null,
                    "defaultTime": null,
                    "userdefinedArray": [],
                    "defaultBoolean": false,
                    "colWidth": "col-xs-6",
                    "listStyle": "list-unstyle",
                    "staticmaster": [{
                        "id": 1,
                        "name": "Agent"
                    }, {
                        "id": 2,
                        "name": "Shipper"
                    }, {
                        "id": 3,
                        "name": "Example"
                    }]
                }]
            },
            {
                "row": [{
                    "labelName": "Agree",
                    "modelName": "param6",
                    "dataType": "BOOLEAN",
                    "minLength": null,
                    "maxLength": null,
                    "required": false,
                    "datePickerFormat": null,
                    "timePickerFormat": null,
                    "datetimePickerFormat": null,
                    "defaultDate": null,
                    "defaultTime": null,
                    "userdefinedArray": [],
                    "colWidth": "col-xs-6",
                    "defaultBoolean": true
                }, {
                    "labelName": "Party Master",
                    "modelName": "param8",
                    "dataType": "LOV",
                    "minLength": null,
                    "maxLength": null,
                    "required": true,
                    "datePickerFormat": null,
                    "timePickerFormat": null,
                    "datetimePickerFormat": null,
                    "defaultDate": null,
                    "defaultTime": null,
                    "userdefinedArray": [],
                    "defaultBoolean": false,
                    "dynamicmaster": [{
                        "id": 1,
                        "name": "Agent",
                        "code": "007"
                    }, {
                        "id": 2,
                        "name": "Shipper",
                        "code": "008"
                    }, {
                        "id": 3,
                        "name": "Example",
                        "code": "009"
                    }],
                    "renderItem": "serviceCode",
                    "colWidth": "col-xs-6",
                    "autocompleteSearchMTD": "getAgent(searchKey)"
                }]
            }
        ];

        $scope.getDateOptions = function(Obj) {
            var pickerOptions;
            if (Obj.dataType == 'DATE' || Obj.dataType == 'TIME') {
                pickerOptions = {
                    format: Obj.dataType == 'DATE' ? Obj.datePickerFormat : Obj.timePickerFormat,
                    useCurrent: Obj.dataType == 'DATE' ? false : true
                };

            } else if (Obj.dataType == 'DATETIME') {
                pickerOptions = {
                    format: Obj.datetimePickerFormat,
                    useCurrent: false,
                    sideBySide: true,
                    viewDate: moment()
                };
            }
            return pickerOptions;
        };

        $scope.cancel = function() {
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;
            /********************* ux fix ends here *****************************/
        };

        //For Selected Lov
        $scope.selectedLov = function(obj1, obj2) {
            console.log("called selected Lov", obj1, obj2);

            for (var i = 0; i < $scope.reportRowObject.length; i++) {
                if ($scope.reportRowObject[i].modelName == obj2) {
                    console.log("$scope.reportRowObject[i]", $scope.reportRowObject[i]);
                    $scope.navigateToNextField($scope.reportRowObject[i + 1].modelName);
                }
            }

        }
        $scope.navigateTo = function(obj) {

            for (var i = 0; i < $scope.reportRowObject.length; i++) {
                if ($scope.reportRowObject[i].modelName == obj) {
                    console.log("$scope.reportRowObject[i]", $scope.reportRowObject[i]);
                    $scope.navigateToNextField($scope.reportRowObject[i + 1].modelName);
                }
            }
        }

        $scope.validateReportsFormData = function(obj, type, fieldName) {
            $rootScope.clientMessage = null;
            if (type == 'LOV') {

                if (obj == undefined || obj == null || obj == "") {

                    $rootScope.clientMessage = fieldName.toLowerCase() + "  " + "is mandatory" + " ";
                } else {
                    if (ValidateUtil.isStatusBlocked(obj.status)) {
                        $rootScope.clientMessage = fieldName.toLowerCase() + "  " + "is blocked" + " ";
                        return false
                    }
                    if (ValidateUtil.isStatusHidden(obj.status)) {
                        $rootScope.clientMessage = fieldName + "is Hidden" + "";
                        return false
                    }
                }
            }

        }


        $scope.reportModal = function(reportRefName, obj) {
            var validated = false;
            $rootScope.clientMessage = null;

            if (reportRefName === 'PICKUP_SHEET_FOR_THE_PERIOD') {
                if (!$rootScope.roleAccess(roleConstant.REPORTS_PICKUP_SHEET_FOR_THE_PERIOD_DOWNLOAD)) {
                    return;
                }
            }
            if (reportRefName === 'VOLUME_REPORT_BY_COUNTRY') {
                if (!$rootScope.roleAccess(roleConstant.REPORTS_VOLUME_REPORT_BY_COUNTRY_DOWNLOAD)) {
                    return;
                }
            }
            if (reportRefName === 'CARRIER_VOLUME') {
                if (!$rootScope.roleAccess(roleConstant.REPORTS_CARRIER_VOLUME_DOWNLOAD)) {
                    return;
                }
            }
            if (reportRefName === 'CARGO_SALES') {
                if (!$rootScope.roleAccess(roleConstant.REPORTS_CARGO_SALES_DOWNLOAD)) {
                    return;
                }
            }


            if (reportRefName != undefined && reportRefName != null) {
                if (reportRefName.includes("PICKUP")) {
                    validated = $scope.validatePickUpReport(obj);
                } else if (reportRefName.includes("VOLUME_REPORT_BY_COUNTRY")) {
                    validated = $scope.validateVolumeReport(obj);
                } else if (reportRefName.includes("CARRIER_VOLUME")) {
                    validated = $scope.validateCarrierReport(obj);
                } else if (reportRefName.includes("CARGO_SALES")) {
                    validated = $scope.validateCargoSales(obj);
                }
            }

            if (validated) {
                $scope.changeReportDownloadFormat("PDF");
                //$scope.dataResourceId = resourceRefId;
                $scope.dataReportName = reportRefName;
                var myOtherModal = $modal({

                    scope: $scope,
                    templateUrl: 'app/components/report/reports_opt.html',
                    show: false
                });

                myOtherModal.$promise.then(myOtherModal.show);
            }
        }

        $scope.validatePickUpReport = function(pickUpObj) {
            if (pickUpObj != undefined && pickUpObj != null) {
                if (pickUpObj.serviceCode == undefined || pickUpObj.serviceCode == null || pickUpObj.serviceCode == "") {
                    $rootScope.clientMessage = $rootScope.nls["ERR070001"];
                    return false;
                }
                if (pickUpObj.fromDate == undefined || pickUpObj.fromDate == null || pickUpObj.fromDate == "") {
                    $rootScope.clientMessage = $rootScope.nls["ERR070002"];
                    return false;
                }
                if (pickUpObj.toDate == undefined || pickUpObj.toDate == null || pickUpObj.toDate == "") {
                    $rootScope.clientMessage = $rootScope.nls["ERR070003"];
                    return false;
                }
                var fromDate = $rootScope.convertToDate(pickUpObj.fromDate);
                var toDate = $rootScope.convertToDate(pickUpObj.toDate);

                if ((fromDate != undefined && fromDate != null && fromDate != "") && (toDate != undefined && toDate != null && toDate != "")) {
                    if (fromDate > toDate) {
                        $rootScope.clientMessage = $rootScope.nls["ERR070004"];
                        return false;
                    }
                }
                return true;
            } else {
                $rootScope.clientMessage = $rootScope.nls["ERR0700010"];
                return false;
            }
        }

        //For Volume Report validations

        $scope.validateVolumeReport = function(volumeObj) {
                if (volumeObj != undefined && volumeObj != null) {
                    if (volumeObj.serviceCode == undefined || volumeObj.serviceCode == null || volumeObj.serviceCode == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070001"];
                        return false;
                    }
                    if (volumeObj.country == undefined || volumeObj.country == null || volumeObj.country == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070008"];
                        return false;
                    }
                    if (volumeObj.port == undefined || volumeObj.port == null || volumeObj.port == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070007"];
                        return false;
                    }
                    if (volumeObj.fromDate == undefined || volumeObj.fromDate == null || volumeObj.fromDate == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070002"];
                        return false;
                    }
                    if (volumeObj.toDate == undefined || volumeObj.toDate == null || volumeObj.toDate == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070003"];
                        return false;
                    }

                    var fromDate = $rootScope.convertToDate(volumeObj.fromDate);
                    var toDate = $rootScope.convertToDate(volumeObj.toDate);

                    if ((fromDate != undefined && fromDate != null && fromDate != "") && (toDate != undefined && toDate != null && toDate != "")) {
                        if (fromDate > toDate) {
                            $rootScope.clientMessage = $rootScope.nls["ERR070004"];
                            return false;
                        }
                    }
                    return true;

                } else {
                    $rootScope.clientMessage = $rootScope.nls["ERR0700010"];
                    return false;
                }
            }
            // Carrier Volume validations end Here.......
        $scope.validateCarrierReport = function(carrierObj) {
                if (carrierObj != undefined && carrierObj != null) {
                    if (carrierObj.serviceCode == undefined || carrierObj.serviceCode == null || carrierObj.serviceCode == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070001"];
                        return false;
                    }
                    if (carrierObj.origin == undefined || carrierObj.origin == null || carrierObj.origin == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070005"];
                        return false;
                    }
                    if (carrierObj.destination == undefined || carrierObj.destination == null || carrierObj.destination == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070006"];
                        return false;
                    }
                    if (carrierObj.carrier == undefined || carrierObj.carrier == null || carrierObj.carrier == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070009"];
                        return false;
                    }
                    if (carrierObj.fromDate == undefined || carrierObj.fromDate == null || carrierObj.fromDate == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070002"];
                        return false;
                    }
                    if (carrierObj.toDate == undefined || carrierObj.toDate == null || carrierObj.toDate == "") {
                        $rootScope.clientMessage = $rootScope.nls["ERR070003"];
                        return false;
                    }
                    var fromDate = $rootScope.convertToDate(carrierObj.fromDate);
                    var toDate = $rootScope.convertToDate(carrierObj.toDate);

                    if ((fromDate != undefined && fromDate != null && fromDate != "") && (toDate != undefined && toDate != null && toDate != "")) {
                        if (fromDate > toDate) {
                            $rootScope.clientMessage = $rootScope.nls["ERR070004"];
                            return false;
                        }
                    }
                    return true;
                } else {
                    $rootScope.clientMessage = $rootScope.nls["ERR0700010"];
                    return false;
                }
            } //Carrier Volume report Validations ends

        //Cargo Sales report validations start here
        $scope.validateCargoSales = function(cargoSalesObj) {
            if (cargoSalesObj != undefined && cargoSalesObj != null) {
                if (cargoSalesObj.serviceName == undefined || cargoSalesObj.serviceName == null || cargoSalesObj.serviceName == "") {
                    $rootScope.clientMessage = $rootScope.nls["ERR070001"];
                    return false;
                }
                if (cargoSalesObj.fromDate == undefined || cargoSalesObj.fromDate == null || cargoSalesObj.fromDate == "") {
                    $rootScope.clientMessage = $rootScope.nls["ERR070002"];
                    return false;
                }
                if (cargoSalesObj.toDate == undefined || cargoSalesObj.toDate == null || cargoSalesObj.toDate == "") {
                    $rootScope.clientMessage = $rootScope.nls["ERR070003"];
                    return false;
                }
                var fromDate = $rootScope.convertToDate(cargoSalesObj.fromDate);
                var toDate = $rootScope.convertToDate(cargoSalesObj.toDate);

                if ((fromDate != undefined && fromDate != null && fromDate != "") && (toDate != undefined && toDate != null && toDate != "")) {
                    if (fromDate > toDate) {
                        $rootScope.clientMessage = $rootScope.nls["ERR070004"];
                        return false;
                    }
                }
                return true;

            } else {
                $rootScope.clientMessage = $rootScope.nls["ERR0700010"];
                return false;
            }
        }

        //Cargo Sales report validations ends here
        $scope.changeReportDownloadFormat = function(dType) {
                console.log("Download type Changed :: ", dType);
                $scope.downloadFileType = dType;
                if (dType === 'PDF') {
                    $scope.generate.TypeArr = ["Download", "Preview", "Print"];
                } else {
                    $scope.generate.TypeArr = ["Download"];
                }
            }
            /*$scope.validateService=function(){
            	if($scope.reportObj[$scope.field.labelName]=='ServiceCode'){
            		
            		if(reportObj[field.modelName]==undefined || reportObj[field.modelName]==null || reportObj[field.modelName]==""){
            			
            			$rootScope.clientMessage = $rootScope.nls["ERR1905"];
            			
            		}else{
            			
            			if(ValidateUtil.isStatusBlocked(reportObj[field.modelName].status)){
            				$scope.errorMap.put("serviceMaster"+index,$rootScope.nls["ERR90032"]);
            				$scope.shipmentTab="general";
            				return false
            			}
            			if(ValidateUtil.isStatusHidden(serviceDetail.serviceMaster.status)){
            				serviceDetail.serviceMaster = null;
            				console.log($rootScope.nls["ERR90033"]);
            				$scope.errorMap.put("serviceMaster"+index,$rootScope.nls["ERR90033"]);
            				$scope.shipmentTab="general";
            				return false

            			}
            			
            		}
            	}
            	
            }*/


        $scope.downloadGenericCall = function(downOption, repoName) {
            //for pickup sheet
            if ($scope.reportObj.reportName == repoName) {
                $scope.reportObj.downloadOption = downOption;
                $scope.reportObj.downloadFileType = $scope.downloadFileType; // PDF
                $scope.reportObj.apiFromDate = $rootScope.sendApiDateAndTime($scope.reportObj.fromDate);
                $scope.reportObj.apiToDate = $rootScope.sendApiDateAndTime($scope.reportObj.toDate);

                downloadFactory.download($scope.reportObj);
            }
        }

        // For List of Values  from DB	
        $scope.generalAjax = function(object, dataType, labelName) {
            $scope.searchDto = {};
            $scope.errorMap = new Map();
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            if (dataType == 'LOV' && labelName == 'Service Code' || labelName == 'Service') {
                return ServiceList.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.serviceMasterList = data.responseObject.searchResult;
                            return $scope.serviceMasterList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Tos');
                    }
                );

            }



            if (dataType == 'LOV' && labelName == 'Carrier') { //For CarrierName
                return CarrierByTransportMode.fetch({
                    "transportMode": 'Air'
                }, $scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.carrierMasterList = data.responseObject.searchResult;
                            return $scope.carrierMasterList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Carrier List Based on Air');
                    }
                );
            }

            if (dataType == 'LOV' && labelName == 'Country') { // For Country
                return CountryList.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.countryList = data.responseObject.searchResult;
                            return $scope.countryList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Countries');
                    }
                );
            }
            if (dataType == 'LOV' && labelName == 'Port') {
                $scope.searchDto.param1 = $scope.reportObj.serviceCode.transportMode
                $scope.searchDto.param2 = $scope.reportObj.country.countryCode;
                return PortByTransportModeCountry.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.destinationList = [];
                            $scope.resultArr = data.responseObject.searchResult;
                            angular.forEach($scope.resultArr, function(map) {
                                if ($scope.selectedService == null || $scope.selectedService.id == null || $scope.selectedService.trim == "") {
                                    $scope.destinationList.push(map);
                                } else {
                                    if ($scope.selectedService.transportMode == map.transportMode) {
                                        $scope.destinationList.push(map);
                                    }
                                }

                            });
                            return $scope.destinationList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Port');
                    }
                );
            }

            if (dataType == 'LOV' && labelName == 'Origin') {
                return PortSearchKeyword.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.destinationList = [];
                            $scope.resultArr = data.responseObject.searchResult;
                            angular.forEach($scope.resultArr, function(map) {
                                if ($scope.selectedService == null || $scope.selectedService.id == null || $scope.selectedService.trim == "") {
                                    $scope.destinationList.push(map);
                                } else {
                                    if ($scope.selectedService.transportMode == map.transportMode) {
                                        $scope.destinationList.push(map);
                                    }
                                }

                            });
                            return $scope.destinationList;

                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Port');
                    }
                );
            }

            if (dataType == 'LOV' && labelName == 'Destination') {
                return PortSearchKeyword.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.destinationList = [];
                            $scope.resultArr = data.responseObject.searchResult;
                            angular.forEach($scope.resultArr, function(map) {
                                if ($scope.selectedService == null || $scope.selectedService.id == null || $scope.selectedService.trim == "") {
                                    $scope.destinationList.push(map);
                                } else {
                                    if ($scope.selectedService.transportMode == map.transportMode) {
                                        $scope.destinationList.push(map);
                                    }
                                }

                            });
                            return $scope.destinationList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Port');
                    }
                );
            }
        }

        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                    label: "Reports",
                    state: "layout.report"
                }];
                break;
        }

    }
]);