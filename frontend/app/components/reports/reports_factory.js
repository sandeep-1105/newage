/**
 *  Author    Mani October 19 2016
 */

app.factory("ReportsDynamicSearch",['$resource', function($resource) {
	return $resource("/api/v1/report/getReports/id/:id", {}, {
		fetch : {
			method : 'GET',
			params : {
				id : ''
				
			},
			isArray : false
		}
	});
}]);


app.factory("ReportsSearch",['$resource', function($resource) {
	return $resource("/api/v1/report/reportSearch", {}, {
		query : {
			method : 'POST'
		}
	});
}]);


app.factory("ReportSearch",['$resource', function($resource) {
	return $resource("/api/v1/report/get/search/keyword", {}, {
		query : {
			method : 'POST'
		}
	});
}]);
/*app.factory("PortMasterList", function($resource) {
	return $resource("/api/v1/report/search/keyword", {}, {
		fetch : {
			method : 'POST'
		}
	});
});

app.factory("PortSearchKeyword", function($resource) {
	return $resource("/api/v1/portmaster/get/search/keyword", {}, {
		fetch : {
			method : 'POST'
		}
	});
});*/
