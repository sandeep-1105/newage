app.controller("crmOperationCtrl", function ($rootScope, $scope) {
   $scope.shipmentData = [
      {
         "service" : "AIR EXPORT FOB",
         "clearance" : "Yes",
         "routing" : {
            "from" : "MAA Chennai",
            "to"   : "SGP Singapore"
         },
         "commodity" : "Batteries",
         "commIcon" : "fa fa-futbol-o",
         "vWeight" : "120.00",
         "cWeight" : "125.00"
      },
      {
         "service" : "AIR IMPORT FOB",
         "clearance" : "Yes",
         "routing" : {
            "from" : "MAA Chennai",
            "to"   : "SGP Singapore"
         },
         "commodity" : "Laptops and Accessories",
         "commIcon" : "",
         "vWeight" : "120.00",
         "cWeight" : "125.00"
      }
   ];
});
