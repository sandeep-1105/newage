app.service('taskAirExportDataService', function($rootScope) {

	this.getEnquiryHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getEnquiryHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "customerName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getEnquiryHeadArray"
		}, {
			"name" : "Enquiry",
			"width" : "w125px",
			"model" : "enquiryNo",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchEnquiryNo",
			"prefWidth" : "125",
			"calledFrom":"getEnquiryHeadArray"
		}, {
			"name" : "Enquiry Received Date",
			"width" : "w175px",
			"model" : "receivedOn",
			"search" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchReceivedOn",
			"prefWidth" : "175",
			"color" : true,
			"calledFrom":"getEnquiryHeadArray"
		}, {
			"name" : "Quote by Date",
			"width" : "w175px",
			"model" : "quoteBy",
			"search" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchQuoteBy",
			"prefWidth" : "175",
			"color" : true,
			"calledFrom":"getEnquiryHeadArray"
		}, {
			"name" : "Service",
			"width" : "w125px",
			"model" : "serviceName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchServiceName",
			"calledFrom":"getEnquiryHeadArray"

		}, {
			"name" : "Origin",
			"width" : "col-xs-6",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getEnquiryHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-6",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getEnquiryHeadArray"
		} ];
	}

	this.getQuotationHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "customerName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : "Quotation",
			"width" : "w100px",
			"model" : "quotationNo",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchQuotationNo",
			"prefWidth" : "125",
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : "Expires On",
			"width" : "inl_l",
			"model" : "expiresOn",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchValidTo",
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : "Salesman",
			"width" : "w150px",
			"model" : "salesman",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchSalesman",
			"prefWidth" : "150",
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : "Service",
			"width" : "w125px",
			"model" : "serviceName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchServiceName",
			"calledFrom":"getQuotationHeadArray"

		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getQuotationHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : "Quote Status",
			"width" : "col-xs-2",
			"model" : "approved",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchQuoteStatus",
			"calledFrom":"getQuotationHeadArray"
		}, {
			"name" : "Quote Type",
			"width" : "col-xs-3",
			"model" : "quoteType",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchQuoteType",
			"calledFrom":"getQuotationHeadArray"
		} ];
	}
	
	this.getShipmentHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getShipmentHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getShipmentHeadArray"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getShipmentHeadArray"
		}, {
			"name" : "Shipment Date",
			"width" : "inl_l",
			"model" : "shipmentReqDate",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchShipmentReqDate",
			"calledFrom":"getShipmentHeadArray"
		}, {
			"name" : "Customer Service",
			"width" : "w150px",
			"model" : "customerService",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCustomerService",
			"prefWidth" : "150",
			"calledFrom":"getShipmentHeadArray"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getShipmentHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getShipmentHeadArray"
		} ];
	}
	
	
	this.getShipmentPickUpHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getShipmentPickUpHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getShipmentPickUpHeadArray"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getShipmentPickUpHeadArray"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getShipmentPickUpHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getShipmentPickUpHeadArray"
		},{
			"name" : "Our Pickup",
			"width" : "w100px",
			"model" : "isOurPickUp",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchIsOurPickup",
			"prefWidth" : "125",
			"calledFrom":"getShipmentPickUpHeadArray"
		}, {
			"name" : "Pickup Follow up Date",
			"width" : "inl_l",
			"model" : "pickUpFollowUp",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchPickUpFollowUp",
			"calledFrom":"getShipmentPickUpHeadArray"
		}];
	}
	
	
	
	this.getShipmentCFSHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getShipmentCFSHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getShipmentCFSHeadArray"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getShipmentCFSHeadArray"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getShipmentCFSHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getShipmentCFSHeadArray"
		},{
			"name" : "Pickup Actual",
			"width" : "inl_l",
			"model" : "pickUpActualUp",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchPickUpActualUp",
			"calledFrom":"getShipmentCFSHeadArray"
		}];
	
	}
	
	
	
	this.getLinkToMasterHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getLinkToMasterHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getLinkToMasterHeadArray"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getLinkToMasterHeadArray"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getLinkToMasterHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getLinkToMasterHeadArray"
		},{
			"name" : "Carrier",
			"width" : "col-xs-4",
			"model" : "carrierName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCarrierName",
			"calledFrom":"getLinkToMasterHeadArray"
		},{
			"name" : "Flight No",
			"width" : "col-xs-4",
			"model" : "flightNumber",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchFlightNumber",
			"calledFrom":"getLinkToMasterHeadArray"
		},{
			"name" : "MAWB",
			"width" : "col-xs-4",
			"model" : "mawb",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchMawb",
			"calledFrom":"getLinkToMasterHeadArray"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getLinkToMasterHeadArray"
		},{
			"name" : "ETA",
			"width" : "inl_l",
			"model" : "eta",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETA",
			"calledFrom":"getLinkToMasterHeadArray"
		}];
	
	}
	
	
	this.getConsolHeadArray = function() {


		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getConsolHeadArray"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getConsolHeadArray"
		}, {
			"name" : "Master Shipment UID",
			"width" : "w100px",
			"model" : "consolUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchConsolUid",
			"prefWidth" : "125",
			"calledFrom":"getConsolHeadArray"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getConsolHeadArray"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getConsolHeadArray"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getConsolHeadArray"
		},{
			"name" : "Carrier",
			"width" : "col-xs-4",
			"model" : "carrierName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCarrierName",
			"calledFrom":"getConsolHeadArray"
		},{
			"name" : "Flight No",
			"width" : "col-xs-4",
			"model" : "flightNumber",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchFlightNumber",
			"calledFrom":"getConsolHeadArray"
		},{
			"name" : "MAWB",
			"width" : "col-xs-4",
			"model" : "mawb",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchMAWB",
			"calledFrom":"getConsolHeadArray"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getConsolHeadArray"
		},{
			"name" : "ETA",
			"width" : "inl_l",
			"model" : "eta",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETA",
			"calledFrom":"getConsolHeadArray"
		}];
	
	
	}
	
	
	this.getHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getHeadArray"
		}, {
			"name" : "Task",
			"width" : "col-md-1half",
			"model" : "taskName",
			"search" : false,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "taskName",
			"calledFrom":"getHeadArray"
		}, {
			"name" : "Date",
			"width" : "inl_l",
			"model" : "creationTime",
			"search" : false,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchValidTo",
			"calledFrom":"getHeadArray"
		}, {
			"name" : "Reference No",
			"width" : "w150px",
			"model" : "referenceNo",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "referenceNo",
			"prefWidth" : "150",
			"calledFrom":"getHeadArray"
		}, {
			"name" : "Action",
			"width" : "col-md-1half",
			"model" : "claimStatus",
			"search" : false,
			"type" : "taskaction",
			"sort" : false,
			"calledFrom":"getHeadArray"
		} ];
	};

	this.getListBreadCrumb = function() {
		return [ {
			label : "Task",
			state : "layout.myTask"
		}, {
			label : "My Tasks",
			state : "layout.myTask"
		} ];
	};

});