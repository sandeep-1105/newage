app.service('taskAirImportDataService', function($rootScope) {
  
	
	this.getEnquiryHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getEnquiryHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "customerName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getEnquiryHeadArray_Import"
		}, {
			"name" : "Enquiry",
			"width" : "w125px",
			"model" : "enquiryNo",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchEnquiryNo",
			"prefWidth" : "125",
			"calledFrom":"getEnquiryHeadArray_Import"
		}, {
			"name" : "Enquiry Received Date",
			"width" : "w175px",
			"model" : "receivedOn",
			"search" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchReceivedOn",
			"prefWidth" : "175",
			"color" : true,
			"calledFrom":"getEnquiryHeadArray_Import"
		}, {
			"name" : "Quote by Date",
			"width" : "w175px",
			"model" : "quoteBy",
			"search" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchQuoteBy",
			"prefWidth" : "175",
			"color" : true,
			"calledFrom":"getEnquiryHeadArray_Import"
		}, {
			"name" : "Service",
			"width" : "w125px",
			"model" : "serviceName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchServiceName",
			"calledFrom":"getEnquiryHeadArray_Import"

		}, {
			"name" : "Origin",
			"width" : "col-xs-6",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getEnquiryHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-6",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getEnquiryHeadArray_Import"
		} ];
	}
	
	this.getQuotationHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "customerName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : "Quotation",
			"width" : "w100px",
			"model" : "quotationNo",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchQuotationNo",
			"prefWidth" : "125",
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : "Expires On",
			"width" : "inl_l",
			"model" : "expiresOn",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchValidTo",
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : "Salesman",
			"width" : "w150px",
			"model" : "salesman",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchSalesman",
			"prefWidth" : "150",
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : "Service",
			"width" : "w125px",
			"model" : "serviceName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchServiceName",
			"calledFrom":"getQuotationHeadArray_Import"

		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getQuotationHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : "Quote Status",
			"width" : "col-xs-2",
			"model" : "approved",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getQuotationHeadArray_Import"
		}, {
			"name" : "Quote Type",
			"width" : "col-xs-3",
			"model" : "quoteType",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchQuoteType",
			"calledFrom":"getQuotationHeadArray_Import"
		} ];
	}
	
	
	
	
	
	
	this.getHeadArray = function() {
        return [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
				"calledFrom":"getHeadArray_Import"
            },
            {
                "name": "Task",
                "width": "col-md-1half",
                "model": "taskName",
                "search": false,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "taskName",
				"calledFrom":"getHeadArray_Import"
            },{
                "name":"Date",
    			"width":"inl_l",
                "model":"creationTime",
                "search":false,
                "wrap_cell":true,
                "type":"date-range",
                "sort":true,
                "key":"searchValidTo",
				"calledFrom":"getHeadArray_Import"
            },{
                "name":"Reference No",
    			"width":"w150px",
                "model":"referenceNo",
                "search":true,
                "wrap_cell":true,
                "type":"text",
                "sort":true,
                "key":"referenceNo",
    			"prefWidth":"150",
				"calledFrom":"getHeadArray_Import"
            },
            {
                "name": "Action",
                "width": "col-md-1half",
                "model": "claimStatus",
                "search": false,
                "type": "taskaction",
                "sort": false,
				"calledFrom":"getHeadArray_Import"
            } 
        ];
    };

    this.getShipmentHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getShipmentHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getShipmentHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getShipmentHeadArray_Import"
		}, {
			"name" : "Shipment Date",
			"width" : "inl_l",
			"model" : "shipmentReqDate",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchShipmentReqDate",
			"calledFrom":"getShipmentHeadArray_Import"
		}, {
			"name" : "Customer Service",
			"width" : "w150px",
			"model" : "customerService",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCustomerService",
			"prefWidth" : "150",
			"calledFrom":"getShipmentHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getShipmentHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getShipmentHeadArray_Import"
		} ];
	}
    
	this.getLinkToMasterHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getLinkToMasterHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "Carrier",
			"width" : "col-xs-4",
			"model" : "carrierName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCarrierName",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "Flight No",
			"width" : "col-xs-4",
			"model" : "flightNumber",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchFlightNumber",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "MAWB",
			"width" : "col-xs-4",
			"model" : "mawb",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchMawb",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "ETA",
			"width" : "inl_l",
			"model" : "eta",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETA",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}];
	
	}
	
	this.getShipmentPickUpHeadArray = function() {
		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getShipmentPickUpHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getShipmentPickUpHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getShipmentPickUpHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getShipmentPickUpHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getShipmentPickUpHeadArray_Import"
		},{
			"name" : "Our Pickup",
			"width" : "w100px",
			"model" : "isOurPickUp",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchIsOurPickup",
			"prefWidth" : "125",
			"calledFrom":"getShipmentPickUpHeadArray_Import"
		}, {
			"name" : "Pickup Follow up Date",
			"width" : "inl_l",
			"model" : "pickUpFollowUp",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchPickUpFollowUp",
			"calledFrom":"getShipmentPickUpHeadArray_Import"
		}];
	}
	
	
	
	this.getShipmentCFSHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getShipmentCFSHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getShipmentCFSHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getShipmentCFSHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getShipmentCFSHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getShipmentCFSHeadArray_Import"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getShipmentCFSHeadArray_Import"
		}];
	
	}
	
	this.getAtdConfirmationHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getAtdConfirmationHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getAtdConfirmationHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getAtdConfirmationHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getAtdConfirmationHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getAtdConfirmationHeadArray_Import"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getAtdConfirmationHeadArray_Import"
		}];
	
	}
	
	this.getLinkToMasterHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getLinkToMasterHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "Carrier",
			"width" : "col-xs-4",
			"model" : "carrierName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCarrierName",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "Flight No",
			"width" : "col-xs-4",
			"model" : "flightNumber",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchFlightNumber",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "MAWB",
			"width" : "col-xs-4",
			"model" : "mawb",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchMawb",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		},{
			"name" : "ETA",
			"width" : "inl_l",
			"model" : "eta",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETA",
			"calledFrom":"getLinkToMasterHeadArray_Import"
		}];
	
	}
	
	this.getAtaConfirmationHeadArray = function() {

		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getAtaConfirmationHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getAtaConfirmationHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getAtaConfirmationHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getAtaConfirmationHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getAtaConfirmationHeadArray_Import"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getAtaConfirmationHeadArray_Import"
		}];
	
	}
	
	this.getConsolHeadArray = function() {


		return [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false,
			"calledFrom":"getConsolHeadArray_Import"
		}, {
			"name" : $rootScope.appMasterData['Party_Label'],
			"width" : "w200px",
			"model" : "shipperName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "color-text",
			"sort" : true,
			"key" : "searchCustomerName",
			"prefWidth" : "200",
			"calledFrom":"getConsolHeadArray_Import"
		}, {
			"name" : "Master Shipment UID",
			"width" : "w100px",
			"model" : "consolUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchConsolUid",
			"prefWidth" : "125",
			"calledFrom":"getConsolHeadArray_Import"
		}, {
			"name" : "Shipment UID",
			"width" : "w100px",
			"model" : "shipmentUid",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchShipmentUid",
			"prefWidth" : "125",
			"calledFrom":"getConsolHeadArray_Import"
		}, {
			"name" : "Origin",
			"width" : "col-xs-4",
			"model" : "originName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchOriginName",
			"calledFrom":"getConsolHeadArray_Import"

		}, {
			"name" : "Destination",
			"width" : "col-xs-4",
			"model" : "destinationName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDestinationName",
			"calledFrom":"getConsolHeadArray_Import"
		},{
			"name" : "Carrier",
			"width" : "col-xs-4",
			"model" : "carrierName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCarrierName",
			"calledFrom":"getConsolHeadArray_Import"
		},{
			"name" : "Flight No",
			"width" : "col-xs-4",
			"model" : "flightNumber",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchFlightNumber",
			"calledFrom":"getConsolHeadArray_Import"
		},{
			"name" : "MAWB",
			"width" : "col-xs-4",
			"model" : "mawb",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchMAWB",
			"calledFrom":"getConsolHeadArray_Import"
		},{
			"name" : "ETD",
			"width" : "inl_l",
			"model" : "etd",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETD",
			"calledFrom":"getConsolHeadArray_Import"
		},{
			"name" : "ETA",
			"width" : "inl_l",
			"model" : "eta",
			"search" : true,
			"wrap_cell" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchETA",
			"calledFrom":"getConsolHeadArray_Import"
		}];
	
	
	}
	
    this.getListBreadCrumb = function() {
        return [{
                label: "Task",
                state: "layout.myTask"
            },
            {
                label: "My Tasks",
                state: "layout.myTask"
            }
        ];
    };

});