(function(){
app.controller("taskAirImportCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'appMetaFactory','taskAirImportDataService', 'Notification','$timeout',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, appMetaFactory, taskAirImportDataService,  Notification, $timeout) {

        var vm = this;
        vm.tableHeadArr = taskAirImportDataService.getHeadArray();
        vm.dataArr = [];
        vm.searchData = {};
        vm.countryMaster = {};
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.enquiryList = [];
        vm.quotationList = [];
        vm.shipmentList = [];
        vm.pickupList = [];
        vm.cargoNotReceivedList = [];
        vm.aesList = [];
        vm.deliverToCFSList = [];
        vm.linkMasterList = [];
        vm.signOffList = [];
        vm.airLineSubmissionList = [];
        vm.jobCompletionList = [];
        vm.invoiceList = [];
        vm.ataConfirmationList = [];
        vm.canAndInvoiceList = [];
        vm.customsList = [];
        vm.deliveryOrderList = [];
        vm.clearanceList = [];
        vm.podList = [];
        
        $scope.init = function() {
    		$scope.searchDto = {};
    		vm.page = 0;
    		vm.limitArr = [10,15,20];
			vm.limit = 10;
			vm.enquiryHeadArray 	   = taskAirImportDataService.getEnquiryHeadArray();
			vm.quotationHeadArray 	   = taskAirImportDataService.getQuotationHeadArray();
			vm.shipmentHeadArray 	   = taskAirImportDataService.getShipmentHeadArray();
			vm.shipmentPickUpHeadArray = taskAirImportDataService.getShipmentPickUpHeadArray();
			vm.shipmentCFSHeadArray    = taskAirImportDataService.getShipmentCFSHeadArray();
			vm.linkToMasterHeadArray   = taskAirImportDataService.getLinkToMasterHeadArray();
			vm.consolHeadArray         = taskAirImportDataService.getConsolHeadArray();
			$scope.search();
    	}
        
        vm.sortSelection = {
            sortKey: "creationTime",
            sortOrder: "asc"
        }
        vm.claimProcess = function(claimProcessObj) {
        	console.log("claimProcess is called." + claimProcessObj);
        }
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
        }
        vm.actionChange = function(param) {
        	console.log("action Change", param.data);
        	if(param.data.claimStatus === true) {
        		vm.goToState(param.data);
        	} else {
        		appMetaFactory.claim.workFlow({
        			taskId: param.data.taskId, userId: $rootScope.userProfile.id
                }).$promise.then(function(data) {
                    if (data.statusCode == 200) { 
                    	vm.goToState(param.data);
                    } else {
                        $log.debug("Exception while getting Country by Id ", data);
                    	vm.goToState(param.data);
                    }

                }, function(error) {
                    $log.debug("Error ", error);
                });

            
        	}
        }
        
        vm.rowSelect = function(param, linkTab) {

        	console.log(param);
        	
        	$rootScope.nav_src_bkref_key = new Date().toISOString();
        	
        	switch(linkTab) {
        	 case "Enquiry":
        		 	
	    			$state.go("layout.viewSalesEnquiry", { 
	    				enquiryId : param.id,
	    				nav_src_bkref_key: $rootScope.nav_src_bkref_key,
	    				bkState: $state.current.name,
	    				bkParam : 'activiti',
	    				bkTab : linkTab,
	    		 		showActionButton :1
	    		 		});
	    	        break;
        	 case "Quotation":
        			$state.go("layout.salesQuotationView", { 
        				quotationId : param.id,
	    				nav_src_bkref_key: $rootScope.nav_src_bkref_key,
	    				bkState: $state.current.name,
	    				bkParam : 'activiti',
	    				bkTab : linkTab,
	    		 		showActionButton :1
	    		 		});
	    	        break;
        	 case "Shipment":
        		 		$state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;
        	 case "Pickup":
        		 /*$state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;*/
        	 case "DeliveryToCfs":
        		 /*$state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;*/
        	 case "AtdConfirmation":
        		 /*$state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;*/
        	 case "LinkToMaster":
        		 /*$state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;*/
        	 case "SignOff":
        		 $state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;
        	 case "Clearance":
        		 $state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;
        	 case "Pod":
        		 $state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;
        	 case "Customs":
        		 $state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;
        	 case "AtaConfirmation": 
        		 	$state.go("layout.viewNewAirConsol", { 
     			 	consolUid : param.consolUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams)
	    				});
        		 	break
        	 case "CanAndInvoice":
        		 $state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;
        	 case "DeliveryOrder":
        		 	$state.go("layout.viewNewAirConsol", { 
      			 	consolUid : param.consolUid,
 	    				fromState:$state.current.name, 
 	    				fromStateParams : JSON.stringify($stateParams)
 	    				});
         		 	break
        	 default:
        		 /*$state.go("layout.viewCrmShipment", { 
	    				shipmentUid : param.shipmentUid,
	    				fromState:$state.current.name, 
	    				fromStateParams : JSON.stringify($stateParams),
	    				showActionButton :1
	    				});
	    	        break;*/
        	}
        }
        
        vm.openDefaultTab=function(){
        	vm.Tabs='Enquiry';
        }


        $scope.search = function() {
        	
        	$scope.searchDto.selectedPageNumber = vm.page;
			$scope.searchDto.recordPerPage = vm.limit;
			$scope.searchDto.keyword = $stateParams.activeTab;
			
			var tmpSearchDto = angular.copy($scope.searchDto);
			 
			if (tmpSearchDto.searchQuoteBy != null) {
				tmpSearchDto.searchQuoteBy.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchQuoteBy.startDate);
				tmpSearchDto.searchQuoteBy.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchQuoteBy.endDate);
			}
		    
			if (tmpSearchDto.searchReceivedOn != null) {
				tmpSearchDto.searchReceivedOn.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchReceivedOn.startDate);
				tmpSearchDto.searchReceivedOn.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchReceivedOn.endDate);
			}
			
			if (tmpSearchDto.searchValidTo != null) {
				tmpSearchDto.searchValidTo.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchValidTo.startDate);
				tmpSearchDto.searchValidTo.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchValidTo.endDate);
			}
			
			if (tmpSearchDto.searchShipmentReqDate != null) {
				tmpSearchDto.searchShipmentReqDate.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchShipmentReqDate.startDate);
				tmpSearchDto.searchShipmentReqDate.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchShipmentReqDate.endDate);
			}
			
			if (tmpSearchDto.searchPickUpFollowUp != null) {
				tmpSearchDto.searchPickUpFollowUp.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchPickUpFollowUp.startDate);
				tmpSearchDto.searchPickUpFollowUp.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchPickUpFollowUp.endDate);
			}
			
			
			if (tmpSearchDto.searchETD != null) {
				tmpSearchDto.searchETD.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchETD.startDate);
				tmpSearchDto.searchETD.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchETD.endDate);
			}
			
			
			if (tmpSearchDto.searchETA != null) {
				tmpSearchDto.searchETA.startDate = $rootScope.sendApiStartDateTime(tmpSearchDto.searchETA.startDate);
				tmpSearchDto.searchETA.endDate = $rootScope.sendApiEndDateTime(tmpSearchDto.searchETA.endDate);
			}
			
			
        	appMetaFactory.airImportFlow.workFlow({groupName:$stateParams.activeTab}, tmpSearchDto).$promise.then(
	    			function(data) {
	    				var response = [];

	    				if (data.responseCode == 'ERR0') {
	    					response = data.responseObject.searchResult;
	    					vm.totalRecord = data.responseObject.totalRecord;
	    					if(response==null || response==undefined){
	    						response = [];
	    					}
	    				}

	    				switch ($stateParams.activeTab) { 
	        	    	    case "Enquiry":
	        	    	    	vm.enquiryList = response;
	        	    	        break;
	        	    	    case "Quotation":
	        	    	    	vm.quotationList = response;
	        	    	        break;
	        	    	    case "Shipment":
	        	    	    	vm.shipmentList = response;
	        	    	        break;
	        	    	    case "Pickup":
	        	    	    	vm.pickupList = response;
	        	    	        if(vm.pickupList!=undefined && vm.pickupList!=null && vm.pickupList.length!=0){
	        	    	    		for(var i=0;i<vm.pickupList.length;i++){
		        	    	    		if(vm.pickupList[i].isOurPickUp){
		        	    	    			vm.pickupList[i].isOurPickUp = 'Yes';
		        	    	    		}else{
		        	    	    			vm.pickupList[i].isOurPickUp = 'No';
		        	    	    		}
		        	    	    	}
	        	    	    	}
	        	    	        break;
	        	    	    case "DeliveryToCfs":
	        	    	    	vm.deliverToCFSList = response;
	        	    	        break; 
	        	    	    case "AtdConfirmation":
	        	    	    	vm.atdConfirmationList = response;
	        	    	        break;
	        	    	    case "LinkToMaster":
	        	    	        vm.linkMasterList = response;
	        	    	        break;
	        	    	    case "AtaConfirmation":
	        	    	    	vm.ataConfirmationList = response;
	        	    	        break;
	        	    	    case "SignOff":
	        	    	    	vm.signOffList = response;
	        	    	        break;
	        	    	    case "CanAndInvoice":
	        	    	    	vm.canAndInvoiceList = response;
	        	    	        break;
	        	    	    case "Customs":
	        	    	    	vm.customsList = response;
	        	    	        break;
	        	    	    case "DeliveryOrder":
	        	    	    	vm.deliveryOrderList = response;
	        	    	        break;
	        	    	    case "Clearance":
	        	    	    	vm.clearanceList = response;
	        	    	        break;
	        	    	    case "Pod":
	        	    	    	vm.podList = response;
	        	    	        break;
	        	    	    default:

	        	    	}
	    				
	    				$timeout(function() {
	    				    $window.dispatchEvent(new Event("resize"));
	    				}, 100);
	        	    	
	    			},
	                function(errResponse) {
	                    $log.error('Error while fetching AE COunt');
	                });
        	
        	
        }
        
        vm.limitChange = function(item){
			vm.page = 0;
			vm.limit = item;
			$scope.search();
		}
		
		vm.changepage = function(param) {
			vm.page = param.page;
			vm.limit = param.size;
			$scope.search();
		}
        
		vm.changeSearch = function(param){
			for (var attrname in param) {
				$scope.searchDto[attrname] = param[attrname];
			}
			vm.page = 0;
			$scope.search();
		}		
		
		vm.sortChange = function(param){
			vm.searchDto.orderByType = param.sortOrder.toUpperCase();
			vm.searchDto.sortByColumn = param.sortKey;
			$scope.search();
		}
        
        $rootScope.breadcrumbArr = [{label:"Tasks", state:"layout.myTask"}, {label:"My Tasks", state:null}];

        switch ($stateParams.action) { 
    	    case "Search":
    	    	vm.activeTab = $stateParams.activeTab;
    	    	$scope.init();
    	        break;
    	}

	      
    }
]);
}) ();