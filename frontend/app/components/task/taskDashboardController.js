(function () {
	app.controller("taskDashboardCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'appMetaFactory', 'Notification', 'roleConstant',
		function ($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, appMetaFactory, Notification, roleConstant) {

			var vm = this;
			vm.airExportCountObj = {};
			vm.airImportCountObj = {};

			$scope.viewAddSalesEnquiry = function () {
				if ($rootScope.roleAccess(roleConstant.SALE_ENQUIRY_CREATE)) {
					$state.go("layout.addSalesEnquiry", {
						bkState: $state.current.name,
						bkParam: 'activiti'
					});
				}
			}


			$scope.viewSalesQuotationAdd = function () {
				if ($rootScope.roleAccess(roleConstant.SALE_QUOTATION_CREATE)) {
					$state.go("layout.salesQuotationAdd", {
						bkState: $state.current.name,
						bkParam: 'activiti'
					});
				}
			}

			$scope.viewAddNewShipment = function () {
				if ($rootScope.roleAccess(roleConstant.CRM_SHIPMENT_CREATE)) {
					$state.go("layout.addNewShipment", {
						fromScreen: $state.current.name,
						forPurpose: 'activiti'
					});
				}
			}

			$rootScope.breadcrumbArr = [{
				label: "Tasks",
				state: "layout.myTask"
			}, {
				label: "My Tasks",
				state: null
			}];

			switch ($stateParams.action) {
				case "Count":
					appMetaFactory.airImportExportCount.workFlow().$promise.then(
						function (data) {
							vm.airExportCountObj = data.airExportTaskCountDto;
							vm.airImportCountObj = data.airImportTaskCountDto;
							console.debug(data)
						},
						function (errResponse) {
							vm.airExportCountObj = {};
							vm.airImportCountObj = {};
							$log.error('Error while fetching AE COunt');
						});

					break;
				default:

			}


		}
	]);
})();