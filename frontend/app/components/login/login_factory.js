(function() {

	app.factory("AuthApi", ['$resource', function($resource) {
		return $resource("/api/v1/auth/login", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("LogoutApi", ['$resource', function($resource) {
		return $resource("/api/v1/auth/logout", {}, {
			get : {
				method : 'GET',
				params : {},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("TimeZoneListAPI", ['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/timezones", {}, {
			get : {
				method : 'GET',
				params : {},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("ForgotApi", ['$resource', function($resource) {
		return $resource("/api/v1/auth/sendforgotpasswordlink", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("UpdateForgotApi", ['$resource', function($resource) {
		return $resource("/api/v1/auth/updatepassword", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("AppEnumApi", ['$resource', function($resource) {
		return $resource("/api/v1/application/getallenums", {}, {
			get : {
				method : 'GET',
				params : {},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("AppNlsApi", ['$resource', function($resource) {
		return $resource("/api/v1/language/nls/:language", {}, {
			get : {
				method : 'GET',
				params : {
					language : ''
				},
				isArray : false
			}
		});
	}]);
		
	

	app.factory("UserRecordAccessLevel", ['$resource', function($resource) {
		return $resource("/api/v1/auth//get/user/:id", {}, {
			get : { method : 'GET',params : {id : ''},isArray : false }
		});
	}]);
	
	
	
})();
