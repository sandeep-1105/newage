/**
 * Created by hmspl on 12/4/16.
 */
app.controller('dashboardCtrl',['$rootScope', '$scope', function($rootScope, $scope){
    $scope.labels = ["AI (15)", "AE (25)", "FCL (60)"];
    $scope.data = [15, 25, 60];
    $scope.colours = ['#4dd0e1', '#ffd54f', '#81c784'];
    $scope.labels1 = ["LE (30)", "LI (60)", "AE (20)"];
    $scope.data1 = [40, 60, 20];
    $scope.colours1 = ['#4fc3f7', '#e57373', '#7986cb'];

    //dashboard TASK DATA
    $scope.taskdata=[
        {
            tasktheme: "disable",
            shipid : "#0410012",
            aircode : "AE",
            routo : "DXB",
            roufrm : "MAA",
            dename : "LG Electronics",
            curms : "Booked",
            nxtms : "Pickup",
            curdate : "21/04/16",
            nxtdate : "25/04/16",
            icon:"icon-bairplane"
        },
        {
            tasktheme: "active",
            shipid : "#0410012",
            aircode : "LE",
            routo : "DXB",
            roufrm : "BOM",
            dename : "Blue Chip Computers",
            curms : "Received",
            nxtms : "Loading",
            curdate : "20/04/16",
            nxtdate : "24/04/16",
            icon:"icon-ocean"
        },
        {
            tasktheme: "active",
            shipid : "#0410013",
            aircode : "LE",
            routo : "DXB",
            roufrm : "BLR",
            dename : "Landmark Group",
            curms : "Sailed",
            nxtms : "Transit Update",
            curdate : "19/04/16",
            nxtdate : "23/04/16",
            icon:"icon-ocean"
        },
        {
            tasktheme: "disable",
            shipid : "#0510012",
            aircode : "AI",
            routo : "LHR",
            roufrm : "DXB",
            dename : "Baby Shop",
            curms : "Cargo Picked",
            nxtms : "Flight Details",
            curdate : "21/04/16",
            nxtdate : "23/04/16",
            icon:"icon-bairplane"
        },
        {
            tasktheme: "active",
            shipid : "#0510013",
            aircode : "LI",
            routo : "NYC",
            roufrm : "DXB",
            dename : "Al Abbas",
            curms : "Nomination Sent",
            nxtms : "Cargo Readiness",
            curdate : "21/04/16",
            nxtdate : "23/04/16",
            icon:"icon-ocean"
        }

    ];

    $rootScope.setNavigate1("Dashboard");


}]);
