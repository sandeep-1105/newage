app.controller('jobledgerController',['$rootScope', '$scope', '$window', '$state', '$stateParams', 'AutoCompleteService', 'appMetaFactory', 
        'Notification', 'downloadFactory', 'downloadMultipleFactory', '$modal',
    function($rootScope, $scope, $window, $state, $stateParams, AutoCompleteService, appMetaFactory, 
        Notification, downloadFactory, downloadMultipleFactory, $modal) {

    $scope.AutoCompleteService = AutoCompleteService;

    $scope.fetchData = function(searchDto) {
        appMetaFactory.fetchJobLedger.query(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                $scope.jobLedgerResDto = data.responseObject;
                Notification.success($rootScope.nls["ERR2000400"]);
            } else {
                Notification.error($rootScope.nls[data.responseCode]);
            }
        }, function(error) {
            console.log("Job Ledger fetch Failed", error);
        });
    }

    $scope.checkZero = function(val, locale, decimalPoint) {

        if (val == 0) {
            return null;
        }

        val = $rootScope.currencyFormatDisplay(locale, decimalPoint, val);

        return val;
    }

    $scope.back = function() {
        if ($stateParams.fromState != null) {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }
    }

    $scope.getInvoiceCrnView = function(jobLedgerReqDto, docObj) {
        var params = {};
        if (docObj.documentTypeCode == 'INV') {
            var param = {
                invoiceId: docObj.invoiceCreditNoteId,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify(jobLedgerReqDto)
            }
            $state.go('layout.viewInvoice', param);
        } else {
            if (docObj.documentTypeCode == 'CRN' && docObj.adjustmentInvoiceNo != null) {
                var param = {
                    creditNoteRevenueId: docObj.invoiceCreditNoteId,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify(jobLedgerReqDto)
                }
                $state.go('layout.viewCreditNoteRevenue', param);
            } else {
                var param = {
                    creditNoteCostId: docObj.invoiceCreditNoteId,
                    fromState: $state.current.name,
                    fromStateParams: JSON.stringify(jobLedgerReqDto)
                }
                $state.go('layout.viewCreditNoteCost', param);
            }
        }
    }

    switch ($stateParams.action) {
        case "VIEW":
            $scope.jobLedgerReqDto = {};
            $scope.jobLedgerReqDto.serviceName = $stateParams.serviceName;
            $scope.jobLedgerReqDto.consolUid = $stateParams.consolUid;
            $scope.jobLedgerReqDto.shipmentUid = $stateParams.shipmentUid;
            $scope.jobLedgerReqDto.shipmentArr = $stateParams.shipmentArr;
            $scope.jobLedgerReqDto.serviceUid = $stateParams.serviceUid;
            $scope.jobLedgerReqDto.documentUid = $stateParams.documentUid;
            $scope.fromState = $stateParams.fromState;
            $scope.shipmentUid = $stateParams.shipmentUid;

            if ($scope.jobLedgerReqDto.shipmentArr != null && $scope.jobLedgerReqDto.shipmentArr != undefined && $scope.jobLedgerReqDto.shipmentArr != '') {
                $scope.shipmentLinkList = $scope.jobLedgerReqDto.shipmentArr.split(',');
                $scope.shipmentLinkList.splice(0, 0, "");
            }



            $scope.fetchData($scope.jobLedgerReqDto);

            $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: null
            }, {
                label: "Job Ledger",
                state: "layout.jobledger"
            }];
            break;
    }




    $scope.openJobLedgerReport = function(jobLedgerReqDto) {
        $scope.reportListMaking(jobLedgerReqDto);
    }

    $scope.reportList = [];
    var allRepoVar = {};
    $scope.allRepoVarModel = {};
    $scope.allRepoVarModel.allReportSelected = false;
    $scope.reportLoadingImage = {};
    $scope.reportLoadingImage.status = false;

    $scope.reportListMaking = function(jobLedgerReqDto) {
        //$scope.reportObject =angular.copy(jobLedgerReqDto);
        $scope.reportList = [];
        $scope.reportDetailData1 = {};
        $scope.reportDetailData1 = {
            reportName: "MASTER_COST_SHEET",
            reportDisplayName: 'Master Cost Sheet'
        };
        $scope.reportList.push($scope.reportDetailData1);

        $scope.reportDetailData2 = {};
        $scope.reportDetailData2 = {
            reportName: "SHIPMENT_COST_SHEET",
            reportDisplayName: 'Shipment Cost Sheet'
        };
        $scope.reportList.push($scope.reportDetailData2);

        /*$scope.reportDetailData2 ={};
        $scope.reportDetailData2 = {resourceRefId:resourceRefId,reportName : "INVOICE_WITH_OUT_LOGO", reportDisplayName : 'Invoice without Logo',key:'invoiceWithoutLogo'};
        $scope.reportList.push($scope.reportDetailData2);*/

    }

    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print", "eMail"];

    $scope.reportModal = function(resourceRefId, reportRefName, key) {
        if ($scope.reportList != undefined && $scope.reportList != null) {
            $scope.allRepoVarModel.allReportSelected = false;
            $scope.isGenerateAll = false;
            for (var i = 0; i < $scope.reportList.length; i++) {
                $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
            }
        }
        $scope.changeReportDownloadFormat("PDF");
        $scope.dataResourceId = resourceRefId;
        $scope.dataReportName = reportRefName;
        $scope.myReportModal = {};
        $scope.myReportModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/report/report_opt.html',
            show: false
        });
        $scope.myReportModal.$promise.then($scope.myReportModal.show);

    };


    $scope.selectAllReports = function() {
        for (var i = 0; i < $scope.reportList.length; i++) {
            $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
        }
        if (!$scope.allRepoVarModel.allReportSelected) {
            $scope.isGenerateAll = false;
        }
    }

    $scope.chkReportStatus = function(chkFlag, detObj) {
        console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
    }

    $scope.changeReportDownloadFormat = function(dType) {
        console.log("Download type Changed :: ", dType);
        $scope.downloadFileType = dType;
        if (dType === 'PDF') {
            $scope.generate.TypeArr = ["Download", "Preview", "Print"];
        } else if (dType === 'eMail') {
            $scope.generate.TypeArr = ["eMail"];
        } else {
            $scope.generate.TypeArr = ["Download"];
        }
    }

    $scope.isSingle = false;
    $scope.isMultiple = false;
    $scope.isGenerateAll = false;

    $scope.generateAll = function() {
        $scope.dataReportName = null;
        var flag = false;
        for (var i = 0; i < $scope.reportList.length; i++) {
            if ($scope.reportList[i].isSelected) {
                flag = true
                break;
            }
        }
        if (!flag) {
            $scope.isGenerateAll = false;
            Notification.warning("Please select atleast one.......)")
        } else {
            $scope.isGenerateAll = true;
            $scope.reportData = {};
            $scope.myReportModal = {};
            $scope.myReportModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/report/report_opt.html',
                show: false
            });
            $rootScope.mainpreloder = false;
            $scope.myReportModal.$promise.then($scope.myReportModal.show);
        }
    }

    $scope.generateAllReports = function() {
        if ($scope.reportList == undefined || $scope.reportList == null || $scope.reportList.length == 0) {} else {
            //Using for mail sending for download reports 
            $scope.emailReportList = [];
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    $scope.emailReport = {};
                    $scope.emailReport.resourceId = $scope.reportList[i].resourceRefId;
                    $scope.emailReport.reportName = $scope.reportList[i].reportName;
                    $scope.emailReport.downloadOption = $scope.downloadOption;
                    $scope.emailReport.downloadFileType = $scope.downloadFileType;
                    $scope.emailReportList.push($scope.emailReport);
                }
            }

        }
        if ($scope.emailReportList != undefined && $scope.emailReportList.length > 0) {
            $scope.isMultiple = true;
            $scope.isSingle = false;
        } else {
            $scope.isSingle = true;
            $scope.isMultiple = false;
        }
        var flag = false;
        if ($scope.isSingle) {
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    flag = true
                    $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
                    $timeout(function() {
                        $scope.reportLoadingImage.status = false;
                    }, 1000);
                }
                if (flag) {
                    break;
                }
            }
            if (!flag) {
                Notification.warning("Please select atleast one.......)");
                return;
            }
        } else if ($scope.isMultiple) {
            $scope.downloadGenericCallMultiple($scope.emailReportList, $scope.downloadOption, $scope.isMultiple, "PDF", $scope.reportList[0].resourceRefId)
            console.log("Nothig selected");
        }
    }


    $scope.downloadGenericCall = function(id, downOption, repoName) {
        $scope.downloadOption = downOption;
        if ($scope.isGenerateAll) {
            $scope.generateAllReports();
        } else {
            var reportDownloadRequest = {
                resourceId: id,
                jobLedgerResDto: $scope.jobLedgerResDto,
                downloadOption: downOption, // "Download","Preview","Print"
                downloadFileType: $scope.downloadFileType, //PDF
                reportName: repoName,
                single: true,
            };
            if (downOption === 'eMail') {
                var flag = checkMail();
                if (!flag) {
                    Notification.error($rootScope.nls["ERR275"]);
                    return false;
                } else {
                    $rootScope.mainpreloder = true;
                    downloadFactory.download(reportDownloadRequest);
                    $timeout(function() {
                        $rootScope.mainpreloder = false;
                        $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                    }, 3000);
                }
            } else {
                $rootScope.mainpreloder = true;
                downloadFactory.download(reportDownloadRequest);
            }
        }
    }


    function checkMail() {
        var flag = false;
        if ($scope.reportObject != null && $scope.reportObject != undefined) {
            if ($scope.reportObject.party != null && $scope.reportObject.party != undefined) {
                if ($scope.reportObject.party.partyAddressList != null && $scope.reportObject.party.partyAddressList != undefined && $scope.reportObject.party.partyAddressList.length > 0) {

                    for (var i = 0; i < $scope.reportObject.party.partyAddressList.length; i++) {
                        if ($scope.reportObject.party.partyAddressList[0].addressType == "Primary") {
                            var emailList = $scope.reportObject.party.partyAddressList[0].email;
                            if (emailList == null || emailList == undefined) {
                                flag = false;
                            } else {
                                flag = true;
                                break;
                            }
                        } else {
                            flag = false;
                        }
                    }
                }
            }
        }
        return flag;
    }


    $scope.downloadGenericCallMultiple = function(emailReportList, downOption, multiple, type, id) {
        var reportDownloadRequest = {
            emailReportList: emailReportList,
            downloadOption: downOption, // "Download","Preview","Print"
            downloadFileType: type, //PDF
            isMultiple: multiple,
            resourceId: id,
            screenType: 'Invoice'
        };
        if (downOption === 'eMail') {
            var flag = checkMail();
            if (!flag) {
                Notification.error($rootScope.nls["ERR275"]);
                return false;
            } else {
                $rootScope.mainpreloder = true;
                downloadMultipleFactory.download(reportDownloadRequest);
                $timeout(function() {
                    $rootScope.mainpreloder = false;
                    $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                }, 3000);
            }
        } else {
            $rootScope.mainpreloder = false;
            for (var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
                var reportFileName = '';
                reportFileName = reportDownloadRequest.emailReportList[i].reportName;
                var reportDownloaddRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: reportFileName,
                    single: true
                };
                $rootScope.mainpreloder = true;
                downloadFactory.download(reportDownloaddRequest);
                if (downOption === 'eMail') {
                    $timeout(function() {
                        $rootScope.mainpreloder = false;
                        $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                    }, 3000);
                }
            }
        }
    }



}]);