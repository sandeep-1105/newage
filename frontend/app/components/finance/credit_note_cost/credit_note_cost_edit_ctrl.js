/**
 * Created by hmspl on 12/4/16.
 */
app.controller('credit_note_edit_ctrl',['$rootScope', '$http', '$timeout', '$scope', '$location', '$modal', 'ngDialog', 'QuotationAttachment', 'NavigationService',
    'invoiceService', 'DaybookList', 'PartiesList', 'PartyMasterService', 'CurrencyMasterSearchKeyword', '$stateParams', '$state',
    'PartyAccountListById', 'CurrencySearchExclude', 'InvoiceAdd', 'ValidateUtil', 'ngProgressFactory', 'cloneService', 'DaybookListByDocumentType',
    'downloadFactory', 'ShipmentGet', 'addressJoiner', 'ConsolGet', 'ServiceGetByUid', 'appConstant', 'CommonValidationService', 
    'ConsolGetByUid', 'Notification', 'roleConstant', 'invoiceCreditNoteValidationService', 'AutoCompleteService', 'documentTypeMasterFactory', 
    function($rootScope, $http, $timeout, $scope, $location, $modal, ngDialog, QuotationAttachment, NavigationService,
    invoiceService, DaybookList, PartiesList, PartyMasterService, CurrencyMasterSearchKeyword, $stateParams, $state,
    PartyAccountListById, CurrencySearchExclude, InvoiceAdd, ValidateUtil, ngProgressFactory, cloneService, DaybookListByDocumentType,
    downloadFactory, ShipmentGet, addressJoiner, ConsolGet, ServiceGetByUid, appConstant, CommonValidationService, 
    ConsolGetByUid, Notification, roleConstant, invoiceCreditNoteValidationService, AutoCompleteService, documentTypeMasterFactory) {

    $scope.$invoiceCreditNoteValidationService = invoiceCreditNoteValidationService;
    $scope.$AutoCompleteService = AutoCompleteService;
    $scope.$roleConstant = roleConstant;
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 10;
    $scope.limitArrActive = [10, 15, 20];
    $scope.pageActive = 0;
    $scope.limitActive = 10;
    $scope.totalRecordActive = 10;
    $scope.searchDto = {};
    $scope.activeSearchDto = {};
    $scope.documentType = "CRN-COST";
    $scope.disableSubmitBtn = false;

    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            console.log("Enable btn ,", new Date());
            $scope.disableSubmitBtn = false;
        }, 300);
    }
    $scope.creditNoteTableCheck = {};

    $scope.init = function() {
        console.log("Init Started...");
        $rootScope.clientMessage = null;
        $scope.invoiceCreditNote = {};
        $scope.invoiceCreditNote.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
        $scope.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
        $scope.firstFocus = true;
        $scope.shipmentCreditNote = false;
        $scope.invoiceCreditNote.invoiceCreditNoteStatus = 'Awaiting_Payment';

        $scope.invoiceCreditNote.locationMaster = $rootScope.userProfile.selectedUserLocation;

        $scope.progressBar();
        $rootScope.setNavigate3($scope.invoiceCreditNote.id != null ? "Edit Credit Note Cost" : "Add Credit Note Cost");
        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";
        $scope.getShipmentOrConsolData();
        $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList = [{}];
        console.log("Init finished");
    }

    $scope.getDocumentTypeByCode = function() {
        documentTypeMasterFactory.findByCode.query({
            documentTypeCode: 'CRN'
        }).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.documentTypeMaster = data.responseObject;
            } else {
                console.log("Exception while getting DocumentType by code ", data.responseDescription);
            }
        }, function(error) {
            console.log("Error ", error);
        });
    };
    $scope.getDocumentTypeByCode();
    $scope.clickOnTabView = function(tab) {
        if (tab == 'charge') {
            $scope.Tabs = tab;
        }
        if (tab == 'attachments') {
            $scope.Tabs = tab;
        }
        if (tab == 'notes' && $rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_COST_NOTES_CREATE)) {
            $scope.Tabs = tab;
        }
    }
    $scope.getShipmentOrConsolData = function() {
        if ($stateParams.shipmentServiceId != null) {
            ServiceGetByUid.get({
                serviceUid: $stateParams.shipmentServiceId
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.shipmentServiceDetail = data.responseObject;
                    $scope.shipmentServiceList = [];
                    $scope.shipmentServiceList.push($scope.shipmentServiceDetail);

                    $scope.invoiceCreditNote.shipmentUid = $scope.shipmentServiceDetail.shipmentUid;
                    if ($scope.shipmentServiceDetail.consolUid != null &&
                        $scope.shipmentServiceDetail.consolUid != undefined &&
                        $scope.shipmentServiceDetail.consolUid != '') {
                        ConsolGetByUid.get({
                            consolUid: $scope.shipmentServiceDetail.consolUid
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                $scope.consol = data.responseObject;
                            }
                        }, function(error) {
                            console.log("Consol get Failed : " + error)
                        });
                        $scope.invoiceCreditNote.masterUid = $scope.shipmentServiceDetail.consolUid;
                    }
                    $scope.shipmentInvoice = true;

                    if ($scope.shipmentServiceDetail != null &&
                        $scope.shipmentServiceDetail != undefined &&
                        $scope.shipmentServiceDetail != '') {
                        if ($scope.shipmentServiceDetail.serviceMaster != null &&
                            $scope.shipmentServiceDetail.serviceMaster != undefined &&
                            $scope.shipmentServiceDetail.serviceMaster != '') {
                            $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());
                            if ($scope.shipmentServiceDetail.serviceMaster.importExport == 'Export') {
                                if ($scope.shipmentServiceDetail.etd != null &&
                                    new Date($scope.shipmentServiceDetail.etd) < new Date()) {
                                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.shipmentServiceDetail.etd);
                                }
                            } else if ($scope.shipmentServiceDetail.serviceMaster.importExport == 'Import') {
                                if ($scope.shipmentServiceDetail.eta != null &&
                                    new Date($scope.shipmentServiceDetail.eta) < new Date()) {
                                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.shipmentServiceDetail.eta);
                                }
                            }
                        }
                    }

                    var newdate = $rootScope.convertToDate($scope.invoiceCreditNote.invoiceCreditNoteDate);
                    $scope.invoiceCreditNote.dueDate = $rootScope.dateToString(newdate.add('days', $rootScope.appMasterData['invoice.duedate']));
                    $scope.changeType();
                }
            }, function(error) {
                console.log("shipmentService get Failed : " + error)
            });

            $scope.shipmentCreditNote = true;
        }

        //credit note cost from consol
        else {
            ConsolGet.get({
                id: $stateParams.consolId
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.consol = data.responseObject;
                    $scope.consol = $scope.consol;
                    $scope.invoiceCreditNote.masterUid = $scope.consol.consolUid;
                    $scope.getConsolShipmentServices();
                    if ($scope.consol.serviceMaster != null &&
                        $scope.consol.serviceMaster != undefined &&
                        $scope.consol.serviceMaster != '') {
                        $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());
                        if ($scope.consol.serviceMaster.importExport == 'Export') {
                            if ($scope.consol.etd != null &&
                                new Date($scope.consol.etd) < new Date()) {
                                $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.consol.etd);
                            }
                        } else if ($scope.consol.serviceMaster.importExport == 'Import') {
                            if ($scope.consol.eta != null &&
                                new Date($scope.consol.eta) < new Date()) {
                                $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.consol.eta);
                            }
                        }
                    }
                    var newdate = $rootScope.convertToDate($scope.invoiceCreditNote.invoiceCreditNoteDate);
                    $scope.invoiceCreditNote.dueDate = $rootScope.dateToString(newdate.add('days', $rootScope.appMasterData['invoice.duedate']));
                    $scope.changeType();
                }
            }, function(error) {
                console.log("Consol get Failed : " + error)
            });
            $scope.shipmentCreditNote = false;
        }
    }



    $scope.changeType = function(id) {
        $scope.invoiceCreditNote.partyAddress = {};
        if ($scope.invoiceCreditNote.customerAgent == null || $scope.invoiceCreditNote.customerAgent == undefined ||
            $scope.invoiceCreditNote.customerAgent == '') {
            return;
        }
        //from shipment
        if ($stateParams.shipmentId != null && $stateParams.shipmentId != undefined &&
            ($stateParams.consolId == null || $stateParams.consolId == undefined)) {

            if ($scope.shipmentServiceDetail.serviceMaster.importExport === "Export") {
                if ($scope.invoiceCreditNote.customerAgent == 'Agent') {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.documentList[0].agent;
                    $scope.selectedParty('addr1');
                } else {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.party;
                    $scope.selectedParty('addr1');
                }
            } else if ($scope.shipmentServiceDetail.serviceMaster.importExport === "Import") {
                if ($scope.invoiceCreditNote.customerAgent == 'Agent') {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.documentList[0].agent;
                    $scope.selectedParty('addr1');
                } else {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.documentList[0].consignee;
                    $scope.selectedParty('addr1');
                }
            }


        }
        //from consol
        else {

            if ($scope.consol != undefined && $scope.consol != null && $scope.consol.serviceMaster.importExport === "Export") {

                if ($scope.invoiceCreditNote.customerAgent == 'Customer') {
                    if ($scope.consol.shipmentLinkList.length == 1) {
                        $scope.invoiceCreditNote.party = $scope.consol.shipmentLinkList[0].service.party;
                        $scope.selectedParty('addr1');
                    }
                } else {
                    $scope.invoiceCreditNote.party = $scope.consol.agent;
                    $scope.selectedParty('addr1');
                }

            } else if ($scope.consol != undefined && $scope.consol != null && $scope.consol.serviceMaster.importExport === "Import") {

                if ($scope.invoiceCreditNote.customerAgent == 'Customer') {
                    if ($scope.consol.shipmentLinkList.length == 1) {
                        $scope.invoiceCreditNote.party = $scope.consol.shipmentLinkList[0].service.documentList[0].consignee;
                        $scope.selectedParty('addr1');
                    }
                } else {
                    $scope.invoiceCreditNote.party = $scope.consol.agent;
                    $scope.selectedParty('addr1');
                }

            }



        }
        if ($scope.invoiceCreditNote.party != null && $scope.invoiceCreditNote.party != undefined)
            $scope.invoiceCreditNote.subledger = $scope.invoiceCreditNote.party.partyCode;
        $scope.getPartyAddress($scope.invoiceCreditNote.party);
        $rootScope.navigateToNextField(id);
    }

    $scope.progressBar = function() {
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('creditnote-panel'));
        $scope.contained_progressbar.setAbsolute();
    }

    /*BUTTON GROUP SCOPE VALUE*/
    $scope.btnGrp = {};
    $scope.btnGrp.TypeArr = ["import", "export", "both"];

    /*PAGE CHANGE FUNCTION*/
    $scope.add = function() {
        //	consolService.set({});
        $state.go("layout.addCreditNoteCost");

    };
    $scope.edit = function() {
        //consolService.set($scope.consol);
        $state.go("layout.editCreditNoteCost");
    };


    $scope.before = function() {
        $scope.afterbefore = "BEFORE";
        $scope.tmpsearch(-1);

    }
    $scope.after = function(index) {
        $scope.afterbefore = "AFTER";
        $scope.tmpsearch(1)
    }
    $scope.attachConfig = {
        "isEdit": true,
        "isDelete": true,
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                "width": "w300px"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file",
                "width": "w200px"
            },
            {
                "name": "Actions",
                "model": "action",
                "type": "action",
                "width": "w100px"
            }
        ]

    }


    $scope.manipulateAttach = function(param) {

        console.log("manipulateAttach param", param);

        var displayObject = new Object();

        displayObject.id = param.data.id;
        displayObject.refNo = param.data.refNo;
        displayObject.fileName = param.data.fileName;
        displayObject.file = param.data.file;
        displayObject.fileContentType = param.data.fileContentType;
        displayObject.tmpFile = param.data.tmpFile;
        displayObject.systemTrack = param.data.systemTrack;

        if (param.index == null) {
            $scope.attachConfig.data.push(param.data);
        } else {
            $scope.attachConfig.data.splice(param.index, 1, param.data);
        }

    }

    $scope.removeAttach = function(param) {
        $scope.attachConfig.data.splice(param.index, 1);
    }

    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/invoicecreditnote/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }




    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print"];


    $scope.backStock = function() {
        NavigationService.set(null);
        NavigationService.setLocation(null);
        NavigationService.setSubData1(null);
        $location.path('/air/stock_management_list');
    }




    $scope.cancelCrnCost = function() {
        if ($scope.crnCostForm.$dirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(
                function(value) {
                    if (value == 1 && $scope.invoiceCreditNote.id == null) {
                        $scope.saveCrnCost();
                    } else if (value == 1 && $scope.invoiceCreditNote.id != null) {
                        //$scope.updateCrnCost();
                    } else if (value == 2) {
                        $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                    } else {
                        console.log("Nothing selected..Cancel")
                    }
                });

        } else {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }
    };


    $scope.selectedDaybook = function(id) {
        if ($scope.validateCrnCost(1))
            $rootScope.navigateToNextField(id);


    };
    $scope.daybookNameRender = function(item) {
        return {
            label: item.daybookName,
            item: item
        }

    }




    //party list


    /* Party Select Picker */
    $scope.showPartyList = false;
    $scope.shipmentPartylistConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
                "title": "partyName",
                seperator: false
            },
            {
                "title": "partyCode",
                seperator: true
            }
        ]
    }


    $scope.showParty = function() {
        //console.log("Showing List of Party...");
        $scope.selectedItem = -1;
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.ajaxPartyEvent(null);
    }

    $scope.ajaxPartyEvent = function(object) {
        console.log("Ajax Party Event method : " + object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        if (object == "") {
            $scope.invoiceCreditNote.partyAddress = {};
        }
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partyList = data.responseObject.searchResult;
                    //$scope.showPartyList=true;
                    return $scope.partyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );



    }

    $scope.selectedParty = function(id) {

        $scope.errorMap = new Map();
        if ($scope.validateCrnCost(4)) {
            $rootScope.navigateToNextField(id);
        }
        $scope.invoiceCreditNote.subledger = $scope.invoiceCreditNote.party.partyCode;
        $scope.getPartyAddress($scope.invoiceCreditNote.party);


        $scope.invoiceCreditNote.partyAccount = null;
        $scope.invoiceCreditNote.billingCurrency = null;
        $scope.invoiceCreditNote.roe = null;


        var object = null;
        $scope.searchDto = {};
        $scope.searchDto.keyword = "" ;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        PartyAccountListById.fetch({
            "partyId": $scope.invoiceCreditNote.party.id
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partyAccountList = data.responseObject.searchResult;

                    if ($scope.partyAccountList != null && $scope.partyAccountList.length > 0) {
                        for (var i = 0; i < $scope.partyAccountList.length; i++) {
                            if ($scope.partyAccountList[i].accountMaster.accountName == 'Creditor' &&
                                $scope.partyAccountList[i].currencyMaster.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                                $scope.invoiceCreditNote.partyAccount = $scope.partyAccountList[i];
                                break;
                            }
                        }


                        if ($scope.invoiceCreditNote.partyAccount == null || $scope.invoiceCreditNote.partyAccount == undefined || $scope.invoiceCreditNote.partyAccount == '') {
                            $scope.invoiceCreditNote.partyAccount = $scope.partyAccountList[0];
                        }
                    }
                    //Billing customer for the party account
                    if ($scope.invoiceCreditNote.partyAccount.billingSubLedgerCode != null &&
                        $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode.id != null) {
                        $scope.invoiceCreditNote.party = $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode;
                        $scope.getPartyAddress($scope.invoiceCreditNote.party);
                        $scope.invoiceCreditNote.subledger = $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode.partyCode;
                    }
                    $scope.partyAccountCurrency($scope.invoiceCreditNote.partyAccount);

                }
            },
            function(errResponse) {
                console.error('Error while fetching Party account');
            }
        );




    };




    $scope.cancelParty = function() {
        $scope.showPartyList = false;
    }


    $scope.getPartyAddress = function(obj) {
        if (obj != null && obj.id != null) {
            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {
                    $scope.invoiceCreditNote.partyAddress = addressJoiner.joinAddressLineByLine($scope.invoiceCreditNote.partyAddress, obj, obj.partyAddressList[i]);
                    break;
                }
            }
        }

    }


    $scope.showPartyAddressList = false;
    $scope.partyAddressListConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddress = function() {
        console.log("Showing Party address list...");
        $scope.panelTitle = "Party Address";
        $scope.showPartyAddressList = true;
        $scope.selectedItem = -1;
        $scope.partyAddressList = [];
        for (var i = 0; i < $scope.invoiceCreditNote.party.partyAddressList.length; i++) {
            $scope.partyAddressList.push($scope.invoiceCreditNote.party.partyAddressList[i]);
        }
    };

    $scope.selectedPartyAddress = function(obj) {
        $scope.invoiceCreditNote.partyAddress = addressJoiner.joinAddressLineByLine($scope.invoiceCreditNote.partyAddress, $scope.invoiceCreditNote.party, obj);
        $scope.cancelPartyAddress();
    };

    $scope.cancelPartyAddress = function() {
        $scope.showPartyAddressList = false;
    }

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }



    $scope.routePartyEvent = function(fromObjModel, isNew, partyMaster) {

        $rootScope.nav_src_bkref_key = new Date().toISOString();
        var param = {
            nav_src_bkref_key: $rootScope.nav_src_bkref_key,
            forObj: fromObjModel,
            category: "CreditNoteCost"
        };
        if (!isNew) {
            if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                param.partyId = partyMaster.id;
                $state.go("layout.editParty", param);
            } else {
                console.log("Party not selected...");
            }
        } else {
            $state.go("layout.addParty", param);
        }
    }

    // invoice header currency select picker
    $scope.listCurrencyConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
                "title": "currencyName",
                seperator: false
            },
            {
                "title": "currencyCode",
                seperator: true
            }

        ]
    }

    $scope.showCurrency = function(selectedCurrencyRateMasterIndex, currency) {
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.selectedCurrencyRateMasterIndex = selectedCurrencyRateMasterIndex;
        /*$scope.ajaxCurrencyEvent(null)*/
    }


    $scope.currencyRender = function(item) {
        return {
            label: item.currencyCode,
            item: item
        }
    }

    $scope.accountCodeRender = function(item) {
        return {
            label: item.accountCode,
            item: item
        }
    }



    $scope.cancelCurrency = function() {
        $scope.showCurrencyList = false;
    }

    $scope.selectedCurrency = function(id) {


        $scope.validateCrnCost(10);

        if ($rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != null && $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != undefined &&
            $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != '') {
            $scope.invoiceCreditNote.roe = $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id].csell;
        } else {

            $scope.invoiceCreditNote.roe = null;
        }

        if ($scope.invoiceCreditNote.billingCurrency.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
            $scope.invoiceCreditNote.roe = 1;
        }


        if ($scope.tempParam != null && $scope.tempParam != undefined && $scope.tempParam != '')
            $scope.calculateChargeAmount($scope.tempParam);
        $rootScope.navigateToNextField(id);

        $scope.updateChargeListData();
    }




    $scope.currencyNameRender = function(item) {
        return {
            label: item.currencyName,
            item: item
        }
    }
    $scope.selectedAccountMaster = function(id) {

        if ($scope.validateCrnCost(9))
            $rootScope.navigateToNextField(id);
        $scope.partyAccountCurrency($scope.invoiceCreditNote.partyAccount);

    }

    $scope.clearPartyAddress = function(object) {
        if (object == "" || object == undefined || object == null || object.id == null) {
            $scope.invoiceCreditNote.partyAddress = {};
        }
    }
    $scope.partyAccountCurrency = function(partyAccount) {

        $scope.errorMap = new Map();
        if (partyAccount == null || partyAccount == undefined || partyAccount == '') {

            $scope.errorMap.put("invoice.partyAccount", $rootScope.nls["ERR40009"]);
            $scope.invoiceCreditNote.billingCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.invoiceCreditNote.roe = 1;
            return false;
        } else {
            $scope.invoiceCreditNote.billingCurrency = partyAccount.currencyMaster;
            if ($rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != null && $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != undefined &&
                $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != '') {
                $scope.invoiceCreditNote.roe = $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id].csell;
                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id == $scope.invoiceCreditNote.billingCurrency.id) {
                    $scope.invoiceCreditNote.roe = 1;
                }
            } else {
                $scope.invoiceCreditNote.roe = 1;
            }
        }


    }


    $scope.saveCrnCost = function() {

        console.log("Save Method is called.");
        $scope.invoiceCreditNote.documentTypeMaster = $scope.documentTypeMaster;
        $scope.invoiceCreditNote.documentTypeCode = $scope.documentTypeMaster.documentTypeCode;
        if (!$scope.disableSubmitBtn) {
            $scope.disableSubmitBtn = true;


            if ($scope.validateCrnCost(0)) {
                if (!$scope.tableInvoiceDetailState()) {

                    $rootScope.clientMessage = $rootScope.nls["ERR40012"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
                    $rootScope.clientMessage = $rootScope.nls["ERR40012"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                /* if($scope.invoiceCreditNote.netLocalAmount>0)
					{
					$rootScope.clientMessage = $rootScope.nls["ERR40021"];
					$scope.Tabs='charge';
					 $scope.enableSubmitBtnFn();
					return false;
					}*/

                if (!$scope.creditNoteTableCheck.isAttachmentCheck()) {
                    $rootScope.clientMessage = $rootScope.nls['ERR250'];
                    console.log("Attachment is invalid");
                    $scope.selectedTabIndex = i;
                    $scope.Tabs = 'attachments';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                //$scope.associateInvoiceAttach();
                $scope.tmpObj = cloneService.clone($scope.invoiceCreditNote);
                $scope.tmpObj.invoiceCreditNoteDate = $rootScope.sendApiStartDateTime($scope.tmpObj.invoiceCreditNoteDate);
                $scope.tmpObj.dueDate = $rootScope.sendApiStartDateTime($scope.tmpObj.dueDate);

                // for shipment invoice masterUid is null
                if ($scope.shipmentCreditNote) {
                    $scope.newObj = {};
                    $scope.newObj.id = $scope.shipmentServiceDetail.id;
                    $scope.tmpObj.shipmentServiceDetail = $scope.newObj;
                    if ($scope.shipmentServiceDetail.consolUid != null &&
                        $scope.shipmentServiceDetail.consolUid != undefined &&
                        $scope.shipmentServiceDetail.consolUid != '') {
                        $scope.tmpObj.masterUid = $scope.shipmentServiceDetail.consolUid;
                    }
                    // $scope.tmpObj.masterUid=null;
                }
                // for consol invoice shipmentServiceDetail is null
                else {
                    $scope.newObj = null;
                    $scope.tmpObj.shipmentServiceDetail = $scope.newObj;
                }

                for (var i = 0; i < $scope.tmpObj.invoiceCreditNoteDetailList.length; i++) {
                    $scope.newObj = {};
                    if ($scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail != "" && $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail != undefined &&
                        $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail != null) {
                        $scope.newObj.id = $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail.id;
                    } else {
                        $scope.newObj = null;
                    }

                    $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail = $scope.newObj;
                    $scope.tmpObj.invoiceCreditNoteDetailList[i].isTaxable = false;
                }

                $scope.progressBar();
                $scope.contained_progressbar.start();
                InvoiceAdd.save($scope.tmpObj).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                invoiceService
                                    .set({});
                                Notification.success($rootScope.nls["ERR40025"] + data.responseObject.invoiceCreditNoteNo);
                                $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                                //$state.go('layout.creditNoteCost');
                                $scope.enableSubmitBtnFn();

                            } else {
                                console
                                    .log("Invoice added Failed " +
                                        data.responseDescription);
                                $scope.enableSubmitBtnFn();
                            }
                            $scope.contained_progressbar
                                .complete();
                        },
                        function(error) {
                            console
                                .log("Invoice added Failed : " +
                                    error);
                            $scope.enableSubmitBtnFn();
                        });

            } else {
                $scope.enableSubmitBtnFn();
                console.log("Validation Failed");
            }
        }
    }




    $scope.focusAndMessageShower = function(resObj, nextField) {

        $scope.errorMap = new Map();
        console.log("resObj :: ", resObj);
        if (resObj.error == false) {
            if (nextField != undefined) {
                $rootScope.navigateToNextField(nextField);
            }
            return false;
        } else {
            $scope.errorMap.put(resObj.errElement, resObj.errMessage);
            return true;
        }
    }
    $scope.validateCrnCost = function(validateCode) {
        if ($scope.focusAndMessageShower(invoiceCreditNoteValidationService.validate(validateCode, $scope.invoiceCreditNote), undefined) == false) {
            return true;
        } else {
            return false;
        }
    }




    $scope.calculateChargeAmount = function(param) {


        if (isNaN(param.localAmount) == false)
            $scope.invoiceCreditNote.localAmount = param.localAmount;
        else {
            $scope.invoiceCreditNote.localAmount = 0.0;
        }

        if (isNaN(param.currencyAmount) == false)
            $scope.invoiceCreditNote.currencyAmount = param.currencyAmount;
        else {
            $scope.invoiceCreditNote.currencyAmount = 0.0;
        }

        if (isNaN(param.amountReceived) == false)
            $scope.invoiceCreditNote.amountReceived = param.amountReceived;
        else {
            $scope.invoiceCreditNote.amountReceived = 0.0;
        }


        if (isNaN(param.outstandingAmount) == false)
            $scope.invoiceCreditNote.outstandingAmount = param.outstandingAmount;
        else {
            $scope.invoiceCreditNote.outstandingAmount = 0.0;
        }

        if (isNaN(param.totalDebitLocalAmount) == false)
            $scope.invoiceCreditNote.totalDebitLocalAmount = param.totalDebitLocalAmount;
        else {
            $scope.invoiceCreditNote.totalDebitLocalAmount = 0.0;
        }

        if (isNaN(param.totalDebitCurrencyAmount) == false)
            $scope.invoiceCreditNote.totalDebitCurrencyAmount = param.totalDebitCurrencyAmount;
        else {
            $scope.invoiceCreditNote.totalDebitCurrencyAmount = 0.0;
        }

        if (isNaN(param.totalCreditLocalAmount) == false)
            $scope.invoiceCreditNote.totalCreditLocalAmount = param.totalCreditLocalAmount;
        else {
            $scope.invoiceCreditNote.totalCreditLocalAmount = 0.0;
        }

        if (isNaN(param.totalCreditCurrencyAmount) == false)
            $scope.invoiceCreditNote.totalCreditCurrencyAmount = param.totalCreditCurrencyAmount;
        else {
            $scope.invoiceCreditNote.totalCreditCurrencyAmount = 0.0;
        }

        if (isNaN(param.netLocalAmount) == false)
            $scope.invoiceCreditNote.netLocalAmount = param.netLocalAmount;
        else {
            $scope.invoiceCreditNote.netLocalAmount = 0.0;
        }

        if (isNaN(param.netCurrencyAmount) == false)
            $scope.invoiceCreditNote.netCurrencyAmount = param.netCurrencyAmount;
        else {
            $scope.invoiceCreditNote.netCurrencyAmount = 0.0;
        }



        $scope.invoiceCreditNote.invoiceCreditNoteTaxList = [];
        $scope.invoiceCreditNote.localAmount = $scope.invoiceCreditNote.netLocalAmount;
        $scope.invoiceCreditNote.currencyAmount = $scope.invoiceCreditNote.netCurrencyAmount;
        $scope.invoiceCreditNote.outstandingAmount = $scope.invoiceCreditNote.currencyAmount - $scope.invoiceCreditNote.amountReceived;

        $scope.invoiceCreditNote.localAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.localAmount);
        $scope.invoiceCreditNote.currencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.currencyAmount);
        $scope.invoiceCreditNote.outstandingAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.outstandingAmount);
        $scope.invoiceCreditNote.amountReceived = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.amountReceived);
        $scope.tempParam = param;

    }



    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }



    $scope.getConsolShipmentServices = function() {

        $scope.shipmentServiceList = [];
        if ($scope.consol != null && $scope.consol != undefined && $scope.consol != '') {
            if ($scope.consol.shipmentLinkList != null &&
                $scope.consol.shipmentLinkList != undefined && $scope.consol.shipmentLinkList != '' &&
                $scope.consol.shipmentLinkList.length > 0
            ) {
                for (var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
                    var tmpObj = $scope.consol.shipmentLinkList[i].service;
                    tmpObj.subJobSequence = $scope.consol.shipmentLinkList[i].subJobSequence;
                    $scope.shipmentServiceList.push(tmpObj);
                }
            }
        }
        console.log("Consol Shipment Service list : ", $scope.shipmentServiceList);
    }



    $scope.$on('addCreditNoteCostEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.invoiceCreditNote;
        $rootScope.category = "Credit Note Cost";
        $rootScope.unfinishedFormTitle = "Credit Note Cost (New)";
        if ($scope.invoiceCreditNote != undefined && $scope.invoiceCreditNote != null &&
            $scope.invoiceCreditNote.daybookMaster != undefined && $scope.invoiceCreditNote.daybookMaster != null &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != undefined &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != null) {
            $rootScope.subTitle = $scope.invoiceCreditNote.daybookMaster.daybookName;
        } else {
            $rootScope.subTitle = "Unknown Daybook"
        }
    })

    $scope.$on('editCreditNoteCostEvent', function(events, args) {
        console.log("Scope Credit Note Cost data :: ", $scope.invoiceCreditNote);
        $rootScope.unfinishedData = $scope.invoiceCreditNote;
        $rootScope.category = "Credit Note Cost";
        $rootScope.unfinishedFormTitle = "Credit Note Cost Edit # " + $scope.invoiceCreditNote.invoiceCreditNoteNo;
        if ($scope.invoiceCreditNote != undefined && $scope.invoiceCreditNote != null &&
            $scope.invoiceCreditNote.daybookMaster != undefined && $scope.invoiceCreditNote.daybookMaster != null &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != undefined &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != null) {
            $rootScope.subTitle = $scope.invoiceCreditNote.daybookMaster.daybookName;
        } else {
            $rootScope.subTitle = "Unknown Daybook"
        }
    })
    $scope.$on('addCreditNoteCostEventReload', function(e, confirmation) {

        console.log("addInvoiceEventReload ", $scope.invoiceCreditNote);
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.invoiceCreditNote);
        localStorage.isCreditNoteCostReloaded = "YES";

        e.preventDefault();
    });
    $scope.$on('editCreditNoteCostEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.invoiceCreditNote);
        localStorage.isCreditNoteCostReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isCreditNoteCostReloaded;

    if ($stateParams.action == "CREATE") {

        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteCostReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.invoiceCreditNote = $rootScope.selectedUnfilledFormData;
                if ($scope.invoiceCreditNote.party != null && $scope.invoiceCreditNote.party != undefined && $scope.invoiceCreditNote.party.id != null) {
                    PartyMasterView.get({
                            id: $scope.invoiceCreditNote.party.id
                        },
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                $scope.invoiceCreditNote.party = data.responseObject;
                                $scope.selectedParty('addr1')

                            }
                        },
                        function(error) {
                            console.log("Party view Failed : ", error)
                        });
                }

            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteCostReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);
                if (($scope.invoiceCreditNote == undefined || $scope.invoiceCreditNote == null) ||
                    ($scope.invoiceCreditNote.shipmentUid == null ||
                        $scope.invoiceCreditNote.shipmentUid == undefined ||
                        $scope.invoiceCreditNote.shipmentUid == "") &&
                    ($scope.invoiceCreditNote.masterUid == null ||
                        $scope.invoiceCreditNote.masterUid == undefined ||
                        $scope.invoiceCreditNote.masterUid == ""))

                {
                    $scope.getShipmentOrConsolData();
                }

            } else {
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.creditNoteCost"
            },
            {
                label: "Credit Note Cost",
                state: "layout.creditNoteCost"
            }, {
                label: "Add Credit Note Cost",
                state: null
            }
        ];
    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteCostReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.invoiceCreditNote = $rootScope.selectedUnfilledFormData;
            }

        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded ="NO";
                localStorage.isCreditNoteCostReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.creditNoteCost"
            },
            {
                label: "Credit Note Cost",
                state: "layout.creditNoteCost"
            }, {
                label: "Edit Credit Note Cost",
                state: null
            }
        ];
    }

    $scope.nextFocus = function(id) {
        console.log("called." + id);
        $rootScope.navigateToNextField(id);
    }

}]);