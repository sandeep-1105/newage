/**
 * Created by hmspl on 12/4/16.
 */
app.controller('invoice_edit_ctrl',['$rootScope', '$http', '$timeout', '$scope', '$location', '$modal', 'ngDialog', 'QuotationAttachment',
    'NavigationService', 'invoiceService', 'DaybookList', 'PartiesList', 'PartyMasterService',
    'CurrencyMasterSearchKeyword', '$stateParams', '$state', 'PartyAccountListById', 'CurrencySearchExclude', 'InvoiceAdd',
    'ValidateUtil', 'ngProgressFactory', 'cloneService', 'DaybookListByDocumentType', 'downloadFactory', 'appConstant', 
    'CommonValidationService', 'PartyMasterView', 'ConsolGet', 'ServiceGetByUid', 'ShipmentGet', 'ConsolGetByUid', 
    'addressJoiner', 'Notification', 'roleConstant', 'invoiceCreditNoteValidationService', 'AutoCompleteService', 
    'documentTypeMasterFactory', 
	function($rootScope, $http, $timeout, $scope, $location, $modal, ngDialog, QuotationAttachment,
    NavigationService, invoiceService, DaybookList, PartiesList, PartyMasterService,
    CurrencyMasterSearchKeyword, $stateParams, $state, PartyAccountListById, CurrencySearchExclude, InvoiceAdd,
    ValidateUtil, ngProgressFactory, cloneService, DaybookListByDocumentType, downloadFactory, appConstant, 
    CommonValidationService, PartyMasterView, ConsolGet, ServiceGetByUid, ShipmentGet, ConsolGetByUid, 
    addressJoiner, Notification, roleConstant, invoiceCreditNoteValidationService, AutoCompleteService, 
    documentTypeMasterFactory) {

    $scope.$invoiceCreditNoteValidationService = invoiceCreditNoteValidationService;
    $scope.$AutoCompleteService = AutoCompleteService;
    $scope.$roleConstant = roleConstant;
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 10;
    $scope.limitArrActive = [10, 15, 20];
    $scope.pageActive = 0;
    $scope.limitActive = 10;
    $scope.totalRecordActive = 10;
    $scope.searchDto = {};
    $scope.activeSearchDto = {};
    $rootScope.clientMessage = null;
    $scope.disableSubmitBtn = false;
    $scope.documentType = "INV";

    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            console.log("Enable btn ,", new Date());
            $scope.disableSubmitBtn = false;
        }, 300);
    }
    $scope.invoiceTableCheck = {};

    $scope.init = function() {
        $scope.invoiceCreditNote = {};
        $scope.invoiceCreditNote.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
        $scope.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
        $scope.firstFocus = true;
        $scope.shipmentInvoice = false;

        $scope.invoiceCreditNote.invoiceCreditNoteStatus = 'Awaiting_Payment';
        $scope.invoiceCreditNote.locationMaster = $rootScope.userProfile.selectedUserLocation;

        $scope.progressBar();
        $rootScope.setNavigate3($scope.invoiceCreditNote.id != null ? "Edit Invoice" : "Add Invoice");
        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";

        $scope.getShipmentOrConsolData();
        $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList = [{}];
        console.log("init finished");
    }

    $scope.clickOnTabView = function(tab) {
        if (tab == 'charge') {
            $scope.Tabs = tab;
        }
        if (tab == 'attachments') {
            $scope.Tabs = tab;
        }
        if (tab == 'notes' && $rootScope.roleAccess(roleConstant.FINANCE_INVOICE_NOTES_CREATE)) {
            $scope.Tabs = tab;
        }
    }

    $scope.getDocumentTypeByCode = function() {
        documentTypeMasterFactory.findByCode.query({
            documentTypeCode: "INV"
        }).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.documentTypeMaster = data.responseObject;
            } else {
                console.log("Exception while getting DocumentType by code ", data.responseDescription);
            }
        }, function(error) {
            console.log("Error ", error);
        });
    };
    $scope.getDocumentTypeByCode();

    $scope.getShipmentOrConsolData = function() {
        //invoice from shipment
        if ($stateParams.shipmentServiceId != null) {
            ServiceGetByUid.get({
                serviceUid: $stateParams.shipmentServiceId
            }, function(serviceData) {
                if (serviceData.responseCode == 'ERR0') {
                    $scope.shipmentServiceDetail = serviceData.responseObject;
                    $scope.shipmentServiceList = [];
                    $scope.shipmentServiceList.push($scope.shipmentServiceDetail);

                    $scope.invoiceCreditNote.shipmentUid = $scope.shipmentServiceDetail.shipmentUid;

                    if ($scope.shipmentServiceDetail.consolUid != null &&
                        $scope.shipmentServiceDetail.consolUid != undefined &&
                        $scope.shipmentServiceDetail.consolUid != '') {
                        ConsolGetByUid.get({
                            consolUid: $scope.shipmentServiceDetail.consolUid
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                $scope.consol = data.responseObject;
                            }
                        }, function(error) {
                            console.log("Consol get Failed : " + error)
                        });
                        $scope.invoiceCreditNote.masterUid = $scope.shipmentServiceDetail.consolUid;
                    }

                    $scope.shipmentInvoice = true;

                    if ($scope.shipmentServiceDetail != null &&
                        $scope.shipmentServiceDetail != undefined &&
                        $scope.shipmentServiceDetail != '') {
                        if ($scope.shipmentServiceDetail.serviceMaster != null &&
                            $scope.shipmentServiceDetail.serviceMaster != undefined &&
                            $scope.shipmentServiceDetail.serviceMaster != '') {
                            $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());
                            if ($scope.shipmentServiceDetail.serviceMaster.importExport == 'Export') {
                                if ($scope.shipmentServiceDetail.etd != null &&
                                    new Date($scope.shipmentServiceDetail.etd) < new Date()) {
                                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.shipmentServiceDetail.etd);
                                }

                            } else if ($scope.shipmentServiceDetail.serviceMaster.importExport == 'Import') {
                                if ($scope.shipmentServiceDetail.eta != null &&
                                    new Date($scope.shipmentServiceDetail.eta) < new Date()) {
                                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.shipmentServiceDetail.eta);
                                }

                            }
                        }
                    }

                    var newdate = $rootScope.convertToDate($scope.invoiceCreditNote.invoiceCreditNoteDate);
                    $scope.invoiceCreditNote.dueDate = $rootScope.dateToString(newdate.add('days', $rootScope.appMasterData['invoice.duedate']));

                    $scope.changeType();
                }
            }, function(error) {
                console.log("shipmentService get Failed : " + error)
            });

        }

        // invoice from consol
        else {
            ConsolGet.get({
                id: $stateParams.consolId
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.consol = data.responseObject;
                    $scope.consol = $scope.consol;
                    $scope.invoiceCreditNote.masterUid = $scope.consol.consolUid;
                    $scope.getConsolShipmentServices();
                    if ($scope.consol != null &&
                        $scope.consol != undefined &&
                        $scope.consol != '') {
                        if ($scope.consol.serviceMaster != null &&
                            $scope.consol.serviceMaster != undefined &&
                            $scope.consol.serviceMaster != '') {
                            $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());
                            if ($scope.consol.serviceMaster.importExport == 'Export') {
                                if ($scope.consol.etd != null &&
                                    new Date($scope.consol.etd) < new Date()) {
                                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.consol.etd);
                                }
                            } else if ($scope.consol.serviceMaster.importExport == 'Import') {
                                if ($scope.consol.eta != null &&
                                    new Date($scope.consol.eta) < new Date()) {
                                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString($scope.consol.eta);
                                }
                            }
                        }
                    }

                    var newdate = $rootScope.convertToDate($scope.invoiceCreditNote.invoiceCreditNoteDate);
                    $scope.invoiceCreditNote.dueDate = $rootScope.dateToString(newdate.add('days', $rootScope.appMasterData['invoice.duedate']));
                    $scope.changeType();
                }
            }, function(error) {
                console.log("Consol get Failed : " + error)
            });
        }
    }
    $scope.changeType = function(id) {
        $scope.invoiceCreditNote.partyAddress = {};
        if ($scope.invoiceCreditNote.customerAgent == null || $scope.invoiceCreditNote.customerAgent == undefined ||
            $scope.invoiceCreditNote.customerAgent == '') {
            return;
        }
        //from shipment
        if ($stateParams.shipmentId != null && $stateParams.shipmentId != undefined &&
            ($stateParams.consolId == null || $stateParams.consolId == undefined)) {

            if ($scope.shipmentServiceDetail.serviceMaster.importExport === "Export") {
                if ($scope.invoiceCreditNote.customerAgent == 'Agent') {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.documentList[0].agent;
                    $scope.selectedParty('addr1');
                } else {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.party;
                    $scope.selectedParty('addr1');
                }
            } else if ($scope.shipmentServiceDetail.serviceMaster.importExport === "Import") {
                if ($scope.invoiceCreditNote.customerAgent == 'Agent') {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.documentList[0].agent;
                    $scope.selectedParty('addr1');
                } else {
                    $scope.invoiceCreditNote.party = $scope.shipmentServiceDetail.documentList[0].consignee;
                    $scope.selectedParty('addr1');
                }
            }


        }
        //from consol
        else {

            if ($scope.consol != undefined && $scope.consol != null && $scope.consol.serviceMaster.importExport === "Export") {

                if ($scope.invoiceCreditNote.customerAgent == 'Customer') {
                    if ($scope.consol.shipmentLinkList.length == 1) {
                        $scope.invoiceCreditNote.party = $scope.consol.shipmentLinkList[0].service.party;
                        $scope.selectedParty('addr1');
                    }
                } else {
                    $scope.invoiceCreditNote.party = $scope.consol.agent;
                    $scope.selectedParty('addr1');
                }

            } else if ($scope.consol != undefined && $scope.consol != null && $scope.consol.serviceMaster.importExport === "Import") {

                if ($scope.invoiceCreditNote.customerAgent == 'Customer') {
                    if ($scope.consol.shipmentLinkList.length == 1) {
                        $scope.invoiceCreditNote.party = $scope.consol.shipmentLinkList[0].service.documentList[0].consignee;
                        $scope.selectedParty('addr1');
                    }
                } else {
                    $scope.invoiceCreditNote.party = $scope.consol.agent;
                    $scope.selectedParty('addr1');
                }

            }



        }
        if ($scope.invoiceCreditNote.party != null && $scope.invoiceCreditNote.party != undefined)
            $scope.invoiceCreditNote.subledger = $scope.invoiceCreditNote.party.partyCode;
        $scope.getPartyAddress($scope.invoiceCreditNote.party);
        $rootScope.navigateToNextField(id);
    }

    $scope.progressBar = function() {
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('invoice-panel'));
        $scope.contained_progressbar.setAbsolute();
    }

    /*BUTTON GROUP SCOPE VALUE*/
    $scope.btnGrp = {};
    $scope.btnGrp.TypeArr = ["import", "export", "both"];

    /*PAGE CHANGE FUNCTION*/
    $scope.add = function() {
        invoiceService.set({});
        $state.go("layout.addInvoice");
    };
    $scope.edit = function() {
        invoiceService.set($scope.invoiceCreditNote);
        $state.go("layout.addInvoice");
    };


    $scope.before = function() {
        $scope.afterbefore = "BEFORE";
        $scope.tmpsearch(-1);

    }
    $scope.after = function(index) {
        $scope.afterbefore = "AFTER";
        $scope.tmpsearch(1)
    }
    $scope.attachConfig = {
        "isEdit": true,
        "isDelete": true,
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                "width": "w300px"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file",
                "width": "w200px"
            },
            {
                "name": "Actions",
                "model": "action",
                "type": "action",
                "width": "w100px"
            }
        ]

    }


    $scope.manipulateAttach = function(param) {

        console.log("manipulateAttach param", param);

        if (param.index == null) {
            $scope.attachConfig.data.push(param.data);
        } else {
            $scope.attachConfig.data.splice(param.index, 1, param.data);
        }

    }

    $scope.removeAttach = function(param) {
        $scope.attachConfig.data.splice(param.index, 1);
    }

    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/invoicecreditnote/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }



    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print"];


    $scope.backStock = function() {
        NavigationService.set(null);
        NavigationService.setLocation(null);
        NavigationService.setSubData1(null);
        $location.path('/air/stock_management_list');
    }




    $scope.cancelInvoice = function() {
        if ($scope.invoiceForm.$dirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(
                function(value) {
                    if (value == 1 && $scope.invoiceCreditNote.id == null) {
                        $scope.saveInvoice();
                    } else if (value == 1 && $scope.invoiceCreditNote.id != null) {
                        //$scope.updateInvoice();
                    } else if (value == 2) {
                        $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                    } else {
                        console.log("Nothing selected..Cancel")
                    }
                });

        } else {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }
    };


    $scope.selectedDaybook = function(id) {
        if ($scope.validateInvoice(1))
            $rootScope.navigateToNextField(id);


    };
    $scope.daybookNameRender = function(item) {
        return {
            label: item.daybookName,
            item: item
        }

    }

    //party list 
    /* Party Select Picker */
    $scope.showPartyList = false;
    $scope.shipmentPartylistConfig = {
        search: true,
        showCode: true,
        ajax: true,
        createButton: true,
        columns: [{
                "title": "partyName",
                seperator: false
            },
            {
                "title": "partyCode",
                seperator: true
            }
        ]
    }


    $scope.showParty = function() {
        //console.log("Showing List of Party...");
        $scope.selectedItem = -1;
        $scope.panelTitle = $rootScope.appMasterData['Party_Label'];
        $scope.ajaxPartyEvent(null);
    }

    $scope.clearPartyAddress = function(object) {
        if (object == "" || object == undefined || object == null || object.id == null) {
            $scope.invoiceCreditNote.partyAddress = {};
        }
    }

    $scope.selectedParty = function(id) {

        $scope.errorMap = new Map();
        if (!$scope.validateInvoice(4)) {
            return;
        }
        $scope.invoiceCreditNote.subledger = $scope.invoiceCreditNote.party.partyCode;
        $scope.getPartyAddress($scope.invoiceCreditNote.party);


        $scope.invoiceCreditNote.partyAccount = null;
        $scope.invoiceCreditNote.billingCurrency = null;
        $scope.invoiceCreditNote.roe = null;


        var object = null;
        $scope.searchDto = {};
        $scope.searchDto.keyword = "";
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        PartyAccountListById.fetch({
            "partyId": $scope.invoiceCreditNote.party.id
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partyAccountList = data.responseObject.searchResult;

                    if ($scope.partyAccountList != null && $scope.partyAccountList.length > 0) {
                        for (var i = 0; i < $scope.partyAccountList.length; i++) {
                            if ($scope.partyAccountList[i].accountMaster.accountName == 'Debitor' &&
                                $scope.partyAccountList[i].currencyMaster.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                                $scope.invoiceCreditNote.partyAccount = $scope.partyAccountList[i];
                                break;
                            }
                        }
                        if ($scope.invoiceCreditNote.partyAccount == null || $scope.invoiceCreditNote.partyAccount == undefined ||
                            $scope.invoiceCreditNote.partyAccount == '') {
                            $scope.invoiceCreditNote.partyAccount = $scope.partyAccountList[0];
                        }
                    }
                    //Billing customer for the party account
                    if ($scope.invoiceCreditNote.partyAccount != null &&
                        $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode != null &&
                        $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode.id != null) {
                        $scope.invoiceCreditNote.party = $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode;
                        $scope.getPartyAddress($scope.invoiceCreditNote.party);
                        $scope.invoiceCreditNote.subledger = $scope.invoiceCreditNote.partyAccount.billingSubLedgerCode.partyCode;
                    }
                    $scope.partyAccountCurrency($scope.invoiceCreditNote.partyAccount);


                }
            },
            function(errResponse) {
                console.error('Error while fetching Party account');
            }
        );



        $rootScope.navigateToNextField(id);




    };

    $scope.cancelParty = function() {
        $scope.showPartyList = false;
    }


    $scope.getPartyAddress = function(obj) {
        if (obj != null && obj.id != null) {
            for (var i = 0; i < obj.partyAddressList.length; i++) {
                if (obj.partyAddressList[i].addressType == 'Primary') {
                    if ($scope.invoiceCreditNote.partyAddress == undefined || $scope.invoiceCreditNote.partyAddress == null || $scope.invoiceCreditNote.partyAddress == '') {
                        $scope.invoiceCreditNote.partyAddress = {};
                    }
                    $scope.invoiceCreditNote.partyAddress = addressJoiner.joinAddressLineByLine($scope.invoiceCreditNote.partyAddress, obj, obj.partyAddressList[i]);
                    break;
                }
            }
        }

    }


    $scope.showPartyAddressList = false;
    $scope.partyAddressListConfig = {
        search: false,
        address: true
    }

    $scope.showPartyAddress = function() {
        console.log("Showing Party address list...");
        $scope.panelTitle = "Party Address";
        $scope.showPartyAddressList = true;
        $scope.selectedItem = -1;
        $scope.partyAddressList = [];
        for (var i = 0; i < $scope.invoiceCreditNote.party.partyAddressList.length; i++) {
            $scope.partyAddressList.push($scope.invoiceCreditNote.party.partyAddressList[i]);
        }
    };

    $scope.selectedPartyAddress = function(obj) {
        $scope.invoiceCreditNote.partyAddress = addressJoiner.joinAddressLineByLine($scope.invoiceCreditNote.partyAddress, $scope.invoiceCreditNote.party, obj);
        $scope.cancelPartyAddress();
    };

    $scope.cancelPartyAddress = function() {
        $scope.showPartyAddressList = false;
    }

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }

    }

    $scope.nextFocus = function(id) {
        console.log("called." + id);
        $rootScope.navigateToNextField(id);
    }


    $scope.routePartyEvent = function(fromObjModel, isNew, partyMaster) {

        $rootScope.nav_src_bkref_key = new Date().toISOString();
        var param = {
            nav_src_bkref_key: $rootScope.nav_src_bkref_key,
            forObj: fromObjModel,
            category: "Invoice"
        };
        if (!isNew) {
            if (partyMaster != undefined && partyMaster != null && partyMaster.id != undefined && partyMaster.id != null) {
                param.partyId = partyMaster.id;
                $state.go("layout.editParty", param);
            } else {
                console.log("Party not selected...");
            }
        } else {
            $state.go("layout.addParty", param);
        }
    }


    // invoice header currency select picker
    $scope.listCurrencyConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
                "title": "currencyName",
                seperator: false
            },
            {
                "title": "currencyCode",
                seperator: true
            }

        ]
    }

    $scope.showCurrency = function(selectedCurrencyRateMasterIndex, currency) {
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.selectedCurrencyRateMasterIndex = selectedCurrencyRateMasterIndex;
        /*$scope.ajaxCurrencyEvent(null)*/
    }

    $scope.currencyRender = function(item) {
        return {
            label: item.currencyCode,
            item: item
        }
    }

    $scope.accountCodeRender = function(item) {
        return {
            label: item.accountCode,
            item: item
        }
    }



    $scope.cancelCurrency = function() {
        $scope.showCurrencyList = false;
    }

    $scope.selectedCurrency = function(id) {


        $scope.validateInvoice(10);

        if ($rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != null && $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != undefined &&
            $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != '') {
            $scope.invoiceCreditNote.roe = $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id].csell;
        } else {
            $scope.invoiceCreditNote.roe = null;
        }

        if ($scope.invoiceCreditNote.billingCurrency.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
            $scope.invoiceCreditNote.roe = 1;
        }

        if ($scope.tempParam != null && $scope.tempParam != undefined && $scope.tempParam != '')
            $scope.calculateChargeAmount($scope.tempParam);
        $rootScope.navigateToNextField(id);
        $scope.updateChargeListData();
    }

    $scope.currencyNameRender = function(item) {
        return {
            label: item.currencyName,
            item: item
        }
    }
    $scope.selectedAccountMaster = function(id) {


        if ($scope.validateInvoice(9))
            $rootScope.navigateToNextField(id);
        $scope.partyAccountCurrency($scope.invoiceCreditNote.partyAccount);


    }


    $scope.partyAccountCurrency = function(partyAccount) {

        $scope.errorMap = new Map();
        if (partyAccount == null || partyAccount == undefined || partyAccount == '') {
            $scope.errorMap.put("invoice.partyAccount", $rootScope.nls["ERR40009"]);
            $scope.invoiceCreditNote.billingCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.invoiceCreditNote.roe = 1;
            return false;
        } else {
            $scope.invoiceCreditNote.billingCurrency = partyAccount.currencyMaster;
            if ($rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != null && $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != undefined &&
                $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id] != '') {
                $scope.invoiceCreditNote.roe = $rootScope.baseCurrenyRate[$scope.invoiceCreditNote.billingCurrency.id].csell;
                if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id == $scope.invoiceCreditNote.billingCurrency.id) {
                    $scope.invoiceCreditNote.roe = 1;
                }
            } else {
                $scope.invoiceCreditNote.roe = 1;
            }
        }


    }


    $scope.saveInvoice = function() {

        console.log("Save Method is called.");
        $scope.invoiceCreditNote.documentTypeMaster = $scope.documentTypeMaster;
        $scope.invoiceCreditNote.documentTypeCode = $scope.documentTypeMaster.documentTypeCode;

        if (!$scope.disableSubmitBtn) {
            $scope.disableSubmitBtn = true;


            if ($scope.validateInvoice(0)) {

                if (!$scope.tableInvoiceDetailState()) {

                    $rootScope.clientMessage = $rootScope.nls["ERR40011"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
                    $rootScope.clientMessage = $rootScope.nls["ERR40011"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                if ($scope.invoiceCreditNote.netLocalAmount < 0) {
                    $rootScope.clientMessage = $rootScope.nls["ERR40020"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                if (!$scope.invoiceTableCheck.isAttachmentCheck()) {
                    $rootScope.clientMessage = $rootScope.nls['ERR250'];
                    console.log("Attachment is invalid");
                    $scope.selectedTabIndex = i;
                    $scope.Tabs = 'attachments';
                    $scope.enableSubmitBtnFn();
                    return false;
                }


                $scope.tmpObj = cloneService.clone($scope.invoiceCreditNote);
                $scope.tmpObj.invoiceCreditNoteDate = $rootScope.sendApiStartDateTime($scope.tmpObj.invoiceCreditNoteDate);
                $scope.tmpObj.dueDate = $rootScope.sendApiStartDateTime($scope.tmpObj.dueDate);


                // for shipment invoice masterUid is null
                if ($scope.shipmentInvoice) {
                    $scope.newObj = {};
                    $scope.newObj.id = $scope.shipmentServiceDetail.id;
                    $scope.tmpObj.shipmentServiceDetail = $scope.newObj;
                    if ($scope.shipmentServiceDetail.consolUid != null &&
                        $scope.shipmentServiceDetail.consolUid != undefined &&
                        $scope.shipmentServiceDetail.consolUid != '') {
                        $scope.tmpObj.masterUid = $scope.shipmentServiceDetail.consolUid;
                    }

                }
                // for consol invoice shipmentServiceDetail is null
                else {
                    $scope.newObj = null;
                    $scope.tmpObj.shipmentServiceDetail = $scope.newObj;
                }


                for (var i = 0; i < $scope.tmpObj.invoiceCreditNoteDetailList.length; i++) {
                    $scope.newObj = {};
                    if ($scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail != "" &&
                        $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail != undefined &&
                        $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail != null) {
                        $scope.newObj.id = $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail.id;
                    } else {
                        $scope.newObj = null;
                    }


                    $scope.tmpObj.invoiceCreditNoteDetailList[i].shipmentServiceDetail = $scope.newObj;


                }

                $scope.progressBar();
                $scope.contained_progressbar.start();

                console.log("$scope.invoiceCreditNote", $scope.invoiceCreditNote);
                InvoiceAdd.save($scope.tmpObj).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                invoiceService
                                    .set({});
                                Notification.success($rootScope.nls["ERR40024"] + data.responseObject.invoiceCreditNoteNo);
                                $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                                //$state.go('layout.invoice');
                                $scope.enableSubmitBtnFn();

                            } else {
                                console.log("Invoice added Failed ", data.responseDescription)
                                $scope.enableSubmitBtnFn();
                            }
                            $scope.contained_progressbar.complete();
                        },
                        function(error) {
                            console
                                .log("Invoice added Failed : " +
                                    error);
                            $scope.enableSubmitBtnFn();
                        });

            } else {
                $scope.enableSubmitBtnFn();
                console.log("Validation Failed");
            }
        }
    }

    $scope.focusAndMessageShower = function(resObj, nextField) {

        $scope.errorMap = new Map();
        console.log("resObj :: ", resObj);
        if (resObj.error == false) {
            if (nextField != undefined) {
                $rootScope.navigateToNextField(nextField);
            }
            return false;
        } else {
            $scope.errorMap.put(resObj.errElement, resObj.errMessage);
            return true;
        }
    }
    $scope.validateInvoice = function(validateCode) {
        if ($scope.focusAndMessageShower(invoiceCreditNoteValidationService.validate(validateCode, $scope.invoiceCreditNote), undefined) == false) {
            return true;
        } else {
            return false;
        }
    }
    $scope.calculateChargeAmount = function(param) {

        if (isNaN(param.localAmount) == false)
            $scope.invoiceCreditNote.localAmount = param.localAmount;
        else {
            $scope.invoiceCreditNote.localAmount = 0.0;
        }
        if (isNaN(param.currencyAmount) == false)
            $scope.invoiceCreditNote.currencyAmount = param.currencyAmount;
        else {
            $scope.invoiceCreditNote.currencyAmount = 0.0;
        }

        if (isNaN(param.amountReceived) == false)
            $scope.invoiceCreditNote.amountReceived = param.amountReceived;
        else {
            $scope.invoiceCreditNote.amountReceived = 0.0;
        }

        if (isNaN(param.outstandingAmount) == false)
            $scope.invoiceCreditNote.outstandingAmount = param.outstandingAmount;
        else {
            $scope.invoiceCreditNote.outstandingAmount = 0.0;
        }

        if (isNaN(param.totalDebitLocalAmount) == false)
            $scope.invoiceCreditNote.totalDebitLocalAmount = param.totalDebitLocalAmount;
        else {
            $scope.invoiceCreditNote.totalDebitLocalAmount = 0.0;
        }

        if (isNaN(param.totalDebitCurrencyAmount) == false)
            $scope.invoiceCreditNote.totalDebitCurrencyAmount = param.totalDebitCurrencyAmount;
        else {
            $scope.invoiceCreditNote.totalDebitCurrencyAmount = 0.0;
        }

        if (isNaN(param.totalCreditLocalAmount) == false)
            $scope.invoiceCreditNote.totalCreditLocalAmount = param.totalCreditLocalAmount;
        else {
            $scope.invoiceCreditNote.totalCreditLocalAmount = 0.0;
        }

        if (isNaN(param.totalCreditCurrencyAmount) == false)
            $scope.invoiceCreditNote.totalCreditCurrencyAmount = param.totalCreditCurrencyAmount;
        else {
            $scope.invoiceCreditNote.totalCreditCurrencyAmount = 0.0;
        }

        if (isNaN(param.netLocalAmount) == false)
            $scope.invoiceCreditNote.netLocalAmount = param.netLocalAmount;
        else {
            $scope.invoiceCreditNote.netLocalAmount = 0.0;
        }

        if (isNaN(param.netCurrencyAmount) == false)
            $scope.invoiceCreditNote.netCurrencyAmount = param.netCurrencyAmount;
        else {
            $scope.invoiceCreditNote.netCurrencyAmount = 0.0;
        }

        $scope.invoiceCreditNote.totalTaxAmount = 0.0;

        $scope.invoiceCreditNote.taxMap = new Map();

        if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == undefined || $scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
            return;
        }

        for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {
            if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != undefined &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != null &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length != 0) {
                for (var j = 0; j < $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length; j++) {
                    if ($scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode) == null) {
                        var tax = {};
                        tax.taxCode = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxCode;
                        tax.taxName = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxName;
                        tax.amount = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount;
                        $scope.invoiceCreditNote.taxMap.put($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode, tax);
                    } else {
                        var tax = $scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode);
                        tax.amount = parseFloat(tax.amount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                    }

                    $scope.invoiceCreditNote.totalTaxAmount = parseFloat($scope.invoiceCreditNote.totalTaxAmount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                }

            }
        }

        $scope.invoiceCreditNote.netLocalAmount = $scope.invoiceCreditNote.netLocalAmount + $scope.invoiceCreditNote.totalTaxAmount;
        $scope.invoiceCreditNote.netCurrencyAmount = $scope.invoiceCreditNote.netCurrencyAmount + ($scope.invoiceCreditNote.totalTaxAmount / $scope.invoiceCreditNote.roe);
        $scope.invoiceCreditNote.localAmount = $scope.invoiceCreditNote.netLocalAmount;
        $scope.invoiceCreditNote.currencyAmount = $scope.invoiceCreditNote.netCurrencyAmount;
        $scope.invoiceCreditNote.outstandingAmount = $scope.invoiceCreditNote.currencyAmount - $scope.invoiceCreditNote.amountReceived;

        $scope.invoiceCreditNote.localAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.localAmount);
        $scope.invoiceCreditNote.currencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.currencyAmount);
        $scope.invoiceCreditNote.outstandingAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.outstandingAmount);
        $scope.invoiceCreditNote.amountReceived = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.amountReceived);
        $scope.tempParam = param;

    }



    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }




    $scope.getConsolShipmentServices = function() {

        $scope.shipmentServiceList = [];
        if ($scope.consol != null && $scope.consol != undefined && $scope.consol != '') {
            if ($scope.consol.shipmentLinkList != null &&
                $scope.consol.shipmentLinkList != undefined && $scope.consol.shipmentLinkList != '' &&
                $scope.consol.shipmentLinkList.length > 0
            ) {
                for (var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
                    var tmpObj = $scope.consol.shipmentLinkList[i].service;
                    tmpObj.subJobSequence = $scope.consol.shipmentLinkList[i].subJobSequence;
                    $scope.shipmentServiceList.push(tmpObj);
                }
            }
        }
        console.log("Consol Shipment Service list : ", $scope.shipmentServiceList);
    }


    $scope.$on('addInvoiceEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.invoiceCreditNote;
        $rootScope.category = "Invoice";
        $rootScope.unfinishedFormTitle = "Invoice (New)";
        if ($scope.invoiceCreditNote != undefined && $scope.invoiceCreditNote != null &&
            $scope.invoiceCreditNote.daybookMaster != undefined && $scope.invoiceCreditNote.daybookMaster != null &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != undefined &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != null) {
            $rootScope.subTitle = $scope.invoiceCreditNote.daybookMaster.daybookName;
        } else {
            $rootScope.subTitle = "Unknown Daybook"
        }
    })

    $scope.$on('editInvoiceEvent', function(events, args) {
        console.log("Scope Invoice data :: ", $scope.invoiceCreditNote);
        $rootScope.unfinishedData = $scope.invoiceCreditNote;
        $rootScope.category = "Invoice";
        $rootScope.unfinishedFormTitle = "Invoice Edit # " + $scope.invoiceCreditNote.invoiceCreditNoteNo;
        if ($scope.invoiceCreditNote != undefined && $scope.invoiceCreditNote != null &&
            $scope.invoiceCreditNote.daybookMaster != undefined && $scope.invoiceCreditNote.daybookMaster != null &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != undefined &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != null) {
            $rootScope.subTitle = $scope.invoiceCreditNote.daybookMaster.daybookName;
        } else {
            $rootScope.subTitle = "Unknown Daybook"
        }
    })
    $scope.$on('addInvoiceEventReload', function(e, confirmation) {

        console.log("addInvoiceEventReload ", $scope.invoiceCreditNote);
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.invoiceCreditNote);
        localStorage.isInvoiceReloaded = "YES";

        e.preventDefault();
    });
    $scope.$on('editInvoiceEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.invoiceCreditNote);
        localStorage.isInvoiceReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isInvoiceReloaded;

    if ($stateParams.action == "CREATE") {

        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isInvoiceReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.invoiceCreditNote = $rootScope.selectedUnfilledFormData;

                if ($scope.invoiceCreditNote.party != null && $scope.invoiceCreditNote.party != undefined && $scope.invoiceCreditNote.party.id != null) {
                    PartyMasterView.get({
                            id: $scope.invoiceCreditNote.party.id
                        },
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                $scope.invoiceCreditNote.party = data.responseObject;
                                $scope.selectedParty('addr1')

                            }
                        },
                        function(error) {
                            console.log("Party view Failed : ", error)
                        });
                }

            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isInvoiceReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

                if (($scope.invoiceCreditNote == undefined || $scope.invoiceCreditNote == null) ||
                    ($scope.invoiceCreditNote.shipmentUid == null ||
                        $scope.invoiceCreditNote.shipmentUid == undefined ||
                        $scope.invoiceCreditNote.shipmentUid == "") &&
                    ($scope.invoiceCreditNote.masterUid == null ||
                        $scope.invoiceCreditNote.masterUid == undefined ||
                        $scope.invoiceCreditNote.masterUid == ""))

                {
                    $scope.getShipmentOrConsolData();

                }


            } else {
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.invoice"
            },
            {
                label: "Invoice",
                state: "layout.invoice"
            }, {
                label: "Add Invoice",
                state: null
            }
        ];

    }

}]);