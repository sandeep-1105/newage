app.service('invoiceCreditNoteValidationService',['$rootScope', '$timeout','$state','RecentHistorySaveService','roleConstant','Notification','$stateParams','ValidateUtil',
                                     function($rootScope, $timeout,$state, RecentHistorySaveService,roleConstant,Notification,$stateParams,ValidateUtil) {
	
	this.validate=function(validateCode,invoiceCreditNote)
     {
		if(validateCode == 0 || validateCode == 1) {

			if(invoiceCreditNote.daybookMaster == undefined ||
					invoiceCreditNote.daybookMaster == null ||
					invoiceCreditNote.daybookMaster.id == undefined ||
					invoiceCreditNote.daybookMaster.id == null) {

				console.log("daybookMaster", $rootScope.nls["ERR38000"]);
				return this.validationResponse(true, "invoice.daybookMaster", $rootScope.nls["ERR38000"], undefined, undefined, undefined);

			} else {

				if(ValidateUtil.isStatusBlocked(invoiceCreditNote.daybookMaster.status)) {
					console.log("invoice.daybookMaster", $rootScope.nls["ERR38001"]);
					invoiceCreditNote.daybookMaster = null;
					return this.validationResponse(true, "invoice.daybookMaster", $rootScope.nls["ERR38001"], undefined, undefined, undefined);
				}

				if(ValidateUtil.isStatusHidden(invoiceCreditNote.daybookMaster.status)) {
					console.log("invoice.daybookMaster", $rootScope.nls["ERR38002"]);
					invoiceCreditNote.daybookMaster = null;
					return this.validationResponse(true, "invoice.daybookMaster", $rootScope.nls["ERR38002"], undefined, undefined, undefined);
				}

			}
		}
		
		
		if(validateCode == 0 || validateCode == 2) {

			if(invoiceCreditNote.customerAgent == undefined ||
					invoiceCreditNote.customerAgent == null ) {
				console.log("invoice.customerAgent", $rootScope.nls["ERR38003"]);
				return this.validationResponse(true, "invoice.customerAgent", $rootScope.nls["ERR38003"], undefined, undefined, undefined);

			} 
		}
		
		if(validateCode == 0 || validateCode == 3) {

			if(invoiceCreditNote.invoiceCreditNoteDate == undefined ||
					invoiceCreditNote.invoiceCreditNoteDate == null ) {

				console.log("invoice.invoiceCreditNoteDate",  $rootScope.nls["ERR38015"]);
				return this.validationResponse(true, "invoice.invoiceCreditNoteDate", $rootScope.nls["ERR38015"], undefined, undefined, undefined);

			} 
		}
		
		
		if (validateCode == 0 || validateCode == 4) {

			if (invoiceCreditNote.party == undefined
					|| invoiceCreditNote.party == null
					|| invoiceCreditNote.party == ""
					|| invoiceCreditNote.party.id == null ) {
				console.log($rootScope.nls["ERR38012"]);
				invoiceCreditNote.partyAccount=null;
				invoiceCreditNote.billingCurrency=null;
				invoiceCreditNote.roe=null;
				invoiceCreditNote.subledger=null;
				return this.validationResponse(true, "invoice.party", $rootScope.nls["ERR38012"], undefined, undefined, undefined);

			}else if(invoiceCreditNote.party.status!=null){
				if(ValidateUtil.isStatusBlocked(invoiceCreditNote.party.status))
                  {
					console.log($rootScope.nls["ERR38013"]);
					errorMap.put("invoice.party",$rootScope.nls["ERR38013"]);
					invoiceCreditNote.party = null;
					invoiceCreditNote.partyAddress={};
					invoiceCreditNote.partyAccount=null;
					invoiceCreditNote.billingCurrency=null;
					invoiceCreditNote.roe=null;
					invoiceCreditNote.subledger=null;
					return this.validationResponse(true, "invoice.party", $rootScope.nls["ERR38013"], undefined, undefined, undefined);

				}

				if(ValidateUtil.isStatusHidden(invoiceCreditNote.party.status))
				{
					console.log($rootScope.nls["ERR38014"]);
					errorMap.put("invoice.party",$rootScope.nls["ERR38014"]);
					invoiceCreditNote.party = null;
					invoiceCreditNote.partyAddress={};
					invoiceCreditNote.partyAccount=null;
					invoiceCreditNote.billingCurrency=null;
					invoiceCreditNote.roe=null;
					invoiceCreditNote.subledger=null;
					return this.validationResponse(true, "invoice.party", $rootScope.nls["ERR38014"], undefined, undefined, undefined);

				}
			}
		}

	 if (validateCode == 0 || validateCode == 5) {
 		if(invoiceCreditNote.partyAddress.addressLine1  == null
 				|| invoiceCreditNote.partyAddress.addressLine1  == ""
 				|| invoiceCreditNote.partyAddress.addressLine1 == undefined) {
 			console.log($rootScope.nls["ERR38005"]);
 			return this.validationResponse(true, "invoiceCreditNote.partyAddress.addressLine1", $rootScope.nls["ERR38005"], undefined, undefined, undefined);
 		}
 		else {
 			
				if (invoiceCreditNote.partyAddress.addressLine1.length>100) {
					console.log($rootScope.nls["ERR38006"]);
					return this.validationResponse(true, "invoiceCreditNote.partyAddress.addressLine1", $rootScope.nls["ERR38006"], undefined, undefined, undefined);
 			}
 	    }
 	}

 	if (validateCode == 0 || validateCode == 6) {
 		if(invoiceCreditNote.partyAddress.addressLine2  == null
 				|| invoiceCreditNote.partyAddress.addressLine2  ==""
 				|| invoiceCreditNote.partyAddress.addressLine2 ==undefined) {
 			/*console.log($rootScope.nls["ERR38007"]);
 			errorMap.put("invoiceCreditNote.partyAddress.addressLine2",$rootScope.nls["ERR38007"]);
 			return false;*/
 		} else {
 			
 			if (invoiceCreditNote.partyAddress.addressLine2.length>100) {
 				console.log($rootScope.nls["ERR38008"]);
 				return this.validationResponse(true, "invoiceCreditNote.partyAddress.addressLine2", $rootScope.nls["ERR38008"], undefined, undefined, undefined);
 			}
 		}
 	}

 	if (validateCode == 0 || validateCode == 7) {

 		if(invoiceCreditNote.partyAddress.addressLine3!=null) {
				
				if (invoiceCreditNote.partyAddress.addressLine3.length>100) {
					console.log($rootScope.nls["ERR38009"]);
					return this.validationResponse(true, "invoiceCreditNote.partyAddress.addressLine3", $rootScope.nls["ERR38009"], undefined, undefined, undefined);
				}
 		}
 	}

 	if (validateCode == 0 || validateCode == 8) {
 		if(invoiceCreditNote.partyAddress.addressLine4  == null
 				|| invoiceCreditNote.partyAddress.addressLine4  ==""
 				|| invoiceCreditNote.partyAddress.addressLine4 ==undefined) {
 		}
 		else {
				
				if (invoiceCreditNote.partyAddress.addressLine4.length>100) {
					console.log($rootScope.nls["ERR38011"]);
					return this.validationResponse(true, "invoiceCreditNote.partyAddress.addressLine4", $rootScope.nls["ERR38011"], undefined, undefined, undefined);
				}
 		}
 	}
 	
 	
 	if (validateCode == 0 || validateCode == 9) {

			if (invoiceCreditNote.partyAccount == undefined
					|| invoiceCreditNote.partyAccount == null
					|| invoiceCreditNote.partyAccount == ""
					|| invoiceCreditNote.partyAccount.id == null ) {
				console.log($rootScope.nls["ERR38022"]);
				return this.validationResponse(true, "invoice.partyAccount", $rootScope.nls["ERR38022"], undefined, undefined, undefined);

			}
		}
 	if (validateCode == 0 || validateCode == 10) {

			if (invoiceCreditNote.billingCurrency == undefined
					|| invoiceCreditNote.billingCurrency == null
					|| invoiceCreditNote.billingCurrency == ""
					|| invoiceCreditNote.billingCurrency.id == null ) {
				console.log($rootScope.nls["ERR38018"]);
				return this.validationResponse(true, "invoice.billingCurrency", $rootScope.nls["ERR38018"], undefined, undefined, undefined);

			}else if(invoiceCreditNote.billingCurrency.status!=null){
				if(ValidateUtil.isStatusBlocked(invoiceCreditNote.billingCurrency.status))
                  {
					console.log($rootScope.nls["ERR38019"]);
					invoiceCreditNote.billingCurrency = null;
					return this.validationResponse(true, "invoice.billingCurrency", $rootScope.nls["ERR38019"], undefined, undefined, undefined);
				}

				if(ValidateUtil.isStatusHidden(invoiceCreditNote.billingCurrency.status))
				{
					console.log($rootScope.nls["ERR38020"]);
					invoiceCreditNote.billingCurrency = null;
					return this.validationResponse(true, "invoice.billingCurrency", $rootScope.nls["ERR38020"], undefined, undefined, undefined);

				}
			}
		}
 	
 	
 	if (validateCode == 0 || validateCode == 11) {
 		
 		if(invoiceCreditNote.roe  == null
 				|| invoiceCreditNote.roe  ==""
 				|| invoiceCreditNote.roe ==undefined) {
 			console.log($rootScope.nls["ERR38010"]);
 			return this.validationResponse(true, "invoiceCreditNote.roe", $rootScope.nls["ERR38010"], undefined, undefined, undefined);
 		}
 		else {
 			
 			if(invoiceCreditNote.billingCurrency !=null && $rootScope.userProfile.selectedUserLocation.currencyMaster.id==invoiceCreditNote.billingCurrency.id)
 				{
 			if (invoiceCreditNote.roe>1) {
					console.log($rootScope.nls["ERR40017"]);
					return this.validationResponse(true, "invoiceCreditNote.roe", $rootScope.nls["ERR40017"], undefined, undefined, undefined);
				}
 			if (invoiceCreditNote.roe<1) {
					console.log($rootScope.nls["ERR38011"]);
					return this.validationResponse(true, "invoiceCreditNote.roe", $rootScope.nls["ERR38011"], undefined, undefined, undefined);
				}
 				}
 			if(invoiceCreditNote.billingCurrency !=null && $rootScope.userProfile.selectedUserLocation.currencyMaster.id!=invoiceCreditNote.billingCurrency.id)
 				{
 				if (invoiceCreditNote.roe==1) {
						console.log($rootScope.nls["ERR40018"]);
						return this.validationResponse(true, "invoiceCreditNote.roe", $rootScope.nls["ERR40018"], undefined, undefined, undefined);
					}
 				if (invoiceCreditNote.roe<=0) {
						console.log($rootScope.nls["ERR38025"]);
						return this.validationResponse(true, "invoiceCreditNote.roe", $rootScope.nls["ERR38025"], undefined, undefined, undefined);
					}
 				}
 			
 			
 		}
 	}
 	
 	return this.validationSuccesResponse();
  }

	
	this.validationSuccesResponse = function() {
	    return {error : false};
	}
	
	this.validationResponse = function(err, elem, message, idx, tabName, accordian) {
		return {error : err, errElement : elem, errMessage : message, serviceIdx : idx, tab : tabName, accordian: accordian};
	}

}]);