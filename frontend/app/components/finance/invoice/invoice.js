/**
 * Created by hmspl on 12/4/16.
 */
app.controller('invoiceCtrl',['$rootScope', '$http', '$scope', '$location', '$modal', 'ShipmentGet', 'ShipmentSearch', '$timeout',
    'ShipmentAsyncSearch', 'ngDialog', 'ShipmentUpdate', 'NavigationService', '$stateParams', '$state',
    'ShipmentStatusChange', 'cloneService', 'InvoiceSearch', 'invoiceService', 'InvoiceView', 'downloadFactory', 
    'downloadMultipleFactory', 'RecentHistorySaveService', '$filter', 'Notification', 'roleConstant',
	function($rootScope, $http, $scope, $location, $modal, ShipmentGet, ShipmentSearch, $timeout,
    ShipmentAsyncSearch, ngDialog, ShipmentUpdate, NavigationService, $stateParams, $state,
    ShipmentStatusChange, cloneService, InvoiceSearch, invoiceService, InvoiceView, downloadFactory, 
    downloadMultipleFactory, RecentHistorySaveService, $filter, Notification, roleConstant) {

    $scope.$roleConstant = roleConstant;
    var tempData = $rootScope.enum['InvoiceCreditNoteStatus'];
    $scope.invoiceCreditNoteStatusData = [];
    for (var i = 0; i < tempData.length; i++) {
        $scope.invoiceCreditNoteStatusData.push($filter('InvoiceCreditNoteStatusFilter')(tempData[i]));

    }

    //Invoice Reports Code Start Here...........

    $scope.openInvoiceReport = function(invoiceCreditNote, invoiceId) {
        $scope.reportObject = angular.copy(invoiceCreditNote);
        $scope.reportListMaking(invoiceCreditNote, invoiceId);
    }

    $scope.reportList = [];
    var allRepoVar = {};
    $scope.allRepoVarModel = {};
    $scope.allRepoVarModel.allReportSelected = false;
    $scope.reportLoadingImage = {};
    $scope.reportLoadingImage.status = false;

    $scope.reportListMaking = function(invoiceCreditNote, resourceRefId) {
        $scope.reportObject = angular.copy(invoiceCreditNote);
        $scope.reportList = [];
        $scope.reportDetailData1 = {};
        $scope.reportDetailData1 = {
            resourceRefId: resourceRefId,
            reportName: "INVOICE_WITH_LOGO",
            reportDisplayName: 'Invoice with Logo',
            key: 'invoiceWithLogo'
        };
        $scope.reportList.push($scope.reportDetailData1);

        $scope.reportDetailData2 = {};
        $scope.reportDetailData2 = {
            resourceRefId: resourceRefId,
            reportName: "INVOICE_WITH_OUT_LOGO",
            reportDisplayName: 'Invoice without Logo',
            key: 'invoiceWithoutLogo'
        };
        $scope.reportList.push($scope.reportDetailData2);

    }

    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print", "eMail"];

    $scope.reportModal = function(resourceRefId, reportRefName, key) {
        if ($scope.reportList != undefined && $scope.reportList != null) {
            $scope.allRepoVarModel.allReportSelected = false;
            $scope.isGenerateAll = false;
            for (var i = 0; i < $scope.reportList.length; i++) {
                $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
            }
        }
        if (key == 'invoiceWithLogo' && !$rootScope.roleAccess(roleConstant.FINANCE_INVOICE_REPORT_INVOICE_WITH_LOGO_DOWNLOAD)) {
            return;
        }
        if (key == 'invoiceWithoutLogo' && !$rootScope.roleAccess(roleConstant.FINANCE_INVOICE_REPORT_INVOICE_WITHOUT_LOGO_DOWNLOAD)) {
            return;
        }
        $scope.changeReportDownloadFormat("PDF");
        $scope.dataResourceId = resourceRefId;
        $scope.dataReportName = reportRefName;
        $scope.myReportModal = {};
        $scope.myReportModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/report/report_opt.html',
            show: false
        });
        $scope.myReportModal.$promise.then($scope.myReportModal.show);

    };


    $scope.selectAllReports = function() {
        for (var i = 0; i < $scope.reportList.length; i++) {
            $scope.reportList[i].isSelected = $scope.allRepoVarModel.allReportSelected;
        }
        if (!$scope.allRepoVarModel.allReportSelected) {
            $scope.isGenerateAll = false;
        }
    }

    $scope.chkReportStatus = function(chkFlag, detObj) {
        console.log("Changed Status :: ", chkFlag, "  -----------  ", detObj);
    }

    $scope.changeReportDownloadFormat = function(dType) {
        console.log("Download type Changed :: ", dType);
        $scope.downloadFileType = dType;
        if (dType === 'PDF') {
            $scope.generate.TypeArr = ["Download", "Preview", "Print"];
        } else if (dType === 'eMail') {
            $scope.generate.TypeArr = ["eMail"];
        } else {
            $scope.generate.TypeArr = ["Download"];
        }
    }

    $scope.isSingle = false;
    $scope.isMultiple = false;
    $scope.isGenerateAll = false;

    $scope.generateAll = function() {
        $scope.dataReportName = null;
        var flag = false;
        for (var i = 0; i < $scope.reportList.length; i++) {
            if ($scope.reportList[i].isSelected) {
                flag = true
                break;
            }
        }
        if (!flag) {
            $scope.isGenerateAll = false;
            Notification.warning("Please select atleast one.......)")
        } else {
            $scope.isGenerateAll = true;
            $scope.reportData = {};
            $scope.myReportModal = {};
            $scope.myReportModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/report/report_opt.html',
                show: false
            });
            $rootScope.mainpreloder = false;
            $scope.myReportModal.$promise.then($scope.myReportModal.show);
        }
    }

    $scope.generateAllReports = function() {
        if ($scope.reportList == undefined || $scope.reportList == null || $scope.reportList.length == 0) {} else {
            //Using for mail sending for download reports 
            $scope.emailReportList = [];
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    $scope.emailReport = {};
                    $scope.emailReport.resourceId = $scope.reportList[i].resourceRefId;
                    $scope.emailReport.reportName = $scope.reportList[i].reportName;
                    $scope.emailReport.downloadOption = $scope.downloadOption;
                    $scope.emailReport.downloadFileType = $scope.downloadFileType;
                    $scope.emailReportList.push($scope.emailReport);
                }
            }

        }
        if ($scope.emailReportList != undefined && $scope.emailReportList.length > 0) {
            $scope.isMultiple = true;
            $scope.isSingle = false;
        } else {
            $scope.isSingle = true;
            $scope.isMultiple = false;
        }
        var flag = false;
        if ($scope.isSingle) {
            for (var i = 0; i < $scope.reportList.length; i++) {
                if ($scope.reportList[i].isSelected) {
                    flag = true
                    $scope.downloadGenericCall($scope.reportList[i].resourceRefId, $scope.downloadOption, $scope.reportList[i].reportName);
                    $timeout(function() {
                        $scope.reportLoadingImage.status = false;
                    }, 1000);
                }
                if (flag) {
                    break;
                }
            }
            if (!flag) {
                Notification.warning("Please select atleast one.......)");
                return;
            }
        } else if ($scope.isMultiple) {
            $scope.downloadGenericCallMultiple($scope.emailReportList, $scope.downloadOption, $scope.isMultiple, "PDF", $scope.reportList[0].resourceRefId)
            console.log("Nothig selected");
        }
    }


    $scope.downloadGenericCall = function(id, downOption, repoName) {
        $scope.downloadOption = downOption;
        if ($scope.isGenerateAll) {
            $scope.generateAllReports();
        } else {
            var reportDownloadRequest = {
                resourceId: id,
                downloadOption: downOption, // "Download","Preview","Print"
                downloadFileType: $scope.downloadFileType, //PDF
                reportName: repoName,
                single: true,
            };
            if (downOption === 'eMail') {
                var flag = checkMail();
                if (!flag) {
                    Notification.error($rootScope.nls["ERR275"]);
                    return false;
                } else {
                    $rootScope.mainpreloder = true;
                    downloadFactory.download(reportDownloadRequest);
                    $timeout(function() {
                        $rootScope.mainpreloder = false;
                        $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                    }, 3000);
                }
            } else {
                $rootScope.mainpreloder = true;
                downloadFactory.download(reportDownloadRequest);
            }
        }
    }


    function checkMail() {
        var flag = false;
        if ($scope.reportObject != null && $scope.reportObject != undefined) {
            if ($scope.reportObject.party != null && $scope.reportObject.party != undefined) {
                if ($scope.reportObject.party.partyAddressList != null && $scope.reportObject.party.partyAddressList != undefined && $scope.reportObject.party.partyAddressList.length > 0) {

                    for (var i = 0; i < $scope.reportObject.party.partyAddressList.length; i++) {
                        if ($scope.reportObject.party.partyAddressList[0].addressType == "Primary") {
                            var emailList = $scope.reportObject.party.partyAddressList[0].email;
                            if (emailList == null || emailList == undefined) {
                                flag = false;
                            } else {
                                flag = true;
                                break;
                            }
                        } else {
                            flag = false;
                        }
                    }
                }
            }
        }
        return flag;
    }


    $scope.downloadGenericCallMultiple = function(emailReportList, downOption, multiple, type, id) {
        var reportDownloadRequest = {
            emailReportList: emailReportList,
            downloadOption: downOption, // "Download","Preview","Print"
            downloadFileType: type, //PDF
            isMultiple: multiple,
            resourceId: id,
            screenType: 'Invoice'
        };
        if (downOption === 'eMail') {
            var flag = checkMail();
            if (!flag) {
                Notification.error($rootScope.nls["ERR275"]);
                return false;
            } else {
                $rootScope.mainpreloder = true;
                downloadMultipleFactory.download(reportDownloadRequest);
                $timeout(function() {
                    $rootScope.mainpreloder = false;
                    $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                }, 3000);
            }
        } else {
            $rootScope.mainpreloder = false;
            for (var i = 0; i < reportDownloadRequest.emailReportList.length; i++) {
                var reportFileName = '';
                reportFileName = reportDownloadRequest.emailReportList[i].reportName;
                var reportDownloaddRequest = {
                    resourceId: id,
                    downloadOption: downOption, // "Download","Preview","Print"
                    downloadFileType: $scope.downloadFileType, //PDF
                    reportName: reportFileName,
                    single: true
                };
                $rootScope.mainpreloder = true;
                downloadFactory.download(reportDownloaddRequest);
                if (downOption === 'eMail') {
                    $timeout(function() {
                        $rootScope.mainpreloder = false;
                        $scope.myReportModal.$promise.then($scope.myReportModal.hide);
                    }, 3000);
                }
            }
        }
    }
    //Invoice Reports Code End Here...........


    $scope.searchHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,

        },

        {
            "name": "Daybook Code",
            "search": true,
            "model": "daybookCode",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "300",
            "key": "daybookCode",
        },
        {
            "name": "Daybook Name",
            "model": "daybookName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "150",
            "key": "daybookName"
        },
        {
            "name": "Document No",
            "model": "invoiceCreditNoteNo",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "invoiceCreditNoteNo"

        },
        {
            "name": "Document Date",
            "model": "invoiceCreditNoteDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "invoiceCreditNoteDate"

        },
        {
            "name": "Due Date",
            "model": "dueDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "dueDate"

        },
        {
            "name": "Master Id",
            "search": true,
            "model": "masterUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "300",
            "key": "masterUid",
        },
        {
            "name": "Shipment Id",
            "search": true,
            "model": "shipmentUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "300",
            "key": "shipmentUid",
        },

        {
            "name": "Service Id",
            "search": true,
            "model": "serviceUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "300",
            "key": "serviceId",
        },


        {
            "name": "Customer / Agent",
            "model": "customerAgent",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "data": $rootScope.enum['CustomerAgent'],
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "customerAgent"
        }, {


            "name": "Party Name",
            "model": "partyName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "partyName"
        }, {
            "name": "Party Account #",
            "model": "accountCode",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "partyAccountCode"
        },
        {
            "name": "Subledger",
            "model": "subledger",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "subledger"
        },
        {
            "name": "Currency",
            "model": "currencyCode",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "currencyCode"
        },
        {
            "name": "Amount",
            "model": "currencyAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "amount"
        },
        {
            "name": "Outstanding Amount",
            "model": "outstandingAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "outstandingAmount"
        },
        {
            "name": "Local Amount",
            "model": "localAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "localAmount"
        },

        {
            "name": "Status",
            "model": "invoiceCreditNoteStatus",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "data": $scope.invoiceCreditNoteStatusData,
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "invoiceCreditNoteStatus"
        }




    ]

    $scope.setSearch = function(value) {
        $scope.detailTab = value;
        $scope.searchFlag = value;
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.invoiceSearch($scope.page, $scope.limit);
    }
    $scope.showActionButton = true;
    $scope.selectedRecordIndex = 0;

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;
            $scope.searchDto.documentTypeCode = "INV";
            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            if ($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
                stateParameters.searchStatus = $scope.searchDto.searchStatus = $stateParams.searchStatus;
            }
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchFlag != undefined && $stateParams.searchFlag != null) {
                stateParameters.searchFlag = $scope.searchFlag = $stateParams.searchFlag;
            } else {
                stateParameters.searchFlag = $scope.searchFlag = "active";
            }
        } else {
            return;
        }
        InvoiceSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.invoiceIndex = stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.invoiceId = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.viewInvoice", stateParameters);
        });
    }

    $scope.sortSelection = {
        sortKey: "party.partyName",
        sortOrder: "asc"
    }

    $scope.rowLink = function(data) {
        console.log("data", data);
    }


    $scope.clickOnTabView = function(tab) {
        if (tab == 'charge' && $rootScope.roleAccess(roleConstant.FINANCE_INVOICE_CHARGE_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'attachments' && $rootScope.roleAccess(roleConstant.FINANCE_INVOICE_ATTACHMENT_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'notes' && $rootScope.roleAccess(roleConstant.FINANCE_INVOICE_NOTES_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'reports' && $rootScope.roleAccess(roleConstant.FINANCE_INVOICE_REPORT_VIEW)) {
            $scope.Tabs = tab;
        }
    }

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.FINANCE_INVOICE_VIEW)) {
            console.log("$scope.searchDto --- ", $scope.searchDto);
            $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var localRefStateParam = {
                invoiceId: data.id,
                invoiceIndex: $scope.selectedRecordIndex,
                searchFlag: $scope.searchFlag,
                totalRecord: $scope.totalRecord
            };

            console.log("Invoice  $scope.searchFlag -------------- ", $scope.searchFlag);
            $state.go("layout.viewInvoice", $scope.searchDtoToStateParams(localRefStateParam));
        }
    }

    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.invoiceIndex;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                //Invoice Status - Active, All
                if ($scope.searchDto.searchStatus != undefined && $scope.searchDto.searchStatus != null) {
                    param.searchStatus = $scope.searchDto.searchStatus;
                }

            }

        }
        return param;
    }


    $scope.viewById = function(id) {
        InvoiceView.get({
            id: id
        }, function(data) {
            $rootScope.setNavigate1("Finance");
            $rootScope.setNavigate2("Invoice");
            $scope.populateInvoiceData(data);
        }, function(error) {
            console.log("Invoice view Failed : " + error)
        });
    }



    $scope.populateInvoiceData = function(data) {
        if (data.responseCode == "ERR0") {
            $scope.invoiceCreditNote = data.responseObject;
            $scope.getTaxMap();
            if ($scope.invoiceCreditNote.netLocalAmount == $scope.invoiceCreditNote.adjustmentAmount) {
                $scope.invoiceCreditNote.statusChange = true;
            } else {
                $scope.invoiceCreditNote.statusChange = false;
            }



            // To fill the attachment table..
            $scope.populateAttachment();

            $rootScope.unfinishedFormTitle = "Invoice View # " + $scope.invoiceCreditNote.invoiceCreditNoteNo;
            var rHistoryObj = {
                'title': 'Invoice View #' + $scope.invoiceCreditNote.invoiceCreditNoteNo,
                'subTitle': $rootScope.subTitle,
                'stateName': $state.current.name,
                'stateParam': JSON.stringify($stateParams),
                'stateCategory': 'Invoice',
                'serviceType': 'AIR'
            }

            RecentHistorySaveService.form(rHistoryObj);
        } else {
            console.log("invoice view Failed " + data.responseDescription)
        }

        if (($stateParams.shipmentUid != null && $stateParams.shipmentUid != undefined && $stateParams.shipmentUid != "") ||
            ($stateParams.consolUid != null && $stateParams.consolUid != undefined && $stateParams.consolUid != "") ||
            ($stateParams.fromState == 'layout.jobledger')) {
            $scope.showActionButton = false;
        }

    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.invoiceSearch($scope.page, $scope.limit);
    }

    $scope.changePage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.invoiceSearch($scope.page, $scope.limit);
    }

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.invoiceSearch($scope.page, $scope.limit);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.invoiceSearch($scope.page, $scope.limit);
    }

    $scope.init = function() {
        $scope.detailTab = 'active';
        $rootScope.setNavigate1("Finance");
        $rootScope.setNavigate2("Invoice");
        $scope.setSearch('active');
        $scope.invoiceCreditNote = {};
        invoiceService.set({});
        $scope.searchDto = {};
        $scope.invoiceSearch($scope.page, $scope.limit);

    }

    $scope.back = function() {
        if ($stateParams.fromState == null || $stateParams.fromState == undefined) {
            $scope.detailTab = $stateParams.viewFrom;
            if ($scope.detailTab == 'active') {
                $scope.setSearch('active');
            } else {
                $scope.setSearch('all');
            }
            $scope.invoiceCreditNote = {};
            $state.go('layout.invoice');
        } else {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }
    }


    $scope.invoiceSearch = function(selectedPageNumber, recordPerPage) {
        $scope.invoiceCreditNote = {};
        //$scope.searchDto.status="Awaiting_Payment";
        $scope.searchDto.documentTypeCode = "INV";
        $scope.searchDto.selectedPageNumber = selectedPageNumber;
        $scope.searchDto.recordPerPage = recordPerPage;
        $scope.tmpSearchDto = cloneService.clone($scope.searchDto);

        if ($scope.tmpSearchDto.invoiceCreditNoteDate != null) {
            $scope.tmpSearchDto.invoiceCreditNoteDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.invoiceCreditNoteDate.startDate);
            $scope.tmpSearchDto.invoiceCreditNoteDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.invoiceCreditNoteDate.endDate);
        }



        $scope.invoiceArr = [];
        InvoiceSearch.query($scope.tmpSearchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.currencyAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.currencyAmount);
                tempObj.outstandingAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.outstandingAmount);
                tempObj.localAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.localAmount);
                tempObj.invoiceCreditNoteStatus = $filter("InvoiceCreditNoteStatusFilter")(item.invoiceCreditNoteStatus);
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.invoiceArr = resultArr;

        });
    }

    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }




    //attachment code starts here

    $scope.attachConfig = {
        "isEdit": false,
        "isDelete": false,
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                "width": "w300px"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file",
                "width": "w200px"
            },

        ]

    }


    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/invoicecreditnote/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }


    $scope.populateAttachment = function() {
        console.log("Populating Attachment....");
        $scope.attachConfig.data = [];
        if ($scope.invoiceCreditNote.invoiceCreditNoteAttachmentList != null && $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList.length != 0) {

            for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList.length; i++) {

                var displayObject = new Object();
                displayObject.id = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].id;
                displayObject.refNo = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].refNo;
                displayObject.fileName = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].fileName;
                displayObject.file = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].file;
                displayObject.fileContentType = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].fileContentType;
                displayObject.systemTrack = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].systemTrack;
                displayObject.versionLock = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].versionLock;

                $scope.attachConfig.data.push(displayObject);
            }
        }
    }

    /***
     * Attachment Code Ends Here 
     ***/

    $scope.cancel = function() {
        $scope.showDetail = false;

    }


    $scope.edit = function() {

        var localRefStateParam = {
            invoiceId: $scope.invoiceCreditNote.id
        };
        console.log("State Parameters :: ", localRefStateParam);
        $state.go("layout.editInvoice", localRefStateParam);

    };

    $scope.invoiceStatusCancelled = function() {
        if ($scope.invoiceCreditNote.statusChange == true) {

            ngDialog.openConfirm({
                template: '<p>Do you want to cancel the invoice?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {


                    if ($stateParams.shipmentUid != null && $stateParams.shipmentUid != undefined) {
                        var shipmentUid = $stateParams.shipmentUid;
                        var shipmentServiceId = $stateParams.shipmentServiceId;

                        var param = {
                            shipmentUid: shipmentUid,
                            shipmentServiceId: shipmentServiceId,
                            invoiceId: $scope.invoiceCreditNote.id,
                            fromState: $state.current.name,
                            fromStateParams: JSON.stringify($stateParams)
                        }
                    } else {
                        var consolUid = $stateParams.consolUid;
                        var param = {
                            consolUid: consolUid,
                            invoiceId: $scope.invoiceCreditNote.id,
                            fromState: $state.current.name,
                            fromStateParams: JSON.stringify($stateParams)
                        }
                    }


                    $state.go('layout.addCreditNoteRevenue', param);


                } else {
                    $scope.invoiceCreditNote.statusChange = false;
                }
            })

        }

    }
    $scope.goToShipmentScreen = function(shipmentServiceDetail) {
        var param = "";
        if ($scope.invoiceCreditNote.shipmentUid != null && $scope.invoiceCreditNote.shipmentUid != undefined && $scope.invoiceCreditNote.shipmentUid != '') {
            param = {
                shipmentUid: $scope.invoiceCreditNote.shipmentUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
        }
        if (shipmentServiceDetail != null && shipmentServiceDetail != undefined && shipmentServiceDetail != '') {
            param = {
                shipmentUid: shipmentServiceDetail.shipmentUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
        }
        $state.go('layout.viewCrmShipment', param);
    }
    $scope.goToConsolScreen = function() {
        var param = {
            consolUid: $scope.invoiceCreditNote.masterUid,
            fromState: $state.current.name,
            fromStateParams: JSON.stringify($stateParams)
        }

        $state.go('layout.viewAirConsol', param);
    }


    $scope.getTaxMap = function() {
        $scope.invoiceCreditNote.totalTaxAmount = 0.0;

        $scope.invoiceCreditNote.taxMap = new Map();

        if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == undefined || $scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
            return;
        }

        for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {
            if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != undefined &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != null &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length != 0) {
                for (var j = 0; j < $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length; j++) {
                    if ($scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode) == null) {
                        var tax = {};
                        tax.taxCode = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxCode;
                        tax.taxName = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxName;
                        tax.amount = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount;
                        $scope.invoiceCreditNote.taxMap.put($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode, tax);
                    } else {
                        var tax = $scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode);
                        tax.amount = parseFloat(tax.amount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                    }

                    $scope.invoiceCreditNote.totalTaxAmount = parseFloat($scope.invoiceCreditNote.totalTaxAmount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                }

            }
        }
    }


    $scope.add = function() {
        $state.go("layout.invoice");
    };


    switch ($stateParams.action) {
        case "VIEW":
            console.log("I am In Invoice View Page");
            $rootScope.unfinishedFormTitle = "Invoice View # " + $stateParams.invoiceId;
            $rootScope.unfinishedData = undefined;
            $scope.selectedTabIndex = 0;
            $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
            $scope.totalRecord = parseInt($stateParams.totalRecord);
            $scope.viewById($stateParams.invoiceId);
            $rootScope.breadcrumbArr = [{
                    label: "Finance",
                    state: "layout.invoice"
                },
                {
                    label: "Invoice",
                    state: "layout.invoice"
                }, {
                    label: "View Invoice",
                    state: null
                }
            ];
            break;
        case "SEARCH":
            console.log("I am In Invoice List Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.invoice"
            }, {
                label: "Invoice",
                state: null
            }];
            break;
        default:

    }

}]);