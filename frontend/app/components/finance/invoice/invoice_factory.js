(function() {

	app.factory("InvoiceSearch",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	

	app.factory("InvoiceAdd",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);


	app.factory("InvoiceView",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("GetByInvoiceNo",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/get/invoiceNo/:invoiceNo/:daybookId", {}, {
			get : {
				method : 'GET',
				params : {
					invoiceNo : '',
					daybookId : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	
	
	
	app.factory("InvoiceRemove",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	
	app.factory("InvoiceServiceTaxPercentage",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/getServiceTaxPercentage/:serviceId/:chargeId/:countryId/:companyId", {}, {
			get : {
				method : 'GET',
				params : {
					serviceId : '',
					chargeId : '',
					countryId : '',
					companyId : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("ShipmentServiceInvoiceList",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/getShipmentServiceInvoiceList", {}, {
			fetch : {
				method : 'POST'
				
				
			}
		});
	}]);
	
	app.factory("ChargeListWithInvoiceNo",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/getChargeListWithInvoiceNo", {}, {
			fetch : {
				method : 'POST'
				
				
			}
		});
	}]);
	
	
	app.factory("GetInvoiceCountByMasterUIDorServiceUID",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/get/invoice/count", {}, {
			count : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("ConsolInvoiceList",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/getConsolInvoiceList/:consolUid", {}, {
			get : {
				method : 'GET',
				params : {
					consolUid : ''
					
				},
				isArray : false
			}
		});
	}]);
	
	
	
	
	
	
	app.factory("GLChargeMappingList",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/get/searchglchargemapping/keyword/:serviceId", {}, {
			fetch : {
				method : 'POST',
				params : {
					serviceId : ''
					
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("InvoiceListBasedOnDaybook",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/get/search/keyword/:daybookId", {}, {
			fetch : {
				method : 'POST',
				params : {
					daybookId : ''
					
				},
				isArray : false
			}
		});
	}]);
	

	app.factory("ReasonList",['$resource', function($resource) {
		return $resource("/api/v1/reasonmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ServiceSignOffStatus",['$resource', function($resource) {
		return $resource("/api/v1/shipment/get/service/signoff/status/:serviceId", {}, {
			get : {
				method : 'GET',
				params : {
					serviceId : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("ConsolSignOffStatus",['$resource', function($resource) {
		return $resource("/api/v1/consol/get/signoff/status/:consolId", {}, {
			get : {
				method : 'GET',
				params : {
					consolId : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("RatesFromProvision",['$resource', function($resource) {
		return $resource("/api/v1/invoicecreditnote/get/getratesfromprovision", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
})();
