/**
 * Created by hmspl on 12/4/16.
 */
app.controller('finance_Ctrl',['$rootScope', '$http', '$scope', '$location', '$modal', 'ShipmentGet', 'ShipmentSearch',
    'ShipmentAsyncSearch', 'ngDialog', 'ShipmentUpdate', 'NavigationService', '$stateParams', '$state',
    'ShipmentStatusChange', 'cloneService', 'InvoiceSearch', 'invoiceService', 'InvoiceView', 'downloadFactory', 
    'RecentHistorySaveService', '$filter', 'roleConstant', 
    function($rootScope, $http, $scope, $location, $modal, ShipmentGet, ShipmentSearch,
    ShipmentAsyncSearch, ngDialog, ShipmentUpdate, NavigationService, $stateParams, $state,
    ShipmentStatusChange, cloneService, InvoiceSearch, invoiceService, InvoiceView, downloadFactory, 
    RecentHistorySaveService, $filter, roleConstant) {

    var tempData = $rootScope.enum['InvoiceCreditNoteStatus'];
    $scope.invoiceCreditNoteStatusData = [];
    for (var i = 0; i < tempData.length; i++) {
        $scope.invoiceCreditNoteStatusData.push($filter('InvoiceCreditNoteStatusFilter')(tempData[i]));

    }
    $scope.searchHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false,

        },

        {
            "name": "Daybook",
            "search": true,
            "model": "daybookCode",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "daybookCode",
        },

        {
            "name": "Document No",
            "search": true,
            "model": "invoiceCreditNoteNo",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "invoiceCreditNoteNo",
        },
        {
            "name": "Adjusted Daybook",
            "search": true,
            "model": "adjDaybookCode",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "300",
            "key": "adjDaybookCode",
        },
        //{
        //    "name":"Daybook Name",
        //    "model":"daybookMaster.daybookName",
        //    "search":true,
        //    "wrap_cell":true,
        //    "type":"text",
        //    "sort":true,
        //    "width":"w200px",
        //    "prefWidth":"150",
        //    "key":"daybookName"
        //},
        {
            "name": "Adjusted Invoice #",
            "model": "adjustmentInvoiceNo",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "adjustmentInvoiceNo"

        },
        {
            "name": "Document Date",
            "model": "invoiceCreditNoteDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "invoiceCreditNoteDate"

        },
        /* {
             "name":"Due Date",
             "model":"dueDate",
             "search":true,
             "wrap_cell":true,
             "type":"date-range",
             "sort":true,
             "width":"w150px",
             "prefWidth":"150",
             "key":"dueDate"

         },*/
        {
            "name": "Master ID",
            "search": true,
            "model": "masterUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "masterUid",
        },
        {
            "name": "Shipment ID",
            "search": true,
            "model": "shipmentUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipmentUid",
        },

        {
            "name": "Service ID",
            "search": true,
            "model": "serviceUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "serviceId",
        },


        //{
        //    "name":"Adjusted Invoice #",
        //        "model":"adjustmentInvoiceNo",
        //        "search":true,
        //        "wrap_cell":true,
        //        "type":"text",
        //        "sort":true,
        //        "width":"w200px",
        //        "prefWidth":"150",
        //        "key":"adjustmentInvoiceNo"
        //
        //},
        //
        {
            "name": "Customer / Agent",
            "model": "customerAgent",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "data": $rootScope.enum['CustomerAgent'],
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "customerAgent"
        }, {


            "name": "Party Name",
            "model": "partyName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "partyName"
        }, {
            "name": "Party Account #",
            "model": "accountCode",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "partyAccountCode"
        },
        {
            "name": "Subledger",
            "model": "subledger",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "subledger"
        },
        {
            "name": "Currency",
            "model": "currencyCode",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "currencyCode"
        },
        {
            "name": "Amount",
            "model": "currencyAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "amount"
        },
        {
            "name": "Outstanding Amount",
            "model": "outstandingAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "outstandingAmount"
        },
        {
            "name": "Local Amount",
            "model": "localAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "localAmount"
        },

        {
            "name": "Status",
            "model": "invoiceCreditNoteStatus",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "data": $scope.invoiceCreditNoteStatusData,
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "invoiceCreditNoteStatus"
        }




    ]

    $scope.setSearch = function(value) {
        $scope.detailTab = value;
        $scope.searchFlag = value;
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.creditNoteRevenueSearch($scope.page, $scope.limit);
    }
    $scope.showActionButton = true;
    $scope.selectedRecordIndex = 0;

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;
            $scope.searchDto.documentTypeCode = "CRN";
            $scope.searchDto.creditNoteType = "CRN-REVENUE";
            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            if ($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
                stateParameters.searchStatus = $scope.searchDto.searchStatus = $stateParams.searchStatus;
            }
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchFlag != undefined && $stateParams.searchFlag != null) {
                stateParameters.searchFlag = $scope.searchFlag = $stateParams.searchFlag;
            } else {
                stateParameters.searchFlag = $scope.searchFlag = "active";
            }
        } else {
            return;
        }
        InvoiceSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.creditNoteRevenueIndex = stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.creditNoteRevenueId = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.viewCreditNoteRevenue", stateParameters);
        });
    }

    $scope.sortSelection = {
        sortKey: "party.partyName",
        sortOrder: "asc"
    }

    $scope.rowLink = function(data) {
        console.log("data", data);
    }

    $scope.clickOnTabView = function(tab) {

        if (tab == 'charge' && $rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_CHARGE_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'attachments' && $rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_ATTACHMENT_VIEW)) {
            $scope.Tabs = tab;
        }
        if (tab == 'notes' && $rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_NOTES_VIEW)) {
            $scope.Tabs = tab;
        }
    }

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.FINANCE_CREDIT_NOTE_REVENUE_VIEW)) {
            console.log("$scope.searchDto --- ", $scope.searchDto);
            $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var localRefStateParam = {
                creditNoteRevenueId: data.id,
                creditNoteRevenueIndex: $scope.selectedRecordIndex,
                searchFlag: $scope.searchFlag,
                totalRecord: $scope.totalRecord
            };

            console.log("Invoice  $scope.searchFlag -------------- ", $scope.searchFlag);
            $state.go("layout.viewCreditNoteRevenue", $scope.searchDtoToStateParams(localRefStateParam));
        }
    }

    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.creditNoteRevenueIndex;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                //Invoice Status - Active, All
                if ($scope.searchDto.searchStatus != undefined && $scope.searchDto.searchStatus != null) {
                    param.searchStatus = $scope.searchDto.searchStatus;
                }

            }

        }
        return param;
    }


    $scope.viewById = function(id) {
        InvoiceView.get({
            id: id
        }, function(data) {
            $rootScope.setNavigate1("Finance");
            $rootScope.setNavigate2("Credit Note Revenue");
            $scope.populateInvoiceData(data);
        }, function(error) {
            console.log("Credit Note Revenue view Failed : " + error)
        });
    }



    $scope.populateInvoiceData = function(data) {
        if (data.responseCode == "ERR0") {
            $scope.invoiceCreditNote = data.responseObject;
            $scope.getTaxMap();
            if ($scope.invoiceCreditNote.adjustmentInvoiceNo == null || $scope.invoiceCreditNote.adjustmentInvoiceNo == undefined) {
                $scope.invoiceCreditNote.statusChange = false;
            } else {
                $scope.invoiceCreditNote.statusChange = true;
            }
            // To fill the attachment table..
            $scope.populateAttachment();

            $rootScope.unfinishedFormTitle = "Credit Note Revenue View # " + $scope.invoiceCreditNote.invoiceCreditNoteNo;
            var rHistoryObj = {
                'title': 'Credit Note Revenue View #' + $scope.invoiceCreditNote.invoiceCreditNoteNo,
                'subTitle': $rootScope.subTitle,
                'stateName': $state.current.name,
                'stateParam': JSON.stringify($stateParams),
                'stateCategory': 'Invoice',
                'serviceType': 'AIR'
            }

            RecentHistorySaveService.form(rHistoryObj);
        } else {
            console.log("Credit Note Revenue view Failed " + data.responseDescription)
        }
        if (($stateParams.shipmentUid != null && $stateParams.shipmentUid != undefined && $stateParams.shipmentUid != "") ||
            ($stateParams.consolUid != null && $stateParams.consolUid != undefined && $stateParams.consolUid != "")) {
            $scope.showActionButton = false;
        }
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.creditNoteRevenueSearch($scope.page, $scope.limit);
    }

    $scope.changePage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.creditNoteRevenueSearch($scope.page, $scope.limit);
    }

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.creditNoteRevenueSearch($scope.page, $scope.limit);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.creditNoteRevenueSearch($scope.page, $scope.limit);
    }

    $scope.init = function() {
        $scope.detailTab = 'active';
        $rootScope.setNavigate1("Finance");
        $rootScope.setNavigate2("Credit Note Revenue");
        $scope.setSearch('active');
        $scope.invoiceCreditNote = {};
        invoiceService.set({});
        $scope.searchDto = {};
        $scope.creditNoteRevenueSearch($scope.page, $scope.limit);

    }

    $scope.back = function() {
        if ($stateParams.fromState == null || $stateParams.fromState == undefined) {
            $scope.detailTab = $stateParams.viewFrom;
            if ($scope.detailTab == 'active') {
                $scope.setSearch('active');
            } else {
                $scope.setSearch('all');
            }
            $scope.invoiceCreditNote = {};
            $state.go("layout.creditNoteRevenue");
        } else {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }
    }


    $scope.creditNoteRevenueSearch = function(selectedPageNumber, recordPerPage) {

        $scope.invoiceCreditNote = {};
        //$scope.searchDto.status="Awaiting_Payment";
        $scope.searchDto.documentTypeCode = "CRN";
        $scope.searchDto.creditNoteType = "CRN-REVENUE";
        $scope.searchDto.selectedPageNumber = selectedPageNumber;
        $scope.searchDto.recordPerPage = recordPerPage;
        $scope.tmpSearchDto = cloneService.clone($scope.searchDto);

        if ($scope.tmpSearchDto.invoiceCreditNoteDate != null) {
            $scope.tmpSearchDto.invoiceCreditNoteDate.startDate = $rootScope.sendApiStartDateTime($scope.tmpSearchDto.invoiceCreditNoteDate.startDate);
            $scope.tmpSearchDto.invoiceCreditNoteDate.endDate = $rootScope.sendApiEndDateTime($scope.tmpSearchDto.invoiceCreditNoteDate.endDate);
        }



        $scope.invoiceArr = [];
        InvoiceSearch.query($scope.tmpSearchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.currencyAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.currencyAmount);
                tempObj.outstandingAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.outstandingAmount);
                tempObj.localAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.localAmount);
                tempObj.invoiceCreditNoteStatus = $filter("InvoiceCreditNoteStatusFilter")(item.invoiceCreditNoteStatus);
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.invoiceArr = resultArr;

        });
    }

    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }




    //attachment code starts here

    $scope.attachConfig = {
        "isEdit": false,
        "isDelete": false,
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                "width": "w300px"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file",
                "width": "w200px"
            },

        ]

    }


    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/invoicecreditnote/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }


    $scope.populateAttachment = function() {
        console.log("Populating Attachment....");
        $scope.attachConfig.data = [];
        if ($scope.invoiceCreditNote.invoiceCreditNoteAttachmentList != null && $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList.length != 0) {

            for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList.length; i++) {

                var displayObject = new Object();
                displayObject.id = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].id;
                displayObject.refNo = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].refNo;
                displayObject.fileName = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].fileName;
                displayObject.file = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].file;
                displayObject.fileContentType = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].fileContentType;
                displayObject.systemTrack = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].systemTrack;
                displayObject.versionLock = $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList[i].versionLock;

                $scope.attachConfig.data.push(displayObject);
            }
        }
    }

    /***
     * Attachment Code Ends Here 
     ***/

    $scope.cancel = function() {
        $scope.showDetail = false;

    }


    $scope.goToShipmentScreen = function(shipmentServiceDetail) {
        var param = "";
        if ($scope.invoiceCreditNote.shipmentUid != null && $scope.invoiceCreditNote.shipmentUid != undefined && $scope.invoiceCreditNote.shipmentUid != '') {
            param = {
                shipmentUid: $scope.invoiceCreditNote.shipmentUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
        }
        if (shipmentServiceDetail != null && shipmentServiceDetail != undefined && shipmentServiceDetail != '') {
            param = {
                shipmentUid: shipmentServiceDetail.shipmentUid,
                fromState: $state.current.name,
                fromStateParams: JSON.stringify($stateParams)
            }
        }
        $state.go('layout.viewCrmShipment', param);
    }
    $scope.goToConsolScreen = function() {
        var param = {
            consolUid: $scope.invoiceCreditNote.masterUid,
            fromState: $state.current.name,
            fromStateParams: JSON.stringify($stateParams)
        }

        $state.go('layout.viewAirConsol', param);
    }



    $scope.add = function() {
        $state.go("layout.creditNoteRevenue");
    };

    $scope.getTaxMap = function() {
        $scope.invoiceCreditNote.totalTaxAmount = 0.0;

        $scope.invoiceCreditNote.taxMap = new Map();

        if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == undefined || $scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
            return;
        }

        for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {
            if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != undefined &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != null &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length != 0) {
                for (var j = 0; j < $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length; j++) {
                    if ($scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode) == null) {
                        var tax = {};
                        tax.taxCode = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxCode;
                        tax.taxName = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxName;
                        tax.amount = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount;
                        $scope.invoiceCreditNote.taxMap.put($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode, tax);
                    } else {
                        var tax = $scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode);
                        tax.amount = parseFloat(tax.amount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                    }

                    $scope.invoiceCreditNote.totalTaxAmount = parseFloat($scope.invoiceCreditNote.totalTaxAmount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                }

            }
        }
    }
    switch ($stateParams.action) {
        case "VIEW":
            console.log("I am In Credit Note Revenue View Page");
            $rootScope.unfinishedFormTitle = "Credit Note Revenue View # " + $stateParams.creditNoteRevenueId;
            $rootScope.unfinishedData = undefined;
            $scope.selectedTabIndex = 0;
            $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
            $scope.totalRecord = parseInt($stateParams.totalRecord);
            $scope.viewById($stateParams.creditNoteRevenueId);
            $rootScope.breadcrumbArr = [{
                    label: "Finance",
                    state: "layout.creditNoteRevenue"
                },
                {
                    label: "Credit Note Revenue",
                    state: "layout.creditNoteRevenue"
                }, {
                    label: "View Credit Note Revenue",
                    state: null
                }
            ];
            break;
        case "SEARCH":
            console.log("I am In Credit Note Revenue List Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.creditNoteRevenue"
            }, {
                label: "Credit Note Revenue",
                state: null
            }];
            break;
        default:

    }

}]);