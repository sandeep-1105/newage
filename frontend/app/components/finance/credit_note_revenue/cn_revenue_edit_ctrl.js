/**
 * Created by hmspl on 12/4/16.
 */
app.controller('finance_edit_ctrl',['$rootScope', '$http', '$timeout', '$scope', '$location', '$modal', 'ngDialog', 'QuotationAttachment', 'NavigationService',
    'invoiceService', 'DaybookList', 'PartiesList', 'PartyMasterService', 'CurrencyMasterSearchKeyword', '$stateParams', '$state',
    'PartyAccountListById', 'CurrencySearchExclude', 'InvoiceAdd', 'ValidateUtil', 'ngProgressFactory', 'cloneService', 'DaybookListByDocumentType',
    'InvoiceListBasedOnDaybook', 'GetByInvoiceNo', 'addressJoiner', 'ReasonList', 'downloadFactory', '$q', 'ShipmentGet', 'ConsolGet', 'ServiceGetByUid', 
    'InvoiceView', 'appConstant', 'CommonValidationService', 'Notification', 'roleConstant', 'documentTypeMasterFactory', 'AutoCompleteService',
    function($rootScope, $http, $timeout, $scope, $location, $modal, ngDialog, QuotationAttachment, NavigationService,
    invoiceService, DaybookList, PartiesList, PartyMasterService, CurrencyMasterSearchKeyword, $stateParams, $state,
    PartyAccountListById, CurrencySearchExclude, InvoiceAdd, ValidateUtil, ngProgressFactory, cloneService, DaybookListByDocumentType,
    InvoiceListBasedOnDaybook, GetByInvoiceNo, addressJoiner, ReasonList, downloadFactory, $q, ShipmentGet, ConsolGet, ServiceGetByUid, 
    InvoiceView, appConstant, CommonValidationService, Notification, roleConstant, documentTypeMasterFactory, AutoCompleteService) {
    $scope.$AutoCompleteService = AutoCompleteService;
    $scope.$roleConstant = roleConstant;
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 10;
    $scope.limitArrActive = [10, 15, 20];
    $scope.pageActive = 0;
    $scope.limitActive = 10;
    $scope.totalRecordActive = 10;
    $scope.searchDto = {};
    $scope.activeSearchDto = {};
    $scope.disableSubmitBtn = false;
    $scope.documentType = "CRN-REVENUE";
    $scope.enableSubmitBtnFn = function() {
        $timeout(function() {
            console.log("Enable btn ,", new Date());
            $scope.disableSubmitBtn = false;
        }, 300);
    }
    $scope.creditNoteTableCheck = {};

    $scope.init = function() {
        console.log("Init Started...");
        $rootScope.clientMessage = null;
        $scope.invoiceCreditNote = {};
        $scope.showDocumentActions = true;
        $scope.showDetail = false;
        $scope.detailTab = 'all';
        $rootScope.setNavigate1("Finance");
        $rootScope.setNavigate2("Credit Note Revenue");
        $scope.searchDto = {};
        $scope.searchDto.status = null;
        $scope.searchDto.importExport = "both"
        $scope.hideBasedonStock = false;
        $scope.activeSearchDto = {};

        $scope.progressBar();
        $rootScope.setNavigate3($scope.invoiceCreditNote.id != null ? "Edit Credit Note Revenue" : "Add Credit Note Revenue");
        $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOpts.widgetParent = "body";
        if ($stateParams.invoiceId != null && $stateParams.invoiceId != undefined) {
            $scope.getCrnRevenueData();
        } else {
            $scope.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.firstFocus = true;
            $scope.invoiceCreditNote.dueDate = null;
            $scope.invoiceCreditNote.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());
            $scope.invoiceCreditNote.invoiceCreditNoteStatus = 'Awaiting_Payment';
            if ($stateParams.shipmentUid != null && $stateParams.shipmentUid != undefined) {
                $scope.invoiceCreditNote.shipmentUid = $stateParams.shipmentUid;
            } else {
                $scope.invoiceCreditNote.masterUid = $stateParams.consolUid;
            }
            $scope.tempInvoiceCreditNote = $scope.invoiceCreditNote;
        }
        $scope.invoiceCreditNote.invoiceCreditNoteAttachmentList = [{}];
        console.log("Init Finished......")

    }

    $scope.getDocumentTypeByCode = function(code) {
        documentTypeMasterFactory.findByCode.query({
            documentTypeCode: code
        }).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                if (code == 'INV') {
                    $scope.invDocumentTypeMaster = data.responseObject;
                } else {
                    $scope.crnDocumentTypeMaster = data.responseObject;
                }
            } else {
                console.log("Exception while getting DocumentType by code ", data.responseDescription);
            }
        }, function(error) {
            console.log("Error ", error);
        });
    };

    $scope.getDocumentTypeByCode('INV');
    $scope.getDocumentTypeByCode('CRN');

    $scope.getCrnRevenueData = function() {
        InvoiceView.get({
            id: $stateParams.invoiceId
        }, function(data) {
            if (data.responseCode == 'ERR0') {
                $scope.invoiceCreditNote = data.responseObject;
                $scope.adjInvoiceCreditNote = $scope.invoiceCreditNote;
                $scope.invoiceCreditNote.daybookMaster = {};
                $scope.invoiceCreditNote.outstandingAmount = 0.0;
                if ($scope.adjInvoiceCreditNote != null) {
                    $scope.invoiceCreditNote.adjustmentInvoiceNo = $scope.adjInvoiceCreditNote.invoiceCreditNoteNo;
                }
                $scope.totalAmount = $scope.invoiceCreditNote.netLocalAmount;
                $scope.firstFocus = true;
                $scope.invoiceCreditNote.dueDate = null;
                $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());
                $scope.invoiceCreditNote.invoiceCreditNoteStatus = 'Awaiting_Payment';
                $scope.shipmentServiceList = [];

                $scope.tempInvoiceCreditNote = $scope.invoiceCreditNote;

                if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList != null && $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length > 0) {
                    for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {

                        if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 1) {
                            $scope.shipmentServiceDetail = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].shipmentServiceDetail;
                            $scope.shipmentServiceList.push($scope.shipmentServiceDetail);
                        } else {
                            $scope.shipmentServiceList.push($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].shipmentServiceDetail);
                        }

                    }
                }
                $scope.getTaxMap();
            }
        }, function(error) {
            console.log("Invoice get Failed : " + error)
        });
    }

    $scope.progressBar = function() {
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('crn-revenue-panel'));
        $scope.contained_progressbar.setAbsolute();
    }

    /*BUTTON GROUP SCOPE VALUE*/
    $scope.btnGrp = {};
    $scope.btnGrp.TypeArr = ["import", "export", "both"];

    /*PAGE CHANGE FUNCTION*/
    $scope.add = function() {
        //	consolService.set({});

        $state.go("layout.addCreditNoteRevenue");
    };
    $scope.edit = function() {
        //consolService.set($scope.consol);
        $state.go("layout.editCreditNoteRevenue");
    };


    $scope.before = function() {
        $scope.afterbefore = "BEFORE";
        $scope.tmpsearch(-1);

    }
    $scope.after = function(index) {
        $scope.afterbefore = "AFTER";
        $scope.tmpsearch(1)
    }
    $scope.attachConfig = {
        "isEdit": true,
        "isDelete": true,
        "columnDefs": [

            {
                "name": "Reference No",
                "model": "refNo",
                "type": "text",
                "width": "w300px"
            },
            {
                "name": "File Name",
                "model": "fileName",
                "type": "file",
                "width": "w200px"
            },
            {
                "name": "Actions",
                "model": "action",
                "type": "action",
                "width": "w100px"
            }
        ]

    }


    $scope.manipulateAttach = function(param) {

        console.log("manipulateAttach param", param);
        var displayObject = new Object();
        displayObject.id = param.data.id;
        displayObject.refNo = param.data.refNo;
        displayObject.fileName = param.data.fileName;
        displayObject.file = param.data.file;
        displayObject.fileContentType = param.data.fileContentType;
        displayObject.tmpFile = param.data.tmpFile;
        displayObject.systemTrack = param.data.systemTrack;

        if (param.index == null) {
            $scope.attachConfig.data.push(param.data);
        } else {
            $scope.attachConfig.data.splice(param.index, 1, param.data);
        }

    }

    $scope.removeAttach = function(param) {
        $scope.attachConfig.data.splice(param.index, 1);
    }

    $scope.downloadAttach = function(param) {
        if (param.data.id != null && param.data.file == null) {
            console.log("API CALL")
            downloadFactory.downloadAttachment('/api/v1/invoicecreditnote/files/', param.data.id, param.data.fileName);
        } else {
            console.log("NO API CALL", param);
            saveAs(param.data.tmpFile, param.data.fileName);
        }
    }

    $scope.downloadFileType = "PDF";
    $scope.generate = {};
    $scope.generate.typeArr = ["Download", "Preview", "Print"];


    $scope.backStock = function() {
        NavigationService.set(null);
        NavigationService.setLocation(null);
        NavigationService.setSubData1(null);
        $location.path('/air/stock_management_list');
    }

    $scope.cancelCrnRevenue = function() {
        if ($scope.crnRevenueForm.$dirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(
                function(value) {
                    if (value == 1 && $scope.invoiceCreditNote.id == null) {
                        $scope.saveCrnRevenue();
                    } else if (value == 1 && $scope.invoiceCreditNote.id != null) {
                        $scope.saveCrnRevenue();
                    } else if (value == 2) {

                        $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                    } else {
                        console.log("Nothing selected..Cancel")
                    }
                });

        } else {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }

    };




    $scope.selectedDaybook = function(id) {
        if ($scope.validateCrnRevenue(1))
            $rootScope.navigateToNextField(id);


    };

    $scope.selectedAdjDaybook = function(id) {
        if ($scope.validateCrnRevenue(3))
            $rootScope.navigateToNextField(id);


    };
    $scope.daybookNameRender = function(item) {
        return {
            label: item.daybookName,
            item: item
        }

    }


    // invoice list render 

    $scope.ajaxAdjInvoiceEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        $scope.searchDto.param1 = $scope.invoiceCreditNote.shipmentUid;
        $scope.searchDto.param2 = $scope.invoiceCreditNote.masterUid;
        $scope.searchDto.param3 = $stateParams.shipmentServiceId;
        if ($scope.invoiceCreditNote.adjustMentDaybookMaster == null || $scope.invoiceCreditNote.adjustMentDaybookMaster == undefined &&
            $scope.invoiceCreditNote.adjustMentDaybookMaster.id == null && $scope.invoiceCreditNote.adjustMentDaybookMaster.id == undefined) {
            console.log("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38000"]);
            $scope.errorMap.put("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38000"]);
            return false;
        }
        console.log("daybook id  :", $scope.invoiceCreditNote.adjustMentDaybookMaster.id);

        return InvoiceListBasedOnDaybook.fetch({
            "daybookId": $scope.invoiceCreditNote.adjustMentDaybookMaster.id
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {

                    $scope.adjInvoiceList = data.responseObject.searchResult;
                    console.log("adjDaybookList : ", $scope.adjInvoiceList);
                    return $scope.adjInvoiceList;

                }
            },
            function(errResponse) {
                console.error('Error while fetching adjInvoiceList');
            }
        );
    }

    $scope.selectedAdjInvoice = function(id) {
        if ($scope.adjInvoiceCreditNote != null) {
            $scope.invoiceCreditNote.adjustmentInvoiceNo = $scope.adjInvoiceCreditNote.invoiceCreditNoteNo;
        }
        if ($scope.validateCrnRevenue(4))
            $rootScope.navigateToNextField(id);
    };
    $scope.invoiceNoRender = function(item) {
        return {
            label: item.invoiceCreditNoteNo,
            item: item
        }

    }


    $scope.saveCrnRevenue = function() {
        console.log("Save Method is called.");
        $scope.invoiceCreditNote.documentTypeMaster = $scope.crnDocumentTypeMaster;
        $scope.invoiceCreditNote.documentTypeCode = $scope.crnDocumentTypeMaster.documentTypeCode;
        $scope.invoiceCreditNote.invoiceCreditNoteNo = null;
        if (!$scope.disableSubmitBtn) {
            $scope.disableSubmitBtn = true;
            $scope.invoiceCreditNote.locationMaster = $rootScope.userProfile.selectedUserLocation;


            if ($scope.validateCrnRevenue(0)) {

                if (!$scope.tableInvoiceDetailState()) {

                    $rootScope.clientMessage = $rootScope.nls["ERR40013"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                $scope.totalRevenueAmount = $scope.invoiceCreditNote.netLocalAmount;
                if ($scope.invoiceCreditNote.adjustmentAmount != null && $scope.invoiceCreditNote.adjustmentAmount != undefined &&
                    $scope.invoiceCreditNote.adjustmentAmount != "") {
                    $scope.totalRevenueAmount = $scope.totalRevenueAmount + $scope.invoiceCreditNote.adjustmentAmount;

                }

                if ($scope.totalRevenueAmount > $scope.totalAmount) {
                    $rootScope.clientMessage = $rootScope.nls["ERR40023"];
                    $scope.Tabs = 'charge';
                    $scope.enableSubmitBtnFn();
                    return false;
                }




                $scope.invoiceCreditNote.adjustmentAmount = null;
                if (!$scope.creditNoteTableCheck.isAttachmentCheck()) {
                    $rootScope.clientMessage = $rootScope.nls['ERR250'];
                    console.log("Attachment is invalid");
                    $scope.selectedTabIndex = i;
                    $scope.Tabs = 'attachments';
                    $scope.enableSubmitBtnFn();
                    return false;
                }

                //$scope.associateInvoiceAttach();
                $scope.tmpObj = cloneService.clone($scope.invoiceCreditNote);
                $scope.tmpObj.invoiceCreditNoteDate = $rootScope.sendApiStartDateTime($scope.tmpObj.invoiceCreditNoteDate);


                $scope.progressBar();
                $scope.contained_progressbar.start();
                InvoiceAdd.save($scope.tmpObj).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                invoiceService
                                    .set({});
                                Notification.success($rootScope.nls["ERR40026"] + data.responseObject.invoiceCreditNoteNo);
                                $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                                //$state.go('layout.creditNoteRevenue');
                                $scope.enableSubmitBtnFn();

                            } else {
                                console
                                    .log("Invoice added Failed " +
                                        data.responseDescription);
                                $scope.enableSubmitBtnFn();
                            }
                            $scope.contained_progressbar
                                .complete();
                        },
                        function(error) {
                            console
                                .log("Invoice added Failed : " +
                                    error);
                            $scope.enableSubmitBtnFn();
                        });

            } else {
                $scope.enableSubmitBtnFn();
                console.log("Validation Failed");
            }
        }
    }




    $scope.validateCrnRevenue = function(validateCode) {
        $scope.firstFocus = false;
        $scope.errorMap = new Map();


        if (validateCode == 0 || validateCode == 1) {

            if ($scope.invoiceCreditNote.daybookMaster == undefined ||
                $scope.invoiceCreditNote.daybookMaster == null ||
                $scope.invoiceCreditNote.daybookMaster.id == undefined ||
                $scope.invoiceCreditNote.daybookMaster.id == null) {

                console.log("daybookMaster", $rootScope.nls["ERR38000"]);
                $scope.errorMap.put("invoice.daybookMaster", $rootScope.nls["ERR38000"]);
                return false;

            } else {

                if (ValidateUtil.isStatusBlocked($scope.invoiceCreditNote.daybookMaster.status)) {
                    console.log("invoice.daybookMaster", $rootScope.nls["ERR38001"]);
                    $scope.errorMap.put("invoice.daybookMaster", $rootScope.nls["ERR38001"]);
                    $scope.invoiceCreditNote.daybookMaster = null;
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.invoiceCreditNote.daybookMaster.status)) {
                    console.log("invoice.daybookMaster", $rootScope.nls["ERR38002"]);
                    $scope.errorMap.put("invoice.daybookMaster", $rootScope.nls["ERR38002"]);
                    $scope.invoiceCreditNote.daybookMaster = null;
                    return false;
                }

            }
        }


        if (validateCode == 0 || validateCode == 2) {

            if ($scope.invoiceCreditNote.customerAgent == undefined ||
                $scope.invoiceCreditNote.customerAgent == null) {

                console.log("invoice.customerAgent", $rootScope.nls["ERR38003"]);
                $scope.errorMap.put("invoice.customerAgent", $rootScope.nls["ERR38003"]);
                return false;

            }
        }

        if (validateCode == 0 || validateCode == 3) {

            if ($scope.invoiceCreditNote.adjustMentDaybookMaster == undefined ||
                $scope.invoiceCreditNote.adjustMentDaybookMaster == null ||
                $scope.invoiceCreditNote.adjustMentDaybookMaster.id == undefined ||
                $scope.invoiceCreditNote.adjustMentDaybookMaster.id == null) {

                console.log("adjustMentDaybookMaster", $rootScope.nls["ERR38000"]);
                $scope.errorMap.put("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38000"]);
                return false;

            } else {

                if (ValidateUtil.isStatusBlocked($scope.invoiceCreditNote.adjustMentDaybookMaster.status)) {
                    console.log("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38001"]);
                    $scope.errorMap.put("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38001"]);
                    $scope.invoiceCreditNote.adjustMentDaybookMaster = null;
                    return false;
                }

                if (ValidateUtil.isStatusHidden($scope.invoiceCreditNote.adjustMentDaybookMaster.status)) {
                    console.log("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38002"]);
                    $scope.errorMap.put("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38002"]);
                    $scope.invoiceCreditNote.adjustMentDaybookMaster = null;
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 4) {

            if ($scope.invoiceCreditNote.adjustmentInvoiceNo == undefined ||
                $scope.invoiceCreditNote.adjustmentInvoiceNo == null) {

                console.log("invoice.adjustmentInvoiceNo", $rootScope.nls["ERR40014"]);
                $scope.errorMap.put("invoice.adjustmentInvoiceNo", $rootScope.nls["ERR40014"]);
                return false;

            }
        }

        return true;
    }




    $scope.calculateChargeAmount = function(param) {

        if (isNaN(param.localAmount) == false)
            $scope.invoiceCreditNote.localAmount = param.localAmount;
        else {
            $scope.invoiceCreditNote.localAmount = 0.0;
        }

        if (isNaN(param.currencyAmount) == false)
            $scope.invoiceCreditNote.currencyAmount = param.currencyAmount;
        else {
            $scope.invoiceCreditNote.currencyAmount = 0.0;
        }

        if (isNaN(param.amountReceived) == false)
            $scope.invoiceCreditNote.amountReceived = param.amountReceived;
        else {
            $scope.invoiceCreditNote.amountReceived = 0.0;
        }


        /*if(isNaN(param.outstandingAmount)==false)
        $scope.invoiceCreditNote.outstandingAmount=param.outstandingAmount;
        else
        	{
        	$scope.invoiceCreditNote.outstandingAmount=0.0;
        	}*/

        $scope.invoiceCreditNote.outstandingAmount = 0.0;

        if (isNaN(param.totalDebitLocalAmount) == false)
            $scope.invoiceCreditNote.totalDebitLocalAmount = param.totalDebitLocalAmount;
        else {
            $scope.invoiceCreditNote.totalDebitLocalAmount = 0.0;
        }

        if (isNaN(param.totalDebitCurrencyAmount) == false)
            $scope.invoiceCreditNote.totalDebitCurrencyAmount = param.totalDebitCurrencyAmount;
        else {
            $scope.invoiceCreditNote.totalDebitCurrencyAmount = 0.0;
        }

        if (isNaN(param.totalCreditLocalAmount) == false)
            $scope.invoiceCreditNote.totalCreditLocalAmount = param.totalCreditLocalAmount;
        else {
            $scope.invoiceCreditNote.totalCreditLocalAmount = 0.0;
        }

        if (isNaN(param.totalCreditCurrencyAmount) == false)
            $scope.invoiceCreditNote.totalCreditCurrencyAmount = param.totalCreditCurrencyAmount;
        else {
            $scope.invoiceCreditNote.totalCreditCurrencyAmount = 0.0;
        }

        if (isNaN(param.netLocalAmount) == false)
            $scope.invoiceCreditNote.netLocalAmount = param.netLocalAmount;
        else {
            $scope.invoiceCreditNote.netLocalAmount = 0.0;
        }

        if (isNaN(param.netCurrencyAmount) == false)
            $scope.invoiceCreditNote.netCurrencyAmount = param.netCurrencyAmount;
        else {
            $scope.invoiceCreditNote.netCurrencyAmount = 0.0;
        }


        $scope.invoiceCreditNote.totalTaxAmount = 0.0;

        $scope.invoiceCreditNote.taxMap = new Map();

        if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == undefined || $scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
            return;
        }

        for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {
            if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != undefined &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != null &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length != 0) {
                for (var j = 0; j < $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length; j++) {
                    $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount)

                    if ($scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode) == null) {
                        var tax = {};
                        tax.taxCode = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxCode;
                        tax.taxName = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxName;
                        tax.amount = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount;
                        $scope.invoiceCreditNote.taxMap.put($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode, tax);
                    } else {
                        var tax = $scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode);
                        tax.amount = parseFloat(tax.amount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                    }

                    $scope.invoiceCreditNote.totalTaxAmount = parseFloat($scope.invoiceCreditNote.totalTaxAmount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                }

            }
        }

        $scope.invoiceCreditNote.netLocalAmount = $scope.invoiceCreditNote.netLocalAmount + $scope.invoiceCreditNote.totalTaxAmount;
        $scope.invoiceCreditNote.netCurrencyAmount = $scope.invoiceCreditNote.netCurrencyAmount + ($scope.invoiceCreditNote.totalTaxAmount / $scope.invoiceCreditNote.roe);
        $scope.invoiceCreditNote.localAmount = $scope.invoiceCreditNote.netLocalAmount;
        $scope.invoiceCreditNote.currencyAmount = $scope.invoiceCreditNote.netCurrencyAmount;

        //$scope.invoiceCreditNote.outstandingAmount=$scope.invoiceCreditNote.currencyAmount-$scope.invoiceCreditNote.amountReceived;
        $scope.invoiceCreditNote.outstandingAmount = 0.0;

        $scope.invoiceCreditNote.localAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.localAmount);
        $scope.invoiceCreditNote.currencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.currencyAmount);
        $scope.invoiceCreditNote.amountReceived = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.amountReceived);
        //$scope.invoiceCreditNote.outstandingAmount= $rootScope.currency($scope.invoiceCreditNote.billingCurrency,$scope.invoiceCreditNote.outstandingAmount);
        $scope.invoiceCreditNote.outstandingAmount = 0.0;
        $scope.invoiceCreditNote.totalDebitLocalAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.totalDebitLocalAmount);
        $scope.invoiceCreditNote.totalDebitCurrencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.totalDebitCurrencyAmount);
        $scope.invoiceCreditNote.totalCreditLocalAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.totalCreditLocalAmount);
        $scope.invoiceCreditNote.totalCreditCurrencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.totalCreditCurrencyAmount);
        $scope.invoiceCreditNote.netLocalAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.netLocalAmount);
        $scope.invoiceCreditNote.netCurrencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.netCurrencyAmount);
        $scope.invoiceCreditNote.totalTaxAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.totalTaxAmount);

        $scope.tempParam = param;

    }



    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }




    $scope.getInvoiceCharges = function(invoiceNo, daybookId) {
        $scope.errorMap = new Map();
        if (daybookId == undefined || daybookId == null || daybookId == '') {
            console.log("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38000"]);
            $scope.errorMap.put("invoice.adjustMentDaybookMaster", $rootScope.nls["ERR38000"]);
            return false;
        }
        if (invoiceNo == undefined || invoiceNo == null || invoiceNo == '') {
            console.log("invoice.adjustmentInvoiceNo", $rootScope.nls["ERR40014"]);
            $scope.errorMap.put("invoice.adjustmentInvoiceNo", $rootScope.nls["ERR40014"]);
            return false;
        }
        GetByInvoiceNo.get({
                invoiceNo: invoiceNo,
                daybookId: daybookId
            }, function(data) {

                if (data.responseCode == 'ERR0') {

                    $scope.invoiceCreditNote = data.responseObject;


                    if ($scope.tempInvoiceCreditNote != undefined && $scope.tempInvoiceCreditNote != null) {
                        $scope.invoiceCreditNote.adjustMentDaybookMaster = $scope.tempInvoiceCreditNote.adjustMentDaybookMaster;
                        $scope.invoiceCreditNote.adjustmentInvoiceNo = $scope.tempInvoiceCreditNote.adjustmentInvoiceNo;
                        $scope.invoiceCreditNote.daybookMaster = $scope.tempInvoiceCreditNote.daybookMaster;
                    }
                    if ($scope.adjInvoiceCreditNote != undefined || $scope.adjInvoiceCreditNote != null) {
                        $scope.invoiceCreditNote.adjustmentInvoiceNo = $scope.adjInvoiceCreditNote.invoiceCreditNoteNo;
                    }
                    $scope.localCurrency = $scope.invoiceCreditNote.localCurrency;
                    $scope.totalAmount = $scope.invoiceCreditNote.netLocalAmount;
                    $scope.shipmentServiceList = [];

                    if ($rootScope.createRevenue != null) {
                        $scope.setFromLocation = $rootScope.createRevenue.setFromLocation;
                        $scope.shipmentServiceDetail = $rootScope.createRevenue.shipmentServiceDetail;

                        if ($scope.shipmentServiceDetail != null) {
                            $scope.shipment = $rootScope.createRevenue.shipment;
                        } else {
                            $scope.consol = $rootScope.createRevenue.consol;
                        }

                    }


                    $scope.invoiceCreditNote.dueDate = null;
                    $scope.invoiceCreditNote.invoiceCreditNoteDate = $rootScope.dateToString(new Date());

                    if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList != null && $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length > 0) {
                        for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {

                            if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 1) {
                                $scope.shipmentServiceDetail = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].shipmentServiceDetail;
                                $scope.shipmentServiceList.push($scope.shipmentServiceDetail);
                            } else {
                                $scope.shipmentServiceList.push($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].shipmentServiceDetail);
                            }

                        }
                    }
                    $scope.invoiceCreditNote.localAmount = $rootScope.currency($scope.invoiceCreditNote.localCurrency, $scope.invoiceCreditNote.localAmount);
                    $scope.invoiceCreditNote.currencyAmount = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.currencyAmount);
                    //$scope.invoiceCreditNote.outstandingAmount= $rootScope.currency($scope.invoiceCreditNote.billingCurrency,$scope.invoiceCreditNote.outstandingAmount);
                    $scope.invoiceCreditNote.outstandingAmount = 0.0;
                    $scope.invoiceCreditNote.amountReceived = $rootScope.currency($scope.invoiceCreditNote.billingCurrency, $scope.invoiceCreditNote.amountReceived);

                    console.log("response : ", data.responseObject);
                    // To fill the attachment table..
                    // $scope.populateAttachment();

                    $scope.getTaxMap();

                } else {

                }
            },
            function(error) {
                console.log("Invoice get Failed : " + error)

            });
        $scope.errorMap = new Map();

    }
    $scope.ajaxReasonEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ReasonList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.reasonList = data.responseObject.searchResult;
                    return $scope.reasonList;

                }
            },
            function(errResponse) {
                console.error('Error while fetching reasonList');
            }
        );

    }
    $scope.selectedReason = function(id) {

        $scope.invoiceCreditNote.reason = $scope.invoiceCreditNote.reasonMaster.reasonName;
        $scope.invoiceCreditNote.reasonCode = $scope.invoiceCreditNote.reasonMaster.reasonCode;
        if ($scope.validateCrnRevenue(13)) {
            $rootScope.navigateToNextField(id);
        }

    };


    $scope.reasonCodeRender = function(item) {
        return {
            label: item.reasonCode,
            item: item
        }
    }

    $scope.getTaxMap = function() {
        $scope.invoiceCreditNote.totalTaxAmount = 0.0;

        $scope.invoiceCreditNote.taxMap = new Map();

        if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList == undefined || $scope.invoiceCreditNote.invoiceCreditNoteDetailList == null || $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length == 0) {
            return;
        }

        for (var i = 0; i < $scope.invoiceCreditNote.invoiceCreditNoteDetailList.length; i++) {
            if ($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != undefined &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList != null &&
                $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length != 0) {
                for (var j = 0; j < $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList.length; j++) {
                    if ($scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode) == null) {
                        var tax = {};
                        tax.taxCode = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxCode;
                        tax.taxName = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].serviceTaxPercentage.taxName;
                        tax.amount = $scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount;
                        $scope.invoiceCreditNote.taxMap.put($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode, tax);
                    } else {
                        var tax = $scope.invoiceCreditNote.taxMap.get($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].taxCode);
                        tax.amount = parseFloat(tax.amount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                    }

                    $scope.invoiceCreditNote.totalTaxAmount = parseFloat($scope.invoiceCreditNote.totalTaxAmount) + parseFloat($scope.invoiceCreditNote.invoiceCreditNoteDetailList[i].invoiceCreditNoteTaxList[j].amount);
                }

            }
        }
    }


    $scope.$on('addCreditNoteRevenueEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.invoiceCreditNote;
        $rootScope.category = "Credit Note Revenue";
        $rootScope.unfinishedFormTitle = "Credit Note Revenue (New)";
        if ($scope.invoiceCreditNote != undefined && $scope.invoiceCreditNote != null &&
            $scope.invoiceCreditNote.daybookMaster != undefined && $scope.invoiceCreditNote.daybookMaster != null &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != undefined &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != null) {
            $rootScope.subTitle = $scope.invoiceCreditNote.daybookMaster.daybookName;
        } else {
            $rootScope.subTitle = "Unknown Daybook"
        }
    })

    $scope.$on('editCreditNoteRevenueEvent', function(events, args) {
        console.log("Scope Credit Note Revenue data :: ", $scope.invoiceCreditNote);
        $rootScope.unfinishedData = $scope.invoiceCreditNote;
        $rootScope.category = "Credit Note Revenue";
        $rootScope.unfinishedFormTitle = "Credit Note Revenue Edit # " + $scope.invoiceCreditNote.invoiceCreditNoteNo;
        if ($scope.invoiceCreditNote != undefined && $scope.invoiceCreditNote != null &&
            $scope.invoiceCreditNote.daybookMaster != undefined && $scope.invoiceCreditNote.daybookMaster != null &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != undefined &&
            $scope.invoiceCreditNote.daybookMaster.daybookName != null) {
            $rootScope.subTitle = $scope.invoiceCreditNote.daybookMaster.daybookName;
        } else {
            $rootScope.subTitle = "Unknown Daybook"
        }
    })
    $scope.$on('addCreditNoteRevenueEventReload', function(e, confirmation) {

        console.log("addInvoiceEventReload ", $scope.invoiceCreditNote);
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.invoiceCreditNote);
        localStorage.isCreditNoteRevenueReloaded = "YES";

        e.preventDefault();
    });
    $scope.$on('editCreditNoteRevenueEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.invoiceCreditNote);
        localStorage.isCreditNoteRevenueReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isCreditNoteRevenueReloaded;

    if ($stateParams.action == "ADD") {

        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteRevenueReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.invoiceCreditNote = $rootScope.selectedUnfilledFormData;

            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteRevenueReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);
                $scope.getTaxMap();
                if (($scope.invoiceCreditNote == undefined || $scope.invoiceCreditNote == null) ||
                    ($scope.invoiceCreditNote.shipmentUid == null ||
                        $scope.invoiceCreditNote.shipmentUid == undefined ||
                        $scope.invoiceCreditNote.shipmentUid == "") &&
                    ($scope.invoiceCreditNote.masterUid == null ||
                        $scope.invoiceCreditNote.masterUid == undefined ||
                        $scope.invoiceCreditNote.masterUid == ""))

                {
                    $scope.init();
                }
                console.log(typeof $scope.invoiceCreditNote.invoiceCreditNoteDate)
            } else {
                $scope.init();
            }
        }
        $scope.adjInvoiceCreditNote = $scope.invoiceCreditNote.adjustmentInvoiceNo;
        $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.creditNoteRevenue"
            },
            {
                label: "Credit Note Revenue",
                state: "layout.creditNoteRevenue"
            }, {
                label: "Add Credit Note Revenue",
                state: null
            }
        ];
    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteRevenueReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.invoiceCreditNote = $rootScope.selectedUnfilledFormData;
            }

        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCreditNoteRevenueReloaded = "NO";
                $scope.invoiceCreditNote = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.init();
            }
        }
        $scope.adjInvoiceCreditNote = $scope.invoiceCreditNote;
        $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.creditNoteRevenue"
            },
            {
                label: "Credit Note Revenue",
                state: "layout.creditNoteRevenue"
            }, {
                label: "Edit Credit Note Revenue",
                state: null
            }
        ];
    }

    $scope.nextFocus = function(id) {
        console.log("called." + id);
        $rootScope.navigateToNextField(id);
    }
}]);