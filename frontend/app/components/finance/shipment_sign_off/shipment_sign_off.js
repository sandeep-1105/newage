app.controller("shipmentSignOffCtrl",['$scope', '$location', '$modal', '$state',
    function($scope, $location, $modal, $state) {

    $scope.page = 0;
    $scope.limit = 10;
    $scope.sortSelection = {
        sortKey: "originCountry",
        sortOrder: "asc"
    };
    /*Pending SHIPMENT SIGNOFF*/
    $scope.PendingshipmentsignoffHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "User",
            "search": true,
            "model": "user",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "200",
            "key": "User"
        },
        {
            "name": "Booking Person",
            "model": "bookingPerson",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "BookingPerson"
        },
        {
            "name": "Sales Person",
            "model": "salesPerson",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "SalesPerson"
        },
        {
            "name": "Master ID",
            "model": "masterId",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w120px",
            "prefWidth": "120",
            "key": "MasterID"

        },
        {
            "name": "Shipment ID",
            "model": "shipmentId",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w120px",
            "prefWidth": "120",
            "key": "ShipmentID"
        },
        {
            "name": "Service ID",
            "model": "serviceId",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "ServiceID"
        },
        {
            "name": "Shipment Date",
            "model": "shipmentDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "ShipmentDate"
        },
        {
            "name": "Billing Customer",
            "model": "billingCustomer",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "BillingCustomer"
        },
        {
            "name": "Self / Agent",
            "model": "selfAgent",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "SelfAgent"
        },
        {
            "name": "Unsign Off By",
            "model": "signUnsign",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w160px",
            "prefWidth": "160",
            "key": "SignUnsign"
        },
        {
            "name": "Action",
            "model": "action",
            "search": true,
            "wrap_cell": true,
            "type": "signoff",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "Action"
        }
    ];
    $scope.PendingshipmentsignoffArr = [{
            "no": "1",
            "user": "Narendran",
            "bookingPerson": "Narendran",
            "salesPerson": "Albert",
            "masterId": "002323233",
            "shipmentId": "56523340741",
            "serviceId": "1312123",
            "billingCustomer": "LTurbo Pvt Ltd",
            "selfAgent": "Self",
            "action": "Sign Off",
            "signUnsign": "-",

        },
        {
            "no": "2",
            "user": "Muthu krishnan",
            "bookingPerson": "Muthu krishnan",
            "salesPerson": "Albert",
            "masterId": "4646464646",
            "shipmentId": "18081140741",
            "serviceId": "2312123",
            "billingCustomer": "LTurbo Pvt Ltd",
            "selfAgent": "Self",
            "action": "Sign Off",
            "signUnsign": "-",

        },
        {
            "no": "3",
            "user": "Narendran",
            "bookingPerson": "Narendran",
            "salesPerson": "Albert",
            "masterId": "7979797999",
            "shipmentId": "1801140752",
            "serviceId": "9444646",
            "billingCustomer": "R&R Techies Pvt Ltd",
            "selfAgent": "Agent",
            "action": "Sign Off",
            "signUnsign": "Ramesh",

        }
    ];

    /*Completed SHIPMENT SIGNOFF*/
    $scope.completedshipmentsignoffHeadArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "User",
            "search": true,
            "model": "user",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "200",
            "key": "User"
        },
        {
            "name": "Booking Person",
            "model": "bookingPerson",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "BookingPerson"
        },
        {
            "name": "Sales Person",
            "model": "salesPerson",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "SalesPerson"
        },
        {
            "name": "Master ID",
            "model": "masterId",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w120px",
            "prefWidth": "120",
            "key": "MasterID"

        },
        {
            "name": "Shipment ID",
            "model": "shipmentId",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w120px",
            "prefWidth": "120",
            "key": "ShipmentID"
        },
        {
            "name": "Service ID",
            "model": "serviceId",
            "search": true,
            "wrap_cell": true,
            "type": "link",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "ServiceID"
        },
        {
            "name": "Shipment Date",
            "model": "shipmentDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "ShipmentDate"
        },
        {
            "name": "Billing Customer",
            "model": "billingCustomer",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "BillingCustomer"
        },
        {
            "name": "Self / Agent",
            "model": "selfAgent",
            "search": true,
            "wrap_cell": true,
            "type": "drop",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "SelfAgent"
        },
        {
            "name": "Sign Off By",
            "model": "signUnsign",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w160px",
            "prefWidth": "160",
            "key": "SignUnsign"
        },
        {
            "name": "Action",
            "model": "action",
            "search": true,
            "wrap_cell": true,
            "type": "signoff",
            "sort": true,
            "width": "w100px",
            "prefWidth": "100",
            "key": "Action"
        }
    ];
    $scope.completedshipmentsignoffArr = [{
            "no": "1",
            "user": "Narendran",
            "bookingPerson": "Narendran",
            "salesPerson": "Albert",
            "masterId": "002323233",
            "shipmentId": "56523340741",
            "serviceId": "1312123",
            "billingCustomer": "LTurbo Pvt Ltd",
            "selfAgent": "Self",
            "action": "Unsign Off",
            "signUnsign": "-",

        },
        {
            "no": "2",
            "user": "Muthu krishnan",
            "bookingPerson": "Muthu krishnan",
            "salesPerson": "Albert",
            "masterId": "4646464646",
            "shipmentId": "18081140741",
            "serviceId": "2312123",
            "billingCustomer": "LTurbo Pvt Ltd",
            "selfAgent": "Self",
            "action": "Unsign Off",
            "signUnsign": "-",

        },
        {
            "no": "3",
            "user": "Narendran",
            "bookingPerson": "Narendran",
            "salesPerson": "Albert",
            "masterId": "7979797999",
            "shipmentId": "1801140752",
            "serviceId": "9444646",
            "billingCustomer": "R&R Techies Pvt Ltd",
            "selfAgent": "Agent",
            "action": "Unsign Off",
            "signUnsign": "Ramesh",

        }
    ];
    var signOffModal = $modal({
        scope: $scope,
        templateUrl: 'app/components/finance/shipment_sign_off/view/sign_off_popup.html',
        show: false
    });

    var unsignOffModal = $modal({
        scope: $scope,
        templateUrl: 'app/components/finance/shipment_sign_off/view/unsign_off_popup.html',
        show: false
    });

    $scope.actionEvent = function(param) {
        if (param.type == "Unsign Off") {
            unsignOffModal.$promise.then(unsignOffModal.show);
        } else {
            signOffModal.$promise.then(signOffModal.show);
        }
    };

    /*BUTTON GROUP SCOPE VALUE*/
    $scope.btnGrp = {};
    $scope.btnGrp.myShipment = ["My Shipments", "Subordinate Shipments", "All"];
    $scope.btnGrp.self = ["Self", "Agent", "All"];
    $scope.selectedgrp = "All";


}]);