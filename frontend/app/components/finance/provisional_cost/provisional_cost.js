/**
 * Created by hmspl on 12/4/16.
 */
app.controller('provisionalCostController',['$rootScope', '$timeout', '$state', '$scope', '$http', '$location', '$modal', 'ngDialog', 'ProvisionalView',
    'ProvisionalSearch', 'ProvisionalService', '$stateParams', 'ConsolIdBasedOnUid', 'ShipmentIdBasedOnUid', 
    'cloneService', 'RecentHistorySaveService', 'roleConstant',
    function($rootScope, $timeout, $state, $scope, $http, $location, $modal, ngDialog, ProvisionalView,
    ProvisionalSearch, ProvisionalService, $stateParams, ConsolIdBasedOnUid, ShipmentIdBasedOnUid, 
    cloneService, RecentHistorySaveService, roleConstant) {

    $scope.provisionalCostArr = [{
            "name": "#",
            "width": "w50px",
            "prefWidth": "50",
            "model": "no",
            "search": false

        },
        {
            "name": "Master ID",
            "search": true,
            "model": "masterUid",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "200",
            "key": "masterUid"
        },
        {
            "name": "Shipment ID",
            "model": "shipmentUid",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "shipmentUid"
        },
        {
            "name": "Service Name",
            "model": "serviceName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "w200px",
            "prefWidth": "200",
            "key": "serviceName"
        },
        {
            "name": "Cost",
            "model": "totalCost",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "totalCost"

        },
        {
            "name": "Revenue",
            "model": "totalRevenue",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "totalRevenue"
        }, {
            "name": "Neutral",
            "model": "netAmount",
            "search": true,
            "wrap_cell": true,
            "type": "currency",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "netAmount"
        }, {
            "name": "Last Updated On",
            "model": "lastUpdatedDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "w150px",
            "prefWidth": "150",
            "key": "lastUpdatedOn"
        }

    ]

    $scope.setSearch = function(value) {
        $scope.detailTab = value;
        $scope.searchFlag = value;
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.provisionSearch($scope.page, $scope.limit);
    }
    $scope.showActionButton = true;
    $scope.selectedRecordIndex = 0;

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            if ($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
                stateParameters.searchStatus = $scope.searchDto.searchStatus = $stateParams.searchStatus;
            }
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchFlag != undefined && $stateParams.searchFlag != null) {
                stateParameters.searchFlag = $scope.searchFlag = $stateParams.searchFlag;
            } else {
                stateParameters.searchFlag = $scope.searchFlag = "active";
            }
        } else {
            return;
        }
        ProvisionalSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.provisionalIndex = stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.provisionalId = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.viewProvisional", stateParameters);
        });
    }

    $scope.sortSelection = {
        sortKey: "shipmentUid",
        sortOrder: "desc"
    }

    $scope.rowLink = function(data) {
        console.log("data", data);
    }

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_VIEW)) {
            console.log("$scope.searchDto --- ", $scope.searchDto);
            $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var localRefStateParam = {
                provisionalId: data.id,
                provisionalIndex: $scope.selectedRecordIndex,
                searchFlag: $scope.searchFlag,
                totalRecord: $scope.totalRecord
            };

            $state.go("layout.viewProvisional", $scope.searchDtoToStateParams(localRefStateParam));
        }
    }

    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.provisionalIndex;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }
                // Status - Active, All
                if ($scope.searchDto.searchStatus != undefined && $scope.searchDto.searchStatus != null) {
                    param.searchStatus = $scope.searchDto.searchStatus;
                }

            }

        }
        return param;
    }


    $scope.viewById = function(id) {
        ProvisionalView.get({
            id: id
        }, function(data) {
            $rootScope.setNavigate1("Finance");
            $rootScope.setNavigate2("Provisional");
            $scope.populateProvisionData(data);
        }, function(error) {
            console.log("Provisional view Failed : " + error)
        });
    }



    $scope.populateProvisionData = function(data) {
        if (data.responseCode == "ERR0") {
            $scope.provisional = data.responseObject;


            $rootScope.unfinishedFormTitle = "Provisional View # " + $scope.provisional.id;
            var rHistoryObj = {
                'title': 'Provisional View #' + $scope.provisional.id,
                'subTitle': $rootScope.subTitle,
                'stateName': $state.current.name,
                'stateParam': JSON.stringify($stateParams),
                'stateCategory': 'Provisional',
                'serviceType': 'AIR'
            }

            RecentHistorySaveService.form(rHistoryObj);
        } else {
            console.log("Provisional view Failed " + data.responseDescription)
        }

        if (($stateParams.shipmentUid != null && $stateParams.shipmentUid != undefined && $stateParams.shipmentUid != "") ||
            ($stateParams.consolUid != null && $stateParams.consolUid != undefined && $stateParams.consolUid != "")) {
            $scope.showActionButton = false;
        }

    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.provisionSearch($scope.page, $scope.limit);
    }

    $scope.changePage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.provisionSearch($scope.page, $scope.limit);
    }

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.provisionSearch($scope.page, $scope.limit);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.provisionSearch($scope.page, $scope.limit);
    }

    $scope.init = function() {
        $scope.detailTab = 'active';
        $rootScope.setNavigate1("Finance");
        $rootScope.setNavigate2("Provisional");
        $scope.setSearch('active');
        $scope.provisional = {};
        ProvisionalService.set({});
        $scope.searchDto = {};
        $scope.provisionSearch($scope.page, $scope.limit);

    }

    $scope.back = function() {
        if ($stateParams.fromState == null || $stateParams.fromState == undefined) {
            $scope.detailTab = $stateParams.viewFrom;
            if ($scope.detailTab == 'active') {
                $scope.setSearch('active');
            } else {
                $scope.setSearch('all');
            }
            $scope.provisional = {};
            $state.go('layout.provisionalCost');
        } else {
            $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
        }
    }


    $scope.provisionSearch = function(selectedPageNumber, recordPerPage) {
        $scope.provisional = {};
        $scope.searchDto.selectedPageNumber = selectedPageNumber;
        $scope.searchDto.recordPerPage = recordPerPage;
        $scope.tmpSearchDto = cloneService.clone($scope.searchDto);
        $scope.provisionalArr = [];
        ProvisionalSearch.query($scope.tmpSearchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.totalRevenue = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.totalRevenue);
                tempObj.netAmount = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.netAmount);
                tempObj.totalCost = $rootScope.currencyFormatDecimalPoint(tempObj.decimalPoint, tempObj.locale, tempObj.totalCost);
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.provisionalArr = resultArr;

        });
    }

    $scope.serialNo = function(index) {
        var index = index + 1;
        return index;
    }



    $scope.cancel = function() {
        $scope.showDetail = false;

    }


    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.FINANCE_PROVISIONAL_MODIFY)) {
            console.log("Provisional Edit Button is Pressed.....");
            ProvisionalService.set($scope.provisional);
            $scope.showDetail = false;
            $scope.consolId = null;
            $scope.shipmentId = null;

            if ($scope.provisional.shipmentUid != null) {
                // get shipment
                ShipmentIdBasedOnUid.get({
                    shipmentUid: $scope.provisional.shipmentUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var shipmentId = data.responseObject;
                        var localRefStateParam = {
                            provisionalId: $scope.provisional.id,
                            shipmentId: shipmentId
                        };
                        $state.go("layout.editProvisional", localRefStateParam);
                    }
                }, function(error) {
                    console.log("ShipmentId getting Failed : " + error)

                });


            } else {
                //get consol
                ConsolIdBasedOnUid.get({
                    consolUid: $scope.provisional.masterUid
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        var consolId = data.responseObject;
                        var localRefStateParam = {
                            provisionalId: $scope.provisional.id,
                            consolId: consolId
                        };
                        $state.go("layout.editProvisional", localRefStateParam);
                    }
                }, function(error) {
                    console.log("consolId getting Failed : " + error)

                });

            }

        }
    };




    $scope.refmodel = function(provisionalItem) {
        var newScope = $scope.$new();
        newScope.provisionalItem = provisionalItem;
        var myTotalOtherModal = $modal({
            scope: newScope,
            templateUrl: 'app/components/finance/provisional_cost/view/refpopup.html',
            show: false
        });
        myTotalOtherModal.$promise.then(myTotalOtherModal.show);
    };




    switch ($stateParams.action) {
        case "VIEW":
            console.log("I am In Provisional View Page");
            $rootScope.unfinishedFormTitle = "Provisional View # " + $stateParams.provisionalId;
            $rootScope.unfinishedData = undefined;
            $scope.selectedTabIndex = 0;
            $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
            $scope.totalRecord = parseInt($stateParams.totalRecord);
            $scope.viewById($stateParams.provisionalId);
            $rootScope.breadcrumbArr = [{
                    label: "Finance",
                    state: "layout.provisionalCost"
                },
                {
                    label: "Provisional",
                    state: "layout.provisionalCost"
                }, {
                    label: "View Provisional",
                    state: null
                }
            ];
            break;
        case "SEARCH":
            console.log("I am In Provisional List Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Finance",
                state: "layout.provisionalCost"
            }, {
                label: "Provisional",
                state: null
            }];
            break;
        default:

    }

}]);