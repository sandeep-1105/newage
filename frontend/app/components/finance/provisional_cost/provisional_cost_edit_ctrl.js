app.controller("provisionalCostEditController",['$rootScope', '$state', '$stateParams', '$timeout', '$scope', '$http',
        '$location', '$modal', 'ngDialog', 'ngProgressFactory', '$compile',
        'ProvisionalEdit', 'ProvisionalSave',
        'ChargeSearchKeyword', 'UnitSearchKeyword',
        'CurrencyMasterSearchKeyword', 'consolService', 'ProvisionalView',
        'ReferenceTypeSearchByKeyword', 'PartiesList', 'cloneService', 'InvoiceServiceTaxPercentage', 'UnitMasterGetByCode',
        'ConsolGet', 'ServiceGetByUid', '$q', 'ShipmentGet', 'appConstant', 'CommonValidationService', 'Notification',
    function($rootScope, $state, $stateParams, $timeout, $scope, $http,
        $location, $modal, ngDialog, ngProgressFactory, $compile,
        ProvisionalEdit, ProvisionalSave,
        ChargeSearchKeyword, UnitSearchKeyword,
        CurrencyMasterSearchKeyword, consolService, ProvisionalView,
        ReferenceTypeSearchByKeyword, PartiesList, cloneService, InvoiceServiceTaxPercentage, UnitMasterGetByCode,
        ConsolGet, ServiceGetByUid, $q, ShipmentGet, appConstant, CommonValidationService, Notification) {


        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('service-panel'));
        $scope.contained_progressbar.setAbsolute();

        $scope.groupCol = [{
            name: 'Charge Code',
            width: '100px',
            valueField: 'chargeCode'
        }, {
            name: 'Charge Name',
            width: '120px',
            valueField: 'chargeName'
        }];

        $scope.init = function() {

            $scope.serviceList = [];
            if ($scope.provisional != undefined &&
                $scope.provisional != null &&
                $scope.provisional.id != null) {


            } else {
                $scope.provisional = {};
                $scope.provisional.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                $scope.provisional.provisionalItemList = [];
                $scope.provisional.provisionalItemList.push({});
                $scope.provisionalItem = {};
            }

            $scope.callAsynchronous();

            $rootScope
                .setNavigate3($scope.provisional.id != null ? "Edit Provisional Cost" :
                    "Add Provisional Cost");
            $scope.contained_progressbar = ngProgressFactory
                .createInstance();
            $scope.contained_progressbar.setParent(document
                .getElementById('service-panel'));
            $scope.contained_progressbar.setAbsolute();


        }
        $scope.callAsynchronous = function() {

            var myPromise = $scope.getShipmentConsolData();

            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function(resolve) {
                console.log("Succesfull getting shipment and consol data");
            }, function(reject) {
                console.log("Failed while getting shipment and consol data");
            });

        }

        $scope.getShipmentConsolData = function() {
            var deferred = $q.defer();
            if ($stateParams.shipmentId != null) {
                ShipmentGet.get({
                    id: $stateParams.shipmentId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.shipment = data.responseObject;

                        if ($stateParams.shipmentServiceId != null && $stateParams.shipmentServiceId != undefined) {
                            ServiceGetByUid.get({
                                serviceUid: $stateParams.shipmentServiceId
                            }, function(data) {
                                if (data.responseCode == 'ERR0') {
                                    $scope.shipmentServiceDetail = data.responseObject;
                                    $scope.provisional.shipmentUid = $scope.shipmentServiceDetail.shipmentUid;

                                    $scope.provisional.serviceUid = $scope.shipmentServiceDetail.serviceUid;

                                    if ($scope.shipmentServiceDetail.consolUid != null &&
                                        $scope.shipmentServiceDetail.consolUid != undefined &&
                                        $scope.shipmentServiceDetail.consolUid != '') {
                                        $scope.provisional.masterUid = $scope.shipmentServiceDetail.consolUid;
                                    }

                                    $scope.provisional.serviceName = $scope.shipmentServiceDetail.serviceMaster.serviceName;

                                    $scope.serviceList.push($scope.shipmentServiceDetail);
                                    if ($scope.serviceList != null && $scope.serviceList != undefined && $scope.serviceList.length == 1) {

                                        $scope.provisional.provisionalItemList[0].shipmentService = $scope.serviceList[0];
                                    }
                                }
                            }, function(error) {
                                console.log("shipmentService get Failed : " + error)
                                deferred.reject('ERROR');
                            });
                        } else {
                            $scope.serviceList = [];
                            for (var i = 0; i < $scope.shipment.shipmentServiceList.length; i++) {
                                $scope.serviceList.push($scope.shipment.shipmentServiceList[i]);
                            }
                        }
                    }
                }, function(error) {
                    console.log("Shipment view Failed : " + error)
                    deferred.reject('ERROR');
                });

            } else {
                ConsolGet.get({
                    id: $stateParams.consolId
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.consol = data.responseObject;
                        $scope.getConsolShipmentServices();
                        $scope.provisional.masterUid = $scope.consol.consolUid;
                        $scope.provisional.serviceName = $scope.consol.serviceMaster.serviceName;
                        if ($scope.serviceList != null && $scope.serviceList != undefined && $scope.serviceList.length == 1) {

                            $scope.provisional.provisionalItemList[0].shipmentService = $scope.serviceList[0];
                        }
                        if ($stateParams.shipmentServiceId != null && $stateParams.shipmentServiceId != undefined) {
                            ServiceGetByUid.get({
                                serviceUid: $stateParams.shipmentServiceId
                            }, function(data) {
                                if (data.responseCode == 'ERR0') {
                                    $scope.shipmentServiceDetail = data.responseObject;

                                }
                            }, function(error) {
                                console.log("shipmentService get Failed : " + error)
                                deferred.reject('ERROR');
                            });
                        }


                    }
                }, function(error) {
                    console.log("Consol get Failed : " + error)
                    deferred.reject('ERROR');
                });
            }
            //return the promise
            return deferred.promise;
        }




        $scope.refeditmodel = function(provisionalItem, index) {

            $scope.provisinalindex = index;
            var startingTabIndex = 'billToParty';


            var myTotalOtherModal = $modal({
                scope: $scope,
                templateUrl: 'app/components/finance/provisional_cost/view/refpopup_edit.html',
                show: false
            });
            myTotalOtherModal.$promise.then(myTotalOtherModal.show);

        };


        $scope.moveTo = function(id) {
            if (id != undefined && id != null && id != "")
                document.getElementById(id).focus();
        }



        $scope.cancelProvisional = function() {
            if ($scope.proForm.$dirty) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).
                then(
                    function(value) {
                        if (value == 1 && $scope.provisional.id == null) {
                            $scope.save();
                        } else if (value == 1 && $scope.provisional.id != null) {
                            $scope.update();
                        } else if (value == 2) {
                            var params = {};

                            if ($stateParams.fromStateParams == null || $stateParams.fromStateParams == undefined ||
                                $stateParams.fromStateParams == '') {
                                $state.go("layout.provisionalCost");
                            } else {
                                $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                            }
                        } else {
                            console.log("Nothing selected..Cancel")
                        }
                    });

            } else {

                if ($stateParams.fromStateParams == null || $stateParams.fromStateParams == undefined ||
                    $stateParams.fromStateParams == '') {
                    $state.go("layout.provisionalCost");
                } else {
                    $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                };
            }
        };


        $scope.save = function() {
            console.log("Save Method is called.");
            if ($scope.callValidateProvisionalItem() == false) {
                console.log("validation failed");
                return;
            }

            if ($scope.provisional != null && $scope.consol != null && $scope.consol.consolUid != null) {
                $scope.provisional.masterUid = $scope.consol.consolUid;
                $scope.provisional.serviceName = $scope.consol.serviceMaster.serviceName;
            }

            $scope.provisional.createProvisional = null;
            $scope.tmpObj = cloneService.clone($scope.provisional);
            for (var i = 0; i < $scope.tmpObj.provisionalItemList.length; i++) {

                if ($scope.tmpObj.provisionalItemList[i].shipmentService != null && $scope.tmpObj.provisionalItemList[i].shipmentService != undefined) {
                    $scope.tmpObj.provisionalItemList[i].serviceUid = $scope.tmpObj.provisionalItemList[i].shipmentService.serviceUid;
                    $scope.newObj = {};
                    $scope.newObj.id = $scope.tmpObj.provisionalItemList[i].shipmentService.id;
                    $scope.tmpObj.provisionalItemList[i].shipmentService = $scope.newObj;
                } else {
                    $scope.tmpObj.provisionalItemList[i].serviceUid = null;
                    $scope.tmpObj.provisionalItemList[i].shipmentService = null;
                }



            }

            if ($scope.validateProvisional(0)) {

                // $scope.contained_progressbar.start();
                ProvisionalSave.save($scope.tmpObj).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {

                                if ($stateParams.fromStateParams == null || $stateParams.fromStateParams == undefined ||
                                    $stateParams.fromStateParams == '') {
                                    $state.go("layout.provisionalCost");
                                } else {
                                    $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                                }


                            } else {
                                Notification.error(data.responseDescription);
                                console.log("Provisional added Failed ", data.responseDescription)
                            }
                            // $scope.contained_progressbar.complete();
                            angular.element(".panel-body").animate({
                                scrollTop: 0
                            }, "slow");
                        },
                        function(error) {
                            console.log("Provisional added Failed : ", error)
                        });
            } else {
                console.log("Invalid Form Submission.");
            }

        }

        $scope.update = function() {
            console.log("Update Method is called.");
            if ($scope.callValidateProvisionalItem() == false) {
                console.log("validation failed");
                return;
            }

            $scope.provisional.createProvisional = null;
            $scope.tmpObj = cloneService.clone($scope.provisional);
            for (var i = 0; i < $scope.tmpObj.provisionalItemList.length; i++) {

                if ($scope.tmpObj.provisionalItemList[i].shipmentService != null && $scope.tmpObj.provisionalItemList[i].shipmentService != undefined) {
                    $scope.tmpObj.provisionalItemList[i].serviceUid = $scope.tmpObj.provisionalItemList[i].shipmentService.serviceUid;
                    $scope.newObj = {};
                    $scope.newObj.id = $scope.tmpObj.provisionalItemList[i].shipmentService.id;
                    $scope.tmpObj.provisionalItemList[i].shipmentService = $scope.newObj;
                } else {
                    $scope.tmpObj.provisionalItemList[i].serviceUid = null;
                    $scope.tmpObj.provisionalItemList[i].shipmentService = null;
                }


            }



            if ($scope.validateProvisional(0)) {
                // $scope.contained_progressbar.start();

                ProvisionalEdit.update($scope.tmpObj).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                if ($stateParams.fromStateParams == null || $stateParams.fromStateParams == undefined ||
                                    $stateParams.fromStateParams == '') {
                                    $state.go("layout.provisionalCost");
                                } else {
                                    $state.go($stateParams.fromState, JSON.parse($stateParams.fromStateParams));
                                }

                            } else {
                                Notification.error(data.responseDescription);
                                console
                                    .log("Provisional updated Failed " +
                                        data.responseDescription)
                            }
                            // $scope.contained_progressbar.complete();
                            angular.element(".panel-body")
                                .animate({
                                    scrollTop: 0
                                }, "slow");
                        },
                        function(error) {
                            console
                                .log("Provisional updated Failed : " +
                                    error)
                        });
            } else {
                console.log("Invalid Form Submission.");
            }

        }



        $scope.netCalculation = function() {
            var totalRevenue = 0;
            var totalCost = 0;
            for (var i = 0; i < $scope.provisional.provisionalItemList.length; i++) {
                if ($scope.provisional.provisionalItemList[i].sellLocalAmount != null &&
                    $scope.provisional.provisionalItemList[i].sellLocalAmount != undefined &&
                    $scope.provisional.provisionalItemList[i].sellLocalAmount != "" &&
                    isNaN($scope.provisional.provisionalItemList[i].sellLocalAmount) == false) {
                    totalRevenue += parseFloat($scope.provisional.provisionalItemList[i].sellLocalAmount);

                }

                if ($scope.provisional.provisionalItemList[i].buyLocalAmount != null &&
                    $scope.provisional.provisionalItemList[i].buyLocalAmount != undefined &&
                    $scope.provisional.provisionalItemList[i].buyLocalAmount != "" &&
                    isNaN($scope.provisional.provisionalItemList[i].buyLocalAmount) == false) {
                    if ($scope.provisional.provisionalItemList[i].plusMinus != undefined &&
                        $scope.provisional.provisionalItemList[i].plusMinus != null &&
                        $scope.provisional.provisionalItemList[i].plusMinus === '+') {
                        totalRevenue += parseFloat($scope.provisional.provisionalItemList[i].buyLocalAmount);
                    } else {
                        totalCost += parseFloat($scope.provisional.provisionalItemList[i].buyLocalAmount);
                    }

                }


            }

            $scope.provisional.totalRevenue = $rootScope.currency($scope.provisional.localCurrency, parseFloat(totalRevenue));
            $scope.provisional.totalCost = $rootScope.currency($scope.provisional.localCurrency, parseFloat(totalCost));
            $scope.provisional.netAmount = $rootScope.currency($scope.provisional.localCurrency, parseFloat($scope.provisional.totalRevenue - $scope.provisional.totalCost));

        }

        $scope.deleteProvisionalItem = function(index) {
            $scope.provisional.provisionalItemList.splice(index, 1);
            console.log("Provisional Deleted");
        }

        // List pickers start

        // Charge

        $scope.ajaxChargeEvent = function(object) {
            console.log("ajaxChargeEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ChargeSearchKeyword.query($scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.totalRecord = data.responseObject.totalRecord;
                            $scope.chargeMasterList = data.responseObject.searchResult;
                            return $scope.chargeMasterList;
                            /* $scope.showChargeMasterList=true; */
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Charge');
                    });
        }

        /*
         * $scope.assignChargeMaster = function(obj){ obj.chargeName =
         * obj.chargeMaster.chargeName; }
         */

        $scope.chargeRender = function(item) {
            return {
                label: item.chargeCode,
                item: item
            }
        }

        // Unit

        $scope.ajaxUnitEvent = function(object) {

            console.log("ajaxUnitEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return UnitSearchKeyword.query($scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.totalRecord = data.responseObject.totalRecord;
                            $scope.unitMasterList = data.responseObject.searchResult;
                            return $scope.unitMasterList;
                            // $scope.showUnitMasterList=true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Unit List');
                    });

        }

        // SellCurrency

        $scope.ajaxSellCurrency = function(object) {
            console.log("ajaxNetCurrency ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencyMasterSearchKeyword
                .query($scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.totalRecord = data.responseObject.totalRecord;
                            $scope.sellCurrencyList = data.responseObject.searchResult;
                            return $scope.sellCurrencyList;
                            // $scope.showNetCurrencyList=true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Currency List');
                    });

        }

        // BuyCurrency
        $scope.ajaxBuyCurrency = function(object) {
            console.log("ajaxNetCurrency ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencyMasterSearchKeyword
                .query($scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.totalRecord = data.responseObject.totalRecord;
                            $scope.buyCurrencyList = data.responseObject.searchResult;
                            return $scope.buyCurrencyList;
                            // $scope.showNetCurrencyList=true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Currency List');
                    });

        }

        // reference type start
        $scope.ajaxReferenceTypeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return ReferenceTypeSearchByKeyword
                .fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.referenceTypeList = data.responseObject.searchResult;
                            return $scope.referenceTypeList;
                            // $scope.showDocumentPackList=true;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching reference type list');
                    });

        }

        // reference type end

        $scope.referenceTypeRender = function(item) {
            return {
                label: item.referenceTypeName,
                item: item
            }
        }

        $scope.unitRender = function(item) {
            return {
                label: item.unitName,
                item: item
            }
        }
        $scope.currencyRender = function(item) {
            return {
                label: item.currencyCode,
                item: item
            }
        }

        $scope.selectedReferenceType = function(referenceType,
            nextIdValue, index) {
            console.log("selectedReferenceType called : ",
                nextIdValue)
            $rootScope.navigateToNextField(nextIdValue + index);

        }

        $scope.selectedPlusMinus = function(provisionalItem, index) {

            console.log("selectedservice called : ", index,
                provisionalItem)
            $scope.refeditmodel(provisionalItem, index)

        }

        $scope.selectedService = function(nextIdValue, index) {

            console.log("selectedservice called : ", nextIdValue)
            $rootScope.navigateToNextField(nextIdValue + index);

        }

        $scope.selectedUnit = function(unit, nextIdValue, index) {

            console.log("selectedUnit called : ", unit)
            $rootScope.navigateToNextField(nextIdValue + index);

        }

        $scope.selectedCharge = function(charge, nextIdValue, index) {
            console.log("selectedCharge called : ", charge)
            $rootScope.navigateToNextField(nextIdValue + index);

        }

        $scope.selectedBuyCurrency = function(provisionalItem, nextIdValue,
            index) {
            if (provisionalItem.buyCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                if ($rootScope.baseCurrenyRate[provisionalItem.buyCurrency.id] == undefined ||
                    $rootScope.baseCurrenyRate[provisionalItem.buyCurrency.id] == null) {

                } else {
                    provisionalItem.buyRoe = $rootScope.baseCurrenyRate[provisionalItem.buyCurrency.id].cbuy;
                }
            } else {
                provisionalItem.buyRoe = 1.0;
            }
            $rootScope.navigateToNextField(nextIdValue + index);

        }

        $scope.selectedSellCurrency = function(provisionalItem, nextIdValue,
            index) {
            if (provisionalItem.sellCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {

                if ($rootScope.baseCurrenyRate[provisionalItem.sellCurrency.id] == undefined ||
                    $rootScope.baseCurrenyRate[provisionalItem.sellCurrency.id] == null) {

                } else {
                    provisionalItem.sellRoe = $rootScope.baseCurrenyRate[provisionalItem.sellCurrency.id].csell;
                }

            } else {
                provisionalItem.sellRoe = 1.0;
            }

            console.log("selectedSellCurrency called : ", provisionalItem.sellCurrency)
            $rootScope.navigateToNextField(nextIdValue + index);

        }

        $scope.getConsolShipmentServices = function() {

            if ($scope.consol != null && $scope.consol != undefined &&
                $scope.consol != '') {
                if ($scope.consol.shipmentLinkList != null &&
                    $scope.consol.shipmentLinkList != undefined &&
                    $scope.consol.shipmentLinkList != '' &&
                    $scope.consol.shipmentLinkList.length > 0) {
                    for (var i = 0; i < $scope.consol.shipmentLinkList.length; i++) {
                        $scope.serviceList.push($scope.consol.shipmentLinkList[i].service);
                    }
                }
            }
            console.log("Consol Shipment Service list : ",
                $scope.serviceList);

        }

        // Validation for Provisional

        $scope.validateProvisional = function(validateCode) {

            $scope.errorMap = new Map();


            /*if (validateCode == 0 || validateCode == 1) {

							if ($scope.provisional.masterUid == undefined
									|| $scope.provisional.masterUid == ""
									|| $scope.provisional.masterUid == null) {

								console.log("masterUid",
										$rootScope.nls["ERR96601"]);
								$scope.errorMap.put("provisional.masterUid",
										$rootScope.nls["ERR96601"]);
								return false;

							}

						}
						
                        */

            return true;
        }


        // Start validation for validateProvisionItem

        var validateProvisionItem = function(dataObj, index, type) {
            var errorArr = [];
            if (type == "row") {
                $scope.secondArr[index].errTextArr = [];
            }
            //serviceId

            if ($scope.provisional.shipmentUid != null && $scope.provisional.shipmentUid != undefined && $scope.provisional.shipmentUid != '') {

                if (dataObj.shipmentService == null || dataObj.shipmentService == undefined || dataObj.shipmentService.id == undefined ||
                    dataObj.shipmentService.id == null) {
                    console.log("serviceId is Mandatory.")
                    if (type == "row") {
                        $scope.secondArr[index].serviceId = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96668"]);
                    }

                    errorArr.push(0);
                } else {
                    if (type == "row") {
                        $scope.secondArr[index].serviceId = false;

                    }

                    errorArr.push(1);
                }
            }

            //chargeMaster

            if (dataObj.chargeMaster == null || dataObj.chargeMaster.id == undefined ||
                dataObj.chargeMaster.id == null || dataObj.chargeMaster.id == "") {
                console.log("Charge is Mandatory.")
                if (type == "row") {
                    $scope.secondArr[index].chargeName = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96607"]);
                }

                errorArr.push(0);
            } else {

                if (dataObj.chargeMaster.status == 'Block') {
                    console.log("Selected Charge is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].chargeName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96608"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.chargeMaster.status == 'Hide') {
                    console.log("Selected Charge is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].chargeName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96609"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].chargeName = false;

                    }

                    errorArr.push(1);
                }


            }



            //unit master


            if (dataObj.unitMaster == null || dataObj.unitMaster.id == undefined ||
                dataObj.unitMaster.id == null || dataObj.unitMaster.id == "") {
                console.log("Unit is Mandatory.")
                /*if(type=="row") {
                    $scope.secondArr[index].unitName = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96610"]);
                }
				                  
                errorArr.push(0);*/
            } else {

                if (dataObj.unitMaster.status == 'Block') {
                    console.log("Selected Unit is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].unitName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96611"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.unitMaster.status == 'Hide') {
                    console.log("Selected Unit is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].unitName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96612"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].unitName = false;

                    }

                    errorArr.push(1);
                }
            }

            // noOfUnits

            if (dataObj.noOfUnits == undefined ||
                dataObj.noOfUnits == null ||
                dataObj.noOfUnits == "" || isNaN(dataObj.noOfUnits)) {
                dataObj.noOfUnits = null;
            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_UNIT, dataObj.noOfUnits)) {
                if (type == "row") {
                    $scope.secondArr[index].noOfUnits = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96667"]);
                }
                errorArr.push(0);

            } else {
                if (type == "row") {
                    $scope.secondArr[index].noOfUnits = false;

                }

                errorArr.push(1);
            }

            //sell currency

            if (dataObj.sellCurrency == null || dataObj.sellCurrency == undefined || dataObj.sellCurrency.id == undefined ||
                dataObj.sellCurrency.id == null || dataObj.sellCurrency.id == "") {
                if (dataObj.buyCurrency == null || dataObj.buyCurrency == undefined || dataObj.buyCurrency.id == undefined ||
                    dataObj.buyCurrency.id == null || dataObj.buyCurrency.id == "" || dataObj.buyRoe == undefined ||
                    dataObj.buyRoe == null ||
                    dataObj.buyRoe == "") {

                    if (type == "row") {
                        console.log("Currency is Mandatory.")
                        $scope.secondArr[index].sellCurrencyName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96622"]);
                    }

                    errorArr.push(0);
                }
            } else {

                if (dataObj.sellCurrency.status == 'Block') {
                    console.log("Selected Currency is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].sellCurrencyName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96623"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.sellCurrency.status == 'Hide') {
                    console.log("Selected Currency is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].sellCurrencyName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96624"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].sellCurrencyName = false;

                    }

                    errorArr.push(1);
                }


            }


            // sellRoe

            if (dataObj.sellRoe == undefined ||
                dataObj.sellRoe == null ||
                dataObj.sellRoe == "") {
                if (dataObj.buyCurrency == null || dataObj.buyCurrency == undefined || dataObj.buyCurrency.id == undefined ||
                    dataObj.buyCurrency.id == null || dataObj.buyCurrency.id == "" || dataObj.buyRoe == undefined ||
                    dataObj.buyRoe == null ||
                    dataObj.buyRoe == "") {
                    if (type == "row") {
                        $scope.secondArr[index].sellRoe = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96625"]);
                    }
                    errorArr.push(0);
                }
            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_ROE, dataObj.sellRoe)) {
                if (type == "row") {
                    $scope.secondArr[index].sellRoe = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96627"]);
                }
                errorArr.push(0);

            } else if (dataObj.sellCurrency != null && $rootScope.userProfile.selectedUserLocation.currencyMaster.id == dataObj.sellCurrency.id) {
                if (dataObj.sellRoe > 1) {

                    $scope.secondArr[index].sellRoe = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40017"]);
                    errorArr.push(0);
                }

            } else if (dataObj.sellCurrency != null && $rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.sellCurrency.id) {
                if (dataObj.sellRoe == 1) {
                    /*
                    					        				
                    					        				 $scope.secondArr[index].sellRoe = true;
                    					                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40018"]);
                    					                         errorArr.push(0);
                    					    				         */
                }

            } else {
                if (type == "row") {
                    $scope.secondArr[index].sellRoe = false;

                }

                errorArr.push(1);
            }



            // sellAmountPerUnit
            if (!isNaN(dataObj.sellAmountPerUnit) && dataObj.sellAmountPerUnit != undefined &&
                dataObj.sellAmountPerUnit != null &&
                dataObj.sellAmountPerUnit != "") {


                if (isNaN(parseFloat(dataObj.sellAmountPerUnit))) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96630"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" &&
                    !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.sellAmountPerUnit)) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96630"]);
                    }
                    errorArr.push(0);
                } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.sellAmountPerUnit)) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96630"]);
                    }
                    errorArr.push(0);
                } else if (dataObj.amount == 0) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96629"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.sellAmountPerUnit < 0 || dataObj.sellAmountPerUnit >= 99999999999.999) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96630"]);
                    }
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmountPerUnit = false;

                    }
                    errorArr.push(1);
                }
            }



            // sellAmount
            if (!isNaN(dataObj.sellAmount) && dataObj.sellAmount != undefined &&
                dataObj.sellAmount != null &&
                dataObj.sellAmount != "") {


                if (isNaN(parseFloat(dataObj.sellAmount))) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96633"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" &&
                    !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.sellAmount)) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96633"]);
                    }
                    errorArr.push(0);
                } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.sellAmount)) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96633"]);
                    }
                    errorArr.push(0);
                } else if (dataObj.amount == 0) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96632"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.sellAmount < 0 || dataObj.sellAmount >= 99999999999.999) {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96633"]);
                    }
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].sellAmount = false;

                    }
                    errorArr.push(1);
                }
            }


            // sellLocalAmount
            if (!isNaN(dataObj.sellLocalAmount) && dataObj.sellLocalAmount != undefined &&
                dataObj.sellLocalAmount != null &&
                dataObj.sellLocalAmount != "") {


                if (isNaN(parseFloat(dataObj.sellLocalAmount))) {
                    if (type == "row") {
                        $scope.secondArr[index].sellLocalAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96633"]);
                    }
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].sellLocalAmount = false;

                    }
                    errorArr.push(1);
                }
            }




            //buy currency

            if (dataObj.buyCurrency == null || dataObj.buyCurrency.id == undefined ||
                dataObj.buyCurrency.id == null || dataObj.buyCurrency.id == "") {
                if (dataObj.sellCurrency == null || dataObj.sellCurrency == undefined || dataObj.sellCurrency.id == undefined ||
                    dataObj.sellCurrency.id == null || dataObj.sellCurrency.id == "" || dataObj.sellRoe == undefined ||
                    dataObj.sellRoe == null ||
                    dataObj.sellRoe == "") {
                    console.log("Buy Currency is Mandatory.")
                    if (type == "row") {
                        $scope.secondArr[index].buyCurrencyName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96639"]);
                    }

                    errorArr.push(0);
                }
            } else {

                if (dataObj.buyCurrency.status == 'Block') {
                    console.log("Selected Currency is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].buyCurrencyName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96640"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.buyCurrency.status == 'Hide') {
                    console.log("Selected Currency is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].buyCurrencyName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96641"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].buyCurrencyName = false;

                    }

                    errorArr.push(1);
                }


            }


            // buy Roe

            if (dataObj.buyRoe == undefined ||
                dataObj.buyRoe == null ||
                dataObj.buyRoe == "") {
                if (dataObj.sellCurrency == null || dataObj.sellCurrency == undefined || dataObj.sellCurrency.id == undefined ||
                    dataObj.sellCurrency.id == null || dataObj.sellCurrency.id == "" || dataObj.sellRoe == undefined ||
                    dataObj.sellRoe == null ||
                    dataObj.sellRoe == "") {
                    if (type == "row") {
                        $scope.secondArr[index].buyRoe = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96642"]);
                    }
                    errorArr.push(0);
                }
            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_ROE, dataObj.buyRoe)) {
                if (type == "row") {
                    $scope.secondArr[index].buyRoe = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96644"]);
                }
                errorArr.push(0);

            } else if (dataObj.buyCurrency != null && $rootScope.userProfile.selectedUserLocation.currencyMaster.id == dataObj.buyCurrency.id) {
                if (dataObj.buyRoe > 1) {

                    $scope.secondArr[index].buyRoe = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40017"]);
                    errorArr.push(0);
                }

            } else if (dataObj.buyCurrency != null && $rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.buyCurrency.id) {
                if (dataObj.buyRoe == 1) {

                    $scope.secondArr[index].buyRoe = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40018"]);
                    errorArr.push(0);
                }

            } else {
                if (type == "row") {
                    $scope.secondArr[index].buyRoe = false;

                }

                errorArr.push(1);
            }


            // buyAmountPerUnit
            if (!isNaN(dataObj.buyAmountPerUnit) && dataObj.buyAmountPerUnit != undefined &&
                dataObj.buyAmountPerUnit != null &&
                dataObj.buyAmountPerUnit != "") {


                if (isNaN(parseFloat(dataObj.buyAmountPerUnit))) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96647"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" &&
                    !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.buyAmountPerUnit)) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96647"]);
                    }
                    errorArr.push(0);
                } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.buyAmountPerUnit)) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96647"]);
                    }
                    errorArr.push(0);
                } else if (dataObj.amount == 0) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96646"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.buyAmountPerUnit < 0 || dataObj.buyAmountPerUnit >= 99999999999.999) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmountPerUnit = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96647"]);
                    }
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmountPerUnit = false;

                    }
                    errorArr.push(1);
                }
            }



            // buyAmount
            if (!isNaN(dataObj.buyAmount) && dataObj.buyAmount != undefined &&
                dataObj.buyAmount != null &&
                dataObj.buyAmount != "") {

                if (isNaN(parseFloat(dataObj.buyAmount))) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96650"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" &&
                    !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.buyAmount)) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96650"]);
                    }
                    errorArr.push(0);
                } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.buyAmount)) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96650"]);
                    }
                    errorArr.push(0);
                } else if (dataObj.buyAmount == 0) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96649"]);
                    }
                    errorArr.push(0);

                } else if (dataObj.buyAmount < 0 || dataObj.buyAmount >= 99999999999.999) {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96650"]);
                    }
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].buyAmount = false;

                    }
                    errorArr.push(1);
                }
            }



            // buyLocalAmount
            if (!isNaN(dataObj.buyLocalAmount) && dataObj.buyLocalAmount != undefined &&
                dataObj.buyLocalAmount != null &&
                dataObj.buyLocalAmount != "") {


                if (isNaN(parseFloat(dataObj.buyLocalAmount))) {
                    if (type == "row") {
                        $scope.secondArr[index].buyLocalAmount = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96653"]);
                    }
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].buyLocalAmount = false;

                    }
                    errorArr.push(1);
                }
            }




            // billToParty




            if (dataObj.billToParty == null || dataObj.billToParty.id == undefined ||
                dataObj.billToParty.id == null || dataObj.billToParty.id == "") {

            } else {

                if (dataObj.billToParty.status == 'Block') {
                    console.log("Selected billToParty is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].billToParty = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96672"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.billToParty.status == 'Hide') {
                    console.log("Selected billToParty is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].billToParty = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96673"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].billToParty = false;

                    }

                    errorArr.push(1);
                }


            }

            // vendorSubledger

            if (dataObj.vendorSubledger == null || dataObj.vendorSubledger.id == undefined ||
                dataObj.vendorSubledger.id == null || dataObj.vendorSubledger.id == "") {

            } else {

                if (dataObj.vendorSubledger.status == 'Block') {
                    console.log("Selected vendorSubledger is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].vendorSubledger = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96674"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.vendorSubledger.status == 'Hide') {
                    console.log("Selected vendorSubledger is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].vendorSubledger = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96674"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].vendorSubledger = false;

                    }

                    errorArr.push(1);
                }


            }

            // sellDescription

            if (dataObj.sellDescription == undefined ||
                dataObj.sellDescription == null ||
                dataObj.sellDescription == "") {

            } else if (dataObj.sellDescription.length > 4000) {
                if (type == "row") {
                    $scope.secondArr[index].sellDescription = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96659"]);
                }
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].sellDescription = false;

                }

                errorArr.push(1);
            }



            // buyDescription

            if (dataObj.buyDescription == undefined ||
                dataObj.buyDescription == null ||
                dataObj.buyDescription == "") {

            } else if (dataObj.buyDescription.length > 4000) {
                if (type == "row") {
                    $scope.secondArr[index].buyDescription = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96661"]);
                }
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].buyDescription = false;

                }

                errorArr.push(1);
            }


            // reference

            if (dataObj.reference == undefined ||
                dataObj.reference == null ||
                dataObj.reference == "") {

            } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_REFERENCE_NO, dataObj.reference)) {
                if (type == "row") {
                    $scope.secondArr[index].reference = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96671"]);
                }
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].reference = false;

                }

                errorArr.push(1);
            }



            //referenceType


            if (dataObj.referenceType == null || dataObj.referenceType.id == undefined ||
                dataObj.referenceType.id == null || dataObj.referenceType.id == "") {

            } else {

                if (dataObj.referenceType.status == 'Block') {
                    console.log("Selected referenceType is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].referenceType = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96663"]);
                    }

                    errorArr.push(0);

                } else if (dataObj.referenceType.status == 'Hide') {
                    console.log("Selected referenceType is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].referenceType = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR96664"]);
                    }

                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].referenceType = false;

                    }

                    errorArr.push(1);
                }


            }




            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }

        };

        // End validation

        //call validate


        $scope.callValidateProvisionalItem = function() {
            var isValid = true;
            var index = $scope.provisional.provisionalItemList.length;
            var fineArr = [];
            $scope.secondArr = [];
            angular.forEach($scope.provisional.provisionalItemList, function(dataObj, index) {
                $scope.secondArr[index] = {};
                var validResult = validateProvisionItem(dataObj, index, "row");
                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArr[index].errRow = false;

                } else {
                    $scope.secondArr[index].errRow = true;
                    fineArr.push(0);
                    isValid = false;
                }

            });

            return isValid;
        }


        //add Prov item mapping row
        $scope.addNewProvisionalItem = function() {

            var index = $scope.provisional.provisionalItemList.length;
            var fineArr = [];
            $scope.secondArr = [];
            angular.forEach($scope.provisional.provisionalItemList, function(dataObj, index) {
                $scope.secondArr[index] = {};
                var validResult = validateProvisionItem(dataObj, index, "row");
                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArr[index].errRow = false;
                    $scope.netCalculation();
                } else {
                    $scope.secondArr[index].errRow = true;
                    fineArr.push(0);
                }
                //console.log();
            });
            if (fineArr.indexOf(0) < 0) {
                $scope.provItem = {};
                if ($scope.serviceList != null && $scope.serviceList != undefined && $scope.serviceList.length == 1) {
                    if ($scope.provisional.shipmentUid == null) {
                        $scope.provItem.shipmentService = "";
                    } else {
                        $scope.provItem.shipmentService = $scope.serviceList[0];
                    }


                }

                $scope.provisional.provisionalItemList.push($scope.provItem);
                $scope.secondArr.push({
                    "errRow": false
                });
                $scope.indexFocus = null;
                if ($scope.tableState) {
                    $scope.tableState({
                        param: true
                    });
                }
            } else {
                if ($scope.tableState) {
                    $scope.tableState({
                        param: false
                    });
                }
            }
        };


        //show errors
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'errorPopUp.html',
            show: false,
            keyboard: true
        });
        var errorOnRowIndex = null;
        $scope.errorShow = function(errorObj, index) {

            $scope.errList = errorObj.errTextArr;
            // Show when some event occurs (use $promise property to ensure the template has been loaded)
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };




        // History Start

        $scope.getProvisionalById = function(provisionalId) {
            console.log("getProvisionalById ", provisionalId);

            ProvisionalView.get({
                id: provisionalId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Successful while getting charge.", data)
                    $scope.provisional = data.responseObject;
                }

            }, function(error) {
                console.log("Error while getting charge.", error)
            });

        }


        //On leave the Unfilled provisional Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

        $scope.$on('addProvisionalEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.provisional;
            localStorage.serviceList = JSON.stringify($scope.serviceList);
            $rootScope.category = "Master";
            $rootScope.unfinishedFormTitle = "Provisional (New)";
            if ($scope.provisional != undefined && $scope.provisional != null) {
                $rootScope.subTitle = ""; //$scope.provisional;
            } else {
                $rootScope.subTitle = "Unknown Provisional"
            }
        })

        $scope.$on('editProvisionalEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.provisional;
            localStorage.serviceList = JSON.stringify($scope.serviceList);
            $rootScope.category = "Master";
            $rootScope.unfinishedFormTitle = "Provisional Edit";
            if ($scope.provisional != undefined && $scope.provisional != null) {
                $rootScope.subTitle = ""; //$scope.provisional;
            } else {
                $rootScope.subTitle = "Unknown Provisional"
            }
        })


        $scope.$on('addProvisionalEventReload', function(e, confirmation) {
            console.log("addProvisionalEventReload is called ", $scope.provisional);
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.provisional);
            localStorage.serviceList = JSON.stringify($scope.serviceList);
            localStorage.isProvisionalReloaded = "YES";
            console.log("addProvisionalEventReload is called ", localStorage.reloadFormData);

            e.preventDefault();
        });


        $scope.$on('editProvisionalEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.provisional);
            localStorage.serviceList = JSON.stringify($scope.serviceList);
            localStorage.isProvisionalReloaded = "YES";
            e.preventDefault();
        });


        $scope.isReloaded = localStorage.isProvisionalReloaded;

        if ($stateParams.action == "CREATE") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    localStorage.isProvisionalReloaded = "NO";
                    $scope.provisional = JSON.parse(localStorage.reloadFormData);
                    $scope.serviceList = JSON.parse(localStorage.serviceList);
                } else {
                    console.log("Add History NotReload...")
                    $scope.provisional = $rootScope.selectedUnfilledFormData;
                    $scope.serviceList = JSON.parse(localStorage.serviceList);


                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isProvisionalReloaded = "NO";
                    $scope.provisional = JSON.parse(localStorage.reloadFormData);
                    $scope.serviceList = JSON.parse(localStorage.serviceList);
                    console.log("Add NotHistory Reload...");
                    if (($scope.provisional == undefined || $scope.provisional == null) ||
                        ($scope.provisional.shipmentUid == null ||
                            $scope.provisional.shipmentUid == undefined ||
                            $scope.provisional.shipmentUid == "") &&
                        ($scope.provisional.masterUid == null ||
                            $scope.provisional.masterUid == undefined ||
                            $scope.provisional.masterUid == ""))

                    {
                        $scope.callAsynchronous();
                    }
                } else {
                    console.log("Add NotHistory NotReload...")
                    $scope.init();
                }
            }

            $rootScope.breadcrumbArr = [{
                    label: "Finance",
                    state: "layout.provisionalCost"
                },
                {
                    label: "Provisional",
                    state: "layout.provisionalCost"
                }, {
                    label: "Add Provisional",
                    state: null
                }
            ];
        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isProvisionalReloaded = "NO";
                    $scope.provisional = JSON.parse(localStorage.reloadFormData);
                    $scope.serviceList = JSON.parse(localStorage.serviceList);
                } else {
                    $scope.provisional = $rootScope.selectedUnfilledFormData;
                    $scope.serviceList = JSON.parse(localStorage.serviceList);
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isProvisionalReloaded = "NO";
                    $scope.provisional = JSON.parse(localStorage.reloadFormData);
                    $scope.serviceList = JSON.parse(localStorage.serviceList);
                } else {
                    $scope.getProvisionalById($stateParams.provisionalId);
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Finance",
                    state: "layout.provisionalCost"
                },
                {
                    label: "Provisional",
                    state: "layout.provisionalCost"
                }, {
                    label: "Edit Provisional",
                    state: null
                }
            ];
        }



        // History End



        $scope.selectedBillToParty = function(obj) {
            $scope.errorMap = new Map();


            $rootScope.navigateToNextField(obj);

        };

        /* billToPartyList Select Picker */
        $scope.ajaxBillToPartyEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.billToPartyList = data.responseObject.searchResult;
                        return $scope.billToPartyList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching billToPartyList');
                }
            );

        }


        $scope.selectedVendor = function(obj) {
            $scope.errorMap = new Map();


            $rootScope.navigateToNextField(obj);

        };

        /* VendorList Select Picker */
        $scope.ajaxVendorEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.VendorList = data.responseObject.searchResult;
                        return $scope.VendorList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching VendorList');
                }
            );

        }

        $scope.partyRender = function(item) {
            return {
                label: item.partyName,
                item: item
            }

        }


        $scope.chargeCalculation = function(index) {


            if (index != null) {
                $scope.provisionalItem = $scope.provisional.provisionalItemList[index];
            }

            if ($scope.provisionalItem != null &&
                $scope.provisionalItem.sellCurrency != null &&
                $scope.provisionalItem.sellCurrency != undefined) {

                //sale roe
                if ($scope.provisionalItem.sellCurrency.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                    $scope.provisionalItem.sellRoe = 1;
                }
                //sale amount calculation
                if ($scope.provisionalItem.noOfUnits != null &&
                    $scope.provisionalItem.noOfUnits != undefined &&
                    $scope.provisionalItem.sellAmountPerUnit != null &&
                    $scope.provisionalItem.sellAmountPerUnit != undefined &&
                    isNaN($scope.provisionalItem.sellAmountPerUnit) == false &&
                    isNaN($scope.provisionalItem.noOfUnits) == false) {



                    $scope.provisionalItem.sellAmount = $scope.provisionalItem.noOfUnits *
                        $scope.provisionalItem.sellAmountPerUnit;
                    $scope.provisionalItem.sellCurrency
                    $scope.provisionalItem.sellAmount = $rootScope.currency($scope.provisionalItem.sellCurrency, parseFloat($scope.provisionalItem.sellAmount));
                }

                // Sale local amount calculation

                if ($scope.provisionalItem.sellRoe != null &&
                    $scope.provisionalItem.sellRoe != undefined &&
                    $scope.provisionalItem.sellAmount != null &&
                    $scope.provisionalItem.sellAmount != undefined && isNaN($scope.provisionalItem.sellAmount) == false &&
                    isNaN($scope.provisionalItem.sellRoe) == false) {
                    $scope.provisionalItem.sellLocalAmount = $scope.provisionalItem.sellAmount * $scope.provisionalItem.sellRoe;
                    $scope.provisionalItem.sellLocalAmount = $rootScope.currency($scope.provisional.localCurrency, parseFloat($scope.provisionalItem.sellLocalAmount));
                }

            }

            if ($scope.provisionalItem != null &&
                $scope.provisionalItem.buyCurrency != null &&
                $scope.provisionalItem.buyCurrency != undefined) {
                //buy roe
                if ($scope.provisionalItem.buyCurrency.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                    $scope.provisionalItem.buyRoe = 1;
                }

                //buy amount calculation
                if ($scope.provisionalItem.noOfUnits != null &&
                    $scope.provisionalItem.noOfUnits != undefined &&
                    $scope.provisionalItem.buyAmountPerUnit != null &&
                    $scope.provisionalItem.buyAmountPerUnit != undefined && isNaN($scope.provisionalItem.buyAmountPerUnit) == false &&
                    isNaN($scope.provisionalItem.noOfUnits) == false) {
                    $scope.provisionalItem.buyAmount = $scope.provisionalItem.noOfUnits *
                        $scope.provisionalItem.buyAmountPerUnit;
                    $scope.provisionalItem.buyAmount = $rootScope.currency($scope.provisionalItem.buyCurrency, parseFloat($scope.provisionalItem.buyAmount));
                }

                // Buy local amount calculation
                if ($scope.provisionalItem.buyRoe != null &&
                    $scope.provisionalItem.buyRoe != undefined &&
                    $scope.provisionalItem.buyAmount != null &&
                    $scope.provisionalItem.buyAmount != undefined && isNaN($scope.provisionalItem.buyAmount) == false &&
                    isNaN($scope.provisionalItem.buyRoe) == false) {
                    $scope.provisionalItem.buyLocalAmount = $scope.provisionalItem.buyAmount *
                        $scope.provisionalItem.buyRoe;
                    $scope.provisionalItem.buyLocalAmount = $rootScope.currency($scope.provisional.localCurrency, parseFloat($scope.provisionalItem.buyLocalAmount));
                }

            }

            $scope.netCalculation();
        }



        $scope.setDeafultValues = function(charge) {

            if (charge.chargeMaster != undefined && charge.chargeMaster != null && charge.chargeMaster != "") {

                if (charge.chargeMaster.calculationType != undefined && charge.chargeMaster.calculationType != null) {
                    if (charge.chargeMaster.calculationType == 'Percentage') {
                        UnitMasterGetByCode.get({
                            unitCode: 'PER'
                        }).$promise.then(function(resObj) {
                            if (resObj.responseCode == "ERR0") {
                                charge.unitMaster = resObj.responseObject;
                            }
                        }, function(error) {
                            console.log("unit Get Failed : " + error)
                        });

                    }
                    if (charge.chargeMaster.calculationType == 'Shipment') {
                        UnitMasterGetByCode.get({
                            unitCode: 'SHPT'
                        }).$promise.then(function(resObj) {
                            if (resObj.responseCode == "ERR0") {
                                charge.unitMaster = resObj.responseObject;
                            }
                        }, function(error) {
                            console.log("unit Get Failed : " + error)
                        });

                    }
                    if (charge.chargeMaster.calculationType == 'Unit') {
                        UnitMasterGetByCode.get({
                            unitCode: 'KG'
                        }).$promise.then(function(resObj) {
                            if (resObj.responseCode == "ERR0") {
                                charge.unitMaster = resObj.responseObject;
                            }
                        }, function(error) {
                            console.log("unit Get Failed : " + error)
                        });

                    }
                    if (charge.chargeMaster.calculationType == 'Document') {
                        UnitMasterGetByCode.get({
                            unitCode: 'DOC'
                        }).$promise.then(function(resObj) {
                            if (resObj.responseCode == "ERR0") {
                                charge.unitMaster = resObj.responseObject;
                            }
                        }, function(error) {
                            console.log("unit Get Failed : " + error)
                        });
                    }
                }

            } else {

                charge.unitMaster = "";
            }
        }

        $scope.serviceRender = function(item) {
            return {
                label: item.serviceUid,
                item: item
            }
        }

        $scope.findServiceChargeTaxCategoryMapping = function(index) {
            if (index != null) {
                $scope.provisionalItem = $scope.provisional.provisionalItemList[index];
            }

            if ($scope.provisionalItem.shipmentService != null && $scope.provisionalItem.shipmentService != undefined &&
                $scope.provisionalItem.shipmentService != '') {
                if ($scope.provisionalItem.shipmentService.serviceMaster != null &&
                    $scope.provisionalItem.shipmentService.serviceMaster != undefined &&
                    $scope.provisionalItem.shipmentService.serviceMaster != '' && $scope.provisionalItem.chargeMaster != null &&
                    $scope.provisionalItem.chargeMaster != undefined && $scope.provisionalItem.chargeMaster != '') {

                    InvoiceServiceTaxPercentage.get({
                            serviceId: $scope.provisionalItem.shipmentService.serviceMaster.id,
                            chargeId: $scope.provisionalItem.chargeMaster.id,
                            countryId: $rootScope.userProfile.selectedUserLocation.countryMaster.id,
                            companyId: $rootScope.userProfile.selectedCompany.id

                        },
                        function(data) {
                            if (data.responseCode == 'ERR0') {

                                console.log(" Successfull", data.responseObject);
                                $scope.serviceTaxPercentageList = data.responseObject.taxPercentageList;
                                if ($scope.serviceTaxPercentageList != null && $scope.serviceTaxPercentageList != undefined &&
                                    $scope.serviceTaxPercentageList != '' && $scope.serviceTaxPercentageList.length > 0) {

                                    $scope.provisionalItem.isTaxable = true;
                                } else {
                                    $scope.provisionalItem.isTaxable = false;

                                }

                            } else {

                                console.log(" Failed " + data.responseDescription);


                                $scope.provisionalItem.isTaxable = false;
                            }
                        },
                        function(error) {
                            console.log(" Failed : " + error)
                        });

                }
            }
        }

    }]);