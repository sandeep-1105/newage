/**
 * 
 */
app.factory('ProvisionalService',[ function() {
	
	var savedData = {}
	var savedConsol = {}
	var savedShipment = {}
	var sevedShipmentServiceDetail={}
	
	var tabVal = null;
	
	var loc=null;
	
	var requestPage = null;
	
	
	
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
	}
	
	function setTabVal(tab) {
		tabVal = tab;
	}
	
	function getTabVal() {
		return tabVal;
	}
	
	function setShipmentServiceDetail(dataShipmentServiceDetail) {
		sevedShipmentServiceDetail = dataShipmentServiceDetail;
		localStorage.savedShipmentServiceDetail=JSON.stringify(sevedShipmentServiceDetail);
	}
	
	function getShipmentServiceDetail() {
		savedShipmentServiceDetail = localStorage.savedShipmentServiceDetail==null?null:JSON.parse(localStorage.savedShipmentServiceDetail);
		return savedShipmentServiceDetail;
	}
	
	
	function setConsol(dataConsol) {
		savedConsol = dataConsol;
		localStorage.savedConsol=JSON.stringify(savedConsol);
	}
	
	function getConsol() {
		savedConsol = localStorage.savedConsol==null?null:JSON.parse(localStorage.savedConsol);
		return savedConsol;
	}
	
	function setShipment(dataShipment) {
		savedShipment = dataShipment;
		localStorage.savedShipment=JSON.stringify(savedShipment);
	}
	
	function getShipment() {
		savedShipment = localStorage.savedShipment==null?null:JSON.parse(localStorage.savedShipment);
		return savedShipment;
	}

	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}
	
	

	return {
		set : set,
		get : get,
		setLoc : setLoc,
		getLoc : getLoc,
		setConsol : setConsol,
		getConsol : getConsol,
		setTabVal : setTabVal,
		getTabVal : getTabVal,
		setRequestPage : setRequestPage,
		getRequestPage : getRequestPage,
		setShipment : setShipment,
		getShipment : getShipment,
		setShipmentServiceDetail : setShipmentServiceDetail,
		getShipmentServiceDetail : getShipmentServiceDetail,
	}

	function setLoc(data) {
		loc = data;
	}
	
	function getLoc() {
		return loc;
	}
	
	function setRequestPage(rp) {
		requestPage = rp;
	}
	
	function getRequestPage() {
		return requestPage;
	}

}]);





/*app.factory('ProvisionalService', function() {
	var savedData = {}
	var loc=null;
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
	}
	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}

	return {
		set : set,
		get : get,
		setLoc : setLoc,
		getLoc : getLoc,
	}
	
	function setLoc(data) {
		loc = data;
	}
	function getLoc() {
		
	return loc;
	}
	
});*/