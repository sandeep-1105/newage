(function() {
	
	app.factory("ProvisionalSearch",['$resource', function($resource) {
		return $resource("/api/v1/provisional/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ProvisionalSave",['$resource', function($resource) {
		return $resource("/api/v1/provisional/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ProvisionalEdit",['$resource', function($resource) {
		return $resource("/api/v1/provisional/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ProvisionalView",['$resource', function($resource) {
		return $resource("/api/v1/provisional/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("ProvisionalViewByConsolUid",['$resource', function($resource) {
		return $resource("/api/v1/provisional/get/masteruid/:masteruid", {}, {
			get : {
				method : 'GET',
				params : {
					masteruid : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	
	app.factory("ProvisionalViewByShipmentServiceUid",['$resource', function($resource) {
		return $resource("/api/v1/provisional/get/serviceuid/:serviceuid", {}, {
			get : {
				method : 'GET',
				params : {
					serviceuid : ''
				},
				isArray : false
			}
		});
	}]);
	
	

	app.factory("ProvisionalRemove",['$resource', function($resource) {
		return $resource("/api/v1/provisional/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);	

	
})();
