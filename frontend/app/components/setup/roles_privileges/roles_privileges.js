app.controller('rolesCtrl',['$rootScope', '$scope','$http', '$location', '$state', '$window', '$stateParams', 
		'ngDialog', 'RoleView', 'RoleSearch', 'RoleRemove', 'SearchRoleByKey', 'roleConstant', 'Notification', 
	function($rootScope, $scope, $http, $location, $state, $window, $stateParams, 
		ngDialog, RoleView, RoleSearch, RoleRemove, SearchRoleByKey, roleConstant, Notification) {

    var tree;
    var rawTreeData = []


    $scope.my_tree_handler = function(branch) {
        console.log('you clicked on', branch)
    }

    function getTree(data, primaryIdName, parentIdName) {
        if (!data || data.length == 0 || !primaryIdName || !parentIdName)
            return [];

        var tree = [],
            rootIds = [],
            item = data[0],
            primaryKey = item[primaryIdName],
            treeObjs = {},
            parentId,
            parent,
            len = data.length,
            i = 0;

        while (i < len) {
            item = data[i++];
            primaryKey = item[primaryIdName];
            treeObjs[primaryKey] = item;
            parentId = item[parentIdName];

            if (parentId) {
                parent = treeObjs[parentId];
                if (parent != undefined) {
                    if (parent.children) {
                        parent.children.push(item);
                    } else {
                        parent.children = [item];
                    }
                }
            } else {
                rootIds.push(primaryKey);
            }
        }

        for (var i = 0; i < rootIds.length; i++) {
            tree.push(treeObjs[rootIds[i]]);
        };

        return tree;
    }

    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_VIEW)) {
            $rootScope.mainpreloder = true;
            $scope.showHistory = false;
            RoleView.get({
                    id: data.id
                },
                function(data) {
                    if (data.responseCode == 'ERR0') {

                        $scope.roleDto = data.responseObject;

                        rawTreeData = data.responseObject.featuresList;

                        var myTreeData = getTree(rawTreeData, 'id', 'parentId');

                        $scope.tree_data = myTreeData;
                        $scope.my_tree = tree = {};
                        $scope.expanding_property = "featureName";
                        $scope.col_defs = [{
                                field: "valList",
                                show: "list",
                                displayName: "List",
                                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                                cellTemplateScope: {
                                    click: function(obj, col) { // this works too: $scope.someMethod;

                                    }
                                }
                            },
                            {
                                field: "valCreate",
                                show: "create",
                                displayName: "Create",
                                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                                cellTemplateScope: {
                                    click: function(obj, col) { // this works too: $scope.someMethod;

                                    }
                                }
                            },
                            {
                                field: "valModify",
                                show: "modify",
                                displayName: "Modify",
                                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                                cellTemplateScope: {
                                    click: function(obj, col) { // this works too: $scope.someMethod;

                                    }
                                }
                            },
                            {
                                field: "valView",
                                show: "view",
                                displayName: "View",
                                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                                cellTemplateScope: {
                                    click: function(obj, col) { // this works too: $scope.someMethod;

                                    }
                                }
                            },
                            {
                                field: "valDelete",
                                show: "delete",
                                displayName: "Delete",
                                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                                cellTemplateScope: {
                                    click: function(obj, col) { // this works too: $scope.someMethod;

                                    }
                                }
                            },
                            {
                                field: "valDownload",
                                show: "download",
                                displayName: "Download",
                                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                                cellTemplateScope: {
                                    click: function(obj, col) { // this works too: $scope.someMethod;

                                    }
                                }
                            }
                        ];

                        $scope.treeLoaded = true;
                        $rootScope.mainpreloder = false;
                        $rootScope.breadcrumbArr = [{
                            label: "Setup",
                            state: "layout.rolesPrivilege"
                        }, {
                            label: "Roles & Privileges",
                            state: "layout.rolesPrivilege"
                        }, {
                            label: "View Roles & Privileges",
                            state: null
                        }];

                    } else {
                        console.log("Role loaded Failed " + data.responseDescription);
                        $rootScope.mainpreloder = false;

                    }
                },
                function(error) {
                    console.log("Role loaded Failed : " + error);
                    $rootScope.mainpreloder = false;

                });
        }
    }


    $scope.init = function() {
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.actClick('FALSE');
        $scope.search();
    }

    $scope.rolesHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false,
        },
        {
            "name": "Name",
            "width": "col-xs-3",
            "model": "roleName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchRoleName"

        },
        {
            "name": "Description",
            "width": "col-xs-4half",
            "model": "description",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchDescription"
        },
        {
            "name": "Status",
            "width": "col-xs-2half",
            "model": "status",
            "search": true,
            "type": "drop",
            "data": $rootScope.enum['LovStatus'],
            "sort": true,
            "key": "searchStatus"
        }
    ];

    $scope.sortSelection = {
        sortKey: "roleName",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.changeSearch = function(param) {

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.search = function() {
        $scope.treeLoaded = false;
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.rolesArr = [];

        SearchRoleByKey.query($scope.searchDto).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;

            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;

            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.rolesArr = resultArr;
        });
        $rootScope.breadcrumbArr = [{
            label: "Setup",
            state: "layout.rolesPrivilege"
        }, {
            label: "Roles & Privileges",
            state: "layout.rolesPrivilege"
        }];
    }

    $scope.clickOnTabView = function(tab) {
        if (tab == 'privileges' && $rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_PRIVILEGES_VIEW)) {
            $scope.Tab = tab;
        }
    }

    $scope.serialNo = function(index) {
        index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
            (index + 1);
        return index;
    }


    $scope.deleteRole = function() {

        if ($rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR98008"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                RoleRemove.remove({
                        id: $scope.roleDto.id
                    },
                    function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("Role deleted Successfully")
                            Notification.success($rootScope.nls["ERR402"]);
                            $scope.search();
                        } else {
                            console.log("Role deleted Failed " + data.responseDescription)
                        }
                    },
                    function(error) {
                        console.log("Role deleted Failed : " + error)
                    });

                $scope.page = 0;
                $scope.search();
            }, function(value) {
                console.log("deleted cancelled");
            });

        }
    }


    $scope.cancel = function() {
        $scope.treeLoaded = false;
        $scope.actClick('FALSE');
        $scope.search();
    }

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_CREATE)) {
            $state.go("layout.addRolesPrivilege");
        }
    };
    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_MODIFY)) {
            $state.go("layout.editRolesPrivilege", {
                id: $scope.roleDto.id
            });
        }
    };

    $scope.actClick = function(boolean) {
        if (boolean === 'TRUE') {
            $scope.showHistory = true;
        } else {
            $scope.showHistory = false;
        }

    }
    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.rolesPrivilege"
            }, {
                label: "Roles & Privileges",
                state: "layout.rolesPrivilege"
            }];
            break;
    }
}]);