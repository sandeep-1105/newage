
 (function() {
	 
	 app.factory("FeatureList",['$resource', function($resource) {
			return $resource("/api/v1/role/get/feature/all", {}, {
				get : {
					method : 'GET',
					
					isArray : false
				}
			});
		}]);
	 
	 app.factory("RoleSearch",['$resource', function($resource) {
			return $resource("/api/v1/role/get/search/all", {}, {
				get : {
					method : 'GET',
					isArray : false
				}
			});
		}]);
	 app.factory("RoleSearchKeyword",['$resource', function($resource) {
			return $resource("/api/v1/role/get/search/keyword", {}, {
				query : {
					method : 'POST',
					isArray : false
				}
			});
		}]);
	 
	 
	 
	 app.factory("AddRole",['$resource', function($resource) {
			return $resource("/api/v1/role/add/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
	 app.factory("RoleDataForm",['$resource', function($resource) {
			return $resource("/api/v1/role/roleform/", {}, {
				query : {
					method : 'POST',
					isArray : false
				}
			});
		}]);
		app.factory("RoleAdd",['$resource', function($resource) {
			return $resource("/api/v1/role/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);

		app.factory("RoleEdit",['$resource', function($resource) {
			return $resource("/api/v1/role/update", {}, {
				update : {
					method : 'POST'
				}
			});
		}]);

		app.factory("RoleView",['$resource', function($resource) {
			return $resource("/api/v1/role/get/id/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		

		app.factory("RoleRemove",['$resource', function($resource) {
			return $resource("/api/v1/role/delete/:id", {}, {
				remove : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		
		app.factory("RoleList",['$resource', function($resource) {
			return $resource("/api/v1/role/get/search/keyword", {}, {
				fetch : {
					method : 'POST'
				}
			});
		}]);
		
		app.factory("SearchRoleByKey",['$resource', function($resource) {
			return $resource("/api/v1/role/get/search", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
	 
 })();

