/**
 * Created by hmspl on 8/7/16.
 */
app.controller("rols_edit_Ctrl",['$rootScope', '$scope', '$location', 'ngDialog', 'RoleService', '$http', '$modal',
    'RoleAdd', 'RoleEdit', 'ngProgressFactory', 'cloneService', 'AddRole', 'UserList', '$state', '$stateParams', 
    'RoleSearchKeyword', 'RoleDataForm', 'roleConstant', 'Notification',
	function($rootScope, $scope, $location, ngDialog, RoleService, $http, $modal,
    RoleAdd, RoleEdit, ngProgressFactory, cloneService, AddRole, UserList, $state, $stateParams, 
    RoleSearchKeyword, RoleDataForm, roleConstant, Notification) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('role-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.treeLoaded = false;
    var tree;
    var rawTreeData = []


    $scope.my_tree_handler = function(branch) {
        console.log('you clicked on', branch)
    }

    function getTree(data, primaryIdName, parentIdName) {
        if (!data || data.length == 0 || !primaryIdName || !parentIdName)
            return [];

        var tree = [],
            rootIds = [],
            item = data[0],
            primaryKey = item[primaryIdName],
            treeObjs = {},
            parentId,
            parent,
            len = data.length,
            i = 0;

        while (i < len) {
            item = data[i++];
            primaryKey = item[primaryIdName];
            treeObjs[primaryKey] = item;
            parentId = item[parentIdName];

            if (parentId) {
                parent = treeObjs[parentId];
                if (parent != undefined) {
                    if (parent.children) {
                        parent.children.push(item);
                    } else {
                        parent.children = [item];
                    }
                }
            } else {
                rootIds.push(primaryKey);
            }
        }

        for (var i = 0; i < rootIds.length; i++) {
            tree.push(treeObjs[rootIds[i]]);
        };

        return tree;
    }

    $scope.formTreeData = function(object) {
        rawTreeData = object;

        var myTreeData = getTree(rawTreeData, 'id', 'parentId');

        $scope.tree_data = myTreeData;
        $scope.my_tree = tree = {};
        $scope.expanding_property = "featureName";
        $scope.col_defs = [{
                field: "valList",
                show: "list",
                displayName: "List",
                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                cellTemplateScope: {
                    click: function(obj, col) { // this works too: $scope.someMethod;
                        if (obj.valList == null) {
                            obj.valList = 'Yes';
                        } else if (obj.valList != null) {
                            obj.valList = null;
                        }
                    }
                }
            },
            {
                field: "valCreate",
                show: "create",
                displayName: "Create",
                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                cellTemplateScope: {
                    click: function(obj, col) { // this works too: $scope.someMethod;
                        if (obj.valCreate == null) {
                            obj.valCreate = 'Yes';
                        } else if (obj.valCreate != null) {
                            obj.valCreate = null;
                        }
                    }
                }
            },
            {
                field: "valModify",
                show: "modify",
                displayName: "Modify",
                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                cellTemplateScope: {
                    click: function(obj, col) { // this works too: $scope.someMethod;
                        if (obj.valModify == null) {
                            obj.valModify = 'Yes';
                        } else if (obj.valModify != null) {
                            obj.valModify = null;
                        }
                    }
                }
            },
            {
                field: "valView",
                show: "view",
                displayName: "View",
                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                cellTemplateScope: {
                    click: function(obj, col) { // this works too: $scope.someMethod;
                        if (obj.valView == null) {
                            obj.valView = 'Yes';
                        } else if (obj.valView != null) {
                            obj.valView = null;
                        }
                    }
                }
            },
            {
                field: "valDelete",
                show: "delete",
                displayName: "Delete",
                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                cellTemplateScope: {
                    click: function(obj, col) { // this works too: $scope.someMethod;
                        if (obj.valDelete == null) {
                            obj.valDelete = 'Yes';
                        } else if (obj.valDelete != null) {
                            obj.valDelete = null;
                        }
                    }
                }
            },
            {
                field: "valDownload",
                show: "download",
                displayName: "Download",
                cellTemplate: "<i ng-class=\"row.branch[col.field] !=null?'fa fa-check-square-o':'fa fa-square-o'\" ng-click=\"cellTemplateScope.click(row.branch, col.field)\" />",
                cellTemplateScope: {
                    click: function(obj, col) { // this works too: $scope.someMethod;
                        if (obj.valDownload == null) {
                            obj.valDownload = 'Yes';
                        } else if (obj.valDownload != null) {
                            obj.valDownload = null;
                        }
                    }
                }
            }
        ];

        $scope.treeLoaded = true;
    }

    $scope.getByRole = function(id) {
        AddRole.get({
                id: id
            },
            function(data) {

                if (data.responseCode == "ERR0") {

                    $scope.roleDto = data.responseObject;

                    $scope.formTreeData(data.responseObject.featuresList);
                }

            },
            function(error) {
                console.error('Error while fetching featureList');
            });

    }

    $scope.clickOnTabView = function(id, tab) {

        if (id != null && id != undefined) {
            if (tab == 'privileges' && $rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_PRIVILEGES_MODIFY)) {
                $scope.Tab = tab;
            }

        } else {
            if (tab == 'privileges' && $rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_PRIVILEGES_CREATE)) {
                $scope.Tab = tab;
            }
        }

    }

    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateRole(0)) {
            $scope.contained_progressbar.start();
            $rootScope.mainpreloder = true;
            var tmpObj = cloneService.clone($scope.roleDto);
            var i = 0;
            for (i = 0; i < tmpObj.featuresList.length; i++) {
                if (tmpObj.featuresList[i].children != undefined && tmpObj.featuresList[i].children != null) {
                    tmpObj.featuresList[i].children = null;
                }
            }

            RoleEdit.update(tmpObj).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Role updated Successfully");
                        Notification.success($rootScope.nls["ERR401"]);
                        $state.go("layout.rolesPrivilege");
                        $rootScope.mainpreloder = false;
                    } else {
                        console.log("Role updated Failed " + data.responseDescription);
                        $rootScope.mainpreloder = false;
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Role updated Failed : " + error);
                    $rootScope.mainpreloder = false;
                });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.save = function() {

        console.log("Save Method is called.");

        if ($scope.validateRole(0)) {
            $scope.contained_progressbar.start();
            $rootScope.mainpreloder = true;
            var tmpObj = cloneService.clone($scope.roleDto);
            var i = 0;
            for (i = 0; i < tmpObj.featuresList.length; i++) {
                if (tmpObj.featuresList[i].children != undefined && tmpObj.featuresList[i].children != null) {
                    tmpObj.featuresList[i].children = null;
                }

            }

            tmpObj.status = 'Active';

            RoleAdd.save(tmpObj).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    Notification.success($rootScope.nls["ERR400"]);
                    $state.go("layout.rolesPrivilege");
                    $rootScope.mainpreloder = false;
                } else {
                    console.log("Role added Failed " + data.responseDescription);
                    $rootScope.mainpreloder = false;
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Role added Failed : " + error);
                $rootScope.mainpreloder = false;
            });
        } else {
            console.log("Invalid Form Submission.");
        }

    }

    $scope.cancel = function() {
        $state.go("layout.rolesPrivilege");
    }

    $scope.validateRole = function(validateCode) {
        console.log("Validate Called");
        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.roleDto.roleName == "undefined" ||
                $scope.roleDto.roleName == null ||
                $scope.roleDto.roleName == "") {
                $rootScope.navigateToNextField("roleName");
                $scope.errorMap.put("roleName", $rootScope.nls["ERR98000"]);
                return false;
            } else {

                var regexp = new RegExp("^[0-9a-zA-Z ]{0,100}$");
                if (!regexp
                    .test($scope.roleDto.roleName)) {
                    console.log($rootScope.nls["ERR98001"]);
                    $scope.errorMap.put("roleName", $rootScope.nls["ERR98001"]);
                    return false;
                }

            }
        }




        return true;

    }


    $scope.roleConfig = {
        search: true,
        multiple: true,
        ajax: true,
        modelName: "roleName",
        showCode: false
    }
    $scope.showRoles = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_ROLES_AND_PRIVILEGES_GET_PRIVILEGES_FROM_ROLES_LIST)) {
            $scope.rolePanel = "Select Privileges from Roles"
            $scope.rolesList = true;
            $scope.ajaxRoleList("");
        }
    }
    $scope.cancelRoles = function() {
        $scope.rolesList = false;
    }

    $scope.ajaxRoleList = function(searchText) {
        console.log("searchText :", searchText)
        $scope.roleListArr = [];
        var searchDto = {};
        searchDto.keyword = searchText;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = 10;
        RoleSearchKeyword.query(searchDto).$promise.then(function(
            data, status) {
            $scope.roleListArr = data.responseObject.searchResult;
        });
    }

    $scope.selecteRoles = function(param) {
        console.log(param);

        var dto = {};
        dto.roleDto = $scope.roleDto;
        dto.selectedList = param;

        RoleDataForm.query(dto).$promise.then(function(
            data, status) {
            $scope.roleDto = data.responseObject;
            $scope.rolesList = false;
            $scope.formTreeData(data.responseObject.featuresList);
            if (param != null && param.length > 0) {
                $scope.validateRole(1);
                Notification.success({
                    message: $rootScope.nls["ERR98007"],
                    delay: 5000
                });
            }
        });

    }


    $scope.addAssociate = function() {

        if ($scope.roleDto.userList == undefined ||
            $scope.roleDto.userList == null ||
            $scope.roleDto.userList.length == 0
        ) {
            $scope.roleDto.userList = [];
        }

        $scope.roleDto.userList.push({})
    }

    $scope.removeAssociate = function(index) {
        $scope.roleDto.userList.splice(index, 1);
    }
    $scope.ajaxUserEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        $scope.searchDto.keyword = object == null ? "" : object;

        return UserList.fetch($scope.searchDto).$promise.then(function(
                data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching UserProfile');
            }

        );
    }

    $scope.selectedUser = function(userDto) {
        userDto.id = userDto.user.id;
        userDto.userName = userDto.user.userName;
        userDto.status = userDto.user.status;
    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }

    //On leave the Unfilled Unit Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addRoleEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.roleDto;
        $rootScope.category = "Role Master";
        $rootScope.unfinishedFormTitle = "Role (New)";
        if ($scope.roleDto != undefined && $scope.roleDto != null && $scope.roleDto.roleName != undefined &&
            $scope.roleDto.roleName != null && $scope.roleDto.roleName != "") {
            $rootScope.subTitle = $scope.roleDto.roleName;
        } else {
            $rootScope.subTitle = "Unknown Role"
        }
    })

    $scope.$on('editRoleEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.roleDto;
        $rootScope.category = "Role Master";
        $rootScope.unfinishedFormTitle = "Role Edit " + $scope.roleDto.roleName;
        if ($scope.roleDto != undefined && $scope.roleDto != null && $scope.roleDto.roleName != undefined &&
            $scope.roleDto.roleName != null && $scope.roleDto.roleName != "") {
            $rootScope.subTitle = $scope.roleDto.roleName;
        } else {
            $rootScope.subTitle = "Unknown Role"
        }
    })


    $scope.$on('addRoleEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.roleDto);
        localStorage.isRoleReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editRoleEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.roleDto);
        localStorage.isRoleReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isRoleReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isRoleReloaded = "NO";
                $scope.roleDto = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add History NotReload...")
                $scope.roleDto = $rootScope.selectedUnfilledFormData;
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isRoleReloaded = "NO";
                $scope.roleDto = JSON.parse(localStorage.reloadFormData);
                $scope.oldData = "";
                console.log("Add NotHistory Reload...")
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.getByRole(-1)
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.rolesPrivilege"
            },
            {
                label: "Roles & Privileges",
                state: "layout.rolesPrivilege"
            },
            {
                label: "Add Roles & Privileges",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isRoleReloaded = "NO";
                $scope.roleDto = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.roleDto = $rootScope.selectedUnfilledFormData;
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isRoleReloaded = "NO";
                $scope.roleDto = JSON.parse(localStorage.reloadFormData);
                $scope.oldData = "";
            } else {
                $scope.getByRole($stateParams.id);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.rolesPrivilege"
            },
            {
                label: "Roles & Privileges",
                state: "layout.rolesPrivilege"
            },
            {
                label: "Edit Roles & Privileges",
                state: null
            }
        ];
    }

    // -------------------------------------------------------------------------------

}]);