app.factory('locationMasterFactory', function($resource) {
    return {
        findById: $resource('/api/v1/locationmaster/get/id/:id', {}, {
            query: { method: 'GET', params: { id: '' }, isArray: false }
        }),
        create: $resource('/api/v1/locationmaster/create', {}, {
            query: { method: 'POST' }
        }),
        update: $resource('/api/v1/locationmaster/update', {}, {
            query: { method: 'POST' }
        }),
        delete: $resource('/api/v1/locationmaster/delete/:id', {}, {
            query: { method: 'DELETE', params: { id: '' } }
        }),
        search: $resource('/api/v1/locationmaster/get/search', {}, {
            fetch: { method: 'POST' }
        })
    };
})