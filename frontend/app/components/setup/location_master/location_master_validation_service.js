app.service('locationMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(location, code) {
		//Validate location name
		if(code == 0 || code == 1) {
			if(location.locationName == undefined || location.locationName == null || location.locationName =="" ){
				return this.validationResponse(true, "locationName", $rootScope.nls["ERR3408"], location);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Location_Name,location.locationName)){
                    return this.validationResponse(true, "locationName", $rootScope.nls["ERR3411"], location);
    			}
			}
		}
		//Validate location code
		if(code == 0 || code == 2) {
			if(location.locationCode == undefined || location.locationCode == null || location.locationCode =="" ){
				return this.validationResponse(true, "locationCode", $rootScope.nls["ERR3407"], location);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Location_Code,location.locationCode)){
                    return this.validationResponse(true, "locationCode", $rootScope.nls["ERR3410"], location);
    			}
			}
		}
		
		
		//Validate company master
		if(code == 0 || code == 3) {
			if(location.companyMaster == null || location.companyMaster == undefined || location.companyMaster== "" ){
				return this.validationResponse(true, "companyName", $rootScope.nls["ERR3415"], location);	
			}
			else{
				if(location.companyMaster.status=="Block"){
					location.companyMaster =null;
					return this.validationResponse(true, "companyName", $rootScope.nls["ERR3419"], location);	
				}
				else if(location.companyMaster.status=="Hide"){
					location.companyMaster =null;
					return this.validationResponse(true, "companyName", $rootScope.nls["ERR3420"], location);	
				}
			}
		}
		
		//Validate branch Name
		if(code == 0 || code == 4) {
			if(location.branchName == undefined || location.branchName == null || location.branchName =="" ){
				return this.validationResponse(true, "branchName", $rootScope.nls["ERR3433"], location);
			}
		}
		
		//Validate country master
		if(code == 0 || code == 5) {
			if(location.countryMaster == null || location.countryMaster == undefined || location.countryMaster== "" ){
				return this.validationResponse(true, "countryName", $rootScope.nls["ERR3412"], location);	
			}
			else{
				if(location.countryMaster.status=="Block"){
					location.countryMaster = null;
					return this.validationResponse(true, "countryName", $rootScope.nls["ERR3421"], location);	
				}
				else if(location.countryMaster.status=="Hide"){
					location.countryMaster = null;
					return this.validationResponse(true, "countryName", $rootScope.nls["ERR3422"], location);	
				}
			}
		}
		//Validate addressLine1
		if(code == 0 || code == 6) {
			if(location.addressLine1 == undefined || location.addressLine1 == null || location.addressLine1 =="" ){
				return this.validationResponse(true, "addressLine1", $rootScope.nls["ERR3434"], location);
			}
		}
		
		//Validate addressLine2
		if(code == 0 || code == 7) {
			if(location.addressLine2 == undefined || location.addressLine2 == null || location.addressLine2 =="" ){
				return this.validationResponse(true, "addressLine2", $rootScope.nls["ERR3435"], location);
			}
		}
		
		//Validate addressLine3
		if(code == 0 || code == 8) {
			if(location.addressLine3 == undefined || location.addressLine3 == null || location.addressLine3 =="" ){
				return this.validationResponse(true, "addressLine3", $rootScope.nls["ERR3436"], location);
			}
		}
		
		
		
		//Validate city master
		if(code == 0 || code == 9) {
			if(location.cityMaster == null || location.cityMaster == undefined || location.cityMaster== "" ){
				return this.validationResponse(true, "cityName", $rootScope.nls["ERR3413"], location);	
			}
			else{
				if(location.cityMaster.status=="Block"){
					location.cityMaster = null;
					return this.validationResponse(true, "cityName", $rootScope.nls["ERR3423"], location);	
				}
				else if(location.cityMaster.status=="Hide"){
					location.cityMaster = null;
					return this.validationResponse(true, "cityName", $rootScope.nls["ERR3424"], location);	
				}
			}
		}
		
		//Validate region master
		if(code == 0 || code == 10) {
			if(location.regionMaster == null || location.regionMaster == undefined || location.regionMaster== "" ){
				//return this.validationResponse(true, "regionName", $rootScope.nls["ERR3413"], location);	
			}
			else{
				if(location.regionMaster.status=="Block"){
					location.regionMaster = null;
					return this.validationResponse(true, "regionName", $rootScope.nls["ERR3425"], location);	
				}
				else if(location.regionMaster.status=="Hide"){
					location.regionMaster = null;
					return this.validationResponse(true, "regionName", $rootScope.nls["ERR3426"], location);	
				}
			}
		}
		
		//Validate zone master
		if(code == 0 || code == 11) {
			if(location.zoneMaster == null || location.zoneMaster == undefined || location.zoneMaster== "" ){
				//return this.validationResponse(true, "zoneName", $rootScope.nls["ERR3414"], location);	
			}
			else{
				if(location.zoneMaster.status=="Block"){
					location.zoneMaster = null;
					return this.validationResponse(true, "zoneName", $rootScope.nls["ERR3427"], location);	
				}
				else if(location.zoneMaster.status=="Hide"){
					location.zoneMaster = null;
					return this.validationResponse(true, "zoneName", $rootScope.nls["ERR3428"], location);	
				}
			}
		}
		
		//Validate division master
		if(code == 0 || code == 12) {
			if(location.divisionMaster == null || location.divisionMaster == undefined || location.divisionMaster== "" ){
				//return this.validationResponse(true, "divisionName", $rootScope.nls["ERR3413"], location);	
			}
			else{
				if(location.divisionMaster.status=="Block"){
					location.divisionMaster = null;
					return this.validationResponse(true, "divisionName", $rootScope.nls["ERR3429"], location);	
				}
				else if(location.divisionMaster.status=="Hide"){
					location.divisionMaster = null;
					return this.validationResponse(true, "divisionName", $rootScope.nls["ERR3430"], location);	
				}
			}
		}
		
		//Validate party master
		if(code == 0 || code == 13) {
			if(location.partyMaster == null || location.partyMaster == undefined || location.partyMaster== "" ){
				//return this.validationResponse(true, "agentName", $rootScope.nls["ERR3413"], location);	
			}
			else{
				if(location.partyMaster.status=="Block"){
					location.partyMaster=null;
					return this.validationResponse(true, "agentName", $rootScope.nls["ERR3431"], location);	
				}
				else if(location.partyMaster.status=="Hide"){
					location.partyMaster=null;
					return this.validationResponse(true, "agentName", $rootScope.nls["ERR3432"], location);	
				}
			}
		}
		
		//Validate timezone
		if(code == 0 || code == 14) {
			if(location.timeZone == undefined || location.timeZone == null || location.timeZone =="" ){
				return this.validationResponse(true, "timeZone", $rootScope.nls["ERR3437"], location);
			}
		}
		
		//Validate date format
		if(code == 0 || code == 14) {
			if(location.dateFormat == undefined || location.dateFormat == null || location.dateFormat =="" ){
				return this.validationResponse(true, "dateFormat", $rootScope.nls["ERR3439"], location);
			}
		}
		
		/*//Validate datetime format
		if(code == 0 || code == 14) {
			if(location.dateTimeFormat == undefined || location.dateTimeFormat == null || location.dateTimeFormat =="" ){
				return this.validationResponse(true, "dateTimeFormat", $rootScope.nls["ERR3440"], location);
			}
		}*/
		
		
		//added by Sathish for Bug
		//Validate Currency master
		if(code == 0 || code == 15) {
			if(location.currencyMaster == null || location.currencyMaster == undefined || location.currencyMaster== ""  ||location.currencyMaster.id==undefined){
				return this.validationResponse(true, "currencyCode", $rootScope.nls["ERR3441"], location);	
			}
			else{
				if(location.currencyMaster.status=="Block"){
					location.currencyMaster = null;
					return this.validationResponse(true, "currencyCode", $rootScope.nls["ERR3442"], location);	
				}
				else if(location.currencyMaster.status=="Hide"){
					location.currencyMaster = null;
					return this.validationResponse(true, "currencyCode", $rootScope.nls["ERR3443"], location);	
				}
			}
		}
		
		if(code == 0 || code == 17) {
			
			if( location.countryNo == undefined || location.countryNo == null || location.countryNo =="" ){
				//return this.validationResponse(true, "countryNo", $rootScope.nls["ERR3408"], location);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,location.countryNo)){
                    return this.validationResponse(true, "countryNo", $rootScope.nls["ERR3444"], location);
    			}
			}
		}
		//Validate location code
		if(code == 0 || code == 18) {
			if(location.cityNo == undefined || location.cityNo == null || location.cityNo =="" ){
				//return this.validationResponse(true, "cityNo", $rootScope.nls["ERR3407"], location);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,location.cityNo)){
                    return this.validationResponse(true, "cityNo", $rootScope.nls["ERR3445"], location);
    			}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		return this.validationSuccesResponse(location);
	}
 
	this.validationSuccesResponse = function(location) {
	    return {error : false, obj : location};
	}
	
	this.validationResponse = function(err, elem, message, location) {
		return {error : err, errElement : elem, errMessage : message, obj : location};
	}
	
}]);