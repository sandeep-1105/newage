/**
 * LocationMaster Factory which initiates all Rest calls using resource service
 */

(function() {
	
	
	app.factory("LocationSearch",['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("LocationSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("LocationSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("CountryWiseLocation",['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/search/keyword/:countryId", {}, {
			fetch : {
				method : 'POST',
				params : {
					countryId : ''
				},
				isArray : false
			}
		});
	}]);
	
	

	app.factory("CompanyWiseLocation",['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/searchByCompany/keyword/:companyId", {}, {
			fetch : {
				method : 'POST',
				params : {
					companyId : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("LocationView",['$resource', function($resource) {
		return $resource("/api/v1/locationmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("SearchLocationNotInList", ['$resource',function($resource) {
		return $resource("/api/v1/locationmaster/get/search/exclude", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	 
	
})();
