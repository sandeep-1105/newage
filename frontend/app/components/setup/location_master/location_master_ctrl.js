(function(){
app.controller("LocationMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'locationMasterFactory', 'RecentHistorySaveService', 'locationMasterValidationService', 'Notification','PortByTransportMode','CityList','CompanyList','CountryList','RegionList','DivisionList','ZoneList','PartiesList','TimeZoneListAPI','CurrencyMasterSearchKeyword','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, locationMasterFactory, RecentHistorySaveService, locationMasterValidationService, Notification,PortByTransportMode,CityList,CompanyList,CountryList,RegionList,DivisionList,ZoneList,PartiesList, TimeZoneListAPI ,CurrencyMasterSearchKeyword,roleConstant) {

        var vm = this;
        vm.deskTopView = true;
        vm.tableHeadArr = [
            			   {
            		            "name":"#",
            		            "width":"w25px",
            		            "model":"no",
            		            "prefWidth":"25",
            		            "search":false
            		        },

            		        {
            		            "name":"Name",
            		            "width":"w120px",
            		            "model":"locationName",
            		            "search":true,
            		            "wrap_cell":true,
            		            "type":"text",
            		            "sort":true,
            		            "prefWidth":"120",
            		            "key":"searchName"

            		        },
            		        {
            		            "name":"Code",
            		            "width":"w60px",
            		            "model":"locationCode",
            		            "search":true,
            		            "type":"text",
            		            "sort":true,
            		            "prefWidth":"60",
            		            "key":"searchCode"
            		        },
            		        {
            		            "name":"Company",
            		            "width":"w150px",
            		            "model":"companyMaster.companyName",
            		            "search":true,
            		            "type":"text",
            		            "sort":true,
            		            "prefWidth":"150",
            		            "key":"searchCompanyName"
            		        },
            		        {
            		            "name":"Branch",
            		            "width":"w100px",
            		            "model":"branchName",
            		            "search":true,
            		            "type":"text",
            		            "sort":true,
            		            "prefWidth":"100",
            		            "key":"searchBranchName"
            		        },
            		        {
            		            "name":"Country",
            		            "width":"w120px",
            		            "model":"countryMaster.countryName",
            		            "search":true,
            		            "type":"text",
            		            "sort":true,
            		            "prefWidth":"120",
            		            "key":"searchCountryName"
            		        },
            		        {
            					"name":"Status",
            					"width":"w120px",
            					"model":"status",
            					"search":true,
            					"type":"drop",
            					"sort":true,
            					"data":$rootScope.enum['LovStatus'],
            					"prefWidth":"120",
            					"key":"searchStatus"
            				}];
        vm.dataArr = [];
        vm.searchDto = {};
        vm.locationMaster = {};
        vm.locationMaster.status='Active';
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.searchDto = {};
        vm.sortSelection = {
            sortKey: "locationName",
            sortOrder: "asc"
        }
        
    	vm.dateFormatArray = [ "DD-MM-YYYY",
    	                           "YYYY-MM-DD",
    	                           "MM-DD-YYYY",
    	                           "DD/MM/YYYY",
    	                           "YYYY/MM/DD",
    	                           "MM/DD/YYYY" ];
        
        
        /**
         * TimeZoneListAPI will get all time zones
         */
        TimeZoneListAPI.get(function(data) {
            if (data.responseCode == 'ERR0') {
                vm.timeZones = data.responseObject;
            } else {
           	 console.log("Failed while getting timeszones")
            }
       }, function(error) {
      	 console.log("Failed while getting timeszones " , error)
       });
        
        /**
         * singlePageNavigation method starts here
         */
        vm.singlePageNavigation = function(val) {
        	if($stateParams != undefined && $stateParams != null) {
                if ((parseInt($stateParams.index) + val) < 0 || (parseInt($stateParams.index) + val) >= parseInt($stateParams.totalRecord))
                    return;
                var stateParameters = {};
                vm.searchDto = {};
                vm.searchDto.recordPerPage = 1;
                vm.hidePage = true;
                vm.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
    			
    			if($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
    				stateParameters.searchStatus = vm.searchDto.searchStatus = $stateParams.searchStatus; 
            	}
    			if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
    				stateParameters.sortByColumn = vm.searchDto.sortByColumn = $stateParams.sortByColumn; 
            	}
    			if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
    				stateParameters.orderByType = vm.searchDto.orderByType = $stateParams.orderByType; 
            	}
    			if($stateParams.searchCode != undefined && $stateParams.searchCode != null) {
    				stateParameters.searchCode = vm.searchDto.searchCode = $stateParams.searchCode; 
            	}
    			if($stateParams.searchName != undefined && $stateParams.searchName != null) {
    				stateParameters.searchName = vm.searchDto.searchName = $stateParams.searchName; 
            	}
    			if($stateParams.searchCompanyName != undefined && $stateParams.searchCompanyName != null) {
    				stateParameters.searchCompanyName = vm.searchDto.searchCompanyName = $stateParams.searchCompanyName; 
            	}
    			if($stateParams.searchBranchName != undefined && $stateParams.searchBranchName != null) {
    				stateParameters.searchBranchName = vm.searchDto.searchBranchName = $stateParams.searchBranchName; 
            	}
    			if($stateParams.searchCountryName != undefined && $stateParams.searchCountryName != null) {
    				stateParameters.searchCountryName = vm.searchDto.searchCountryName = $stateParams.searchCountryName; 
            	}
    			if($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
    				stateParameters.searchStatus = vm.searchDto.searchStatus = $stateParams.searchStatus; 
            	}
    			
    			
        	} else {
        		return;
        	}
        	locationMasterFactory.search.fetch(vm.searchDto).$promise.then(
                    function(data) {
                    	 vm.totalRecord = data.responseObject.totalRecord;
                    	 stateParameters.selectedPageNumber = vm.searchDto.selectedPageNumber;
                         stateParameters.id = data.responseObject.searchResult[0].id;
                         $scope.hidePage = false;
                         $state.go("layout.locationMasterView", stateParameters);
                    });
        }
        
        /**
         * fetching company list start here
         */
        vm.ajaxCompanyEvent = function(object) {
            vm.searchDto = {};
            vm.searchDto.keyword = object == null ? "" : object;
            vm.searchDto.selectedPageNumber = 0;
            vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CompanyList.fetch(vm.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                        	vm.companyList = data.responseObject.searchResult;
                        	return vm.companyList;
                            
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Company');
                    });
        }
        vm.selectedCompany = function(object) {
              if(vm.validate('companyName', 3)){
              $rootScope.navigateToNextField(object);
              }
        };
        
        vm.ajaxCurrencyEvent = function (object) {

        	vm.searchDto = {};
    		vm.searchDto.keyword = object == null ? "" : object;
    		vm.searchDto.selectedPageNumber = 0;
    		vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CurrencyMasterSearchKeyword.query(vm.searchDto).$promise.then(function (data, status) {
                    if (data.responseCode == "ERR0") {
                        vm.currencyMasterList = data.responseObject.searchResult;
                        return vm.currencyMasterList;
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Currency');
                }
            );
        }
        
        vm.selectedCurrency = function(object) {
        	if(vm.validate('currencyCode', 15)){
        		$rootScope.navigateToNextField(object);        		
        	}
        };
      
        /**
         * fetching country list start here
         */
        vm.ajaxCountryEvent = function(object) {
    		vm.searchDto = {};
    		vm.searchDto.keyword = object == null ? "" : object;
    		vm.searchDto.selectedPageNumber = 0;
    		vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		return CountryList.fetch(vm.searchDto).$promise
    				.then(
    						function(data) {
    							if (data.responseCode == "ERR0") {
    								vm.countryList = data.responseObject.searchResult;
    								return vm.countryList;
    							}
    						},
    						function(errResponse) {
    							console
    									.error('Error while fetching Country');
    						});

    	}
        vm.selectedCountry = function(object,countryObj) {
        	if(countryObj!=undefined){
        		vm.locationMaster.currencyMaster=countryObj.currencyMaster;
        	}
        	if(vm.validate('countryName', 5)){
        		$rootScope.navigateToNextField(object);        		
        	}
        };
        /**
         * fetching city list start here
         */
        vm.ajaxCityEvent = function(object) {
        	if(vm.locationMaster.countryMaster==null || vm.locationMaster.countryMaster==undefined){
        		vm.validate('countryName', 5);
        	}
            vm.searchDto = {};
            vm.searchDto.keyword = object == null ? "" : object;
            vm.searchDto.selectedPageNumber = 0;
            vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CityList.fetch({
						"countryId" : vm.locationMaster.countryMaster.id
					}, vm.searchDto).$promise.then(
					function(data) {
						if (data.responseCode == "ERR0") {
							vm.cityList = data.responseObject.searchResult;
							return vm.cityList;
						}
					},
					function(errResponse) {
						console
								.error('Error while fetching City List Based on the Country');
					});
        }
        vm.selectedCity = function(object) {
             if(vm.validate('cityName', 9)){
            	 $rootScope.navigateToNextField(object);            	 
             }
        };
        /**
         * fetching region list start here
         */
        vm.ajaxRegionEvent = function(object) {
    		vm.searchDto = {};
    		vm.searchDto.keyword = object == null ? "" : object;
    		vm.searchDto.selectedPageNumber = 0;
    		vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		return RegionList.fetch(vm.searchDto).$promise
    				.then(
    						function(data) {
    							if (data.responseCode == "ERR0") {
    								vm.regionList = data.responseObject.searchResult;
    								return vm.regionList;
    							}
    						},
    						function(errResponse) {
    							console
    									.error('Error while fetching Region');
    						});

    	}
        vm.selectedRegion = function(object) {
          if( vm.validate('regionName', 10)){
        	  $rootScope.navigateToNextField(object);        	  
          }
        };
        
        /**
         * fetching zone list start here
         */
        vm.ajaxZoneEvent = function(object) {
    		vm.searchDto = {};
    		vm.searchDto.keyword = object == null ? "" : object;
    		vm.searchDto.selectedPageNumber = 0;
    		vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		return ZoneList.fetch(vm.searchDto).$promise
    				.then(
    						function(data) {
    							if (data.responseCode == "ERR0") {
    								vm.zoneList = data.responseObject.searchResult;
    								return vm.zoneList;
    							}
    						},
    						function(errResponse) {
    							console
    									.error('Error while fetching Zone');
    						});

    	}
        vm.selectedZone = function(object) {
            if(vm.validate('zoneName', 11)){
            	$rootScope.navigateToNextField(object);            	
            }
        };
        /**
         * fetching division list start here
         */
        vm.ajaxDivisionEvent = function(object) {
    		vm.searchDto = {};
    		vm.searchDto.keyword = object == null ? "" : object;
    		vm.searchDto.selectedPageNumber = 0;
    		vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		return DivisionList.fetch(vm.searchDto).$promise
            .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            vm.divisionList = data.responseObject.searchResult;
                            return vm.divisionList;

                        }
                    },
                    function(error) {
                        console
                            .error('Error while fetching division');
                    });

    	}
        vm.selectedDivision = function(object) {
          if( vm.validate('divisionName', 12)){
        	  $rootScope.navigateToNextField(object);        	  
          }
        };
        /**
         * fetching agent list start here
         */
        vm.ajaxAgentEvent = function(object) {
    		vm.searchDto = {};
    		vm.searchDto.keyword = object == null ? "" : object;
    		vm.searchDto.selectedPageNumber = 0;
    		vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		return PartiesList.query(vm.searchDto).$promise
			.then(
					function(data, status) {
						if (data.responseCode == "ERR0") {
							vm.partyList = data.responseObject.searchResult;
							return vm.partyList;
						}
					},
					function(errResponse) {
						console
								.error('Error while fetching Party');
					});

    	}
        vm.selectedAgent = function(object) {
            if(vm.validate('agentName', 13)){
            	$rootScope.navigateToNextField(object);  	
            }
        };
      
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchDto[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchDto.orderByType = param.sortOrder.toUpperCase();
            vm.searchDto.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * Location Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Location Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Location Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Location Master from List page.
         * @param {string} data.id - The id of the Selected Location Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data,index) {
        	if($rootScope.roleAccess(roleConstant.SETUP_LOCATION_MASTER_VIEW)){
        	
            console.log("RowSelect ", data, index);
			
			vm.selectedRecordIndex = index;
		    
	
			/*************************************************************
			 * ux - by Muthu
			 * reason - ipad compatibility fix
			 *
			 * **********************************************************/
	
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				$scope.deskTopView = false;
			}
			else{
				$scope.deskTopView = true;
			}
			
            vm.selectedRowIndex =  (vm.searchDto.recordPerPage * vm.searchDto.selectedPageNumber) + index;
			
			var param = {
						id                      : data.id,
			    		selectedPageNumber 		: vm.selectedRowIndex,
			    		totalRecord 			: vm.totalRecord
			    };
			    
			$state.go("layout.locationMasterView", vm.searchDtoToStateParams(param));
        	}
		}
        
        vm.searchDtoToStateParams = function(param) {
   		 if(param == undefined || param == null) {
   		    		param = {};
   		    	}
   		 if(vm.searchDto != undefined && vm.searchDto != null) {
       		if(vm.searchDto != undefined && vm.searchDto != null) {
           		param.recordPerPage = 1;
       			if(vm.searchDto.sortByColumn != undefined && vm.searchDto.sortByColumn != null) {
               		param.sortByColumn = vm.searchDto.sortByColumn; 
               	}
       			if(vm.searchDto.orderByType != undefined && vm.searchDto.orderByType != null) {
               		param.orderByType = vm.searchDto.orderByType; 
               	}
       			if(vm.searchDto.searchCode != undefined && vm.searchDto.searchCode != null) {
               		param.searchCode = vm.searchDto.searchCode; 
               	}
       			
       			if(vm.searchDto.searchName != undefined && vm.searchDto.searchName != null) {
               		param.searchName = vm.searchDto.searchName; 
               	}
       			
       			if(vm.searchDto.searchCompanyName != undefined && vm.searchDto.searchCompanyName != null) {
               		param.searchCompanyName = vm.searchDto.searchCompanyName; 
               	}
       			
       			if(vm.searchDto.searchBranchName != undefined && vm.searchDto.searchBranchName != null) {
               		param.searchBranchName = vm.searchDto.searchBranchName; 
               	}
       			
       			if(vm.searchDto.searchCountryName != undefined && vm.searchDto.searchCountryName != null) {
               		param.searchCountryName = vm.searchDto.searchCountryName; 
               	}
       			if(vm.searchDto.searchStatus != undefined && vm.searchDto.searchStatus != null) {
               		param.searchStatus = vm.searchDto.searchStatus; 
               	}
       		}
       		
   		}
   		return param;
   		}

        /**
         * Location Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchDto = {};
            vm.searchDto.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchDto.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
       
       
        /**
         * Get Location Master's Based on Search Data.
         */
        vm.search = function() {
            vm.searchDto.selectedPageNumber = vm.page;
            vm.searchDto.recordPerPage = vm.limit;
            vm.dataArr = [];
            locationMasterFactory.search.fetch(vm.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching Location');
                });
           }

        /**
         * Get Location Master By id.
         * @param {int} id - The id of a Location.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Location.
         */
        vm.view = function(id, isView) {
        	locationMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.locationMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.locationMaster != undefined && vm.locationMaster != null && vm.locationMaster.id != undefined && vm.locationMaster.id != null) {
                        $rootScope.subTitle = vm.locationMaster.locationName;
                        vm.locationMaster.isHeadOffice=='TRUE'?vm.locationMaster.isHeadOffice=true:vm.locationMaster.isHeadOffice=false;
                        vm.locationMaster.partyMaster=vm.locationMaster.tmpPartyMaster;
                    } else {
                        $rootScope.subTitle = "Unknown Location"
                    }
                    $rootScope.unfinishedFormTitle = "Location  # " + vm.locationMaster.locationCode;
                    if (isView) {
                    	vm.selectedRowIndex=parseInt($stateParams.selectedPageNumber);
                    	vm.totalRecord=parseInt($stateParams.totalRecord);
                        rHistoryObj = {
                            'title': "Location #" + vm.locationMaster.locationCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Location Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Location # ' + $stateParams.id,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Location Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Location by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add Location Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.SETUP_LOCATION_MASTER_CREATE)){
				 $state.go("layout.locationMasterAdd");
			}
        }

        /**
         * Validating field of Location Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = locationMasterValidationService.validate(vm.locationMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
				return false;
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            	return true;
            }
        }
        
        
        vm.backToList = function() {
            if(vm.locationMasterForm!=undefined){
		    	vm.goTocancel(vm.locationMasterForm.$dirty);
            }else{
            	vm.goTocancel(false);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		        	 vm.create();
     		          } else if (value == 2) {
     		        	 $state.go("layout.locationMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.locationMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Location Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.SETUP_LOCATION_MASTER_MODIFY)){
        		$state.go("layout.locationMasterEdit", {
                    id: id
                });
			}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.SETUP_LOCATION_MASTER_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR3438"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	locationMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	$state.go('layout.locationMaster');
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        	}
        }

        /**
         * Create New Location Master.
         */
        vm.create = function() {
            var validationResponse = locationMasterValidationService.validate(vm.locationMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create Location..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	//vm.locationMaster.dateFormat='DD-MM-YYYY';
            	//vm.locationMaster.dateTimeFormat='DD-MM-YYYY HH:mm';
            	if(vm.locationMaster.id == null) {
    				var successMessage = $rootScope.nls["ERR400"];	
    			} else {
    				var successMessage = $rootScope.nls["ERR401"];	
    			}
            	vm.locationMaster.isHeadOffice==true?vm.locationMaster.isHeadOffice='TRUE':vm.locationMaster.isHeadOffice='FALSE';
            	locationMasterFactory.create.query(vm.locationMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Location Saved Successfully Response :: ", data);
		            	Notification.success(successMessage);
		            	$state.go('layout.locationMaster');
		            } else {
		                $log.debug("Create and update Location Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create and update  Location Failed : " + error)
		        });
            } 
        };
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			vm.errorMap = new Map();
			if(valResponse.error == true){
				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
		
       /* vm.update = function() {
        	 var validationResponse = locationMasterValidationService.validate(vm.locationMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the Location..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
            		if(vm.locationMaster.id == null) {
        				var successMessage = $rootScope.nls["ERR400"];	
        			} else {
        				var successMessage = $rootScope.nls["ERR401"];	
        			}
             	locationMasterFactory.update.query(vm.locationMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("Location Updated Successfully Response :: ", data);
 		            	Notification.success(successMessage);
 		            	$state.go('layout.locationMaster');
 		            } else {
 		                $log.debug("Updating Location Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating Location Failed : " + error)
 		        });
             } 
        };*/
        
       
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isLocationMasterReloaded;
        $scope.$on('addLocationMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.locationMaster;
			$rootScope.category = "Location Master";
			$rootScope.unfinishedFormTitle = "Location (New)";
        	if(vm.locationMaster != undefined && vm.locationMaster != null && vm.locationMaster.locationName != undefined && vm.locationMaster.locationName != null) {
        		$rootScope.subTitle = vm.locationMaster.locationName;
        	} else {
        		$rootScope.subTitle = "Unknown Location "
        	}
        })
      
	    $scope.$on('editLocationMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.locationMaster;
			$rootScope.category = "Location Master";
			$rootScope.unfinishedFormTitle = "Location Edit # "+vm.locationMaster.locationCode;
	    })
	      
	    $scope.$on('addLocationMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.locationMaster) ;
	    	    localStorage.isLocationMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editLocationMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.locationMaster) ;
	    	    localStorage.isLocationMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
        case "VIEW":
            console.log("I am In Location View Page");
            $rootScope.unfinishedFormTitle = "Location View # " + $stateParams.id;
            $rootScope.unfinishedData = undefined;
            vm.selectedTabIndex=0;
            if($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
				vm.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
			}
			
    	    if($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
				vm.totalRecord = parseInt($stateParams.totalRecord);
			}
            vm.view($stateParams.id,true);
            $rootScope.breadcrumbArr = [{label:"Setup", state:"layout.locationMaster"},
                                        {label:"Location", state:"layout.locationMaster"}, {label:"View Location", state:null}];
            break;
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isLocationMasterReloaded = "NO";
                       vm.locationMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.locationMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isLocationMasterReloaded = "NO";
                       vm.locationMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = [{label: "Setup",state: "layout.location"},{label: "Location",state: "layout.location"},{label: "Add Location",state: null}];
                break;
            case "EDIT":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isLocationMasterReloaded = "NO";
                       vm.locationMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.locationMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isLocationMasterReloaded = "NO";
                       vm.locationMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = [{label: "Setup",state: "layout.location"},{label: "Location",state: "layout.location"},{label: "Edit Location",state: null}];
                break;
            default:
                $rootScope.breadcrumbArr = [{label: "Setup",state: "layout.location"},{label: "Location",state: "layout.location"}];
                vm.searchInit();
        }

    }
]);
}) ();