app.service('DocIssueValidationService', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	
	this.validate = function(dataObject, code) {
		if(code == 0 || code == 1) {
			if (dataObject.locationMaster == null || dataObject.locationMaster.id == undefined || dataObject.locationMaster.id == null || dataObject.locationMaster.id == "") {
				return this.validationResponse(true, "dirLocation", "none", -1, $rootScope.nls["ERR0135"], dataObject);
			} else if(dataObject.locationMaster.status != null) {
	            if (dataObject.locationMaster.status == "Block") {
	            	dataObject.locationMaster = null;
					return this.validationResponse(true, "dirLocation", "none", -1, $rootScope.nls["ERR0136"], dataObject);
	            }
	        }
		}
		
		if(code == 0 || code == 2) {
			if (dataObject.serviceMaster == null || dataObject.serviceMaster.id == undefined || dataObject.serviceMaster.id == null || dataObject.serviceMaster.id == "") {
				return this.validationResponse(true, "dirService", "none", -1, $rootScope.nls["ERR0139"], dataObject);
			} else if(dataObject.serviceMaster.status != null) {
	            if (dataObject.serviceMaster.status == "Block") {
	            	dataObject.serviceMaster = null;
					return this.validationResponse(true, "dirService", "none", -1, $rootScope.nls["ERR0140"], dataObject);
	            }
	        }
		}
		return this.validationSuccesResponse(dataObject);
	}

	this.validationSuccesResponse = function(dataObject) {
	    return {error : false, obj : dataObject};
	}

	this.validationResponse = function(err, elem, tab, index, message, dataObject) {
		return {error : err, errElement : elem, tab : tab, index :index, errMessage : message, obj : dataObject};
	}
	
});