angular.module('NewAge')
	.factory('docIssueRestrictionFactory', function($resource) {
        return {
            action: $resource('/api/v1/documentissuerestriction', {}, {
                save: { method : 'POST' }
            }),
            search: $resource('/api/v1/documentissuerestriction/search/:id', {}, {
                query: { method : 'POST' },
                findById: { method : 'GET', params: { id : '' }, isArray: false }
            })
        };
    })