'use strict';

angular.module('NewAge')
    .controller('DocIssueRestrictionCtrl',
        function($http, $scope, $rootScope, $stateParams, $state, $filter, $timeout, $window, appMetaFactory, ServiceList, ngDialog,
            docIssueRestrictionFactory, DocIssueValidationService, RecentHistorySaveService, roleConstant) {
            $scope.action = "GETALL";
            $scope.docIssueRestriction = {};
            $scope.docIssueHeadArr = [{}];
            $scope.tosmasterList = [];
            $scope.searchData = {};

            $scope.limitArr = [10, 15, 20];
            $scope.page = 0;
            $scope.limit = 10;
            $scope.totalRecord = 0;

            $scope.lovLocationList = [];
            $scope.add = function() {
                if ($rootScope.roleAccess(roleConstant.SETUP_DOCUMENT_ISSUE_RESTRICTION_CREATE)) {
                    $state.go("layout.addDocIssueRestriction");
                }
            };

            $scope.edit = function() {
                if ($rootScope.roleAccess(roleConstant.SETUP_DOCUMENT_ISSUE_RESTRICTION_MODIFY)) {
                    $state.go("layout.editDocIssueRestriction", { id: $scope.docIssueRestriction.id });
                }
            };

            $scope.remove = function() {
                console.log("delete function called");
                if ($rootScope.roleAccess(roleConstant.SETUP_DOCUMENT_ISSUE_RESTRICTION_DELETE)) {
                    console.log("deleted..");
                }
            }
            $scope.setOldDataVal = function() {

                $timeout(function() {
                    $scope.oldData = JSON.stringify($scope.docIssueRestriction);
                }, 2000);
            }
            $scope.submit = function() {
                $scope.diasbleSubmitBtnFn();
                var validationResponse = DocIssueValidationService.validate($scope.docIssueRestriction, 0);
                console.log("Validation Response -- ", validationResponse);
                if (validationResponse.error == true) {
                    console.log("Validation Response 2-- ", validationResponse);
                    $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
                } else {
                    docIssueRestrictionFactory.action.save($scope.docIssueRestriction).$promise.then(function(data) {
                            if (data.responseCode == "ERR0") {
                                console.log("Enquiry  updated Successfully")
                                $state.go('layout.docIssueRestriction', { submitAction: "Saved" });
                            } else {
                                console.log("added Failed " + data.responseDescription)
                            }
                        },
                        function(error) {
                            console.log("Error", error);
                        });
                }
            };
            $scope.cancel = function() {
                if ($scope.oldData == JSON.stringify($scope.docIssueRestriction)) {
                    console.log("both are Equal");

                    $state.go('layout.docIssueRestriction', { submitAction: "Cancelled" });
                } else {

                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                                '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(function(value) {
                            if (value == 1) {
                                $scope.submit();
                            } else if (value == 2) {
                                $state.go('layout.docIssueRestriction', { submitAction: "Cancelled" });
                            } else {
                                console.log("cancelled");
                            }
                        });
                }

            };

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/

            $scope.searchInit = function() {

                $scope.docIssueHeadArr = [{
                    "name": "#",
                    "width": "inl_sno",
                    "model": "no",
                    "search": false
                }, {
                    "name": "Location",
                    "width": "inl_m",
                    "model": "locationMaster.locationName",
                    "search": true,
                    "wrap_cell": true,
                    "type": "text",
                    "sort": true,
                    "key": "locationMaster"

                }, {
                    "name": "Service",
                    "width": "inl_m",
                    "model": "serviceMaster.serviceName",
                    "search": true,
                    "wrap_cell": true,
                    "type": "text",
                    "sort": true,
                    "key": "serviceMaster"

                }, {
                    "name": "Check Invoice",
                    "width": "inl_m",
                    "model": "enableIssueRestriction",
                    "search": true,
                    "wrap_cell": true,
                    "type": "drop",
                    "sort": false,
                    "key": "enableIssueRestriction"

                }, {
                    "name": "Check Credit Limit",
                    "width": "inl_m",
                    "model": "enableCreditLimitChecking",
                    "search": true,
                    "wrap_cell": true,
                    "type": "drop",
                    "sort": false,
                    "key": "enableCreditLimitChecking"

                }];

            }

            $scope.limitChange = function(item) {
                $scope.page = 0;
                $scope.limit = item;
                $scope.enquirysearch($scope.page, $scope.limit);
            }
            $scope.changepage = function(param) {
                $scope.page = param.page;
                $scope.limit = param.size;
                $scope.search($scope.page, $scope.limit);
            }

            $scope.rowSelect = function(selectedObj) {
                if ($rootScope.roleAccess(roleConstant.SETUP_DOCUMENT_ISSUE_RESTRICTION_VIEW)) {
                    console.log("rowSelect selectedObj :: ", selectedObj);
                    $scope.docIssueRestriction = selectedObj;
                    if ($scope.docIssueRestriction.exceptionalTerms == null) {
                        $scope.docIssueRestriction.exceptionalTerms = [];
                    }
                    $scope.showHistory = false;
                    $scope.showDetail = true;

                    /************************************************************
                     * ux - by Muthu
                     * reason - ipad compatibility fix
                     *
                     * *********************************************************/

                    var windowInner = $window.innerWidth;
                    if (windowInner <= 1199) {
                        $scope.deskTopView = false;

                    } else {
                        $scope.deskTopView = true;
                    }

                    /********************* ux fix ends here *****************************/

                }
            }

            $scope.changeSearch = function(param) {
                for (var attrname in param) {
                    console.log("attrname - ", attrname, " ; Value - ", param[attrname]);
                    $scope.searchData[attrname] = param[attrname];
                }
                $scope.page = 0;
                $scope.search();
            }


            $scope.sortChange = function(param) {
                console.log("Sort Param ,", param);
                switch (param.sortKey) {
                    case "locationMaster":
                        $scope.searchData.sortByColumn = "di.locationMaster.locationName";
                        break;
                    case "serviceMaster":
                        $scope.searchData.sortByColumn = "di.serviceMaster.serviceName";
                        break;
                    default:
                        $scope.searchData.sortByColumn = "di.id";
                }
                $scope.searchData.orderByType = param.sortOrder.toUpperCase();
                $scope.search();
            };
            $scope.diasbleSubmitBtnFn = function() {
                $scope.disableSubmitBtn = true;
                $timeout(function() {
                    $scope.disableSubmitBtn = false;
                }, 2000);
            }
            $scope.getAllTos = function() {
                //				ServiceList.fetch($scope.searchDto).$promise
                appMetaFactory.tos.findall({}).$promise.then(function(
                    data, status) {
                    console.log("tosmasterList ", data);
                    $scope.tosmasterList = data.responseObject;
                    if ($scope.tosmasterList == undefined || $scope.tosmasterList == null) {
                        $scope.tosmasterList = [];
                    }
                });
            }

            $scope.compareTosFn = function(obj1, obj2) {
                return obj1.id === obj2.id;
            };

            //Input Focus Navigation - Starts Here
            $scope.navigateToNextField = function(id) {
                    console.log("Element Id ", id);
                    var focElement = document.getElementById(id);
                    if (focElement != null) {
                        focElement.focus();
                    }
                }
                //Input Focus Navigation - Ends Here

            //Common Validation Focus Functionality - starts here
            $scope.validationErrorAndFocus = function(valResponse, elemId) {
                $scope.errorMap = new Map();
                if (valResponse.error == true) {
                    if (valResponse.tab == "none") {
                        $scope.docIssueRestriction = valResponse.obj;
                    }
                    $scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
                    console.log("$scope.valResponse -- ", valResponse);
                    console.log("$scope.errorMap -- ", $scope.errorMap);
                }
                $scope.navigateToNextField(elemId);
            }

            /* Location Lov - Starts Here*/
            $scope.locationRender = function(item) {
                return {
                    label: item.locationName,
                    item: item
                }
            };
            $scope.ajaxLovLocationEvent = function(object) {
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                return appMetaFactory.location.search({ 'companyId': $rootScope.userProfile.selectedCompany.id }, $scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.serviceList = data.responseObject.searchResult;
                            return $scope.serviceList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching  Services');
                    }
                );

            }
            $scope.selectedLocation = function(elemId) {
                    $scope.validationErrorAndFocus(DocIssueValidationService.validate($scope.docIssueRestriction, 1), elemId);
                }
                /* Location Lov - Ends Here*/
                /* Service Lov - Starts Here*/

            $scope.ajaxServiceEvent = function(object) {

                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                return ServiceList.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.serviceList = data.responseObject.searchResult;
                            return $scope.serviceList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching  Services');
                    }
                );

            }


            $scope.selectedService = function(elemId) {
                $scope.validationErrorAndFocus(DocIssueValidationService.validate($scope.docIssueRestriction, 2), elemId);
            };

            $scope.serviceRender = function(item) {
                return {
                    label: item.serviceName,
                    item: item
                }
            };
            /* Service Lov - Ends Here*/
            $scope.search = function() {
                $scope.searchData.selectedPageNumber = $scope.page;
                $scope.searchData.recordPerPage = $scope.limit;
                $scope.docIssueArr = [];
                docIssueRestrictionFactory.search.query($scope.searchData).$promise.then(function(
                    data, status) {

                    $scope.totalRecord = data.responseObject.totalRecord;
                    $scope.docIssueArr = data.responseObject.searchResult;

                });
            }

            $scope.getAndAssignData = function(id) {
                docIssueRestrictionFactory.search.findById({ id: id }).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.docIssueRestriction = data.responseObject;
                    } else {
                        console.log("Exception while getting Country DynamicField by Id ", data.responseDescription);
                    }
                }, function(error) {
                    console.log("Error ", error);
                });
            };

            $scope.addInit = function() {
                $scope.docIssueRestriction = {};
                $scope.docIssueRestriction.companyMaster = $rootScope.userProfile.selectedCompany;
                console.log("$scope.docIssueRestriction.companyMaster ----- ", $scope.docIssueRestriction.companyMaster);
                $scope.docIssueRestriction.enableInvoiceChecking = false;
                $scope.docIssueRestriction.enableIssueRestriction = false;
                $scope.docIssueRestriction.enableCreditLimitChecking = false;
                $scope.getAllTos();
            }

            $scope.editInit = function() {
                $scope.getAndAssignData($stateParams.id);

            }
            $scope.$on('addDocIssueRestrictionEvent', function(events, args) {
                $rootScope.unfinishedData = $scope.docIssueRestriction;
                $rootScope.category = "Document Issue Restriction";
                $rootScope.unfinishedFormTitle = "Document Issue Restriction (New)";
                if ($scope.docIssueRestriction != undefined && $scope.docIssueRestriction != null && $scope.docIssueRestriction.serviceMaster != undefined && $scope.docIssueRestriction.serviceMaster != null) {
                    $rootScope.subTitle = $scope.docIssueRestriction.serviceMaster.serviceName != null ? $scope.docIssueRestriction.serviceMaster.serviceName : "";
                } else {
                    $rootScope.subTitle = "Empty Service"
                }
            });

            $scope.$on('editDocIssueRestrictionEvent', function(events, args) {
                $rootScope.unfinishedData = $scope.docIssueRestriction;
                $rootScope.category = "Document Issue Restriction";
                $rootScope.unfinishedFormTitle = "Document Issue Restriction Edit # " + $scope.docIssueRestriction.serviceMaster.serviceName;
                if ($scope.docIssueRestriction != undefined && $scope.docIssueRestriction != null && $scope.docIssueRestriction.serviceMaster != undefined && $scope.docIssueRestriction.serviceMaster != null) {
                    $rootScope.subTitle = $scope.docIssueRestriction.serviceMaster.serviceName != null ? $scope.docIssueRestriction.serviceMaster.serviceName : "";
                } else {
                    $rootScope.subTitle = "Empty Service"
                }
            });

            $scope.$on('addDocIssueRestrictionEventReload', function(e, confirmation) {
                localStorage.reloadFormData = JSON.stringify($scope.docIssueRestriction);
                localStorage.isDocIssueRestrictionReloaded = "YES";
                e.preventDefault();
            });

            $scope.$on('editDocIssueRestrictionEventReload', function(e, confirmation) {
                localStorage.reloadFormData = JSON.stringify($scope.docIssueRestriction);
                localStorage.isDocIssueRestrictionReloaded = "YES";
                e.preventDefault();
            });

            $scope.isReloaded = localStorage.isDocIssueRestrictionReloaded;

            if ($stateParams.action == "SEARCH") {
                $scope.searchInit();
                $scope.search();
                $rootScope.breadcrumbArr = [{ label: "Setup", state: "layout.docIssueRestriction" },
                    { label: "Document Issue restriction", state: null }
                ];
            } else if ($stateParams.action == "ADD") {
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isDocIssueRestrictionReloaded = "NO";
                        $scope.docIssueRestriction = JSON.parse(localStorage.reloadFormData);
                    } else {
                        $scope.docIssueRestriction = $rootScope.selectedUnfilledFormData;
                    }
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isDocIssueRestrictionReloaded = "NO";
                        $scope.docIssueRestriction = JSON.parse(localStorage.reloadFormData);
                    } else {
                        $scope.addInit();
                    }
                }
                $scope.getAllTos();
                $scope.setOldDataVal();
                $rootScope.breadcrumbArr = [{ label: "Setup", state: "layout.docIssueRestriction" },
                    { label: "Document Issue restriction", state: "layout.docIssueRestriction" },
                    { label: "Add Document Issue restriction", state: null }
                ];
            } else if ($stateParams.action == "EDIT") {
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isDocIssueRestrictionReloaded = "NO";
                        $scope.docIssueRestriction = JSON.parse(localStorage.reloadFormData);
                        //default focus for this page 
                    } else {
                        $scope.docIssueRestriction = $rootScope.selectedUnfilledFormData;
                    }
                    $scope.setOldDataVal();
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isDocIssueRestrictionReloaded = "NO";
                        $scope.docIssueRestriction = JSON.parse(localStorage.reloadFormData);
                        $scope.setOldDataVal();
                    } else {
                        $scope.editInit($stateParams.id);
                    }
                }
                $scope.getAllTos();
                $rootScope.breadcrumbArr = [{ label: "Setup", state: "layout.docIssueRestriction" },
                    { label: "Document Issue restriction", state: "layout.docIssueRestriction" },
                    { label: "Edit Document Issue restriction", state: null }
                ];
            }



        });