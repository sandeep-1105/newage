app.controller("agentPortCtrl",['$scope', '$rootScope', '$modal', 'ngDialog', '$stateParams', '$state', 'Notification', 
        'ServiceList', 'AgentPortUpdate', 'AgentPortRemove',
        'AgentPortSearch', 'PartiesList', 'PortByTransportMode', 'roleConstant',
    function($scope, $rootScope, $modal, ngDialog, $stateParams, $state, Notification, 
        ServiceList, AgentPortUpdate, AgentPortRemove,
        AgentPortSearch, PartiesList, PortByTransportMode, roleConstant) {

    $scope.init = function() {
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search();
    }

    $scope.search = function() {
        console.log("Search is called....");
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        AgentPortSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.agentPortArray = resultArr;
            console.log("$scope.agentPortArray ", $scope.appConfigArray);
        });
    }

    $scope.agentPortHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",

            "model": "no",
            "search": false
        },
        {
            "name": "Service",
            "search": true,
            "model": "serviceMaster",
            "wrap_cell": true,
            "type": "lov",
            "sort": true,
            "width": "col-xs-3 col-sm-2 col-md-3",

            "key": "searchService",
            "lov": {
                selName: 'serviceName',
                viewKey: 'serviceMaster.serviceName',
                ajaxFn: 'ajaxServiceEvent(param)'
            }
        }, {
            "name": "Agent",
            "search": true,
            "model": "agent",
            "wrap_cell": true,
            "type": "lov",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "key": "searchAgent",
            "lov": {
                selName: 'partyName',
                viewKey: 'agent.partyName',
                ajaxFn: 'ajaxPartyEvent(param)'
            }
        }, {
            "name": "Port",
            "search": true,
            "model": "port",
            "wrap_cell": true,
            "type": "lov",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "key": "searchPort",
            "lov": {
                selName: 'portName',
                viewKey: 'port.portName',
                ajaxFn: 'ajaxPortEvent(param)'
            }
        },
        {
            "name": "Action",
            "model": "",
            "search": false,
            "wrap_cell": true,
            "type": "action",
            "actype": "action",
            "sort": false,
            "width": "col-xs-1 col-sm-3 col-md-2",

            "key": ""
        }
    ];

    $scope.sortSelection = {
        sortKey: "serviceMaster",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        console.log("change search", $scope.searchDto);
        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        if (param.sortKey.includes("serviceMaster")) {
            $scope.searchDto.sortByColumn = "serviceMaster.serviceName"
        } else if (param.sortKey.includes("agent")) {
            $scope.searchDto.sortByColumn = "agent.partyName"
        } else {
            $scope.searchDto.sortByColumn = "port.portName"
        }
        //$scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.changepage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.remove = function(param) {
        console.log("Remove ", param);
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR05215"];
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button></div>',
            plain: true,
            scope: newScope,
            closeByDocument: false,
            className: 'ngdialog-theme-default'
        }).then(function(value) {
            AgentPortRemove.remove({
                id: param.item.id
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Agent Port  deleted Successfully");
                    $scope.init();
                    Notification.error($rootScope.nls["ERR402"]);
                } else {
                    console.log("Agent Port  delete Failed ", data.responseDescription);
                }
            }, function(error) {
                console.log("Agent Port  Code delete Failed : ", error);
            });
        }, function(value) {
            console.log("delete Cancelled");
        });
    }


    $scope.validation = function(param) {
        var errorFlag = true;
        if (param != undefined && param != null && param.item != undefined && param.item != null) {
            if (param.item.serviceMaster.status == 'Block') {
                Notification.error($rootScope.nls["ERR05203"]);
                errorFlag = false;
            } else if (param.item.serviceMaster.status == 'Hide') {
                Notification.error($rootScope.nls["ERR05204"]);
                errorFlag = false;
            }
            if (param.item.agent.status == 'Block') {
                Notification.error($rootScope.nls["ERR05210"]);
                errorFlag = false;
            } else if (param.item.agent.status == 'Hide') {
                Notification.error($rootScope.nls["ERR05211"]);
                errorFlag = false;
            }
            if (param.item.port.status == 'Block') {
                Notification.error($rootScope.nls["ERR05213"]);
                errorFlag = false;
            } else if (param.item.agent.port == 'Hide') {
                Notification.error($rootScope.nls["ERR05214"]);
                errorFlag = false;
            }
        }
        return errorFlag;
    }

    var checkAndUpdate = function(param) {
        console.log("checkAndUpdate ", param);
        if ($scope.validation(param)) {
            $scope.update(param);
        } else {
            console.log('Found error while validating app config.');
            return false;
        }
    }


    $scope.update = function(param) {
        console.log("Update ", param);
        AgentPortUpdate.update(param.item).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                Notification.success($rootScope.nls["ERR401"]);
                $scope.init();
                return true;
            } else {
                console.log("dateConfiguration updated Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("dateConfiguration updated Failed : ", error)
        });
    }




    $scope.actionClick = function(param) {
        console.log(param);
        if (param.type === 'SAVE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_AGENT_PORT_MODIFY)) {
                return;
            }
            return checkAndUpdate(param);
        }

        if (param.type === 'DELETE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_AGENT_PORT_DELETE)) {
                return;
            }
            $scope.remove(param);
        }
    };



    $scope.addPage = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_AGENT_PORT_CREATE)) {
            var params = {};
            params.tab = $scope.detailTab;
            $state.go("layout.addAgentPort", params);
        }
    };


    $scope.cancel = function() {
        $state.go("layout.appConfiguration");
    };

    $scope.ajaxPortEvent = function(object, type, obj) {
        $rootScope.clientMessage = null;
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PortByTransportMode.fetch({
            "transportMode": "Air"
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.portList = data.responseObject.searchResult;
                    console.log("$scope.originList ", $scope.originList);

                    return $scope.portList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching port');
            });

    };


    $scope.ajaxPartyEvent = function(object) {
        console.log("Ajax Party Event method : " + object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partiesList = data.responseObject.searchResult;
                    return $scope.partiesList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );
    };


    $scope.ajaxServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceList = data.responseObject.searchResult;
                    return $scope.serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );
    }
    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            break;
    }

}]);