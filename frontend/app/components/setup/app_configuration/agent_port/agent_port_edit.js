app.controller("agentPortOperationCtrl",['$scope', '$state', '$rootScope', '$stateParams', '$modal', 'ngDialog', 
        'ServiceList', 'Notification', 'AgentPortSave', 'PartiesList', 'PortByTransportMode',
    function($scope, $state, $rootScope, $stateParams, $modal, ngDialog, 
        ServiceList, Notification, AgentPortSave, PartiesList, PortByTransportMode) {

    $scope.errorMap = new Map();

    $scope.init = function() {
        $scope.agentPortList = [];
        $scope.agentPortList.push({});


    }


    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });
    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.validation = function(index) {

        $scope.errorArray[index] = {};
        $scope.errorArray[index].errTextArr = [];
        var errorFound = false;

        if ($scope.agentPortList[index].serviceMaster == undefined || $scope.agentPortList[index].serviceMaster == null ||
            $scope.agentPortList[index].serviceMaster == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].serviceNameErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05202"]);
            errorFound = true;
        } else {
            if ($scope.agentPortList[index].serviceMaster.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceNameErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05203"]);
                errorFound = true;
            } else if ($scope.agentPortList[index].serviceMaster.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceNameErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05204"]);
                errorFound = true;
            }
        }

        if ($scope.agentPortList[index].agent == undefined || $scope.agentPortList[index].agent == null || $scope.agentPortList[index].agent == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].partyErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05209"]);
            errorFound = true;
        } else {
            if ($scope.agentPortList[index].agent.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].partyErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05210"]);
                errorFound = true;
            } else if ($scope.agentPortList[index].agent.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].partyErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05211"]);
                errorFound = true;
            }
        }

        if ($scope.agentPortList[index].port == undefined || $scope.agentPortList[index].port == null || $scope.agentPortList[index].port == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].portErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05212"]);
            errorFound = true;
        } else {
            if ($scope.agentPortList[index].port.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].portErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05213"]);
                errorFound = true;
            } else if ($scope.agentPortList[index].port.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].portErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05214"]);
                errorFound = true;
            }
        }


        return errorFound;

    }
    $scope.selectOnAgent = function(id) {
        if (id != undefined && id != null) {
            $rootScope.navigateToNextField(id);
        }
    }

    $scope.addAgent = function() {
        console.log("Add Row is clicked");
        $scope.firstFocus = true;
        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.agentPortList != undefined && $scope.agentPortList != null && $scope.agentPortList.length > 0) {
            for (var index = 0; index < $scope.agentPortList.length; index++) {
                if ($scope.validation(index)) {
                    listErrorFlag = true;
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                $scope.agentPortList.push({});
            }
        } else {
            $scope.agentPortList.push({});
        }

    }

    $scope.isEmptyRow = function(obj) {
        var isempty = true;
        if (!obj) {
            return isempty;
        }
        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;
            }
        }
        return isempty;
    }

    $scope.saveAgent = function() {
        console.log("Save button is pressed...");

        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.agentPortList != undefined && $scope.agentPortList != null && $scope.agentPortList.length > 0) {
            for (var index = 0; index < $scope.agentPortList.length; index++) {

                if (index == 0) {
                    if ($scope.validation(index)) {
                        listErrorFlag = true;
                    }
                } else {
                    if ($scope.isEmptyRow($scope.agentPortList[index])) {
                        $scope.agentPortList.splice(index, 1);
                        listErrorFlag = false;
                    } else {
                        if ($scope.validation(index)) {
                            listErrorFlag = true;
                        }
                    }
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                AgentPortSave.save($scope.agentPortList).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("AgentPort Saving Successful..");
                        var params = {};
                        params.tab = $stateParams != undefined ? $stateParams.tab : "";
                        Notification.success($rootScope.nls["ERR400"]);
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("AgentPort Saving Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("AgentPort Saving Failed ", error)
                });
            }
        }
    }


    $scope.deleteRow = function(index) {
        console.log('deleted ..', index);
        $scope.agentPortList.splice(index, 1);

    }

    $scope.cancel = function() {


        $scope.isDirty = $scope.agentPortForm.$dirty;
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.saveAgent();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        params.tab = $stateParams.tab;
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            params.tab = $stateParams.tab;
            $state.go("layout.appConfiguration", params);
        }
    };


    $scope.ajaxPortEvent = function(object, type, obj) {
        $rootScope.clientMessage = null;
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PortByTransportMode.fetch({
            "transportMode": "Air"
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.portList = data.responseObject.searchResult;
                    console.log("$scope.originList ", $scope.originList);

                    return $scope.portList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching port');
            });

    };

    $scope.PortRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    };

    $scope.ajaxPartyEvent = function(object) {
        console.log("Ajax Party Event method : " + object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partiesList = data.responseObject.searchResult;
                    return $scope.partiesList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );
    };

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }
    }
    $scope.ajaxServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceList = data.responseObject.searchResult;
                    return $scope.serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );

    }

    $scope.serviceRender = function(item) {
        return {
            label: item.serviceName,
            item: item
        }
    };


    if ($stateParams.action == "ADD") {
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.appConfiguration"
            },
            {
                label: "App Configuration",
                state: "layout.appConfiguration"
            },
            {
                label: "Add Agent Port",
                state: null
            }
        ];
    }


}]);