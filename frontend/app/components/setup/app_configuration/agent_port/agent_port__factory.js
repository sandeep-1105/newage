(function() {

		app.factory("AgentPortSearch",['$resource', function($resource) {
			return $resource("/api/v1/agentportmaster/get/search/keyword", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("AgentPortUpdate",['$resource', function($resource) {
			return $resource("/api/v1/agentportmaster/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
		
		
		app.factory("AgentPortSave",['$resource', function($resource) {
			return $resource("/api/v1/agentportmaster/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		app.factory("AgentPortRemove",['$resource', function($resource) {
			return $resource("/api/v1/agentportmaster/delete/:id", {}, {
				remove : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		
})();
