app.controller("app_config_edit_ctrl",['$scope', '$state', '$rootScope', '$stateParams', '$modal', 'ngDialog', 'ServiceList', 
        'Notification', 'DateConfigurationSave',
    function($scope, $state, $rootScope, $stateParams, $modal, ngDialog, ServiceList, 
        Notification, DateConfigurationSave) {

    $scope.errorMap = new Map();

    $scope.dateLogicArr = $rootScope.enum['DateLogic'];
    $scope.whichTransactionDateArr = $rootScope.enum['WhichTransactionDate'];

    $scope.init = function() {
        $scope.dateConfigurationList = [];
        $scope.dateConfigurationList.push({});


    }


    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });
    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.validation = function(index) {

        $scope.errorArray[index] = {};
        $scope.errorArray[index].errTextArr = [];
        var errorFound = false;

        if ($scope.dateConfigurationList[index].serviceMaster == undefined || $scope.dateConfigurationList[index].serviceMaster == null ||
            $scope.dateConfigurationList[index].serviceMaster == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].serviceNameErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05202"]);
            errorFound = true;
        } else {
            if ($scope.dateConfigurationList[index].serviceMaster.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceNameErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05203"]);
                errorFound = true;
            } else if ($scope.dateConfigurationList[index].serviceMaster.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceNameErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05204"]);
                errorFound = true;
            }


        }


        if ($scope.dateConfigurationList[index].whichTransactionDate == undefined || $scope.dateConfigurationList[index].whichTransactionDate == null ||
            $scope.dateConfigurationList[index].whichTransactionDate == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].whichTransactionDateErr = true;

            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05205"]);
            errorFound = true;
        }


        if ($scope.dateConfigurationList[index].dateLogic == undefined || $scope.dateConfigurationList[index].dateLogic == null ||
            $scope.dateConfigurationList[index].dateLogic == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].dateLogiceErr = true;

            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05206"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].dateLogiceErr = false;
        }




        return errorFound;

    }


    $scope.addConfig = function() {
        console.log("Add Row is clicked");
        $scope.firstFocus = true;
        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.dateConfigurationList != undefined && $scope.dateConfigurationList != null && $scope.dateConfigurationList.length > 0) {
            for (var index = 0; index < $scope.dateConfigurationList.length; index++) {
                if ($scope.validation(index)) {
                    listErrorFlag = true;
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                $scope.dateConfigurationList.push({});
            }
        } else {
            $scope.dateConfigurationList.push({});
        }

    }
    $scope.isEmptyRow = function(obj) {
        var isempty = true;
        if (!obj) {
            return isempty;
        }
        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;
            }
        }
        return isempty;
    }

    $scope.saveConfig = function() {
        console.log("Save button is pressed...", $scope.dateConfigurationList);

        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.dateConfigurationList != undefined && $scope.dateConfigurationList != null && $scope.dateConfigurationList.length > 0) {
            for (var index = 0; index < $scope.dateConfigurationList.length; index++) {
                if (index == 0) {
                    if ($scope.validation(index)) {
                        listErrorFlag = true;
                    }
                } else {
                    if ($scope.isEmptyRow($scope.dateConfigurationList[index])) {
                        $scope.dateConfigurationList.splice(index, 1);
                        listErrorFlag = false;
                    } else {
                        if ($scope.validation(index)) {
                            listErrorFlag = true;
                        }
                    }

                }
            }
            if (listErrorFlag) {
                return;
            } else {
                DateConfigurationSave.save($scope.dateConfigurationList).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("DateConfigurationList Saving Successful..");
                        Notification.success($rootScope.nls["ERR400"]);
                        var params = {};
                        params.tab = $stateParams != undefined ? $stateParams.tab : "";
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("DateConfigurationList Saving Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("DateConfigurationList Saving Failed ", error)
                });
            }
        }
    }


    $scope.deleteRow = function(index) {
        console.log('deleted ..', index);
        $scope.dateConfigurationList.splice(index, 1);

    }

    $scope.cancel = function() {


        $scope.isDirty = $scope.dateConfigForm.$dirty;
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.saveConfig();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        params.tab = $stateParams.tab;
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            params.tab = $stateParams.tab;
            $state.go("layout.appConfiguration", params);
        }
    };


    $scope.ajaxServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceList = data.responseObject.searchResult;
                    return $scope.serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );

    }

    $scope.serviceRender = function(item) {
        return {
            label: item.serviceName,
            item: item
        }
    };


    if ($stateParams.action == "ADD") {
        $scope.init();

        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.appConfiguration"
            },
            {
                label: "App Configuration",
                state: "layout.appConfiguration"
            },
            {
                label: "Add Job Date Configuration",
                state: null
            }
        ];
    }


}]);