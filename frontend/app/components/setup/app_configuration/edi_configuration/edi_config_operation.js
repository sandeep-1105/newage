app.controller("EDIConfigOperationCtrl", function($scope, $state,$rootScope,$stateParams, $modal,ngDialog, Notification, EDIConfigSave ) {

	$scope.errorMap = new Map();

	
	$scope.init = function(){
		if($scope.ediConfigList ==undefined || $scope.ediConfigList==null ){
			$scope.ediConfigList=[]
		}
			$scope.ediConfigList.push({});
			$scope.oldData = JSON.stringify($scope.ediConfigList);
	 }
	
	 
	 var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
     var errorOnRowIndex = null;

     $scope.errorShow = function(errorObj,index){
         $scope.errList = errorObj.errTextArr;
         errorOnRowIndex = index;
         myOtherModal.$promise.then(myOtherModal.show);
     };
	 
     $scope.validation = function(index){
    	 
 		$scope.errorArray[index] = {};
 		$scope.errorArray[index].errTextArr = [];
 		var errorFound = false;
    	
 		if($scope.ediConfigList[index].ediConfigurationKey == undefined || $scope.ediConfigList[index].ediConfigurationKey == null ||
				$scope.ediConfigList[index].key == "") {
			$scope.errorArray[index].errRow = true;
			$scope.errorArray[index].key = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05231"]);	
            errorFound = true;
		}
 		
 		if($scope.ediConfigList[index].ediConfigurationValue == undefined || $scope.ediConfigList[index].ediConfigurationValue == null ||
				$scope.ediConfigList[index].ediConfigurationValue == "") {
			$scope.errorArray[index].errRow = true;
			$scope.errorArray[index].value = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05232"]);	
            errorFound = true;
		}
 		return errorFound;
     }
    
     
     $scope.addEDI=function(){
    
    	 if($scope.ediConfigList ==undefined || $scope.ediConfigList==null ){
 			$scope.ediConfigList=[]
    	 }
 			$scope.ediConfigList.push({});
     }
	
     
	 $scope.isEmptyRow=function(obj){
         var isempty = true; 
         if (!obj) {
             return isempty;
         }
         var k = Object.getOwnPropertyNames(obj);
         for (var i = 0; i < k.length; i++){
             if (obj[k[i]]) {
                 isempty = false; // not empty
                 break;
             }
         }
         return isempty;
      }
	
	$scope.saveEDI = function(){
     console.log("Save button is pressed...");
    	
    	$scope.errorArray = [];
    	var listErrorFlag = false;
    	if($scope.ediConfigList != undefined && $scope.ediConfigList!= null && $scope.ediConfigList.length >0) {
    		for(var index = 0; index < $scope.ediConfigList.length; index++) {
    			
    			if(index==0){
    				if($scope.validation(index)) {
    					listErrorFlag = true;
    				}
    			}else{
    				 if($scope.isEmptyRow($scope.ediConfigList[index])){
						  $scope.ediConfigList.splice(index,1);
						  listErrorFlag = false;
					  }else{
						  if($scope.validation(index)) {
							  listErrorFlag = true;
						  }
					  }
    			 }
    		}
    		if(listErrorFlag) {
    			return;
    		}  else {
    			EDIConfigSave.save($scope.ediConfigList).$promise.then(function(data) {
         	if (data.responseCode == 'ERR0') {
				var params = {};
				params.tab=$stateParams.tab;
        		Notification.success($rootScope.nls["ERR400"]);
         		 $state.go("layout.appConfiguration",params);
         	} else {
         		console.log("Edi Config Saving Failed " , data.responseDescription)
         	}
         }, function(error) {
        	 console.log("Edi Config Saving Failed ");
         });
    		}
    	}
	}
	
	
	$scope.deleteRow = function(index){
		console.log('deleted ..',index);
		$scope.ediConfigList.splice(index, 1);
		
	}
	
	$scope.cancelEDI = function() {

		if ($scope.oldData !=  JSON.stringify($scope.ediConfigList)) {
				var newScope = $scope.$new();
				newScope.errorMessage = $rootScope.nls["ERR200"];
				ngDialog.openConfirm({
					template: '<p>{{errorMessage}}</p>' +
					'<div class="ngdialog-footer">' +
						' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
						'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
						'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
					'</div>',
					plain: true,
					closeByDocument : false,
					scope: newScope,
					className: 'ngdialog-theme-default'
				}).then(
								function(value) {
									if (value == 1) {
										$scope.save();
									} else if (value == 2) {
										var params = {};
										params.tab=$stateParams.tab;
										params.submitAction = 'Cancelled';
										$state.go("layout.appConfiguration",params);
									} else {
										console.log("cancelled");
									}
								});
			} 
		 else {
			var params = {};
			params.submitAction = 'Cancelled';
			params.tab=$stateParams.tab;
			$state.go("layout.appConfiguration",params);
		}
	};
	
	
	
	
	  if ($stateParams.action == "ADD") {
		  $scope.init();
		  $rootScope.breadcrumbArr = [{label: "Setup",state: "layout.appConfiguration"},
			                      	    {label: "App Configuration",state: "layout.appConfiguration"},
			                      	    {label: "Add EDI Configuration",state: null}];
	    }

	  
});
