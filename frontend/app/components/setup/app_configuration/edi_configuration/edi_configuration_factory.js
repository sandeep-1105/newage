(function() {

		app.factory("EdiConfigSearch",['$resource', function($resource) {
			return $resource("/api/v1/ediconfiguration/get/search/:locationId", {}, {
				fetch : {
					method : 'POST',
					params : {
						locationId : ''
					},
					isArray : false
				}
			});
		}]);
		
		app.factory("EDIConfigSave",['$resource', function($resource) {
			return $resource("/api/v1/ediconfiguration/create", {}, {
			save : {
					method : 'POST'
				}
			});
		}]);
		
		
		app.factory("EdiConfigUpdate",['$resource', function($resource) {
			return $resource("/api/v1/ediconfiguration/update", {
				save : {
					method : 'POST'
				}
			});
		}]);
		
		
		
})();
