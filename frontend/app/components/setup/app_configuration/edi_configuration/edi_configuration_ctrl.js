app.controller("ediConfiguratinCtrl", ['$scope', '$rootScope', '$stateParams', '$state', 'Notification', 'roleConstant', 'EdiConfigSearch', 'EdiConfigUpdate',
    function($scope, $rootScope, $stateParams, $state, Notification, roleConstant, EdiConfigSearch, EdiConfigUpdate) {

        $scope.editConfigHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",

            "model": "no",
            "search": false
        }, {
            "name": "Key",
            "search": true,
            "model": "ediConfigurationKey",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "col-xs-4 col-sm-4 col-md-4",

            "key": "searchEdiKey"
        }, {
            "name": "Value",
            "model": "ediConfigurationValue",
            "search": true,
            "wrap_cell": true,
            "type": "input",
            "sort": true,
            "width": "col-xs-4 col-sm-4 col-md-4",

            "key": "searchEdiValue"
        }, {
            "name": "Description",
            "model": "description",
            "search": true,
            "wrap_cell": true,
            "type": "input",
            "sort": true,
            "width": "col-xs-3 col-sm-3 col-md-3",

            "key": "searchDescription"
        }, {
            "name": "Action",
            "model": "",
            "search": false,
            "wrap_cell": true,
            "type": "action",
            "actype": "action",
            "sort": false,
            "width": "col-xs-2 col-sm-2 col-md-3",

            "key": ""
        }];

        $scope.init = function() {
            $scope.searchDto = {};
            $scope.limitArr = [10, 15, 20];
            $scope.page = 0;
            $scope.limit = 10;
            $scope.totalRecord = 0;
            $scope.search();
        }

        $scope.search = function() {
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            EdiConfigSearch.fetch({
                "locationId": $rootScope.userProfile.selectedUserLocation.id
            }, $scope.searchDto).$promise.then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.ediConfigurationArray = resultArr;
            });
        }

        $scope.sortSelection = {
            sortKey: "ediConfigurationKey",
            sortOrder: "asc"
        }

        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.changeSearch = function(param) {
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.search();
        }

        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        }

        $scope.changepage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }

        $scope.validation = function(param) {
            var errorFlag = true;
            if (param != undefined && param != null && param.item != undefined && param.item != null) {

            }
            return errorFlag;
        }

        var checkAndUpdate = function(param) {
            if ($scope.validation(param)) {
                $scope.update(param);
            } else {
                console.log('Found error while validating app config.');
                return false;
            }
        }


        $scope.update = function(param) {
            EdiConfigUpdate.save(param.item).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    Notification.success($rootScope.nls["ERR401"]);
                    $scope.init();
                    return true;
                } else {
                    Notification.error(data.responseDescription);
                    console.log("ediConfiguration updated Failed ", data.responseDescription)
                }
            }, function(error) {
                Notification.error(error);
                console.log("ediConfiguration updated Failed : ", error)
            });
        }

        $scope.actionClick = function(param) {
            if (param.type === 'SAVE') {
                if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_AGENT_PORT_MODIFY)) {
                    return;
                }
                return checkAndUpdate(param);
            }
        };


        $scope.addEDIPage = function() {

            if ($rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_EDI_CONFIG_CREATE)) {
                var params = {};
                params.tab = $scope.detailTab;
                $state.go("layout.addEdiConfiguration", params);
            }
        }



        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                break;
        }

    }
]);