(function() {

		app.factory("DateConfigurationSearch",['$resource', function($resource) {
			return $resource("/api/v1/dateconfiguration/get/search", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("DateConfigurationUpdate",['$resource', function($resource) {
			return $resource("/api/v1/dateconfiguration/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
		
		
		app.factory("DateConfigurationSave",['$resource', function($resource) {
			return $resource("/api/v1/dateconfiguration/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		app.factory("DateConfigurationRemove",['$resource', function($resource) {
			return $resource("/api/v1/dateconfiguration/delete/:id", {}, {
				remove : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		
})();
