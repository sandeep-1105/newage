(function() {

		app.factory("SurchargeMappingSearch",['$resource', function($resource) {
			return $resource("/api/v1/surchargemapping/get/search/keyword", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("SurchargeMappingUpdate",['$resource', function($resource) {
			return $resource("/api/v1/surchargemapping/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
		
		
		app.factory("SurchargeMappingSave",['$resource', function($resource) {
			return $resource("/api/v1/surchargemapping/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		app.factory("SurchargeMappingRemove",['$resource', function($resource) {
			return $resource("/api/v1/surchargemapping/delete/:id", {}, {
				remove : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		
})();
