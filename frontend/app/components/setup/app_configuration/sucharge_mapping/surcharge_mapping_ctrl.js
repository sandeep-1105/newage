app.controller("surchargeMappingCtrl",['$scope', '$rootScope',
    '$modal', 'ngDialog', '$stateParams', '$state', 'Notification', 'ChargeSearchKeyword',
    'SurchargeMappingUpdate', 'SurchargeMappingRemove',
    'SurchargeMappingSearch', 'roleConstant', function($scope, $rootScope,
    $modal, ngDialog, $stateParams, $state, Notification, ChargeSearchKeyword,
    SurchargeMappingUpdate, SurchargeMappingRemove,
    SurchargeMappingSearch, roleConstant) {

    $scope.init = function() {
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search();
    }

    $scope.search = function() {
        console.log("Search is called....");
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        SurchargeMappingSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.surchargeMappingArray = resultArr;
        });
    }
    $scope.surchargeTypeArr = $rootScope.enum['SurchargeType'];
    $scope.surchargeMappingHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",

            "model": "no",
            "search": false
        },
        {
            "name": "Charge",
            "search": true,
            "model": "chargeMaster",
            "wrap_cell": true,
            "type": "lov",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "key": "searchChargeName",
            "lov": {
                selName: 'chargeName',
                viewKey: 'chargeMaster.chargeName',
                ajaxFn: 'ajaxChargeEvent(param)'
            }
        },
        {
            "name": "Surcharge Type",
            "model": "surchargeType",
            "search": true,
            "wrap_cell": true,
            "type": "select",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "data": $scope.surchargeTypeArr,
            "key": "searchSurchargeType"
        }, {
            "name": "Action",
            "model": "",
            "search": false,
            "wrap_cell": true,
            "type": "action",
            "actype": "action",
            "sort": false,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "key": ""
        }
    ];

    $scope.sortSelection = {
        sortKey: "chargeMaster",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        console.log("change search", $scope.searchDto);
        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        if (param.sortKey.includes("chargeMaster")) {
            $scope.searchDto.sortByColumn = "chargeMaster.chargeName"
        }
        $scope.search();
    }

    $scope.changepage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.remove = function(param) {
        console.log("Remove ", param);
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR06288"];
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button></div>',
            plain: true,
            scope: newScope,
            closeByDocument: false,
            className: 'ngdialog-theme-default'
        }).then(function(value) {
            SurchargeMappingRemove.remove({
                id: param.item.id
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Surcharge Mapping deleted Successfully");
                    $scope.init();
                    Notification.error($rootScope.nls["ERR402"]);
                } else {
                    console.log("Surcharge Mapping delete Failed ", data.responseDescription);
                }
            }, function(error) {
                console.log("Surcharge Mapping delete Failed : ", error);
            });
        }, function(value) {
            console.log("Surcharge Mapping delete Cancelled");
        });
    }


    $scope.validation = function(param) {
        var errorFlag = true;
        if (param != undefined && param != null && param.item != undefined && param.item != null) {
            if (param.item.chargeMaster.status == 'Block') {
                Notification.error($rootScope.nls["ERR06290"]);
                errorFlag = false;
            } else if (param.item.chargeMaster.status == 'Hide') {
                Notification.error($rootScope.nls["ERR06291"]);
                errorFlag = false;
            }

        }
        return errorFlag;
    }

    var checkAndUpdate = function(param) {
        console.log("checkAndUpdate ", param);
        if ($scope.validation(param)) {
            $scope.update(param);
        } else {
            console.log('Found error while validating app config.');
            return false;
        }
    }


    $scope.update = function(param) {
        console.log("Update ", param);
        SurchargeMappingUpdate.update(param.item).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                Notification.success($rootScope.nls["ERR401"]);
                $scope.init();
                return true;
            } else {
                console.log("dateConfiguration updated Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("dateConfiguration updated Failed : ", error)
        });
    }




    $scope.actionClick = function(param) {
        console.log(param);
        if (param.type === 'SAVE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SURCHARGE_MAPPING_MODIFY)) {
                return;
            }
            return checkAndUpdate(param);
        }

        if (param.type === 'DELETE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SURCHARGE_MAPPING_DELETE)) {
                return;
            }
            $scope.remove(param);
        }
    };



    $scope.addPage = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SURCHARGE_MAPPING_CREATE)) {
            var params = {};
            params.tab = $scope.detailTab;
            $state.go("layout.addSurchargeMapping", params);
        }
    };


    $scope.cancel = function() {
        $state.go("layout.appConfiguration");
    };

    $scope.ajaxChargeEvent = function(object) {
        console.log('ajaxChargeEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ChargeSearchKeyword.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.chargeList = data.responseObject.searchResult;
                        return $scope.chargeList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Charge');
                });
    }
    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            break;
    }

}]);