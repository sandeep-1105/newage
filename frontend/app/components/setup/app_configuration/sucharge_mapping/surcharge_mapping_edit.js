app.controller("surchargeMappingOperationCtrl",['$scope', '$state', '$rootScope', '$stateParams',
    '$modal', 'ngDialog', 'ChargeSearchKeyword', 'Notification', 'SurchargeMappingSave',
    function($scope, $state, $rootScope, $stateParams,
    $modal, ngDialog, ChargeSearchKeyword, Notification, SurchargeMappingSave) {

    $scope.errorMap = new Map();

    $scope.init = function() {
        $scope.surchargemappingList = [];
        $scope.surchargemappingList.push({});
    }


    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });
    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.validation = function(index) {
        $scope.errorArray[index] = {};
        $scope.errorArray[index].errTextArr = [];
        var errorFound = false;

        if ($scope.surchargemappingList[index].chargeMaster == undefined ||
            $scope.surchargemappingList[index].chargeMaster == null ||
            $scope.surchargemappingList[index].chargeMaster == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].chargeNameErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06289"]);
            errorFound = true;
        } else {
            if ($scope.surchargemappingList[index].chargeMaster.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].chargeNameErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06290"]);
                errorFound = true;
            } else if ($scope.surchargemappingList[index].chargeMaster.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].chargeNameErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06291"]);
                errorFound = true;
            }
        }

        if ($scope.surchargemappingList[index].surchargeType == undefined ||
            $scope.surchargemappingList[index].surchargeType == null ||
            $scope.surchargemappingList[index].surchargeType == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].surchargeTypeErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06293"]);
            errorFound = true;
        }


        return errorFound;

    }


    $scope.addSurchargeMapping = function() {
        console.log("addSuchargeMapping is clicked");
        $scope.firstFocus = true;
        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.surchargemappingList != undefined && $scope.surchargemappingList != null && $scope.surchargemappingList.length > 0) {
            for (var index = 0; index < $scope.surchargemappingList.length; index++) {
                if ($scope.validation(index)) {
                    listErrorFlag = true;
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                $scope.surchargemappingList.push({});
            }
        } else {
            $scope.surchargemappingList.push({});
        }

    }

    $scope.isEmptyRow = function(obj) {
        var isempty = true;
        if (!obj) {
            return isempty;
        }
        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;
            }
        }
        return isempty;
    }

    $scope.saveSurchargeMapping = function() {
        console.log("Save button is pressed...");
        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.surchargemappingList != undefined && $scope.surchargemappingList != null &&
            $scope.surchargemappingList.length > 0) {
            for (var index = 0; index < $scope.surchargemappingList.length; index++) {

                if (index == 0) {
                    if ($scope.validation(index)) {
                        listErrorFlag = true;
                    }
                } else {
                    if ($scope.isEmptyRow($scope.surchargemappingList[index])) {
                        $scope.surchargemappingList.splice(index, 1);
                        listErrorFlag = false;
                    } else {
                        if ($scope.validation(index)) {
                            listErrorFlag = true;
                        }
                    }
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                SurchargeMappingSave.save($scope.surchargemappingList).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        Notification.success($rootScope.nls["ERR400"]);
                        var params = {};
                        params.tab = $stateParams.tab;
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("SurchargeMapping Saving Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("SurchargeMapping Saving Failed ", error)
                });
            }
        }
    }


    $scope.deleteRow = function(index) {
        console.log('deleted ..', index);
        $scope.surchargemappingList.splice(index, 1);

    }

    $scope.cancel = function() {
        $scope.isDirty = $scope.surchargemappingForm.$dirty;
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.saveSurchargeMapping();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        params.tab = $stateParams.tab;
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            params.tab = $stateParams.tab;
            $state.go("layout.appConfiguration", params);
        }
    };


    $scope.ajaxChargeEvent = function(object) {
        console.log('ajaxChargeEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ChargeSearchKeyword.query($scope.searchDto).$promise
            .then(
                function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.chargeList = data.responseObject.searchResult;
                        return $scope.chargeList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Charge');
                });
    }

    $scope.chargeRender = function(item) {
        return {
            label: item.chargeName,
            item: item
        }
    };


    if ($stateParams.action == "ADD") {
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.appConfiguration"
            },
            {
                label: "App Configuration",
                state: "layout.appConfiguration"
            },
            {
                label: "Add Surcharge Mapping",
                state: null
            }
        ];
    }


}]);