app.controller("ServiceMappingCtrl",['$scope', '$rootScope', '$modal', 'ngDialog', '$stateParams', '$state', 'Notification', 
        'ServiceList', 'ServiceMappingSearch', 'ServiceMappingUpdate', 'ServiceMappingDelete', 'roleConstant',
    function($scope, $rootScope, $modal, ngDialog, $stateParams, $state, Notification, 
        ServiceList, ServiceMappingSearch, ServiceMappingUpdate, ServiceMappingDelete, roleConstant) {

    $scope.init = function() {
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search();
    }

    $scope.serviceMappingHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",

        "model": "no",
        "search": false
    }, {
        "name": "Import",
        "search": true,
        "model": "importService",
        "wrap_cell": true,
        "type": "lov",
        "sort": true,
        "width": "col-sm-4",

        "key": "searchImport",
        "lov": {
            selName: 'serviceName',
            viewKey: 'importService.serviceName',
            ajaxFn: 'ajaxImportServiceEvent(param)',
            selectedFn: 'selectedImportService(param)'

        }
    }, {
        "name": "Export",
        "search": true,
        "model": "exportService",
        "wrap_cell": true,
        "type": "lov",
        "sort": true,
        "width": "col-sm-4",

        "key": "searchExport",
        "lov": {
            selName: 'serviceName',
            viewKey: 'exportService.serviceName',
            ajaxFn: 'ajaxExportServiceEvent(param)',
            selectedFn: 'selectedExportService(param)'
        }
    }, {
        "name": "Action",
        "model": "",
        "search": false,
        "wrap_cell": true,
        "type": "action",
        "actype": "action",
        "sort": false,
        "width": "col-sm-4",

        "key": ""
    }];

    var importServiceSelected = null;
    $scope.selectedImportService = function(param) {
        importServiceSelected = param.id;
    }

    var exportServiceSelected = null;
    $scope.selectedExportService = function(param) {
        exportServiceSelected = param.id;
    }

    $scope.sortSelection = {
        sortKey: "importService",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        console.log("change search", $scope.searchDto);
        $scope.search();
    }

    $scope.search = function() {
        console.log("Search is called....");
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        ServiceMappingSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.serviceMappingArray = resultArr;
        });
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        if (param.sortKey.includes("importService")) {
            $scope.searchDto.sortByColumn = "importService.serviceName"
        } else {
            $scope.searchDto.sortByColumn = "exportService.serviceName"
        }
        //$scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.changepage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.addServiceMapping = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SERVICE_MAPPING_CREATE)) {
            var params = {};
            params.tab = $scope.detailTab;
            $state.go('layout.addserviceMapping', params);
        }
    }




    $scope.validation = function(param) {
        var errorFlag = true;
        if (param != undefined && param != null && param.item != undefined && param.item != null) {
            if (param.item.importService.id == undefined) {
                Notification.error($rootScope.nls["ERR05223"]);
                errorFlag = false;
            }

            if (param.item.exportService.id == undefined) {
                Notification.error($rootScope.nls["ERR05224"]);
                errorFlag = false;
            }
            if (param.item.importService != undefined || param.item.importService != null || param.item.importService != "") {
                if (param.item.importService.status == 'Block') {
                    Notification.error($rootScope.nls["ERR05203"]);
                    errorFlag = false;
                } else if (param.item.importService.status == 'Hide') {
                    Notification.error($rootScope.nls["ERR05204"]);
                    errorFlag = false;
                }
            }

            if (param.item.exportService != undefined || param.item.exportService != null || param.item.exportService != "") {

                if (param.item.exportService.status == 'Block') {
                    Notification.error($rootScope.nls["ERR05203"]);
                    errorFlag = false;
                } else if (param.item.exportService.status == 'Hide') {
                    Notification.error($rootScope.nls["ERR05204"]);
                    errorFlag = false;
                }

            }

        }
        return errorFlag;
    }

    var checkAndUpdate = function(param) {
        console.log("checkAndUpdate ", param);
        if ($scope.validation(param)) {
            $scope.update(param);
        } else {
            console.log('Found error while validating ServiceMapping.');
            return false;
        }
    }


    $scope.update = function(param) {
        console.log("Update ", param);
        ServiceMappingUpdate.update(param.item).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                Notification.success($rootScope.nls["ERR401"]);
                $scope.init();
                return true;
            } else {
                console.log("ServiceMapping updated Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("ServiceMapping updated Failed : ", error)
        });
    }

    $scope.actionClick = function(param) {
        if (param.type === 'SAVE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SERVICE_MAPPING_MODIFY)) {
                return;
            }
            return checkAndUpdate(param);
        }

        if (param.type === 'DELETE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SERVICE_MAPPING_DELETE)) {
                return;
            }
            $scope.remove(param);
        }
        console.log("param", param);
    };

    $scope.remove = function(param) {
        console.log("Remove ", param);
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR05222"];
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button></div>',
            plain: true,
            scope: newScope,
            closeByDocument: false,
            className: 'ngdialog-theme-default'
        }).then(function(value) {
            ServiceMappingDelete.remove({
                id: param.item.id
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Service mapping  deleted Successfully");
                    $scope.init();
                    Notification.success($rootScope.nls["ERR402"]);
                } else {
                    console.log("Service mapping  delete Failed ", data.responseDescription);
                }
            }, function(error) {
                console.log("Service mapping  delete Failed : ", error);
            });
        }, function(value) {
            console.log("delete Cancelled");
        });
    }


    var filterService = function(array, id) {
        for (var i in array) {
            if (array[i].id == id) {
                array.splice(i, 1);
                break;
            }
        }
        return array;
    }

    $scope.ajaxImportServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    serviceList = data.responseObject.searchResult;
                    if (exportServiceSelected != null) {
                        serviceList = filterService(serviceList, exportServiceSelected)
                    }
                    return serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );
    }

    $scope.ajaxExportServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    serviceList = data.responseObject.searchResult;
                    if (importServiceSelected != null) {
                        serviceList = filterService(serviceList, importServiceSelected)
                    }
                    return serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );
    }

    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            break;
    }

}]);