app.controller("ServiceMappingOperationCtrl",['$scope', '$state', '$rootScope', '$stateParams', '$modal', 'ngDialog', 
        'ServiceList', 'Notification', 'ServiceMappingSave',
    function($scope, $state, $rootScope, $stateParams, $modal, ngDialog, 
        ServiceList, Notification, ServiceMappingSave) {

    $scope.errorMap = new Map();

    $scope.init = function() {
        $scope.serviceMappingList = [];
        $scope.serviceMappingList.push({});
        $scope.oldData = JSON.stringify($scope.serviceMappingList);

    }


    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });
    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.validation = function(index) {

        $scope.errorArray[index] = {};
        $scope.errorArray[index].errTextArr = [];
        var errorFound = false;

        if ($scope.serviceMappingList[index].importService == undefined || $scope.serviceMappingList[index].importService == null ||
            $scope.serviceMappingList[index].importService == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].serviceImportErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05225"]);
            errorFound = true;
        } else {
            if ($scope.serviceMappingList[index].importService.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceImportErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05227"]);
                errorFound = true;
            } else if ($scope.serviceMappingList[index].importService.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceImportErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05228"]);
                errorFound = true;
            }
        }
        if ($scope.serviceMappingList[index].exportService == undefined || $scope.serviceMappingList[index].exportService == null ||
            $scope.serviceMappingList[index].exportService == "") {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].serviceExportErr = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05226"]);
            errorFound = true;
        } else {
            if ($scope.serviceMappingList[index].exportService.status == 'Block') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceExportErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05229"]);
                errorFound = true;
            } else if ($scope.serviceMappingList[index].exportService.status == 'Hide') {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceExportErr = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05230"]);
                errorFound = true;
            }
        }


        return errorFound;

    }


    $scope.addServiceMapping = function() {
        console.log("Add Row is clicked");
        $scope.firstFocus = true;
        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.serviceMappingList != undefined && $scope.serviceMappingList != null && $scope.serviceMappingList.length > 0) {
            for (var index = 0; index < $scope.serviceMappingList.length; index++) {
                if ($scope.validation(index)) {
                    listErrorFlag = true;
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                $scope.serviceMappingList.push({});
            }
        } else {
            $scope.serviceMappingList.push({});
        }

    }

    $scope.isEmptyRow = function(obj) {
        var isempty = true;
        if (!obj) {
            return isempty;
        }
        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;
            }
        }
        return isempty;
    }

    $scope.save = function() {
        console.log("Save button is pressed...");

        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.serviceMappingList != undefined && $scope.serviceMappingList != null && $scope.serviceMappingList.length > 0) {
            for (var index = 0; index < $scope.serviceMappingList.length; index++) {

                if (index == 0) {
                    if ($scope.validation(index)) {
                        listErrorFlag = true;
                    }
                } else {
                    if ($scope.isEmptyRow($scope.serviceMappingList[index])) {
                        $scope.serviceMappingList.splice(index, 1);
                        listErrorFlag = false;
                    } else {
                        if ($scope.validation(index)) {
                            listErrorFlag = true;
                        }
                    }
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                ServiceMappingSave.save($scope.serviceMappingList).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("serviceMapping Saving Successful..");
                        var params = {};
                        params.tab = $stateParams.tab;
                        Notification.success($rootScope.nls["ERR400"]);
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("serviceMapping Saving Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("serviceMapping Saving Failed ", error)
                });
            }
        }
    }


    $scope.deleteRow = function(index) {
        console.log('deleted ..', index);
        $scope.serviceMappingList.splice(index, 1);

    }

    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.serviceMappingList)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.save();
                    } else if (value == 2) {
                        var params = {};
                        params.tab = $stateParams.tab;
                        params.submitAction = 'Cancelled';
                        $state.go("layout.appConfiguration", params);
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            params.tab = $stateParams.tab;
            $state.go("layout.appConfiguration", params);
        }
    };
    $scope.filterService = function(array, id) {
        for (var i in array) {
            if (array[i].id == id) {
                array.splice(i, 1);
                break;
            }
        }
        return array;
    }

    $scope.ajaxServiceEvent = function(object, type, obj) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceList = data.responseObject.searchResult;

                    if (obj.importService != undefined && obj.importService != null && obj.importService != "") {
                        $scope.serviceList = $scope.filterService($scope.serviceList, obj.importService.id);
                    }

                    if (obj.exportService != undefined && obj.exportService != null && obj.exportService != "") {
                        $scope.serviceList = $scope.filterService($scope.serviceList, obj.exportService.id);
                    }


                    return $scope.serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );

    }

    if ($stateParams.action == "ADD") {
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.appConfiguration"
            },
            {
                label: "App Configuration",
                state: "layout.appConfiguration"
            },
            {
                label: "Add Service Mapping",
                state: null
            }
        ];
    }


}]);