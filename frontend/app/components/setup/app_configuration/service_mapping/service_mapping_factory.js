(function() {

		app.factory("ServiceMappingSearch",['$resource', function($resource) {
			return $resource("/api/v1/servicemapping/get/search/keyword", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("ServiceMappingSave",['$resource', function($resource) {
			return $resource("/api/v1/servicemapping/create", {}, {
			save : {
					method : 'POST'
				}
			});
		}]);
		app.factory("ServiceMappingUpdate",['$resource', function($resource) {
			return $resource("/api/v1/servicemapping/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
		app.factory("ServiceMappingDelete",['$resource', function($resource) {
			return $resource("/api/v1/servicemapping/delete/:id", {}, {
				remove : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
})();
