app.controller("app_config_ctrl",['$scope', '$rootScope', '$modal', 'ngDialog', '$stateParams', '$state', 'Notification', 'ServiceList',
    'DateConfigurationUpdate', 'DateConfigurationRemove', 'DateConfigurationSearch', 'roleConstant', 
    function($scope, $rootScope, $modal, ngDialog, $stateParams, $state, Notification, ServiceList,
    DateConfigurationUpdate, DateConfigurationRemove, DateConfigurationSearch, roleConstant) {

    $scope.$roleConstant = roleConstant;
    $scope.init = function() {
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search();

    };

    $scope.clickOnTab = function(tab) {


        if (tab == 'jobDate' && $rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_JOB_DATE_LIST)) {
            $scope.detailTab = 'jobDate';
        } else if (tab == 'agentPort' && $rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_AGENT_PORT_LIST)) {
            $scope.detailTab = 'agentPort';
        } else if (tab == 'surchargeMapping' && $rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SURCHARGE_MAPPING_LIST)) {
            $scope.detailTab = 'surchargeMapping';
        } else if (tab == 'enquiryServiceMapping' && $rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_ENQUIRY_SERVICE_MAPPING_LIST)) {
            $scope.detailTab = 'enquiryServiceMapping';
        } else if (tab == 'serviceMapping' && $rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SERVICE_MAPPING_LIST)) {
            $scope.detailTab = 'serviceMapping';
        } else if (tab == 'ediConfiguration' && $rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_SERVICE_MAPPING_LIST)) {
            $scope.detailTab = 'ediConfiguration';
        }
        $scope.init();
    }
    $scope.search = function() {
        console.log("Search is called....");

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        DateConfigurationSearch.query($scope.searchDto).$promise.then(function(data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;

            var tempArr = [];
            var resultArr = [];

            tempArr = data.responseObject.searchResult;

            var tempObj = {};

            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });

            $scope.appConfigArray = resultArr;
        });
    }

    $scope.dateTypeArr = $rootScope.enum['DateLogic'];
    $scope.screenTypeArr = $rootScope.enum['WhichTransactionDate'];
    //$scope.dateTypeArr = ["ETD", "ATD", "SYS Date"];
    //$scope.screenTypeArr = ["Shipment", "Service", "Master"];
    $scope.appConfigHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",

            "model": "no",
            "search": false
        },
        {
            "name": "Service",
            "search": true,
            "model": "serviceMaster",
            "wrap_cell": true,
            "type": "lov",
            "sort": true,
            "width": "col-xs-3 col-sm-3 col-md-3",

            "key": "serviceName",
            "lov": {
                selName: 'serviceName',
                viewKey: 'serviceMaster.serviceName',
                ajaxFn: 'ajaxServiceEvent(param)'
            }
        },
        {
            "name": "Screen",
            "model": "whichTransactionDate",
            "search": true,
            "wrap_cell": true,
            "type": "select",
            "sort": true,
            "width": "col-xs-3 col-sm-3 col-md-3",

            "data": $scope.screenTypeArr,
            "key": "whichTransaction"
        },
        {
            "name": "Date Type",
            "model": "dateLogic",
            "search": true,
            "wrap_cell": true,
            "type": "select",
            "sort": true,
            "width": "col-xs-3 col-sm-3 col-md-3",

            "data": $scope.dateTypeArr,
            "key": "dateLogic"
        },
        {
            "name": "Action",
            "model": "",
            "search": false,
            "wrap_cell": true,
            "type": "action",
            "actype": "action",
            "sort": false,
            "width": "col-xs-3",
            "prefWidth": "",
            "key": ""
        }
    ];
    /*$scope.appConfigArray = [
	{
		"no" : "1",
		"service" : {
			serviceTypeName:"Clearance"
		},
		"screen" : "Shipment",
		"dateType" : "ETD"
	},
	{
		"no" : "2",
		"service" : {
			serviceTypeName:"Haulage"
		},
		"screen" : "Service",
		"dateType" : "ATD"
	},
	{
		"no" : "3",
		"service" : {
			serviceTypeName:"Roll On Roll Off"
		},
		"screen" : "Master",
		"dateType" : "SYS Date"
	}
];
*/

    $scope.sortSelection = {
        sortKey: "serviceMaster",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.changeSearch = function(param) {

        console.log("Change Search", param);

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        $scope.page = 0;
        console.log("change search", $scope.searchDto);

        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);

        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;

        $scope.search();
    }

    $scope.changepage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.remove = function(param) {
        console.log("Remove ", param);
        var newScope = $scope.$new();
        newScope.errorMessage = $rootScope.nls["ERR05207"];
        ngDialog.openConfirm({
            template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button></div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).then(function(value) {
            DateConfigurationRemove.remove({
                id: param.item.id
            }, function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("DateConfiguration  deleted Successfully");
                    Notification.error($rootScope.nls["ERR402"]);
                    $scope.init();

                } else {
                    console.log("DateConfiguration delete Failed ", data.responseDescription);
                }
            }, function(error) {
                console.log("DateConfiguration Code delete Failed : ", error);
            });
        }, function(value) {
            console.log("delete Cancelled");
        });

    }


    $scope.validation = function(param) {
        var errorFlag = true;
        if (param != undefined && param != null && param.item != undefined && param.item != null) {
            if (param.item.serviceMaster.status == undefined || param.item.serviceMaster.status == 'Block') {
                Notification.error($rootScope.nls["ERR05203"]);
                errorFlag = false;
            } else if (param.item.serviceMaster.status == 'Hide') {
                Notification.error($rootScope.nls["ERR05204"]);
                errorFlag = false;
            }
        }
        return errorFlag;
    }

    var checkAndUpdate = function(param) {
        console.log("checkAndUpdate ", param);
        if ($scope.validation(param)) {
            $scope.update(param);
        } else {
            console.log('Found error while validating app config.');
            return false;
        }


    }


    $scope.update = function(param) {
        console.log("Update ", param);
        DateConfigurationUpdate.update(param.item).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                Notification.success($rootScope.nls["ERR401"]);
                $scope.init();
                return true;
            } else {
                console.log("dateConfiguration updated Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("dateConfiguration updated Failed : ", error)
        });
    }




    $scope.actionClick = function(param) {
        console.log(param);
        if (param.type === 'SAVE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_JOB_DATE_MODIFY)) {
                return;
            }
            return checkAndUpdate(param);
        }

        if (param.type === 'DELETE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_JOB_DATE_DELETE)) {
                return;
            }
            $scope.remove(param);
        }
    };



    $scope.addPage = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_JOB_DATE_CREATE)) {
            var params = {};
            params.tab = $scope.detailTab;
            $state.go("layout.addappConfiguration", params);
        }
    };


    $scope.ajaxServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceList = data.responseObject.searchResult;
                    return $scope.serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );
    }

    switch ($stateParams.action) {
        case "SEARCH":
            console.log("DateConfiguation Search Page....");
            if ($stateParams.tab != undefined) {
                $scope.detailTab = $stateParams.tab;
            } else {
                $scope.detailTab = 'jobDate';
            }
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Setup",
                    state: "layout.appConfiguration"
                },
                {
                    label: "App Configuration",
                    state: "layout.appConfiguration"
                }
            ];
            break;
    }
}]);