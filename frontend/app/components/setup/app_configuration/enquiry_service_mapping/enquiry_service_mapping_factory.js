(function() {

		app.factory("EnquiryServiceMappingSearch",['$resource', function($resource) {
			return $resource("/api/v1/enquiryservicemapping/get/search/keyword", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		app.factory("EnquiryServiceMappingUpdate",['$resource', function($resource) {
			return $resource("/api/v1/enquiryservicemapping/save", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);
		
})();
