app.controller("EnquiryServiceMappingCtrl",['$scope', '$rootScope', '$modal', 'ngDialog', '$stateParams', '$state', 
        'Notification', 'ServiceList', 'EnquiryServiceMappingSearch',
        'EnquiryServiceMappingUpdate', 'roleConstant',
    function($scope, $rootScope, $modal, ngDialog, $stateParams, $state, 
        Notification, ServiceList, EnquiryServiceMappingSearch,
        EnquiryServiceMappingUpdate, roleConstant) {

    $scope.init = function() {
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 0;
        $scope.search();
    }

    $scope.serviceArr = $rootScope.enum['Service']
    $scope.importExportArr = $rootScope.enum['ImportExport']


    $scope.enquiryServiceMappingHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",

            "model": "no",
            "search": false
        },
        {
            "name": "Transport",
            "search": false,
            "model": "service",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "data": $scope.serviceArr,
            "key": "searchService",

        }, {
            "name": "Trade",
            "search": false,
            "model": "importExport",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-4",

            "data": $scope.importExportArr,
            "key": "searchImportExport"
        }, {
            "name": "Service",
            "search": true,
            "model": "serviceMaster",
            "wrap_cell": true,
            "type": "lov",
            "sort": true,
            "width": "col-xs-3 col-sm-4 col-md-5",

            "key": "searchTransport",
            "lov": {
                selName: 'serviceName',
                viewKey: 'serviceMaster.serviceName',
                ajaxFn: 'ajaxServiceEvent(param)'
            }
        },
        {
            "name": "Action",
            "model": "",
            "search": false,
            "wrap_cell": true,
            "type": "action",
            "actype": "action",
            "sort": false,
            "width": "col-xs-2 col-sm-2 col-md-2",

            "key": ""
        }
    ];

    $scope.sortSelection = {
        sortKey: "service",
        sortOrder: "asc"
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        console.log("change search", $scope.searchDto);
        $scope.search();
    }

    $scope.search = function() {
        console.log("Search is called....");
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        EnquiryServiceMappingSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.enquiryServiceMappingArray = resultArr;
        });
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        if (param.sortKey.includes("service")) {
            $scope.searchDto.sortByColumn = "service"
        } else if (param.sortKey.includes("importExport")) {
            $scope.searchDto.sortByColumn = "importExport"
        } else {
            $scope.searchDto.sortByColumn = "serviceMaster.serviceName"
        }
        //$scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.changepage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }




    $scope.validation = function(param) {
        var errorFlag = true;
        if (param != undefined && param != null && param.item != undefined && param.item != null) {
            if (param.item.serviceMaster.status == 'Block') {
                Notification.error($rootScope.nls["ERR05203"]);
                errorFlag = false;
            } else if (param.item.serviceMaster.status == 'Hide') {
                Notification.error($rootScope.nls["ERR05204"]);
                errorFlag = false;
            } else if (param.item.serviceMaster.id == undefined) {
                Notification.error($rootScope.nls["ERR05218"]);
                errorFlag = false;
            }
        }
        return errorFlag;
    }

    var checkAndUpdate = function(param) {
        console.log("checkAndUpdate ", param);
        if ($scope.validation(param)) {
            $scope.update(param);
        } else {
            console.log('Found error while validating EnquiryServiceMapping.');
            return false;
        }
    }


    $scope.update = function(param) {
        console.log("Update ", param);
        EnquiryServiceMappingUpdate.update(param.item).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
                Notification.success($rootScope.nls["ERR401"]);
                $scope.init();
                return true;
            } else {
                console.log("EnquiryServiceMapping updated Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("EnquiryServiceMapping updated Failed : ", error)
        });
    }

    $scope.actionClick = function(param) {
        if (param.type === 'SAVE') {
            if (!$rootScope.roleAccess(roleConstant.SETUP_APP_CONFIGURATION_ENQUIRY_SERVICE_MAPPING_MODIFY)) {
                return;
            }
            return checkAndUpdate(param);
        }
        console.log("param", param);
    };

    $scope.ajaxServiceEvent = function(object) {
        console.log('ajaxServiceEvent called');
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ServiceList.fetch($scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.serviceList = data.responseObject.searchResult;
                    return $scope.serviceList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Services');
            }
        );
    }
    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            break;
    }

}]);