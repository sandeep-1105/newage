'use strict';
app.controller('EmailTemplateCtrl',function($scope, $rootScope,emailTemplateSearch, $stateParams, $state,roleConstant) {
			
	$scope.eventArr = [{displayValue:"Shipment Booking Confirmation", value:"SHIPMENT_BOOKING_CONFIRMATION"},
                       {displayValue:"Enquiry Sales Confirmation", value:"ENQUIRYCONFTOSALSE"},
                       {displayValue:"Quotation Approving Confirmation", value:"QUOTATION_APPROVING_CONFIRMATION"},
                       {displayValue:"Shipment Event", value:"SHIPMENT_EVENT"},
                       {displayValue:"Shipment Status", value:"SHIPMENT_STATUS"},
                       {displayValue:"Cargo Loaded Confirmation", value:"SHIPMENT_CARGO_LOADED"},
                       {displayValue:"Pricing Expiry Date Remainder", value:"EXPIRY_PRICING_BILL"},
                       {displayValue:"EnquiryRateRequest", value:"ENQUIRY_RATE_REQUEST"},
                       {displayValue:"Shipment Report Attachment", value:"SHIPMENT_REPORT_ATTACHMENT"},
                       {displayValue:"Consol Report Attachment", value:"CONSOL_REPORT_ATTACHMENT"},
                       {displayValue:"Quotation Report Attachment", value:"QUOTATION_REPORT_ATTACHMENT"},
                       {displayValue:"Invoice and Credit Note Report Attachment", value:"INVOICE_REPORT_ATTACHMENT"},
                       {displayValue:"Credit Note Attachment", value:"CREDIT_NOTE_REPORT_ATTACHMENT"},
                       {displayValue:"Account Activation Confirmation", value:"ACCOUNT_ACTIVATION_CONFIRMATION"},
                       {displayValue:"Pre Alert to Agent", value:"PRE_ALRETTO_AGENT"},
					   {displayValue:"Airline EDI", value:"AIRLINE_EDI_EMAIL"},
					   {displayValue:"Shipment Tracking", value:"SHIPMENT_TRACKING_STATUS"}]; 
	
    $scope.emailTemplateHeadArray = [ 
            {
				"name" : "#",
				"width" : "col-xs-0half",
				"model" : "no",
				"search" : false
			}, {
				"name" : "Sender Address",
				"width" : "w90px",
				"model" : "fromEmailId",
				"search" : true,
				"wrap_cell" : true,
				"type" : "text",
				"sort" : true,
				"key" : "searchFromEmailId"
			}, {
				"name" : "Subject",
				"width" : "w150px",
				"model" : "subject",
				"search" : true,
				"type" : "text",
				"sort" : true,
				"key" : "searchSubject"
			}, {
				"name" : "Type",
				"width" : "w90px",
				"model" : "templateType",
				"search" : true,
				"type" : "text",
				"sort" : true,
				"key" : "searchTemplateType"
			}];
			
			$scope.init = function() {
				$scope.limitArr = [10,15,20];
				$scope.page = 0;
				$scope.limit = 10;
				$scope.searchDto = {};
				$scope.search();
			}
		     
			$scope.search = function(){
				$scope.searchDto.selectedPageNumber = $scope.page;
				$scope.searchDto.recordPerPage = $scope.limit;
				emailTemplateSearch.getall($scope.searchDto).$promise.then(function(data, status) {
					$scope.totalRecord = data.responseObject.totalRecord;
					var tempArr = [];
					var resultArr = [];
					tempArr = data.responseObject.searchResult;
					var tempObj = {};
					angular.forEach(tempArr,function(item,index) {
						tempObj = item;
						tempObj.no = (index+1)+($scope.page*$scope.limit);
						
						$scope.eventArr.find(function(t) {
							if(t.value == tempObj.templateType) {
								tempObj.templateType = t.displayValue;	
							}
						})
						
						tempObj.template=null;
						resultArr.push(tempObj);
						tempObj = {};
					});
					$scope.emailTemplateArray = resultArr;
					console.log("$scope.emailTemplateArray ", $scope.emailTemplateArray);
				});
			}
		    
		    $scope.sortSelection = { sortKey : "fromEmailId", sortOrder : "asc" };
		
			$scope.changePage = function(param) {
				$scope.page = param.page;
				$scope.limit = param.size;
				$scope.search();
			}
		
			$scope.limitChange = function(item) {
				$scope.page = 0;
				$scope.limit = item;
				$scope.search();
			}
			
			$scope.sortChange = function(param) {
				$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
				$scope.searchDto.sortByColumn = param.sortKey;
				$scope.search();
			};
		
			$scope.changeSearch = function(param) {
				console.log("Change Search is called ", param);
				for ( var attrname in param) {
					$scope.searchDto[attrname] = param[attrname];
				}
				$scope.page = 0;
				$scope.search();
			}
		
			$scope.rowSelect = function(data, index){
				$scope.edit(data.id);
			}
			
			$scope.edit = function(id){
				if(!$rootScope.roleAccess(roleConstant.SETUP_EMAIL_TEMPLATE_MODIFY)){
					 return;
				 }
				var params = {};
				params.emailTemplateId =id;
				$state.go('layout.editEmailTemplate',params);
			}
	  
		switch ($stateParams.action) {
			case "SEARCH":
				$scope.init();
				$rootScope.breadcrumbArr = [{ label:"Setup",  state:"layout.emailTemplate"},  { label:"eMail Template", state:"layout.emailTemplate"}];
				break;
		    }	
				
			
		});
