app.factory('emailTemplateSearch', function($resource) {
	return $resource("/api/v1/emailtemplate/get/search", {}, {
		getall : { method : 'POST'} 
	});
   })
	
	.factory('emailTemplateUpdateFactory', function($resource) {
		return $resource("/api/v1/emailtemplate/update", {}, {
			query : { method : 'POST' } 
		});
	})
	.factory('emailTemplateGetByResourceFactory', function($resource) {
		return $resource("/api/v1/emailtemplate/getbyid/:id", {}, {
			query  : { method : 'GET',  params : {id:''}, isArray : false } 
		});
	})