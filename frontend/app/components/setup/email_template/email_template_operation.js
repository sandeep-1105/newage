'use strict';
app.controller('EmailTemplateOperation',function($scope,$stateParams, $rootScope,$state,ngDialog,Notification,emailTemplateSearch,emailTemplateUpdateFactory, emailTemplateGetByResourceFactory,roleConstant,CommonValidationService) {
			
	
	$scope.eventArr = [{displayValue:"Shipment Booking Confirmation", value:"SHIPMENT_BOOKING_CONFIRMATION"},
                       {displayValue:"Enquiry Sales Confirmation", value:"ENQUIRYCONFTOSALSE"},
                       {displayValue:"Quotation Approving Confirmation", value:"QUOTATION_APPROVING_CONFIRMATION"},
                       {displayValue:"Shipment Event", value:"SHIPMENT_EVENT"},
                       {displayValue:"Shipment Status", value:"SHIPMENT_STATUS"},
                       {displayValue:"Cargo Loaded Confirmation", value:"SHIPMENT_CARGO_LOADED"},
                       {displayValue:"Pricing Expiry Date Remainder", value:"EXPIRY_PRICING_BILL"},
                       {displayValue:"EnquiryRateRequest", value:"ENQUIRY_RATE_REQUEST"},
                       {displayValue:"Shipment Report Attachment", value:"SHIPMENT_REPORT_ATTACHMENT"},
                       {displayValue:"Consol Report Attachment", value:"CONSOL_REPORT_ATTACHMENT"},
                       {displayValue:"Quotation Report Attachment", value:"QUOTATION_REPORT_ATTACHMENT"},
                       {displayValue:"Invoice and Credit Note Report Attachment", value:"INVOICE_REPORT_ATTACHMENT"},
                       {displayValue:"Credit Note Attachment", value:"CREDIT_NOTE_REPORT_ATTACHMENT"},
                       {displayValue:"Account Activation Confirmation", value:"ACCOUNT_ACTIVATION_CONFIRMATION"},
                       {displayValue:"Pre Alert to Agent", value:"PRE_ALRETTO_AGENT"},
					   {displayValue:"Airline EDI", value:"AIRLINE_EDI_EMAIL"},
					   {displayValue:"Shipment Tracking", value:"SHIPMENT_TRACKING_STATUS"}]; 
	 
	$(".toggle-password").click(function() {

	   	  $(this).toggleClass("fa-eye fa-eye-slash");
	   	  var input = $($(this).attr("toggle"));
	   	  if (input.attr("type") == "password") {
	   	    input.attr("type", "text");
	   	  } else {
	   	    input.attr("type", "password");
	   	  }
	   	});
	
	$scope.cancel= function(){
		var isDirty = $scope.emailTemplateForm.$dirty
	   	 if(isDirty){
	   				var newScope = $scope.$new();
	   				newScope.errorMessage = $rootScope.nls["ERR200"];
	   				ngDialog.openConfirm({
	   					template: '<p>{{errorMessage}}</p>' +
	   					'<div class="ngdialog-footer">' +
	   						' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
	   						'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
	   						'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
	   					'</div>',
	   					plain: true,
	   					closeByDocument:false,
	   					scope: newScope,
	   					className: 'ngdialog-theme-default'}).then(function(value) {
	   									if (value == 1) {
	   										$scope.update();
	   									} else if (value == 2) {
	   										var params = {};
	   										params.submitAction = 'Cancelled';
	   										$state.go('layout.emailTemplate',params);
	   									} else {
	   										console.log("cancelled");
	   									}
	   								});
	   			} 
	   		 else {
	   			var params = {};
	   			params.submitAction = 'Cancelled';
	   			$state.go('layout.emailTemplate',params);
	   		}
	}
	

	
	
	$scope.emailVaidate = function(){
		$scope.errorMap = new Map();
		if($scope.selectedEmailTemplate.fromEmailId==null ||  $scope.selectedEmailTemplate.fromEmailId==undefined || $scope.selectedEmailTemplate.fromEmailId==""){
			
		}else{
			  if(!CommonValidationService.checkMultipleMail($scope.selectedEmailTemplate.fromEmailId)){
				  $rootScope.navigateToNextField("senderEmail");
				  $scope.errorMap.put("senderEmail",$rootScope.nls["ERR60024"]);
				  return false;
			}
		}
	}
	
	/*$scope.validateEmailPassword = function(){
		$scope.errorMap = new Map();
		if($scope.selectedEmailTemplate.fromEmailPassword==undefined ||$scope.selectedEmailTemplate.fromEmailPassword == null || $scope.selectedEmailTemplate.fromEmailPassword == ""){
			
		}else{
			var regexp = new RegExp("^[a-zA-Z0-9](?=.*[0-9])(?=.*[@!#$%&()+={};<>,.?/\|])[a-zA-Z0-9!#@$%&()+={};<>,.?/\|]{5,20}$");
			if (!regexp.test($scope.selectedEmailTemplate.fromEmailPassword)) {
				$scope.errorMap.put("senderPassword",$rootScope.nls["ERR2000305"]);
				return false;
			}
		
		}
	}*/
	
	
	$scope.validateEmailTemplate = function(code){
		
		$scope.errorMap = new Map();
		
		if(code==0 || code==1){
			if($scope.selectedEmailTemplate.templateType==undefined || $scope.selectedEmailTemplate.templateType == null ){
				$scope.errorMap.put("eventType",$rootScope.nls["ERR2000300"]);
				return false;
			}
		}
		
		if(code==0 || code==2){
			if($scope.selectedEmailTemplate.subject==undefined || $scope.selectedEmailTemplate.subject == null || $scope.selectedEmailTemplate.subject == ""){
				$scope.errorMap.put("subject",$rootScope.nls["ERR2000301"]);
				return false;
			}if($scope.selectedEmailTemplate.subject!=undefined && $scope.selectedEmailTemplate.subject.length<6){
				$scope.errorMap.put("subject",$rootScope.nls["ERR2000302"]);
				return false;
			}
		}
		
		if(code==0 || code==3){
			if($scope.selectedEmailTemplate.fromEmailId==undefined ||$scope.selectedEmailTemplate.fromEmailId == null || $scope.selectedEmailTemplate.fromEmailId == ""){
				$scope.errorMap.put("senderEmail",$rootScope.nls["ERR2000303"]);
				return false;
			}else{
				if(!CommonValidationService.checkMultipleMail($scope.selectedEmailTemplate.fromEmailId)){
					  $scope.errorMap.put("senderEmail",$rootScope.nls["ERR60024"]);
					  return false;
				}
			}
		}
		
		if(code==0 || code==4){
			if($scope.selectedEmailTemplate.fromEmailPassword==undefined ||$scope.selectedEmailTemplate.fromEmailPassword == null || $scope.selectedEmailTemplate.fromEmailPassword == ""){
				$scope.errorMap.put("senderPassword",$rootScope.nls["ERR2000304"]);
				return false;
			}
		}		
		
		if(code==0 || code==5){
			if($scope.selectedEmailTemplate.emailType==undefined ||$scope.selectedEmailTemplate.emailType == null || $scope.selectedEmailTemplate.emailType == ""){
				$scope.errorMap.put("emailType",$rootScope.nls["ERR2000306"]);
				return false;
			}
		}	
		
		
		if(code==0 || code==6){
			if($scope.selectedEmailTemplate.smtpServerAddress==undefined ||$scope.selectedEmailTemplate.smtpServerAddress == null || $scope.selectedEmailTemplate.smtpServerAddress == ""){
				$scope.errorMap.put("smptServerAddress",$rootScope.nls["ERR2000307"]);
				return false;
			}else{
				
				if($scope.selectedEmailTemplate.smtpServerAddress!=undefined && $scope.selectedEmailTemplate.smtpServerAddress.length<6 ){
					$scope.errorMap.put("smptServerAddress",$rootScope.nls["ERR2000308"]);
					return false;
				}
			}
		}	
		
		if(code==0 || code==7){
			if($scope.selectedEmailTemplate.port!=0 && ($scope.selectedEmailTemplate.port==undefined ||$scope.selectedEmailTemplate.port == null || $scope.selectedEmailTemplate.port == "")){
				$scope.errorMap.put("smtpPort",$rootScope.nls["ERR2000309"]);
				return false;
			}else{
				var port =Number($scope.selectedEmailTemplate.port);
				if(port<=0 ){
					$scope.errorMap.put("smtpPort",$rootScope.nls["ERR2000310"]);
					return false;
				}
				if(port>65535 ){
					$scope.errorMap.put("smtpPort",$rootScope.nls["ERR2000311"]);
					return false;
				}				
			}
		}
		
		if(code==0 || code==8){
			if($scope.selectedEmailTemplate.ccEmailAddress !=undefined && $scope.selectedEmailTemplate.ccEmailAddress !=null && $scope.selectedEmailTemplate.ccEmailAddress !=""){
				if(!CommonValidationService.checkMultipleMail($scope.selectedEmailTemplate.ccEmailAddress)){
					  $rootScope.navigateToNextField("cceMail");
					  $scope.errorMap.put("cceMail",$rootScope.nls["ERR60024"]);
					  return false;
				}	
			}
		}
		
		if(code==0 || code==9){
			if($scope.selectedEmailTemplate.bccEmailAddress !=undefined && $scope.selectedEmailTemplate.bccEmailAddress !=null && $scope.selectedEmailTemplate.bccEmailAddress !=""){
				if(!CommonValidationService.checkMultipleMail($scope.selectedEmailTemplate.bccEmailAddress)){
					  $rootScope.navigateToNextField("bcceMail");
					  $scope.errorMap.put("bcceMail",$rootScope.nls["ERR60024"]);
					  return false;
				}	
			}
		}
		
		
		return true;
	}
	
	
	$scope.update = function() {
		if($scope.validateEmailTemplate(0)){
			emailTemplateUpdateFactory.query($scope.selectedEmailTemplate).$promise.then(function(data){
				if (data.responseCode == "ERR0") {
					console.log("Email Template  updated Successfully")
	                Notification.success($rootScope.nls["ERR401"]);
					$state.go('layout.emailTemplate');
				} else {
					console.log("email template update Failed. " + data.responseDescription)
				}
			},function(error){
				console.log("email template update Failed.", error);
			});	
		}
	};
	$scope.viewEmailTemplate = function(){
		$scope.selectedEmailTemplate={}; 
		emailTemplateGetByResourceFactory.query({id:$stateParams.emailTemplateId},function(data) {
			$scope.selectedEmailTemplate=data.responseObject;
		},function(error){ 
			console.log("Error found getting email template.");
		});
	}
	
	
	
	if($stateParams.action == "EDIT") {
		$scope.viewEmailTemplate();
    	$rootScope.breadcrumbArr = [{ label:"Setup",  state:"layout.emailTemplate"},  { label:"eMail Template", state:"layout.emailTemplate"},{ label:"Edit eMail Template", state:null}];
		
	}	
});
