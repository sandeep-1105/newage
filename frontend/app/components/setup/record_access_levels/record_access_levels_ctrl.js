app.controller('RalCtrl',function($rootScope,$scope,$location,$window, ngDialog,$state) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;



    /********************* ux fix ends here *****************************/
    $scope.recordAccessLevelHeadArr=[
        {
            "name":"#",
            "width":"col-xs-0half",
            "model":"no",
            "search":false
        },
        {
            "name":"Name",
            "width":"col-xs-3",
            "model":"rasName",
            "search":true,
            "wrap_cell":true,
            "type":"text",
            "sort":true,
            "key":"RasName"

        },
        {
            "name":"Description",
            "width":"col-xs-5half",
            "model":"description",
            "search":true,
            "type":"text",
            "sort":true,
            "key":"searchDescription"
        },
        {
            "name":"Status",
            "width":"col-xs-2",
            "model":"status",
            "search":true,
            "type":"drop",
            "data":$rootScope.enum['LovStatus'],
            "key":"searchStatus"
        }];
    $scope.recordAccessLevelArr = [
        {
            "no" : 1,
            "rasName" : "EFSL-CHN-AIR",
            "description" : "Admin",
            "status": "Active"
        },
        {
            "no" : 2,
            "rasName" : "EFSL-CHN",
            "description" : "",
            "status": "Active"
        },
        {
            "no" : 3,
            "rasName" : "LION-IMPORTS-EXPORTS",
            "description" : "",
            "status": "Active"
        },
        {
            "no" : 4,
            "rasName" : "EFSL-BOM",
            "rasDesc" : "",
            "status": "Active"
        }
    ];
    $scope.sortSelection = {
        sortKey : "chargeName",
        sortOrder : "asc"
    };



    $scope.rowSelect = function(){
        $scope.showDetail = true;

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/

        var windowInner=$window.innerWidth;
        if(windowInner<=1199){
            $scope.deskTopView = false;
        } else{
            $scope.deskTopView = true;
        }

        /********************* ux fix ends here *****************************/
    };
    $scope.cancel = function() {
        $scope.showDetail = false;
        $scope.showLogs = false;
        $scope.showHistory = false;
        $scope.chargeMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };
    $scope.back = function() {
        $scope.showDetail = false;
    };
    $scope.addRecordAccessLevel = function() {
        $state.go("layout.addRecordAccessLevels");
    };
    $scope.daybook = {};
    $scope.daybook = {
        "status" : "Active"
    };

    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder
            .toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.search();
    };

    $scope.deleteMaster = function() {
        ngDialog
            .openConfirm(
                {
                    template : '<p>Are you sure you want to delete selected charge?</p>'
                    + '<div class="ngdialog-footer">'
                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
                    + '</button></div>',
                    plain : true,
                    className : 'ngdialog-theme-default'
                })
            .then(
                function(value) {
                    ChargeRemove
                        .remove(
                            {
                                id : $scope.chargeMaster.id
                            },
                            function(data) {
                                if (data.responseCode == 'ERR0') {
                                    console
                                        .log("Charge deleted Successfully")
                                    $scope
                                        .init();
                                } else {
                                    console
                                        .log("Charge deleted Failed "
                                            + data.responseDescription)
                                }
                            },
                            function(error) {
                                console
                                    .log("Charge deleted Failed : "
                                        + error)
                            });
                }, function(value) {
                    console.log("deleted cancelled");

                });
    }
    $scope.changeSearch = function(param) {
        for ( var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();

    }

    $scope.mouseOver = function(column) {

        if ($scope.searchDto.sortByColumn != column) {
            if ($scope.carrierSortIconClass == "icon-downarrow") {
                $scope.carrierSortIconClass = "icon-uparrow";
            } else {
                $scope.carrierSortIconClass = "icon-downarrow";
            }
        }
        $scope.searchDto.sortByColumn = column;

    }

    $scope.changeSorting = function(column) {
        $scope.sortIconCarrierName = " ";
        if ($scope.searchDto.sortByColumn != column) {
            $scope.carrierSortIconClass = "icon-downarrow";
            $scope.searchDto.orderByType = "ASC";
            $scope.search();

        } else {
            $scope.searchDto.sortByColumn = column;
            if ($scope.searchDto.orderByType == "ASC") {
                $scope.search();
                $scope.searchDto.orderByType = "DESC";
                $scope.carrierSortIconClass = "icon-uparrow";
            } else {
                $scope.searchDto.orderByType = "DESC";
                $scope.carrierSortIconClass = "icon-uparrow";
                $scope.search();
                $scope.searchDto.orderByType = "ASC";
                $scope.carrierSortIconClass = "icon-downarrow";
            }

        }
        $scope.searchDto.sortByColumn = column;

    }

    $scope.search = function() {
        $scope.chargeMaster = {};

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        console.log($scope.searchDto);
        $scope.chargeArr = [];
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        ChargeSearch.query($scope.searchDto).$promise
            .then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item,
                                                  index) {
                    tempObj = item;
                    tempObj.no = (index + 1)
                        + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.chargeArr = resultArr;
            });

    }

    $scope.serialNo = function(index) {
        index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage)
            + (index + 1);
        return index;
    }



});
