

app.controller('CompanyLocationWizardController',function($rootScope, $scope, LocationSetupWizardDataService, CommonValidationService,
		LocationSetupWizardFactory, Notification, $location, $timeout, $http) {
	
	$scope.setTab = function(tabName) {
		$scope.currentTab = tabName;
		
		if(tabName === "companyTab") {
			$scope.step = 1;
		} else if(tabName == "loginTab") {
			$scope.step = 2;
		} else if(tabName == "configurationTab") {
			$scope.step = 3;
			$scope.getTimeZoneList("FIN");
		} else if(tabName == "metaTab") {
			$scope.step = 4;
		} else if(tabName == "financeTab") {
			$scope.step = 5;
		} else if(tabName == "chargeTab") {
			$scope.step = 6;
		} else {
			$scope.step = 7;
		}
	}
	
	
	$scope.getTimeZoneList = function(saasId) {
		 	$http({
	            method: "get",
	            headers: {'SAAS_ID': saasId},
	            url: urlPath + "/public/api/v1/timezones/get",
	        }).then(function(response) {
				if(response.data!=null && response.data.responseObject){
					$scope.timeZoneList = response.data.responseObject;
				} 
			}, function (error) {
				console.log("Error" , error);
			});
	}
	
	var protocol = $location.protocol();
	var host = $location.host();
	var port = $location.port();
	if(port!=undefined && port!=null){
		port = ":" + port
	}else{
		port = "";
	}
	var urlPath = protocol +"://"+host+port;
	
	var saasId = "COM";
	
	$scope.getCountryList = function(keyword) {
		return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/countrymaster/get/search/keyword").then(function(response) {
			   if(response.data!=null && response.data.responseObject){
				   return response.data.responseObject.searchResult
			   } 
        }, function (error) {
        	console.log("Error" , error);
        })	
	}
	
	$scope.getCityList = function(keyword) {
		if($scope.locationSetup.countryMaster != undefined && $scope.locationSetup.countryMaster != null && $scope.locationSetup.countryMaster.id != undefined) {
			return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/citymaster/get/search/keyword/" + $scope.locationSetup.countryMaster.id).then(function(response) {
				   if(response.data!=null && response.data.responseObject){
					   return response.data.responseObject.searchResult
				   } 
	        }, function (error) {
	        	console.log("Error" , error);
	        });
		}
	}
	
	$scope.getStateList = function(keyword) {
		if($scope.locationSetup.countryMaster != undefined && $scope.locationSetup.countryMaster != null && $scope.locationSetup.countryMaster.id != undefined) {
			return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/statemaster/get/search/keyword/" + $scope.locationSetup.countryMaster.id).then(function(response) {
				   if(response.data!=null && response.data.responseObject){
					   return response.data.responseObject.searchResult
				   } 
	        }, function (error) {
	        	console.log("Error" , error);
	        });
		}
	}
	
	$scope.getRegionList = function(keyword) {
		return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/regionmaster/get/search/keyword").then(function(response) {
			   if(response.data!=null && response.data.responseObject){
				   return response.data.responseObject.searchResult
			   } 
        }, function (error) {
        	console.log("Error" , error);
        });
	}
	
	
	$scope.getCurrencyList = function(keyword) {
		return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/currencymaster/get/search/keyword").then(function(response) {
			if(response.data!=null && response.data.responseObject){
				return response.data.responseObject.searchResult
			} 
		}, function (error) {
			console.log("Error" , error);
		});
	}

	$scope.getServiceList = function(keyword) {
		return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/servicemaster/get/search/keyword").then(function(response) {
			if(response.data!=null && response.data.responseObject){
				return response.data.responseObject.searchResult
			} 
		}, function (error) {
			console.log("Error" , error);
		});
	}
	
	
	$scope.getLanguageList = function(keyword) {
		return LocationSetupWizardDataService.getList(keyword, saasId, urlPath + "/public/api/v1/language/get/search/keyword").then(function(response) {
			if(response.data!=null && response.data.responseObject){
				return response.data.responseObject.searchResult
			} 
		}, function (error) {
			console.log("Error" , error);
		});
	}
	
	$scope.selectedCountry = function(id) {
		console.log("Selected Country ", id);
		$rootScope.navigateToNextField(id);
	}
	
	$scope.selectedCity = function(id) {
		console.log("Selected City ", id);
		$rootScope.navigateToNextField(id);
	}
	
	$scope.selectedRegion = function(id) {
		console.log("Selected Region ", id);
		$rootScope.navigateToNextField(id);
	};
	
	 
	$scope.validateEmail = function(object) {
		$scope.errorMap = new Map();
		if (object.email != undefined && object.email != null && object.email != "") {
			if(!CommonValidationService.checkMultipleMail(object.email)){
				$scope.errorMap.put("email", "Email is invalid");
				return false;
			}
		}
		return true;
	}
	
	$scope.createLocation = function() {
		console.log("Created Wizard Location Object", $scope.locationSetup);
		
		locationSetupObject = angular.copy($scope.locationSetup);
		locationSetupObject.isHeadOffice = locationSetupObject.isHeadOffice?"TRUE":"FALSE"; 
		locationSetupObject.logo = locationSetupObject.encodedLogo;
		LocationSetupWizardFactory.create.query(locationSetupObject).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
            	console.log("Location Master saved :: ", data);
            	Notification.success($rootScope.nls["ERR400"]);
            	$scope.setTab("loginTab");
            } else {
            	console.log("Create Failed :" + data.responseDescription)
            }

        }, function(error) {
        	console.log("Create Failed : " + error)
        });
	}
	
	
	$scope.createLogin = function() {
		console.log("Created Wizard Login Information Object ", $scope.locationSetup.loginInformation);
		$scope.setTab("configurationTab");
	}
	
	
	$scope.uploadImage = function(file){

		if(file != null || file != undefined){
			var fileName = file.name;
			var extention = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
		
			console.log("file extention ",extention);
			var valid = false;
			if(extention == 'jpeg' || extention == 'jpg' || extention == 'png' || extention == 'gif'){
				valid = true;
			}
			
			var reader = new FileReader();
			
			reader.onload = function(event){
				var contents = event.target.result;
				var uploadedFile = btoa(contents);
				console.log("File contents : " + uploadedFile);
				$scope.locationSetup.encodedLogo = uploadedFile;
				$scope.uploadText=false;
				$scope.isBusy = true;
				$timeout(function(){
					$scope.isBusy = false;
				},1000);
			};
			reader.readAsBinaryString(file);
		}
	}
	
	$scope.bindDenomination=function(){
		if($scope.locationSetup.configInformation.decimalPoint == "2") {
			$scope.locationSetup.configInformation.denomination = 100;
		} else if($scope.locationSetup.configInformation.decimalPoint == "3") {
			$scope.locationSetup.configInformation.denomination = 1000;
		} else {
			$scope.locationSetup.configInformation.denomination = "";
		}
	};
	
	
	$scope.locationSetup = { isHeadOffice : false };
	$scope.locationSetup.loginInformation = {};
	$scope.locationSetup.configInformation = {};
	
	$scope.uploadText=true;
	$scope.isBusy = false;
	$scope.step = 1;
	$scope.noOfStep = 7;
});