/**
 * bankMasterFactory-used to connect with server
 */

app.factory('LocationSetupWizardFactory', function($resource) {
	return {
		create : $resource('/public/api/v1/locationsetup/create_company', {}, {
			query : {method : 'POST'}
		}),
		getAll : $resource('/public/api/v1/locationsetup/getall', {}, {
			query : {method : 'GET', params : {id : ''}, isArray : false}
		})
	};
})


app.service("LocationSetupWizardDataService", function($http) {
	
	this.getList = function(keyword, saasId, urlPath) {
		var searchDto = {keyword : (keyword == null) ? "" : keyword, selectedPageNumber : 0, recordPerPage : 10};
		
        var request = $http({
            method: "post",
            headers: {'SAAS_ID': saasId},
            url: urlPath,
            data:searchDto
        })
        
        return request;
	}
});