/**
 * bankMasterFactory-used to connect with server
 */

app.factory('RecordAccessLevelFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/recordaccesslevel/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/recordaccesslevel/create', {}, {
			query : {method : 'POST'}
		}),
		search : $resource('/api/v1/recordaccesslevel/search', {}, {
			query : {method : 'POST'}
		}),
		remove : $resource('/api/v1/recordaccesslevel/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}, isArray : false}
		})
	};
}])

