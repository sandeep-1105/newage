app.controller('RecordAccessLevelController',function($rootScope,$scope,$location,$window, ngDialog, $state,$stateParams, RecordAccessLevelService, RecordAccessLevelFactory, RecentHistorySaveService,Notification,roleConstant) {

	/************************************************************
	 * ux - by Muthu
	 * reason - ipad compatibility fix
	 *
	 * *********************************************************/
	$scope.deskTopView = true;

	/********************* ux fix ends here *****************************/
	
	$scope.init = function() {
		$scope.searchDto = {};
		$scope.search();
	}
	
	$scope.setTab = function(tabName) {
		$scope.currentTab = tabName;
	}
	
	$scope.getTab = function(tabName) {
		if($scope.currentTab == tabName) 
			return true;
		else
			return false;
	}
	
	
	$scope.limitArr = [10,15,20];
	$scope.page = 0;
	$scope.limit = 10;
	
	
    $scope.recordAccessLevelHeadArr=[
        {
            "name":"#",
            "width":"col-xs-0half",
            "model":"no",
            "search":false
        },
        {
            "name":"Name",
            "width":"col-xs-3",
            "model":"name",
            "search":true,
            "wrap_cell":true,
            "type":"text",
            "sort":true,
            "key":"name"
        },
        {
            "name":"Description",
            "width":"col-xs-5half",
            "model":"description",
            "search":true,
            "type":"text",
            "sort":true,
            "key":"description"
        },
        {
            "name":"Status",
            "width":"col-xs-2",
            "model":"lovStatus",
            "search":true,
            "type":"drop",
            "data":$rootScope.enum['LovStatus'],
            "key":"status"
        }];
    
    
    
    
    $scope.sortSelection = {
        sortKey : "name",
        sortOrder : "asc"
    };

	$scope.changePage = function(param) {
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.search();
	}
	
	$scope.limitChange = function(item) {
		$scope.page = 0;
		$scope.limit = item;
		$scope.search();
	}
	
	$scope.sortChange = function(param) {
		$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.searchDto.sortByColumn = param.sortKey;
		$scope.search();
	};
	
	$scope.changeSearch = function(param) {
		console.log("Change Search is called ", param);
		for ( var attrname in param) {
			$scope.searchDto[attrname] = param[attrname];
		}
		$scope.page = 0;
		$scope.search();
	}
	
   $scope.search = function() {
	   console.log("Search is called.");
		
	   $scope.searchDto.selectedPageNumber = $scope.page;
	   $scope.searchDto.recordPerPage = $scope.limit;
		
	   RecordAccessLevelFactory.search.query($scope.searchDto).$promise.then(function(data, status) {
		   $scope.totalRecord = data.responseObject.totalRecord;
		   $scope.recordAccessLevelData = data.responseObject.searchResult;
	   });
   }
	    

	$scope.rowSelect = function(data, index){
		if($rootScope.roleAccess(roleConstant.SETUP_RECORD_ACCESS_LEVEL_VIEW)){
		console.log("Row Select", data, index);
		

		var param = { id   : data.id};
	    $state.go("layout.viewRecordAccessLevel", param);
		
		

		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/

		var windowInner=$window.innerWidth;
		if(windowInner<=1199){
			$scope.deskTopView = false;
		}
		else{
			$scope.deskTopView = true;
		}							
		}
	}
  
	
    $scope.backToListPage = function() {
        $state.go("layout.RecordAccessLevel");
        /************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
	    	$scope.deskTopView = true;

		/********************* ux fix ends here *****************************/
    };
    
    
    
    
    // Goto Add page
    $scope.addRecordAccessLevel = function() {
    	if($rootScope.roleAccess(roleConstant.SETUP_RECORD_ACCESS_LEVEL_CREATE)){
    		 $state.go("layout.addRecordAccessLevel");
		}
    };
    
    $scope.editRecordAccessLevel = function() {
    	if($rootScope.roleAccess(roleConstant.SETUP_RECORD_ACCESS_LEVEL_MODIFY)){
    		var param = { id   : $scope.recordAccessLevel.id };
    	    $state.go("layout.editRecordAccessLevel", param);
		}
    };
    
    $scope.deleteRecordAccessLevel = function() {
    	if($rootScope.roleAccess(roleConstant.SETUP_RECORD_ACCESS_LEVEL_DELETE)){
    	console.log("Delete Button is Pressed....................................");
    	
		var newScope = $scope.$new();
		newScope.errorMessage = $rootScope.nls["ERR4217"];
		ngDialog.openConfirm({template: '<p>{{errorMessage}}</p>' +
                '<div class="ngdialog-footer">' +
                  '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                  '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                '</button>' +
				   '</div>',
            plain: true,
            scope: newScope,
            className: 'ngdialog-theme-default'
        }).then(function (value) {
        	RecordAccessLevelFactory.remove.query({id : $scope.recordAccessLevel.id}, function(data) {
        		if (data.responseCode == 'ERR0') {
        			console.log("Deleted Successfully");
        			Notification.success($rootScope.nls["ERR402"]);
        			$state.go("layout.RecordAccessLevel");
        		} else {
        			console.log(data.responseDescription)
        		}
        	}, function(error) {
        		console.log(error);
        	})
        }, function(value) {
        	console.log("Cancelled.");
        } );
    }
    
    }

    $scope.view = function(id) {
    	console.log("View is called ", id);
    	
    	RecordAccessLevelFactory.findById.query({id : id}, function(data) {
    		if(data.responseCode == 'ERR0') {
    			$scope.recordAccessLevel = data.responseObject;
    			
    			console.log("RecordAccessLevel ", $scope.recordAccessLevel);
    			 
    			if ($scope.recordAccessLevel != undefined && $scope.recordAccessLevel != null && $scope.recordAccessLevel.recordAccessLevelName != undefined && $scope.recordAccessLevel.recordAccessLevelName != null) {
    				$rootScope.subTitle = $scope.recordAccessLevel.recordAccessLevelName;
    			}

    			
    			 
    			var rHistoryObj = {
                    'title': 'Record Access Level View #' + $stateParams.id,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'RecordAccessLevel',
                    'serviceType': 'Setup'
    			 }

    			 RecentHistorySaveService.form(rHistoryObj);
    		} else {
    			console.log("Error ", data.responseDescription);
    		}
    	}, function(error) {
    		console.log("Error ", error);
    	});
    	
    }



     

 
    
    switch ($stateParams.action) {
    case "VIEW":
        console.log("View Page", $stateParams.id);
       
        $scope.view($stateParams.id);
        $rootScope.breadcrumbArr = [
                                    { label:"Setup", 
                                      state:"layout.RecordAccessLevel"}, 
                                    { label:"Record Access Level", 
                                      state:"layout.RecordAccessLevel"},
                                    { label:"View Record Access Level", 
                                      state:null}];
        break;
    case "SEARCH":
        console.log("Search Page");
        $scope.init();
        $rootScope.breadcrumbArr = [
                                    { label:"Setup", 
                                      state:"layout.RecordAccessLevel"}, 
                                    { label:"Record Access Level", 
                                      state:"layout.RecordAccessLevel"}];
        break;
    default:

}			


});