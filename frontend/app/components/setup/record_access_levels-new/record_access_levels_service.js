app.service("RecordAccessLevelService", function(RecordAccessLevelFactory, EmptyRowChecker, $rootScope) {


    this.getRecordAccessLevel = function() {
        var recordAccessLevel = {};
        recordAccessLevel.status = "Active";

        recordAccessLevel.countryList = []
        recordAccessLevel.locationList = []
        recordAccessLevel.divisionList = []
        recordAccessLevel.serviceList = []
        recordAccessLevel.salesmanList = []
        recordAccessLevel.customerList = []
        recordAccessLevel.userList = []

        return recordAccessLevel;
    }

    this.addEmptyObject = function(list) {
        if (list == undefined || list == null || list.length == 0) {
            list = [];
            list.push({});
        }
    }

    this.removeEmptyObject = function(list) {
        if (list != undefined && list != null && list.length > 0) {
            angular.forEach(list, function(obj, index) {
                if (EmptyRowChecker.isEmptyRow(obj)) {
                    list.splice(index, 1);
                }
            });
        }
    }





    this.validateCountry = function(country, index) {

        var countryErrorObj = {};
        countryErrorObj.errorObj = [];
        countryErrorObj.errorObj[index] = {};
        countryErrorObj.errorObj[index].isErrorFound = false;

        if (country.countryMaster == undefined || country.countryMaster == null || country.countryMaster.id == null) {
            countryErrorObj.errorObj[index].errorControl = true;
            countryErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4202"];
            countryErrorObj.isErrorFound = true;
        } else if (country.countryMaster.status == 'Block') {
            countryErrorObj.errorObj[index].errorControl = true;
            countryErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4203"];
            countryErrorObj.isErrorFound = true;
        } else {
            countryErrorObj.isErrorFound = false;
        }

        return countryErrorObj;

    }


    this.validateLocation = function(location, index) {

        var locationErrorObj = {};
        locationErrorObj.errorObj = [];
        locationErrorObj.errorObj[index] = {};
        locationErrorObj.errorObj[index].isErrorFound = false;

        if (location.locationMaster == undefined || location.locationMaster == null || location.locationMaster.id == null) {
            locationErrorObj.errorObj[index].errorControl = true;
            locationErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4204"];
            locationErrorObj.isErrorFound = true;
        } else if (location.locationMaster.status == 'Block') {
            locationErrorObj.errorObj[index].errorControl = true;
            locationErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4205"];
            locationErrorObj.isErrorFound = true;
        } else {
            locationErrorObj.isErrorFound = false;
        }

        return locationErrorObj;

    }


    this.validateDivision = function(division, index) {

        var divisionErrorObj = {};
        divisionErrorObj.errorObj = [];
        divisionErrorObj.errorObj[index] = {};
        divisionErrorObj.errorObj[index].isErrorFound = false;

        if (division.divisionMaster == undefined || division.divisionMaster == null || division.divisionMaster.id == null) {
            divisionErrorObj.errorObj[index].errorControl = true;
            divisionErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4206"];
            divisionErrorObj.isErrorFound = true;
        } else if (division.divisionMaster.status == 'Block') {
            divisionErrorObj.errorObj[index].errorControl = true;
            divisionErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4207"];
            divisionErrorObj.isErrorFound = true;
        } else {
            divisionErrorObj.isErrorFound = false;
        }

        return divisionErrorObj;

    }


    this.validateService = function(service, index) {

        var serviceErrorObj = {};
        serviceErrorObj.errorObj = [];
        serviceErrorObj.errorObj[index] = {};
        serviceErrorObj.errorObj[index].isErrorFound = false;

        if (service.serviceMaster == undefined || service.serviceMaster == null || service.serviceMaster.id == null) {
            serviceErrorObj.errorObj[index].errorControl = true;
            serviceErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4208"];
            serviceErrorObj.isErrorFound = true;
        } else if (service.serviceMaster.status == 'Block') {
            serviceErrorObj.errorObj[index].errorControl = true;
            serviceErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4209"];
            serviceErrorObj.isErrorFound = true;
        } else {
            serviceErrorObj.isErrorFound = false;
        }

        return serviceErrorObj;

    }

    this.validateSalesman = function(salesman, index) {

        var salesmanErrorObj = {};
        salesmanErrorObj.errorObj = [];
        salesmanErrorObj.errorObj[index] = {};
        salesmanErrorObj.errorObj[index].isErrorFound = false;

        if (salesman.employeeMaster == undefined || salesman.employeeMaster == null || salesman.employeeMaster.id == null) {
            salesmanErrorObj.errorObj[index].errorControl = true;
            salesmanErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4210"];
            salesmanErrorObj.isErrorFound = true;
        } else if (salesman.employeeMaster.employementStatus == 'TERMINATED') {
            salesmanErrorObj.errorObj[index].errorControl = true;
            salesmanErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4211"];
            salesmanErrorObj.isErrorFound = true;
        } else if (salesman.employeeMaster.employementStatus == 'RESIGNED') {
            salesmanErrorObj.errorObj[index].errorControl = true;
            salesmanErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4212"];
            salesmanErrorObj.isErrorFound = true;
        } else {
            salesmanErrorObj.isErrorFound = false;
        }

        return salesmanErrorObj;

    }

    this.validateCustomer = function(customer, index) {

        var customerErrorObj = {};
        customerErrorObj.errorObj = [];
        customerErrorObj.errorObj[index] = {};
        customerErrorObj.errorObj[index].isErrorFound = false;

        if (customer.partyMaster == undefined || customer.partyMaster == null || customer.partyMaster.id == null) {
            customerErrorObj.errorObj[index].errorControl = true;
            customerErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4213"];
            customerErrorObj.isErrorFound = true;
        } else if (customer.partyMaster.status == 'Block') {
            customerErrorObj.errorObj[index].errorControl = true;
            customerErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4214"];
            customerErrorObj.isErrorFound = true;
        } else {
            customerErrorObj.isErrorFound = false;
        }

        return customerErrorObj;

    }


    this.validateUser = function(user, index) {

        var userErrorObj = {};
        userErrorObj.errorObj = [];
        userErrorObj.errorObj[index] = {};
        userErrorObj.errorObj[index].isErrorFound = false;

        if (user.userProfile == undefined || user.userProfile == null || user.userProfile.id == null) {
            userErrorObj.errorObj[index].errorControl = true;
            userErrorObj.errorObj[index].errorMessage = $rootScope.nls["ERR4215"];
            userErrorObj.isErrorFound = true;
        } else {
            userErrorObj.isErrorFound = false;
        }

        return userErrorObj;

    }






});