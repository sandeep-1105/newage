app.controller('RecordAccessLevelOperationController',function($rootScope,$scope, ngDialog, $modal, $state, $stateParams, AutoCompleteService, RecordAccessLevelService, RecordAccessLevelFactory, Notification, ngProgressFactory) {

	$scope.contained_progressbar = ngProgressFactory.createInstance();
	$scope.contained_progressbar.setParent(document.getElementById('ral-panel'));
	$scope.contained_progressbar.setAbsolute();

	// Injecting the Service in the Controller 
	$scope.$AutoCompleteService = AutoCompleteService;
	$scope.setTab = function(tabName) {
		$scope.currentTab = tabName;
	}
	
	$scope.getTab = function(tabName) {
		if($scope.currentTab == tabName) 
			return true;
		else
			return false;
	}
	
	
	$scope.init = function() {
		console.log("Init is executed...............................");
		
		
		if($scope.recordAccessLevel == undefined) {
			$scope.recordAccessLevel = RecordAccessLevelService.getRecordAccessLevel();
		}
		
		
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.countryList);
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.locationList);
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.divisionList);
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.serviceList);
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.salesmanList);
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.customerList);
		if($scope.recordAccessLevel.userList != undefined)
		RecordAccessLevelService.addEmptyObject($scope.recordAccessLevel.userList);
		
		
		$scope.oldData = JSON.stringify($scope.recordAccessLevel);
	}
	
	
	
	

	$scope.cancel = function() {
		if ($scope.oldData !== JSON.stringify($scope.recordAccessLevel)) {
			var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR200"];
			ngDialog.openConfirm({
				template: '<p>{{errorMessage}}</p>' +
				'<div class="ngdialog-footer">' +
					' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
					'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
					'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
				'</div>',
				plain: true,
				scope: newScope,
				className: 'ngdialog-theme-default'
			}).then(function(value) {
				if (value == 1) {
					$scope.saveOrUpdate();
				} else if(value == 2) {
					$state.go("layout.RecordAccessLevel", {submitAction : 'Cancelled'});
				}else{
					console.log("Cancelled")
				}
			});
		} else {
			$state.go("layout.RecordAccessLevel", {submitAction : 'Cancelled'});
		}
	};
	
	
	
	
	

	$scope.save = function() {
		console.log("Save is called....................");
		console.log($scope.recordAccessLevel);
		
		
		
		
		
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.countryList);
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.locationList);
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.divisionList);
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.serviceList);
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.salesmanList);
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.customerList);
		RecordAccessLevelService.removeEmptyObject($scope.recordAccessLevel.userList);
		
		if($scope.validateRecordAccessLevel()) {
			if($scope.recordAccessLevel.id == null) {
				var successMessage = $rootScope.nls["ERR400"];	
			} else {
				var successMessage = $rootScope.nls["ERR401"];	
			}
			
			$scope.contained_progressbar.start();
			
			RecordAccessLevelFactory.create.query($scope.recordAccessLevel).$promise.then(function(data) {
				if (data.responseCode == 'ERR0') {
					console.log("Record Access Level Saved.....");
					Notification.success(successMessage);
					$state.go("layout.RecordAccessLevel", {submitAction : 'Saved'});
	            } else {
	            	Notification.error(data.responseDescription);
	            	console.log("Create Record Access Level Failed :" , data.responseDescription);
	            	$scope.init();
	            }
	        }, function(error) {
	        	console.log("Create Record Access Level Failed : " , error);
	        });
			/*$scope.contained_progressbar.complete();
			angular.element(".panel-body").animate({scrollTop : 0}, "slow");*/
		} else {
			console.log("Validation Failed.");
			$scope.init();
		}
		
		
	}


    $scope.setStatus = function(data){
        if(data=='Active'){
            return 'activetype';
        } else if(data=='Block'){
            return 'blockedtype';
        }else if(data=='Hide'){
            return 'hiddentype';
        }
    }



    
    /* Country Table Start */
    
    $scope.getCountryNotInList = function(keyword) {
    	  
    	if($scope.recordAccessLevel.countryList != null && $scope.recordAccessLevel.countryList.length > 0) {
   		   var idList = [];
   		   for(var i = 0; i < $scope.recordAccessLevel.countryList.length; i++) {
   			   if($scope.recordAccessLevel.countryList[i] != null && $scope.recordAccessLevel.countryList[i].countryMaster != null) {
   				   idList.push($scope.recordAccessLevel.countryList[i].countryMaster.id);
   			   }
   		   }
    	}
   	   
   	   return AutoCompleteService.getCountryNotInList(keyword, idList);
    }
    
    $scope.selectedCountry = function() {
    	$scope.addCountryRow();
    }

    
    
    
   
    
    $scope.addCountryRow = function(index) {
    	console.log("Add Country Row");
    	
    	$scope.countryErrorObject = {};
    	
    	if($scope.recordAccessLevel.countryList != null && $scope.recordAccessLevel.countryList.length > 0) {
    		angular.forEach($scope.recordAccessLevel.countryList, function(country, index) {
    			$scope.countryErrorObject = RecordAccessLevelService.validateCountry(country, index);
    		});
    	}
    	
    	if(!$scope.countryErrorObject.isErrorFound) {
    		$scope.recordAccessLevel.countryList.push({});
        	$rootScope.navigateToNextField('country' + ($scope.recordAccessLevel.countryList.length - 1));
    	}
    }
    

    
    $scope.deleteCountryRow = function(index) {
    	console.log("Delete Country Row");
    	$scope.recordAccessLevel.countryList.splice(index, 1);
    }
    
    
    
    /* Country Table End */

    
    
 /* Location Table Start */
    
    
    
    $scope.getLocationNotInList = function(keyword) {
  	  
    	if($scope.recordAccessLevel.locationList != null && $scope.recordAccessLevel.locationList.length > 0) {
   		   var idList = [];
   		   for(var i = 0; i < $scope.recordAccessLevel.locationList.length; i++) {
   			   if($scope.recordAccessLevel.locationList[i] != null && $scope.recordAccessLevel.locationList[i].locationMaster != null) {
   				   idList.push($scope.recordAccessLevel.locationList[i].locationMaster.id);
   			   }
   		   }
    	}
   	   
   	   return AutoCompleteService.getLocationNotInList(keyword, idList);
    }
    
    $scope.selectedLocation = function() {
    	$scope.addLocationRow();
    }
    
    
    $scope.addLocationRow = function(index) {
    	console.log("Add Location Row");
    	
    	$scope.locationErrorObject = {};
    	
    	if($scope.recordAccessLevel.locationList != null && $scope.recordAccessLevel.locationList.length > 0) {
    		angular.forEach($scope.recordAccessLevel.locationList, function(location, index) {
    			$scope.locationErrorObject = RecordAccessLevelService.validateLocation (location, index);
    		});
    	}
    	
    	if(!$scope.locationErrorObject.isErrorFound) {
    		$scope.recordAccessLevel.locationList.push({});
        	$rootScope.navigateToNextField('location' + ($scope.recordAccessLevel.locationList.length - 1));
    	}
    }
    

    
    $scope.deleteLocationRow = function(index) {
    	console.log("Delete Location Row");
    	$scope.recordAccessLevel.locationList.splice(index, 1);
    }
    
    
    
    /* Location Table End */
    


    

    /* Division Table Start */
    
    $scope.getDivisionNotInList = function(keyword) {
    	  
    	if($scope.recordAccessLevel.divisionList != null && $scope.recordAccessLevel.divisionList.length > 0) {
   		   var idList = [];
   		   for(var i = 0; i < $scope.recordAccessLevel.divisionList.length; i++) {
   			   if($scope.recordAccessLevel.divisionList[i] != null && $scope.recordAccessLevel.divisionList[i].divisionMaster != null) {
   				   idList.push($scope.recordAccessLevel.divisionList[i].divisionMaster.id);
   			   }
   		   }
    	}
   	   
   	   return AutoCompleteService.getDivisionNotInList(keyword, idList);
    }
    
    $scope.selectedDivision = function() {
    	$scope.addDivisionRow();
    }
       
       
    $scope.addDivisionRow = function(index) {
       	console.log("Add Division Row");
       	
       	$scope.divisionErrorObject = {};
       	
       	if($scope.recordAccessLevel.divisionList != null && $scope.recordAccessLevel.divisionList.length > 0) {
       		angular.forEach($scope.recordAccessLevel.divisionList, function(division, index) {
       			$scope.divisionErrorObject = RecordAccessLevelService.validateDivision (division, index);
       		});
       	}
       	
       	if(!$scope.divisionErrorObject.isErrorFound) {
       		$scope.recordAccessLevel.divisionList.push({});
           	$rootScope.navigateToNextField('division' + ($scope.recordAccessLevel.divisionList.length - 1));
       	}
       }
       

       
       $scope.deleteDivisionRow = function(index) {
       	console.log("Delete division Row");
       	$scope.recordAccessLevel.divisionList.splice(index, 1);
       }
       
       
       
       /* Division Table End */

    

       
       

       /* Service Table Start */
          
       $scope.getSearchServiceNotInList = function(keyword) {
    	   if($scope.recordAccessLevel.serviceList != null && $scope.recordAccessLevel.serviceList.length > 0) {
    		   var idList = [];
    		   for(var i = 0; i < $scope.recordAccessLevel.serviceList.length; i++) {
    			   if($scope.recordAccessLevel.serviceList[i] != null && $scope.recordAccessLevel.serviceList[i].serviceMaster != null) {
    				   idList.push($scope.recordAccessLevel.serviceList[i].serviceMaster.id);
    			   }
    		   }
    	   }
    	   
    	   return AutoCompleteService.getSearchServiceNotInList(keyword, idList);
       }
       
       
       $scope.selectedService = function() {
    	   $scope.addServiceRow();
       }
          
       $scope.addServiceRow = function(index) {
          	console.log("Add Service Row");
          	
          	$scope.serviceErrorObject = {};
          	
          	if($scope.recordAccessLevel.serviceList != null && $scope.recordAccessLevel.serviceList.length > 0) {
          		angular.forEach($scope.recordAccessLevel.serviceList, function(service, index) {
          			$scope.serviceErrorObject = RecordAccessLevelService.validateService (service, index);
          		});
          	}
          	
          	if(!$scope.serviceErrorObject.isErrorFound) {
          		$scope.recordAccessLevel.serviceList.push({});
              	$rootScope.navigateToNextField('service' + ($scope.recordAccessLevel.serviceList.length - 1));
          	}
          }
          

          
          $scope.deleteServiceRow = function(index) {
          	console.log("Delete Service Row");
          	$scope.recordAccessLevel.serviceList.splice(index, 1);
          }
          
          
          
          /* Service Table End */

       
       

          /* Salesman Table Start */
          
          $scope.selectedSalesman = function() {
        	  $scope.addSalesmanRow();
          }
          $scope.addSalesmanRow = function(index) {
             	console.log("Add Service Row");
             	
             	$scope.salesmanErrorObject = {};
             	
             	if($scope.recordAccessLevel.salesmanList != null && $scope.recordAccessLevel.salesmanList.length > 0) {
             		angular.forEach($scope.recordAccessLevel.salesmanList, function(salesman, index) {
             			$scope.salesmanErrorObject = RecordAccessLevelService.validateSalesman (salesman, index);
             		});
             	}
             	
             	if(!$scope.salesmanErrorObject.isErrorFound) {
             		$scope.recordAccessLevel.salesmanList.push({});
                 	$rootScope.navigateToNextField('salesman' + ($scope.recordAccessLevel.salesmanList.length - 1));
             	}
             }
             

             
             $scope.deleteSalesmanRow = function(index) {
             	console.log("Delete salesman Row");
             	$scope.recordAccessLevel.salesmanList.splice(index, 1);
             }
             
             
             
             /* Salesman Table End */

          
          
    
    

             /* Customer Table Start */
             
             $scope.getSearchPartyNotInList = function(keyword) {
          	   if($scope.recordAccessLevel.customerList != null && $scope.recordAccessLevel.customerList.length > 0) {
          		   var idList = [];
          		   for(var i = 0; i < $scope.recordAccessLevel.customerList.length; i++) {
          			   if($scope.recordAccessLevel.customerList[i] != null && $scope.recordAccessLevel.customerList[i].partyMaster != null) {
          				   idList.push($scope.recordAccessLevel.customerList[i].partyMaster.id);
          			   }
          		   }
          	   }
          	   
          	   return AutoCompleteService.getSearchPartyNotInList(keyword, idList);
             }
             
             
             $scope.selectedCustomer = function() {
          	   $scope.addCustomerRow();
             }
             
             $scope.addCustomerRow = function(index) {
                	console.log("Add Service Row");
                	
                	$scope.customerErrorObject = {};
                	
                	if($scope.recordAccessLevel.customerList != null && $scope.recordAccessLevel.customerList.length > 0) {
                		angular.forEach($scope.recordAccessLevel.customerList, function(customer, index) {
                			$scope.customerErrorObject = RecordAccessLevelService.validateCustomer (customer, index);
                		});
                	}
                	
                	if(!$scope.customerErrorObject.isErrorFound) {
                		$scope.recordAccessLevel.customerList.push({});
                    	$rootScope.navigateToNextField('customer' + ($scope.recordAccessLevel.customerList.length - 1));
                	}
                }
                

                
                $scope.deleteCustomerRow = function(index) {
                	console.log("Delete Customer Row");
                	$scope.recordAccessLevel.customerList.splice(index, 1);
                }
                
                
                
                /* Customer Table End */

             
             
             
    
    
    
    /* User Table Start */
                
                
    $scope.getSearchUserNotInList = function(keyword) {
   	   if($scope.recordAccessLevel.userList != null && $scope.recordAccessLevel.userList.length > 0) {
  		   var idList = [];
  		   for(var i = 0; i < $scope.recordAccessLevel.userList.length; i++) {
  			   if($scope.recordAccessLevel.userList[i] != null && $scope.recordAccessLevel.userList[i].userProfile != null) {
  				   idList.push($scope.recordAccessLevel.userList[i].userProfile.id);
  			   }
  		   }
  	   }
  	   
  	   return AutoCompleteService.getSearchUserNotInList(keyword, idList);
     }
     
     
     $scope.selectedUser = function() {
  	   $scope.addUserRow();
     }
    
    $scope.addUserRow = function(index) {
    	console.log("Add User Row");
    	
    	$scope.userErrorObject = {};
    	
    	if($scope.recordAccessLevel.userList != null && $scope.recordAccessLevel.userList.length > 0) {
    		angular.forEach($scope.recordAccessLevel.userList, function(user, index) {
    			$scope.userErrorObject = RecordAccessLevelService.validateUser(user, index);
    		});
    	}
    		
    	if(!$scope.userErrorObject.isErrorFound) {
    		$scope.recordAccessLevel.userList.push({});
        	$rootScope.navigateToNextField('user' + ($scope.recordAccessLevel.userList.length - 1));
    	}
    	
    }
    
    $scope.deleteUserRow = function(index) {
    	console.log("Delete User Row");
    	$scope.recordAccessLevel.userList.splice(index, 1);
    }
    
    
    
    
    
    
    /* user Table End */
    
    
    
    
    
    
    
    /*Validations Starts */
    
    $scope.validateRecordAccessLevel = function() {
    	
    	$scope.errorMap = new Map();
    	
    	if($scope.recordAccessLevel.recordAccessLevelName == null || $scope.recordAccessLevel.recordAccessLevelName == "" || $scope.recordAccessLevel.recordAccessLevelName.length == 0) {
    		$scope.errorMap.put("recordAccessLevelName",$rootScope.nls["ERR4201"]);
    		console.log("recordAccessLevelName ", $rootScope.nls["ERR4201"])
			return false;
    	}
    	
    	
    	return true;
    }
    
    /*Validations Ends */
    
    
    
    
    
    
    
    
    var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
    
    
	$scope.errorShow = function(errorObj,index){
        $scope.errObj = errorObj.errorMessage;
        myOtherModal.$promise.then(myOtherModal.show);
    };
    
    
    
    $scope.getById = function(id) {
		console.log("getById ", id);
		
		RecordAccessLevelFactory.findById.query({id : id}, function(data) {
    		if(data.responseCode == 'ERR0') {
    			$scope.recordAccessLevel = data.responseObject;
    			console.log("RecordAccessLevel ", $scope.recordAccessLevel);
    			 $scope.init();
    		} else {
    			console.log("Error ", data.responseDescription);
    		}
    	}, function(error) {
    		console.log("Error ", error);
    	});
	}
    
    $scope.toggleAccessInfo = function(fieldId) {
    	if($scope.accessInfo == true) {
    		$scope.accessInfo = false;
    	} else {
    		$scope.accessInfo = true;
    		$scope.moveCursor(fieldId);
    	}
    }
    
    
    
    $scope.toggleDescriptionInfo = function(fieldId) {
    	if($scope.descriptionInfo == true) {
    		$scope.descriptionInfo = false;
    	} else {
    		$scope.descriptionInfo = true;
    		$scope.moveCursor(fieldId);
    	}
    }
    
    
    $scope.toggleUserInfo = function(fieldId) {
    	if($scope.userInfo == true) {
    		$scope.userInfo = false;
    	} else {
    		$scope.userInfo = true;
    		$scope.moveCursor(fieldId);
    	}
    }
    
    $scope.moveCursor = function(id) {
    	if(id != undefined && id != null) {
    		$rootScope.navigateToNextField(id);
    	}
    }
    
    
    //On leave the Unfilled Unit Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

	$scope.$on('addRecordAccessLevelEvent', function(events, args){
		
			$rootScope.unfinishedData = $scope.recordAccessLevel;
			$rootScope.category = "Record Access Level";
			$rootScope.unfinishedFormTitle = "Record Access Level (New)";
			if($scope.recordAccessLevel != undefined && $scope.recordAccessLevel != null && $scope.recordAccessLevel.recordAccessLevelName != undefined 
					&& $scope.recordAccessLevel.recordAccessLevelName != null && $scope.recordAccessLevel.recordAccessLevelName != "") {
				$rootScope.subTitle = $scope.recordAccessLevel.recordAccessLevelName;
			} else {
				$rootScope.subTitle = "Unknown Record Access Level"
			}
	})
	  
	 $scope.$on('editRecordAccessLevelEvent', function(events, args) {
			$rootScope.unfinishedData = $scope.recordAccessLevel;
			$rootScope.category = "Record Access Level";
			$rootScope.unfinishedFormTitle = "Record Access Level Edit " + $scope.recordAccessLevel.recordAccessLevelName;
			if($scope.recordAccessLevel != undefined && $scope.recordAccessLevel != null && $scope.recordAccessLevel.recordAccessLevelName != undefined 
					&& $scope.recordAccessLevel.recordAccessLevelName != null && $scope.recordAccessLevel.recordAccessLevelName != "") {
				$rootScope.subTitle = $scope.recordAccessLevel.recordAccessLevelName;
			} else {
				$rootScope.subTitle = "Unknown Record Access Level"
			}
	})
	
	
    $scope.$on('addRecordAccessLevelEventReload', function (e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.recordAccessLevel) ;
        localStorage.isRecordAccessReload = "YES";
        e.preventDefault();
    });

	
	$scope.$on('editRecordAccessLevelEventReload', function (e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.recordAccessLevel) ;
        localStorage.isRecordAccessReload = "YES";
        e.preventDefault();
    });
	
	
	$scope.isReloaded = localStorage.isRecordAccessReload;
	
	if ($stateParams.action == "ADD") {
		 
		if($stateParams.fromHistory == 'Yes') {
			if($scope.isReloaded == "YES") {
				console.log("Add History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isRecordAccessReload = "NO";
				$scope.recordAccessLevel  = JSON.parse(localStorage.reloadFormData);
			} else {
				console.log("Add History Not Reload...")
				$scope.recordAccessLevel = $rootScope.selectedUnfilledFormData;
			}
		} else {
			if($scope.isReloaded == "YES") {
				console.log("Add Not History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isRecordAccessReload = "NO";
				$scope.recordAccessLevel  = JSON.parse(localStorage.reloadFormData);
			} else {
				console.log("Add Not History Not Reload...")
				$scope.init();
			}	
		}
		
		$rootScope.breadcrumbArr = [{ label:"Setup",  state:"layout.RecordAccessLevel"}, 
                                    { label:"Record Access Level", state:"layout.RecordAccessLevel"}, 
                                    { label:"Add Record Access Level", state:null}];	
		$rootScope.navigateToNextField("recordAccessLevelName");
		
	} else {
		
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isRecordAccessReload = "NO";
				$scope.recordAccessLevel  = JSON.parse(localStorage.reloadFormData);
			} else {
				$scope.recordAccessLevel = $rootScope.selectedUnfilledFormData;
			}
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isRecordAccessReload = "NO";
				$scope.recordAccessLevel  = JSON.parse(localStorage.reloadFormData);
			} else {
				$scope.getById($stateParams.id);
			}
		}
		$rootScope.breadcrumbArr = [ { label:"Setup",  state:"layout.RecordAccessLevel"}, 
		                             { label:"Record Access Level",  state:"layout.RecordAccessLevel"}, 
                                     { label:"Edit Record Access Level", state:null }];
		
		$rootScope.navigateToNextField("recordAccessLevelName");
	
		
		
	}


});