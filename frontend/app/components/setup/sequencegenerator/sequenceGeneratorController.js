(function(){
app.controller("SequenceGeneratorCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'sequenceGeneratorDataService', 'sequenceGeneratorFactory', 'RecentHistorySaveService', 'sequenceGeneratorValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, sequenceGeneratorDataService, sequenceGeneratorFactory, RecentHistorySaveService, sequenceGeneratorValidationService, Notification, roleConstant ) {
	
	
	  var vm = this;
      vm.tableHeadArr = sequenceGeneratorDataService.getHeadArray();
      vm.dataArr = [];
      vm.searchData = {};
      vm.sequenceGenerator = {};
      vm.limitArr = [10, 15, 20];
      vm.page = 0;
      vm.limit = 10;
      vm.totalRecord = 100;
      vm.showDetail=false;
      vm.showHistory = false;
      vm.errorMap = new Map();

      vm.sortSelection = {
          sortKey: "sequenceName",
          sortOrder: "asc"
      }
      
      vm.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              vm.searchData[attrname] = param[attrname];
          }
          vm.page = 0;
          vm.search();
      }

      vm.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          vm.searchData.orderByType = param.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = param.sortKey;
          vm.search();
      }
      
      
      /**
       * sequenceGenerator Master List Page Search Data  initializing and calling search Mtd.
       */
      vm.searchInit = function() {
          vm.searchData = {};
          vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = vm.sortSelection.sortKey;
          vm.search();
      };
      

      /**
       * Get sequenceGenerator Master's Based on Search Data.
       */
      vm.search = function() {
      	vm.sequenceGeneratorView=false;
          vm.searchData.selectedPageNumber = vm.page;
          vm.searchData.recordPerPage = vm.limit;
          vm.dataArr = [];
          sequenceGeneratorFactory.search.fetch(vm.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      vm.totalRecord = data.responseObject.totalRecord;
                      vm.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching SequenceGeneratorMaster');
              });
         }
      
      /* Master Select Picker */
      vm.ajaxServiceEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return sequenceGeneratorFactory.searchByService.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.serviceMasterList = data.responseObject.searchResult;
                      return vm.serviceMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching Service Master');
              }
          );

      }
      
      
      /* Master Select Picker */
      vm.ajaxChargeEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return sequenceGeneratorFactory.searchByCharge.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching charge Master');
              }
          );

      }
      
      
      /* Master Select Picker */
      vm.ajaxCategoryEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return sequenceGeneratorFactory.searchByCategory.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.categoryMasterList = data.responseObject.searchResult;
                      return vm.categoryMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching category Master');
              }
          );

      }
      
      vm.selectedMaster = function(obj,master){
    	  
    	  var validationResponse=sequenceGeneratorValidationService.validate(master);
    	  if(validationResponse.error==true){
    		  $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
    	  }
    	  else{
    		  validationResponse=false;
    		  vm.errorMap.put(obj, null);
    		  $rootScope.navigateToNextField(obj);
    	  }
      }
      
     
      
      
      
     /* *//**
       * Reason Master List Page Search Data  initializing and calling search Mtd.
       * @param {Object} data - The Selected Reason Master from List page.
       * @param {string} data.id - The id of the Selected Reason Master.
       * @param {int} index - The Selected row index from List page.
       *//*
      
      
      *//**
       * Modify Existing Reason Master.
       * @param {int} id - The id of a Country.
       *//*
      vm.edit = function(id) {
          $state.go("layout.reasonMasterEdit", {
              id: id
          });
      }
      
      // set status of reason master
      
      vm.setStatus = function(data){
  		if(data=='Active'){
  			return 'activetype';
  		} else if(data=='Block'){
  			return 'blockedtype';
  		}else if(data=='Hide'){
  			return 'hiddentype';
  		}
  	}
  	*/
      
      
      /**
       * Go Back to reason Master List Page.
       */
      
      vm.backToList = function() {
    	  //$state.go("layout.sequencegenerator");
    	  vm.sequenceGeneratorView=false;
        }
      
      
      /**
       * Go To Add SequenceGeneratorMaster.
       */
      
      vm.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_CREATE)){
    		  $state.go("layout.sequencegeneratorAdd");
    	  }
      }
      
      
      /**
       * Cancel Add or Edit SequenceGeneratorMaster.
       * @param {int} id - The id of a SequenceGeneratorMaster
       */
     
      
      vm.edit = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_MODIFY)){
    		  $state.go("layout.sequencegeneratorEdit", {
                  id: id
              });
    	  }
      }
      
      
      /**
       * Delete SequenceGeneratorMaster.
       * @param {int} id - The id of a SequenceGeneratorMaster.
       */
      
      vm.delete = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_DELETE)){
    		  var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR06604"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	sequenceGeneratorFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	//$state.go("layout.sequencegenerator");
    	                        	vm.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
    	  }    	  
        }
      
      
      //Update SequenceGeneratorMaster
      vm.update = function() {
    	  var validationResponse = sequenceGeneratorValidationService.validate(vm.sequenceGenerator,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the Reason..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
        	   sequenceGeneratorFactory.update.query(vm.sequenceGenerator).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.sequencegenerator');
		            } else {
		                $log.debug("Updating Reason Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating Reason Failed : " + error)
		        });
           } 
      };
     
      
      /**
       * Create New SequenceGeneratorMaster.
       */
      vm.create = function() {
      	var validationResponse = sequenceGeneratorValidationService.validate(vm.sequenceGenerator,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create ReasonMaster..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
          	sequenceGeneratorFactory.create.query(vm.sequenceGenerator).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.sequencegenerator');
		            } else {
		                $log.debug("Create Reason Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Reason Failed : " + error)
		        });
          } 
      };
      
      

      /**
       * Cancel Add or Edit SequenceGeneratorMaster.
       * @param {int} id - The id of a SequenceGeneratorMaster
       */
      vm.cancel = function(objId) {
          if(vm.sequenceGeneratorForm!=undefined){
		    	vm.goTocancel(vm.sequenceGeneratorForm.$dirty,objId);
          }else{
          	vm.goTocancel(false,objId);
          }
      }
     
     vm.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	vm.update();
   		            }else{
   		            	vm.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.sequencegenerator", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.sequencegenerator", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }
      
     
     /**
      * Common Validation Focus Functionality - starts here" 
      */
    
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
		
		
		/**
	       * Get SequenceGeneratorMaster  By id.
	       * @param {int} id - The id of a SequenceGeneratorMaster.
	       * @param {boolean} isView - TO identify purpose of calling is for View or Edit a SequenceGeneratorMaster.
	       */
	     
	      vm.view = function(id, isView) {
	    	  vm.id=id;
	      	sequenceGeneratorFactory.findById.query({
	              id: vm.id
	          }).$promise.then(function(data) {
	              if (data.responseCode == 'ERR0') {
	            	  $log.debug("response is sucess");
	                  vm.sequenceGenerator = data.responseObject;
	                  var rHistoryObj = {}

	                  if (vm.sequenceGenerator != undefined && vm.sequenceGenerator != null && vm.sequenceGenerator.id != undefined && vm.sequenceGenerator.id != null) {
	                      $rootScope.subTitle = "SequenceGenerator";
	                  } else {
	                      $rootScope.subTitle = "Unknown SequenceGenerator"
	                  }
	                  $rootScope.unfinishedFormTitle = "SequenceGenerator";
	                  if (isView) {
	                      rHistoryObj = {
	                          'title': 'SequenceGeneratorMaster',
	                          'subTitle': $rootScope.subTitle,
	                          'stateName': $state.current.name,
	                          'stateParam': JSON.stringify($stateParams),
	                          'stateCategory': 'SequenceGenerator Master',
	                          'serviceType': undefined,
	                          'showService': false
	                      }

	                  } else {

	                      rHistoryObj = {
	                          'title': 'SequenceGeneratorMaster',
	                          'subTitle': $rootScope.subTitle,
	                          'stateName': $state.current.name,
	                          'stateParam': JSON.stringify($stateParams),
	                          'stateCategory': 'SequenceGenerator Master',
	                          'serviceType': 'MASTER',
	                          'serviceCode': ' ',
	                          'orginAndDestination': ' '
	                      }
	                  }
	                  RecentHistorySaveService.form(rHistoryObj);
	              } else {
	                  $log.debug("Exception while getting SequenceGenerator by Id ", data.responseDescription);
	              }
	          }, function(error) {
	              $log.debug("Error ", error);
	          });
	      };
	      
	      /**
	       * SequenceGeneratorMaster  List Page Search Data  initializing and calling search Mtd.
	       * @param {Object} data - The Selected SequenceGeneratorMaster  from List page.
	       * @param {string} data.id - The id of the Selected SequenceGeneratorMaster.
	       * @param {int} index - The Selected row index from List page.
	       */
	      
	      vm.rowSelect = function(data) {
	    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_VIEW)){
	    		  vm.sequenceGenerator = data;
		  			vm.sequenceGeneratorView=true;
		  			 vm.showDetail=true;
		  			/*vm.id=data.id;
		  			$state.go("layout.sequencegeneratorView",{id:data.id});*/
		  			var windowInner=$window.innerWidth;
		  			if(windowInner<=1199){
		  				vm.deskTopView = false;
		  			}
		  			else{
		  				vm.deskTopView = true;
		  			}
		  			angular.element($window).bind('resize', function(){
		  				if($window.innerWidth>=1200){
		  					vm.deskTopView = true;
		  				}
		  				else if($window.innerWidth<=1199){
		  					if(vm.reasonMaster.id!==false){
		  						vm.deskTopView = false;
		  					}
		  					else {
		  						vm.deskTopView = true;
		  					}
		  				}
		  				$scope.$digest();
		  			});
	    		  
	    	  }
	  		}
	     
	      
	      
      
      
      /**
       * State Identification Handling Here.
       * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
       */
      //
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
              $rootScope.breadcrumbArr = sequenceGeneratorDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if (vm.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.issequenceGeneratorReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 vm.view($stateParams.id, false);
                   }
                 }
              $rootScope.breadcrumbArr = sequenceGeneratorDataService.getEditBreadCrumb();
              break;
          case "VIEW":
        	  vm.view($stateParams.id, false);
        	  $rootScope.breadcrumbArr = sequenceGeneratorDataService.getViewBreadCrumb();
        	  break;
          default:
              $rootScope.breadcrumbArr = sequenceGeneratorDataService.getListBreadCrumb();
              vm.searchInit();
      }
      
 
}
]);
}) ();