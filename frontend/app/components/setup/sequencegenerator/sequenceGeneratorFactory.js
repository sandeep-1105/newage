app.factory('sequenceGeneratorFactory', function($resource) {
	return {
		findById : $resource('/api/v1/sequencegenerator/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/sequencegenerator/get/search/impl', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/sequencegenerator/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/sequencegenerator/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		create : $resource('/api/v1/sequencegenerator/create', {}, {
			query : {method : 'POST'}
		})
		,
		searchByService : $resource("/api/v1/servicemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByCharge : $resource("/api/v1/chargemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByCategory : $resource("/api/v1/categorymaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
	};
})