app.service('sequenceGeneratorValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(sequenceGenerator, code) {
		//Validate sequenceGenerator service code
		if(code == 0 || code == 1) {
			if(sequenceGenerator.sequenceName == undefined || sequenceGenerator.sequenceName == null || sequenceGenerator.sequenceName =="" ){
				return this.validationResponse(true, "sequenceName", $rootScope.nls["ERR1408"], sequenceGenerator);
			}
		}
		//Validate StcGroupMaster charge code
		if(code == 0 || code == 2) {
			if(sequenceGenerator.sequenceType == undefined || sequenceGenerator.sequenceType == null || sequenceGenerator.sequenceType =="" ){
				return this.validationResponse(true, "sequenceType", $rootScope.nls["ERR2310"], sequenceGenerator);
			}
		}
		
		//Validate StcGroupMaster Category code
		if(code == 0 || code == 3) {
			if(sequenceGenerator.currentSequenceValue == undefined || sequenceGenerator.currentSequenceValue == null || sequenceGenerator.currentSequenceValue =="" ){
				return this.validationResponse(true, "currentSequenceValue", $rootScope.nls["ERR1307"], sequenceGenerator);
			}
		}
		
		if(code == 0 || code == 3) {
			if(sequenceGenerator.prefix == undefined || sequenceGenerator.prefix == null || sequenceGenerator.prefix =="" ){
				return this.validationResponse(true, "prefix", $rootScope.nls["ERR1307"], sequenceGenerator);
			}
		}
		
		if(code == 0 || code == 3) {
			if(sequenceGenerator.format == undefined || sequenceGenerator.format == null || sequenceGenerator.format =="" ){
				return this.validationResponse(true, "format", $rootScope.nls["ERR1307"], sequenceGenerator);
			}
		}
		
		
		return this.validationSuccesResponse(sequenceGenerator);
	}
		
 
	this.validationSuccesResponse = function(sequenceGenerator) {
	    return {error : false, obj : sequenceGenerator};
	}
	
	this.validationResponse = function(err, elem, message, sequenceGenerator) {
		return {error : err, errElement : elem, errMessage : message, obj : sequenceGenerator};
	}
	
	
}]);