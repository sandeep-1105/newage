app.service('sequenceGeneratorDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Sequence Name",
					"width":"col-md-1half",
					"model":"sequenceName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchSequenceName"
				},{
					"name":"Sequence Type",
					"width":"col-md-2",
					"model":"sequenceType",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['SequenceType'],
					"key":"searchSequenceType",
					"sort":true
				},{
					"name":"Current Sequence 0Value",
					"width":"col-md-1half",
					"model":"currentSequenceValue",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchCurrentSequenceValue"
				},{
					"name":"Prefix",
					"width":"col-md-1half",
					"model":"prefix",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchPrefix"
				},{
					"name":"Sequence Format",
					"width":"col-md-2",
					"model":"format",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['SequenceFormat'],
					"key":"searchFormat",
					"sort":true
				}
				];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.sequencegenerator"
            },
            {
                label: "Sequence Generator",
                state: "layout.sequencegenerator"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.sequencegenerator"
            },
            {
                label: "Sequence Generator",
                state: "layout.sequencegenerator"
            },
            {
                label: "Add Sequence Generator",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.sequencegenerator"
            },
            {
            	label: "Sequence Generator",
                state: "layout.sequencegenerator"
            },
            {
                label: "Edit Sequence Generator",
                state: null
            }
        ];
    };
    
    this.getViewBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.sequencegenerator"
            },
            {
            	label: "Sequence Generator",
                state: "layout.sequencegenerator"
            },
            {
                label: "View Sequence Generator",
                state: null
            }
        ];
    };
   

});