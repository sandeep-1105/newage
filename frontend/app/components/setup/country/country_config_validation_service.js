app.service('CountryConfigValidationService', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(dataObject, code) {
		//Validate Buyer's Consolidation

		if (code == 0 || code == 1) {
            if (dataObject.fieldName == undefined || dataObject.fieldName == null || dataObject.fieldName == "" ) {
            	return  this.validationResponse(true, "countryConfFieldName" , "none", -1, $rootScope.nls["ERR0171"], dataObject);
            } else if(!CommonValidationService.checkFormat(dataObject.fieldName, appConstant.freeText250CharsExpr)){
            	return this.validationResponse(true, "countryConfFieldName" , "none", -1, $rootScope.nls["ERR0172"], dataObject);
            }
        }

		if (code == 0 || code == 2) {
            if (dataObject.fieldId == undefined || dataObject.fieldId == null || dataObject.fieldId == "" ) {
            	return this.validationResponse(true, "countryConfFieldId" , "none", -1, $rootScope.nls["ERR0174"], dataObject);
            } else if(!CommonValidationService.checkFormat(dataObject.fieldName, appConstant.freeText250CharsExpr)){
            	return this.validationResponse(true, "countryConfFieldId" , "none", -1, $rootScope.nls["ERR0175"], dataObject);
            }
        }
		
		if (code == 0 || code == 3) {
            if (dataObject.screen != undefined && dataObject.screen != null && dataObject.screen != ""  && !CommonValidationService.checkFormat(dataObject.screen, appConstant.freeText250CharsExpr)) {
            	return this.validationResponse(true, "countryConfScreen" , "none", -1, $rootScope.nls["ERR0177"], dataObject);
            }
        }
		if (code == 0 || code == 4) {
            if (dataObject.screentab != undefined && dataObject.screentab != null && dataObject.screentab != "" && !CommonValidationService.checkFormat(dataObject.screentab, appConstant.freeText250CharsExpr)) {
            	return this.validationResponse(true, "countryConfScreenTab" , "none", -1, $rootScope.nls["ERR0178"], dataObject);
            } 
        }

		if (code == 0 || code == 5) {
            if (dataObject.remark != undefined && dataObject.remark != null && dataObject.remark != "" && !CommonValidationService.checkFormat(dataObject.remark, appConstant.freeText250CharsExpr)) {
            	return this.validationResponse(true, "countryConfRemark" , "none", -1, $rootScope.nls["ERR0179"], dataObject);
            } 
        }
		return this.validationSuccesResponse(dataObject);
	}

	this.validateCountry = function(dfCountry, index) {
		var errorArr = [];
		if (dfCountry == undefined || dfCountry == null || dfCountry.id == undefined || dfCountry.id == null || dfCountry.id == "") {
			errorArr.push(this.validationResponse(true, "countryConfCountry"+ index, "none", index, $rootScope.nls["ERR0180"], dfCountry));
		} else if(dfCountry.status != null && dfCountry.status == "Block") {
				errorArr.push(this.validationResponse(true, "countryConfCountry"+ index, "none", index, $rootScope.nls["ERR0182"], dfCountry));
        }
		 else if(dfCountry.status != null && dfCountry.status == "Hide") {
			errorArr.push(this.validationResponse(true, "countryConfCountry"+ index, "none", index, $rootScope.nls["ERR0183"], dfCountry));
		}
        return errorArr;
	}
	


	this.validationSuccesResponse = function(dataObject) {
	    return {error : false, obj : dataObject};
	}

	this.validationResponse = function(err, elem, tab, index, message, dataObject) {
		return {error : err, errElement : elem, tab : tab, index :index, errMessage : message, obj : dataObject};
	}
	
});