app.controller("country_field_edit_Ctrl", ['$rootScope', '$scope', '$location', 'ngDialog', '$http', '$modal', '$state', '$stateParams', '$timeout',
    'ngProgressFactory', 'cloneService', 'CountryList', 'appMetaFactory',
    'CountryConfigValidationService', 'CommonValidationService', 'countryConfigFactory', 'RecentHistorySaveService',
    function($rootScope, $scope, $location, ngDialog, $http, $modal, $state, $stateParams, $timeout,
        ngProgressFactory, cloneService, CountryList, appMetaFactory,
        CountryConfigValidationService, CommonValidationService, countryConfigFactory, RecentHistorySaveService) {

        $scope.oldData = "";
        $scope.errorMap = new Map();
        $scope.disableSubmitBtn = false;
        $scope.screenArr = ["Enquiry", "Master Shipment", $rootScope.appMasterData['Party_Label'] + " Master", "Shipment", "Invoice"];
        $scope.diasbleSubmitBtnFn = function() {
            $scope.disableSubmitBtn = true;
            $timeout(function() {
                $scope.disableSubmitBtn = false;
            }, 2000);
        }
        $scope.setOldDataVal = function() {

            $timeout(function() {
                $scope.oldData = JSON.stringify($scope.countryDynamicField);
            }, 2000);
        }
        $scope.submitDynamicField = function() {
            $scope.diasbleSubmitBtnFn();
            if ($scope.oldData == JSON.stringify($scope.countryDynamicField)) {
                console.log("both are Equal");
            } else {
                console.log("both are Not Equal");
            }
            console.log("OLD " + $scope.oldData);
            console.log("NEW " + JSON.stringify($scope.countryDynamicField));
            var validationResponse = CountryConfigValidationService.validate($scope.countryDynamicField, 0);
            console.log("Validation Response -- ", validationResponse);
            if (validationResponse.error == true) {
                console.log("Validation Response 2-- ", validationResponse);
                $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
            } else {
                var childError = false;
                console.log("1 - ", childError);
                if ($scope.validateCountry(true)) {
                    childError = true;
                }
                if (!childError) {
                    console.log("3 - ", childError);
                    $scope.tmpObj = cloneService.clone($scope.countryDynamicField);
                    countryConfigFactory.common.create($scope.tmpObj).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("After Save Response :: ", data);

                            if ($rootScope.userProfile != null && $rootScope.userProfile.selectedUserLocation != null &&
                                $rootScope.userProfile.selectedUserLocation.countryMaster != null && $rootScope.userProfile.selectedUserLocation.countryMaster.id != null) {

                                appMetaFactory.country.dynamicFields({
                                        countryId: $rootScope.userProfile.selectedUserLocation.countryMaster.id
                                    }, function(data) {
                                        //console.log("REsponse -- ",JSON.stringify(data));
                                        $rootScope.dynamicFields = data.responseObject;
                                        $rootScope.dynamicFormFieldMap = new Map();
                                        if ($rootScope.dynamicFields != undefined && $rootScope.dynamicFields != null && $rootScope.dynamicFields.length > 0) {
                                            for (var it = 0; it < $rootScope.dynamicFields.length; it++) {
                                                $rootScope.dynamicFormFieldMap.put($rootScope.dynamicFields[it].fieldName, $rootScope.dynamicFields[it].showField);
                                            }
                                        }
                                    },
                                    function(error) {
                                        console.log("Error");
                                    });
                            }
                            $state.go('layout.countryDynamicField', {
                                submitAction: "Saved"
                            });
                        } else {
                            $scope.addEmptyRows();
                            console.log("PO added Failed " + data.responseDescription)
                        }

                    }, function(error) {
                        $scope.addEmptyRows();
                        console.log("PO added Failed : " + error)
                    });

                }
            }
        }

        $scope.cancel = function() {
            if ($scope.oldData == JSON.stringify($scope.countryDynamicField)) {
                console.log("both are Equal");

                $state.go('layout.countryDynamicField', {
                    submitAction: "Cancelled"
                });
            } else {

                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(function(value) {
                        if (value == 1) {
                            $scope.submitDynamicField();
                        } else if (value == 2) {
                            $state.go('layout.countryDynamicField', {
                                submitAction: "Cancelled"
                            });
                        } else {
                            console.log("cancelled");
                        }
                    });
            }

        };



        // Buyer's Consolidation - dropdown starts Here
        $scope.ajaxCountryEvent = function(keyword) {
            $scope.searchCountry = {};
            $scope.searchCountry.keyword = keyword == null ? "" : keyword.trim();
            $scope.searchCountry.selectedPageNumber = 0;
            $scope.searchCountry.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.searchCountry.id = "";
            var scountryId = "";
            for (var ij = 0; ij < $scope.countryDynamicField.countryMasterList.length; ij++) {
                if ($scope.countryDynamicField.countryMasterList[ij] != null && $scope.countryDynamicField.countryMasterList[ij].id != undefined && $scope.countryDynamicField.countryMasterList[ij].id != null) {
                    scountryId = scountryId + $scope.countryDynamicField.countryMasterList[ij].id + ",";
                }
            }
            if (scountryId != "") {
                scountryId = scountryId.substring(0, (scountryId.length - 1));
            }
            $scope.searchCountry.id = scountryId;
            return CountryList.fetch($scope.searchCountry).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    } else {
                        console.log("added Failed ", data.responseDescription)
                    }
                },
                function(error) {
                    console.log("added Failed : ", error)
                });
        }

        $scope.selectedCountry = function(elemId) {
            //		$scope.validationErrorAndFocus(CountryConfigValidationService.validateCountry($scope.countryDynamicField, 1), elemId);
        }

        $scope.countryRender = function(item) {
                return {
                    label: item.countryName,
                    item: item
                }
            }
            // Buyer's Consolidation - dropdown starts Here

        //Input Focus Navigation - Starts Here
        $scope.navigateToNextField = function(id) {
                var focElement = document.getElementById(id);
                if (focElement != null) {
                    focElement.focus();
                }
            }
            //Input Focus Navigation - Ends Here

        //Common Validation Focus Functionality - starts here
        $scope.validationErrorAndFocus = function(valResponse, elemId) {
            $scope.errorMap = new Map();
            if (valResponse.error == true) {
                if (valResponse.tab == "none") {
                    $scope.countryDynamicField = valResponse.obj;
                } else if (valResponse.tab == "tab1") {
                    $scope.countryDynamicField.countryMasterList[valResponse.index] = valResponse.obj;
                    $scope.Tabs = "tab1";
                }
                $scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
                console.log("$scope.valResponse -- ", valResponse);
                console.log("$scope.errorMap -- ", $scope.errorMap);
            }
            $scope.navigateToNextField(elemId);
        }

        $scope.validateCountry = function(isSubmit) {
            var isError = false,
                sliceArrIndx = [];
            console.log("isSubmit -- ", isSubmit);
            console.log("Starts countryMasterList ", $scope.countryDynamicField.countryMasterList);
            for (var i = 0; i < $scope.countryDynamicField.countryMasterList.length; i++) {
                if (CommonValidationService.checkObjectIsEmpty($scope.countryDynamicField.countryMasterList[i])) {
                    //$scope.countryDynamicField.countryMasterList[i] = {};
                    if (isSubmit) {
                        sliceArrIndx.push(i);
                    } else {
                        $scope.countryDynamicField.countryMasterList[i].error = true;
                        $scope.countryDynamicField.countryMasterList[i].errorList = [$rootScope.nls["ERR0180"]];
                        $scope.errorMap.put("countryConfCountry" + i, $rootScope.nls["ERR0180"]);
                        isError = true;
                    }
                } else {
                    console.log("NonEmpty Object");
                    var validationResponse = CountryConfigValidationService.validateCountry($scope.countryDynamicField.countryMasterList[i], i);
                    $scope.countryDynamicField.countryMasterList[i].error = false;
                    $scope.countryDynamicField.countryMasterList[i].errorList = [];
                    if (validationResponse instanceof Array) {
                        for (var j = 0; j < validationResponse.length; j++) {
                            if (validationResponse[j].error != undefined && validationResponse[j].error == true) {
                                $scope.countryDynamicField.countryMasterList[i].error = true;
                                if ($scope.countryDynamicField.countryMasterList[i].errorList == undefined) {
                                    $scope.countryDynamicField.countryMasterList[i].errorList = [];
                                }
                                $scope.countryDynamicField.countryMasterList[i].errorList.push(validationResponse[j].errMessage);
                                isError = true;
                                console.log("Chk in isError -- ", isError);
                                $scope.errorMap.put(validationResponse[j].errElement, "Error");
                            }
                        }
                    }
                }
            }
            if (isSubmit && sliceArrIndx.length > 0) {
                for (var i = 0; i < sliceArrIndx.length; i++) {
                    $scope.countryDynamicField.countryMasterList.splice(sliceArrIndx[i], 1);
                }
            }
            console.log("Chk in isError -- ", isError);
            return isError;
        }



        $scope.removeCountry = function(index) {
            $scope.errorMap = new Map();
            if ($scope.countryDynamicField.countryMasterList.length > 1) {
                $scope.countryDynamicField.countryMasterList.splice(index, 1);
            } else {
                $scope.countryDynamicField.countryMasterList = [{}];
            }
        }

        //Common Validation Focus Functionality - ends here
        $scope.addNewCountry = function() {
            $scope.errorMap = new Map();
            if (!$scope.validateCountry(false)) {
                $scope.countryDynamicField.countryMasterList.push({});
            }

            if ($scope.countryDynamicField.countryMasterList.length > 0) {
                $scope.navigateToNextField("countryConfCountry" + $scope.countryDynamicField.countryMasterList.length);
            }
        }

        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'errorPopUp.html',
            show: false,
            keyboard: true
        });

        var errorOnRowIndex = null;

        $scope.errorShow = function(errorArr, index) {
            $scope.errList = errorArr;
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };
        $scope.addInit = function() {

            $scope.countryDynamicField = {};
            $scope.addEmptyRows();
            console.log("$scope.countryDynamicField -- ", $scope.countryDynamicField);
        }


        $scope.editInit = function(id) {

            console.log("Edit Init method is called ", id);

            countryConfigFactory.common.findById({
                id: id
            }).$promise.then(function(data) {

                if (data.responseCode == 'ERR0') {
                    console.log("Server Message : ", data.responseDescription);

                    $scope.countryDynamicField = data.responseObject;
                    $scope.addEmptyRows();
                    if ($scope.countryDynamicField != undefined && $scope.countryDynamicField != null && $scope.countryDynamicField.fieldName != undefined && $scope.countryDynamicField.fieldName != null) {
                        $rootScope.subTitle = $scope.countryDynamicField.fieldName;
                    } else {
                        $rootScope.subTitle = "Empty Fieldname"
                    }
                    var rHistoryObj = {
                        'title': "Country DynamicField Edit # " + $scope.countryDynamicField.fieldName,
                        'subTitle': $rootScope.subTitle,
                        'stateName': $state.current.name,
                        'stateParam': JSON.stringify($stateParams),
                        'stateCategory': 'countryDynamicField',
                        'serviceType': 'AIR',
                        'showService': false
                    }

                    RecentHistorySaveService.form(rHistoryObj);
                    $scope.setOldDataVal();
                } else {
                    console.log("Exception while getting Purchase Order by Id ", data.responseDescription);
                }

            }, function(error) {
                console.log("Error ", error);
            });

        };

        $scope.addEmptyRows = function() {
            if ($scope.countryDynamicField != undefined && $scope.countryDynamicField != null) {
                if ($scope.countryDynamicField.countryMasterList == undefined || $scope.countryDynamicField.countryMasterList == null ||
                    $scope.countryDynamicField.countryMasterList.length == 0) {
                    $scope.countryDynamicField.countryMasterList = [{}];
                }
            }

        }


        //On leave the Unfilled Enquiry Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js
        //This block is listener for Add Sales Enquiry Event
        /*
        	'data' : $rootScope.unfinishedData,
        	'title' : $rootScope.unfinishedFormTitle,*/
        $scope.$on('addCountryDynamicFieldEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.countryDynamicField;
            $rootScope.category = "Country DynamicField";
            $rootScope.unfinishedFormTitle = "Country DynamicField (New)";
            if ($scope.countryDynamicField != undefined && $scope.countryDynamicField != null && $scope.countryDynamicField.fieldName != undefined && $scope.countryDynamicField.fieldName != null) {
                $rootScope.subTitle = $scope.countryDynamicField.fieldName;
            } else {
                $rootScope.subTitle = "Empty Fieldname"
            }
        });

        $scope.$on('editCountryDynamicFieldEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.countryDynamicField;
            $rootScope.category = "Country DynamicField";
            $rootScope.unfinishedFormTitle = "Country DynamicField Edit # " + $scope.countryDynamicField.poNo;
            if ($scope.countryDynamicField != undefined && $scope.countryDynamicField != null && $scope.countryDynamicField.fieldName != undefined && $scope.countryDynamicField.fieldName != null) {
                $rootScope.subTitle = $scope.countryDynamicField.fieldName;
            } else {
                $rootScope.subTitle = "Empty Fieldname"
            }
        });

        $scope.$on('addCountryDynamicFieldEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.countryDynamicField);
            localStorage.isDynamicFieldReloaded = "YES";
            e.preventDefault();
        });

        $scope.$on('editCountryDynamicFieldEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.countryDynamicField);
            localStorage.isDynamicFieldReloaded = "YES";
            e.preventDefault();
        });

        $scope.isReloaded = localStorage.isDynamicFieldReloaded;

        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDynamicFieldReloaded = "NO";
                    $scope.countryDynamicField = JSON.parse(localStorage.reloadFormData);
                    //default focus for this page 
                } else {
                    $scope.countryDynamicField = $rootScope.selectedUnfilledFormData;
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDynamicFieldReloaded = "NO";
                    $scope.countryDynamicField = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.addInit();
                }
            }
            $scope.setOldDataVal();
            $rootScope.breadcrumbArr = [{
                    label: "Setup",
                    state: "layout.countryDynamicField"
                },
                {
                    label: "Country Configuration",
                    state: "layout.countryDynamicField"
                },
                {
                    label: "Add Country Configuration",
                    state: null
                }
            ];
        } else if ($stateParams.action == "EDIT") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDynamicFieldReloaded = "NO";
                    $scope.countryDynamicField = JSON.parse(localStorage.reloadFormData);
                    //default focus for this page 
                } else {
                    $scope.countryDynamicField = $rootScope.selectedUnfilledFormData;
                }
                $scope.setOldDataVal();
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDynamicFieldReloaded = "NO";
                    $scope.countryDynamicField = JSON.parse(localStorage.reloadFormData);
                    $scope.setOldDataVal();
                } else {
                    $scope.editInit($stateParams.id);
                }
            }

            $rootScope.breadcrumbArr = [{
                    label: "Setup",
                    state: "layout.countryDynamicField"
                },
                {
                    label: "Country Configuration",
                    state: "layout.countryDynamicField"
                },
                {
                    label: "Edit Country Configuration",
                    state: null
                }
            ];
        }

    }
]);