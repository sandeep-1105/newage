app.controller('countryConfigCtrl',['$rootScope', '$scope', '$http', '$location', '$window', 'CountryList',
    'ngTableParams', 'ngDialog', '$state', '$stateParams', '$modal', 'countryConfigFactory', 
    'RecentHistorySaveService', 'roleConstant',
	function($rootScope, $scope, $http, $location, $window, CountryList,
    ngTableParams, ngDialog, $state, $stateParams, $modal, countryConfigFactory, 
    RecentHistorySaveService, roleConstant) {

    $scope.selectedCountry = {};
    $scope.countryHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false
    }, {
        "name": "Field Name",
        "width": "col-xs-3",
        "model": "fieldName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "fieldName"

    }, {
        "name": "Screen",
        "width": "col-xs-2",
        "model": "screen",
        "search": true,
        "sort": true,
        "type": "text",
        "key": "screen"
    }, {
        "name": "Section / Tab",
        "width": "col-xs-2",
        "model": "screentab",
        "search": true,
        "sort": true,
        "type": "text",
        "key": "section"
    }, {
        "name": "Remarks",
        "width": "col-xs-3",
        "model": "remark",
        "search": false,
        "type": "text",
    }];

    $scope.sortSelection = {
        sortKey: "name",
        sortOrder: "asc"
    };


    $scope.sortSelection = {
        sortKey: "po.id",
        sortOrder: "desc"
    };

    $scope.searchData = {};

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;
    $scope.totalRecord = 0;

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.enquirysearch($scope.page, $scope.limit);
    }
    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search($scope.page, $scope.limit);
    }

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.SETUP_COUNTRY_CONFIGURATION_VIEW)) {

            var sParam = JSON.parse(JSON.stringify($scope.searchData));;


            $scope.selectedRecordIndex = ($scope.searchData.recordPerPage * $scope.searchData.selectedPageNumber) + index;
            sParam.id = data.id;
            sParam.selectedPageNumber = $scope.selectedRecordIndex;
            sParam.totalRecord = $scope.totalRecord;
            sParam.recordPerPage = 1;
            if ($scope.searchData.countryId != undefined && $scope.searchData.countryId != null && $scope.searchData.countryId != "") {
                sParam.countryId = $scope.searchData.countryId;
            }
            if ($scope.searchData.fieldName != undefined && $scope.searchData.fieldName != null && $scope.searchData.fieldName != "") {
                sParam.fieldName = $scope.searchData.fieldName;
            }
            if ($scope.searchData.screen != undefined && $scope.searchData.screen != null && $scope.searchData.screen != "") {
                sParam.screen = $scope.searchData.screen;
            }
            if ($scope.searchData.section != undefined && $scope.searchData.section != null && $scope.searchData.section != "") {
                sParam.section = $scope.searchData.section;
            }
            $state.go("layout.viewCountryDynamicField", sParam);

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }

            /********************* ux fix ends here *****************************/

        }
    };
    $scope.cancel = function() {
        $scope.showDetail = false;
        $scope.showLogs = false;
        $scope.showHistory = false;
        $scope.chargeMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };
    $scope.back = function() {
        var param = {};
        $state.go("layout.countryDynamicField");
    };
    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_COUNTRY_CONFIGURATION_CREATE)) {
            $state.go("layout.addCountryDynamicField");
        }
    };

    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.SETUP_COUNTRY_CONFIGURATION_MODIFY)) {
            $state.go("layout.editCountryDynamicField", {
                id: $stateParams.id
            });
        }
    };
    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            console.log("attrname - ", attrname, " ; Value - ", param[attrname]);
            $scope.searchData[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
    }


    $scope.sortChange = function(param) {
        console.log("Sort Param ,", param);
        switch (param.sortKey) {
            case "screentab":
                $scope.searchData.sortByColumn = "df.screentab";
                break;
            case "screen":
                $scope.searchData.sortByColumn = "df.screen";
                break;
            case "fieldName":
                $scope.searchData.sortByColumn = "df.fieldName";
                break;
            default:
                $scope.searchData.sortByColumn = "df.id";
        }
        $scope.searchData.orderByType = param.sortOrder.toUpperCase();
        $scope.search();
    };

    $scope.search = function() {
        $scope.searchData.selectedPageNumber = $scope.page;
        $scope.searchData.recordPerPage = $scope.limit;
        //		$scope.searchData.fieldName = "san";
        $scope.countryDynamicFieldArr = [];
        countryConfigFactory.search.query($scope.searchData).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            $scope.countryDynamicFieldArr = data.responseObject.searchResult;

        });
    }


    $scope.view = function(id) {

        console.log("View method is called ", id);

        countryConfigFactory.common.findById({
            id: id
        }).$promise.then(function(data) {

            if (data.responseCode == 'ERR0') {
                $scope.countryDynamicField = data.responseObject;

                $rootScope.subTitle = $scope.countryDynamicField.screen
                $rootScope.unfinishedFormTitle = "Country DynamicField View # " + $scope.countryDynamicField.fieldName;
                var rHistoryObj = {
                    'title': "Country DynamicField View #" + $scope.countryDynamicField.fieldName,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'CountryDynamicField',
                    'showService': false
                }

                RecentHistorySaveService.form(rHistoryObj);
            } else {
                console.log("Exception while getting Country DynamicField by Id ", data.responseDescription);
            }

        }, function(error) {
            console.log("Error ", error);
        });

    };


    // Buyer's Consolidation - dropdown starts Here
    $scope.ajaxCountryEvent = function(keyword) {
        console.log("$scope.searchBuyer -- ", $scope.searchCountry);
        $scope.searchCountry = {};
        $scope.searchCountry.keyword = keyword == null ? "" : keyword.trim();
        $scope.searchCountry.selectedPageNumber = 0;
        $scope.searchCountry.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CountryList.fetch($scope.searchCountry).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                } else {
                    console.log("added Failed ", data.responseDescription)
                }
            },
            function(error) {
                console.log("added Failed : ", error)
            });
    }

    $scope.changeCountry = function() {
        if ($scope.selectedCountry != undefined && $scope.selectedCountry != null && $scope.selectedCountry.id != null && $scope.selectedCountry.id != "") {

            $scope.searchData.countryId = $scope.selectedCountry.id;
            $scope.search();
        } else {
            $scope.searchData = {};
            $scope.search();
        }
    }

    $scope.countryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }

    $scope.singlePageNavigation = function(val) {
        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            $scope.searchData.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
            $scope.searchData.recordPerPage = 1;
            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchData.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchData.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.fieldId != undefined && $stateParams.fieldId != null) {
                stateParameters.fieldId = $scope.searchData.fieldId = $stateParams.fieldId;
            }
            if ($stateParams.screen != undefined && $stateParams.screen != null) {
                stateParameters.screen = $scope.searchData.screen = $stateParams.screen;
            }
            if ($stateParams.section != undefined && $stateParams.section != null) {
                stateParameters.section = $scope.searchData.section = $stateParams.section;
            }
            if ($stateParams.countryId != undefined && $stateParams.countryId != null) {
                stateParameters.countryId = $scope.searchData.countryId = $stateParams.countryId;
            }
        } else {
            return;
        }
        countryConfigFactory.search.query($scope.searchData).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.selectedPageNumber = $scope.searchData.selectedPageNumber;
            stateParameters.id = data.responseObject.searchResult[0].id;
            $scope.hidePage = false;
            $state.go("layout.viewCountryDynamicField", stateParameters);

        });
    }
    if ($stateParams.action == "SEARCH") {
        console.log("Search Page....");
        $scope.search();

        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.countryDynamicField"
            },
            {
                label: "Country Configuration",
                state: null
            }
        ];

    } else if ($stateParams.action == "VIEW") {

        console.log("View Page....", $stateParams.id);

        $scope.view($stateParams.id);

        $scope.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
        $scope.totalRecord = parseInt($stateParams.totalRecord);
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.countryDynamicField"
            },
            {
                label: "Country Configuration",
                state: "layout.countryDynamicField"
            },
            {
                label: "View Country Configuration",
                state: null
            }
        ];
    }

}]);