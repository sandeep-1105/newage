app.factory('countryConfigFactory', function($resource) {

	return {
		common : $resource('/api/v1/dynamicfield/:id', {}, {
			create : {method : 'POST'},
			findById : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		search : $resource('/api/v1/dynamicfield/search', {}, {
			query : {method : 'POST'}
		})
	};
})