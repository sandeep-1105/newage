app.controller('userCtrl', ['$rootScope', '$scope', '$http', '$location', 'ngProgressFactory', '$window',
    'ngTableParams', '$state', '$stateParams', 'ngDialog', 'FeatureList', 'UserView', 'UserService',
    'UserSearch', 'UserActivate', 'UserDeactivate', 'UserRemove', 'SearchUserByKey', 'UserEdit', 'roleConstant', 'Notification',
    function ($rootScope, $scope, $http, $location, ngProgressFactory, $window,
        ngTableParams, $state, $stateParams, ngDialog, FeatureList, UserView, UserService,
        UserSearch, UserActivate, UserDeactivate, UserRemove, SearchUserByKey, UserEdit, roleConstant, Notification) {

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/

        $scope.tempUserStatus = $rootScope.enum['UserStatus'];
        $scope.userStatus = [];
        for (var i = 0; i < $scope.tempUserStatus.length; i++) {
            $scope.userStatus.push($scope.tempUserStatus[i].replace(/_/g, ' '));
        }
        $scope.usersHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false,
        },
        {
            "name": "Name",
            "width": "col-xs-4",
            "model": "userName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchUserName"

        },
        {
            "name": "Status",
            "width": "col-xs-2",
            "model": "status",
            "search": true,
            "sort": true,
            "type": "drop",
            "data": $scope.userStatus,
            "key": "searchStatus"
        }
        ];

        $scope.sortSelection = {
            sortKey: "userName",
            sortOrder: "asc"
        }


        $scope.sortSelection = {
            sortKey: "userName",
            sortOrder: "asc"
        }



        $scope.user = {};
        $scope.selectedMaster = {};
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        UserService.set({});

        $scope.init = function () {

            console.log("user Init method called.................");
            $scope.contained_progressbar = ngProgressFactory.createInstance();
            $scope.contained_progressbar.setParent(document.getElementById('user-view'));
            $scope.contained_progressbar.setAbsolute();
            $rootScope.setNavigate1("Setup");
            $rootScope.setNavigate2("User Creation");
            UserService.set({});
            $scope.searchDto = {};
            $scope.user = {};
            $scope.Tab = "users";
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_ROLES_VIEW, true)) {
                $scope.roleCheck = true;
            } else {
                $scope.roleCheck = false;
            }
            $scope.search();

            if ($scope.user.id == null)
                $scope.user.status = 'Active';
        };


        $(".toggle-password").click(function () {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });


        $scope.limitChange = function (item) {
            console.log("Limit Change is called...", item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.tabSelect = function (obj) {
            if (obj === "users") {
                if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_ROLES_VIEW)) {
                    $scope.Tab = "users";
                }
            } else {
                if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_VIEW)) {
                    $scope.Tab = "company";
                }
            }
        }



        $scope.rowSelect = function (data) {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_VIEW)) {
                console.log("Data", data);
                $scope.errorMap = new Map();
                $scope.showHistory = false;
                $rootScope.mainpreloder = true;
                UserView.get({
                    id: data.id
                },
                    function (data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("User loaded Successfully")
                            $scope.showLogs = false;
                            $rootScope.mainpreloder = false;
                            $scope.user = data.responseObject;

                        } else {
                            console.log("User loaded Failed " + data.responseDescription)
                        }
                    },
                    function (error) {
                        console.log("User loaded Failed : " + error)
                    });

                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }

                /********************* ux fix ends here *****************************/
            }
        }



        $scope.changeSearch = function (param) {

            console.log("Change Search", param)
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }

        $scope.sortChange = function (param) {
            console.log("Sort Change Called.", param);
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        }

        $scope.changepage = function (param) {
            console.log("Change Page Called.", param);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }



        $scope.search = function () {

            $rootScope.mainpreloder = true;
            console.log("Search method is called............................!");

            $scope.user = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            if ($scope.searchDto.searchStatus != "null" && $scope.searchDto.searchStatus != null && $scope.searchDto.searchStatus != undefined)
                $scope.searchDto.searchStatus = String($scope.searchDto.searchStatus).replace(/ /g, '_');
            $scope.usersArr = [];


            SearchUserByKey.query($scope.searchDto).$promise.then(function (
                data, status) {

                $scope.totalRecord = data.responseObject.totalRecord;
                console.log($scope.totalRecord);
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function (item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    tempObj.status = tempObj.status.replace(/_/g, ' ');
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.usersArr = resultArr;
                $rootScope.mainpreloder = false;
            }, function (error) {
                $rootScope.mainpreloder = false;
            });

        }

        $scope.serialNo = function (index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
                (index + 1);
            return index;
        }




        $scope.deleteUser = function () {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_DELETE)) {
                ngDialog
                    .openConfirm({
                        template: '<p>Are you sure you want to delete selected user?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function (value) {
                            UserRemove.remove({
                                id: $scope.user.id
                            },
                                function (data) {
                                    if (data.responseCode == 'ERR0') {
                                        console.log("User deleted Successfully")
                                        $scope.init();
                                        $scope.deskTopView = true;
                                    } else {
                                        console.log("User deleted Failed " + data.responseDescription)
                                    }
                                },
                                function (error) {
                                    console.log("User deleted Failed : " + error)
                                });
                        },
                        function (value) {
                            console.log("deleted cancelled");
                        });
            }
        }


        $scope.editUser = function (data) {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_MODIFY)) {
                console.log("Edit Button is Pressed.");
                UserService.set($scope.user);
                $state.go("layout.editUser", {
                    userId: $scope.user.id
                });
            }
        }




        $scope.updateStatus = function () {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_DEACTIVATE_MODIFY)) {
                console.log("updateStatus Method is called.");

                $scope.errorMap = new Map();

                if ($scope.user.status == 'To_be_activated') {
                    if ($scope.user.roleList == null || $scope.user.roleList == undefined || $scope.user.roleList.length == 0) {
                        //$scope.errorMap.put("user.errormsg",$rootScope.nls["ERR99022"]);
                        Notification.error($rootScope.nls["ERR99022"]);
                        return false;
                    }

                    if ($scope.user.userCompanyList == null || $scope.user.userCompanyList == undefined || $scope.user.userCompanyList.length == 0) {
                        //$scope.errorMap.put("user.errormsg",$rootScope.nls["ERR99022"]);
                        Notification.error($rootScope.nls["ERR99022"]);
                        return false;
                    }

                }


                $scope.contained_progressbar.start();
                var newScope = $scope.$new();

                newScope.statusOperation = $scope.user.status == 'Active' ? 'deactivate' : 'activate';
                ngDialog
                    .openConfirm({
                        template: '<p>Are you sure you want to {{statusOperation}} selected user?</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(function (value) {

                        var actionToCall;
                        if ($scope.user.status == 'Active') {
                            $scope.user.deactivatedUser = $rootScope.userProfile.userName;
                            $scope.user.deactivatedDate = $rootScope.sendApiStartDateTime($rootScope.dateToString(new Date()));
                            $scope.user.status = 'Deactivate';
                            $scope.user.status = $scope.user.status.replace(/ /g, '_');
                            UserDeactivate.update({
                                id: $scope.user.id
                            }).$promise.then(
                                function (data) {
                                    if (data.responseCode == 'ERR0') {
                                        console.log("User status updated Successfully")
                                        UserService.set({});
                                        $scope.search();
                                        //$location.path('/users/user_detail_list');
                                        $state.go("layout.user");

                                    } else {
                                        console.log("User status updated Failed " +
                                            data.responseDescription)
                                    }
                                    $scope.contained_progressbar.complete();
                                    angular.element(".panel-body").animate({
                                        scrollTop: 0
                                    }, "slow");
                                },
                                function (error) {
                                    console.log("User status updated Failed : " + error)
                                });
                        } else {
                            $scope.user.activatedUser = $rootScope.userProfile.userName;
                            $scope.user.activatedDate = $rootScope.sendApiStartDateTime($rootScope.dateToString(new Date()));
                            $scope.user.status = 'Active';
                            $scope.user.status = $scope.user.status.replace(/ /g, '_');

                            var userId = $scope.user.id;
                            UserActivate.update({
                                id: userId
                            },
                                function (data) {
                                    if (data.responseCode == 'ERR0') {
                                        console.log("User status updated Successfully")
                                        UserService.set({});
                                        $scope.search();
                                        //$location.path('/users/user_detail_list');
                                        $state.go("layout.user");

                                    } else {
                                        console.log("User status updated Failed " +
                                            data.responseDescription)
                                    }
                                    $scope.contained_progressbar.complete();
                                    angular.element(".panel-body").animate({
                                        scrollTop: 0
                                    }, "slow");
                                },
                                function (error) {
                                    console.log("User status updated Failed : " + error)
                                });

                        }

                    }

                    );
            }

        }

        $scope.userOperation = function () {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_CREATE)) {
                $scope.user = {};
                console.log("Add Button is Pressed.");
                UserService.set({});
                $state.go("layout.addUser");
            }
        }


        $scope.serialNo = function (index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) + (index + 1);
            return index;
        }


        $scope.cancel = function () {
            $scope.showHistory = false;
            $scope.user = {};
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
            ;
        }

        /*=====TREEVIEW========*/
        $scope.customOpts = {
            useCheckboxes: false,
            twistieExpandedTpl: '<span class="fa fa-caret-down"></span>',
            twistieCollapsedTpl: '<span class="fa fa-caret-right"></span>',
            twistieLeafTpl: '<span class="fa fa-circle"></span>',
            labelAttribute: 'featureName',
            selectedAttribute: 'chk',

        }


        /*$scope.featureList = [{
            label: 'Dashboard',
            value: 'Dashboard',
        }, {
            label: 'Sales',
            value: 'Sales',
            children: [{
                label: 'Call History',
                value: 'Call History'
            }, {
                label: 'Enquiry Log',
                value: 'Enquiry Log',
                children: [{
                    label: 'Create Enquiry Log',
                    value: 'Create Enquiry Log'
                }, {
                    label: 'Modify Enquiry Log',
                    value: 'Modify Enquiry Log'
                },
                    {
                        label: 'Delete Enquiry Log',
                        value: 'Delete Enquiry Log'
                    }],
            }]
        }, {
            label: 'Quotation',
            value: 'Quotation',
        }]*/


        $scope.add = function () {
            $state.go("layout.addUser");
        };
        $scope.edit = function () {
            ServiceFeatureList.set($scope.featureList);
            UserService.set($scope.user);
            $state.go("layout.editUser", {
                userId: $scope.user.id
            });
        };
        $scope.users = {};
        $scope.users.status = 'Active';

        $scope.isCollapsed = false;
        $scope.popover = {
            "content": "1213"
        };

        switch ($stateParams.action) {
            case "SEARCH":
                console.log("user Master Search Page....");
                $scope.init();
                $rootScope.breadcrumbArr = [{
                    label: "Setup",
                    state: "layout.user"
                },
                {
                    label: "User Creation",
                    state: "layout.user"
                }
                ];
                break;
        }

    }]);