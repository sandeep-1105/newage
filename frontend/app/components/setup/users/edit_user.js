/**
 * Created by hmspl on 8/7/16.
 */
app.controller("user_edit_Ctrl", ['$rootScope', '$scope', '$location', 'ngDialog', '$modal', 'UserService', '$http',
    'UserAdd', 'UserEdit', 'UserSearch', 'UserRemove', 'ngProgressFactory', 'RoleList', 'CompanyList', 'CompanyWiseLocation',
    'CompanyWiseEmployee', 'EmployeeList', 'cloneService', '$stateParams', '$state', 'UserView', 'roleConstant', 'Notification',
    function($rootScope, $scope, $location, ngDialog, $modal, UserService, $http,
        UserAdd, UserEdit, UserSearch, UserRemove, ngProgressFactory, RoleList, CompanyList, CompanyWiseLocation,
        CompanyWiseEmployee, EmployeeList, cloneService, $stateParams, $state, UserView, roleConstant, Notification) {

        $scope.directiveDatePickerOptsLeft = angular.copy($rootScope.datePickerOptions);
        $scope.directiveDatePickerOptsLeft.widgetParent = "body";
        $scope.directiveDatePickerOptsLeft.widgetPositioning = {
            horizontal: 'right'
        };

        $scope.tempUserStatus = $rootScope.enum['UserStatus'];
        $scope.userStatus = [];
        for (var i = 0; i < $scope.tempUserStatus.length; i++) {
            $scope.userStatus.push($scope.tempUserStatus[i].replace(/_/g, ' '));
        }

        $scope.init = function() {
            if ($scope.user == undefined || $scope.user == null || $scope.user == 'Master') {
                $scope.user = {};
            }
            if ($scope.user.relatedData == undefined || $scope.user.relatedData == null) {
                $scope.user.relatedData = {};
                $scope.user.relatedData.selectedDefaultLocation = [];
                $scope.user.relatedData.selectedCompanyList = [];
                $scope.user.relatedData.roleData = [];
            }
            $scope.contained_progressbar = ngProgressFactory.createInstance();
            $scope.contained_progressbar.setParent(document.getElementById('user-panel'));
            $scope.contained_progressbar.setAbsolute();
            $rootScope.setNavigate1("Setup");
            $rootScope.setNavigate2("User Creation");
            $rootScope.setNavigate3($scope.user.id != null ? "Edit User" : "Add User");
            if ($scope.user.id == null || $scope.user.id == undefined || $scope.user.id == '') {
                $scope.user.status = 'To_be_activated';
            } else {
                if ($scope.user.roleList != null && $scope.user.roleList.length > 0) {
                    for (var i = 0; i < $scope.user.roleList.length; i++) {
                        $scope.tempObj = {};
                        $scope.tempObj.roleMaster = $scope.user.roleList[i];

                        if (!$rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_ROLES_MODIFY, true)) {
                            $scope.tempObj.roleCheck = true;
                        } else {
                            $scope.tempObj.roleCheck = false;
                        }
                        $scope.user.relatedData.roleData.push($scope.tempObj);
                    }
                }
                if ($scope.user.userCompanyList != null && $scope.user.userCompanyList.length > 0) {
                    for (var i = 0; i < $scope.user.userCompanyList.length; i++) {
                        $scope.user.relatedData.selectedCompanyList.push($scope.user.userCompanyList[i].companyMaster);
                    }
                }

                if ($scope.user.userCompanyList != null && $scope.user.userCompanyList.length > 0) {
                    for (var i = 0; i < $scope.user.userCompanyList.length; i++) {
                        $scope.user.userCompanyList[i].yesNo = $scope.user.userCompanyList[i].yesNo == 'Yes' ? true : false
                        if ($scope.user.userCompanyList[i].yesNo) {
                            $scope.user.relatedData.selectedDefaultCompany = $scope.user.userCompanyList[i].companyMaster;
                        }
                        if (!$rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_MODIFY, true)) {
                            $scope.user.userCompanyList[i].roleCheck = true;
                        } else {
                            $scope.user.userCompanyList[i].roleCheck = false;
                        }

                        if ($scope.user.userCompanyList[i].userCompanyLocationList != null && $scope.user.userCompanyList[i].userCompanyLocationList.length > 0) {
                            for (var j = 0; j < $scope.user.userCompanyList[i].userCompanyLocationList.length; j++) {
                                if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_MODIFY, true)) {
                                    if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_LOCATION_MODIFY, true)) {
                                        $scope.user.userCompanyList[i].userCompanyLocationList[j].roleCheck = false;
                                    } else {
                                        $scope.user.userCompanyList[i].userCompanyLocationList[j].roleCheck = true;
                                    }
                                } else {
                                    $scope.user.userCompanyList[i].userCompanyLocationList[j].roleCheck = true;
                                }
                                $scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo = $scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo == 'Yes' ? true : false
                                if ($scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo) {
                                    $scope.user.relatedData.selectedDefaultLocation.push($scope.user.userCompanyList[i].userCompanyLocationList[j]);
                                }
                            }
                        }
                    }
                }

                $scope.user.status = $scope.user.status.replace(/_/g, ' ');
                console.log($scope.user.relatedData.roleData);
                $scope.user.passwordExpiresOn = $rootScope.dateToString($scope.user.passwordExpiresOn);
            }
            console.log("user id : ", $scope.user.id);
        };



        $scope.tempInit = function() {
            if ($scope.user == undefined || $scope.user == null || $scope.user == 'Master') {
                $scope.user = {};
            }
            if ($scope.user.relatedData == undefined || $scope.user.relatedData == null) {
                $scope.user.relatedData = {};
                $scope.user.relatedData.selectedDefaultLocation = [];
                $scope.user.relatedData.selectedCompanyList = [];
                $scope.user.relatedData.roleData = [];
            }
            $scope.contained_progressbar = ngProgressFactory.createInstance();
            $scope.contained_progressbar.setParent(document.getElementById('user-panel'));
            $scope.contained_progressbar.setAbsolute();
            $rootScope.setNavigate1("Setup");
            $rootScope.setNavigate2("User Creation");
            $rootScope.setNavigate3($scope.user.id != null ? "Edit User" : "Add User");
            console.log("user id : ", $scope.user.id);
        };


        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $scope.cancel = function() {
            var tempObj = $scope.user;
            $scope.isDirty = $scope.userform.$dirty;
            if ($scope.isDirty) {
                if (Object.keys(tempObj).length) {
                    ngDialog
                        .openConfirm({
                            template: '<p>Do you want to save your changes?</p>' +
                                '<div class="ngdialog-footer">' +
                                '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                                '</div>',
                            plain: true,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                if (value == 1 &&
                                    $scope.user.id == null) {
                                    $scope.save();
                                } else if (value == 1 &&
                                    $scope.user.id != null) {
                                    $scope.update();
                                } else if (value == 2) {
                                    var params = {};
                                    params.submitAction = 'Cancelled';
                                    $state.go("layout.user", params);
                                } else {
                                    var params = {};
                                    params.submitAction = 'Cancelled';
                                    $state.go("layout.user", params);
                                }

                            });
                } else {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.user", params);
                }
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go("layout.user", params);
            }
        }

        $scope.setUserCompanyData = function() {
            $scope.user.roleList = [];
            for (var i = 0; i < $scope.user.relatedData.roleData.length; i++) {
                if ($scope.user.relatedData.roleData[i].roleMaster != null && $scope.user.relatedData.roleData[i].roleMaster != undefined && $scope.user.relatedData.roleData[i].roleMaster != '') {
                    $scope.tmpObj = {};
                    $scope.tmpObj.id = $scope.user.relatedData.roleData[i].roleMaster.id;
                    $scope.user.roleList.push($scope.tmpObj);
                }
            }
            if ($scope.user.userCompanyList != null &&
                $scope.user.userCompanyList.length > 0) {
                if ($scope.user.userCompanyList.length == 1) {
                    $scope.user.userCompanyList[0].yesNo = true;
                }
                for (var i = 0; i < $scope.user.userCompanyList.length; i++) {

                    if ($scope.validateUserCompany(
                            $scope.user.userCompanyList[i], i, 0)) {
                        $scope.user.userCompanyList[i].yesNo = $scope.user.userCompanyList[i].yesNo == true ? 'Yes' : 'No'

                        if ($scope.user.userCompanyList[i].userCompanyLocationList != null &&
                            $scope.user.userCompanyList[i].userCompanyLocationList.length > 0) {

                            if ($scope.user.userCompanyList[i].userCompanyLocationList.length == 1) {
                                $scope.user.userCompanyList[i].userCompanyLocationList[0].yesNo = true;
                            }
                            for (var j = 0; j < $scope.user.userCompanyList[i].userCompanyLocationList.length; j++) {

                                if ($scope.user.userCompanyList[i].userCompanyLocationList[j].locationMaster == undefined &&
                                    $scope.user.userCompanyList[i].userCompanyLocationList[j].locationMaster == null) {
                                    $scope.user.userCompanyList[i].userCompanyLocationList.splice(j, 1);
                                } else {
                                    $scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo = $scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo == true ? 'Yes' : 'No'
                                }
                            }
                        }
                    } else {
                        console.log("user company validation failed");
                        return false;
                    }
                }
            } else {
                if ($scope.user.id != null) {
                    Notification.error($rootScope.nls["ERR99022"]);
                    return false;
                }
            }
            return true;
        }


        $scope.update = function() {

            $scope.errorMap = new Map();
            console.log("Update Method is called.");

            if ($scope.validateUser(0) && $scope.setUserCompanyData()) {
                if ($scope.user.roleList == null || $scope.user.roleList == undefined || $scope.user.roleList.length == 0) {
                    Notification.error($rootScope.nls["ERR99022"]);
                    return false;
                }
                $rootScope.mainpreloder = true;
                if ($scope.user != undefined && $scope.user.relatedData != undefined && $scope.user.relatedData.roleData != undefined &&
                    $scope.user.relatedData.roleData.length > 0) {
                    for (var i = 0; i < $scope.user.relatedData.roleData.length; i++) {
                        if ($scope.user.relatedData.roleData[i].roleMaster != undefined && $scope.user.relatedData.roleData[i].roleMaster.status == 'Block') {
                            Notification.error($rootScope.nls["ERR99017"]);
                            return false;
                        } else if ($scope.user.relatedData.roleData[i].roleMaster != undefined && $scope.user.relatedData.roleData[i].roleMaster.status == 'Hide') {
                            Notification.error($rootScope.nls["ERR99018"]);
                            return false;
                        }
                    }
                }

                $scope.tmpUserObj = cloneService.clone($scope.user);
                $scope.tmpUserObj.status = $scope.tmpUserObj.status.replace(/ /g, '_');
                $scope.tmpUserObj.passwordExpiresOn = $rootScope.sendApiDateAndTime($scope.tmpUserObj.passwordExpiresOn);

                $scope.contained_progressbar.start();
                UserEdit.update($scope.tmpUserObj).$promise.then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("User updated Successfully")
                            UserService.set({});
                            $rootScope.mainpreloder = false;
                            var params = {}
                            params.submitAction = 'Saved';
                            $state.go("layout.user", params);



                        } else {
                            console.log("User updated Failed " +
                                data.responseDescription)
                            $rootScope.mainpreloder = false;
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    },
                    function(error) {
                        console.log("User updated Failed : " + error)
                        $rootScope.mainpreloder = false;
                    });
            } else {
                $rootScope.mainpreloder = false;
                console.log("Invalid Form Submission.");
            }

        }


        $scope.save = function() {

            console.log("Save Method is called.", $scope.user.relatedData.roleData);

            if ($scope.validateUser(0) && $scope.setUserCompanyData()) {

                if ($scope.user != undefined && $scope.user.relatedData != undefined && $scope.user.relatedData.roleData != undefined &&
                    $scope.user.relatedData.roleData.length > 0) {
                    for (var i = 0; i < $scope.user.relatedData.roleData.length; i++) {
                        if ($scope.user.relatedData.roleData[i].roleMaster != undefined && $scope.user.relatedData.roleData[i].roleMaster.status == 'Block') {
                            Notification.error($rootScope.nls["ERR99017"]);
                            return false;
                        } else if ($scope.user.relatedData.roleData[i].roleMaster != undefined && $scope.user.relatedData.roleData[i].roleMaster.status == 'Hide') {
                            Notification.error($rootScope.nls["ERR99018"]);
                            return false;
                        }
                    }
                }
                $rootScope.mainpreloder = true;
                $scope.tmpUserObj = cloneService.clone($scope.user);
                $scope.tmpUserObj.status = $scope.tmpUserObj.status.replace(/ /g, '_');
                $scope.tmpUserObj.passwordExpiresOn = $rootScope.sendApiDateAndTime($scope.tmpUserObj.passwordExpiresOn);
                $scope.tmpUserObj.reportMap = null;
                $scope.tmpUserObj.relatedData = null;

                $scope.contained_progressbar.start();
                UserAdd.save($scope.tmpUserObj).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $rootScope.mainpreloder = false;
                        UserService.set({});
                        var params = {}
                        params.submitAction = 'Saved';
                        $state.go("layout.user", params);
                    } else {
                        $rootScope.mainpreloder = false;
                        console.log("User added Failed " + data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                }, function(error) {
                    $rootScope.mainpreloder = false;
                    console.log("User added Failed : " + error)
                });
            } else {
                $rootScope.mainpreloder = false;
                console.log("Invalid Form Submission.");
            }

        }

        $scope.validateUserCompany = function(company, index, validateCode) {

            console.log("validateUserCompany Called");
            $scope.errorMap = new Map();


            if (validateCode == 0 || validateCode == 1) {

                if (company == undefined || company == null || company == "") {
                    //console.log("Company is mandatory");
                    //$scope.errorMap.put("user.company",$rootScope.nls["ERR99020"]);
                    //return false;
                } else {
                    var parentIndex = $scope.user.userCompanyList.indexOf(company);
                    if (company.employee == null || company.employee == undefined || company.employee == '') {
                        $rootScope.navigateToNextField('employeeName' + index);
                        $scope.errorMap.put("user.company.employee", $rootScope.nls["ERR99021"]);
                        Notification.error($rootScope.nls["ERR99021"] + "-" + company.companyMaster.companyName);
                        // $scope.errorMap.put("user.company.msg",company.companyMaster.companyName +"-"+$rootScope.nls["ERR99021"]);
                        return false;
                    } else {
                        if (company.userCompanyLocationList == undefined) {
                            $scope.addLocationRow(undefined, 0, company);
                        }
                    }

                    if ((validateCode == 0) && (company.userCompanyLocationList == null || company.userCompanyLocationList == undefined || company.userCompanyLocationList.length == 0 || company.userCompanyLocationList[0] == undefined || company.userCompanyLocationList[0].locationMaster == undefined ||
                            company.userCompanyLocationList[0].locationMaster == null || company.userCompanyLocationList[0].locationMaster.id == undefined)) {
                        //$rootScope.navigateToNextField('employeeName'+index);
                        //$scope.errorMap.put("user.company.msg",company.companyMaster.companyName +"-"+$rootScope.nls["ERR99013"]);
                        $scope.errorMap.put("user.company.employee", $rootScope.nls["ERR99021"]);
                        Notification.error($rootScope.nls["ERR99013"] + "-" + company.companyMaster.companyName);
                        return false;
                    }
                    if (company.userCompanyLocationList != undefined && company.userCompanyLocationList.length == 1) {
                        $scope.user.relatedData.selectedDefaultLocation[index] = {};
                        $scope.user.relatedData.selectedDefaultLocation[index].locationMaster = company.userCompanyLocationList[0].locationMaster;
                        company.userCompanyLocationList[0].yesNo = true;
                    }
                    if (company.userCompanyLocationList != undefined && company.userCompanyLocationList.length > 0) {
                        for (var index = 0; index < company.userCompanyLocationList.length; index++) {
                            if (company.userCompanyLocationList[index].locationMaster != undefined && company.userCompanyLocationList[index].locationMaster != null) {
                                if (company.userCompanyLocationList[index].locationMaster.status == 'Block') {
                                    // Notification.error($rootScope.nls["ERR99014"]+"-"+company.companyMaster.companyName );
                                    return false;
                                } else if (company.userCompanyLocationList[index].locationMaster.status == 'Hide') {
                                    Notification.error($rootScope.nls["ERR99015"] + "-" + company.companyMaster.companyName);
                                    return false;
                                }

                            }
                        }
                    }

                }
                if (company != undefined && company != null && company != "") {

                }
            }
            return true;
        }
        $scope.validateUser = function(validateCode) {

            console.log("Validate Called");
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.user.userName == undefined ||
                    $scope.user.userName == null ||
                    $scope.user.userName == "") {
                    $rootScope.navigateToNextField('userName');
                    $scope.errorMap.put("user.userName", $rootScope.nls["ERR99005"]);
                    return false;
                } else {

                    var regexp = new RegExp("^[0-9a-zA-Z]{0,30}$");
                    if (!regexp
                        .test($scope.user.userName)) {
                        $rootScope.navigateToNextField('userName');
                        $scope.errorMap.put("user.userName", $rootScope.nls["ERR99006"]);
                        return false;
                    }

                }
            }
            if (validateCode == 0 || validateCode == 2) {
                if ($scope.user.password == undefined ||
                    $scope.user.password == null ||
                    $scope.user.password == "") {
                    $rootScope.navigateToNextField('password-field');
                    $scope.errorMap.put("user.password", $rootScope.nls["ERR99003"]);
                    return false;
                } else {
                    var regexp = new RegExp("^[a-zA-Z0-9](?=.*[0-9])(?=.*[@!#$%&()+={};<>,.?/\|])[a-zA-Z0-9!#@$%&()+={};<>,.?/\|]{5,20}$");
                    if (!regexp
                        .test($scope.user.password)) {
                        $rootScope.navigateToNextField('password-field');
                        $scope.errorMap.put("user.password", $rootScope.nls["ERR99004"]);
                        return false;
                    }


                }
            }

            if (validateCode == 0 || validateCode == 3) {

                if ($scope.user.passwordExpiresOn != null) {
                    var passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    var now = moment().startOf('day');
                    if (passwordExpiresOn >= now) {
                        console.log("Valid passwordExpiresOn");
                        $rootScope.navigateToNextField('description');
                    } else {
                        console.log($rootScope.nls["ERR99010"]);
                        $rootScope.navigateToNextField('passwordExpiresOn');
                        $scope.errorMap.put("user.expiresOn", $rootScope.nls["ERR99010"]);
                        return false;
                    }
                }
            }
            return true;
        }

        $scope.navigateToNextField = function(id) {
            var focElement = document.getElementById(id);
            if (focElement != undefined && focElement != null) {
                console.log("focus elemnet" + focElement);
                focElement.focus();
            }
        }

        $scope.setStatus = function(data) {

            if (data == 'To_be_activated' || data == 'To be activated') {
                return 'tobeactivetype';
            } else if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Deactivate') {
                return 'blockedtype';
            }
        }

        //Role mapping started

        $scope.removeRoleObj = function(index) {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_ROLES_DELETE)) {
                $scope.user.relatedData.roleData.splice(index, 1);
            }
        }


        //add role mapping row
        $scope.addRoleRow = function(dataObj) {
            $scope.firstFocus = true;
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_ROLES_CREATE)) {
                if ($scope.user.relatedData == undefined || $scope.user.relatedData == null) {
                    $scope.user.relatedData = {};
                }

                if ($scope.user.relatedData.roleData == undefined || $scope.user.relatedData.roleData == null) {
                    $scope.user.relatedData.roleData = [];
                }
                var fineArr = [];
                $scope.secondArr = [];
                angular.forEach($scope.user.relatedData.roleData, function(dataObj, index) {
                    $scope.secondArr[index] = {};
                    var validResult = validateUserRoleObj(dataObj, index, "row");
                    if (validResult) {
                        fineArr.push(1);
                        $scope.secondArr[index].errRow = false;
                    } else {
                        $scope.secondArr[index].errRow = true;
                        fineArr.push(0);
                    }
                    //console.log();
                });
                if (fineArr.indexOf(0) < 0) {
                    $scope.user.relatedData.roleData.push({});
                    $scope.secondArr.push({
                        "errRow": false
                    });
                    $scope.indexFocus = null;
                    if ($scope.tableState) {
                        $scope.tableState({
                            param: true
                        });
                    }
                } else {
                    if ($scope.tableState) {
                        $scope.tableState({
                            param: false
                        });
                    }
                }
            }
        };

        var validateUserRoleObj = function(dataObj, index, type) {
            var errorArr = [];
            if (type == "row") {
                $scope.secondArr[index].errTextArr = [];
            }
            if (dataObj.roleMaster == null || dataObj.roleMaster.id == undefined || dataObj.roleMaster.id == null || dataObj.roleMaster.id == "") {
                console.log("Role is Mandatory.")
                if (type == "row") {
                    $scope.secondArr[index].roleName = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99016"]);
                }
                //return false;
                errorArr.push(0);
            } else {

                if (dataObj.roleMaster.status == 'Block') {
                    console.log("Selected Role is Blocked.");
                    if (type == "row") {
                        $scope.secondArr[index].roleName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99017"]);
                    }
                    //return false;
                    errorArr.push(0);

                } else if (dataObj.roleMaster.status == 'Hide') {
                    console.log("Selected Role is Hidden.");
                    if (type == "row") {
                        $scope.secondArr[index].roleName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99018"]);
                    }
                    //return false;
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.secondArr[index].roleName = false;

                    }
                    //return false;
                    errorArr.push(1);
                }


            }
            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }

        };

        //Role list 
        $scope.showRoles = function(role) {
            $scope.selectedItem1 = -1;
            $scope.panelTitle = "Role";

        };


        $scope.ajaxRoleEvent = function(object) {
            console.log("role event called..");
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return RoleList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    console.log("data.." + data.responseObject);
                    if (data.responseCode == "ERR0") {
                        $scope.roleList = data.responseObject.searchResult;
                        return $scope.roleList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching  Roles');
                }
            );

        }

        $scope.selectedRole = function(roleObj) {
            //$scope.validateRole(0);
        };

        $scope.cancelRole = function() {
            $scope.showRoleList = false;
        }

        $scope.listConfigRole = {
                search: true,
                showCode: true,
                ajax: true,
                columns: [{
                        "title": "roleName",
                        seperator: false
                    }

                ]
            }
            //company mapping started
        $scope.companyUniqueCheck = function(obj, list) {
            var found, y;
            found = false;
            for (y = 0; y < list.length; y++) {
                if (obj.id == list[y].id) {
                    found = true;

                    console.log("Duplicate object…");
                    break;
                }
            }
            if (!found) {
                console.log("Object Added….");
                list.push(obj);
            }
            return found;
        }
        $scope.checkAllCompany = function(index, checkedAll) {
            console.log("index : ", index, checkedAll);
            if (checkedAll) {
                for (var i = 0; i < $scope.companyList.length; i++) {
                    if (!$scope.companyUniqueCheck($scope.companyList[i], $scope.user.relatedData.selectedCompanyList)) {
                        $scope.companyList[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < $scope.companyList.length; i++) {
                    $scope.companyList[i].checked = false;
                    $scope.user.relatedData.selectedCompanyList.splice($scope.companyList[i], 1);
                }
            }
        }

        $scope.checkCompany = function(index, checked, company) {
            console.log("index : ", index, checked);
            if (checked) {

                if ($scope.companyUniqueCheck(company, $scope.user.relatedData.selectedCompanyList)) {
                    $scope.errorMap = new Map();
                    //$scope.errorMap.put("duplicate.company",$rootScope.nls["ERR99011"]);
                    Notification.error($rootScope.nls["ERR99011"]);
                }
            } else {
                var flag = false;
                for (var i = 0; i < $scope.user.relatedData.selectedCompanyList.length; i++) {
                    if ($scope.user.relatedData.selectedCompanyList[i].id == company.id) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    $scope.user.relatedData.selectedCompanyList.splice($scope.user.relatedData.selectedCompanyList.indexOf(company), 1);
                }

            }
            console.log("selected company list : ", $scope.user.relatedData.selectedCompanyList);

        }

        $scope.removeCompanyMapping = function(index) {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_DELETE)) {
                $scope.user.relatedData.selectedCompanyList.splice(index, 1);
                $scope.copyCompanies(false);
            }
        }


        $scope.selectDefaultCompany = function() {


            if ($scope.user.userCompanyList != null && $scope.user.userCompanyList.length > 0) {

                for (var i = 0; i < $scope.user.userCompanyList.length; i++) {

                    if ($scope.user.userCompanyList[i].companyMaster != undefined &&
                        $scope.user.relatedData.selectedDefaultCompany.companyMaster != undefined &&
                        $scope.user.userCompanyList[i].companyMaster.id == $scope.user.relatedData.selectedDefaultCompany.companyMaster.id) {
                        $scope.user.userCompanyList[i].yesNo = true;
                    } else {
                        $scope.user.userCompanyList[i].yesNo = false;
                    }


                }

            }
        }

        $scope.selectDefaultLocation = function(index, userCompany, selectedDefaultLocation, isUncheck) {

            if (!isUncheck) {
                $scope.user.relatedData.selectedDefaultLocation[$scope.user.userCompanyList.indexOf(userCompany)] = null;
                return;
            }

            if ($scope.user.relatedData.selectedDefaultLocation == undefined || $scope.user.relatedData.selectedDefaultLocation == null || $scope.user.relatedData.selectedDefaultLocation == '') {
                $scope.user.relatedData.selectedDefaultLocation = [];
            }
            $scope.user.relatedData.selectedDefaultLocation[$scope.user.userCompanyList.indexOf(userCompany)] = selectedDefaultLocation;

            if (index != null) {
                $scope.user.relatedData.selectedDefaultLocation[$scope.user.userCompanyList.indexOf(userCompany)] = $scope.user.userCompanyList[$scope.user.userCompanyList.indexOf(userCompany)].userCompanyLocationList[index];
            }

            if ($scope.user.userCompanyList != null && $scope.user.userCompanyList.length > 0) {

                for (var i = 0; i < $scope.user.userCompanyList.length; i++) {

                    if ($scope.user.userCompanyList[i].userCompanyLocationList != null && $scope.user.userCompanyList[i].userCompanyLocationList.length > 0) {
                        for (var j = 0; j < $scope.user.userCompanyList[i].userCompanyLocationList.length; j++) {


                            if (i == $scope.user.userCompanyList.indexOf(userCompany)) {
                                if (selectedDefaultLocation != null && selectedDefaultLocation.locationMaster != null &&
                                    $scope.user.userCompanyList[i].userCompanyLocationList[j].locationMaster.locationName == selectedDefaultLocation.locationMaster.locationName)
                                    $scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo = true;
                                else {
                                    $scope.user.userCompanyList[i].userCompanyLocationList[j].yesNo = false;
                                }
                            }
                        }
                    }
                }
            }
            console.log($scope.user.relatedData.selectedDefaultLocation);
        }

        $scope.copyCompanies = function(flag) {
            if (flag) {
                if ($scope.user.relatedData.selectedCompanyList.length == 0) {
                    Notification.error($rootScope.nls["ERR99023"]);
                    return false;
                }
            }
            $scope.userCompanyList = [];
            if ($scope.user.relatedData.selectedCompanyList != null && $scope.user.relatedData.selectedCompanyList.length > 0) {

                $scope.user.relatedData.selectedDefaultCompany = $scope.user.relatedData.selectedCompanyList[0];
                for (var i = 0; i < $scope.user.relatedData.selectedCompanyList.length; i++) {
                    //if($scope.user.relatedData.selectedCompanyList.length==1){
                    //}
                    if ($scope.user.id != null && $scope.user.userCompanyList[i] != null && $scope.user.relatedData.selectedCompanyList[i].companyName == $scope.user.userCompanyList[i].companyMaster.companyName) {
                        $scope.userCompany = $scope.user.userCompanyList[i];
                    } else {
                        $scope.userCompany = {};
                    }

                    $scope.userCompany.companyMaster = $scope.user.relatedData.selectedCompanyList[i];
                    $scope.userCompanyList.push($scope.userCompany);
                }

            }
            $scope.user.userCompanyList = $scope.userCompanyList;
            return true;
        }

        $scope.showModal = function() {
            $scope.errorMap = new Map();
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_CREATE)) {
                $scope.getAllCompanies(null);
            }
        };

        $scope.getAllCompanies = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            /*$scope.searchDto.selectedPageNumber = 1;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;*/
            CompanyList.fetch($scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.companyList = data.responseObject.searchResult;
                        filterCompanyIfExists($scope.companyList);
                        var myOtherModal = $modal({
                            scope: $scope,
                            templateUrl: 'app/components/setup/users/view/company_popup.html',
                            show: false
                        });
                        myOtherModal.$promise.then(myOtherModal.show);
                        console.log("Company List ", $scope.companyList)
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching  Companies');
                }
            );
        }

        function filterCompanyIfExists(companyList) {

            if (companyList == undefined || companyList == null || companyList == "" || companyList.length == 0) {




            } else {
                if ($scope.user != undefined && $scope.user.relatedData != undefined && $scope.user.relatedData.selectedCompanyList != undefined && $scope.user.relatedData.selectedCompanyList != null && $scope.user.relatedData.selectedCompanyList.length > 0) {

                    for (var i = 0; i < $scope.user.relatedData.selectedCompanyList.length; i++) {

                        for (var j = 0; j < companyList.length; j++) {

                            if ($scope.user.relatedData.selectedCompanyList[i].id == companyList[j].id) {
                                $scope.companyList.splice(j, 1);
                            }
                        }

                    }
                }
            }
        }


        //location mapping started
        $scope.showLocationList = false;
        $scope.selectedItemLocation = -1;
        $scope.locationList = [];
        $scope.searchText = "";
        $scope.listConfigLocation = {
            search: true,
            ajax: true,
            showCode: true,
            columns: [{
                    "title": "locationName",
                    seperator: false
                },
                {
                    "title": "locationCode",
                    seperator: true
                }
            ]

        }
        $scope.showLocation = function() {
            $scope.showLocationList = false;
            $scope.panelTitle = "Location";
            $scope.selectedItemLocation = -1;


        };
        $scope.ajaxLocationEvent = function(object, companyId, obj) {

            if (companyId != null) {
                $scope.searchDto = {};
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                $scope.searchDto.keyword = object == null ? "" : object;
                return CompanyWiseLocation.fetch({
                    "companyId": companyId
                }, $scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.locationList = data.responseObject.searchResult;
                            if ($scope.locationList != undefined && obj != undefined && obj.length != 0) {
                                for (var i = 0; i < obj.length; i++) {
                                    if (obj[i].locationMaster != undefined && obj[i].locationMaster.id != undefined) {
                                        $scope.locationList = $scope.filterLocation($scope.locationList, obj[i].locationMaster.id);
                                    }
                                }
                            }
                            return $scope.locationList;
                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching Location');
                    }
                );

            }
        }


        $scope.filterLocation = function(array, id) {
            for (var i in array) {
                if (array[i].id == id) {
                    array.splice(i, 1);
                    break;
                }
            }
            return array;
        }

        /*$scope.ajaxLocationEvent(null);*/
        $scope.selectedLocation = function(obj) {
            $scope.cancelLocation();
            //$scope.validateAddressMapping(1);


        };
        $scope.cancelLocation = function() {
            $scope.searchText = "";
            $scope.selectedItemLocation = -1;
            $scope.showLocationList = false;
        };


        //employee list 

        $scope.showEmployeeList = false;
        $scope.selectedItemEmployee = -1;
        $scope.employeeList = [];
        $scope.searchText = "";
        $scope.listConfigEmployee = {
            search: true,
            ajax: true,
            showCode: true,
            columns: [{
                    "title": "employeeName",
                    seperator: false
                },
                {
                    "title": "employeeCode",
                    seperator: true
                }
            ]

        }
        $scope.showEmployee = function() {
            $scope.showEmployeeList = false;
            $scope.panelTitle = "Employee";
            $scope.selectedItemEmployee = -1;


        };


        $scope.ajaxEmployeeEvent = function(object, companyId) {

            if (companyId != null) {
                $scope.searchDto = {};
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                $scope.searchDto.keyword = object == null ? "" : object;
                return CompanyWiseEmployee.fetch({
                    "companyId": companyId
                }, $scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.employeeList = data.responseObject.searchResult;
                            return $scope.employeeList;

                        }
                    },
                    function(errResponse) {
                        console.error('Error while fetching employees');
                    }
                );

            }
        }

        /*$scope.ajaxEmployeeEvent(null);*/
        $scope.selectedEmployee = function(obj) {
            $scope.cancelEmployee();
            //$scope.validateAddressMapping(1);


        };
        $scope.cancelEmployee = function() {
            $scope.searchText = "";
            $scope.selectedItemEmployee = -1;
            $scope.showEmployeeList = false;
        };


        $scope.userCompanyLocationList = [];

        //add new row
        $scope.addLocationRow = function(dataObj, index, userCompany) {
            var parentIndex = $scope.user.userCompanyList.indexOf(userCompany);
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_LOCATION_CREATE)) {
                var fineArr = [];
                $scope.companyArr = [];
                $scope.companyArr[parentIndex] = {};
                $scope.companyArr[parentIndex].secondArr = [];
                $scope.firstFocus = true;
                if ($scope.user.userCompanyList[parentIndex].userCompanyLocationList == null || $scope.user.userCompanyList[parentIndex].userCompanyLocationList == undefined || $scope.user.userCompanyList[parentIndex].userCompanyLocationList == '') {
                    $scope.user.userCompanyList[parentIndex].userCompanyLocationList = [];
                }
                var index = $scope.user.userCompanyList[parentIndex].userCompanyLocationList.length;

                angular.forEach($scope.user.userCompanyList[parentIndex].userCompanyLocationList, function(dataObj, index) {

                    $scope.companyArr[parentIndex].secondArr[index] = {};

                    var validResult = $scope.validateUserCompanyLocationObj(parentIndex, dataObj, index, "row");
                    if (validResult) {
                        fineArr.push(1);
                        $scope.companyArr[parentIndex].secondArr[index].errRow = false;
                    } else {
                        $scope.companyArr[parentIndex].secondArr[index].errRow = true;
                        fineArr.push(0);
                    }
                    //console.log();
                });

                if (fineArr.indexOf(0) < 0) {
                    $scope.user.userCompanyList[parentIndex].userCompanyLocationList.push({});
                    $scope.companyArr[parentIndex].secondArr.push({
                        "errRow": false
                    });
                    $scope.indexFocus = null;
                }
            }
        };


        $scope.removeObj = function(index, userCompany) {
            if ($rootScope.roleAccess(roleConstant.SETUP_USER_CREATION_COMPANY_LOCATION_DELETE)) {
                $scope.user.userCompanyList[$scope.user.userCompanyList.indexOf(userCompany)].userCompanyLocationList.splice(index, 1);
            }
            if ($scope.user.userCompanyList[$scope.user.userCompanyList.indexOf(userCompany)].userCompanyLocationList.length == 0) {
                $scope.user.relatedData.selectedDefaultLocation[index].locationMaster = {};
            }
        }

        //show errors
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'errorPopUp.html',
            show: false,
            keyboard: true
        });
        var errorOnRowIndex = null;

        $scope.errorShow = function(errorObj, index) {
            $scope.errList = errorObj.errTextArr[index];
            // Show when some event occurs (use $promise property to ensure the template has been loaded)
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };

        $scope.errorLocationShow = function(obj, userCompany, cIdx) {
            var parentIndex = $scope.user.userCompanyList.indexOf(userCompany);
            $scope.errList = obj[parentIndex].secondArr[cIdx].errTextArr;
            errorOnRowIndex = cIdx;
            myOtherModal.$promise.then(myOtherModal.show);
        }


        //error validation rules
        $scope.validateUserCompanyLocationObj = function(parentIndex, dataObj, index, type) {

            var errorArr = [];
            $scope.companyArr = [];
            $scope.companyArr[parentIndex] = {};
            $scope.companyArr[parentIndex].secondArr = [];
            $scope.companyArr[parentIndex].secondArr[index] = {};
            $scope.companyArr[parentIndex].secondArr[index].errTextArr = [];

            if (dataObj.locationMaster == null || dataObj.locationMaster.id == undefined || dataObj.locationMaster.id == null || dataObj.locationMaster.id == "") {
                console.log("Location is Mandatory.")
                if (type == "row") {
                    $scope.companyArr[parentIndex].secondArr[index].locationName = true;
                    $scope.companyArr[parentIndex].secondArr[index].errTextArr.push($rootScope.nls["ERR99013"]);
                }
                //return false;
                errorArr.push(0);
            } else {

                if (dataObj.locationMaster.status == 'Block') {
                    console.log("Selected Location is Blocked.");
                    if (type == "row") {
                        $scope.companyArr[parentIndex].secondArr[index].locationName = true;
                        $scope.companyArr[parentIndex].secondArr[index].errTextArr.push($rootScope.nls["ERR99014"]);
                    }
                    //return false;
                    errorArr.push(0);

                } else if (dataObj.locationMaster.status == 'Hide') {
                    console.log("Selected Location is Hidden.");
                    if (type == "row") {
                        $scope.companyArr[parentIndex].secondArr[index].locationName = true;
                        $scope.companyArr[parentIndex].secondArr[index].errTextArr.push($rootScope.nls["ERR99015"]);
                    }
                    //return false;
                    errorArr.push(0);

                } else {
                    if (type == "row") {
                        $scope.companyArr[parentIndex].secondArr[index].locationName = false;

                    }
                    //return false;
                    errorArr.push(1);
                }
            }

            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }

        };

        $scope.roleRender = function(item) {
            return {
                label: item.roleName,
                item: item
            }
        }
        $scope.employeeRender = function(item) {
            return {
                label: item.employeeName,
                item: item
            }
        }
        $scope.locationRender = function(item) {
            return {
                label: item.locationName,
                item: item
            }
        }

        // History Start

        $scope.getUserById = function(userId) {
            console.log("getUserById ", userId);

            UserView.get({
                id: userId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Successful while getting user.", data)
                    $scope.user = data.responseObject;
                    $scope.init();
                    $scope.getRoleList();
                }

            }, function(error) {
                console.log("Error while getting user.", error)
            });

        }

        $scope.showHint = function() {
            Notification.info($rootScope.nls["ERR99024"]);
        }


        //On leave the Unfilled user Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

        $scope.$on('addUserEvent', function(events, args) {

            $rootScope.unfinishedData = $scope.user;
            $rootScope.user = "Master";
            $rootScope.unfinishedFormTitle = "User (New)";
            if ($scope.user != undefined && $scope.user != null && $scope.user.userName != undefined &&
                $scope.user.userName != null && $scope.user.userName != "") {
                $rootScope.subTitle = $scope.user.userName;
            } else {
                $rootScope.subTitle = "Unknown User"
            }
        })

        $scope.$on('editUserEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.user;
            $rootScope.user = "Master";
            $rootScope.unfinishedFormTitle = "User Edit";
            if ($scope.user != undefined && $scope.user != null && $scope.user.userName != undefined &&
                $scope.user.userName != null && $scope.user.userName != "") {
                $rootScope.subTitle = $scope.user.userName;
            } else {
                $rootScope.subTitle = "Unknown User"
            }
        })


        $scope.$on('addUserEventReload', function(e, confirmation) {
            console.log("addUserEventReload is called ", $scope.user);
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.user);
            localStorage.isUserReloaded = "YES";
            e.preventDefault();
        });


        $scope.$on('editUserEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.user);
            localStorage.isUserReloaded = "YES";
            e.preventDefault();
        });

        $scope.getRoleList = function() {
            $scope.user.roleList = [];
            for (var i = 0; i < $scope.user.relatedData.roleData.length; i++) {
                if ($scope.user.relatedData.roleData[i].roleMaster != null && $scope.user.relatedData.roleData[i].roleMaster != undefined && $scope.user.relatedData.roleData[i].roleMaster != '') {
                    $scope.tmpObj = {};
                    $scope.tmpObj.id = $scope.user.relatedData.roleData[i].roleMaster.id;
                    $scope.user.roleList.push($scope.tmpObj);
                }
            }
            // $scope.user.passwordExpiresOn=$rootScope.convertToDate($scope.user.passwordExpiresOn);
        }


        $scope.isReloaded = localStorage.isUserReloaded;

        if ($stateParams.action == "ADD") {

            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    localStorage.isUserReloaded = "NO";
                    $scope.user = JSON.parse(localStorage.reloadFormData);
                    $scope.user.passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    $scope.tempInit();
                } else {
                    console.log("Add History NotReload...")
                    $scope.user = $rootScope.selectedUnfilledFormData;
                    $scope.user.passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    $scope.tempInit();
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isUserReloaded = "NO";
                    $scope.user = JSON.parse(localStorage.reloadFormData);
                    $scope.user.passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    console.log("Add NotHistory Reload...");
                    $scope.tempInit();
                } else {
                    console.log("Add NotHistory NotReload...")
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Setup",
                    state: "layout.user"
                },
                {
                    label: "User Creation",
                    state: "layout.user"
                },
                {
                    label: "Add User",
                    state: null
                }
            ];

        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isUserReloaded = "NO";
                    $scope.user = JSON.parse(localStorage.reloadFormData);
                    $scope.user.passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    $scope.tempInit();
                } else {
                    $scope.user = $rootScope.selectedUnfilledFormData;
                    $scope.user.passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    $scope.tempInit();
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isUserReloaded = "NO";
                    $scope.user = JSON.parse(localStorage.reloadFormData);
                    $scope.user.passwordExpiresOn = $rootScope.convertToDate($scope.user.passwordExpiresOn);
                    $scope.tempInit();
                } else {
                    $scope.getUserById($stateParams.userId);

                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Setup",
                    state: "layout.user"
                },
                {
                    label: "User Creation",
                    state: "layout.user"
                },
                {
                    label: "Edit User",
                    state: null
                }
            ];
        }


        // History End

    }
]);