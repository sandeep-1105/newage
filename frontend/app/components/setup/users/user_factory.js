(function() {

    app.factory("UserSearch", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/get/search/all", {}, {
            get: {
                method: 'GET',
                isArray: false
            }
        });
    }]);



    app.factory("UserProfileList", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/get/search/list", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

    app.factory("UserAdd", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("UserEdit", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/update", {}, {
            update: {
                method: 'POST',
            }
        });
    }]);

    app.factory("UserActivate", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/:id/activate", {}, {
            update: {
                method: 'POST',
                params: {
                    id: '@id'
                },
                isArray: false
            }
        });
    }]);

    app.factory("UserDeactivate", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/:id/deactivate", {}, {
            update: {
                method: 'POST',
                params: {
                    id: '@id'
                },
                isArray: false
            }
        });
    }]);

    app.factory("UserView", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("UserRemove", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("UserList", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("SearchUserNotInList", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/get/search/exclude", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("SearchUserByKey", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);


    app.factory("ChangeProfileServer", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/changeprofile/:locationId", {}, {
            get: {
                method: 'GET',
                params: {
                    locationId: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("GetAdressForCompany", ['$resource', function($resource) {
        return $resource("/api/v1/userprofile/companyaddress/:companyId", {}, {
            get: {
                method: 'GET',
                params: {
                    companyId: ''
                },
                isArray: false
            }
        });
    }]);


})();