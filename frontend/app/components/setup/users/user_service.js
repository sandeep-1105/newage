app.factory('UserService',[ function() {
	var savedData = {}
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
		
	}
	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}

	return {
		set : set,
		get : get
	}

}]);