
app.factory('reportMasterFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/reportmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
	   create:$resource('/api/v1/reportmaster/create', {}, {
			save : {method : 'POST'}
		}),
		update : $resource('/api/v1/reportmaster/update', {}, {
			query : {method : 'POST'}
		}),
		remove : $resource('/api/v1/reportmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/reportmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		searchAll : $resource('/api/v1/reportmaster/get/all', {}, {
			 get : {method : 'GET', params : {}, isArray : false}
		})
	};
}])
