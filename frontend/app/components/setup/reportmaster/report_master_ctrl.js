app.controller("report_master_ctrl", function($scope, $rootScope,$modal,ngDialog,$stateParams, $state, Notification,cloneService,reportMasterFactory,DocumentMasterList,roleConstant) {
    
	
	
	$scope.YesNo = ["Yes",'No'];
	
	
	$scope.init = function() {
		$scope.reportMasterSearchDto = {};
		$scope.limitArr = [10,15,20];
		$scope.page = 0;
		$scope.limit = 10;
		$scope.totalRecord = 0;
		$scope.search();
		$scope.detailTab="reportmaster";
		
	};
	
	$scope.callValidation = function(event) {
		if(!event.shiftKey && event.keyCode==9){
			$scope.addRow();			
		}
    }
	
    $scope.initAdd=function(){
		
		if($scope.reportMasterList==undefined || $scope.reportMasterList==null){
			$scope.reportMasterList=[];
			$scope.reportMasterList.push({'isReportEnable':false});
		}
		$scope.firstFocus = true;
		$scope.errorArray = [];
		
	}
    
    var myOtherModal = $modal({scope:$scope,templateUrl: 'app/components/setup/reportmaster/view/error.html', show: false,keyboard:true});
    var errorOnRowIndex = null;
    
    $scope.errorShow = function(errorObj,index){
        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };
		
	$scope.clickOnTab=function(tab){
		if(tab=='reportmaster' &&  $rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_REPORT_MASTER_LIST)){
			$scope.detailTab='reportmaster';	
			$scope.init();
     }else if(tab=='cntrycnfgmaster' && $rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_LIST)){
			$scope.detailTab='cntrycnfgmaster';
		}
	}
	
	
	
	$scope.search = function() {
		console.log("Search is called....");
		$scope.reportMasterSearchDto.selectedPageNumber = $scope.page;
		$scope.reportMasterSearchDto.recordPerPage = $scope.limit;
		reportMasterFactory.search.query($scope.reportMasterSearchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			var tempArr = [];
			var resultArr = [];
			tempArr = data.responseObject.searchResult;
			console.log("tempArr", tempArr);
			var tempObj = {};
			angular.forEach(tempArr,function(item,index) {
				tempObj = item;
				tempObj.no = (index+1)+($scope.page*$scope.limit);
				tempObj.isReportEnable=tempObj.isReportEnable=='Yes' || tempObj.isReportEnable == true?'Yes':'No';
				resultArr.push(tempObj);
				tempObj = {};
			});
			$scope.reportArray = resultArr;
		});
	}

	

	$scope.reportConfigHead=[
		{
			"name":"#",
			"width":"col-xs-0half",
			"model":"no",
			"search":false
		},
		{
			"name": "Report Code",
			"search": true,
			"model": "reportMasterCode",
			"wrap_cell": true,
			"type": "input",
			"sort": true,
			"width": "w150px",
			"prefWidth": "150",
			"key": "searchreportMasterCode"
		},
		{
			"name": "Report Name",
			"model": "reportMasterName",
			"search": true,
			"wrap_cell": true,
			"type": "input",
			"sort": true,
			"width": "w200px",
			"prefWidth": "200",
			"key": "searchreportMasterName"
		},{
			"name": "Report Key",
			"model": "reportKey",
			"search": true,
			"wrap_cell": true,
			"type": "select",
			"sort": true,
			"data": $rootScope.enum['ReportName'],
			"width": "w200px",
			"prefWidth": "200",
			"key": "searchreportMasterKey"
		},{
			"name": "Document",
			"search": true,
			"model": "documentMaster",
			"wrap_cell": true,
			"type": "lov",
			"sort": true,
			"width": "w150px",
			"prefWidth": "150",
			"key": "searchDocument",
			"lov":{
				selName:'documentName',
				viewKey:'documentMaster.documentName',
				ajaxFn:'ajaxDocumentMaster(param)'
			}
		},{
			"name": "Is Attachment Need",
			"model": "isReportEnable",
			"search": true,
			"wrap_cell": true,
			"type": "select",
			"sort": true,
			"data":$scope.YesNo,
			"width": "w100px",
			"prefWidth": "100",
			"key": "isReportEnable"
		},
		{
			"name": "Action",
			"model": "",
			"search": false,
			"wrap_cell": true,
			"type": "action",
			"actype": "action",
			"sort": false,
			"width": "w100px",
			"prefWidth": "100",
			"key": ""
		}
	];

	$scope.sortSelection = {
			sortKey:"reportMasterName",
			sortOrder:"asc"
	}
	
	$scope.limitChange = function(item){
		console.log("Limit Change is called...", item);
		$scope.page = 0;
		$scope.limit = item;
		$scope.search();
	}
	
	$scope.changeSearch = function(param) {
		
		console.log("Change Search", param);
		
		for (var attrname in param) {
			$scope.reportMasterSearchDto[attrname] = param[attrname];
		}
		
		$scope.page = 0;
		console.log("change search",$scope.reportMasterSearchDto);

		$scope.search();
	}
	
	$scope.sortChange = function(param){
		console.log("Sort Change Called.", param);
		
		$scope.reportMasterSearchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.reportMasterSearchDto.sortByColumn = param.sortKey;

		$scope.search();
	}
	
	$scope.changepage = function(param){
		console.log("Change Page Called.", param);
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.search();
	}
	
	  $scope.isEmptyRow=function(obj){
          //return (Object.getOwnPropertyNames(obj).length === 0);
          var isempty = true; //  empty
          if (!obj) {
              return isempty;
          }
          var k = Object.getOwnPropertyNames(obj);
          for (var i = 0; i < k.length; i++){
              if (obj[k[i]]) {
                  isempty = false; // not empty
                  break;
              }
          }
          return isempty;
  }
	
	$scope.addRow=function(){
		$scope.firstFocus = true;
    	$scope.errorArray = [];
    	var listErrorFlag = false;
    	if($scope.reportMasterList != undefined && $scope.reportMasterList!= null && $scope.reportMasterList.length >0) {
    		for(var index = 0; index < $scope.reportMasterList.length; index++) {
    			if($scope.validation(index)) {
    				listErrorFlag = true;
    			}
    		}
    		if(listErrorFlag) {
    			return;
    		}  else {
    			$scope.reportMasterList.push({'isReportEnable':false});
        	}
    	} else {
    		$scope.reportMasterList.push({'isReportEnable':false});
    	}
	}
	
	$scope.deleteRow=function(index){
		$scope.reportMasterList.splice(index,1);	
	}
	
	$scope.cancel=function(){
		$state.go('layout.reportMaster');
	}

	 $scope.remove = function(param) {
	    	console.log("Remove ", param);
	    	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR06352"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	reportMasterFactory.remove.query({ id : param.item.id }, function(data) {
	            			if (data.responseCode == 'ERR0') {
	        	        		Notification.success($rootScope.nls["ERR402"]);
	            				$scope.init();
	            			} else {
	            			}
	            		}, function(error) {
	            		});
	                }, function(value) {
	                });
	    }
	 
	 
	 $scope.validation = function(index){
		 $scope.errorArray[index] = {};
			$scope.errorArray[index].errTextArr = [];
			var errorFound = false;
			if($scope.reportMasterList[index].reportMasterCode == undefined || $scope.reportMasterList[index].reportMasterCode == null ||
					$scope.reportMasterList[index].reportMasterCode.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].reportMasterCode = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06355"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].reportMasterCode = false;
			}
			
			if($scope.reportMasterList[index].reportMasterName == undefined || $scope.reportMasterList[index].reportMasterName == null ||
					$scope.reportMasterList[index].reportMasterName.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].reportMasterName = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06357"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].reportMasterName = false;
			}
			if($scope.reportMasterList[index].reportKey == undefined || $scope.reportMasterList[index].reportKey == null ||
					$scope.reportMasterList[index].reportKey.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].reportKey = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06367"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].reportKey = false;
			}
			
			
			
			
			if($scope.reportMasterList[index].documentMaster != undefined && $scope.reportMasterList[index].documentMaster != null && 
					$scope.reportMasterList[index].documentMaster.id != null) {
				
				if($scope.reportMasterList[index].documentMaster.status == "Block") {
					$scope.errorArray[index].errRow = true;
					$scope.errorArray[index].documentMaster = true;
		            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06399"]);	
		            errorFound = true;
				} else {
					$scope.errorArray[index].documentMaster = false;
				}
				
			}
			
			
			if($scope.reportMasterList[index].isReportEnable == undefined || $scope.reportMasterList[index].isReportEnable == null ||
					$scope.reportMasterList[index].isReportEnable.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].isReportEnable = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR06359"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].isReportEnable = false;
			}
			
	 	return errorFound;		
	 }

    var checkAndUpdate = function(param) {
			return $scope.update(param);
    }
    
    $scope.saveReport=function(){
    	  $scope.errorArray = [];
		  var listErrorFlag = false;
		  if($scope.reportMasterList != undefined && $scope.reportMasterList!= null && $scope.reportMasterList.length >0) {
			  for(var index = 0; index < $scope.reportMasterList.length; index++) {
					  if(index==0){
						  if($scope.validation(index)) {
							  listErrorFlag = true;
						   }
						  }else{
							  if($scope.isEmptyRow($scope.reportMasterList[index])){
								  $scope.reportMasterList.splice(index,1);
								  listErrorFlag = false;
							  }else{
								  if($scope.validation(index)) {
									  listErrorFlag = true;
								  }
							  }
						  }
				  }
			  if(listErrorFlag) {
				  return;
			  }  else {
				  $scope.tmpReportMasterList = cloneService.clone($scope.reportMasterList);
			  }
			  reportMasterFactory.create.save($scope.tmpReportMasterList).$promise.then(function(data) {
					  if (data.responseCode == 'ERR0') {
						  console.log("CarrierRate Saved successfully..");
						  var params = {};
						  params.submitAction = 'Saved';
						  $rootScope.successDesc=$rootScope.nls["ERR400"];
						  $state.go("layout.reportMaster",params);
					  } else {
					  }
				  }, function(error) {
					   });
		
		  }  
    }
    
	$scope.ajaxDocumentMaster = function(object) {

		console.log("ajaxDocumentMaster ", object);

		$scope.searchDto={};
		$scope.searchDto.keyword=object==null?"":object;
		$scope.searchDto.selectedPageNumber = 0;
		$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

	return	DocumentMasterList.query($scope.searchDto).$promise.then(function(data, status) {
				if (data.responseCode =="ERR0"){
					$scope.totalRecord = data.responseObject.totalRecord;
					 $scope.documentMasterList = data.responseObject.searchResult;
					console.log("$scope.documentMasterList", $scope.documentMasterList.length);
					 $scope.showDocumentList=true;
					 return $scope.documentMasterList
				}
			},
			function(errResponse){
				console.error('Error while fetching Currency List');
			}
		);
	}
    
    $scope.update = function(param) {
    	console.log("Update ", param);
    	if(param.item.reportMasterCode==undefined || param.item.reportMasterCode==null || param.item.reportMasterCode=="" ){
    		Notification.error($rootScope.nls["ERR06355"]+"at line no: "+(parseInt(param.index)+1));
    		return false ;
    	}
    	if(param.item.reportMasterName==undefined || param.item.reportMasterName==null || param.item.reportMasterName=="" ){
    		Notification.error($rootScope.nls["ERR06356"]+"at line no: "+(parseInt(param.index)+1));
    		return false ;
    	}
    	if(param.item.reportKey==undefined || param.item.reportKey==null || param.item.reportKey=="" ){
    		Notification.error($rootScope.nls["ERR06359"]+"at line no: "+(parseInt(param.index)+1));
    		return false ;
    	}
    	if(param.item.documentMaster==undefined || param.item.documentMaster==null || param.item.documentMaster=="" ||param.item.documentMaster.id==undefined){
    		param.item.documentMaster=null;
    	}else{
    		if(param.item.documentMaster.status == "Block") {
    			Notification.error($rootScope.nls["ERR06399"]+"at line no: "+(parseInt(param.index)+1));
    			return false ;
			} 
    	}
    	reportMasterFactory.update.query(param.item).$promise.then(function(data) {
	        	if (data.responseCode == 'ERR0') {
	        		Notification.success($rootScope.nls["ERR401"]);
	        		return true;
	        	} else {
	        	}
	        }, function(error) {
	        });
    }
    
    

   $scope.actionClick = function(param){   // report master save and delete role access
        console.log(param);
        
        if(param.type === 'SAVE') {
      	  if(!$rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_REPORT_MASTER_MODIFY)){
                  return;
      	  }
      	  return checkAndUpdate(param);
        }
        if(param.type === 'DELETE') {
      	  if(!$rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_REPORT_MASTER_DELETE)){
      		  return;
      	  }
      	  $scope.remove(param);
        }
	};

	 $scope.documentRender = function(item){
         return{
             label:item.documentName,
             item:item
         }
     }
    
	
	 
    $scope.addPage = function() {
    	 if($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_REPORT_MASTER_CREATE)){
    		 $state.go("layout.addReportMaster");
     	}
	};


	switch ($stateParams.action) {
	case "SEARCH":
		$scope.init();
		$rootScope.breadcrumbArr = [
		                  	      {label: "Setup",state: "layout.reportMaster"},
		                  	      {label: "Report Configuration",state: "layout.reportMaster"}
		                  	    ];
		break;
	case "ADD":
		$rootScope.breadcrumbArr = [ {label: "Setup",state: "layout.reportMaster" },
			                  	      {label: "Report Configuration",state: "layout.reportMaster"},
			                  	      {label: "Add Report Master",state:null}
			                  	    ];
		
		
		$scope.initAdd();
		break;
		
		
		
}		
 });