(function() {
	app.factory("ErrorSearch",['$resource', function($resource) {
		return $resource("/api/v1/language/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	app.factory("ErrorUpdate",['$resource', function($resource) {
		return $resource("/api/v1/language/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ErrorSave",['$resource', function($resource) {
		return $resource("/api/v1/language/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("ErrorRemove",['$resource', function($resource) {
		return $resource("/api/v1/language/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
})();
