app.controller("errormsgCtrl", ['$scope', '$location', '$window', '$modal', 'ngDialog', '$state', '$stateParams', '$rootScope', 'Notification',
    'ErrorSearch', 'ErrorUpdate', 'ErrorRemove', 'AppNlsApi', 'roleConstant',
    function($scope, $location, $window, $modal, ngDialog, $state, $stateParams, $rootScope, Notification,
        ErrorSearch, ErrorUpdate, ErrorRemove, AppNlsApi, roleConstant) {


        $scope.init = function() {
            console.log("Init method is called....");

            $rootScope.setNavigate1("Setup");
            $rootScope.setNavigate2("Error Message Master");
            $scope.searchDto = {};
            $scope.limitArr = [10, 15, 20];
            $scope.page = 0;
            $scope.limit = 10;
            $scope.search();
        }


        $scope.search = function() {
            console.log("Search is called....");

            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;

            if ($scope.searchDto.lastUpdatedOn != undefined && $scope.searchDto.lastUpdatedOn != null) {
                if (typeof $scope.searchDto.lastUpdatedOn.startDate != "string") {
                    $scope.searchDto.lastUpdatedOn.startDate = $rootScope.sendApiStartDateTime($scope.searchDto.lastUpdatedOn.startDate);
                    $scope.searchDto.lastUpdatedOn.endDate = $rootScope.sendApiStartDateTime($scope.searchDto.lastUpdatedOn.endDate);
                }
            }
            ErrorSearch.query($scope.searchDto).$promise.then(function(data, status) {

                $scope.totalRecord = data.responseObject.totalRecord;

                var tempArr = [];
                var resultArr = [];

                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);

                var tempObj = {};

                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });

                $scope.errorMsgArray = resultArr;
                console.log("$scope.errorMsgArray ", $scope.errorMsgArray);
            });
        }


        $scope.errormsgHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "prefWidth": "50",
            "model": "no",
            "search": false
        }, {
            "name": "Code",
            "search": true,
            "model": "languageCode",
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "col-md-2 col-xs-3",

            "key": "searchLanguageCode"
        }, {
            "name": "Message",
            "model": "languageDesc",
            "search": true,
            "wrap_cell": true,
            "type": "input",
            "sort": true,
            "width": "col-md-6 col-xs-6",

            "key": "searchLanguageDesc"
        }, {
            "name": "Group",
            "model": "groupName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "col-md-2 col-xs-3",

            "key": "searchGroupName"
        }, {
            "name": "Flagged",
            "model": "flagged",
            "search": true,
            "wrap_cell": true,
            "type": "select",
            "data": $rootScope.enum['YesNo'],
            "sort": true,
            "width": "col-md-2 col-xs-3",

            "key": "searchQcVerify"
        }, {
            "name": "Language",
            "model": "language.languageName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "width": "col-md-2 col-xs-3",

            "key": "searchLanguageName"
        }, {
            "name": "Last updated On",
            "model": "systemTrack.lastUpdatedDate",
            "search": true,
            "wrap_cell": true,
            "type": "date-range",
            "sort": true,
            "width": "col-md-2 col-xs-3",

            "key": "lastUpdatedOn"
        }, {
            "name": "Action",
            "model": "",
            "search": false,
            "wrap_cell": true,
            "type": "action",
            "actype": "action",
            "sort": false,
            "width": "col-md-2 col-xs-2",

            "key": ""
        }];


        $scope.sortSelection = {
            sortKey: "languageCode",
            sortOrder: "asc"
        }

        $scope.limitChange = function(item) {
            console.log("Limit Change is called...", item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.changeSearch = function(param) {

            console.log("Change Search", param);

            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }

            $scope.page = 0;
            console.log("change search", $scope.searchDto);

            $scope.search();
        }

        $scope.sortChange = function(param) {
            console.log("Sort Change Called.", param);

            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;

            $scope.search();
        }

        $scope.changepage = function(param) {
            console.log("Change Page Called.", param);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }



        $scope.actionClick = function(param) {
            console.log("param", param);

            if (param.type === 'SAVE') {
                if (!$rootScope.roleAccess(roleConstant.SETUP_ERROR_MESSAGE_MODIFY)) {
                    return;
                }
                checkAndUpdate(param);
            } else if (param.type === 'DELETE') {
                remove(param);
            } else {
                $scope.oldMessage = param.item.languageDesc;
                $scope.count1 = ($scope.oldMessage.match(/%s/g) || []).length;
                console.log("number of %s in the string ", $scope.count1);
            }

        }

        var remove = function(param) {
            console.log("Remove ", param);


            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected error message?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                ErrorRemove.remove({
                    id: param.item.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Error Code deleted Successfully");
                        $rootScope.clientMessage = $rootScope.nls["ERR402"];

                        // TODO
                        AppNlsApi.get({
                            language: 'English'
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                $rootScope.nls = data.responseObject;
                                sessionStorage.nls = JSON.stringify($rootScope.nls);
                                console.log("NLS Loaded Successfully : ")
                            } else {
                                console.log("NLS Loaded failed")
                            }
                        }, function(error) {
                            console.log("NLS Loaded failed : " + error)
                        });


                        $scope.init();

                    } else {
                        console.log("Error Code delete Failed ", data.responseDescription);
                    }
                }, function(error) {
                    console.log("Error Code delete Failed : ", error);
                });
            }, function(value) {
                console.log("delete Cancelled");
            });

        }


        var checkAndUpdate = function(param) {

            console.log("checkAndUpdate ", param);


            var newMessage = param.item.languageDesc;
            $scope.count2 = (newMessage.match(/%s/g) || []).length;
            console.log("number of %s in the modified message ", $scope.count2);

            if ($scope.count1 == $scope.count2) {
                $scope.update(param);
            } else if ($scope.count1 == 0 && $scope.count2 > 0) {
                //$rootScope.clientMessage = "Parameter substitution (%s) is not allowed";
                Notification.error("Parameter substitution (%s) is not allowed");
                $scope.errorMsgArray[param.index].languageDesc = $scope.oldMessage;
                return;
            } else if ($scope.count1 < $scope.count2) {
                $scope.errorMsgArray[param.index].languageDesc = $scope.oldMessage;
                //$rootScope.clientMessage = "Only "+$scope.count1+" parameter substitution (%s) are allowed";
                Notification.error("Only " + $scope.count1 + " parameter substitution (%s) are allowed");

                return;
            } else {
                $scope.errorMsgArray[param.index].languageDesc = $scope.oldMessage;
                //$rootScope.clientMessage = "Remove parameter substitution (%s)";
                Notification.error("Remove parameter substitution (%s)");

                return;
            }
        }



        $scope.update = function(param) {
            console.log("Update ", param);
            $rootScope.clientMessage = null;
            ErrorUpdate.update(param.item).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    param.item = data.responseObject;
                    $rootScope.nls[data.responseObject.languageCode] = data.responseObject.languageDesc;
                    Notification.success($rootScope.nls["ERR401"]);
                    //$scope.errorMsgArray[param.index].languageDesc = $rootScope.nls[$scope.errorMsgArray[param.index].languageCode]
                    console.log("NLS Loaded Successfully.....");
                } else {
                    console.log("ErrorCode updated Failed ", data.responseDescription)
                }
            }, function(error) {
                console.log("ErrorCode updated Failed : ", error)
            });
        }


        $scope.addPage = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_ERROR_MESSAGE_CREATE)) {
                $state.go("layout.addErrorMessage");
            }
        };

        switch ($stateParams.action) {
            case "SEARCH":
                console.log("Error Message Search Page....");
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Setup",
                        state: "layout.errorMessage"
                    },
                    {
                        label: "Error Message Master",
                        state: "layout.errorMessage"
                    }
                ];
                break;
        }

    }
]);