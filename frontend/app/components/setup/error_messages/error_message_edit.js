app.controller("errormessage_edit_ctrl",['$rootScope', '$scope', '$window', '$modal', 'ngDialog', '$state', '$stateParams', 'LanguageSearch', 'ErrorSave',
    function($rootScope, $scope, $window, $modal, ngDialog, $state, $stateParams, LanguageSearch, ErrorSave) {

    $scope.init = function() {
        $scope.errorMessageList = [];
        $scope.errorArray = [];
        $scope.errorMessageList.push({});
        $rootScope.setNavigate1("Setup");
        $rootScope.setNavigate2("Error Message Master");
        $rootScope.setNavigate3("Add Error Message");
        $scope.navigateToNextField("languageCode0");
    }

    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });
    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.addRow = function() {

        console.log("Add Row is clicked");
        $scope.firstFocus = true;
        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.errorMessageList != undefined && $scope.errorMessageList != null && $scope.errorMessageList.length > 0) {
            for (var index = 0; index < $scope.errorMessageList.length; index++) {
                if ($scope.validation(index)) {
                    listErrorFlag = true;
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                $scope.errorMessageList.push({});
            }
        } else {
            $scope.errorMessageList.push({});
        }
    };

    $scope.deleteRow = function(index) {
        console.log("Delete Row is clicked");
        $scope.isDirty = $scope.errorMessageForm.$dirty;
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR203"];
            newScope.errorMessage = newScope.errorMessage.replace("%s", " the selected error code");
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.errorMessageList.splice(index, 1);
                } else if (value == 2) {
                    console.log("You selected No");
                }
            });
        } else {
            $scope.errorMessageList.splice(index, 1);
        }
    }

    $scope.cancel = function() {

        $scope.isDirty = $scope.errorMessageForm.$dirty;
        if ($scope.isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.saveAll();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go("layout.errorMessage");
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.errorMessage");
        }
    }

    $scope.saveAll = function() {
        console.log("Save button is pressed...", $scope.errorMessageList);

        $scope.errorArray = [];
        var listErrorFlag = false;
        if ($scope.errorMessageList != undefined && $scope.errorMessageList != null && $scope.errorMessageList.length > 0) {
            for (var index = 0; index < $scope.errorMessageList.length; index++) {
                if ($scope.validation(index)) {
                    listErrorFlag = true;
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                ErrorSave.save($scope.errorMessageList).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("ErrorCode Saving Successful..");
                        $rootScope.successDesc = $rootScope.nls["ERR400"];
                        $state.go("layout.errorMessage");
                    } else {
                        console.log("ErrorCode Saving Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("ErrorCode Saving Failed ", error)
                });
            }
        }




    }

    $scope.ajaxLanguageEvent = function(object) {
        console.log("ajaxLanguageEvent is called ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return LanguageSearch.query($scope.searchDto).$promise.then(function(
            data, status) {
            if (data.responseCode == "ERR0") {
                $scope.totalRecord = data.responseObject.totalRecord;
                $scope.languageList = data.responseObject.searchResult;
                return $scope.languageList;
            }
        }, function(errResponse) {
            console.error('Error while fetching Language', errResponse);
        });
    }




    $scope.selectedLanguage = function(languageObj) {
        console.log("Language Selected ", languageObj);
    };


    $scope.selectedGroupName = function(object, nextCtrl) {
        console.log("selectedGroupName  ", object, nextCtrl);
        $rootScope.navigateToNextField(nextCtrl);
    }

    $scope.languageRender = function(item) {
        return {
            label: item.languageName,
            item: item
        }
    };


    $scope.validation = function(index) {

        $scope.errorArray[index] = {};
        $scope.errorArray[index].errTextArr = [];
        var errorFound = false;
        if ($scope.errorMessageList[index].languageCode == undefined || $scope.errorMessageList[index].languageCode == null ||
            $scope.errorMessageList[index].languageCode.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].languageCode = true;

            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR2505"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].languageCode = false;
        }


        if ($scope.errorMessageList[index].languageDesc == undefined || $scope.errorMessageList[index].languageDesc == null ||
            $scope.errorMessageList[index].languageDesc.length == 0) {

            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].languageDesc = true;

            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR2506"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].languageDesc = false;
        }


        if ($scope.errorMessageList[index].groupName == undefined || $scope.errorMessageList[index].groupName == null ||
            $scope.errorMessageList[index].groupName.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].groupName = true;

            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR2507"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].groupName = false;
        }



        if ($scope.errorMessageList[index].language == undefined || $scope.errorMessageList[index].language == null ||
            $scope.errorMessageList[index].language.length == 0) {

            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].language = true;

            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR2509"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].language = false;
        }


        return errorFound;

    }

    if ($stateParams.action === 'ADD') {
        console.log("Add Page....!");
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Setup",
                state: "layout.errorMessage"
            },
            {
                label: "Error Message Master",
                state: "layout.errorMessage"
            },
            {
                label: "Add Error Message",
                state: null
            }
        ];
    }

}]);