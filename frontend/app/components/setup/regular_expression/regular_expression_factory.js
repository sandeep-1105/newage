(function() {
	app.factory("RegExpAll", ['$resource',function($resource) {
		return $resource("/api/v1/regularexpression/get/all", {}, {
			get : {
				method : 'GET',
				params : {},
				isArray : false
			}
		});
	}]);
	
})();
