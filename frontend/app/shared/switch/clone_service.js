/**
 * 
 */
app.service('cloneService', function() {


    this.clone = function(data) {

        return JSON.parse(JSON.stringify(data));
    };

});

app.service('compareService', function() {


    this.compareArray = function(array) {

        var duplicateFound = false;
        if (array == undefined || array == null || array.length == undefined || array.length == 0) {

            return this.response(duplicateFound, null, null);
        } else {

            for (var i = 0; i < array.length; i++) {

                for (var j = 0; j < array.length; j++) {
                    if (i == j) {
                        continue;
                    }
                    if (JSON.stringify(array[i]) === JSON.stringify(array[j])) {
                        duplicateFound = true;
                        return this.response(duplicateFound, i, j);
                        
                    }
                }

            }
        }
        return this.response(duplicateFound, null, null);
    };

    this.response = function(duplicatefound, start, end) {
        return {
            isDuplicate: duplicatefound,
            start: start,
            end: end
        };
    }


});