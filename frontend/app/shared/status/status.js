
app.directive("showStatus",[ function() {
	return {
		scope : {
			status:'@'
		},
		templateUrl : 'app/shared/status/status.html',
		restrict : 'AE'
	};
}]);


app.directive("showMail",[ function() {
	
	return {
		scope : {
			status:'@'
		},
		templateUrl : 'app/shared/status/automailstatus.html',
		restrict : 'AE'
	};
}]);


