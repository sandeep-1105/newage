/**
 * Created by saravanan on 5/4/16.
 */
app.directive("customTable", ['$timeout', 'ngProgressFactory', function($timeout, ngProgressFactory) {
    return {
        restrict: 'E',
        scope: {
            "data": "=",
            "head": "=",
            "sort": "=",
            "searchEvent": "&",
            "dropEvent": "&",
            "clickEvent": "&",
            "sortEvent": "&"
        },
        templateUrl: 'app/shared/customTable/customTable.html',
        link: function($scope, elem, attr) {
            $scope.limitCharacter = 1;
            $timeout(function() {
                var setLimit = parseInt($(".wrapit").closest('table').find('th').eq($(".wrapit").parent().index()).outerWidth() / 11);
                $scope.limitCharacter = setLimit != 0 ? setLimit : 15;
                console.log("$scope.limitCharacter", $scope.limitCharacter);
            }, 0);
            $(window).bind('resize', function() {
                var setLimit = parseInt($(".wrapit").closest('table').find('th').eq($(".wrapit").parent().index()).outerWidth() / 11);
                $scope.limitCharacter = setLimit != 0 ? setLimit : 15;
                console.log("$scope.limitCharacter", $scope.limitCharacter);
            });


        },
        controller: function($scope, $rootScope) {
            $scope.searchData = {};
            $scope.sortData = {};
            $scope.contained_progressbar = ngProgressFactory.createInstance();
            $scope.contained_progressbar.setParent(document.getElementById('table-body'));
            $scope.contained_progressbar.setAbsolute();
            $scope.contained_progressbar.start();
            $scope.waitLoad = false;
            $scope.$watch('data', function(newvalue, oldvalue) {
                $timeout(function() {
                    $scope.contained_progressbar.complete();
                    $scope.waitLoad = false;
                }, 1000);
                if (newvalue != oldvalue) {
                    $scope.selectedRow = -1;
                }
            })

            $scope.datepickeropts = {
                locale: {
                    applyClass: 'btn-green',
                    applyLabel: "Apply",
                    fromLabel: "From",
                    format: "DD-MM-YYYY",
                    toLabel: "To",
                    cancelLabel: 'Cancel',
                    customRangeLabel: 'Custom range'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()]
                },
                eventHandlers: {
                    'apply.daterangepicker': function(ev, picker) {
                        console.log("ev", ev);
                        console.log("ev", JSON.stringify(ev.model));
                        console.log("picker", JSON.stringify(picker));
                        $scope.changeFunction();
                        //$scope.changeSearch();
                    }
                }
            };
            $scope.getStyle = function(index, row) {
                if (index == row) {
                    return {
                        "border-color": "#66afe9",
                        "background": "rgb(199,228,252)",
                        "background-color": "rgb(199,228,252)",
                        "outline": "0px auto -webkit-focus-ring-color",
                        "outline-offset": "0px",
                        "-webkit-box-shadow": "none",
                        "box-shadow": "none"
                    }
                }
            }

            $scope.tdClass = function(data) {
                if (data.wrap_cell) {
                    return "wrap_cell"
                }
            }
            $scope.changeFunction = function() {
                $scope.selectedRow = -1;
                $scope.searchEvent({ "param": $scope.searchData });
            }
            $scope.rowSelect = function(data, index) {
                $scope.selectedRow = index;
                $scope.clickEvent({ "param": data });
            }
            $scope.setthWidth = function(data) {
                if (data == "normal") {
                    return "width15per"
                } else if (data == "mid") {
                    return "width18per"
                } else if (data == "small") {
                    return "width10per"
                } else if (data == "xtrasmall") {
                    return "width4per"
                } else if (data == "large") {
                    return "width22per"
                }
            }

            $scope.config = {
                autoHideScrollbar: true,
                theme: 'light',
                setHeight: 200,
                scrollInertia: 0
            }

            $scope.changeOrder = function(data) {
                if (data.sort) {
                    if ($scope.sort.sortKey == data.model) {
                        $scope.selectedRow = -1;
                        $scope.sort.sortKey = data.model;
                        $scope.sort.sortOrder = $scope.sort.sortOrder == 'asc' ? 'desc' : 'asc';
                        $scope.sortEvent({ param: $scope.sort });
                    } else {
                        $scope.selectedRow = -1;
                        $scope.sort.sortKey = data.model;
                        $scope.sort.sortOrder = 'asc';
                        $scope.sortEvent({ param: $scope.sort });
                    }
                }

            }

            $scope.addClassestd = function(data) {
                if ($scope.head[data].wrap_cell) {
                    return 'wrap-cell';
                }

            }

            function clearObjectNull(obj) {
                for (var i in obj) {
                    if (obj[i] === null || obj[i] === undefined) {
                        delete obj[i];
                    }
                }
                return obj;
            }
            $scope.findProp = function(obj, prop, defval) {
                if (typeof defval == 'undefined') defval = null;
                prop = prop.split('.');
                obj = clearObjectNull(obj);
                for (var i = 0; i < prop.length; i++) {
                    //console.log("value",obj[prop[i]],typeof obj[prop[i]],prop[i]);
                    if (obj[prop[i]] == null && typeof obj[prop[i]] == 'undefined')
                        return defval;
                    obj = obj[prop[i]];
                }
                return obj;
            }
            $scope.setDate = function(obj, model) {
                return $rootScope.dateToString($scope.findProp(obj, model));
            }
            $scope.setTitle = function(type, obj, model) {
                if (type != 'img') {
                    if (type == 'date-range') {
                        return $rootScope.dateToString($scope.findProp(obj, model));
                    } else {
                        return $scope.findProp(obj, model);
                    }
                }
            }


        }
    }
}]).filter('strLimit', ['$filter', function($filter) {
    return function(value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                //Also remove . and , so its gives a cleaner result.
                if (value.charAt(lastspace - 1) == '.' || value.charAt(lastspace - 1) == ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
}]);