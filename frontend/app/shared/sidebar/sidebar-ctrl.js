/**
 * Created by saravanan on 29/3/16.
 */
app.controller("sidebarCtrl",["$scope",function($scope){
    $scope.menudata = {};
    $scope.menudata.opened = false;
    $scope.submenustatus = false;
  /*  $scope.mainMenuArr = [
        {
        "icon":"dashboard",
        "title":"Dashboard",
        "submenu":false,
        "link":"#/home"
    }, {
        "icon":"task",
        "title":"Tasks",
        "submenu":true
    },{
        "icon":"sales",
        "title":"Sales",
        "submenu":true,
        "link":"#/home"
    },{
        "icon":"crm",
        "title":"CRM",
        "submenu":true,
        "link":"#/home"
    },{
        "icon":"air-plane",
        "title":"Air",
        "submenu":true,
        "link":"#/home"
    },{
        "icon":"ocean",
        "title":"Ocean",
        "submenu":true,
        "link":"#/home"
    },{
        "icon":"truck",
        "title":"Land",
        "submenu":true,
        "link":"#/home"
    },{
        "icon":"finance",
        "title":"Finance",
        "submenu":true,
        "link":"#/home"
    },{
        "icon":"master",
        "title":"Masters",
        "submenu":true,
        "submenudata":[
                {

                    "icon":"fa-tag",
                    "title":"Segment",
                    "submenu":false,
                    "link":"/segment"

                },{

                    "icon":"fa-tag",
                    "title":"Carrier",
                    "submenu":false,
                    "link":"/carrier"

                },{

                    "icon":"fa-tag",
                    "title":"Unit",
                    "submenu":false,
                    "link":"/unit"

                },{

                    "icon":"fa-tag",
                    "title":"Charge",
                    "submenu":false,
                    "link":"/charge"

                },{

                "icon":"fa-tag",
                "title":"TOS",
                "submenu":false,
                "link":"/tos"

            },{

                "icon":"fa-tag",
                "title":"Pack",
                "submenu":false,
                "link":"/pack"

            }]
    }];*/

    $scope.$on('menuOpenEvent', function (event, args) {
        $scope.menuState = args.menuState;
    });
    function closeMenufn(){
        if(!$scope.menudata.opened) {
            $scope.menuState = false;
            $scope.submenustatus = false;
        }
        if( $scope.menudata.opened){
            $scope.menudata.opened = false;
            $scope.submenustatus = false;
            $scope.$emit("submenudata",{menuData:$scope.menudata});
        }

    }
    $scope.closeMenu = function(){
        closeMenufn();
    };

    $scope.$on('closeAllMenu', function (event) {
        closeMenufn();
    });

    $scope.openSubMenu = function (menu) {
        $scope.menudata.menu = menu;
        $scope.menudata.opened = true;
        if($scope.menuState!=true) {
            $scope.submenustatus = true;
        }
        $scope.$emit("submenudata",{menuData:$scope.menudata});
    }

    $scope.openMenuLink = function(menu){
        console.log(menu);
    }
}]);
