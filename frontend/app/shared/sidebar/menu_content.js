app.service('MenuContentService', ['$rootScope','roleConstant', function($rootScope,roleConstant) {

    this.getMenuContent = function() {

        return [{
                "icon": "icon-dashboard",
                "title": "Dashboard",
                "submenu": false,
                "link": "/home",
                "activeMenu": "home"
            }, {
                "icon": "icon-calendar",
                "title": "Tasks",
                "submenu": true,
                "activeMenu": "task",
                "submenudata": [{
                    "icon": "fa-tag",
                    "title": "My Tasks",
                    "submenu": false,
                    "listLink": "layout.myTask",
                    "accessList": roleConstant.TASKS_MY_TASKS_LIST
                }]
            }, {
                "icon": "icon-sales",
                "title": "Sales",
                "submenu": true,
                "activeMenu": "sales",
                "submenudata": [{

                    "icon": "fa-tag",
                    "title": "Enquiry",
                    "submenu": false,
                    "listLink": "layout.salesEnquiry",
                    "operationLink": "layout.addSalesEnquiry",
                    "accessList": roleConstant.SALE_ENQUIRY_LIST, 
                    "accessAdd": roleConstant.SALE_ENQUIRY_CREATE,
                    "activeMenu": "enquiry"
                }, /*{

                    "icon": "fa-tag",
                    "title": "Enquiry Design 1",
                    "submenu": false,
                    "listLink": "layout.addSalesEnquiryDesign1",
                    "activeMenu": "enquiry",
                    "access": $rootScope.userProfile.featureMap['SALES_ENQUIRY']
                }, {

                    "icon": "fa-tag",
                    "title": "Enquiry Design 2",
                    "submenu": false,
                    "listLink": "layout.addSalesEnquiryDesign2",
                    "activeMenu": "enquiry",
                    "access": $rootScope.userProfile.featureMap['SALES_ENQUIRY']
                }, {

                    "icon": "fa-tag",
                    "title": "Enquiry Design 3",
                    "submenu": false,
                    "listLink": "layout.addSalesEnquiryDesign3",
                    "activeMenu": "enquiry",
                    "access": $rootScope.userProfile.featureMap['SALES_ENQUIRY']
                }, */{

                    "icon": "fa-tag",
                    "title": "Quotation",
                    "submenu": false,
                    "listLink": "layout.salesQuotation",
                    "operationLink": "layout.salesQuotationAdd",
                    "accessList": roleConstant.SALE_QUOTATION_LIST, 
                    "accessAdd": roleConstant.SALE_QUOTATION_CREATE,
                    "activeMenu": "quotation"
                }]
            },
                  
             {
                "icon": "icon-crm",
                "title": "CRM",
                "submenu": true,
                "activeMenu": "crm",
                "submenudata": [{

                        "icon": "fa-tag",
                        "title": "Shipment",
                        "submenu": false,
                        "listLink": "layout.crmShipment",
                        "operationLink": "layout.addNewShipment",
                        "accessList": roleConstant.CRM_SHIPMENT_LIST, 
                        "accessAdd": roleConstant.CRM_SHIPMENT_CREATE,
                        "activeMenu": "shipment"
                    },  {

                        "icon": "fa-tag",
                        "title": "Import to Export",
                        "submenu": false,
                        "listLink": "layout.crmImportToExport",
                        "accessList": roleConstant.CRM_IMPORT_TO_EXPORT_LIST, 
                        "activeMenu": "importExport"
                    },
                    {
                        "icon": "fa-tag",
                        "title": "Default Charges",
                        "submenu": false,
                        "listLink": "layout.defaultCharges",
                        "operationLink": "layout.addDefaultCharges",
                        "accessList": roleConstant.CRM_DEFAULT_CHARGES_LIST, 
                        "activeMenu": "defaultCharges"
                    },
                    {
                        "icon": "fa-tag",
                        "title": "Purchase Order",
                        "submenu": false,
                        "listLink": "layout.PurchaseOrder",
                        "operationLink": "layout.addPurchaseOrder",
                        "accessList": roleConstant.CRM_PURCHASE_ORDER_LIST, 
                        "accessAdd": roleConstant.CRM_PURCHASE_ORDER_CREATE,
                        "activeMenu": "purchase"
                    },
                    {
                        "icon": "fa-tag",
                        "title": "Automated Export System",
                        "submenu": false,
                        "listLink": "layout.aesList",
                        "accessList": roleConstant.CRM_AUTOMATED_EXPORT_SYSTEM_LIST, 
                        "activeMenu": "aesList"
                    },
                    {
                        "icon": "fa-tag",
                        "title": "Pricing Air",
                        "submenu": false,
                        "listLink": "layout.pricingAir",
                        "operationLink": "layout.pricingAirAdd",
                        "accessList": roleConstant.CRM_PRICING_AIR_LIST, 
                        "accessAdd": roleConstant.CRM_PRICING_AIR_CREATE,
                        "activeMenu": "pricingAir"
                    }, {
                        "icon": "fa-tag",
                        "title": "IATA Rates - Air",
                        "submenu": false,
                        "listLink": "layout.iataRatesAir",
                        "operationLink": "layout.iataRatesAirAdd",
                        "accessList": roleConstant.CRM_IATA_RATES_AIR_LIST, 
                        "accessAdd": roleConstant.CRM_IATA_RATES_AIR_CREATE,
                        "activeMenu": "iataRates"
                    }
                ]


            }, {
                "icon": "icon-air-plane",
                "title": "Air",
                "submenu": true,
                "activeMenu": "air",
                "submenudata": [{
                    "icon": "fa-tag",
                    "title": "Master Shipment",
                    "submenu": false,
                    "listLink": "layout.airNewConsol",
                    "operationLink": "layout.addNewAirConsol",
                    "accessList": roleConstant.AIR_MASTER_SHIPMENT_LIST, 
                    "accessAdd": roleConstant.AIR_MASTER_SHIPMENT_CREATE,
                    "activeMenu": "consol"
                },/*{
                    "icon": "fa-tag",
                    "title": "Master Shipment",
                    "submenu": false,
                    "listLink": "layout.airConsol",
                    "operationLink": "layout.addAirConsol",
                    "accessList": roleConstant.AIR_MASTER_SHIPMENT_LIST, 
                    "accessAdd": roleConstant.AIR_MASTER_SHIPMENT_CREATE,
                    "activeMenu": "consol"
                },*/ {
                    "icon": "fa-tag",
                    "title": "Documents",
                    "submenu": false,
                    "listLink": "layout.airDocuments",
                    "operationLink": "/air/document_operation",
                    "accessList": roleConstant.AIR_DOCUMENT_LIST, 
                    "activeMenu": "Airdocuments"
                }, {
                    "icon": "fa-tag",
                    "title": "Flight Schedule",
                    "submenu": false,
                    "listLink": "layout.airFlightPlan",
                    "operationLink": "layout.addFlightPlan",
                    "accessList": roleConstant.AIR_FLIGHT_SCHEDULE_LIST, 
                    "accessAdd": roleConstant.AIR_FLIGHT_SCHEDULE_CREATE,
                    "activeMenu": "flightPlan"
                }, {
                    "icon": "fa-tag",
                    "title": "MAWB Stock Generation",
                    "submenu": false,
                    "listLink": "layout.airStock",
                    "accessList": roleConstant.AIR_MAWB_STOCK_GENERATION_LIST,
                    "activeMenu": "mawbStock"
                }, {
                    "icon": "fa-tag",
                    "title": "CFS Receive Entry",
                    "submenu": false,
                    "listLink": "layout.airCFS",
                    "operationLink": "layout.addAirCFS",
                    "accessList": roleConstant.AIR_CFS_RECIEVE_ENTRY_LIST, 
                    "accessAdd": roleConstant.AIR_CFS_RECIEVE_ENTRY_CREATE,
                    "activeMenu": "AirCFS"
                }, {
                    "icon": "fa-tag",
                    "title": "Airline Prebooking Entry",
                    "submenu": false,
                    "listLink": "layout.airlinePrebooking",
                    "operationLink": "layout.addAirlinePrebooking",
                    "accessList": roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_LIST, 
                    "accessAdd": roleConstant.AIR_AIRLINE_PREBOOKING_ENTRY_CREATE,
                    "activeMenu": "airPrebooking"
                } /*{
                    "icon": "fa-tag",
                    "title": "WIN Status",
                    "submenu": false,
                    "listLink": "layout.winstatus",
                    "access": $rootScope.userProfile.featureMap['WIN_STATUS'],
                    "activeMenu": "winStatus"
                }*/]
            }, {
                "icon": "icon-ocean",
                "title": "Ocean",
                "submenu": true,
                "link": "#/home",
                "activeMenu": "ocean"
            }, {
                "icon": "icon-truck",
                "title": "Land",
                "submenu": true,
                "link": "#/home",
                "activeMenu": "truck"
            }, {
                "icon": "icon-train",
                "title": "Rail",
                "submenu": true,
                "link": "#/home",
                "activeMenu": "train"
            }, {
                "icon": "icon-finance",
                "title": "Finance",
                "submenu": true,
                "activeMenu": "finance",
                "submenudata": [{

                    "icon": "fa-tag",
                    "title": "Invoice",
                    "submenu": false,
                    "listLink": "layout.invoice",
                    "accessList": roleConstant.FINANCE_INVOICE_LIST, 
                    "activeMenu": "invoice"
                }, {

                    "icon": "fa-tag",
                    "title": "Credit Note Cost",
                    "submenu": false,
                    "listLink": "layout.creditNoteCost",
                    "accessList": roleConstant.FINANCE_CREDIT_NOTE_COST_LIST, 
                    "activeMenu": "cnc"
                },{
                    "icon": "fa-tag",
                    "title": "Job Ledger",
                    "submenu": false,
                    "listLink": "layout.jobledger",
                    "accessList": roleConstant.FINANCE_JOB_LEDGER_VIEW, 
                    "activeMenu": "jobLedger"
                },  {

                    "icon": "fa-tag",
                    "title": "Credit Note Revenue",
                    "submenu": false,
                    "listLink": "layout.creditNoteRevenue",
                    "accessList": roleConstant.FINANCE_CREDIT_NOTE_REVENUE_LIST, 
                    "activeMenu": "cnr"
                }, {
                    "icon": "fa-tag",
                    "title": "Provisional Revenue / Cost",
                    "submenu": false,
                    "listLink": "layout.provisionalCost",
                    "activeMenu": "prc",
                    "accessList": roleConstant.FINANCE_PROVISIONAL_LIST
                }, {

                    "icon": "fa-tag",
                    "title": "Shipment Sign Off",
                    "submenu": false,
                    "listLink": "layout.ShipmentSignOff",
                    "activeMenu": "sso",
                    "accessList": roleConstant.FINANCE_SHIPMENT_SIGN_OFF_LIST 
                }]
            },

            {
                "icon": "icon-report",
                "title": "Reports",
                "submenu": false,
                "link": "/reports/reports_list",
                "activeMenu": "report"
            }, {
                "icon": "icon-masters",
                "title": "Masters",
                "submenu": true,
                "activeMenu": "master",
                "submenudata": [{
                    "icon": "fa-tag",
                    "title": "General",
                    "submenu": true,
                    "activeMenu": "General",
                    "submenudata": [{
                            "icon": "fa-tag",
                            "title": "AES Filer",
                            "submenu": false,
                            "listLink": "layout.aesFilerMaster",
                            "operationLink": "layout.aesFilerMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_AES_FILER_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_AES_FILER_CREATE,
                            "activeMenu": "aesFiler"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Grade",
                            "submenu": false,
                            "listLink": "layout.gradeMaster",
                            "operationLink": "layout.gradeMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_GRADE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_GRADE_CREATE,
                            "activeMenu": "Grade"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "General Note",
                            "submenu": false,
                            "listLink": "layout.generalnotemaster",
                            "operationLink": "layout.generalnotemasterAdd",
                            "activeMenu": "GeneralNoteMaster"
                        }
                        ,{
                            "icon": "fa-tag",
                            "title": "Carrier",
                            "submenu": false,
                            "listLink": "layout.carrier",
                            "operationLink": "layout.addCarrier",
	                        "accessList": roleConstant.MASTER_GENERAL_CARRIER_LIST, 
	                        "accessAdd": roleConstant.MASTER_GENERAL_CARRIER_CREATE,
                            "activeMenu": "carrier"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "City",
                            "submenu": false,
                            "listLink": "layout.city",
                            "operationLink": "layout.addCity",
                            "accessList": roleConstant.MASTER_GENERAL_CITY_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_CITY_CREATE,
                            "activeMenu": "city"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Comment",
                            "submenu": false,
                            "listLink": "layout.commentMaster",
                            "operationLink": "layout.commentMasterAdd",
                            "activeMenu": "commentMaster"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Commodity",
                            "submenu": false,
                            "listLink": "layout.commodity",
                            "operationLink": "layout.addCommodity",
                            "accessList": roleConstant.MASTER_GENERAL_COMMODITY_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_COMMODITY_CREATE,
                            "activeMenu": "commodity"
                        }, {
                            "icon": "fa-tag",
                            "title": "Company",
                            "submenu": false,
                            "listLink": "layout.company",
                            "operationLink": "layout.addCompany",
                            "access": $rootScope.userProfile.featureMap[
                                'MASTER_COMPANY'
                                ],
                            "activeMenu": "company"
                        },{
                            "icon": "fa-tag",
                            "title": "Currency",
                            "submenu": false,
                            "listLink": "layout.currency",
                            "operationLink": "layout.addCurrency",
	                        "accessList": roleConstant.MASTER_GENERAL_CURRENCY_LIST, 
	                        "accessAdd": roleConstant.MASTER_GENERAL_CURRENCY_CREATE,
                            "activeMenu": "currency"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Country",
                            "submenu": false,
                            "listLink": "layout.countryMaster",
                            "operationLink": "layout.countryMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_COUNTRY_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_COUNTRY_CREATE,
                            "activeMenu": "country"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Default",
                            "submenu": false,
                            "listLink": "layout.defaultMaster",
                            "operationLink": "layout.defaultMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_DEFAULT_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_DEFAULT_CREATE,
                            "activeMenu": "default"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Object Group",
                            "submenu": false,
                            "listLink": "layout.objectGroupMaster",
                            "operationLink": "layout.objectGroupMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_OBJECT_GROUP_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_OBJECT_GROUP_CREATE,
                            "activeMenu": "ObjectGroup"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Object SubGroup",
                            "submenu": false,
                            "listLink": "layout.objectSubGroupMaster",
                            "operationLink": "layout.objectSubGroupMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_OBJECT_SUBGROUP_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_OBJECT_SUBGROUP_CREATE,
                            "activeMenu": "ObjectSubGroup"
                        },{
                            "icon": "fa-tag",
                            "title": "Charter",
                            "submenu": false,
                            "listLink": "layout.charter",
                            "operationLink": "layout.addCharter",
                            "access": $rootScope.userProfile.featureMap[
                                'MASTER_PACK'
                                ],
                            "activeMenu": "charter"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Pack",
                            "submenu": false,
                            "listLink": "layout.pack",
                            "operationLink": "layout.addPack",
                            "accessList": roleConstant.MASTER_GENERAL_PACK_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_PACK_CREATE,
                            "activeMenu": "pack"
                        }, {
                            "icon": "fa-tag",
                            "title": "Account",
                            "submenu": false,
                            "listLink": "layout.account",
                            "operationLink": "layout.addAccount",
                            "activeMenu": "account"
                        },
                        
                        {
                            "icon": "fa-tag",
                            "title": "Port Group",
                            "submenu": false,
                            "listLink": "layout.portGroup",
                            "operationLink": "layout.portGroupAdd",
                            "accessList": roleConstant.MASTER_GENERAL_PORT_GROUP_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_PORT_GROUP_CREATE,
                            "activeMenu": "portGroup"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Port",
                            "submenu": false,
                            "listLink": "layout.port",
                            "operationLink": "layout.addPort",
                            "accessList": roleConstant.MASTER_GENERAL_PORT_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_PORT_CREATE,
                            "activeMenu": "port"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "State / Province",
                            "submenu": false,
                            "listLink": "layout.state",
                            "operationLink": "layout.addState",
                            "accessList": roleConstant.MASTER_GENERAL_STATE_PROVINCE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_STATE_PROVINCE_CREATE,
                            "activeMenu": "state"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Service Type",
                            "submenu": false,
                            "listLink": "layout.serviceType",
                            "operationLink": "layout.addServiceType",
                            "accessList": roleConstant.MASTER_GENERAL_SERVICE_TYPE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_SERVICE_TYPE_CREATE,
                            "activeMenu": "serviceType"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Service",
                            "submenu": false,
                            "listLink": "layout.service",
                            "operationLink": "layout.addService",
                            "accessList": roleConstant.MASTER_GENERAL_SERVICE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_SERVICE_CREATE,
                            "activeMenu": "service"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Unit",
                            "submenu": false,
                            "listLink": "layout.unit",
                            "operationLink": "layout.addUnit",
                            "accessList": roleConstant.MASTER_GENERAL_UNIT_LIST, 
                           "accessAdd": roleConstant.MASTER_GENERAL_UNIT_CREATE,
                            "activeMenu": "unit"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Terms of Shipment",
                            "submenu": false,
                            "listLink": "layout.tos",
                            "operationLink": "layout.addTos",
                            "accessList": roleConstant.MASTER_GENERAL_TOS_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_TOS_CREATE,
                            "activeMenu": "tos"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Software",
                            "submenu": false,
                            "listLink": "layout.softwareMaster",
                            "operationLink": "layout.softwareMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_SOFTWARE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_SOFTWARE_CREATE,
                            "activeMenu": "software"
                        },{
                            "icon": "fa-tag",
                            "title": "Value Added Service",
                            "submenu": false,
                            "listLink": "layout.valueAddedServices",
                            "operationLink": "layout.valueAddedServicesAdd",
                            "accessList": roleConstant.MASTER_GENERAL_VALUE_ADDED_SERVICE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_VALUE_ADDED_SERVICE_CREATE,
                            "activeMenu": "valueAddedServices"
                        },{
                            "icon": "fa-tag",
                            "title": "Region / Sector",
                            "submenu": false,
                            "listLink": "layout.regionMaster",
                            "operationLink": "layout.regionMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_REGION_SECTOR_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_REGION_SECTOR_CREATE,
                            "activeMenu": "region"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Zone",
                            "submenu": false,
                            "listLink": "layout.zoneMaster",
                            "operationLink": "layout.zoneMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_ZONE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_ZONE_CREATE,
                            "activeMenu": "zone"
                        }, {
                            "icon": "fa-tag",
                            "title": "Division",
                            "submenu": false,
                            "listLink": "layout.divisionMaster",
                            "operationLink": "layout.divisionMasterAdd",
                            "accessList": roleConstant.MASTER_GENERAL_DIVISION_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_DIVISION_CREATE,
                            "activeMenu": "division"
                        }, {
                            "icon": "fa-tag",
                            "title": "Document Prefix",
                            "submenu": false,
                            "listLink": "layout.docprefix",
                            "operationLink": "layout.addDocprefix",
                            "accessList": roleConstant.MASTER_GENERAL_DOCUMENT_PREFIX_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_DOCUMENT_PREFIX_CREATE,
                            "activeMenu": "documentPrefix"
                        }, {

                            "icon": "fa-tag",
                            "title": "Auto Mail",
                            "submenu": false,
                            "listLink": "layout.automail",
                            "operationLink": "layout.addAutomail",
                            "access": $rootScope.userProfile.featureMap['MASTER_AUTO_MAIL'],
                            "accessList": roleConstant.MASTER_GENERAL_AUTO_MAIL_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_AUTO_MAIL_CREATE,
                            "activeMenu": "autoEmail"
                        },
                        {

                            "icon": "fa-tag",
                            "title": "Essential Charge",
                            "submenu": false,
                            "listLink": "layout.essentialList",
                            "operationLink": "layout.essentialChargeAdd",
                            "accessList": roleConstant.MASTER_GENERAL_ESSENTIAL_CHARGE_LIST, 
                            "accessAdd": roleConstant.MASTER_GENERAL_ESSENTIAL_CHARGE_CREATE,
                            "activeMenu": "essentialCharges"
                        }
                    ]
                }, {
                    "icon": "fa-tag",
                    "title": "eCHA",
                    "submenu": false,
                    "activeMenu": "eCHA"
                }, {
                    "icon": "fa-tag",
                    "title": "eHRMS",
                    "submenu": true,
                    "activeMenu": "ehrms",
                    "submenudata": [{
                        "icon": "fa-tag",
                        "title": "Department",
                        "submenu": false,
                        "listLink": "layout.department",
                        "operationLink": "layout.addDepartment",
                        "accessList": roleConstant.MASTER_HRMS_DEPARTMENT_LIST, 
                        "accessAdd": roleConstant.MASTER_HRMS_DEPARTMENT_CREATE,
                        "activeMenu": "department"
                    }, {
                        "icon": "fa-tag",
                        "title": "Designation",
                        "submenu": false,
                        "listLink": "layout.designation",
                        "operationLink": "layout.addDesignation",
                        "accessList": roleConstant.MASTER_HRMS_DESIGNATION_LIST, 
                        "accessAdd": roleConstant.MASTER_HRMS_DESIGNATION_CREATE,
                        "activeMenu": "designation"
                    }, {

                        "icon": "fa-tag",
                        "title": "Employee",
                        "submenu": false,
                        "listLink": "layout.employee",
                        "operationLink": "layout.addEmployee",
                        "accessList": roleConstant.MASTER_HRMS_EMPLOYEE_LIST, 
                        "accessAdd": roleConstant.MASTER_HRMS_EMPLOYEE_CREATE,
                        "activeMenu": "employee"
                    }]

                }, {
                    "icon": "fa-tag",
                    "title": "eAir",
                    "submenu": true,
                    "activeMenu": "eair",
                    "submenudata": [{
                        "icon": "fa-tag",
                        "title": "Carrier Rate",
                        "submenu": false,
                        "listLink": "layout.carrierRate",
                        "operationLink": "layout.carrierRateAdd",
                        "accessList": roleConstant.MASTER_EAIR_CARRIER_RATE_LIST, 
                        "accessAdd": roleConstant.MASTER_EAIR_CARRIER_RATE_CREATE,
                        "activeMenu": "carrierRate"
                    }]

                }, {
                    "icon": "fa-tag",
                    "title": "eCRM",
                    "submenu": true,
                    "activeMenu": "eCRM",
                    "submenudata": [{
                            "icon": "fa-tag",
                            "title": "Buyer Consolidation",
                            "submenu": false,
                            "listLink": "layout.buyerconsolidation",
                            "operationLink": "layout.buyerconsolidationAdd",
                            "accessList": roleConstant.MASTER_CRM_BUYER_CONSOLIDATION_LIST, 
                            "accessAdd": roleConstant.MASTER_CRM_BUYER_CONSOLIDATION_CREATE,
                            "activeMenu": "buyerConsolidation"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Trigger ",
                            "submenu": false,
                            "listLink": "layout.triggerMaster",
                            "operationLink": "layout.triggerMasterAdd",
                            "accessList": roleConstant.MASTER_CRM_TRIGGER_LIST, 
                            "accessAdd": roleConstant.MASTER_CRM_TRIGGER_CREATE,
                            "activeMenu": "trigger"
                        },
                        {
                            "icon": "fa-tag",
                            "title": "Trigger Type",
                            "submenu": false,
                            "listLink": "layout.triggerTypeMaster",
                            "operationLink": "layout.triggerTypeMasterAdd",
                            "accessList": roleConstant.MASTER_CRM_TRIGGER_TYPE_LIST, 
                            "accessAdd": roleConstant.MASTER_CRM_TRIGGER_TYPE_CREATE,
                            "activeMenu": "triggerType"
                        },
                        {
                            "icon": "fa-tag",
                            "title": $rootScope.appMasterData!=undefined && $rootScope.appMasterData!=null?$rootScope.appMasterData['Party_Label']:'Party',
                            "submenu": false,
                            "listLink": "layout.party",
                            "operationLink": "layout.addParty",
                            "accessList": roleConstant.MASTER_CRM_PARTY_LIST, 
                            "accessAdd": roleConstant.MASTER_CRM_PARTY_CREATE,
                            "activeMenu": "party"
                        },
                        {
                            "icon": "fa-tag",
                            "title": ($rootScope.appMasterData!=undefined && $rootScope.appMasterData!=null?$rootScope.appMasterData['Party_Label']:'Party') + ' Group',
                            "submenu": false,
                            "listLink": "layout.partyGroup",
                            "operationLink": "layout.addPartyGroup",
                            "accessList": roleConstant.MASTER_CRM_PARTY_GROUP_LIST, 
                            "accessAdd": roleConstant.MASTER_CRM_PARTY_GROUP_CREATE,
                            "activeMenu": "partyGroup"
                        },
                        {
                            "icon": "fa-tag",
                            "title": ($rootScope.appMasterData!=undefined && $rootScope.appMasterData!=null?$rootScope.appMasterData['Party_Label']:'Party') + ' Type',
                            "submenu": false,
                            "listLink": "layout.partyType",
                            "operationLink": "layout.addPartyType",
                            "accessList": roleConstant.MASTER_CRM_PARTY_TYPE_LIST, 
                            "accessAdd": roleConstant.MASTER_CRM_PARTY_TYPE_CREATE,
                            "activeMenu": "partyType"
                        }
                    ]
                }, {
                    "icon": "fa-tag",
                    "title": "eFinance",
                    "submenu": true,
                    "activeMenu": "eFinance",
                    "submenudata": [{
                        "icon": "fa-tag",
                        "title": "Bank",
                        "submenu": false,
                        "listLink": "layout.bankMaster",
                        "operationLink": "layout.bankMasterAdd",
                        "accessList": roleConstant.MASTER_FINANCE_BANK_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_BANK_CREATE,
                        "activeMenu": "bank"
                    }, {
                        "icon": "fa-tag",
                        "title": "Vertical",
                        "submenu": false,
                        "listLink": "layout.category",
                        "operationLink": "layout.addCategory",
                        "accessList": roleConstant.MASTER_FINANCE_VERTICAL_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_VERTICAL_CREATE,
                        "activeMenu": "category"
                    }, {

                        "icon": "fa-tag",
                        "title": "Charge",
                        "submenu": false,
                        "listLink": "layout.charge",
                        "operationLink": "layout.addCharge",
                        "accessList": roleConstant.MASTER_FINANCE_CHARGE_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_CHARGE_CREATE,
                        "activeMenu": "charges"
                    }, {

                        "icon": "fa-tag",
                        "title": "Currency Rate",
                        "submenu": false,
                        "listLink": "layout.currencyRate",
                        "operationLink": "layout.addCurrencyRate",
                        "accessList": roleConstant.MASTER_FINANCE_CURRENCY_RATE_LIST, 
                        "accessAdd" : roleConstant.MASTER_FINANCE_CURRENCY_RATE_CREATE, 
                        "activeMenu": "currencyRate"
                    }, {

                        "icon": "fa-tag",
                        "title": "Cost Center",
                        "submenu": false,
                        "listLink": "layout.costCenter",
                        "operationLink": "layout.addCostCenter",
                        "accessList": roleConstant.MASTER_FINANCE_COST_CENTER_LIST, 
                        "accessAdd" : roleConstant.MASTER_FINANCE_COST_CENTER_CREATE,
                        "activeMenu": "costCenter"
                    }, {

                        "icon": "fa-tag",
                        "title": "General Ledger",
                        "submenu": false,
                        "listLink": "layout.generalLedgerCA",
                        "operationLink": "layout.addGeneralLedgerCA",
                        "access": $rootScope.userProfile.featureMap['MASTER_GL_MASTER'],
                        "activeMenu": "glMaster"
                    },
                    {

                        "icon": "fa-tag",
                        "title": "General Ledger New",
                        "submenu": false,
                        "listLink": "layout.generalLedgerNewCA",
                        "operationLink": "layout.addGeneralLedgerNewCA",
                        "access": $rootScope.userProfile.featureMap['MASTER_GL_MASTER'],
                        "activeMenu": "glMaster"
                    },
                    
                    {

                        "icon": "fa-tag",
                        "title": "Sub Ledger",
                        "submenu": false,
                        "listLink": "layout.subLedgerCA",
                        "operationLink": "layout.addSubLedgerCA",
                        "accessList": roleConstant.MASTER_FINANCE_SUBLEDGER_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_SUBLEDGER_CREATE,
                        "activeMenu": "subLedger"
                    }, 
                    {

                        "icon": "fa-tag",
                        "title": "Sub Ledger New Screen",
                        "submenu": false,
                        "listLink": "layout.subLedgerList",
                        "operationLink": "layout.addEditSubLedgerCA",
                        "accessList": roleConstant.MASTER_FINANCE_SUBLEDGER_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_SUBLEDGER_CREATE,
                        "activeMenu": "subLedger"
                    }, 
                    {

                        "icon": "fa-tag",
                        "title": "Daybook",
                        "submenu": false,
                        "listLink": "layout.daybookMaster",
                        "operationLink": "layout.daybookMasterAdd",
                        "accessList": roleConstant.MASTER_FINANCE_DAY_BOOK_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_DAY_BOOK_CREATE,
                        "activeMenu": "dayBook"
                    }, {

                        "icon": "fa-tag",
                        "title": "Service Tax Charge Group",
                        "submenu": false,
                        "listLink": "layout.stcgroupmaster",
                        "operationLink": "layout.stcgroupmasterAdd",
                        "accessList": roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_CREATE,
                        "activeMenu": "serviceTax"
                    }, {

                        "icon": "fa-tag",
                        "title": "Service Tax Category",
                        "submenu": false,
                        "listLink": "layout.serTaxCatMaster",
                        "operationLink": "layout.serTaxCatMasterAdd",
                        "accessList": roleConstant.MASTER_FINANCE_SERVICE_TAX_CATEGORY_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_SERVICE_TAX_CATEGORY_CREATE,
                        "activeMenu": "ServiceTaxCategory"
                    }, {

                        "icon": "fa-tag",
                        "title": "Reason",
                        "submenu": false,
                        "listLink": "layout.reasonMaster",
                        "operationLink": "layout.reasonMasterAdd",
                        "accessList": roleConstant.MASTER_FINANCE_REASON_LIST, 
                        "accessAdd": roleConstant.MASTER_FINANCE_REASON_CREATE,
                        "activeMenu": "reason"
                    },{

                        "icon": "fa-tag",
                        "title": "Document",
                        "submenu": false,
                        "listLink": "layout.documentMaster",
                        "operationLink": "layout.addDocumentMaster",
                        "accessList": roleConstant.MASTER_FINANCE_DOCUMENT_MASTER_LIST, 
                        "accessAdd" : roleConstant.MASTER_FINANCE_DOCUMENT_MASTER_CREATE,
                        "activeMenu": "Document Master"
                    }, {
                        "icon": "fa-tag",
                        "title": "Document Type",
                        "submenu": false,
                        "listLink": "layout.documentTypeMaster",
                        "operationLink": "layout.documentTypeMasterAdd",
                        "accessList": roleConstant.MASTER_FINANCE_DOCUMENT_TYPE_LIST, 
                        "accessAdd" : roleConstant.MASTER_FINANCE_DOCUMENT_TYPE_CREATE,
                        "activeMenu": "documentType"
                    }]

                }, {
                    "icon": "fa-tag",
                    "title": "eOcean",
                    "submenu": true,
                    "activeMenu": "eOcean",
                    "submenudata": [{
                        "icon": "fa-tag",
                        "title": "CFS",
                        "submenu": false,
                        "listLink": "layout.cfsMaster",
                        "operationLink": "layout.cfsMasterAdd",
                        "accessList": roleConstant.MASTER_OCEAN_CFS_LIST, 
                        "accessAdd" : roleConstant.MASTER_OCEAN_CFS_CREATE,
                        "activeMenu": "cfsMaster"
                    }, {
                        "icon": "fa-tag",
                        "title": "Event",
                        "submenu": false,
                        "listLink": "layout.eventMaster",
                        "operationLink": "layout.eventMasterAdd",
                        "accessList": roleConstant.MASTER_OCEAN_EVENT_LIST, 
                        "accessAdd" : roleConstant.MASTER_OCEAN_EVENT_CREATE,
                        "activeMenu": "eventMaster"
                    }, {

                        "icon": "fa-tag",
                        "title": "Reference",
                        "submenu": false,
                        "listLink": "layout.referenceType",
                        "operationLink": "layout.addReferenceType",
                        "accessList": roleConstant.MASTER_OCEAN_REFERENCE_TYPE_LIST, 
                        "accessAdd" : roleConstant.MASTER_OCEAN_REFERENCE_TYPE_CREATE,
                        "activeMenu": "referenceType"
                    }]
                }, {
                    "icon": "fa-tag",
                    "title": "eSales",
                    "submenu": true,
                    "activeMenu": "eSales",
                    "submenudata": []
                }, {
                    "icon": "fa-tag",
                    "title": "eWMS",
                    "submenu": true,
                    "activeMenu": "eWMS",
                    "submenudata": []
                }, {
                    "icon": "fa-tag",
                    "title": "eMove",
                    "submenu": true,
                    "activeMenu": "eMove",
                    "submenudata": []
                }, {
                    "icon": "fa-tag",
                    "title": "eLand",
                    "submenu": true,
                    "activeMenu": "eLand",
                    "submenudata": []
                }]
            }, {
                "icon": "icon-setup",
                "title": "Setup",
                "submenu": true,
                "activeMenu": "Setup",
                "submenudata": [{

                        "icon": "fa-tag",
                        "title": "Roles & Privileges",
                        "submenu": false,
                        "listLink": "layout.rolesPrivilege",
                        "operationLink": "layout.addRolesPrivilege",
                        "activeMenu": "roles",
                        "accessList": roleConstant.SETUP_ROLES_AND_PRIVILEGES_LIST, 
                        "accessAdd": roleConstant.SETUP_ROLES_AND_PRIVILEGES_CREATE
                    }, {

                        "icon": "fa-tag",
                        "title": "Company and Location Setup Wizard",
                        "submenu": false,
                        "listLink": "layout.locationSetupWizard",
                        "operationLink": "layout.locationSetupWizard",
                        "activeMenu": "roles",
                        "accessList": roleConstant.SETUP_ROLES_AND_PRIVILEGES_LIST, 
                        "accessAdd": roleConstant.SETUP_ROLES_AND_PRIVILEGES_CREATE
                    }, {

                        "icon": "fa-tag",
                        "title": "User Creation",
                        "submenu": false,
                        "listLink": "layout.user",
                        "operationLink": "layout.addUser",
                        "activeMenu": "userCreation",
                        "accessList": roleConstant.SETUP_USER_CREATION_LIST, 
                        "accessAdd" : roleConstant.SETUP_USER_CREATION_CREATE
                    }, {

                        "icon": "fa-tag",
                        "title": "Location",
                        "submenu": false,
                        "listLink": "layout.locationMaster",
                        "operationLink": "layout.locationMasterAdd",
                        "activeMenu": "location",
                        "accessList": roleConstant.SETUP_LOCATION_MASTER_LIST, 
                        "accessAdd": roleConstant.SETUP_LOCATION_MASTER_CREATE
                    },
                    {
                        "icon": "fa-tag",
                        "title": "Error Message",
                        "submenu": false,
                        "listLink": "layout.errorMessage",
                        "operationLink": "layout.addErrorMessage",
                        "accessList": roleConstant.SETUP_ERROR_MESSAGE_LIST, 
                        "accessAdd": roleConstant.SETUP_ERROR_MESSAGE_CREATE,
                        "activeMenu": "errorMessage"
                    }, {
                        "icon": "fa-envelope-o",
                        "title": "eMail Template",
                        "submenu": false,
                        "listLink": "layout.emailTemplate",
                        "accessList": roleConstant.SETUP_EMAIL_TEMPLATE_LIST, 
                        "activeMenu": "emailTemplate"
                    },
                   
                    {
                        "icon": "fa-tag",
                        "title": "Country Configuration",
                        "submenu": false,
                        "listLink": "layout.countryDynamicField",
                        "operationLink": "layout.addCountryDynamicField",
                        "accessList": roleConstant.SETUP_COUNTRY_CONFIGURATION_LIST, 
                        "accessAdd": roleConstant.SETUP_COUNTRY_CONFIGURATION_CREATE,
                        "activeMenu": "countryConfiguration"
                    },
                    {

                        "icon": "fa-tag",
                        "title": "Document Issue Restriction",
                        "submenu": false,
                        "listLink": "layout.docIssueRestriction",
                        "operationLink": "layout.addDocIssueRestriction",
                        "accessList": roleConstant.SETUP_DOCUMENT_ISSUE_RESTRICTION_LIST, 
                        "accessAdd": roleConstant.SETUP_DOCUMENT_ISSUE_RESTRICTION_CREATE,
                        "activeMenu": "docIssueRestriction"
                    },
                    {
                        "icon": "fa-tag",
                        "title": "App Configuration",
                        "submenu": false,
                        "listLink": "layout.appConfiguration",
                        "accessList": roleConstant.SETUP_APP_CONFIGURATION_LIST, 
                        "activeMenu": "appConfiguration"
                    }, {

                        "icon": "fa-tag",
                        "title": "Logo",
                        "submenu": false,
                        "listLink": "layout.logo",
                        "operationLink": "layout.addLogo",
                        "accessList": roleConstant.SETUP_LOGO_MASTER_LIST, 
                        "accessAdd" : roleConstant.SETUP_LOGO_MASTER_CREATE,
                        "activeMenu": "logo"
                    }, {

                        "icon": "fa-tag",
                        "title": "Report Configuration",
                        "submenu": false,
                        "listLink": "layout.reportMaster",
                        "operationLink": "layout.addReportMaster",
                        "accessList": roleConstant.SETUP_REPORT_CONFIGURATION_LIST,
                        "accessAdd":  roleConstant.SETUP_REPORT_CONFIGURATION_REPORT_MASTER_CREATE,
                        "activeMenu": "Report Master"
                    },
                    {
                        "icon": "fa-tag",
                        "title": "Record Access Level",
                        "submenu": false,
                        "listLink": "layout.RecordAccessLevel",
                        "operationLink": "layout.addRecordAccessLevel",
                        "activeMenu": "RecordAccessLevel",
                        "accessList": roleConstant.SETUP_RECORD_ACCESS_LEVEL_LIST, 
                        "accessAdd": roleConstant.SETUP_RECORD_ACCESS_LEVEL_LIST,
                    }
                    
                    
                    
                ]
            }
        ];
    };
}]);