/**
 * Created by saravanan on 29/3/16.
 */
 app.directive("sidebarMenu",['$compile', function($compile){
    return {
        restrict: 'E',
        transclude:true,
        templateUrl :'app/shared/sidebar/sidebar.html',
        link: function(scope, iElement, iAttrs) {
        	scope.$watch(iAttrs.menuState, function(newValue, oldValue) {
                var shown = !!newValue;
                if (shown !== !!oldValue) {
                    console.log("menustate");
                    iElement.animate({
                        width: shown ? '+=175px' : '-=175px'
                    },350,function(){
                        var newElem;
                        if(shown) {
                            newElem = angular.element('<div id="side-bar-backdrop" ng-click="closeMenu()"></div>').fadeIn("slow");
                            iElement.after(newElem);
                            $compile(newElem)(scope);
                            //iElement.after("<div id=\"side-bar-backdrop\"></div>");
                        }else{
                            $("#side-bar-backdrop").fadeOut("slow",function(){
                                $(this).remove();
                            });
                        }
                    });
                }
            });

            scope.$watch(iAttrs.subOpened,function(newValue, oldValue) {
                var shown = !!newValue;
                if (shown !== !!oldValue) {
                    var newElem;
                    if(shown) {
                        newElem = angular.element('<div id="side-bar-backdrop" ng-click="closeMenu()"></div>').fadeIn("slow");
                        iElement.after(newElem);
                        $compile(newElem)(scope);
                        //iElement.after("<div id=\"side-bar-backdrop\"></div>");
                    }else{
                        $("#side-bar-backdrop").fadeOut("slow",function(){
                            $(this).remove();
                        });
                    }
                }
            });
        }
    }
}])