/**
 * Created by saravanan on 29/3/16.
 */
app.controller("subMenuCtrl",["$scope","$location",function($scope,$location){
    $scope.closeSubMenu = function(){
        $scope.menuData.opened = false;
        $scope.$emit("submenudata",{menuData:$scope.menudata});
    }

    $scope.listSubmenu = function(link){
        $scope.$emit("closeAllMenu");
        $location.path("/"+link+"_master");
    }
    $scope.addSubmenu = function(link){
        $scope.$emit("closeAllMenu");
        $location.path("/"+link+"_operation");
    }

}]);
