/**
 * Created by saravanan on 29/3/16.
 */
app.directive("subMenu",['$compile', function($compile){
    return {
        restrict: 'E',
        replace:false,
        templateUrl :'app/shared/sidebar/submenu/submenu.html',
        link: function(scope, iElement, iAttrs) {
            scope.$on('submenudata', function (event, args) {
                var menuData = args.menuData;
                scope.menuData = args.menuData;
                console.log("submenu",scope.menuData.menu);
                iElement.find(".sub-main-menu").animate({
                    left: menuData.opened ? '+=231px' : '-=231px'
                },350);
            });

        }
    }
}])