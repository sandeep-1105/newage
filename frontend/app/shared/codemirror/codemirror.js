app.directive('forDirect',[function(){
    return{
        restrict:'EA',
        scope:{
            dataup:'&',
            getval:'=',
            mode:'='
        },
        link:function(scope,elem,attr,ctrl){

            scope.editorOptions = {
                lineNumbers: true,
                mode: "mode"
            };
        },

        template:
        '<div class="airline-edi">'+
        '<label>Generated EDI Content</label>'+
        '<textarea class="bodyof" ui-codemirror ui-codemirror-opts="editorOptions" ng-model="getval" contenteditable="true"></textarea>'+

        '</div>'
    }
}]);