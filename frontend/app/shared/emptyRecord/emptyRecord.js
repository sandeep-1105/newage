/**
 * Created by saravanan on 21/9/16.
 */
app.directive('emptyRecord',[function(){
    return {
        restrict:'EA',
        replace:true,
        scope:{},
        template: function(elem,attr){
            return '<div class="emptypage_box">'+
            '<i class="emptypage_icon fa fa-file-text-o fa-5x"></i>'+
            '<p class="head">{{text}}</p>'+
            '</div>'
        },
        link:function(scope,elem,attr){
         scope.text = attr.text;
        }

    }
}]);