/**
 * Created by saravanan on 4/4/16.
 */
app.directive("menuHeight",['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            //var docHt = $(window).height()-220;
            var docHt = $(window).height()-72;
            $timeout(function(){
                iElement.css({
                    "min-height":docHt+"px",
                    "height":docHt+"px"
                });
            },0);
                $(window).bind('resize', function(){
                var docHt = $(window).height()-72;
                iElement.css({
                    "min-height":docHt+"px",
                    "height":docHt+"px"
                });
            });
        }
    }
}]);