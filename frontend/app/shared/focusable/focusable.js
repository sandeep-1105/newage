/**
 * Created by Tanveer.
 */
app.directive('focusable',['$timeout', '$parse', function($timeout, $parse) {

    return {
        link: function(scope, element, attrs) {

            var model = $parse(attrs.setFocus);
            scope.$watch(model, function(value) {
                console.log(angular.element(element[0]) + ' got focus');
                console.log("focusable.....................................................");
                if (value == true) {
                    $timeout(function() {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function() {
                // console.log('blur')
                //  scope.$apply(model.assign(scope, false));
            })
        }
    };
}]);