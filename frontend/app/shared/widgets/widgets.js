/**
 * Created by saravanan on 5/4/16.
 */
app.directive("widgets",['$timeout','ngProgressFactory','$window','$rootScope',
    function($timeout,ngProgressFactory,$window,$rootScope){
    return {
        restrict: 'E',
        scope:{
            "fieldData":"="
        },
        replace:true,
        template: '<div class="dynamic-field" ng-include="getTemplateUrl()"></div>',
        link:function(scope,elem,attr){
            scope.getTemplateUrl = function() {
                var type = scope.fieldData.type || 'text';
                return 'app/shared/widgets/views/'+type+'.html';
            }
        }
    }
}]);