/**
 * Created by saravanan on 10/4/16.
 */
app.directive("subbarMenu",[ function(){
    return {
        restrict: 'E',
        transclude:true,
        templateUrl :'app/shared/submenubar/submenubar.html'
    }
}])