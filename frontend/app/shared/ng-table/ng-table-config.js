(function() {
	"use strict";

	app.run(configureDefaults);
	configureDefaults.$inject = [ "ngTableDefaults" ];

	function configureDefaults(ngTableDefaults) {
		ngTableDefaults.params.count = 5;
		ngTableDefaults.settings.counts = [ ];
	}
})();