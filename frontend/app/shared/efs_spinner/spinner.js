/**
 * Created by hmspl on 21/7/16.
 */
app.directive('efsSpinner',[function(){
    return{
        restrict:'E',
        scope:{
            spinnerStatus:"="
        },
        template:'<div class="abs loader-container opc-8 bg-white ng-hide" ng-show="spinnerStatus">' +
                      '<img src="../../../assets/images/loader.gif" class="img-loader"/>'+
                  '</div>'
    }
}]).directive('modalSpinner',[function(){
    return{
        restrict:'E',
        scope:{
            spinnerStatus:"="
        },
        template:'<div class="abs loader-container mainefs-overlay" ng-show="spinnerStatus">' +
        '<div class="sprite_hmblue img-loader">'+
        '</div>'+
        '</div>'
    }
}]).directive('formSpinner',[function(){
        return{
            restrict:'E',
            scope:{
                spinnerStatus:"="
            },
            template:'<div class="abs loader-container mainefs-overlay" ng-show="spinnerStatus">' +
            '<div class="sprite_hmblue img-loader">'+
            '</div>'+
            '</div>'
        }
}]).directive('attachSpinner',[function(){
    return{
        restrict:'E',
        scope:{
            spinnerStatus:"="
        },
        template:'<div class="abs loader-container mainefs-overlay" ng-show="spinnerStatus">' +
        '<div class="sprite_hmblue img-loader">'+
        '</div>'+
        '</div>'
    }
}])