
        function isCharNumKey(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;

            var keychar = String.fromCharCode(key);
            var keycheck = /[a-z,A-Z,0-9]|\+/;

            if (!(key == 8 || key == 27 || key == 46 || key == 9 || key == 32)) {
                if (!keycheck.test(keychar)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault)
                        theEvent.preventDefault();

                }
            }
        }
    