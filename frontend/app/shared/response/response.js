
app.directive("showResponse", ['$timeout', '$rootScope', function ($timeout, $rootScope) {
	return {
		templateUrl: 'app/shared/response/response.html',
		replace: true,
		restrict: 'AE',
		link: function (scope, elem, attr) {
			scope.$watch('respCode', function (newValue, oldValue) {
				if (newValue != oldValue) {
					// if parameters are there in the server response, then replace the %s in respDesc message
					if ($rootScope.respDesc != "" 
							&& $rootScope.respParams != undefined 
							&& $rootScope.respParams != null 
							&& $rootScope.respParams.length > 0) {
						for (var i = 0; i < $rootScope.respParams.length; i++) {
							if ($rootScope.respDesc != undefined && $rootScope.respDesc != null) {
								$rootScope.respDesc = $rootScope.respDesc.replace("%s", $rootScope.respParams[i]);
							}
						}
					}
				}

			})

		}

	};
}]);



