

app.directive("highlight",[ function() {
	return function($scope, element, attr) {
		
		element.bind("mouseenter", function() {
			element.addClass("hover-row");
		});
		
		element.bind("mouseleave", function() {
			element.removeClass("hover-row");
		});
	};
}]);