/**
 * Created by saravanan on 5/4/16.
 */
app.directive("selectListPicker",['$timeout','ngProgressFactory','$filter' ,'$rootScope',
    function($timeout,ngProgressFactory,$filter ,$rootScope){
    return {
        restrict: 'E',
        scope:{
            "panelTitle":"=",
            "data":"=",
            "selected":"=",
            "cancelEvent":"&",
            "clickEvent":"&",
            "ajaxEvent":"&",
            "routeEvent":"&",
            "config":"=",
            "pickerShow":"="
        },

        templateUrl :'app/shared/selectListPicker/selectListPicker.html',
        link: function (scope, iElement, iAttrs) {
            //var docHt = $(window).height()-220;
            scope.indexArr = [];
            if(!scope.config.hasOwnProperty('showCode') && !scope.config.hasOwnProperty('address')){
                var tempData = scope.data;
                scope.data = [];
                angular.forEach(tempData,function(item){
                    if(iAttrs.filterType){
                        var filterItem = $filter(iAttrs.filterType)(item);
                        var tempObj = {
                            key:item,
                            value:filterItem
                        }
                        scope.filterName = iAttrs.filterType;
                        scope.indexArr.push(tempObj);
                        scope.data.push(filterItem)
                    }else{
                        scope.filterName ="";
                        scope.data.push(item);
                    }
                })
            }

            $timeout(function() {
                var docHt = $   (window).height() - 182;
                iElement.find('.panel-body').css({
                    "min-height": docHt + 40 + "px",
                    "height": docHt + 40 + "px"
                });

            },0);
            //scope.$watch('pickerShow',function(newvalue,oldvalue){
            //    if(newvalue){
            //        $timeout(function() {
            //            iElement.find('.panel-body .picker-search-section input').focus();
            //        },0);
            //    }
            //    $timeout(function(){
            //        scope.searchText = '';
            //    },1000);
            //});
            $(window).bind('resize', function () {
                var docHt = $(window).height() - 182;
                iElement.find('.panel-body').css({
                    "min-height":docHt+40+"px",
                    "height":docHt+40+"px"
                });
            });
        },
        controller:function($scope){
            $scope.contained_progressbar = ngProgressFactory.createInstance();
            $scope.contained_progressbar.setParent(document.getElementById('selectList-Panel'));
            $scope.contained_progressbar.setAbsolute();
            $scope.contained_progressbar.start();
            $scope.$watch('data',function(newvalue,oldvalue){
            	console.log("New Value : ", newvalue);
                $scope.selectedAll = null;
                //if(newvalue!=oldvalue) {
                    $timeout(function () {
                        $scope.contained_progressbar.complete();
                        $scope.ajaxBusy = false;
                    }, 350);
                //}
            });

            $scope.watchText = function(data){
                if($scope.config && $scope.config.ajax){
                    if($scope.ajaxEvent){
                        $scope.ajaxBusy = true;
                        $scope.ajaxEvent({"param":data});

                    }
                }
            }



            $scope.selectList = function(data){
                $scope.data = [];
                $scope.searchText = '';
                $scope.clickEvent({"param":data});

            }

            $scope.addNew = function(){
                if($scope.routeEvent) {
                    $scope.routeEvent();
                }
            }
            $scope.cancelList = function(){
                //$scope.data = [];
                $scope.searchText = '';
                $scope.cancelEvent();
            }

            $scope.rowSelect = function(data,index){
                //if($scope.filterName){
                //    $scope.clickEvent({"param":data});
                //}else{
                //    $scope.clickEvent({"param":data});
                //}

                if(!$scope.config.multiple) {
                    $scope.selected = -1;
                    $scope.selected = index;
                    if($scope.filterName){
                        angular.forEach($scope.indexArr,function(item){
                            console.log("item",item,data);
                            if(item.value==data){
                                $scope.data = [];
                                $scope.searchText = '';
                                $scope.clickEvent({"param": item.key});
                            }
                        });
                    }else{
                        $scope.data = [];
                        $scope.searchText = '';
                        $scope.clickEvent({"param": data});
                    }


                }
            }
            $scope.rowSelectAddress = function(data,index){
                $scope.selected = -1;
                $scope.selected = index;
                //$scope.data = [];
                $scope.searchText = '';
                $scope.clickEvent({"param":data});
            }
            $scope.goToOnSpace = function (e, data,index) {

                    console.log("e",e.keyCode);
                   if (e.keyCode === 32 || e.charCode === 32) {
                       // Code that checks additional Parameter to go to requested link
                       $scope.selected = -1;
                       $scope.selected = index;
                       $scope.clickEvent({"param": data});
                   }

            };

            $scope.checkAll = function () {

                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }
                angular.forEach($scope.data, function (dataObj) {
                    dataObj.Selected = $scope.selectedAll;
                });

            };



            
            $scope.findProp = function(obj, prop, defval){
                if (typeof defval == 'undefined') defval = null;
                prop = prop.split('.');
                for (var i = 0; i < prop.length; i++) {
                    try {
                        if (typeof obj[prop[i]] == 'undefined')
                            return defval;
                        obj = obj[prop[i]];
                    }catch(e){
                        return null;
                    }
                }
                return obj;
            }

            $scope.fetchAll = function(){
                var tempArr = [];
                angular.forEach($scope.data, function (dataObj) {
                    if(dataObj.Selected && dataObj.Selected == true){
                        tempArr.push(dataObj);
                    }
                });
				
                console.log("tempArr",tempArr);
                $scope.clickEvent({"param":tempArr});
                $scope.searchText = '';
            };
            $scope.close = $rootScope.close;

        }
    }
}]);