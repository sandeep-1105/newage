app.directive('multiselButtongroup', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            states: '=',
            state: '=',
            btnIndex: '@',
            onStateChange: '&'
        },
        template: '<div class="btn-group btn-group-justified">' +
            '<div class="btn-group" role="group" ng-repeat="s in tempList">' +
            '<button value="{{s.value}}" tabindex="{{btnIndex}}" type="button" class="btn fsl-btn-group" data-state={{s.key}} ng-click="select(s, $event)">' +
            '{{s.value}}' +
            '</button>' +
            '</div>' +
            '</div>',
        link: function (scope, element, attr) {
            scope.tempList = [];
//            console.log("item",scope.states);
            angular.forEach(scope.states, function (item) {

                if (attr.filterType) {
                    var filterItem = $filter(attr.filterType)(item);
                    var tempObj = {
                        "key": item,
                        "value": filterItem
                    }
                    scope.tempList.push(tempObj);
                } else {
                    var tempObj = {
                        "key": item,
                        "value": item
                    }
                    scope.tempList.push(tempObj);
                }


            })
        },
        controller: function ($scope, $element) {

            // Make sure that style is applied to initial state value
            $scope.$watch(function () {
                return $($element).find('.btn').length; // it checks if the buttons are added to the DOM
            }, function (newVal) {
                // it applies the selected style to the currently defined state, if any

                if (newVal > 0) {
                    $($element).find('.btn').each(function (index, elm) {
//                        console.log("elm file",elm);
                        if ($(elm).data('state') == $scope.state) $(elm).addClass('active');
                    });
                }
            }, true);


            // Apply style changes according to selection
            $scope.select = function (s, evt) {
                if (s) {
                    if ($scope.state.indexOf(s.key) == -1 && s.key != 'ALL') {
                        $('.btn').filter(function(){return this.value=='ALL'}).removeClass('active');
                        $scope.state.push(s.key);
                    }else{
                        angular.forEach($scope.state, function (item, index) {
                            if (item == s.key) {
                                $scope.state.splice(index, 1)
                            }
                        });
                    }

                    if (s.key == 'ALL'){
                        if($('.btn').filter(function(){return this.value=='ALL'}).hasClass('active')){
                            $scope.state = [];
                        }else{
                            $scope.state = ['ALL'];
                            $element.find('.btn').removeClass('active');
                        }
                    }else{
                        angular.forEach($scope.state, function (item, index) {
                            if (item == 'ALL') {
                                $scope.state.splice(index, 1)
                            }
                        });
                    }
                    if ($(evt.target).hasClass('active')) {
                        $(evt.target).removeClass('active');
                    } else {
                        $(evt.target).addClass('active');
                    }
                    console.log($scope.state)
                }
            };

            // Execute callback if it was provided
            $scope.$watch('state', function (newValue, oldValue) {
                if (newValue != oldValue) {
                    if (newValue != null) {
                        if ($scope.onStateChange) {
                            $scope.onStateChange();
                        }
                    } else {
                        $scope.select(newValue);
                    }
                }
            }, true);
        }
    };
});