/**
 * 
 */
app.service('ValidateUtil',['$rootScope','$injector','appConstant', function($rootScope,$injector,appConstant) {
	
	
    this.isStatusBlocked = function(status) { 
    	if(status=="BLOCK" || status=="Block")
    		return true;
    	else
    		return false;
    };
    
    this.isStatusHidden = function(status) { 
    	if(status=="HIDE" || status=="Hide")
    		return true;
    	else
    		return false;
    };
    
    this.isEmployeeResigned = function(employmentStatus) { 
    	if(employmentStatus=="RESIGNED")
    		return true;
    	else
    		return false;
    };
    
    this.isEmployeeTerminated = function(employmentStatus) { 
    	if(employmentStatus=="TERMINATED")
    		return true;
    	else
    		return false;
    };
    
    this.isEmployeeSalesman = function(status) { 
    	if(status || status=="YES" || status=="Yes")
    		return true;
    	else
    		return false;
    };
    
    this.isContactPerson = function(contactPerson) { 
    	 var regex = /^[a-zA-Z. ]{0,100}$/;
    	return regex.test(contactPerson)
    };

    
   this.isTSAExpired = function(party,partyAddressType) { 

    	if(party!=undefined && party!=null && party!="" && party.id!=undefined && partyAddressType!=undefined && 
    			partyAddressType!=null && party.partyAddressList!=undefined && party.partyAddressList!=null 
    			&&party.partyAddressList.length>=1){
             for(var i=0;i<party.partyAddressList.length;i++){
            	 if(party.partyAddressList[i].addressType==partyAddressType){
                       if(party.partyAddressList[i].partyCountryField!=undefined && 
                    		   party.partyAddressList[i].partyCountryField!=null &&
                    		   party.partyAddressList[i].partyCountryField.knownShipperValidationNo!=undefined 
                    		   &&party.partyAddressList[i].partyCountryField.knownShipperValidationNo!=null&&
                    		   party.partyAddressList[i].partyCountryField.knownShipperValidationDate!=undefined && 
                    		   party.partyAddressList[i].partyCountryField.knownShipperValidationDate!=null){
                    	   var knownDate =party.partyAddressList[i].partyCountryField.knownShipperValidationDate;
                    	   var todayDate =$rootScope.sendApiStartDateTime($rootScope.dateToString(new Date()));
                    		 if (knownDate < todayDate) {
                                 return true;
                             }else{
                            	 return false;
                             }
            	 }
                       break;
    		}
    	}	
    	}else{
    		return false;
    	}
    	
    };
    
    
    this.isNotNull=function(prop){
    	
    	if(prop==undefined || prop ==null || prop ==""){
    		return true;
    	}else{
    		return false;
    	}

    }
    
    
    
    this.isValidMobile = function(obj,id,err,index,tab) {
    
    	if(obj!=undefined && obj!=null){

        	if(!$injector.get('CommonValidationService').checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,obj)){
        		return this.validationResponse(true,id, $rootScope.nls[err],index,tab, undefined);
        	}
        	 return this.validationSuccesResponse(); 	
    		
    	}
    	
    	
    }
    
    
  
    this.isValidMailNew = function(obj,id,err,index,tab) {
    	
    	if(obj!=undefined && obj!=null){
    		
    		var validMail=true;
       	 var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
       	 console.log($rootScope.appMasterData['single.text.split.by.multiple.email']);
            var emailArray = (obj).split($rootScope.appMasterData['single.text.split.by.multiple.email']);
            emailArray.forEach(function(email) {
           	 if(!regexp.test(email)){
           		 validMail=false;
           		 return;
           	 }
            });
            if(validMail){
           	 return this.validationSuccesResponse(); 
            }else{
           	 return this.validationResponse(true,id,$rootScope.nls[err],index,tab, undefined);
            }	
    		
    	}
    	
     };
    
    
    
    
    
     this.validationSuccesResponse = function() {
 	    return {error : false};
 	}
 	
 	this.validationResponse = function(err, elem, message, idx, tabName, accordian) {
 		return {error : err, errElement : elem, errMessage : message, serviceIdx : idx, tab : tabName, accordian: accordian};
 	}
    
    
    
    
    
    
    
    this.isValidMail = function(emailid) {
    	if(emailid!=undefined && emailid!=null && emailid!=""){
    		var validMail=true;
       	 var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
       	 console.log($rootScope.appMasterData['single.text.split.by.multiple.email']);
            var emailArray = (emailid).split($rootScope.appMasterData['single.text.split.by.multiple.email']);
            emailArray.forEach(function(email) {
           	 console.log("valid mail test"+regexp.test(email));
           	 if(regexp.test(email)){
           		 validMail=false;
           		 return;
           	 }
            });
           return validMail;	
    		
    	}else{
    		return false;
    	}
    	
     };
   
    
    
    
    
    
    
}]);