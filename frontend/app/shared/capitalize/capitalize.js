app.directive('capitalize',[ function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var caretPos,
            capitalize = function (inputValue) {
                caretPos = element[0].selectionStart; // save current caret position
                
                //console.log('curPos before change', caretPos);
                if (inputValue == undefined) 
                  inputValue = '';

              var capitalized = inputValue.toUpperCase();
                //console.log(capitalized, inputValue);
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                    element[0].selectionStart = caretPos; // restore position
                    element[0].selectionEnd = caretPos;
                }
                return capitalized;
            };
            modelCtrl.$parsers.push(capitalize);

            capitalize(scope[attrs.ngModel]); // capitalize initial value
        }
    };
}]);