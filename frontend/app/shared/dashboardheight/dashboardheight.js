/**
 * Created by saravanan on 4/4/16.
 */
app.directive("dashboardHeight",['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            //var docHt = $(window).height()-220;
            var docHt = $(window).height()-100;
            $timeout(function(){
                iElement.css({
                    "min-height":docHt+"px",
                    "height":docHt+"px",
                    "overflow":"hidden"
                });
            },1);

            //$(document).on('touchmove',function(e){
            //    console.log("e", e.target);
            //});
            $(window).bind('resize', function(){
                var docHt = $(window).height()-100;

                iElement.css({
                    "min-height":docHt+"px",
                    "height":docHt+"px",
                    "overflow":"hidden"
                });
            });
        }
    }
}]);