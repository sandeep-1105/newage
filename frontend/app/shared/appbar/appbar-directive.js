/**
 * Created by saravanan on 29/3/16.
 */
app.directive("appBar",[ function(){
    return {
        restrict: 'E',
        templateUrl :'app/shared/appbar/appbar.html'
    }
}])