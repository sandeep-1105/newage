/**
 * Created by saravanan on 31/7/16.
 */
app.directive("autoComplete",['$parse', '$timeout', '$q', '$rootScope', '$interpolate', function($parse, $timeout, $q, $rootScope, $interpolate) {
        return {
            restrict: 'A',
            scope: {
                callFun: "&",
                ngModel: "=",
                renderItem: "=",
                onSelect: "&",
                availCheck: "=?"

            },
            require: "ngModel",
            link: function(scope, elem, attr, ctrl) {
                if (!ctrl) {
                    return;
                }
                scope.$watch(function() {
                        try {
                            return $interpolate($(elem).attr('disabled'))(scope); //this will evaluate attribute value `{{}}``
                        } catch (e) {}
                    },
                    function(val) {
                        if (val == "disabled") {
                            if (!$(elem).next(".select-arrow").hasClass("cursor-disable")) {
                                $(elem).next(".select-arrow").addClass("cursor-disable");
                            }
                            $(elem).next(".select-arrow").off('click');

                        } else {
                            if ($(elem).next(".select-arrow").hasClass("cursor-disable")) {
                                $(elem).next(".select-arrow").removeClass("cursor-disable");
                            }
                            $(elem).next(".select-arrow").on('click', arrowClick);
                        }
                    });

                if ($(elem).attr('ng-disabled') != undefined) {
                    $(elem).after("<span class=\"select-arrow cursor-disable\"><b></b></span>");
                } else {
                    $(elem).after("<span class=\"select-arrow\"><b></b></span>");
                }

                if ($(elem).attr('ng-disabled') == undefined || $(elem).attr('ng-disabled') == false) {
                    $(elem).next(".select-arrow").on("click", arrowClick);
                }
                var autoCompOption = $parse(attr.autoCompleteOption);
                var showArror = true;
                if (angular.isDefined(autoCompOption)) {
                    try {
                        showArror = scope.$eval(autoCompOption).arrow;
                    } catch (e) {
                        showArror = true;
                    }
                }
                if (angular.isDefined(autoCompOption) && showArror == false) {
                    $(elem).next(".select-arrow").hide();
                }

                function arrowClick(event) {
                    var autocompleteBox = $(elem).autocomplete('widget');
                    if (autocompleteBox.is(':hidden')) {
                        $(elem).autocomplete('search', '').focus();
                    } else {
                        autocompleteBox.hide();
                    }

                }
                $timeout(function() {
                    scope.$watch('ngModel', function(newValue, oldValue) {
                        if (typeof newValue == "object") {
                            try {
                                elem.val(scope.findProp(newValue, attr.selectedValue));
                            } catch (e) {

                            }
                        }
                        if (typeof newValue == "string" && typeof oldValue == "object") {
                            try {
                                elem.val(scope.findProp(oldValue, attr.selectedValue));
                            } catch (e) {

                            }
                        }
                        if (newValue == "") {
                            try {
                                ctrl.$setViewValue(null);
                            } catch (e) {


                            }
                        }

                    });
                }, 3000);

                ctrl.$render = function() {
                    if (ctrl.$modelValue) {
                        if (attr.selectedValue) {
                            elem.val(scope.findProp(ctrl.$modelValue, attr.selectedValue));
                        } else {
                            throw new Error("Expected Selected Value");
                        }

                    } else {
                        elem.val("");
                    }

                };

                function tabEventHandle(type) {
                    if (ctrl.$dirty) {
                        var textValue = $(elem).val().toLowerCase();
                        var valid = false;
                        var selectedVal;
                        if (textValue) {
                            try {
                                angular.forEach(scope.filterArr, function(item, index) {
                                    if (textValue == item.label.toLowerCase()) {
                                        selectedVal = item;
                                        valid = true;
                                        return;
                                    }
                                });
                                if (!valid) {
                                    if (attr.availCheck) {
                                        var currentVal = $(elem).val();
                                        ctrl.$setViewValue(currentVal);
                                        if (type == "tab") {
                                            $timeout(function() {
                                                if (scope.onSelect) {
                                                    scope.onSelect({
                                                        param: currentVal
                                                    });
                                                }
                                            }, 3)
                                        }
                                    } else {
                                        $(elem).val("");
                                        ctrl.$setViewValue(null);
                                        ctrl.$render();
                                        if (type == "tab") {
                                            $timeout(function() {
                                                if (scope.onSelect) {
                                                    scope.onSelect({
                                                        param: null
                                                    });
                                                }
                                            }, 3)
                                        }
                                    }
                                } else {
                                    $(elem).val(selectedVal.label);
                                    ctrl.$setViewValue(selectedVal.item);
                                    ctrl.$render();
                                    if (type == "tab") {
                                        $timeout(function() {
                                            if (scope.onSelect) {
                                                scope.onSelect({
                                                    param: selectedVal.item
                                                });
                                            }
                                        }, 3)
                                    }
                                }
                            } catch (e) {}
                        } else {
                            $timeout(function() {
                                ctrl.$setViewValue(null);
                                //ctrl.$render();
                                if (type == "tab") {
                                    $timeout(function() {
                                        if (scope.onSelect) {
                                            scope.onSelect({
                                                param: null
                                            });
                                        }
                                    }, 2)
                                }
                            }, 2);

                        }

                    }

                }

                $(elem).on("keydown", function(event) {
                    if (event.keyCode === $.ui.keyCode.TAB && !event.shiftKey) {
                        tabEventHandle('tab');
                        if ($(elem).autocomplete("instance").menu.active) {
                            event.preventDefault();
                        }
                    } else if (event.keyCode === $.ui.keyCode.TAB && event.shiftKey) {
                        tabEventHandle('shiftTab');
                    }
                });

                function widgetHide() {
                    try {
                        var autocompleteBox = $(elem).autocomplete('widget');
                        if (!autocompleteBox.is(':hidden')) {
                            autocompleteBox.hide();
                        }
                    } catch (e) {

                    }
                }
                $(window).resize(function() {
                    widgetHide();
                });
                $rootScope.$on('scrolling', function(event, args) {
                    widgetHide();
                });

                scope.filterArr = [];
                if (angular.isDefined(attr.onAdd)) {
                    var addFn = $parse(attr.onAdd);
                }

                scope.findProp = function(obj, prop, defval) {

                    if (typeof defval == 'undefined') defval = null;
                    prop = prop.split('.');
                    for (var i = 0; i < prop.length; i++) {
                        //console.log("value",obj[prop[i]],typeof obj[prop[i]],prop[i]);
                        try {
                            if (obj[prop[i]] == null && typeof obj[prop[i]] == 'undefined')
                                return defval;
                            obj = obj[prop[i]];
                        } catch (e) {
                            return null;
                        }

                    }
                    return obj;
                };
                $timeout(function() {
                    $(elem).autocomplete({
                        minLength: 0,
                        delay: 500,
                        open: function(event, ui) {
                            var elemWidth = $(elem).width();
                            if (elemWidth < 150) {
                                elemWidth = 150
                            }
                            $(elem).autocomplete("widget").css({
                                "width": elemWidth + 17 + "px"
                            });
                        },
                        source: function(request, response) {
                            try {
                                if (scope.callFun) {
                                    scope.callFun({
                                        param: request.term
                                    }).then(function(data) {
                                        if (data == null || data.legnth <= 0) {
                                            data = [];
                                        }
                                        var resultArr = data;
                                        if (resultArr.length > 0) {
                                            var finalArr = $.map(resultArr, function(item) {
                                                if (attr.forDynamic && attr.forDynamic == "true") {
                                                    return {
                                                        label: scope.findProp(item, attr.selectedValue),
                                                        item: item
                                                    };
                                                } else {
                                                    return scope.renderItem(item);
                                                }
                                            });
                                            scope.filterArr = angular.copy(finalArr);
                                            if (angular.isDefined(attr.onAdd)) {
                                                if (finalArr[0].val != -2) {
                                                    finalArr.unshift({
                                                        label: '+ Add New',
                                                        val: -2
                                                    });
                                                }
                                            } else {
                                                if (finalArr[0].val == -2) {
                                                    finalArr.shift();
                                                }
                                            }
                                            response(finalArr);
                                        } else {
                                            response([{
                                                label: 'No Records Found',
                                                val: -1
                                            }]);
                                        }
                                    }, function(err) {
                                        response([{
                                            label: 'No Records Found',
                                            val: -1
                                        }]);
                                    });
                                }
                            } catch (e) {
                                response([{
                                    label: 'No Records Found',
                                    val: -1
                                }]);
                            }

                        },
                        close: function() {
                            scope.searchAllow = false;
                            if (elem.val() != "") {
                                console.log("auto closed");
                                tabEventHandle('tab');
                            }

                        },
                        focus: function(event, ui) {
                            if (ui.item.val == -1 || ui.item.val == -2) {
                                return false
                            }
                            // prevent value inserted on focus
                            //return false;
                        },
                        select: function(event, ui) {
                            if (ui.item.val == -1 || ui.item.val == -2) {
                                if (ui.item.val == -2) {
                                    addFn(scope.$parent);
                                    //                                    ctrl.$setViewValue(undefined);
                                    //                                    ctrl.$render();
                                    //                                    elem.val("");

                                }

                                return false;
                            } else {
                                ctrl.$setViewValue(ui.item.item);
                                ctrl.$render();
                                //                            $timeout(function(){
                                //                                //elem.val(scope.findProp(ui.item.item,attr.selectedValue));
                                //                                if(scope.onSelect){
                                //                                    scope.onSelect({param:ui.item.item});
                                //                                }
                                //                            },3);
                            }
                        }
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        if (item.val != -1 && item.val != -2) {
                            if (item.item.status == "Block") {
                                return $("<li></li>").data("ui-autocomplete-item", item)
                                    .append("<a style=\"color:#A94442\">" + item.label + "</a>")
                                    .appendTo(ul);
                            } else {
                                return $("<li></li>").data("ui-autocomplete-item", item)
                                    .append(item.label)
                                    .appendTo(ul);
                            }
                        } else {
                            return $("<li></li>").data("ui-autocomplete-item", item)
                                .append(item.label)
                                .appendTo(ul);
                        }

                    };
                }, 0);

            }
        }

    }])
    .directive("autoCompleteManual",['$parse', '$timeout', '$filter', '$rootScope', function($parse, $timeout, $filter, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                itemData: "=",
                ngModel: "=",
                renderItem: "=",
                onSelect: "&",
                availCheck: "=?"

            },
            require: "ngModel",
            link: function(scope, elem, attr, ctrl) {
                if (!ctrl) {
                    return;
                }
                elem.after("<span class=\"select-arrow\"><b></b></span>");
                ctrl.$render = function() {
                    if (ctrl.$modelValue) {
                        elem.val(ctrl.$modelValue);
                    } else {
                        elem.val("");
                    }

                };
                $(window).resize(function() {
                    try {
                        var autocompleteBox = $(elem).autocomplete('widget');
                        if (!autocompleteBox.is(':hidden')) {
                            autocompleteBox.hide();
                        }
                    } catch (e) {

                    }
                });
                scope.$watch('ngModel', function(newValue, oldValue) {
                    if (newValue != oldValue && newValue != null) {
                        elem.val(newValue);
                    }
                    if (newValue == "") {
                        try {
                            ctrl.$setViewValue(null);
                        } catch (e) {


                        }
                    }
                });

                function tabEventHandle(type) {
                    if (ctrl.$dirty) {
                        var textValue = $(elem).val().toLowerCase();
                        var valid = false;
                        var selectedVal;
                        if (textValue) {
                            try {
                                angular.forEach(scope.filterArr, function(item, index) {
                                    if (textValue == item.label.toLowerCase()) {
                                        selectedVal = item;
                                        valid = true;
                                        return;
                                    }
                                });
                                if (!valid) {
                                    if (attr.availCheck) {
                                        var currentVal = $(elem).val();
                                        ctrl.$setViewValue(currentVal);
                                        if (type == "tab") {
                                            $timeout(function() {
                                                if (scope.onSelect) {
                                                    scope.onSelect({
                                                        param: currentVal
                                                    });
                                                }
                                            }, 3)
                                        }
                                    } else {
                                        $(elem).val("");
                                        ctrl.$setViewValue(null);
                                        ctrl.$render();
                                        if (type == "tab") {
                                            $timeout(function() {
                                                if (scope.onSelect) {
                                                    scope.onSelect({
                                                        param: null
                                                    });
                                                }
                                            }, 3)
                                        }
                                    }
                                } else {
                                    $(elem).val(selectedVal.label);
                                    ctrl.$setViewValue(selectedVal.item);
                                    ctrl.$render();
                                    if (type == "tab") {
                                        $timeout(function() {
                                            if (scope.onSelect) {
                                                scope.onSelect({
                                                    param: selectedVal.item
                                                });
                                            }
                                        }, 3)
                                    }
                                }
                            } catch (e) {}
                        }

                    }

                }

                $(elem).on("keydown", function(event) {
                    if (event.keyCode === $.ui.keyCode.TAB && !event.shiftKey) {
                        tabEventHandle('tab');
                        if ($(elem).autocomplete("instance").menu.active) {
                            event.preventDefault();
                        }
                    } else if (event.keyCode === $.ui.keyCode.TAB && event.shiftKey) {
                        tabEventHandle('shiftTab');
                    }
                });

                $rootScope.$on('scrolling', function(event, args) {
                    try {
                        var autocompleteBox = $(elem).autocomplete('widget');
                        if (!autocompleteBox.is(':hidden')) {
                            autocompleteBox.hide();
                        }
                    } catch (e) {

                    }

                });

                $(elem).next(".select-arrow").on("click", function(event) {
                    var autocompleteBox = $(elem).autocomplete('widget');
                    if (autocompleteBox.is(':hidden')) {
                        $(elem).autocomplete('search', '').focus();
                    } else {
                        autocompleteBox.hide();
                    }
                });
                scope.filterArr = [];

                $timeout(function() {
                    $(elem).autocomplete({
                        minLength: 0,
                        delay: 500,
                        open: function(event, ui) {
                            var elemWidth = $(elem).width();
                            if (elemWidth < 150) {
                                elemWidth = 150
                            }
                            $(elem).autocomplete("widget").css({
                                "width": elemWidth + 16 + "px"
                            });
                            //if(scope.filterArr.length>2){
                            //    $(elem).autocomplete("widget").niceScroll($rootScope.ngScrollConfig);
                            //}else{
                            //    $(elem).autocomplete("widget").css({
                            //        "overflow-y":"hidden"
                            //    });
                            //}
                        },
                        close: function() {
                            if (elem.val() != "") {
                                tabEventHandle('tab');
                            }
                        },
                        source: function(request, response) {

                            try {
                                var term = request.term,
                                    matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
                                console.log("term", term);
                                if (scope.itemData && scope.itemData.length > 0) {
                                    var finalArr = $.grep(scope.itemData, function(item) {
                                        return matcher.test(item);
                                    });
                                    if (finalArr && finalArr.length > 0) {
                                        scope.filterArr = angular.copy(finalArr);
                                        if (angular.isDefined(attr.onAdd)) {
                                            if (finalArr[0].val != -2) {
                                                finalArr.unshift({
                                                    label: '+ Add New',
                                                    val: -2
                                                });
                                            }
                                        } else {
                                            if (finalArr[0].val == -2) {
                                                finalArr.shift();
                                            }
                                        }
                                        response(finalArr);
                                    } else {
                                        response([{
                                            label: 'No Records Found',
                                            val: -1
                                        }]);
                                    }

                                } else {
                                    response([{
                                        label: 'No Records Found',
                                        val: -1
                                    }]);
                                }
                            } catch (e) {
                                response([{
                                    label: 'No Records Found',
                                    val: -1
                                }]);
                            }

                        },
                        focus: function(evet, ui) {
                            // prevent value inserted on focus
                            if (ui.item.val == -1 || ui.item.val == -2) {
                                return false
                            }
                        },
                        select: function(event, ui) {
                            if (ui.item.val == -1 || ui.item.val == -2) {
                                if (angular.isDefined(attr.onAdd)) {
                                    if (ui.item.val == -2) {
                                        ctrl.$setViewValue(undefined);
                                        ctrl.$render();
                                        elem.val("");
                                        var addFn = $parse(attr.onAdd);
                                        addFn(scope.$parent);
                                    }
                                }
                                return false;

                            } else {

                                ctrl.$setViewValue(ui.item.value);
                                ctrl.$render();
                                $timeout(function() {
                                    if (scope.onSelect) {
                                        scope.onSelect({
                                            param: ui.item.value
                                        });
                                    }
                                }, 3)

                            }

                        }
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //if(angular.isDefined(attr.filterType)){
                        //    return $( "<li></li>" ).data("ui-autocomplete-item", item)
                        //        .append($filter(attr.filterType)(item.label))
                        //        .appendTo(ul);
                        //
                        //}else{
                        return $("<li></li>").data("ui-autocomplete-item", item)
                            .append(item.label)
                            .appendTo(ul);

                        //}

                    };
                }, 0);

            }
        }

    }])
    .directive("autoCompleteGroup",['$parse', '$timeout', '$q', '$rootScope', '$interpolate', function($parse, $timeout, $q, $rootScope, $interpolate) {
        return {
            restrict: 'A',
            scope: {
                callFun: "&",
                ngModel: "=",
                columns: "=",
                renderItem: "=",
                onSelect: "&",
                availCheck: "=?"

            },
            require: "ngModel",
            link: function(scope, elem, attr, ctrl) {
                if (!ctrl) {
                    return;
                }
                scope.$watch(function() {
                    try {
                        return $interpolate($(elem).attr('disabled'))(scope); //this will evaluate attribute value `{{}}``
                    } catch (e) {}
                }, function(val) {
                    if (val == "disabled") {
                        if (!$(elem).next(".select-arrow").hasClass("cursor-disable")) {
                            $(elem).next(".select-arrow").addClass("cursor-disable");
                        }
                        $(elem).next(".select-arrow").off('click');

                    } else {
                        if ($(elem).next(".select-arrow").hasClass("cursor-disable")) {
                            $(elem).next(".select-arrow").removeClass("cursor-disable");
                        }
                        $(elem).next(".select-arrow").on('click', arrowClick);
                    }
                });

                if ($(elem).attr('ng-disabled') != undefined) {
                    $(elem).after("<span class=\"select-arrow cursor-disable\"><b></b></span>");
                } else {
                    $(elem).after("<span class=\"select-arrow\"><b></b></span>");
                }

                if ($(elem).attr('ng-disabled') == undefined || $(elem).attr('ng-disabled') == false) {
                    $(elem).next(".select-arrow").on("click", arrowClick);
                }
                var autoCompOption = $parse(attr.autoCompleteOption);
                var showArror = true;
                if (angular.isDefined(autoCompOption)) {
                    try {
                        showArror = scope.$eval(autoCompOption).arrow;
                    } catch (e) {
                        showArror = true;
                    }
                }
                if (angular.isDefined(autoCompOption) && showArror == false) {
                    $(elem).next(".select-arrow").hide();
                }

                function arrowClick(event) {
                    var autocompleteBox = $(elem).mcautocomplete('widget');
                    if (autocompleteBox.is(':hidden')) {
                        $(elem).mcautocomplete('search', '').focus();
                    } else {
                        autocompleteBox.hide();
                    }

                }

                scope.$watch('ngModel', function(newValue, oldValue) {
                    console.log("typeof newValue", typeof newValue);
                    if (typeof newValue == "object") {
                        try {
                            elem.val(scope.findProp(newValue, attr.selectedValue));
                        } catch (e) {

                        }
                    }
                    if (newValue == "") {
                        try {
                            ctrl.$setViewValue(null);
                        } catch (e) {


                        }
                    }
                });

                ctrl.$render = function() {
                    if (ctrl.$modelValue) {
                        if (attr.selectedValue) {
                            elem.val(scope.findProp(ctrl.$modelValue, attr.selectedValue));
                        } else {
                            throw new Error("Expected Selected Value");
                        }

                    } else {
                        elem.val("");
                    }

                };
                scope.keypressValid = false;

                function tabEventHandle(type) {
                    if (ctrl.$dirty) {
                        var textValue = $(elem).val().toLowerCase();
                        var valid = false;
                        var selectedVal;
                        if (textValue) {
                            try {
                                angular.forEach(scope.filterArr, function(item, index) {
                                    if (textValue == item.label.toLowerCase()) {
                                        selectedVal = item;
                                        valid = true;
                                        return;
                                    }
                                });
                                if (!valid) {
                                    if (attr.availCheck) {
                                        var currentVal = $(elem).val();
                                        ctrl.$setViewValue(currentVal);
                                        if (type == "tab") {
                                            $timeout(function() {
                                                if (scope.onSelect) {
                                                    scope.onSelect({
                                                        param: currentVal
                                                    });
                                                }
                                            }, 3)
                                        }
                                    } else {
                                        console.log("wrongly called");
                                        $(elem).val("");
                                        ctrl.$setViewValue(null);
                                        ctrl.$render();
                                        if (type == "tab") {
                                            $timeout(function() {
                                                if (scope.onSelect) {
                                                    scope.onSelect({
                                                        param: null
                                                    });
                                                }
                                            }, 3)
                                        }
                                    }
                                } else {
                                    $(elem).val(selectedVal.label);
                                    ctrl.$setViewValue(selectedVal.item);
                                    ctrl.$render();
                                    if (type == "tab") {
                                        $timeout(function() {
                                            if (scope.onSelect) {
                                                scope.onSelect({
                                                    param: selectedVal.item
                                                });
                                            }
                                        }, 3)
                                    }
                                }
                            } catch (e) {}
                        }

                    }

                }

                $(elem).on("keydown", function(event) {
                    if (event.keyCode === $.ui.keyCode.TAB && !event.shiftKey) {
                        tabEventHandle('tab');
                        if ($(elem).mcautocomplete("instance").menu.active) {
                            event.preventDefault();
                        }
                    } else if (event.keyCode === $.ui.keyCode.TAB && event.shiftKey) {
                        tabEventHandle('shiftTab');
                    }
                });

                function widgetHide() {
                    try {
                        var autocompleteBox = $(elem).mcautocomplete('widget');
                        if (!autocompleteBox.is(':hidden')) {
                            autocompleteBox.hide();
                        }
                    } catch (e) {

                    }
                }
                $(window).resize(function() {
                    widgetHide();
                });
                $rootScope.$on('scrolling', function(event, args) {
                    widgetHide();
                });

                scope.filterArr = [];
                if (angular.isDefined(attr.onAdd)) {
                    var addFn = $parse(attr.onAdd);
                }

                scope.findProp = function(obj, prop, defval) {

                    if (typeof defval == 'undefined') defval = null;
                    prop = prop.split('.');
                    for (var i = 0; i < prop.length; i++) {
                        //console.log("value",obj[prop[i]],typeof obj[prop[i]],prop[i]);
                        try {
                            if (obj[prop[i]] == null && typeof obj[prop[i]] == 'undefined')
                                return defval;
                            obj = obj[prop[i]];
                        } catch (e) {
                            return null;
                        }

                    }
                    return obj;
                };
                $timeout(function() {
                    $(elem).mcautocomplete({
                        showHeader: true,
                        minLength: 0,
                        delay: 500,
                        columns: scope.columns,
                        select: function(event, ui) {
                            $timeout(function() {
                                ctrl.$setViewValue(ui.item);
                                ctrl.$render();
                                $timeout(function() {
                                    if (scope.onSelect) {
                                        scope.onSelect({
                                            param: ui.item
                                        });
                                    }
                                }, 3);
                            }, 3);

                        },
                        open: function(event, ui) {
                            scope.tempText = elem.val();
                        },
                        close: function() {
                            if (elem.val() == "" && scope.keypressValid != true) {
                                elem.val(scope.tempText);
                            } else {
                                tabEventHandle('tab');
                            }
                            scope.keypressValid = false;
                        },
                        focus: function(event, ui) {
                            if (ui.item.val == -1) {
                                return false
                            }
                        },
                        source: function(request, response) {
                            try {
                                if (scope.callFun) {
                                    scope.callFun({
                                        param: request.term
                                    }).then(function(data) {
                                        if (data == null || data.legnth <= 0) {
                                            data = [];
                                        }
                                        var resultArr = data;
                                        console.log("resultArr", resultArr);
                                        if (resultArr.length > 0) {
                                            scope.filterArr = angular.copy(resultArr);
                                            response(scope.filterArr);
                                        } else {
                                            response([{
                                                label: 'No Records Found',
                                                val: -1
                                            }]);
                                        }
                                    }, function(err) {
                                        response([{
                                            label: 'No Records Found',
                                            val: -1
                                        }]);
                                    });
                                }
                            } catch (e) {
                                response([{
                                    label: 'No Records Found',
                                    val: -1
                                }]);
                            }
                        }
                    }).data("ui-autocomplete");

                }, 0);

            }
        }

    }])