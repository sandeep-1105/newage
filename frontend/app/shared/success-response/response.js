app.directive("successMessage",['$timeout','$rootScope', function($timeout,$rootScope) {
	return {
		templateUrl : 'app/shared/success-response/response.html',
		replace: true,
		restrict : 'AE',
		link:function(scope,elem,attr){
			$timeout(function(){
				scope.statusTimeOut = true;
				scope.susCode = "ERR0";
				//scope.susDesc = "Saved SuccessFully";
			},0);
			scope.$watch('susCode',function(newValue,oldValue){
				if(newValue!=oldValue){
					$timeout(function(){
						scope.statusTimeOut = false;
						$rootScope.successDesc=null;
					},3500);
				}
			});
		}
	};
}]);