/**
 * Created by saravanan on 10/4/16.
 */
 app.directive("iconbarMenu",[ function(){
 	return {
 		restrict: 'E',
 		transclude:true,
 		templateUrl :'app/shared/iconbar/iconbar.html'
 	}
 }])