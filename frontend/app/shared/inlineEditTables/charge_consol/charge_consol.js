/**
 * Created by saravanan on 23/6/16.
 */
app.directive('consolChargeTable', ['$timeout', '$compile', '$rootScope', 'ValidateUtil', 'appConstant', 'CommonValidationService', 'Notification',
    function($timeout, $compile, $rootScope, ValidateUtil, appConstant, CommonValidationService, Notification) {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/inlineEditTables/charge_consol/charge_consol.html',
            scope: {
                dependData: "=",
                tableData: "=",
                chargeState: "&",
                calcFn: "&",
                addRole: "=",
                editRole: "=",
                deleteRole: "=",
                showHistory: "=",
                defaultChargeRole: "=",
                tableCheck: "=?",
                fnRole: "&"
            },
            controller: function($scope, $rootScope, $popover, $modal, $http, ChargeSearchKeyword, DefaultEssentialCharge, CurrencyMasterSearchKeyword, UnitSearchKeyword) {
                $timeout(function() {
                    $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                    //console charge audit

                    /*$scope.limitArr = [10, 15, 20];
                $scope.page = 0;
                $scope.limit = 10;
                $scope.totalRecord = 100;
                
                
                $scope.limitChange = function(nLimit) {
                	
                    $scope.page = 0;
                    $scope.limit = nLimit;
                    $scope.showChargeHistory();
                }
                
                $scope.changepage = function(param) {
                	
                    $scope.page = param.page;
                    $scope.limit = param.size;
                    $scope.showChargeHistory();
                }
                
                
                $scope.initialPage = function(){
                	$scope.limitArr = [10, 15, 20];
                    $scope.page = 0;
                    $scope.limit = 10;
                    $scope.totalRecord = 100;
                }*/

                    $scope.groupCol = [{
                        name: 'Charge Code',
                        width: '100px',
                        valueField: 'chargeCode'
                    }, {
                        name: 'Charge Name',
                        width: '120px',
                        valueField: 'chargeName'
                    }];


                    var dirtArr = [];
                    $scope.tableisChecked = true;
                    $scope.$watch('tableData', function(newValue, oldValue) {
                        if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                            initTable();
                        }
                    });

                    function isEmptyRow(obj) {
                        //return (Object.getOwnPropertyNames(obj).length === 0);
                        var isempty = true; //  empty
                        if (!obj) {
                            return isempty;
                        }

                        var k = Object.getOwnPropertyNames(obj);
                        for (var i = 0; i < k.length; i++) {
                            if (obj[k[i]]) {
                                isempty = false; // not empty
                                break;

                            }
                        }
                        return isempty;
                    }

                    $scope.currencyFormat = function(currencyMaster, amount) {
                        return $rootScope.currencyFormat(currencyMaster, amount);
                    }


                    $scope.tableCheck = function() {
                        $scope.tableisChecked = false;
                        var fineArr = [];
                        $scope.secondArr = [];
                        var validResult;
                        var finalData = [];
                        angular.forEach($scope.tableData, function(dataObj, index) {
                            $scope.secondArr[index] = {};
                            if (isEmptyRow(dataObj)) {
                                validResult = true;
                                $scope.tableData.splice(index, 1);
                                delete $scope.tableData[index];
                            } else {
                                validResult = validateObj(dataObj, index);
                                finalData.push(dataObj);
                            }
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                $scope.secondArr[index].errRow = true;
                                fineArr.push(0);
                            }
                        });
                        if (fineArr.indexOf(0) < 0) {
                            $scope.tableData = finalData;
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.ngScrollConfigScroll = {
                        "cursorcolor": '#3F8DEB',
                        "cursorwidth": '8px',
                        "smoothscroll": true,
                        "autohidemode": false
                    };

                    $scope.crnArr = $rootScope.enum['CRN'];
                    $scope.ppcArr = $rootScope.enum['PPCC'];
                    $scope.dueArr = $rootScope.enum['DUE'];

                    $scope.indexFocus = null;

                    $scope.secondArr = [];

                    $scope.totalLocalAmt = 0.0;

                    //while coming from shipment attach to master need to set the totalLocalAmount
                    if ($scope.dependData != undefined && $scope.dependData.totalAmount != null && $scope.dependData.totalAmount != undefined && $scope.dependData.totalAmount != "") {
                        $scope.totalLocalAmt = $scope.dependData.totalAmount;
                    }

                    $scope.getDecimalValues = function() {
                            for (var i = 0; i < $scope.tableData.length; i++) {
                                if (isNaN($scope.tableData[i].amount) == false) {
                                    $scope.tableData[i].amount = $rootScope.currency($scope.tableData[i].currency, $scope.tableData[i].amount);
                                }
                                if (isNaN($scope.tableData[i].localAmount) == false) {
                                    $scope.tableData[i].localAmount = $rootScope.currency($scope.tableData[i].currency, $scope.tableData[i].localAmount);
                                }
                                $scope.totalLocalAmt = $rootScope.currency($scope.dependData.localCurrency, $scope.totalLocalAmt);
                            }
                        }
                        //while editing and reloading, getting the decimal format of amount
                    if ($scope.tableData != null && $scope.tableData != undefined && $scope.tableData.length > 0) {
                        $scope.getDecimalValues();
                    }

                    $scope.calculateSum = function(item) {

                        $scope.totalLocalAmt = 0.00;
                        for (var i = 0; i < $scope.tableData.length; i++) {
                            if (isNaN($scope.tableData[i].localAmount) == false) {
                                $scope.totalLocalAmt = parseFloat($scope.totalLocalAmt) + parseFloat($scope.tableData[i].localAmount);
                            }
                        }
                        $scope.getDecimalValues();
                        $scope.calcFn({ param: { totalAmount: $scope.totalLocalAmt } });
                    }

                    $scope.selectedCharge = function(dataObj) {
                        console.log("dataObj", dataObj);
                    }
                    $scope.$watch('dependData.totalAmount', function(newVal, oldVal) {
                        if (newVal != oldVal) {
                            $scope.totalLocalAmt = $scope.dependData.totalAmount;
                        }
                    });

                    function initTable() {
                        angular.forEach($scope.tableData, function(item) {
                            $scope.secondArr.push({ "errRow": false });
                        });
                    }

                    initTable();

                    /*if(!$scope.tableData.length){
                     $scope.tableData.push({});
                     //dirtArr.push(0);
                     }*/

                    $scope.addRow = function() {
                        var fineArr = [];
                        angular.forEach($scope.tableData, function(dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            $scope.secondArr[index] = {};
                            var validResult = validateObj(dataObj, index);;
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                $scope.secondArr[index].errRow = true;
                                fineArr.push(0);
                            }
                            //console.log();
                        });
                        if (fineArr.indexOf(0) < 0) {
                            if ($scope.tableData == undefined || $scope.tableData == null)
                                $scope.tableData = [];
                            $scope.tableData.push({});
                            $scope.secondArr.push({ "errRow": false });
                            $scope.indexFocus = null;
                            $scope.secondArr[$scope.tableData.length - 1].chargeCodeFocus = true;
                            if ($scope.chargeState) {
                                $scope.chargeState({ param: false });
                            }
                        } else {
                            if ($scope.chargeState) {
                                $scope.chargeState({ param: false });
                            }
                        }
                    };


                    var myOtherModal = $modal({ scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true });
                    var errorOnRowIndex = null;
                    $scope.errorShow = function(errorObj, index) {
                        $scope.errList = errorObj.errTextArr;
                        // Show when some event occurs (use $promise property to ensure the template has been loaded)
                        errorOnRowIndex = index;
                        myOtherModal.$promise.then(myOtherModal.show);
                    };
                    //error validation rules

                    var validateObj = function(dataObj, index) {

                        console.log("data validate", dataObj);

                        var errorArr = [];

                        $scope.secondArr[index].errTextArr = [];


                        if (dataObj.chargeMaster != undefined && dataObj.chargeMaster != null) {

                            dataObj.chargeName = dataObj.chargeMaster.chargeName;
                            dataObj.chargeCode = dataObj.chargeMaster.chargeCode;

                        }


                        if (dataObj.currency != undefined && dataObj.currency != null) {

                            dataObj.currencyName = dataObj.currency.currencyName;
                            dataObj.currencyCode = dataObj.currency.currencyCode;

                        }


                        if (dataObj.chargeMaster == undefined ||
                            dataObj.chargeMaster == null ||
                            dataObj.chargeMaster == "") {

                            $scope.secondArr[index].chargeCode = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95620"]);
                            errorArr.push(0);
                        } else {

                            if (ValidateUtil.isStatusBlocked(dataObj.chargeMaster.status)) {
                                $scope.secondArr[index].chargeCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95621"]);
                                errorArr.push(0);
                            } else if (ValidateUtil.isStatusHidden(dataObj.chargeMaster.status)) {
                                $scope.secondArr[index].chargeCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95622"]);
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].chargeCode = false;
                                errorArr.push(1);
                            }

                        }

                        if (dataObj.chargeName == null || dataObj.chargeName == "" ||
                            dataObj.chargeName == undefined) {
                            $scope.secondArr[index].chargeName = true;
                            errorArr.push(0);
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95623"]);
                        } else {

                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charge_Name, dataObj.chargeName)) {
                                console.log("chargeName invalid ", $rootScope.nls["ERR95624"])
                                $scope.secondArr[index].chargeName = true;
                                errorArr.push(0);
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95624"]);
                            } else {
                                $scope.secondArr[index].chargeName = false;
                                errorArr.push(1);
                            }
                        }

                        if (dataObj.crn == null || dataObj.crn == "" || dataObj.crn == undefined) {
                            $scope.secondArr[index].crn = true;
                            errorArr.push(0);
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95633"]);
                        } else {
                            $scope.secondArr[index].crn = false;
                            errorArr.push(1);
                        }

                        if (dataObj.currency == undefined || dataObj.currency == null || dataObj.currency == "") {

                            $scope.secondArr[index].currency = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95625"]);
                            errorArr.push(0);

                        } else {
                            if (ValidateUtil.isStatusBlocked(dataObj.currency.status)) {
                                $scope.secondArr[index].currency = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95626"]);
                                errorArr.push(0);
                            } else if (ValidateUtil.isStatusHidden(dataObj.currency.status)) {
                                $scope.secondArr[index].currency = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95627"]);
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].currency = false;
                                errorArr.push(1);
                            }

                        }


                        if (dataObj.unit != null || dataObj.unit != "" || dataObj.unit != undefined) {
                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.unit)) {
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95628"]);
                                $scope.secondArr[index].unit = true;
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].unit = false;
                                errorArr.push(1);
                            }
                        }


                        if (!isNaN(dataObj.currencyRoe) && dataObj.currencyRoe != undefined && dataObj.currencyRoe != null && dataObj.currencyRoe != "") {
                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_ROE, dataObj.currencyRoe)) {
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95629"]);
                                $scope.secondArr[index].currencyRoe = true;
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].currencyRoe = false;
                                errorArr.push(1);
                            }
                        }


                        if (!isNaN(dataObj.amountPerUnit) && dataObj.amountPerUnit != undefined && dataObj.amountPerUnit != null && dataObj.amountPerUnit != "") {
                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.amountPerUnit)) {
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95630"]);
                                $scope.secondArr[index].amountPerUnit = true;
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].amountPerUnit = false;
                                errorArr.push(1);
                            }

                        }


                        if (!isNaN(dataObj.amount) && dataObj.amount != null && dataObj.amount != "" && dataObj.amount != undefined) {
                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.amount)) {
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95631"]);
                                $scope.secondArr[index].amount = true;
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].amount = false;
                                errorArr.push(1);
                            }
                        }


                        if (!isNaN(dataObj.localAmount) && dataObj.localAmount != null && dataObj.localAmount != "" && dataObj.localAmount != undefined) {
                            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.localAmount)) {
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR95632"]);
                                $scope.secondArr[index].localAmount = true;
                                errorArr.push(0);
                            } else {
                                $scope.secondArr[index].localAmount = false;
                                errorArr.push(1);
                            }

                        }


                        console.log("data", $scope.secondArr);

                        if (errorArr.indexOf(0) < 0) {
                            return true
                        } else {
                            return false;
                        }


                    };

                    $scope.dirtyIndex = function(index) {
                        $scope.indexFocus = index;
                    };

                    $(document).on('keydown', function(e) {
                        if (e.keyCode == 27) {
                            if (myOtherModal) {
                                myOtherModal.$promise.then(myOtherModal.hide);
                            }
                        }
                    })

                    $scope.$on('modal.hide', function() {
                        if ($scope.secondArr[errorOnRowIndex].chargeCodeFocus) {
                            $scope.secondArr[errorOnRowIndex].chargeCodeFocus = false;
                        }
                        $timeout(function() {
                            $scope.secondArr[errorOnRowIndex].chargeCodeFocus = true;
                            errorOnRowIndex = null;
                        }, 10)
                    });


                    $scope.chargeMasterList = [];

                    $scope.localSearch = function(object) {

                        console.log("ajaxChargeEvent ", object);

                        $scope.searchDto = {};
                        $scope.searchDto.keyword = object == null ? "" : object;
                        $scope.searchDto.selectedPageNumber = 0;
                        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                        console.log("search data", $scope.searchDto);
                        return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                if (data.responseCode == "ERR0") {
                                    $scope.totalRecord = data.responseObject.totalRecord;
                                    $scope.chargeMasterList = data.responseObject.searchResult;
                                    return $scope.chargeMasterList;
                                }
                            },
                            function(errResponse) {
                                console.error('Error while fetching Charge');
                            }
                        );

                    }


                    $scope.localCurrencySearch = function(object) {

                        console.log("ajaxCurrencyMasterEvent is called ", object);

                        $scope.searchDto = {};
                        $scope.searchDto.keyword = object == null ? "" : object;
                        $scope.searchDto.selectedPageNumber = 0;
                        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


                        return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                if (data.responseCode == "ERR0") {
                                    $scope.totalRecord = data.responseObject.totalRecord;
                                    $scope.currencyMasterList = data.responseObject.searchResult;
                                    return $scope.currencyMasterList;

                                }
                            },
                            function(errResponse) {
                                console.error('Error while fetching Currency');
                            }
                        );

                    }

                    $scope.clearChargeName = function(charge) {
                        if (charge.chargeMaster == undefined || charge.chargeMaster == null || charge.chargeMaster == "") {
                            charge.chargeName = "";
                        }
                    }


                    $scope.chargeChange = function(obj) {
                        if (obj.chargeMaster != undefined && obj.chargeMaster != null && obj.chargeMaster != "") {

                            obj.chargeName = obj.chargeMaster.chargeName;

                            if (obj.chargeMaster.chargeType === "Origin") {
                                obj.ppcc = "Prepaid";
                                if ($scope.dependData.origin != undefined && $scope.dependData.origin != null) {
                                    obj.currency = $scope.dependData.origin.portGroupMaster.countryMaster.currencyMaster;
                                }
                            } else if (obj.chargeMaster.chargeType === "Destination") {
                                obj.ppcc = "Collect";
                                if ($scope.dependData.destination != undefined && $scope.dependData.destination != null) {
                                    obj.currency = $scope.dependData.destination.portGroupMaster.countryMaster.currencyMaster;
                                }
                            } else if (obj.chargeMaster.chargeType === "Freight") {
                                if ($scope.dependData.ppcc === 'Prepaid') {
                                    obj.ppcc = "Prepaid";
                                } else {
                                    obj.ppcc = "Collect";
                                }
                                if ($scope.dependData.origin != undefined && $scope.dependData.origin != null) {
                                    obj.currency = $scope.dependData.origin.portGroupMaster.countryMaster.currencyMaster;
                                }
                            } else {
                                obj.ppcc = null;
                                obj.currency = $scope.dependData.localCurrency;
                            }


                            if (obj.chargeMaster.calculationType === 'Unit') {
                                obj.unit = $scope.dependData.consolDocument.chargebleWeight;
                            } else if (obj.chargeMaster.calculationType === 'Document') {
                                obj.unit = 1;
                            } else if (obj.chargeMaster.calculationType === 'Shipment') {
                                obj.unit = 1;
                            }

                            obj.crn = "Cost";

                            obj.currencyRoe = 1;
                            $scope.selectedCurrency(obj);

                        } else {
                            obj.chargeName = ""
                        }

                    }



                    $scope.$watch('indexFocus', function(newvalue, oldValue) {
                        if (newvalue != null && oldValue != null && newvalue != oldValue) {
                            var focusErrArr = [];
                            angular.forEach($scope.tableData, function(dataObj, index) {
                                //var curObj = $scope.tableData.data[index];
                                var validResult = validateObj(dataObj, index);
                                if (validResult) {
                                    $scope.secondArr[index].errRow = false;
                                    focusErrArr.push(1);
                                } else {
                                    $scope.secondArr[index].errRow = true;
                                    focusErrArr.push(0);

                                }
                                //console.log();
                            });
                            if (focusErrArr.indexOf(0) < 0) {
                                if ($scope.chargeState) {
                                    $scope.chargeState({ param: true });
                                }
                            } else {
                                if ($scope.chargeState) {
                                    $scope.chargeState({ param: false });
                                }
                            }
                        }
                    });

                    $scope.removeObj = function(obj, index) {
                        $scope.tableData.splice(index, 1);
                        $scope.secondArr.splice(index, 1);
                        $scope.indexFocus = null;
                        $scope.changeTotAmt();
                        //console.log("dirtArr",dirtArr)
                        //var dirtIndex = dirtArr.indexOf(index);
                        //dirtArr.splice(dirtIndex,1);
                    };
                    $scope.changeTotAmt = function() {
                        $scope.totalLocalAmt = 0;
                        angular.forEach($scope.tableData, function(item) {
                            $scope.calculateSum(item);
                        });
                    };
                    $scope.calculateAmt = function(dataObj) {

                        if (dataObj.unit != undefined && dataObj.amountPerUnit != undefined &&
                            dataObj.unit != null && dataObj.amountPerUnit != null &&
                            dataObj.unit >= 0 && dataObj.amountPerUnit >= 0) {

                            dataObj.amount = parseFloat(isNaN(dataObj.unit) ? 0 : dataObj.unit) * parseFloat(isNaN(dataObj.amountPerUnit) ? 0 : dataObj.amountPerUnit);

                            dataObj.amount = $rootScope.currency(dataObj.currency, dataObj.amount);
                        }

                        if (dataObj.currencyRoe != undefined && dataObj.amount != undefined &&
                            dataObj.currencyRoe != null && dataObj.amount != null &&
                            dataObj.currencyRoe >= 0 && dataObj.amount >= 0) {

                            dataObj.localAmount = parseFloat(isNaN(dataObj.currencyRoe) ? 0 : dataObj.currencyRoe) * parseFloat(isNaN(dataObj.amount) ? 0 : dataObj.amount);
                            dataObj.localAmount = $rootScope.currency($scope.dependData.localCurrency, dataObj.localAmount);
                            $scope.calculateLocalAmt(dataObj);
                        }

                    };
                    $scope.calculateLocalAmt = function(dataObj) {
                        if (dataObj.locamt > 0) {
                            $scope.changeTotAmt();
                            if ($scope.calcFn) {
                                //callback for parent controller
                                //$scope.calcFn({param:{grossWeight:$scope.dimensionGrossWeight,volumeWeight:$scope.dimensionVolWeight}});
                            }
                        } else {
                            $scope.changeTotAmt();
                        }

                    }

                    $scope.defaultCharge = function() {
                        var obj = {};
                        obj.serviceId = $scope.dependData.serviceMaster.id;
                        obj.originId = $scope.dependData.origin.id;
                        obj.destinationId = $scope.dependData.destination.id;
                        //obj.tosId = $scope.dependData.tosMaster.id;

                        DefaultEssentialCharge.query(obj).$promise.then(function(
                            data, status) {

                            var resultArr = data.responseObject.searchResult;

                            if (resultArr != undefined && resultArr != null && resultArr.length != 0) {
                                $scope.tableCheck();
                                for (var i = 0; i < resultArr.length; i++) {
                                    var charge = {};
                                    charge.chargeMaster = resultArr[i].chargeMaster;
                                    charge.chargeName = resultArr[i].chargeMaster.chargeName;
                                    charge.crn = resultArr[i].crn;
                                    charge.ppcc = resultArr[i].ppcc;
                                    $scope.tableData.push(charge);
                                }
                            }

                        });
                    }

                    $scope.showChargeHistory = function() {
                        /*var searchDto={};
				searchDto.selectedPageNumber =$scope.page;
				searchDto.recordPerPage =$scope.limit;*/
                        var auditArr = [];
                        $http({
                            method: 'GET',
                            url: '/api/v1/consol/getchargehistory/' + $scope.dependData.consolUid
                        }).then(function(response) {
                            if (response.data.responseCode == "ERR0") {
                                $scope.auditArr = [];
                                $scope.auditArr = response.data.responseObject;
                                var chargeModal;
                                chargeModal = $modal({
                                    scope: $scope,
                                    backdrop: 'static',
                                    templateUrl: '/app/shared/inlineEditTables/charge_consol/charge_history.html',
                                    show: false
                                });
                                chargeModal.$promise.then(chargeModal.show)
                            } else if (response.data.responseCode == "ERR0700012") {
                                Notification.error($rootScope.nls["ERR0700012"]);
                            }
                        });
                    }


                    /*    $scope.showChargeHistory=function(){
                     	   $http({	
                    			method: 'POST', 
                    			url: urlPath+'/api/v1/consol/getchargehistory', 
                    			headers: {'SAAS_ID': 'FSL'}
                    	}).then(function(response) {
                    	 if (response.responseCode == "ERR0") {
                          $scope.historyObj = response.responseObject;
                          var chargeModal;
                      	chargeModal= $modal({
                               scope: $scope,
                               backdrop: 'static',
                               templateUrl: '/app/shared/inlineEditTables/charge_consol/charge_history.html',
                               show: false
                           });
                      	chargeModal.$promise.then(chargeModal.show)
                      } else{
                     	 $scope.console("no history");
                      }
                    		
                    	});		
                         }*/



                    $scope.selectedCurrency = function(dataObj) {
                        if (dataObj.currency != undefined && dataObj.currency != null && dataObj.currency.id != null) {
                            if ($scope.dependData.localCurrency.id === dataObj.currency.id) {
                                dataObj.currencyRoe = 1;
                            } else {
                                if ($rootScope.baseCurrenyRate[dataObj.currency.id] != null) {
                                    dataObj.currencyRoe = $rootScope.baseCurrenyRate[dataObj.currency.id].csell;
                                }
                            }
                            $scope.calculateAmt(dataObj);
                        }
                    }

                    $scope.copy = $rootScope.copy;
                    $scope.copyObj = function(copyObj) {
                        var tmpObj = angular.copy(copyObj);
                        tmpObj.id = null;
                        tmpObj.systemTrack = null;
                        tmpObj.versionLock = 0;
                        if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                            $scope.tableData = [];
                        } else {
                            var tmpIndex = $scope.tableData.length - 1;
                            if (isEmptyRow($scope.tableData[tmpIndex])) {
                                $scope.tableData.splice(tmpIndex, 1);
                                delete $scope.tableData[tmpIndex]

                                if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                                    $scope.tableData = [];
                                }
                            }
                        }
                        $scope.tableData.push(tmpObj);
                        $scope.calculateAmt($scope.tableData[$scope.tableData.length - 1]);
                    }

                    $scope.chargeRender = function(item) {
                        return {
                            label: item.chargeCode,
                            item: item
                        }
                    }
                    $scope.currencyRender = function(item) {
                        return {
                            label: item.currencyCode,
                            item: item
                        }
                    }

                    $scope.moveToNextField = function(nextIdValue, index) {
                        console.log("id : ", nextIdValue)
                        $rootScope.navigateToNextField(nextIdValue + index);

                    }
                    $scope.delete = $rootScope.delete;
                    $scope.errorList = $rootScope.errorList;

                }, 2);
            }
        }
    }
]);