/**
 * Created by saravanan on 23/6/16.
 */
app.directive('partyDivisionTable',['$timeout','$compile','$rootScope','ValidateUtil','DivisionListByCompany','CompanyList',
    function($timeout,$compile,$rootScope,ValidateUtil,DivisionListByCompany,CompanyList){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/party_division/party_division.html',
        scope:{
            tableData:"=",
            tableState:"&",
            tableCheck:"=",
            calcFn:"&",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"="
            	
        },
        controller : function($scope,$rootScope,$popover,$modal){

            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.indexFocus = null;

                $scope.secondArr = [];


                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row


                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableData == undefined || $scope.tableData == null)
                            $scope.tableData = [];
                        $scope.tableData.push({});
                        $scope.tableState({param: false});
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].companyNameFocus = true;
                        }, 10);
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].companyNameFocus = true;
                        }, 10);
                    }
                };


                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxCompanyEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CompanyList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.companyList = data.responseObject.searchResult;
                                return $scope.companyList;
                                //$scope.showCompanyList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Company');
                        }
                    );

                }
                //$scope.ajaxCompanyEvent(null);

                $scope.changeDivision = function (dataObj, nextFieldId) {
                    $scope.companyId = dataObj.companyMaster != undefined ? dataObj.companyMaster.id : null;
                    dataObj.divisionMaster = {};
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }
                    //$scope.ajaxDivisionEvent(null);
                };

                $scope.ajaxDivisionEvent = function (object) {
                    if ($scope.companyId) {
                        console.log("ajaxStateEvent ", object);

                        $scope.searchDto = {};
                        $scope.searchDto.keyword = object == null ? "" : object;
                        $scope.searchDto.selectedPageNumber = 0;
                        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


                        return DivisionListByCompany.fetch({"companyId": $scope.companyId}, $scope.searchDto).$promise.then(
                            function (data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.divisionList = data.responseObject.searchResult;
                                    return $scope.divisionList;
                                    //$scope.showDivisionList=true;
                                }
                            },
                            function (errResponse) {
                                console.error('Error while fetching State List Based on the Country');
                            }
                        );

                    }
                }

                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }


                    if (dataObj.companyMaster == null || dataObj.companyMaster.id == undefined
                        || dataObj.companyMaster.id == null
                        || dataObj.companyMaster.id == "") {
                        console.log("Company is Mandatory..........")
                        if (type == "row") {
                            $scope.secondArr[index].companyName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6012"]);
                        }
                        //return false;
                        errorArr.push(0);
                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].companyName = false;

                        }
                        //return false;
                        errorArr.push(1);

                    }


                    if (dataObj.hasOwnProperty('divisionMaster') && (dataObj.divisionMaster != undefined && dataObj.divisionMaster != "" && dataObj.divisionMaster != null && dataObj.divisionMaster.id != null)
                    ) {

                        if (dataObj.divisionMaster.status == 'Block') {
                            if (type == "row") {
                                $scope.secondArr[index].divisionName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.divisionMaster.status == 'Hide') {
                            if (type == "row") {
                                $scope.secondArr[index].divisionName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR23"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].divisionName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].companyNameFocus) {
                        $scope.secondArr[errorOnRowIndex].companyNameFocus = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].companyNameFocus = true;
                        errorOnRowIndex = null;
                    }, 10)
                });


                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.tableState({param: true});
                    $scope.indexFocus = null;
                };

                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                };

                $scope.companyRender = function (item) {
                    return {
                        label: item.companyName,
                        item: item
                    }
                }
                $scope.divisionRender = function (item) {
                    return {
                        label: item.divisionName,
                        item: item
                    }
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;
            },2);
        }
    }
}]);