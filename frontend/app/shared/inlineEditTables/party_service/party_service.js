/**
 * Created by saravanan on 23/6/16.
 */
app.directive('partyServiceTable',function($timeout,$compile,$rootScope,ServiceList,ValidateUtil,TosSearchKeyword,CountryWiseLocation,SalesmanList,EmployeeList){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/party_service/party_service.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            tableState:"&",
            tableCheck:"=",
            calcFn:"&",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
            
            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                //index focus initialisation
                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.indexFocus = null;

                $scope.secondArr = [];


                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();

                //add new row
                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableData == undefined || $scope.tableData == null)
                            $scope.tableData = [];
                        $scope.tableData.push({});
                        $scope.tableState({param: false});
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].locationFocus = true;
                        }, 5);
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].locationFocus = true;
                        }, 5);
                    }
                };

                //show errors
                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];

                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.serviceMaster == null || dataObj.serviceMaster.id == undefined || dataObj.serviceMaster.id == null || dataObj.serviceMaster.id == "") {
                        console.log("Service is Mandatory.")
                        if (type == "row") {
                            $scope.secondArr[index].serviceName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6008"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }

                    else {

                        if (dataObj.serviceMaster.status == 'Block') {
                            console.log("Selected Service is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR24"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.serviceMaster.status == 'Hide') {
                            console.log("Selected Service is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR25"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (dataObj.tosMaster != null && dataObj.tosMaster.id != undefined && dataObj.tosMaster.id != null && dataObj.tosMaster.id != "") {


                        if (dataObj.tosMaster.status == 'Block') {
                            console.log("Selected Tos is Blocked.");

                            if (type == "row") {
                                $scope.secondArr[index].tosName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR26"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.tosMaster.status == 'Hide') {
                            console.log("Selected Tos is Hidden.");

                            if (type == "row") {
                                $scope.secondArr[index].tosName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR27"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].tosName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (dataObj.locationMaster == null || dataObj.locationMaster.id == undefined || dataObj.locationMaster.id == null || dataObj.locationMaster.id == "") {
                        console.log("Location is Mandatory..........")
                        if (type == "row") {
                            $scope.secondArr[index].locationName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR44"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }


                    else {

                        if (dataObj.locationMaster.status == 'Block') {
                            console.log("Selected Location is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].locationName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6028"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.locationMaster.status == 'Hide') {
                            console.log("Selected Location is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].locationName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6029"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].locationName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (dataObj.salesman == null || dataObj.salesman.id == undefined || dataObj.salesman.id == null || dataObj.salesman.id == "") {
                        console.log("Salesman is Mandatory..........")
                        if (type == "row") {
                            $scope.secondArr[index].employeeName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6051"]);
                        }
                        //return false;
                        errorArr.push(0);

                    } else {

                        if (dataObj.salesman.status == 'Block') {
                            console.log("Selected Salesman is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].employeeName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR28"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.salesman.status == 'Hide') {
                            if (type == "row") {
                                $scope.secondArr[index].employeeName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR29"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].employeeName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (dataObj.customerService == null || dataObj.customerService.id == undefined || dataObj.customerService.id == null || dataObj.customerService.id == "") {


                    } else {

                        if (dataObj.customerService.status == 'Block') {
                            console.log("Selected Customer Service Person Name is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].employeeName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR30"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.customerService.status == 'Hide') {
                            if (type == "row") {
                                $scope.secondArr[index].employeeName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR31"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].employeeName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };

                //ajax hit
                $scope.ajaxServiceEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return ServiceList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.serviceList = data.responseObject.searchResult;
                                return $scope.serviceList;

                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Services');
                        }
                    );

                }
                //$scope.ajaxServiceEvent(null);

                $scope.ajaxTosEvent = function (object) {

                    console.log("ajaxTosEvent " + object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return TosSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.tosList = data.responseObject.searchResult;
                                return $scope.tosList;
                                //$scope.showTosList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching TOS List..........');
                        }
                    );

                }
                //$scope.ajaxTosEvent(null);

                $scope.ajaxLocationEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CountryWiseLocation.fetch({"countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.locationList = data.responseObject.searchResult;
                                return $scope.locationList;
                                //$scope.showLocationList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Locations');
                        }
                    );

                }
                //$scope.ajaxLocationEvent(null);

                $scope.ajaxSalesmansEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return SalesmanList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.salesmanList = data.responseObject.searchResult;
                                return $scope.salesmanList;
                                //$scope.showSalesmanList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Salesmans');
                        }
                    );

                }
                //$scope.ajaxSalesmansEvent(null);

                $scope.ajaxCustomerServiceEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return EmployeeList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.customerServiceList = data.responseObject.searchResult;
                                return $scope.customerServiceList;
                                //$scope.showCustomerServiceList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );

                }
                //$scope.ajaxCustomerServiceEvent(null);


                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].locationFocus) {
                        $scope.secondArr[errorOnRowIndex].locationFocus = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].locationFocus = true;
                        errorOnRowIndex = null;
                    }, 10)
                });


                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.tableState({param: true});
                    $scope.indexFocus = null;
                };

                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                };

                $scope.serviceRender = function (item) {
                    return {
                        label: item.serviceName,
                        item: item
                    }
                }
                $scope.tosRender = function (item) {
                    return {
                        label: item.tosName,
                        item: item
                    }
                }
                $scope.locationRender = function (item) {
                    return {
                        label: item.locationName,
                        item: item
                    }
                }
                $scope.salesmanRender = function (item) {
                    return {
                        label: item.employeeName,
                        item: item
                    }
                }
                $scope.selectedServiceName = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.selectedCustomerService = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.selectedlocation = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.selectedSalesman = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;

            },2);
        }
    }
});