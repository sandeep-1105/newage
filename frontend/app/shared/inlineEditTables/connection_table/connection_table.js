/**
 * Created by saravanan on 23/6/16.
 */
app.directive('connectionTable',['$timeout','$compile','$rootScope','ValidateUtil','PortByTransportMode','CarrierByTransportMode',
    function($timeout,$compile,$rootScope,ValidateUtil,PortByTransportMode,CarrierByTransportMode){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/connection_table/connection_table.html',
        scope:{
        	indexKey:"=",
            tableData:"=",
            dependData:"=",
            tableState:"&",
            calcFn:"&",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"=?",
        },
        controller : function($scope,$rootScope,$popover,$modal){
        	$scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";
            $timeout(function() {
            	$scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                $scope.tableisChecked = true;
                var dirtArr = [];
                $scope.currencyCode = $rootScope.userProfile.selectedUserLocation.currencyCode;
                $scope.serviceConnection = {};
                $scope.moveArr = ["Air", "Ocean", "Road", "Rail"];
                
                $scope.connectionArr = $rootScope.enum['ConnectionStatus'];
                $scope.flag = false;

                $scope.$watch('tableData', function (newValue, oldValue) {
                    if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                        initTable();
                    }
                });


                $scope.tableCheck = function () {
                    $scope.tableisChecked = false;
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    //if (!obj) {
                    //    return isempty;
                    //}
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.secondArr = [];

                function initTable() {

                    $scope.emptyObjRef = {};
                    if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData.length != 0) {
                    	$scope.emptyObjRef = angular.copy($scope.tableData[$scope.tableData.length - 1]);
                    }
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();

                $scope.addRow = function () {
                    $scope.emptyObjRef = {};
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });

                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableData == undefined || $scope.tableData == null) {
                            $scope.tableData = [];
                        }
                        $scope.connection = {};
                        $scope.connection.connectionStatus = 'Planned';
                        $scope.tableData.push($scope.connection);
                        $rootScope.navigateToNextField($scope.indexKey + 'connectionMove' + ($scope.tableData.length - 1));
                        $scope.emptyObjRef = angular.copy($scope.connection);
                        $scope.tableState({param: false});
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                    }
                };


                $scope.ajaxConnPolEvent = function (object) {
                    console.log("ajaxPortOfLoadingEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PortByTransportMode.fetch({"transportMode": $scope.transportMode}, $scope.searchDto).$promise.then(
                        function (data) {


                            if (data.responseCode == "ERR0") {
                                $scope.connPolList = [];
                                var resultList = data.responseObject.searchResult;
                                if (resultList != null && resultList.length != 0) {
                                    if ($scope.serviceConnection.pod == undefined || $scope.serviceConnection.pod == null) {
                                        $scope.connPolList = resultList;
                                    }
                                    else {
                                        for (var i = 0; i < resultList.length; i++) {

                                            if (resultList[i].id != $scope.serviceConnection.pod.id) {
                                                $scope.connPolList.push(resultList[i]);
                                            }

                                        }
                                    }
                                }
                                return $scope.connPolList;
                                //$scope.showConnPol=true;
                            }


                        },
                        function (errResponse) {
                            console.error('Error while fetching PortOfLoading List');
                        }
                    );

                }

                $scope.ajaxConnPodEvent = function (object) {
                    console.log("ajaxPortOfLoadingEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PortByTransportMode.fetch({"transportMode": $scope.transportMode}, $scope.searchDto).$promise.then(
                        function (data) {


                            if (data.responseCode == "ERR0") {
                                $scope.connPodList = [];
                                var resultList = data.responseObject.searchResult;

                                if (resultList != null && resultList.length != 0) {
                                    if ($scope.serviceConnection.pol == undefined || $scope.serviceConnection.pol == null) {
                                        $scope.connPodList = resultList;
                                    }
                                    else {
                                        for (var i = 0; i < resultList.length; i++) {

                                            if (resultList[i].id != $scope.serviceConnection.pol.id) {
                                                $scope.connPodList.push(resultList[i]);
                                            }

                                        }
                                    }
                                }
                                return $scope.connPodList;
                                //$scope.showConnPod=true;
                            }


                        },
                        function (errResponse) {
                            console.error('Error while fetching PortOfLoading List');
                        }
                    );

                }

                $scope.ajaxConnCarrierEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CarrierByTransportMode.fetch({"transportMode": $scope.transportMode}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.connCarrierList = data.responseObject.searchResult;
                                return $scope.connCarrierList;
                                //$scope.showConnCarrierList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Carrier List Based on Air');
                        }
                    );

                }

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.changeConnMove = function (dataObj, nextIdValue) {
                    if (dataObj.move == "Air") {

                        $scope.serviceConnection.pol = {};
                        $scope.serviceConnection.pod = {};
                        $scope.serviceConnection.carrierMaster = {};

                    }

                    if (dataObj.move == "Ocean") {


                        $scope.serviceConnection.pol = {};
                        $scope.serviceConnection.pod = {};
                        $scope.serviceConnection.carrierMaster = {};

                    }

                    if (dataObj.move == "Road") {

                        $scope.serviceConnection.pol = {};
                        $scope.serviceConnection.pod = {};

                        $scope.serviceConnection.carrierMaster = {};

                    }
                    if (dataObj.move == "Rail") {

                        $scope.serviceConnection.pol = {};
                        $scope.serviceConnection.pod = {};
                        $scope.serviceConnection.carrierMaster = {};
                    }
                    $scope.transportMode = dataObj.move;
                    $rootScope.navigateToNextField(nextIdValue);
                }

                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    $scope.secondArr[index] = {};
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }
                    if (dataObj.move == undefined || dataObj.move == null
                        || dataObj.move == null
                        || dataObj.move == "") {
                        if (type == "row") {
                            $scope.secondArr[index].move = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90346"]);
                        }
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].move = false;
                        }
                        errorArr.push(1);
                    }

                    if (dataObj.pol == undefined || dataObj.pol == null
                        || dataObj.pol.id == null
                        || dataObj.pol.id.trim == "") {
                        if (type == "row") {
                            $scope.secondArr[index].pol = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90348"]);
                        }
                        errorArr.push(0);
                    }
                    else if (ValidateUtil.isStatusBlocked(dataObj.pol.status)) {
                        dataObj.pol = null;
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90349"]);
                            $scope.secondArr[index].errRow = true;
                        }
                        errorArr.push(0);

                    }

                    else if (ValidateUtil.isStatusHidden(dataObj.pol.status)) {
                        dataObj.pol = null;
                        if (type == "row") {
                            $scope.secondArr[index].pol = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90350"]);
                            $scope.secondArr[index].errRow = true;
                        }
                        errorArr.push(0);

                    }
                    /* else if(dataObj.pod !=null && dataObj.pod !=undefined && dataObj.pol.id==dataObj.pod.id){

                     if(type=="row") {
                     $scope.secondArr[index].polportName = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90362"]);
                     }
                     errorArr.push(0);

                     }*/
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].pol = false;
                        }
                        errorArr.push(1);
                    }

                    if (dataObj.pod == undefined || dataObj.pod == null
                        || dataObj.pod.id == null
                        || dataObj.pod.id.trim == "") {
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90351"]);
                        }
                        errorArr.push(0);
                    }
                    else if (ValidateUtil.isStatusBlocked(dataObj.pod.status)) {
                        dataObj.pod = null;
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90352"]);
                            $scope.secondArr[index].errRow = true;
                        }
                        errorArr.push(0);
                    }

                    else if (ValidateUtil.isStatusHidden(dataObj.pod.status)) {
                        dataObj.pod = null;
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90353"]);
                            $scope.secondArr[index].errRow = true;
                        }
                        errorArr.push(0);

                    }
                    else if (dataObj.pol != null && dataObj.pol != undefined && dataObj.pod != undefined && dataObj.pod.id == dataObj.pol.id) {

                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90362"]);
                        }
                        errorArr.push(0);

                    }

                    else {
                        if (type == "row") {
                            $scope.secondArr[index].pod = false;

                        }
                        errorArr.push(1);
                    }


                    if (dataObj.etd == undefined
                        || dataObj.etd == null
                        || dataObj.etd == "") {
                        if (type == "row") {
                            $scope.secondArr[index].etd = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90355"]);
                        }
                        errorArr.push(0);

                    }

                    else {
                        if (type == "row") {
                            $scope.secondArr[index].etd = false;
                        }
                        errorArr.push(1);
                    }


                    if (dataObj.eta == undefined
                        || dataObj.eta == null
                        || dataObj.eta == "") {
                        if (type == "row") {
                            $scope.secondArr[index].eta = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90354"]);
                        }
                        errorArr.push(0);

                    }
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].eta = false;

                        }
                        errorArr.push(1);
                    }


                    if (dataObj.etd != undefined && dataObj.etd != null) {
                        if (dataObj.etd > dataObj.eta) {
                            if (type == "row") {
                                $scope.secondArr[index].eta = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90356"]);
                            }
                            errorArr.push(0);

                        } 
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].eta = false;

                            }
                            errorArr.push(1);
                        }

                    }
                    

                    if (dataObj.etd != undefined && dataObj.etd != null) {

                        if($scope.dependData.etd!=null){
                        	var serviceEtd=new Date($scope.dependData.etd);
                        	serviceEtd.setHours(0, 0, 0, 0);
                        	var etd=new Date(dataObj.etd);
                        	etd.setHours(0, 0, 0, 0);
                        	
	                    	  if (etd < serviceEtd){
	                    		  $scope.secondArr[index].etd = true;
	                              $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2700"]);
	                              errorArr.push(0);
	                    	  }
	                    	  
	                    	  
	                    	 
                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].etd = false;

                            }
                            errorArr.push(1);
                        }

                    }if(dataObj.eta != undefined && dataObj.eta != null) {

                        if($scope.dependData.eta!=null){
                        	var serviceEta=new Date($scope.dependData.eta);
                        	serviceEta.setHours(0, 0, 0, 0);
                        	var eta=new Date(dataObj.eta);
                        	eta.setHours(0, 0, 0, 0);
                        	
	                    	  if (eta < serviceEta){
	                    		  $scope.secondArr[index].eta = true;
	                              $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2714"]);
	                              errorArr.push(0);
	                    	  }
	                    	 
                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].eta = false;

                            }
                            errorArr.push(1);
                        }

                    }

                    if (dataObj.carrierMaster == undefined || dataObj.carrierMaster == null
                        || dataObj.carrierMaster.id == null
                        || dataObj.carrierMaster.id.trim == "") {
                        if (type == "row") {
                            $scope.secondArr[index].carrierName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90357"]);
                        }
                        errorArr.push(0);

                    }
                    else if (ValidateUtil.isStatusBlocked(dataObj.carrierMaster.status)) {

                        if (type == "row") {
                            $scope.secondArr[index].carrierName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90358"]);
                        }
                        errorArr.push(0);

                    }

                    else if (ValidateUtil.isStatusHidden(dataObj.carrierMaster.status)) {

                        if (type == "row") {
                            $scope.secondArr[index].carrierName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90359"]);
                        }
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].carrierName = false;

                        }
                        errorArr.push(1);
                    }



                    if (dataObj.flightVoyageNo == undefined
                        || dataObj.flightVoyageNo == null
                        || dataObj.flightVoyageNo == "") {
                        if (type == "row") {
                            $scope.secondArr[index].flightVoyageNo = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90360"]);
                        }
                        errorArr.push(0);

                    }else {
                        if (type == "row") {
                            $scope.secondArr[index].flightVoyageNo = false;
                        }
                        errorArr.push(1);
                    }


                    if (dataObj.connectionStatus == undefined || dataObj.connectionStatus == null
                        || dataObj.connectionStatus == null
                        || dataObj.connectionStatus == "") {
                        if (type == "row") {
                            $scope.secondArr[index].connectionStatus = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90347"]);
                        }
                        errorArr.push(0);
                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].connectionStatus = false;

                        }
                        errorArr.push(1);
                    }

                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });
                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });


                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.tableState({param: true});
                    $scope.secondArr[index].errRow = false;
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;
                };


                $scope.portRender = function (item) {
                    return {
                        label: item.portName,
                        item: item
                    }
                }
                $scope.carrierRender = function (item) {
                    return {
                        label: item.carrierName,
                        item: item
                    }
                }
                $scope.selectConnections = function (nextIdValue) {
                    console.log("called." + nextIdValue);
                    $rootScope.navigateToNextField(nextIdValue);
                }
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;
            },2);
        }
    }
}]);