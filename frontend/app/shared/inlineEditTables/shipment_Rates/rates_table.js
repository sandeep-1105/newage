 /**
  * Created by saravanan on 23/6/16.
  */
 app.directive('ratesTable', ['$rootScope', '$timeout', '$compile', '$http', 'ValidateUtil', 'ChargeSearchKeyword',
     'UnitMasterGetByCode', 'QuotationViewByQuotationNo', 'UnitSearchKeyword', 'CurrencyMasterSearchKeyword',
     'PartiesList', 'appConstant', 'CommonValidationService', 'Notification',
     function($rootScope, $timeout, $compile, $http, ValidateUtil, ChargeSearchKeyword,
         UnitMasterGetByCode, QuotationViewByQuotationNo, UnitSearchKeyword, CurrencyMasterSearchKeyword,
         PartiesList, appConstant, CommonValidationService, Notification) {
         return {
             restrict: 'E',
             templateUrl: 'app/shared/inlineEditTables/shipment_Rates/rates_table.html',
             scope: {
                 indexKey: "=",
                 tableData: "=",
                 dependData: "=",
                 shipment: "=",
                 showErrors: "=",
                 tableState: "&",
                 calcFn: "&",
                 fnRole: "&",
                 addRole: "=",
                 editRole: "=",
                 deleteRole: "=",
                 tableCheck: "="

             },
             controller: function($scope, $rootScope, $popover, $modal) {
                 $scope.rateMerged = false;
                 var rateMergedValue = $rootScope.appMasterData['booking.rates.merged'];
                 if (rateMergedValue.toLowerCase() == 'true' || rateMergedValue == true) {
                     $scope.rateMerged = true;
                 }
                 $scope.rateRemoveNotAllowFlag = false;
                 var rateRemove = $rootScope.appMasterData['shipment.from.quotation.rate.remove.not.allowed'];
                 if (rateRemove.toLowerCase() == 'true' || rateRemove == true) {
                     $scope.rateRemoveNotAllowFlag = true;
                 }
                 $scope.groupCol = [{
                     name: 'Charge Code',
                     width: '100px',
                     valueField: 'chargeCode'
                 }, {
                     name: 'Charge Name',
                     width: '120px',
                     valueField: 'chargeName'
                 }];

                 $scope.dueArr = $rootScope.enum['DUE'];

                 $timeout(function() {
                     var dirtArr = [];
                     $scope.tableisChecked = true;

                     console.log("appConstant", appConstant.chargeNumberOfUnitList);
                     $scope.numberOfUnitsList = appConstant.chargeNumberOfUnitList;

                     $scope.tableCheck = function() {
                         $scope.tableisChecked = false;
                         var fineArr = [];
                         $scope.secondArr = [];
                         var validResult;
                         var finalData = $scope.tableData;
                         var emptyIndex = [];
                         angular.forEach($scope.tableData, function(dataObj, index) {
                             $scope.secondArr[index] = {};
                             if (isEmptyRow(dataObj)) {
                                 validResult = true;
                                 finalData[index].isEmpty = true;

                             } else {
                                 validResult = validateObj(dataObj, index, "row");

                             }
                             if (validResult) {
                                 fineArr.push(1);
                                 $scope.secondArr[index].errRow = false;
                             } else {
                                 $scope.secondArr[index].errRow = true;
                                 fineArr.push(0);
                             }
                         });
                         angular.forEach(finalData, function(dataObj, index) {
                             if (dataObj.isEmpty) {
                                 finalData.splice(index, 1);
                                 $scope.secondArr.splice(index, 1);
                             }
                         });
                         console.log("***8$scope.tableData", finalData);
                         if (fineArr.indexOf(0) < 0) {
                             $scope.tableData = finalData;
                             if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData.length != undefined && $scope.tableData.length != 0) {
                                 $scope.rateCalculation();
                             } else {
                                 $scope.tableData = null;
                             }
                             return true;
                         } else {
                             return false;
                         }
                     };


                     function isEmptyRow(obj) {
                         //return (Object.getOwnPropertyNames(obj).length === 0);
                         var isempty = true; //  empty
                         //if (!obj) {
                         //    return isempty;
                         //}
                         console.log("old Obj**********", obj);
                         console.log("$scope.emptyObjRef[k[i]]**********", $scope.emptyObjRef);
                         var k = Object.getOwnPropertyNames(obj);
                         for (var i = 0; i < k.length; i++) {
                             if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                                 isempty = false; // not empty
                                 break;

                             }
                         }
                         return isempty;
                     }


                     $scope.shipmentPrec = [{ id: "Prepaid", title: 'Prepaid' }, { id: "Collect", title: 'Collect' }];
                     $scope.dropStyle = { "overflow-x": "auto", "overflow-y": "hidden" };
                     $scope.ngScrollConfigScroll = {
                         "cursorcolor": '#3F8DEB',
                         "cursorwidth": '8px',
                         "smoothscroll": true,
                         "autohidemode": false
                     };
                     $scope.chargeType = [{ id: "CHARGEABLE", title: "No" }, { id: "ACTUAL", title: "Yes" }];

                     $scope.flag = false;

                     if ($scope.tableData == null || $scope.tableData == undefined || $scope.tableData.length == 0) {
                         $scope.tableData = [];
                     }

                     $scope.rateCalculation = function() {
                         $scope.dependData.localCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                         $scope.dependData.localCurrencyCode = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode;
                         $scope.dependData.clientGrossRate = 0;
                         $scope.dependData.clientNetRate = 0;
                         $scope.dependData.declaredCost = 0;
                         $scope.dependData.cost = 0;
                         $scope.dependData.rate = 0;
                         for (var i = 0; i < $scope.tableData.length; i++) {
                             $scope.secondArr[i] = {};
                             $scope.secondArr[i].errTextArr = [];

                             $scope.chargeTemp = $scope.tableData[i];
                             $scope.chargeTemp.localCurrency = $scope.dependData.localCurrency;
                             $scope.chargeTemp.localCurrencyCode = $scope.dependData.localCurrency.currencyCode;

                             $scope.chargeTemp.estimatedUnit = $rootScope.unitCalc($scope.chargeTemp.unitMaster, $scope.dependData.bookedGrossWeightUnitKg, $scope.dependData.bookedVolumeWeightUnitKg, $scope.dependData.bookedPieces, $scope.dependData.bookedVolumeUnitCbm, $scope.dependData.bookedChargeableUnit);

                             if ($scope.chargeTemp.grossSaleCurrency != undefined && $scope.chargeTemp.grossSaleCurrency != null && $scope.chargeTemp.grossSaleCurrency.id != null) {
                                 if ($scope.chargeTemp.grossSaleCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id &&
                                     $rootScope.baseCurrenyRate[$scope.chargeTemp.grossSaleCurrency.id] == null) {
                                     $scope.secondArr[i].grossSaleCurrency = true;
                                     $scope.secondArr[i].errTextArr.push($rootScope.nls["ERR90345-1"]);
                                 } else {
                                     $scope.secondArr[i].grossSaleCurrency = false;
                                 }

                                 var tmpGrossSaleAmount = 0;
                                 if (isNaN($scope.chargeTemp.grossSaleAmount) == true) {
                                     tmpGrossSaleAmount = 0;
                                 } else {
                                     tmpGrossSaleAmount = $scope.chargeTemp.grossSaleAmount;
                                 }
                                 var tmpCGR = $scope.chargeTemp.estimatedUnit * tmpGrossSaleAmount;

                                 if ($scope.chargeTemp.grossSaleMinimum != null && $scope.chargeTemp.grossSaleMinimum != "" && isNaN($scope.chargeTemp.grossSaleMinimum) == false) {
                                     $scope.chargeTemp.totalGrossSale = tmpCGR > $scope.chargeTemp.grossSaleMinimum ? tmpCGR : $scope.chargeTemp.grossSaleMinimum;
                                 } else {
                                     $scope.chargeTemp.totalGrossSale = tmpCGR;
                                 }

                                 $scope.chargeTemp.totalGrossSale = $rootScope.currency($scope.chargeTemp.grossSaleCurrency, $scope.chargeTemp.totalGrossSale);

                                 var sellRate = null;

                                 if ($rootScope.baseCurrenyRate[$scope.chargeTemp.grossSaleCurrency.id] != null && $scope.chargeTemp.localCurrency.id != $scope.chargeTemp.grossSaleCurrency.id) {
                                     sellRate = $rootScope.baseCurrenyRate[$scope.chargeTemp.grossSaleCurrency.id].csell;
                                 } else if ($scope.chargeTemp.localCurrency.id == $scope.chargeTemp.grossSaleCurrency.id) {
                                     sellRate = 1;
                                 }

                                 if (sellRate != null) {
                                     $scope.chargeTemp.localGrossSaleRoe = sellRate;
                                     $scope.chargeTemp.localTotalGrossSale = $scope.chargeTemp.totalGrossSale * $scope.chargeTemp.localGrossSaleRoe;
                                     $scope.chargeTemp.localTotalGrossSale = $rootScope.currency($scope.chargeTemp.grossSaleCurrency, $scope.chargeTemp.localTotalGrossSale);

                                     $scope.dependData.clientGrossRate = $scope.dependData.clientGrossRate + $scope.chargeTemp.localTotalGrossSale;
                                 }

                             }

                             if ($scope.chargeTemp.netSaleCurrency != undefined && $scope.chargeTemp.netSaleCurrency != null && $scope.chargeTemp.netSaleCurrency.id != null) {
                                 if ($scope.chargeTemp.netSaleCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id &&
                                     $rootScope.baseCurrenyRate[$scope.chargeTemp.netSaleCurrency.id] == null) {
                                     $scope.secondArr[i].netSaleCurrency = true;
                                     $scope.secondArr[i].errTextArr.push($rootScope.nls["ERR90345-2"]);
                                 } else {
                                     $scope.secondArr[i].netSaleCurrency = false;
                                 }
                                 var tmpNetSaleAmount = 0;
                                 if (isNaN($scope.chargeTemp.netSaleAmount) == true) {
                                     tmpNetSaleAmount = 0;
                                 } else {
                                     tmpNetSaleAmount = $scope.chargeTemp.netSaleAmount;
                                 }
                                 var tmpCNR = $scope.chargeTemp.estimatedUnit * tmpNetSaleAmount;
                                 if ($scope.chargeTemp.netSaleMinimum != null && $scope.chargeTemp.netSaleMinimum != "" && isNaN($scope.chargeTemp.netSaleMinimum) == false) {
                                     $scope.chargeTemp.totalNetSale = tmpCNR > $scope.chargeTemp.netSaleMinimum ? tmpCNR : $scope.chargeTemp.netSaleMinimum;
                                 } else {
                                     $scope.chargeTemp.totalNetSale = tmpCNR;
                                 }
                                 $scope.chargeTemp.totalNetSale = $rootScope.currency($scope.chargeTemp.netSaleCurrency, $scope.chargeTemp.totalNetSale);

                                 var sellRate = null;

                                 if ($rootScope.baseCurrenyRate[$scope.chargeTemp.netSaleCurrency.id] != null && $scope.chargeTemp.localCurrency.id != $scope.chargeTemp.netSaleCurrency.id) {
                                     sellRate = $rootScope.baseCurrenyRate[$scope.chargeTemp.netSaleCurrency.id].csell;
                                 } else if ($scope.chargeTemp.localCurrency.id == $scope.chargeTemp.netSaleCurrency.id) {
                                     sellRate = 1;
                                 }

                                 if (sellRate != null) {
                                     $scope.chargeTemp.localNetSaleRoe = sellRate;
                                     $scope.chargeTemp.localTotalNetSale = $scope.chargeTemp.totalNetSale * $scope.chargeTemp.localNetSaleRoe;
                                     $scope.chargeTemp.localTotalNetSale = $rootScope.currency($scope.chargeTemp.netSaleCurrency, $scope.chargeTemp.localTotalNetSale);

                                     $scope.dependData.clientNetRate = $scope.dependData.clientNetRate + $scope.chargeTemp.localTotalNetSale;
                                 }


                             }

                             if ($scope.chargeTemp.declaredSaleCurrency != undefined && $scope.chargeTemp.declaredSaleCurrency != null && $scope.chargeTemp.declaredSaleCurrency.id != null) {
                                 if ($scope.chargeTemp.declaredSaleCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id &&
                                     $rootScope.baseCurrenyRate[$scope.chargeTemp.declaredSaleCurrency.id] == null) {
                                     $scope.secondArr[i].declaredSaleCurrency = true;
                                     $scope.secondArr[i].errTextArr.push($rootScope.nls["ERR90345-3"]);
                                 } else {
                                     $scope.secondArr[i].declaredSaleCurrency = false;
                                 }
                                 var tmpDeclaredSaleAmount = 0;
                                 if (isNaN($scope.chargeTemp.declaredSaleAmount) == true) {
                                     tmpDeclaredSaleAmount = 0;
                                 } else {
                                     tmpDeclaredSaleAmount = $scope.chargeTemp.declaredSaleAmount;
                                 }
                                 var tmpDC = $scope.chargeTemp.estimatedUnit * tmpDeclaredSaleAmount;

                                 if ($scope.chargeTemp.declaredSaleMinimum != null && $scope.chargeTemp.declaredSaleMinimum != "" && isNaN($scope.chargeTemp.declaredSaleMinimum) == false) {
                                     $scope.chargeTemp.totalDeclaredSale = tmpDC > $scope.chargeTemp.declaredSaleMinimum ? tmpDC : $scope.chargeTemp.declaredSaleMinimum;
                                 } else {
                                     $scope.chargeTemp.totalDeclaredSale = tmpDC;
                                 }
                                 $scope.chargeTemp.totalDeclaredSale = $rootScope.currency($scope.chargeTemp.declaredSaleCurrency, $scope.chargeTemp.totalDeclaredSale);

                                 var sellRate = null;

                                 if ($rootScope.baseCurrenyRate[$scope.chargeTemp.declaredSaleCurrency.id] != null && $scope.chargeTemp.localCurrency.id != $scope.chargeTemp.declaredSaleCurrency.id) {
                                     sellRate = $rootScope.baseCurrenyRate[$scope.chargeTemp.declaredSaleCurrency.id].csell;
                                 } else if ($scope.chargeTemp.localCurrency.id == $scope.chargeTemp.declaredSaleCurrency.id) {
                                     sellRate = 1;
                                 }

                                 if (sellRate != null) {
                                     $scope.chargeTemp.localDeclaredSaleRoe = sellRate;
                                     $scope.chargeTemp.localTotalDeclaredSale = $scope.chargeTemp.totalDeclaredSale * $scope.chargeTemp.localDeclaredSaleRoe;
                                     $scope.chargeTemp.localTotalDeclaredSale = $rootScope.currency($scope.chargeTemp.declaredSaleCurrency, $scope.chargeTemp.localTotalDeclaredSale);

                                     $scope.dependData.declaredCost = $scope.dependData.declaredCost + $scope.chargeTemp.localTotalDeclaredSale;
                                 }


                             }

                             if ($scope.chargeTemp.costCurrency != undefined && $scope.chargeTemp.costCurrency != null && $scope.chargeTemp.costCurrency.id != null) {
                                 if ($scope.chargeTemp.costCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id &&
                                     $rootScope.baseCurrenyRate[$scope.chargeTemp.costCurrency.id] == null) {
                                     $scope.secondArr[i].costCurrency = true;
                                     $scope.secondArr[i].errTextArr.push($rootScope.nls["ERR90345-4"]);
                                 } else {
                                     $scope.secondArr[i].costCurrency = false;
                                 }

                                 var tmpCostAmount = 0;
                                 if (isNaN($scope.chargeTemp.costAmount) == true) {
                                     if ($scope.chargeTemp.costAmount != '.') {
                                         tmpCostAmount = 0;
                                     } else {
                                         tmpCostAmount = $scope.chargeTemp.costAmount;
                                     }

                                 } else {
                                     tmpCostAmount = $scope.chargeTemp.costAmount;
                                 }
                                 var tmpCost = $scope.chargeTemp.estimatedUnit * tmpCostAmount;
                                 if ($scope.chargeTemp.costMinimum != null && $scope.chargeTemp.costMinimum != "" && isNaN($scope.chargeTemp.costMinimum) == false) {
                                     $scope.chargeTemp.totalCostAmount = tmpCost > $scope.chargeTemp.costMinimum ? tmpCost : $scope.chargeTemp.costMinimum;
                                 } else {
                                     $scope.chargeTemp.totalCostAmount = tmpCost;
                                 }
                                 $scope.chargeTemp.totalCostAmount = $rootScope.currency($scope.chargeTemp.costCurrency, $scope.chargeTemp.totalCostAmount);

                                 var sellRate = null;

                                 if ($rootScope.baseCurrenyRate[$scope.chargeTemp.costCurrency.id] != null && $scope.chargeTemp.localCurrency.id != $scope.chargeTemp.costCurrency.id) {
                                     sellRate = $rootScope.baseCurrenyRate[$scope.chargeTemp.costCurrency.id].csell;
                                 } else if ($scope.chargeTemp.localCurrency.id == $scope.chargeTemp.costCurrency.id) {
                                     sellRate = 1;
                                 }

                                 if (sellRate != null) {
                                     $scope.chargeTemp.localCostAmountRoe = sellRate;
                                     $scope.chargeTemp.localTotalCostAmount = $scope.chargeTemp.totalCostAmount * $scope.chargeTemp.localCostAmountRoe;
                                     $scope.chargeTemp.localTotalCostAmount = $rootScope.currency($scope.chargeTemp.costCurrency, $scope.chargeTemp.localTotalCostAmount);

                                     $scope.dependData.cost = $scope.dependData.cost + $scope.chargeTemp.localTotalCostAmount;
                                 }


                             }

                             if ($scope.chargeTemp.rateCurrency != undefined && $scope.chargeTemp.rateCurrency != null && $scope.chargeTemp.rateCurrency.id != null) {

                                 if ($scope.chargeTemp.rateCurrency.id != $rootScope.userProfile.selectedUserLocation.currencyMaster.id &&
                                     $rootScope.baseCurrenyRate[$scope.chargeTemp.rateCurrency.id] == null) {
                                     $scope.secondArr[i].rateCurrency = true;
                                     $scope.secondArr[i].errTextArr.push($rootScope.nls["ERR90345-5"]);

                                 } else {
                                     $scope.secondArr[i].rateCurrency = false;
                                 }

                                 var tmpRateAmount = 0;
                                 if (isNaN($scope.chargeTemp.rateAmount) == true) {
                                     tmpRateAmount = 0;
                                 } else {
                                     tmpRateAmount = $scope.chargeTemp.rateAmount
                                 }
                                 var tmpRate = $scope.chargeTemp.estimatedUnit * tmpRateAmount;

                                 if ($scope.chargeTemp.rateMinimum != null && $scope.chargeTemp.rateMinimum != "" && isNaN($scope.chargeTemp.rateMinimum) == false) {
                                     $scope.chargeTemp.totalRateAmount = tmpRate > $scope.chargeTemp.rateMinimum ? tmpRate : $scope.chargeTemp.rateMinimum;
                                 } else {
                                     $scope.chargeTemp.totalRateAmount = tmpRate;
                                 }
                                 $scope.chargeTemp.totalRateAmount = $rootScope.currency($scope.chargeTemp.rateCurrency, $scope.chargeTemp.totalRateAmount);

                                 var sellRate = null;

                                 if ($rootScope.baseCurrenyRate[$scope.chargeTemp.rateCurrency.id] != null && $scope.chargeTemp.localCurrency.id != $scope.chargeTemp.rateCurrency.id) {
                                     sellRate = $rootScope.baseCurrenyRate[$scope.chargeTemp.rateCurrency.id].csell;
                                 } else if ($scope.chargeTemp.localCurrency.id == $scope.chargeTemp.rateCurrency.id) {
                                     sellRate = 1;
                                 }

                                 if (sellRate != null) {
                                     $scope.chargeTemp.localRateAmountRoe = sellRate;
                                     $scope.chargeTemp.localTotalRateAmount = $scope.chargeTemp.totalRateAmount * $scope.chargeTemp.localRateAmountRoe;
                                     $scope.chargeTemp.localTotalRateAmount = $rootScope.currency($scope.chargeTemp.rateCurrency, $scope.chargeTemp.localTotalRateAmount);

                                     $scope.dependData.rate = $scope.dependData.rate + $scope.chargeTemp.localTotalRateAmount;
                                 }


                             }

                         }

                         $scope.dependData.clientGrossRate = $rootScope.currency($scope.dependData.localCurrency, $scope.dependData.clientGrossRate);
                         $scope.dependData.clientNetRate = $rootScope.currency($scope.dependData.localCurrency, $scope.dependData.clientNetRate);
                         $scope.dependData.declaredCost = $rootScope.currency($scope.dependData.localCurrency, $scope.dependData.declaredCost);
                         $scope.dependData.cost = $rootScope.currency($scope.dependData.localCurrency, $scope.dependData.cost);
                         $scope.dependData.rate = $rootScope.currency($scope.dependData.localCurrency, $scope.dependData.rate);

                         //$scope.makeTableState();

                     }


                     $scope.secondArr = [];

                     $scope.totalNoOfPieces = 0;

                     // $scope.calculateSum = function(item){
                     //
                     // $scope.totalLocalAmt+=item.locamt?parseFloat(isNaN(item.locamt)?0:item.locamt):0;
                     //
                     // };

                     function initTable() {
                         $scope.emptyObjRef = {};
                         if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                             $scope.tableData = [];
                             $scope.rateObj = {};
                             $scope.rateObj.actualChargeable = 'CHARGEABLE';
                             $scope.tableData.push($scope.rateObj);
                             $scope.emptyObjRef = angular.copy($scope.rateObj);
                         } else {
                             if ($scope.tableData.length > 0) {
                                 $scope.rateObj = {};
                                 $scope.rateObj.actualChargeable = 'CHARGEABLE';
                                 $scope.emptyObjRef = angular.copy($scope.rateObj);
                                 //$scope.rateCalculation();

                             }
                         }

                         angular.forEach($scope.tableData, function(item) {
                             // $scope.calculateSum(item);
                             $scope.secondArr.push({ "errRow": false });
                         });


                     }

                     initTable();


                     $scope.addRow = function() {
                         console.log("add row called");
                         $scope.emptyObjRef = {};
                         var fineArr = [];
                         angular.forEach($scope.tableData, function(dataObj, index) {
                             // var curObj = $scope.tableData.data[index];
                             var validResult = validateObj(dataObj, index, "row");
                             if (validResult) {
                                 fineArr.push(1);
                                 $scope.secondArr[index].errRow = false;
                             } else {
                                 $scope.secondArr[index].errRow = true;
                                 fineArr.push(0);
                             }
                         });

                         if (fineArr.indexOf(0) < 0) {
                             if ($scope.tableData == undefined || $scope.tableData == null)
                                 $scope.tableData = [];
                             /*if($scope.tableData.length>0){
                              $scope.rateCalculation();
                              }*/
                             $scope.rateObj = {};
                             $scope.rateObj.actualChargeable = 'CHARGEABLE';
                             $scope.tableData.push($scope.rateObj);
                             $rootScope.navigateToNextField($scope.indexKey + 'chargeCode' + ($scope.tableData.length - 1))
                             $scope.emptyObjRef = angular.copy($scope.rateObj);
                             $scope.secondArr.push({ "errRow": false });
                             $scope.secondArr[$scope.tableData.length - 1].chargeFocus = true;
                             $scope.tableState({ param: false });
                             $scope.indexFocus = null;
                             console.log("set index on if condition");
                             /*$timeout(function(){
                              $scope.secondArr[$scope.tableData.length-1].chargeFocus = true;
                              },10);*/
                         }


                     };


                     var myOtherModal = $modal({ scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true });
                     var errorOnRowIndex = null;
                     $scope.errorShow = function(errorObj, index) {
                         $scope.errList = errorObj.errTextArr;
                         // Show when some event occurs (use $promise property to ensure
                         // the template has been loaded)
                         errorOnRowIndex = index;
                         myOtherModal.$promise.then(myOtherModal.show);
                     };

                     $scope.ajaxChargeEvent = function(object) {

                         console.log("ajaxChargeEvent ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.chargeMasterList = data.responseObject.searchResult;
                                     return $scope.chargeMasterList;
                                     /* $scope.showChargeMasterList=true; */
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Charge');
                             }
                         );
                     }

                     $scope.assignChargeMaster = function(obj) {
                         obj.chargeName = obj.chargeMaster.chargeName;
                         $scope.setDeafultValues(obj);
                     }
                     $scope.ajaxUnitEvent = function(object) {

                         console.log("ajaxUnitEvent ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.unitMasterList = data.responseObject.searchResult;
                                     return $scope.unitMasterList;
                                     // $scope.showUnitMasterList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Unit List');
                             }
                         );

                     }

                     $scope.ajaxGrossCurrency = function(object) {

                         console.log("ajaxGrossCurrency ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.grossCurrencyList = data.responseObject.searchResult;
                                     return $scope.grossCurrencyList;
                                     // $scope.showGrossCurrencyList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Currency List');
                             }
                         );

                     }
                     $scope.ajaxNetCurrency = function(object) {

                         console.log("ajaxNetCurrency ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.netCurrencyList = data.responseObject.searchResult;
                                     return $scope.netCurrencyList;
                                     // $scope.showNetCurrencyList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Currency List');
                             }
                         );

                     }

                     $scope.ajaxDeclaredCurrency = function(object) {

                         console.log("ajaxDeclaredCurrency ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.declaredCurrencyList = data.responseObject.searchResult;
                                     return $scope.declaredCurrencyList;
                                     // $scope.showDeclaredCurrencyList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Currency List');
                             }
                         );

                     }

                     $scope.ajaxRateCurrency = function(object) {

                         console.log("ajaxRateCurrency ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.rateCurrencyList = data.responseObject.searchResult;
                                     return $scope.rateCurrencyList;
                                     // $scope.showRateCurrencyList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Currency List');
                             }
                         );
                     }

                     $scope.ajaxCostCurrency = function(object) {

                         console.log("ajaxCostCurrency ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.costCurrencyList = data.responseObject.searchResult;
                                     return $scope.costCurrencyList;
                                     // $scope.showCostCurrencyList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Currency List');
                             }
                         );

                     }
                     $scope.ajaxVendorEvent = function(object) {

                         console.log("ajaxVendorEvent ", object);

                         $scope.searchDto = {};
                         $scope.searchDto.keyword = object == null ? "" : object;
                         $scope.searchDto.selectedPageNumber = 0;
                         $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                         return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                                 if (data.responseCode == "ERR0") {
                                     $scope.totalRecord = data.responseObject.totalRecord;
                                     $scope.vendorList = data.responseObject.searchResult;
                                     return $scope.vendorList;
                                     // $scope.showVendorList=true;
                                 }
                             },
                             function(errResponse) {
                                 console.error('Error while fetching Currency List');
                             }
                         );

                     }


                     $scope.setToDeclaredCostAmt = function(rate, cost) {

                         rate.declaredSaleAmount = cost;
                         $scope.rateCalculation();
                     }


                     $scope.setToDeclaredCostMin = function(rate, costMin) {
                         rate.declaredSaleMinimum = costMin;
                         $scope.rateCalculation();
                     }

                     $scope.setToClientGrossRateAmt = function(rate, cost) {

                         rate.grossSaleAmount = cost;
                         $scope.rateCalculation();
                     }


                     $scope.setToClientGrossRateAmtMin = function(rate, costMin) {
                         rate.grossSaleMinimum = costMin;
                         $scope.rateCalculation();
                     }



                     // //error validation rules
                     var validateObj = function(dataObj, index, type) {

                         var errorArr = [];
                         if (type == "row") {
                             $scope.secondArr[index] = {};
                             $scope.secondArr[index].errTextArr = [];
                         }

                         if (dataObj.chargeMaster == undefined ||
                             dataObj.chargeMaster == null ||
                             dataObj.chargeMaster.id == undefined ||
                             dataObj.chargeMaster.id == null) {


                             if (type == "row") {
                                 $scope.secondArr[index].chargeCode = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90300"]);
                                 dataObj.chargeName = '';
                             }
                             errorArr.push(0);


                         } else {

                             if (ValidateUtil.isStatusBlocked(dataObj.chargeMaster.status)) {
                                 console.log("chargeMaster", $rootScope.nls["ERR90301"]);
                                 if (type == "row") {
                                     $scope.secondArr[index].chargeCode = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90301"]);

                                 }
                                 errorArr.push(0);

                             } else if (ValidateUtil.isStatusHidden(dataObj.chargeMaster.status)) {
                                 console.log("chargeMaster", $rootScope.nls["ERR90302"]);
                                 if (type == "row") {
                                     $scope.secondArr[index].chargeCode = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90302"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].chargeCode = false;

                                 }
                                 errorArr.push(1);
                             }

                         }
                         if (dataObj.chargeName == undefined ||
                             dataObj.chargeName == null ||
                             dataObj.chargeName == "") {
                             console.log("chargeName", $rootScope.nls["ERR90303"]);
                             if (type == "row") {
                                 $scope.secondArr[index].chargeName = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90303"]);
                             }
                             errorArr.push(0);
                         } else {
                             if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charge_Name, dataObj.chargeName)) {
                                 console.log("chargeName invalid ", $rootScope.nls["ERR90304"])
                                 if (type == "row") {
                                     $scope.secondArr[index].chargeName = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90304"]);
                                 }
                                 errorArr.push(0);
                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].chargeName = false;
                                 }
                                 errorArr.push(1);
                             }
                         }
                         if (dataObj.unitMaster == undefined ||
                             dataObj.unitMaster == null ||
                             dataObj.unitMaster.id == undefined ||
                             dataObj.unitMaster.id == null) {

                             console.log("Please Select Unit....");
                             if (type == "row") {
                                 $scope.secondArr[index].unitName = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90305"]);
                             }
                             errorArr.push(0);


                         } else if (dataObj.unitMaster.status != null) {

                             if (ValidateUtil.isStatusBlocked(dataObj.unitMaster.status)) {


                                 if (type == "row") {
                                     $scope.secondArr[index].unitName = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90306"]);
                                 }
                                 errorArr.push(0);
                                 console.log("Selected unitMaster is Blocked");


                             } else if (ValidateUtil.isStatusHidden(dataObj.unitMaster.status)) {

                                 if (type == "row") {
                                     $scope.secondArr[index].unitName = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90307"]);
                                 }
                                 errorArr.push(0);

                                 console.log("Selected unitMaster is Hidden");


                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].unitName = false;

                                 }
                                 errorArr.push(1);
                             }

                         }

                         // currecny validations

                         if ($scope.rateMerged == false || $scope.rateMerged == "false") {

                             if (dataObj.grossSaleCurrency != undefined &&
                                 dataObj.grossSaleCurrency != null &&
                                 dataObj.grossSaleCurrency.id != undefined &&
                                 dataObj.grossSaleCurrency.id != null) {


                                 if (ValidateUtil.isStatusBlocked(dataObj.grossSaleCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].grossSaleCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90313"]);
                                     }
                                     errorArr.push(0);


                                 } else if (ValidateUtil.isStatusHidden(dataObj.grossSaleCurrency.status)) {


                                     if (type == "row") {
                                         $scope.secondArr[index].grossSaleCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90314"]);
                                     }
                                     errorArr.push(0);

                                 } else if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.grossSaleCurrency.id) {
                                     if ($rootScope.baseCurrenyRate[dataObj.grossSaleCurrency.id] == null) {

                                         if (type == "row") {
                                             $scope.secondArr[index].grossSaleCurrency = true;
                                             $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90345"]);
                                         }
                                         errorArr.push(0);

                                     }
                                 } else {

                                     if (type == "row") {
                                         $scope.secondArr[index].grossSaleCurrency = false;

                                     }
                                     errorArr.push(1);
                                 }

                             }

                         }


                         if (!isNaN(dataObj.grossSaleAmount) && dataObj.grossSaleAmount != undefined &&
                             dataObj.grossSaleAmount != null &&
                             dataObj.grossSaleAmount != "") {

                             if (isNaN(parseFloat(dataObj.grossSaleAmount))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90316"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.grossSaleCurrency != null && dataObj.grossSaleCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.grossSaleAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90316"]);
                                 }
                                 errorArr.push(0);
                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.grossSaleAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90316"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.grossSaleAmount == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90315"]);
                                 }
                                 errorArr.push(0);


                             } else if (dataObj.grossSaleAmount < 0 || dataObj.grossSaleAmount >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90316"]);
                                 }
                                 errorArr.push(0);
                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleAmount = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if (!isNaN(dataObj.grossSaleMinimum) && dataObj.grossSaleMinimum != undefined &&
                             dataObj.grossSaleMinimum != null &&
                             dataObj.grossSaleMinimum != "") {

                             if (isNaN(parseFloat(dataObj.grossSaleMinimum))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90318"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.grossSaleCurrency != null && dataObj.grossSaleCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.grossSaleMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90318"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.grossSaleMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90318"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.grossSaleMinimum == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90317"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.grossSaleMinimum < 0 || dataObj.grossSaleMinimum >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90318"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].grossSaleMinimum = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if ($scope.rateMerged == false || $scope.rateMerged == "false") {
                             if (dataObj.netSaleCurrency != undefined &&
                                 dataObj.netSaleCurrency != null &&
                                 dataObj.netSaleCurrency.id != undefined &&
                                 dataObj.netSaleCurrency.id != null) {

                                 if (ValidateUtil.isStatusBlocked(dataObj.netSaleCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].netSaleCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90319"]);
                                     }
                                     errorArr.push(0);

                                 } else if (ValidateUtil.isStatusHidden(dataObj.netSaleCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].netSaleCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90320"]);
                                     }
                                     errorArr.push(0);

                                     console.log("Selected netSaleCurrency is Hidden");

                                 } else if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.netSaleCurrency.id) {
                                     if ($rootScope.baseCurrenyRate[dataObj.netSaleCurrency.id] == null) {
                                         if (type == "row") {
                                             $scope.secondArr[index].netSaleCurrency = true;
                                             $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90345"]);
                                         }
                                         errorArr.push(0);

                                     }
                                 } else {
                                     if (type == "row") {
                                         $scope.secondArr[index].netSaleCurrency = false;

                                     }
                                     errorArr.push(1);
                                 }
                             }

                         }


                         if (!isNaN(dataObj.netSaleAmount) && dataObj.netSaleAmount != undefined &&
                             dataObj.netSaleAmount != null &&
                             dataObj.netSaleAmount != "") {
                             if (isNaN(parseFloat(dataObj.netSaleAmount))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90322"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.netSaleCurrency != null && dataObj.netSaleCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.netSaleAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90322"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.netSaleAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90322"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.netSaleAmount == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90321"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.netSaleAmount < 0 || dataObj.netSaleAmount >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90322"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleAmount = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if (!isNaN(dataObj.netSaleMinimum) && dataObj.netSaleMinimum != undefined &&
                             dataObj.netSaleMinimum != null &&
                             dataObj.netSaleMinimum != "") {
                             if (isNaN(parseFloat(dataObj.netSaleMinimum))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90324"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.netSaleCurrency != null && dataObj.netSaleCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.netSaleMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90324"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.netSaleMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90324"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.netSaleMinimum == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90323"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.netSaleMinimum < 0 || dataObj.netSaleMinimum >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90324"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].netSaleMinimum = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if ($scope.rateMerged == false || $scope.rateMerged == "false") {
                             if (dataObj.declaredSaleCurrency != undefined &&
                                 dataObj.declaredSaleCurrency != null &&
                                 dataObj.declaredSaleCurrency.id != undefined &&
                                 dataObj.declaredSaleCurrency.id != null) {

                                 if (ValidateUtil.isStatusBlocked(dataObj.declaredSaleCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].declaredSaleCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90325"]);
                                     }
                                     errorArr.push(0);


                                 } else if (ValidateUtil.isStatusHidden(dataObj.declaredSaleCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].declaredSaleCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90326"]);
                                     }
                                     errorArr.push(0);


                                 } else if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.declaredSaleCurrency.id) {
                                     if ($rootScope.baseCurrenyRate[dataObj.declaredSaleCurrency.id] == null) {
                                         if (type == "row") {
                                             $scope.secondArr[index].declaredSaleCurrency = true;
                                             $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90345"]);
                                         }
                                         errorArr.push(0);

                                     }
                                 } else {
                                     if (type == "row") {
                                         $scope.secondArr[index].declaredSaleCurrency = false;

                                     }
                                     errorArr.push(1);
                                 }

                             }

                         }


                         if (!isNaN(dataObj.declaredSaleAmount) && dataObj.declaredSaleAmount != undefined &&
                             dataObj.declaredSaleAmount != null &&
                             dataObj.declaredSaleAmount != "") {
                             if (isNaN(parseFloat(dataObj.declaredSaleAmount))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90328"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.declaredSaleCurrency != null && dataObj.declaredSaleCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.declaredSaleAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90328"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.declaredSaleAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90328"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.declaredSaleAmount == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90327"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.declaredSaleAmount < 0 || dataObj.declaredSaleAmount >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90328"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleAmount = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if (!isNaN(dataObj.declaredSaleMinimum) && dataObj.declaredSaleMinimum != undefined &&
                             dataObj.declaredSaleMinimum != null &&
                             dataObj.declaredSaleMinimum != "") {

                             if (isNaN(parseFloat(dataObj.declaredSaleMinimum))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90330"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.declaredSaleCurrency != null && dataObj.declaredSaleCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.declaredSaleMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90330"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.declaredSaleMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90330"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.declaredSaleMinimum == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90329"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.declaredSaleMinimum < 0 || dataObj.declaredSaleMinimum >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90330"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].declaredSaleMinimum = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if ($scope.rateMerged || $scope.rateMerged == "true") {
                             if (dataObj.rateCurrency != undefined &&
                                 dataObj.rateCurrency != null &&
                                 dataObj.rateCurrency.id != undefined &&
                                 dataObj.rateCurrency.id != null) {

                                 if (ValidateUtil.isStatusBlocked(dataObj.rateCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].rateCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90331"]);
                                     }
                                     errorArr.push(0);


                                 } else if (ValidateUtil.isStatusHidden(dataObj.rateCurrency.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].rateCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90332"]);
                                     }
                                     errorArr.push(0);


                                 } else if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.rateCurrency.id) {
                                     if ($rootScope.baseCurrenyRate[dataObj.rateCurrency.id] == null) {
                                         if (type == "row") {
                                             $scope.secondArr[index].rateCurrency = true;
                                             $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90345"]);
                                         }
                                         errorArr.push(0);

                                     }
                                 } else {
                                     if (type == "row") {
                                         $scope.secondArr[index].rateCurrency = false;

                                     }
                                     errorArr.push(1);
                                 }

                             }


                         }
                         if (!isNaN(dataObj.rateAmount) && dataObj.rateAmount != undefined &&
                             dataObj.rateAmount != null &&
                             dataObj.rateAmount != "") {
                             if (isNaN(parseFloat(dataObj.rateAmount))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90334"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.rateCurrency != null && dataObj.rateCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.rateAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90334"]);
                                 }
                                 errorArr.push(0);
                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.rateAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90334"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.rateAmount == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90333"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.rateAmount < 0 || dataObj.rateAmount >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90334"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateAmount = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if (!isNaN(dataObj.rateMinimum) && dataObj.rateMinimum != undefined &&
                             dataObj.rateMinimum != null &&
                             dataObj.rateMinimum != "") {


                             if (isNaN(parseFloat(dataObj.rateMinimum))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90336"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.rateCurrency != null && dataObj.rateCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.rateMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90336"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.rateMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90336"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.rateMinimum == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90335"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.rateMinimum < 0 || dataObj.rateMinimum >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90336"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].rateMinimum = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if (dataObj.costCurrency != undefined &&
                             dataObj.costCurrency != null &&
                             dataObj.costCurrency.id != undefined &&
                             dataObj.costCurrency.id != null) {

                             if (ValidateUtil.isStatusBlocked(dataObj.costCurrency.status)) {

                                 if (type == "row") {
                                     $scope.secondArr[index].costCurrency = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90337"]);
                                 }
                                 errorArr.push(0);
                             } else if (ValidateUtil.isStatusHidden(dataObj.costCurrency.status)) {

                                 if (type == "row") {
                                     $scope.secondArr[index].costCurrency = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90338"]);
                                 }
                                 errorArr.push(0);

                             } else if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.costCurrency.id) {
                                 if ($rootScope.baseCurrenyRate[dataObj.costCurrency.id] == null) {

                                     var st = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + " -> " + dataObj.costCurrency.currencyCode;
                                     if (type == "row") {
                                         $scope.secondArr[index].costCurrency = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90345"]);
                                     }
                                     errorArr.push(0);

                                 }
                             } else {

                                 if (type == "row") {
                                     $scope.secondArr[index].costCurrency = false;

                                 }
                                 errorArr.push(1);
                             }

                         }


                         if (!isNaN(dataObj.costAmount) && dataObj.costAmount != undefined &&
                             dataObj.costAmount != null &&
                             dataObj.costAmount != "") {
                             if (isNaN(parseFloat(dataObj.costAmount))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90340"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.costCurrency != null && dataObj.costCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.costAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90340"]);
                                 }
                                 errorArr.push(0);
                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.costAmount)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90340"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.costAmount == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90339"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.costAmount < 0 || dataObj.costAmount >= 99999999999.999) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costAmount = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90340"]);
                                 }
                                 errorArr.push(0);

                             } else {

                                 if (type == "row") {
                                     $scope.secondArr[index].costAmount = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if (!isNaN(dataObj.costMinimum) && dataObj.costMinimum != undefined &&
                             dataObj.costMinimum != null &&
                             dataObj.costMinimum != "") {


                             if (isNaN(parseFloat(dataObj.costMinimum))) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90342"]);
                                 }

                             } else if (dataObj.costCurrency != null && dataObj.costCurrency.decimalPoint == "2" &&
                                 !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.costMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90342"]);
                                 }
                                 errorArr.push(0);

                             } else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.costMinimum)) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90342"]);
                                 }
                                 errorArr.push(0);
                             } else if (dataObj.costMinimum == 0) {
                                 if (type == "row") {
                                     $scope.secondArr[index].costMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90341"]);
                                 }
                                 errorArr.push(0);

                             } else if (dataObj.costMinimum < 0 || dataObj.costMinimum >= 99999999999.999) {

                                 if (type == "row") {
                                     $scope.secondArr[index].costMinimum = true;
                                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90342"]);
                                 }
                                 errorArr.push(0);

                             } else {
                                 if (type == "row") {
                                     $scope.secondArr[index].costMinimum = false;

                                 }
                                 errorArr.push(1);
                             }
                         }


                         if ($scope.rateMerged || $scope.rateMerged == "true") {

                             if (dataObj.vendor == undefined ||
                                 dataObj.vendor == null ||
                                 dataObj.vendor == "" ||
                                 dataObj.vendor.id == null) {


                             } else if (dataObj.vendor.status != null) {
                                 if (ValidateUtil.isStatusBlocked(dataObj.vendor.status)) {

                                     if (type == "row") {
                                         $scope.secondArr[index].vendor = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90343"]);
                                     }
                                     errorArr.push(0);


                                 } else if (ValidateUtil.isStatusHidden(dataObj.vendor.status)) {
                                     if (type == "row") {
                                         $scope.secondArr[index].vendor = true;
                                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90344"]);
                                     }
                                     errorArr.push(0);

                                 } else {
                                     if (type == "row") {
                                         $scope.secondArr[index].vendor = false;

                                     }
                                     errorArr.push(1);
                                 }
                             }
                         }

                         if (errorArr.indexOf(0) < 0) {
                             return true
                         } else {
                             return false;
                         }

                     };


                     function changeLastStatus() {
                         angular.forEach($scope.tableData, function(dataObj, index) {
                             // var curObj = $scope.tableData.data[index];
                             var validResult = validateObj(dataObj, index, "row");
                             $scope.secondArr[index].errRow = false;
                         });
                     }

                     $scope.dirtyIndex = function(index) {
                         $scope.indexFocus = index;
                     };

                     $(document).on('keydown', function(e) {
                         if (e.keyCode == 27) {
                             if (myOtherModal) {
                                 myOtherModal.$promise.then(myOtherModal.hide);
                             }
                         }
                     });

                     $scope.$on('modal.hide', function(e, $modal) {
                         if ($($modal.$element).attr("id") == "rateMore") {
                             $scope.addRow();
                         } else {
                             if ($scope.secondArr[errorOnRowIndex].chargeFocus) {
                                 $scope.secondArr[errorOnRowIndex].chargeFocus = false;
                             }
                             $timeout(function() {
                                 $scope.secondArr[errorOnRowIndex].chargeFocus = true;
                                 errorOnRowIndex = null;
                             }, 10)
                         }
                     });


                     $scope.$watch('tableData', function(newValue, oldValue) {
                         if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                             initTable();
                         }
                     });


                     $scope.$watch('indexFocus', function(newvalue, oldValue) {
                         if (newvalue != null && oldValue != null && newvalue != oldValue) {
                             var focusErrArr = [];
                             angular.forEach($scope.tableData, function(dataObj, index) {
                                 // var curObj = $scope.tableData.data[index];
                                 var validResult = validateObj(dataObj, index, "row");
                                 if (validResult) {
                                     $scope.secondArr[index].errRow = false;
                                     focusErrArr.push(1);
                                 } else {
                                     $scope.secondArr[index].errRow = true;
                                     focusErrArr.push(0);

                                 }
                                 // console.log();
                             });
                             if (focusErrArr.indexOf(0) < 0) {
                                 if ($scope.tableState) {
                                     $scope.tableState({ param: true });
                                 }
                             } else {
                                 if ($scope.tableState) {
                                     $scope.tableState({ param: false });
                                 }
                             }
                         }
                     });

                     $scope.chargeChange = function(obj) {
                         obj.chargeName = obj.chargeMaster.chargeName
                     }

                     $scope.removeObj = function(obj, index) {
                         $scope.tableData.splice(index, 1);
                         $scope.secondArr.splice(index, 1);
                         $scope.indexFocus = null;
                         //$scope.makeTableState();
                         $scope.rateCalculation();
                     };

                     $scope.openEvent = function(param) {
                         console.log("value", param);
                         if (param == true) {
                             $scope.dropStyle = { "overflow-x": "hidden" };
                         } else {
                             $scope.dropStyle = { "overflow-x": "auto", "overflow-y": "hidden" };
                         }
                     }
                     $scope.chargeRender = function(item) {
                         return {
                             label: item.charge,
                             item: item
                         }
                     }
                     $scope.unitRender = function(item) {
                         return {
                             label: item.unitName,
                             item: item
                         }
                     }
                     $scope.currencyRender = function(item) {
                         return {
                             label: item.currencyCode,
                             item: item
                         }
                     }
                     $scope.partyRender = function(item) {
                         return {
                             label: item.partyName,
                             item: item
                         }
                     }

                     $scope.setCurrency = function(charge) {
                         var currency = null;
                         if (charge.chargeMaster.chargeType == 'Origin') {
                             if ($scope.dependData.origin != undefined && $scope.dependData.origin != null) {
                                 currency = $scope.dependData.origin.portGroupMaster.countryMaster.currencyMaster;
                             }
                         } else if (charge.chargeMaster.chargeType == 'Destination') {
                             if ($scope.dependData.destination != undefined && $scope.dependData.destination != null) {
                                 currency = $scope.dependData.destination.portGroupMaster.countryMaster.currencyMaster;
                             }
                         } else if (charge.chargeMaster.chargeType == 'Freight') {
                             if ($scope.dependData.origin != undefined && $scope.dependData.origin != null) {
                                 currency = $scope.dependData.origin.portGroupMaster.countryMaster.currencyMaster;
                             }
                         } else {
                             currency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                         }





                         if (charge.chargeMaster != undefined && charge.chargeMaster != null && charge.chargeMaster != "") {
                             if (charge.grossSaleCurrency == undefined || charge.grossSaleCurrency == null || charge.grossSaleCurrency == "" && ($scope.rateMerged == false || $scope.rateMerged == "false"))
                                 charge.grossSaleCurrency = currency;

                             if (charge.netSaleCurrency == undefined || charge.netSaleCurrency == null || charge.netSaleCurrency == "" && ($scope.rateMerged == false || $scope.rateMerged == "false"))
                                 charge.netSaleCurrency = currency;

                             if (charge.declaredSaleCurrency == undefined || charge.declaredSaleCurrency == null || charge.declaredSaleCurrency == "" && ($scope.rateMerged == false || $scope.rateMerged == "false"))
                                 charge.declaredSaleCurrency = currency;

                             if (charge.rateCurrency == undefined || charge.rateCurrency == null || charge.rateCurrency == "" && ($scope.rateMerged || $scope.rateMerged == "true"))
                                 charge.rateCurrency = currency;

                             if (charge.costCurrency == undefined || charge.costCurrency == null || charge.costCurrency == "")
                                 charge.costCurrency = currency;
                         }
                     }

                     $scope.setToClientNetRate = function(charge, value) {

                         if (charge != undefined && value == 'amount') {
                             charge.netSaleAmount = charge.grossSaleAmount;
                         }
                         if (charge != undefined && value == 'min') {
                             charge.netSaleMinimum = charge.grossSaleMinimum;
                         }

                         $scope.rateCalculation();
                     }


                     $scope.changeCollectOrPrepaid = function(charge) {
                         if (charge.chargeMaster != undefined && charge.chargeMaster != null && charge.chargeMaster != "" && charge.chargeMaster.status != 'Block') {
                             if (charge.chargeMaster.chargeType == 'Origin')
                                 charge.ppcc = 'Prepaid';
                             else if (charge.chargeMaster.chargeType == 'Destination')
                                 charge.ppcc = 'Collect';
                             else if (charge.chargeMaster.chargeType == 'Freight') {
                                 if ($scope.dependData.ppcc == false || $scope.dependData.ppcc == 'Prepaid') {
                                     charge.ppcc = 'Prepaid';
                                 } else {
                                     charge.ppcc = 'Collect';
                                 }
                             } else if ($scope.dependData != null && $scope.dependData != undefined &&
                                 $scope.dependData.tosMaster != null &&
                                 $scope.dependData.tosMaster != undefined &&
                                 $scope.dependData.tosMaster != '') {
                                 charge.ppcc = $scope.dependData.tosMaster.freightPPCC;
                             }

                             if ($scope.dependData.serviceMaster.importExport === 'Import') {
                                 //As discussed with suresh and QC team, If Import shipment, default value always collect for all charges
                                 charge.ppcc = 'Collect';
                             }

                         } else {
                             charge.ppcc = null;
                         }
                     }


                     $scope.ratePopup = $modal({ scope: $scope, templateUrl: 'ratesMore.html', backdrop: 'static', show: false, keyboard: true });

                     $scope.ratespopup = function(index) {
                         $scope.noteIndex = index;
                         $scope.ratePopup.$promise.then($scope.ratePopup.show);
                         // $scope.popUpNote = $scope.tableData[$scope.noteIndex].note ? $scope.tableData[$scope.noteIndex].note : "";
                         // $scope.popUpParty = $scope.tableData[$scope.noteIndex].vendor ? $scope.tableData[$scope.noteIndex].vendor : null;
                     };


                     $scope.save = function(rateParty, note) {
                         /*  if ($scope.tableData != undefined && $scope.tableData != null) {
                           	$scope.tableData[$scope.noteIndex].vendor = rateParty;
                               $scope.tableData[$scope.noteIndex].note = note;
                           }*/
                         $scope.ratePopup.$promise.then($scope.ratePopup.hide);
                     }

                     $scope.getRatesFromQuotaion = function() {
                         $rootScope.clientMessage = null;
                         if ($scope.dependData.quotationUid != undefined && $scope.dependData.quotationUid != null && $scope.dependData.quotationUid != "") {
                             QuotationViewByQuotationNo.get({
                                 quotationNumber: $scope.dependData.quotationUid
                             }, function(data) {
                                 if (data.responseCode == 'ERR0') {
                                     $scope.quotation = data.responseObject;
                                     if ($scope.quotation.quotationDetailList != null && $scope.quotation.quotationDetailList.length > 0) {
                                         for (var i = 0; i < $scope.quotation.quotationDetailList.length; i++) {
                                             if ($scope.quotation.quotationDetailList[i].quotationChargeList != null && $scope.quotation.quotationDetailList[i].quotationChargeList.length != 0) {
                                                 if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData == "") {
                                                     $scope.tableData[$scope.tableData.length] = {};
                                                 }
                                                 for (var j = 0; j < $scope.quotation.quotationDetailList[i].quotationChargeList.length; j++) {

                                                     var rateObj = {};
                                                     rateObj.chargeMaster = $scope.quotation.quotationDetailList[i].quotationChargeList[j].chargeMaster;
                                                     rateObj.chargeName = $scope.quotation.quotationDetailList[i].quotationChargeList[j].chargeName;
                                                     rateObj.unitMaster = $scope.quotation.quotationDetailList[i].quotationChargeList[j].unitMaster;

                                                     if (rateObj.chargeMaster.chargeType == "Origin") {
                                                         rateObj.ppcc = "Prepaid";
                                                     } else if (rateObj.chargeMaster.chargeType == "Destination" || rateObj.chargeMaster.chargeType == "Freight") {
                                                         rateObj.ppcc = "Collect";
                                                     } else if (rateObj.chargeMaster.chargeType == "Other") {
                                                         if ($scope.quotation.quotationDetailList[i].serviceMaster.importExport == 'Import') {
                                                             rateObj.ppcc = 'Prepaid';
                                                         } else {
                                                             rateObj.ppcc = 'Collect';
                                                         }
                                                     }


                                                     rateObj.numberOfUnit = $scope.quotation.quotationDetailList[i].quotationChargeList[j].numberOfUnit;

                                                     rateObj.grossSaleCurrency = $scope.quotation.quotationDetailList[i].quotationChargeList[j].currencyMaster;
                                                     rateObj.grossSaleAmount = $scope.quotation.quotationDetailList[i].quotationChargeList[j].sellRateAmountPerUnit;
                                                     rateObj.grossSaleMinimum = $scope.quotation.quotationDetailList[i].quotationChargeList[j].sellRateMinAmount;
                                                     rateObj.netSaleCurrency = $scope.quotation.quotationDetailList[i].quotationChargeList[j].currencyMaster;
                                                     rateObj.netSaleAmount = $scope.quotation.quotationDetailList[i].quotationChargeList[j].sellRateAmountPerUnit;
                                                     rateObj.netSaleMinimum = $scope.quotation.quotationDetailList[i].quotationChargeList[j].sellRateMinAmount;
                                                     rateObj.declaredSaleCurrency = $scope.quotation.quotationDetailList[i].quotationChargeList[j].currencyMaster;
                                                     rateObj.declaredSaleAmount = $scope.quotation.quotationDetailList[i].quotationChargeList[j].buyRateCostPerUnit;
                                                     rateObj.declaredSaleMinimum = $scope.quotation.quotationDetailList[i].quotationChargeList[j].buyRateMinCost;

                                                     rateObj.rateCurrency = $scope.quotation.quotationDetailList[i].quotationChargeList[j].currencyMaster;
                                                     rateObj.rateAmount = $scope.quotation.quotationDetailList[i].quotationChargeList[j].sellRateAmountPerUnit;
                                                     rateObj.rateMinimum = $scope.quotation.quotationDetailList[i].quotationChargeList[j].sellRateMinAmount;


                                                     rateObj.costCurrency = $scope.quotation.quotationDetailList[i].quotationChargeList[j].currencyMaster;
                                                     rateObj.costAmount = $scope.quotation.quotationDetailList[i].quotationChargeList[j].buyRateCostPerUnit;
                                                     rateObj.costMinimum = $scope.quotation.quotationDetailList[i].quotationChargeList[j].buyRateMinCost;
                                                     rateObj.actualChargeable = $scope.quotation.quotationDetailList[i].quotationChargeList[j].actualchargeable;
                                                     $scope.tableData.push(rateObj);
                                                 }
                                             } else {
                                                 $rootScope.clientMessage = $rootScope.nls["ERR22158"];
                                             }

                                         }
                                     }


                                 }

                             }, function(error) {
                                 console.log("Error while fetching charges");
                             });
                         } else {
                             $rootScope.clientMessage = $rootScope.nls["ERR22156"];
                         }

                     }

                     $scope.isNumberOfUnitsMapped = false;
                     $scope.unitSelected = "";
                     $scope.moveToNextField = function(nextIdValue, index, numberOfUnits) {
                         $scope.highlightSeletedRates(numberOfUnits);
                         console.log("id : ", nextIdValue)
                         $rootScope.navigateToNextField(nextIdValue + index);

                     };

                     $scope.highlightSeletedRates = function(numberOfUnits) {
                         if (numberOfUnits) {

                             var chWt = $scope.shipment.shipmentServiceList[0].documentList[0].chargebleWeight;


                             if ((chWt > 0 && chWt <= 45) && numberOfUnits == "-45") {
                                 $scope.isNumberOfUnitsMapped = true;
                                 $scope.unitSelected = "-45";
                             }


                             if ((chWt > 45 && chWt <= 100) && numberOfUnits == "+45") {
                                 $scope.isNumberOfUnitsMapped = true;
                                 $scope.unitSelected = "+45";
                             }


                             if ((chWt > 100 && chWt <= 250) && numberOfUnits == "+100") {
                                 $scope.isNumberOfUnitsMapped = true;
                                 $scope.unitSelected = "+100";
                             }


                             if ((chWt > 250 && chWt <= 500) && numberOfUnits == "+250") {
                                 $scope.isNumberOfUnitsMapped = true;
                                 $scope.unitSelected = "+250";
                             }


                             if ((chWt > 500) && numberOfUnits == "+500") {
                                 $scope.isNumberOfUnitsMapped = true;
                                 $scope.unitSelected = "+500";
                             }
                         }
                     };

                     angular.forEach($scope.tableData, function(val) {
                             $scope.highlightSeletedRates(val.numberOfUnit);
                         });

                     $scope.showHistory = function(id) {

                         /*var searchDto={};
				searchDto.selectedPageNumber =$scope.page;
				searchDto.recordPerPage =$scope.limit;*/
                         var auditArr = [];
                         $http({
                             method: 'GET',
                             url: '/api/v1/shipment/getshipmentchargehistory/' + id
                         }).then(function(response) {
                             if (response.data.responseCode == "ERR0") {
                                 $scope.auditArr = [];
                                 $scope.auditArr = response.data.responseObject;
                                 var chargeModal;
                                 chargeModal = $modal({
                                     scope: $scope,
                                     backdrop: 'static',
                                     templateUrl: '/app/shared/inlineEditTables/shipment_Rates/shipment_rate_history.html',
                                     show: false
                                 });
                                 chargeModal.$promise.then(chargeModal.show)
                             } else if (response.data.responseCode == "ERR0700012") {
                                 Notification.error($rootScope.nls["ERR0700012"]);
                             }
                         });
                     }

                     $scope.clearChargeName = function(charge) {
                         if (charge.chargeMaster == undefined || charge.chargeMaster == null || charge.chargeMaster == "") {
                             charge.chargeName = "";
                         }
                     }


                     $scope.setDeafultValues = function(charge) {

                         if (charge.chargeMaster != undefined && charge.chargeMaster != null && charge.chargeMaster != "") {

                             if (charge.chargeMaster.calculationType != undefined && charge.chargeMaster.calculationType != null) {
                                 if (charge.chargeMaster.calculationType == 'Percentage') {
                                     UnitMasterGetByCode.get({
                                         unitCode: 'PER'
                                     }).$promise.then(function(resObj) {
                                         if (resObj.responseCode == "ERR0") {
                                             charge.unitMaster = resObj.responseObject;
                                         }
                                     }, function(error) {
                                         console.log("unit Get Failed : " + error)
                                     });

                                 }
                                 if (charge.chargeMaster.calculationType == 'Shipment') {
                                     UnitMasterGetByCode.get({
                                         unitCode: 'SHPT'
                                     }).$promise.then(function(resObj) {
                                         if (resObj.responseCode == "ERR0") {
                                             charge.unitMaster = resObj.responseObject;
                                         }
                                     }, function(error) {
                                         console.log("unit Get Failed : " + error)
                                     });

                                 }
                                 if (charge.chargeMaster.calculationType == 'Unit') {
                                     UnitMasterGetByCode.get({
                                         unitCode: 'KG'
                                     }).$promise.then(function(resObj) {
                                         if (resObj.responseCode == "ERR0") {
                                             charge.unitMaster = resObj.responseObject;
                                         }
                                     }, function(error) {
                                         console.log("unit Get Failed : " + error)
                                     });

                                 }
                                 if (charge.chargeMaster.calculationType == 'Document') {
                                     UnitMasterGetByCode.get({
                                         unitCode: 'DOC'
                                     }).$promise.then(function(resObj) {
                                         if (resObj.responseCode == "ERR0") {
                                             charge.unitMaster = resObj.responseObject;
                                         }
                                     }, function(error) {
                                         console.log("unit Get Failed : " + error)
                                     });
                                 }
                             }

                         } else {
                             charge.currencyMaster = "";
                             charge.unitMaster = "";
                         }
                     }


                     $scope.selectServiceRates = function(charge, unitKey, NoOfUnits, index) {
                         $scope.setCurrency(charge);
                         $scope.changeCollectOrPrepaid(charge);
                         $scope.assignChargeMaster(charge);
                         $scope.secondArr[index].chargeFocus = false;
                         if (charge.unitMaster == undefined || charge.unitMaster == null || charge.unitMaster == "") {
                             $scope.moveToNextField($scope.indexKey + unitKey, index);
                         } else {
                             $scope.moveToNextField($scope.indexKey + NoOfUnits, index);
                         }

                     }


                     $scope.copy = $rootScope.copy;
                     $scope.copyObj = function(copyObj) {
                         var tmpObj = angular.copy(copyObj);
                         tmpObj.id = null;
                         tmpObj.quotationId = null;
                         tmpObj.systemTrack = null;
                         tmpObj.versionLock = 0;
                         if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                             $scope.tableData = [];
                         } else {
                             var tmpIndex = $scope.tableData.length - 1;
                             if (isEmptyRow($scope.tableData[tmpIndex])) {
                                 $scope.tableData.splice(tmpIndex, 1);
                                 delete $scope.tableData[tmpIndex]

                                 if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                                     $scope.tableData = [];
                                 }
                             }
                         }
                         $scope.tableData.push(tmpObj);
                     }

                     $scope.goToNext = function(id) {
                         if (id != undefined)
                             $rootScope.navigateToNextField(id);
                     }


                     /***** GetShiprite Table More Info Popup  *****/
                     var myRateModel = $modal({
                         scope: $scope,
                         templateUrl: '/app/shared/inlineEditTables/shipment_Rates/rates_shiprite.html',
                         show: false
                     });
                     $scope.getshipRite = function() {
                         console.log("Shipe");
                         myRateModel.$promise.then(myRateModel.show);
                     };
                     $scope.delete = $rootScope.delete;
                     $scope.errorList = $rootScope.errorList;
                 }, 2);
             }
         }
     }
 ]);