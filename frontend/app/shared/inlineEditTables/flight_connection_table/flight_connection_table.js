/**
 * Created by saravanan on 23/6/16.
 */
app.directive('flightConnectionTable',['$timeout','$compile','$rootScope','ValidateUtil',
        'PortByTransportMode','CarrierByTransportMode','appConstant','CommonValidationService',
        function($timeout,$compile,$rootScope,ValidateUtil,
		PortByTransportMode,CarrierByTransportMode,appConstant,CommonValidationService){

    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/flight_connection_table/flight_connection_table.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            tableState:"&",
            tableCheck:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){

            
            $scope.directiveDateTimePickerOpts = {
                    format:$rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                    useCurrent:false,
                    sideBySide:true,
                    viewDate:moment()
                };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";

            $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";

            $scope.provisionalObj = {};

            $scope.dateTimePickerOptionsLeft = {
                    format:$rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                    useCurrent:false,
                    sideBySide:true,
                    viewDate:moment()
                };
            $scope.dateTimePickerOptionsLeft.widgetParent = "body";
            $scope.dateTimePickerOptionsLeft.widgetPositioning = {
                horizontal: 'right'
            };

        	
        	$timeout(function() {
            	$scope.dataObj = {};

                $scope.connection = {};


                $scope.indexFocus = null;

                $scope.secondArr = [];

                $scope.selectedPolName = null;


                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {

                        if (isEmptyRow(dataObj) && $scope.tableData.length == 1) {
                            validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                $scope.secondArr[index].errRow = true;
                                fineArr.push(0);
                            }
                        } else {

                            $scope.secondArr[index] = {};
                            if (isEmptyRow(dataObj)) {
                                validResult = true;
                                delete  $scope.tableData[index];
                            } else {
                                validResult = validateObj(dataObj, index, "row");
                                finalData.push(dataObj);
                            }
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                $scope.secondArr[index].errRow = true;
                                fineArr.push(0);
                            }

                        }

                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }


                };

                function initTable() {

                    console.log("initTable is called TableData = ", $scope.tableData);

                    angular.forEach($scope.tableData, function (item) {

                        $scope.secondArr.push({"errRow": false});

                    });

                }

                initTable();

                $scope.makeTableState = function () {
                    var fineArr = [];
                    $timeout(function () {
                        angular.forEach($scope.tableData, function (dataObj, index) {
                            var validResult = validateObj(dataObj, index, "change");
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;

                            } else {
                                fineArr.push(0);
                                $scope.secondArr[index].errRow = true;
                            }
                        });
                        console.log("fine arr", fineArr);
                        if (fineArr.indexOf(0) < 0) {
                            if ($scope.tableState) {
                                changeLastStatus();
                                $scope.tableState({param: true});
                            }
                        } else {
                            if ($scope.tableState) {
                                $scope.tableState({param: false});
                            }
                        }
                    }, 10);

                };

                $scope.$watch('showErrors', function (newValue, oldValue) {

                    console.log("show Errors", newValue, oldValue);

                    if (newValue) {

                        var focusErrArr = [];

                        angular.forEach($scope.tableData, function (dataObj, index) {

                            var validResult = validateObj(dataObj, index, "row");

                            if (validResult) {

                                $scope.secondArr[index].errRow = false;

                                focusErrArr.push(1);

                            } else {

                                $scope.secondArr[index].errRow = true;

                                focusErrArr.push(0);

                            }
                        });

                        if (focusErrArr.indexOf(0) < 0) {

                            $scope.showErrors = false;

                        } else {

                            $scope.showErrors = true;

                        }
                    }
                });


                $scope.addRow = function () {

                    var fineArr = [];

                    angular.forEach($scope.tableData, function (dataObj, index) {

                        var validResult = validateObj(dataObj, index, "row");

                        if (validResult) {

                            fineArr.push(1);

                            $scope.secondArr[index].errRow = false;

                        } else {

                            fineArr.push(0);

                            $scope.secondArr[index].errRow = true;
                        }

                    });

                    if (fineArr.indexOf(0) < 0) {

                        $scope.tableData.push({});

                        $scope.secondArr.push({"errRow": false});

                        $scope.indexFocus = null;
                        $scope.secondArr[$scope.tableData.length - 1].carrierName = true;
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }

                    } else {

                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }

                    }
                };


                // To display the carrier
                $scope.ajaxCarrierEvent = function (object) {
                    console.log("Displaying List of Carrier....");
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return CarrierByTransportMode.fetch({"transportMode": 'Air'}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.carrierMasterList = data.responseObject.searchResult;
                                console.log("List of Carrier : ", $scope.carrierMasterList);
                                return $scope.carrierMasterList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Carrier List');
                        }
                    );

                }

                // To display the pol
                $scope.ajaxPolEvent = function (object) {
                    console.log("Displaying List of Ports....");
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PortByTransportMode.fetch({"transportMode": 'Air'}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                var resultList = data.responseObject.searchResult;
                                console.log("List of Ports : ", resultList);
                                if (resultList != null && resultList.length != 0) {
                                    $scope.polList = resultList;
                                }
                                return $scope.polList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching PortOfLoading List');
                        }
                    );

                }

                $scope.removeSamePortInPod = function (param, nextId) {

                    console.log("removeSamePortInPod method called : " + param.portName);

                    if (param.portName != undefined && param.portName != null && param.portName != "") {

                        $scope.selectedPolName = param.portName;
                        $rootScope.navigateToNextField(nextId);
                    }


                }

                // To display the pod
                $scope.ajaxPodEvent = function (object) {
                    console.log("Displaying List of Ports....");
                    console.log("$scope.selectedPolName : " + $scope.selectedPolName);
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PortByTransportMode.fetch({"transportMode": 'Air'}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                var resultList = data.responseObject.searchResult;
                                console.log("List of Ports before removing : ", resultList);
                                if (resultList != null && resultList.length != 0) {

                                    for (var int = 0; int < resultList.length; int++) {
                                        if ($scope.selectedPolName != undefined && $scope.selectedPolName != null && $scope.selectedPolName != "") {
                                            if (resultList[int].portName == $scope.selectedPolName) {

                                                var index = int;
                                                resultList.splice(index, 1);

                                            }

                                        }

                                    }
                                    console.log("List of Ports after removing : ", resultList);
                                    $scope.podList = resultList;
                                }
                                return $scope.podList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching PortOfDischarge List');
                        }
                    );

                }


                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                //error validation rules
                var validateObj = function (dataObj, index, type) {

                    $scope.secondArr[index] = {};
                    console.log("Validate ", dataObj);
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr = [];
                        $scope.secondArr[index] = {};
                        $scope.secondArr[index].errTextArr = [];
                    }
                    // Validating Carrier
                    if (dataObj.carrierMaster == undefined
                        || dataObj.carrierMaster == null
                        || dataObj.carrierMaster.id == null) {

                        if (type == "row") {
                            $scope.secondArr[index].carrierName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99501"]);
                            console.log($rootScope.nls["ERR99501"]);
                        }
                        errorArr.push(0);
                    }
                    else if (ValidateUtil.isStatusBlocked(dataObj.carrierMaster.status)) {
                        dataObj.carrierMaster = null;
                        if (type == "row") {
                            $scope.secondArr[index].carrierName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99502"]);
                            console.log($rootScope.nls["ERR99502"]);
                        }
                        errorArr.push(0);
                    }
                    else if (ValidateUtil.isStatusHidden(dataObj.carrierMaster.status)) {
                        dataObj.carrierMaster = null;
                        if (type == "row") {
                            $scope.secondArr[index].carrierName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99503"]);
                            console.log($rootScope.nls["ERR99503"]);
                        }
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].carrierName = false;
                        }
                        errorArr.push(1);
                    }


                    if (dataObj.flightNo == undefined
                        || dataObj.flightNo == null
                        || dataObj.flightNo == "") {

                        if (type == "row") {
                            $scope.secondArr[index].flightNumber = true;

                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99504"]);
                            console.log($rootScope.nls["ERR99504"]);
                        }
                        errorArr.push(0);
                    } else {
 
                        if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FLIGHT_PLAN_FLIGHTNUMBER,dataObj.flightNo)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].flightNumber = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99505"]);
                                 console.log($rootScope.nls["ERR99505"]);
                             }
                             errorArr.push(0);
        				}
                        
                        
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].flightNumber = false;

                            }
                            errorArr.push(1);
                        }
                    }

                    if (dataObj.pol == undefined
                        || dataObj.pol == null
                        || dataObj.pol.id == null) {

                        if (type == "row") {
                            $scope.secondArr[index].pol = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99506"]);
                            console.log($rootScope.nls["ERR99506"]);
                        }
                        errorArr.push(0);
                    }
                    else if (ValidateUtil.isStatusBlocked(dataObj.pol.status)) {
                        dataObj.pol = null;
                        if (type == "row") {
                            $scope.secondArr[index].pol = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99507"]);
                            console.log($rootScope.nls["ERR99507"]);
                        }
                        errorArr.push(0);

                    }
                    else if (ValidateUtil.isStatusHidden(dataObj.pol.status)) {
                        dataObj.pol = null;
                        if (type == "row") {
                            $scope.secondArr[index].pol = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99508"]);
                            console.log($rootScope.nls["ERR99508"]);
                        }
                        errorArr.push(0);

                    }
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].pol = false;
                        }
                        errorArr.push(1);
                    }
                    if (dataObj.pod == undefined || dataObj.pod == null
                        || dataObj.pod.id == null
                        || dataObj.pod.id.trim == "") {
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99509"]);
                            console.log($rootScope.nls["ERR99509"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }

                    else if (ValidateUtil.isStatusBlocked(dataObj.pod.status)) {
                        dataObj.pod = null;
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99510"]);
                            console.log($rootScope.nls["ERR99510"]);
                        }
                        errorArr.push(0);
                    }

                    else if (ValidateUtil.isStatusHidden(dataObj.pod.status)) {
                        dataObj.pod = null;
                        if (type == "row") {
                            $scope.secondArr[index].pod = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99511"]);
                            console.log($rootScope.nls["ERR99511"]);
                        }
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].pod = false;
                        }
                        errorArr.push(1);
                    }
                    if (dataObj.pod != undefined && dataObj.pod != null && dataObj.pod != "" && dataObj.pol != undefined && dataObj.pol != null && dataObj.pol != "") {
                        if (type == "row" || type == "change") {
                            if (dataObj.pol.portCode === dataObj.pod.portCode) {
                                $scope.secondArr[index].pol = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99512"]);
                                console.log($rootScope.nls["ERR99512"]);
                                errorArr.push(0);
                            }
                        }

                    }
                    //dates

                    if (dataObj.etd == undefined
                        || dataObj.etd == null
                        || dataObj.etd == "") {
                        if (type == "row") {
                            $scope.secondArr[index].etd = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99513"]);
                            console.log($rootScope.nls["ERR99513"]);
                        }
                        errorArr.push(0);
                    }
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].etd = false;
                        }
                        errorArr.push(1);
                    }
                    if (dataObj.eta == undefined
                        || dataObj.eta == null
                        || dataObj.eta == "") {
                        if (type == "row") {
                            $scope.secondArr[index].eta = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99514"]);
                            console.log($rootScope.nls["ERR99514"]);
                        }
                        errorArr.push(0);
                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].eta = false;
                        }
                        errorArr.push(1);
                    }


                    if (dataObj.eta != undefined
                        && dataObj.eta != null
                        && dataObj.eta != "" && dataObj.etd != undefined
                        && dataObj.etd != null
                        && dataObj.etd != "") {
                        var etd = $rootScope.convertToDate(dataObj.etd);
                        var eta = $rootScope.convertToDate(dataObj.eta);

                        if (etd > eta) {
                            if (type == "row") {
                                $scope.secondArr[index].etd = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR99515"]);
                                console.log($rootScope.nls["ERR99515"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].etd = false;
                            }
                            errorArr.push(1);
                        }

                    }

                    if (errorArr.indexOf(0) < 0) {
                        $scope.secondArr[index].errRow = false;
                        return true
                    } else {
                        return false;
                    }


                };

                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.$watch('indexFocus', function (newvalue, oldValue) {
                    if (newvalue != null && oldValue != null && newvalue != oldValue) {
                        var focusErrArr = [];
                        angular.forEach($scope.tableData, function (dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                $scope.secondArr[index].errRow = false;
                                focusErrArr.push(1);
                            } else {
                                $scope.secondArr[index].errRow = true;
                                focusErrArr.push(0);

                            }
                            //console.log();
                        });
                        if (focusErrArr.indexOf(0) < 0) {
                            if ($scope.tableState) {
                                $scope.tableState({param: true});
                            }
                        } else {
                            if ($scope.tableState) {
                                $scope.tableState({param: false});
                            }
                        }
                    }
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;
                };

                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};

                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "auto"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                };

                $scope.openDatePicker = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "auto"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                }

                $scope.carrierRender = function (item) {
                    return {
                        label: item.carrierName,
                        item: item
                    }
                }
                $scope.portRender = function (item) {
                    return {
                        label: item.portName,
                        item: item
                    }
                }
                $scope.nextEta = function (nextId) {
                    $rootScope.navigateToNextField(nextId);
                }
                $scope.selectDischarge = function (nextId) {
                    $rootScope.navigateToNextField(nextId);
                }
                $scope.selectCarrier = function (nextId) {
                    $rootScope.navigateToNextField(nextId);
                }

                $scope.dateTimePickerOptions = $rootScope.datePickerOptions;
                $scope.dateTimePickerOptions = {
                        format:$rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                        useCurrent:false,
                        sideBySide:true,
                        viewDate:moment()
                    };
                $scope.dateTimePickerOptions.widgetParent = "body";
                $scope.dateTimePickerOptionsLeft = {
                        format:$rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                        useCurrent:false,
                        sideBySide:true,
                        viewDate:moment()
                    };
                $scope.dateTimePickerOptionsLeft.widgetPositioning = {
                    horizontal: 'right'
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;
            },2);
        }
    }

}]);