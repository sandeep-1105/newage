/**
 * Created by saravanan on 23/6/16.
 */
app.directive('statusChatTable',['$timeout', '$compile', '$rootScope', 'ValidateUtil', 'TriggerTypeSearchKeyword',
    'TriggerSearchKeyword', 'EmployeeList', 
    function($timeout, $compile, $rootScope, ValidateUtil, TriggerTypeSearchKeyword,
    TriggerSearchKeyword, EmployeeList) {
    return {
        restrict: 'E',
        templateUrl: 'app/shared/inlineEditTables/status_chat_table/status_chat_table.html',
        scope: {
            indexKey: "=",
            tableData: "=",
            fnRole: "&",
            addRole: "=",
            editRole: "=",
            deleteRole: "=",
            tableCheck: "=",
            dependData: "=",
            assignCurrentObject: "="
        },
        controller: function($scope, $rootScope, $popover, $modal) {
        	 
        	$scope.toggleArr = [{id: true, title: 'Yes'}, {id: false, title: 'No'}];
            $scope.assignCurrentObject = function(obj, objIndex) {
                console.log("assignCurrentObject ", obj);
                $scope.statusUpdateObj = obj;
                $scope.editObjectIndex = objIndex;
                $rootScope.navigateToNextField('notesStatusUpdate');
            }
            
            var validateStatusObject = function(dataObj) {

                var errorArr = [];

                $scope.errorObject = {};
                $scope.errorObject.errTextArr = [];


                if (dataObj.date == undefined || dataObj.date == null || dataObj.date == "") {
                	$scope.errorObject.date = true;
                	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90413"]);
                    errorArr.push(0);
                } else {
                	$scope.errorObject.date = false;
                	errorArr.push(1);
                }

                
                // Trigger Type Master
                if (dataObj.triggerTypeMaster == undefined || dataObj.triggerTypeMaster == null || dataObj.triggerTypeMaster == "" || dataObj.triggerTypeMaster.id == null) {
                	$scope.errorObject.triggerTypeMaster = true;
                	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90400"]);
                    errorArr.push(0);
                } else if (dataObj.triggerTypeMaster.status != null) {
                    if (ValidateUtil.isStatusBlocked(dataObj.triggerTypeMaster.status)) {
                    	$scope.errorObject.triggerTypeMaster = true;
                    	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90401"]);
                        errorArr.push(0);
                    } else if (ValidateUtil.isStatusHidden(dataObj.triggerTypeMaster.status)) {
                    	$scope.errorObject.triggerTypeMaster = true;
                    	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90402"]);
                        errorArr.push(0);
                    } else {
                    	$scope.errorObject.triggerTypeMaster = false;
                        errorArr.push(1);
                    }
                }


                // Trigger Master
                if (dataObj.triggerMaster == undefined || dataObj.triggerMaster == null || dataObj.triggerMaster == "" || dataObj.triggerMaster.id == null) {
                	$scope.errorObject.triggerMaster = true;
                	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90410"]);
                    errorArr.push(0);
                } else if (dataObj.triggerMaster.status != null) {
                	if (ValidateUtil.isStatusBlocked(dataObj.triggerMaster.status)) {
                		$scope.errorObject.triggerMaster = true;
                		$scope.errorObject.errTextArr.push($rootScope.nls["ERR90411"]);
                		errorArr.push(0);
                    } else if (ValidateUtil.isStatusHidden(dataObj.triggerMaster.status)) {
                    	$scope.errorObject.triggerMaster = true;
                    	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90412"]);
                        errorArr.push(0);
                    } else {
                    	$scope.errorObject.triggerMaster = false;
                        errorArr.push(1);
                    }
                }
                
                if (dataObj.note == undefined || dataObj.note == "" || dataObj.note == null) {
                	$scope.errorObject.note = true;
                	$scope.errorObject.errTextArr.push($rootScope.nls["ERR90440"]);
                    errorArr.push(0);
                }else {
                    	$scope.errorObject.note = false;
                    	errorArr.push(1);
                }


                if (errorArr.indexOf(0) < 0) {
                    return true
                } else {
                    return false;
                }
            }
            
            


            $scope.saveObj = function(obj) {
                console.log("SaveObj ", obj);

                if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                    $scope.tableData = [];
                }
                
                var validResult = validateStatusObject(obj);
                if (validResult) {
                    $scope.errorObject.errRow = false;
                    if ($scope.editObjectIndex == undefined || $scope.editObjectIndex == null) {
                    	if($rootScope.userProfile != null && $rootScope.userProfile.selectedUserLocation != null) {
                    		obj.locationName = $rootScope.userProfile.selectedUserLocation.locationName;
                    		obj.dateTimeZone = $rootScope.userProfile.selectedUserLocation.timeZoneAbb;
                    	}
                    	
                        $scope.tableData.push(obj);
	                } else {
	                	$scope.tableData[$scope.editObjectIndex] = obj;
	                	$scope.editObjectIndex = null;
	                }
	                $scope.statusUpdateObj = { completed: false, followUpRequired: false, protect: false };
                } else {
                	$scope.errorObject.errRow = true;
                }
            }

            console.log("table data", $scope.tableData, $scope.dependData);
            $scope.currentServiceUid = $scope.dependData;

            $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";
            $scope.directiveDateTimePickerOpts = {
                    format:$rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                    useCurrent:false,
                    sideBySide:true,
                    viewDate:moment()
                };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";

            $timeout(function() {
                $scope.tableisChecked = true;
                $scope.$watch('tableData', function(newValue, oldValue) {
                    if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                        initTable();
                    }
                });


                


                function initTable() {
                    $scope.statusUpdateObj = {
                        completed: false,
                        followUpRequired: false,
                        protect: false
                    };
                    $scope.emptyObjRef = angular.copy($scope.statusUpdateObj);
                }

                initTable();


                //call Validate

                var myOtherModal = $modal({
                    scope: $scope,
                    templateUrl: 'errorPopUp.html',
                    show: false,
                    keyboard: true
                });
                var errorOnRowIndex = null;
                $scope.errorShow = function(errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxTriggerTypeMasterEvent = function(object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return TriggerTypeSearchKeyword.fetch($scope.searchDto)
                        .$promise.then(
                            function(data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.triggerTypeMasterList = data.responseObject.searchResult;
                                    //$scope.showTriggerTypeMasterList=true;
                                    return $scope.triggerTypeMasterList;
                                }
                            },
                            function(errResponse) {
                                console.error('Error while fetching Customer Services');
                            }
                        );

                };
                $scope.selectTriggerType = function(dataOb) {
                    $scope.triggerTypeMaster = dataOb.triggerTypeMaster;

                }

                $scope.selectTriggerMaster = function(triggerMasterObj) {
                	if(triggerMasterObj!=null){
                		$scope.statusUpdateObj.note=triggerMasterObj.note;
                    }

                }
                $scope.ajaxTriggerMasterEvent = function(object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    $scope.triggerMasterList = [];
                    if ($scope.triggerTypeMaster.triggerTypeName != undefined && $scope.triggerTypeMaster.triggerTypeName != null && $scope.triggerTypeMaster.triggerTypeName != "") {
                        return TriggerSearchKeyword.fetch({
                                "triggerType": $scope.triggerTypeMaster.triggerTypeName
                            }, $scope.searchDto)
                            .$promise.then(
                                function(data) {
                                    if (data.responseCode == "ERR0") {
                                        $scope.triggerMasterList = data.responseObject.searchResult;
                                        return $scope.triggerMasterList;
                                    }
                                },
                                function(errResponse) {
                                    console.error('Error while fetching Customer Services');
                                }
                            );

                    }
                }
                
                
                

                $(document)
                    .on('keydown', function(e) {
                        if (e.keyCode == 27) {
                            if (myOtherModal) {
                                myOtherModal.$promise.then(myOtherModal.hide);
                            }
                        }
                    });

                $scope.$on('modal.hide', function() {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function() {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });


                $scope.triggerTypeRender = function(item) {
                    return {
                        label: item.triggerTypeName,
                        item: item
                    }
                }
                $scope.triggerRender = function(item) {
                    return {
                        label: item.triggerName,
                        item: item
                    }
                }
                $scope.statusSelectFocus = function(id) {
                    console.log("called." + id);
                    $rootScope.navigateToNextField(id);
                }
                $scope.goToNext = function(id) {
                    console.log("called." + id);
                    $rootScope.navigateToNextField(id);
                };
                $scope.errorList = $rootScope.errorList;
            }, 0);
        }
    }
}]);