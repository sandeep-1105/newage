app.directive('vendorTable',['$timeout', '$rootScope', 'ValidateUtil', 'PartiesList', 'Notification','AgentByPort', 
    function($timeout, $rootScope, ValidateUtil, PartiesList, Notification, AgentByPort) {
    return {
        restrict: 'E',
        templateUrl: ' app/shared/inlineEditTables/enquiryRateRequest/vendordetails/vendorDetail.html',
        scope: {
            indexKey: "=",
            dependData: "=",
            tableData: "=",
            showErrors: "=",
            tableCheck: "=",
            addRole: "=",
            editRole: "=",
            deleteRole: "=",
            fnRole: "&",
            defaultAgent : "="
        },
        controller: function($scope, $rootScope, $popover, $modal) {
            $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
            $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";

            $scope.directiveDateTimePickerOpts = {
                format: $rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                useCurrent: false,
                sideBySide: true,
                viewDate: moment()
            };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";

            $timeout(function() {
                $scope.YesNoArr = $rootScope.enum['YesNo'];

                function initTable() {
                    if ($scope.tableData == undefined || $scope.tableData == null) {
                        $scope.tableData = [{}];
                    }
                }
                initTable();
                $scope.rateRequest = {};
                $scope.saveEnquiryChargeList = function(index) {

                    if ($scope.rateRequest != undefined && $scope.rateRequest.isRateChargeCheck != undefined && $scope.rateRequest.isRateChargeCheck()) {
                        $scope.chargeModal.$promise.then($scope.chargeModal.hide)
                    } else {
                        console.log("Rate request validation failed..")
                    }
                }
                //add row
                $scope.addRow = function() {

                    var fineArr = [];
                    $scope.secondArr = [];
                    $scope.rateAcceptedArr = [];
                    angular.forEach($scope.tableData, function(dataObj, index) {
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    if (fineArr.indexOf(0) < 0) {
                        var objRateRequest = {};
                        if ($scope.defaultAgent != undefined && $scope.defaultAgent != null && $scope.defaultAgent.partyAddressList != null && $scope.defaultAgent.partyAddressList.length > 0 && $scope.tableData.length === 0) {
                            objRateRequest.vendor = $scope.defaultAgent;
                            $scope.tableData.push(objRateRequest);
                            angular.forEach($scope.defaultAgent.partyAddressList, function(data, index) {
                                if (data.addressType == 'Primary') {
                                    if (data.contactPerson != undefined && data.contactPerson != null) {
                                        objRateRequest.contactPerson = data.contactPerson;
                                    }
                                    if (data.email != undefined && data.email != null) {
                                        objRateRequest.emailId = data.email;
                                    }
                                    objRateRequest.rateAccepted = 'No';
                                    return
                                }
                            });
                        }
                        else{
                            $scope.tableData.push({}); 
                        }
                        $rootScope.navigateToNextField($scope.indexKey + 'vendorName' + ($scope.tableData.length - 1))
                        $scope.secondArr.push({
                            "errRow": false
                        });
                    }

                };

                $scope.removeVendor = function(obj, index) {
                    $scope.tableData.splice(index, 1);
                    if ($scope.secondArr != undefined && $scope.secondArr != null)
                        $scope.secondArr.splice(index, 1);
                }


                // AJAX CALLS --FOR VENDOR--PARTY

                $scope.ajaxPartyEvent = function(object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return PartiesList.query($scope.searchDto).$promise.then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                $scope.partyList = [];
                                $scope.partyList = data.responseObject.searchResult;
                                return $scope.partyList;
                            }
                        },
                        function(errResponse) {
                            console.error('Error while fetching party');
                        }
                    );
                }

                $scope.selectVendor = function(party, rateReqObj, id) {
                    if (party != undefined && party != null && party != "") {
                        if (party.partyAddressList != null && party.partyAddressList.length > 0) {
                            angular.forEach(party.partyAddressList, function(data, index) {
                                if (data.addressType == 'Primary') {
                                    if (data.contactPerson != undefined && data.contactPerson != null) {
                                        rateReqObj.contactPerson = data.contactPerson;
                                    }
                                    if (data.email != undefined && data.email != null) {
                                        rateReqObj.emailId = data.email;
                                    }
                                    rateReqObj.rateAccepted = 'No';
                                    $scope.navigateToNextField(id);
                                    return
                                }
                            });
                        }
                    }
                }

                $scope.rateAcceptedChange = function(value, index) {
                    if (value == 'Yes') {
                        for (var i = 0; i < $scope.tableData.length; i++) {
                            if (i != index) {
                                $scope.tableData[i].rateAccepted = 'No';

                            }
                        }
                    }
                }

                var myOtherModal = $modal({
                    scope: $scope,
                    templateUrl: 'errorPopUp.html',
                    show: false,
                    keyboard: true
                });

                $scope.errorShow = function(errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };



                function isEmptyRow(obj) {
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }


                //Validation for PageRequest

                $scope.tableCheck = function() {
                    $scope.tableisChecked = false;
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    $scope.rateAcceptedArr = [];
                    angular.forEach($scope.tableData, function(dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;
                        } else {

                            validResult = validateObj(dataObj, index, "row");
                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });

                    angular.forEach(finalData, function(dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });

                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                //validation operation
                var validateObj = function(dataObj, index, type) {

                    $scope.secondArr[index] = {};
                    var errorArr = [];
                    errorArr.push(1);
                    $scope.secondArr[index].errTextArr = [];


                    //Vendor Validation
                    if (dataObj.vendor == undefined || dataObj.vendor == null || dataObj.vendor.id == undefined) {
                        $scope.secondArr[index].vendor = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8816"]);
                        errorArr.push(0);
                    } else {
                        if (ValidateUtil.isStatusBlocked(dataObj.vendor.status)) {
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8817"]);
                            $scope.secondArr[index].vendor = true;
                            errorArr.push(0);
                        }
                        if (ValidateUtil.isStatusHidden(dataObj.vendor.status)) {
                            $scope.secondArr[index].vendor = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8817"]);
                            errorArr.push(0);
                        }
                    }


                    //Vendor Email Validation

                    if (dataObj.emailId == undefined && dataObj.emailId == null && dataObj.emailId == "") {
                        $scope.secondArr[index].vendorEmail = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8842"]);
                        errorArr.push(0);
                    } else {
                        if (!ValidateUtil.isValidMail(dataObj.emailId)) {
                            console.log("email valid");
                        } else {
                            $scope.secondArr[index].vendorEmail = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8819"]);
                            errorArr.push(0);
                        }
                    }

                    //Enquiry Received Date

                    if (dataObj.responseDate != undefined && dataObj.responseDate != null && dataObj.responseDate != "") {

                        if ($scope.dependData != undefined && $scope.dependData.receivedOn != undefined && $scope.dependData.receivedOn != null) {

                            var enqReceivedOn = $rootScope.convertToDate($scope.dependData.receivedOn);
                            var venRequestDate = $rootScope.convertToDate(dataObj.responseDate);

                            if (venRequestDate > enqReceivedOn) {
                                $scope.secondArr[index].vendorReceivedOn = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8821"]);
                                errorArr.push(0);
                            }
                        }
                    } else {
                        dataObj.responseDate = null;
                    }
                    //rate accepted--
                    if (dataObj.rateAccepted != undefined && dataObj.rateAccepted != null && dataObj.rateAccepted != "") {
                        if (dataObj.rateAccepted == 'Yes') {
                            $scope.rateAcceptedArr.push('Yes');
                            //which is used for if user finalizes the vendor--it shoud contain charge
                            if (dataObj.chargeList == undefined || dataObj.chargeList == null || dataObj.chargeList.length == 0) {
                                $scope.secondArr[index].rateAccepted = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8822"]);
                                errorArr.push(0);
                            } else {
                                //if charge list is 1 and need to check if that one is empty ..if its empty need to throw error else no need..
                                if (dataObj.chargeList.length == 1) {
                                    if (isEmptyRow(dataObj.chargeList[0])) {
                                        $scope.secondArr[index].rateAccepted = true;
                                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8822"]);
                                        errorArr.push(0);
                                    }
                                }
                            }
                        }
                        if ($scope.rateAcceptedArr != null && $scope.rateAcceptedArr.length > 1) {
                            $scope.secondArr[index].rateAccepted = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8822"]);
                            errorArr.push(0);
                        }
                    } else {
                        dataObj.rateAccepted = null;
                    }

                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                }




                $scope.showChargeDetails = function(obj, index) {

                    if (obj == undefined || obj.vendor == undefined || obj.vendor.id == undefined) {
                        Notification.error("please select corresponing vendor");
                        return;
                    }
                    $scope.enquiryRateChargeDummy = {};
                    if (obj.chargeList != undefined && obj.chargeList != null) {
                        $scope.tableData[index].chargeList = obj.chargeList;
                    } else {
                        $scope.tableData[index].chargeList = [{}];
                    }
                    $scope.enquiryRateChargeDummy.enquiryChargeIndex = index;
                    $scope.enquiryRateChargeDummy.chargeVendorName = obj.vendor.partyName;
                    //$scope.enquiryChargeList.push(obj.chargeList);
                    $scope.chargeModal = {};
                    $scope.chargeModal = $modal({
                        scope: $scope,
                        backdrop: 'static',
                        templateUrl: '/app/shared/inlineEditTables/enquiryRateRequest/enquiryChargeDetails/enquiryChargePopUp.html',
                        show: false
                    });
                    $scope.chargeModal.$promise.then($scope.chargeModal.show)
                    $rootScope.navigateToNextField(index + 'chargeCodeField' + index);
                }



                $scope.navigateToNextField = function(id) {
                    if (id != undefined && id != null && id != "")
                        document.getElementById(id).focus();
                };

                $scope.delete = $rootScope.delete;
                $scope.showCharge = $rootScope.showCharge;
            }, 2);
        }
    }
}]);