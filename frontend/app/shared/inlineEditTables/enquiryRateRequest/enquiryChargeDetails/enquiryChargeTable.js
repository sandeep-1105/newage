/**
 * Created by saravanan on 5/4/16.
 */

(function() {
	app.directive("enquiryChargeTable",['$rootScope', '$timeout', '$modal', '$log', 'ngProgressFactory','ngDialog',
			'ValidateUtil','ChargeSearchKeyword','UnitSearchKeyword','CurrencyMasterSearchKeyword',
			'UnitMasterGetByCode','CommonValidationService','appConstant','cloneService','roleConstant',
		function($rootScope, $timeout, $modal, $log, ngProgressFactory,ngDialog,
			ValidateUtil,ChargeSearchKeyword,UnitSearchKeyword,CurrencyMasterSearchKeyword,
			UnitMasterGetByCode,CommonValidationService,appConstant,cloneService,roleConstant){
	    return {
	        restrict: 'E',
	        scope: {
	        	"tableCheck":"=",
	        	"indexKey":"=",
	        	"dependData":"=",
	            "tableData":"=?",
	            "addRole":"=",
	            "editRole":"=",
	            "deleteRole":"=",
	            "fnRole":"&"
	        },
	        templateUrl: '/app/shared/inlineEditTables/enquiryRateRequest/enquiryChargeDetails/enquiryChargeTable.html',
	        controller:function($scope, $rootScope){
	        	$scope.$roleConstant = roleConstant;
	        	$scope.updateRole = $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_RATE_REQUEST_MODIFY, true);
	        	$rootScope.navigateToNextField($scope.indexKey + 'chargeCodeField0');
	        	if($scope.dependData!=undefined && $scope.dependData.vendor!=undefined ){
	        		$scope.vendorName=$scope.dependData.vendor.partyName;	
	        	}
	        	 $scope.smArr = $rootScope.enum['ShipmentMovement'];
	            $scope.delete = $rootScope.delete;
	            $scope.errorList = $rootScope.errorList;
	            $scope.numberOfUnitsList = appConstant.chargeNumberOfUnitList;

               
	          /* $scope.saveRateCharge=function(){
                	
                
               
               }*/
	           $scope.clearChargeName = function(charge) {
	            	if(charge.chargeMaster == undefined || charge.chargeMaster == null || charge.chargeMasater == "") {
	            		charge.chargeName = "";
	            	}
	            }
	            $scope.secondArr = [];
	            
	            function initTable(){
	            	  if ($scope.tableData == undefined || $scope.tableData == null) {
	                      $scope.tableData = [{}];
	                  }
	            }
	            initTable();

	            $scope.addRow = function(){
	            	var addRole = $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_RATE_REQUEST_CREATE);   
	            	if(addRole){
	            		   var fineArr = [];
		            	   $scope.secondArr = [];
		                   angular.forEach($scope.tableData, function (dataObj, index) {
		                       var validResult = validateObj(dataObj, index, "row");
		                       if (validResult) {
		                           fineArr.push(1);
		                           $scope.secondArr[index].errRow = false;
		                       } else {
		                           $scope.secondArr[index].errRow = true;
		                           fineArr.push(0);
		                       }
		                   });
		                   if (fineArr.indexOf(0) < 0) {
		                       $scope.tableData.push({});
		                       $scope.secondArr.push({"errRow": false});
		                   }
		                   $rootScope.navigateToNextField($scope.indexKey + 'chargeCodeField' + ($scope.tableData.length-1));
		            	   
	            	   }
	            	};
	            
	            
	           
	            
	            $scope.removeRateReqCharge=function(obj,index){
	            	var deleteRole = $rootScope.roleAccess(roleConstant.SALE_ENQUIRY_RATE_REQUEST_DELETE);   
	            	if(deleteRole){
	            		$scope.tableData.splice(index,1);
		            	if($scope.secondArr!=undefined && $scope.secondArr !=null)
		            	$scope.secondArr.splice(index, 1);	            		
	            	}
	            }
	            
	            
	            function isEmptyRow(obj){
	                //return (Object.getOwnPropertyNames(obj).length === 0);
	                var isempty = true; //  empty
	                if (!obj) {
	                    return isempty;
	                }

	                var k = Object.getOwnPropertyNames(obj);
	                for (var i = 0; i < k.length; i++){
	                    if (obj[k[i]]) {
	                        isempty = false; // not empty
	                        break;

	                    }
	                }
	                return isempty;
	            }
	            
	            
	            $scope.copyRequestCharge=function(charge,index){
	            	
	            	var chargeN=angular.copy(charge);
	            	var validResult = validateObj(charge, index, "row");
	            	if(validResult){
	            	chargeN.id=null;
	            	$scope.tableData.push(chargeN);
	            	}
	            }
	            
	            
	            //validation
	            
	            
	            $scope.tableCheck = function(){
	                var fineArr = [];
	                $scope.secondArr=[];
	                var validResult;
	                var finalData = $scope.tableData;
	                var emptyIndex = [];
	                angular.forEach($scope.tableData,function(dataObj,index){
	                    $scope.secondArr[index]={};
	                    if (isEmptyRow(dataObj)){
	                        validResult = true;
	                        finalData[index].isEmpty = true;
	                    }else{
	                        validResult = validateObj(dataObj,index);
	                    }
	                    if(validResult){
	                        fineArr.push(1);
	                        $scope.secondArr[index].errRow = false;
	                    }else{
	                        $scope.secondArr[index].errRow = true;
	                        fineArr.push(0);
	                    }
	                });
	                angular.forEach(finalData,function(dataObj,index){
	                    if(dataObj.isEmpty){
	                        finalData.splice(index,1)
	                        $scope.secondArr.splice(index,1);
	                    }
	                });
	                if(fineArr.indexOf(0)<0){
	                    $scope.tableData = finalData;
	                    return true;
	                }else{
	                    return false;
	                }
	            };
	            
	            
	            var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
	            var errorOnRowIndex = null;

	            $scope.errorShow = function(errorObj,index){
	                $scope.errList = errorObj.errTextArr;
	                errorOnRowIndex = index;
	                myOtherModal.$promise.then(myOtherModal.show);
	            };

	            
	            
	            var validateObj = function (dataObj, index, type) {
	            	
	            	 $scope.secondArr[index] = {};
	            	 var errorArr = [];
	            	 errorArr.push(1);
	            	 $scope.secondArr[index].errTextArr = [];
	            	 
	            	 
	            	//Charge Validation
	            	 if (dataObj.chargeMaster == undefined || dataObj.chargeMaster == null || dataObj.chargeMaster.id == undefined) {
	                         $scope.secondArr[index].charge = true;
	                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8823"]);
	                     errorArr.push(0);
	                     
	                 }else{
	                	 if(ValidateUtil.isStatusBlocked(dataObj.chargeMaster.status)){
	                		 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8824"]);
	                		 $scope.secondArr[index].charge = true;
	                		 errorArr.push(0);
							}
							if(ValidateUtil.isStatusHidden(dataObj.chargeMaster.status)){
								 $scope.secondArr[index].charge = true;
								 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8825"]);
								 errorArr.push(0);
							}
	                 }
	            	
	            	 //charge name
	            	 if (dataObj.chargeName == undefined || dataObj.chargeName == null || dataObj.chargeName == "") {
	            		 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8826"]);
                		 $scope.secondArr[index].chargeName = true;
                		 errorArr.push(0);
	            	 }
	            	 //unitMaster
	            	 
	            	 if (dataObj.unitMaster == undefined || dataObj.unitMaster == null || dataObj.unitMaster.id == undefined) {
                         $scope.secondArr[index].unit = true;
                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8827"]);
                     errorArr.push(0);
                     
                 }else{
                	 if(ValidateUtil.isStatusBlocked(dataObj.unitMaster.status)){
                		 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8828"]);
                		 $scope.secondArr[index].unit = true;
                		 errorArr.push(0);
						}
						if(ValidateUtil.isStatusHidden(dataObj.unitMaster.status)){
							 $scope.secondArr[index].unit = true;
							 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8829"]);
							 errorArr.push(0);
						}
                   }
	            	 
	            	 //currency Master
	            	 
	            	 
	            	 if (dataObj.currencyMaster == undefined || dataObj.currencyMaster == null || dataObj.currencyMaster.id == undefined) {
                         $scope.secondArr[index].currency = true;
                         $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8833"]);
                     errorArr.push(0);
                     
                 }else{
                	 if(ValidateUtil.isStatusBlocked(dataObj.currencyMaster.status)){
                		 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8834"]);
                		 $scope.secondArr[index].currency = true;
                		 errorArr.push(0);
						}
						if(ValidateUtil.isStatusHidden(dataObj.currencyMaster.status)){
							 $scope.secondArr[index].currency = true;
							 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8835"]);
							 errorArr.push(0);
						}
                   }
	            	 
	            	 
	            	 // buy rate
	            	 
	            	 
	            	 if(dataObj.buyRate==undefined || dataObj.buyRate==null || isNaN(dataObj.buyRate) || dataObj.buyRate==""){
	            		 $scope.secondArr[index].buyRate = true;
	                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8844"]);
	                     errorArr.push(0);
	            	 }
	            	 if (!isNaN(dataObj.buyRate) && dataObj.buyRate != undefined
	                         && dataObj.buyRate != null
	                         && dataObj.buyRate != "") {

	                         if (isNaN(parseFloat(dataObj.buyRate))) {
	                                 $scope.secondArr[index].buyRate = true;
	                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8830"]);
	                             errorArr.push(0);
	                         }
	                         else if (dataObj.buyRate != null && dataObj.currencyMaster.decimalPoint == "2" && 
	                         		!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL, dataObj.buyRate)) {
	                                 $scope.secondArr[index].grossSaleMinimum = true;
	                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8830"]);
	                             errorArr.push(0);
	                         }
	                         
	                         else if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL, dataObj.buyRate)) {
	                                 $scope.secondArr[index].grossSaleMinimum = true;
	                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8830"]);
	                             errorArr.push(0);
	                         }

	                         else if (dataObj.grossSaleMinimum == 0) {
	                                 $scope.secondArr[index].buyRate = true;
	                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8830"]);
         	                             errorArr.push(0);

	                         }
	                         else if (dataObj.buyRate < 0 || dataObj.buyRate >= 99999999999.999) {
	                                 $scope.secondArr[index].buyRate = true;
	                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR8830"]);
	                                 errorArr.push(0);

	                         }
	                         else {
	                                 $scope.secondArr[index].buyRate = false;
	                                 
	                             }
	                         }
	                           if (errorArr.indexOf(0) < 0) {
                                         return true
                                       } else {
                                         return false;
                                   }
	            	 
	            	
	            }
	            
	            
	            
	            // Ajax Calls
	            
                $scope.localSearch = function (object) {

                    console.log("ajaxChargeEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    console.log("search data", $scope.searchDto);
                    return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.chargeMasterList = data.responseObject.searchResult;
                                return $scope.chargeMasterList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Charge');
                        }
                    );

                }

                $scope.selectEnquiryCharge = function(rateRequestCharge,id){
                	
              	  if(rateRequestCharge.chargeMaster != undefined && rateRequestCharge.chargeMaster != null) {
              		  rateRequestCharge.chargeName = rateRequestCharge.chargeMaster.chargeName;
              		if(rateRequestCharge.chargeMaster.calculationType != undefined && rateRequestCharge.chargeMaster.calculationType != null){
              			var unitCode;
              			if(rateRequestCharge.chargeMaster.calculationType=='Percentage'){
              				unitCode = 'PER';
              			} else if(rateRequestCharge.chargeMaster.calculationType=='Shipment') {
              				unitCode = 'SHPT';
              			} else if(rateRequestCharge.chargeMaster.calculationType=='Unit') {
              				unitCode = 'KG';
              			} else if(rateRequestCharge.chargeMaster.calculationType=='Document') {
              				unitCode = 'DOC';
              			} else {
              				
              			}
              			UnitMasterGetByCode.get({
        		   		 	unitCode : unitCode
        		   		 }).$promise.then( function(resObj) {
        		   		 	if (resObj.responseCode =="ERR0"){
        		   		 	rateRequestCharge.unitMaster=resObj.responseObject;
        		   		 		$scope.navigateToNextField(id);
        		   		 	} else {
        		   		 		
        		   		 		
        		   		 	}
        		   		}, function(error) {
        		   			
        		   		});
              		}
              	  }  
                };
                
                $scope.localUnitSearch = function(object) {
                    console.log("ajaxUnitMasterEvent is called ", object);
                    $scope.searchDto={};
                    $scope.searchDto.keyword=object==null?"":object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                            if (data.responseCode =="ERR0"){
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.unitMasterList = data.responseObject.searchResult;
                                return $scope.unitMasterList;
                            }
                        },
                        function(errResponse){
                            console.error('Error while fetching Charge');
                        }
                    );


                }
                
                
                $scope.localCurrencySearch = function (object) {

                    console.log("ajaxCurrencyMasterEvent is called ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.currencyMasterList = data.responseObject.searchResult;
                                return $scope.currencyMasterList;

                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Currency');
                        }
                    );

                }
	        
	            
	            $scope.navigateToNextField = function(id) {
                    if (id != undefined && id != null && id != "")
                        document.getElementById(id).focus();
                };

	        }
	    }
	}]);
	
})();
