/**
 * Created by hmspl on 20/7/16.
 */

/**
 * Created by saravanan on 23/6/16.
 */
app.directive('invoiceChargeTable',['$timeout','$compile','$rootScope','ValidateUtil','EmployeeList','ChargeSearchKeyword',
        'EventSearchKeyword','CurrencyMasterSearchKeyword','InvoiceServiceTaxPercentage',
        'GLChargeMappingList','appConstant','ConsolGetByUid',
        'CommonValidationService','cloneService','RatesFromProvision','Notification',
    function($timeout,$compile,$rootScope,ValidateUtil,EmployeeList,ChargeSearchKeyword,
		EventSearchKeyword,CurrencyMasterSearchKeyword,InvoiceServiceTaxPercentage,
        GLChargeMappingList,appConstant,ConsolGetByUid,
		CommonValidationService,cloneService,RatesFromProvision,Notification){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/invoice_charge/charge_table.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            dependData:"=",
            shipmentServiceDetail:"=",
            shipmentServiceList:"=",
            consol:"=",
            documentType:"=",
            calcFn:"&",
            btnIndex:"=",
            tableCheck:"=?",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            rateRole:"=",
            fnRole:"&",
            updateCharge:"=?"
        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
            	
            	 $scope.rateMerged = false;
         	    var rateMergedValue=$rootScope.appMasterData['booking.rates.merged'];
         	    if(rateMergedValue.toLowerCase() == 'true'  || rateMergedValue == true ) {
         	    	$scope.rateMerged = true;
         	    } 
                $scope.crnArr = $rootScope.enum['CRN'];
                $scope.localCurrencyCode = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode;
                if ($scope.invoiceCreditNoteDetail == undefined || $scope.invoiceCreditNoteDetail == null 
                		|| $scope.invoiceCreditNoteDetail == '') {
                    $scope.invoiceCreditNoteDetail = {};
                }
                $scope.debitCreditArr = $rootScope.enum['DebitCredit'];
               

                function isEmptyRow(obj) {

                    var isempty = true;
                    console.log("empty obj*******", obj);

                    $scope.emptyObjRef=$scope.getDefaultData();
                    //$scope.emptyObjRef = {};
                    console.log("$scope.emptyObjRef*****", $scope.emptyObjRef);
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {

                        //check the new key is empty or not
                        if (obj[k[i]]) {
                            // check the default object contains the key
                            if (!$scope.emptyObjRef.hasOwnProperty(k[i])) {
                                isempty = false;
                                break;
                            }
                            //check the type is object or not
                            if (angular.isObject(obj[k[i]])) {
                                if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]].id != null && obj[k[i]].id != $scope.emptyObjRef[k[i]].id) {
                                    isempty = false;
                                    break;
                                }
                            }
                            else {
                                if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                                    isempty = false;
                                    break;

                                }
                            }
                        }
                    }
                    console.log("empty check *****", isempty)
                    return isempty;
                }

                $scope.tableCheck = function () {

                    console.log("status table table check called");
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });

                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };
                $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
                $scope.directiveDatePickerOpts.widgetParent = "body";
                $scope.toggleArr = [{id: true, title: 'Yes'}, {id: false, title: 'No'}];


                //show errors
                $scope.$watch('showErrors', function (newValue, oldValue) {


                    console.log("show Errors", newValue, oldValue);
                    if (newValue) {
                        var focusErrArr = [];
                        angular.forEach($scope.tableData, function (dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                $scope.secondArr[index].errRow = false;
                                focusErrArr.push(1);
                            } else {
                                $scope.secondArr[index].errRow = true;
                                focusErrArr.push(0);

                            }
                            //console.log();


                        });
                        if (focusErrArr.indexOf(0) < 0) {
                            $scope.showErrors = false;
                        } else {
                            $scope.showErrors = true;
                        }
                    }
                    if ($scope.tableData == null || $scope.tableData == undefined) {
                        $scope.tableData = [];
                    }
                    else {
                        if ($scope.tableData.length == 0) {
                            $scope.addRow();
                        }
                    }

                });

                //indexfocus initialization
                $scope.indexFocus = null;
                //temp array
                $scope.secondArr = [];


                $scope.getDefaultData = function () {
                	
                	console.log("$scope.dependData",$scope.dependData);
                    var invoiceCreditNoteDetail = {};
                    if($scope.dependData.shipmentUid==null)
                    	{
                    	invoiceCreditNoteDetail.shipmentServiceDetail = "";
                    	}
                    else
                    	{
                    	invoiceCreditNoteDetail.shipmentServiceDetail = $scope.shipmentServiceDetail;
                    	}
                    
                    invoiceCreditNoteDetail.chargeName = '';
                    invoiceCreditNoteDetail.crn = 'Revenue';
                    invoiceCreditNoteDetail.debitCredit = 'Credit';

                    if ($scope.documentType == 'CRN-COST') {
                        invoiceCreditNoteDetail.crn = 'Cost';
                        invoiceCreditNoteDetail.debitCredit = 'Debit';
                    }

                    invoiceCreditNoteDetail.currencyMaster = angular.copy($rootScope.userProfile.selectedUserLocation.currencyMaster);
                    invoiceCreditNoteDetail.roe = 1;
                    return invoiceCreditNoteDetail;
                }


                function initTable() {
                	$scope.emptyObjRef = {};
                    if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                        $scope.tableData = [];

                        $scope.invoiceCreditNoteDetail = $scope.getDefaultData();

                        $scope.tableData.push($scope.invoiceCreditNoteDetail);
                    }
                    $scope.emptyObjRef = angular.copy($scope.tableData[$scope.tableData.length - 1]);
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row
                $scope.addRow = function () {
                	if($scope.documentType=='CRN-REVENUE')
                		{
                		console.log("Can't add new charge for credit note revenue");
                		return;
                		}
                    $scope.emptyObjRef = {};
                    var fineArr = [];

                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableData == undefined || $scope.tableData == null)
                            $scope.tableData = [];

                        $scope.invoiceCreditNoteDetail = $scope.getDefaultData();


                        $scope.tableData.push($scope.invoiceCreditNoteDetail);
                        $scope.emptyObjRef = angular.copy($scope.invoiceCreditNoteDetail);
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;

                        if ($scope.tableState) {
                            $scope.tableState({param: true});
                        }
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                    }
                    $scope.moveToNextField('serviceList',($scope.tableData.length-1));
                };

                //call Validate
                $scope.callValidate = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {

                        var validResult = validateObj(dataObj, index, "row");

                        if (validResult) {

                            $scope.secondArr[index].errRow = false;
                            $scope.tableState({param: true});
                        } else {
                            $scope.secondArr[index].errRow = true;
                            $scope.tableState({param: false});

                        }

                    });


                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxTriggerTypeMasterEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    TriggerTypeSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.triggerTypeMasterList = data.responseObject.searchResult;
                                //$scope.showTriggerTypeMasterList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );
                };
                $scope.selectTriggerType = function (dataOb) {
                    $scope.triggerTypeMaster = dataOb.triggerTypeMaster;
                    $scope.callValidate();
                }
                $scope.ajaxTriggerMasterEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    if ($scope.triggerTypeMaster.triggerTypeName != undefined && $scope.triggerTypeMaster.triggerTypeName != null 
                    		&& $scope.triggerTypeMaster.triggerTypeName != "") {
                        TriggerSearchKeyword.fetch({"triggerType": $scope.triggerTypeMaster.triggerTypeName}, $scope.searchDto).$promise.then(
                            function (data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.triggerMasterList = data.responseObject.searchResult;
                                    //$scope.showTriggerMasterList=true;
                                }
                            },
                            function (errResponse) {
                                console.error('Error while fetching Customer Services');
                            }
                        );
                    } else {
                        $scope.validateShipmentServiceTrigger(1);
                    }
                }
                $scope.ajaxAssignedToEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return EmployeeList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.assignedToList = data.responseObject.searchResult;
                                return $scope.assignedToList;
                                //$scope.showAssignedToList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );

                }

                $scope.ajaxEventMasterEvent = function (object) {
                    console.log("ajaxEventMasterEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return EventSearchKeyword.fetch($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.eventMasterList = data.responseObject.searchResult;
                                return $scope.eventMasterList;
                                //$scope.showEventMasterList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Event');
                        }
                    );

                }


                ////error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index] = {};
                        $scope.secondArr[index].errTextArr = [];
                    }


                    if (dataObj.shipmentServiceDetail == undefined ||
                        dataObj.shipmentServiceDetail == null ||
                        dataObj.shipmentServiceDetail.id == undefined ||
                        dataObj.shipmentServiceDetail.id == null) {

                    if($scope.dependData.shipmentUid!=null && $scope.dependData.shipmentUid!=undefined 
                    		&& $scope.dependData.shipmentUid !='')
                    	{
                        if (type == "row") {
                            $scope.secondArr[index].serviceId = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40010"]);

                        }
                        errorArr.push(0);

                    	}
                    }

                    if (dataObj.chargeMaster == undefined ||
                        dataObj.chargeMaster == null ||
                        dataObj.chargeMaster.id == undefined ||
                        dataObj.chargeMaster.id == null) {


                        if (type == "row") {
                            $scope.secondArr[index].chargeCode = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39053"]);
                            dataObj.chargeName = '';
                        }
                        errorArr.push(0);


                    } else {


                        if (ValidateUtil.isStatusBlocked(dataObj.chargeMaster.status)) {
                            console.log("chargeMaster", $rootScope.nls["ERR39054"]);
                            if (type == "row") {
                                $scope.secondArr[index].chargeCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39054"]);

                            }
                            errorArr.push(0);

                        }

                        else if (ValidateUtil.isStatusHidden(dataObj.chargeMaster.status)) {
                            console.log("chargeMaster", $rootScope.nls["ERR39055"]);
                            if (type == "row") {
                                $scope.secondArr[index].chargeCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39055"]);
                            }
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].chargeCode = false;

                            }
                            errorArr.push(1);
                        }

                    }
                    if (dataObj.chargeName == undefined ||
                        dataObj.chargeName == null ||
                        dataObj.chargeName == "") {
                        console.log("chargeName", $rootScope.nls["ERR39079"]);
                        if (type == "row") {
                            $scope.secondArr[index].chargeName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39079"]);
                        }
                        errorArr.push(0);
                    } else {
                       
                        if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charge_Name,dataObj.chargeName)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].chargeName = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39080"]);
                             }
                             errorArr.push(0);
    					}
                        
                        
                        
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].chargeName = false;
                            }
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.crn == undefined || dataObj.crn == null
                        || dataObj.crn == null
                        || dataObj.crn == "") {

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].crn = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }


                    if (dataObj.currencyMaster != undefined &&
                        dataObj.currencyMaster != null &&
                        dataObj.currencyMaster.id != undefined &&
                        dataObj.currencyMaster.id != null) {

                        if (ValidateUtil.isStatusBlocked(dataObj.currencyMaster.status)) {

                            if (type == "row") {
                                $scope.secondArr[index].currencyMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39066"]);
                            }
                            errorArr.push(0);

                        }

                        else if (ValidateUtil.isStatusHidden(dataObj.currencyMaster.status)) {

                            if (type == "row") {
                                $scope.secondArr[index].currencyMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39067"]);
                            }
                            errorArr.push(0);

                            console.log("Selected currencyMaster is Hidden");

                        }

                        else if ($rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.currencyMaster.id) {
                            if ($rootScope.baseCurrenyRate[dataObj.currencyMaster.id] == null) {
                                if (type == "row") {
                                    $scope.secondArr[index].currencyMaster = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90345"]);
                                }
                                errorArr.push(0);

                            }
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].currencyMaster = false;

                            }
                            errorArr.push(1);
                        }
                    }


                    

                    if (dataObj.roe == undefined
                        || dataObj.roe == null
                        || dataObj.roe == "") {


                    }
                    else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_ROE,dataObj.roe)){
                        if (type == "row") {
                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39068"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }


                    else if (dataObj.currencyMaster != null 
                    		&& $rootScope.userProfile.selectedUserLocation.currencyMaster.id == dataObj.currencyMaster.id) {
                        if (dataObj.roe > 1) {

                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40017"]);
                            errorArr.push(0);
                        }
                        
                        if (dataObj.roe < 1) {

                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR38025"]);
                            errorArr.push(0);
                        }
                        

                    }

                    else if (dataObj.currencyMaster != null && $scope.dependData.billingCurrency.id == dataObj.currencyMaster.id) {


                        if (dataObj.roe != $scope.dependData.roe) {

                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40019"]);
                            errorArr.push(0);
                        }
                    }

                    else if (dataObj.currencyMaster != null
                        && $rootScope.userProfile.selectedUserLocation.currencyMaster.id != dataObj.currencyMaster.id) {
                        if (dataObj.roe == 1) {

                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR40018"]);
                            errorArr.push(0);
                        }
                        
                        if (dataObj.roe <=0) {

                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR38025"]);
                            errorArr.push(0);
                        }
                      
                    }

                    else {
                        if (type == "row") {
                            $scope.secondArr[index].roe = false;
                        }
                        //return false;
                        errorArr.push(1);
                    }


                    if (dataObj.unit != undefined
                        && dataObj.unit != null
                        && dataObj.unit != "") {

                        
                        if (isNaN(parseFloat(dataObj.unit))) {
                            if (type == "row") {
                                $scope.secondArr[index].unit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39070"]);
                            }
                            errorArr.push(0);

                        }
                        else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_FINANCE_UNIT,dataObj.unit)){
                            if (type == "row") {
                                $scope.secondArr[index].unit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39070"]);
                            }
                            errorArr.push(0);

                        }

                        else if (dataObj.noOfMinUnit == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].unit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39069"]);
                            }
                            errorArr.push(0);

                        }
                        else if (dataObj.unit < 0 || dataObj.unit >= 99999999999.999) {
                            if (type == "row") {
                                $scope.secondArr[index].unit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39070"]);
                            }
                            errorArr.push(0);

                        }


                        else {
                            if (type == "row") {
                                $scope.secondArr[index].unit = false;

                            }
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.amountPerUnit != undefined
                        && dataObj.amountPerUnit != null
                        && dataObj.amountPerUnit != "") {

                        
                        if (isNaN(parseFloat(dataObj.amountPerUnit))) {
                            if (type == "row") {
                                $scope.secondArr[index].amountPerUnit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39072"]);
                            }
                            errorArr.push(0);

                        }


                        else if(dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" 
                        	&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL,dataObj.amountPerUnit)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].amountPerUnit = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39072"]);
                             }
                             errorArr.push(0);
                   	 }
                        
                        else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL,dataObj.amountPerUnit)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].amountPerUnit = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39072"]);
                             }
                             errorArr.push(0);
                   	 }

                        else if (dataObj.amountPerUnit == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].amountPerUnit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39071"]);
                            }
                            errorArr.push(0);

                        }
                        else if (dataObj.amountPerUnit < 0 || dataObj.amountPerUnit >= 99999999999.999) {
                            if (type == "row") {
                                $scope.secondArr[index].amountPerUnit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39072"]);
                            }
                            errorArr.push(0);

                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].amountPerUnit = false;

                            }
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.amount != undefined
                        && dataObj.amount != null
                        && dataObj.amount != "") {

                       
                        if (isNaN(parseFloat(dataObj.amount))) {
                            if (type == "row") {
                                $scope.secondArr[index].amount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39074"]);
                            }
                            errorArr.push(0);

                        }
                        
                        else if(dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" 
                        	&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL,dataObj.amount)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].amount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39074"]);
                             }
                             errorArr.push(0);
                   	 }
                        
                        else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL,dataObj.amount)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].amount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39074"]);
                             }
                             errorArr.push(0);
                   	 }

                        else if (dataObj.amount == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].amount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39073"]);
                            }
                            errorArr.push(0);

                        }
                        else if (dataObj.amount < 0 || dataObj.amount >= 99999999999.999) {
                            if (type == "row") {
                                $scope.secondArr[index].amount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39074"]);
                            }
                            errorArr.push(0);

                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].amount = false;

                            }
                            errorArr.push(1);
                        }
                    }
                    else
                    	{
                    	 if (type == "row") {
                             $scope.secondArr[index].amount = true;
                             console.log($rootScope.nls["ERR39060"]);
                             $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39060"]);
                         }
                         errorArr.push(0);
                    	}


                    if (dataObj.localAmount != undefined
                        && dataObj.localAmount != null
                        && dataObj.localAmount != "") {

                       
                        if (isNaN(parseFloat(dataObj.localAmount))) {
                            if (type == "row") {
                                $scope.secondArr[index].localAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39076"]);
                            }
                            errorArr.push(0);

                        }

                        
                        else if(dataObj.currencyMaster != null && dataObj.currencyMaster.decimalPoint == "2" 
                        	&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL,dataObj.localAmount)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].localAmount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39076"]);
                             }
                             errorArr.push(0);
                   	 }
                        
                        else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL,dataObj.amount)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].localAmount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39076"]);
                             }
                             errorArr.push(0);
                   	 }

                        else if (dataObj.localAmount == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].localAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39075"]);
                            }
                            errorArr.push(0);

                        }
                        else if (dataObj.localAmount < 0 || dataObj.localAmount >= 99999999999.999) {
                            if (type == "row") {
                                $scope.secondArr[index].localAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39076"]);
                            }
                            errorArr.push(0);

                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].localAmount = false;

                            }
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.currencyAmount != undefined
                        && dataObj.currencyAmount != null
                        && dataObj.currencyAmount != "") {


                      
                        if (isNaN(parseFloat(dataObj.currencyAmount))) {
                            if (type == "row") {
                                $scope.secondArr[index].currencyAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39078"]);
                            }
                            errorArr.push(0);

                        }



                        else if(dataObj.currencyMaster != null && $scope.dependData.billingCurrency.decimalPoint == "2" 
                        	&& !CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_TWO_DECIMAL,dataObj.currencyAmount)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].currencyAmount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39078"]);
                             }
                             errorArr.push(0);
                   	 }
                        
                        else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_RATE_THREE_DECIMAL,dataObj.currencyAmount)){
                        	 if (type == "row") {
                                 $scope.secondArr[index].currencyAmount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39078"]);
                             }
                             errorArr.push(0);
                   	 }
                        
                        else if (dataObj.currencyAmount == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].currencyAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39077"]);
                            }
                            errorArr.push(0);

                        }
                        else if (dataObj.currencyAmount < 0 || dataObj.currencyAmount >= 99999999999.999) {
                            if (type == "row") {
                                $scope.secondArr[index].currencyAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR39078"]);
                            }
                            errorArr.push(0);

                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].currencyAmount = false;

                            }
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.debitCredit == undefined || dataObj.debitCredit == null
                        || dataObj.debitCredit == null
                        || dataObj.debitCredit == "") {
                       
                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].debitCredit = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };

                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.makeTableState = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        var validResult = validateObj(dataObj, index, "change");
                        if (validResult) {
                            fineArr.push(1);
                        } else {
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    console.log("fine arr", fineArr);
                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableState) {
                            changeLastStatus();
                            $scope.tableState({param: true});
                        }
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                    }
                };
                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.$watch('indexFocus', function (newvalue, oldValue) {
                    if (newvalue != null && oldValue != null && newvalue != oldValue) {
                        var focusErrArr = [];
                        angular.forEach($scope.tableData, function (dataObj, index) {
                           
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                $scope.secondArr[index].errRow = false;
                                focusErrArr.push(1);
                            } else {
                                $scope.secondArr[index].errRow = true;
                                focusErrArr.push(0);

                            }
                            //console.log();
                        });
                        if (focusErrArr.indexOf(0) < 0) {
                            if ($scope.tableState) {
                                $scope.tableState({param: true});
                            }
                        } else {
                            if ($scope.tableState) {
                                $scope.tableState({param: false});
                            }
                        }
                    }
                });


                $scope.updateSubJobSequence=function(invoiceCreditNoteDetail,subJobSequence)
                {
                	
                	invoiceCreditNoteDetail.subJobSequence=subJobSequence;
                }
                
                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;
                    
                    if($scope.tableData!=null && $scope.tableData.length!=0){
                    	for(var i=0;i<$scope.tableData.length;i++){
                    		 $scope.chargeCalculation(i);
                    	}
                    }else{
                    	$scope.calculateInvoiceHeaderAmounts();
                    }
                   

                };
                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                }

                $scope.eventRender = function (item) {
                    return {
                        label: item.eventName,
                        item: item
                    }
                }
                $scope.employeeRender = function (item) {
                    return {
                        label: item.employeeName,
                        item: item
                    }
                }


                $scope.serviceSearch = function (object) {

                    return $scope.shipmentServiceList;

                }

                $scope.localSearch = function (object) {

                    console.log("ajaxChargeEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    console.log("search data", $scope.searchDto);
                    return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.chargeMasterList = data.responseObject.searchResult;
                                return $scope.chargeMasterList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Charge');
                        }
                    );

                }

                $scope.chargeRender = function (item) {

                    return {
                        label: item.chargeMaster.chargeCode,
                        item: item.chargeMaster
                    }
                }

                $scope.shipmentServiceRender = function (item) {

                    return {
                        label: item.id,
                        item: item
                    }
                }

                $scope.chargeChange = function (index) {

                   $scope.tableData[index].chargeName = $scope.tableData[index].chargeMaster.chargeName;
                   $scope.tableData[index].chargeCode = $scope.tableData[index].chargeMaster.chargeCode;

                   /*  if($scope.tableData[index].crn=='Revenue')
                     {
                    	 $scope.tableData[index].accountCode=$scope.tableData[index].glChargeMaster.revenueAccountCode;
                    	 $scope.tableData[index].subledgerCode=$scope.tableData[index].glChargeMaster.revenueSubledgerCode;
                     }

                     if($scope.tableData[index].crn=='Cost')
                     {
                    	 $scope.tableData[index].accountCode=$scope.tableData[index].glChargeMaster.costAccountCode;
                    	 $scope.tableData[index].subledgerCode=$scope.tableData[index].glChargeMaster.costSubledgerCode;
                     }
                     if($scope.tableData[index].crn=='Neutral')
                     {
                    	 $scope.tableData[index].accountCode=$scope.tableData[index].glChargeMaster.neutralAccountCode;
                    	 $scope.tableData[index].subledgerCode=$scope.tableData[index].glChargeMaster.neutralSubledgerCode;
                     }*/

                }

                $scope.ajaxInvoiceDetailCurrencyEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


                    return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.invoiceDetailCurrencyList = data.responseObject.searchResult;
                                return $scope.invoiceDetailCurrencyList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Currency List');
                        }
                    );


                }


                $scope.currencyRender = function (item) {
                    return {
                        label: item.currencyCode,
                        item: item
                    }
                }

                $scope.selectedInvoiceDetailCurrency = function (obj,invoiceCreditNoteDetail) {
                    
                   
                    if ($rootScope.baseCurrenyRate[obj.id] != null && $rootScope.baseCurrenyRate[obj.id] != undefined && $rootScope.baseCurrenyRate[obj.id] != '') {
                        invoiceCreditNoteDetail.roe = $rootScope.baseCurrenyRate[obj.id].csell;
                    }
                    else {
                        invoiceCreditNoteDetail.roe = null;
                    }

                }

                $scope.updateCharge=function()
                {
                	
                	console.log("updateCharge called");
                	for(var i=0;i<$scope.dependData.invoiceCreditNoteDetailList.length;i++)
                		{
                		 $scope.chargeCalculation(i);
                		}
                }
                
                $scope.chargeCalculation = function (index) {
                	
                    $rootScope.clientMessage = null;
                    if ($scope.dependData.billingCurrency == undefined
                        || $scope.dependData.billingCurrency == null
                        || $scope.dependData.billingCurrency == ""
                        || $scope.dependData.billingCurrency.id == null) {
                        $rootScope.clientMessage = $rootScope.nls["ERR38018"];
                        return false;
                    }

                    if ($scope.dependData.roe == null || $scope.dependData.roe == undefined) {
                        $rootScope.clientMessage = $rootScope.nls["ERR38021"];
                        return false;
                    }

                    if (index != null && $scope.tableData[index] !=null && $scope.tableData[index]!=undefined) {
                        $scope.invoiceCreditNoteDetail = $scope.tableData[index];
                        if ($scope.dependData.billingCurrency.id == $scope.invoiceCreditNoteDetail.currencyMaster.id) {

                            $scope.invoiceCreditNoteDetail.roe = $scope.dependData.roe;
                        }
                    }

                    if ($scope.invoiceCreditNoteDetail.currencyMaster.id == $rootScope.userProfile.selectedUserLocation.currencyMaster.id) {
                        $scope.invoiceCreditNoteDetail.roe = 1;
                    }

                    
                    if($scope.documentType!='CRN-REVENUE')
            		{
                    	//amount calculation
                        if ($scope.invoiceCreditNoteDetail.unit != null && $scope.invoiceCreditNoteDetail.unit != undefined &&
                            $scope.invoiceCreditNoteDetail.amountPerUnit != null && $scope.invoiceCreditNoteDetail.amountPerUnit != undefined) {
                            $scope.invoiceCreditNoteDetail.amount = $scope.invoiceCreditNoteDetail.unit * $scope.invoiceCreditNoteDetail.amountPerUnit;
                            $scope.invoiceCreditNoteDetail.amount = $rootScope.currency($scope.invoiceCreditNoteDetail.currencyMaster,$scope.invoiceCreditNoteDetail.amount); 
                        }
            		}
                
                    //local amount calculation

                    if ($scope.invoiceCreditNoteDetail.roe != null && $scope.invoiceCreditNoteDetail.roe != undefined &&
                        $scope.invoiceCreditNoteDetail.amount != null && $scope.invoiceCreditNoteDetail.amount != undefined) {
                        $scope.invoiceCreditNoteDetail.localAmount = $rootScope.roundOffCurrencyWithLocation($scope.invoiceCreditNoteDetail.amount * $scope.invoiceCreditNoteDetail.roe);
                        $scope.invoiceCreditNoteDetail.localAmount = $rootScope.currency($scope.dependData.localCurrency,$scope.invoiceCreditNoteDetail.localAmount);
                    }

                    //currency amount calculation
                    if ($scope.invoiceCreditNoteDetail.localAmount != null && $scope.invoiceCreditNoteDetail.localAmount != undefined &&
                        $scope.dependData.roe != null && $scope.dependData.roe != undefined) {
                        $scope.invoiceCreditNoteDetail.currencyAmount = $rootScope.roundOffCurrencyWithLocation($scope.invoiceCreditNoteDetail.localAmount / $scope.dependData.roe);
                        $scope.invoiceCreditNoteDetail.currencyAmount = $rootScope.currency($scope.dependData.billingCurrency,$scope.invoiceCreditNoteDetail.currencyAmount);
                    }
                    
                           
                    $scope.taxCalculate($scope.invoiceCreditNoteDetail);                
                    $scope.calculateInvoiceHeaderAmounts();

                }

                $scope.taxCalculate = function(invoiceCreditNoteDetail){
                	if(invoiceCreditNoteDetail.invoiceCreditNoteTaxList!=undefined && invoiceCreditNoteDetail.invoiceCreditNoteTaxList!=null && invoiceCreditNoteDetail.invoiceCreditNoteTaxList.length!=0){
                    	for(var i=0;i<invoiceCreditNoteDetail.invoiceCreditNoteTaxList.length;i++){
                    		invoiceCreditNoteDetail.invoiceCreditNoteTaxList[i].amount = (invoiceCreditNoteDetail.invoiceCreditNoteTaxList[i].serviceTaxPercentage.percentage/100) * parseFloat(invoiceCreditNoteDetail.localAmount);
                    		invoiceCreditNoteDetail.invoiceCreditNoteTaxList[i].amount = $rootScope.currency($scope.dependData.localCurrency,invoiceCreditNoteDetail.invoiceCreditNoteTaxList[i].amount);
                    	}
                    }  
                }
                
                $scope.changeDebitCredit = function (invoiceCreditNoteDetail) {

                    if (invoiceCreditNoteDetail.crn == 'Revenue' || invoiceCreditNoteDetail.crn == 'Neutral') {
                        invoiceCreditNoteDetail.debitCredit = 'Credit';
                    }
                    else if (invoiceCreditNoteDetail.crn = 'Cost') {
                        invoiceCreditNoteDetail.debitCredit = 'Debit';
                    }

                }

                $scope.calculateInvoiceHeaderAmounts = function () {


                    $scope.totalAmountReceived = 0.0;
                    $scope.totalDebitLocalAmount = 0.0;
                    $scope.totalDebitCurrencyAmount = 0.0;
                    $scope.totalCreditLocalAmount = 0.0;
                    $scope.totalCreditCurrencyAmount = 0.0;
                    $scope.netLocalAmount = 0.0;
                    $scope.netCurrencyAmount = 0.0;
                    $scope.totalOutstandingAmount = 0.0;
                    
                    if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData != '') {
                        for (var i = 0; i < $scope.tableData.length; i++) {
                            if ($scope.tableData[i].localAmount == null || $scope.tableData[i].localAmount == undefined 
                            		|| isNaN($scope.tableData[i].localAmount)) {
                                $scope.tableData[i].localAmount = 0.0;
                            }

                            if ($scope.tableData[i].debitCredit == 'Debit') {
                            	
                                $scope.totalDebitLocalAmount = parseFloat($scope.totalDebitLocalAmount) + parseFloat($scope.tableData[i].localAmount);
                                $scope.totalDebitCurrencyAmount = parseFloat($scope.totalDebitCurrencyAmount) + parseFloat($scope.tableData[i].currencyAmount);
                            }
                            if ($scope.tableData[i].debitCredit == 'Credit') {
                                $scope.totalCreditLocalAmount = parseFloat($scope.totalCreditLocalAmount) + parseFloat($scope.tableData[i].localAmount);
                                $scope.totalCreditCurrencyAmount = parseFloat($scope.totalCreditCurrencyAmount) + parseFloat($scope.tableData[i].currencyAmount);

                            }

                        }


                        $scope.netLocalAmount = $scope.totalCreditLocalAmount - $scope.totalDebitLocalAmount;
                        $scope.netCurrencyAmount = $scope.totalCreditCurrencyAmount - $scope.totalDebitCurrencyAmount;
                        $scope.totalOutstandingAmount = $scope.netCurrencyAmount - $scope.totalAmountReceived;

                        // Round decimal based on default master configuration
                        $scope.totalDebitLocalAmount = $rootScope.currency($scope.dependData.localCurrency,$scope.totalDebitLocalAmount);
                        $scope.totalCreditLocalAmount = $rootScope.currency($scope.dependData.localCurrency,$scope.totalCreditLocalAmount);
                        $scope.totalAmountReceived = $rootScope.currency($scope.dependData.billingCurrency,$scope.totalAmountReceived);
                        $scope.totalDebitCurrencyAmount = $rootScope.currency($scope.dependData.billingCurrency,$scope.totalDebitCurrencyAmount);
                        $scope.totalCreditCurrencyAmount = $rootScope.currency($scope.dependData.billingCurrency,$scope.totalCreditCurrencyAmount); 
                        $scope.netLocalAmount = $rootScope.currency($scope.dependData.localCurrency,$scope.netLocalAmount);
                        $scope.netCurrencyAmount = $rootScope.currency($scope.dependData.billingCurrency,$scope.netCurrencyAmount);
                        $scope.totalOutstandingAmount = $rootScope.currency($scope.dependData.billingCurrency,$scope.totalOutstandingAmount);

                    }
                    $scope.calcFn({
                        param: {
                            localAmount: Math.abs($scope.netLocalAmount),
                            currencyAmount: Math.abs($scope.netCurrencyAmount),
                            amountReceived: Math.abs($scope.totalAmountReceived),
                            outstandingAmount: Math.abs($scope.totalOutstandingAmount),
                            totalDebitLocalAmount: Math.abs($scope.totalDebitLocalAmount),
                            totalDebitCurrencyAmount: Math.abs($scope.totalDebitCurrencyAmount),
                            totalCreditLocalAmount: Math.abs($scope.totalCreditLocalAmount),
                            totalCreditCurrencyAmount: Math.abs($scope.totalCreditCurrencyAmount),
                            netLocalAmount: Math.abs($scope.netLocalAmount),
                            netCurrencyAmount: Math.abs($scope.netCurrencyAmount)
                        }
                    });
                }


                $scope.unique = function (obj, list) {
                    var found, y;
                    found = false;
                    for (y = 0; y < list.length; y++) {
                        if (obj.taxCode == list[y].taxCode) {
                            found = true;
                            console.log("Duplicate object…");
                            list[y].amount = list[y].amount + obj.amount;
                            break;
                        }
                    }
                    if (!found) {
                        console.log("Object Added….");
                        list.push(obj);
                    }
                    return found;
                }

                $scope.findServiceChargeTaxCategoryMapping = function (invoiceCreditNoteDetail) {
                	if($rootScope.dynamicFormFieldMap.get('Invoice > Tax')=='true'){
                    $scope.invoiceCreditNoteTaxList = [];
                    if (invoiceCreditNoteDetail.shipmentServiceDetail.serviceMaster != null 
                    		&& invoiceCreditNoteDetail.shipmentServiceDetail.serviceMaster != undefined
                        && invoiceCreditNoteDetail.shipmentServiceDetail.serviceMaster != '' 
                        	&& invoiceCreditNoteDetail.chargeMaster != null &&
                       invoiceCreditNoteDetail.chargeMaster != undefined && invoiceCreditNoteDetail.chargeMaster != '') {

                        InvoiceServiceTaxPercentage.get(
                            {
                                serviceId: invoiceCreditNoteDetail.shipmentServiceDetail.serviceMaster.id,
                                chargeId:invoiceCreditNoteDetail.chargeMaster.id,
                                countryId: $rootScope.userProfile.selectedUserLocation.countryMaster.id,
                                companyId: $rootScope.userProfile.selectedCompany.id

                            },
                            function (data) {
                                if (data.responseCode == 'ERR0') {
                                    $scope.serviceTaxPercentageList = data.responseObject.taxPercentageList;
                                    if ($scope.serviceTaxPercentageList != null && $scope.serviceTaxPercentageList != undefined
                                        && $scope.serviceTaxPercentageList != '' && $scope.serviceTaxPercentageList.length > 0) {

                                        for (var i = 0; i < $scope.serviceTaxPercentageList.length; i++) {
                                            var invoiceCreditNoteTax = {};
                                            invoiceCreditNoteTax.serviceTaxPercentage = $scope.serviceTaxPercentageList[i];
                                            invoiceCreditNoteTax.taxCode = $scope.serviceTaxPercentageList[i].taxCode;
                                            invoiceCreditNoteTax.percentage = $scope.serviceTaxPercentageList[i].percentage;
                                            $scope.invoiceCreditNoteTaxList.push(invoiceCreditNoteTax);
                                        }

                                        $scope.calcFn({
                                            param: {
                                                localAmount: $scope.netLocalAmount,
                                                currencyAmount: $scope.netCurrencyAmount,
                                                amountReceived: $scope.totalAmountReceived,
                                                outstandingAmount: $scope.totalOutstandingAmount,
                                                totalDebitLocalAmount: $scope.totalDebitLocalAmount,
                                                totalDebitCurrencyAmount: $scope.totalDebitCurrencyAmount,
                                                totalCreditLocalAmount: $scope.totalCreditLocalAmount,
                                                totalCreditCurrencyAmount: $scope.totalCreditCurrencyAmount,
                                                netLocalAmount: $scope.netLocalAmount,
                                                netCurrencyAmount: $scope.netCurrencyAmount,
                                                invoiceCreditNoteTaxList: $scope.invoiceCreditNoteTaxList
                                            }
                                        });

                                       invoiceCreditNoteDetail.isTaxable = true;
                                       invoiceCreditNoteDetail.invoiceCreditNoteTaxList=$scope.invoiceCreditNoteTaxList;
                                    }
                                    else {

                                       invoiceCreditNoteDetail.isTaxable = false;
                                       invoiceCreditNoteDetail.invoiceCreditNoteTaxList=[];
                                        $scope.calcFn({
                                            param: {
                                                localAmount: $scope.netLocalAmount,
                                                currencyAmount: $scope.netCurrencyAmount,
                                                amountReceived: $scope.totalAmountReceived,
                                                outstandingAmount: $scope.totalOutstandingAmount,
                                                totalDebitLocalAmount: $scope.totalDebitLocalAmount,
                                                totalDebitCurrencyAmount: $scope.totalDebitCurrencyAmount,
                                                totalCreditLocalAmount: $scope.totalCreditLocalAmount,
                                                totalCreditCurrencyAmount: $scope.totalCreditCurrencyAmount,
                                                netLocalAmount: $scope.netLocalAmount,
                                                netCurrencyAmount: $scope.netCurrencyAmount,
                                                invoiceCreditNoteTaxList: []
                                            }
                                        });

                                    }

                                } else {

                                    console.log(" Failed " + data.responseDescription);
                                    $scope.calcFn({
                                        param: {
                                            localAmount: $scope.netLocalAmount,
                                            currencyAmount: $scope.netCurrencyAmount,
                                            amountReceived: $scope.totalAmountReceived,
                                            outstandingAmount: $scope.totalOutstandingAmount,
                                            totalDebitLocalAmount: $scope.totalDebitLocalAmount,
                                            totalDebitCurrencyAmount: $scope.totalDebitCurrencyAmount,
                                            totalCreditLocalAmount: $scope.totalCreditLocalAmount,
                                            totalCreditCurrencyAmount: $scope.totalCreditCurrencyAmount,
                                            netLocalAmount: $scope.netLocalAmount,
                                            netCurrencyAmount: $scope.netCurrencyAmount,
                                            invoiceCreditNoteTaxList: []
                                        }
                                    });

                                   invoiceCreditNoteDetail.isTaxable = false;
                                }
                            },
                            function (error) {
                                console.log(" Failed : " + error)
                            });

                    }
                
                	}
                }

                $scope.moveToNextField = function (nextIdValue, index) {
                    console.log("id : ", nextIdValue)
                    $rootScope.navigateToNextField(nextIdValue + index);

                }
                
                $scope.getRate=function(){
                	$scope.searchDto={};
            		$scope.searchDto.param3=$scope.documentType;
            		$scope.searchDto.param4=$scope.dependData.customerAgent;
            		
            		if($scope.dependData.customerAgent==null || $scope.dependData.customerAgent==undefined){
            			Notification.error($rootScope.nls["ERR38003"]);
            			return;
            		}
            		
                	//shipmentUid
                	if ($scope.shipmentServiceDetail != null
							&& $scope.shipmentServiceDetail != undefined
							&& $scope.shipmentServiceDetail != '') {
                		$scope.searchDto.param1=$scope.shipmentServiceDetail.serviceUid;
                		$scope.searchDto.param2=null;
                	}
                	//masterUid
            		else if ($scope.consol != null
								&& $scope.consol != undefined
								&& $scope.consol != ''
								&& $scope.consol.id != undefined) {
	                		$scope.searchDto.param1=null
	                		$scope.searchDto.param2=$scope.consol.consolUid;
					}
                		
                		RatesFromProvision.query($scope.searchDto).$promise.then(function(data, status) {
                			if(data.responseCode=='ERR0'){
                				
                				if(data.params!=null && data.params!=undefined && data.params.length>0){
                        			Notification.error($rootScope.nls["ERR97000"].replace("%s",data.params));
                        		}
                				
                				if(data.responseObject!=null && data.responseObject !=undefined){
                				
                					if($scope.tableData==undefined || $scope.tableData==null || $scope.tableData.length==0){
										$scope.tableData = [];
									}else{
										if($scope.tableData.length>0){
											var lastIndex = $scope.tableData.length-1;
											if(isEmptyRow($scope.tableData[lastIndex])){
					                			$scope.tableData.splice(lastIndex,1); 
					                            delete  $scope.tableData[lastIndex]
											}
										}
									}
                					
                					for(var i=0;i<data.responseObject.length;i++){
                						for(j=0;j<$scope.shipmentServiceList.length;j++)
                						{
                							if($scope.shipmentServiceList[j].serviceUid===data.responseObject[i].shipmentServiceDetail.serviceUid){
                								data.responseObject[i].shipmentServiceDetail = $scope.shipmentServiceList[j];
                								break;
                							}
                						}
                						
                						$scope.tableData.push(data.responseObject[i]);
		                        	}
	            					
                					for (var i = 0; i < $scope.tableData.length; i++) {
	            						$scope.findServiceChargeTaxCategoryMapping($scope.tableData[i]);
	            						$scope.chargeCalculation(i);
	            					}
                				}
                			}
                        });
                }

               
                
                $scope.getGLChargeMappingList = function (object, serviceId, index) {

                    if (serviceId == null || serviceId == undefined || serviceId == '') {


							 if ($scope.dependData.masterUid != null) {
								 serviceId = $scope.consol.serviceMaster.id;
													
												} else {
													$scope.secondArr[index] = {};
													$scope.secondArr[index].errTextArr = [];

													$scope.secondArr[index].serviceID = true;
													$scope.secondArr[index].errTextArr
															.push($rootScope.nls["ERR40010"]);

													errorArr.push(0);
												}
                    }

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return GLChargeMappingList.fetch({"serviceId": serviceId}, $scope.searchDto).$promise.then(function (data, status) {


                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.gLChargeMappingList = [];
                                
                                for(var i=0;i<data.responseObject.searchResult.length;i++){
                                	$scope.gLChargeMappingList.push(data.responseObject.searchResult[i].chargeMaster);
                                }
                                
                                return $scope.gLChargeMappingList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching List');
                        }
                    );


                }
                $scope.copyObj=function(copyObj){
                	var tmpObj = cloneService.clone(copyObj);
                	tmpObj.id=null;
                	tmpObj.systemTrack=null;
                	tmpObj.versionLock=0;
                	
                	if($scope.tableData==undefined || $scope.tableData==null || $scope.tableData.length==0){
                		$scope.tableData = [];
                	}else{
                		var tmpIndex = $scope.tableData.length-1;
                		if(isEmptyRow($scope.tableData[tmpIndex])){
                			$scope.tableData.splice(tmpIndex,1); 
                            delete  $scope.tableData[tmpIndex]
                            
                            if($scope.tableData==undefined || $scope.tableData==null || $scope.tableData.length==0){
                        		$scope.tableData = [];
                        	}
                		}
                	}
                	
                	$scope.tableData.push(tmpObj);
                	$scope.chargeCalculation($scope.tableData.length-1);
                }
                

            },1500);
        }
    }
}]);