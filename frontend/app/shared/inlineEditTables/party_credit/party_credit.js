/**
 * Created by saravanan on 23/6/16.
 */
app.directive('partyCreditTable',['$timeout','$compile','$rootScope','ValidateUtil','CountryWiseLocation',
        'ServiceList','DivisionList', 'CommonValidationService', 'appConstant',
    function($timeout,$compile,$rootScope,ValidateUtil,CountryWiseLocation,
        ServiceList,DivisionList, CommonValidationService, appConstant){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/party_credit/party_credit.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            tableState:"&",
            tableCheck:"=",
            calcFn:"&",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                var dirtArr = [];


                $scope.currencyCode = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode;

                $scope.scrollConfig = $rootScope.ngScrollConfigScroll = {
                    "cursorcolor": '#3F8DEB',
                    "cursorwidth": '8px',
                    "smoothscroll": true,
                    "autohidemode": false
                };

                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                    	$scope.secondArr[index]=[];
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                    	if( $scope.tableData==undefined ||$scope.tableData==null ){
                    		$scope.tableData=[];
                    	}
                    	$scope.tableData.push({});
                        $scope.tableState({param: false});
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].creditlocationNameFocus = true;
                        }, 10);
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].creditlocationNameFocus = true;
                        }, 10);
                    }
                };


                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }


                $scope.indexFocus = null;

                $scope.secondArr = [];

                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                $scope.ajaxCreditLocationEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CountryWiseLocation.fetch({"countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.locationList = data.responseObject.searchResult;
                                return $scope.locationList;
                                //$scope.showCreditLocationList=true;

                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Locations');
                        }
                    );

                };
                //$scope.ajaxCreditLocationEvent(null);

                $scope.ajaxCreditServiceEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return ServiceList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.serviceList = data.responseObject.searchResult;
                                return $scope.serviceList;
                                //$scope.showCreditServiceList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Services');
                        }
                    );

                }
                //$scope.ajaxCreditServiceEvent(null);

                $scope.ajaxCreditDivisionEvent = function (object) {

                    console.log("ajaxStateEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


                    return DivisionList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.divisionList = data.responseObject.searchResult;
                                return $scope.divisionList;
                                //$scope.showCreditDivisionList=true;

                            }
                        },
                        function (error) {
                            console.error('Error while fetching division');
                        });

                }
                //$scope.ajaxCreditDivisionEvent(null);


                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.locationMaster == null || dataObj.locationMaster.id == undefined || dataObj.locationMaster.id == null || dataObj.locationMaster.id == "") {
                        console.log("Location is Mandatory..........")
                        if (type == "row") {
                            $scope.secondArr[index].locationName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR44"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }


                    else {


                        if (dataObj.locationMaster.status == 'Block') {
                            console.log("Selected Location is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].locationName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6028"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }
                        else if (dataObj.locationMaster.status == 'Hide') {
                            console.log("Selected Location is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].locationName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6029"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].locationName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (dataObj.serviceMaster != null && dataObj.serviceMaster.id != undefined && dataObj.serviceMaster.id != null && dataObj.serviceMaster.id != "") {
                        if (dataObj.serviceMaster.status == 'Block') {
                            console.log("Selected Service is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR24"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.serviceMaster.status == 'Hide') {
                            console.log("Selected Service is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR25"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = false;

                            }
                            //return false;
                            errorArr.push(1)
                        }


                    }
                    else {
                        if (dataObj.hasOwnProperty('divisionMaster') && (dataObj.divisionMaster.id == null
                            || dataObj.divisionMaster.id == "")) {
                            if (type == "row") {
                                $scope.secondArr[index].divisionName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6052"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].divisionName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.divisionMaster != null && dataObj.divisionMaster.id != undefined &&
                        dataObj.divisionMaster.id != null &&
                        dataObj.divisionMaster.id != "") {


                        if (dataObj.divisionMaster.status == 'Block') {

                            if (type == "row") {
                                $scope.secondArr[index].divisionName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.divisionMaster.status == 'Hide') {

                            if (type == "row") {
                                $scope.secondArr[index].divisionName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR23"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].divisionName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }
                    else {
                        if (dataObj.serviceMaster == null || dataObj.serviceMaster.id == undefined
                            || dataObj.serviceMaster.id == null
                            || dataObj.serviceMaster.id == "") {
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6052"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].serviceName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }

                    if (dataObj.creditDays != undefined
                        && dataObj.creditDays != null
                        && dataObj.creditDays != "") {

                        if (dataObj.creditDays == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].creditDays = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6026"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }
                        if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CREDIT_DAYS,dataObj.creditDays)){
                    		if (type == "row") {
                                $scope.secondArr[index].creditDays = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6022"]);
                            }
                            errorArr.push(0);
            			} else {
                            if (type == "row") {
                                $scope.secondArr[index].creditDays = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }
                    else {

                        if (dataObj.creditAmount == undefined || dataObj.creditAmount == "" || dataObj.creditAmount == null) {

                            if (type == "row") {
                                $scope.secondArr[index].creditDays = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6027"]);
                            }
                            //return false;
                            errorArr.push(0);
                        }


                    }

                    if (dataObj.publishCreditDays != undefined
                        && dataObj.publishCreditDays != null
                        && dataObj.publishCreditDays != "") {

                        if (dataObj.publishCreditDays == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].publishCreditDays = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6020"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }
                        if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_PUBLISHED_CREDIT_DAYS,dataObj.publishCreditDays)){
                    		if (type == "row") {
                    			$scope.secondArr[index].publishCreditDays = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6023"]);
                            }
                            errorArr.push(0);
            			} else {
                            if (type == "row") {
                                $scope.secondArr[index].publishCreditDays = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }

                    if (dataObj.creditAmount != undefined
                        && dataObj.creditAmount != null
                        && dataObj.creditAmount != "") {
                        if (dataObj.creditAmount == 0) {
                            if (type == "row") {
                                $scope.secondArr[index].creditAmount = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6021"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CREDIT_AMOUNT,dataObj.creditAmount)){
                    		if (type == "row") {
                    			 $scope.secondArr[index].creditAmount = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6024"]);
                            }
                            errorArr.push(0);
            			} else {
                            if (type == "row") {
                                $scope.secondArr[index].creditAmount = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }
                    else {

                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].creditlocationNameFocus) {
                        $scope.secondArr[errorOnRowIndex].creditlocationNameFocus = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].creditlocationNameFocus = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.tableState({param: true});
                    $scope.indexFocus = null;
                };

                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                }
                $scope.locationRender = function (item) {
                    return {
                        label: item.locationName,
                        item: item
                    }
                }
                $scope.serviceRender = function (item) {
                    return {
                        label: item.serviceName,
                        item: item
                    }
                }
                $scope.divisionRender = function (item) {
                    return {
                        label: item.divisionName,
                        item: item
                    }
                }
                $scope.selectCreditLocation = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.selectServiceName = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.selectDivisionName = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;


            },2);
            
        }
    }
}]);