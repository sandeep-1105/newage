/**
 * Created by saravanan on 23/6/16.
 */
app.directive('dimensionTable', ['$timeout', '$compile', '$rootScope', '$stateParams', 'appConstant', 'CommonValidationService',
    function($timeout, $compile, $rootScope, $stateParams, appConstant, CommonValidationService) {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/inlineEditTables/dimensionTable/dimensionTable.html',
            scope: {
                countryMaster: "=",
                indexKey: "=",
                unitType: "=",
                unitValue: "=",
                tableData: "=",
                showErrors: "=",
                calcFn: "&",
                addRole: "=",
                editRole: "=",
                deleteRole: "=",
                fnRole: "&",
                tableCheck: "=?",
                toggleFn: "=",
                reTrigger: "=?",
                btnIndex: "="
            },
            controller: function($scope, $rootScope, $popover, $modal) {
                $timeout(function() {
                    $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);

                    $scope.tableisChecked = true;

                    function isEmptyRow(obj) {
                        //return (Object.getOwnPropertyNames(obj).length === 0);
                        var isempty = true; //  empty
                        if (!obj) {
                            return isempty;
                        }

                        var k = Object.getOwnPropertyNames(obj);
                        for (var i = 0; i < k.length; i++) {

                            //which is using for dimensions in Consol.While moving from shipment to consol we added some serviceId and ServiceDocumentId, since we are doing like that\\
                            if (k[i] == 'serviceId' || k[i] == 'serviceDocumentId') {
                                continue;
                            }

                            if (obj[k[i]]) {
                                isempty = false; // not empty
                                break;

                            }
                        }
                        return isempty;
                    }

                    $scope.reTrigger = function() {

                        $timeout(function() {
                            initTable();
                        }, 1000);
                    }


                    $scope.tableCheck = function() {

                        $scope.tableisChecked = false;
                        var fineArr = [];
                        $scope.secondArr = [];
                        var validResult;
                        var finalData = [];
                        angular.forEach($scope.tableData, function(dataObj, index) {
                            $scope.secondArr[index] = {};
                            if (isEmptyRow(dataObj)) {
                                validResult = true;
                                $scope.tableData.splice(index, 1);
                                delete $scope.tableData[index]
                            } else {
                                validResult = validateObj(dataObj, index, "row");
                                finalData.push(dataObj);
                            }
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                $scope.secondArr[index].errRow = true;
                                fineArr.push(0);
                            }
                        });
                        if (fineArr.indexOf(0) < 0) {
                            $scope.tableData = finalData;
                            return true;
                        } else {
                            return false;
                        }
                    };

                    $scope.changeDimensionDirective = function() {
                        if ($scope.toggleFn) {
                            $scope.toggleFn();
                        }
                    };

                    $scope.$watch('tableData', function(newValue, oldValue) {

                        if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData.length != 0) {
                            if ($scope.tableData.length == 1) {
                                if (!isEmptyRow($scope.tableData[0])) {
                                    $scope.changeCheckSum();
                                } else {
                                    $scope.totalNoOfPieces = 0;
                                    $scope.dimensionVolWeight = 0;
                                    $scope.dimensionGrossWeight = 0;
                                    $scope.dimensionGrossWeightInKg = 0;
                                }
                            } else {
                                $scope.changeCheckSum();
                            }
                        } else {
                            $scope.changeCheckSum();
                        }


                    }, true);

                    $scope.$watch('unitValue', function(newValue, oldValue) {
                        /* if(newValue && newValue!=oldValue) {
                             $scope.changeCheckSum();
                             if($scope.calcFn){
                                 $scope.calcFn({param:{grossWeight:$scope.dimensionGrossWeight,volumeWeight:$scope.dimensionVolWeight,totalPieces:$scope.totalNoOfPieces,dimensionGrossWeightInKg:$scope.dimensionGrossWeightInKg}});
                             }
         
                         }*/
                    });



                    $scope.indexFocus = null;

                    $scope.secondArr = [];

                    $scope.totalNoOfPieces = 0;
                    $scope.dimensionVolWeight = 0.0;
                    $scope.dimensionGrossWeight = 0.0;
                    $scope.dimensionGrossWeightInKg = 0.0;

                    $scope.calculateSum = function(item) {

                        if (item.grossWeight != null) {
                            if ($scope.unitType) {
                                item.grossWeightKg = Math.round((item.grossWeight * 0.453592) * 100) / 100;
                            } else {
                                item.grossWeightKg = Math.round(item.grossWeight * 100) / 100;
                            }
                        }

                        if (item.noOfPiece > 0 && item.length > 0 &&
                            item.width > 0 && item.height > 0) {
                            item.volWeight = (item.noOfPiece * (item.length * item.width * item.height)) / $scope.unitValue;
                            item.volWeight = Math.round(item.volWeight * 100) / 100;
                        } else {
                            item.volWeight = 0;
                        }
                        $scope.totalNoOfPieces += item.noOfPiece ? parseInt(isNaN(item.noOfPiece) ? 0 : item.noOfPiece) : 0;
                        $scope.dimensionVolWeight += item.volWeight ? parseFloat(item.volWeight) : 0;
                        $scope.dimensionGrossWeight += item.grossWeight ? parseFloat(item.grossWeight) : 0;
                        $scope.dimensionGrossWeightInKg += item.grossWeightKg ? parseFloat(item.grossWeightKg) : 0;
                        $scope.dimensionVolWeight = Math.round($scope.dimensionVolWeight * 100) / 100;
                        $scope.dimensionGrossWeight = Math.round($scope.dimensionGrossWeight * 100) / 100;
                        $scope.dimensionGrossWeightInKg = Math.round($scope.dimensionGrossWeightInKg * 100) / 100;
                        $scope.totalNoOfPieces = $scope.totalNoOfPieces;
                    };


                    $scope.addRow = function() {
                        var fineArr = [];
                        $scope.secondArr = [];
                        angular.forEach($scope.tableData, function(dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            $scope.secondArr[index] = {};
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                $scope.secondArr[index].errRow = true;
                                fineArr.push(0);
                            }
                            //console.log();
                        });
                        if (fineArr.indexOf(0) < 0) {
                            if ($scope.tableData == undefined || $scope.tableData == null)
                                $scope.tableData = [];
                            $scope.tableData.push({});
                            $rootScope.navigateToNextField($scope.indexKey + 'noOfPiece' + ($scope.tableData.length - 1))
                            $scope.secondArr.push({ "errRow": false });
                            $scope.indexFocus = null;
                            $scope.secondArr[$scope.tableData.length - 1].noOfPieceFocus = true;
                            /* if($scope.dimensionState){
                                 $scope.dimensionState({param:true});
                             }*/
                        }
                    };

                    var myOtherModal = $modal({ scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true });
                    var errorOnRowIndex = null;

                    $scope.errorShow = function(errorObj, index) {
                        $scope.errList = errorObj.errTextArr;
                        // Show when some event occurs (use $promise property to ensure the template has been loaded)
                        errorOnRowIndex = index;
                        myOtherModal.$promise.then(myOtherModal.show);
                    };
                    //error validation rules
                    var validateObj = function(dataObj, index, type) {

                        var errorArr = [];
                        if (type == "row") {
                            $scope.secondArr[index].errTextArr = [];
                        }
                        if (dataObj.noOfPiece == null || dataObj.noOfPiece == undefined || dataObj.noOfPiece === "" || parseInt(dataObj.noOfPiece) <= 0) {
                            if (type == "row") {
                                $scope.secondArr[index].noOfPiece = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22062"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_NO_OF_PIECES, dataObj.noOfPiece)) {
                                if (type == "row") {
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22105"]);
                                    $scope.secondArr[index].noOfPiece = true;
                                }
                                errorArr.push(0);
                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].noOfPiece = false;
                                }
                                errorArr.push(1);
                            }
                        }


                        if (dataObj.length == null || dataObj.length == undefined || dataObj.length === "" || parseInt(dataObj.length) <= 0) {
                            if (type == "row") {
                                $scope.secondArr[index].length = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22064"]);
                            }
                            errorArr.push(0);

                        } else {
                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_LENGTH, dataObj.length)) {
                                if (type == "row") {
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22106"]);
                                    $scope.secondArr[index].length = true;
                                }
                                errorArr.push(0);
                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].length = false;
                                }
                                errorArr.push(1);
                            }
                        }


                        if (dataObj.width == null || dataObj.width == undefined || dataObj.width === "" || parseInt(dataObj.width) <= 0) {
                            if (type == "row") {
                                $scope.secondArr[index].width = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22066"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_WIDTH, dataObj.width)) {
                                if (type == "row") {
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22107"]);
                                    $scope.secondArr[index].width = true;
                                }
                                errorArr.push(0);
                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].width = false;
                                }
                                errorArr.push(1);
                            }
                        }




                        if (dataObj.height == null || dataObj.height == undefined || dataObj.height === "" || parseInt(dataObj.height) <= 0) {
                            if (type == "row") {
                                $scope.secondArr[index].height = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22068"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_HEIGHT, dataObj.height)) {
                                if (type == "row") {
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22108"]);
                                    $scope.secondArr[index].height = true;
                                }
                                errorArr.push(0);
                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].height = false;
                                }
                                errorArr.push(1);
                            }

                        }

                        if (dataObj.grossWeight === null || dataObj.grossWeight === undefined || dataObj.grossWeight === "" || parseInt(dataObj.grossWeight) <= 0) {
                            if (type == "row") {
                                $scope.secondArr[index].grossWeight = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22070"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (dataObj.grossWeight < 0 || dataObj.grossWeight > 9999999999.99) {
                                if (type == "row") {
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22109"]);
                                    $scope.secondArr[index].grossWeight = true;
                                }
                                errorArr.push(0);
                            } else if (!CommonValidationService.checkRegExp(appConstant.REG_EXP_DIMENSION_GROSS_WEIGHT, dataObj.grossWeight)) {
                                if (type == "row") {
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22109"]);
                                    $scope.secondArr[index].grossWeight = true;
                                }
                                errorArr.push(0);
                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].grossWeight = false;
                                }
                                errorArr.push(1);
                            }
                        }

                        if (errorArr.indexOf(0) < 0) {
                            return true
                        } else {
                            return false;
                        }

                    };




                    $(document).on('keydown', function(e) {
                        if (e.keyCode == 27) {
                            if (myOtherModal) {
                                myOtherModal.$promise.then(myOtherModal.hide);
                            }
                        }
                    })

                    $scope.$on('modal.hide', function() {
                        if ($scope.secondArr[errorOnRowIndex].noOfPieceFocus) {
                            $scope.secondArr[errorOnRowIndex].noOfPieceFocus = false;
                        }
                        $timeout(function() {
                            $scope.secondArr[errorOnRowIndex].noOfPieceFocus = true;
                            errorOnRowIndex = null;
                        }, 10)
                    });



                    $scope.removeObj = function(obj, index) {
                        $scope.tableData.splice(index, 1);
                        $scope.secondArr.splice(index, 1);
                        $scope.indexFocus = null;
                        $scope.changeCheckSum();
                        if ($scope.calcFn) {
                            $scope.calcFn({ param: { grossWeight: $scope.dimensionGrossWeight, volumeWeight: $scope.dimensionVolWeight, totalPieces: $scope.totalNoOfPieces, dimensionGrossWeightInKg: $scope.dimensionGrossWeightInKg, removeFlag: false } });
                        }
                        /*$scope.dimensionState({param:true});*/
                        //console.log("dirtArr",dirtArr)
                        //var dirtIndex = dirtArr.indexOf(index);
                        //dirtArr.splice(dirtIndex,1);
                    };

                    $scope.changeCheckSum = function() {
                        $scope.totalNoOfPieces = 0;
                        $scope.dimensionVolWeight = 0;
                        $scope.dimensionGrossWeight = 0;
                        $scope.dimensionGrossWeightInKg = 0;
                        angular.forEach($scope.tableData, function(item) {
                            $scope.calculateSum(item);
                        });
                    };

                    $scope.calculateVolumeWeight = function(dataObj) {

                        if (dataObj.grossWeight != null) {
                            if ($scope.unitType) {
                                dataObj.grossWeightKg = Math.round((dataObj.grossWeight * 0.453592) * 100) / 100;
                            } else {
                                dataObj.grossWeightKg = dataObj.grossWeight;
                            }
                        }

                        if (dataObj.noOfPiece > 0 && dataObj.length > 0 &&
                            dataObj.width > 0 && dataObj.height > 0) {
                            dataObj.volWeight = (dataObj.noOfPiece * (dataObj.length * dataObj.width * dataObj.height)) / $scope.unitValue;
                            dataObj.volWeight = Math.round(dataObj.volWeight * 100) / 100;
                            $scope.changeCheckSum();
                            if ($scope.calcFn) {
                                $scope.calcFn({ param: { grossWeight: $scope.dimensionGrossWeight, volumeWeight: $scope.dimensionVolWeight, totalPieces: $scope.totalNoOfPieces, dimensionGrossWeightInKg: $scope.dimensionGrossWeightInKg, removeFlag: false } });
                            }
                        } else {
                            $scope.changeCheckSum();
                            //dataObj.volWeight="";
                        }

                    };

                    function initTable() {

                        console.log("$scope.tableData******", $scope.tableData);

                        angular.forEach($scope.tableData, function(item) {
                            $scope.secondArr.push({ "errRow": false });
                        });


                        if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData.length != 0) {


                            if ($scope.tableData.length == 1) {
                                if (!isEmptyRow($scope.tableData[0])) {
                                    $scope.changeCheckSum();
                                }
                            } else {
                                $scope.changeCheckSum();
                            }
                        }
                    }

                    $timeout(function() {
                        initTable();
                    }, 1000);


                    $scope.copyObj = function(copyObj) {
                        var tmpObj = angular.copy(copyObj);
                        tmpObj.id = null;
                        tmpObj.systemTrack = null;
                        tmpObj.versionLock = 0;

                        if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                            $scope.tableData = [];
                        } else {
                            var tmpIndex = $scope.tableData.length - 1;
                            if (isEmptyRow($scope.tableData[tmpIndex])) {
                                $scope.tableData.splice(tmpIndex, 1);
                                delete $scope.tableData[tmpIndex]

                                if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                                    $scope.tableData = [];
                                }
                            }
                        }
                        $scope.tableData.push(tmpObj);
                        $scope.changeCheckSum();
                    }

                    $scope.copy = $rootScope.copy;


                    $scope.delete = $rootScope.delete;
                    $scope.errorList = $rootScope.errorList;

                }, 2);

            }
        }

    }
]);