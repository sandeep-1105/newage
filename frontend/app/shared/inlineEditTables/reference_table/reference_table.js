
app.directive('referenceTable',['$timeout', '$rootScope', 'ValidateUtil', 'ReferenceTypeSearchByKeyword',
    function($timeout, $rootScope, ValidateUtil, ReferenceTypeSearchByKeyword){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/reference_table/reference_table.html',
        scope:{
        	indexKey:"=",
            tableData:"=",
            showErrors:"=",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"="

        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
            	$scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                $scope.tableisChecked = true;
                $scope.$watch('tableData', function (newValue, oldValue) {
                    if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                        initTable();
                    }
                });

                $scope.firstFocus=true;
                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.tableCheck = function () {
                    $scope.tableisChecked = false;
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;
                        } else {
                            validResult = validateObj(dataObj, index, "row");
                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };

                //temp array
                $scope.secondArr = [];

                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row
                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.flag = true;
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData.push({});
                        $rootScope.navigateToNextField($scope.indexKey + 'referenceTypeMaster' + ($scope.tableData.length - 1))
                        $scope.secondArr.push({"errRow": false});

                    }
                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;

                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                $scope.ajaxReferenceTypeEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    $scope.searchDto.keyword = object == null ? "" : object;

                    return ReferenceTypeSearchByKeyword.fetch($scope.searchDto).$promise.then(function (data) {
                        if (data.responseCode == "ERR0") {
                            $scope.referenceTypeList = data.responseObject.searchResult;
                            return $scope.referenceTypeList;
                        }
                    }, function (errResponse) {
                        console.error('Error while fetching Reference type list', errResponse);
                    });
                }


                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    $scope.secondArr[index] = {};

                    $scope.firstFocus=false;
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.referenceTypeMaster == null || dataObj.referenceTypeMaster == undefined || dataObj.referenceTypeMaster.id == null) {
                        if (type == "row") {
                            $scope.secondArr[index].referenceTypeMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90519"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }

                    else if (dataObj.referenceTypeMaster.status == 'Block') {
                        if (type == "row") {
                            console.log("Selected referenceTypeMaster is Block.");
                            $scope.secondArr[index].referenceTypeMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90520"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }

                    else if (dataObj.referenceTypeMaster.status == 'Hide') {
                        console.log("Selected referenceTypeMaster is Hidden.");
                        if (type == "row") {
                            $scope.secondArr[index].referenceTypeMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90521"]);
                        }
                        //return false;
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].referenceTypeMaster = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }


                    if (dataObj.referenceNumber == undefined
                        || dataObj.referenceNumber == null
                        || dataObj.referenceNumber == "") {

                        if (type == "row") {
                            $scope.secondArr[index].referenceNumber = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90522"]);
                        }
                        errorArr.push(0);
                    }
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].referenceNumber = false;
                        }
                        errorArr.push(1);
                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                };


                $scope.referenceTypeRender = function (item) {
                    return {
                        label: item.referenceTypeName,
                        item: item
                    }
                };

                $scope.navigateToNextField = function (id) {
                    if (id != undefined && id != null && id != "")
                        document.getElementById(id).focus();
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;

            },2);
        }
    }
}]);