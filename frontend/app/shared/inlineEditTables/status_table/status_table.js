/**
 * Created by saravanan on 23/6/16.
 */
app.directive('statusTable',['$timeout','$compile','$rootScope','ValidateUtil','TriggerTypeSearchKeyword',
            'TriggerSearchKeyword','EmployeeList',
    function($timeout,$compile,$rootScope,ValidateUtil,TriggerTypeSearchKeyword,
            TriggerSearchKeyword,EmployeeList){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/status_table/status_table.html',
        scope:{
        	indexKey:"=",
            tableData:"=",
            tableState:"&",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"=",
            dependData:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
        	
        	console.log("table data", $scope.tableData, $scope.dependData);
        	$scope.currentServiceUid = $scope.dependData;
        	
        	$scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";
            $scope.directiveDateTimePickerOpts = {
                    format:$rootScope.userProfile.selectedUserLocation.dateTimeFormat,
                    useCurrent:false,
                    sideBySide:true,
                    viewDate:moment()
                };
            $scope.directiveDateTimePickerOpts.widgetParent = "body";
            
            $timeout(function() {
                $scope.tableisChecked = true;
                $scope.$watch('tableData', function (newValue, oldValue) {
                    if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                        initTable();
                    }
                });
                $scope.tableCheck = function () {
                    $scope.tableisChecked = false;
                    console.log("status table table check called");
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });

                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    //if (!obj) {
                    //    return isempty;
                    //}
                    console.log("called empty row", $scope.emptyObjRef);
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                
                $scope.toggleArr = [{id: true, title: 'Yes'}, {id: false, title: 'No'}];
                $scope.flag = false;

                //show errors


                //indexfocus initialization
                $scope.indexFocus = null;
                //temp array
                $scope.secondArr = [];

                $scope.notePopup = $modal({scope: $scope, templateUrl: 'statusNote.html', backdrop : 'static', show: false, keyboard: true});
                $scope.notespopup = function (index,id) {
                    $scope.noteIndex = index;
                    $scope.notePopup.$promise.then($scope.notePopup.show);
                    $scope.popUpNote = $scope.tableData[$scope.noteIndex].note ? $scope.tableData[$scope.noteIndex].note : "";
                    $rootScope.navigateToNextField(id);
                };
                
                $scope.save = function (note) {
                    if ($scope.tableData != undefined && $scope.tableData != null) {
                        $scope.tableData[$scope.noteIndex].note = note;
                    }
                    $scope.notePopup.$promise.then($scope.notePopup.hide);
                }
                
                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };
                function initTable() {
                    $scope.emptyObjRef = {};
                    if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                        $scope.tableData = [];
                        $scope.statusObj = {};
                        $scope.statusObj.completed = false;
                        $scope.statusObj.followUpRequired = false;
                        // $scope.statusObj.followUpRequired = true;
                        $scope.statusObj.protect = false;
                        $scope.tableData.push($scope.statusObj);

                    }
                    $scope.emptyObjRef = angular.copy($scope.tableData[$scope.tableData.length - 1]);
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row

                $scope.addRow = function () {
                    $scope.emptyObjRef = {};
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.flag = true;
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });

                    if ($scope.flag) {
                        if (fineArr.indexOf(0) < 0) {
                            if ($scope.tableData == undefined || $scope.tableData == null)
                                $scope.tableData = [];
                            $scope.statusObj = {};
                            $scope.statusObj.completed = false;
                            $scope.statusObj.followUpRequired = false;
                            // $scope.statusObj.followUpRequired = true;
                            $scope.statusObj.protect = false;

                            $scope.tableData.push($scope.statusObj);
                            $rootScope.navigateToNextField($scope.indexKey + 'connectionMove' + ($scope.tableData.length - 1));
                            $scope.emptyObjRef = angular.copy($scope.statusObj);
                            $scope.secondArr.push({"errRow": false});
                            $scope.indexFocus = null;
                            $scope.secondArr[$scope.tableData.length - 1].date = true;
                            if ($scope.tableState) {
                                $scope.tableState({param: true});
                            }
                        } else {
                            if ($scope.tableState) {
                                $scope.tableState({param: false});
                            }
                        }
                    } else {
                        if ($scope.tableData == undefined || $scope.tableData == null)
                            $scope.tableData = [];
                        $scope.statusObj = {};
                        $scope.statusObj.completed = false;
                        $scope.statusObj.followUpRequired = false;
                        //  $scope.statusObj.followUpRequired = true;
                        $scope.statusObj.protect = false;

                        $scope.tableData.push($scope.statusObj);
                        $scope.emptyObjRef = angular.copy($scope.statusObj);
                        $scope.tableState({param: false});
                    }
                };

                //call Validate
                $scope.callValidate = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {

                        var validResult = validateObj(dataObj, index, "row");

                        if (validResult) {


                            $scope.secondArr[index].errRow = false;
                            $scope.tableState({param: true});
                        } else {
                            $scope.secondArr[index].errRow = true;
                            $scope.tableState({param: false});

                        }

                    });


                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxTriggerTypeMasterEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return TriggerTypeSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.triggerTypeMasterList = data.responseObject.searchResult;
                                //$scope.showTriggerTypeMasterList=true;
                                return $scope.triggerTypeMasterList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );

                };
                $scope.selectTriggerType = function (dataOb) {
                    $scope.triggerTypeMaster = dataOb.triggerTypeMaster;

                }

                $scope.selectTriggerMaster = function (triggerMasterObj) {


                }
                $scope.ajaxTriggerMasterEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    $scope.triggerMasterList = [];
                    if ($scope.triggerTypeMaster.triggerTypeName != undefined && $scope.triggerTypeMaster.triggerTypeName != null && $scope.triggerTypeMaster.triggerTypeName != "") {
                        return TriggerSearchKeyword.fetch({"triggerType": $scope.triggerTypeMaster.triggerTypeName}, $scope.searchDto).$promise.then(
                            function (data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.triggerMasterList = data.responseObject.searchResult;
                                    return $scope.triggerMasterList;
                                }
                            },
                            function (errResponse) {
                                console.error('Error while fetching Customer Services');
                            }
                        );

                    }
                }
                $scope.ajaxAssignedToEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return EmployeeList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.assignedToList = data.responseObject.searchResult;
                                return $scope.assignedToList;
                                //$scope.showAssignedToList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );

                }

                //error validation rules
                var validateObj = function (dataObj, index, type) {

                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index] = {};
                        $scope.secondArr[index].errTextArr = [];
                    }


                    if (dataObj.date == undefined
                        || dataObj.date == null
                        || dataObj.date == "") {


                        if (type == "row") {
                            $scope.secondArr[index].date = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90413"]);
                        }
                        errorArr.push(0);


                    }
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].date = false;

                        }
                        errorArr.push(1);
                    }


                    if (dataObj.triggerTypeMaster == undefined
                        || dataObj.triggerTypeMaster == null
                        || dataObj.triggerTypeMaster == ""
                        || dataObj.triggerTypeMaster.id == null) {


                        if (type == "row") {
                            $scope.secondArr[index].triggerTypeMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90400"]);
                        }
                        errorArr.push(0);

                        //  console.log($rootScope.nls["ERR90400"]);
                        //  $scope.errorMap.put("triggerType",$rootScope.nls["ERR90400"]);
                        // return false;

                    } else if (dataObj.triggerTypeMaster.status != null) {
                        if (ValidateUtil.isStatusBlocked(dataObj.triggerTypeMaster.status)) {


                            if (type == "row") {
                                $scope.secondArr[index].triggerTypeMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90401"]);
                            }

                            errorArr.push(0);
                            //  console.log($rootScope.nls["ERR90401"]);
                            //  $scope.errorMap.put("triggerType",$rootScope.nls["ERR90401"]);
                            //  $scope.dataObj.triggerTypeMaster = null;
                            // $scope.dataObj.triggerMaster=null;
                            // return false

                        }

                        else if (ValidateUtil.isStatusHidden(dataObj.triggerTypeMaster.status)) {


                            if (type == "row") {
                                $scope.secondArr[index].triggerTypeMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90402"]);
                            }
                            errorArr.push(0);
                            //  console.log($rootScope.nls["ERR90402"]);
                            //  $scope.errorMap.put("triggerType",$rootScope.nls["ERR90402"]);
                            //$scope.dataObj.triggerTypeMaster = null;
                            // $scope.dataObj.triggerMaster=null;
                            // return false

                        }
                        else {

                            if (type == "row") {
                                $scope.secondArr[index].triggerTypeMaster = false;

                            }
                            errorArr.push(1);
                        }

                    }


                    if (dataObj.triggerMaster == undefined
                        || dataObj.triggerMaster == null
                        || dataObj.triggerMaster == ""
                        || dataObj.triggerMaster.id == null) {

                        if (type == "row") {
                            $scope.secondArr[index].triggerMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90410"]);
                        }
                        errorArr.push(0);
                        //console.log($rootScope.nls["ERR90410"]);
                        // $scope.errorMap.put("trigger",$rootScope.nls["ERR90410"]);
                        //  return false;

                    } else if (dataObj.triggerMaster.status != null) {
                        if (ValidateUtil.isStatusBlocked(dataObj.triggerMaster.status)) {

                            if (type == "row") {
                                $scope.secondArr[index].triggerMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90411"]);
                            }
                            errorArr.push(0);
                            //console.log($rootScope.nls["ERR90411"]);
                            //$scope.errorMap.put("trigger",$rootScope.nls["ERR90411"]);
                            //$scope.dataObj.triggerMaster = null;
                            //$scope.dataObj.note=null;
                            //return false

                        }

                        else if (ValidateUtil.isStatusHidden(dataObj.triggerMaster.status)) {

                            if (type == "row") {
                                $scope.secondArr[index].triggerMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90412"]);
                            }
                            errorArr.push(0);
                            //console.log($rootScope.nls["ERR90412"]);
                            //$scope.errorMap.put("trigger",$rootScope.nls["ERR90412"]);
                            //$scope.dataObj.triggerMaster = null;
                            //$scope.dataObj.note=null;
                            //return false

                        }
                        else {

                            if (type == "row") {
                                $scope.secondArr[index].triggerMaster = false;

                            }
                            errorArr.push(1);
                        }
                    }

                    if (dataObj.followUpRequired == 'No' || dataObj.followUpRequired == false) {
                        if (type == "row") {
                            $scope.secondArr[index].followUpDate = false;
                            $scope.secondArr[index].employeeMaster = false;
                            dataObj.employeeMaster = null;
                            dataObj.followUpDate = null;
                        }
                        errorArr.push(1);
                    }


                    // code commented ,As discussed with testing team assign To is not require for status update.

                    /*if(dataObj.followUpRequired=='Yes' || dataObj.followUpRequired==true)
                     {

                     if (dataObj.employeeMaster == undefined|| dataObj.employeeMaster == null|| dataObj.employeeMaster == ""|| dataObj.employeeMaster.id == null )
                     {

                     if(type=="row") {
                     $scope.secondArr[index].employeeMaster = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90420"]);
                     }
                     errorArr.push(0);
                     // console.log($rootScope.nls["ERR90410"]);
                     //$scope.errorMap.put("triggeremployeeName",$rootScope.nls["ERR90420"]);
                     //return false;
                     }
                     else if(dataObj.employeeMaster.status!=null)
                     {
                     if(ValidateUtil.isEmployeeResigned(dataObj.employeeMaster.employementStatus))
                     {


                     if(type=="row") {
                     $scope.secondArr[index].employeeMaster = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90421"]);
                     }
                     errorArr.push(0);
                     //console.log($rootScope.nls["ERR90411"]);
                     // $scope.errorMap.put("triggeremployeeName",$rootScope.nls["ERR90421"]);
                     //$scope.dataObj.triggerMaster = null;
                     //return false
                     }
                     else  if(ValidateUtil.isEmployeeTerminated(dataObj.employeeMaster.employementStatus))
                     {

                     if(type=="row") {
                     $scope.secondArr[index].employeeMaster = true;
                     $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90422"]);
                     }
                     errorArr.push(0);
                     //console.log($rootScope.nls["ERR90412"]);
                     //$scope.errorMap.put("triggeremployeeName",$rootScope.nls["ERR90422"]);
                     //$scope.dataObj.triggerMaster = null;
                     //return false
                     }

                     else{

                     if(type=="row") {
                     $scope.secondArr[index].employeeMaster = false;

                     }
                     errorArr.push(1);

                     }

                     }


                     }*/


                    if (dataObj.followUpRequired == 'Yes' || dataObj.followUpRequired == true) {

                        if (dataObj.followUpDate == undefined
                            || dataObj.followUpDate == null
                            || dataObj.followUpDate == ""
                        ) {
                            if (type == "row") {
                                $scope.secondArr[index].followUpDate = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90430"]);
                            }
                            errorArr.push(0);
                            //console.log($rootScope.nls["ERR90410"]);
                            //$scope.errorMap.put("followUpDate",$rootScope.nls["ERR90430"]);
                            //return false;

                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].followUpDate = false;

                            }
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.note == undefined || dataObj.note == "" || dataObj.note == null) {

                        if (type == "row") {
                            $scope.secondArr[index].note = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90440"]);
                        }
                        errorArr.push(0);
                        //$scope.errorMap.put("statusUpdate.notes", $rootScope.nls["ERR90440"]);
                        //return false;
                    }

                    else if (dataObj.note != undefined && dataObj.note != "" && dataObj.note != null) {
                    }
                    else {

                        if (type == "row") {
                            $scope.secondArr[index].note = false;

                        }
                        errorArr.push(1);
                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });
                
                
                $scope.copyObj=function(copyObj){
                	var tmpObj = angular.copy(copyObj);
                	tmpObj.id=null;
                	tmpObj.systemTrack=null;
                	tmpObj.versionLock=0;
                	
                	if($scope.tableData==undefined || $scope.tableData==null || $scope.tableData.length==0){
                		$scope.tableData = [];
                	}else{
                		var tmpIndex = $scope.tableData.length-1;
                		if(isEmptyRow($scope.tableData[tmpIndex])){
                			$scope.tableData.splice(tmpIndex,1); 
                            delete  $scope.tableData[tmpIndex]
                            
                            if($scope.tableData==undefined || $scope.tableData==null || $scope.tableData.length==0){
                        		$scope.tableData = [];
                        	}
                		}
                	}
                	$scope.tableData.push(tmpObj);
                }
                
                $scope.copy = $rootScope.copy;
                $scope.sentTrigger = $rootScope.sentTrigger;
                $scope.receivedTrigger = $rootScope.receivedTrigger;


                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;
                };


                $scope.triggerTypeRender = function (item) {
                    return {
                        label: item.triggerTypeName,
                        item: item
                    }
                }
                $scope.triggerRender = function (item) {
                    return {
                        label: item.triggerName,
                        item: item
                    }
                }
                $scope.employeeRender = function (item) {
                    return {
                        label: item.employeeName,
                        item: item
                    }
                }
                $scope.statusSelectFocus = function (id) {
                    console.log("called." + id);
                    $rootScope.navigateToNextField(id);
                }
                $scope.goToNext = function (id) {
                    console.log("called." + id);
                    $rootScope.navigateToNextField(id);
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;
            },0);
        }
    }
}]);