/**
 * Created by saravanan on 23/6/16.
 */
app.directive('partyAccountTable',['$timeout','$compile','$rootScope','ValidateUtil','PartyAccountListByNotInId','CountryWiseLocation',
        'AccountList','CurrencyMasterSearchKeyword','GLAccountOnlySubledgerYes','LedgerList',
    function($timeout,$compile,$rootScope,ValidateUtil,PartyAccountListByNotInId,CountryWiseLocation,
        AccountList,CurrencyMasterSearchKeyword,GLAccountOnlySubledgerYes,LedgerList){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/party_account/party_account.html',
        scope:{
        	dependData:"=",
            tableData:"=",
            showErrors:"=",
            tableState:"&",
            tableCheck:"=",
            calcFn:"&",
            fnRole:"&",
            addRole:"=",
            deleteRole:"=",
            editRole:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                var dirtArr = [];
                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.termCodeArr = $rootScope.enum['TermCode'];
                $scope.duplicateExists = false;
                $scope.lovStatusArr = $rootScope.enum['LovStatus']
                $scope.taxArr = [{value: true, title: 'Yes'}, {value: false, title: 'No'}];

                $scope.scrollConfig = $rootScope.ngScrollConfigScroll = {
                    "cursorcolor": '#3F8DEB',
                    "cursorwidth": '8px',
                    "smoothscroll": true,
                    "autohidemode": false
                };
                $scope.groupCol = [{
                    name: 'Account Name',
                    width: '150px',
                    valueField: 'accountMaster.accountName'
                }, {
                    name: 'Party Name',
                    width: '120px',
                    valueField: 'tmpPartyMaster.partyName'
                }];

                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                function isEmptyRow(obj) {

                    var isempty = true;

                    console.log("empty obj*******", obj);

                    $scope.emptyObjRef = $scope.getAccountDefaultData();
                    console.log("$scope.emptyObjRef*****", $scope.emptyObjRef);
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {

                        //check the new key is empty or not
                        if (obj[k[i]]) {
                            // check the default object contains the key
                            if (!$scope.emptyObjRef.hasOwnProperty(k[i])) {
                                isempty = false;
                                break;
                            }
                            //check the type is object or not
                            if (angular.isObject(obj[k[i]])) {
                                if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]].id != null && obj[k[i]].id != $scope.emptyObjRef[k[i]].id) {
                                    isempty = false;
                                    break;
                                }
                            }
                            else {
                                if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                                    isempty = false;
                                    break;

                                }
                            }
                        }
                    }
                    console.log("empty check *****", isempty)
                    return isempty;
                }


                $scope.indexFocus = null;

                $scope.secondArr = [];


                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                $scope.getAccountDefaultData = function () {
                    accountObj = {};
                    accountObj.status = 'Active';
                    accountObj.isTaxExempted = false;
                    return accountObj;
                }


                $scope.addRow = function () {
                    $scope.emptyObjRef = {};
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableData == undefined || $scope.tableData == null)
                            $scope.tableData = [];
                        $scope.accountObj = {};
                        $scope.accountObj.status = 'Active';
                        $scope.accountObj.isTaxExempted = false;
                        $scope.tableData.push($scope.accountObj);
                        $scope.emptyObjRef = angular.copy($scope.accountObj);
                        $scope.tableState({param: false});
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].locationNameFocus = true;
                        }, 10);
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].locationNameFocus = true;
                        }, 10);
                    }
                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxAccLocationEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CountryWiseLocation.fetch({"countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.accountlocationList = data.responseObject.searchResult;
                                return $scope.accountlocationList;
                                //$scope.showaccountLocationList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Location');
                        }
                    );

                }
                //$scope.ajaxAccLocationEvent(null);

                $scope.ajaxAccountEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return GLAccountOnlySubledgerYes.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                            	
                                $scope.accountList = data.responseObject.searchResult;
                                return $scope.accountList;
                                //$scope.showAccountList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Accounts');
                        }
                    );


                }

                //$scope.ajaxAccountEvent(null);

                $scope.ajaxAccCurrencyEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.currencyList = data.responseObject.searchResult;
                                return $scope.currencyList;
                                //$scope.showCurrencyList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Currency');
                        }
                    );


                }

                //$scope.ajaxAccCurrencyEvent(null);

                $scope.ajaxLedgerAccNameEvent = function (object, partyMaster, partyAccount) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PartyAccountListByNotInId.fetch({
                        "partyId": partyMaster.id == undefined
                        || partyMaster.id == null ? -1 : partyMaster.id
                    }, $scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.ledgerNameList = data.responseObject.searchResult;
                                if ($scope.searchDto != undefined && $scope.searchDto.keyword == "") {
                                    if (partyAccount.billingSubLedgerCode == undefined)
                                        partyAccount.billingSubLedgerCode = {};
                                    partyAccount.billingSubLedgerCode.partyCode = null;
                                }
                                return $scope.ledgerNameList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Party account');
                        }
                    );


                }


                $scope.ajaxLedgerCodeEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return LedgerList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.ledgerCodeList = data.responseObject.searchResult;
                                return $scope.ledgerCodeList
                                //$scope.showLedgerCodeList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Ledgers');
                        }
                    );

                }

                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.locationMaster == null || dataObj.locationMaster.id == undefined || dataObj.locationMaster.id == null || dataObj.locationMaster.id == "") {
                        console.log("Location is Mandatory..........")
                        if (type == "row") {
                            $scope.secondArr[index].locationName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6015"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }

                    else {

                        if (dataObj.locationMaster.status == 'Block') {
                            console.log("Selected Location is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].locationName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6028"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.locationMaster.status == 'Hide') {
                            console.log("Selected Location is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].locationName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6029"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].locationName = false;
                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }


                    if (dataObj.accountMaster == null || dataObj.accountMaster.id == undefined || dataObj.accountMaster.id == null || dataObj.accountMaster.id == "") {
                        console.log("Account is Mandatory..........")
                        if (type == "row") {
                            $scope.secondArr[index].accountName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6016"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }

                    else {
                        if (dataObj.accountMaster.status == 'Block') {
                            console.log("Selected Account is Blocked.");
                            if (type == "row") {
                                $scope.secondArr[index].accountName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR32"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }

                        else if (dataObj.accountMaster.status == 'Hide') {
                            console.log("Selected Account is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].accountName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR33"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].accountName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                    }
                    if (dataObj.currencyMaster == null || dataObj.currencyMaster.id == undefined || dataObj.currencyMaster.id == null || dataObj.currencyMaster.id == "") {
                        if (type == "row") {
                            $scope.secondArr[index].currencyCode = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6014"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }
                    else {

                        if (dataObj.currencyMaster.status == 'Block') {
                            if (type == "row") {
                                $scope.secondArr[index].currencyCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR34"]);
                            }
                            //return false;
                            errorArr.push(0);

                        }
                        else if (dataObj.currencyMaster.status == 'Hide') {
                            console.log("Selected Currency is Hidden.");
                            if (type == "row") {
                                $scope.secondArr[index].currencyCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR35"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].currencyCode = false;
                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }

                    if (dataObj.billingAccountName != undefined && dataObj.billingAccountName != null && dataObj.billingAccountName != "" && dataObj.billingAccountName.id!=undefined) {

                        if (dataObj.billingSubLedgerCode == undefined || dataObj.billingSubLedgerCode == null || dataObj.billingSubLedgerCode == "") {
                            if (type == "row") {
                                $scope.secondArr[index].subLedgerName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6018"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].subLedgerName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                        if (dataObj.billingSubLedgerCode != undefined && dataObj.billingSubLedgerCode != null && dataObj.billingSubLedgerCode != "") {
                            if (dataObj.billingSubLedgerCode.status == 'Block') {
                                console.log("Selected billingSubLedgerCode is Blocked.");
                                if (type == "row") {
                                    $scope.secondArr[index].billingSubLedgerCode = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6031"]);
                                }
                                //return false;
                                errorArr.push(0);

                            }

                            else if (dataObj.billingSubLedgerCode.status == 'Hide') {
                                console.log("Selected billingSubLedgerCode is Hidden.");
                                if (type == "row") {
                                    $scope.secondArr[index].billingSubLedgerCode = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6030"]);
                                }
                                //return false;
                                errorArr.push(0);

                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].billingSubLedgerCode = false;

                                }
                                //return false;
                                errorArr.push(1);
                            }
                        }
                    }else{
                    	dataObj.billingSubLedgerCode=null;
                    }
                    if (dataObj.billingSubLedgerCode != undefined && dataObj.billingSubLedgerCode != null && dataObj.billingSubLedgerCode != ""  && dataObj.billingSubLedgerCode.id!=undefined) {
                        if (dataObj.billingAccountName == undefined || dataObj.billingAccountName == null || dataObj.billingAccountName == "") {
                            if (type == "row") {
                                $scope.secondArr[index].ledgerName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6019"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].ledgerName = true;
                            }
                            //return false;
                            errorArr.push(1);
                        }

                        if (dataObj.billingAccountName != undefined && dataObj.billingAccountName != null && dataObj.billingAccountName != "") {
                            if (dataObj.billingAccountName.status == 'Block') {
                                console.log("Selected billingAccountName is Blocked.");
                                if (type == "row") {
                                    $scope.secondArr[index].billingAccountName = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6032"]);
                                }
                                //return false;
                                errorArr.push(0);

                            }

                            else if (dataObj.billingAccountName.status == 'Hide') {
                                console.log("Selected billingAccountName is Hidden.");
                                if (type == "row") {
                                    $scope.secondArr[index].billingAccountName = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6033"]);
                                }
                                //return false;
                                errorArr.push(0);

                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].billingAccountName = false;

                                }
                                //return false;
                                errorArr.push(1);
                            }
                        }

                    }

                    if (dataObj.status == undefined || dataObj.status == null || dataObj.status == "") {
                        if (type == "row") {
                            $scope.secondArr[index].status = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6081"]);
                        }
                        //return false;
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].status = true;
                        }
                        //return false;
                        errorArr.push(1);
                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].locationNameFocus) {
                        $scope.secondArr[errorOnRowIndex].locationNameFocus = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].locationNameFocus = true;
                        errorOnRowIndex = null;
                    }, 10)
                });


                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.tableState({param: true});
                    $scope.indexFocus = null;
                };

                $scope.openEvent = function (param) {
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                }

                $scope.locationRender = function (item) {
                    return {
                        label: item.locationName,
                        item: item
                    }
                }
                $scope.accountRender = function (item) {
                    return {
                        label: item.accountName,
                        item: item
                    }
                }
                $scope.currencyRender = function (item) {
                    return {
                        label: item.currencyCode,
                        item: item
                    }
                }
                $scope.partyAccountRender = function (item) {
                    return {
                        label: item.accountMaster.accountName,
                        item: item
                    }
                }
                $scope.subLedgerRender = function (item) {
                    return {
                        label: item.ledgerName,
                        item: item
                    }
                }
                $scope.setStatus = function (obj, nextFieldId) {

                    if (obj != undefined && obj != null)
                        obj.status = 'Active';

                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

                }
                $scope.selectAccount = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

                }

                $scope.selectTermCode = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

                }
                $scope.selectCurrencyCode = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

                }
                $scope.selectBillAccntName = function (nextFieldId, index, partyAccount) {

                    partyAccount.billingSubLedgerCode = partyAccount.billingAccountName ? partyAccount.billingAccountName.tmpPartyMaster : "";
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

                }
                $scope.selectLedgerCode = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

                }
                $scope.selectTaxExem = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }

            }
           $scope.selectAccount =function(nextFieldId){
        	   if(nextFieldId!=undefined){
           		$rootScope.navigateToNextField(nextFieldId);
           	}  
        	   
           }
           
           $scope.selectTermCode =function(nextFieldId){
        	   if(nextFieldId!=undefined){
           		$rootScope.navigateToNextField(nextFieldId);
           	}  
        	   
           }
           $scope.selectCurrencyCode =function(nextFieldId){
        	   if(nextFieldId!=undefined){
           		$rootScope.navigateToNextField(nextFieldId);
           	}  
        	   
           } 
           
           $scope.changeBillingSubLedgerCode=function(partyAccount)
           {
        	   if(partyAccount.billingAccountName==null || partyAccount.billingAccountName==undefined ||
        		   partyAccount.billingAccountName=="")
        		   {
        		   partyAccount.billingSubLedgerCode=null;
        		   }
           }
           $scope.selectBillAccntName =function(nextFieldId, index, partyAccount){
        	   
        	   partyAccount.billingSubLedgerCode = partyAccount.billingAccountName ? partyAccount.billingAccountName.tmpPartyMaster:"";
        	   if(nextFieldId!=undefined){
           		$rootScope.navigateToNextField(nextFieldId);
           	}  
        	   
           }
           $scope.selectLedgerCode =function(nextFieldId){
        	   if(nextFieldId!=undefined){
           		$rootScope.navigateToNextField(nextFieldId);
           	}  
        	   
           } 
           $scope.selectTaxExem =function(nextFieldId){
        	   if(nextFieldId!=undefined){
           		$rootScope.navigateToNextField(nextFieldId);
           	}  
        	   
           };
            $scope.delete = $rootScope.delete;
            $scope.errorList = $rootScope.errorList;

            },2);
        }
    }
}]);