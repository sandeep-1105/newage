/**
 * Created by saravanan on 23/6/16.
 */
app.directive('mailGroupTable',['$timeout','$compile','$rootScope','appConstant','CommonValidationService',
    function($timeout,$compile,$rootScope,appConstant,CommonValidationService){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/automail_group/add_group.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            tableCheck:"=?",
            calcFn:"&"
        },
        controller : function($scope,$rootScope,$popover,$modal){
         $timeout(function() {
            //show errors
            $scope.statusArr = $rootScope.enum['Status'];
            $scope.messTypeArr = [{"title": "Internal", "value": "Internal"}, {
                "title": "External",
                "value": "External"
            }];
            $scope.eventArr = $rootScope.enum['MailEventType'];
            $scope.indexFocus = null;
            $scope.secondArr = [];
            function initTable() {
            	if($scope.tableData==undefined || $scope.tableData==null){
            		$scope.tableData=[];
            		$scope.emptyObjRef = {};
                    $scope.dataObj={};
                    $scope.dataObj.autoMailStatus = 'Enable';
                    $scope.tableData.push($scope.dataObj);
                    $scope.emptyObjRef = angular.copy($scope.dataObj);
                    $scope.secondArr.push({"errRow": false});
            	}else{
            		$scope.emptyObjRef = {};
            		  angular.forEach($scope.tableData, function (item) {
            			  if(item.id==undefined || item.id==null){
            				  $scope.secondArr.push({"errRow": false});
                              $scope.dataObj={};
                              $scope.dataObj.autoMailStatus = 'Enable';
                              $scope.tableData.push($scope.dataObj);
                              $scope.emptyObjRef = angular.copy($scope.dataObj);  
            			  }else{
            				  $scope.emptyObjRef = angular.copy(item.autoMailStatus);
            			  }
                      });
            	}
            }
            initTable();
            try {
                if ($scope.tableData.length < 0) {
                    $scope.tableData = [];
                    $scope.tableData.push({});
                    //dirtArr.push(0);
                    initTable();
                }
            } catch (e) {
                $scope.tableData = [];
                initTable();
            }
            //add row
            $scope.addRow = function () {
                var fineArr = [];
                if($scope.tableData==undefined || $scope.tableData==null){
                	$scope.tableData=[];
                	 $scope.dataObj={};
                     $scope.dataObj.autoMailStatus = 'Enable';
                     $scope.tableData.push($scope.dataObj);
                     $scope.emptyObjRef = angular.copy($scope.dataObj);
                }else{
                	angular.forEach($scope.tableData, function (dataObj, index) {
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                		$scope.emptyObjRef = {};
                        $scope.dataObj={};
                        $scope.dataObj.autoMailStatus = 'Enable';
                        $scope.tableData.push($scope.dataObj);
                        $scope.emptyObjRef = angular.copy($scope.dataObj);
                    } else {
                    	return false;
                    }
                }
                
            };

            
 $scope.tableCheck = function(){
	 console.log("automail table check");
            	var fineArr = [];
                $scope.secondArr=[];
                var validResult;
                var finalData = [];
                angular.forEach($scope.tableData,function(dataObj,index){
                    $scope.secondArr[index]={};
                    if (isEmptyRow(dataObj)){
                        validResult = true;
                        $scope.tableData.splice(index,1); 
                        delete  $scope.tableData[index]
                    }else{
                        validResult =validateObj(dataObj,index,"row");
                        finalData.push(dataObj);
                    }
                    if(validResult){
                        fineArr.push(1);
                        $scope.secondArr[index].errRow = false;
                    }else{
                        $scope.secondArr[index].errRow = true;
                        fineArr.push(0);
                    }
                });
                if(fineArr.indexOf(0)<0){
                    $scope.tableData = finalData;
                    return true;
                }else{
                    return false;
                }
            };
            
            function isEmptyRow(obj){
                //return (Object.getOwnPropertyNames(obj).length === 0);
                var isempty = true; //  empty
                if (!obj) {
                    return isempty;
                }
                var k = Object.getOwnPropertyNames(obj);
                for (var i = 0; i < k.length; i++){
                    if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                        isempty = false; // not empty
                        break;

                    }
                }
                return isempty;
            }
            
            
            
            var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
            var errorOnRowIndex = null;
            $scope.errorShow = function (errorObj, index) {
                $scope.errList = errorObj.errTextArr;
                // Show when some event occurs (use $promise property to ensure the template has been loaded)
                errorOnRowIndex = index;
                myOtherModal.$promise.then(myOtherModal.show);
            };


            //error validation rules
            var validateObj = function (dataObj, index, type) {
                var errorArr = [];
                $scope.secondArr[index] = {};
                $scope.secondArr[index].errTextArr = [];
                if (dataObj.messageName == undefined
                    || dataObj.messageName == null
                    || dataObj.messageName == "") {
                    if (type == "row") {
                        $scope.secondArr[index].messageName = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR10011"]);
                    }
                    //return false;
                    errorArr.push(0);

                } else {
                	
                	if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_AUTO_MAIL_NAME,dataObj.messageName)){
                		  if (type == "row") {
                              $scope.secondArr[index].messageName = true;
                              $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR10010"]);
                          }
                          //return false;
                          errorArr.push(0);
					}
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].messageName = false;
                        }
                        //return false;
                        errorArr.push(1);
                    }

                }


                if (dataObj.messageCode == undefined
                    || dataObj.messageCode == null
                    || dataObj.messageCode == "") {
                    if (type == "row") {
                        $scope.secondArr[index].messageCode = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR10021"]);
                    }
                    //return false;
                    errorArr.push(0);

                }
                else {
                	if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_AUTO_MAIL_CODE, dataObj.messageCode)){
                		 if (type == "row") {
                             $scope.secondArr[index].messageCode = true;
                             $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR10020"]);
                         }
                         //return false;
                         errorArr.push(0);
					} else {
                        if (type == "row") {
                            $scope.secondArr[index].messageCode = false;
                        }
                        //return false;
                        errorArr.push(1);
                    }

                }


                if (errorArr.indexOf(0) < 0) {
                    return true
                } else {
                    return false;
                }

            };


            function changeLastStatus() {
                angular.forEach($scope.tableData, function (dataObj, index) {
                    //var curObj = $scope.tableData.data[index];
                    var validResult = validateObj(dataObj, index, "row");
                    $scope.secondArr[index].errRow = false;
                });
            }


            $scope.dirtyIndex = function (index) {
                $scope.indexFocus = index;
            };

            $(document).on('keydown', function (e) {
                if (e.keyCode == 27) {
                    if (myOtherModal) {
                        myOtherModal.$promise.then(myOtherModal.hide);
                    }
                }
            });

            $scope.$on('modal.hide', function () {
                if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                    $scope.secondArr[errorOnRowIndex].chargeCode = false;
                }
                $timeout(function () {
                    $scope.secondArr[errorOnRowIndex].chargeCode = true;
                    errorOnRowIndex = null;
                }, 10)
            });

            $scope.removeObj = function (obj, index) {
                $scope.tableData.splice(index, 1);
                $scope.secondArr.splice(index, 1);
                $scope.indexFocus = null;
            };
             $scope.delete = $rootScope.delete;
             $scope.errorList = $rootScope.errorList;
        },2);

        }
    }
}]);