/**
 * Created by hmspl on 20/7/16.
 */

/**
 * Created by saravanan on 23/6/16.
 */
app.directive('eventTable',['$timeout','$compile','$rootScope','ValidateUtil','EmployeeList','EventSearchKeyword', 'AutoCompleteService',
    function($timeout,$compile,$rootScope,ValidateUtil,EmployeeList,EventSearchKeyword, AutoCompleteService){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/event_table/event_table.html',
        scope:{
        	indexKey:"=",
            tableData:"=",
            showErrors:"=",
            tableState:"&",
            calcFn:"&",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"=",
            eventStatus:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
        	
        	$scope.$AutoCompleteService = AutoCompleteService;
        	
        	$scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";
            
            $scope.firstFocus=true;
            
        	$timeout(function() {

        	    $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);

            	$scope.tableisChecked=true;
            	
            	  $scope.$watch('tableData',function(newValue,oldValue){
            		  if($scope.tableisChecked!=undefined && $scope.tableisChecked){
                      		initTable();	
                      	}
                  });
            	  
            	  $scope.setIsCompleted=function(obj){
            		  if(obj!=undefined && obj!=null){
            			  obj.isCompleted='No';
            		  }
            	  }
            	
            	$scope.tableCheck = function(){
            		$scope.tableisChecked=false;
                    var fineArr = [];
                    $scope.secondArr=[];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData,function(dataObj,index){
                        $scope.secondArr[index]={};
                        if (isEmptyRow(dataObj)){
                            validResult = true;
                            finalData[index].isEmpty = true;

                        }else{
                            validResult = validateObj(dataObj,index,"row");

                        }
                        if(validResult){
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        }else{
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    angular.forEach(finalData,function(dataObj,index){
                        if(dataObj.isEmpty){
                            finalData.splice(index,1);
                            $scope.secondArr.splice(index,1);
                        }
                    });
                    console.log("***8$scope.tableData",finalData);
                    if(fineArr.indexOf(0)<0){
                        $scope.tableData = finalData;
                        return true;
                    }else{
                        return false;
                    }
                };

                function isEmptyRow(obj){
                    // return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; // empty
                    // if (!obj) {
                    // return isempty;
                    // }
                    console.log("obj*********",obj);	
                    console.log("emptyObjRef*********",$scope.emptyObjRef);	

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++){
                        if (obj[k[i]]!=$scope.emptyObjRef[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }
                
                $scope.toggleArr = [{id:true,title:'Yes'},{id:false,title:'No'}];
                $scope.flag=false;

                // show errors

                // temp array
                $scope.secondArr = [];

                $scope.$watch('eventStatus',function(newValue,oldValue){
                    if(newValue!=oldValue){
                    	$scope.tempEventStatus = angular.copy($scope.eventStatus);
                        if($scope.eventStatus==true 
                        		|| $scope.eventStatus=='Active'
                        		|| $scope.eventStatus=='Closed'
                        		|| $scope.eventStatus=='Cancelled'){
                        	$scope.tempEventStatus = 'Generated';
                        }

                    }
                });



                function initTable(){
                	$scope.emptyObjRef = {isCompleted : 'Yes', followUpRequired : 'No'};
                    if($scope.tableData != undefined && $scope.tableData != null && $scope.tableData.length != 0) {
                    	$scope.emptyObjRef = angular.copy($scope.tableData[$scope.tableData.length-1]);
                    }
                    angular.forEach($scope.tableData,function(item){
                        $scope.secondArr.push({"errRow":false});
                    });
                }
                initTable();


                // add row
                $scope.addRow = function(){
                	$scope.emptyObjRef = {};
                    var fineArr = [];
                    angular.forEach($scope.tableData,function(dataObj,index){
                        // var curObj = $scope.tableData.data[index];
                    	$scope.secondArr[index]={};
                        var validResult  = validateObj(dataObj,index,"row");
                        if(validResult){
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        }else{
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        // console.log();
                    });
                    if(fineArr.indexOf(0)<0){
                        $scope.eventsObj = {};
                        $scope.eventsObj.isCompleted = 'Yes';
                        $scope.eventsObj.followUpRequired = 'No';
                    	$scope.tableData.push($scope.eventsObj);
                    	$rootScope.navigateToNextField($scope.indexKey+'eventMaster'+($scope.tableData.length-1))
                        $scope.emptyObjRef  = angular.copy($scope.eventsObj);
                        $scope.secondArr.push({"errRow":false});
                        $scope.indexFocus = null;
                    }
                };
                
                // call Validate
                $scope.callValidate = function(){
                    var fineArr = [];
                    angular.forEach($scope.tableData,function(dataObj,index){
                       
                        var validResult  = validateObj(dataObj,index,"row");
                        
                        if(validResult){
                        
                            $scope.secondArr[index].errRow = false;
                            $scope.tableState({param:true});
                        }else{
                            $scope.secondArr[index].errRow = true;
                            $scope.tableState({param:false});
                            
                        }
                       
                    });
                    
                   
                };

                var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
                var errorOnRowIndex = null;
                $scope.errorShow = function(errorObj,index){
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to
					// ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxTriggerTypeMasterEvent = function (object){
                    $scope.searchDto={};
                    $scope.searchDto.keyword=object==null?"":object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    TriggerTypeSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function(data) {
                            if (data.responseCode =="ERR0"){
                                $scope.triggerTypeMasterList = data.responseObject.searchResult;
                                // $scope.showTriggerTypeMasterList=true;
                            }
                        },
                        function(errResponse){
                            console.error('Error while fetching Customer Services');
                        }
                    );
                };
                $scope.selectTriggerType = function(dataOb){
                    $scope.triggerTypeMaster = dataOb.triggerTypeMaster;
                    $scope.callValidate();
                }
                $scope.ajaxTriggerMasterEvent = function (object){
                    $scope.searchDto={};
                    $scope.searchDto.keyword=object==null?"":object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    if($scope.triggerTypeMaster.triggerTypeName!=undefined &&  $scope.triggerTypeMaster.triggerTypeName!=null && $scope.triggerTypeMaster.triggerTypeName!="" ){
                        TriggerSearchKeyword.fetch({"triggerType":$scope.triggerTypeMaster.triggerTypeName},$scope.searchDto).$promise.then(
                            function(data) {
                                if (data.responseCode =="ERR0"){
                                    $scope.triggerMasterList = data.responseObject.searchResult;
                                    // $scope.showTriggerMasterList=true;
                                }
                            },
                            function(errResponse){
                                console.error('Error while fetching Customer Services');
                            }
                        );
                    }else{
                        $scope.validateShipmentServiceTrigger(1);
                    }
                }
                $scope.ajaxAssignedToEvent = function (object){
                    $scope.searchDto={};
                    $scope.searchDto.keyword=object==null?"":object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return EmployeeList.fetch($scope.searchDto).$promise.then(
                        function(data) {
                            if (data.responseCode =="ERR0"){
                                $scope.assignedToList = data.responseObject.searchResult;
                                return $scope.assignedToList;
                                // $scope.showAssignedToList=true;
                            }
                        },
                        function(errResponse){
                            console.error('Error while fetching Customer Services');
                        }
                    );

                }

                $scope.ajaxEventMasterEvent = function(object) {
                    console.log("ajaxEventMasterEvent ", object);

                    $scope.searchDto={};
                    $scope.searchDto.keyword=object==null?"":object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return EventSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                            if (data.responseCode =="ERR0"){
                                $scope.eventMasterList = data.responseObject.searchResult;
                                return $scope.eventMasterList;
                                // $scope.showEventMasterList=true;
                            }
                        },
                        function(errResponse){
                            console.error('Error while fetching Event');
                        }
                    );

                }
                
                
                
                
                
                
                
                
                $scope.getEventNotInList = function(keyword) {
                	
                	
                	if($scope.tableData != null && $scope.tableData.length > 0) {
                		   var idList = [];

                		   for(var i = 0; i < $scope.tableData.length; i++) {
                			   if($scope.tableData[i] != null && 
                					   $scope.tableData[i].eventMaster  != null) {
                				   idList.push($scope.tableData[i].eventMaster.id);
                			   }
                		   }
                	}
                	   
                	   return AutoCompleteService.getEventNotInList(keyword, idList);
                	
                }
                
                
                

                // error validation rules
                var validateObj = function(dataObj,index,type){
                	
                	$scope.firstFocus=false;
                    var errorArr = [];
                    if(type=="row"){
                    	$scope.secondArr[index] = {};
                        $scope.secondArr[index].errTextArr = [];
                    }
                    
                   

                        if (dataObj.eventMaster == undefined
                            || dataObj.eventMaster == null
                            || dataObj.eventMaster == ""
                            || dataObj.eventMaster.id == null ) {

                            if(type=="row") {
                                $scope.secondArr[index].eventMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90465"]);
                            }
                            // return false;
                            errorArr.push(0);
                           // console.log($rootScope.nls["ERR90465"]);
                           // $scope.errorMap.put("shipment.event.eventMaster",$rootScope.nls["ERR90465"]);
                           // return false;

                        }else if(dataObj.eventMaster.status!=null){
                            if(ValidateUtil.isStatusBlocked(dataObj.eventMaster.status))
                            {

                                if(type=="row") {
                                    $scope.secondArr[index].eventMaster = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90466"]);
                                }
                                errorArr.push(0);
                                // console.log($rootScope.nls["ERR90466"]);
                                // $scope.errorMap.put("shipment.event.eventMaster",$rootScope.nls["ERR90466"]);
                                // dataObj.eventMaster = null;

                                // return false

                            }

                            else if(ValidateUtil.isStatusHidden(dataObj.eventMaster.status))
                            {

                                if(type=="row") {
                                    $scope.secondArr[index].eventMaster = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90467"]);
                                }
                                errorArr.push(0);

                                // console.log($rootScope.nls["ERR90467"]);
                                // $scope.errorMap.put("shipment.event.eventMaster",$rootScope.nls["ERR90467"]);
                                // dataObj.eventMaster = null;
                                // return false

                            }
                        }
                    else{
                    	
                            if(type=="row") {
                                $scope.secondArr[index].eventMaster = false;

                            }
                            errorArr.push(1);

                        }

                        if (dataObj.eventDate == undefined|| dataObj.eventDate == null || dataObj.eventDate == "" ) {
                            if(type=="row") {
                                $scope.secondArr[index].eventDate = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90468"]);
                            }
                            errorArr.push(0);

                            // console.log($rootScope.nls["ERR90468"]);
                           // $scope.errorMap.put("shipment.event.eventDate",$rootScope.nls["ERR90468"]);
                           // return false;


                        }

                        else{
                            if(type=="row") {
                                $scope.secondArr[index].eventDate = false;

                            }
                            errorArr.push(1);

                        }

                        if(dataObj.followUpRequired=='No' || dataObj.followUpRequired==false)
                    	{
                    	 if(type=="row") {
                    		 $scope.secondArr[index].followUpDate = false;
                             $scope.secondArr[index].assignedTo = false;
                             dataObj.assignedTo=null;
                             dataObj.followUpDate=null;
                         }
                         errorArr.push(1);
                    	}
                        if(dataObj.followUpRequired=='Yes' || dataObj.followUpRequired==true){

                            if (dataObj.assignedTo == undefined
                                || dataObj.assignedTo == null
                                || dataObj.assignedTo == ""
                                || dataObj.assignedTo.id == null ) {

                                if(type=="row") {
                                    $scope.secondArr[index].assignedTo = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90469"]);
                                }
                                errorArr.push(0);
                                // console.log($rootScope.nls["ERR90469"]);
                                // $scope.errorMap.put("shipment.event.assignedTo",$rootScope.nls["ERR90469"]);
                                // return false;

                            }else if(dataObj.assignedTo.status!=null){
                                if(ValidateUtil.isEmployeeResigned(dataObj.assignedTo.employementStatus))
                                {

                                    if(type=="row") {
                                        $scope.secondArr[index].assignedTo = true;
                                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90470"]);
                                    }
                                    errorArr.push(0);
                                    // console.log($rootScope.nls["ERR90470"]);
                                    // $scope.errorMap.put("shipment.event.assignedTo",$rootScope.nls["ERR90470"]);
                                    // dataObj.assignedTo = null;
                                    // return false
                                }

                              else  if(ValidateUtil.isEmployeeTerminated(dataObj.assignedTo.employementStatus))
                                {

                                    if(type=="row") {
                                        $scope.secondArr[index].assignedTo = true;
                                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90471"]);
                                    }
                                    errorArr.push(0);
                                    // console.log($rootScope.nls["ERR90471"]);
                                    // $scope.errorMap.put("shipment.event.assignedTo",$rootScope.nls["ERR90471"]);
                                    // dataObj.assignedTo = null;
                                    // return false

                                }
                            }
                            else{
                                if(type=="row") {
                                    $scope.secondArr[index].assignedTo = false;

                                }
                                errorArr.push(1);

                            }
                        }



                        if(dataObj.followUpRequired=='Yes' || dataObj.followUpRequired==true){

                            if (dataObj.followUpDate == undefined
                                || dataObj.followUpDate == null
                                || dataObj.followUpDate == ""
                            ) {

                                if(type=="row") {
                                    $scope.secondArr[index].followUpDate = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90472"]);
                                }
                                errorArr.push(0);
                                // console.log($rootScope.nls["ERR90472"]);
                                // $scope.errorMap.put("shipment.event.followUpDate",$rootScope.nls["ERR90472"]);
                                // return false;

                            }
                            else{
                                if(type=="row") {
                                    $scope.secondArr[index].followUpDate = false;

                                }
                                errorArr.push(1);

                            }

                        }








                    if(errorArr.indexOf(0)<0){
                        return true
                    }else{
                        return false;
                    }

                };



                          $(document).on('keydown',function(e){
                    if(e.keyCode==27){
                        if(myOtherModal){
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide',function(){
                    if($scope.secondArr[errorOnRowIndex].chargeCode){
                        $scope.secondArr[errorOnRowIndex].chargeCode=false;
                    }
                    $timeout(function(){
                        $scope.secondArr[errorOnRowIndex].chargeCode=true;
                        errorOnRowIndex = null;
                    },10)
                });





                $scope.removeObj = function(obj,index){
                    $scope.tableData.splice(index,1);
                    $scope.secondArr.splice(index,1);
                    $scope.indexFocus = null;
                };


                $scope.eventRender = function(item){
                    return {
                        label:item.eventName,
                        item:item
                    }
                }
                $scope.employeeRender = function(item){
                    return {
                        label:item.employeeName,
                        item:item
                    }
                }
                $scope.nextToFocus = function(id){
            		console.log("called."+id);
            		$rootScope.navigateToNextField(id);
            	}
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;

            
        	
        	},2);
        }
    }
}]);