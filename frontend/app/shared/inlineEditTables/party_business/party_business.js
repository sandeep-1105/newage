/**
 * Created by saravanan on 23/6/16.
 */
app.directive('partyBusinessTable',['$rootScope','$timeout','$compile','ValidateUtil','ServiceList','TosSearchKeyword',
            'PortSearchKeyword','PeriodList','CommodityList', 'UnitSearchKeyword', 'appConstant', 'CommonValidationService',
    function($rootScope,$timeout,$compile,ValidateUtil,ServiceList,TosSearchKeyword,
            PortSearchKeyword,PeriodList,CommodityList, UnitSearchKeyword, appConstant, CommonValidationService){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/party_business/party_business.html',
        scope:{
            tableData:"=",
            tableCheck:"=",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                var dirtArr = [];

                $scope.potentialArr = [{value: true, title: 'Current'}, {value: false, title: 'Potential'}];

                $scope.partyBusinessArr = $rootScope.enum['PartyBusinessStatus'];
                $scope.selectLocation = $rootScope.userProfile.selectedUserLocation;
                $scope.partyReasonArr = $rootScope.enum['Reason'];

                $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
                $scope.directiveDatePickerOpts.widgetParent = "body";

                $scope.scrollConfig = $rootScope.ngScrollConfigScroll = {
                    "cursorcolor": '#3F8DEB',
                    "cursorwidth": '8px',
                    "smoothscroll": true,
                    "autohidemode": false
                };
                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                PeriodList.get().$promise.then(function (data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.frequencies = data.responseObject;
                        }
                    },
                    function (errResponse) {
                        console.error('Error while fetching Frequency');
                    }
                );


                $scope.getDefaultBussinessData = function () {
                    businessObj = {};
                    businessObj.bussinessStatus = 'Active';
                    return businessObj;
                }
                function isEmptyRow(obj) {

                    var isempty = true;
                    console.log("empty obj*******", obj);

                    $scope.emptyObjRef = $scope.getDefaultBussinessData();
                    console.log("$scope.emptyObjRef*****", $scope.emptyObjRef);
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {

                        //check the new key is empty or not
                        if (obj[k[i]]) {
                            // check the default object contains the key
                            if (!$scope.emptyObjRef.hasOwnProperty(k[i])) {
                                isempty = false;
                                break;
                            }
                            //check the type is object or not
                            if (angular.isObject(obj[k[i]])) {
                                if (obj[k[i]].id != null && $scope.emptyObjRef[k[i]].id != null && obj[k[i]].id != $scope.emptyObjRef[k[i]].id) {
                                    isempty = false;
                                    break;
                                }
                            }
                            else {
                                if (obj[k[i]] != $scope.emptyObjRef[k[i]]) {
                                    isempty = false;
                                    break;

                                }
                            }
                        }
                    }
                    console.log("empty check *****", isempty)
                    return isempty;
                }

                $scope.indexFocus = null;
                $scope.secondArr = [];

                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                $scope.ajaxBusinessService = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return ServiceList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.partyBusinessServiceList = data.responseObject.searchResult;
                                return $scope.partyBusinessServiceList
                                //$scope.showBusinessServiceList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Services');
                        }
                    );

                }

                $scope.ajaxBusinessPeriodEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PeriodList.get().$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.frequencies = data.responseObject;
                                return $scope.frequencies;
                                //$scope.showBusinessServiceList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching  Periods');
                        }
                    );

                }


                //$scope.ajaxBusinessService(null);

                $scope.ajaxBusinessTosEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return TosSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.partyBusinessTosList = data.responseObject.searchResult;
                                return $scope.partyBusinessTosList;
                                //$scope.showBusinessTosList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );

                }

                //$scope.ajaxBusinessTosEvent(null);

                $scope.selectedService = {};
                $scope.selectedOrigin = {};
                $scope.changeService = function (dataObj, nextFieldId) {
                    $scope.selectedService = dataObj.serviceMaster;
                    $scope.selectedOrigin = dataObj.origin;
                    if (dataObj.serviceMaster != undefined && dataObj.serviceMaster != null && dataObj.serviceMaster != "")
                        if (dataObj.bussinessStatus == undefined || dataObj.bussinessStatus == null)
                            dataObj.bussinessStatus = 'Active';
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }
                }


                $scope.changeTos = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }
                }
                $scope.changeOrigin = function (nextFieldId) {
                    if (nextFieldId != undefined) {
                        $rootScope.navigateToNextField(nextFieldId);
                    }
                }

                $scope.ajaxPortOriginEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return PortSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.originList = [];
                                $scope.resultArr = data.responseObject.searchResult;
                                angular.forEach($scope.resultArr, function (map) {
                                    if ($scope.selectedService == null || $scope.selectedService.id == null || $scope.selectedService.id.trim == "") {
                                        $scope.originList.push(map);
                                    } else {
                                        if ($scope.selectedService.transportMode == map.transportMode) {
                                            $scope.originList.push(map);
                                        }
                                    }

                                });
                                return $scope.originList;
                                //$scope.showBusinessoriginList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Port');
                        }
                    );


                }

                $scope.ajaxPortDestinationEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return PortSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.destinationList = [];
                                $scope.resultArr = data.responseObject.searchResult;
                                angular.forEach($scope.resultArr, function (map) {
                                    $scope.destinationList.push(map);

                                });
                                return $scope.destinationList;

                                //$scope.showBusinessDestinationList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Port');
                        }
                    );


                }

                $scope.ajaxBusinessCommodityEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return CommodityList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.businesscommodityList = data.responseObject.searchResult;
                                return $scope.businesscommodityList;
                                //$scope.showBusinessCommodityList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching commodity Services');
                        }
                    );

                }
                //$scope.ajaxBusinessCommodityEvent(null);


                $scope.selectDestination = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.selectCommodity = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.nextToFocus = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }
                $scope.callValidate = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                }


                $scope.addRow = function () {
                    $scope.emptyObjRef = {};
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        $scope.secondArr[index] = {};
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });

                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableData == undefined || $scope.tableData == null)
                            $scope.tableData = [];
                        $scope.businessObj = {};
                        $scope.businessObj.bussinessStatus = 'Active';
                        $scope.tableData.push($scope.businessObj);
                        $scope.emptyObjRef = angular.copy($scope.businessObj);
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].businessServiceFocus = true;
                        }, 10);
                    } else {
                        $timeout(function () {
                            $scope.secondArr[$scope.tableData.length - 1].businessServiceFocus = true;
                        }, 10);
                    }
                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.serviceMaster == null || dataObj.serviceMaster.serviceName == null || dataObj.serviceMaster.serviceName == undefined ||
                        dataObj.serviceMaster.serviceName == "") {
                        if (type == "row") {
                            $scope.secondArr[index].serviceName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6008"]);
                        }
                        //return false;
                        errorArr.push(0);


                    }
                    else if (dataObj.serviceMaster.status == "Block") {
                        if (type == "row") {
                            $scope.secondArr[index].serviceName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6061"]);
                        }
                        //return false;
                        errorArr.push(0);


                    }
                    else {
                        if (type == "row") {
                            $scope.secondArr[index].serviceName = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }

                    if (dataObj.origin != null) {
                        if (dataObj.origin.status == "Block") {
                            if (type == "row") {
                                $scope.secondArr[index].origin = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6062"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].origin = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }

                    if (dataObj.destination != undefined && dataObj.destination != null
                        && dataObj.origin != undefined && dataObj.origin != null) {
                        if (dataObj.destination.portCode != undefined) {
                            if (dataObj.destination.portCode == dataObj.origin.portCode) {
                                if (type == "row") {
                                    $scope.secondArr[index].destination = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6063"]);
                                }
                                //return false;
                                errorArr.push(0);

                            } else {
                                if (type == "row") {
                                    $scope.secondArr[index].destination = false;

                                }
                                //return false;
                                errorArr.push(1);
                            }
                        }
                    }

                    if (dataObj.destination != null) {
                        if (dataObj.destination.status == "Block") {
                            if (type == "row") {
                                $scope.secondArr[index].destination = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6064"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].destination = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }
                    if (dataObj.estimatedRevenue != null && dataObj.estimatedRevenue != "" &&
                        dataObj.estimatedRevenue != undefined) {
                    	
                    	
                    	if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_ESTIMATED_REVENUE,dataObj.estimatedRevenue)){
                    		if (type == "row") {
                                $scope.secondArr[index].estimatedRevenue = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6101"]);
                            }
                            errorArr.push(0);
            			}
                    	else {
                            if (type == "row") {
                                $scope.secondArr[index].estimatedRevenue = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }
                    if (dataObj.businessNotes != null) {
                        var regexp = new RegExp("[ A-Za-z0-9\n_@.#&+-]{0,500}");
                        if (!regexp
                                .test(dataObj.businessNotes)) {
                            if (type == "row") {
                                $scope.secondArr[index].businessNotes = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6065"]);
                            }
                            //return false;
                            errorArr.push(0);

                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].businessNotes = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }
                    if(dataObj.noOfUnits != null) {
                    	
                    	if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_NO_OF_UNITS,dataObj.noOfUnits)){
                    		if (type == "row") {
                    			$scope.secondArr[index].noOfUnits = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6102"]);
                            }
                            errorArr.push(0);
            			}
                    	 else {
                            if (type == "row") {
                                $scope.secondArr[index].noOfUnits = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }

                    if (dataObj.noOfShipments != null) {

                    	if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_NO_OF_SHIPMENTS,dataObj.noOfShipments)){
                    		if (type == "row") {
                    			 $scope.secondArr[index].noOfShipments = true;
                                 $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6103"]);
                            }
                            errorArr.push(0);
            			}
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].noOfShipments = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                    }


                    if (dataObj.commodity != null) {
                        if (dataObj.commodity.status == "Block") {
                            if (type == "row") {
                                $scope.secondArr[index].commodity = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6066"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].commodity = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.tosMaster != null) {
                        if (dataObj.tosMaster.status == "Block") {
                            if (type == "row") {
                                $scope.secondArr[index].tosName = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6067"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].tosName = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }
                    }


                    if (dataObj.unitMaster != null) {

                        if (dataObj.unitMaster.status == 'Block') {
                            $scope.secondArr[index].unitMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR6090"]);
                            errorArr.push(0);
                        } else {
                            $scope.secondArr[index].unitMaster = false;
                            errorArr.push(1);
                        }

                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };
                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].businessServiceFocus) {
                        $scope.secondArr[errorOnRowIndex].businessServiceFocus = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].businessServiceFocus = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;
                };


                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                };

                $scope.serviceRender = function (item) {
                    return {
                        label: item.serviceName,
                        item: item
                    }
                }
                $scope.tosRender = function (item) {
                    return {
                        label: item.tosName,
                        item: item
                    }
                }
                $scope.portRender = function (item) {
                    return {
                        label: item.portName,
                        item: item
                    }
                }
                $scope.commodityRender = function (item) {
                    return {
                        label: item.hsName,
                        item: item
                    }
                }

                $scope.periodRender = function (item) {
                    return {
                        label: item.frequencyName,
                        item: item
                    }
                }

                /***** Business Table More Info Popup  *****/

                $scope.moreInfo = function (moredit) {
                    var newScope = $scope.$new();
                    newScope.moredit = moredit;
                    var myTotalOtherModal = $modal(
                        {
                            scope: newScope,
                            templateUrl: '/app/components/master/party/business_table_popup.html',
                            show: false
                        });
                    myTotalOtherModal.$promise.then(myTotalOtherModal.show);
                };


                $scope.ajaxBusinessUnitEvent = function (object) {

                    console.log("ajaxUnitEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return UnitSearchKeyword.query($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.unitMasterList = data.responseObject.searchResult;
                                return $scope.unitMasterList;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Unit List');
                        }
                    );

                }

                $scope.unitRender = function (item) {
                    return {
                        label: item.unitCode,
                        item: item
                    }
                }

                $scope.selectedUnitMaster = function (nextFieldId) {
                    if (nextFieldId != undefined)
                        $rootScope.navigateToNextField(nextFieldId);
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;
            },2);
        }

    }
}]);