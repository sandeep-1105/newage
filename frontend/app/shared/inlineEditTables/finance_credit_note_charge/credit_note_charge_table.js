/**
 * Created by hmspl on 20/7/16.
 */

/**
 * Created by saravanan on 23/6/16.
 */
app.directive('creditNoteChargeTable',['$timeout','$compile','$rootScope','ValidateUtil','EmployeeList','EventSearchKeyword',
    function($timeout,$compile,$rootScope,ValidateUtil,EmployeeList,EventSearchKeyword){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/finance_credit_note_charge/credit_note_charge_table.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            tableState:"&",
            calcFn:"&"
        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
                if ($scope.tableData == null || $scope.tableData == undefined) {
                    $scope.tableData = [];

                }

                $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
                $scope.directiveDatePickerOpts.widgetParent = "body";
                $scope.toggleArr = [{id: true, title: 'Yes'}, {id: false, title: 'No'}];


                //show errors
                $scope.$watch('showErrors', function (newValue, oldValue) {
                    console.log("show Errors", newValue, oldValue);
                    if (newValue) {
                        var focusErrArr = [];
                        angular.forEach($scope.tableData, function (dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                $scope.secondArr[index].errRow = false;
                                focusErrArr.push(1);
                            } else {
                                $scope.secondArr[index].errRow = true;
                                focusErrArr.push(0);

                            }
                            //console.log();
                        });
                        if (focusErrArr.indexOf(0) < 0) {
                            $scope.showErrors = false;
                        } else {
                            $scope.showErrors = true;
                        }
                    }
                });

                //indexfocus initialization
                $scope.indexFocus = null;
                //temp array
                $scope.secondArr = [];


                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();

                try {
                    if ($scope.tableData.length < 0) {
                        $scope.tableData = [];
                        $scope.tableData.push({});
                        //dirtArr.push(0);
                        initTable();
                    }
                } catch (e) {
                    $scope.tableData = [];
                    $scope.tableData.push({});
                    initTable();
                }
                //add row
                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData.push({});
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                        $scope.secondArr[$scope.tableData.length - 1].eventMaster = true;
                        if ($scope.tableState) {
                            $scope.tableState({param: true});
                        }
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                    }
                };

                //call Validate
                $scope.callValidate = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {

                        var validResult = validateObj(dataObj, index, "row");

                        if (validResult) {

                            $scope.secondArr[index].errRow = false;
                            $scope.tableState({param: true});
                        } else {
                            $scope.secondArr[index].errRow = true;
                            $scope.tableState({param: false});

                        }

                    });


                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;
                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                $scope.ajaxTriggerTypeMasterEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    TriggerTypeSearchKeyword.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.triggerTypeMasterList = data.responseObject.searchResult;
                                //$scope.showTriggerTypeMasterList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );
                };
                $scope.selectTriggerType = function (dataOb) {
                    $scope.triggerTypeMaster = dataOb.triggerTypeMaster;
                    $scope.callValidate();
                }
                $scope.ajaxTriggerMasterEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    if ($scope.triggerTypeMaster.triggerTypeName != undefined && $scope.triggerTypeMaster.triggerTypeName != null && $scope.triggerTypeMaster.triggerTypeName != "") {
                        TriggerSearchKeyword.fetch({"triggerType": $scope.triggerTypeMaster.triggerTypeName}, $scope.searchDto).$promise.then(
                            function (data) {
                                if (data.responseCode == "ERR0") {
                                    $scope.triggerMasterList = data.responseObject.searchResult;
                                    //$scope.showTriggerMasterList=true;
                                }
                            },
                            function (errResponse) {
                                console.error('Error while fetching Customer Services');
                            }
                        );
                    } else {
                        $scope.validateShipmentServiceTrigger(1);
                    }
                }
                $scope.ajaxAssignedToEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return EmployeeList.fetch($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.assignedToList = data.responseObject.searchResult;
                                return $scope.assignedToList;
                                //$scope.showAssignedToList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Customer Services');
                        }
                    );

                }

                $scope.ajaxEventMasterEvent = function (object) {
                    console.log("ajaxEventMasterEvent ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return EventSearchKeyword.fetch($scope.searchDto).$promise.then(function (data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.eventMasterList = data.responseObject.searchResult;
                                return $scope.eventMasterList;
                                //$scope.showEventMasterList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Event');
                        }
                    );

                }

                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index] = {};
                        $scope.secondArr[index].errTextArr = [];
                    }


                    if (dataObj.eventMaster == undefined
                        || dataObj.eventMaster == null
                        || dataObj.eventMaster == ""
                        || dataObj.eventMaster.id == null) {

                        if (type == "row") {
                            $scope.secondArr[index].eventMaster = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90465"]);
                        }
                        //return false;
                        errorArr.push(0);
                        // console.log($rootScope.nls["ERR90465"]);
                        // $scope.errorMap.put("shipment.event.eventMaster",$rootScope.nls["ERR90465"]);
                        // return false;

                    } else if (dataObj.eventMaster.status != null) {
                        if (ValidateUtil.isStatusBlocked(dataObj.eventMaster.status)) {

                            if (type == "row") {
                                $scope.secondArr[index].eventMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90466"]);
                            }
                            errorArr.push(0);
                            //console.log($rootScope.nls["ERR90466"]);
                            //$scope.errorMap.put("shipment.event.eventMaster",$rootScope.nls["ERR90466"]);
                            //dataObj.eventMaster = null;

                            //return false

                        }

                        else if (ValidateUtil.isStatusHidden(dataObj.eventMaster.status)) {

                            if (type == "row") {
                                $scope.secondArr[index].eventMaster = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90467"]);
                            }
                            errorArr.push(0);

                            //console.log($rootScope.nls["ERR90467"]);
                            //$scope.errorMap.put("shipment.event.eventMaster",$rootScope.nls["ERR90467"]);
                            //dataObj.eventMaster = null;
                            //return false

                        }
                    }
                    else {

                        if (type == "row") {
                            $scope.secondArr[index].eventMaster = false;

                        }
                        errorArr.push(1);

                    }

                    if (dataObj.eventDate == undefined || dataObj.eventDate == null || dataObj.eventDate == "") {
                        if (type == "row") {
                            $scope.secondArr[index].eventDate = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90468"]);
                        }
                        errorArr.push(0);

                        // console.log($rootScope.nls["ERR90468"]);
                        // $scope.errorMap.put("shipment.event.eventDate",$rootScope.nls["ERR90468"]);
                        // return false;


                    }

                    else {
                        if (type == "row") {
                            $scope.secondArr[index].eventDate = false;

                        }
                        errorArr.push(1);

                    }

                    if (dataObj.followUpRequired == 'No' || dataObj.followUpRequired == false) {
                        if (type == "row") {
                            $scope.secondArr[index].followUpDate = false;
                            $scope.secondArr[index].assignedTo = false;
                            dataObj.assignedTo = null;
                            dataObj.followUpDate = null;
                        }
                        errorArr.push(1);
                    }
                    if (dataObj.followUpRequired == 'Yes' || dataObj.followUpRequired == true) {

                        if (dataObj.assignedTo == undefined
                            || dataObj.assignedTo == null
                            || dataObj.assignedTo == ""
                            || dataObj.assignedTo.id == null) {

                            if (type == "row") {
                                $scope.secondArr[index].assignedTo = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90469"]);
                            }
                            errorArr.push(0);
                            //console.log($rootScope.nls["ERR90469"]);
                            //$scope.errorMap.put("shipment.event.assignedTo",$rootScope.nls["ERR90469"]);
                            //return false;

                        } else if (dataObj.assignedTo.status != null) {
                            if (ValidateUtil.isEmployeeResigned(dataObj.assignedTo.employementStatus)) {

                                if (type == "row") {
                                    $scope.secondArr[index].assignedTo = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90470"]);
                                }
                                errorArr.push(0);
                                //console.log($rootScope.nls["ERR90470"]);
                                //$scope.errorMap.put("shipment.event.assignedTo",$rootScope.nls["ERR90470"]);
                                //dataObj.assignedTo = null;
                                //return false
                            }

                            else if (ValidateUtil.isEmployeeTerminated(dataObj.assignedTo.employementStatus)) {

                                if (type == "row") {
                                    $scope.secondArr[index].assignedTo = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90471"]);
                                }
                                errorArr.push(0);
                                //console.log($rootScope.nls["ERR90471"]);
                                //$scope.errorMap.put("shipment.event.assignedTo",$rootScope.nls["ERR90471"]);
                                //dataObj.assignedTo = null;
                                //return false

                            }
                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].assignedTo = false;

                            }
                            errorArr.push(1);

                        }
                    }


                    if (dataObj.followUpRequired == 'Yes' || dataObj.followUpRequired == true) {

                        if (dataObj.followUpDate == undefined
                            || dataObj.followUpDate == null
                            || dataObj.followUpDate == ""
                        ) {

                            if (type == "row") {
                                $scope.secondArr[index].followUpDate = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90472"]);
                            }
                            errorArr.push(0);
                            //console.log($rootScope.nls["ERR90472"]);
                            //$scope.errorMap.put("shipment.event.followUpDate",$rootScope.nls["ERR90472"]);
                            //return false;

                        }
                        else {
                            if (type == "row") {
                                $scope.secondArr[index].followUpDate = false;

                            }
                            errorArr.push(1);

                        }

                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.makeTableState = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "change");
                        if (validResult) {
                            fineArr.push(1);
                        } else {
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    console.log("fine arr", fineArr);
                    if (fineArr.indexOf(0) < 0) {
                        if ($scope.tableState) {
                            changeLastStatus();
                            $scope.tableState({param: true});
                        }
                    } else {
                        if ($scope.tableState) {
                            $scope.tableState({param: false});
                        }
                    }
                };
                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.$watch('indexFocus', function (newvalue, oldValue) {
                    if (newvalue != null && oldValue != null && newvalue != oldValue) {
                        var focusErrArr = [];
                        angular.forEach($scope.tableData, function (dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                $scope.secondArr[index].errRow = false;
                                focusErrArr.push(1);
                            } else {
                                $scope.secondArr[index].errRow = true;
                                focusErrArr.push(0);

                            }
                            //console.log();
                        });
                        if (focusErrArr.indexOf(0) < 0) {
                            if ($scope.tableState) {
                                $scope.tableState({param: true});
                            }
                        } else {
                            if ($scope.tableState) {
                                $scope.tableState({param: false});
                            }
                        }
                    }
                });


                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;
                };
                $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                $scope.openEvent = function (param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = {"overflow-x": "hidden"};
                    } else {
                        $scope.dropStyle = {"overflow-x": "auto", "overflow-y": "hidden"};
                    }
                }

                $scope.eventRender = function (item) {
                    return {
                        label: item.eventName,
                        item: item
                    }
                }
                $scope.employeeRender = function (item) {
                    return {
                        label: item.employeeName,
                        item: item
                    }
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;
            },2);
        }
    }
}]);