/**
 * Created by saravanan on 23/6/16.
 */
app.directive('quotationChargeTable', ['$timeout', '$compile', '$rootScope', 'ChargeSearchKeyword', 'CurrencyMasterSearchKeyword', 'Notification',
    'UnitSearchKeyword', 'UnitMasterGetByCode', 'ValidateUtil', 'roleConstant', 'appConstant',
    'CommonValidationService', 'appMetaFactory', 'GetSurcharge', '$http',
    function($timeout, $compile, $rootScope, ChargeSearchKeyword, CurrencyMasterSearchKeyword, Notification,
        UnitSearchKeyword, UnitMasterGetByCode, ValidateUtil, roleConstant, appConstant,
        CommonValidationService, appMetaFactory, GetSurcharge, $http) {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/inlineEditTables/charge_quotation/charge_quotation.html',
            scope: {
                indexKey: "=",
                tableData: "=",
                dependData: "=",
                dependServiceData: "=",
                dependCarrierData: "=",
                fnRole: "&",
                callFun: "&",
                addRole: "=",
                editRole: "=",
                deleteRole: "=",
                fetchRole: "=",
                btnIndex: '@',
                tableCheck: "=?"
            },
            controller: function($scope, $rootScope, $popover, $modal) {

                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);

                var dirtArr = [];

                $scope.buyRateAccess = $rootScope.roleAccess(roleConstant.SALE_QUOTATION_SERVICE_CHARGE_BUY_RATE_VIEW, true);

                $scope.groupCol = [{
                    name: 'Charge Code',
                    width: '100px',
                    valueField: 'chargeCode'
                }, {
                    name: 'Charge Name',
                    width: '120px',
                    valueField: 'chargeName'
                }];

                $scope.chargeType = [{ id: "CHARGEABLE", title: "No" }, { id: "ACTUAL", title: "Yes" }];

                console.log("appConstant", appConstant.chargeNumberOfUnitList);
                $scope.numberOfUnitsList = appConstant.chargeNumberOfUnitList;
                $scope.indexFocus = null;
                $scope.secondArr = [];
                $scope.totalNoOfPieces = 0;

                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.isSurchargeAvailable = function(charge) {
                    charge.surChargeFlag = false;
                    GetSurcharge.get({
                        chargeId: charge.chargeMaster.id
                    }, function(data) {
                        if (data.responseCode == "ERR0") {
                            if (data.responseObject == true) {
                                charge.surChargeFlag = true;
                                if ($scope.dependData.customer != null && $scope.dependData.customer != undefined && $scope.dependData.customer != '') {
                                    if (($scope.dependData.customer.partyDetail != null &&
                                            $scope.dependData.customer.partyDetail != undefined) &&
                                        $scope.dependData.customer.partyDetail.billingFscScOnGrossWeight == true) {
                                        charge.actualchargeable = 'ACTUAL';
                                    } else {
                                        charge.actualchargeable = 'CHARGEABLE';
                                    }
                                }

                            }

                        } else {
                            console.log("GetSurcharge Failed " + data.responseDescription)
                        }
                    }, function(error) {
                        console.log("GetSurcharge Failed : " + error)
                    });

                }



                $scope.showChargeHistory = function(id) {
                    /*var searchDto={};
        		searchDto.selectedPageNumber =$scope.page;
        		searchDto.recordPerPage =$scope.limit;*/
                    var auditArr = [];
                    $http({
                        method: 'GET',
                        url: '/api/v1/quotation/getquotation/chargehistory/' + id
                    }).then(function(response) {
                        if (response.data.responseCode == "ERR0") {
                            $scope.auditArr = [];
                            $scope.auditArr = response.data.responseObject;
                            var chargeModal;
                            chargeModal = $modal({
                                scope: $scope,
                                backdrop: 'static',
                                templateUrl: '/app/shared/inlineEditTables/charge_quotation/charge_quotation_history.html',
                                show: false
                            });
                            chargeModal.$promise.then(chargeModal.show)
                        } else if (response.data.responseCode == "ERR0700012") {
                            Notification.error($rootScope.nls["ERR0700012"]);
                        }
                    });
                }

                $scope.tableCheck = function() {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = [];
                    angular.forEach($scope.tableData, function(dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            $scope.tableData.splice(index, 1);
                        } else {
                            validResult = validateObj(dataObj, index, "row");
                            finalData.push(dataObj);
                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        $scope.callFun();
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.fetchCharge = function() {
                    if ($scope.dependServiceData != null) {
                        $rootScope.mainpreloder = true;

                        if ($scope.dependServiceData.serviceMaster != undefined && $scope.dependServiceData.serviceMaster != null &&
                            $scope.dependServiceData.serviceMaster.id != null) {
                            if ($scope.dependServiceData.origin != undefined && $scope.dependServiceData.origin != null &&
                                $scope.dependServiceData.origin.id != null) {
                                if ($scope.dependServiceData.destination != undefined &&
                                    $scope.dependServiceData.destination != null && $scope.dependServiceData.destination.id != null) {

                                    var carrierId = null;

                                    if ($scope.dependCarrierData.carrierMaster != undefined && $scope.dependCarrierData.carrierMaster.id != null) {
                                        carrierId = $scope.dependCarrierData.carrierMaster.id;
                                    }
                                    /*else {
                        			Notification.error($rootScope.nls["ERR2322"]);
                        		}*/

                                    var searchChargeData = {
                                        originId: $scope.dependServiceData.origin.id,
                                        destinationId: $scope.dependServiceData.destination.id,
                                        carrierId: carrierId,
                                        hazardous: $scope.dependServiceData.hazardous,
                                        chargable: $scope.dependServiceData.chargebleWeight,
                                        transportMode: $scope.dependServiceData.serviceMaster.transportMode,
                                        whoRouted: $scope.dependData.whoRouted ? 'Agent' : 'Self'
                                    };

                                    appMetaFactory.fetchQuotationCharge.findByOD(searchChargeData).$promise.then(function(data) {
                                            if (data.responseCode == "ERR0") {
                                                console.log("data", data);
                                                if (data.responseObject != undefined && data.responseObject != null && data.responseObject.length != undefined && data.responseObject.length > 0) {

                                                    var tmpIndex = $scope.tableData.length - 1;
                                                    if (isEmptyRow($scope.tableData[tmpIndex])) {
                                                        $scope.tableData.splice(tmpIndex, 1);
                                                        delete $scope.tableData[tmpIndex]
                                                    }

                                                    $scope.tmpChargeODMap = new Map();
                                                    $scope.tmpChargeFRMap = new Map();
                                                    $scope.tmpChargeFUMap = new Map();
                                                    $scope.tmpChargeSEMap = new Map();
                                                    $scope.tmpRateRequestChargeMap = new Map();
                                                    $scope.tmpValueAddedChargeMap = new Map();

                                                    if ($scope.tableData != undefined && $scope.tableData != null && $scope.tableData.length != 0) {
                                                        for (var i = 0; i < $scope.tableData.length; i++) {
                                                            if ($scope.tableData[i].pricingPortPairId != null && $scope.tableData[i].pricingCarrierId == null) {
                                                                $scope.tmpChargeODMap.put($scope.tableData[i].pricingPortPairId, $scope.tableData[i]);
                                                            } else if ($scope.tableData[i].pricingCarrierId != null && $scope.tableData[i].pricingPortPairId == null) {
                                                                if ($scope.tableData[i].surchargeType == undefined || $scope.tableData[i].surchargeType == null) {
                                                                    $scope.tmpChargeFRMap.put($scope.tableData[i].pricingPortPairId, $scope.tableData[i]);
                                                                } else if ($scope.tableData[i].surchargeType == 'Fuel') {
                                                                    $scope.tmpChargeFUMap.put($scope.tableData[i].pricingPortPairId, $scope.tableData[i]);
                                                                } else if ($scope.tableData[i].surchargeType == 'Security') {
                                                                    $scope.tmpChargeSEMap.put($scope.tableData[i].pricingPortPairId, $scope.tableData[i]);
                                                                }
                                                            }

                                                            if ($scope.tableData[i].agentRateRequestChargeId != undefined &&
                                                                $scope.tableData[i].agentRateRequestChargeId != null) {
                                                                $scope.tmpRateRequestChargeMap.put($scope.tableData[i].agentRateRequestChargeId, $scope.tableData[i]);
                                                            }

                                                            if ($scope.tableData[i].valueAddedId != undefined &&
                                                                $scope.tableData[i].valueAddedId != null) {
                                                                $scope.tmpValueAddedChargeMap.put($scope.tableData[i].valueAddedId, $scope.tableData[i]);
                                                            }
                                                        }
                                                    }

                                                    var priceAdded = false;

                                                    for (var i = 0; i < data.responseObject.length; i++) {

                                                        if (data.responseObject[i].pricingPortPairId != null && data.responseObject[i].pricingCarrierId == null) {
                                                            if ($scope.tmpChargeODMap.get(data.responseObject[i].pricingPortPairId) != null) {
                                                                continue;
                                                            }
                                                        } else if (data.responseObject[i].pricingCarrierId != null && data.responseObject[i].pricingPortPairId == null) {
                                                            if (data.responseObject[i].surchargeType == undefined || data.responseObject[i].surchargeType == null) {
                                                                if ($scope.tmpChargeFRMap.get(data.responseObject[i].pricingPortPairId) != null) {
                                                                    continue;;
                                                                }
                                                            } else if (data.responseObject[i].surchargeType == 'Fuel') {
                                                                if ($scope.tmpChargeFUMap.get(data.responseObject[i].pricingPortPairId) != null) {
                                                                    continue;;
                                                                }
                                                            } else if (data.responseObject[i].surchargeType == 'Security') {
                                                                if ($scope.tmpChargeSEMap.get(data.responseObject[i].pricingPortPairId) != null) {
                                                                    continue;;
                                                                }
                                                            }
                                                        }

                                                        /* Rate Request Charge Mapped to Pricing Charge -- Start Here*/
                                                        var tmpkey = $scope.tmpRateRequestChargeMap.allKeys();
                                                        var isRateRequest = false;
                                                        for (var k = 0; k < tmpkey.length; k++) {
                                                            if ($scope.tmpRateRequestChargeMap.get(tmpkey[k]) != undefined &&
                                                                $scope.tmpRateRequestChargeMap.get(tmpkey[k]) != null &&
                                                                $scope.tmpRateRequestChargeMap.get(tmpkey[k]).chargeMaster != undefined &&
                                                                $scope.tmpRateRequestChargeMap.get(tmpkey[k]).chargeMaster != null) {

                                                                if ($scope.tmpRateRequestChargeMap.get(tmpkey[k]).chargeMaster.id == data.responseObject[i].chargeMaster.id) {
                                                                    $scope.tmpRateRequestChargeMap.get(tmpkey[k]).sellRateAmountPerUnit = data.responseObject[i].sellRateAmountPerUnit;
                                                                    $scope.tmpRateRequestChargeMap.get(tmpkey[k]).sellRateMinAmount = data.responseObject[i].sellRateMinAmount;

                                                                    $scope.tmpRateRequestChargeMap.get(tmpkey[k]).pricingPortPairId = data.responseObject[i].pricingPortPairId;
                                                                    $scope.tmpRateRequestChargeMap.get(tmpkey[k]).pricingCarrierId = data.responseObject[i].pricingCarrierId;
                                                                    $scope.tmpRateRequestChargeMap.get(tmpkey[k]).surchargeType = data.responseObject[i].surchargeType;

                                                                    isRateRequest = true;
                                                                    break;
                                                                }
                                                            }

                                                        }

                                                        if (isRateRequest) {
                                                            continue;
                                                        }
                                                        /* Rate Request Charge Mapped to Pricing Charge -- End Here*/


                                                        /* Value added Charge Mapped to Pricing Charge -- Start Here*/
                                                        var tmpVatkey = $scope.tmpValueAddedChargeMap.allKeys();
                                                        var isValueAdded = false;
                                                        for (var k = 0; k < tmpVatkey.length; k++) {
                                                            if ($scope.tmpValueAddedChargeMap.get(tmpVatkey[k]) != undefined &&
                                                                $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]) != null &&
                                                                $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).chargeMaster != undefined &&
                                                                $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).chargeMaster != null) {

                                                                if ($scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).chargeMaster.id == data.responseObject[i].chargeMaster.id) {
                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).sellRateAmountPerUnit = data.responseObject[i].sellRateAmountPerUnit;
                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).sellRateMinAmount = data.responseObject[i].sellRateMinAmount;

                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).buyRateCostPerUnit = data.responseObject[i].buyRateCostPerUnit;
                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).buyRateMinCost = data.responseObject[i].buyRateMinCost;

                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).pricingPortPairId = data.responseObject[i].pricingPortPairId;
                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).pricingCarrierId = data.responseObject[i].pricingCarrierId;
                                                                    $scope.tmpValueAddedChargeMap.get(tmpVatkey[k]).surchargeType = data.responseObject[i].surchargeType;

                                                                    isValueAdded = true;
                                                                    break;
                                                                }
                                                            }

                                                        }

                                                        if (isValueAdded) {
                                                            priceAdded = true;
                                                            continue;
                                                        }
                                                        /* Value added Charge Mapped to Pricing Charge -- End Here*/

                                                        if (data.responseObject[i].unitMaster == undefined ||
                                                            data.responseObject[i].unitMaster == null ||
                                                            data.responseObject[i].unitMaster.id == undefined ||
                                                            data.responseObject[i].unitMaster.id == null) {

                                                            $scope.getUnitMaster(data.responseObject[i]);
                                                        } else {

                                                            if (data.responseObject[i].unitMaster.unitCode === 'KG') {
                                                                data.responseObject[i].actualchargeable = 'CHARGEABLE';
                                                            }
                                                            $scope.isSurchargeAvailable(data.responseObject[i]);
                                                        }

                                                        $scope.getRoe(data.responseObject[i]);

                                                        if ($scope.tableData == undefined || $scope.tableData == null) {
                                                            $scope.tableData = [];
                                                        }

                                                        priceAdded = true;
                                                        $scope.tableData.push(data.responseObject[i]);
                                                    }

                                                    if (priceAdded == false) {
                                                        Notification.error($rootScope.nls["ERR22170"]);
                                                    } else {
                                                        Notification.success($rootScope.nls["ERR22171"]);
                                                        if ($scope.dependServiceData.hazardous) {
                                                            Notification.warning($rootScope.nls["ERR4151"]);
                                                        }
                                                    }

                                                } else {
                                                    Notification.error($rootScope.nls["ERR22167"]);
                                                }
                                            } else {
                                                console.log("added Failed " + data.responseDescription)
                                            }
                                        },
                                        function(error) {
                                            console.log("Error", error);
                                        });


                                } else {
                                    Notification.error($rootScope.nls["ERR2323"]);
                                }
                            } else {
                                Notification.error($rootScope.nls["ERR2324"]);
                            }
                        } else {
                            Notification.error($rootScope.nls["ERR2325"]);
                        }




                        /*YesNo hazardous = YesNo.No;
                        Long originId;
                        Long destinationId;
                        Long carrierId;*/
                    }
                    $rootScope.mainpreloder = false;
                }
                $scope.addRow = function() {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function(dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData.push({});
                        $scope.secondArr.push({ "errRow": false });
                        $scope.indexFocus = null;
                        $scope.secondArr[$scope.tableData.length - 1].chargeFocus = true;
                    }
                };

                var myOtherModal = $modal({ scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true });
                var errorOnRowIndex = null;
                $scope.errorShow = function(errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                //error validation rules
                var validateObj = function(dataObj, index, type) {
                    var errorArr = [];
                    if (type == "row") {
                        if ($scope.secondArr[index] == undefined || $scope.secondArr[index] == null) {
                            $scope.secondArr[index] = {};
                        }
                        $scope.secondArr[index].errTextArr = [];
                    }
                    if (dataObj.chargeMaster == null || dataObj.chargeMaster.id == undefined || dataObj.chargeMaster.id == null || dataObj.chargeMaster.id == "") {
                        if (type == "row") {
                            $scope.secondArr[index].chargeCode = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22072"]);
                        }
                        errorArr.push(0);
                    } else if (dataObj.chargeMaster.status != null) {
                        if (ValidateUtil.isStatusBlocked(dataObj.chargeMaster.status)) {
                            dataObj.chargeMaster = null;
                            dataObj.chargeName = "";

                            if (type == "row") {
                                $scope.secondArr[index].chargeCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22073"]);
                            }
                            errorArr.push(0);
                        } else if (ValidateUtil.isStatusHidden(dataObj.chargeMaster.status)) {
                            dataObj.chargeMaster = null;

                            if (type == "row") {
                                $scope.secondArr[index].chargeCode = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22074"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].chargeCode = false;
                            }
                            errorArr.push(1);
                        }
                    }

                    if (dataObj.chargeName == undefined || dataObj.chargeName == null || dataObj.chargeName == "" || dataObj.chargeName.trim().length < 1) {
                        if (type == 'row') {
                            $scope.secondArr[index].chargeName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22075"]);
                        }
                        errorArr.push(0);
                    } else {
                        $scope.secondArr[index].chargeName = false;
                    }

                    if (dataObj.currencyMaster == null || dataObj.currencyMaster.id == undefined || dataObj.currencyMaster.id == null || dataObj.currencyMaster.id == "") {

                        if (type == "row") {
                            $scope.secondArr[index].currency = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22077"]);
                        }
                        errorArr.push(0);
                    } else if (dataObj.currencyMaster.status != null) {

                        if (ValidateUtil.isStatusBlocked(dataObj.currencyMaster.status)) {
                            dataObj.currencyMaster = null;
                            if (type == "row") {
                                $scope.secondArr[index].currency = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22078"]);
                            }
                            errorArr.push(0);
                        } else if (ValidateUtil.isStatusHidden(dataObj.currencyMaster.status)) {

                            dataObj.currencyMaster = null;

                            if (type == "row") {
                                $scope.secondArr[index].currency = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22079"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].currency = false;
                            }
                            errorArr.push(1);
                        }
                    }

                    if (typeof dataObj.roe == "string") {
                        dataObj.roe = parseFloat(dataObj.roe);
                    }
                    if (isNaN(dataObj.roe) || dataObj.roe == null || dataObj.roe == undefined || dataObj.roe == "") {
                        $scope.secondArr[index].roe = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR38021"]);
                        errorArr.push(0);
                    } else if (dataObj.roe <= 0) {
                        $scope.secondArr[index].roe = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR38026"]);
                        errorArr.push(0);
                    } else if (dataObj.roe == 1) {
                        if (dataObj.currencyMaster.id != $scope.dependData.localCurrency.id) {
                            $scope.secondArr[index].roe = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22168"]);
                            errorArr.push(0);
                        }
                    } else {
                        $scope.secondArr[index].roe = false;
                        errorArr.push(1);
                    }

                    if (dataObj.unitMaster == null || dataObj.unitMaster.id == undefined ||
                        dataObj.unitMaster.id == null ||
                        dataObj.unitMaster.id == "") {

                        if (type == "row") {
                            $scope.secondArr[index].unit = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22080"]);
                        }
                        errorArr.push(0);
                    } else if (dataObj.unitMaster.status != null) {

                        if (ValidateUtil.isStatusBlocked(dataObj.unitMaster.status)) {

                            dataObj.unitMaster = null;

                            if (type == "row") {
                                $scope.secondArr[index].unit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22081"]);
                            }
                            errorArr.push(0);
                        } else if (ValidateUtil.isStatusHidden(dataObj.unitMaster.status)) {

                            dataObj.unitMaster = null;

                            if (type == "row") {
                                $scope.secondArr[index].unit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22082"]);
                            }
                            errorArr.push(0);

                        } else {
                            if (type == 'row') {
                                $scope.secondArr[index].unit = false;
                            }
                            errorArr.push(1);
                        }

                    }
                    if (dataObj.sellRateAmountPerUnit == undefined || dataObj.sellRateAmountPerUnit == null ||
                        dataObj.sellRateAmountPerUnit == "") {
                        if (type == 'row') {
                            $scope.secondArr[index].sellRateAmountPerUnit = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22132"]);
                        }
                        errorArr.push(0);


                    } else if (dataObj.sellRateAmountPerUnit != null) {
                        if (dataObj.sellRateAmountPerUnit <= 0) {
                            if (type == 'row') {
                                $scope.secondArr[index].sellRateAmountPerUnit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22117"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (isNaN(parseFloat(dataObj.sellRateAmountPerUnit))) {
                                if (type == 'row') {
                                    $scope.secondArr[index].sellRateAmountPerUnit = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22118"]);
                                }
                                errorArr.push(0);

                            } else if (!CommonValidationService.checkRegExp($rootScope.amtRegularExp(dataObj.currencyMaster), dataObj.sellRateAmountPerUnit)) {
                                if (type == 'row') {
                                    $scope.secondArr[index].sellRateAmountPerUnit = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22118"]);
                                }
                                errorArr.push(0);
                            } else if (dataObj.sellRateAmountPerUnit < 0 || dataObj.sellRateAmountPerUnit > 99999999999.999) {
                                if (type == 'row') {
                                    $scope.secondArr[index].sellRateAmountPerUnit = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22118"]);
                                }
                                errorArr.push(0);

                            } else {
                                if (type == 'row') {
                                    $scope.secondArr[index].sellRateAmountPerUnit = false;
                                }
                                errorArr.push(1);
                            }


                        }
                    }


                    if (!isNaN(dataObj.sellRateMinAmount) && dataObj.sellRateMinAmount != null && dataObj.sellRateMinAmount != undefined && dataObj.sellRateMinAmount != "") {

                        if (dataObj.sellRateMinAmount != null) {
                            if (dataObj.sellRateMinAmount <= 0) {
                                if (type == 'row') {
                                    $scope.secondArr[index].sellRateMinAmount = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22119"]);
                                }
                                errorArr.push(0);
                            } else {



                                if (isNaN(parseFloat(dataObj.sellRateMinAmount))) {
                                    if (type == 'row') {
                                        $scope.secondArr[index].sellRateMinAmount = true;
                                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22120"]);
                                    }
                                    errorArr.push(0);
                                } else if (!CommonValidationService.checkRegExp($rootScope.amtRegularExp(dataObj.currencyMaster), dataObj.sellRateMinAmount)) {
                                    if (type == 'row') {
                                        $scope.secondArr[index].sellRateMinAmount = true;
                                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22120"]);
                                    }
                                    errorArr.push(0);
                                } else if (dataObj.sellRateMinAmount < 0 || dataObj.sellRateMinAmount > 99999999999.999) {
                                    if (type == 'row') {
                                        $scope.secondArr[index].sellRateMinAmount = true;
                                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22120"]);
                                    }
                                    errorArr.push(0);
                                } else {
                                    if (type == 'row') {
                                        $scope.secondArr[index].sellRateMinAmount = false;
                                    }
                                    errorArr.push(1);
                                }
                            }
                        }

                    }

                    if (!isNaN(dataObj.buyRateCostPerUnit) && dataObj.buyRateCostPerUnit != null && dataObj.buyRateCostPerUnit != undefined && dataObj.buyRateCostPerUnit != "") {
                        if (dataObj.buyRateCostPerUnit <= 0) {
                            if (type == 'row') {
                                $scope.secondArr[index].buyRateCostPerUnit = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22121"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (isNaN(parseFloat(dataObj.buyRateCostPerUnit))) {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateCostPerUnit = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22122"]);
                                }
                                errorArr.push(0);
                            } else if (!CommonValidationService.checkRegExp($rootScope.amtRegularExp(dataObj.currencyMaster), dataObj.buyRateCostPerUnit)) {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateCostPerUnit = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22122"]);
                                }
                                errorArr.push(0);
                            } else if (dataObj.buyRateCostPerUnit < 0 || dataObj.buyRateCostPerUnit > 99999999999.999) {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateCostPerUnit = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22122"]);
                                }
                                errorArr.push(0);
                            } else {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateCostPerUnit = false;
                                }
                                errorArr.push(1);
                            }

                        }
                    }

                    if (!isNaN(dataObj.buyRateMinCost) && dataObj.buyRateMinCost != undefined && dataObj.buyRateMinCost != null && dataObj.buyRateMinCost != "") {
                        if (dataObj.buyRateMinCost <= 0) {
                            if (type == 'row') {
                                $scope.secondArr[index].buyRateMinCost = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22123"]);
                            }
                            errorArr.push(0);
                        } else {
                            if (isNaN(parseFloat(dataObj.buyRateMinCost))) {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateMinCost = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22124"]);
                                }
                                errorArr.push(0);
                            } else if (!CommonValidationService.checkRegExp($rootScope.amtRegularExp(dataObj.currencyMaster), dataObj.buyRateMinCost)) {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateMinCost = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22124"]);
                                }
                                errorArr.push(0);
                            } else if (dataObj.buyRateMinCost < 0 || dataObj.buyRateMinCost > 99999999999.999) {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateMinCost = true;
                                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22124"]);
                                }
                                errorArr.push(0);
                            } else {
                                if (type == 'row') {
                                    $scope.secondArr[index].buyRateMinCost = false;
                                }
                                errorArr.push(1);
                            }
                        }
                    }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }
                };

                $scope.groupPopup = $modal({ scope: $scope, templateUrl: 'isGroupName.html', backdrop: 'static', show: false, keyboard: true });
                $scope.showGroupPopup = function(index) {
                    $scope.groupPopupIndex = index;
                    $scope.groupPopup.$promise.then($scope.groupPopup.show);
                    $scope.groupName = $scope.tableData[$scope.groupPopupIndex].groupName;
                };

                $scope.save = function(groupName) {
                    if ($scope.tableData != undefined && $scope.tableData != null) {
                        $scope.tableData[$scope.groupPopupIndex].groupName = groupName;

                        if ($scope.tableData[$scope.groupPopupIndex].groupName == undefined ||
                            $scope.tableData[$scope.groupPopupIndex].groupName == null ||
                            $scope.tableData[$scope.groupPopupIndex].groupName == '') {
                            $scope.tableData[$scope.groupPopupIndex].isGroup = false;
                        } else {
                            $scope.tableData[$scope.groupPopupIndex].isGroup = true;
                        }

                    }
                    $scope.groupPopup.$promise.then($scope.groupPopup.hide);
                }

                $scope.cancel = function() {
                    if ($scope.tableData != undefined && $scope.tableData != null) {
                        if ($scope.tableData[$scope.groupPopupIndex].groupName == undefined ||
                            $scope.tableData[$scope.groupPopupIndex].groupName == null ||
                            $scope.tableData[$scope.groupPopupIndex].groupName == '') {
                            $scope.tableData[$scope.groupPopupIndex].isGroup = false;
                        } else {
                            $scope.tableData[$scope.groupPopupIndex].isGroup = true;
                        }
                    }
                    $scope.groupPopup.$promise.then($scope.groupPopup.hide);
                }

                $scope.localSearch = function(object) {
                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.chargeMasterList = data.responseObject.searchResult;
                                return $scope.chargeMasterList;
                            }
                        },
                        function(errResponse) {
                            console.error('Error while fetching Charge');
                        }
                    );

                }

                $scope.getRoe = function(dataObj) {
                    var sellRate = null;
                    if (dataObj.currencyMaster != null && dataObj.currencyMaster != undefined && dataObj.currencyMaster != '') {
                        if ($rootScope.baseCurrenyRate[dataObj.currencyMaster.id] != null && $scope.dependData.localCurrency.id != dataObj.currencyMaster.id) {
                            sellRate = $rootScope.baseCurrenyRate[dataObj.currencyMaster.id].csell;
                        } else if (dataObj.currencyMaster.id == $scope.dependData.localCurrency.id) {
                            sellRate = 1;
                        }
                    }
                    dataObj.roe = sellRate;
                }
                $scope.selectedCurrency = function(focus, index, dataObj) {
                    $scope.getRoe(dataObj);
                    $scope.moveToNextField(focus, index)
                }
                $scope.localCurrencySearch = function(object) {

                    console.log("ajaxCurrencyMasterEvent is called ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;


                    return CurrencyMasterSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.currencyMasterList = data.responseObject.searchResult;
                                return $scope.currencyMasterList;

                            }
                        },
                        function(errResponse) {
                            console.error('Error while fetching Currency');
                        }
                    );

                }

                $scope.localUnitSearch = function(object) {
                    console.log("ajaxUnitMasterEvent is called ", object);

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                    return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                            if (data.responseCode == "ERR0") {
                                $scope.totalRecord = data.responseObject.totalRecord;
                                $scope.unitMasterList = data.responseObject.searchResult;
                                return $scope.unitMasterList;
                            }
                        },
                        function(errResponse) {
                            console.error('Error while fetching Charge');
                        }
                    );


                }

                $scope.selectedUnitMaster = function(focus, index) {
                    if ($scope.tableData[index].unitMaster.unitType != 'Unit') {
                        $scope.tableData[index].actualchargeable = null;
                    }
                    $scope.moveToNextField(focus, index)
                }



                $(document).on('keydown', function(e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function() {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeFocus = false;
                    }
                    $timeout(function() {
                        $scope.secondArr[errorOnRowIndex].chargeFocus = true;
                        errorOnRowIndex = null;
                    }, 10)
                });





                $scope.removeObj = function(obj, index) {
                    if (obj.valueAddedId != undefined || obj.valueAddedId != null) {
                        Notification.error($rootScope.nls["ERR22166"]);
                    } else {
                        $scope.tableData.splice(index, 1);
                        $scope.secondArr.splice(index, 1);
                        $scope.indexFocus = null;
                    }

                }

                $scope.dropStyle = { "overflow-x": "auto", "overflow-y": "hidden" };
                $scope.openEvent = function(param) {
                    console.log("value", param);
                    if (param == true) {
                        $scope.dropStyle = { "overflow-x": "hidden" };
                    } else {
                        $scope.dropStyle = { "overflow-x": "auto", "overflow-y": "hidden" };
                    }
                }

                $scope.chargeRender = function(item) {
                    return {
                        label: item.chargeCode,
                        item: item
                    }
                }
                $scope.currencyRender = function(item) {
                    return {
                        label: item.currencyCode,
                        item: item
                    }
                }
                $scope.unitRender = function(item) {
                    return {
                        label: item.unitCode,
                        item: item
                    }
                }


                $scope.selectChargeQuotation = function(charge, index) {
                    charge.currencyMaster = null;
                    charge.roe = null;
                    charge.unitMaster = null;

                    if (charge.chargeMaster != undefined && charge.chargeMaster != null) {
                        charge.chargeName = charge.chargeMaster.chargeName;

                        if (charge.chargeMaster.chargeType == 'Origin') {
                            if ($scope.dependServiceData.origin != undefined && $scope.dependServiceData.origin != null && $scope.dependServiceData.origin.id != null) {
                                if ($scope.dependServiceData.origin.portGroupMaster != undefined && $scope.dependServiceData.origin.portGroupMaster != null) {
                                    charge.currencyMaster = $scope.dependServiceData.origin.portGroupMaster.countryMaster.currencyMaster;
                                }
                            }
                        } else if (charge.chargeMaster.chargeType == 'Destination') {
                            if ($scope.dependServiceData.destination != undefined && $scope.dependServiceData.destination != null && $scope.dependServiceData.destination.id != null) {
                                if ($scope.dependServiceData.destination.portGroupMaster != undefined && $scope.dependServiceData.destination.portGroupMaster != null) {
                                    charge.currencyMaster = $scope.dependServiceData.destination.portGroupMaster.countryMaster.currencyMaster;
                                }
                            }
                        } else if (charge.chargeMaster.chargeType == 'Freight') {
                            if ($scope.dependServiceData.origin != undefined && $scope.dependServiceData.origin != null && $scope.dependServiceData.origin.id != null) {
                                if ($scope.dependServiceData.origin.portGroupMaster != undefined && $scope.dependServiceData.origin.portGroupMaster != null) {
                                    charge.currencyMaster = $scope.dependServiceData.origin.portGroupMaster.countryMaster.currencyMaster;
                                }
                            }
                        } else {
                            charge.currencyMaster = $scope.dependData.localCurrency;
                        }

                        $scope.getRoe(charge);



                        if (charge.chargeMaster.calculationType != undefined && charge.chargeMaster.calculationType != null) {
                            var unitCode;

                            if (charge.chargeMaster.calculationType == 'Percentage') {
                                unitCode = 'PER';
                            } else if (charge.chargeMaster.calculationType == 'Shipment') {
                                unitCode = 'SHPT';
                            } else if (charge.chargeMaster.calculationType == 'Unit') {
                                unitCode = 'KG';
                            } else if (charge.chargeMaster.calculationType == 'Document') {
                                unitCode = 'DOC';
                            } else {

                            }

                            UnitMasterGetByCode.get({
                                unitCode: unitCode
                            }).$promise.then(function(resObj) {
                                if (resObj.responseCode == "ERR0") {
                                    charge.unitMaster = resObj.responseObject;
                                    if (charge.currencyMaster == undefined || charge.currencyMaster == null || charge.currencyMaster == "") {
                                        $scope.moveToNextField('chargeCurrency', index);
                                    } else if (charge.unitMaster == null) {
                                        $scope.moveToNextField('unitMaster', index);
                                    } else {
                                        if (charge.unitMaster.unitType != 'Unit') {
                                            charge.actualchargeable = null;
                                        }
                                        $scope.moveToNextField('numberOfUnit', index);
                                    }
                                } else {
                                    $scope.moveToNextField('unitMaster', index);
                                }
                            }, function(error) {
                                console.log("unit Get Failed : " + error)
                                $scope.moveToNextField('unitMaster', index);
                            });
                        }

                    }

                    if (charge.actualchargeable == undefined || charge.actualchargeable == null || charge.actualchargeable == "") {
                        charge.actualchargeable = 'CHARGEABLE';
                    }

                    console.log("Selected Unit Master ", charge.unitMaster);
                    $scope.isSurchargeAvailable(charge);

                };


                $scope.clearChargeName = function(charge) {
                    if (charge.chargeMaster == undefined || charge.chargeMaster == null || charge.chargeMasater == "") {
                        charge.chargeName = "";
                    }
                }

                $scope.getUnitMaster = function(charge) {
                    if (charge.chargeMaster.calculationType != undefined && charge.chargeMaster.calculationType != null) {
                        var unitCode;

                        if (charge.chargeMaster.calculationType == 'Percentage') {
                            unitCode = 'PER';
                        } else if (charge.chargeMaster.calculationType == 'Shipment') {
                            unitCode = 'SHPT';
                        } else if (charge.chargeMaster.calculationType == 'Unit') {
                            unitCode = 'KG';
                        } else if (charge.chargeMaster.calculationType == 'Document') {
                            unitCode = 'DOC';
                        } else {

                        }

                        UnitMasterGetByCode.get({
                            unitCode: unitCode
                        }).$promise.then(function(resObj) {
                            if (resObj.responseCode == "ERR0") {
                                charge.unitMaster = resObj.responseObject;
                                if (charge.currencyMaster == undefined || charge.currencyMaster == null || charge.currencyMaster == "") {
                                    $scope.moveToNextField('chargeCurrency', index);
                                } else if (charge.unitMaster == null) {
                                    $scope.moveToNextField('unitMaster', index);
                                } else {
                                    if (charge.unitMaster.unitType != 'Unit') {
                                        charge.actualchargeable = null;
                                    }
                                    $scope.moveToNextField('numberOfUnit', index);
                                }
                            } else {
                                $scope.moveToNextField('unitMaster', index);
                            }
                        }, function(error) {
                            console.log("unit Get Failed : " + error)
                            $scope.moveToNextField('unitMaster', index);
                        });
                    }

                    if (charge.actualchargeable == undefined || charge.actualchargeable == null || charge.actualchargeable == "") {
                        charge.actualchargeable = 'CHARGEABLE';
                    }

                    console.log("Selected Unit Master ", charge.unitMaster);
                    $scope.isSurchargeAvailable(charge);
                }

                $scope.currencyFormat = function(currency, amount) {
                    return $rootScope.currencyFormat(currency, amount);
                }
                $scope.moveToNextField = function(nextIdValue, index) {
                    $rootScope.navigateToNextField($scope.indexKey + nextIdValue + index);
                }

                function initTable() {
                    if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                        $scope.tableData = [];
                        $scope.tableData.push({});
                    }

                    angular.forEach($scope.tableData, function(item) {
                        $scope.secondArr.push({ "errRow": false });
                    });
                }

                $scope.copy = $rootScope.copy;
                $scope.copyObj = function(copyObj) {
                    var tmpObj = angular.copy(copyObj);
                    tmpObj.id = null;
                    tmpObj.systemTrack = null;
                    tmpObj.versionLock = 0;
                    if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                        $scope.tableData = [];
                    } else {
                        var tmpIndex = $scope.tableData.length - 1;
                        if (isEmptyRow($scope.tableData[tmpIndex])) {
                            $scope.tableData.splice(tmpIndex, 1);
                            delete $scope.tableData[tmpIndex]

                            if ($scope.tableData == undefined || $scope.tableData == null || $scope.tableData.length == 0) {
                                $scope.tableData = [];
                            }
                        }
                    }
                    $scope.tableData.push(tmpObj);
                }

                initTable();
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;

            }
        }

    }
]);