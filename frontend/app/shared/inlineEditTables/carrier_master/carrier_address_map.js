/**
 * Created by saravanan on 23/6/16.
 */
app.directive('carrierAddressMap',function($timeout,$compile,$rootScope,ValidateUtil,CountryWiseLocation,PartiesList){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/carrier_master/carrier_address_map.html',
        scope:{
            tableData:"=",
            showErrors:"=",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"="

        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;
                        } else {
                            validResult = validateObj(dataObj, index, "row");
                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };

                //temp array
                $scope.secondArr = [];

                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row
                $scope.addRow = function () {
                	
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    if (fineArr.indexOf(0) < 0) {
                    	if ($scope.tableData == undefined || $scope.tableData == null) {
                            $scope.tableData = [];
                        }
                        $scope.tableData.push({});
                        
                           $scope.secondArr.push({"errRow": false});
                    }
                    $rootScope.navigateToNextField('location'+($scope.tableData.length-1));
                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;

                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                $scope.ajaxLocationEvent = function (object) {
                    $scope.searchDto = {};
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    $scope.searchDto.keyword = object == null ? "" : object;
                    return CountryWiseLocation.fetch({"countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id}, $scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.locationList = data.responseObject.searchResult;
                                return $scope.locationList;
                                //$scope.showLocationList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Location');
                        }
                    );

                }


                $scope.selectLoctionMaster = function (nextId) {
                    $rootScope.navigateToNextField(nextId);
                }

                $scope.selectPartyName = function (nextId) {
                    $rootScope.navigateToNextField(nextId);
                }

                $scope.ajaxPartyEvent = function (object) {

                    $scope.searchDto = {};
                    $scope.searchDto.keyword = object == null ? "" : object;
                    $scope.searchDto.selectedPageNumber = 0;
                    $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                    return PartiesList.query($scope.searchDto).$promise.then(
                        function (data) {
                            if (data.responseCode == "ERR0") {
                                $scope.partyList = [];
                                $scope.partyList = data.responseObject.searchResult;
                                return $scope.partyList;
                                //$scope.showCarrierAgentList=true;
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching party');
                        }
                    );

                }

                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    $scope.secondArr[index] = {};
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.locationMaster == null || dataObj.locationMaster == undefined || dataObj.locationMaster.id == null) {
                        if (type == "row") {
                            $scope.secondArr[index].locationName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR1230"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }

                    else if (dataObj.locationMaster.status == 'Block') {
                        if (type == "row") {
                            $scope.secondArr[index].locationName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2233"]);
                        }
                        //return false;
                        errorArr.push(0);

                    }

                    else if (dataObj.locationMaster.status == 'Hide') {
                        console.log("Selected Location is Hidden.");
                        if (type == "row" || type == "change") {
                            $scope.secondArr[index].locationName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2234"]);
                        }
                        //return false;
                        errorArr.push(0);

                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].locationName = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }


                    if (dataObj.partyMaster == null || dataObj.partyMaster == undefined || dataObj.partyMaster.id == null) {
                        if (type == "row") {
                            $scope.secondArr[index].partyName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR1237"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }

                    else if (dataObj.partyMaster.status == 'Block') {
                        if (type == "row") {
                            $scope.secondArr[index].partyName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR1239"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }

                    else if (dataObj.partyMaster.status == 'Hide') {
                        if (type == "row") {
                            $scope.secondArr[index].partyName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR1238"]);
                        }
                        //return false;
                        errorArr.push(0);
                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].partyName = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }


                    var regexp = new RegExp("^[0-9A-Za-z ]{0,30}$");
                    if (dataObj.accountNumber == undefined
                        || dataObj.accountNumber == null
                        || dataObj.accountNumber == "") {

                        if (type == "row") {
                            $scope.secondArr[index].accountNumber = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR1236"]);
                        }
                        //return false;
                        errorArr.push(0);
                    }

                    else if (!regexp
                            .test(dataObj.accountNumber)) {
                        if (type == "row") {
                            $scope.secondArr[index].accountNumber = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR1240"]);
                        }
                        //return false;
                        errorArr.push(0);
                    } else {
                        if (type == "row") {
                            $scope.secondArr[index].accountNumber = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }
                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                };
                $scope.locationRender = function (item) {
                    return {
                        label: item.locationName,
                        item: item
                    }
                };
                $scope.partyRender = function (item) {
                    return {
                        label: item.partyName,
                        item: item
                    }
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;

            },2);
        }
    }
});