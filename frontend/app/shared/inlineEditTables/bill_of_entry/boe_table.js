
app.directive('boeTable',['$timeout', '$rootScope', 'ValidateUtil', 'ReferenceTypeSearchByKeyword',
    'CommonValidationService', 'appConstant',
    function($timeout, $rootScope, ValidateUtil, ReferenceTypeSearchByKeyword,CommonValidationService, appConstant){
    return {
        restrict:'E',
        templateUrl :'app/shared/inlineEditTables/bill_of_entry/boe_popup.html',
        scope:{
        	indexKey:"=",
            tableData:"=",
            serviceObj:"=",
            showErrors:"=",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"="

        },
        controller : function($scope,$rootScope,$popover,$modal){
        	
        	$scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
            $scope.directiveDatePickerOpts.widgetParent = "body";
            
            $scope.firstFocus=true;
            
            $timeout(function() {
            	$scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                $scope.tableisChecked = true;
                $scope.$watch('tableData', function (newValue, oldValue) {
                    if ($scope.tableisChecked != undefined && $scope.tableisChecked) {
                        initTable();
                    }
                });
                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }
                
                
              //boe note button
                var noteBoeModal;
                $scope.addnoteboemodel = function(dataObject) {
                	$scope.noteObject = dataObject;
                 noteBoeModal= $modal({
                     scope: $scope,
                     templateUrl: 'app/components/crm/new-shipment/dialog/shipment_boe_notes_popup.html',
                     show: false
                 });
                 
                 noteBoeModal.$promise.then(noteBoeModal.show)
                };

                $scope.tableCheck = function () {
                    $scope.tableisChecked = false;
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;
                        } else {
                            validResult = validateObj(dataObj, index, "row");
                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };

                //temp array
                $scope.secondArr = [];

                function initTable() {
                	
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row
                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.flag = true;
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData.push({});
                        $rootScope.navigateToNextField($scope.indexKey + 'billOfEntry' + ($scope.tableData.length - 1))
                        $scope.secondArr.push({"errRow": false});

                    }
                };

                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;

                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };


                //error validation rules
                var validateObj = function (dataObj, index, type) {
                    $scope.secondArr[index] = {};

                    $scope.firstFocus=false;
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }

                    if (dataObj.billOfEntry == null || dataObj.billOfEntry == undefined || dataObj.billOfEntry == "") {
                        if (type == "row") {
                        	//ERR70100
                            $scope.secondArr[index].billOfEntry = true;
                            $scope.secondArr[index].errTextArr.push("Bill of entry is mandatory");
                        }
                        //return false;
                        errorArr.push(0);
                    }else if(isNaN(dataObj.billOfEntry)){
                    		$scope.secondArr[index].billOfEntry = true;
                            $scope.secondArr[index].errTextArr.push("Bill of entry should be numeric");
                            errorArr.push(0);
                    	}
                  else {
                        if (type == "row") {
                            $scope.secondArr[index].billOfEntry = false;

                        }
                        //return false;
                        errorArr.push(1);
                    }
                    
                    if (dataObj.decleartionNo != null && dataObj.decleartionNo != undefined && dataObj.decleartionNo != "" && isNaN(dataObj.decleartionNo)) {
                      	$scope.secondArr[index].decleartionNo = true; 
                          $scope.secondArr[index].errTextArr.push("DecleartionNo Should be numeric");
                          errorArr.push(0);
                  }else {
                      if (type == "row") {
                          $scope.secondArr[index].decleartionNo = false;

                      }
                      //return false;
                      errorArr.push(1);
                  }
                    
                    if (dataObj.boeValue != null && dataObj.boeValue != undefined && dataObj.boeValue != "" && isNaN(dataObj.boeValue)) {
                      	$scope.secondArr[index].boeValue = true; 
                          $scope.secondArr[index].errTextArr.push("Value Should be numeric");
                          errorArr.push(0);
                  }else {
                      if (type == "row") {
                          $scope.secondArr[index].boeValue = false;

                      }
                      //return false;
                      errorArr.push(1);
                  }

                    if (dataObj.amount != null && dataObj.amount != undefined && dataObj.amount != "" && isNaN(dataObj.amount)) {
                      	$scope.secondArr[index].amount = true; 
                          $scope.secondArr[index].errTextArr.push("Amount Should be numeric");
                          errorArr.push(0);
                  }else {
                      if (type == "row") {
                          $scope.secondArr[index].amount = false;

                      }
                      //return false;
                      errorArr.push(1);
                  }

                    if (dataObj.ackNo != null && dataObj.ackNo != undefined && dataObj.ackNo != "" && isNaN(dataObj.ackNo)) {
                      	$scope.secondArr[index].ackNo = true; 
                          $scope.secondArr[index].errTextArr.push("AckNo Should be numeric");
                          errorArr.push(0);
                  }else {
                      if (type == "row") {
                          $scope.secondArr[index].ackNo = false;

                      }
                      //return false;
                      errorArr.push(1);
                  }


                    if (errorArr.indexOf(0) < 0) {
                        return true
                    } else {
                        return false;
                    }

                };


                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex]!=undefined && $scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                    	if($scope.secondArr[errorOnRowIndex]!=undefined){
                    		$scope.secondArr[errorOnRowIndex].chargeCode = true;
                    		errorOnRowIndex = null;
                    	}
                    }, 10)
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                };


                $scope.referenceTypeRender = function (item) {
                    return {
                        label: item.referenceTypeName,
                        item: item
                    }
                };

                $scope.navigateToNextField = function (id) {
                    if (id != undefined && id != null && id != "")
                        document.getElementById(id).focus();
                };
                $scope.delete = $rootScope.delete;
                $scope.errorList = $rootScope.errorList;

            },2);
        }
    }
}]);