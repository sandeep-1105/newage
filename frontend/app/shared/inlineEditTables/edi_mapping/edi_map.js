/**
 * Created by saravanan on 23/6/16.
 */
app.directive('ediMapTable',['$timeout','$compile','$rootScope','ValidateUtil',
    function($timeout,$compile,$rootScope,ValidateUtil){
    return {
        restrict:'E',
        transclude: true,
        templateUrl :'app/shared/inlineEditTables/edi_mapping/edi_map.html',
        scope:{
            tableData:"=",
            fnRole:"&",
            addRole:"=",
            editRole:"=",
            deleteRole:"=",
            tableCheck:"="
        },
        controller : function($scope,$rootScope,$popover,$modal){
            $timeout(function() {
                $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
                $scope.flag = false;
                //show errors
                function isEmptyRow(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }

                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;

                        }
                    }
                    return isempty;
                }

                $scope.tableCheck = function () {
                    var fineArr = [];
                    $scope.secondArr = [];
                    var validResult;
                    var finalData = $scope.tableData;
                    var emptyIndex = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        $scope.secondArr[index] = {};
                        if (isEmptyRow(dataObj)) {
                            validResult = true;
                            finalData[index].isEmpty = true;

                        } else {
                            validResult = validateObj(dataObj, index, "row");

                        }
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                    });
                    console.log("******emptyIndex", emptyIndex, finalData);
                    angular.forEach(finalData, function (dataObj, index) {
                        if (dataObj.isEmpty) {
                            finalData.splice(index, 1);
                            $scope.secondArr.splice(index, 1);
                        }
                    });
                    console.log("***8$scope.tableData", finalData);
                    if (fineArr.indexOf(0) < 0) {
                        $scope.tableData = finalData;
                        return true;
                    } else {
                        return false;
                    }
                };


                //indexfocus initialization
                $scope.indexFocus = null;
                //temp array
                $scope.secondArr = [];
                function initTable() {
                    angular.forEach($scope.tableData, function (item) {
                        $scope.secondArr.push({"errRow": false});
                    });
                }

                initTable();


                //add row
                $scope.addRow = function () {
                    var fineArr = [];
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        var validResult = validateObj(dataObj, index, "row");
                        if (validResult) {
                            fineArr.push(1);
                            $scope.secondArr[index].errRow = false;
                        } else {
                            $scope.secondArr[index].errRow = true;
                            fineArr.push(0);
                        }
                        //console.log();
                    });

                    if (fineArr.indexOf(0) < 0) {
                    	if ($scope.tableData == undefined || $scope.tableData == null) {
                            $scope.tableData = [];
                        }
                        $scope.tableData.push({});
                        $rootScope.navigateToNextField('ediType' + ($scope.tableData.length - 1));
                        $scope.secondArr.push({"errRow": false});
                        $scope.indexFocus = null;
                    }

                };


                var myOtherModal = $modal({scope: $scope, templateUrl: 'errorPopUp.html', show: false, keyboard: true});
                var errorOnRowIndex = null;

                $scope.errorShow = function (errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    // Show when some event occurs (use $promise property to ensure the template has been loaded)
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                //error validation rules
                var validateObj = function (dataObj, index, type) {

                    $scope.secondArr[index] = {};
                    var errorArr = [];
                    if (type == "row") {
                        $scope.secondArr[index].errTextArr = [];
                    }
                    if (type == "row") {
                        if (dataObj.ediType == undefined
                            || dataObj.ediType == null
                            || dataObj.ediType == "") {

                            if (type == "row") {
                                $scope.secondArr[index].ediType = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2320"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].ediType = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }


                        if (dataObj.ediValue == undefined
                            || dataObj.ediValue == null
                            || dataObj.ediValue == "") {

                            if (type == "row") {
                                $scope.secondArr[index].ediValue = true;
                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2321"]);
                            }
                            //return false;
                            errorArr.push(0);
                        } else {
                            if (type == "row") {
                                $scope.secondArr[index].ediValue = false;

                            }
                            //return false;
                            errorArr.push(1);
                        }

                        if (errorArr.indexOf(0) < 0) {
                            return true
                        } else {
                            return false;
                        }
                    }

                };

                function changeLastStatus() {
                    angular.forEach($scope.tableData, function (dataObj, index) {
                        //var curObj = $scope.tableData.data[index];
                        var validResult = validateObj(dataObj, index, "row");
                        $scope.secondArr[index].errRow = false;
                    });
                }

                $scope.dirtyIndex = function (index) {
                    $scope.indexFocus = index;
                };

                $(document).on('keydown', function (e) {
                    if (e.keyCode == 27) {
                        if (myOtherModal) {
                            myOtherModal.$promise.then(myOtherModal.hide);
                        }
                    }
                });

                $scope.$on('modal.hide', function () {
                    if ($scope.secondArr[errorOnRowIndex].chargeCode) {
                        $scope.secondArr[errorOnRowIndex].chargeCode = false;
                    }
                    $timeout(function () {
                        $scope.secondArr[errorOnRowIndex].chargeCode = true;
                        errorOnRowIndex = null;
                    }, 10)
                });

                $scope.$watch('indexFocus', function (newvalue, oldValue) {
                    if (newvalue != null && oldValue != null && newvalue != oldValue) {
                        var focusErrArr = [];
                        angular.forEach($scope.tableData, function (dataObj, index) {
                            //var curObj = $scope.tableData.data[index];
                            var validResult = validateObj(dataObj, index, "row");
                            if (validResult) {
                                $scope.secondArr[index].errRow = false;
                                focusErrArr.push(1);
                            } else {
                                $scope.secondArr[index].errRow = true;
                                focusErrArr.push(0);

                            }
                            //console.log();
                        });
                   
                    }
                });

                $scope.removeObj = function (obj, index) {
                    $scope.tableData.splice(index, 1);
                    $scope.secondArr.splice(index, 1);
                    $scope.indexFocus = null;

                };
                $scope.delete = $rootScope.delete;
            },2);
        }
    }
}]);