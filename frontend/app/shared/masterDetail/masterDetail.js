/**
 * Created by saravanan on 5/4/16.
 */
app.directive("masterDetail",['$timeout',function($timeout){
    return {
        restrict: 'A',
        scope:{
            "masterTemp":"=",
        },
        link:function($scope,elem,attr){
        	$timeout(function(){
        		
        		var findProp = function(obj, prop, defval){
       	    	 console.log("**",obj,prop);
                    if (typeof defval == 'undefined') defval = null;
                    prop = prop.split('.');
                    for (var i = 0; i < prop.length; i++) {
                        console.log("value",obj[prop[i]],typeof obj[prop[i]],prop[i]);
                        if(obj[prop[i]] ==null && typeof obj[prop[i]] == 'undefined')
                            return defval;
                        obj = obj[prop[i]];
                    }
                    return obj;
                }
        		 	
        		console.log("data master",attr.masterTemp);
        		console.log("data master",$scope.masterTemp);
        		var tosMaster = findProp($scope.masterTemp,"tosMaster");
        		var tosName='-';
        		if(tosMaster){
        			tosName = findProp($scope.masterTemp,"tosMaster.tosName");
        		}
        		var period = findProp($scope.masterTemp,"period");
        		if(!period){
        			period = '-';
        		}
        		var commodityObj = findProp($scope.masterTemp,"commodity");
        		var hsName = '-';
        		if(commodityObj){
        			hsName = findProp($scope.masterTemp,"commodity.hsName");
        		}
        		var reason = findProp($scope.masterTemp,"reason");
        		if(!reason){
        			reason = '-';
        		}
        		var businessNotes = findProp($scope.masterTemp,"businessNotes");
        		if(!businessNotes){
        			businessNotes = '-';
        		}
        		
        		var newelem ="<tr class=\"tbl-child-row\">\
                    <td colspan=\"11\">\
                       <div class=\"row\">\
                           <ul class=\"list-inline tab-cell\">\
                               <li class=\"col-xs-3\">\
                                       <span >TOS</span>\
                                       <h6 >"+tosName+"</h6>\
                               </li>\
                               <li class=\"col-xs-3\">\
                                       <span >Frequency</span>\
                                       <h6 >"+period.frequencyName+"</h6>\
                               </li>\
                               <li class=\"col-xs-3\">\
                                   <span >Commodity</span>\
                                   <h6 >"+hsName+"</h6>\
                               </li>\
                               <li class=\"col-xs-3\">\
                                   <span >Reason</span>\
                                   <h6 >"+reason+"</h6>\
                               </li>\
                           </ul>\
                       </div>\
                       <div class=\"row\">\
                           <ul class=\"list-inline tab-cell\">\
                               <li class=\"col-xs-12\">\
                                   <span >Notes</span>\
                                   <h6 >"+businessNotes+"</h6>\
                               </li>\
                           </ul>\
                       </div>\
                   </td>\
               </tr>";
        		
        	     
        	     
                elem.bind('click',function(){
                    if(!elem.hasClass('active-master-tbl')){
                        //elem.siblings('tr').removeClass('active');
                        //elem.siblings('tr.tbl-child-row').fadeOut(200, function() {
                        //    $(this).remove();
                        //});
                        //elem.siblings('tr').find('td:last-child i').attr('class','fa fa-chevron-down');
                    	
                        elem.find('td:last-child i').attr('class','icon-uparrow');
                        var item =  $(newelem).hide().fadeIn(200);
                        elem.after(item);
                        elem.addClass('active-master-tbl');
                    }else{
                        elem.removeClass('active-master-tbl');
                        elem.next('.tbl-child-row').fadeOut(200, function() {
                            $(this).remove();
                        });
                        elem.find('td:last-child i').attr('class','icon-downarrow');
                    }

                })
        	},0);
            

        }
    }
}]);