/**
 * Created by saravanan on 4/4/16.
 */
app.directive("stopAnimation",['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            $timeout(function(){
                iElement.removeClass("no-animate");
                iElement.children().removeClass("no-animate");
            },1000);
            iElement.addClass("no-animate");
            iElement.children().addClass("no-animate");
        }
    }
}]);