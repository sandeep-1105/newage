/**
 * 
 */

 app.filter('commaLess', [
 	function() { 
 		return function(input) {
 			return (input)?input.toString().trim().replace(",",""):null;
 		};
 	}]).filter('myDropdownFilter',['$sce','$filter', function($sce,$filter) {
 		return function(label, query, item, options, element) {
 			if(item.status=='Block') {
 				var html = '<span class="text-danger">' + $filter('highlight')(label,query) + '</span> ';
 			}else{
 				var html = $filter('highlight')(label,query);
 			}
 			return $sce.trustAsHtml(html);
 		};
 	}]).filter('highlight',[ function() {
 		function escapeRegexp(queryToEscape) {
 			return ('' + queryToEscape).replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
 		}

 		return function(matchItem, query) {
 			return query && matchItem ? ('' + matchItem).replace(new RegExp(escapeRegexp(query), 'gi'), '<span class="highlight">$&</span>') : matchItem;
 		};
 	}]);