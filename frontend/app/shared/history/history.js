
app.directive("showHistory",[ function() {
	return {
			templateUrl : 'app/shared/history/history.html',
			replace: true,
			restrict : 'E',
			scope : {
				displayFlag : "=",
				createdBy : "=",
				createdOn : "=",
				updatedBy : "=",
				updatedOn : "="
			},
			controller:function ($scope,$rootScope) {
				$scope.close = $rootScope.close;
			}
	};


}]);



