
app.service('NavigationService', function() {
	var navigationData;
	var navigationLocation;
	var subData1;
	var subData2;
	var subData3;
	var subData4;
	var subData5; 
	var subData6; 
	
	var resultObj;
	var whichColumn;
	
	this.clear = function () {
		navigationData = null;
		navigationLocation = null;
		subData1 = null;
		subData2 = null;
		subData3 = null;
		subData4 = null;
		subData5 = null; 
		subData6 = null;
		resultObj = null;
		whichColumn = null;
	}
	
	this.setWhichColumn = function (data) {
		whichColumn = data;
	}
	
	this.getWhichColumn = function() {
		return whichColumn;
	}
	
	this.setResultObj = function (data) {
		resultObj = data;
	}
	
	this.getResultObj = function() {
		return resultObj;
	}
	
	this.setSubData5 = function (data) {
		subData5 = data;
	}
	
	this.getSubData5 = function() {
		return subData5;
	}
	
	this.setSubData4 = function (data) {
		subData4 = data;
	}
	
	this.getSubData4 = function() {
		return subData4;
	}
	
	this.setSubData3 = function (data) {
		subData3 = data;
	}
	
	this.getSubData3 = function() {
		return subData3;
	}
	
	this.setSubData2 = function (data) {
		subData2 = data;
	}
	
	this.getSubData2 = function() {
		return subData2;
	}
	
	this.setSubData1 = function (data) {
		subData1 = data;
	}
	
	this.getSubData1 = function() {
		return subData1;
	}
	
	this.setSubData6 = function (data) {
		subData6 = data;
	}
	
	this.getSubData6 = function() {
		return subData6;
	}
	
	this.set = function (data) {
		navigationData = data;
	}
	
	this.get = function() {
		return navigationData;
	}

	this.setLocation = function (data) {
		navigationLocation = data;
	}
	
	this.getLocation = function() {
		return navigationLocation;
	}
	

});