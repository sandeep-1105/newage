/**
 * Created by saravanan on 5/4/16.
 */
app.directive("attachmentSectionSimple",['$timeout','ngProgressFactory','ngDialog', 'cloneService',
             'discardService', '$rootScope', 'ValidateUtil','DocumentMasterList','$modal',
    function($timeout,ngProgressFactory,ngDialog, cloneService,
             discardService, $rootScope, ValidateUtil,DocumentMasterList,$modal){
    return {
        restrict: 'E',
        scope: {
        	"isEdit" : "=",
        	"isDelete" : "=",
            "tableData":"=",
            "removeFn": "&",
            "downloadFn":"&",
            "fnRole" : "&",
            "addRole" : "=",
            "editRole" : "=",
            "deleteRole" : "=",
            "downloadRole" : "=",
            "tableCheck":"=?",
            "indexKey" : "="
        },
        templateUrl: 'app/shared/attachmentSection/simpleAttachment/simpleattachment.html',
        controller:function($scope, $rootScope){
        	$timeout(function(){
        	$scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
        	
        	$scope.attachmentConfig = {
        				"isDelete": true,
        				"columnDefs": [

        					{"name": "Reference No", "model": "refNo", "type": "text", "width": "w300px"},
        					{"name": "File Name", "model": "fileName", "type": "file", "width": "w200px"}
        				]
        		}
        		
            $scope.attachIndex = null;
            $scope.uploadText = false;
            $scope.isBusy = false;
            $scope.customArr = [{id:true,title:'Yes'},{id:false,title:'No'}];

            function isEmptyRow(obj){
                var isempty = true; //  empty
                if (!obj) {
                    return isempty;
                }
                var k = Object.getOwnPropertyNames(obj);
                for (var i = 0; i < k.length; i++){
                    if (obj[k[i]]) {
                        isempty = false; // not empty
                        break;
                    }
                }
                return isempty;
            }

            $scope.tableCheck = function(){
                var fineArr = [];
                $scope.secondArr=[];
                var validResult;
                var finalData = $scope.tableData;
                var emptyIndex = [];
                angular.forEach($scope.tableData,function(dataObj,index){
                    $scope.secondArr[index]={};
                    if (isEmptyRow(dataObj)){
                        validResult = true;
                        finalData[index].isEmpty = true;
                    }else{
                        validResult = validateObj(dataObj,index,"row");
                    }
                    if(validResult){
                        fineArr.push(1);
                        $scope.secondArr[index].errRow = false;
                    }else{
                        $scope.secondArr[index].errRow = true;
                        fineArr.push(0);
                    }
                });
                console.log("******emptyIndex",emptyIndex, finalData);
                angular.forEach(finalData,function(dataObj,index){
                    if(dataObj.isEmpty){
                        finalData.splice(index,1);
                        $scope.secondArr.splice(index,1);
                    }
                });
                console.log("***8$scope.tableData",finalData);
                if(fineArr.indexOf(0)<0){
                    $scope.tableData = finalData;
                    return true;
                }else{
                    return false;
                }
            };

            $scope.secondArr = [];

            function initTable(){
        
                angular.forEach($scope.tableData,function(item){
                    $scope.secondArr.push({"errRow":false});
                });
            }
            initTable();

            $scope.addRow = function(){
                var fineArr = [];
                angular.forEach($scope.tableData,function(dataObj,index){
                    var validResult  = validateObj(dataObj,index,"row");
                    if(validResult){
                        fineArr.push(1);
                        $scope.secondArr[index].errRow = false;
                    }else{
                        $scope.secondArr[index].errRow = true;
                        fineArr.push(0);
                    }
                });
                if(fineArr.indexOf(0)<0){
                    if( $scope.tableData==undefined ||  $scope.tableData==null){
                        $scope.tableData=[];
                    }
                    $scope.tableData.push({});
                    $scope.secondArr.push({"errRow":false});
                    
                }
                console.log($scope.indexKey);
                var nextFocus = (($scope.indexKey!=undefined&&$scope.indexKey!='')?$scope.indexKey:'')+'attachRefNo'+($scope.tableData.length-1);
                console.log(nextFocus)
                $rootScope.navigateToNextField(nextFocus);
            };

            var validateObj = function(dataObj,index,type){
                $scope.secondArr[index] = {};
                var errorArr = [];
                if(type=="row"){
                    $scope.secondArr[index].errTextArr = [];
                }

                if (dataObj.refNo == undefined
                    || dataObj.refNo == null
                    || dataObj.refNo == "") {
                    $scope.secondArr[index].refNo = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22142"]);
                    errorArr.push(0);
                } else {
                    var checkDuplicate = false;
                    for(var i=0; i<$scope.tableData.length; i++) {
                        if(index!=i && $scope.tableData[i].refNo == dataObj.refNo) {
                            checkDuplicate = true;
                        }
                    }
                    if(!checkDuplicate){
                        $scope.secondArr[index].refNo = false;
                        errorArr.push(1);
                    }else{
                        $scope.secondArr[index].refNo = true;
                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22145"]);
                        errorArr.push(0);
                    }

                }



                if (dataObj.fileName == undefined
                    || dataObj.fileName == null
                    || dataObj.fileName == "") {
                    console.log("Error Message : "+ $rootScope.nls["ERR22144"])
                    $scope.secondArr[index].fileName = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22144"]);
                    errorArr.push(0);
                }
                else {
                    var allowedSize = $rootScope.appMasterData['upload.file.size.in.mb'];

                    if(allowedSize != undefined) {
                        allowedSize = allowedSize * 1024 * 1024;
                        if(dataObj.fileSize <= 0) {
                            console.log("Error Message : "+ $rootScope.nls["ERR22148"])
                            $scope.secondArr[index].fileName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22148"]);
                            errorArr.push(0);
                            $scope.uploadText = true;
                            $scope.isBusy = false;
                        }
                        else if(dataObj.fileSize > allowedSize) {
                            console.log("Error Message : "+ $rootScope.nls["ERR22146"])
                            $scope.secondArr[index].fileName = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR22146"]);
                            errorArr.push(0);
                            $scope.uploadText = true;
                            $scope.isBusy = false;
                        }else{
                            $scope.secondArr[index].fileName = false;
                            errorArr.push(1);
                        }
                    }
                }
                if(errorArr.indexOf(0)<0){
                    return true
                }else{
                    return false;
                }

            };

            var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
            var errorOnRowIndex = null;

            $scope.errorShow = function(errorObj,index){
                $scope.errList = errorObj.errTextArr;
                // Show when some event occurs (use $promise property to ensure the template has been loaded)
                errorOnRowIndex = index;
                myOtherModal.$promise.then(myOtherModal.show);
            };

            $(document).on('keydown',function(e){
                if(e.keyCode==27){
                    if(myOtherModal){
                        myOtherModal.$promise.then(myOtherModal.hide);
                    }
                }
            });

            $scope.$on('modal.hide',function(){
                if($scope.secondArr[errorOnRowIndex].refNoFocus){
                    $scope.secondArr[errorOnRowIndex].refNoFocus=false;
                }
                $timeout(function(){
                    $scope.secondArr[errorOnRowIndex].refNoFocus=true;
                    errorOnRowIndex = null;
                },10)
            });

            
            $scope.uploadAttach = function(file, dataObj, index) {
            	console.log("Files " , file);
            	if(angular.isArray(file)) {
            		if(file.length > 0) {
            			for(i = 0; i < file.length; i++) {
            				if(i == 0) {
            					$scope.uploadSingleFile(file[i], dataObj);
            				} else {
            					$scope.tableData.push({});
            					index++;
            					$scope.uploadSingleFile(file[i], $scope.tableData[index]);
            				}
	            		}	
            		}
            	}
            }


            $scope.uploadSingleFile = function(file, dataObj) {
            	if (file != null || file != undefined) {
            		dataObj.tmpFile = file;
            		dataObj.fileName = file.name;
            		dataObj.fileContentType = file.type;
            		dataObj.fileSize = file.size;
            		$scope.convertToByteArray(file,dataObj);
            	}	
            }
            
            $scope.convertToByteArray = function(file, dataObj) {
            	if(file != null || file != undefined) {
        			var reader = new FileReader();
        			reader.onload = function(event){
        				var contents = event.target.result;
        				var uploadedFile = btoa(contents);
        				dataObj.file = uploadedFile;
        			};
        			reader.readAsBinaryString(file);
        		}
            }



            $scope.showDocName = function(dataObj){
                if($scope.pickerFn){
                    $scope.pickerFn({param:{data:dataObj}});
                }
            }


            $scope.download = function(dataObj,type){
                if($scope.downloadFn){
                	if($rootScope.roleAccess($scope.downloadRole))
                		$scope.downloadFn({param:{data:dataObj,type:type}});
                }
            }







            $scope.findProp = function(obj, prop, defval){
                if (typeof defval == 'undefined') defval = null;
                prop = prop.split('.');
                for (var i = 0; i < prop.length; i++) {
                    //console.log("value",obj[prop[i]],typeof obj[prop[i]],prop[i]);
                    try {
                        if (obj[prop[i]] == null && typeof obj[prop[i]] == 'undefined')
                            return defval;
                        obj = obj[prop[i]];
                    }catch(e){
                        return null;
                    }

                }
                return obj;

            }

            $scope.setValue = function(type,obj,model){
                if(type!='action') {
                    return $scope.findProp(obj, model);
                }
            }

            $scope.documentRender = function(item){
                return{
                    label:item.documentName,
                    item:item
                }
            }

            $scope.removeObj = function(obj,index){
                $scope.tableData.splice(index,1);
                $scope.secondArr.splice(index,1);
            };
            $scope.delete= $rootScope.delete;
            $scope.downloadIcon= $rootScope.downloadIcon;
            $scope.editIcon= $rootScope.editIcon;
            $scope.errorList= $rootScope.errorList;
        },2);

        }
    }

}]);