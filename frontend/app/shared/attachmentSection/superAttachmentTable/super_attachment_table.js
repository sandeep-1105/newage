/**
 * Created by saravanan on 5/4/16.
 */

(function() {
	app.directive("superAttachmentTable",['$rootScope', '$timeout', '$modal', '$log', 'ngProgressFactory',
		'ngDialog', 'ValidateUtil', 'DocumentMasterList',
		function($rootScope, $timeout, $modal, $log, ngProgressFactory,ngDialog, ValidateUtil, DocumentMasterList){
	    return {
	        restrict: 'E',
	        scope: {
	        	"indexKey":"=",
	            "attachmentConfig": "=",
	            "tableData":"=?",
	            "removeFn": "&",
	            "downloadFn":"&",
	            "fnRole" : "&",
	            "addRole" : "=",
	            "editRole" : "=",
	            "deleteRole" : "=",
	            "downloadRole":"=",
	            "tableCheck":"=?"
	        },
	        templateUrl: 'app/shared/attachmentSection/superAttachmentTable/super_attachment_table.html',
	        controller:function($scope, $rootScope){
	        	console.log("DOWNLOAD ROLE : " +$scope.downloadRole);
	        	$scope.firstFocus=true;
	        	$scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
	            $scope.delete = $rootScope.delete;
	            $scope.editIcon = $rootScope.editIcon;
	            $scope.downloadIcon = $rootScope.downloadIcon;
	            $scope.errorList = $rootScope.errorList;
	            $scope.attachIndex = null;

	            function isEmptyRow(obj){
	                //return (Object.getOwnPropertyNames(obj).length === 0);
	                var isempty = true; //  empty
	                if (!obj) {
	                    return isempty;
	                }

	                var k = Object.getOwnPropertyNames(obj);
	                for (var i = 0; i < k.length; i++){
	                    if (obj[k[i]]) {
	                        isempty = false; // not empty
	                        break;

	                    }
	                }
	                return isempty;
	            }

	            $scope.tableCheck = function(){
	                var fineArr = [];
	                $scope.secondArr=[];
	                var validResult;
	                var finalData = $scope.tableData;
	                var emptyIndex = [];
	                angular.forEach($scope.tableData,function(dataObj,index){
	                    $scope.secondArr[index]={};
	                    if (isEmptyRow(dataObj)){
	                        validResult = true;
	                        finalData[index].isEmpty = true;
	                    }else{
	                        validResult = $scope.validateObj(dataObj,index);
	                    }
	                    if(validResult){
	                        fineArr.push(1);
	                        $scope.secondArr[index].errRow = false;
	                    }else{
	                        $scope.secondArr[index].errRow = true;
	                        fineArr.push(0);
	                    }
	                });
	                console.log("******emptyIndex",emptyIndex, finalData);
	                angular.forEach(finalData,function(dataObj,index){
	                    if(dataObj.isEmpty){
	                        finalData.splice(index,1);
	                        $scope.secondArr.splice(index,1);
	                    }
	                });
	                console.log("***8$scope.tableData",finalData);
	                if(fineArr.indexOf(0)<0){
	                    $scope.tableData = finalData;
	                    return true;
	                }else{
	                    return false;
	                }
	            };

	            $scope.secondArr = [];

	            function initTable(){
	                angular.forEach($scope.tableData,function(item){
	                    $scope.secondArr.push({"errRow":false});
	                });
	            }
	            initTable();

	            $scope.addRow = function(){
	                var fineArr = [];
	                angular.forEach($scope.tableData,function(dataObj,index){
	                    var validResult  = $scope.validateObj(dataObj,index);
	                    if(validResult){
	                        fineArr.push(1);
	                        $scope.secondArr[index].errRow = false;
	                    }else{
	                        $scope.secondArr[index].errRow = true;
	                        fineArr.push(0);
	                    }
	                });
	                if(fineArr.indexOf(0)<0){
	                    if( $scope.tableData==undefined ||  $scope.tableData==null){
	                        $scope.tableData=[];
	                    }
	                    $scope.tableData.push({});
	                    $scope.secondArr.push({"errRow":false});
	                    $rootScope.navigateToNextField($scope.indexKey + 'attachDocument' + ($scope.tableData.length - 1))
	                }
	            };

	            $scope.validateObj = function(dataObj,index) {
	            	
	            	$scope.firstFocus=false;
	                $scope.secondArr[index] = {};
	                var errorArr = [];
	                $scope.secondArr[index].errTextArr = [];


	                if(dataObj.documentMaster != undefined && dataObj.documentMaster != null && dataObj.documentMaster.id != null) {

	                        if(ValidateUtil.isStatusBlocked(dataObj.documentMaster.status)) {
	                        	console.log("Selected Document is Blocked");

	                            $scope.secondArr[index].documentMaster = true;
	                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90600"]);
	                            errorArr.push(0);
	                        } else if(ValidateUtil.isStatusHidden(dataObj.documentMaster.status)) {
	                            console.log("Selected Document is Hidden");

	                            $scope.secondArr[index].documentMaster = true;
	                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90601"]);
	                            errorArr.push(0);
	                        } else{
	                            $scope.secondArr[index].documentMaster = false;
	                            errorArr.push(1);
	                        }
	                    }

	                    if (dataObj.refNo == undefined || dataObj.refNo == null || dataObj.refNo == "") {

	                        console.log("Error Message : "+ $rootScope.nls["ERR90602"])

	                        $scope.secondArr[index].refNo = true;
	                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90602"]);
	                        errorArr.push(0);

	                    } else {

	                    	var refNoCheck = false;
	                        
	                    	for(var i=0; i<$scope.tableData.length; i++) {
	                            if(index!=i && $scope.tableData[i].refNo == dataObj.refNo) {
	                                refNoCheck = true;
	                            }
	                        }

	                    	if(!refNoCheck) {
	                            $scope.secondArr[index].refNo = false;
	                            errorArr.push(1);
	                        } else {
	                            $scope.secondArr[index].refNo = true;
	                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90604"]);
	                            errorArr.push(0);
	                        }
	                    }


	                    console.log("Checking the file size to upload.....");

	                    if(dataObj.fileSize != undefined && dataObj.fileSize != null) {

	                        var allowedSize = $rootScope.appMasterData['upload.file.size.in.mb'];

	                        if(allowedSize != undefined) {

	                        	console.log("Allowed File Size protected : "+ allowedSize + " MB");

	                            console.log("Uploaded File Size protected : ", dataObj.fileSize);

	                            allowedSize = allowedSize * 1024 * 1024;

	                            if(dataObj.fileSize <= 0) {

	                                console.log("Error Message : "+ $rootScope.nls["ERR90605"])

	                                $scope.secondArr[index].protectedFile = true;
	                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90605"]);
	                                errorArr.push(0);

	                            }

	                            else if(dataObj.fileSize > allowedSize) {

	                                console.log("Error Message : "+ $rootScope.nls["ERR90606"])

	                                $scope.secondArr[index].protectedFile = true;
	                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90606"]);
	                                errorArr.push(0);

	                            } else {
	                                
	                            	$scope.secondArr[index].protectedFile = false;
	                                errorArr.push(1);
	                            }
	                        }
	                    }



	                if(errorArr.indexOf(0)<0) {
	                    return true
	                } else {
	                    return false;
	                }

	            }

	            var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
	            var errorOnRowIndex = null;

	            $scope.errorShow = function(errorObj,index){
	                $scope.errList = errorObj.errTextArr;
	                errorOnRowIndex = index;
	                myOtherModal.$promise.then(myOtherModal.show);
	            };

	            $(document).on('keydown',function(e){
	                if(e.keyCode==27){
	                    if(myOtherModal){
	                        myOtherModal.$promise.then(myOtherModal.hide);
	                    }
	                }
	            });

	            $scope.$on('modal.hide',function(){
	                if($scope.secondArr[errorOnRowIndex].refNoFocus){
	                    $scope.secondArr[errorOnRowIndex].refNoFocus=false;
	                }
	                $timeout(function(){
	                    $scope.secondArr[errorOnRowIndex].refNoFocus=true;
	                    errorOnRowIndex = null;
	                },10)
	            });
	            
	            $scope.ajaxDocumentMaster = function(object) {

	                console.log("ajaxDocumentMaster ", object);

	                $scope.searchDto={};
	                $scope.searchDto.keyword=object==null?"":object;
	                $scope.searchDto.selectedPageNumber = 0;
	                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

	                return DocumentMasterList.query($scope.searchDto).$promise.then(function(data, status) {
	                        if (data.responseCode =="ERR0"){
	                            $scope.totalRecord = data.responseObject.totalRecord;
	                            $scope.documentMasterList = data.responseObject.searchResult;
	                            return $scope.documentMasterList;
	                        }
	                    },
	                    function(errResponse){
	                        console.error('Error while fetching Currency List');
	                    }
	                );
	            }
	            
	            $scope.uploadAttach = function(file, dataObj, index) {
	            	
	            	console.log("Files " , file);
	            	
	            	
	            	if(angular.isArray(file)) {
	            		
	            		if(file.length > 0) {
	            			for(i = 0; i < file.length; i++) {
	            				if(i == 0) {
	            					$scope.uploadSingleFile(file[i], dataObj);
	            				} else {
	            					$scope.tableData.push({});
	            					index++;
	            					$scope.uploadSingleFile(file[i], $scope.tableData[index]);
	            				}
	            				
		            		}	
	            		}
	            	}
	            		
	            }


	            $scope.uploadSingleFile = function(file, dataObj) {
	            	if (file != null || file != undefined) {
	            		dataObj.tmpFile = file;
	            		dataObj.fileName = file.name;
	            		dataObj.fileContentType = file.type;
	            		dataObj.fileSize = file.size;
	            		$scope.convertToByteArray(file,dataObj);
	            	}	
	            }
	            
	            $scope.convertToByteArray = function(file, dataObj) {
	            	if(file != null || file != undefined) {
	        			var reader = new FileReader();
	        			reader.onload = function(event){
	        				var contents = event.target.result;
	        				var uploadedFile = btoa(contents);
	        				dataObj.file = uploadedFile;
	        			};
	        			reader.readAsBinaryString(file);
	        		}
	            }

	            $scope.download = function(dataObj,type){
	                if($scope.downloadFn){
	                	if($rootScope.roleAccess($scope.downloadRole)) {
	                		$scope.downloadFn({param:{data:dataObj,type:type}});
	                	}
	                }
	            }
	            
	            $scope.unprotectedAlert=function(obj){
	            	
	            	if(obj==undefined || obj==null || obj==""){
	            		
	            		return;
	            	}else{
	            		
	            		if(obj.documentMaster.isProtected=='Yes' || obj.documentMaster.isProtected==true){
	            			
	            			if(obj.isProtected==false){
	            				
	            				var newScope = $scope.$new();
	            				newScope.errorMessage = $rootScope.nls["ERR266"];
	            				
	            				ngDialog.openConfirm({
	            					template: '<p>{{errorMessage}}</p>' +
	            					'<div class="ngdialog-footer">' +
	            						' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
	            						'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
	            						'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
	            					'</div>',
	            					plain: true,
	            					scope: newScope,
	            					className: 'ngdialog-theme-default'
	            				}).then(
	            								function(value) {
	            									if (value == 1) {
	            										obj.isProtected=false;
	            									} else if (value == 2) {
	            										obj.isProtected=true;
	            									} else {
	            										obj.isProtected=true;
	            									}
	            								});
	            				
	            			}
	            			
	            		}
	            		
	            	}
	            }

	            


	   
	           
	        $scope.findProp = function(obj, prop, defval){
	                    if (typeof defval == 'undefined') defval = null;
	                    prop = prop.split('.');
	                    for (var i = 0; i < prop.length; i++) {
	                        try {
	                            if (obj[prop[i]] == null && typeof obj[prop[i]] == 'undefined')
	                                return defval;
	                            obj = obj[prop[i]];
	                        }catch(e){
	                            return null;
	                        }

	                    }
	                    return obj;
	            }

	            $scope.setValue = function(type,obj,model) {
	                if(type != 'action' && model != 'isCustoms' && model != 'isProtected') {
	                    return $scope.findProp(obj, model);
	                }else if(model == 'isCustoms' || model == 'isProtected'){
	                	return $scope.findProp(obj, model)==true?'Yes':'No';
	                }
	            }
	            
	            
	            $scope.selectDocument =function(nextFieldId, $index, obj){
	            	
	            	console.log(obj.documentMaster);
	            	
	            	obj.isProtected = obj.documentMaster.isProtected;
	            	
	            	if(nextFieldId != undefined && nextFieldId != null)
	            		$rootScope.navigateToNextField(nextFieldId);
	            }
	            
	            $scope.changeIsProtected = function(obj) {
	            	if(obj.documentMaster != undefined && obj.documentMaster != null && obj.documentMaster != "") {
	            		if(obj.documentMaster.isProtected == true) {
	            			obj.isProtected = obj.documentMaster.isProtected;
	            		}
	            	}
	            }
	            
	            $scope.removeObj = function(obj,index){
	                $scope.tableData.splice(index,1);
	                $scope.secondArr.splice(index,1);
	            };

	            

	        }
	    }
	}]);
	
})();
