/**
 * Created by saravanan on 5/4/16.
 */

(function() {
	app.directive("attachmentSectionAdvance",['$rootScope', '$timeout', '$modal', '$log', 'ngProgressFactory',
		'ngDialog', 'ValidateUtil', 'DocumentMasterList',
		function($rootScope, $timeout, $modal, $log, ngProgressFactory,ngDialog, ValidateUtil, DocumentMasterList){
	    return {
	        restrict: 'E',
	        scope: {
	        	"indexKey":"=",
	            "attachmentConfig": "=",
	            "tableData":"=?",
	            "removeFn": "&",
	            "downloadFn":"&",
	            "fnRole" : "&",
	            "addRole" : "=",
	            "editRole" : "=",
	            "deleteRole" : "=",
	            "downloadRole" : "=",
	            "tableCheck":"=?"
	        },
	        templateUrl: 'app/shared/attachmentSection/advanceAttachment/advanceattachment.html',
	        controller:function($scope, $rootScope){
	            $scope.updateRole = $rootScope.roleAccess($scope.editRole, true);
	            $scope.delete = $rootScope.delete;
	            $scope.editIcon = $rootScope.editIcon;
	            $scope.downloadIcon = $rootScope.downloadIcon;
	            $scope.errorList = $rootScope.errorList;
	            $scope.attachMentData = {};

	            $scope.attachIndex = null;

	            $scope.uploadText = false;
	            $scope.isBusy = false;

	            $scope.customArr = [{id:true,title:'Yes'},{id:false,title:'No'}];

	            function isEmptyRow(obj){
	                //return (Object.getOwnPropertyNames(obj).length === 0);
	                var isempty = true; //  empty
	                if (!obj) {
	                    return isempty;
	                }

	                var k = Object.getOwnPropertyNames(obj);
	                for (var i = 0; i < k.length; i++){
	                    if (obj[k[i]]) {
	                        isempty = false; // not empty
	                        break;

	                    }
	                }
	                return isempty;
	            }

	            $scope.tableCheck = function(){
	                var fineArr = [];
	                $scope.secondArr=[];
	                var validResult;
	                var finalData = $scope.tableData;
	                var emptyIndex = [];
	                angular.forEach($scope.tableData,function(dataObj,index){
	                    $scope.secondArr[index]={};
	                    if (isEmptyRow(dataObj)){
	                        validResult = true;
	                        finalData[index].isEmpty = true;
	                    }else{
	                        validResult = validateObj(dataObj,index,"row");
	                    }
	                    if(validResult){
	                        fineArr.push(1);
	                        $scope.secondArr[index].errRow = false;
	                    }else{
	                        $scope.secondArr[index].errRow = true;
	                        fineArr.push(0);
	                    }
	                });
	                $log.info("******emptyIndex",emptyIndex, finalData);
	                angular.forEach(finalData,function(dataObj,index){
	                    if(dataObj.isEmpty){
	                        finalData.splice(index,1);
	                        $scope.secondArr.splice(index,1);
	                    }
	                });
	                $log.info("***8$scope.tableData",finalData);
	                if(fineArr.indexOf(0)<0){
	                    $scope.tableData = finalData;
	                    return true;
	                }else{
	                    return false;
	                }
	            };

	            $scope.secondArr = [];

	            function initTable(){
	                angular.forEach($scope.tableData,function(item){
	                    $scope.secondArr.push({"errRow":false});
	                });
	            }
	            initTable();

	            $scope.addRow = function(){
	                var fineArr = [];
	                angular.forEach($scope.tableData,function(dataObj,index){
	                    //var curObj = $scope.tableData.data[index];
	                    var validResult  = validateObj(dataObj,index,"row");
	                    if(validResult){
	                        fineArr.push(1);
	                        $scope.secondArr[index].errRow = false;
	                    }else{
	                        $scope.secondArr[index].errRow = true;
	                        fineArr.push(0);
	                    }
	                });
	                if(fineArr.indexOf(0)<0){
	                    if( $scope.tableData==undefined ||  $scope.tableData==null){
	                        $scope.tableData=[];
	                    }
	                    $scope.tableData.push({});
	                    $scope.secondArr.push({"errRow":false});
	                    $rootScope.navigateToNextField($scope.indexKey + 'attachDocument' + ($scope.tableData.length - 1))
	                }
	            };

	            var validateObj = function(dataObj,index,type){
	                $scope.secondArr[index] = {};
	                var errorArr = [];
	                if(type=="row"){
	                    $scope.secondArr[index].errTextArr = [];
	                }

	                if($scope.attachmentConfig.page && $scope.attachmentConfig.page!='noDoc'){
	                    if(dataObj.documentMaster != undefined &&
	                        dataObj.documentMaster != null &&
	                        dataObj.documentMaster.id!=null) {

	                        if(ValidateUtil.isStatusBlocked(dataObj.documentMaster.status)) {
	                            $log.info("Selected Document is Blocked");

	                            $scope.secondArr[index].documentMaster = true;
	                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90600"]);
	                            errorArr.push(0);
	                        }

	                        else if(ValidateUtil.isStatusHidden(dataObj.documentMaster.status)) {
	                            $log.info("Selected Document is Hidden");

	                            $scope.secondArr[index].documentMaster = true;
	                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90601"]);
	                            errorArr.push(0);
	                        }else{
	                            $scope.secondArr[index].documentMaster = false;
	                            errorArr.push(1);
	                        }
	                    }
	                }





	                    if (dataObj.refNo == undefined
	                        || dataObj.refNo == null
	                        || dataObj.refNo == "") {

	                        $log.info("Error Message : "+ $rootScope.nls["ERR90602"])

	                        $scope.secondArr[index].refNo = true;
	                        $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90602"]);
	                        errorArr.push(0);

	                    } else {
	                        $log.info("$scope.tableData", $scope.tableData);
	                         var refNoCheck = false;
	                        for(var i=0; i<$scope.tableData.length; i++) {

	                            if(index!=i && $scope.tableData[i].refNo == dataObj.refNo) {

	                                $log.info("$scope.tableData["+i+"]", $scope.tableData[i]);

	                                $log.info("dataObj.refNo", dataObj);

	                                $log.info("index",index);
	                                refNoCheck = true;

	                            }
	                        }
	                        if(!refNoCheck){
	                            $scope.secondArr[index].refNo = false;
	                            errorArr.push(1);
	                        }else{
	                            $scope.secondArr[index].refNo = true;
	                            $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90604"]);
	                            errorArr.push(0);
	                        }
	                    }




	                    $log.info("Checking the protected file size to upload.....");

	                    if(dataObj.protectedFileSize != undefined &&
	                        dataObj.protectedFileSize != null) {

	                        var allowedSize = $rootScope.appMasterData['upload.protected.file.size.in.mb'];

	                        if(allowedSize != undefined) {

	                            $log.info("Allowed File Size protected : "+ allowedSize + " MB");

	                            $log.info("Uploaded File Size protected : ", dataObj.protectedFileSize);

	                            allowedSize = allowedSize * 1024 * 1024;

	                            if(dataObj.protectedFileSize <= 0) {

	                                $log.info("Error Message : "+ $rootScope.nls["ERR90605"])

	                                $scope.secondArr[index].protectedFile = true;
	                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90605"]);
	                                errorArr.push(0);

	                                $scope.procText = true;
	                                $scope.isBusy = false;
	                            }

	                            else if(dataObj.protectedFileSize > allowedSize) {

	                                $log.info("Error Message : "+ $rootScope.nls["ERR90606"])

	                                $scope.errorMap.put("protectedFile",$rootScope.nls["ERR90606"]);
	                                $scope.secondArr[index].protectedFile = true;
	                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90606"]);
	                                errorArr.push(0);
	                                $scope.procText = true;
	                                $scope.isBusy = false;
	                            }else{
	                                $scope.secondArr[index].protectedFile = false;
	                                errorArr.push(1);
	                            }
	                        }
	                    }


	                    $log.info("Checking the unprotected file size to upload.....");


	                    if(dataObj.unprotectedFileSize != undefined &&
	                        dataObj.unprotectedFileSize != null) {

	                        var allowedSize = $rootScope.appMasterData['upload.unprotected.file.size.in.mb'];

	                        if(allowedSize != undefined) {

	                            $log.info("Allowed File Size for unprotected: "+ allowedSize + " MB");

	                            $log.info("Uploaded File Size unprotected: ", dataObj.unprotectedFileSize);

	                            allowedSize = allowedSize * 1024 * 1024;

	                            if(dataObj.unprotectedFileSize <= 0) {

	                                $log.info("Error Message unProtectedFile : "+ $rootScope.nls["ERR90607"])

	                                $scope.secondArr[index].unprotected = true;
	                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90607"]);
	                                errorArr.push(0);
	                                $scope.unProcText = true;
	                                $scope.isBusy = false;

	                            }

	                            else if(dataObj.unprotectedFileSize > allowedSize) {

	                                $log.info("Error Message unProtectedFile: "+ $rootScope.nls["ERR90608"])
	                                $scope.secondArr[index].unprotected = true;
	                                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR90608"]);
	                                errorArr.push(0);
	                                $scope.unProcText = true;
	                                $scope.isBusy = false;

	                            }else{
	                                $scope.secondArr[index].unprotected = false;
	                                errorArr.push(1);
	                            }

	                        }
	                }

	                if(errorArr.indexOf(0)<0){
	                    return true
	                }else{
	                    return false;
	                }

	            };

	            var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
	            var errorOnRowIndex = null;

	            $scope.errorShow = function(errorObj,index){
	                $scope.errList = errorObj.errTextArr;
	                // Show when some event occurs (use $promise property to ensure the template has been loaded)
	                errorOnRowIndex = index;
	                myOtherModal.$promise.then(myOtherModal.show);
	            };
	            $(document).on('keydown',function(e){
	                if(e.keyCode==27){
	                    if(myOtherModal){
	                        myOtherModal.$promise.then(myOtherModal.hide);
	                    }
	                }
	            });

	            $scope.$on('modal.hide',function(){
	                if($scope.secondArr[errorOnRowIndex].refNoFocus){
	                    $scope.secondArr[errorOnRowIndex].refNoFocus=false;
	                }
	                $timeout(function(){
	                    $scope.secondArr[errorOnRowIndex].refNoFocus=true;
	                    errorOnRowIndex = null;
	                },10)
	            });
	            $scope.ajaxDocumentMaster = function(object) {

	                $log.info("ajaxDocumentMaster ", object);

	                $scope.searchDto={};
	                $scope.searchDto.keyword=object==null?"":object;
	                $scope.searchDto.selectedPageNumber = 0;
	                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

	                return DocumentMasterList.query($scope.searchDto).$promise.then(function(data, status) {
	                        if (data.responseCode =="ERR0"){
	                            $scope.totalRecord = data.responseObject.totalRecord;
	                            $scope.documentMasterList = data.responseObject.searchResult;
	                            return $scope.documentMasterList;
	                            //$scope.showDocumentList=true;
	                        }
	                    },
	                    function(errResponse){
	                        console.error('Error while fetching Currency List');
	                    }

	                );

	            }
	            
	            $scope.uploadAttach = function(type,file,dataObj) {
	            	
	            	$scope.uploadText = false;

	            	$log.info("################ file : ",file," -Type :: ",type);
	            	
	                if(type=='protected'){
	                	
	                    $scope.isProcBusy = true;
	                    
	                    $log.info('protected file getting upload...')
	                    
	                    if (file != null || file != undefined) {
	                    	$log.info("#--111111file.name ------------- ", file);
	                        dataObj.tmpFile1=file;
	                    	$log.info("#000000000000 file.name ------------- ", file.name);
	                        dataObj.protectedFileName = file.name;
	                    	$log.info("#11111111 $scope.attachMentData.protectedFileName------------- ", dataObj.protectedFileName);
	                        dataObj.protectedFileContentType = file.type;

	                        dataObj.protectedFileSize = file.size;
	                        
	                    	$scope.procText = false;
	                        
	                    	if($scope.validateAttachmentShipment(3)) {
	                    		
	                    		$scope.convertToByteArray(type, file,dataObj);
	                    	
	                    	}
	                    	else {
	                            dataObj.protectedFile = null;

	                            dataObj.tmpFile1 = null;

	                            dataObj.protectedFileName = null;

	                            dataObj.protectedFileContentType = null;

	                            dataObj.protectedFileSize = null;
	                    		
	                    	}
	                        	
	                    }
	                    $timeout(function(){
	                        $scope.isProcBusy = false;
	                    },5000);

	                }
	                else if(type=='unprotected') {
	                    
	                	$scope.isUnProcBusy = true;
	                    
	                	$log.info('unprotected file getting upload...')
	                    
	                    	$log.info("#--111111file.name ------------- ", file);
	                	if (file != null || file != undefined) {

	                        dataObj.tmpFile2=file;
	                		$log.info("#CHK #1111111 ------------- ", file.name);
	                        dataObj.unprotectedFileName = file.name;
	                		$log.info("#CHk #2222222------------- ", dataObj.unprotectedFileName );
	                        dataObj.unprotectedFileContentType = file.type;

	                        dataObj.unprotectedFileSize = file.size;
	                        
	                		$scope.unProcText = false;
	                        
	                		if($scope.validateAttachmentShipment(4)) {
	                		
	                			$scope.convertToByteArray(type, file,dataObj);
	                		
	                		} else {
	                			
	                			dataObj.unprotectedFile = null;
	                			
	                			dataObj.tmpFile2 = null;
	                			
	                    		dataObj.unprotectedFileName = null;
	                    		
	                    		dataObj.unprotectedFileContentType  = null;
	                    		
	                    		dataObj.unprotectedFileSize = null;
	                		}
	                		
	                    	
	                    }
	                    $timeout(function(){
	                        $scope.isUnProcBusy = false;
	                    },5000);
	                }
	                $log.info("#CHk #2222222------------- ", dataObj.unprotectedFileName );
	                $log.info("#33333333333 dataObj.protectedFileName------------- ", dataObj.protectedFileName);
	            }


	            
	            $scope.convertToByteArray = function(type, file,dataObj) {
	            	
	            	if(file != null || file != undefined){
	            		
	        			var reader = new FileReader();
	        			
	        			reader.onload = function(event){
	        			
	        				var contents = event.target.result;
	        				
	        				var uploadedFile = btoa(contents);
	        				

	        				if(type=='general') {
	        					$log.info("General File Converted")
	        					dataObj.file = uploadedFile;
	        				}
	        				if(type == 'protected') {
	        					$log.info("protected File Converted")
	        					dataObj.protectedFile = uploadedFile;
	        				}
	        				if(type == 'unprotected') {
	        					$log.info("unprotected File Converted")
	        					dataObj.unprotectedFile = uploadedFile;
	        				}
	        			};
	        			
	        			reader.readAsBinaryString(file);
	                    $log.info("dataObj",dataObj);
	        		}
	            }

	            $scope.swapAttachment = function(dataObj){
	            	$log.info("Swap button pressed.................", $scope.attachIndex);
	                $scope.isUnProcBusy = true;
	                $scope.isProcBusy = true;
	                $log.info("dataObj",dataObj);
	            	var temp = dataObj.protectedFile;
	            	dataObj.protectedFile =  dataObj.unprotectedFile;
	            	dataObj.unprotectedFile = temp;

	                var tempText = $scope.procText;
	                $scope.procText =  $scope.unProcText;
	                $scope.unProcText = tempText;
	                
	                $timeout(function(){
	                    $scope.isProcBusy = false;
	                    $scope.isUnProcBusy = false;
	                },5000);

	            	temp = dataObj.tmpFile1;
	            	dataObj.tmpFile1 = dataObj.tmpFile2;
	            	dataObj.tmpFile2 = temp;
	            	

	            	
	            	
	            	
	            	temp = dataObj.protectedFileName;
	            	dataObj.protectedFileName = dataObj.unprotectedFileName;
	            	dataObj.unprotectedFileName = temp;
	            	
	            	temp = dataObj.protectedFileContentType;
	            	dataObj.protectedFileContentType = dataObj.unprotectedFileContentType;
	            	dataObj.unprotectedFileContentType = temp;
	            	
	            	$log.info("dataObj.swapped" + dataObj.swapped);
	            	if(dataObj.id != null && dataObj.id != undefined) {
	            		if(dataObj.swapped){
	            			dataObj.swapped = false;
	            		} else {
	            			dataObj.swapped = true;
	            		}
	            	} else {
	        			dataObj.swapped = false;
	        		}
	            
	            	$log.info("Swaping is done!");
	            }


	            $scope.download = function(dataObj,type){
	            	$log.info("Download in directive : ", dataObj, type);
	                if($scope.downloadFn){
	                	if($rootScope.roleAccess($scope.downloadRole))
	                    $scope.downloadFn({param:{data:dataObj,type:type}});
	                }
	            }
	            

	            
	            $scope.saveAttachShip = function(type,index){
	            	$scope.firstFieldFocus = false;
	            	$log.info("Save Button is Pressed...........", type, index);
	            	
	                if ($scope.validateAttachmentShipment(0)) {
	                	
	                    $log.info("fully validated");
	                    
	                    $log.info("scope",$scope.attachMentData);
	                    
	                    $scope.attachMentData.customs = $scope.attachMentData.customs==true?'Yes':'No';
	                  
	                    $log.info("Before : " ,$scope.attachMentData.refNo.length);
	                	$scope.attachMentData.refNo = $scope.attachMentData.refNo.trim();
	                	$log.info("After : " ,$scope.attachMentData.refNo.length);
	                	$log.info("Saving ",$scope.attachMentData.refNo);
	                  
	                    $scope.attachmentShow= false;
	                    
	                    $scope.attachIndex = null;
	                    myOtherModal.$promise.then(myOtherModal.hide);
	                }

	            }


	   
	           
	        $scope.findProp = function(obj, prop, defval){
	                    if (typeof defval == 'undefined') defval = null;
	                    prop = prop.split('.');
	                    for (var i = 0; i < prop.length; i++) {
	                        //$log.info("value",obj[prop[i]],typeof obj[prop[i]],prop[i]);
	                        try {
	                            if (obj[prop[i]] == null && typeof obj[prop[i]] == 'undefined')
	                                return defval;
	                            obj = obj[prop[i]];
	                        }catch(e){
	                            return null;
	                        }

	                    }
	                    return obj;

	            }

	            $scope.setValue = function(type,obj,model){
	                if(type!='action' && model!='customs') {
	                    return $scope.findProp(obj, model);
	                }else if(model=='customs'){
	                	return $scope.findProp(obj, model)==true?'Yes':'No';
	                }
	            }
	            
	            
	            
	        	
	            $scope.validateAttachmentShipment = function(validateCode){
	            	
	            	$log.info("Validating Shipment Attachment ", validateCode);
	            	
	            	$scope.errorMap = new Map();
	            	
	            	$scope.firstFieldFocus = false;
	            	
	            	if(validateCode == 0 || validateCode == 1) {
	            		
	            		if($scope.attachMentData.documentMaster != undefined && 
	            				$scope.attachMentData.documentMaster != null && 
	            				$scope.attachMentData.documentMaster.id!=null) {
	            			
	            			if(ValidateUtil.isStatusBlocked($scope.attachMentData.documentMaster.status)) {
	            				$log.info("Selected Document is Blocked");
	            				$scope.attachMentData.documentMaster = null;
	            				$scope.errorMap.put("documentMaster", $rootScope.nls["ERR90600"]);
	            				return false;
	            			}
	            			
	            			if(ValidateUtil.isStatusHidden($scope.attachMentData.documentMaster.status)) {
	            				$log.info("Selected Document is Hidden");
	            				$scope.attachMentData.documentMaster = null;
	            				$scope.errorMap.put("documentMaster", $rootScope.nls["ERR90601"]);
	            				return false;
	            			}
	            		}
	            		
	            	}
	            	
	                if(validateCode == 0 || validateCode == 2) {
	            		

	                    if ($scope.attachMentData.refNo == undefined
	                        || $scope.attachMentData.refNo == null
	                        || $scope.attachMentData.refNo == "") {
	                    	
	                    	$log.info("Error Message : "+ $rootScope.nls["ERR90602"])
	                        
	                    	$scope.errorMap.put("refNo",$rootScope.nls["ERR90602"]);
	                        
	                    	return false;
	                   
	                    } else {
	                    	//Reference no. allows special charcters also
	                    	/*var regexp = new RegExp("^[A-Za-z0-9]{0,100}$");
	        				
	                    	if (!regexp.test($scope.attachMentData.refNo)) {
	        				
	                    		$scope.errorMap.put("refNo",$rootScope.nls["ERR90603"]);
	        					
	                    		return false;
	        				}*/
	                    	
	                    	
	                    	$log.info("$scope.tableData", $scope.tableData);

	        				for(var i=0; i<$scope.tableData.length; i++) {
	        				
	        					if($scope.attachIndex!=i && $scope.tableData[i].refNo == $scope.attachMentData.refNo) {
	        					
	        						$log.info("$scope.tableData["+i+"]", $scope.tableData[i]);
	        						
	        						$log.info("$scope.attachMentData.refNo", $scope.attachMentData);
	        						
	        						$log.info("$scope.attachIndex",$scope.attachIndex);
	        						
	        						$scope.errorMap.put("refNo",$rootScope.nls["ERR90604"]);
	            					
	        						return false;
	        					}
	        				}
	                    }

	            		
	            	}
	            	
	                if(validateCode==0 || validateCode==3) {
	                     
	                 	$log.info("Checking the protected file size to upload.....");
	                 	
	                 	if($scope.attachMentData.protectedFileSize != undefined && 
	                 			$scope.attachMentData.protectedFileSize != null) {

	                 		var allowedSize = $rootScope.appMasterData['upload.protected.file.size.in.mb'];
	                 		
	                 		if(allowedSize != undefined) {
	                 		
	                 			$log.info("Allowed File Size protected : "+ allowedSize + " MB");
	                 			
	                 			$log.info("Uploaded File Size protected : ", $scope.attachMentData.protectedFileSize);
	                 			
	                 			allowedSize = allowedSize * 1024 * 1024;
	                 			
	                 			if($scope.attachMentData.protectedFileSize <= 0) {
	                         	
	                 				$log.info("Error Message : "+ $rootScope.nls["ERR90605"])
	                         		
	                 				$scope.errorMap.put("protectedFile",$rootScope.nls["ERR90605"]);

	                        		$scope.procText = true;
	                        		$scope.isBusy = false;
	                 				return false;
	                         	}
	                         	
	                         	if($scope.attachMentData.protectedFileSize > allowedSize) {
	                         	
	                         		$log.info("Error Message : "+ $rootScope.nls["ERR90606"])
	                         		
	                         		$scope.errorMap.put("protectedFile",$rootScope.nls["ERR90606"]);

	                        		$scope.procText = true;
	                        		$scope.isBusy = false;
	                         		return false;
	                         	}	
	                     	}
	                 	}
	                 }

	                if(validateCode==0 || validateCode==4){
	                	 
	                 	$log.info("Checking the unprotected file size to upload.....");
	                 	/*
	                	if($scope.attachMentData.unprotectedFile == null && $scope.attachMentData.protectedFile == null)
	                     	
	             		{
	                        $log.info("Error Message : "+ $rootScope.nls["ERR90609"]);
	                 		
	                 		$scope.errorMap.put("unProtectedFile",$rootScope.nls["ERR90609"]);
	             		
	             		return false;
	             		}*/
	                 	
	                 	if($scope.attachMentData.unprotectedFileSize != undefined && 
	                 			$scope.attachMentData.unprotectedFileSize != null) {
	                 		
	                 		var allowedSize = $rootScope.appMasterData['upload.unprotected.file.size.in.mb'];
	                 		
	                 		if(allowedSize != undefined) {
	                 		
	                 			$log.info("Allowed File Size for unprotected: "+ allowedSize + " MB");
	                 			
	                 			$log.info("Uploaded File Size unprotected: ", $scope.attachMentData.unprotectedFileSize);
	                 			
	                 			allowedSize = allowedSize * 1024 * 1024;
	                 			
	                 			if($scope.attachMentData.unprotectedFileSize <= 0) {
	                         	
	                 				$log.info("Error Message unProtectedFile : "+ $rootScope.nls["ERR90607"])
	                         		
	                 				$scope.errorMap.put("unProtectedFile",$rootScope.nls["ERR90607"]);
	                        		$scope.unProcText = true;
	                        		$scope.isBusy = false;
	                 				return false;
	                         	
	                 			}
	                         	
	                         	if($scope.attachMentData.unprotectedFileSize > allowedSize) {
	                         	
	                         		$log.info("Error Message unProtectedFile: "+ $rootScope.nls["ERR90608"])
	                         		
	                         		$scope.errorMap.put("unProtectedFile",$rootScope.nls["ERR90608"]);
	                        		$scope.unProcText = true;
	                        		$scope.isBusy = false;
	                         		return false;
	                         
	                         	}	
	                     	
	                 		}
	                 	}
	                 }

	                 return true;
	            	
	            }
	            
	            
	            
	            
	         
	            $scope.selectDocument =function(nextFieldId){
	            	if($scope.validateAttachmentShipment(1)){
	            		if(nextFieldId!=undefined&&nextFieldId!=null)
	            	$rootScope.navigateToNextField(nextFieldId);
	            	}
	            	
	            }
	            
	            
	            $scope.documentRender = function(item){
	                return{
	                    label:item.documentName,
	                    item:item
	                }
	            }
	            $scope.removeObj = function(obj,index){
	                $scope.tableData.splice(index,1);
	                $scope.secondArr.splice(index,1);
	            };


	        }
	    }
	}]);
	
})();
