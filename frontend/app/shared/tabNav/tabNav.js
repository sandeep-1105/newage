    /**
 * Created by saravanan on 23/6/16.
 */
app.directive('tabNav',['$timeout', function($timeout) {
    return function(scope, element, attr) {

        element.on('keydown.tabNav', 'input[type="text"],select,textarea,a', handleNavigation);


        function handleNavigation(e) {

            var key = e.keyCode ? e.keyCode : e.which;
            if(key==9 && !e.shiftKey) {
                var focusedElement = $(e.target);
                if(focusedElement.hasClass('last-child')){
                    var table = $(focusedElement).closest('table');
                    var lastRow = $(table).find("tbody tr:last").find('.last-child');
                    var focusedElementKey = focusedElement[0]['$$hashKey'];
                    var lastRowKey;
                    lastRowKey = lastRow[0]['$$hashKey'];
                    if(lastRowKey==undefined && lastRow[1]!=undefined){
                    	lastRowKey = lastRow[1]['$$hashKey'];
                    }
                    console.log("focusedElementKey",focusedElementKey);
                    console.log("lastRowKey",lastRowKey);
                    if(focusedElementKey==lastRowKey){
                        if(attr.tabRow){
                            scope.$evalAsync(attr.tabRow);
                        }
                    }


                    //if($(table +'tr:last-child td'))


                }

            }

        }
    };
}]);
/*Created BY Tanveer*/
app.directive('blurFocusForward',[ function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {

            var key = event.keyCode ? event.keyCode : event.which;
            if(key === 9 && !event.shiftKey) {
                scope.$apply(function (){
                    scope.$eval(attrs.blurFocusForward);
                });

                event.preventDefault();
            }
        });
    };
}]);