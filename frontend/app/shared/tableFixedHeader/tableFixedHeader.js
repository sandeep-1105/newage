/**
 * Created by saravanan on 24/5/16.
 */
app.directive('tableFixHeader',['$timeout','deviceDetector', function($timeout,deviceDetector){
    return {
        restrict: 'A',
        link: link
    };

    function link($scope, $elem) {
        $timeout(function(){
            var $table = $elem;
            var tableWidth = 0;
            $table.find('thead tr:first-child th').each(function(index,item){
                var prefW = $(this).attr('data-prefWidth');
                if(prefW){
                    tableWidth+=parseInt(prefW);
                }
            })
            var panel_body = $table.closest('.panel-body').width();
            if(tableWidth>0 && tableWidth > panel_body) {
                $table.css("width", tableWidth + "px");

            }else{
                $table.css("width", "100%");
            }
            /*$table.floatThead({
                position: 'fixed',
                scrollContainer: true,
                getSizingRow: function($table){
                    if(deviceDetector.browser=="ie"){
                        $table.trigger('reflow');
                    }

                }
            });*/
            if(deviceDetector.browser!="ie"){
                $table.trigger('reflow');
            }
        },1)


    }
}])
