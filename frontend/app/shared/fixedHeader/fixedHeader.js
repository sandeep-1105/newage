/**
 * Created by saravanan on 5/4/16.
 */
app.directive('fixedHeader',['$timeout', function($timeout){
    return {
        restrict: 'A',
        link: link
    };

    function link($scope, $elem, $attrs, $ctrl) {
        var elem = $elem[0];
        //console.log("element",elem);

        // wait for data to load and then transform the table
        $scope.$watch(tableDataLoaded, function(isTableDataLoaded) {
            if (isTableDataLoaded) {
                transformTable("first");
            }
        });
        $(window).bind('resize', function(){
            transformTable("resize");
        });

        function tableDataLoaded() {
            // first cell in the tbody exists when data is loaded but doesn't have a width
            // until after the table is transformed
            var firstCell = elem.querySelector('tbody tr:first-child td:first-child');
            return firstCell && !firstCell.style.width;
        }

        function transformTable(type) {
            // reset display styles so column widths are correct when measured below
            angular.element(elem.querySelectorAll('thead, tbody')).css('display', '');


            // wrap in $timeout to give table a chance to finish rendering
            $timeout(function () {
                // set widths of columns

                angular.forEach(elem.querySelectorAll('thead tr:first-child th'), function (thElem, i) {
                    //console.log("thelem",angular.element(thElem));
                    var tdElems = elem.querySelector('tbody tr:first-child td:nth-child(' + (i + 1) + ')');
                    var columnWidth = tdElems ? tdElems.offsetWidth : thElem.offsetWidth;
                    //console.log("data",tdElems.offsetWidth, thElem.offsetWidth);
                    //console.log(angular.element(thElem).innerWidth(),angular.element(thElem).outerWidth());
                    if (tdElems) {
                        tdElems.style.width = columnWidth + 'px';
                    }
                    if(type=="resize") {
                        if (thElem) {
                            thElem.style.width = columnWidth + 'px';
                        }
                    }

                });
                // set css styles on thead and tbody
                var theadHt = $('.list-container').find('table thead').height();
                angular.element(elem.querySelectorAll('thead')).css('display', 'block');
                var tbodyHt = $('.list-container').find('.panel-body').height() - theadHt;
                angular.element(elem.querySelectorAll('tbody')).css({
                    'display': 'block',
                    //'height': $('.list-container').find('.panel-body').height()-45+"px" || 'inherit'
                    'min-height':tbodyHt+"px",
                    'height': tbodyHt+"px" || 'inherit'
                });




                // reduce width of last column by width of scrollbar
                var tbody = elem.querySelector('tbody');
                var scrollBarWidth = tbody.offsetWidth - tbody.clientWidth;
                if (scrollBarWidth > 0) {
                    // for some reason trimming the width by 2px lines everything up better
                    scrollBarWidth -= 2;
                    var lastColumn = elem.querySelector('tbody tr:first-child td:last-child');
                    lastColumn.style.width = (lastColumn.offsetWidth - scrollBarWidth) + 'px';
                }

            });

        }
    }
}])