app.service('addressJoiner',['$rootScope', function($rootScope) {

    this.joinAddressLineByLine = function(addressMaster, partyMaster, partyAddress) {

        $rootScope.partyAddressType = partyAddress != undefined ? partyAddress.addressType : null;

        if (addressMaster == undefined || addressMaster == null) {
            addressMaster = {};
        }

        if (partyMaster == undefined || partyMaster == null) {
            return addressMaster;
        }

        addressMaster.addressLine1 = "";
        addressMaster.addressLine2 = "";
        addressMaster.addressLine3 = "";
        addressMaster.addressLine4 = "";
        addressMaster.phone = "";
        addressMaster.mobile = "";
        addressMaster.email = "";
        addressMaster.fullAddress = "";
        addressMaster.contactPerson = "";
        addressMaster.zipCode = "";

        var map = new Map();
        var i = 1;
        if(!partyAddress) return false;
        if (partyAddress.addressLine1 != undefined && partyAddress.addressLine1 != null && partyAddress.addressLine1 != "") {
            map.put(i, partyAddress.addressLine1);
            i = i + 1;
        };

        if (partyAddress.addressLine2 != undefined && partyAddress.addressLine2 != null && partyAddress.addressLine2 != "") {
            map.put(i, partyAddress.addressLine2);
            i = i + 1;
        };

        if (partyAddress.addressLine3 != undefined && partyAddress.addressLine3 != null && partyAddress.addressLine3 != "") {
            map.put(i, partyAddress.addressLine3);
            i = i + 1;
        };

        map.put(i, this.cityStateLine(partyMaster, partyAddress));
        i = i + 1;

        var keys = map.allKeys();
        for (var j = 0; j < keys.length; j++) {
            if (j == 0) {
                addressMaster.addressLine1 = map.get(keys[j]);
            }
            if (j == 1) {
                addressMaster.addressLine2 = map.get(keys[j]);
            }
            if (j == 2) {
                addressMaster.addressLine3 = map.get(keys[j]);
            }
            if (j == 3) {
                addressMaster.addressLine4 = map.get(keys[j]);
            }
        }

        addressMaster.phone = partyAddress.phone;
        addressMaster.mobileNo = partyAddress.mobileNo;
        addressMaster.email = partyAddress.email;
        addressMaster.contactPerson = partyAddress.contactPerson;
        addressMaster.city = partyAddress.cityMaster;
        addressMaster.state = partyAddress.stateMaster;
        addressMaster.zipCode = partyAddress.zipCode;
        addressMaster.country = partyMaster.countryMaster;


        var fullAddress = null;
        if (addressMaster.addressLine1 != null) {
            fullAddress = addressMaster.addressLine1;
        }
        if (addressMaster.addressLine2 != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.addressLine2;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.addressLine2;
            }
        }

        if (addressMaster.addressLine3 != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.addressLine3;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.addressLine3;
            }
        }

        if (addressMaster.addressLine4 != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.addressLine4;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.addressLine4;
            }
        }
        addressMaster.fullAddress = fullAddress;

        return addressMaster;

    };




    this.addressMasterToString = function(addressMaster) {
        if (addressMaster == undefined || (Object.getOwnPropertyNames(addressMaster) != null && Object.getOwnPropertyNames(addressMaster).length == 0)) {
            return "";
        }

        var fullAddress = null;
        if (addressMaster.addressLine1 != undefined && addressMaster.addressLine1 != null) {
            fullAddress = addressMaster.addressLine1;
        }
        if (addressMaster.addressLine2 != undefined && addressMaster.addressLine2 != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.addressLine2;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.addressLine2;
            }
        }

        if (addressMaster.addressLine3 != undefined && addressMaster.addressLine3 != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.addressLine3;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.addressLine3;
            }
        }

        if (addressMaster.addressLine4 != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.addressLine4;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.addressLine4;
            }
        }

        if (addressMaster.city != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.city;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.city;
            }
        }

        if (addressMaster.poBox != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.poBox;
            } else {
                fullAddress = fullAddress + ", " + addressMaster.poBox;
            }
        }


        if (addressMaster.state != null) {
            if (fullAddress == null || fullAddress == '') {
                fullAddress = addressMaster.state;
            } else {
                fullAddress = fullAddress + ",\n" + addressMaster.state;
            }
        }

        addressMaster.fullAddress = fullAddress;

        return addressMaster;

    }



    this.cityStateLine = function(partyMaster, partyAddress) {
        var str = "";

        if (partyAddress.cityMaster != undefined && partyAddress.cityMaster != null && partyAddress.cityMaster != "") {
            str = str + partyAddress.cityMaster.cityName;
        }

        if (partyAddress.stateMaster != undefined && partyAddress.stateMaster != null && partyAddress.stateMaster != "") {
            if (str == "") {
                str = partyAddress.stateMaster.stateCode;
            } else {
                str = str + ", " + partyAddress.stateMaster.stateCode;
            }

        }
        if (partyAddress.zipCode != undefined && partyAddress.zipCode != null && partyAddress.zipCode != "") {
            if (str == "") {
                str = partyAddress.zipCode;
            } else {
                str = str + ", " + partyAddress.zipCode;
            }

        }

        if (partyMaster.countryMaster != undefined && partyMaster.countryMaster != null && partyMaster.countryMaster != "") {
            if (str == "") {
                str = partyMaster.countryMaster.countryName;
            } else {
                str = str + ", " + partyMaster.countryMaster.countryName;
            }

        }

        return str;
    }
}]);