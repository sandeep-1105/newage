app.service('EmptyRowChecker', function() {

	this.isEmptyRow = function(obj) {
		var isempty = true; // empty
		if (!obj) {
			return isempty;
		}
		var k = Object.getOwnPropertyNames(obj);
		for (var i = 0; i < k.length; i++) {
			if (obj[k[i]]) {
				isempty = false; // not empty
				break;
			}
		}
		return isempty;
	}
	
});