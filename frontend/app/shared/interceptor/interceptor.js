
app.factory('httpInterceptor', ['$q', '$rootScope', '$location', function ($q, $rootScope, $location) {

	$rootScope.respCode = null;
	$rootScope.respDesc = "";
	var canceller = $q.defer();
	return {
		request: function (config, TIME_OUT) {
			if ($location.$$url == '/setup/location_setup') {
				console.log('State Name :: ', $location)
				config.headers['SAAS_ID'] = "COM";
			} else {
				config.headers['SAAS_ID'] = localStorage.saasId;
			}
			config.timeout = CONNECTION_TIME_OUT;
			if ((config && config.url && config.url.indexOf('/api/v1/') != -1) && localStorage.saasId == "") {
				config.timeout = canceller.promise;
				canceller.resolve('Cancelled');
			}
			$rootScope.respCode = null;
			$rootScope.respDesc = "";
			if (IS_DEV_ENV && config.url.indexOf('api') > 0) {
				if (!config.url.split("//")[1])
					config.url = "http://" + CONFIG_BASE_URL + config.url;
				else
					config.url = config.url.replace(config.url.split("//")[1].split("/")[0], CONFIG_BASE_URL);

			}

			return config || $q.when(config);
		},
		response: function (response) {
			if (response != "" && response.data != "" && response.data.responseCode != "") {
				$rootScope.respCode = response.data.responseCode;
				$rootScope.respDesc = response.data.responseDescription;
				if (response.data.params != undefined && response.data.params != null && response.data.params.length > 0) {
					$rootScope.respParams = response.data.params;
					console.log("Response params : ", response.data.params);
				}

				$rootScope.respStat = response.status;
			}

			return response || $q.when(response);
		},
		responseError: function (response) {
			console.log('failure response.status:' + response.status);
			if (response.status === 401 && response.data.status === "ERR2") {
				response.data = {
					status: response.data.status,
					error: response.data.error
				};
			} else if (response.status === 401) {
				localStorage.saasId = "";
				localStorage.authorized = false;
				localStorage.userProfile = null;
				localStorage.enum = null;

				$rootScope.authorized = false;
				$rootScope.enum = null;

				$location.path("/logout");
			}
			else if (response.status === 403) {
				response.data = {};
				response.data.status = 'ERR50';
				response.data.error = $rootScope.nls["ERR50"];
			} else if (response.status === 400) {
				response.data = {};
				response.data.status = 400;
				response.data.error = "Bad request sent to the server";
			} else if (response.status === -1) {
				if (response.config.url && API_NAMES) {
					for (var i = 0; i < API_NAMES.length; i++) {
						if (response.config.url.indexOf(API_NAMES[i]) >= 0) {
							return;
						}
					}
				}
				response.data = {};
				response.data.status = -1;
				response.data.error = "Connection timed out";
			} else if (response.status === 500 && response.data.message.indexOf('ERR4') >= 0) {
				response.data = {};
				response.data.status = 'ERR4';
				response.data.error = "Invalid SaaS Id";
			}
			else {
				if (!response.status) {
					response.data = {};
				}
				else {
					response.data.error = "Service Unavailable";
					console.log('failure response.data.status:' + response.data.status);
					console.log('failure response.data.error:' + response.data.error);
				}

			}

			$rootScope.respCode = response.data.status;
			$rootScope.respDesc = response.data.message || response.data.error;
			return $q.reject(response);
		}
	};
}]);




