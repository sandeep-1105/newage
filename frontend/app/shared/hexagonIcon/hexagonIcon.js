/**
 * Created by saravanan on 30/9/16.
 */
app.directive('hmInitIcon',[function() {
    return {
        restrict:'EA',
        scope:{
            customerName:'=',
            saturation:'=?',
            lightness:'=?'

        },
        link:function(scope,elem,attr) {

            function setcolorFn(){

                scope.customerNameA=scope.customerName.split(' ')[0];

                scope.customerNameB=scope.customerName.split(' ')[1];

                console.log("a",scope.customerNameA);
                console.log("b",scope.customerNameB);
                var  char = '';
                var seed = 10;
                var str = scope.customerName;
                for (i = 0; i < str.length; i++) {
                    char = str.charCodeAt(i);
                    seed = char + (seed << 6) + (seed << 16) - seed;
                }

                console.log(" Generated hash for ",scope.customerName, " ---> ",seed);

                var hue = Math.sin(seed);

                console.log("hue value for sign of seed ", hue);
                hue = hue < 0 ? -hue : hue;
                hue = Math.round(hue * 359) + 1;
                console.log("final value of hue is ", hue);
                if(hue>=100 && hue<=130) {
                    hue=hue+40;
                    console.log("100hue vlaue",hue);
                }
                var hslstr = "hsl("+hue+", 80% 40%) !important";
                if(!scope.saturation){
                    scope.saturation = 89;
                }
                if(!scope.lightness){
                    scope.lightness = 71;
                }
                console.log("scope.saturation",scope.saturation);
                console.log("scope.lightness",scope.lightness);
                //var hslstr = "hsl(76, 79%, 72%) !important";
                scope.newColor=hue; //we may adjust the saturation and brightness for better colors later.

            }

            scope.$watch('customerName',function(newValue,oldValue){
                if(newValue){
                    setcolorFn()
                }
            })
        },
        template: '<span class="hexagonCard" style="background: hsl({{newColor}},{{saturation}}%,{{lightness}}%)">' +

        '<span class="hexagonCardTop" style="border-bottom-color: hsl({{newColor}},{{saturation}}%,{{lightness}}%)"></span>'+
        '{{customerNameA|limitTo:1}}' +
        '{{customerNameB|limitTo:1}}' +
        '<span class="hexagonCardBottom" style="border-top-color: hsl({{newColor}},{{saturation}}%,{{lightness}}%)"></span>'+
        '</span>'
    };
}]);