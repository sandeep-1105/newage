
/**
 * 
 */

app.factory("SequenceFactoryForDO",['$resource', function($resource) {
		return $resource("/api/v1/shipment/genDoNum/:documentId", {}, {
			fetch : {
				method : 'GET',
				params : {
					documentId : ''
				},
				isArray : false
			}
		});
}]);


app.factory("SequenceFactoryForCAN",['$resource', function($resource) {
	return $resource("/api/v1/shipment/genCanNum/:documentId", {}, {
		fetch : {
			method : 'GET',
			params : {
				documentId : ''
			},
			isArray : false
		}
	});
}]);


