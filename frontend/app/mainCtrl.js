app.controller('mainCtrl', ['$rootScope', '$scope', '$location', '$timeout', '$window',
    '$modal', 'AuthApi', '$stateParams', '$state', 'GoToUnfilledFormNavigationService', 'RecentHistoryService',
    'GoToRecentHistoryNavigationService', 'LogoutApi', 'CurrencyRateByBase', 'ChangeProfileServer', 'RegExpAll',
    'AppEnumApi', 'AppNlsApi', 'ForgotApi', 'UpdateForgotApi', 'ngDialog', 'recentHistoryFactory', 'RecentHistoryGroupMapService',
    'appMetaFactory', 'UnitMasterGetByCode', 'appConstant', 'LogoGetByLocation', 'Notification', 'MenuContentService',
    'roleConstant', 'reportMasterFactory', 'UserRecordAccessLevel',
    function($rootScope, $scope, $location, $timeout, $window,
        $modal, AuthApi, $stateParams, $state, GoToUnfilledFormNavigationService, RecentHistoryService,
        GoToRecentHistoryNavigationService, LogoutApi, CurrencyRateByBase, ChangeProfileServer, RegExpAll,
        AppEnumApi, AppNlsApi, ForgotApi, UpdateForgotApi, ngDialog, recentHistoryFactory, RecentHistoryGroupMapService,
        appMetaFactory, UnitMasterGetByCode, appConstant, LogoGetByLocation, Notification, MenuContentService,
        roleConstant, reportMasterFactory, UserRecordAccessLevel) {


        $rootScope.weightDecimalPoint = 2;
        $rootScope.totalRecordFetchPerRequest = 10;
        $rootScope.baseURL = $location.protocol() + '://' + $location.host() + ($location.port() != null ? ':' + $location.port() : '');
        $rootScope.recentBrowseHistoryList = [];
        $rootScope.userLogged = false;
        $scope.forgotPassword = null;
        $scope.forgotPasswordDto = {};
        $rootScope.unsavedByGroupForm = new Map();
        $rootScope.dynamicFormFieldMap = new Map();
        // menu initial
        $scope.menuState = true;
        $scope.sidebarState = false;
        $scope.toolbarState = false;

        // Root Scope Data for History Maintain & Multi Screen Navigation
        $rootScope.unfinishedData = {};
        $rootScope.unfinishedFormHistoryList = [];
        $rootScope.recentHistoryList = [];
        $rootScope.clientMessage = null;
        $rootScope.clear = function() {
            console.log("Clear Method is called....!");
            $rootScope.clientMessage = null;
        }


        Utils.showDebugLogs();

        $rootScope.ngScrollConfig = {
            "cursorcolor": '#3F8DEB',
            "cursorwidth": '8px',
            "smoothscroll": true,
            "autohidemode": false,
            "horizrailenabled": false
        };
        $rootScope.ngScrollConfigScroll = {
            "cursorcolor": '#3F8DEB',
            "cursorwidth": '8px',
            "smoothscroll": true,
            "autohidemode": false
        };

        $rootScope.datePickerOptions = {
            format: "DD-MM-YYYY",
            useCurrent: false
        };

        $rootScope.dateTimePickerOptions = {
            format: "DD-MM-YYYY HH:mm",
            useCurrent: false,
            sideBySide: true,
            viewDate: moment()
        };

        $rootScope.timePickerOptions = {
            format: "HH:mm",
            useCurrent: true
        };

        $rootScope.enum = {};
        $rootScope.nls = {};
        $rootScope.appRegExp = {};

        $rootScope.navigate1 = "";
        $rootScope.navigate2 = "";
        $rootScope.navigate3 = "";

        $scope.loginDto = {};
        $rootScope.userProfile = {};

        $rootScope.respClear = function() {
            $rootScope.respCode = null;
            $rootScope.respDesc = "";
        };

        $rootScope.showResponseForConcurrentException = function(entityType, entityId, entityUid, successState, errorState, successStateObj) {
            $rootScope.mainpreloder = false;
            var newScope = $rootScope.$new();
            newScope.entityUid = entityUid;
            newScope.entityType = entityType;

            ngDialog.openConfirm({
                template: '<p>{{entityType}} {{entityUid}} is updated by another user. Please reload by pressing OK.</p> ' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Ok </button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default top_9999'
            }).then(function(value) {
                $state.go(successState, successStateObj);
            }, function(value) {
                $state.go(errorState, {
                    submitAction: 'Cancelled'
                });
            });
        }

        $rootScope.susClear = function() {
            $rootScope.susDesc = null;
        };

        $rootScope.populatCompanyChangeInterceptor = function(companyMaster) {
            $rootScope.userProfile.selectedCompany = companyMaster;
            $rootScope.populatCompanyChange();
        }
        $rootScope.populatCompanyChange = function() {
            $rootScope.userProfile.profileCompanyLocationlist = [];
            $rootScope.userProfile.selectedUserLocation = null;

            for (var i = 0; i < $rootScope.userProfile.userCompanyList.length; i++) {

                if ($rootScope.userProfile.selectedCompany.id == $rootScope.userProfile.userCompanyList[i].companyMaster.id) {

                    for (var j = 0; j < $rootScope.userProfile.userCompanyList[i].userCompanyLocationList.length; j++) {

                        if ($rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster.status != 'Active')
                            continue;

                        $rootScope.userProfile.profileCompanyLocationlist.push($rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster);

                        if ($rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].yesNo === true ||
                            $rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].yesNo === 'Yes') {
                            $rootScope.userProfile.selectedUserLocation = $rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster;
                        }


                    }
                }
            }

            $scope.changeProfile();
        }


        $rootScope.populateUserProfileData = function() {

            $rootScope.userProfile.profileCompanyList = [];

            for (var i = 0; i < $rootScope.userProfile.userCompanyList.length; i++) {

                if ($rootScope.userProfile.userCompanyList[i].companyMaster.status != 'Active')
                    continue;

                $rootScope.userProfile.profileCompanyList.push($rootScope.userProfile.userCompanyList[i].companyMaster);

                if ($rootScope.userProfile.selectedCompany.id == $rootScope.userProfile.userCompanyList[i].companyMaster.id) {
                    $rootScope.userProfile.selectedCompany = $rootScope.userProfile.userCompanyList[i].companyMaster;

                    $rootScope.userProfile.profileCompanyLocationlist = [];

                    for (var j = 0; j < $rootScope.userProfile.userCompanyList[i].userCompanyLocationList.length; j++) {

                        if ($rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster.status != 'Active')
                            continue;

                        $rootScope.userProfile.profileCompanyLocationlist.push($rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster);
                        if ($rootScope.userProfile.selectedUserLocation != null && $rootScope.userProfile.selectedUserLocation.id == $rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster.id) {
                            $rootScope.userProfile.selectedUserLocation = $rootScope.userProfile.userCompanyList[i].userCompanyLocationList[j].locationMaster;
                        }
                    }

                }
            }

            if ($rootScope.userProfile != null && $rootScope.userProfile.selectedUserLocation != null &&
                $rootScope.userProfile.selectedUserLocation.countryMaster != null && $rootScope.userProfile.selectedUserLocation.countryMaster.id != null) {

                appMetaFactory.country.dynamicFields({
                        countryId: $rootScope.userProfile.selectedUserLocation.countryMaster.id
                    }, function(data) {
                        $rootScope.dynamicFields = data.responseObject;
                        $rootScope.dynamicFormFieldMap = new Map();
                        if ($rootScope.dynamicFields != undefined && $rootScope.dynamicFields != null && $rootScope.dynamicFields.length > 0) {
                            for (var it = 0; it < $rootScope.dynamicFields.length; it++) {
                                $rootScope.dynamicFormFieldMap.put($rootScope.dynamicFields[it].fieldName, $rootScope.dynamicFields[it].showField);
                            }
                        }
                    },
                    function(error) {
                        console.log("Error");
                    });
            }
            if ($rootScope.userProfile.selectedUserLocation == null) {
                $rootScope.datePickerOptions.format = "DD-MM-YYYY";
                $rootScope.dateTimePickerOptions.format = "DD-MM-YYYY HH:mm";
            } else {
                $rootScope.datePickerOptions.format = $rootScope.userProfile.selectedUserLocation.dateFormat;
                $rootScope.dateTimePickerOptions.format = $rootScope.userProfile.selectedUserLocation.dateTimeFormat;
                $rootScope.datePickerOptions.widgetParent = "body";
            }
            try {
                localStorage.userProfile = JSON.stringify($rootScope.userProfile);
            } catch (e) {
                console.log("exception in storage item");
            }

            $rootScope.loadMenu();



            // Load Menu Array Data After Login
        }

        $scope.changeProfileModuleData = function() {
            //      
            if ($stateParams.module != undefined && $stateParams.module != null && $stateParams.module != "") {
                console.log("$stateParams.module -- ", $stateParams.module);
                switch ($stateParams.module) {
                    case "Enquiry":
                        $scope.chkCurrentStateAndGo('layout.salesEnquiry');
                        break;
                    case "Quotation":
                        $scope.chkCurrentStateAndGo('layout.salesQuotation');
                        break;
                    case "Shipment":
                        $scope.chkCurrentStateAndGo('layout.crmShipment');
                        break;
                    case "Consol":
                        $scope.chkCurrentStateAndGo('layout.airNewConsol');
                        break;
                    case "Party":
                        $scope.chkCurrentStateAndGo('layout.party');
                        break;
                    default:
                        console.log("No need to change state");
                }
            }
            if ($state.current.name === 'layout.myTask') $state.reload();
            else $state.go('layout.myTask');
        }
        $scope.chkCurrentStateAndGo = function(stateName) {
            var count = 11;
            if ($state.current.name == stateName) {
                console.log("Same State");
                count = $stateParams.refCount + 1;
            }
            $state.go(stateName, {
                refCount: count
            });
        }
        $scope.changeProfileInterceptor = function(locationData) {
            $rootScope.userProfile.selectedUserLocation = locationData;
            $scope.changeProfile();
        }
        $scope.changeProfile = function() {

            if ($rootScope.userProfile.selectedUserLocation != null) {
                ChangeProfileServer.get({
                        locationId: $rootScope.userProfile.selectedUserLocation.id
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("Profile Changed Successfully")

                            $rootScope.userProfile.selectedCompany.id = data.responseObject.selectedUserLocation.companyMaster.id;
                            $rootScope.userProfile.selectedCompany.companyName = data.responseObject.selectedUserLocation.companyMaster.companyName;
                            $rootScope.userProfile.selectedCompany.status = data.responseObject.selectedUserLocation.companyMaster.status;

                            $rootScope.userProfile.selectedUserLocation.id = data.responseObject.selectedUserLocation.id;
                            $rootScope.userProfile.selectedUserLocation.locationName = data.responseObject.selectedUserLocation.locationName;
                            $rootScope.userProfile.selectedUserLocation.status = data.responseObject.selectedUserLocation.status;
                            $rootScope.userProfile.selectedUserLocation.tmpPartyMaster = data.responseObject.selectedUserLocation.tmpPartyMaster;

                            /*$scope.locationLogo();*/

                            $rootScope.appMasterData = data.responseObject.appMasterData;
                            localStorage.appMasterData = JSON.stringify($rootScope.appMasterData);

                            $rootScope.populateUserProfileData();

                            CurrencyRateByBase.get({
                                fromCurrencyId: data.responseObject.selectedUserLocation.countryMaster.currencyMaster.id
                            }, function(data) {
                                if (data.responseCode == 'ERR0') {
                                    $rootScope.baseCurrenyRate = data.responseObject.searchResult;
                                    localStorage.baseCurrenyRate = JSON.stringify($rootScope.baseCurrenyRate);
                                    console.log("Currency Rate Loaded Successfully : ")
                                } else {
                                    console.log("Currency Rate Loaded failed")
                                }
                            }, function(error) {
                                console.log("Currency Rate Loaded failed : " + error)
                            });

                            localStorage.userProfile = JSON.stringify($rootScope.userProfile);

                            $scope.changeProfileModuleData();
                        } else {
                            console.log("Profile Changed Failed " + data.responseDescription)
                        }
                    },
                    function(error) {
                        console.log("Profile Changed Failed : " + error)
                    });
            }


        }

        $scope.locationLogo = function() {
            LogoGetByLocation.get({
                    locationid: $rootScope.userProfile.selectedUserLocation.id
                }, function(data) {
                    if (data.responseCode == 'ERR0' && data.responseObject.encodedLogo != null) {
                        $rootScope.userProfile.selectedUserLocation.encodedLogo = data.responseObject.encodedLogo;
                    } else {
                        console.log("Logo is not avialable")
                    }
                },
                function(error) {
                    console.log("Consol get Failed : " + error)
                });
        }

        $scope.login = function() {
            localStorage.saasId = $scope.loginDto.saasId;
            $scope.mainpreloder = true;
            AuthApi.save($scope.loginDto).$promise.then(function(data) {
                $scope.mainpreloder = false;
                if (data.responseCode == 'ERR0') {
                    $rootScope.userLogged = true;
                    localStorage.authorized = true;
                    $rootScope.userProfile = data.responseObject;

                    var passwordExpiresOn = data.responseObject.passwordExpiresOn;
                    if (passwordExpiresOn != null && passwordExpiresOn != undefined && passwordExpiresOn != "") {
                        var expiry = moment(passwordExpiresOn);
                        var today = new Date()
                        var currentDate = new Date().setDate(today.getDate());
                        var currentDay = moment(currentDate);
                        if (currentDay >= expiry) {
                            Notification.error("Your password has been expired.Please contact admin team!");
                            //Notification.error($rootScope.nls["ERR2617"]);
                            return;
                        }
                    } else {
                        console.log("passwordExpiresOn is not provided in userCreation to check password expiry");
                    }

                    $rootScope.appMasterData = data.responseObject.appMasterData;
                    localStorage.appMasterData = JSON.stringify($rootScope.appMasterData);
                    localStorage.trackId = data.trackId;

                    AppEnumApi.get(function(data) {
                        $rootScope.enum = data.responseObject;
                        localStorage.enum = JSON.stringify($rootScope.enum);
                    });

                    AppNlsApi.get({
                        language: 'English'
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            $rootScope.nls = data.responseObject;
                            console.log("NLS Loaded Successfully : ")
                        } else {
                            console.log("NLS Loaded failed")
                        }
                    }, function(error) {
                        console.log("NLS Loaded failed : " + error)
                    });

                    RegExpAll.get(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $rootScope.appRegExp = data.responseObject;
                            console.log("appRegExp Loaded Successfully : ")
                        } else {
                            console.log("appRegExp Loaded failed")
                        }
                    }, function(error) {
                        console.log("appRegExp Loaded failed : " + error)
                    });


                    /*  UserRecordAccessLevel.get({id :  $rootScope.userProfile.id }, function(data) {
                        if (data.responseCode == 'ERR0') {
                             $rootScope.userRecordAccess = data.responseObject;
                             console.log("UserRecordAccessLevel Loaded Successfully", $rootScope.userRecordAccess);
                         } else {
                             console.log("UserRecordAccessLevel Loaded failed", data.responseDescription);
                         }
                    }, function(error) {
                        console.log("UserRecordAccessLevel Loaded failed : " , error)
                    });*/

                    $rootScope.populateUserProfileData();



                    $rootScope.unfinishedFormHistoryList = [];
                    localStorage.unfinishedFormHistoryList = null;

                    if ($rootScope.userProfile.selectedUserLocation.currencyMaster != undefined && $rootScope.userProfile.selectedUserLocation.currencyMaster != null) {
                        CurrencyRateByBase.get({
                            fromCurrencyId: $rootScope.userProfile.selectedUserLocation.currencyMaster.id
                        }, function(data) {
                            if (data.responseCode == 'ERR0') {
                                $rootScope.baseCurrenyRate = data.responseObject.searchResult;
                                localStorage.baseCurrenyRate = JSON.stringify($rootScope.baseCurrenyRate);
                                console.log("Currency Rate Loaded Successfully : ")
                            } else {
                                console.log("Currency Rate Loaded failed")
                            }
                        }, function(error) {
                            console.log("Currency Rate Loaded failed : " + error)
                        });
                    }

                    // $rootScope.userProfile.language.languageName

                    reportMasterFactory.searchAll.get(function(data) {
                        if (data.responseCode == "ERR0") {
                            $rootScope.reportMap = data.responseObject;
                            localStorage.reportMapJson = JSON.stringify($rootScope.reportMap);
                        } else {

                        }
                    }, function(error) {
                        console.log("Error while fetching Report type List : ", error)

                    });


                    // $location.path('/home');
                    console.log("Goto -dashboard");
                    recentHistoryFactory.list(
                        function(data) {
                            console.log("recentHistoryFactory. ------- ", data);
                            if (data.responseCode == 'ERR0') {
                                $rootScope.recentBrowseHistoryList = data.responseObject.content;
                                $rootScope.testRecentHistoryDayMapData = [];
                                angular.copy($rootScope.recentBrowseHistoryList, $rootScope.testRecentHistoryDayMapData);
                                $rootScope.testRecentHistoryDayMapData.messages = $rootScope.convertToDayWiseData($rootScope.testRecentHistoryDayMapData, 'groupByDate', true);
                                RecentHistoryGroupMapService.form();
                            } else {
                                $rootScope.recentBrowseHistoryList = [];
                            }
                        },
                        function(error) {
                            console.log("REcent History Loaded failed : ", error)
                        });
                    $state.go("layout.myTask");
                    var notificationLimit = $rootScope.appMasterData['password.expiry.notification.limit'];
                    if (notificationLimit != null && notificationLimit != undefined && notificationLimit != "") {
                        var expiry = moment(passwordExpiresOn);
                        var expiryDate = new Date().setDate(today.getDate() + parseInt(notificationLimit));
                        var toBeExpiry = moment(expiryDate);
                        if (toBeExpiry >= expiry) {
                            $timeout(function() {
                                //Notification.warning("Your password will expire soon!");  
                                Notification.warning($rootScope.nls["ERR2618"] + " " + passwordExpiresOn);
                            }, 800);
                        }
                    } else {
                        console.log("Default master doesn't provide a value to 'password.expiry.bandwidth' to check password expiry");
                    }
                } else {
                    localStorage.saasId = "";
                    console.log("Login Failed : " + data.responseCode + data.responseDescription)
                }
            }, function(error) {
                console.log("Login Failed : ", error)
                $scope.mainpreloder = false;
                $rootScope.userLogged = false;
                localStorage.saasId = "";
                if (error.data && error.data.responseDescription) {
                    Notification.error(error.data.responseDescription);
                }
            });
        }

        $scope.logout = function() {
            LogoutApi.get(function(data) {
                console.log("Logout Called")
                localStorage.savedData = null;
                localStorage.saasId = "";
                localStorage.authorized = false;
                localStorage.userProfile = null;
                localStorage.enum = null;
                localStorage.nls = null;
                localStorage.appRegExp = null;
                $scope.loginDto = {};
                $rootScope.setNavigate("");
                $rootScope.userLogged = false;

                localStorage.unfinishedFormHistoryList = null;
                $rootScope.unfinishedFormHistoryList = [];
                localStorage.recentHistoryList = null;
                $rootScope.appMasterData = null;
                localStorage.appMasterData = null;
                $rootScope.baseCurrenyRate = null;
                localStorage.baseCurrenyRate = null;

                $location.path('/logout');
            });
        }

        $scope.goToForgotPassword = function() {
            $scope.forgotPassword = 'FORGOT';
            $scope.forgotPasswordDto = {};
        }

        $scope.goToLogin = function() {
            $scope.forgotPassword = null;
            $scope.forgotPasswordDto = {};
        }

        $scope.sendForgotPasswordLink = function() {
            ForgotApi.save($scope.forgotPasswordDto).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.goToLogin();
                } else {
                    console.log("Login Failed : " + data.responseCode + data.responseDescription)
                }
            }, function(error) {
                console.log("Login Failed : ")
            });
        }

        if (getParameterByName('rpt') != null && getParameterByName('rpt').length != 0 &&
            getParameterByName('rphid') != null && getParameterByName('rphid').length != 0 &&
            getParameterByName('sid') != null && getParameterByName('sid').length != 0) {
            $scope.forgotPassword = 'CONFIRM';
            $scope.forgotPasswordDto = {};
            $scope.forgotPasswordDto.resetPasswordToken = getParameterByName('rpt');
            $scope.forgotPasswordDto.resetPasswordId = getParameterByName('rphid');
            $scope.forgotPasswordDto.resetPasswordSid = getParameterByName('sid')
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        $scope.updatePassword = function() {
            UpdateForgotApi.save($scope.forgotPasswordDto).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.goToLogin();
                } else {
                    console.log("Login Failed : " + data.responseCode + data.responseDescription)
                }
            }, function(error) {
                console.log("Login Failed : ")
            });
        }

        $scope.goToMaster = function() {
            $location.path("home");
            $rootScope.setNavigate1("Master");
        }

        $rootScope.setNavigate = function(arg) {
            $rootScope.navigate1 = arg;
            localStorage.navigate1 = $rootScope.navigate1;
            $rootScope.setNavigate1("");
        }

        $rootScope.setNavigate1 = function(arg) {
            $rootScope.navigate1 = arg;
            localStorage.navigate1 = $rootScope.navigate1;
            $rootScope.setNavigate2("");
        }

        $rootScope.setNavigate2 = function(arg) {
            $rootScope.navigate2 = arg;
            localStorage.navigate2 = $rootScope.navigate2;
            $rootScope.setNavigate3("");
        }

        $rootScope.setNavigate3 = function(arg) {
            $rootScope.navigate3 = arg;
            localStorage.navigate3 = $rootScope.navigate3;
        }


        $rootScope.sendApiStartDateTime = function(parsedate, isSecondsAllowed) {
            // console.log("parsedate:"+typeof parsedate+","+parsedate)

            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            } else if (typeof parsedate == "string" && parsedate.endsWith(" 00:00:00")) {
                parsedate = parsedate.replace(" 00:00:00", "");
            }

            if (typeof parsedate == "string") {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat).format("YYYY-MM-DD") + " 00:00:00";
            } else {
                if (parsedate == null)
                    return null;
                if (typeof parsedate == "object") {
                    if(!isSecondsAllowed) return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat).format("YYYY-MM-DD") + " 00:00:00";
                    else return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateTimeFormat).format("YYYY-MM-DD HH:mm:ss");
                } else {
                    var date = new Date(parsedate);
                    var day = date.getDate();
                    var monthIndex = date.getMonth();
                    var year = date.getFullYear();
                    return year + '-' + (monthIndex + 1) + '-' + day + ' 00:00:00';
                }
            }
        }

        $rootScope.sendApiEndDateTime = function(parsedate) {
            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            } else if (typeof parsedate == "string" && parsedate.endsWith(" 00:00:00")) {
                parsedate = parsedate.replace(" 00:00:00", "");
            }

            if (typeof parsedate == "string") {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat).format("YYYY-MM-DD") + " 23:59:59";
            } else {
                if (parsedate == null)
                    return null;
                if (typeof parsedate == "object") {
                    return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat).format("YYYY-MM-DD") + " 23:59:59";
                } else {
                    var date = new Date(parsedate);
                    var day = date.getDate();
                    var monthIndex = date.getMonth();
                    var year = date.getFullYear();
                    return year + '-' + (monthIndex + 1) + '-' + day + ' 23:59:59';
                }
            }
        };

        $rootScope.sendApiDateAndTime = function(parsedate) {
            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            }
            if (typeof parsedate == "string") {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateTimeFormat).format("YYYY-MM-DD HH:mm:ss");
            } else {
                if (parsedate == null)
                    return null;

                var date = new Date(parsedate);
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                var hour = date.getHours();
                var min = date.getMinutes();
                var sec = date.getSeconds();
                return year + '-' + (monthIndex + 1) + '-' + day + ' ' + hour + ":" + min + ":" + sec + "'";
            }
        }

        $rootScope.sendApiDate = function (parsedate) {
            var date = new Date(parsedate);
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            return year + '-' + (monthIndex + 1) + '-' + day;   
        }

        $rootScope.getDate = function(parsedate) {
            console.log("typeof parsedate : " + typeof parsedate)
            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            }

            if (typeof parsedate == "string") {
                return moment(parsedate, "YYYY-MM-DD HH:mm:ss");
            } else {
                return moment(parsedate, "YYYY-MM-DD HH:mm:ss");
            }

        }


        $rootScope.dateToString = function(parsedate, isTimeFormat) {
            if (parsedate == undefined || parsedate == null || $rootScope.userProfile.selectedUserLocation == undefined ||
                $rootScope.userProfile.selectedUserLocation.dateTimeFormat == undefined || $rootScope.userProfile.selectedUserLocation.dateTimeFormat == null)
                return null;

            var date;
            if (!isTimeFormat)
                date = moment(parsedate).format($rootScope.userProfile.selectedUserLocation.dateFormat);
            else
                date = moment(parsedate).format($rootScope.userProfile.selectedUserLocation.dateTimeFormat);
            if (date == undefined || date == "Invalid date") {
                return parsedate;
            } else {
                return date;
            }
        }


        $rootScope.dateAndTimeToString = function(parsedate) {
            if (parsedate == undefined || parsedate == null || $rootScope.userProfile.selectedUserLocation == undefined ||
                $rootScope.userProfile.selectedUserLocation.dateTimeFormat == undefined || $rootScope.userProfile.selectedUserLocation.dateTimeFormat == null)
                return null;
            var date = moment(parsedate).format($rootScope.userProfile.selectedUserLocation.dateTimeFormat);
            if (date == undefined || date == "Invalid date") {
                return parsedate;
            } else {
                return date;
            }
        }

        $rootScope.convertToDate = function(parsedate) {
            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string") {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat);
            } else {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat);
            }
        }

        $rootScope.convertToDateTime = function(parsedate) {
            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string") {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateTimeFormat);
            } else {
                return moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateTimeFormat);
            }
        }

        $rootScope.editDisplayDate = function(parsedate) {
            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            }

            if (typeof parsedate == "string") {
                parsedate = moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateFormat);
            }
            return moment(parsedate).format($rootScope.userProfile.selectedUserLocation.dateFormat);
        }

        $rootScope.editDisplayDateTime = function(parsedate) {
            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            }

            if (typeof parsedate == "string") {
                parsedate = moment(parsedate, $rootScope.userProfile.selectedUserLocation.dateTimeFormat);
            }
            return moment(parsedate).format($rootScope.userProfile.selectedUserLocation.dateTimeFormat);
        }

        $rootScope.getTodayDate = function(parsedate) {
            return moment(parsedate).format('DD-MMM-YYYY');
        }

        $scope.recentDocsToggle = false;
        $scope.openMenu = function() {
            console.log("openMenu");
            $scope.menuState = $scope.menuState === false ? true : false;
            $scope.sidebarState = true;
            $scope.recentDocHide();
        }

        $scope.sidebarOpenMenu = function() {
            console.log("sidebarOpenMenu");
            $scope.menuState = $scope.menuState === false ? true : false;
            $scope.sidebarState = false;
            $scope.recentDocHide();
        }

        $rootScope.loadMenu = function() {
            console.log("loadMenu");
            $timeout(function() {
                $rootScope.mainMenuArr = MenuContentService.getMenuContent();
            }, 100);
        }

        $scope.openMenuLink = function(link) {
            console.log("openMenuLink", link);
            $scope.menuState = true;
            $scope.sidebarState = false;
            $scope.toolbarState = false;
            $location.path(link);
        }

        var level1 = false;
        var level3 = false;
        $scope.openSubMenu = function(subData, level) {
            console.log("openSubMenu", subData, level);
            $scope.toolbarState = true;
            $scope.sidebarState = false;
            $scope.oldMenuObj = $scope.subMenuObj;
            $scope.subMenuObj = subData;
            $scope.activeState = subData.activeMenu;

            if (level == "level1") {
                level1 = true;
            } else {
                level1 = false;
            }

            if (level == "level3") {
                level3 = true;
            } else {
                level3 = false;
            }
            $scope.menuState = false;
        }

        $scope.backSubmenu = function() {
            console.log("backSubmenu");
            if (level3 == true) {
                $scope.sidebarState = true;
                $scope.toolbarState = true;
                $scope.openSubMenu($scope.oldMenuObj, "level2");
                level3 = false;
            } else {
                $scope.sidebarState = true;
                $scope.toolbarState = false;
            }
        }

        $scope.navigateMenu = function(link) {

            console.log("navigateMenu");
            $scope.menuState = true;
            $scope.sidebarState = false;
            $scope.toolbarState = false;
            $rootScope.naviGotoStateName = link;
            $scope.recentDocHide();
            $state.go(link);



        }

        $scope.listNavigateMenu = function(menuObject) {
            if ($rootScope.roleAccess(menuObject.accessList)) {
                console.log("navigateMenu");
                $scope.menuState = true;
                $scope.sidebarState = false;
                $scope.toolbarState = false;
                $rootScope.naviGotoStateName = menuObject.listLink;
                $scope.recentDocHide();
                $state.go(menuObject.listLink);
            }


        }

        $scope.addNavigateMenu = function(menuObject) {
            if ($rootScope.roleAccess(menuObject.accessAdd)) {
                console.log("addnavigateMenu");
                $scope.menuState = true;
                $scope.sidebarState = false;
                $scope.toolbarState = false;
                $rootScope.naviGotoStateName = menuObject.operationLink;
                $scope.recentDocHide();
                $state.go(menuObject.operationLink);
            }

        }

        $rootScope.ifDataElseHifen = function(data) {

            if (data == undefined || data == null || data == "") {

                return '-';

            } else {
                return data;
            }

        }

        $scope.overlayClick = function() {
            /*  if(!$scope.menuState){
                  if($scope.sidebarState){
                      $scope.sidebarState = false;
                      $scope.menuState = true;
                  }else if($scope.toolbarState && !$scope.sidebarState){
                      $scope.toolbarState = false;
                      if(level1) {
                          $scope.sidebarState = false;
                          $scope.menuState = true;
                      }else{
                          $scope.sidebarState = true;
                          $scope.menuState = false;
                      }
                  }
              }*/
            $scope.menuState = true;
            $scope.sidebarState = true;
            $scope.recentDocsToggle = false;
        }


        $scope.recentDoc = function() {
            $scope.recentDocsToggle = !$scope.recentDocsToggle;
            $scope.menuState = true;
            $scope.sidebarState = true;
        }

        $scope.recentDocHide = function() {
            $scope.recentDocsToggle = false;
        }

        function mappingUnitCalculate(calculationType, calculationValue, actualValue) {
            var retvalue = 1;

            if (calculationType == 'ADDITION') {
                retvalue = actualValue + calculationValue;
            } else if (calculationType == 'SUBTRACTION') {
                retvalue = actualValue - calculationValue;
            } else if (calculationType == 'MULTIPLICATION') {
                retvalue = actualValue * calculationValue;
            } else if (calculationType == 'DIVISION') {
                retvalue = actualValue / calculationValue;
            }

            return retvalue;
        }

        $rootScope.unitCalc = function(unit, grossWeight, volumeWeight, pieces, volume, chargeableWeight) {
            var retValue = 1;
            var mappingValue1 = null;
            var mappingValue2 = null;

            if (unit != null && unit.unitType == "Unit") {
                if (unit.mappingUnit1 != null && unit.mappingUnit1 == 'GROSS_WEIGHT') {
                    mappingValue1 = mappingUnitCalculate(unit.calcType1, unit.calcValue1, grossWeight);
                } else if (unit.mappingUnit1 != null && unit.mappingUnit1 == 'VOLUME_WEIGHT') {
                    mappingValue1 = mappingUnitCalculate(unit.calcType1, unit.calcValue1, volumeWeight);
                } else if (unit.mappingUnit1 != null && unit.mappingUnit1 == 'PIECES') {
                    mappingValue1 = mappingUnitCalculate(unit.calcType1, unit.calcValue1, pieces);
                } else if (unit.mappingUnit1 != null && unit.mappingUnit1 == 'VOLUME') {
                    mappingValue1 = mappingUnitCalculate(unit.calcType1, unit.calcValue1, volume);
                } else if (unit.mappingUnit1 != null && unit.mappingUnit1 == 'CHARGEABLE_UNIT') {
                    mappingValue1 = mappingUnitCalculate(unit.calcType1, unit.calcValue1, chargeableWeight);
                }

                if (unit.mappingUnit2 != null && unit.mappingUnit2 == 'GROSS_WEIGHT') {
                    mappingValue2 = mappingUnitCalculate(unit.calcType2, unit.calcValue2, grossWeight);
                } else if (unit.mappingUnit2 != null && unit.mappingUnit2 == 'VOLUME_WEIGHT') {
                    mappingValue2 = mappingUnitCalculate(unit.calcType2, unit.calcValue1, volumeWeight);
                } else if (unit.mappingUnit2 != null && unit.mappingUnit2 == 'PIECES') {
                    mappingValue2 = mappingUnitCalculate(unit.calcType2, unit.calcValue1, pieces);
                } else if (unit.mappingUnit2 != null && unit.mappingUnit2 == 'VOLUME') {
                    mappingValue2 = mappingUnitCalculate(unit.calcType2, unit.calcValue1, volume);
                } else if (unit.mappingUnit2 != null && unit.mappingUnit2 == 'CHARGEABLE_UNIT') {
                    mappingValue2 = mappingUnitCalculate(unit.calcType2, unit.calcValue1, chargeableWeight);
                }

                if (mappingValue2 != null) {
                    if (mappingValue1 > mappingValue2)
                        retValue = mappingValue1;
                    else
                        retValue = mappingValue2;
                } else {
                    retValue = mappingValue1;
                }
            } else {
                retValue = 1;
            }
            return retValue;
        }

        $rootScope.amtRegularExp = function(currency) {

            if (currency === undefined || currency === null || currency.decimalPoint === undefined || currency.decimalPoint === null) {
                return appConstant.Reg_Exp_RATE_THREE_DECIMAL;
            }

            if (currency.decimalPoint === "3" || currency.decimalPoint === 3) {
                return appConstant.Reg_Exp_RATE_THREE_DECIMAL;
            } else {
                return appConstant.Reg_Exp_RATE_TWO_DECIMAL;
            }
        }

        $rootScope.currencyRoeFormat = function(currencyMaster, amount) {
            if (amount === undefined || amount === null || amount === '')
                return null;

            if (currencyMaster == undefined || currencyMaster == null || currencyMaster == "")
                return null;

            return amount.toLocaleString(currencyMaster.countryMaster.locale, {
                minimumFractionDigits: currencyMaster.decimalPoint,
                maximumFractionDigits: 6
            })
        }

        $rootScope.percentageFormat = function(amount) {

            if (amount === undefined || amount === null || amount === '')
                return null;


            return amount.toLocaleString("en-IN", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            })
        }

        $rootScope.currencyFormat = function(currencyMaster, amount) {

            if (amount === undefined || amount === null || amount === '')
                return null;

            if (currencyMaster == undefined || currencyMaster == null || currencyMaster == "")
                return null;

            if (currencyMaster.countryMaster == undefined || currencyMaster.countryMaster == null || currencyMaster.decimalPoint == undefined || currencyMaster.decimalPoint == null || currencyMaster.countryMaster.locale == undefined) {

                return amount;
            }

            return amount.toLocaleString(currencyMaster.countryMaster.locale, {
                minimumFractionDigits: currencyMaster.decimalPoint,
                maximumFractionDigits: currencyMaster.decimalPoint
            })
        }

        $rootScope.currencyFormatDisplay = function(locale, decimalPoint, amount) {

            if (amount === undefined || amount === null || amount === '')
                return null;

            if (locale == undefined || locale == null || locale == "")
                return null;

            if (decimalPoint == undefined || decimalPoint == null || decimalPoint == "")
                return null;

            return amount.toLocaleString(locale, {
                minimumFractionDigits: decimalPoint,
                maximumFractionDigits: decimalPoint
            })
        }

        $rootScope.currencyFormatDecimalPoint = function(decimalPoint, locale, amount) {

            if (amount === undefined || amount === null || amount === '')
                return null;

            if (decimalPoint == undefined || decimalPoint == null || decimalPoint == "")
                return null;

            return amount.toLocaleString(locale, {
                minimumFractionDigits: decimalPoint,
                maximumFractionDigits: decimalPoint
            })
        }

        $rootScope.integerFormat = function(number, locale) {
            if (number === undefined || number === null || number === '')
                return null;
            return number.toLocaleString(locale, {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            })
        }

        $rootScope.weightFormat = function(weight, locale) {
            if (weight === undefined || weight === null || weight === '' || isNaN(weight))
                return null;
            return weight.toLocaleString(locale, {
                minimumFractionDigits: $rootScope.weightDecimalPoint,
                maximumFractionDigits: $rootScope.weightDecimalPoint
            })
        }


        $rootScope.currency = function(currencyMaster, amount) {
            var formattedAmount;

            if (amount === undefined || amount === null || amount === '')
                return null;

            if (currencyMaster == undefined || currencyMaster == null || currencyMaster == "")
                return null;


            if (currencyMaster.decimalPoint === '3' || currencyMaster.decimalPoint === "3") {
                formattedAmount = Math.round(amount * 1000) / 1000;
            } else {
                formattedAmount = Math.round(amount * 100) / 100;
            }
            return formattedAmount;
        }

        $rootScope.roundOffCurrencyWithLocation = function(amount) {
            var tmpWeight = Math.abs(amount);
            var decimal = tmpWeight - Math.floor(tmpWeight);
            var checkDecimal = $rootScope.appMasterData['invoice.round'];
            if (decimal != 0.0 && checkDecimal != undefined && checkDecimal != null) {
                if (decimal >= checkDecimal) {
                    return Math.round(amount);
                } else {
                    return Math.floor(amount);
                }
            }
            return tmpWeight;
        }

        $rootScope.weight = function(decimalPoint, weight) {
            var formattedAmount;

            if (weight === undefined || weight === null || weight === '')
                return null;

            formattedAmount = Math.round(weight * 100) / 100;

            return formattedAmount;
        }

        $rootScope.getDecimalSeparator = function(countryMaster) {
            var decSep = ".";

            try {
                // this works in FF, Chrome, IE, Safari and Opera
                var sep = parseFloat(3 / 2).toLocaleString(countryMaster.locale).substring(1, 2);
                if (sep === '.' || sep === ',') {
                    decSep = sep;
                }
            } catch (e) {

            }

            return decSep;
        }


        $rootScope.roundOffWeight = function(weight) {
            var tmpWeight = Math.abs(weight);
            var decimal = tmpWeight - Math.floor(tmpWeight);

            if (decimal != 0.5 && decimal != 0.0) {
                if (decimal > 0.5) {
                    return Math.round(weight);
                } else {
                    return Math.floor(weight) + 0.5;
                }
            }
            return tmpWeight;
        }

        $scope.startsWith = function(actual, expected) {
            var lowerStr = (actual == null || actual == "" ? "" : actual + "").toLowerCase();

            if (lowerStr == null || lowerStr == "") {
                return -1;
            }

            return lowerStr.indexOf(expected == null || expected == "" ? "" : expected.toLowerCase()) === 0;
        }

        $rootScope.hasAccess = function(feature) {
            return true;
        }

        $rootScope.roleAccess = function(feature, messageFlag) {
            if ($rootScope.userProfile.featureMap[feature]) {
                // console.log("Access Permitted....");
                return true;
            } else {
                // console.log("Access Denied....");
                if (!messageFlag) {
                    Notification.error($rootScope.nls["ERR50"]);
                }
                return false;
            }
            return true;
        }

        $rootScope.roleAccessCustomMessage = function(feature, messageFlag, errorMessage) {
            if ($rootScope.userProfile.featureMap[feature]) {
                return true;
            } else {
                if (!messageFlag) {
                    if (errorMessage) {
                        Notification.error(errorMessage);
                    } else {
                        Notification.error($rootScope.nls["ERR50"]);
                    }
                }

                return false;
            }
            return true;
        }

        $rootScope.isGoodsReceived = function(service) {
            var isGoodsReceived = false;

            if (service.eventList != null && service.eventList.length != 0) {
                for (var i = 0; i < service.eventList.length; i++) {

                    if (service.eventList[i].eventMaster != undefined && service.eventList[i].eventMaster.eventMasterType == 'CARGO_RECEIVED') {
                        isGoodsReceived = true;
                    }
                }
            }

            return isGoodsReceived;
        }



        $rootScope.validateMawb = function(carrierMaster, mawb) {

            if (mawb.length != 11) {
                return "F";
            }

            var carrierNo = mawb.substring(0, 3);
            var slNo = mawb.substring(3, 10);
            var checkDigit = mawb.substring(10, 11);

            if (carrierMaster.carrierNo != carrierNo) {
                return "F";
            }

            try {
                var tmp = parseInt(slNo);
                var tmp1 = parseInt(checkDigit);

                var tmpCheck = slNo % 7;

                if (tmpCheck != checkDigit) {
                    return tmpCheck;
                }
            } catch (err) {
                return "F";
            }
            return "S";
        }

        $rootScope.checkIsTranshipment = function(serviceObj) {

            var isTranshipment = false;
            if (serviceObj != undefined && serviceObj.origin != undefined && serviceObj.origin != null && serviceObj.origin.id != undefined &&
                serviceObj.pol != undefined && serviceObj.pol != null && serviceObj.pol.id != undefined && serviceObj.origin.id != serviceObj.pol.id) {
                isTranshipment = true;
            }

            if (serviceObj != undefined && serviceObj.destination != undefined && serviceObj.destination != null && serviceObj.destination.id != undefined &&
                serviceObj.pod != undefined && serviceObj.pod != null && serviceObj.pod.id != undefined && serviceObj.destination.id != serviceObj.pod.id) {
                isTranshipment = true;
            }
            return isTranshipment;

        }


        $scope.localScrollConfigScroll = angular.copy($rootScope.ngScrollConfigScroll);
        angular.extend($scope.localScrollConfigScroll, {
            "z-index": 10000
        });
        $rootScope.transactionServiceStateArr = ["layout.viewSalesEnquiry", "layout.addSalesEnquiry",
            "layout.editSalesEnquiry", "layout.salesQuotationView",
            "layout.salesQuotationCreate", "layout.salesQuotationAdd",
            "layout.salesQuotationEdit",
            "layout.addNewShipment", "layout.editNewShipment",
            "layout.viewCrmShipment",
            "layout.createCrmShipment", "layout.addNewAirConsol",
            "layout.editAirConsol", "layout.viewNewAirConsol", "layout.crmImportToExportView"
        ];
        $rootScope.stateEventMap = new Map();

        $rootScope.stateEventMap.put("layout.addParty", "addMasterPartyEvent");
        $rootScope.stateEventMap.put("layout.editParty", "editMasterPartyEvent");
        $rootScope.stateEventMap.put("layout.addSalesEnquiry", "addSalesEnquiryEvent");
        $rootScope.stateEventMap.put("layout.editSalesEnquiry", "editSalesEnquiryEvent");
        $rootScope.stateEventMap.put("layout.salesQuotationAdd", "addSalesQuotationEvent");
        $rootScope.stateEventMap.put("layout.salesQuotationCreate", "createSalesQuotationEvent");
        $rootScope.stateEventMap.put("layout.salesQuotationEdit", "editSalesQuotationEvent");
        $rootScope.stateEventMap.put("layout.addNewShipment", "addNewShipmentEvent");
        $rootScope.stateEventMap.put("layout.editNewShipment", "editNewShipmentEvent");

        $rootScope.stateEventMap.put("layout.createCrmShipment", "createCrmShipmentEvent");
        $rootScope.stateEventMap.put("layout.addNewAirConsol", "addNewAirConsolEvent");
        $rootScope.stateEventMap.put("layout.addAirConsolWizard", "addWizardAirConsolEvent");
        $rootScope.stateEventMap.put("layout.editAirConsol", "editAirConsolEvent");
        $rootScope.stateEventMap.put("layout.createAirConsol", "createAirConsolEvent");
        $rootScope.stateEventMap.put("layout.addService", "addServiceEvent");
        $rootScope.stateEventMap.put("layout.editService", "editServiceEvent");
        $rootScope.stateEventMap.put("layout.addCarrier", "addCarrierEvent");
        $rootScope.stateEventMap.put("layout.editCarrier", "editCarrierEvent");
        $rootScope.stateEventMap.put("layout.addPort", "addPortEvent");
        $rootScope.stateEventMap.put("layout.editPort", "editPortEvent");
        $rootScope.stateEventMap.put("layout.addUnit", "addUnitEvent");
        $rootScope.stateEventMap.put("layout.editUnit", "editUnitEvent");
        $rootScope.stateEventMap.put("layout.addTos", "addTosEvent");
        $rootScope.stateEventMap.put("layout.editTos", "editTosEvent");

        $rootScope.stateEventMap.put("layout.addCompany", "addCompanyEvent");
        $rootScope.stateEventMap.put("layout.editCompany", "editCompanyEvent");
        $rootScope.stateEventMap.put("layout.regionMasterAdd", "regionMasterAddEvent");
        $rootScope.stateEventMap.put("layout.regionMasterEdit", "regionMasterEditEvent");

        $rootScope.stateEventMap.put("layout.addCity", "addCityEvent");
        $rootScope.stateEventMap.put("layout.editCity", "editCityEvent");
        $rootScope.stateEventMap.put("layout.addState", "addStateEvent");
        $rootScope.stateEventMap.put("layout.editState", "editStateEvent");
        $rootScope.stateEventMap.put("layout.addPack", "addPackEvent");
        $rootScope.stateEventMap.put("layout.editPack", "editPackEvent");
        $rootScope.stateEventMap.put("layout.addCurrency", "addCurrencyEvent");
        $rootScope.stateEventMap.put("layout.editCurrency", "editCurrencyEvent");
        $rootScope.stateEventMap.put("layout.addCurrencyRate", "addCurrencyRateEvent");
        $rootScope.stateEventMap.put("layout.editCurrencyRate", "editCurrencyRateEvent");
        $rootScope.stateEventMap.put("layout.addCharge", "addChargeEvent");
        $rootScope.stateEventMap.put("layout.editCharge", "editChargeEvent");
        $rootScope.stateEventMap.put("layout.addCommodity", "addCommodityEvent");
        $rootScope.stateEventMap.put("layout.editCommodity", "editCommodityEvent");
        $rootScope.stateEventMap.put("layout.addCategory", "addCategoryEvent");
        $rootScope.stateEventMap.put("layout.editCategory", "editCategoryEvent");
        $rootScope.stateEventMap.put("layout.addDocprefix", "addDocprefixEvent");
        $rootScope.stateEventMap.put("layout.editDocprefix", "editDocprefixEvent");
        $rootScope.stateEventMap.put("layout.addLogo", "addLogoEvent");
        $rootScope.stateEventMap.put("layout.editLogo", "editLogoEvent");
        $rootScope.stateEventMap.put("layout.addPartyGroup", "addPartyGroupEvent");
        $rootScope.stateEventMap.put("layout.editPartyGroup", "editPartyGroupEvent");
        $rootScope.stateEventMap.put("layout.addFlightPlan", "addFlightPlanEvent");
        $rootScope.stateEventMap.put("layout.editFlightPlan", "editFlightPlanEvent");
        $rootScope.stateEventMap.put("layout.invoiceCreate", "addInvoiceEvent");
        $rootScope.stateEventMap.put("layout.editInvoice", "editInvoiceEvent");
        $rootScope.stateEventMap.put("layout.creditNoteCostCreate", "addCreditNoteCostEvent");
        $rootScope.stateEventMap.put("layout.editCreditNoteCost", "editCreditNoteCostEvent");
        $rootScope.stateEventMap.put("layout.addCreditNoteRevenue", "addCreditNoteRevenueEvent");
        $rootScope.stateEventMap.put("layout.editCreditNoteRevenue", "editCreditNoteRevenueEvent");
        $rootScope.stateEventMap.put("layout.addCostCenter", "addCostCenterEvent");
        $rootScope.stateEventMap.put("layout.editCostCenter", "editCostCenterEvent");
        $rootScope.stateEventMap.put("layout.addAutomail", "addAutomailEvent");
        $rootScope.stateEventMap.put("layout.editAutomail", "editAutomailEvent");
        $rootScope.stateEventMap.put("layout.addCurrencyRate", "addCurrencyRateEvent");
        $rootScope.stateEventMap.put("layout.editCurrencyRate", "editCurrencyRateEvent");
        $rootScope.stateEventMap.put("layout.addPack", "addPackEvent");
        $rootScope.stateEventMap.put("layout.editPack", "editPackEvent");

        $rootScope.stateEventMap.put("layout.addEmployee", "addEmployeeEvent");
        $rootScope.stateEventMap.put("layout.editEmployee", "editEmployeeEvent");


        $rootScope.stateEventMap.put("layout.addProvisional", "addProvisionalEvent");
        $rootScope.stateEventMap.put("layout.editProvisional", "editProvisionalEvent");
        $rootScope.stateEventMap.put("layout.addServiceType", "addServiceTypeEvent");
        $rootScope.stateEventMap.put("layout.editServiceType", "editServiceTypeEvent");
        $rootScope.stateEventMap.put("layout.addPartyType", "addPartyTypeEvent");
        $rootScope.stateEventMap.put("layout.editPartyType", "editPartyTypeEvent");
        $rootScope.stateEventMap.put("layout.addCarrier", "addCarrierEvent");
        $rootScope.stateEventMap.put("layout.editCarrier", "editCarrierEvent");
        $rootScope.stateEventMap.put("layout.addReferenceType", "addReferenceTypeEvent");
        $rootScope.stateEventMap.put("layout.editReferenceType", "editReferenceTypeEvent");
        $rootScope.stateEventMap.put("layout.carrierRateAdd", "carrierRateAddEvent");
        $rootScope.stateEventMap.put("layout.carrierRateEdit", "carrierRateEditEvent");
        $rootScope.stateEventMap.put("layout.crmImportToExport", "crmImportToExportEvent");
        $rootScope.stateEventMap.put("layout.crmImportToExportView", "crmImportToExportViewEvent");

        $rootScope.stateEventMap.put("layout.pricingAirAdd", "addPricingAirEvent");
        $rootScope.stateEventMap.put("layout.pricingAirEdit", "editPricingAirEvent");

        $rootScope.stateEventMap.put("layout.essentialChargeAdd", "essentialChargeAddEvent");
        $rootScope.stateEventMap.put("layout.essentialChargeEdit", "essentialChargeEditEvent");



        $rootScope.stateEventMap.put("layout.addUser", "addUserEvent");
        $rootScope.stateEventMap.put("layout.editUser", "editUserEvent");

        $rootScope.stateEventMap.put("layout.addPortGroup", "addPortGroupEvent");
        $rootScope.stateEventMap.put("layout.editPortGroup", "editPortGroupEvent");

        $rootScope.stateEventMap.put("layout.addDepartment", "addDepartmentEvent");
        $rootScope.stateEventMap.put("layout.editDepartment", "editDepartmentEvent");

        $rootScope.stateEventMap.put("layout.addDesignation", "addDesignationEvent");
        $rootScope.stateEventMap.put("layout.editDesignation", "editDesignationEvent");

        $rootScope.stateEventMap.put("layout.addAirCFS", "addAirCFSEvent");
        $rootScope.stateEventMap.put("layout.editAirCFS", "editAirCFSEvent");

        $rootScope.stateEventMap.put("layout.addAirlinePrebooking", "addAirlinePrebookingEvent");
        $rootScope.stateEventMap.put("layout.editAirlinePrebooking", "editAirlinePrebookingEvent");

        $rootScope.stateEventMap.put("layout.aesView", "aesViewEvent");
        $rootScope.stateEventMap.put("layout.editAes", "editAesEvent");
        $rootScope.stateEventMap.put("layout.addAes", "addAesEvent");

        $rootScope.stateEventMap.put("layout.addPurchaseOrder", "addPurchaseOrderEvent");
        $rootScope.stateEventMap.put("layout.editPurchaseOrder", "editPurchaseOrderEvent");
        $rootScope.stateEventMap.put("layout.addCountryDynamicField", "addCountryDynamicFieldEvent");
        $rootScope.stateEventMap.put("layout.editCountryDynamicField", "editCountryDynamicFieldEvent");
        $rootScope.stateEventMap.put("layout.addDocIssueRestriction", "addDocIssueRestrictionEvent");
        $rootScope.stateEventMap.put("layout.editDocIssueRestriction", "editDocIssueRestrictionEvent");


        $rootScope.stateEventMap.put("layout.addDocIssueRestriction", "addDocIssueRestrictionEvent");
        $rootScope.stateEventMap.put("layout.editDocIssueRestriction", "editDocIssueRestrictionEvent");

        $rootScope.stateEventMap.put("layout.iataRatesAirAdd", "iataRatesAirAddEvent");
        $rootScope.stateEventMap.put("layout.iataRatesAirEdit", "iataRatesAirEditEvent");

        $rootScope.stateEventMap.put("layout.addDefaultCharges", "addDefaultChargesEvent");
        $rootScope.stateEventMap.put("layout.editDefaultCharges", "editDefaultChargesEvent");

        $rootScope.stateEventMap.put("layout.countryMasterAdd", "addCountryMasterEvent");
        $rootScope.stateEventMap.put("layout.countryMasterEdit", "editCountryMasterEvent");

        $rootScope.stateEventMap.put("layout.zoneMasterAdd", "addZoneMasterEvent");
        $rootScope.stateEventMap.put("layout.zoneMasterEdit", "editZoneMasterEvent");

        $rootScope.stateEventMap.put("layout.bankMasterAdd", "addBankMasterEvent");
        $rootScope.stateEventMap.put("layout.bankMasterEdit", "editBankMasterEvent");

        $rootScope.stateEventMap.put("layout.triggerTypeMasterAdd", "addTriggerTypeMasterEvent");
        $rootScope.stateEventMap.put("layout.triggerTypeMasterEdit", "editTriggerTypeMasterEvent");

        $rootScope.stateEventMap.put("layout.triggerMasterAdd", "addTriggerMasterEvent");
        $rootScope.stateEventMap.put("layout.triggerMasterEdit", "editTriggerMasterEvent");

        $rootScope.stateEventMap.put("layout.defaultMasterAdd", "addDefaultMasterEvent");
        $rootScope.stateEventMap.put("layout.defaultMasterEdit", "editDefaultMasterEvent");

        $rootScope.stateEventMap.put("layout.gradeMasterAdd", "addGradeMasterEvent");
        $rootScope.stateEventMap.put("layout.gradeMasterEdit", "editGradeMasterEvent");

        $rootScope.stateEventMap.put("layout.valueAddedServicesAdd", "addValueAddedServicesEvent");
        $rootScope.stateEventMap.put("layout.valueAddedServicesEdit", "editValueAddedServicesEvent");

        $rootScope.stateEventMap.put("layout.eventMasterAdd", "addEventMasterEvent");
        $rootScope.stateEventMap.put("layout.eventMasterEdit", "editEventTypeMasterEvent");

        $rootScope.stateEventMap.put("layout.cfsMasterAdd", "addCFSMasterEvent");
        $rootScope.stateEventMap.put("layout.cfsMasterEdit", "editCFSMasterEvent");

        $rootScope.stateEventMap.put("layout.locationMasterAdd", "addLocationMasterEvent");
        $rootScope.stateEventMap.put("layout.locationMasterEdit", "editLocationMasterEvent");


        $rootScope.stateEventMap.put("layout.documentTypeMasterAdd", "addDocumentTypeMasterEvent");
        $rootScope.stateEventMap.put("layout.documentTypeMasterEdit", "editDocumentTypeMasterEvent");

        $rootScope.stateEventMap.put("layout.aesFilerMasterAdd", "addAESFilerMasterEvent");
        $rootScope.stateEventMap.put("layout.aesFilerMasterEdit", "editAESFilerMasterEvent");

        $rootScope.stateEventMap.put("layout.divisionMasterAdd", "addDivisionMasterEvent");
        $rootScope.stateEventMap.put("layout.divisionMasterEdit", "editDivisionMasterEvent");

        $rootScope.stateEventMap.put("layout.daybookMasterAdd", "addDaybookMasterEvent");
        $rootScope.stateEventMap.put("layout.daybookMasterEdit", "editDaybookMasterEvent");

        $rootScope.stateEventMap.put("layout.addRecordAccessLevel", "addRecordAccessLevelEvent");
        $rootScope.stateEventMap.put("layout.editRecordAccessLevel", "editRecordAccessLevelEvent");

        $rootScope.stateEventMap.put("layout.buyerconsolidationAdd", "addBuyerConsolidationEvent");
        $rootScope.stateEventMap.put("layout.buyerconsolidationEdit", "editBuyerConsolidationEvent");

        $rootScope.navigateToNextField = function(id) {
            $timeout(function() {
                if (id != undefined && id != null) {
                    var docElement = document.getElementById(id);
                    if (docElement != undefined && docElement != null) {
                        docElement.focus();
                    }
                }
            })
        }


        // Delete All Recent History
        $scope.deleteHistory = function() {
            recentHistoryFactory.deleteHistory({}).$promise.then(function(data) {
                console.log("RecentHistoryFactory :: ", data);
                if (data.responseCode == 'ERR0') {
                    $rootScope.recentBrowseHistoryList = [];
                    $rootScope.testRecentHistoryDayMapData = [];
                    $rootScope.recentHistoryGroupMap = new Map();
                }

            }, function(error) {
                console.log("Save response :: ", error);
            });
        }


        // goBackByParticularPage fun() - from Unsaved form history
        $scope.goBackByParticularPage = function(currentHisObj) {
                $scope.recentDocHide();
                console.log(" -- Hey I am in Go BackBy Particular Page Fun, ", GoToUnfilledFormNavigationService.goToState(currentHisObj), undefined);
            }
            // goBackByParticularPage fun() - from Unsaved form history
        $scope.goToRecentStatePage = function(currentHisObj) {
                var ishistoryFounded = false;
                if (currentHisObj.stateName.includes("edit")) {
                    if ($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0) {
                        for (var iIndex = 0; iIndex < $rootScope.unfinishedFormHistoryList.length; iIndex++) {
                            var tit = currentHisObj.title;
                            if ($rootScope.unfinishedFormHistoryList[iIndex].title == tit) {
                                var unSavedData = $rootScope.unfinishedFormHistoryList[iIndex];
                                GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[iIndex], undefined); // $rootScope.unfinishedFormHistoryList.push(stateDataObject);
                                ishistoryFounded = true;
                            }
                            if (ishistoryFounded) {
                                break;
                            }
                        }
                    }
                }
                $scope.recentDocHide();
                if (!ishistoryFounded) {
                    $state.go(currentHisObj.stateName, JSON.parse(currentHisObj.stateParam));
                }
            }
            // goBackToParticularRecentPage fun() - from recent history List
        $scope.goBackToParticularRecentPage = function(currentHisObj) {
            $scope.recentDocHide();
            console.log(" -- Hey I am in Go BackBy Particular Page Fun, ", GoToRecentHistoryNavigationService.goToState(currentHisObj));
        }


        $scope.getHistoryDateConversion = function(parsedate) {
            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            }

            if (typeof parsedate == "string") {
                return moment(parsedate, "YYYY-MM-DD HH:mm:ss");
            } else {
                return moment(parsedate, "YYYY-MM-DD HH:mm:ss");
            }

        }

        $rootScope.isEmptyRow = function(obj) {
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;
                }
            }
            return isempty;
        }


        $rootScope.getConvertDateTimeHistory = function(date) {
            if (date != undefined && date != null)
                return $rootScope.dateAndTimeToString($scope.getHistoryDateConversion(date));
            else
                return "";
        }
        $rootScope.convertToDayWiseData = function(arr, key, dayWise) {
            var groups = {};
            for (var i = 0; l = arr.length, i < l; i++) {
                if (dayWise) {
                    arr[i][key] = arr[i][key];
                } else {
                    arr[i][key] = arr[i][key].toTimeString();
                }
                groups[arr[i][key]] = groups[arr[i][key]] || [];
                groups[arr[i][key]].push(arr[i]);
            }
            return groups;
        };
        $rootScope.groupByRecentHistory = function(haveToGroup) {
                if (haveToGroup) {
                    $rootScope.recentHistoryGroupMap = new Map();
                    appMetaFactory.fetchRecentHistoryGroup.get(
                        function(data) {
                            console.log("recentHistoryFactory. ------- ", data);
                            if (data.responseCode == 'ERR0') {
                                $rootScope.recentGroupHistoryList = [];
                                $rootScope.recentGroupHistoryList = data.responseObject;
                                for (var gi = 0; gi < $rootScope.recentGroupHistoryList.length; gi++) {

                                    $rootScope.junkRecenttempArray = $rootScope.recentHistoryGroupMap.get($rootScope.recentGroupHistoryList[gi].stateCategory);
                                    if ($rootScope.junkRecenttempArray == undefined || $rootScope.junkRecenttempArray == null) {
                                        $rootScope.junkRecenttempArray = [];
                                    }
                                    $rootScope.junkRecenttempArray.push($rootScope.recentGroupHistoryList[gi]);


                                    $rootScope.recentHistoryGroupMap.put(
                                        $rootScope.recentGroupHistoryList[gi].stateCategory,
                                        $rootScope.junkRecenttempArray);
                                }
                                //                      RecentHistoryGroupMapService.form();
                            } else {
                                $rootScope.recentGroupHistoryList = [];
                            }
                        },
                        function(error) {
                            console.log("REcent History Loaded failed : ", error)
                        });
                }
            }
            /*
             * Render Items for LOV
             * */

        $rootScope.portRender = function(item) {
            return {
                label: item.portName,
                item: item
            }
        }

        var changeProfile;
        $scope.changeProfileModel = function(nextFocus) {
            changeProfile = $modal({
                scope: $scope,
                backdrop: 'static',
                templateUrl: 'app/components/login/change_profile_popup.html',
                show: false
            });
            changeProfile.$promise.then(changeProfile.show);
            $timeout(function() {
                $rootScope.navigateToNextField(nextFocus);
            }, 800);
        };

        /*  $scope.deleteBC = function(id) {
                  
                  var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR03215"];
                    ngDialog.openConfirm( {
                 template : 
                   '<div class="col-md-12" style="height: 150px;">'
                   + '<div class="row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.12); padding-top:0px; padding-bottom:10px">'
                   + '<span style="font-size:16px; padding-left: 0px;">Change Profile'
                   + '</span>'
                   + '<button type="button" class="btn btn-property accent-btn pull-right" style="margin-right:20px;" ng-click="closeThisDialog(0)">Close'
                   + '</button>'
                   + '</div>'
                   + '<select id="companyProfile" style="height:30px; width:232px; margin-top:20px;" class="nopadding" ng-model="userProfile.selectedCompany" ng-change="populatCompanyChange()" ng-options="company as company.companyName for company in userProfile.profileCompanyList"></select>'
                       + '<select style="margin-left:20px; margin-top:20px; width: 160px; height: 30px;" class="nopadding" ng-model="userProfile.selectedUserLocation"  ng-change="changeProfile()"  ng-options="location as location.locationName for location in userProfile.profileCompanyLocationlist"></select>'
                     + '</div>',
                        
                      plain : true,
                      scope : newScope,
                      closeByDocument: false,
                      className : 'ngdialog-theme-default'
                                
                               
                            }).then( function(value) {
                                
                            }, function(value) {
                           
                        });
                    
                    $timeout(function(){
                        $rootScope.navigateToNextField("companyProfile");
                        },800);
              } */

        /* Tooltip Value */

        $rootScope.sentTrigger = {
            sentTrigger: 'Sent',
            checked: false
        };
        $rootScope.receivedTrigger = {
            receivedTrigger: 'Received',
            checked: false
        };
        $rootScope.copy = {
            copy: 'Copy',
            checked: false
        };
        $rootScope.back = {
            back: 'Back',
            checked: false
        };
        $rootScope.activity = {
            activity: 'Activity',
            checked: false
        };
        $rootScope.edit = {
            edit: 'Edit',
            checked: false
        };
        $rootScope.editIcon = {
            editIcon: 'Edit',
            checked: false
        };
        $rootScope.delete = {
            delete: 'Delete',
            checked: false
        };
        $rootScope.add = {
            add: 'Add',
            checked: false
        };
        $rootScope.addressIcon = {
            addressIcon: 'Show Address',
            checked: false
        };
        $rootScope.selectAddress = {
            selectAddress: 'Select  Address',
            checked: false
        };
        $rootScope.editpartyIcon = {
            editpartyIcon: 'Edit Party',
            checked: false
        };
        $rootScope.recentListIcon = {
            recentListIcon: 'Recent List',
            checked: false
        };
        $rootScope.hazIcon = {
            hazIcon: 'Hazardous Goods',
            checked: false
        };
        $rootScope.imco = {
            imco: 'IMCO',
            checked: false
        };
        $rootScope.errorList = {
            errorList: 'Click To See Error List',
            checked: false
        };
        $rootScope.newTab = {
            newTab: 'New Tab',
            checked: false
        };
        $rootScope.tabDelete = {
            tabDelete: 'Delete Service',
            checked: false
        };
        $rootScope.recent = {
            recent: 'Open Recent',
            checked: false
        };
        $rootScope.prev = {
            prev: 'Previous',
            checked: false
        };
        $rootScope.next = {
            next: 'Next',
            checked: false
        };
        $rootScope.close = {
            close: 'Close',
            checked: false
        };
        $rootScope.addEnquiryCustomer = {
            addEnquiryCustomer: 'Enter Customer Details',
            checked: false
        };
        $rootScope.editParty = {
            editParty: 'Edit Party',
            checked: false
        };
        $rootScope.editCustomer = {
            editCustomer: 'Edit Customer',
            checked: false
        };
        $rootScope.showCharge = {
            showCharge: 'Show Charge',
            checked: false
        };
        $rootScope.getInvoiceIcon = {
            getInvoiceIcon: 'Get Invoice Charges',
            checked: false
        };
        $rootScope.togglePassword = {
            togglePassword: 'Show/Hide Password',
            checked: false
        };
        $rootScope.downloadIcon = {
            downloadIcon: 'Download',
            checked: false
        };
        $rootScope.upload = {
            upload: 'Upload',
            checked: false
        };
        $rootScope.selectPartyAddress = {
            selectPartyAddress: 'Select Party Address',
            checked: false
        };
        $rootScope.selectShipperAddress = {
            selectShipperAddress: 'Select Shipper Address',
            checked: false
        };
        $rootScope.selectConsigneeAddress = {
            selectConsigneeAddress: 'Select Consignee Address',
            checked: false
        };
        $rootScope.selctNotify1 = {
            selctNotify1: 'Selct Notify Customer 1 Address',
            checked: false
        };
        $rootScope.selctNotify2 = {
            selctNotify2: 'Selct Notify Customer 2 Address',
            checked: false
        };
        $rootScope.selctNotify3 = {
            selctNotify3: 'Selct Notify Customer 3 Address',
            checked: false
        };
        $rootScope.selectForwarderAddress = {
            selectForwarderAddress: 'Select Forwarder Address',
            checked: false
        };
        $rootScope.selectNotifyCustomer2Address = {
            selectNotifyCustomer2Address: 'Select Notify Customer 2 Address',
            checked: false
        };
        $rootScope.selectAgentAddress = {
            selectAgentAddress: 'Select Agent Address',
            checked: false
        };
        $rootScope.selectIssuingAgentAddress = {
            selectIssuingAgentAddress: 'Select Issuing Agent Address',
            checked: false
        };
        $rootScope.selectChaAgentAddress = {
            selectChaAgentAddress: 'Select Custom House Agent Address',
            checked: false
        };

        $rootScope.duplicate = {
            duplicate: 'Duplicate',
            checked: false
        };
        $rootScope.editShipper = {
            editShipper: 'Edit Shipper',
            checked: false
        };
        $rootScope.editAgent = {
            editAgent: 'Edit Agent',
            checked: false
        };
        $rootScope.editConsignee = {
            editConsignee: 'Edit Consignee',
            checked: false
        };
        $rootScope.editNotifyCustomer1 = {
            editNotifyCustomer1: 'Edit Notify Customer 1',
            checked: false
        };
        $rootScope.editNotifyCustomer2 = {
            editNotifyCustomer2: 'Edit Notify Customer 2',
            checked: false
        };
        $rootScope.editForwarder = {
            editForwarder: 'Edit Forwarder',
            checked: false
        };
        $rootScope.editIssuingAgent = {
            editIssuingAgent: 'Edit Issuing Agent',
            checked: false
        };
        $rootScope.editChaAgent = {
            editChaAgent: 'Edit Custom House Agent',
            checked: false
        };
        $rootScope.deleteDocument = {
            deleteDocument: 'Delete Document',
            checked: false
        };
        $rootScope.selectPickUpPlace = {
            selectPickUpPlace: 'Select Pick Up Place',
            checked: false
        };
        $rootScope.selectPickUpAddress = {
            selectPickUpAddress: 'Select Pick Up Address',
            checked: false
        };
        $rootScope.selectDeliveryAddress = {
            selectDeliveryAddress: 'Select Delivery Address',
            checked: false
        };
        $rootScope.selectDeliveryPlace = {
            selectDeliveryPlace: 'Select Delivery Place',
            checked: false
        };
        $rootScope.selectDoorDeliveryPlace = {
            selectDoorDeliveryPlace: 'Select Door Delivery Place',
            checked: false
        };
        $rootScope.selectAddress = {
            selectAddress: 'Select Address',
            checked: false
        };
        $rootScope.removewidget = {
            removewidget: 'Remove widget',
            checked: false
        };
        $rootScope.selectNotifyCustomer1Address = {
            selectNotifyCustomer1Address: 'Select Notify Customer 1 Address',
            checked: false
        };

        $rootScope.bookingget = {
            bookingget: 'Customer : Aren Electronics LLC <br /> Service: Air Export',
            checked: false
        };
        $rootScope.picupget = {
            picupget: 'Pickup Date : 04-04-2017 <br /> Pickup point : Warehouse',
            checked: false
        };
        $rootScope.pointget = {
            pointget: 'Received Date : 04-04-2017 <br /> Receive point: Chennai Airtport',
            checked: false
        };
        $rootScope.recivedget = {
            recivedget: 'Received Date  : 04-04-2017 <br /> Receive point: Chennai Airtport',
            checked: false
        };
        $rootScope.mmaget = {
            mmaget: 'Carrier No: E3403 <br /> ETD date: 12/04/2017 17:35',
            checked: false
        };
        $rootScope.blrget = {
            blrget: 'Carrier: Luthansa <br /> Carrier No: E5479 <br /> ETD date: 12/04/2017 17:35',
            checked: false
        };
        $rootScope.blretdget = {
            blretdget: 'Carrier: Emirates <br /> Carrier No: E3403 <br /> ETD date: 12/04/2017 17:35',
            checked: false
        };
        $rootScope.uaeget = {
            uaeget: 'Carrier: Luthansa <br /> Carrier No: E5479 <br /> ETD date: 15/04/2017 01:05',
            checked: false
        };
        $rootScope.uaeetdget = {
            uaeetdget: 'Carrier: Emirates <br /> Carrier No: E3547 <br /> ETD date: 15/04/2017 08:25',
            checked: false
        };
        $rootScope.canget = {
            canget: 'Carrier: Luthansa <br /> Carrier No: L7842 <br /> ETD date: 16/04/2017 12:05',
            checked: false
        };
        $rootScope.candatget = {
            candatget: 'CAN Date : 17-04-2017 <br /> Notice sent to : Abi Electronis LTD',
            checked: false
        };
        $rootScope.deliveryget = {
            deliveryget: 'Delivery Date : 17-04-2017 <br /> Delivery point: Warehouse ',
            checked: false
        };
    }
]);