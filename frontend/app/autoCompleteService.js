app.service("AutoCompleteService",['$rootScope', 'CommodityList', 'ServiceByTransportMode', 'PartiesList', 
    'TosSearchKeyword', 'PortByTransportMode', 'ChargeSearchKeyword', 'CurrencySearchExclude',
    'SalesmanList', 'DivisionListByCompany', 'PartiesListByType', 'CarrierByTransportMode', 'EmployeeList', 
    'ProjectList', 'PackList', 'PortFromCFS',
    'CityList', 'StateList', 'CurrencyMasterSearchKeyword', 'CountryList', 'SalesmanListOnLocation', 'AccountList',
    'CountryWiseLocation', 'DocumentTypeSearchKeyword', 'DaybookList', 'UserProfileList',
    'SearchServiceNotInList', 'countryMasterFactory', 'SearchLocationNotInList', 'SearchDivisionNotInList',
    'SearchPartyNotInList', 'SearchUserNotInList', 'DaybookListByDocumentType', 'PartyAccountListById',
    'SearchChargeNotInList', 'AgentPartyList', 'RegionList', 'gradeMasterFactory', 'GLAccount', 
    'GLAccountOnlySubledgerYes', 'GLAccountOnlySubledgerNo', 'GLAccountOnlySubledgerNoWithBankAccount', 
    'GLAccountOnlySubledgerNoWithOutBankAccount', 'SearchEventNotInList', 

    function($rootScope, CommodityList, ServiceByTransportMode, PartiesList, 
    TosSearchKeyword, PortByTransportMode, ChargeSearchKeyword, CurrencySearchExclude,
    SalesmanList, DivisionListByCompany, PartiesListByType, CarrierByTransportMode, EmployeeList, 
    ProjectList, PackList, PortFromCFS, 
    CityList, StateList, CurrencyMasterSearchKeyword, CountryList, SalesmanListOnLocation, AccountList,
    CountryWiseLocation, DocumentTypeSearchKeyword, DaybookList, UserProfileList,
    SearchServiceNotInList, countryMasterFactory, SearchLocationNotInList, SearchDivisionNotInList,
    SearchPartyNotInList, SearchUserNotInList, DaybookListByDocumentType, PartyAccountListById,
    SearchChargeNotInList, AgentPartyList, RegionList, gradeMasterFactory, GLAccount, 
    GLAccountOnlySubledgerYes, GLAccountOnlySubledgerNo, GLAccountOnlySubledgerNoWithBankAccount, 
    GLAccountOnlySubledgerNoWithOutBankAccount, SearchEventNotInList) {

    this.getCommodityList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return CommodityList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching commodity List ', errResponse);
            return [];
        });
    }

    this.getPartyList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return PartiesList.query(searchDto).$promise.then(function(data, status) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching Party List', errResponse);
            return [];
        });
    }

    this.getRoutedByAgentPartyList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return AgentPartyList.query(searchDto).$promise.then(function(data, status) {
            if (data.responseCode == "ERR0") {
                var responseDataArr = data.responseObject.searchResult;
                var returnArr = [];
                for (var i = 0; i < responseDataArr.length; i++) {
                    for (var j = 0; j < responseDataArr[i].length; j++) {
                        if (responseDataArr[i][j].partyName != null && responseDataArr[i][j].partyName != undefined) {
                            returnArr.push(responseDataArr[i][j]);
                        }
                    }
                }
                return returnArr;
            }
        }, function(errResponse) {
            console.error('Error while fetching Party List', errResponse);
            return [];
        });
    }


    this.getPartyListByPartyType = function(keyword, partyType) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return PartiesListByType.fetch({
            "partyType": partyType
        }, searchDto).$promise.then(function(data, status) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching Party', errResponse);
            return [];
        });
    }

    this.getServiceListByTransportMode = function(keyword, tm) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        if (tm == undefined || tm == null || tm.trim().length == 0) {
            tm = null;
        }
        return ServiceByTransportMode.fetch({
            "transportMode": tm
        }, searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ServiceMaster ', errResponse);
                return [];
            }
        );

    }


    this.getSearchServiceNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return SearchServiceNotInList.fetch(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ServiceListExcludeExisting ', errResponse);
                return [];
            }
        );

    }

    this.getTosList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return TosSearchKeyword.fetch(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Tos', errResponse);
                return [];
            }
        );
    }


    this.getPortListByTransportMode = function(keyword, removeObj, serviceObj) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        var transportMode = undefined;

        if (serviceObj != undefined && serviceObj != null && serviceObj.transportMode != undefined &&
            serviceObj.transportMode != null) {
            transportMode = serviceObj.transportMode;
        }

        return PortByTransportMode.fetch({
            "transportMode": transportMode
        }, searchDto).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    if (removeObj != undefined && removeObj != null && removeObj.id != undefined && removeObj.id != null) {
                        var responseDataArr = data.responseObject.searchResult;
                        var returnArr = [];
                        for (var i = 0; i < responseDataArr.length; i++) {
                            if (responseDataArr[i].id != removeObj.id) {
                                returnArr.push(responseDataArr[i]);
                            }
                        }
                        return returnArr;
                    } else {
                        return data.responseObject.searchResult;
                    }
                }
            },
            function(errResponse) {
                console.error('Error while fetching Port ', errResponse);
                return [];
            });
    }

    /*Salesman Select Picker*/
    this.getSalesmanList = function(keyword) {
        console.log("getSalesman ", keyword);
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return SalesmanList.fetch(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Salesman ', errResponse);
                return [];
            }
        );
    }

    /* Shipment service details division list */
    this.getDivisionList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return DivisionListByCompany.fetch({
            "companyId": $rootScope.userProfile.selectedUserLocation.companyMaster.id
        }, searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching division List Based on the Company ', errResponse);
                return [];
            }
        );
    }

    this.getCarrierList = function(transportMode, keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return CarrierByTransportMode.fetch({
            "transportMode": transportMode
        }, searchDto).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier List Based on Air ', errResponse);
                return [];
            }
        );
    }


    //Added by Sathish on Mar-24th

    this.getEmplyoeeList = function(object) {
        var searchDto = {};
        var customerServiceList = [];
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return EmployeeList.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return customerServiceList = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Customer Services');
                    return customerServiceList;
                });

    }


    this.getProjectList = function(object) {

        var searchDto = {};
        var projectListData = [];
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return ProjectList.query(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return projectListData = data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );

    }


    this.getPackList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return PackList.fetch(searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Pack List');
                return [];
            }
        );

    }
    this.getStateList = function(keyword, country) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return StateList.fetch({
            "countryId": country.id
        }, searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console
                    .error('Error while fetching State List Based on the Country');
                return [];
            });

    }

    this.getCityList = function(keyword, country) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        if (country == undefined || country == null || country.id == undefined) {
            var country = {
                id: -1
            };
        }
        return CityList
            .fetch({
                "countryId": country.id
            }, searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching City List Based on the Country');
                    return [];
                });

    }

    this.getPartyListByType = function(type, keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return PartiesListByType.fetch({
            "partyType": type
        }, searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
                return [];
            }
        );

    }


    this.getPortFromCFS = function(keyword, portId) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return PortFromCFS.fetch({
            "portId": portId
        }, searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }

            },
            function(errResponse) {
                console.error('Error while fetching DeliveryPoint');
                return [];
            }
        );

    }

    this.getPortByRoad = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return PortByTransportMode.fetch({
            "transportMode": 'Road'
        }, searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }

            },
            function(errResponse) {
                return [];
            }
        );

    }

    //for charge list
    this.getChargeList = function(object) {

        console.log("ajaxChargeEvent ", object);

        var searchDto = {};
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ChargeSearchKeyword.query(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                return [];
            }
        );
    }


    //for currency



    this.getCurrencyExcludeList = function(object) {

        var searchDto = {};
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CurrencySearchExclude.fetch({
            "excludeCurrencyCode": $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode
        }, searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                return [];
            }
        );


    }


    this.getCurrencyIncludeList = function(object) {

        var searchDto = {};
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CurrencyMasterSearchKeyword.query(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency List');
                return [];
            }
        );
    }


    this.getCountryList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };


        return CountryList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching Countries ', errResponse);
            return [];
        });
    }




    this.getLocationBasedSalesmanList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SalesmanListOnLocation.fetch({
            "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
        }, searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Emplyoees ', errResponse);
                return [];
            }
        );
    }

    this.getGLAccount = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return GLAccount.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Accounts');
                });
    }

    this.getAccountsOnlySubledgerNoWithBankAccount = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return GLAccountOnlySubledgerNoWithBankAccount.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Accounts');
                });
    }

    this.getAccountsOnlySubledgerNoWithOutBankAccount = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return GLAccountOnlySubledgerNoWithOutBankAccount.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Accounts');
                });
    }

    this.getAccountsOnlySubledgerNo = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return GLAccountOnlySubledgerNo.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Accounts');
                });
    }


    this.getLocationList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return CountryWiseLocation.fetch({
                "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
            }, searchDto)
            .$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Location');
                });
    }

    this.getDocumentTypeList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return DocumentTypeSearchKeyword.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Document type');
                });
    }

    this.getDaybookList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return DaybookList.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Daybook');
                });
    }


    this.getUserProfileList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return UserProfileList.fetch(searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  UserProfile');
            });
    }


    this.getCountryNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return countryMasterFactory.searchCountryNotInList.query(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching Countries ', errResponse);
            return [];
        });
    }


    this.getLocationNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SearchLocationNotInList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching getLocationNotInList ', errResponse);
            return [];
        });
    }


    this.getDivisionNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SearchDivisionNotInList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching getLocationNotInList ', errResponse);
            return [];
        });
    }


    this.getSearchPartyNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SearchPartyNotInList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching getLocationNotInList ', errResponse);
            return [];
        });
    }

    this.getSearchUserNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SearchUserNotInList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching getLocationNotInList ', errResponse);
            return [];
        });
    }


    this.getSearchDaybookByDocumentTypeList = function(keyword, documentTypeId) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return DaybookListByDocumentType.fetch({
                "documentTypeId": documentTypeId
            }, searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching  Daybook list By DocumentType');
                });
    }


    this.getSearchPartyAccountList = function(keyword, partyId) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return PartyAccountListById.fetch({
                "partyId": partyId
            }, searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        return data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Party Account list');
                });
    }

    this.getChargeNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SearchChargeNotInList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching SearchChargeNotInList ', errResponse);
            return [];
        });
    }


    this.getEventNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };

        return SearchEventNotInList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching SearchEventNotInList ', errResponse);
            return [];
        });
    }


    this.getRegionList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return RegionList.fetch(searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                return data.responseObject.searchResult;
            }
        }, function(errResponse) {
            console.error('Error while fetching Region', errResponse);
        });
    }




    this.gradeList = function(keyword) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: $rootScope.totalRecordFetchPerRequest
        };
        return gradeMasterFactory.getall.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        var gradeList = data.responseObject.searchResult;
                        return gradeList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Grade List');
                });
    }


}]);