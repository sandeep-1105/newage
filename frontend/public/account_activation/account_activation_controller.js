var appNewAge = angular.module('NewAgePublic', [], function($locationProvider) {
	$locationProvider.html5Mode({ enabled : true, requireBase : false });
});

appNewAge.controller('AccountActivationController', ['$scope', '$http', '$timeout', '$location', function($scope, $http, $timeout, $location){
	
	$scope.activationObj = {};
	$scope.flagSuccess = false;
	$scope.isUserActivatedFlag = false;
	$scope.confirm = function() {
		
		if($scope.activationObj != undefined && $scope.activationObj != null && $scope.activationObj.key == undefined || $scope.activationObj.key == null || $scope.activationObj.key == "") {
			$scope.errorMessage = "Please provide the activation code";
			return;
		}
		
		console.log("Absolute URL : " + $location.absUrl())
		var str = $location.absUrl();
		var id = str.substring(str.lastIndexOf("?") + 1, str.length)
		
		
		
		$http({ 
			url: $scope.urlPath + "/public/api/v1/locationsetup/activate/" + $scope.activationObj.key + "/" + id,
		    method: "GET",
		    headers : { "SAAS_ID" : $scope.saasId }
		    
		}).success(function(data){
			if (data.responseCode == "ERR0") {
				$scope.flagSuccess = true;
			} else {
				$scope.flagSuccess = false;
				$scope.errorMessage = "Invalid activation code";
				return;
			}
		})
	}
	
	 
	$scope.isUserActivated = function() {
		
		console.log("Absolute URL : " + $location.absUrl())
		var str = $location.absUrl();
		var id = str.substring(str.lastIndexOf("?") + 1, str.length)
		
		$http({
			url : $scope.urlPath + "/public/api/v1/locationsetup/is_user_active/" + id,
			method : "GET",
			headers : {"SAAS_ID" : $scope.saasId}
		}).success(function(data) {
			if(data.responseCode == 'ERR0') {
				$scope.isUserActivatedFlag = true;
				window.location = "/index.html#/login";
			} else {
				$scope.isUserActivatedFlag = false;
				console.log("User is not activated");
			}
		})
	}
	

	$scope.navigateToNextField = function(id) {
		$timeout(function(){
			if(id!=undefined && id != null) {
				var docElement = document.getElementById(id);
				if(docElement != undefined && docElement != null){
					docElement.focus();
				}
			}
		})
	}
	
	var protocol = $location.protocol();
	var host = $location.host();
	var urlPath = protocol + "://" + host; 
	
	var port = $location.port();
	if(port != undefined && port != null){
		port = ":" + port
	} else {
		port = "";
	}
	
	$scope.urlPath = urlPath + port;
	console.log("URL Path " + $scope.urlPath);
	$scope.saasId = "COM";
	
	$scope.isUserActivated();
	
	$scope.navigateToNextField("code");
}]);