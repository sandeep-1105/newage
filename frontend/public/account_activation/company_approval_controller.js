var appNewAge = angular.module('NewAgePublic', [], function($locationProvider) {
	$locationProvider.html5Mode({ enabled : true, requireBase : false });
});


appNewAge.controller('CompanyApprovalController', ['$scope', '$http', '$timeout', '$location', function($scope, $http, $timeout, $location){

	var protocol = $location.protocol();
	var host = $location.host();
	var port = $location.port();
	if(port!=undefined && port!=null){
		port = ":"+port
	}else{
		port="";
	}
	var urlPath = protocol +"://"+host+port;
	
	var saasId = "COM"; 

	$http({	
			method: 'GET', 
			url: urlPath + '/public/api/v1/locationsetup/getall',
			headers: {'SAAS_ID': saasId}
	}).then(function(response) {
		if(response.data.responseCode==='ERR0'){
			console.log(response.data.responseObject);
			$scope.companyList = response.data.responseObject;
		} else {
			console.log(response.data.responseDescription);
		}
	}, function(error) {
		console.log("Error");
	});

	
	$scope.approveCompany = function(companyId) {
		console.log("Company Approved " + companyId);
		
		$http({	
			method: 'GET', 
			url: urlPath + '/public/api/v1/locationsetup/approve_company/'+ companyId,
			headers: {'SAAS_ID': saasId}
		}).then(function(response) {
			console.log(response.data);
		}, function(error) {
			console.log("Error");
		});

	}
}]);