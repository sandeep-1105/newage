var appNewAge = angular.module('NewAgePublic', [], function($locationProvider) {
	$locationProvider.html5Mode({ enabled : true, requireBase : false });
});

appNewAge.controller('AccountActivationSuccessController', ['$scope', '$http', '$timeout', '$location', function($scope, $http, $timeout, $location){
	
	$scope.closeInitiation=function(){
		window.close();
	 }
	
}]);