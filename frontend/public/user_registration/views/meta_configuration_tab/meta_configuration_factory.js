(function() {

NewAgeRegistration.factory("CustomerUpload", ['$resource',function($resource) {
		return $resource("/public/api/v1/upload", {}, {
			upload : {
				method : 'POST',
				headers : {"SAAS_ID" : "COM"}
			}
		});
	}]);

	NewAgeRegistration.factory("DownloadCustomer", ['$resource',function($resource) {
		return $resource("/public/api/v1/download", {}, {
			template : {
				method : 'GET',
				headers : {"SAAS_ID" : "COM"}
			}
		});
	}]);
	
	NewAgeRegistration.factory("ValidateFile", ['$resource',function($resource) {
		return $resource("/public/api/v1/locationsetup/validate_file", {}, {
			upload : {
				method : 'POST',
				headers : {"SAAS_ID" : "COM"}
			}
		});
	}]);
	
})();