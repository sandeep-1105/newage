angular.module("NewAgeRegistration").controller("MetaConfigTabController",['$rootScope', '$scope', '$timeout', '$state', '$http', 'Notification', 'PartyMasterBulkUpload',
        'AgentPortMasterBulkUpload', '$stateParams', 'UserRegistrationFactory', 'ValidateFile',
    function($rootScope, $scope, $timeout, $state, $http, Notification, PartyMasterBulkUpload,
        AgentPortMasterBulkUpload, $stateParams, UserRegistrationFactory, ValidateFile) {


        $scope.initMetaConfigTab = function() {
            console.log("$scope.initMetaConfigTab();");


            if ($stateParams != undefined && $stateParams != null && $stateParams.id != null) {

                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
                    method: "GET",
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }

                }).success(function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationSetup = data.responseObject;
                        console.log("locationSetup ", $scope.locationSetup);
                        if ($scope.locationSetup != null) {
                            if ($scope.locationSetup.customerUploadedFile != null) {
                                $scope.customerUploadText = false;
                                $scope.isCustomerBusy = false;
                                $scope.customer.file = $scope.locationSetup.customerUploadedFile;
                                $scope.customer.fileName = $scope.locationSetup.customerFileName;
                            }
                            if ($scope.locationSetup.agentUploadedFile != null) {
                                $scope.agentUploadText = false;
                                $scope.isAgentBusy = false;
                                $scope.agent.file = $scope.locationSetup.agentUploadedFile;
                                $scope.agent.fileName = $scope.locationSetup.agentFileName;
                            }
                            if ($scope.locationSetup.agentPortUploadedFile != null) {
                                $scope.agentPortUploadText = false;
                                $scope.isAgentPortBusy = false;
                                $scope.agentPort.file = $scope.locationSetup.agentPortUploadedFile;
                                $scope.agentPort.fileName = $scope.locationSetup.agentPortFileName;
                            }
                            $rootScope.currentLocationSetupId = $scope.locationSetup.id;
                        }
                    }
                })
            }
        }

        $scope.customer = {};
        $scope.agent = {};
        $scope.agentPort = {};
        $scope.customerUploadText = true;
        $scope.agentUploadText = true;
        $scope.agentPortUploadText = true;

        // Download Template
        $scope.downloadTemplate = function(type) {
            var fileName;
            if (type == 'AGENT') {
                fileName = 'agent_template.xlsx';
            } else if (type == 'CUSTOMER') {
                fileName = 'customer_template.xlsx';
            } else if (type == 'AGENT PORT') {
                fileName = 'agent_port_template.xlsx';
            }
            $http({
                url: $rootScope.urlPath + '/public/api/v1/download/template/' + type,
                method: "GET",
                headers: {
                    "SAAS_ID": $rootScope.saasId
                },
                responseType: 'arraybuffer'
            }).success(function(data, status, headers, config) {
                var blob = new Blob([data], {});
                saveAs(blob, fileName);
            }).error(function(data, status, headers, config) {
                console.error("Problem while downloading..... ", data);
            });

        }

        $scope.back = function() {
            $state.go("app.configuration", {
                id: $rootScope.currentLocationSetupId
            });
        }
        $scope.chooseFile = function(obj, err, txt, btn) {
            if ($scope.validateUploadFile(obj.file, err)) {
                if (txt == 'customerUploadText') {
                    $scope.customer.fileName = obj.file.name;
                    $scope.customerUploadText = false;
                    $scope.custBtn = true;
                    $scope.isCustomerBusy = true;
                } else if (txt == 'agentUploadText') {
                    $scope.agent.fileName = obj.file.name;
                    $scope.agentUploadText = false;
                    $scope.agentBtn = true;
                    $scope.isAgentBusy = true;
                } else if (txt == 'agentPortUploadText') {
                    $scope.agentPort.fileName = obj.file.name;
                    $scope.agentPortUploadText = false;
                    $scope.agentPortBtn = true;
                    $scope.isAgentPortBusy = true;
                }
                $timeout(function() {
                    if (txt == 'customerUploadText') {
                        $scope.isCustomerBusy = false;
                        if (btn == 'custBtn') {
                            $scope.agentOrCustomerUpload('fileUpload', btn);
                        }
                    }
                    if (txt == 'agentUploadText') {
                        $scope.isAgentBusy = false;

                        if (btn == 'agentBtn') {
                            $scope.agentOrCustomerUpload('agentUpload', btn)
                        }
                    }
                    if (txt == 'agentPortUploadText') {
                        $scope.isAgentPortBusy = false;
                        if (btn == 'agentPortBtn') {
                            $scope.agentPortUpload('agentPort', btn);
                        }
                    }
                    $scope.enableUploadFn(btn);
                }, 2000);

            } else {
                if (btn == 'custBtn') {
                    $scope.locationSetup.customerUploadedFile = null;
                    $scope.locationSetup.customerFileName = null;
                    $scope.customer.fileName = null;
                    $scope.isCustomerBusy = false;
                    $scope.customerUploadText = true;
                }
                if (btn == 'agentBtn') {
                    $scope.locationSetup.agentUploadedFile = null;
                    $scope.locationSetup.agentFileName = null;
                    $scope.agent.fileName = null;
                    $scope.agentUploadText = true;
                    $scope.isAgentBusy = false;
                }
                $scope.enableUploadFn(btn);
                console.log("Not valid file");
            }


        };

        $scope.enableUploadFn = function(btn) {
            $timeout(function() {
                if (btn == 'custBtn') {
                    $scope.custBtn = false;
                }
                if (btn == 'agentBtn') {
                    $scope.agentBtn = false;
                }
                if (btn == 'agentPortBtn') {
                    $scope.agentPortBtn = false;
                }
            }, 200);
        }

        $scope.agentOrCustomerUpload = function(errMessage, btn) {
            var file, fileName, fileType;
            if (btn == 'custBtn') {
                if ($scope.customer.file != undefined) {
                    $scope.custBtn = true;
                    $scope.customerUploadText = false;
                    $scope.isCustomerBusy = true;
                    file = $scope.customer.file;
                    fileName = $scope.customer.file.name;
                    fileType = $scope.customer.file.type;
                }
            }
            if (btn == 'agentBtn') {
                if ($scope.agent.file != undefined) {
                    $scope.agentBtn = true;
                    $scope.agentUploadText = false;
                    $scope.isAgentBusy = true;
                    file = $scope.agent.file;
                    fileName = $scope.agent.file.name;
                    fileType = $scope.agent.file.type;
                }
            }
            var reader = new FileReader();
            reader.onload = function(event) {
                var contents = event.target.result;
                var uploadedFile = btoa(contents);
                $scope.file = uploadedFile;
            };
            reader.readAsBinaryString(file);
            $timeout(function() {
                $scope.fileUploadDto = {};
                $scope.fileUploadDto.fileName = fileName;
                $scope.fileUploadDto.fileType = fileType
                $scope.fileUploadDto.file = $scope.file;
                ValidateFile.upload($scope.fileUploadDto).$promise.then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            if (btn == 'custBtn') {
                                $scope.locationSetup.customerUploadedFile = $scope.file;
                                $scope.locationSetup.customerFileName = fileName;
                                $scope.isCustomerBusy = false;
                                Notification.success($rootScope.errorMessage["ERR05419"]);
                            }

                            if (btn == 'agentBtn') {
                                $scope.locationSetup.agentUploadedFile = $scope.file;
                                $scope.locationSetup.agentFileName = fileName;
                                $scope.isAgentBusy = false;
                                Notification.success($rootScope.errorMessage["ERR05418"]);
                            }

                        } else {
                            if (btn == 'custBtn') {
                                $scope.locationSetup.customerUploadedFile = null;
                                $scope.locationSetup.customerFileName = null;
                                $scope.customer.fileName = null;
                                $scope.isCustomerBusy = false;
                                $scope.customerUploadText = true;
                            }
                            if (btn == 'agentBtn') {
                                $scope.locationSetup.agentUploadedFile = null;
                                $scope.locationSetup.agentFileName = null;
                                $scope.agent.fileName = null;
                                $scope.isAgentBusy = false;
                                $scope.agentUploadText = true;
                            }
                            $scope.enableUploadFn(btn);

                            var errMsg = $rootScope.errorMessage[data.responseCode];
                            var params = data.params;
                            if (errMsg != "" && params != undefined && params != null && params.length > 0) {
                                for (var i = 0; i < params.length; i++) {
                                    if (errMsg != undefined && errMsg != null) {
                                        errMsg = errMsg.replace("%s", params[i]);
                                    }
                                }
                            }
                            Notification.error(errMsg);
                        }
                    },
                    function(errResponse) {
                        if (btn == 'custBtn') {
                            $scope.locationSetup.customerUploadedFile = null;
                            $scope.locationSetup.customerFileName = null;
                            $scope.customer.fileName = null;
                            $scope.isCustomerBusy = false;
                            $scope.customerUploadText = true;
                        }
                        if (btn == 'agentBtn') {
                            $scope.locationSetup.agentUploadedFile = null;
                            $scope.locationSetup.agentFileName = null;
                            $scope.isAgentBusy = false;
                            $scope.agent.fileName = null;
                            $scope.agentUploadText = true;
                        }
                        $scope.enableUploadFn(btn);
                        Notification.error(errResponse);
                        console.log('Error while uploading  AgentOrCustomer Upload', errResponse);
                    });
            }, 5000);
        }


        $scope.agentPortUpload = function(errMessage, btn) {
            if ($scope.validateUploadFile($scope.agentPort.file, errMessage)) {
                if (btn == 'agentPortBtn') {
                    $scope.agentPortBtn = true;
                }
                $scope.agentPortUploadText = false;
                $scope.isAgentPortBusy = true;
                $scope.agentPort.fileName = $scope.agentPort.file.name;
                var reader = new FileReader();

                reader.onload = function(event) {
                    var contents = event.target.result;
                    var uploadedFile = btoa(contents);
                    $scope.file = uploadedFile;
                };
                reader.readAsBinaryString($scope.agentPort.file);

                if (btn == 'agentPortBtn') {
                    $scope.locationSetup.agentPortUploadedFile = $scope.file;
                    $scope.locationSetup.agentPortFileName = $scope.agentPort.file.name;
                    $scope.isAgentPortBusy = false;
                }

                $timeout(function() {
                    $scope.fileUploadDto = {};
                    $scope.fileUploadDto.fileName = $scope.agentPort.file.name;
                    $scope.fileUploadDto.fileType = $scope.agentPort.file.type;
                    $scope.fileUploadDto.file = $scope.file;
                    if (btn == 'agentPortBtn') {
                        $scope.locationSetup.agentPortUploadedFile = $scope.file;
                        $scope.locationSetup.agentPortFileName = $scope.agentPort.file.name;
                    }
                    /*return AgentPortMasterBulkUpload.upload($scope.fileUploadDto).$promise.then(
                    				function(data, status) {
                    					if (data.responseCode == "ERR0") {
                    						$scope.isAgentPortBusy = false;
                    						$scope.enableUploadFn(btn);
                    						Notification.success("saved successfully");
                    						
                    					} else {
                    						$scope.isAgentPortBusy = false;
                    						$scope.enableUploadFn(btn);
                    						Notification.error(data.responseDescription);
                    					}
                    				},
                    				function(errResponse) {
                    					$scope.isAgentPortBusy = false;
                    					$scope.enableUploadFn(btn);
                    					Notification.error(errResponse);
                    					console.log('Error while uploading  AgentPort Upload',errResponse);
                    				});*/
                }, 5000);
            } else {
                console.log('invalid file format');
            }
        }


        $scope.validateUploadFile = function(file, errMessage) {
            $scope.errorMap = new Map();
            if (file == null && file == undefined) {
                $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0126"]);
                return false;
            }
            if (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                file.name.includes('.xlsx') || file.name.includes('.csv')) {

                var allowedSize = 5;

                if (allowedSize != undefined) {

                    allowedSize = allowedSize * 1024 * 1024;

                    if (file.size <= 0) {

                        $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0127"]);
                        return false;
                    } else if (file.size > allowedSize) {

                        $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0128"]);
                        return false;
                    }

                }
            } else {
                $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0129"]);
                return false;
            }


            return true;
        }

        $scope.validateMetaConfigTab = function(validateCode) {
            console.log("$scope.validateMetaConfigTab");
            return true;
        }

        $rootScope.saveMetaConfigTab = function(stateName) {
            if ($scope.validateMetaConfigTab(0)) {

                console.log("Meta Config Tab ", $scope.locationSetup);

                locationSetupObject = angular.copy($scope.locationSetup);

                locationSetupObject.currentSection = "MetaConfig";

                UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Save Meta Config ", data);
                        $state.go(stateName, {
                            id: data.responseObject.id
                        });
                    } else {
                        Notification.error($rootScope.errorMessage[data.responseCode]);
                        console.log("Create Failed :" + data.responseDescription)
                    }
                }, function(error) {
                    console.log("Create Failed : " + error)
                });
            } else {
                console.log("Login Tab data is not valid");
            }

        }

        $rootScope.currentSection = 'metaConfig';

        $scope.initMetaConfigTab();

    }]);