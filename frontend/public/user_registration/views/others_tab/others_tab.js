angular.module("NewAgeRegistration").directive('capitalize', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var caretPos,
                capitalize = function(inputValue) {
                    caretPos = element[0].selectionStart; // save current caret position

                    //console.log('curPos before change', caretPos);
                    if (inputValue == undefined)
                        inputValue = '';

                    var capitalized = inputValue.toUpperCase();
                    //console.log(capitalized, inputValue);
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                        element[0].selectionStart = caretPos; // restore position
                        element[0].selectionEnd = caretPos;
                    }
                    return capitalized;
                };
            modelCtrl.$parsers.push(capitalize);

            capitalize(scope[attrs.ngModel]); // capitalize initial value
        }
    };
});

angular.module("NewAgeRegistration").filter('SequenceFilter', function() {

    return function(value) {

        if (value == 'PARTY') {
            return 'Customer';
        } else if (value == 'EMPLOYEE') {
            return 'Employee';
        } else if (value == 'ENQUERY') {
            return 'Enquiry';
        } else if (value == 'SHIPMENT') {
            return 'Shipment';
        } else if (value == 'CONSOL') {
            return 'Master Shipment';
        } else if (value == 'SERVICE') {
            return 'Service';
        } else if (value == 'DOCUMENT') {
            return 'Document';
        } else if (value == 'FLIGHTPLAN') {
            return 'Flight Plan'
        } else if (value == 'DO') {
            return 'DO';
        } else if (value == 'CAN') {
            return 'CAN';
        } else if (value == 'QUOTATION') {
            return 'Quotation';
        } else if (value == 'CFS') {
            return 'CFS';
        } else if (value == 'PURCHASE_ORDER') {
            return 'Purchase Order';
        } else if (value == 'AIRLINE_PREBOOKING') {
            return 'Airline Prebooking';
        } else if (value == 'REFERENCE_NO') {
            return 'Reference No';
        } else if (value == 'DESIGNATION') {
            return 'Designation';
        } else if (value == 'DEPARTMENT') {
            return 'Department';
        } else if (value == 'AES_BATCH') {
            return 'AES Batch';
        }

    }
});




angular.module("NewAgeRegistration").controller("OthersTabController",['$rootScope', '$scope', '$timeout', '$state', 'Notification', '$location', '$http',
        'UserRegistrationDataService', 'UserRegistrationFactory', '$stateParams', 'UserRegistrationUpdateService', '$modal',
    function($rootScope, $scope, $timeout, $state, Notification, $location, $http,
        UserRegistrationDataService, UserRegistrationFactory, $stateParams, UserRegistrationUpdateService, $modal) {


        $scope.initOtherTab = function() {
            console.log("Init Other Tab");
            if ($stateParams != undefined && $stateParams != null && $stateParams.id != null) {
                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
                    method: "GET",
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }
                }).success(function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationSetup = data.responseObject;


                        if ($scope.locationSetup != null) {

                            if ($scope.locationSetup.sequenceList == null || $scope.locationSetup.sequenceList.length == 0) {
                                $http({
                                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/loadsequencefromexcel",
                                    method: "GET",
                                    headers: {
                                        "SAAS_ID": $rootScope.saasId
                                    }
                                }).success(function(data) {
                                    if (data.responseCode == "ERR0") {
                                        $scope.locationSetup.sequenceList = data.responseObject;
                                        if ($scope.locationSetup.sequenceList != undefined && $scope.locationSetup.sequenceList != null && $scope.locationSetup.sequenceList.length > 0) {
                                            for (var i = 0; i < $scope.locationSetup.sequenceList.length; i++) {
                                                $scope.getSampleSequenceValue(i);
                                            }
                                        }
                                    }
                                    if ($scope.locationSetup.sequenceList == undefined || $scope.locationSetup.sequenceList == null || $scope.locationSetup.sequenceList.length == 0) {
                                        $scope.locationSetup.sequenceList = [{}];
                                    }
                                });
                            } else {
                                for (var i = 0; i < $scope.locationSetup.sequenceList.length; i++) {
                                    $scope.getSampleSequenceValue(i);
                                }
                            }

                            if ($scope.locationSetup.daybookMasterList == undefined || $scope.locationSetup.daybookMasterList == null || $scope.locationSetup.daybookMasterList.length == 0) {
                                $scope.locationSetup.daybookMasterList = [{}];
                            }

                            $scope.locationSetup.exportJobDate = 'ETD';
                            $scope.locationSetup.importJobDate = 'ETA';
                            console.log("locationSetup ", $scope.locationSetup);
                            $rootScope.navigateToNextField("sequence.sequenceType0");
                            $rootScope.currentLocationSetupId = $scope.locationSetup.id;
                        }
                    }
                });
            }
        }

        $scope.boolToStr = function(arg) {
            return arg ? 'Yes' : 'No'
        };

        $scope.directiveDatePickerOpts = angular.copy($rootScope.dateWithOutTimePickerOptions);

        $scope.setTab = function(tabName) {
            $scope.currentTab = tabName;
            if (tabName == "tax_vat_tab") {
                $rootScope.navigateToNextField("serviceTaxNo");
            } else if (tabName == "job_no_tab") {
                $rootScope.navigateToNextField("sequence.sequenceType0");
            } else if (tabName == "accounts_tab") {
                $rootScope.navigateToNextField("daybookObj.daybookCode0");
            } else if (tabName == "job_date_tab") {
                $rootScope.navigateToNextField("exportTransportMode");
            }
        }

        $scope.isTab = function(tabName) {
            return $scope.currentTab == tabName;
        }

        $scope.back = function() {
            $state.go("app.charge", {
                id: $rootScope.currentLocationSetupId
            });
        }
        $scope.ajaxServiceMasterEvent = function(object) {
            console.log("ajaxServiceMasterEvent is called ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return UserRegistrationDataService.serviceSearch($scope.searchDto, urlPath, $rootScope.saasId).then(function(response) {
                if (response.data.responseCode == 'ERR0') {
                    return $scope.serviceMasterList = response.data.responseObject.searchResult;
                }
            }).catch(function(response) {
                console.error('Status', response.status, response);
            });
        }

        $scope.selectedServiceMaster = function(taxOrVatSetupDto, nextField) {
            $rootScope.navigateToNextField(nextField);
        }

        $scope.ajaxChargeMasterEvent = function(object) {
            console.log("ajaxChargeMasterEvent is called ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return UserRegistrationDataService.chargeSearch($scope.searchDto, urlPath, $rootScope.saasId).then(function(response) {
                if (response.data.responseCode == 'ERR0') {
                    return $scope.chargeMasterList = response.data.responseObject.searchResult;
                }
            }).catch(function(response) {
                console.error('Status', response.status, response);
            });
        }

        $scope.selectedChargeMaster = function(taxOrVatSetupDto, nextField) {
            if (taxOrVatSetupDto.chargeMaster != undefined && taxOrVatSetupDto.chargeMaster != null && taxOrVatSetupDto.chargeMaster.id != null) {
                taxOrVatSetupDto.chargeCode = taxOrVatSetupDto.chargeMaster.chargeCode;
                $rootScope.navigateToNextField(nextField);
            } else {
                taxOrVatSetupDto.chargeCode = null;
            }
        }

        $scope.ajaxCategoryMasterEvent = function(object) {
            console.log("ajaxCategoryMasterEvent is called ", object);
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return UserRegistrationDataService.categorySearch($scope.searchDto, urlPath, $rootScope.saasId).then(function(response) {
                if (response.data.responseCode == 'ERR0') {
                    return $scope.categoryMasterList = response.data.responseObject.searchResult;
                }
            }).catch(function(response) {
                console.error('Status', response.status, response);
            });
        }

        $scope.selectedCategoryMaster = function(taxOrVatSetupDto, nextField) {
            if (taxOrVatSetupDto.categoryMaster != undefined && taxOrVatSetupDto.categoryMaster != null && taxOrVatSetupDto.categoryMaster.id != null) {
                taxOrVatSetupDto.categoryCode = taxOrVatSetupDto.categoryMaster.categoryCode;
                $rootScope.navigateToNextField(nextField);
            } else {
                taxOrVatSetupDto.categoryCode = null;
            }
        }

        $scope.deleteTaxOrVatSetupRow = function(index) {
            $scope.locationSetup.taxOrVatSetupDtoList.splice(index, 1);
            $scope.taxOrVatErrorArr = [];
        }

        $scope.deleteJobNumber = function(index) {
            $scope.locationSetup.sequenceList.splice(index, 1);
            $scope.errorArrayForSequence = [];
        }

        $scope.deleteAccountDocumentNoSetupRow = function(index) {
            $scope.locationSetup.daybookMasterList.splice(index, 1);
            $scope.errorArrayForDaybook = [];
        }

        //Add Tax Or Vat Setup row
        $scope.addTaxOrVatSetupRow = function() {
            $scope.firstFocus = true;
            if ($scope.otherSetupDto == undefined || $scope.otherSetupDto == null) {
                $scope.otherSetupDto = {};
            }
            if ($scope.locationSetup.taxOrVatSetupDtoList == undefined || $scope.locationSetup.taxOrVatSetupDtoList == null) {
                $scope.locationSetup.taxOrVatSetupDtoList = [];
            }
            var index = $scope.locationSetup.taxOrVatSetupDtoList.length;
            var fineArr = [];
            $scope.taxOrVatErrorArr = [];
            angular.forEach($scope.locationSetup.taxOrVatSetupDtoList, function(dataObj, index) {
                $scope.taxOrVatErrorArr[index] = {};
                var validResult = $scope.validateTaxOrVatSetup(dataObj, index);
                if (validResult) {
                    fineArr.push(1);
                    $scope.taxOrVatErrorArr[index].errRow = false;
                } else {
                    $scope.taxOrVatErrorArr[index].errRow = true;
                    fineArr.push(0);
                }
            });
            if (fineArr.indexOf(0) < 0) {
                $scope.locationSetup.taxOrVatSetupDtoList.push({});
                $scope.taxOrVatErrorArr.push({
                    "errRow": false
                });
                $scope.indexFocus = null;
                if ($scope.tableState) {
                    $scope.tableState({
                        param: true
                    });
                }
            } else {
                if ($scope.tableState) {
                    $scope.tableState({
                        param: false
                    });
                }
            }

        };

        $scope.validateTaxOrVatSetup = function(dataObj, index) {
            var errorArr = [];
            $scope.taxOrVatErrorArr = [];
            $scope.taxOrVatErrorArr[index] = {};
            $scope.taxOrVatErrorArr[index].errTextArr = [];

            //charge master
            if (dataObj.chargeMaster == null || dataObj.chargeMaster == undefined || dataObj.chargeMaster.id == null || dataObj.chargeMaster.id == undefined) {
                console.log("Charge is Mandatory.")
                $scope.taxOrVatErrorArr[index].chargeMaster = true;
                $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0132"]);
                errorArr.push(0);
            } else {
                if (dataObj.chargeMaster.status == 'Block') {
                    console.log("Selected Charge is Blocked.");
                    $scope.taxOrVatErrorArr[index].chargeMaster = true;
                    $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0133"]);
                    errorArr.push(0);
                } else if (dataObj.chargeMaster.status == 'Hide') {
                    console.log("Selected Charge is Hidden.");
                    $scope.taxOrVatErrorArr[index].chargeMaster = true;
                    $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0134"]);
                    errorArr.push(0);
                } else {
                    $scope.taxOrVatErrorArr[index].chargeMaster = false;
                    errorArr.push(1);
                }
            }
            //category Master
            if (dataObj.categoryMaster == null || dataObj.categoryMaster == undefined || dataObj.categoryMaster.id == null || dataObj.categoryMaster.id == undefined) {
                console.log("Category is Mandatory.")
                $scope.taxOrVatErrorArr[index].categoryMaster = true;
                $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0135"]);
                errorArr.push(0);
            } else {
                if (dataObj.chargeMaster.status == 'Block') {
                    console.log("Selected Category is Blocked.");
                    $scope.taxOrVatErrorArr[index].chargeMaster = true;
                    $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0136"]);
                    errorArr.push(0);
                } else if (dataObj.categoryMaster.status == 'Hide') {
                    console.log("Selected Category is Hidden.");
                    $scope.taxOrVatErrorArr[index].categoryMaster = true;
                    $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0137"]);
                    errorArr.push(0);
                } else {
                    $scope.taxOrVatErrorArr[index].categoryMaster = false;
                    errorArr.push(1);
                }
            }

            //percentage
            if (dataObj.percentage != null && dataObj.percentage != undefined && dataObj.percentage != '') {
                var percentage = parseFloat(dataObj.percentage);
                if (isNaN(percentage) || percentage < 0 || percentage > 100) {
                    $scope.taxOrVatErrorArr[index].percentage = true;
                    $scope.taxOrVatErrorArr[index].errTextArr.push($rootScope.errorMessage["ERRC0138"]);
                    errorArr.push(0);
                }
            }

            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }

        };

        validateSequenceObj = function(dataObj, index, sequenceList) {

            var errorArr = [];
            $scope.errorArrayForSequence[index] = {};


            $scope.errorArrayForSequence[index].errTextArr = [];

            if (dataObj.sequenceType == undefined || dataObj.sequenceType == null || dataObj.sequenceType == "") {
                $scope.errorArrayForSequence[index].sequenceType = true;
                $scope.errorArrayForSequence[index].errTextArr.push($rootScope.errorMessage["ERRC0139"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForSequence[index].sequenceType = false;
                errorArr.push(1);
            }

            angular.forEach(sequenceList, function(sequence, indexing) {
                if (indexing != index && sequence.sequenceType == dataObj.sequenceType) {
                    $scope.errorArrayForSequence[index].sequenceType = true;
                    $scope.errorArrayForSequence[index].errTextArr.push($rootScope.errorMessage["ERR14"]);
                    errorArr.push(0);
                }

                if (indexing != index && sequence.prefix == dataObj.prefix && dataObj.prefix != null && dataObj.prefix.trim().length != 0) {
                    $scope.errorArrayForSequence[index].prefix = true;
                    $scope.errorArrayForSequence[index].errTextArr.push($rootScope.errorMessage["ERR48"]);
                    errorArr.push(0);
                }
            });


            if (dataObj.format == undefined || dataObj.format == null || dataObj.format == "") {
                $scope.errorArrayForSequence[index].noOfDigits = true;
                $scope.errorArrayForSequence[index].errTextArr.push($rootScope.errorMessage["ERRC0140"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForSequence[index].noOfDigits = false;
                errorArr.push(1);
            }

            if (dataObj.currentSequenceValue == undefined || dataObj.currentSequenceValue == null || dataObj.currentSequenceValue == "") {
                $scope.errorArrayForSequence[index].currentSequenceValue = true;
                $scope.errorArrayForSequence[index].errTextArr.push($rootScope.errorMessage["ERRC0141"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForSequence[index].currentSequenceValue = false;
                errorArr.push(1);
            }

            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }
        }


        function isEmptyRow(obj) {

            var isempty = true;
            if (!obj) {
                return isempty;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;

                }
            }
            return isempty;

        }

        $scope.jobNumberTableCheck = function() {
            var fineArr = [];
            $scope.errorArrayForSequence = [];
            var validResult;
            var finalData = $scope.locationSetup.sequenceList;
            var emptyIndex = [];
            angular.forEach($scope.locationSetup.sequenceList, function(dataObj, index) {
                $scope.errorArrayForSequence[index] = {};
                if (isEmptyRow(dataObj)) {
                    validResult = true;
                    finalData[index].isEmpty = true;
                } else {
                    validResult = validateSequenceObj(dataObj, index, $scope.locationSetup.sequenceList);
                }
                if (validResult) {
                    fineArr.push(1);
                    $scope.errorArrayForSequence[index].errRow = false;
                } else {
                    $scope.errorArrayForSequence[index].errRow = true;
                    fineArr.push(0);
                }
            });
            angular.forEach(finalData, function(dataObj, index) {
                if (dataObj.isEmpty) {
                    finalData.splice(index, 1);
                    $scope.errorArrayForSequence.splice(index, 1);
                }
            });
            if (fineArr.indexOf(0) < 0) {
                $scope.locationSetup.sequenceList = finalData;
                return true;
            } else {
                return false;
            }
        }


        $scope.copyJobNo = function(obj) {
            var tempObj = angular.copy(obj);
            tempObj.id = null;
            $scope.locationSetup.sequenceList.push(tempObj);
        }

        $scope.copyAccountDocNo = function(obj) {
            var tempObj = angular.copy(obj);
            tempObj.id = null;
            $scope.locationSetup.daybookMasterList.push(tempObj);
        }

        $scope.errorArrayForSequence = [];

        $scope.createNewRow = function(event, type) {
            if (!event.shiftKey && event.keyCode == 9) {
                if (type == 'job') {
                    $scope.addJobNumber();
                } else if (type == 'account') {
                    $scope.addAccDocNoSetupRow();
                }
            }
        }
        $scope.addJobNumber = function() {
            var fineArr = [];
            angular.forEach($scope.locationSetup.sequenceList, function(dataObj, index) {
                $scope.errorArrayForSequence[index] = {};
                var validResult = validateSequenceObj(dataObj, index, $scope.locationSetup.sequenceList);
                if (validResult) {
                    fineArr.push(1);
                    $scope.errorArrayForSequence[index].errRow = false;
                } else {
                    $scope.errorArrayForSequence[index].errRow = true;
                    fineArr.push(0);
                }
            });

            if (fineArr.indexOf(0) < 0) {
                if ($scope.locationSetup.sequenceList == undefined || $scope.locationSetup.sequenceList == null) {
                    $scope.locationSetup.sequenceList = [];
                }
                $scope.locationSetup.sequenceList.push({});
                $rootScope.navigateToNextField("sequence.sequenceType" + $scope.locationSetup.sequenceList.length - 1);
                $scope.errorArrayForSequence.push({
                    "errRow": false
                });
            }
        }


        validateDaybookObj = function(dataObj, index, daybookList) {

            var errorArr = [];
            $scope.errorArrayForDaybook[index] = {};

            $scope.errorArrayForDaybook[index].errTextArr = [];

            if (dataObj.daybookCode == undefined || dataObj.daybookCode == null || dataObj.daybookCode == "") {
                $scope.errorArrayForDaybook[index].daybookCode = true;
                $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERRC0142"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForDaybook[index].daybookCode = false;
                errorArr.push(1);
            }

            if (dataObj.daybookName == undefined || dataObj.daybookName == null || dataObj.daybookName == "") {
                $scope.errorArrayForDaybook[index].daybookName = true;
                $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERRC0143"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForDaybook[index].daybookName = false;
                errorArr.push(1);
            }

            if (dataObj.documentTypeMaster == undefined || dataObj.documentTypeMaster == null || dataObj.documentTypeMaster == "") {
                $scope.errorArrayForDaybook[index].documentTypeMaster = true;
                $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERRC0151"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForDaybook[index].documentTypeMaster = false;
                errorArr.push(1);
            }


            if (dataObj.format == undefined || dataObj.format == null || dataObj.format == "") {
                $scope.errorArrayForDaybook[index].format = true;
                $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERRC0140"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForDaybook[index].format = false;
                errorArr.push(1);
            }

            if (dataObj.currentSequenceValue == undefined || dataObj.currentSequenceValue == null || dataObj.currentSequenceValue == "") {
                $scope.errorArrayForDaybook[index].currentSequenceValue = true;
                $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERRC0141"]);
                errorArr.push(0);
            } else {
                $scope.errorArrayForDaybook[index].currentSequenceValue = false;
                errorArr.push(1);
            }


            angular.forEach(daybookList, function(daybook, indexing) {
                if (indexing != index && daybook.daybookCode == dataObj.daybookCode) {
                    $scope.errorArrayForDaybook[index].daybookCode = true;
                    $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERR200009"]);
                    errorArr.push(0);
                }

                if (indexing != index && daybook.daybookName == dataObj.daybookName) {
                    $scope.errorArrayForDaybook[index].daybookName = true;
                    $scope.errorArrayForDaybook[index].errTextArr.push($rootScope.errorMessage["ERR200010"]);
                    errorArr.push(0);
                }
            });

            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }

            return true;

        }

        $scope.daybookTableCheck = function() {
            var fineArr = [];
            $scope.errorArrayForDaybook = [];
            var validResult;
            var finalData = $scope.locationSetup.daybookMasterList;
            var emptyIndex = [];
            angular.forEach($scope.locationSetup.daybookMasterList, function(dataObj, index) {
                $scope.errorArrayForDaybook[index] = {};
                if (isEmptyRow(dataObj)) {
                    validResult = true;
                    finalData[index].isEmpty = true;
                } else {
                    validResult = validateDaybookObj(dataObj, index, $scope.locationSetup.daybookMasterList);
                }
                if (validResult) {
                    fineArr.push(1);
                    $scope.errorArrayForDaybook[index].errRow = false;
                } else {
                    $scope.errorArrayForDaybook[index].errRow = true;
                    fineArr.push(0);
                }
            });
            angular.forEach(finalData, function(dataObj, index) {
                if (dataObj.isEmpty) {
                    finalData.splice(index, 1);
                    $scope.errorArrayForDaybook.splice(index, 1);
                }
            });
            if (fineArr.indexOf(0) < 0) {
                $scope.locationSetup.daybookMasterList = finalData;
                return true;
            } else {
                return false;
            }
        }



        $scope.errorArrayForDaybook = [];
        $scope.addAccDocNoSetupRow = function() {

            var fineArr = [];
            angular.forEach($scope.locationSetup.daybookMasterList, function(dataObj, index) {
                $scope.errorArrayForDaybook[index] = {};
                var validResult = validateDaybookObj(dataObj, index, $scope.locationSetup.daybookMasterList);
                if (validResult) {
                    fineArr.push(1);
                    $scope.errorArrayForDaybook[index].errRow = false;
                } else {
                    $scope.errorArrayForDaybook[index].errRow = true;
                    fineArr.push(0);
                }
            });

            if (fineArr.indexOf(0) < 0) {
                if ($scope.locationSetup.daybookMasterList == undefined || $scope.locationSetup.daybookMasterList == null) {
                    $scope.locationSetup.daybookMasterList = [];
                }
                $scope.locationSetup.daybookMasterList.push({});
                $rootScope.navigateToNextField("daybookObj.daybookCode" + $scope.locationSetup.daybookMasterList.length - 1);
                $scope.errorArrayForDaybook.push({
                    "errRow": false
                });
            }
        };


        $rootScope.saveOtherTab = function(stateName) {

            if ($scope.jobNumberTableCheck()) {

                if ($scope.daybookTableCheck()) {

                    console.log("Others Tab", $scope.locationSetup);
                    locationSetupObject = angular.copy($scope.locationSetup);

                    if (locationSetupObject.importJobDate == 'ETA') {
                        //locationSetupObject.importJobDate = 'Eta'
                    } else {
                        locationSetupObject.importJobDate = 'Date'
                    }

                    if (locationSetupObject.exportJobDate == 'ETD') {
                        //locationSetupObject.exportJobDate = 'Etd'
                    } else {
                        locationSetupObject.exportJobDate = 'Date'
                    }
                    if (stateName == 'viewMail') {
                        locationSetupObject.currentSection = "Completed";
                    } else {
                        locationSetupObject.currentSection = "Other";
                    }
                    UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("Other Tab Saved - ", data);
                            $state.go(stateName, {
                                id: $rootScope.currentLocationSetupId
                            });
                        } else {
                            Notification.error($rootScope.errorMessage[data.responseCode]);
                            console.log("Create Failed :" + data.responseDescription)
                        }
                    }, function(error) {
                        console.log("Create Failed : " + error)
                    });
                } else {
                    $scope.setTab('accounts_tab');
                    return false;
                }
            } else {
                $scope.setTab('job_no_tab');
                return false;
            }

        }


        $scope.getSampleDaybook = function(index) {

            if (index == null || index == undefined) {
                return;
            }
            var daybookObj = $scope.locationSetup.daybookMasterList[index];

            if (daybookObj == null) {
                return;
            }

            var genaratedSequence = "";

            var separator = "";

            if (daybookObj.separator != null && daybookObj.separator != undefined && daybookObj.separator != '') {
                separator = daybookObj.separator;
            }


            /*if(daybookObj.daybookCode!=null && daybookObj.daybookCode!=undefined && daybookObj.daybookCode!=''){
              if(genaratedSequence==''){
                genaratedSequence = genaratedSequence + daybookObj.daybookCode;
              }else{
                genaratedSequence = genaratedSequence + separator + daybookObj.daybookCode;
              }
            }
    
            if(daybookObj.daybookName!=null && daybookObj.daybookName!=undefined && daybookObj.daybookName!=''){
              if(genaratedSequence==''){
                genaratedSequence = genaratedSequence + daybookObj.daybookName;
              }else{
                genaratedSequence = genaratedSequence + separator + daybookObj.daybookName;
              }
            }*/

            if (daybookObj.prefix != null && daybookObj.prefix != undefined && daybookObj.prefix != '') {
                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + daybookObj.prefix;
                } else {
                    genaratedSequence = genaratedSequence + separator + daybookObj.prefix;
                }
            }

            if (daybookObj.isYear != null && (daybookObj.isYear == 'Yes' || daybookObj.isYear == true)) {

                var dt = new Date();

                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + (dt.getYear() + 1900);
                } else {
                    genaratedSequence = genaratedSequence + separator + (dt.getYear() + 1900);
                }
            }

            if (daybookObj.isMonth != null && (daybookObj.isMonth == 'Yes' || daybookObj.isMonth == true)) {

                var dt = new Date();

                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + $scope.pad(dt.getMonth() + 1);
                } else {
                    genaratedSequence = genaratedSequence + separator + $scope.pad(dt.getMonth() + 1);
                }
            }

            if (genaratedSequence == '') {
                genaratedSequence = genaratedSequence + $scope.format(daybookObj.format, daybookObj.currentSequenceValue);
            } else {
                genaratedSequence = genaratedSequence + separator + $scope.format(daybookObj.format, daybookObj.currentSequenceValue);
            }

            if (daybookObj.suffix != null && daybookObj.suffix != '') {
                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + daybookObj.suffix;
                } else {
                    genaratedSequence = genaratedSequence + separator + daybookObj.suffix;
                }
            }

            daybookObj.genaratedSequence = genaratedSequence;

        }


        $scope.getSampleSequenceValue = function(index) {

            if (index == null || index == undefined) {
                return;
            }
            var sequence = $scope.locationSetup.sequenceList[index];

            if (sequence == null) {
                return;
            }

            var genaratedSequence = "";

            var separator = "";

            if (sequence.separator != null && sequence.separator != undefined && sequence.separator != '') {
                separator = sequence.separator;
            }

            if (sequence.prefix != null && sequence.prefix != undefined && sequence.prefix != '') {
                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + sequence.prefix;
                } else {
                    genaratedSequence = genaratedSequence + separator + prefix;
                }
            }

            if (sequence.isYear != null && (sequence.isYear == 'Yes' || sequence.isYear == true)) {

                var dt = new Date();

                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + (dt.getYear() + 1900);
                } else {
                    genaratedSequence = genaratedSequence + separator + (dt.getYear() + 1900);
                }
            }

            if (sequence.isMonth != null && (sequence.isMonth == 'Yes' || sequence.isMonth == true)) {

                var dt = new Date();

                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + $scope.pad(dt.getMonth() + 1);
                } else {
                    genaratedSequence = genaratedSequence + separator + $scope.pad(dt.getMonth() + 1);
                }
            }

            if (genaratedSequence == '') {
                genaratedSequence = genaratedSequence + $scope.format(sequence.format, sequence.currentSequenceValue);
            } else {
                genaratedSequence = genaratedSequence + separator + $scope.format(sequence.format, sequence.currentSequenceValue);
            }

            if (sequence.suffix != null && sequence.suffix != '') {
                if (genaratedSequence == '') {
                    genaratedSequence = genaratedSequence + sequence.suffix;
                } else {
                    genaratedSequence = genaratedSequence + separator + sequence.suffix;
                }
            }

            sequence.genaratedSequence = genaratedSequence;

        }

        $scope.pad = function(d) {
            return (d < 10) ? '0' + d.toString() : d.toString();
        }

        $scope.getNumber = function(string) {
            if (string == 'ONE') {
                return 1;
            } else if (string == 'TWO') {
                return 2;
            } else if (string == 'THREE') {
                return 3;
            } else if (string == 'FOUR') {
                return 4;
            } else if (string == 'FIVE') {
                return 5;
            } else if (string == 'SIX') {
                return 6;
            } else if (string == 'SEVEN') {
                return 7;
            } else if (string == 'EIGHT') {
                return 8;
            } else if (string == 'NINE') {
                return 9;
            } else if (string == 'TEN') {
                return 10;
            }
        }

        $scope.format = function(theString, currentSequence) {
            var value = '';
            if (theString == null || theString == undefined) {
                return value;
            }
            var num = $scope.getNumber(theString);
            if (currentSequence == undefined || currentSequence == null || currentSequence == "") {
                currentSequence = 1;
            }
            num = num - currentSequence.toString().length;

            for (var i = 1; i <= num; i++) {
                value = value + '0';
            }
            value = value + currentSequence;
            return value;
        }

        //show errors
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'views/others_tab/errorPopUp.html',
            show: false,
            keyboard: true
        });
        var errorOnRowIndex = null;
        $scope.errorShow = function(errorObj, index) {
            $scope.errList = errorObj.errTextArr;
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };


        $scope.sequenceTypeArray = ["PARTY", "EMPLOYEE", "ENQUERY", "SHIPMENT", "CONSOL", "SERVICE", "DOCUMENT", "FLIGHTPLAN",
            "DO", "CAN", "QUOTATION", "CFS", "PURCHASE_ORDER", "AIRLINE_PREBOOKING",
            "REFERENCE_NO", "DESIGNATION", "DEPARTMENT", "AES_BATCH"
        ];

        $scope.getDocumentTypeList = function(keyword) {
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/documenttype/get/search/keyword").then(function(response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function(error) {
                console.log("Error", error);
            });
        }

        $scope.selectedDocumentType = function(id) {
            $scope.navigateToNextField(id);
        };


        $scope.numberOfDigitsArray = [{
                displayValue: "1",
                value: "ONE"
            },
            {
                displayValue: "2",
                value: "TWO"
            },
            {
                displayValue: "3",
                value: "THREE"
            },
            {
                displayValue: "4",
                value: "FOUR"
            },
            {
                displayValue: "5",
                value: "FIVE"
            },
            {
                displayValue: "6",
                value: "SIX"
            },
            {
                displayValue: "7",
                value: "SEVEN"
            },
            {
                displayValue: "8",
                value: "EIGHT"
            },
            {
                displayValue: "9",
                value: "NINE"
            },
            {
                displayValue: "10",
                value: "TEN"
            }
        ];

        $rootScope.currentSection = 'Other';

        $scope.initOtherTab();


    }]);