angular.module("NewAgeRegistration").controller("ConfigurationTabController",['$rootScope', '$scope', '$timeout', '$state', 'Notification',
        'UserRegistrationDataService', 'UserRegistrationFactory', '$http', '$stateParams',
    function($rootScope, $scope, $timeout, $state, Notification,
        UserRegistrationDataService, UserRegistrationFactory, $http, $stateParams) {


        $scope.initConfigurationTab = function() {
            console.log("initConfigurationTab");
            if ($stateParams != undefined && $stateParams != null && $stateParams.id != null) {
                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
                    method: "GET",
                    
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }

                }).success(function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationSetup = data.responseObject;
                        console.log("locationSetup ", $scope.locationSetup);

                        if ($rootScope.configurations != undefined && $rootScope.configurations != null) {
                            var makeImperial = $rootScope.configurations['imperial.or.metric'];
                            var makeImperialArray;
                            if (makeImperial != null && makeImperial != "") {
                                makeImperialArray = makeImperial.split(",");
                            }


                            if ($scope.locationSetup.countryMaster != undefined &&
                                $scope.locationSetup.countryMaster != null &&
                                $scope.locationSetup.countryMaster.countryCode != null &&
                                $scope.locationSetup.countryMaster.countryCode != undefined) {

                                if (makeImperialArray != null) {
                                    var found = makeImperialArray.find(function(m) {
                                        return m === $scope.locationSetup.countryMaster.countryCode;
                                    });

                                    if (found != undefined && found != "") {
                                        $scope.locationSetup.measurement = false;
                                    } else {
                                        $scope.locationSetup.measurement = true;
                                    }
                                }
                            }
                        }

                        if ($scope.locationSetup != null) {
                            $rootScope.currentLocationSetupId = $scope.locationSetup.id;
                        }
                    }
                })
            }
        }
        $scope.back = function() {
            $state.go("app.login", {
                id: $rootScope.currentLocationSetupId
            });
        }
        $scope.getCurrencyList = function(keyword) {
            if(!keyword)return false;
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/currencymaster/get/search/keyword").then(function(response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function(error) {
                console.log("Error", error);
            });
        }



        $scope.selectFinancialStart = function() {
            if ($scope.locationSetup.financialStartDate != undefined && $scope.locationSetup.financialStartDate != null && $scope.locationSetup.financialStartDate != "") {
                $scope.locationSetup.financialEndDate = (moment($scope.locationSetup.financialStartDate).add("months", "11").format($rootScope.datePickerOptions.format));
            } else {
                $scope.locationSetup.financialEndDate = null;
                $scope.locationSetup.financialStartDate = null;
            }
            if ($scope.locationSetup.financialEndDate == "Invalid date") {
                $scope.locationSetup.financialStartDate = null;
                $scope.locationSetup.financialEndDate = null;
            }
            $rootScope.navigateToNextField("backButton");
        }

        $scope.selectedCurrency = function(id) {
            $scope.errorMap = new Map();
            $scope.errorMap.put("locationSetup.currencyMaster", null);
            $rootScope.navigateToNextField(id);
        };

        $scope.getLanguageList = function(keyword) {
            if(!keyword)return false;
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/language/get/search/keyword").then(function(response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function(error) {
                console.log("Error", error);
            });
        }

        $scope.selectedLanguage = function(id) {
            $scope.errorMap = new Map();
            $scope.errorMap.put("locationSetup.languageMaster", null);
            $rootScope.navigateToNextField(id);
        };

        $scope.validateConfigurationTab = function(validateCode) {
            console.debug("validateCode for Config tab =====>",validateCode)

            if (validateCode == 0 || validateCode == 2) {
                $scope.errorMap = new Map();
                if ($scope.locationSetup.currencyMaster == null || $scope.locationSetup.currencyMaster == "") {
                    $scope.errorMap.put("locationSetup.currencyMaster", "Value should be Provided in the Currency field.");
                    $scope.locationSetup.currencyMaster = null;
                    return false;
                }
                if ($scope.locationSetup.languageMaster == null || $scope.locationSetup.languageMaster == "") {
                    $scope.errorMap.put("locationSetup.languageMaster", "Language cannot be empty");
                    $scope.locationSetup.languageMaster = null;
                    return false;
                }
            }

            return true;
        }

        $rootScope.saveLocationConfiguration = function(stateName) {
            var locationSetupObject = angular.copy($scope.locationSetup);
            
            //for clicking back seamlessly i.e no data check
            if(stateName === 'app.login'){
                saveLocationConfigurationFn(locationSetupObject,stateName);
            } else if(stateName === 'app.metaconfig'){
                if ($scope.validateConfigurationTab(0)) {
                    locationSetupObject.currentSection = "Configuration";
                    saveLocationConfigurationFn(locationSetupObject,stateName);
                } else {
                    console.log("Configuration Tab data is not valid");
                }
            }
            
        }

        var saveLocationConfigurationFn = function(locationSetupObject,stateName){
            UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Location Master saved :: ", data);
                    $state.go(stateName, {
                        id: data.responseObject.id
                    });
                } else {
                    Notification.error($rootScope.errorMessage[data.responseCode]);
                    console.log("Create Failed :" + data.responseDescription)
                }
            }, function(error) {
                console.log("Create Failed : " + error)
            });
        };

        $rootScope.currentSection = 'Configuration';
        $rootScope.navigateToNextField("currencyMaster");
        $scope.initConfigurationTab();
    }]);