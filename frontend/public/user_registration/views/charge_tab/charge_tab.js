angular.module("NewAgeRegistration").directive('capitalize', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var caretPos,
                capitalize = function(inputValue) {
                    caretPos = element[0].selectionStart; // save current caret position

                    //console.log('curPos before change', caretPos);
                    if (inputValue == undefined)
                        inputValue = '';

                    var capitalized = inputValue.toUpperCase();
                    //console.log(capitalized, inputValue);
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                        element[0].selectionStart = caretPos; // restore position
                        element[0].selectionEnd = caretPos;
                    }
                    return capitalized;
                };
            modelCtrl.$parsers.push(capitalize);

            capitalize(scope[attrs.ngModel]); // capitalize initial value
        }
    };
});

angular.module("NewAgeRegistration").directive('tabNav', ['$timeout', function($timeout) {
    return function(scope, element, attr) {

        element.on('keydown.tabNav', 'input[type="text"],select,textarea,a', handleNavigation);


        function handleNavigation(e) {

            var key = e.keyCode ? e.keyCode : e.which;
            if (key == 9 && !e.shiftKey) {
                var focusedElement = $(e.target);
                if (focusedElement.hasClass('last-child')) {
                    var table = $(focusedElement).closest('table');
                    var lastRow = $(table).find("tbody tr:last").find('.last-child');
                    var focusedElementKey = focusedElement[0]['$$hashKey'];
                    var lastRowKey;
                    lastRowKey = lastRow[0]['$$hashKey'];
                    if (lastRowKey == undefined && lastRow[1] != undefined) {
                        lastRowKey = lastRow[1]['$$hashKey'];
                    }
                    console.log("focusedElementKey", focusedElementKey);
                    console.log("lastRowKey", lastRowKey);
                    if (focusedElementKey == lastRowKey) {
                        if (attr.tabRow) {
                            scope.$evalAsync(attr.tabRow);
                        }
                    }


                    //if($(table +'tr:last-child td'))


                }

            }

        }
    };
}]);

angular.module("NewAgeRegistration").controller("ChargeTabController", ['$rootScope', '$scope', '$timeout', '$state', 'Notification',
                'UserRegistrationDataService', 'UserRegistrationFactory', '$http', 'ChargeMasterBulkUpload', '$stateParams', '$modal',
            function($rootScope, $scope, $timeout, $state, Notification,
                UserRegistrationDataService, UserRegistrationFactory, $http, ChargeMasterBulkUpload, $stateParams, $modal) {

                $scope.initChargeTab = function() {
                    console.log("initConfigurationTab");
                    if ($scope.locationSetup.chargeList == undefined || $scope.locationSetup.chargeList == null || $scope.locationSetup.chargeList.length == 0) {
                        $scope.locationSetup.chargeList = [];
                        pushObj();
                    }
                    if ($stateParams != undefined && $stateParams != null && $stateParams.id != null) {
                        $http({
                            url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
                            method: "GET",
                            headers: {
                                "SAAS_ID": $rootScope.saasId
                            }

                        }).success(function(data) {
                            if (data.responseCode == "ERR0") {
                                $scope.locationSetup = data.responseObject;
                                console.log("locationSetup ", $scope.locationSetup);

                                if ($scope.locationSetup.chargeList == undefined || $scope.locationSetup.chargeList == null || $scope.locationSetup.chargeList.length == 0) {
                                    $scope.locationSetup.chargeList = [];
                                    pushObj();
                                }


                                if ($scope.locationSetup != null) {
                                    $rootScope.currentLocationSetupId = $scope.locationSetup.id;
                                }

                                $rootScope.navigateToNextField('chargeName');
                            }
                        })
                    }
                }




                $scope.chargeMaster = {};
                $scope.check = true;
                $scope.index = -1;

                //$scope.locationSetup.chargeList.push({});
                $scope.chargeMasterUploadText = true;
                $scope.chargeTypeArr = ['Origin', 'Destination', 'Freight', 'Other'];
                $scope.calcypeArr = ['Percentage', 'Shipment', 'Unit', 'Document'];

                $scope.addCharge = function() {
                    if ($scope.validateChargeList(0, false)) {
                        pushObj();
                        $rootScope.navigateToNextField($scope.locationSetup.chargeList.length - 1 + 'chargeName');
                    } else {
                        console.log("validation error");
                    }
                }

                function pushObj() {

                    $scope.chargeMaster = {};
                    $scope.chargeMaster.status = 'Active';
                    $scope.locationSetup.chargeList.push($scope.chargeMaster);

                }

                $scope.deleteCharge = function(ind) {
                    $scope.locationSetup.chargeList.splice(ind, 1);
                }

                $scope.updateCharge = function() {
                    if ($scope.validateChargeMaster(0, $scope.index)) {
                        $scope.locationSetup.chargeList[$scope.index].chargeName = $scope.chargeMaster.chargeName;
                        $scope.locationSetup.chargeList[$scope.index].chargeCode = $scope.chargeMaster.chargeCode;
                        $scope.locationSetup.chargeList[$scope.index].chargeType = $scope.chargeMaster.chargeType;
                        $scope.locationSetup.chargeList[$scope.index].calculationType = $scope.chargeMaster.calculationType;
                        $scope.chargeMaster = {};
                        $scope.check = true;
                        $scope.index = -1;
                        $rootScope.navigateToNextField('chargeName');
                    }
                }

                $scope.editChargeMaster = function(index) {
                    $scope.index = index;
                    $scope.chargeMaster.chargeName = $scope.locationSetup.chargeList[index].chargeName;
                    $scope.chargeMaster.chargeCode = $scope.locationSetup.chargeList[index].chargeCode;
                    $scope.chargeMaster.chargeType = $scope.locationSetup.chargeList[index].chargeType;
                    $scope.chargeMaster.calculationType = $scope.locationSetup.chargeList[index].calculationType;
                    $scope.check = false;
                    $rootScope.navigateToNextField('chargeName');
                }


                $scope.validateChargeList = function(validateCode, isFinalSave) {
                    console.log("Validate Called ", validateCode);
                    $scope.secondArr = [];
                    var fineArr = [];
                    angular.forEach($scope.locationSetup.chargeList, function(item, index) {

                        if ($scope.isEmptyRow(item)) {
                            //empty
                            if (index == 0) {
                                if (isFinalSave) {
                                    fineArr.push(1);
                                    $scope.locationSetup.chargeList = null;
                                    return true;
                                } else {
                                    $scope.secondArr[index] = {};
                                    $scope.secondArr[index].errTextArr = [];
                                    if (validateCharge(0, index)) {
                                        fineArr.push(1);
                                        $scope.secondArr[index].errRow = false;
                                    } else {
                                        fineArr.push(0);
                                        $scope.secondArr[index].errRow = true;
                                    }
                                }
                            } else {
                                if (isFinalSave) {
                                    $scope.locationSetup.chargeList.splice(index, 1);
                                } else {
                                    $scope.secondArr[index] = {};
                                    $scope.secondArr[index].errTextArr = [];
                                    if (validateCharge(0, index)) {
                                        fineArr.push(1);
                                        $scope.secondArr[index].errRow = false;
                                    } else {
                                        fineArr.push(0);
                                        $scope.secondArr[index].errRow = true;
                                    }
                                }
                            }
                        } else {
                            $scope.secondArr[index] = {};
                            $scope.secondArr[index].errTextArr = [];
                            if (validateCharge(0, index)) {
                                fineArr.push(1);
                                $scope.secondArr[index].errRow = false;
                            } else {
                                fineArr.push(0);
                                $scope.secondArr[index].errRow = true;
                            }
                        }
                    });

                    if (fineArr.indexOf(0) < 0) {
                        console.debug("Is fine Arr---->",true)
                        return true;
                    } else {
                        console.debug("Is fine Arr---->",false)
                        return false;
                    }

                }

                $scope.isEmptyRow = function(obj) {
                    //return (Object.getOwnPropertyNames(obj).length === 0);
                    var isempty = true; //  empty
                    if (!obj) {
                        return isempty;
                    }
                    var k = Object.getOwnPropertyNames(obj);
                    for (var i = 0; i < k.length; i++) {
                        if (k[i] === "status") {
                            continue;
                        }
                        if (obj[k[i]]) {
                            isempty = false; // not empty
                            break;
                        }
                    }
                    return isempty;
                }


                var myOtherModal = $modal({
                    scope: $scope,
                    templateUrl: 'views/others_tab/errorPopUp.html',
                    show: false,
                    keyboard: true
                });

                var errorOnRowIndex = null;
                $scope.errorShow = function(errorObj, index) {
                    $scope.errList = errorObj.errTextArr;
                    errorOnRowIndex = index;
                    myOtherModal.$promise.then(myOtherModal.show);
                };

                function validateCharge(validateCode, index) {

                    $scope.errorMap = new Map();

                    if (validateCode == 0 || validateCode == 1) {
                        if ($scope.locationSetup.chargeList[index].chargeName == undefined || $scope.locationSetup.chargeList[index].chargeName == null || $scope.locationSetup.chargeList[index].chargeName == "") {
                            $scope.secondArr[index].errTextArr.push($rootScope.errorMessage["ERRC0121"]);
                            $scope.secondArr[index].chargeName = true;
                        } else {
                            if ($scope.locationSetup.chargeList != undefined && $scope.locationSetup.chargeList != null && $scope.locationSetup.chargeList.length > 0) {

                                for (var i = 0; i < $scope.locationSetup.chargeList.length; i++) {

                                    if ($scope.locationSetup.chargeList[i].chargeName == $scope.locationSetup.chargeList[index].chargeName && index != i) {
                                        $scope.secondArr[index].errTextArr.push("Charge name is duplicated");
                                        $scope.secondArr[index].chargeName = true;
                                    }

                                }
                            }


                        }
                    }

                    if (validateCode == 0 || validateCode == 2) {
                        if ($scope.locationSetup.chargeList[index].chargeCode == undefined ||
                            $scope.locationSetup.chargeList[index].chargeCode == null ||
                            $scope.locationSetup.chargeList[index].chargeCode == "") {
                            $scope.secondArr[index].errTextArr.push($rootScope.errorMessage["ERRC0122"]);
                            $scope.secondArr[index].chargeCode = true;
                        } else {
                            if ($scope.locationSetup.chargeList != undefined && $scope.locationSetup.chargeList != null && $scope.locationSetup.chargeList.length > 0) {

                                for (var i = 0; i < $scope.locationSetup.chargeList.length; i++) {

                                    if ($scope.locationSetup.chargeList[i].chargeCode == $scope.locationSetup.chargeList[index].chargeCode && index != i) {
                                        $scope.secondArr[index].errTextArr.push("Charge code is duplicated");
                                        $scope.secondArr[index].chargeCode = true;
                                    }

                                }
                            }
                        }
                    }

                    if (validateCode == 0 || validateCode == 3) {
                        if ($scope.locationSetup.chargeList[index].chargeType == undefined ||
                            $scope.locationSetup.chargeList[index].chargeType == null ||
                            $scope.locationSetup.chargeList[index].chargeType == "") {
                            $scope.secondArr[index].chargeType = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.errorMessage["ERRC0123"]);
                        }
                    }

                    if (validateCode == 0 || validateCode == 4) {
                        if ($scope.locationSetup.chargeList[index].calculationType == undefined ||
                            $scope.locationSetup.chargeList[index].calculationType == null ||
                            $scope.locationSetup.chargeList[index].calculationType == "") {
                            $scope.secondArr[index].calculationType = true;
                            $scope.secondArr[index].errTextArr.push($rootScope.errorMessage["ERRC0124"]);
                        }
                    }

                    if ($scope.secondArr[index].errTextArr != undefined && $scope.secondArr[index].errTextArr.length > 0) {
                        return false;
                    } else {
                        return true;
                    }

                }

                // Download Template
                $scope.downloadTemplate = function(type) {
                    var fileName;
                    if (type == 'charge') {
                        fileName = 'charge_template.xlsx';
                    }
                    $http({
                        url: $rootScope.urlPath + '/public/api/v1/download/chargetemplate/' + type,
                        method: "GET",
                        headers: {
                            "SAAS_ID": $rootScope.saasId
                        },
                        responseType: 'arraybuffer'
                    }).success(function(data, status, headers, config) {
                        var blob = new Blob([data], {});
                        saveAs(blob, fileName);
                    }).error(function(data, status, headers, config) {
                        console.error("Problem while downloading..... ", data);
                    });

                }

                $scope.chargeMasterUpload = function(errMessage, btn) {
                    if ($scope.validateUploadFile($scope.chargeMaster.file, errMessage)) {
                        if (btn == 'chargeMasterBtn') {
                            $scope.chargeMasterBtn = true;
                        }
                        $scope.chargeMasterUploadText = false;
                        $scope.chargeMasterBusy = true;
                        $scope.chargeMaster.fileName = $scope.chargeMaster.file.name;
                        var reader = new FileReader();

                        reader.onload = function(event) {
                            var contents = event.target.result;
                            var uploadedFile = btoa(contents);
                            $scope.file = uploadedFile;
                        };
                        reader.readAsBinaryString($scope.chargeMaster.file);
                        $timeout(function() {
                            $scope.fileUploadDto = {};
                            $scope.fileUploadDto.userId = 3;
                            $scope.fileUploadDto.fileName = $scope.chargeMaster.file.name;
                            $scope.fileUploadDto.fileType = $scope.chargeMaster.file.type;
                            $scope.fileUploadDto.file = $scope.file;
                            return ChargeMasterBulkUpload.upload($scope.fileUploadDto).$promise.then(
                                function(data, status) {
                                    if (data.responseCode == "ERR0") {
                                        $scope.chargeMasterBusy = false;
                                        $scope.enableUploadFn(btn);
                                        if ($scope.locationSetup.chargeList.length == 0) {
                                            $scope.locationSetup.chargeList = data.responseObject;
                                        } else {
                                            for (var i = 0; i < $scope.locationSetup.chargeList.length; i++) {
                                                if ($scope.locationSetup.chargeList[i].chargeCode == null || $scope.locationSetup.chargeList[i].chargeCode == undefined) {
                                                    $scope.locationSetup.chargeList.splice(i, 1);
                                                }
                                            }
                                            for (var i = 0; i < data.responseObject.length; i++) {
                                                $scope.locationSetup.chargeList.push(data.responseObject[i]);
                                            }
                                            $scope.locationSetup.chargeList.push({});
                                        }
                                        Notification.success($rootScope.errorMessage["ERR05420"]);
                                    } else {
                                        $scope.chargeMasterBusy = false;
                                        $scope.enableUploadFn(btn);

                                        var errMsg = $rootScope.errorMessage[data.responseCode];
                                        var params = data.params;
                                        if (errMsg != "" && params != undefined && params != null && params.length > 0) {
                                            for (var i = 0; i < params.length; i++) {
                                                if (errMsg != undefined && errMsg != null) {
                                                    errMsg = errMsg.replace("%s", params[i]);
                                                }
                                            }
                                        }
                                        Notification.error(errMsg);
                                        //Notification.error($rootScope.errorMessage[data.responseCode]);
                                    }
                                },
                                function(errResponse) {
                                    $scope.chargeMasterBusy = false;
                                    $scope.enableUploadFn(btn);
                                    Notification.error(errResponse);
                                    console.log('Error while uploading  Chargemaster Upload', errResponse);
                                });
                        }, 5000);
                    } else {
                        console.log('invalid file format');
                    }
                }


                $scope.validateUploadFile = function(file, errMessage) {
                    $scope.errorMap = new Map();
                    if (file == null || file == undefined) {
                        $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0126"]);
                        return false;
                    }
                    if (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                        file.name.includes('.xlsx') || file.name.includes('.csv')) {
                        var allowedSize = 5;
                        if (allowedSize != undefined) {
                            allowedSize = allowedSize * 1024 * 1024;
                            if (file.size <= 0) {
                                $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0127"]);
                                return false;
                            } else if (file.size > allowedSize) {
                                $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0128"]);
                                return false;
                            }
                        }
                    } else {
                        $scope.errorMap.put(errMessage, $rootScope.errorMessage["ERRC0129"]);
                        return false;
                    }
                    return true;
                }

                $scope.chooseFile = function(obj, err, txt, btn) {
                    if ($scope.validateUploadFile(obj.file, err)) {
                        if (txt == 'chargeMasterUploadText') {
                            $scope.chargeMaster.fileName = obj.file.name;
                            $scope.chargeMasterUploadText = false;
                            $scope.chargeMasterBtn = true;
                            $scope.chargeMasterBusy = true;
                        }
                        $timeout(function() {
                            if (txt == 'chargeMasterUploadText') {
                                $scope.chargeMasterBusy = false;
                            }
                            $scope.enableUploadFn(btn);
                        }, 2000);

                    } else {
                        $scope.enableUploadFn(btn);
                        console.log("Not valid file");
                    }

                };
                $scope.validateChargeSetup = function() {
                    return true;
                }
                $scope.back = function() {
                    $state.go("app.finance", {
                        id: $rootScope.currentLocationSetupId
                    });
                }
                $rootScope.saveChargeSetup = function(stateName) {

                    if ($scope.validateChargeList(0, true)) {

                        console.log("validateChargeSetup ", $scope.locationSetup);

                        locationSetupObject = angular.copy($scope.locationSetup);

                        locationSetupObject.currentSection = "Charge";

                        UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function(data) {
                            if (data.responseCode == 'ERR0') {
                                console.log("Location Master saved :: ", data);
                                $state.go(stateName, {
                                    id: data.responseObject.id
                                });
                            } else {
                                Notification.error($rootScope.errorMessage[data.responseCode]);
                                console.log("Create Failed :" + data.responseDescription)
                            }
                        }, function(error) {
                            console.log("Create Failed : " + error)
                        });
                    } else {
                        console.log("Configuration Tab data is not valid");
                    }
                }

                $scope.enableUploadFn = function(btn) {
                    $timeout(function() {
                        if (btn == 'chargeMasterBtn') {
                            $scope.chargeMasterBtn = false;
                        }
                    }, 200);
                }

                $scope.multipleCharge = false;
                $scope.initChargeTab();
                $rootScope.currentSection = 'Charge';
                $rootScope.navigateToNextField('chargeName');

            }]);