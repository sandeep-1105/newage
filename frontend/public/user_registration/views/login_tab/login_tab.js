angular.module("NewAgeRegistration").controller("LoginTabController",['$rootScope', '$scope', '$timeout', '$state', '$stateParams', '$http', 'Notification', 'UserRegistrationDataService', 'UserRegistrationFactory',
    function($rootScope, $scope, $timeout, $state, $stateParams, $http, Notification, UserRegistrationDataService, UserRegistrationFactory) {

        $scope.shopArr = [{
            id: 1,
            name: 'abc'
        }];
        console.log("Login Tab Controller");

        $scope.commonServiceList = [];
        $scope.getAllService = function() {
            $http({
                method: "GET",
                headers: {
                    'SAAS_ID': $rootScope.saasId
                },
                url: $rootScope.urlPath + '/public/api/v1/common/servicemaster/get/all'
            }).then(function(response) {
                if (response.data != null && response.data.responseObject) {
                    $scope.commonServiceList = response.data.responseObject;
                }
            }, function(error) {
                console.log("Error", error);
            });
        }
        $scope.getAllService();

        $scope.initLoginTab = function() {
            console.log("inside initLoginTab")
            
            if ($stateParams != undefined && $stateParams != null && $stateParams.id != null) {

                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
                    method: "GET",
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }

                }).success(function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationSetup = data.responseObject;
                        console.log("locationSetup ", $scope.locationSetup);
                        if ($scope.locationSetup != null) {
                            $rootScope.currentLocationSetupId = $scope.locationSetup.id;
                            $scope.locationSetup.password = "";
                            $scope.locationSetup.confirmPassword = "";

                            if ($scope.locationSetup.userName == null || $scope.locationSetup.userName == "") {
                                $scope.locationSetup.userName = $scope.locationSetup.firstName;
                            }



                            if ($scope.locationSetup.serviceList == undefined || $scope.locationSetup.serviceList == null) {
                                $scope.locationSetup.serviceList = [];
                            } else {
                                $scope.tempServiceList = [];
                                for (var i = 0; i < $scope.commonServiceList.length; i++) {
                                    for (var j = 0; j < $scope.locationSetup.serviceList.length; j++) {
                                        if ($scope.commonServiceList[i].id == $scope.locationSetup.serviceList[j].serviceMaster.id) {
                                            $scope.commonServiceList[i].checked = true;
                                            $scope.tempServiceList.push($scope.locationSetup.serviceList[j].serviceMaster);
                                            console.log($scope.commonServiceList[i].serviceName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }

        $scope.list = ["Air Import", "Air Export"];
        $scope.getServiceList = function(keyword) {

            if ($scope.locationSetup != null && $scope.locationSetup.serviceList != null && $scope.locationSetup.serviceList.length > 0) {
                var idList = [];
                for (var i = 0; i < $scope.locationSetup.serviceList.length; i++) {
                    idList.push($scope.locationSetup.serviceList[i].serviceMaster.id);
                }
            }

            return UserRegistrationDataService.getSearchServiceNotInList(keyword, idList);
        }

        $scope.tempServiceList = [];
        $scope.selectedService = function() {
            $scope.loginErrorMap = new Map();
            serviceMasterTemp = angular.copy($scope.serviceMaster);
            console.log("Selected service ", $scope.serviceMaster);
            if ($scope.serviceMaster != null && $scope.serviceMaster.status != 'Block' && $scope.serviceMaster.status != 'Hide') {
                $scope.serviceMaster = null;
                var locationSetupServiceObj = {};
                locationSetupServiceObj.serviceMaster = serviceMasterTemp;
                $scope.locationSetup.serviceList.push(locationSetupServiceObj);
            } else {
                $scope.loginErrorMap.put("locationSetup.serviceList", "This service cannot be selected");
            }

            $scope.navigateToNextField("locationSetup.serviceList");
        }

        $scope.removeService = function(index) {
            $scope.locationSetup.serviceList.splice(index, 1);
            $scope.navigateToNextField("locationSetup.serviceList");
        }

        $scope.back = function() {
            $state.go("app.company", {
                id: $rootScope.currentLocationSetupId
            });
        }

        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $scope.showHint = function() {
            Notification.info($rootScope.errorMessage["ERRC0144"]);
        }

        $scope.validateLoginTab = function(validateCode) {

            if ($scope.locationSetup != undefined && $scope.locationSetup != null) {
                $scope.loginErrorMap = new Map();
                
                if (validateCode == 0 || validateCode == 1) {
                    if ($scope.locationSetup.position == null || $scope.locationSetup.position.trim().length == 0) {
                        $scope.loginErrorMap.put("locationSetup.position", $rootScope.errorMessage["ERRC0113"]);
                        $scope.navigateToNextField("locationSetup.position");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 2) {
                    if ($scope.locationSetup.userName == null || $scope.locationSetup.userName.trim().length == 0) {
                        $scope.loginErrorMap.put("locationSetup.userName", $rootScope.errorMessage["ERRC0114"]);
                        $scope.navigateToNextField("locationSetup.userName");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 3) {
                    if ($scope.locationSetup.password == null || $scope.locationSetup.password.trim().length == 0) {
                        $scope.loginErrorMap.put("locationSetup.password", $rootScope.errorMessage["ERRC0115"]);
                        $scope.navigateToNextField("locationSetup.password");
                        return false;
                    } else {
                        var regexp = new RegExp("^[a-zA-Z0-9](?=.*[0-9])(?=.*[@!#$%&()+={};<>,.?/\|])[a-zA-Z0-9!#@$%&()+={};<>,.?/\|]{5,20}$");
                        if (!regexp.test($scope.locationSetup.password)) {
                            $rootScope.navigateToNextField('locationSetup.password');
                            //$rootScope.errorMessage["ERRC0145"]
                            $scope.loginErrorMap.put("locationSetup.password", $rootScope.errorMessage["ERRC0144"]);
                            return false;
                        }
                    }
                }

                if (validateCode == 0 || validateCode == 4) {
                    if ($scope.locationSetup.confirmPassword == null || $scope.locationSetup.confirmPassword.trim().length == 0) {
                        $scope.loginErrorMap.put("locationSetup.confirmPassword", $rootScope.errorMessage["ERRC0116"]);
                        $scope.navigateToNextField("locationSetup.confirmPassword");
                        return false;
                    } else {

                        if ($scope.locationSetup.confirmPassword != $scope.locationSetup.password) {
                            $scope.loginErrorMap.put("locationSetup.confirmPassword", $rootScope.errorMessage["ERRC0117"]);
                            $scope.navigateToNextField("locationSetup.confirmPassword");
                            return false;
                        }

                    }
                }



                if (validateCode == 0 || validateCode == 5) {

                    if ($scope.tempServiceList != undefined &&
                        $scope.tempServiceList != null &&
                        $scope.tempServiceList.length == 0) {
                        $scope.loginErrorMap.put("locationSetup.serviceList", $rootScope.errorMessage["ERRC0118"]);
                        $scope.navigateToNextField("locationSetup.serviceList");
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        $scope.checkUniqueObject = function(obj, list) {
            var found, y;
            found = false;
            for (y = 0; y < list.length; y++) {
                if (obj.serviceMaster.id == list[y].serviceMaster.id) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                list.push(obj);
            }
            return found;
        }

        $scope.removeDeselectObject = function(obj, tempServiceList, serviceList) {
            var found, y;
            found = false;
            for (y = 0; y < tempServiceList.length; y++) {
                if (obj.serviceMaster.id == tempServiceList[y].id) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                var index = serviceList.indexOf(obj);
                serviceList.splice(index, 1);
            }
            return found;

        }

        $rootScope.saveLoginTab = function(stateName) {
            console.log("Login Tab data is valid ", $scope.locationSetup);
            var locationSetupObject = angular.copy($scope.locationSetup);
            
            //for clicking back seamlessly i.e no data check
            if(stateName === 'app.company'){
                $scope.loginErrorMap = new Map();
                if ($scope.locationSetup.password == null || $scope.locationSetup.password.trim().length == 0) {
                    $scope.loginErrorMap.put("locationSetup.password", $rootScope.errorMessage["ERRC0115"]);
                    $scope.navigateToNextField("locationSetup.password");
                    return false;
                } else {
                    var regexp = new RegExp("^[a-zA-Z0-9](?=.*[0-9])(?=.*[@!#$%&()+={};<>,.?/\|])[a-zA-Z0-9!#@$%&()+={};<>,.?/\|]{5,20}$");
                    if (!regexp.test($scope.locationSetup.password)) {
                        $rootScope.navigateToNextField('locationSetup.password');
                        //$rootScope.errorMessage["ERRC0145"]
                        $scope.loginErrorMap.put("locationSetup.password", $rootScope.errorMessage["ERRC0144"]);
                        return false;
                    }
                }


                if ($scope.locationSetup.confirmPassword == null || $scope.locationSetup.confirmPassword.trim().length == 0) {
                    $scope.loginErrorMap.put("locationSetup.confirmPassword", $rootScope.errorMessage["ERRC0116"]);
                    $scope.navigateToNextField("locationSetup.confirmPassword");
                    return false;
                } else {

                    if ($scope.locationSetup.confirmPassword != $scope.locationSetup.password) {
                        $scope.loginErrorMap.put("locationSetup.confirmPassword", $rootScope.errorMessage["ERRC0117"]);
                        $scope.navigateToNextField("locationSetup.confirmPassword");
                        return false;
                    }

                }

                saveloginFn(locationSetupObject,stateName);
            } else if(stateName === 'app.configuration'){

                if ($scope.validateLoginTab(0)) {
                    
                    for (var i = 0; i < $scope.tempServiceList.length; i++) {
                        var locationSetupServiceObj = {};
                        locationSetupServiceObj.serviceMaster = $scope.tempServiceList[i];
                        $scope.checkUniqueObject(locationSetupServiceObj, locationSetupObject.serviceList);
                    };
                    var cloneServiceList = angular.copy(locationSetupObject.serviceList);
                    
                    for (var i = 0; i < cloneServiceList.length; i++) {
                        $scope.removeDeselectObject(cloneServiceList[i], $scope.tempServiceList, locationSetupObject.serviceList);
                    };
                    console.log(locationSetupObject.serviceList);

                    if (locationSetupObject.encodedLogo != null) {
                        locationSetupObject.logo = locationSetupObject.encodedLogo;
                    }
                    locationSetupObject.currentSection = "Login";
                    saveloginFn(locationSetupObject,stateName);

                } else {
                    console.log("Login Tab data is not valid");
                }
            }    
        }

        var saveloginFn = function(locationSetupObject,stateName){
            UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Location Master saved :: ", data);
                    $state.go(stateName, {
                        id: data.responseObject.id
                    });
                } else {
                    console.log("Create Failed :" + data.responseDescription)
                }

            }, function(error) {
                console.log("Create Failed : " + error)
            });
        };



        $rootScope.currentSection = 'Login';
        $rootScope.navigateToNextField("locationSetup.position");
        $scope.initLoginTab();

    }]);