angular.module("NewAgeRegistration").controller("FinanceTabController", ['$rootScope', '$scope', '$timeout', '$state', 'Notification', 
				'UserRegistrationDataService', 'UserRegistrationFactory', '$stateParams', '$http',
		function($rootScope, $scope, $timeout, $state, Notification, 
				UserRegistrationDataService, UserRegistrationFactory, $stateParams, $http) {
	
	
	
	$scope.initFinanceTab = function() {
		console.log("Init Company Tab");
		
		if($stateParams != undefined && $stateParams != null && $stateParams.id != null) {
			$http({ url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
				    method: "GET",
				    headers : { "SAAS_ID" : $rootScope.saasId }
			}).success(function(data){
				if (data.responseCode == "ERR0") {
					$scope.locationSetup = data.responseObject;
					console.log("locationSetup ",$scope.locationSetup);
					if($scope.locationSetup != null) {
						$rootScope.currentLocationSetupId = $scope.locationSetup.id;
					}
				}
			});
		}
	}	
	
	$scope.validateFinanceTab = function(validateCode) {
		return true;
	}
	
	$scope.back=function(){
		$state.go("app.metaconfig", {id : $rootScope.currentLocationSetupId});
	}
	
	$rootScope.saveFinanceTab = function(stateName) {
		if($scope.validateFinanceTab(0)) {
			console.log("Finance Tab ", $scope.locationSetup);
			locationSetupObject = angular.copy($scope.locationSetup);

			locationSetupObject.currentSection = "Finance";
			
			UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function(data) {
				if (data.responseCode == 'ERR0') {
		            	$state.go(stateName, {id : $rootScope.currentLocationSetupId});
		            } else {
		            	Notification.error($rootScope.errorMessage[data.responseCode]);
		            	console.log("Create Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	console.log("Create Failed : " + error)
		        });
		} else {
			console.log("Finance Tab data is not valid");
		}
	}
	
	$rootScope.currentSection = 'Finance';
	$rootScope.navigateToNextField("quote");
	$scope.initFinanceTab();
}]);