angular.module("NewAgeRegistration").controller("CompanyTabController", ['$rootScope', '$scope',
    '$timeout', '$state', 'Notification', 'UserRegistrationDataService', 'UserRegistrationFactory', '$stateParams',
    '$http', '$modal', 'ValidationService',
    function ($rootScope, $scope, $timeout, $state, Notification, UserRegistrationDataService,
        UserRegistrationFactory, $stateParams, $http, $modal, ValidationService) {


        $scope.SalutationType = ["Mr", "Ms", "Mrs", "Dr", "Prof", "Miss"];

        $scope.initCompanyTab = function () {
            console.log("Init Company Tab");

            if ($stateParams != undefined && $stateParams != null && $stateParams.id != null) {

                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/get/" + $stateParams.id,
                    method: "GET",
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }

                }).success(function (data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationSetup = data.responseObject;
                        console.log("locationSetup ", $scope.locationSetup);
                        if ($scope.locationSetup != null) {
                            if ($scope.locationSetup.encodedLogo != null) {
                                $scope.companyUploadText = false;
                                $scope.companyUploadIsBusy = false;
                            }
                            if ($scope.locationSetup.isHeadOffice == null) {
                                $scope.locationSetup.isHeadOffice = false;
                            } else {
                                $scope.locationSetup.isHeadOffice == 'TRUE' ? true : false;
                            }
                            $rootScope.currentLocationSetupId = $scope.locationSetup.id;
                        }
                    }
                })
            }



            $scope.companyUploadText = true;
            $scope.companyUploadIsBusy = false;
        }
        $scope.changedSalutation = function (id) {
            $scope.companyErrorMap = new Map();
            $scope.navigateToNextField(id);
        }
        $scope.getCountryList = function (keyword) {
            if(!keyword)return false;
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/countrymaster/get/search/keyword").then(function (response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function (error) {
                console.log("Error", error);
            })
        }



        $scope.$watch('locationSetup.countryMaster', function (newVal, oldVal) {

            if (oldVal != undefined && oldVal != null && newVal != undefined && newVal != null) {
                if (oldVal.id != newVal.id) {
                    console.log("changed the country");
                    $scope.locationSetup.regionMaster = null;
                    $scope.locationSetup.stateMaster = null;
                    $scope.locationSetup.cityMaster = null;
                    $scope.locationSetup.dialCode = null;
                }
            } else if (newVal == null) {
                $scope.locationSetup.regionMaster = null;
                $scope.locationSetup.stateMaster = null;
                $scope.locationSetup.cityMaster = null;
                $scope.locationSetup.dialCode = null;
            }

        });

        $scope.getCityList = function (keyword) {
            if(!keyword)return false;
            $scope.companyErrorMap = new Map();
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/citymaster/get/search/keyword/-1/-1").then(function (response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function (error) {
                console.log("Error", error);
            });
            /*}*/
        }

        $scope.getStateList = function (keyword) {
            if(!keyword)return false;
            $scope.companyErrorMap = new Map();
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/statemaster/get/search/keyword/-1").then(function (response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function (error) {
                console.log("Error", error);
            });
            /*}*/
        }

        $scope.getRegionList = function (keyword) {
            if(!keyword)return false;
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/regionmaster/get/search/keyword").then(function (response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function (error) {
                console.log("Error", error);
            });
        }



        $scope.getTimeZoneMasterList = function (keyword) {
            if(!keyword)return false;
            return UserRegistrationDataService.getList(keyword, $rootScope.saasId, $rootScope.urlPath + "/public/api/v1/common/timezone/get/search/keyword").then(function (response) {
                if (response.data != null && response.data.responseObject) {
                    return response.data.responseObject.searchResult
                }
            }, function (error) {
                console.log("Error", error);
            });
        }


        $scope.selectedTimeZoneMaster = function (id) {
            console.log("Selected selectedTimeZoneMaster ", id);
            $scope.navigateToNextField(id);
        };



        $scope.selectedCountry = function (id) {
            console.log("Selected Country ", id);

            if ($scope.locationSetup.countryMaster != undefined && $scope.locationSetup.countryMaster != null) {
                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/phone_number_code/" + $scope.locationSetup.countryMaster.countryCode,
                    method: "GET",
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }
                }).success(function (data) {
                    if (data != undefined && data != null && data.responseCode == 'ERR0') {
                        $scope.locationSetup.dialCode = "+" + data.responseObject;
                    }
                })

            }

            $scope.navigateToNextField(id);
        }


        $scope.selectDataFromCountry = function () {
            if ($scope.locationSetup.countryMaster != undefined && $scope.locationSetup.countryMaster != null && $scope.locationSetup.countryMaster.regionMaster != null && $scope.locationSetup.countryMaster.regionMaster != undefined) {
                $scope.locationSetup.regionMaster = $scope.locationSetup.countryMaster.regionMaster;
            }
        }

        $scope.selectedState = function (id) {
            console.log("Selected State ", id);
            $scope.navigateToNextField(id);
        }

        $scope.selectedCity = function (obj, id) {
            console.log("Selected City ", id);
            if (obj != undefined || obj != null || obj.cityMaster != undefined || obj.cityMaster != null || obj.cityMaster != "") {
                $scope.locationSetup.stateMaster = obj.cityMaster.stateMaster;
            }
            $scope.navigateToNextField(id);
        }

        $scope.selectedRegion = function (id) {
            console.log("Selected Region ", id);
            $scope.navigateToNextField(id);
        };

        $scope.checkCaptcha = function (response) {
            $scope.captchaResponse = response;

        }


        $rootScope.validateCompanyTab = function (validateCode) {

            if ($scope.invalidPhoneNumber) {
                $scope.isPhoneNumberValid();
                return false;
            }

            $scope.companyErrorMap = new Map();



            if ($scope.locationSetup != undefined && $scope.locationSetup != null) {

                if (validateCode == 0) {
                    if ($scope.companyNameUnique != undefined && $scope.companyNameUnique == false) {
                        $scope.companyNameUniqueErrorMessage = $rootScope.errorMessage["ERRC0010"];
                        return false;
                    }
                }

                if (validateCode == 0 || validateCode == 1) {
                    if ($scope.locationSetup.salutation == null || $scope.locationSetup.salutation.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.salutation", $rootScope.errorMessage["ERRC0101"]);
                        $scope.navigateToNextField("locationSetup.salutation");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 2) {
                    if ($scope.locationSetup.firstName == null || $scope.locationSetup.firstName.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.firstName", $rootScope.errorMessage["ERRC0102"]);
                        $scope.navigateToNextField("locationSetup.firstName");
                        return false;
                    } else {
                        if (!ValidationService.checkRegExp('first.name', $scope.locationSetup.firstName)) {
                            $scope.companyErrorMap.put("locationSetup.firstName", $rootScope.errorMessage["ERRC0208"]);
                            $scope.navigateToNextField("locationSetup.firstName");
                            return false;
                        }
                    }
                }


                if (validateCode == 0 || validateCode == 3) {
                    if ($scope.locationSetup.lastName == null || $scope.locationSetup.lastName.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.lastName", $rootScope.errorMessage["ERRC0103"]);
                        $scope.navigateToNextField("locationSetup.lastName");
                        return false;
                    } else {
                        if (!ValidationService.checkRegExp('last.name', $scope.locationSetup.lastName)) {
                            $scope.companyErrorMap.put("locationSetup.lastName", $rootScope.errorMessage["ERRC0209"]);
                            $scope.navigateToNextField("locationSetup.lastName");
                            return false;
                        }
                    }
                }

                if (validateCode == 0 || validateCode == 4) {
                    if ($scope.locationSetup.email == null || $scope.locationSetup.email.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.email", $rootScope.errorMessage["ERRC0104"]);
                        $scope.navigateToNextField("email");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 5) {
                    if ($scope.locationSetup.countryMaster == null || $scope.locationSetup.countryMaster == "") {
                        $scope.companyErrorMap.put("locationSetup.countryMaster", $rootScope.errorMessage["ERRC0105"]);
                        $scope.navigateToNextField("locationSetup.countryMaster");
                        $scope.locationSetup.countryMaster = null;
                        return false;
                    }
                }

                if (validateCode == 0 || validateCode == 6) {
                    if ($scope.locationSetup.phone == null || $scope.locationSetup.phone.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.phone", $rootScope.errorMessage["ERRC0106"]);
                        $scope.navigateToNextField("locationSetup.phone");
                        return false;
                    } else {
                        if (!ValidationService.checkRegExp('phone.number', $scope.locationSetup.phone)) {
                            $scope.companyErrorMap.put("locationSetup.phone", $rootScope.errorMessage["ERRC0149"]);
                            $scope.navigateToNextField("locationSetup.phone");
                            return false;
                        }
                    }
                }

                if (validateCode == 0 || validateCode == 61) {
                    if ($scope.locationSetup.mobile != undefined && $scope.locationSetup.mobile != null && $scope.locationSetup.mobile.trim().length >= 0) {
                        if (!ValidationService.checkRegExp('mobile.number', $scope.locationSetup.mobile)) {
                            $scope.companyErrorMap.put("locationSetup.mobile", $rootScope.errorMessage["ERRC0150"]);
                            $scope.navigateToNextField("locationSetup.mobile");
                            return false;
                        }
                    }
                }


                if (validateCode == 0 || validateCode == 7) {
                    if ($scope.locationSetup.companyRegisteredName == null || $scope.locationSetup.companyRegisteredName.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.companyRegisteredName", $rootScope.errorMessage["ERRC0107"]);
                        $scope.navigateToNextField("locationSetup.companyRegisteredName");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 8) {
                    if ($scope.locationSetup.addressLine1 == null || $scope.locationSetup.addressLine1.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.addressLine1", $rootScope.errorMessage["ERRC0108"]);
                        $scope.navigateToNextField("locationSetup.addressLine1");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 9) {
                    /*if($scope.locationSetup.addressLine2 == null || $scope.locationSetup.addressLine2.trim().length == 0) {
                    	$scope.companyErrorMap.put("locationSetup.addressLine2", $rootScope.errorMessage["ERRC0109"]);
                    	$scope.navigateToNextField("locationSetup.addressLine2");
                    	return false;
                    }*/
                }

                if (validateCode == 0 || validateCode == 10) {
                    /*if($scope.locationSetup.addressLine3 == null || $scope.locationSetup.addressLine3.trim().length == 0) {
                    	$scope.companyErrorMap.put("locationSetup.addressLine3", $rootScope.errorMessage["ERRC01010"]);
                    	$scope.navigateToNextField("locationSetup.addressLine3");
                    	return false;
                    }*/
                }


                if (validateCode == 0 || validateCode == 11) {
                    if ($scope.locationSetup.area == undefined || $scope.locationSetup.area == null || $scope.locationSetup.area == "" || $scope.locationSetup.area.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.area", $rootScope.errorMessage["ERRC0146"]);
                        $scope.navigateToNextField("locationSetup.area");
                        return false;
                    } else {
                        if (!ValidationService.checkRegExp('area.name', $scope.locationSetup.area)) {
                            $scope.companyErrorMap.put("locationSetup.area", $rootScope.errorMessage["ERRC030"]);
                            $scope.navigateToNextField("locationSetup.area");
                            return false;
                        }
                    }
                }

                if (validateCode == 0 || validateCode == 14) {
                    if ($scope.locationSetup.cityMaster == undefined || $scope.locationSetup.cityMaster == null || $scope.locationSetup.cityMaster == "") {
                        $scope.companyErrorMap.put("locCity", $rootScope.errorMessage["ERRC0210"]);
                        $scope.navigateToNextField("locCity");
                        return false;
                    }
                }


                if (validateCode == 0 || validateCode == 12) {

                    if ($scope.locationSetup.timeZoneMaster == undefined || $scope.locationSetup.timeZoneMaster == null || $scope.locationSetup.timeZoneMaster.id == null) {
                        $scope.companyErrorMap.put("locationSetup.timeZone", $rootScope.errorMessage["ERRC0130"]);
                        $scope.navigateToNextField("locationSetup.timeZone");
                        return false;
                    }
                }

                if (validateCode == 0 || validateCode == 13) {

                    if ($scope.locationSetup.dateFormat == undefined || $scope.locationSetup.dateFormat == null || $scope.locationSetup.dateFormat.trim().length == 0) {
                        $scope.companyErrorMap.put("locationSetup.dateFormat", $rootScope.errorMessage["ERRC0131"]);
                        $scope.navigateToNextField("locationSetup.dateFormat");
                        return false;
                    }



                }




                if ($scope.locationSetup.countryMaster == "") {
                    $scope.locationSetup.countryMaster = null;
                }

                if ($scope.locationSetup.stateMaster == "") {
                    $scope.locationSetup.stateMaster = null;
                }

                if ($scope.locationSetup.cityMaster == "") {
                    $scope.locationSetup.cityMaster = null;
                }




                return true;
            }

            return false;
        }


        $rootScope.saveCompanyTab = function (stateName) {
            if ($rootScope.validateCompanyTab(0)) {

                /*if($scope.locationSetup.isTermsAccepted == undefined || !$scope.locationSetup.isTermsAccepted) {
                	alert($rootScope.errorMessage["ERRC0125"]);
                	return;
                }*/

                console.log("Company Tab ", $scope.locationSetup);

                locationSetupObject = angular.copy($scope.locationSetup);

                locationSetupObject.recaptchaResponse = $scope.captchaResponse;


                if (locationSetupObject.encodedLogo != null) {
                    locationSetupObject.logo = locationSetupObject.encodedLogo;
                }

                locationSetupObject.currentSection = "Company";

                UserRegistrationFactory.create.query(locationSetupObject).$promise.then(function (data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Location Master saved :: ", data);
                        $state.go(stateName, {
                            id: data.responseObject.id
                        });
                    } else {
                        Notification.error($rootScope.errorMessage[data.responseCode]);
                        console.log("Create Failed :" + data.responseDescription)
                    }
                }, function (error) {
                    console.log("Create Failed : " + error)
                });

            } else {
                console.log("Login Tab data is not valid");
            }
        }


        $scope.checkFormat = function (object, regExp) {
            var regexp = new RegExp(regExp);
            if (!regexp.test(object)) {
                return false;
            }
            return true;
        }

        var multipleEmailExpr = /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        $scope.checkMultipleMail = function (value) {
            var emailArr = value.split(";");
            if (emailArr != null && emailArr.length > 0) {
                for (var i = 0; i < emailArr.length; i++) {
                    if (!$scope.checkFormat(emailArr[i], multipleEmailExpr)) {
                        return false;
                    }
                }
            }
            return true;
        }

        $scope.validateEmail = function (object) {
            $scope.companyErrorMap = new Map();
            if (object.email != undefined && object.email != null && object.email != "") {
                if (!$scope.checkMultipleMail(object.email)) {
                    $scope.companyErrorMap.put("email", $rootScope.errorMessage["ERRC0112"]);
                    return false;
                }
            }
            return true;
        }



        $scope.companyLogoUpload = function (file) {
            console.log("companyLogoUpload ", file);
            if (file != null || file != undefined) {
                var fileName = file.name;
                var extention = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
                console.log("file extention ", extention);
                var valid = false;
                if (extention == 'jpeg' || extention == 'jpg' || extention == 'png' || extention == 'gif') {
                    valid = true;
                }

                var reader = new FileReader();

                reader.onload = function (event) {
                    var contents = event.target.result;
                    var uploadedFile = btoa(contents);
                    console.log("File contents : " + uploadedFile);
                    $scope.locationSetup.encodedLogo = uploadedFile;
                    $scope.companyUploadText = false;
                    $scope.companyUploadIsBusy = true;
                    $timeout(function () {
                        $scope.companyUploadIsBusy = false;
                    }, 1000);

                };
                reader.readAsBinaryString(file);
            }


        }


        var termsModal = $modal({
            scope: $scope,
            backdrop: 'static',
            templateUrl: '/public/user_registration/views/terms_conditions/terms_conditions.html',
            show: false
        });

        $scope.showTermsModal = function () {
            console.log("Showing Terms Modal")
            termsModal.$promise.then(termsModal.show);
        };

        $scope.cancelPopup = function () {
            termsModal.$promise.then(termsModal.hide);
        }


        /********************************************************************************/

        $scope.isCompanyNameUnique = function () {

            if ($scope.locationSetup.companyRegisteredName != undefined &&
                $scope.locationSetup.companyRegisteredName != "" &&
                $scope.locationSetup.companyRegisteredName.length != 0) {

                var localId = -1;
                if ($scope.locationSetup.id != undefined && $scope.locationSetup.id != null) {
                    localId = $scope.locationSetup.id;
                }
                $http({
                    url: $rootScope.urlPath + "/public/api/v1/locationsetup/is_company_unique/" + $scope.locationSetup.companyRegisteredName + "/" + localId,
                    method: "GET",
                    headers: {
                        "SAAS_ID": $rootScope.saasId
                    }
                }).success(function (data) {
                    if (data.responseCode == 'ERRC0010') {
                        $scope.companyNameUnique = false;
                        $scope.companyNameUniqueErrorMessage = $rootScope.errorMessage["ERRC0010"];
                    } else if (data.responseCode == 'ERRC0009') {
                        $scope.companyNameUnique = true;
                    } else {
                        console.log("Error while checking the company uniqueness");
                    }
                }).error(function (data, status, headers, config) {
                    if ((status === 409) && data.responseCode === 'ERRC0010') {
                        $scope.companyNameUnique = false;
                        $scope.companyNameUniqueErrorMessage = $rootScope.errorMessage["ERRC0010"];
                    }
                });
            }

        }

        /********************************************************************************/

        $scope.isPhoneNumberValid = function () {
            $scope.companyErrorMap = new Map();
            if ($scope.locationSetup.countryMaster != undefined && $scope.locationSetup.countryMaster != null && $scope.locationSetup.countryMaster.countryCode != "") {

                if ($scope.locationSetup.phone != undefined && $scope.locationSetup.phone != "" && $scope.locationSetup.phone.length != 0) {

                    $http({
                        url: $rootScope.urlPath + "/public/api/v1/locationsetup/is_phone_number_valid/" + $scope.locationSetup.countryMaster.countryCode + "/" + $scope.locationSetup.phone,
                        method: "GET",
                        headers: {
                            "SAAS_ID": $rootScope.saasId
                        }
                    }).success(function (data) {

                        if (data.responseCode == 'ERRC0008') {
                            $scope.invalidPhoneNumber = true;
                            $scope.invalidPhoneNumberMessage = $rootScope.errorMessage['ERRC0008'];
                        } else {
                            $scope.invalidPhoneNumber = false;
                        }
                    })

                }

            }
        }

        /********************************************************************************/
        $scope.getTimeZoneList = function (saasId) {
            $http({
                method: "get",
                headers: {
                    'SAAS_ID': saasId
                },
                url: $rootScope.urlPath + "/public/api/v1/timezones/get",
            }).then(function (response) {
                if (response.data != null && response.data.responseObject) {
                    $scope.timeZoneList = response.data.responseObject;
                }
            }, function (error) {
                console.log("Error", error);
            });
        }

        $scope.changedTimeZone = function (id) {
            console.log("changedTimeZone ", id);
            $rootScope.navigateToNextField(id);
        }


        $scope.getTimeZoneList($rootScope.saasId);
        /********************************************************************************/

        $scope.dateFormatArray = ["DD-MM-YYYY",
            "YYYY-MM-DD",
            "MM-DD-YYYY",
            "DD/MM/YYYY",
            "YYYY/MM/DD",
            "MM/DD/YYYY"
        ];

        $scope.changeDateFormat = function (id) {
            console.log("changeDateFormat ", id);
            $rootScope.navigateToNextField(id);
        }
        if ($scope.locationSetup == undefined || $scope.locationSetup == null) {
            $scope.locationSetup = {};
            $scope.locationSetup.quoteNeedApproval = false;
            $scope.locationSetup.isProvisionalCost = false;
            $scope.locationSetup.isHeadOffice = false;
            $scope.locationSetup.measurement = false;
        }
        $rootScope.navigateToNextField("locationSetup.salutation");
        $scope.companyUploadText = false;
        $scope.companyUploadIsBusy = false;
        $rootScope.currentSection = 'Company';
        $scope.initCompanyTab();
    }]);
