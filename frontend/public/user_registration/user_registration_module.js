var NewAgeRegistration = angular.module("NewAgeRegistration", 
		[ 'am.multiselect',
		  'ngStorage',
		  'oc.lazyLoad', 
		  'ui.router', 
		  'ui-notification', 
		  'ui.bootstrap',
		  'ngFileUpload',
		  'uiSwitch',
		  'ngResource',
		  'datetimepicker',
		  'ngDialog',
		  'mgcrea.ngStrap'
		]
);
