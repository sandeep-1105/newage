/**
 * Created by saravanan on 4/4/16.
 */
NewAgeRegistration.directive("panelHeight",['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, iElement, iAttrs) {
                //var docHt = $(window).height()-220;
                function setHeight() {
                    var docHt = $(window).height() - 182; //182
                    var docHtTab = $(window).height() - 280; //280
                    var docHtViewTab = $(window).height() - 305; //280
                    var winWt = $(window).width() <= 1366 ? 15 : 0;
                    iElement.css({
                        "min-height": docHt + "px",
                        "height": docHt + "px",
                        "overflow": "hidden"
                    });
                    if (iElement.hasClass('nav-tab-container')) {
                        iElement.css({
                            "min-height": docHt - 35 + winWt + "px",
                            "height": docHt - 35 + winWt + "px",
                            "overflow": "hidden"
                        });
                    } else if (iElement.hasClass('half-master-detail')) {
                        iElement.css({
                            "min-height": docHt + 50 + winWt + "px",
                            "height": docHt + 50 + winWt + "px",
                            "overflow": "hidden"
                        });
                    } else if (iElement.hasClass('half-master-detail-tab')) {
                        iElement.css({
                            "min-height": docHt + 5 + winWt + "px",
                            "height": docHt + 5 + winWt + "px",
                            "overflow": "hidden"
                        });
                    } else if (iElement.hasClass('half-master-detail-report-tab')) {
                        iElement.css({
                            "min-height": docHt + 50 + winWt + "px",
                            "height": docHt + 50 + winWt + "px",
                            "overflow": "hidden"
                        });
                    } else if (iElement.hasClass('half-master-edit')) {
                        iElement.css({
                            "min-height": docHtTab - 5 + winWt + "px",
                            "height": docHtTab - 5 + winWt + "px",
                            "overflow-y": "auto"
                        });
                    } else if (iElement.hasClass('half-master-edit-tab')) {
                        iElement.css({
                            "min-height": docHtTab - 55 + winWt + "px",
                            "height": docHtTab - 55 + winWt + "px",
                            "overflow-y": "auto"
                        });
                    }



                }

                function setFocusEvent() {
                    var winWtFocus = $(window).width();
                    if (winWtFocus <= 1300) {
                        console.log("set focus called");
                        iElement.find('input,textarea,select').on('focus', function() {
                            var topPos = $(this).offset().top;
                            console.log("topoffset", topPos);
                            console.log("$(window).scrollTop()", $(iElement).scrollTop());
                            $(iElement).animate({
                                scrollTop: topPos
                            }, 100);

                        });
                    }
                }

                $timeout(function() {
                    setHeight();
                    //setFocusEvent();
                }, 0);

                $(window).bind('resize', function() {
                    setHeight();
                });
            }
        }
    }])
    .directive('panelHeightFit', function() {
        return {
            restrict: 'A',
            link: function(scope, iElement, iAttrs) {
                var docHt = $(window).height() - 182;
                iElement.css({
                    "min-height": docHt + 40 + "px",
                    "height": docHt + 40 + "px",
                    "overflow": "hidden"
                });
                $(window).bind('resize', function() {
                    var docHt = $(window).height() - 182;
                    iElement.css({
                        "min-height": docHt + 40 + "px",
                        "height": docHt + 40 + "px",
                        "overflow": "hidden"
                    });
                });
            }
        }
    })
    .directive('panelHeightFitBody',['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: {
                activityShow: '='
            },
            link: function(scope, iElement, iAttrs) {
                scope.$watch('activityShow', function(newValue, oldValue) {
                    if (newValue && newValue != oldValue) {
                        //var panelHt = iElement.parents('.panel-body').height();
                        var panelHt = iElement.parents('.panel-body').height();
                        if (iElement.hasClass('enquiry-activity')) {
                            iElement.css({
                                "min-height": panelHt + "px",
                                "height": panelHt + "px",
                                "overflow": "hidden"
                            });
                        } else {
                            iElement.css({
                                "min-height": panelHt + 80 + "px",
                                "height": panelHt + 80 + "px",
                                "overflow": "hidden"
                            });
                        }
                    }
                });



                $(window).bind('resize', function() {
                    var panelHt = iElement.parents('.panel-body').height();
                    if (iElement.hasClass('enquiry-activity')) {
                        iElement.css({
                            "min-height": panelHt + "px",
                            "height": panelHt + "px",
                            "overflow": "hidden"
                        });
                    } else {
                        iElement.css({
                            "min-height": panelHt + 80 + "px",
                            "height": panelHt + 80 + "px",
                            "overflow": "hidden"
                        });
                    }
                });
            }
        }
    }])
    .directive('modalHeight', function() {
        return {
            restrict: 'A',
            link: function(scope, elem, attr) {
                var winHt = $(window).height();
                $(elem).css({
                    height: winHt - 140 + "px"
                });

                $(window).resize(function() {
                    var winHt = $(window).height();
                    $(elem).css({
                        height: winHt - 180 + "px"
                    });
                })
            }
        }
    })
    .directive('bDir', function() {
        return {
            restrict: 'E',
            scope: {
                myBadge: '=',
                ngModel: '='
            },
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var textArea = $(elem).find("textarea:first-child");
                var dummy = $(elem).find("textarea:last-child");

                function getHeight() {

                    dummy.val(textArea.val());
                    return dummy.prop("scrollHeight");

                }

                ctrl.$render = function() {
                    if (ctrl.$modelValue) {
                        console.log(ctrl.$modelValue);
                        $(elem).find('textarea').val(ctrl.$modelValue);
                    }

                }
                textArea.keyup(function(event) {

                    while (getHeight() > textArea.height()) {
                        textArea.css('font-size', '-=1');
                        dummy.css('font-size', '-=1');
                    }
                    if (event.keyCode == 8 || event.keyCode == 46) {
                        while (getHeight() <= textArea.height() && textArea.css('font-size') <= "12px") {
                            textArea.css('font-size', '+=1');
                            dummy.css('font-size', '+=1');
                        }
                        textArea.css('font-size', '-=1');
                        dummy.css('font-size', '-=1');
                    }
                    ctrl.$setViewValue(textArea.val());

                });
            },
            template: '<textarea  id="textarea"></textarea>' +
                '<textarea id="dummy"></textarea>'
        }

    })
    .directive('xsTableResponsive',['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, iElement, attr) {
                function setTableWidth() {
                    var winWt = $(window).width();
                    console.log(winWt);
                    if (winWt >= 768 && winWt <= 1024) {
                        iElement.css({
                            "width": "800px"
                        });
                    } else {
                        iElement.css({
                            "width": "100%"
                        });
                    }
                }
                $timeout(function() {
                    setTableWidth();
                }, 0);
                $(window).bind('resize', function() {
                    setTableWidth();
                });
            }
        }
    }])
    .directive('smTableResponsive',['$timeout', function($timeout) {
        return {
            restrict: 'A',
            scope: {
                watchData: "="
            },
            link: function(scope, iElement, attr) {
                function setTableWidth() {
                    var winWt = $(window).width();
                    if (winWt >= 768 && winWt <= 1024) {
                        iElement.addClass('responsive-stacked-table');
                        iElement.find('.label_table').remove();
                        iElement.find("tbody tr").each(function() {
                            $(this).find('td').each(function(index, value) {
                                var dyLabel = "<ul class=\"list-unstyled label_table\">";
                                if ($(this).data('group')) {
                                    if ($(this).data('grouphide') == true) {
                                        dyLabel += "<li class=\"text-left minH-20\"></li>";
                                    } else {
                                        dyLabel += "<li class=\"text-left minH-20\">" + $(this).data('group');
                                        dyLabel += " </li>";
                                    }
                                }
                                if ($(this).data('headtitle')) {
                                    dyLabel += "<li class=\"custom_label text-left\">" + $(this).data('headtitle');
                                    if ($(this).data('required') == true) {
                                        dyLabel += "<span class=\"reqColor text-left\">*</span>";
                                    }
                                    dyLabel += " :</li></ul> ";
                                }
                                if (dyLabel) {
                                    $(this).prepend(dyLabel);
                                }
                                if ($(this).data('nofloat') != true) {
                                    $(this).addClass('floattd');
                                }
                                if ($(this).data('action') == true) {
                                    $(this).addClass('td-action');
                                }
                                if ($(this).data('width')) {
                                    $(this).addClass($(this).data('width'));
                                }
                                if ($(this).data('headtitle') == "#") {
                                    $(this).removeClass('text-center');
                                }


                            });
                        });
                    } else {
                        iElement.removeClass('responsive-stacked-table');
                        iElement.find('.label_table').remove();
                        iElement.find("tbody tr").each(function() {
                            $(this).find('td').each(function(index, value) {
                                $(this).removeClass('floattd');
                                $(this).removeClass('td-action');
                                if ($(this).data('width')) {
                                    $(this).removeClass($(this).data('width'));
                                }
                                if ($(this).data('headtitle') == "#") {
                                    $(this).addClass('text-center');
                                }
                            });
                        });
                    }
                }
                //$timeout(function(){
                //    setTableWidth();
                //},5);
                scope.$watch('watchData', function(newValue, oldValue) {
                    $timeout(function() {
                        setTableWidth();
                    }, 5);
                }, true);
                $(window).bind('resize', function() {
                    setTableWidth();
                });
            }
        }
    }])
    .directive('mdTableResponsive',['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, iElement, attr) {
                function setTableWidth() {
                    var winWt = $(window).width();
                    console.log(winWt);
                    if (winWt >= 768 && winWt <= 1024) {
                        iElement.css({
                            "width": "1200px"
                        });
                    } else {
                        iElement.css({
                            "width": "100%"
                        });
                    }
                }
                $timeout(function() {
                    setTableWidth();
                }, 0);
                $(window).bind('resize', function() {
                    setTableWidth();
                });
            }
        }
    }])
    .directive('lgTableResponsive',['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, iElement, attr) {
                function setTableWidth() {
                    var winWt = $(window).width();
                    console.log(winWt);
                    if (winWt >= 768 && winWt <= 1024) {
                        iElement.css({
                            "width": "1400px"
                        });
                    } else {
                        iElement.css({
                            "width": "100%"
                        });
                    }
                }
                $timeout(function() {
                    setTableWidth();
                }, 0);
                $(window).bind('resize', function() {
                    setTableWidth();
                });
            }
        }
    }])
    .directive('setTabHeight',['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, elem, attr) {
                function setTabHeight() {
                    $(elem).equalize();
                }

                $timeout(function() {
                    setTabHeight();
                }, 0);


            }
        }
    }]);