NewAgeRegistration.factory('httpInterceptor',['$q', function($q) {
	
	return {
		request : function(config,TIME_OUT) {
			
			if(IS_DEV_ENV && config.url.indexOf('api')>0 ){
				if(!config.url.split("//")[1])
					config.url = "http://"+CONFIG_BASE_URL+config.url;
				else
					config.url = config.url.replace(config.url.split("//")[1].split("/")[0],CONFIG_BASE_URL);
				
			}
			return config || $q.when(config);
		},
		response : function(response) {
			
			return response || $q.when(response);
		},
		responseError : function(response) {
			
			
			return $q.reject(response);
		}
	};
}]);