	'use strict';
	NewAgeRegistration.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	    function($stateProvider, $urlRouterProvider, $httpProvider) {
	    	$httpProvider.interceptors.push('httpInterceptor');
	        $httpProvider.defaults.withCredentials = true;

	        $urlRouterProvider.otherwise('/');

	        $stateProvider.state('viewMail', {
	            templateUrl: '../../public/user_registration/view_mail.html',
	            url: '/view/mail'
	        })

	        .state('app', {
	            templateUrl: '../../public/user_registration/layout.html',
	            url: "/",
	            controller: "UserRegistrationCtrl",
	            
	        })

	        .state('app.trailregistration', {
	            templateUrl: '../../public/trailregistration/user_trail_registration.html',
	            url: "/",
	            controller: "UserTrailRegistrationCtrl"
	        })
	        .state("app.login", {
	            url: "login/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                },
	            },
	            templateUrl: "views/login_tab/login_tab.html",
	            controller: "LoginTabController",
	            
	        }).state("app.company", {
	            url: "company/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                },
	            },
	            templateUrl: "views/company_tab/company_tab.html",
	            controller: "CompanyTabController",
	           
	        }).state("app.configuration", {
	            url: "configuration/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                },
	            },
	            templateUrl: "views/configuration_tab/configuration_tab.html",
	            controller: "ConfigurationTabController",
	            
	        })
	        .state("app.metaconfig", {
	            url: "meta_configuration/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                },
	            },
	            templateUrl: "views/meta_configuration_tab/meta_configuration_tab.html",
	            controller: "MetaConfigTabController",
	           
	        })
	        .state("app.finance", {
	            url: "finance/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                },
	            },
	            templateUrl: "views/finance_tab/finance_tab.html",
	            controller: "FinanceTabController",
	            
	        })
	        .state("app.charge", {
	            url: "charge/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                }
	            },
	            templateUrl: "views/charge_tab/charge_tab.html",
	            controller: "ChargeTabController",
	           
	        })
	        .state("app.others", {
	            url: "others/:id",
	            params: {
	                id: {
	                    value: null,
	                    squash: true
	                },
	            },
	            templateUrl: "views/others_tab/others_tab.html",
	            controller: "OthersTabController",
	            
	        });
	    }
	]).run(function($rootScope, $state, $window){
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if(fromState.name === "app.company" && toState.name === "app"){
				$window.history.back();
			}

		});
	});