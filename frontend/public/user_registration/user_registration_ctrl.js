
NewAgeRegistration.controller("UserRegistrationCtrl", ['$rootScope', '$scope', '$state', '$timeout', '$stateParams', 'UserRegistrationDataService', 'UserRegistrationFactory', '$location', 'Notification', '$http',
    function($rootScope, $scope, $state, $timeout, $stateParams, UserRegistrationDataService, UserRegistrationFactory, $location, Notification, $http) {

        $rootScope.datePickerOptions = {
            format: "YYYY-MM",
            useCurrent: false,
        };

        $rootScope.dateTimePickerOptions = {
            format: "DD-MM-YYYY HH:mm",
            useCurrent: false,
            sideBySide: true,
            viewDate: moment()
        };

        $rootScope.dateWithOutTimePickerOptions = {
            format: "DD-MM-YYYY",
            useCurrent: false,
            sideBySide: true,
            viewDate: moment()
        };

        $rootScope.timePickerOptions = {
            format: "HH:mm",
            useCurrent: true
        };

        $rootScope.convertToDate = function(parsedate, dateFormat) {

            if (parsedate == undefined || parsedate == null)
                return null;

            if (typeof parsedate == "string") {
                return moment(parsedate, dateFormat);
            } else {
                return moment(parsedate, dateFormat);
            }
        }

        $rootScope.sendApiStartDateTime = function(parsedate, dateFormat) {
            if (typeof parsedate == "string" && parsedate.includes("Z")) {
                var str = parsedate.replace(".000Z", "");
                var res = parsedate.replace("T", " ");
                parsedate = moment(res, "YYYY-MM-DD HH:mm:ss");
            } else if (typeof parsedate == "string" && parsedate.endsWith(" 00:00:00")) {
                parsedate = parsedate.replace(" 00:00:00", "");
            }

            if (typeof parsedate == "string") {
                return moment(parsedate, dateFormat).format("YYYY-MM-DD") + " 00:00:00";
            } else {
                if (parsedate == null)
                    return null;
                if (typeof parsedate == "object") {
                    return moment(parsedate, dateFormat).format("YYYY-MM-DD") + " 00:00:00";
                } else {
                    var date = new Date(parsedate);
                    var day = date.getDate();
                    var monthIndex = date.getMonth();
                    var year = date.getFullYear();
                    return year + '-' + (monthIndex + 1) + '-' + day + ' 00:00:00';
                }
            }
        }

        Utils.showDebugLogs();

        var protocol = $location.protocol();
        var host = $location.host();
        var urlPath = protocol + "://" + host;

        var port = $location.port();
        if (port != undefined && port != null) {
            port = ":" + port
        } else {
            port = "";
        }

        urlPath = urlPath + port;
        $rootScope.urlPath = urlPath;
        // $rootScope.urlPath = BASE_URL;
        $rootScope.saasId = "COM";

        $rootScope.exportTransportMode = ["ETD", "Current Date"];
        $rootScope.importTransportMode = ["ETA", "Current Date"];


        $rootScope.navigateToNextField = function(id) {
            $timeout(function() {
                if (id != undefined && id != null) {
                    var docElement = document.getElementById(id);
                    if (docElement != undefined && docElement != null) {
                        docElement.focus();
                    }
                }
            })
        }

        $scope.gotoLogin = function() {
            $state.go("app.login", {
                id: $scope.currentLocationSetupId
            });
        }

        $scope.gotoCompany = function() {
            $state.go("app.company", {
                id: $scope.currentLocationSetupId
            });
        }

        $scope.gotoConfiguration = function() {
            $state.go("app.configuration", {
                id: $scope.currentLocationSetupId
            });
        }

        $scope.gotoMetaConfig = function() {
            $state.go("app.metaconfig", {
                id: $scope.currentLocationSetupId
            });
        }

        $scope.gotoFinance = function() {
            $state.go("app.finance", {
                id: $scope.currentLocationSetupId
            });
        }

        $scope.gotoCharge = function() {
            $state.go("app.charge", {
                id: $scope.currentLocationSetupId
            });
        }

        $scope.gotoOthers = function() {
            $state.go("app.others", {
                id: $scope.currentLocationSetupId
            });
        }

        console.log("UserRegistrationCtrl is loaded.............", $state, $stateParams);

        if ($scope.locationSetup == undefined && $scope.locationSetup == null) {
            $scope.locationSetup = {};
            $http({
                method: "GET",
                headers: {
                    'SAAS_ID': $rootScope.saasId
                },
                url: $rootScope.urlPath + '/public/api/v1/locationsetup/errors'
            }).then(function(response) {
                if (response.data != null && response.data.responseObject) {
                    $rootScope.errorMessage = response.data.responseObject;
                    console.log("Error Messages loaded.........................", $rootScope.errorMessage);

                    $http({
                        method: "GET",
                        headers: {
                            'SAAS_ID': $rootScope.saasId
                        },
                        url: $rootScope.urlPath + '/public/api/v1/locationsetup/reg_exp'
                    }).then(function(response) {
                        if (response.data != null && response.data.responseObject) {
                            $rootScope.regularExpressions = response.data.responseObject;
                            console.log("Regular Expressions loaded.........................", $rootScope.regularExpressions);

                            $http({
                                method: "GET",
                                headers: {
                                    'SAAS_ID': $rootScope.saasId
                                },
                                url: $rootScope.urlPath + '/public/api/v1/locationsetup/configurations'
                            }).then(function(response) {
                                if (response.data != null && response.data.responseObject) {
                                    $rootScope.configurations = response.data.responseObject;
                                    console.log("Configurations loaded.........................", $rootScope.configurations);
                                    $state.go("app.company");
                                }
                            }, function(error) {
                                console.log("Error", error);
                            });


                            $state.go("app.company");
                        }
                    }, function(error) {
                        console.log("Error", error);
                    });
                }
            }, function(error) {
                console.log("Error", error);
            });
        }

        $rootScope.currentSection = 'Company';



        $scope.gotoState = function(stateName) {

            console.log("Current Section : " + $rootScope.currentSection);
            console.log("State Name : " + stateName);

            if ($rootScope.currentSection == 'Company') {
                $rootScope.saveCompanyTab(stateName);
            } else if ($rootScope.currentSection == 'Login') {
                $rootScope.saveLoginTab(stateName);
            } else if ($rootScope.currentSection == 'Configuration') {
                $rootScope.saveLocationConfiguration(stateName);
            } else if ($rootScope.currentSection == 'metaConfig') {
                $rootScope.saveMetaConfigTab(stateName);
            } else if ($rootScope.currentSection == 'Finance') {
                $rootScope.saveFinanceTab(stateName);
            } else if ($rootScope.currentSection == 'Charge') {
                $rootScope.saveChargeSetup(stateName);
            } else if ($rootScope.currentSection == 'Other') {
                $rootScope.saveOtherTab(stateName);
            }


        }

    }
]);