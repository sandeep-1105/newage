NewAgeRegistration.factory("SearchServiceNotInList",['$resource', function($resource) {
    return $resource("/public/api/v1/common/servicemaster/get/search/exclude", {}, {
        fetch: {
            method: 'POST',
            headers: {
                'SAAS_ID': 'COM'
            }
        }
    });
}]);

NewAgeRegistration.service("ValidationService",['$rootScope', function($rootScope) {

    this.checkFormat = function(object, regExp) {
        var regexp = new RegExp(regExp);
        if (!regexp.test(object)) {
            return false;
        }
        return true;
    }

    this.checkRegExp = function(key, object) {
        console.log("regular expression", $rootScope.regularExpressions[key]);
        return this.checkFormat(object, $rootScope.regularExpressions[key]);
    }
}]);



NewAgeRegistration.factory("PartyMasterBulkUpload",['$resource', function($resource) {
    return $resource("/public/api/v1/customeroragent/bulkupload", {}, {
        upload: {
            method: 'POST',
            headers: {
                'SAAS_ID': 'COM'
            }
        }
    });
}]);

NewAgeRegistration.factory("AgentPortMasterBulkUpload",['$resource', function($resource) {
    return $resource("/public/api/v1/agentportmaster/bulkupload", {}, {
        upload: {
            method: 'POST',
            headers: {
                'SAAS_ID': 'COM'
            }
        }
    });
}]);


NewAgeRegistration.factory("ChargeMasterBulkUpload",['$resource', function($resource) {
    return $resource("/public/api/v1/chargemaster/bulkupload", {}, {
        upload: {
            method: 'POST',
            headers: {
                'SAAS_ID': 'COM'
            }
        }
    });
}]);

NewAgeRegistration.service("UserRegistrationDataService",['$http','SearchServiceNotInList', function($http, SearchServiceNotInList) {

    this.getList = function(keyword, saasId, urlPath, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            selectedPageNumber: 0,
            recordPerPage: 10
        };

        if (idList != undefined && idList.length > 0) {
            searchDto.idList = angular.copy(idList);
        }

        var request = $http({
            method: "post",
            headers: {
                'SAAS_ID': saasId
            },
            url: urlPath,
            data: searchDto
        })

        return request;
    }

    this.getSearchServiceNotInList = function(keyword, idList) {
        var searchDto = {
            keyword: (keyword == null) ? "" : keyword,
            ids: idList,
            selectedPageNumber: 0,
            recordPerPage: 10
        };
        return SearchServiceNotInList.fetch(searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    return data.responseObject.searchResult;
                }
            },
            function(errResponse) {
                console.error('Error while fetching ServiceListExcludeExisting ', errResponse);
                return [];
            }
        );
    }


    this.getLocationSetupById = function(saasId, urlPath) {
        var request = $http({
            method: "GET",
            headers: {
                'SAAS_ID': saasId
            },
            url: urlPath
        })

        return request;
    }

    this.serviceSearch = function(searchDto, urlPath, saasId) {
        var request = $http({
            method: "POST",
            headers: {
                "SAAS_ID": saasId
            },
            url: urlPath + "/public/api/v1/servicemaster/search/keyword",
            data: searchDto
        });
        return request;

    }

    this.chargeSearch = function(searchDto, urlPath, saasId) {
        var request = $http({
            method: "POST",
            headers: {
                "SAAS_ID": saasId
            },
            url: urlPath + "/public/api/v1/chargemaster/get/search/keyword",
            data: searchDto
        });
        return request;

    }

    this.categorySearch = function(searchDto, urlPath, saasId) {
        var request = $http({
            method: "POST",
            headers: {
                "SAAS_ID": saasId
            },
            url: urlPath + "/public/api/v1/categoryMaster/search/keyword",
            data: searchDto
        });
        return request;

    }


    this.getAllSequenceGenerator = function(urlPath, saasId) {

        var request = $http({
            method: "GET",
            headers: {
                "SAAS_ID": saasId
            },
            url: urlPath + "/public/api/v1/locationsetup/sequence_generation"
        });

        return request;
    }

    this.daybookSearch = function(searchDto, urlPath, saasId) {
        var request = $http({
            method: "POST",
            headers: {
                "SAAS_ID": saasId
            },
            url: urlPath + "/public/api/v1/daybook/search",
            data: searchDto
        });
        return request;

    }
}]);


NewAgeRegistration.factory('UserRegistrationFactory',['$resource', function($resource) {
    return {
        create: $resource('/public/api/v1/locationsetup/create_company', {}, {
            query: {
                method: 'POST',
                headers: {
                    'SAAS_ID': 'COM'
                }
            }
        })
    };
}]);


NewAgeRegistration.service("UserRegistrationUpdateService",['$http', function($http) {

    this.post = function(obj, saasId, urlPath) {

        var request = $http({
            method: "POST",
            headers: {
                'SAAS_ID': saasId
            },
            url: urlPath,
            data: obj
        })

        return request;
    }

}]);