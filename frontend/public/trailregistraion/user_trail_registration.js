var NewAgeTrailRegistration = angular.module("NewAgeTrailRegistration", [
    'ngStorage',
    'ui-notification',
    'ui.bootstrap',
    'ngResource',
    'mgcrea.ngStrap'
]);


NewAgeTrailRegistration.service("ValidationService",['$rootScope', function($rootScope) {

    this.checkFormat = function(object, regExp) {
        var regexp = new RegExp(regExp);
        if (!regexp.test(object)) {
            return false;
        }
        return true;
    }

    this.checkRegExp = function(key, object) {
        console.log("regular expression", $rootScope.regularExpressions[key]);
        return this.checkFormat(object, $rootScope.regularExpressions[key]);
    }
}]);

NewAgeTrailRegistration.factory('UserRegistrationFactory',['$resource', function($resource) {
    return {
        create: $resource('/public/api/v1/locationsetup/create_company', {}, {
            query: {
                method: 'POST',
                headers: {
                    'SAAS_ID': 'COM'
                }
            }
        })
    };
}]);


NewAgeTrailRegistration.controller("UserTrailRegistrationCtrl", ['$rootScope', '$scope', '$timeout', '$location', 'Notification', '$http', 'UserRegistrationFactory', 'ValidationService',
    function($rootScope, $scope, $timeout, $location, Notification, $http, UserRegistrationFactory, ValidationService) {

        //initialize the object
        $scope.init = function() {
            $scope.locationSetup = {};
        }

        var protocol = $location.protocol();
        var host = $location.host();
        var urlPath = protocol + "://" + host;

        var port = $location.port();
        if (port != undefined && port != null) {
            port = ":" + port
        } else {
            port = "";
        }

        urlPath = urlPath + port;
        $rootScope.urlPath = urlPath;
        // $rootScope.urlPath = BASE_URL;
        $rootScope.saasId = "COM";


        //Free trail invocation
        $scope.startFreeTrail = function() {
            console.log("free trail called");
            if ($scope.validate(0)) { //validation
                var tmpObj = angular.copy($scope.locationSetup);
                tmpObj.currentSection = "Trail";
                UserRegistrationFactory.create.query(tmpObj).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Location Master saved :: ", data);
                        //Notification.success($rootScope.errorMessage["ERRC0206"]);
                        window.open(urlPath + "/public/trailregistraion/trail_accout_initiation_success.html");
                        $scope.locationSetup = {};
                    } else {
                        console.log("Create Failed :" + data.responseDescription)
                    }
                }, function(error) {
                    console.log("Create Failed : " + error)
                });
            } else {
                console.log("validation error...");
            }
        }



        /*  $scope.singleMailFormat = function(object) {
    		var regexp = new RegExp("^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$");
    		if (!regexp.test(object)) {
    			return false;
    		}
    		return true;
    }
	*/

        $scope.checkFormat = function(object, regExp) {
            var regexp = new RegExp(regExp);
            if (!regexp.test(object)) {
                return false;
            }
            return true;
        }
        var multipleEmailExpr = /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        $scope.checkMultipleMail = function(value) {
            var emailArr = value.split(";");
            if (emailArr != null && emailArr.length > 0) {
                for (var i = 0; i < emailArr.length; i++) {
                    if (!$scope.checkFormat(emailArr[i], multipleEmailExpr)) {
                        return false;
                    }
                }
            }
            return true;
        }



        $scope.validate = function(value) {

            $scope.errorMap = new Map();

            if (value == 0 || value == 1) {

                if ($scope.locationSetup.email == undefined || $scope.locationSetup.email == null || $scope.locationSetup.email == "") {
                    $scope.errorMap.put('email', $rootScope.errorMessage["ERRC0104"]);
                    return false;
                } else {
                    if (!$scope.checkMultipleMail($scope.locationSetup.email)) {
                        $rootScope.navigateToNextField("email");
                        $scope.errorMap.put("email", $rootScope.errorMessage["ERRC0204"]);
                        return false;
                    }
                }

                /* var regexp = new RegExp("^(([^<>()\[\]\\.,,:\s@']+(\.[^<>()\[\]\\.,,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
			if (!regexp
					.test($scope.locationSetup.email)) {
				$scope.errorMap.put("email",$rootScope.errorMessage["ERRC0204"]);
				return false;
			}*/
            }

            if (value == 0 || value == 2) {
                if ($scope.locationSetup.firstName == undefined || $scope.locationSetup.firstName == null || $scope.locationSetup.firstName == "") {
                    $scope.errorMap.put('firstName', $rootScope.errorMessage["ERRC0202"]);
                    $scope.navigateToNextField("firstName");
                    return false;
                } else {
                    if (!ValidationService.checkRegExp('first.name', $scope.locationSetup.firstName)) {
                        $scope.errorMap.put("firstName", $rootScope.errorMessage["ERRC0208"]);
                        $scope.navigateToNextField("firstName");
                        return false;
                    }
                }
            }
            if (value == 0 || value == 3) {

                if ($scope.locationSetup.lastName == undefined || $scope.locationSetup.lastName == null || $scope.locationSetup.lastName == "") {
                    $scope.errorMap.put('lastName', $rootScope.errorMessage["ERRC0203"]);
                    $scope.navigateToNextField("lastName");
                    return false;
                } else {
                    if (!ValidationService.checkRegExp('last.name', $scope.locationSetup.lastName)) {
                        $scope.errorMap.put("lastName", $rootScope.errorMessage["ERRC0209"]);
                        $scope.navigateToNextField("lastName");
                        return false;
                    }
                }
            }

            if (value == 0 || value == 4) {
                if ($scope.locationSetup.password == undefined || $scope.locationSetup.password == null || $scope.locationSetup.password == "") {
                    $scope.errorMap.put('password', $rootScope.errorMessage["ERRC0115"]);
                    $scope.navigateToNextField("password");
                    return false;
                }
                var regexp = new RegExp("^[a-zA-Z0-9](?=.*[0-9])(?=.*[@!#$%&()+={};<>,.?/\|])[a-zA-Z0-9!#@$%&()+={};<>,.?/\|]{5,20}$");
                if (!regexp
                    .test($scope.locationSetup.password)) {
                    $scope.errorMap.put("password", $rootScope.errorMessage["ERRC0205"]);
                    $scope.navigateToNextField("password");
                    return false;
                }
            }
            return true;
        }

        $scope.showHint = function() {
            Notification.info($rootScope.errorMessage["ERRC0205"]);
        }

        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        function checkEmail(object, regExp) {
            var regexp = new RegExp(regExp);
            if (!regexp.test(object)) {
                return false;
            }
            return true;
        }




        $rootScope.navigateToNextField = function(id) {
            $timeout(function() {
                if (id != undefined && id != null) {
                    var docElement = document.getElementById(id);
                    if (docElement != undefined && docElement != null) {
                        docElement.focus();
                    }
                }
            })
        }




        if ($scope.locationSetup == undefined && $scope.locationSetup == null) {
            $scope.locationSetup = {};
            $http({
                method: "GET",
                headers: {
                    'SAAS_ID': $rootScope.saasId
                },
                url: $rootScope.urlPath + '/public/api/v1/locationsetup/errors'
            }).then(function(response) {
                if (response.data != null && response.data.responseObject) {
                    $rootScope.errorMessage = response.data.responseObject;
                    console.log("Error Messages loaded........................." + $rootScope.errorMessage.length);

                    $http({
                        method: "GET",
                        headers: {
                            'SAAS_ID': $rootScope.saasId
                        },
                        url: $rootScope.urlPath + '/public/api/v1/locationsetup/reg_exp'
                    }).then(function(response) {
                        if (response.data != null && response.data.responseObject) {
                            $rootScope.regularExpressions = response.data.responseObject;
                            console.log("Regular Expressions loaded.........................", $rootScope.regularExpressions);
                        }
                    }, function(error) {
                        console.log("Error", error);
                    });

                }
            }, function(error) {});
        }

        try {
            $scope.init();
            $rootScope.currentSection = 'Trail';
        } catch (e) {
            Notification.error(e);
            console.log(e);
        }



        function Map() {
            this.keys = new Array();
            this.data = new Object();

            this.put = function(key, value) {
                if (this.data[key] == null) {
                    this.keys.push(key);
                }
                this.data[key] = value;
            };

            this.get = function(key) {
                //console.log("key:"+key+", "+this.data[key])
                return this.data[key];
            };

        }



    }
]);