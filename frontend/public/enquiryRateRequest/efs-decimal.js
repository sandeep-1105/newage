appNewAge.directive("efsdecimals", function ($filter, $rootScope) {  
    return {
        restrict: "A", // Only usable as an attribute of another HTML element
        require: "?ngModel",
        scope: {
        	efsdecimals: "=",
            efsdecimalPoint: "="
        },
        link: function (scope, element, attr, ngModel) {

        	var decimalCount = parseInt(scope.efsdecimals);
            var decimalPoint = scope.efsdecimalPoint ;

            scope.$watch('efsdecimals', function(newVal, oldVal) {
            	if(newVal==undefined || newVal==null ){
            		newVal=oldVal;
            	}
            	decimalCount = parseInt(newVal);
            	ngModel.$render();
              });
            
            scope.$watch('efsdecimalPoint', function(newVal, oldVal) {
            	if(newVal==undefined || newVal==null  ){
            		newVal=oldVal;
            	}
            	decimalPoint = newVal;
                ngModel.$render();
              });

            // Run when the model is first rendered and when the model is changed from code
            ngModel.$render = function() {
                if (ngModel.$modelValue != null && ngModel.$modelValue >= 0) {
                    if (typeof decimalCount === "number") {
                    	var strVal = (ngModel.$modelValue);
                        element.val(strVal.toLocaleString(decimalPoint, { minimumFractionDigits: decimalCount, maximumFractionDigits :decimalCount }));
                    } else{
                    	var strVal = (ngModel.$modelValue);
                        element.val(strVal.toLocaleString(decimalPoint, { minimumFractionDigits: decimalCount, maximumFractionDigits :decimalCount }));
                    }
                }else if(ngModel.$modelValue != null && typeof ngModel.$modelValue=="string" && ngModel.$modelValue!=""){
                	 element.val(ngModel.$modelValue);
                }else{
                	 element.val(null);
                	console.log("render not happen");
                }
            }

            // Run when the view value changes - after each keypress
            // The returned value is then written to the model
            ngModel.$parsers.unshift(function(newValue) {
                if (typeof decimalCount === "number") {
                	var floatValue = newValue.match(/[0-9,.]*/);
                	if (floatValue!==null) {
                	    floatValue= parseFloat( floatValue[0].replace(/,/g, '') ); // replace , thousands separator
                	}
                    if (decimalCount === 0) {
                        return parseInt(floatValue);
                    }
                    return parseFloat(floatValue.toFixed(decimalCount));
                }

                var floatValue = newValue.match(/[0-9,.]*/);
            	
            	if (floatValue!==null) {
            	    floatValue= parseFloat( floatValue[0].replace(/,/g, '') ); // replace , thousands separator
            	}
            	
                return parseFloat(floatValue);
            });

            // Formats the displayed value when the input field loses focus
            element.on("change", function(e) {
            	
            	var floatValue = element.val().match(/[0-9,.]*/);
            	
            	if (floatValue!==null) {
            	    floatValue= parseFloat( floatValue[0].replace(/,/g, '') ); // replace , thousands separator
            	
                	if(isNaN(floatValue)){
                		floatValue = null;
                	}

            	}
            	
            	
                if (!isNaN(floatValue) && floatValue!=null && typeof decimalCount === "number") {
                    if (decimalCount === 0) {
                        element.val(parseInt(floatValue));
                    } else {
                    	var strVal = floatValue.toLocaleString(decimalPoint, { minimumFractionDigits: decimalCount, maximumFractionDigits :decimalCount });
                        element.val(strVal);
                    }
               }
                
               
            });
        }
    }
});
