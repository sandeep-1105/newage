appNewAge.controller('enquiryRateRequestCtrl', function($scope,$rootScope,$http,$location,$timeout,publicService,publicService,Notification){
	
   	$scope.chargeList=[];
	$scope.chargeList.push({});
    	$scope.smArr=['General','SOC','COC'];
   	$scope.numberOfUnitsList=["-45", "+45", "+100", "+250", "+300", "+500", "+1000"];
	$rootScope.weightDecimalPoint=2;
	$rootScope.totalRecordFetchPerRequest=10;
   	$scope.showDimension=false;
   	$scope.showCommdoity=false;
   	
	
	var protocol = $location.protocol();
	var host = $location.host();
	var port = $location.port();
	if(port!=undefined && port!=null){
		port = ":"+port
	}else{
		port="";
	}
	var urlPath = protocol +"://"+host+port;
	$scope.confirmed=false;
	$scope.responseMessage="";
	
	var saasId = $location.search().saasId; 
	var rateRequestId = $location.search().rateRequestId; 

	//Getting document
	$http({	
			method: 'GET', 
			url: urlPath+'/public/api/v1/enquiry/raterequest/'+rateRequestId, 
			headers: {'SAAS_ID': saasId}
	}).then(function(response) {
		if(response.data.responseCode==='ERR0'){
			$scope.rateRequest = response.data.responseObject;
			if($scope.rateRequest==null){
				window.location=urlPath+"/public/enquiryRateRequest/page_not_found.html";
				console.log("rate request not created");
				return;
			}else{
				$http({	
					method: 'GET', 
					url: urlPath+'/public/api/v1/enquiry/enquirydetail/'+rateRequestId, 
					headers: {'SAAS_ID': saasId}
			}).then(function(response) {
				if(response.data.responseCode==='ERR0'){
					$scope.enquiryDetail = response.data.responseObject;
					if($scope.enquiryDetail==null){
						console.log("rate request not created");
						window.location=urlPath+"/public/enquiryRateRequest/page_not_found.html";
					}else{
						$scope.checkEnquiryStatus($scope.enquiryDetail.enquiryNo,urlPath);
					}
				}
			});
		 }
		}
	});
	
	$scope.checkEnquiryStatus=function(enquiryNo,urlPath){
		$http({	
			method: 'GET', 
			url: urlPath+'/public/api/v1/enquiry/enquirystatus/'+enquiryNo, 
			headers: {'SAAS_ID': saasId}
	}).then(function(response) {
		if(response.data.responseCode==='ERR0'){
			if($scope.rateRequest.chargeList==undefined || $scope.rateRequest.chargeList==null || $scope.rateRequest.chargeList.length==0){
				$scope.rateRequest.chargeList=[];
				$scope.rateRequest.chargeList.push({});
				$scope.navigateToNextField('chargeCode0');
			}
			if($scope.enquiryDetail.overDimension=='Yes' || $scope.enquiryDetail.overDimension==true){
				$scope.showDimension=true;
			}
			if($scope.enquiryDetail.hazardous=='Yes' || $scope.enquiryDetail.hazardous==true){
				$scope.showCommdoity=true;
			}
			$scope.navigateToNextField('chargeCode0');
		}else{
			window.location=urlPath+"/public/enquiryRateRequest/page_not_found.html";
			scope.rateRequest.chargeList=null;
		}
	});
}
	
	
	//Getting error code and error messages
	$http({	
		method: 'GET', 
		url: urlPath+'/public/api/v1/nls/'+'English', 
		headers: {'SAAS_ID': saasId}
    }).then(function(response) {
	if(response.data.responseCode==='ERR0'){
		   $scope.nls = response.data.responseObject;
           console.log("NLS Loaded Successfully : ")
       } else {
      	 console.log("NLS Loaded failed")
       }
    });
	
	$scope.validateChargeList=function(rateRequest,what){
		  var fineArr = [];
		  var validResult="";
		  var emptyList=true;
		  if(isEmptyRow(rateRequest.chargeList[0])){
			  Notification.error($scope.nls["ERR2301"]);
			  return;
		  }
		  angular.forEach(rateRequest.chargeList,function(charge,index){
		  if (isEmptyRow(charge)) {
              validResult = true;
              rateRequest.chargeList[index].isEmpty = true;
              if(what=='Save'){
            	  rateRequest.chargeList.splice(index,1);
            	  if(rateRequest.chargeList==undefined || rateRequest.chargeList==null || rateRequest.chargeList.length==0){
        			  Notification.error($scope.nls["ERR2301"]);
        			  emptyList=false;
        			  return;
        		  }
              }
              if(what=='Add'){
            	  if(rateRequest.chargeList==undefined || rateRequest.chargeList==null || rateRequest.chargeList.length==0){
        			  Notification.error($scope.nls["ERR2301"]);
        			  emptyList=false;
        			  return;
        		  }
              }
          } else {
        	  rateRequest.chargeList[index].isEmpty = false;
              var validResult = validateObj(charge,index);
              $scope.flag = true;
              if (validResult) {
                  fineArr.push(1);
                  $scope.secondArr[index].errRow = false;
              } else {
                  $scope.secondArr[index].errRow = true;
                  fineArr.push(0);
              }
          }
     });
	
	if(!emptyList){
		return false;
	}
     if (fineArr.indexOf(0) < 0 && what=='Add') {
    	 if(rateRequest.chargeList[(rateRequest.chargeList.length-1)].isEmpty==false){
    		 rateRequest.chargeList.push({});
    	 }
         $scope.secondArr.push({"errRow": false});
         $scope.navigateToNextField('chargeCode'+(rateRequest.chargeList.length-1));
         return true;
       }else if(fineArr.indexOf(0) < 0 && what=='Save'){
    	   $scope.secondArr.push({"errRow": false});
    	   return true;
       }else{
    	   $scope.secondArr.push({"errRow": true});
    	   return false;
    	   console.log("nothing...done");
       }
 
 		
	}
	
	$scope.addNewRow=function(obj,index){
		
		if(obj!=undefined && index!=undefined){
			
			if(obj.chargeList!=null && obj.chargeList!=undefined && obj.chargeList.length>0){
				
                if(isEmptyRow(obj.chargeList[index+1])){
                	 $scope.addRow();
                }else{
                	console.log("Nothing do");
                	
                }
                
			}
		}
	}
	
	
    function isEmptyRow(obj) {
        //return (Object.getOwnPropertyNames(obj).length === 0);
        var isempty = true; //  empty
        if (!obj) {
            return isempty;
        }

        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;

            }
        }
        return isempty;
    }
	
    $scope.secondArr = [];
    
	var validateObj = function (dataObj, index) {
        	 $scope.secondArr[index] = {};
        	 var errorArr = [];
        	 errorArr.push(1);
        	 $scope.secondArr[index].errTextArr = [];
        	//Charge Validation
        	 if (dataObj.chargeMaster == undefined || dataObj.chargeMaster == null || dataObj.chargeMaster.id == undefined) {
                     $scope.secondArr[index].charge = true;
                     $scope.secondArr[index].errTextArr.push($scope.nls["ERR8823"]);
                     Notification.error($scope.nls["ERR8823"]+" in line no " +(index+1));
                 errorArr.push(0);
                 return false;
             }else{
            	 if(dataObj.chargeMaster.status=='Block'){
            		 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8824"]);
            		 $scope.secondArr[index].charge = true;
            		 Notification.error($scope.nls["ERR8824"]+" in line no " +(index+1));
            		 errorArr.push(0);
            		 return false;
					}
					if(dataObj.chargeMaster.status=='Hide'){
						 $scope.secondArr[index].charge = true;
						 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8825"]);
						 Notification.error($scope.nls["ERR8825"]+" in line no " +(index+1));
						 errorArr.push(0);
						 return false;
					}
             }
        	
        	 //charge name
        	 if (dataObj.chargeName == undefined || dataObj.chargeName == null || dataObj.chargeName == "") {
        		 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8826"]);
        		 $scope.secondArr[index].chargeName = true;
        		 Notification.error($scope.nls["ERR8826"]+" in line no " +(index+1));
        		 errorArr.push(0);
        		 return false;
        	 }
        	 //unitMaster
        	 
        	 if (dataObj.unitMaster == undefined || dataObj.unitMaster == null || dataObj.unitMaster.id == undefined) {
                 $scope.secondArr[index].unit = true;
                 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8827"]);
                 Notification.error($scope.nls["ERR8827"]+" in line no " +(index+1));
             errorArr.push(0);
             return false;
             
         }else{
        	 if(dataObj.unitMaster.status=='Block'){
        		 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8828"]);
        		 $scope.secondArr[index].unit = true;
        		 Notification.error($scope.nls["ERR8828"]+" in line no " +(index+1));
        		 errorArr.push(0);
        		 return false;
				}
				if(dataObj.unitMaster.status=='Hide'){
					 $scope.secondArr[index].unit = true;
					 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8829"]);
					 Notification.error($scope.nls["ERR8829"]+" in line no " +(index+1));
					 errorArr.push(0);
					 return false;
				}
           }
        	 
        	 //currency Master
        	 
        	 
        	 if (dataObj.currencyMaster == undefined || dataObj.currencyMaster == null || dataObj.currencyMaster.id == undefined) {
                 $scope.secondArr[index].currency = true;
                 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8833"]);
                 Notification.error($scope.nls["ERR8833"]+" in line no " +(index+1));
             errorArr.push(0);
             return false;
         }else{
        	 if(dataObj.currencyMaster.status=='Block'){
        		 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8834"]);
        		 $scope.secondArr[index].currency = true;
        		  Notification.error($scope.nls["ERR8834"]+" in line no " +(index+1));
        		 errorArr.push(0);
        		 return false;
				}
				if(dataObj.currencyMaster.status=='Hide'){
					 $scope.secondArr[index].currency = true;
					 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8835"]);
					 Notification.error($scope.nls["ERR8835"]+" in line no " +(index+1));
					 errorArr.push(0);
					 return false;
				}
           }
        	 
        	 
        	 // buy rate
        	 
        	 if(dataObj.buyRate==undefined || dataObj.buyRate==null || isNaN(dataObj.buyRate) || dataObj.buyRate==""){
        		 
        		 $scope.secondArr[index].buyRate = true;
                 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8844"]);
                 Notification.error($scope.nls["ERR8844"]+" in line no " +(index+1));
                 errorArr.push(0);
                 return false; 
        		 
        	 }
        	 
        	 if (!isNaN(dataObj.buyRate) && dataObj.buyRate != undefined
                     && dataObj.buyRate != null
                     && dataObj.buyRate != "") {

                     if (isNaN(parseFloat(dataObj.buyRate))) {
                             $scope.secondArr[index].buyRate = true;
                             $scope.secondArr[index].errTextArr.push($scope.nls["ERR8830"]);
                             Notification.error($scope.nls["ERR8830"]+" in line no " +(index+1));
                             return false;
                         errorArr.push(0);
                     }
                     else if (dataObj.grossSaleMinimum == 0) {
                             $scope.secondArr[index].buyRate = true;
                             $scope.secondArr[index].errTextArr.push($scope.nls["ERR8830"]);
                             Notification.error($scope.nls["ERR8830"]+" in line no " +(index+1));
 	                             errorArr.push(0);
 	                            return false;

                     }
                     else if (dataObj.buyRate < 0 || dataObj.buyRate >= 99999999999.99) {
                             $scope.secondArr[index].buyRate = true;
                             $scope.secondArr[index].errTextArr.push($scope.nls["ERR8830"]);
                             Notification.error($scope.nls["ERR8830"]+" in line no " +(index+1));
                             errorArr.push(0);
                             return false;

                     }
                     else {
                             $scope.secondArr[index].buyRate = false;
                         }
                     }else{
                    	 $scope.secondArr[index].buyRate = true;
                    	 $scope.secondArr[index].errTextArr.push($scope.nls["ERR8830"]);
                         Notification.error($scope.nls["ERR8830"]+" in line no " +(index+1));
	                             errorArr.push(0);
	                            return false;
                     }
        	 
                       if (errorArr.indexOf(0) < 0) {
                                 return true
                               } else {
                                 return false;
                           }
        }
        
	  $scope.removeRateReqCharge=function(obj,index){
		 obj.splice(index,1);
         $scope.secondArr.splice(index, 1);
      }
        
        // Ajax Calls
     $scope.localSearch = function (object) {

            console.log("ajaxChargeEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            console.log("search data", $scope.searchDto);
            return publicService.chargeSearch($scope.searchDto,saasId,urlPath).then(function(response) {
            	return $scope.chargeMasterList = response.searchResult;
            }).catch(function(response) {
            	  console.error('Status', response.status, response);
            });
        }
     
     
     
        $scope.selectEnquiryCharge = function(rateRequestCharge,id){
        	
      	  if(rateRequestCharge.chargeMaster != undefined && rateRequestCharge.chargeMaster != null) {
      		  rateRequestCharge.chargeName = rateRequestCharge.chargeMaster.chargeName;
      		if(rateRequestCharge.chargeMaster.calculationType != undefined && rateRequestCharge.chargeMaster.calculationType != null){
      			var unitCode;
      			if(rateRequestCharge.chargeMaster.calculationType=='Percentage'){
      				unitCode = 'PER';
      			} else if(rateRequestCharge.chargeMaster.calculationType=='Shipment') {
      				unitCode = 'SHPT';
      			} else if(rateRequestCharge.chargeMaster.calculationType=='Unit') {
      				unitCode = 'KG';
      			} else if(rateRequestCharge.chargeMaster.calculationType=='Document') {
      				unitCode = 'DOC';
      			} else {
      			    console.log("unit not selected")	
      			}
      			publicService.unitCodeSearch(unitCode,saasId,urlPath).then(function(response) {
      				if(response!=undefined && response!=null && response.responseObject===undefined){
      					rateRequestCharge.unitMaster = response;
      				}else if(response!=undefined && response!=null && response.responseObject!=undefined){
      					rateRequestCharge.unitMaster = response.responseObject;
      				}
      				$scope.navigateToNextField(id);
                }).catch(function(response) {
                	  console.error('Status', response.status, response);
                });
      		}
      	  }  
        };
        
        $scope.localUnitSearch = function(object) {
            console.log("ajaxUnitMasterEvent is called ", object);
            $scope.searchDto={};
            $scope.searchDto.keyword=object==null?"":object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            
            return publicService.unitSearch($scope.searchDto,saasId,urlPath).then(function(response) {
            	return $scope.unitMasterList = response.searchResult;
            }).catch(function(response) {
            	  console.error('Status', response.status, response);
            });
            
        }
        
        
        $scope.localCurrencySearch = function (object) {

            console.log("ajaxCurrencyMasterEvent is called ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return publicService.currencySearch($scope.searchDto,saasId,urlPath).then(function(response) {
            	return $scope.currencyMasterList = response.searchResult;
            }).catch(function(response) {
            	  console.error('Status', response.status, response);
            });
        }
        
        $scope.clearChargeName = function(charge) {
        	if(charge.chargeMaster == undefined || charge.chargeMaster == null || charge.chargeMasater == "") {
        		charge.chargeName = "";
        	}
        }
        
        $scope.navigateToNextField = function(id) {
        	$timeout(function(){
        		if(id!=undefined && id != null) {
        			var docElement = document.getElementById(id);
        			if(docElement != undefined && docElement != null){
        				docElement.focus();
        			}
        		}
        	})
        }
        
        $scope.addRow = function(){
         $scope.validateChargeList($scope.rateRequest,'Add');
         $scope.navigateToNextField('chargeCode'+($scope.rateRequest.chargeList.length-1));
        }
        
		
		$scope.dateToString=function(date,format){
			return publicService.dateToString(date,format);
		}
		$scope.weightFormat = function (weight, locale){
			return publicService.weightFormat(weight,locale);
		}
		
		$scope.copyRequestCharge=function(charge,index){
        	var chargeN=JSON.parse(JSON.stringify(charge));
        	if(chargeN.id!=undefined && chargeN.id!=null){
        		chargeN.id=null;
        	}
        	$scope.rateRequest.chargeList.push(chargeN);
        }
		
	    	 $scope.cancelChargeList=function(){
	    			    var r = confirm($scope.nls["ERR205"]);
	    			    if (r == true){
	    			    	window.close();
	    			    } else {
	    			    	console.log("dont close the window");
	    			    }
		     }
		
		 $scope.removeRateReqCharge=function(obj,index){
			 obj.splice(index,1);
         }
		 
		 $scope.integerFormat = function (number, locale){
		    	if(number===undefined || number===null || number==='')
		    		return null;
		    	return number.toLocaleString(locale, { minimumFractionDigits: 0, maximumFractionDigits: 0 })
		    }
		    
		 $scope.weightFormat = function (weight, locale){
		    	if(weight===undefined || weight===null || weight==='')
		    		return null;
		    	return weight.toLocaleString(locale, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
		    }
		
		//save charge list for rate request
		$scope.saveChargeList=function(){
			
			$scope.rateRequest.enquiryDetail=$scope.enquiryDetail;
			/*if($scope.enquiryDetail!=undefined && $scope.enquiryDetail.rateRequestList!=null && $scope.enquiryDetail.rateRequestList.length>0){
				for(var i=0;i<$scope.enquiryDetail.rateRequestList.length;i++){
					if($scope.rateRequest.id==$scope.enquiryDetail.rateRequestList[i].id){
						$scope.enquiryDetail.rateRequestList[i]=angular.copy($scope.rateRequest);
						$scope.rateRequest.enquiryDetail=$scope.enquiryDetail;
						break;
					}
				}
			}*/
			if($scope.rateRequest.chargeList==undefined || $scope.rateRequest.chargeList==null || $scope.rateRequest.chargeList.length==0){
				Notification.error($scope.nls["ERR8839"]);
				return;
			}else{
				//save enquiry detail
			if($scope.validateChargeList($scope.rateRequest,'Save')){
				if($scope.rateRequest.pin==undefined || $scope.rateRequest.pin==undefined || $scope.rateRequest.pin==""){
					Notification.error($scope.nls["ERR8837"]);
					return;
				}else{
	if($scope.rateRequest.vendor.partyDetail==undefined || $scope.rateRequest.vendor.partyDetail==null ||
			$scope.rateRequest.vendor.partyDetail.pin==undefined || $scope.rateRequest.vendor.partyDetail.pin==null){
						Notification.error($scope.nls["ERR8838"]);
						return;
	}
					if($scope.rateRequest.pin==$scope.rateRequest.vendor.partyDetail.pin){
	                 console.log("pin was correct");					
					}else{
						Notification.error($scope.nls["ERR8838"]);
						return;
					}
				}
                 publicService.enquiryDetailSave($scope.rateRequest,saasId,urlPath).then(function(response) {
                	 if(response.responseCode=="ERR0"){
                		 window.location=urlPath+"/public/enquiryRateRequest/save-popup.html";
                		 Notification.success($scope.nls["ERR8841"]);
                	 }else{
                		 Notification.error($scope.nls[response.responseCode]);
                	 }
	            }).catch(function(response) {
	            	Notification.error($scope.nls["ERR8840"]);
	            	  console.error('Status', response.status, response);
	            });
  			}	
			 
			}
		}
});