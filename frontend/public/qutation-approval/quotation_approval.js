
var appNewAge =  angular.module('NewAgePublic', [
    'mgcrea.ngStrap','ui.bootstrap'], function($locationProvider) {
	$locationProvider.html5Mode({
		  enabled: true,
		  requireBase: false
		});
	});

appNewAge.filter('chargeType', function() {
	
	return function(value) {
		
		if(value == 'CHARGEABLE') {
			return 'No';
		}
		else if(value == 'ACTUAL') {
			return 'Yes';
		}
		
	}
});


appNewAge.filter('quoteType', function() {
	
	return function(value) {
		
		if(value == 'GENERAL') {
			return 'General';
		}
		else if(value == 'LUMPSUM') {
			return 'Lump Sum';
		}
		else if(value == 'PERKG') {
			return 'Per KG';
		}
	}
	
});

appNewAge.controller('quotationApprovalCtrl', function($scope,$window,$http,$modal, $location,publicService){
	var protocol = $location.protocol();
	var host = $location.host();
	var port = $location.port();
	if(port!=undefined && port!=null){
		port = ":"+port
	}else{
		port="";
	}
	var urlPath = protocol +"://"+host+port;
	var saasId = $location.search().saasId; 
	var quotationId = $location.search().quotationId; 
	
	$scope.message="";
	$scope.quotationApproval = {};
	$scope.rejectReason=false;
	$scope.approved="Pending";
	$scope.selectedCarrierIndex = 0;
	

    $scope.navigateToNextField = function(id) {
    	$timeout(function(){
    		if(id!=undefined && id != null) {
    			var docElement = document.getElementById(id);
    			if(docElement != undefined && docElement != null){
    				docElement.focus();
    			}
    		}
    	})
    }

	$scope.moveCursor = function(id) {
		$scope.navigateToNextField(id);
	}
	
    

	$scope.clickSelectedService = function(index){
		
		$scope.selectedServiceIndex = index;
		
		$scope.selectMultiCarrierModel(0);
	}
	
	$scope.rejectQuotation=function(){
		$scope.quotation.rejectReason=null;
		if($scope.validate()==false){
			console.log("quotation approval validation Failed")
			return false;
		}
		$scope.rejectReason=true;
	}
	
	 $scope.reload = function() { 
		 $window.location.reload();
		  };

	//Getting quotation
	$http({	
			method: 'GET', 
			url: urlPath+'/public/api/v1/quotation/'+quotationId, 
			headers: {'SAAS_ID': saasId}
	}).then(function(response) {
		if(response.data.responseCode==='ERR0'){
			$scope.quotation = response.data.responseObject;
			$scope.clickSelectedService(0);
			
			//getting logo based on location id
			$http({	
				method: 'GET', 
				url: urlPath+'/public/api/v1/logoByLocation/'+$scope.quotation.locationMaster.id, 
				headers: {'SAAS_ID': saasId}
		}).then(function(response) {
			if(response.data.responseCode==='ERR0'){
				$scope.logoMaster = response.data.responseObject;
			}else{
				 console.log("failed ");
			}
		});
			if($scope.quotation.approved == 'Gained') {
				$scope.approved='Approved';
				$scope.message=$scope.nls["ERR259"];
			} else if($scope.quotation.approved == 'Rejected') {
				$scope.approved='Rejected';
				$scope.message=$scope.nls["ERR260"];
			}
		}else{
			 console.log("failed ");
		}
	});
	
	
	//Getting error code and error messages
	$http({	
		method: 'GET', 
		url: urlPath+'/public/api/v1/nls/'+'English', 
		headers: {'SAAS_ID': saasId}
    }).then(function(response) {
	if(response.data.responseCode==='ERR0'){
		   $scope.nls = response.data.responseObject;
           console.log("NLS Loaded Successfully : ")
       } else {
      	 console.log("NLS Loaded failed")
       }
    });
	

       
       $scope.selectMultiCarrierModel = function(index) {
    	   	$scope.selectedCarrierIndex = index;
    	   	
    	   	if($scope.quotation.quotationDetailList[$scope.selectedServiceIndex].quotationCarrierList!=undefined
    	   			&& $scope.quotation.quotationDetailList[$scope.selectedServiceIndex].quotationCarrierList!=null
    	   			&& $scope.quotation.quotationDetailList[$scope.selectedServiceIndex].quotationCarrierList.length==1){
    	   		$scope.quotationApproval.quotationCarrierId = $scope.quotation.quotationDetailList[$scope.selectedServiceIndex].quotationCarrierList[$scope.selectedCarrierIndex].id;
    	   	}
    	   	 
    	   }
       
	$scope.callAPI = function(status) {
		
		if($scope.validate()==false){
			console.log("quotation approval validation Failed")
			return false;
		}
		
		$scope.quotationApproval.quotationNo = $scope.quotation.quotationNo;
		$scope.quotationApproval.approved = status;
		$http({
			  method: 'POST',
			  headers: {'SAAS_ID': saasId},
			  url: urlPath+'/public/api/v1/quotation/party/approval',
			  data: $scope.quotationApproval
			}).then(
			  function (response) {
				  
				  if(response.data.responseCode==='ERR0'){
						if(response.data.responseObject == 'Approved') {
							$scope.approved="Approved";
							$scope.message=$scope.nls["ERR261"];
						} else if(response.data.responseObject == 'Rejected') {
							$scope.approved="Rejected";
							$scope.message=$scope.nls["ERR262"];
						}
					}else{
						$scope.message=$scope.nls[response.data.responseCode];
						 console.log("failed ");
					}
				  
			  }, function (error) {
				  console.log("Exception ",error);
			  });
		
		
	}
		$scope.validate = function() {
			if($scope.quotationApproval.pin == null 
					|| $scope.quotationApproval.pin == undefined || $scope.quotationApproval.pin == null){
				$scope.errorMsg=$scope.nls["ERR258"];
					return false;
			}
			
			if($scope.quotationApproval.pin != $scope.quotation.customer.partyDetail.pin){
					$scope.errorMsg=$scope.nls["ERR255"];
					return false;
			}
			
			
		return true;
		}
	
		
		$scope.dateToString=function(date,format){
			return publicService.dateToString(date,format);
		}
		$scope.weightFormat = function (weight, locale){
			return publicService.weightFormat(weight,locale);
		}
		
		$scope.currencyFormat = function (currency, amount){
			return publicService.currencyFormat(currency,amount);
		}
		
	
});