
var appNewAge =  angular.module('NewAgePublic', [], function($locationProvider) {
	$locationProvider.html5Mode({
		  enabled: true,
		  requireBase: false
		});
	});

appNewAge.controller('hawbConfirmCtrl', function($scope, $http, $location,publicService){
	
	$scope.spinner = true;
	
	var protocol = $location.protocol();
	var host = $location.host();
	var port = $location.port();
	if(port!=undefined && port!=null){
		port = ":"+port
	}else{
		port="";
	}
	var urlPath = protocol +"://"+host+port;
	$scope.confirmed=false;
	$scope.responseMessage="";
	$scope.hawbCustomerConfirm = {};
	$scope.connection={};
	$scope.documentDetail={};
	
	var saasId = $location.search().saasId; 
	var documentId = $location.search().documentId; 

	//Getting document
	$http({	
			method: 'GET', 
			url: urlPath+'/public/api/v1/document/'+documentId, 
			headers: {'SAAS_ID': saasId}
	}).then(function(response) {
		if(response.data.responseCode==='ERR0'){
			
			$scope.documentDetail = response.data.responseObject;
			if($scope.documentDetail.isCustomerConform == 'Yes' || $scope.documentDetail.isCustomerConform ==true) {
				$scope.confirmed=true;
				$scope.responseMessage= $scope.nls["ERR90624"];
				return;
		    }
			
			$scope.dimensionLabel=" PCS - L  X  W  X  H" + (($scope.documentDetail.dimensionUnit=="Inch/Pounds" 
				|| $scope.documentDetail.dimensionUnit==true) ?'(inch)':'(cms)');		 
			$scope.documentDetail.dimension="";
		    if($scope.documentDetail.commodityDescription!=undefined && $scope.documentDetail.commodityDescription!=null)
			$scope.documentDetail.dimension=$scope.documentDetail.commodityDescription;
		    if($scope.documentDetail!=undefined && $scope.documentDetail!=null && $scope.documentDetail.dimensionList.length>0){
		    	$scope.documentDetail.dimension= $scope.documentDetail.dimension+'\n'+$scope.dimensionLabel;
		    if(!isEmptyRow($scope.documentDetail.dimensionList[0])){	
		    	 for(var i=0;i<$scope.documentDetail.dimensionList.length;i++){
						 $scope.documentDetail.dimension=$scope.documentDetail.dimension +'\n'+"  "+
						   $scope.documentDetail.dimensionList[i].noOfPiece+" "+"-"+' '+$scope.documentDetail.dimensionList[i].length+' '+"X"+' '+$scope.documentDetail.dimensionList[i].width+' '+"X"+
						  '  '+$scope.documentDetail.dimensionList[i].height+'\n'+" ";
					}
		    }else{
		    	$scope.documentDetail.dimension=null;
		    }
			}
		    if($scope.documentDetail.mawbNo!=undefined && $scope.documentDetail.mawbNo!=null && $scope.documentDetail.mawbNo!=""){
				var MawbNo=$scope.documentDetail.mawbNo.match(/(.{1,3})/g);
				var mawbwithoutPrefix=MawbNo[1]+MawbNo[2]+MawbNo[3];// having the values of after slicing the carrier num	
			    mawbwithoutPrefix=mawbwithoutPrefix.match(/(.{1,4})/g);//having the vli
			    $scope.documentDetail.mawbNowithoutPrefix1=mawbwithoutPrefix[0]+" "+mawbwithoutPrefix[1];
				$scope.documentDetail.mawbNowithoutPrefix2=MawbNo[0]+"|"+mawbwithoutPrefix[0]+" "+mawbwithoutPrefix[1];
			}
		    
		    if($scope.documentDetail!=undefined && $scope.documentDetail!=null){

				$scope.documentDetail.accountandNotify=""
					if($scope.documentDetail.accoutingInformation!=undefined && $scope.documentDetail.accoutingInformation!=null){
						$scope.documentDetail.accountandNotify=$scope.documentDetail.accoutingInformation
					}
						
						if($scope.documentDetail.firstNotify!=undefined && $scope.documentDetail.firstNotify!=null){
							
							$scope.documentDetail.accountandNotify=$scope.documentDetail.accountandNotify +'\n'+
							
							$scope.documentDetail.firstNotify.partyName +'\n'+ $scope.documentDetail.firstNotify.partyAddressList[0].addressLine1 +'\n'+
							
							$scope.documentDetail.firstNotify.partyAddressList[0].addressLine2 +'\n'+ ""
							
							if($scope.documentDetail.firstNotify.partyAddressList[0].addressLine3!=undefined && $scope.documentDetail.firstNotify.partyAddressList[0].addressLine3!=null)
								$scope.documentDetail.accountandNotify =$scope.documentDetail.accountandNotify+$scope.documentDetail.firstNotify.partyAddressList[0].addressLine3+'\n'+$scope.getPhoneandFax($scope.documentDetail.firstNotify);
						}
				}
		    
		   
			//Getting shipment service
			$http({	
					method: 'GET', 
					url: urlPath+'/public/api/v1/shipmentservice/'+$scope.documentDetail.serviceUid, 
					headers: {'SAAS_ID': saasId}
			}).then(function(response) {
				 

				if(response.data.responseCode==='ERR0'){
					$scope.shipmentServiceDetail = response.data.responseObject;
					
					 if($scope.shipmentServiceDetail.routeNo!=null && $scope.shipmentServiceDetail.routeNo!="" && $scope.shipmentServiceDetail.etd!=null && $scope.shipmentServiceDetail.etd!=""){
						 var etdDate = "";
						 if($scope.isEdit){
							 etdDate = $scope.shipmentServiceDetail.etd;
						 }else{
							 etdDate = $scope.dateToString($scope.shipmentServiceDetail.etd,$scope.shipmentServiceDetail.location.dateFormat);
						 }
						 $scope.shipmentServiceDetail.flightanddate=$scope.shipmentServiceDetail.routeNo +"/"+  etdDate; 
					 }  
				   
				    if($scope.shipmentServiceDetail.connectionList[length-1]!=undefined && $scope.shipmentServiceDetail.connectionList[length-1].flightVoyageNo!=undefined){
						var etdDate = "";
						 if($scope.isEdit){
							 etdDate = $scope.shipmentServiceDetail.connectionList[length-1].etd;
						 }else{
							 etdDate = $rootScope.dateToString($scope.shipmentServiceDetail.connectionList[length-1].etd,$scope.shipmentServiceDetail.location.dateFormat);
						 }
						 $scope.connection.flightanddate=$scope.shipmentServiceDetail.connectionList[length-1].flightVoyageNo +"/"+  etdDate;
					}
					
					
					  if($scope.shipmentServiceDetail.isAgreed || $scope.shipmentServiceDetail.isAgreed=='Yes'){
							 $scope.documentDetail.totalAmount="AS AGREED";  // Total AMount
							 $scope.documentDetail.ratePerCharge=null;
							
							 if($scope.documentDetail.chgs=='P'){
								 $scope.documentDetail.preWeightCharge="AS AGREED";
								 $scope.documentDetail.totalPrepaid="AS AGREED";
							 }
							 if($scope.documentDetail.chgs=='C'){
								 $scope.documentDetail.collWeightCharge="AS AGREED";
								 $scope.documentDetail.totalCollect="AS AGREED";    
							 }
						 }
					 
					  if ($scope.documentDetail.shipperSignature == undefined || $scope.documentDetail.shipperSignature == null || $scope.documentDetail.shipperSignature == "") {
			                 $scope.documentDetail.shipperSignature = null;
			             }
					  $scope.getAccounts(0);
					  $scope.hawbLabel = "HAWB";
					  $scope.spinner = false;
				}
			});
		}
	});
	//Accounts code for Parties
    $scope.getAccounts = function(code) {

        if (code == 0 || code == 1) {

            $scope.documentDetail.shipperAccount = null;
            if ($scope.documentDetail.shipper != undefined && $scope.documentDetail.shipper != null && $scope.documentDetail.shipper.partyAccountList != null && $scope.documentDetail.shipper.partyAccountList.length > 0) {

                $scope.documentDetail.shipperAccount = $scope.documentDetail.shipper.partyAccountList[0].accountMaster.accountCode;
            } else {
                $scope.documentDetail.shipperAccount = null;
            }

        }

        if (code == 0 || code == 2) {

            $scope.documentDetail.consigneeAccount = null;
            if ($scope.documentDetail.consignee != undefined && $scope.documentDetail.consignee != null && $scope.documentDetail.consignee.partyAccountList != null && $scope.documentDetail.consignee.partyAccountList.length > 0) {

                $scope.documentDetail.consigneeAccount = $scope.documentDetail.consignee.partyAccountList[0].accountMaster.accountCode;

            } else {

                $scope.documentDetail.consigneeAccount = null;
            }
        }

        if (code == 0 || code == 3) {

            $scope.documentDetail.issuingAgentAccount = null;
            if ($scope.documentDetail.issuingAgent != undefined && $scope.documentDetail.issuingAgent != null && $scope.documentDetail.issuingAgent.partyAccountList != null && $scope.documentDetail.issuingAgent.partyAccountList.length > 0) {

                $scope.documentDetail.issuingAgentAccount = $scope.documentDetail.issuingAgent.partyAccountList[0].accountMaster.accountCode;

            } else {

                $scope.documentDetail.issuingAgentAccount = null;
            }

        }
    }
	 $scope.range = function(min, max, step) {
		    step = step || 1;
		    var input = [];
		    for (var i = min; i <= max; i += step) {
		    	if($scope.shipmentServiceDetail.shipmentChargeList!=null 
		    			&& $scope.shipmentServiceDetail.shipmentChargeList!=undefined 
		    			&& $scope.shipmentServiceDetail.shipmentChargeList[i]!=undefined){
		    		if($scope.shipmentServiceDetail.shipmentChargeList[i].due!=undefined 
		    				|| $scope.shipmentServiceDetail.shipmentChargeList[i].due!=null){
		    			var obj = $scope.shipmentServiceDetail.shipmentChargeList[i];
		    			if($scope.rateMergedValue || $scope.rateMergedValue=='true'){
		    				obj.localTotalRateAmount = $rootScope.currency(obj.localCurrency,obj.localTotalRateAmount);	
		    			}else{
		    				obj.localTotalNetSale=$rootScope.currency(obj.netSaleCurrency,obj.localTotalNetSale);
		    			}
		            	input.push(obj);	
		    		}else{
		    			console.log("dont push charge");
		    		}
		    	}
		    }
		    return input;
		};
	
	//Getting error code and error messages
	$http({	
		method: 'GET', 
		url: urlPath+'/public/api/v1/nls/'+'English', 
		headers: {'SAAS_ID': saasId}
    }).then(function(response) {
	if(response.data.responseCode==='ERR0'){
		   $scope.nls = response.data.responseObject;
           console.log("NLS Loaded Successfully : ")
       } else {
      	 console.log("NLS Loaded failed")
       }
    });
	
	  function isEmptyRow(obj){
	        var isempty = true; //  empty
	        if (!obj) {
	            return isempty;
	        }

	        var k = Object.getOwnPropertyNames(obj);
	        for (var i = 0; i < k.length; i++){
	            if (obj[k[i]]) {
	                isempty = false; // not empty
	                break;

	            }
	        }
	        return isempty;
	    }
	
	$scope.callAPI = function(status) {
		if(status=='No'){
			$scope.confirmed=true;
			$scope.responseMessage= $scope.nls["ERR90623"];
			return;
		}
		
		if($scope.validate()==false){
			console.log("Draft hawb conformation validation Failed")
			return false;
		}
		
		$scope.hawbCustomerConfirm.documentId = $scope.documentDetail.id;
		$scope.hawbCustomerConfirm.isCustomerConform = status;
		
		$http({
			  method: 'POST',
			  headers: {'SAAS_ID': saasId},
			  url: urlPath+'/public/api/v1/hawb/party/confirmation',
			  data: $scope.hawbCustomerConfirm
			}).then(
			  function (response) {
				  
				  if(response.data.responseCode==='ERR0'){
						if(response.data.responseObject == 'Yes' || response.data.responseObject == true) {
							$scope.confirmed=true;
							$scope.responseMessage= $scope.nls["ERR90622"];
						} 
					}else{
						$scope.confirmed=true;
						$scope.responseMessage= $scope.nls["ERR90621"];
					}
				  
			  }, function (error) {
				  console.log("Exception ",error);
				  $scope.confirmed=true;
				  $scope.responseMessage=$scope.nls["ERR90621"]; 
			  });
		
		
	}
		$scope.validate = function() {
			if($scope.hawbCustomerConfirm.pin == null 
					|| $scope.hawbCustomerConfirm.pin == undefined || $scope.hawbCustomerConfirm.pin == ""){
					$scope.errorMsg= $scope.nls["ERR90620"];
					return false;
			}
			
			if($scope.shipmentServiceDetail.party.partyDetail==null || $scope.hawbCustomerConfirm.pin != $scope.shipmentServiceDetail.party.partyDetail.pin){
					$scope.errorMsg= $scope.nls["ERR90619"];
					return false;
			}
		return true;
		}
	
		
		$scope.dateToString=function(date,format){
			return publicService.dateToString(date,format);
		}
		
		$scope.weightFormat = function (weight, locale){
			return publicService.weightFormat(weight,locale);
		}

		$scope.currencyFormat = function (currency, locale){
			return publicService.currencyFormat(currency,locale);
		}
	
});