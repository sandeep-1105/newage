NewAgeRegistration.directive('buttonGroup',['$filter', function($filter) {
    return {
        restrict: 'E',
        scope: {
            states: '=',
            state: '=',
            btnIndex: '@',
            btnId: '@',
            onStateChange: '&',
            btnDisabled: '=?'
        },
        template: '<div class="btn-group btn-group-justified">' +
            '<div class="btn-group" role="group" ng-repeat="s in tempList" ng-class="{\'curnot_allowed\':btnDisabled}">' +
            '<button tabindex="{{btnIndex}}" id="{{btnId}}" ng-disabled="btnDisabled" type="button" class="btn fsl-btn-group" data-state={{s.key}} ng-click="select(s, $event)">' +
            '{{s.value}}' +
            '</button>' +
            '</div>' +
            '</div>',
        link: function(scope, element, attr) {

            console.log("item", scope.states);
            scope.$watch('states', function(newValue, oldValue) {
                //if(newValue && newValue!=oldValue){
                scope.tempList = [];
                angular.forEach(scope.states, function(item) {
                    if (attr.filterType) {
                        var filterItem = $filter(attr.filterType)(item);
                        var tempObj = {
                            "key": item,
                            "value": filterItem
                        }
                        scope.tempList.push(tempObj);
                    } else {
                        var tempObj = {
                            "key": item,
                            "value": item
                        }
                        scope.tempList.push(tempObj);
                    }


                })
                //}
            });
        },
        controller: function($scope, $element, $timeout) {

            // Make sure that style is applied to initial state value
            //$timeout(function(){
            //    $scope.$watch(function () {
            //        return $($element).find('.btn').length; // it checks if the buttons are added to the DOM
            //    }, function (newVal) {
            //        // it applies the selected style to the currently defined state, if any
            //        if (newVal > 0) {
            //            $($element).find('.btn').each(function (index, elm) {
            //                //console.log("elm file",elm);
            //                if ($(elm).data('state') == $scope.state) $(elm).addClass('active');
            //            });
            //        }
            //    }, true);
            //},0)
            $scope.setValueBtn = function(stateValue) {
                var elemLength = $($element).find('.btn').length;
                if (elemLength > 0) {
                    $element.find('.btn').removeClass('active');
                    $($element).find('.btn').each(function(index, elm) {
                        if ($(elm).data('state') == stateValue) $(elm).addClass('active');
                    });
                }
            }

            // Apply style changes according to selection
            $scope.select = function(s, evt) {
                if (s) {
                    $scope.state = s.key;
                    $element.find('.btn').removeClass('active'); // reset styles on all buttons
                    //                angular.element(evt.srcElement).addClass('active'); // apply style only to selected button
                    $(evt.target).addClass('active'); // apply style only to selected button
                } else {
                    $element.find('.btn').removeClass('active');
                }
            };

            // Execute callback if it was provided
            $scope.$watch('state', function(newValue, oldValue) {
                console.log("sate value", newValue, oldValue);
                if (newValue != null) {
                    $timeout(function() {
                        $scope.setValueBtn(newValue);
                    }, 3);

                    if ($scope.onStateChange) {
                        $scope.onStateChange();
                    }
                } else {
                    $scope.select(newValue);
                }

            }, true);
        }
    };
}]);