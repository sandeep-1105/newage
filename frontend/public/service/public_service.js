appNewAge.service('publicService', ['$http','$q','$log', function($http,$q,$log){

	this.weightDecimalPoint=2;
	
	this.dateToString = function(parsedate,format){
		return moment(parsedate).format(format); 
	}
	
	this.dateAndTimeToString = function(parsedate,format){
		return moment(parsedate).format(format); 
	}

	this.currencyRoeFormat = function (currencyMaster, amount){
    	
    	if(amount===undefined || amount===null || amount==='')
    		return null;
    	
    	if(currencyMaster == undefined || currencyMaster == null || currencyMaster == "") 
    		return null;
    	
    	return amount.toLocaleString(currencyMaster.countryMaster.locale, { minimumFractionDigits: currencyMaster.decimalPoint, maximumFractionDigits: 6 })
    }

	this.currencyFormat = function (currencyMaster, amount){
    	
    	if(amount===undefined || amount===null || amount==='')
    		return null;
    	
    	if(currencyMaster == undefined || currencyMaster == null || currencyMaster == "") 
    		return null;
    	
    	return amount.toLocaleString(currencyMaster.countryMaster.locale, { minimumFractionDigits: currencyMaster.decimalPoint, maximumFractionDigits: currencyMaster.decimalPoint })
    }
    
	this.integerFormat = function (number, locale){
    	if(number===undefined || number===null || number==='')
    		return null;
    	return number.toLocaleString(locale, { minimumFractionDigits: 0, maximumFractionDigits: 0 })
    }
    
	this.weightFormat = function (weight, locale){
    	if(weight===undefined || weight===null || weight==='')
    		return null;
    	return weight.toLocaleString(locale, { minimumFractionDigits: this.weightDecimalPoint, maximumFractionDigits: this.weightDecimalPoint })
    }
	
	
	
	
	
	
	
	
	//Ajax Calls
	
	this.chargeSearch=function(searchDto,saasId,urlPath){
		
             var request = $http({
                 method: "post",
                 headers: {'SAAS_ID': saasId},
                 url: urlPath+"/public/api/v1/chargemaster/get/search/keyword",
                 data:searchDto
             });
             return(request.then( handleSuccess, handleError ) );
		
	}
	
   this.unitSearch=function(searchDto,saasId,urlPath){
		
		
	   var request = $http({
           method: "post",
           headers: {'SAAS_ID': saasId},
           url: urlPath+"/public/api/v1/unitmaster/get/search/keyword",
           data:searchDto
       });
       return(request.then( handleSuccess, handleError ) );
		
	}
   
   this.currencySearch=function(searchDto,saasId,urlPath){
		
	   var request = $http({
           method: "post",
           headers: {'SAAS_ID': saasId},
           url:urlPath+"/public/api/v1/currencymaster/get/search/keyword",
           data:searchDto
       });
       return(request.then( handleSuccess, handleError ) );
	   
	}
   this.unitCodeSearch=function(unitCode,saasId,urlPath){
		
	   var request = $http({
           method: "get",
           headers: {'SAAS_ID': saasId},
           url: "/public/api/v1/unitmaster/get/code/"+unitCode,
       });
	   return(request.then( handleSuccess, handleError ) );
   }
	
   
   this.enquiryDetailSave=function(rateRequest,saasId,urlPath){
		
	   var request = $http({
           method: "post",
           headers: {'SAAS_ID': saasId},
           url:urlPath+"/public/api/v1/enquiry/enquirydetail/save",
           data:rateRequest
       });
       return(request.then( handleSuccess, handleError ) );
	}
   
	
   function handleError( response ) {
       // The API response from the server should be returned in a
       // nomralized format. However, if the request was not handled by the
       // server (or what not handles properly - ex. server error), then we
       // may have to normalize it on our end, as best we can.
       if (
           ! angular.isObject( response.data ) ||
           ! response.data.message
           ) {
           return( $q.reject( "An unknown error occurred." ) );
       }
       // Otherwise, use expected error message.
       return( $q.reject( response.data.message ) );
   }
   // I transform the successful response, unwrapping the application data
   // from the API response payload.
   function handleSuccess( response ) {
	   if(response.data!=null && response.data.responseObject){
		   return( response.data.responseObject);
	   }else{
		   return (response.data);
	   }
   }
	
	
	
	
	
	
	
	
	
	
	
	
}]);