
var Utils = (function(){

 return {
    showDebugLogs: function () { 
    	if(!SHOW_DEBUG_LOGS){
    	    if(!window.console) window.console = {};
    	    var methods = ["log", "debug", "warn", "info"];
    	    for(var i=0;i<methods.length;i++){
    	        console[methods[i]] = function(){};
    	    }
    	}
    }
 }

}())