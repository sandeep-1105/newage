const browserSync = require("browser-sync").create();
var gulp = require('gulp');
var fs = require('fs');
var file = require('gulp-file');
var domSrc = require('gulp-dom-src');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var cheerio = require('gulp-cheerio');
var cleanDest = require('gulp-clean-dest');
var clean = require('gulp-clean');
var cleancss = require('gulp-clean-css');
var minifyJS = require('gulp-minify');
var gutil = require('gulp-util');
var less = require('gulp-less');
var concatCss = require('gulp-concat-css');
var isProduction = true;

var destBuildPath = '../backend/core/build/resources/main/static';

/****
Copying files from app folder to destination app folder
****/
gulp.task('app_to_build', function () {
    return gulp.src(['app/**/*'])
        .pipe(cleanDest(destBuildPath))
        .pipe(gulp.dest(destBuildPath + '/app'));
})

/****
Copying files from assets folder to destination assets folder
****/
gulp.task('assets_to_build', function () {
    return gulp.src(['assets/**/*'])
        .pipe(gulp.dest(destBuildPath + '/assets'));
})


/****
Copying files from css folder to destination css folder
****/
gulp.task('css_to_build', function () {
    return gulp.src(['css/**/*'])
        .pipe(gulp.dest(destBuildPath + '/css'));
})

/****
Copying files from document-templates folder to destination document-templates folder
****/
gulp.task('document_templates_to_build', function () {
    return gulp.src(['document-templates/**/*'])
        .pipe(gulp.dest(destBuildPath + '/document-templates'));
})


/****
Copying files from partials folder, Concatenate all less files and convert to css format.
****/
gulp.task('less_to_css', function () {
    return gulp.src(['main.less'])
        .pipe(less())
        .pipe(concat('main.css'))
        .pipe(cleancss())
        .pipe(gulp.dest(destBuildPath));
})

/****
Copying files from plugins folder to destination plugins folder
****/
gulp.task('plugins_to_build', function () {
    return gulp.src(['plugins/**/*'])
        .pipe(gulp.dest(destBuildPath + '/plugins'));
})

/****
Copying files from public folder to destination public folder
****/
gulp.task('public_to_build', function () {
    return gulp.src(['public/**/*'])
        .pipe(gulp.dest(destBuildPath + '/public'));
})


/****
Copying main_layout file from frontend folder to destination static folder
****/
gulp.task('main_layout_to_build', function () {
    var htmlContent = fs.readFileSync("mainLayout.html", "utf8");
    return file('mainLayout.html', htmlContent, {
        src: true
    })
        .pipe(gulp.dest(destBuildPath));
})

/**
    copy boot.js file to build folder
*/
gulp.task('config_to_build' ,function(){
    return gulp.src(["config.js","utils.js"])
    .pipe(gulp.dest(destBuildPath));
})

/****
Copying panel-details file from frontend folder to destination static folder
****/
gulp.task('panel_details_to_build', function () {
    var htmlContent = fs.readFileSync("paneldetails.html", "utf8");
    return file('paneldetails.html', htmlContent, {
        src: true
    })
        .pipe(gulp.dest(destBuildPath));
})


var devMode = false;
/****
this task will run all the task listed as an array in second parameter.
****/
gulp.task('copy', [
    'app_to_build',
    'assets_to_build',
    'css_to_build',
    'document_templates_to_build',
    'less_to_css',
    'plugins_to_build',
    'public_to_build',
    'main_layout_to_build',
    'panel_details_to_build',
    'config_to_build',
    'user_registration_build',
    'user_registration_libjs',
    'user_registration_index',
    'bundle_vendor_files',
    'replace_tags_in_indexHTML',
    'bundle_components_files',
    'vendor_css'
    //'user_registration_css'

]);

gulp.task('browser-sync', function () {
    console.log(destBuildPath + "/index.html");
    browserSync.init(null, {
        open: true,
        server: {
            baseDir: destBuildPath
        },
        //startPath: "/index.html"
    });
});

gulp.task('start', function () {
    devMode = true;
    gulp.start(['copy', 'browser-sync']);
    /*gulp.watch(['index.html'], [destBuildPath]);*/
    /*gulp.watch("*.html").on('change', browserSync.reload);*/
})
/***
Cleaning destination folder
***/
//gulp.task('clean',['cleanBuild'])

gulp.task('clean', function () {
    return gulp.src(destBuildPath, { read: false })
        .pipe(clean({ force: true }))
})

/*******************************************************
    For Signup / Registration Module : Start
********************************************************/

/****
creating single js file from custom js files and put to destination folder
**/
gulp.task('user_registration_build', function () {
    htmlContent = fs.readFileSync("public/user_registration/user_registration.html", "utf8");
    //console.log(">>>>>>>>>> ", htmlContent);
    objReg = /\<\!--user registration start--\>[\s\S]*\<\!--user registration end--\>/gi;
    strMatch = htmlContent.match(objReg)[0];
    objReg = /\.\/[\s\S]*?(.js)/gi;
    arrAppScriptFilePaths = strMatch.match(objReg);
    console.log("arr ", arrAppScriptFilePaths);
    return gulp.src(arrAppScriptFilePaths)
        .pipe(concat('components.js'))
        .pipe(uglify({ mangle: false }))
        .on('error', function (err) { 
            gutil.log(gutil.colors.red('[Error]'), err.toString()); 
            throw err;
        })
        .pipe(gulp.dest(destBuildPath + '/public/user_registration'))
});

/****
creating single js file from vendor js files and put to destination folder
**/
gulp.task('user_registration_libjs', function () {
    htmlContent = fs.readFileSync("public/user_registration/user_registration.html", "utf8");
    //console.log(">>>>>>>>>> ", htmlContent);
    objReg = /\<\!--user registration libjs start--\>[\s\S]*\<\!--user registration libjs end--\>/gi;
    strMatch = htmlContent.match(objReg)[0];
    console.log('strMatch', strMatch);
    objReg = /\.\/[\s\S]*?(.js)/gi;
    arrAppScriptFilePaths = strMatch.match(objReg);
    console.log("arr ", arrAppScriptFilePaths);
    return gulp.src(arrAppScriptFilePaths)
        .pipe(concat('vendor.js'))
        // .pipe(uglify())
        .on('error', function (err) { 
            gutil.log(gutil.colors.red('[Error]'), err.toString()); 
            throw err;
        })
        .pipe(gulp.dest(destBuildPath + '/public/user_registration'))
});

/****
creating single js file from custom js files and put to destination folder
**/
var arrAppCSSFilePaths = [];
gulp.task('user_registration_css', function () {
    var htmlContent = fs.readFileSync("public/user_registration/user_registration.html", "utf8");
    var objReg = /\<\!--App CSS starts here--\>[\s\S]*\<\!--App CSS ends here--\>/gi;
    var strMatch = htmlContent.match(objReg)[0];
    console.log('strMatch ', strMatch);
    objReg = /\.\/[\s\S]*?(.css)/gi;

    arrAppCSSFilePaths = strMatch.match(objReg);
    console.log("arrAppCSSFilePaths", arrAppCSSFilePaths);
    return gulp.src(arrAppCSSFilePaths)
        .pipe(concat('app.css'))
        .pipe(cleancss())
        .pipe(gulp.dest(destBuildPath + '/public/user_registration/css/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});


/***
Replacing script tag with the newly created js
***/
gulp.task('user_registration_index', function indexhtml() {
    htmlContent = fs.readFileSync("public/user_registration/user_registration.html", "utf8");
    var objReg = /\<\!--user registration start--\>[\s\S]*\<\!--user registration end--\>/gi;
    htmlContent = htmlContent.replace(objReg, '<script src="components.js"></script>');

    objReg = /\<\!--user registration libjs start--\>[\s\S]*\<\!--user registration libjs end--\>/gi;
    htmlContent = htmlContent.replace(objReg, '<script src="vendor.js"></script>');

    /*objReg = /\<\!--App CSS starts here--\>[\s\S]*\<\!--App CSS ends here--\>/gi;
    htmlContent = htmlContent.replace(objReg, '<link href="plugins/css/app.css" rel="stylesheet">');*/

    return file('registration.html', htmlContent, {
        src: true
    })
        .pipe(gulp.dest(destBuildPath + '/public/user_registration'));
});


/*******************************************************
    For Signup / Registration Module : End
********************************************************/




/*******************************************************
    For Login Module : Start
********************************************************/


/***
Replacing script tag with the newly created js
***/
gulp.task('replace_tags_in_indexHTML', function indexhtml() {
    console.log('isProduction  ', isProduction)
    htmlContent = fs.readFileSync("index.html", "utf8");
    var objReg = /\<\!--index_vendor start--\>[\s\S]*\<\!--index_vendor end--\>/gi;
    if (isProduction) {
        console.log('isProduction  ', 'correct');
        htmlContent = htmlContent.replace(objReg, '<script src="vendor.js"></script>');
    }

    objReg = /\<\!--index_components start--\>[\s\S]*\<\!--index_components end--\>/gi;
    if (isProduction) {
        htmlContent = htmlContent.replace(objReg, '<script src="components.js"></script>');
    }

    objReg = /\<\!--App CSS starts here--\>[\s\S]*\<\!--App CSS ends here--\>/gi;
    if (isProduction) {
        htmlContent = htmlContent.replace(objReg, '<link href="plugins/css/app.css" rel="stylesheet">');
    }

    return file('index.html', htmlContent, {
        src: true
    })
        .pipe(gulp.dest(destBuildPath));
});


/****
    creating single js file from vendor js files and put to destination folder
**/
gulp.task('bundle_vendor_files', function () {
    console.log('isProduction  ', isProduction)
    if (!isProduction) return;
    console.log('isProduction  ', 'correct');
    htmlContent = fs.readFileSync("index.html", "utf8");
    //console.log(">>>>>>>>>> ", htmlContent);
    objReg = /\<\!--index_vendor start--\>[\s\S]*\<\!--index_vendor end--\>/gi;
    strMatch = htmlContent.match(objReg)[0];
    objReg = /\.\/[\s\S]*?(.js)/gi;
    arrAppScriptFilePaths = strMatch.match(objReg);
    console.log("arr ", arrAppScriptFilePaths);
    return gulp.src(arrAppScriptFilePaths)
        .pipe(concat('vendor.js'))
        .pipe(uglify({ mangle: false }))
        .on('error', function (err) { 
            gutil.log(gutil.colors.red('[Error]'), err.toString()); 
            throw err;
        })
        .pipe(gulp.dest(destBuildPath))
});

/****
    creating single js file from vendor js files and put to destination folder
**/
gulp.task('bundle_components_files', function () {
    if (!isProduction) return;
    htmlContent = fs.readFileSync("index.html", "utf8");
    //console.log(">>>>>>>>>> ", htmlContent);
    objReg = /\<\!--index_components start--\>[\s\S]*\<\!--index_components end--\>/gi;
    strMatch = htmlContent.match(objReg)[0];
    objReg = /\.\/[\s\S]*?(.js)/gi;
    arrAppScriptFilePaths = strMatch.match(objReg);
    console.log("arr ", arrAppScriptFilePaths);
    return gulp.src(arrAppScriptFilePaths)
        .pipe(concat('components.js'))
        .pipe(uglify({ mangle: false }))
        .on('error', function (err) { 
            gutil.log(gutil.colors.red('[Error]'), err.toString()); 
            throw err;
        })
        .pipe(gulp.dest(destBuildPath))
});

/****
creating single js file from custom js files and put to destination folder
**/
var arrAppCSSFilePaths = [];
gulp.task('vendor_css', function () {
    if (!isProduction) return;
    var htmlContent = fs.readFileSync("index.html", "utf8");
    var objReg = /\<\!--App CSS starts here--\>[\s\S]*\<\!--App CSS ends here--\>/gi;
    var strMatch = htmlContent.match(objReg)[0];
    console.log('strMatch ', strMatch);
    objReg = /\.\/plugins\/[\s\S]*?(.css)/gi;

    arrAppCSSFilePaths = strMatch.match(objReg);
    console.log("arrAppCSSFilePaths", arrAppCSSFilePaths);
    return gulp.src(arrAppCSSFilePaths)
        .pipe(concatCss('app.css'))
        .pipe(cleancss())
        .pipe(gulp.dest(destBuildPath + '/plugins/css'))

});


/*******************************************************
    For Login Module : End
********************************************************/

/****
Copy resource from back end folder to frontend folder.
****/
gulp.task('static_content', function () {
    return gulp.src('../backend/core/src/main/resources/static/**/*')
        .pipe(gulp.dest('./'));
})

gulp.task('default', ['copy']);
